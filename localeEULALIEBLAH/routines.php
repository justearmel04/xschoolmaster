<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Salle.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$sallex=new Salle();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

  if($_SESSION['user']['groupe']==1)
  {
    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
    {
      $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);

      //nous allons chercher la liste des etablissements du groupe

      $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
        }

        if(strlen($_SESSION['user']['codeEtab'])>0)
        {

        }else {
          $_SESSION['user']['codeEtab']=$codeEtabAssigner;
        }

        $_SESSION['user']['session']=$libellesessionencours;





         // var_dump($allcodeEtabs);

        $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

        $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


    }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
    {
      $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);
      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
          $_SESSION['user']['session']=$libellesessionencours;

          if(strlen($_SESSION['user']['codeEtab'])>0)
          {

          }else {
            $_SESSION['user']['codeEtab']=$codeEtabAssigner;
          }

          $notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

          // $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
          $classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);
          $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);
        }

    }
  }

$allLibellesHours=$etabs->getHoursAllLibs($_SESSION['user']['codeEtab'],$libellesessionencours);

  // var_dump($allcodeEtabs);
  $nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
// echo $libellesessionencours;
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>

   <link href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css" rel="stylesheet" >
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::EmploisduTemps ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::EmploisduTemps ?></li>
                            </ol>
                        </div>
                    </div>

                    <?php

                          if(isset($_SESSION['user']['addprogra']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                    <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: '<?php echo L::Felicitations ?>',
                      text: '<?php echo $_SESSION['user']['addprogra'] ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['addprogra']);
                          }

                           ?>
					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['updateroutineok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
        title: '<?php echo L::Felicitations ?>',
        text: "<?php echo $_SESSION['user']['updateroutineok']; ?>",
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '<?php echo L::AddroutinesNewCourse ?>',
        cancelButtonText: '<?php echo L::AnnulerBtn ?>',
        }).then((result) => {
        if (result.value) {
          document.location.href="routinesadd.php";
        }else {

        }
        })
            </script>
                  <?php
                  unset($_SESSION['user']['updateroutineok']);
                }

                 ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>
                 <div class="pull-right">


                     <?php
                     if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="Coordinateur"||$_SESSION['user']['fonctionuser']=="CC"||$_SESSION['user']['fonctionuser']=="CD"||$_SESSION['user']['fonctionuser']=="DG")
                    {
                     ?>
                      <!-- <a class="btn btn-primary " href="routinesadd.php"><i class="fa fa-plus-circle"> <?php //echo L::CoursesAdding ?></i></a> -->
                     <?php
                   }
                    ?>
                    <?php
                    if($_SESSION['user']['fonctionuser']=="CAES"|| $_SESSION['user']['fonctionuser']=="Accountant")
                    {

                    }else {
                      ?>
                      <a class="btn btn-warning" target="_blank" href="scheduletabclasse.php?classe=<?php echo $_GET['classe']; ?>&codeEtab=<?php echo $_SESSION['user']['codeEtab']; ?>&session=<?php echo $libellesessionencours; ?>" ><i class="fa fa-print"> <?php echo L::EmploisduTemps ?></i></a>
                      <?php
                    }
                     ?>


                   <!-- <a class="btn btn-warning " href="#" onclick="generetedroutines(<?php //echo $_GET['classe']; ?>,'<?php //echo $codeEtabsession; ?>','<?php //echo $libellesessionencours; ?>')"><i class="fa fa-print"> Emploi du temps</i></a> -->
                   <!-- <a class="btn btn-success " href="#" data-toggle="modal" data-target="#smallModel" ><i class="fa fa-send"> <?php //echo L::Sending ?></i></a> -->

                   <div class="modal fade" id="smallModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					    <div class="modal-dialog modal-md" role="document">
					        <div class="modal-content">
					            <div class="modal-header">
					                <h4 class="modal-title" id="exampleModalLabel" style="color: Dodgerblue;"> <i class="fa fa-send "></i> <?php echo L::ByEmailling ?> </h4>
					                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					                    <span aria-hidden="true">&times;</span>
					                </button>
					            </div>
					            <div class="modal-body">

                          <div class="form-group row">
                              <label class="control-label col-md-4"><?php echo L::Destinatairesingle ?>
                                  <span class="required">*  </span>
                              </label>
                              <div class="col-md-8">
                                <select class="form-control" id="desti" name="desti" style="width:100%" onchange="searchmail()">
                                    <option value=""><?php echo L::SelectDestinatairesingle ?></option>
                                    <option value="1"><?php echo L::AdminsMenu ?></option>
                                    <option value="2"><?php echo L::ParentsingleMenu ?></option>
                                    <option value="3"><?php echo L::TeatcherCapsingles ?></option>


                                </select>
                                </div>
                          </div>
                          <div class="form-group row">
                              <label class="control-label col-md-4"><?php echo L::NamestudentTab ?>
                                  <span class="required">*  </span>
                              </label>
                              <div class="col-md-8">
                                <select class="form-control" id="destimail" name="destimail" style="width:100%" onchange="selectmail()">
                                    <option value=""><?php echo L::SelectDestinatairesingle ?></option>
                                </select>
                                </div>
                          </div>
                          <div class="form-group row">
                              <label class="control-label col-md-4"><?php echo L::EmailstudentTab ?>
                                  <span class="required">*  </span>
                              </label>
                              <div class="col-md-8">
                                <input type="text" class="form-control" id="mail" name="mail"  style="width:100%" readonly>

                                </div>
                          </div>

					            </div>
					            <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="sendbutton" disabled onclick="send()"><?php echo L::Sendbutton ?></button>
					                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo L::Closebtn ?></button>

					            </div>
					        </div>
					    </div>
					</div>

          <a href="#" style="display:none" id="matieremodal" data-toggle="modal" data-target="#smallModelmatiere"></a>
          <div class="modal fade" id="smallModelmatiere" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel"><?php echo L::AddaMatiere ?></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                      <form class="" id="FormAddmatiere" action="#" method="post">
                        <div id="divrecherchestation">

                          <div class="row">



                              <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                                <!-- <div class="blogThumb"> -->

                                  <div class="course-box">

                                    <div class="" style="text-align:center">
                                      <h4>

                                              <!-- <span class="label label-lg label-warning" id="spaninfo"  style="text-align:center;">fffffffffff</span> -->


                                      </h4>
                                    </div>
                                  </div>

                                    <div class="col-md-12">
                                     <div class="form-group">
                         <label for="matiere"><?php echo L::MatiereMenusingle ?><span class="required">*</span> :</label>
                                <select class="form-control" name="matiere" id="matiere" style="width:100%">

                                </select>
                                <input type="hidden" id="libelleheureid" name="libelleheureid" value="">
                                <input type="hidden" name="shortday" id="shortday" value="">
                                <input type="hidden" name="hours" id="hours" value="">
                                <input type="hidden" name="codeEtab" id="codeEtab" value="">
                                <input type="hidden" name="sessionEtab" id="sessionEtab" value="">
                                <input type="hidden" name="classeid" id="classeid" value="">

                                         </div>
                                         <div class="form-group">
                             <label for="salle"><?php echo L::SallesingleMenu ?><span class="required">*</span> :</label>
                                    <select class="form-control" name="salle" id="salle" style="width:100%">

                                    </select>



                                             </div>
                                         <div class="form-actions">
                                                               <div class="row">
                                                                   <div class="col-md-9">
                                                                       <button type="submit" class="btn btn-info"><?php echo L::AddMenu ?></button>
                                                                       <!-- <button type="button" class="btn btn-danger"><?php //echo L::AnnulerBtn ?></button> -->
                                                                   </div>
                                                                 </div>
                                                              </div>
                                      </div>




                                <!-- </div> -->
                              </div>




                          </div>

                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closebtn"><?php echo L::Closebtn  ?></button>
                        <button type="button" style="display:none" class="btn btn-primary" onclick="validerMember()" id="btnsaving"><?php echo L::Saving ?></button>
                    </div>
                </div>
            </div>
 </div>
 <a href="#" style="display:none" id="libmodal" data-toggle="modal" data-target="#modalroutine"></a>
 <div class="modal fade" id="modalroutine" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-md" role="document">
       <div class="modal-content">
           <div class="modal-header">
               <h4 class="modal-title" id="exampleModalLabel"><?php echo L::AddaLibelle ?></h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
           </div>
           <div class="modal-body">
             <form class="" id="FormAddlib" action="#" method="post">
               <div id="divrecherchestation">

                 <div class="row">



                     <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                       <!-- <div class="blogThumb"> -->

                         <div class="course-box">

                           <div class="" style="text-align:center">
                             <h4>

                                     <!-- <span class="label label-lg label-warning" id="spaninfo"  style="text-align:center;">fffffffffff</span> -->


                             </h4>
                           </div>
                         </div>

                           <div class="col-md-12">
                            <div class="form-group">
                <label for="libelleroutine"><?php echo L::Lib ?><span class="required">*</span> :</label>
                      <input type="text" class="form-control" name="libelleroutine" id="libelleroutine" value="">
                       <input type="hidden" id="libelleheureid" name="libelleheureid" value="">
                       <input type="hidden" name="shortday" id="shortday" value="">
                       <input type="hidden" name="hours" id="hours" value="">
                       <input type="hidden" name="codeEtab" id="codeEtab" value="">
                       <input type="hidden" name="sessionEtab" id="sessionEtab" value="">
                       <input type="hidden" name="classeid" id="classeid" value="">

                                </div>
                                <div class="form-actions">
                                                      <div class="row">
                                                          <div class="col-md-9">
                                                              <button type="submit" class="btn btn-info"><?php echo L::AddMenu ?></button>
                                                              <!-- <button type="button" class="btn btn-danger"><?php //echo L::AnnulerBtn ?></button> -->
                                                          </div>
                                                        </div>
                                                     </div>
                             </div>




                       <!-- </div> -->
                     </div>




                 </div>

               </div>
             </form>
           </div>
           <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closebtn"><?php echo L::Closebtn  ?></button>
               <button type="button" style="display:none" class="btn btn-primary" onclick="validerMember()" id="btnsaving"><?php echo L::Saving ?></button>
           </div>
       </div>
   </div>
</div>

<a href="#" style="display:none" id="matiereUpmodal" data-toggle="modal" data-target="#smallModelmatiereUp"></a>
<div class="modal fade" id="smallModelmatiereUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="exampleModalLabel"><?php echo L::AddaMatiere ?></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <form class="" id="FormUpdatematiere" action="#" method="post">
              <div id="divrecherchestation">

                <div class="row">



                    <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                      <!-- <div class="blogThumb"> -->

                        <div class="course-box">

                          <div class="" style="text-align:center">
                            <h4>

                                    <!-- <span class="label label-lg label-warning" id="spaninfo"  style="text-align:center;">fffffffffff</span> -->


                            </h4>
                          </div>
                        </div>

                          <div class="col-md-12">
                           <div class="form-group">
               <label for="matiere"><?php echo L::MatiereMenusingle ?><span class="required">*</span> :</label>
                      <select class="form-control" name="matiereUp" id="matiereUp" style="width:100%">

                      </select>
                      <input type="hidden" id="libelleheureid" name="libelleheureid" value="">
                      <input type="hidden" name="shortday" id="shortday" value="">
                      <input type="hidden" name="hours" id="hours" value="">
                      <input type="hidden" name="codeEtab" id="codeEtab" value="">
                      <input type="hidden" name="sessionEtab" id="sessionEtab" value="">
                      <input type="hidden" name="classeid" id="classeid" value="">
                      <input type="hidden" name="routeid" id="routeid" value="">

                               </div>
                               <div class="form-actions">
                                                     <div class="row">
                                                         <div class="col-md-9">
                                                             <button type="submit" class="btn btn-info"><?php echo L::AddMenu ?></button>
                                                             <!-- <button type="button" class="btn btn-danger"><?php //echo L::AnnulerBtn ?></button> -->
                                                         </div>
                                                       </div>
                                                    </div>
                            </div>




                      <!-- </div> -->
                    </div>




                </div>

              </div>
            </form>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closebtn"><?php echo L::Closebtn  ?></button>
              <button type="button" style="display:none" class="btn btn-primary" onclick="validerMember()" id="btnsaving"><?php echo L::Saving ?></button>
          </div>
      </div>
  </div>
</div>

<a href="#" style="display:none" id="libUpmodal" data-toggle="modal" data-target="#modalroutineUp"></a>
<div class="modal fade" id="modalroutineUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="exampleModalLabel"><?php echo L::AddaLibelle ?></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <form class="" id="FormAddlibUp" action="#" method="post">
              <div id="divrecherchestation">

                <div class="row">



                    <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                      <!-- <div class="blogThumb"> -->

                        <div class="course-box">

                          <div class="" style="text-align:center">
                            <h4>

                                    <!-- <span class="label label-lg label-warning" id="spaninfo"  style="text-align:center;">fffffffffff</span> -->


                            </h4>
                          </div>
                        </div>

                          <div class="col-md-12">
                           <div class="form-group">
               <label for="libelleroutine"><?php echo L::Lib ?><span class="required">*</span> :</label>
                     <input type="text" class="form-control" name="libelleUproutine" id="libelleUproutine" value="">
                      <input type="hidden" id="libelleheureid" name="libelleheureid" value="">
                      <input type="hidden" name="shortday" id="shortday" value="">
                      <input type="hidden" name="hours" id="hours" value="">
                      <input type="hidden" name="codeEtab" id="codeEtab" value="">
                      <input type="hidden" name="sessionEtab" id="sessionEtab" value="">
                      <input type="hidden" name="classeid" id="classeid" value="">

                               </div>
                               <div class="form-actions">
                                                     <div class="row">
                                                         <div class="col-md-9">
                                                             <button type="submit" class="btn btn-info"><?php echo L::AddMenu ?></button>
                                                             <!-- <button type="button" class="btn btn-danger"><?php //echo L::AnnulerBtn ?></button> -->
                                                         </div>
                                                       </div>
                                                    </div>
                            </div>




                      <!-- </div> -->
                    </div>




                </div>

              </div>
            </form>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closebtn"><?php echo L::Closebtn  ?></button>
              <button type="button" style="display:none" class="btn btn-primary" onclick="validerMember()" id="btnsaving"><?php echo L::Saving ?></button>
          </div>
      </div>
  </div>
</div>



                 </div>

<br/><br/>
          <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card card-topline-green">
                                                    <div class="card-head">
                                                        <header><?php echo L::RoutinesByClasses ?></header>
                                                        <div class="tools">
                                                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
            												<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
            												<a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                                        </div>
                                                    </div>
                                                    <div class="card-body ">
                                        <div class="table-responsive">
                                            <table class="table table-hover" id='tableone'>
                                              <?php
                                                $dataday=$etabs->getAllweeksLimit();
                                                $tabLibday=array();
                                                $tabhoraires=array();
                                                $tabidHeures=array();
                                                $tabshortday=array();
                                                $i=0;
                                                $j=0;
                                                $w=1;
                                                $z=1;

                                                  foreach ($dataday as $value):
                                                    if($_SESSION['user']['lang']=="fr")
                                                    {
                                                    $tabLibday[$z]=$value->libelle_days;
                                                    }else {
                                                    $tabLibday[$z]=$value->libelleen_days;
                                                    }
                                                     $tabshortday[$z]=$value->short_days;
                                                    $i++;
                                                    $z++;

                                                  endforeach;

                                                  // echo $i;

                                                    foreach ($allLibellesHours as $value1):
                                                      $tabidHeures[$w]=$value1->id_heure;
                                                      $tabhoraires[$w]=returnHours($value1->heuredeb_heure). " - ".returnHours($value1->heurefin_heure);
                                                      $j++;
                                                      $w++;
                                                    endforeach;
                                                    // echo $j;
                                                    // var_dump($tabhoraires);

                                                    for($k=0;$k<=$j;$k++)
                                                    {
                                                      ?>
                                                        <tr>
                                                        <?php
                                                        for($u=0;$u<=$i;$u++)
                                                        {
                                                          if($k==0)
                                                          {
                                                            ?>

                                                              <?php
                                                              if($u==0)
                                                              {
                                                                ?>
                                                                <td style="border: 1px solid #333;width:20%;text-align:center"><?php echo L::Times ?></td>
                                                                <?php

                                                              }else {
                                                                ?>
                                                                <td style="border: 1px solid #333;width:20%;text-align:center" ><?php echo $tabLibday[$u]; ?></td>
                                                                <?php
                                                              }
                                                               ?>

                                                            <?php

                                                          }else {

                                                            if($u==0)
                                                            {
                                                              // echo $w;
                                                              ?>
                                                              <td style="border: 1px solid #333;"><?php echo $tabhoraires[$k];



                                                              ?></td>
                                                              <?php
                                                            }else {
                                                              if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="Coordinateur"||$_SESSION['user']['fonctionuser']=="CC"||$_SESSION['user']['fonctionuser']=="CD"||$_SESSION['user']['fonctionuser']=="DG")
                                                             {
                                                               ?>
                                                                 <td style="border: 1px solid #333;" onclick="determination(<?php echo $tabidHeures[$k] ?>,'<?php echo $tabshortday[$u] ?>','<?php echo $tabhoraires[$k] ?>')">
                                                               <?php
                                                             }else {
                                                               ?>
                                                               <td>
                                                               <?php
                                                             }
                                                              ?>

                                                                <?php
                                                              $nbmatiereday=$etabs->getnumberofmatierethisdayNew($tabidHeures[$k],$tabshortday[$u],$_SESSION['user']['codeEtab'],$libellesessionencours,$_GET['classe']);
                                                              if($nbmatiereday>0)
                                                              {
                                                                // $datas=$etabs->getMatierethisday($tabidHeures[$k],$tabshortday[$u],$_SESSION['user']['codeEtab'],$libellesessionencours,$_GET['classe']);
                                                                $datas=$etabs->getRoutinesLibellename($tabidHeures[$k],$tabshortday[$u],$_SESSION['user']['codeEtab'],$libellesessionencours,$_GET['classe']);
                                                                // var_dump($datas);
                                                                $tabdatas=explode("*",$datas);
                                                                ?>
                                                                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">


                                <div class="btn-group" role="group">
                                  <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?php echo $tabdatas[0];?> <br>
                                    <?php echo $sallex->getLibelleSallesByschoolCodewithId($_SESSION['user']['codeEtab'],$tabdatas[2]);?>
                                  </button>

                                     <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                       <a class="dropdown-item" style="display:none" href="#" onclick="modifyRoutines(<?php echo $tabdatas[1] ?>,<?php echo $tabidHeures[$k] ?>,'<?php echo $tabshortday[$u] ?>','<?php echo $tabhoraires[$k] ?>')"><i class="fa fa-pencil"></i> <?php echo L::ModifierBtn ?></a>
                                        <a class="dropdown-item" href="#" onclick="deletedRoutines(<?php echo $tabdatas[1] ?>,<?php echo $tabidHeures[$k] ?>,'<?php echo $tabshortday[$u] ?>','<?php echo $tabhoraires[$k] ?>')"><i class="fa fa-trash-o"></i> <?php echo L::DeleteLib ?></a>
                                      </div>


                                </div>
                              </div>
                              <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                              <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                              <script>

                              function myFunction(idcompte)
                              {
                                //var url="detailslocal.php?compte="+idcompte;
                              document.location.href="detailsadmin.php?compte="+idcompte;
                              }

                              function modify(id)
                              {
                                var classeid="<?php echo $_GET['classe']; ?>";

                                Swal.fire({
                              title: '<?php echo L::WarningLib ?>',
                              text: "<?php echo L::DoyouReallyModifyingThisCourses ?>",
                              type: 'warning',
                              showCancelButton: true,
                              confirmButtonColor: '#3085d6',
                              cancelButtonColor: '#d33',
                              confirmButtonText: '<?php echo L::ModifierBtn ?>',
                              cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                              }).then((result) => {
                              if (result.value) {
                              document.location.href="updateroutine.php?compte="+id+"&classeid="+classeid;
                              }else {

                              }
                              })
                              }

                              function deleted(id,classeid)
{
var codeEtab="<?php echo $_SESSION['user']['codeEtab']; ?>";
var sessionEtab="<?php echo $libellesessionencours; ?>";

                                Swal.fire({
                              title: '<?php echo L::WarningLib ?>',
                              text: "<?php echo L::DoyouReallyDeletingThisCourses ?>",
                              type: 'warning',
                              showCancelButton: true,
                              confirmButtonColor: '#3085d6',
                              cancelButtonColor: '#d33',
                              confirmButtonText: '<?php echo L::DeleteLib ?>',
                              cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                              }).then((result) => {
                              if (result.value) {

                              document.location.href="../controller/routine.php?etape=5&compte="+id+"&classe="+classeid+"&codeEtab="+codeEtab+"&sessionEtab="+sessionEtab;
                              }else {

                              }
                              })
                              }

                              </script>
                                                                <?php
                                                              }

                                                               ?></td>
                                                              <?php
                                                            }

                                                          }
                                                        }
                                                         ?>
                                                      </tr>
                                                      <?php


                                                    }

                                                ?>




                                            </table>
                                            </div>

                                        </div>
                                                </div>
          </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function modifyRoutines(routeid,heurelibid,dayshort,horaires)
   {
     var classe="<?php echo $_GET['classe']; ?>";
     var codeEtab="<?php echo $_SESSION['user']['codeEtab']; ?>";
     var sessionEtab="<?php echo $libellesessionencours; ?>";

     Swal.fire({
   // title: '<?php //echo L::WarningLib ?>',
   text: "<?php echo L::WhatdoyoudoAtthistime ?>",
   type: 'info',
   showCancelButton: true,
   confirmButtonColor: '#3085d6',
   cancelButtonColor: '#19dc49',
   confirmButtonText: '<?php echo L::MatiereMenusingle ?>',
   cancelButtonText: '<?php echo L::Lib ?>',
   }).then((result) => {
   if (result.value) {
     $("#FormUpdatematiere #libelleheureid").val(heurelibid);
     $("#FormUpdatematiere #shortday").val(dayshort);
     $("#FormUpdatematiere #hours").val(horaires);
     $("#FormUpdatematiere #codeEtab").val(codeEtab);
     $("#FormUpdatematiere #sessionEtab").val(sessionEtab);
     $("#FormUpdatematiere #classeid").val(classeid);
     $("#FormUpdatematiere #routeid").val(routeid);

     //nous allons rechercher ce que nous avons a cette heure (matiere ou libelle)

     var etape=5;
     $.ajax({
       url: '../ajax/routines.php',
       type: 'POST',
       async:true,
       data: 'routeid=' +routeid+ '&etape=' + etape+'&classe='+classe+'&heurelibid='+heurelibid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&horaires='+horaires+'&dayshort='+dayshort,
       dataType: 'text',
       success: function (content, statut) {


         $("#FormUpdatematiere #matiereUp").html("");
         $("#FormUpdatematiere #matiereUp").html(content);

       }
     });
     //
     $("#matiereUpmodal").click();
   }else {
     $("#FormAddlibUp #libelleheureid").val(heurelibid);
     $("#FormAddlibUp #shortday").val(dayshort);
     $("#FormAddlibUp #hours").val(horaires);
     $("#FormAddlibUp #codeEtab").val(codeEtab);
     $("#FormAddlibUp #sessionEtab").val(sessionEtab);
     $("#FormAddlibUp #classeid").val(classeid);
     $("#FormAddlibUp #routeid").val(routeid);

     var etape=6;
     $.ajax({
       url: '../ajax/routines.php',
       type: 'POST',
       async:true,
       data: 'routeid=' +routeid+ '&etape=' + etape+'&classe='+classe+'&heurelibid='+heurelibid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&horaires='+horaires+'&dayshort='+dayshort,
       dataType: 'text',
       success: function (content, statut) {


         $("#FormAddlibUp #libelleUproutine").val(content);


       }
     });

     //
     $("#libUpmodal").click();
   }
   })

   }

   function deletedRoutines(routeid,heurelibid,dayshort,horaires)
   {
     var classe="<?php echo $_GET['classe']; ?>";
     var codeEtab="<?php echo $_SESSION['user']['codeEtab']; ?>";
     var sessionEtab="<?php echo $libellesessionencours; ?>";

     Swal.fire({
   title: '<?php echo L::WarningLib ?>',
   text: "<?php echo L::DoyouReallyDeletingThisCourses ?>",
   type: 'warning',
   showCancelButton: true,
   confirmButtonColor: '#3085d6',
   cancelButtonColor: '#d33',
   confirmButtonText: '<?php echo L::DeleteLib ?>',
   cancelButtonText: '<?php echo L::AnnulerBtn ?>',
   }).then((result) => {
   if (result.value) {
     var etape=4;
     $.ajax({
       url: '../ajax/routines.php',
       type: 'POST',
       async:true,
       data: 'routeid=' +routeid+ '&etape=' + etape+'&classe='+classe+'&heurelibid='+heurelibid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&horaires='+horaires+'&dayshort='+dayshort,
       dataType: 'text',
       success: function (content, statut) {


         window.location.reload();

       }
     });

   // document.location.href="../controller/routine.php?etape=5&compte="+id+"&classe="+classeid+"&codeEtab="+codeEtab+"&sessionEtab="+sessionEtab;
   }else {

   }
   })

   }




   function SetcodeEtab(codeEtab)
   {
     var etape=3;
     $.ajax({
       url: '../ajax/sessions.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function send()
   {
     //recuperaion des variables

     var desti=$("#desti").val();
     var destimail=$("#destimail").val();
     var mail=$("#mail").val();
     var codeEtab="<?php echo $_SESSION['user']['codeEtab']; ?>";
     var session="<?php echo $libellesessionencours; ?>";
     var classe="<?php echo $_GET['classe']; ?>";

     //nous allons generer le pdf de

   }

   function selectmail()
   {
      var desti=$("#desti").val();
      var destimail=$("#destimail").val();
      var etape=8;
      var codeEtab="<?php echo $_SESSION['user']['codeEtab']; ?>";
      var session="<?php echo $libellesessionencours; ?>";

      $.ajax({
        url: '../ajax/admission.php',
        type: 'POST',
        async:false,
        data: 'codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&desti='+desti+'&destimail='+destimail,
        dataType: 'text',
        success: function (response, statut) {

          $("#mail").val("");
          $("#mail").val(response);

          //sendbutton

          if(response.length>0)
          {
            $("#sendbutton").removeAttr("disabled");
          }else {
          $("#sendbutton").prop("disabled", true);
          }

        }
      });
   }

   function searchmail()
   {
     var desti=$("#desti").val();
     var etape=7;
     var codeEtab="<?php echo $_SESSION['user']['codeEtab']; ?>";
     var session="<?php echo $libellesessionencours; ?>";
     var classe="<?php echo $_GET['classe']; ?>";

     if(desti!=4)
     {
       $.ajax({
         url: '../ajax/admission.php',
         type: 'POST',
         async:false,
         data: 'codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&desti='+desti+'&classe='+classe,
         dataType: 'text',
         success: function (response, statut) {

           $("#destimail").html("");
           $("#destimail").html(response);

           $("#sendbutton").prop("disabled", true);
           $("#mail").val("");

         }
       });
     }else {
       alert("egale à 4");
     }

   }

   function generetedroutines(classeid,codeEtab,session)
   {
     var etape=3;
     $.ajax({
       url: '../ajax/etat.php',
       type: 'POST',
       async:false,
       data: 'codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&classe='+classeid,
       dataType: 'text',
       success: function (response, statut) {

           window.open(response, '_blank');

       }
     });
   }

   function searchmatiere()
   {
       var codeEtab="<?php echo $_SESSION['user']['codeEtab'];?>";
       var classe="<?php echo $_GET['classe'] ?>";
       var etape=2;
        $.ajax({

          url: '../ajax/matiere.php',
          type: 'POST',
          async:true,
          data: 'code=' + codeEtab+ '&etape=' + etape+'&classe='+classe,
          dataType: 'text',
          success: function (content, statut) {

            $("#FormAddmatiere #matiere").html("");
            $("#FormAddmatiere #matiere").html(content);

          }
        });
   }

   function determination(heurelibid,dayshort,horaires)
   {
     // alert(heurelibid+ " "+dayshort+ " "+horaires);
     var codeEtab="<?php echo $_SESSION['user']['codeEtab'];?>";
     var sessionEtab="<?php echo $libellesessionencours ?>";
     var classeid="<?php echo $_GET['classe']; ?>";
     var etape=7;
     $.ajax({
       url: '../ajax/routines.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&heurelibid=' +heurelibid+ '&dayshort='+dayshort+ '&horaires=' +horaires+ '&codeEtab='+codeEtab+ '&sessionEtab=' +sessionEtab+'&classeid='+classeid,
       dataType: 'text',
       success: function (content, statut) {

         var matiere=content.split("*")[0];
         var salle=content.split("*")[1];

         // alert(content);

         if(matiere==0)
         {
           //rien dans la cellule

           $("#matieremodal").click();

           $("#FormAddmatiere #salle").html("");
           $("#FormAddmatiere #salle").html(salle);
           $("#FormAddmatiere #libelleheureid").val(heurelibid);
           $("#FormAddmatiere #shortday").val(dayshort);
           $("#FormAddmatiere #hours").val(horaires);
           $("#FormAddmatiere #codeEtab").val(codeEtab);
           $("#FormAddmatiere #sessionEtab").val(sessionEtab);
           $("#FormAddmatiere #classeid").val(classeid);


         }

       }
     });

   }
   $(document).ready(function() {
     searchmatiere()
     $("#matiere").select2();
     $("#salle").select2();



     $("#FormAddmatiere").validate({

       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
     },
     highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
     },
     success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },
        rules:{

          // matiere:"required",
          // classe:"required",
          // teatcher:"required",
          // coef:"required",
          classe:"required",
          matiere:"required",
          controle:"required",
          coef:"required",
          teatcher:"required",
          datectrl:"required",
          typesess:"required",
          competence:"required",
          salle:"required"


        },
        messages: {
          // matiere:"Merci de renseigner la matière",
          // classe:"<?php echo L::PleaseSelectclasserequired ?>",
          // teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
          // coef:"Merci de renseigner le coefficient de la matière"
          classe:"<?php echo L::PleaseSelectclasserequired ?>",
          matiere:"<?php echo L::SubjectSelectedrequired ?>",
          controle:"<?php echo L::Controlsrequired ?>",
          coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
          teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
            datectrl:"<?php echo L::ControlsDaterequired ?>",
            typesess:"<?php echo L::PeriodRequired ?>",
            competence:"<?php echo L::PleaseselectCompetence ?>",
            salle:"<?php echo L::PleaseSelectSalle ?>"


        },
        submitHandler: function(form) {


          var heurelibid=$("#FormAddmatiere #libelleheureid").val();
          var dayshort=$("#FormAddmatiere #shortday").val();
          var horaires=$("#FormAddmatiere #hours").val();
          var codeEtab=$("#FormAddmatiere #codeEtab").val();
          var sessionEtab=$("#FormAddmatiere #sessionEtab").val();
          var classeid=$("#FormAddmatiere #classeid").val();
          var matiere=$("#FormAddmatiere #matiere").val();
          var salle=$("#FormAddmatiere #salle").val();

     // nous allons verifier un controle similaire n'existe pas
          var etape=8;
          //
           $.ajax({
             url: '../ajax/routines.php',
             type: 'POST',
             async:true,
             data: 'matiere=' +matiere+ '&etape=' + etape+'&classe='+classeid+'&heurelibid='+heurelibid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&horaires='+horaires+'&dayshort='+dayshort+'&salle='+salle,
             dataType: 'text',
             success: function (content, statut) {


               window.location.reload();

             }
           });

               }


             });

             $("#FormAddlib").validate({

               errorPlacement: function(label, element) {
               label.addClass('mt-2 text-danger');
               label.insertAfter(element);
             },
             highlight: function(element, errorClass) {
               $(element).parent().addClass('has-danger')
               $(element).addClass('form-control-danger')
             },
             success: function (e) {
                   $(e).closest('.control-group').removeClass('error').addClass('info');
                   $(e).remove();
               },
                rules:{

                  // matiere:"required",
                  // classe:"required",
                  // teatcher:"required",
                  // coef:"required",
                  classe:"required",
                  matiere:"required",
                  controle:"required",
                  coef:"required",
                  teatcher:"required",
                  datectrl:"required",
                  typesess:"required",
                  competence:"required",
                  libelleroutine:"required"


                },
                messages: {
                  // matiere:"Merci de renseigner la matière",
                  // classe:"<?php echo L::PleaseSelectclasserequired ?>",
                  // teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
                  // coef:"Merci de renseigner le coefficient de la matière"
                  classe:"<?php echo L::PleaseSelectclasserequired ?>",
                  matiere:"<?php echo L::SubjectSelectedrequired ?>",
                  controle:"<?php echo L::Controlsrequired ?>",
                  coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
                  teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
                    datectrl:"<?php echo L::ControlsDaterequired ?>",
                    typesess:"<?php echo L::PeriodRequired ?>",
                    competence:"<?php echo L::PleaseselectCompetence ?>",
                    libelleroutine:"<?php echo L::PleaseAddTheLib ?>"


                },
                submitHandler: function(form) {


                  var heurelibid=$("#FormAddlib #libelleheureid").val();
                  var dayshort=$("#FormAddlib #shortday").val();
                  var horaires=$("#FormAddlib #hours").val();
                  var codeEtab=$("#FormAddlib #codeEtab").val();
                  var sessionEtab=$("#FormAddlib #sessionEtab").val();
                  var classeid=$("#FormAddlib #classeid").val();
                  var libelle=$("#FormAddlib #libelleroutine").val();

             // nous allons verifier un controle similaire n'existe pas
                  var etape=3;
                  //
                  $.ajax({
                    url: '../ajax/routines.php',
                    type: 'POST',
                    async:true,
                    data: 'libelle=' +libelle+ '&etape=' + etape+'&classe='+classeid+'&heurelibid='+heurelibid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&horaires='+horaires+'&dayshort='+dayshort,
                    dataType: 'text',
                    success: function (content, statut) {


                      window.location.reload();

                    }
                  });

                       }


                     });


                     $("#FormAddlibUp").validate({

                       errorPlacement: function(label, element) {
                       label.addClass('mt-2 text-danger');
                       label.insertAfter(element);
                     },
                     highlight: function(element, errorClass) {
                       $(element).parent().addClass('has-danger')
                       $(element).addClass('form-control-danger')
                     },
                     success: function (e) {
                           $(e).closest('.control-group').removeClass('error').addClass('info');
                           $(e).remove();
                       },
                        rules:{

                          // matiere:"required",
                          // classe:"required",
                          // teatcher:"required",
                          // coef:"required",
                          classe:"required",
                          matiere:"required",
                          controle:"required",
                          coef:"required",
                          teatcher:"required",
                          datectrl:"required",
                          typesess:"required",
                          competence:"required",
                          libelleUproutine:"required"


                        },
                        messages: {
                          // matiere:"Merci de renseigner la matière",
                          // classe:"<?php echo L::PleaseSelectclasserequired ?>",
                          // teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
                          // coef:"Merci de renseigner le coefficient de la matière"
                          classe:"<?php echo L::PleaseSelectclasserequired ?>",
                          matiere:"<?php echo L::SubjectSelectedrequired ?>",
                          controle:"<?php echo L::Controlsrequired ?>",
                          coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
                          teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
                            datectrl:"<?php echo L::ControlsDaterequired ?>",
                            typesess:"<?php echo L::PeriodRequired ?>",
                            competence:"<?php echo L::PleaseselectCompetence ?>",
                            libelleUproutine:"<?php echo L::PleaseAddTheLib ?>"


                        },
                        submitHandler: function(form) {


                          var heurelibid=$("#FormAddlib #libelleheureid").val();
                          var dayshort=$("#FormAddlibUp #shortday").val();
                          var horaires=$("#FormAddlibUp #hours").val();
                          var codeEtab=$("#FormAddlibUp #codeEtab").val();
                          var sessionEtab=$("#FormAddlibUp #sessionEtab").val();
                          var classeid=$("#FormAddlibUp#classeid").val();
                          var libelle=$("#FormAddlibUp #libelleUproutine").val();

                     // nous allons verifier un controle similaire n'existe pas
                          var etape=3;
                          //
                          $.ajax({
                            url: '../ajax/routines.php',
                            type: 'POST',
                            async:true,
                            data: 'libelle=' +libelle+ '&etape=' + etape+'&classe='+classeid+'&heurelibid='+heurelibid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&horaires='+horaires+'&dayshort='+dayshort,
                            dataType: 'text',
                            success: function (content, statut) {


                              window.location.reload();

                            }
                          });

                               }


                             });


     $('#tableone').DataTable( {
         "scrollX": true,
         "responsive": true

     } );

   });

   </script>
    <!-- end js include path -->
  </body>

</html>
