<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}

  $studentInfos=$student->getAllInformationsOfStudentOne($_GET['compte'],$libellesessionencours);

  $tabStudent=array();

  foreach ($studentInfos as $personnal):
    $tabStudent[0]= $personnal->id_compte;
    $tabStudent[1]=$personnal->matricule_eleve;
    $tabStudent[2]= $personnal->nom_eleve;
    $tabStudent[3]=$personnal->prenom_eleve;
    $tabStudent[4]= $personnal->datenais_eleve;
    $tabStudent[5]=$personnal->lieunais_eleve;
    $tabStudent[6]= $personnal->sexe_eleve;
    $tabStudent[7]=$personnal->email_eleve;
    $tabStudent[8]=$personnal->email_eleve;
    $tabStudent[9]= $personnal->libelle_classe;
    $tabStudent[10]=$personnal->codeEtab_classe;
    $tabStudent[11]= $personnal->photo_compte;
    $tabStudent[12]=$personnal->tel_compte;
    $tabStudent[13]= $personnal->login_compte;
    $tabStudent[14]=$personnal->codeEtab_inscrip;
    $tabStudent[15]= $personnal->id_classe;
    $tabStudent[16]=$personnal->allergie_eleve;
    $tabStudent[17]=$personnal->condphy_eleve;
  endforeach;


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::RenseignementMedicaux ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="classinfos.php?classe=<?php echo $_GET['classeid'] ?>&codeEtab=<?php echo $codeEtabAssigner ?>"><?php echo L::studMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::AddRenseignementMedicaux ?></li>
                          </ol>
                      </div>
                  </div>

                  <?php

                        if(isset($_SESSION['user']['addStudok']))
                        {

                          ?>
                          <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?php
                        //echo $_SESSION['user']['addetabok'];
                        ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                           </a>
                        </div-->
      <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
      <script src="../assets/js/sweetalert2.min.js"></script>

                    <script>
                    Swal.fire({
                    type: 'success',
                    title: '<?php echo L::Felicitations ?>',
                    text: '<?php echo $_SESSION['user']['addStudok'] ?>',

                    })
                    </script>
                          <?php
                          unset($_SESSION['user']['addStudok']);
                        }

                         ?>

                  <?php

                        if(isset($_SESSION['user']['addparok']))
                        {

                          ?>
                          <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?php
                        //echo $_SESSION['user']['addetabok'];
                        ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                           </a>
                        </div-->
      <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
      <script src="../assets/js/sweetalert2.min.js"></script>

                    <script>
                    Swal.fire({
      title: '<?php echo L::Felicitations ?>',
      text: "<?php echo $_SESSION['user']['addparok']; ?>",
      type: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '<?php echo L::AddNews ?>',
      cancelButtonText: '<?php echo L::AnnulerBtn ?>',
    }).then((result) => {
      if (result.value) {

      }else {
        document.location.href="index.php";
      }
    })
                    </script>
                          <?php
                          unset($_SESSION['user']['addparok']);
                        }

                         ?>
					<!-- start widget -->

					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


                 <div class="row">
                     <div class="col-md-12 col-sm-12">
                         <div class="card card-box">
                             <div class="card-head">
                                 <header></header>

                             </div>

                             <div class="card-body" id="bar-parent">
                                 <form  id="FormAddMedical" class="form-horizontal" action="../controller/medicales.php" method="post" enctype="multipart/form-data" >
                                     <div class="form-body">
                                       <div class="form-group row">
                                               <label class="control-label col-md-4"><?php echo L::BloodInfosTab ?>
                                                   <span class="required"> * </span>
                                               </label>
                                               <div class="col-md-6">
                                                   <input type="text" name="blood" id="blood" data-required="1" placeholder="<?php echo L::EnterBloodInfosTab ?>" class="form-control" />

                                                 </div>
                                                 <input type="hidden" name="etape" id="etape" value="1">
                                                 <input type="hidden" name="nomad" id="nomad" value="<?php echo $tabStudent[2] ?>">
                                                 <input type="hidden" name="matricule" id="matricule" value="<?php echo $tabStudent[1] ?>">
                                                 <input type="hidden" name="carnetnb" id="carnetnb" value="0">
                                                 <input type="hidden" name="idcompte" id="idcompte"  value="<?php echo $tabStudent[0] ?>">
                                                 <input type="hidden" name="codeEtab" id="codeEtab"  value="<?php echo $tabStudent[10] ?>">
                                                 <input type="hidden" name="sessionEtab" id="sessionEtab"  value="<?php echo $libellesessionencours ?>">
                                           </div>
                                           <div class="form-group row">
                                                   <label class="control-label col-md-4"><?php echo L::HospitalnameTab ?>
                                                       <span class="required">  </span>
                                                   </label>
                                                   <div class="col-md-6">
                                                       <input type="text" name="hopital" id="hopital" data-required="1" placeholder="<?php echo L::EnterHospitalnameTab ?>" class="form-control" />

                                                     </div>
                                               </div>
                                           <div class="form-group row">
                                                   <label class="control-label col-md-4"><?php echo L::HospitalchildnumberTab ?>
                                                       <span class="required">  </span>
                                                   </label>
                                                   <div class="col-md-6">
                                                       <input type="text" name="pinchildhopial" id="pinchildhopial" data-required="1" placeholder="<?php echo L::EnterHospitalchildnumberTab ?>" class="form-control" />

                                                     </div>
                                               </div>

                                               <div class="form-group row">
                                                       <label class="control-label col-md-4"><?php echo L::HospitaldoctorTab ?>
                                                           <span class="required">  </span>
                                                       </label>
                                                       <div class="col-md-6">
                                                           <input type="text" name="doctor" id="doctor" data-required="1" placeholder="<?php echo L::EnterHospitaldoctorTab ?>" class="form-control" />

                                                         </div>
                                                   </div>
                                                   <div class="form-group row">
                                                           <label class="control-label col-md-4"><?php echo L::TeldoctorTab ?>
                                                               <span class="required">  </span>
                                                           </label>
                                                           <div class="col-md-6">
                                                               <input type="text" name="phonedoctor" id="phonedoctor" data-required="1" placeholder="<?php echo L::EnterTeldoctorTab ?>" class="form-control" />

                                                             </div>
                                                       </div>
                                                       <div class="form-group row">

                                                         <div class="offset-md-4 col-md-8">
                                                           <button type="button" class="btn btn-primary" id="AllergiesNew" onclick="addAllergies()"> <i class="fa fa-plus"></i> <?php echo L::Allergiesingle ?></button>
                                                           <input type="hidden" name="nballergies" id="nballergies" value="0">
                                                           <input type="hidden" name="concatnballergies" id="concatnballergies" value="">
                                                           <input type="hidden" name="nbconcatnballergies" id="nbconcatnballergies" value="0">
                                                         </div>



                                                         </div>
                                                         <div class="form-group row">
                                                             <label class="control-label col-md-4">
                                                                 <span class="required">  </span>
                                                             </label>
                                                             <div class="col-md-6">
                                                             <table class="table" id="fields_allergies" border=0></table>
                                                               </div>
                                                         </div>

                                                       <div class="form-group row" style="margin-top:-35px;">

                                                         <div class="offset-md-4 col-md-8">
                                                           <button type="button" class="btn btn-primary" id="InfantilesDNew" onclick="addInfantiles()"> <i class="fa fa-plus"></i> <?php echo L::ChildDeseasesingle ?></button>
                                                           <input type="hidden" name="nbinfantilesD" id="nbinfantilesD" value="0">
                                                           <input type="hidden" name="concatnbinfantilesD" id="concatnbinfantilesD" value="">
                                                           <input type="hidden" name="nbconcatnbinfantilesD" id="nbconcatnbinfantilesD" value="0">
                                                         </div>


                                                         </div>

                                                         <div class="form-group row">
                                                             <label class="control-label col-md-4">
                                                                 <span class="required">  </span>
                                                             </label>
                                                             <div class="col-md-6">
                                                             <table class="table" id="fields_infantiles" border=0></table>
                                                               </div>
                                                         </div>


                                                       <div class="form-group row" style="margin-top:-35px;">

                                                         <div class="offset-md-4 col-md-8" >
                                                           <button type="button" class="btn btn-primary" id="MedicalNew"> <i class="fa fa-plus"></i> <?php echo L::MedicalAntecedent ?></button>
                                                           <input type="hidden" name="nbantecedent" id="nbantecedent" value="0">
                                                           <input type="hidden" name="concatantecedent" id="concatantecedent" value="">
                                                           <input type="hidden" name="nbconcatantecedent" id="nbconcatantecedent" value="0">
                                                         </div>


                                                         </div>


                                                       <div class="form-group row">

                                                               <div class="col-md-12">
                                                                 <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                                                   <thead>
                                                                     <th style="width:350px;"><?php echo L::HistoriqDiseases ?></th>
                                                                     <th style="width:80px;text-align:center"><?php echo L::Child ?></th>
                                                                     <th style="width:80px;text-align:center"><?php echo L::Father ?></th>
                                                                     <th style="width:80px;text-align:center"><?php echo L::Mother ?></th>
                                                                   </thead>
                                                                   <tbody id="tablebody">
                                                                     <tr id="lignevide">
                                                                       <td colspan="4" style="text-align:center"><?php echo L::NoInformations ?></td>
                                                                     </tr>
                                                                   </tbody>

                                                                 </table>

                                                                 </div>
                                                           </div>
                                                           <div class="form-group row">
                                                                   <label class="control-label col-md-4"><?php echo L::CarnetPhotos ?>
                                                                       <span class="required"> * </span>
                                                                   </label>
                                                                   <div class="col-md-6">
                                                                         <input type="file" id="carnet" name="carnet" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="../photo/images_files2.jpg" data-allowed-file-extensions="gif png jpg jpeg pjpeg pdf" onchange="verifypictures()" />

                                                                     </div>
                                                               </div>
                                                               <div class="form-group row">
                                                                 <!-- <label class="control-label col-md-4">
                                                                 <span class="required"> </span>
                                                                 </label> -->
                                                                 <label class="col-sm-7 col-md-7"><?php echo L::ReconduireChildLocation ?>
                                                                 <span class="required"> </span>
                                                                 </label>
                                                               </div>

                                                               <div class="form-group row">

                                                         <!-- <label class="control-label col-md-4">
                                                         <span class="required"> </span>
                                                         </label> -->
                                                         <div class="col-sm-7 col-md-7">
                                                         <div class="input-group">
                                                         <div id="radioBtn3" class="btn-group">
                                                         <a class="btn btn-primary btn-sm active" id="btn31" data-toggle="localisation" data-title="1" onclick="Home()"><?php echo L::ReconduireAtHome ?></a>
                                                         <a class="btn btn-primary btn-sm notActive" id="btn32" data-toggle="localisation" data-title="0" onclick="HomeN()"><?php echo L::ReconduireAtAdress ?></a>
                                                         </div>
                                                         <input type="hidden" name="localisation" id="localisation" value="1">
                                                         </div>
                                                         </div>
                                                         </div>

                                                         <div class="form-group row">
                                                           <!-- <label class="control-label col-md-4">
                                                           <span class="required"> </span>
                                                           </label> -->
                                                           <label class="col-sm-7 col-md-7"><?php echo L::DetailsGeoAdress ?>
                                                           <span class="required"> </span>
                                                           </label>
                                                         </div>
                                                         <div class="form-group row">
                                                             <!-- <label class="control-label col-md-4">Maladies infantiles
                                                                 <span class="required">  </span>
                                                             </label> -->
                                                             <div class="offset-md-3 col-md-7">
                                                               <textarea name="adresgeodetails" id="adresgeodetails" rows="8" cols="80" class="form-control"></textarea>
                                                               </div>
                                                         </div>
                                                         <div class="form-group row">
                                                                 <label class="control-label col-md-4"><?php echo L::InformPerson ?>
                                                                     <span class="required"> * </span>
                                                                 </label>
                                                                 <div class="col-md-6">
                                                                       <input type="text" class="form-control" name="guardian" id="guardian" value="" placeholder="<?php echo L::EnterInformPerson ?>">

                                                                   </div>
                                                             </div>
                                                             <div class="form-group row">
                                                                     <label class="control-label col-md-4"><?php echo L::TelMobile ?>
                                                                         <span class="required"> * </span>
                                                                     </label>
                                                                     <div class="col-md-6">
                                                                           <input type="text" class="form-control" name="telMobguardian" id="telmobguardian" value="" placeholder="<?php echo L::EnterTelMobile ?>">

                                                                       </div>
                                                                 </div>
                                                             <div class="form-group row">
                                                                     <label class="control-label col-md-4"><?php echo L::TelBureau ?>
                                                                         <span class="required">  </span>
                                                                     </label>
                                                                     <div class="col-md-6">
                                                                           <input type="text" class="form-control" name="telBguardian" id="telBguardian" value="" placeholder="<?php echo L::EnterTelBureau ?>">

                                                                       </div>
                                                                 </div>

                                                                     <div class="form-group row">
                                                                             <label class="control-label col-md-4"><?php echo L::TelDomicile ?>
                                                                                 <span class="required">  </span>
                                                                             </label>
                                                                             <div class="col-md-6">
                                                                                   <input type="text" class="form-control" name="domicileguardian" id="domicileguardian" value="" placeholder="<?php echo L::EnterTelDomicile ?>">

                                                                               </div>
                                                                         </div>







                   <div class="form-actions">
                                         <div class="row">
                                             <div class="offset-md-4 col-md-8">
                                                 <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                                 <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                             </div>
                                           </div>
                                        </div>
                 </div>
                                 </form>
                             </div>
                         </div>
                     </div>
                 </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <script src="../assets2/js/pages/material_select/getmdl-select.js" ></script>
   <script src="../assets/js/formatter/jquery.formatter.min.js"></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
   <script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
   <script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>

   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script>

 function verifypictures()
 {
   var carnet=$('#carnet').val();
   var nbcaracteres=carnet.length;
   if(nbcaracteres>0)
   {
     $('#carnetnb').val(1);
   }else {
     $('#carnetnb').val(0);
   }
 }

 function addFrench()
 {
   var etape=1;
   var lang="fr";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function addEnglish()
 {
   var etape=1;
   var lang="en";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 $("#sexeTea").select2();
 var date = new Date();

var newDate = new Date(date.setTime( date.getTime() + (-6120 * 86400000)));

 $('#datenaisTea').bootstrapMaterialDatePicker
 ({
 date: true,
 shortTime: false,
 time: false,
 maxDate:newDate ,
 // minDate:newDate ,
 // format: 'YYYY-MM-DD',
 format: 'DD-MM-YYYY',
 lang: 'fr',
 cancelText: '<?php echo L::AnnulerBtn ?>',
 okText: '<?php echo L::Okay ?>',
 clearText: '<?php echo L::Eraser ?>',
 nowText: '<?php echo L::Now ?>'

 });

 function AddMedicalsRow()
 {
   //var nb=$("#nbantecedent").val();
   var nb=$("#nbconcatantecedent").val();

   if(nb==0)
     {
       $("#lignevide").remove();

       AddRow();

     }else {
       AddRow();
     }
 }

 function addInfantiles()
 {
   var nb=$("#nbinfantilesD").val();
   var nouveau= parseInt(nb)+1;
   $("#nbinfantilesD").val(nouveau);
   var concatnbinfantilesD=$("#concatnbinfantilesD").val();
   $("#concatnbinfantilesD").val(concatnbinfantilesD+nouveau+"@");
   recalculinfantilesnb();

   $('#fields_infantiles').append('<tr id="rowInfantiles'+nouveau+'"><td><input type="text" name="Infantiles_'+nouveau+'" id="Infantiles_'+nouveau+'" placeholder="<?php echo L::EnterInfantilesMaladies ?>" class="form-control" /></td><td><button type="button" id="deleteInfantiles'+nouveau+'" id="deleteInfantiles'+nouveau+'"  onclick="deleteInfantiles('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');

   for(var i=1;i<=nouveau;i++)
   {
     $("#Infantiles_"+i).rules( "add", {
         required: true,
         messages: {
         required: "<?php echo L::PleaseEnterInfantilesMaladies ?>"
}
       });
   }
 }

 function addAllergies()
 {
   var nb=$("#nballergies").val();
   var nouveau= parseInt(nb)+1;
   $("#nballergies").val(nouveau);
   var concatallergies=$("#concatnballergies").val();
   $("#concatnballergies").val(concatallergies+nouveau+"@");

   recalculallergiesnb();

   $('#fields_allergies').append('<tr id="rowAllerg'+nouveau+'"><td><input type="text" name="Allergies_'+nouveau+'" id="Allergies_'+nouveau+'" placeholder="<?php echo L::EnterAllergiesingle ?>" class="form-control" /></td><td><button type="button" id="deleteAllerg'+nouveau+'" id="deleteAllerg'+nouveau+'"  onclick="deletedAllergies('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');

   for(var i=1;i<=nouveau;i++)
   {
     $("#Allergies_"+i).rules( "add", {
         required: true,
         messages: {
         required: "<?php echo L::PleaseEnterAllergiesingle ?>"
}
       });
     }
 }

 function recalculallergiesnb()
 {
   var concatnballergies=$("#concatnballergies").val();

   var tab=concatnballergies.split("@");

   var nbtab=tab.length;

   var nbtabnew=parseInt(nbtab)-1;

   $("#nbconcatnballergie").val(nbtabnew);
 }

 function recalculinfantilesnb()
 {
   var concatnbinfantilesD=$("#concatnbinfantilesD").val();

   var tab=concatnbinfantilesD.split("@");

   var nbtab=tab.length;

   var nbtabnew=parseInt(nbtab)-1;

   $("#nbconcatnbinfantilesD").val(nbtabnew);
 }

 function deletedAllergies(id)
 {
   var concatnballergies=$("#concatnballergies").val();

   $("#concatnballergies").val($("#concatnballergies").val().replace(id+"@", ""));

    $('#rowAllerg'+id+'').remove();

   recalculallergiesnb();
 }

 function deleteInfantiles(id)
 {
   var concatnbinfantilesD=$("#concatnbinfantilesD").val();

   $("#concatnbinfantilesD").val($("#concatnbinfantilesD").val().replace(id+"@", ""));

    $('#rowInfantiles'+id+'').remove();

   recalculinfantilesnb();
 }

 function deletedlineAnte(nouveau)
 {
   var concatantecedent=$("#concatantecedent").val();
   $("#concatantecedent").val($("#concatantecedent").val().replace(nouveau+"@", ""));
   $("#ligneAnte"+nouveau).remove();

   var tabconcatligne=$("#concatantecedent").val().split("@");
   var nb=(tabconcatligne.length)-1;
   if(nb==0)
   {
     Addlignestandard();
   }
 }

 function Addlignestandard()
 {
   var ligne="<tr id=\"lignevide\">";
   ligne=ligne+"<td colspan=\"4\" style=\"text-align:center\"><?php echo L::NoInformations ?></td>";
   ligne=ligne+"</tr>";

   $('#tablebody').append(ligne);

 }

 function Home()
 {
   $("#localisation").val(1);
   $("#btn31").removeClass('notActive').addClass('active');
   $("#btn32").removeClass('active').addClass('notActive');
 }

 function HomeN()
 {
   $("#localisation").val(0);
   $("#btn32").removeClass('notActive').addClass('active');
   $("#btn31").removeClass('active').addClass('notActive');
 }

 function getandecedentRow(nouveau)
 {
   var session="<?php echo $libellesessionencours; ?>";
   var codeEtab="<?php echo $codeEtabAssigner; ?>";
   var etape=17;

    $.ajax({
      url: '../ajax/admission.php',
      type: 'POST',
      async:false,
      data: 'session=' +session+ '&etape=' + etape+'&codeEtab='+codeEtab,
      dataType: 'text',
      success: function (response, statut) {
        $("#libelleantecedet"+nouveau).html("");
        $("#libelleantecedet"+nouveau).html(response);
        // $("#heureLibelles"+nouveau).html(content);

      }
    });


 }

 function AddRow()
 {
   //var nb=$("#nbantecedent").val();
   var nb=$("#nbconcatantecedent").val();
   var nouveau= parseInt(nb)+1;
   var concatantecedent=$("#concatantecedent").val();
   $("#concatantecedent").val(concatantecedent+nouveau+"@");
   // recalculantecedentnb();
   getandecedentRow(nouveau);

   var ligne="<tr  id=\"ligneAnte"+nouveau+"\" style=\"cursor:pointer\" ondblclick=\"deletedlineAnte("+nouveau+")\" >";
   ligne=ligne+"<td>";
   // ligne=ligne+"<select class=\"form-control \" id=\"libelleantecedet"+nouveau+"\" name=\"libelleantecedet"+nouveau+"\" style=\"width:100%;text-align:center\">";
   // ligne=ligne+"<option value=\"\">Selectionner un Antécédent Médical</option>";
   // ligne=ligne+"<option value=\"1\">Asthme / Asthma</option>";
   // ligne=ligne+"<option value=\"2\">Drépanocytose / Sickle Cell Disease</option>";
   // ligne=ligne+"<option value=\"3\">Cardiopathie / Heart Disease</option>";
   // ligne=ligne+"<option value=\"4\">Epilepsie / Epilepsy</option>";
   // ligne=ligne+"<option value=\"5\">Hypertension / High Blood Pressure</option>";
   // ligne=ligne+"<option value=\"6\">Rhumatisme / Rheumatism</option>";
   // ligne=ligne+"<option value=\"7\">Diabète / Diabetes</option>";
   // ligne=ligne+"</select>";
     ligne=ligne+"<input type=\"text\" class=\"form-control \" id=\"libelleantecedet"+nouveau+"\" name=\"libelleantecedet"+nouveau+"\" style=\"width:100%;text-align:center\" >";
   ligne=ligne+"</td>";

   ligne=ligne+"<td>";
   ligne=ligne+"<select class=\"form-control \" id=\"childantecedet"+nouveau+"\" name=\"childantecedet"+nouveau+"\" style=\"width:100%;text-align:center\">";
   ligne=ligne+"<option value=\"1\" selected><?php echo L::True ?></option>";
   ligne=ligne+"<option value=\"2\"><?php echo L::False ?></option>";
   ligne=ligne+"</select>";
   ligne=ligne+"</td>";

   ligne=ligne+"<td>";
   ligne=ligne+"<select class=\"form-control \" id=\"fatherantecedet"+nouveau+"\" name=\"fatherantecedet"+nouveau+"\" style=\"width:100%;text-align:center\">";
   ligne=ligne+"<option value=\"1\" selected><?php echo L::True ?></option>";
   ligne=ligne+"<option value=\"2\"><?php echo L::False ?></option>";
   ligne=ligne+"</select>";
   ligne=ligne+"</td>";

   ligne=ligne+"<td>";
   ligne=ligne+"<select class=\"form-control \" id=\"motherantecedet"+nouveau+"\" name=\"motherantecedet"+nouveau+"\" style=\"width:100%;text-align:center\">";
   ligne=ligne+"<option value=\"1\" selected><?php echo L::True ?></option>";
   ligne=ligne+"<option value=\"2\"><?php echo L::False ?></option>";
   ligne=ligne+"</select>";
   ligne=ligne+"</td>";
   ligne=ligne+"</tr>";

   $("#nbconcatantecedent").val(nouveau);

   $('#tablebody').append(ligne);

 }

 jQuery(document).ready(function() {

   $('#MedicalNew').click(function(){
         AddMedicalsRow();

       });

   $("#datenaisTea").formatter({pattern:"{{99}}-{{99}}-{{9999}}"});
   $("#contactTea").formatter({pattern:"{{99}}{{99}}{{99}}{{99}}"});
   $("#phonedoctor").formatter({pattern:"{{99}}{{99}}{{99}}{{99}}"});
   $("#telmobguardian").formatter({pattern:"{{99}}{{99}}{{99}}{{99}}"});

   $("#telBguardian").formatter({pattern:"{{99}}{{99}}{{99}}{{99}}"});
   $("#domicileguardian").formatter({pattern:"{{99}}{{99}}{{99}}{{99}}"});


   $("#nbchild").formatter({pattern:"{{999}}"});
   $("#nbchildsco").formatter({pattern:"{{999}}"});


   $("#FormAddLocalAd").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{
        passTea: {
            required: true,
            minlength: 6
        },
        confirmTea:{
            required: true,
            minlength: 6,
            equalTo:'#passTea'
        },
        // fonctionTea:"required",
        // cniTea:"required",

        loginTea:"required",
        // emailTea: {
        //            required: true,
        //            email: true
        //        },
        contactTea:{
          required: true

        },
        // datenaisTea:"required",
        prenomTea:"required",
        nomTea:"required",
        gradeTea:"required",
        dateEmbTea:"required",
        sexeTea:"required"



      },
      messages: {
        confirmTea:{
            required:"<?php echo L::Confirmcheck ?>",
            minlength:"<?php echo L::Confirmincheck ?>",
            equalTo: "<?php echo L::ConfirmSamecheck ?>"
        },
        passTea: {
            required:"<?php echo L::Passcheck ?>",
            minlength:"<?php echo L::Confirmincheck ?>"
        },
        loginTea:"<?php echo L::Logincheck ?>",
        emailTea:"<?php echo L::PleaseEnterEmailAdress ?>",
        contactTea:"<?php echo L::PleaseEnterPhoneNumber ?>",
        datenaisTea:"<?php echo L::PleaseEnterPhonestudentTab ?>",
        prenomTea:"<?php echo L::PleaseEnterPrename ?>",
        nomTea:"<?php echo L::PleaseEnterName ?> ",
        fonctionTea:"<?php echo L::PleaseEnterFonction ?>",
        gradeTea:"<?php echo L::EnterGrade ?>",
        dateEmbTea:"<?php echo L::EnterDateEmbauche ?>",
        sexeTea:"<?php echo L::PleaseEnterSexe ?>",
        // cniTea:"Merci de renseigner le numéro CNI"
      },
      submitHandler: function(form) {
        //verifier si ce compte n'existe pas encore dans la base de données
           var etape=1;
           $.ajax({
             url: '../ajax/parent.php',
             type: 'POST',
             async:false,
             data: 'login=' + $("#loginTea").val()+ '&email=' + $("#emailTea").val() + '&cni=' + $("#cniTea").val() + '&etape=' + etape+'&codeEtab='+$("#codeEtab").val(),
             dataType: 'text',
             success: function (content, statut) {




               if(content==0)
               {
                 //le compte n'existe pas dans la base on peut l'ajouter

                 form.submit();
               }else if(content==1) {
                 //Un Parent existe dejà avec cette CNI

                 Swal.fire({
                 type: 'warning',
                 title: '<?php echo L::WarningLib ?>',
                 text: '<?php echo L::LoginAllreadyExiste ?>',

                 })

                 // $("#newparent").val(0);
                 //
                 // form.submit();



               }else if(content==2)
               {
                  form.submit();
                 // Swal.fire({
                 // type: 'warning',
                 // title: '<?php echo L::WarningLib ?>',
                 // text: 'Un Parent existe dejà avec ce numéro CNI',
                 //
                 // })
               }
               /*else if(content==2) {
                 //le compte existe dejà dans la base de données
                 Swal.fire({
                 type: 'warning',
                 title: '<?php echo L::WarningLib ?>',
                 text: 'Un Parent existe dejà avec cette adresse email',

                 })

               }else if(content==3) {
                 //le compte existe dejà dans la base de données
                 Swal.fire({
                 type: 'warning',
                 title: '<?php echo L::WarningLib ?>',
                 text: 'Cette adresse email est dejà utilisée dans le système',

                 })

               }*/

             }


           });
      }


   });

var form = $("#FormAddMedical");

form.validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
},
highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
},
success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
rules:{
  guardian:"required",
  telMobguardian:"required",
  adresgeodetails:"required",
  blood:"required"



},
messages: {
  guardian:"<?php echo L::PleaseEnterPreventPersonne ?>",
  telBguardian:"<?php echo L::PleaseEnterTelBureau ?>",
  telMobguardian:"<?php echo L::PleaseEnterTelMobile ?>",
  domicileguardian:"<?php echo L::PleaseEnterTelDomicile ?>",
  adresgeodetails:"<?php echo L::PleaseEnterGeoAdress ?>",
  blood:"<?php echo L::PleaseEnterBloodInfosTab ?>",


},
submitHandler: function(form) {

//nous allons rechercher le parent

// form.submit();

var carnet=$('#carnet').val();

var nbcaracteres=carnet.length;
if(nbcaracteres>0)
{
  form.submit();
}else {
  Swal.fire({
          type: 'warning',
          title: '<?php echo L::WarningLib ?>',
          text: "Merci de charger la copie du carnet",

          })
}





}

});



 });
 </script>
    <!-- end js include path -->
  </body>

</html>
