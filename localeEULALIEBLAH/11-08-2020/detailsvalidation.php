<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$matierestud= new Matiere();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);

  if($etablissementType==1||$etablissementType==3)
  {
    if($nbsessionOn>0){
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];
      // $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
      // $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      // $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
    }

  }else {
    if($nbsessionOn>0)
    {
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];
      $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
      $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
    }
  }

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$validations=$etabs->getstandbymodificationNotes($codeEtabAssigner);

//recuperation des variables

$validationid=htmlspecialchars(addslashes($_GET['modify']));

$validations=$etabs->getstandbymodificationNotesbyId($validationid,$codeEtabAssigner);






 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::NotesValidationStandBy ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li ><?php echo L::EvalnotesMenu ?>&nbsp;<i class="fa fa-angle-right"></i></li>
                                <li class="active"><?php echo L::NotesValidationStandBy ?></li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
          <div class="row ">


                <!-- /.col -->

                <!-- /.col -->

                <!-- /.col -->

                <!-- /.col -->
              </div>
					<div class="state-overview">

						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


                 <div class="row">
                                         <div class="col-md-12">
                                             <div class="tabbable-line">
                                                <!--ul class="nav nav-pills nav-pills-rose">
                 									<li class="nav-item tab-all"><a class="nav-link active show"
                 										href="#tab1" data-toggle="tab">Liste</a></li>
                 									<li class="nav-item tab-all"><a class="nav-link" href="#tab2"
                 										data-toggle="tab">Grille</a></li>
                 								</ul-->
                                                 <div class="tab-content">
                                                     <div class="tab-pane active fontawesome-demo" id="tab1">
                                                         <div class="row">
                 					                        <div class="col-md-12">
                 					                            <div class="card  card-box">
                 					                                <div class="card-head">
                 					                                    <header></header>
                 					                                    <div class="tools">
                 					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                 						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                 						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                 					                                    </div>
                 					                                </div>
                 					                                <div class="card-body ">

                 					                                  <div class="table-scrollable">

                                                              <table class="table table-hover table-checkable order-column full-width" id="example4">
                                                                 <thead>
                                                                     <tr>

                                                                          <th>N&deg;</th>
                                                                          <th class="center"><?php echo L::NamestudentTab ?>s </th>
                                                                          <th class="center"><?php echo L::OldessNotes ?> </th>
                                                                          <th class="center"> <?php echo L::NewessNotes ?>  </th>
                                                                           <th class="center"> <?php echo L::Actions ?> </th>
                                                                     </tr>
                                                                 </thead>
                                                                 <tbody>
                                                                    <?php
                                                                    $i=1;


                //var_dump($teatchers);
                                                                      foreach ($validations as $value):
                                                                      ?>
                                               <tr class="odd gradeX">




                                                 <td><?php echo $i; ?></td>
                                                 <td><?php echo $etabs->getUtilisateurName($value->eleveid_modifnote); ?></td>
                                                 <td class="left">
                                                   <?php

                                                   $controle=$value->controle_modifnote;
                                                   $examen=$value->examen_modifnote;
                                                   $noteid=0;


                                                   if($controle==1&&$examen==0)
                                                   {
                                                     //recuperer la note de controle

                                                      $nb=$etabs->getNotecontrolevalidationNb($value->designationid_modifnote,$value->eleveid_modifnote,$value->matiereid_modifnote);
                                                      if($nb>0)
                                                      {
                                                        $valeurnotes=$etabs->getNotecontrolevalidation($value->designationid_modifnote,$value->eleveid_modifnote,$value->matiereid_modifnote);
                                                        echo $valeurnotes;
                                                        $noteid=$etabs->getNoteidcontrolevalidation($value->designationid_modifnote,$value->eleveid_modifnote,$value->matiereid_modifnote);
                                                      }else {
                                                        ?>
                                                        <span class="label label-sm label-warning"><?php echo L::NewessNotes ?></span>
                                                        <?php
                                                      }

                                                   }else if($controle==0&&$examen==1)
                                                   {
                                                     //recuperer la note d'examen
                                                     $nb=$etabs->getNoteexamenvalidationNb($value->designationid_modifnote,$value->eleveid_modifnote,$value->matiereid_modifnote);

                                                     if($nb>0)
                                                     {
                                                       $valeurnotes=$etabs->getNoteexamenvalidation($value->designationid_modifnote,$value->eleveid_modifnote,$value->matiereid_modifnote);
                                                       echo $valeurnotes;
                                                        $noteid=$etabs->getNoteidexamenvalidation($value->designationid_modifnote,$value->eleveid_modifnote,$value->matiereid_modifnote);
                                                     }else {
                                                       ?>
                                                       <span class="label label-sm label-warning"><?php echo L::NewessNotes ?>/span>
                                                       <?php
                                                     }

                                                   }


                                                    ?>
                                                 </td>
                                                  <td class="center">
                                                    <?php
                                                    echo $value->notes_modifnote;
                                                     ?>
                                                  </td>
                                                  <td class="center">
                                                    <?php
                                                    if($controle==1&&$examen==0)
                                                    {
                                                      ?>



                                                      <?php

                                                        if($noteid>0)
                                                        {
                                                          ?>
                                        <a href="#" onclick="valideoldcontrole(<?php echo $noteid;?>,<?php echo $valeurnotes; ?>,<?php echo $value->notes_modifnote; ?>,<?php echo $value->eleveid_modifnote; ?>,<?php echo $value->id_piste;?>,'<?php echo $value->observ_modifnote;?>')" class="btn btn-success btn-xs" title="<?php echo L::ModificationValidating ?>"> <i class="fa fa-check-circle"></i>  </a>
                                                          <?php
                                                        }else if($noteid==0)
                                                        {
                                                          ?>
                                      <a href="#" onclick="validenewcontrole(<?php echo $value->id_modifnote;?>,<?php echo $value->notes_modifnote; ?>,<?php echo $value->eleveid_modifnote; ?>,<?php echo $value->id_piste;?>,'<?php echo $value->observ_modifnote;?>')" class="btn btn-success btn-xs" title="<?php echo L::ModificationValidating ?>"> <i class="fa fa-check-circle"></i>  </a>
                                                          <?php
                                                        }


                                                      if($nb>0)
                                                      {

                                                          if($noteid>0)
                                                              {
                                                              ?>
                                   <a href="#" onclick="rejeteoldcontrole(<?php echo $noteid;?>,<?php echo $valeurnotes; ?>,<?php echo $value->notes_modifnote; ?>,<?php echo $value->eleveid_modifnote; ?>,<?php echo $value->id_piste;?>,'<?php echo $value->observ_modifnote;?>')" class="btn btn-danger btn-xs" title="<?php echo L::ModificationRejecting ?>"> <i class="fa  fa-times-rectangle"></i>  </a>

                                                                <?php
                                                              }else if($noteid==0)
                                                                {
                                                              ?>
                                   <a href="#" onclick="rejetenewcontrole(<?php echo $value->id_modifnote;?>,<?php echo $value->notes_modifnote; ?>,<?php echo $value->eleveid_modifnote; ?>,<?php echo $value->id_piste;?>,'<?php echo $value->observ_modifnote;?>'')"  class="btn btn-danger btn-xs" title="<?php echo L::ModificationRejecting ?>"> <i class="fa  fa-times-rectangle"></i>  </a>

                                                                <?php
                                                              }
                                                      }
                                                       ?>


                                                      <?php
                                                    }else if($controle==0&&$examen==1)
                                                    {
                                                      ?>


                                                      <?php

                                                        if($noteid>0)
                                                        {
                                                          ?>
                                                          <a href="#" onclick="valideoldexamen(<?php echo $noteid;?>,<?php echo $valeurnotes; ?>,<?php echo $value->notes_modifnote; ?>,<?php echo $value->eleveid_modifnote; ?>,<?php echo $value->id_piste;?>,'<?php echo $value->observ_modifnote;?>')" class="btn btn-success btn-xs" title="<?php echo L::ModificationValidating ?>"> <i class="fa fa-check-circle"></i>  </a>
                                                          <?php
                                                        }else if($noteid==0)
                                                        {
                                                          ?>
                                                          <a href="#" onclick="validenewexamen(<?php echo $value->id_modifnote;?>,<?php echo $value->notes_modifnote; ?>,<?php echo $value->eleveid_modifnote; ?>,<?php echo $value->id_piste;?>,'<?php echo $value->observ_modifnote;?>')" class="btn btn-success btn-xs" title="<?php echo L::ModificationValidating ?>"> <i class="fa fa-check-circle"></i>  </a>
                                                          <?php
                                                        }

                                                      if($nb>0)
                                                      {
                                                        ?>
                                                        <a href="#" onclick="rejeterexamen(<?php echo $noteid;?>,<?php echo $valeurnotes; ?>,<?php echo $value->notes_modifnote; ?>,<?php echo $value->eleveid_modifnote; ?>,<?php echo $value->id_piste;?>,'<?php echo $value->observ_modifnote;?>')" class="btn btn-danger btn-xs" title="<?php echo L::ModificationRejecting ?>"> <i class="fa  fa-times-rectangle"></i>  </a>
                                                        <?php
                                                      }
                                                       ?>


                                                      <?php
                                                    }
                                                     ?>

                                                  </td>
                                               </tr>
                                               <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                               <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                               <script type="text/javascript">

                                               function valideoldcontrole(noteid,oldnote,newnote,eleveid,pisteid,observation)
                                               {
                                                 var etape=7;
                                                 var typeEtab="<?php echo $etablissementType; ?>";
                                                 var codeEtab="<?php echo $codeEtabAssigner; ?>";
                                                 var controle=1;
                                                 var examen=0;
                                                 Swal.fire({
                                   title: '<?php echo L::WarningLib ?>',
                                   text: "<?php echo L::DoyouReallyValidateModifyingNotes ?>",
                                   type: 'warning',
                                   showCancelButton: true,
                                   confirmButtonColor: '#3085d6',
                                   cancelButtonColor: '#d33',
                                   confirmButtonText: '<?php echo L::Validate ?>',
                                   cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                 }).then((result) => {
                                   if (result.value) {
                                     document.location.href="../controller/notes.php?etape="+etape+"&noteid="+noteid+"&olnote="+oldnote+"&newnote="+newnote+"&eleveid="+eleveid+"&pisteid="+pisteid+"&observation="+observation+"&typeEtab="+typeEtab+"&codeEtab="+codeEtab+"&controle="+controle+"&examen="+examen;
                                   }else {

                                   }
                                 })
                                               }

                         function validenewcontrole(modifid,nouvellenote,eleveid,pisteid,observation)
                          {
                            var etape=8;
                            var typeEtab="<?php echo $etablissementType; ?>";
                            var codeEtab="<?php echo $codeEtabAssigner; ?>";
                            var session="<?php echo $libellesessionencours; ?>";
                            var controle=1;
                            var examen=0;
                            Swal.fire({
              title: '<?php echo L::WarningLib ?>',
              text: "<?php echo L::DoyouReallyValidateModifyingNotes ?>",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: '<?php echo L::Validate ?>',
              cancelButtonText: '<?php echo L::AnnulerBtn ?>',
            }).then((result) => {
              if (result.value) {
                document.location.href="../controller/notes.php?etape="+etape+"&modifnoteid="+modifid+"&nouvellenote="+nouvellenote+"&eleveid="+eleveid+"&pisteid="+pisteid+"&observation="+observation+"&typeEtab="+typeEtab+"&codeEtab="+codeEtab+"&controle="+controle+"&examen="+examen+"&session="+session;
              }else {

              }
            })
                          }

                          function rejeteoldcontrole(noteid,oldnote,newnote,eleveid,pisteid,observation)
                          {
                            var etape=9;
                            var typeEtab="<?php echo $etablissementType; ?>";
                            var codeEtab="<?php echo $codeEtabAssigner; ?>";
                            var controle=1;
                            var examen=0;
                            Swal.fire({
              title: '<?php echo L::WarningLib ?>',
              text: "<?php echo L::DoyouReallyValidateRejectingNotes ?>",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: '<?php echo L::Validate ?>',
              cancelButtonText: '<?php echo L::AnnulerBtn ?>',
            }).then((result) => {
              if (result.value) {
                document.location.href="../controller/notes.php?etape="+etape+"&noteid="+noteid+"&oldnote="+oldnote+"&newnote="+newnote+"&eleveid="+eleveid+"&pisteid="+pisteid+"&observation="+observation+"&typeEtab="+typeEtab+"&codeEtab="+codeEtab+"&controle="+controle+"&examen="+examen;
              }else {

              }
            })
                          }

                          function rejetenewcontrole(modifid,nouvellenote,eleveid,pisteid)
                          {
                            var etape=10;
                            var typeEtab="<?php echo $etablissementType; ?>";
                            var codeEtab="<?php echo $codeEtabAssigner; ?>";
                            var controle=1;
                            var examen=0;
                            Swal.fire({
              title: '<?php echo L::WarningLib ?>',
              text: "<?php echo L::DoyouReallyValidateRejectingNotes ?>",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: '<?php echo L::Validate ?>',
              cancelButtonText: '<?php echo L::AnnulerBtn ?>',
            }).then((result) => {
              if (result.value) {
                document.location.href="../controller/notes.php?etape="+etape+"&modifid="+modifid+"&nouvellenote="+nouvellenote+"&eleveid="+eleveid+"&pisteid="+pisteid+"&observation="+observation+"&typeEtab="+typeEtab+"&codeEtab="+codeEtab+"&controle="+controle+"&examen="+examen;
              }else {

              }
            })
                          }

        function valideoldexamen(noteid,oldnote,newnote,eleveid,pisteid,observation)
        {
          var etape=11;
          var typeEtab="<?php echo $etablissementType; ?>";
          var codeEtab="<?php echo $codeEtabAssigner; ?>";
          var controle=0;
          var examen=1;
          Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::DoyouReallyValidateModifyingNotes ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::Validate ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
document.location.href="../controller/notes.php?etape="+etape+"&noteid="+noteid+"&olnote="+oldnote+"&newnote="+newnote+"&eleveid="+eleveid+"&pisteid="+pisteid+"&observation="+observation+"&typeEtab="+typeEtab+"&codeEtab="+codeEtab+"&controle="+controle+"&examen="+examen;
}else {

}
})
        }

function validenewexamen(modifid,nouvellenote,eleveid,pisteid,observation)
 {
   var etape=12;
   var typeEtab="<?php echo $etablissementType; ?>";
   var codeEtab="<?php echo $codeEtabAssigner; ?>";
   var session="<?php echo $libellesessionencours; ?>";
   var controle=0;
   var examen=1;
   Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::DoyouReallyValidateModifyingNotes ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::Validate ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
document.location.href="../controller/notes.php?etape="+etape+"&modifnoteid="+modifid+"&nouvellenote="+nouvellenote+"&eleveid="+eleveid+"&pisteid="+pisteid+"&observation="+observation+"&typeEtab="+typeEtab+"&codeEtab="+codeEtab+"&controle="+controle+"&examen="+examen+"&session="+session;
}else {

}
})
 }

 function rejeterexamen(noteid,oldnote,newnote,eleveid,pisteid,observation)
 {
   var etape=13;
   var typeEtab="<?php echo $etablissementType; ?>";
   var codeEtab="<?php echo $codeEtabAssigner; ?>";
   var controle=0;
   var examen=1;
   Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::DoyouReallyValidateRejectingNotes ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::Validate ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
document.location.href="../controller/notes.php?etape="+etape+"&noteid="+noteid+"&oldnote="+oldnote+"&newnote="+newnote+"&eleveid="+eleveid+"&pisteid="+pisteid+"&observation="+observation+"&typeEtab="+typeEtab+"&codeEtab="+codeEtab+"&controle="+controle+"&examen="+examen;
}else {

}
})
 }




                                               </script>



                                                <?php
                                                                                 $i++;
                                                                                 endforeach;
                                                                                 ?>

                                             </tbody>
                                                             </table>

                 					                                  </div>
                 					                                </div>
                 					                            </div>
                 					                        </div>
                 					                    </div>
                                                     </div>

                                                 </div>
                                             </div>
                                         </div>
                                     </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function searchlibetab()
   {

     var etape=2;

     $.ajax({
              url: '../ajax/school.php',
              type: 'POST',
              async:false,
              data: 'code='+ $("#codeetab").val()+'&etape='+etape,
              dataType: 'text',
              success: function (content, statut) {

                $("#libetab").html("");
                $("#libetab").html(content);

                //nous allons chercher la liste des admins local de cet etablissement

                $.ajax({
                         url: '../ajax/classe.php',
                         type: 'POST',
                         async:false,
                         data: 'code='+ $("#codeetab").val()+'&etape='+etape,
                         dataType: 'text',
                         success: function (response, statut) {


                           $("#classex").html("");
                           $("#classex").html(response);
                         }
                       });

              }

            });

   }
   $(document).ready(function() {

     $('#example5').DataTable( {
         "scrollX": true

     } );

     $("#codeetab").select2();

     $("#libetab").select2();
     $("#classex").select2();

   });

   </script>
    <!-- end js include path -->
  </body>

</html>
