<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Teatcher.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$userId=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$matiere=new Matiere();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement
$schoolsofassign=$etabs->getEtablissementbyCodeEtab($codeEtabAssigner);

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$codesEtab=$schoolsofassign;
$codeEtabLocal=$etabs->getcodeEtabByLocalId($userId);
$classes=$classe->getAllClassesbyschoolCode($codeEtabLocal);
$teatchers=$teatcher->getAllTeatchersBySchoolCode($codeEtabLocal);
$matieres=$matiere->getAllMatiereOfThisSchool($codeEtabLocal);

//recuperer tous les cntrole de cet etablissement

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabLocal);

if($etablissementType==1||$etablissementType==3)
{

  if($nbsessionOn>0)
  {
    //recuperer la session en cours
    $sessionencours=$session->getSessionEncours($codeEtabLocal);
    $tabsessionencours=explode("*",$sessionencours);
    $libellesessionencours=$tabsessionencours[0];
    $sessionencoursid=$tabsessionencours[1];
    $typesessionencours=$tabsessionencours[2];
    // $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
    // $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabLocal,$libellesessionencours);
    // $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabLocal,$libellesessionencours);
    $controles=$matiere->getAllControleMatiereOfThisSchool($codeEtabLocal,$libellesessionencours);
  }


}else {
  if($nbsessionOn>0)
  {
    //recuperer la session en cours
    $sessionencours=$session->getSessionEncours($codeEtabLocal);
    $tabsessionencours=explode("*",$sessionencours);
    $libellesessionencours=$tabsessionencours[0];
    $sessionencoursid=$tabsessionencours[1];
    $typesessionencours=$tabsessionencours[2];
    $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
    $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabLocal,$libellesessionencours);
    $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabLocal,$libellesessionencours);
    $controles=$matiere->getAllControleMatiereOfThisSchool($codeEtabLocal,$libellesessionencours);
  }
}

$familles=$etabs->getAllfamillyEtab($codeEtabLocal);



 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
        <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
  <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
  <link href="../assets2/css/jquery-ui.css" rel="stylesheet">
  <link href="../assets2/css/awesomplete.css" rel="stylesheet">
  <link href="../assets2/css/prism.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <style media="screen">
    .ui-autocomplete {
    padding: 0;
    list-style: none;
    background-color: #fff;
    width: 218px;
    border: 1px solid #B0BECA;
    max-height: 350px;
    overflow-x: hidden;
  }
  .ui-autocomplete .ui-menu-item {
    border-top: 1px solid #B0BECA;
    display: block;
    padding: 4px 6px;
    color: #353D44;
    cursor: pointer;
  }
  .ui-autocomplete .ui-menu-item:first-child {
    border-top: none;
  }
  .ui-autocomplete .ui-menu-item.ui-state-focus {
    background-color: #D5E5F4;
    color: #161A1C;
  }
    </style>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title"><?php echo L::Famillygestion ?></div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">

                          <li><a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>

                          <li class="active"><?php echo L::Famillygestion  ?></li>
                      </ol>
                  </div>
              </div>

              <div class="modal fade" id="largeModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					    <div class="modal-dialog modal-lg" role="document">
					        <div class="modal-content">
					            <div class="modal-header">
					                <h4 class="modal-title" id="exampleModalLabel"><?php echo L::AddMembersList ?></h4>
					                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					                    <span aria-hidden="true">&times;</span>
					                </button>
					            </div>
					            <div class="modal-body">
                        <div id="divrecherchestation">

                          <p><?php echo L::SearchCritere ?></p>

                          <form class = "" id = "FormSearch">
                            <div class="row">
                              <div class="col-md-6">
          <div class="form-group">
          <label for=""><b><?php echo L::TypeAccount ?><span class="required">  </span> :</b></label>

          <select class="form-control input-height" name="account" id="account"  style="width:100%" onchange="searchcompte()">
              <option value=""><?php echo L::TypeAccountselect ?></option>
              <option value="Parent"><?php echo L::ParentCaps ?></option>
              <option value="Student"><?php echo L::EleveCaps ?></option>



          </select>

          </div>

          </div>
                                                <div class="col-md-6">
                            <div class="form-group">
                            <label for=""><b><?php echo L::Name ?><span class="required">  </span> :</b></label>

                            <!-- <select class="form-control" name="nomserach" id="nomserach"  style="width:100%" autofocus="autofocus" autocorrect="off" autocomplete="off" >


                            </select> -->

                            <input type="text" class="form-control" name="nomserach" id="nomserach"  style="width:100%" value="" list="mylist">
                            <datalist id="mylist">

                            </datalist>




                            </div>

                            </div>

                             <div class="col-md-6">
                                 <button type="submit" class="btn btn-danger" id="searchbtn"><i class="fa fa-search"></i> <?php echo L::Seacher ?></button>
                                 <img src="../photo/ajax-loader.gif" id="imgLoader" style="display:none;" />

                              </div>


                                          </div>
                          </form>
                          <br>
                          <table class="table table-striped table-bordered table-hover">
    <thead style="background-color:#28a745; color:white; font-weight: bold;">
    <tr>
    <th style=""><?php echo L::Name ?></th>
    <th class="visible-lg"><?php echo L::PreName ?> </th>
    <th> <?php echo L::TelMobiles ?> </th>
    <th style=""><?php echo L::EmailstudentTab ?> </th>
    <th style=""><?php echo L::Actions ?> </th>
  </tr>
   </thead>
   <tbody id="tabStationBody">

  <tr>

    <td colspan="5"><?php echo L::NoLigne ?></td>

  </tr>

                    </tbody>
                     </table>
                     <br>
                     <p><?php echo L::Familycomposition ?></p>

                     <table class="table table-striped table-bordered table-hover" id="tablemembers">
<thead style="background-color:#28a745; color:white; font-weight: bold;">
<tr>
<th style=""><?php echo L::Name ?></th>
<th class="visible-lg"><?php echo L::PreName ?> </th>
<th> <?php echo L::TelMobiles ?> </th>
<th style=""><?php echo L::TypeAccount ?> </th>
<th style=""><?php echo L::Actions ?> </th>
</tr>
</thead>
<tbody id="tabMemberBody">

<tr id="aucuneLinge">

<td colspan="5"><?php echo L::NoLigne ?></td>

</tr>

               </tbody>
                </table>
                <form class="" action="#">
 <input type="text" name="stationselect" id="stationselect" value="">
 <input type="text" name="famillycode" id="famillycode" value="">

                </form>

                        </div>
					            </div>
					            <div class="modal-footer">
					                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closing()"><?php echo L::Closebtn  ?></button>
					                <button type="button" class="btn btn-primary" onclick="validerMember()" id="btnsaving"><?php echo L::Saving ?></button>
					            </div>
					        </div>
					    </div>
					</div>

              <?php

                    if(isset($_SESSION['user']['addctrleok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
              <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
              <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
                type: 'success',
                title: '<?php echo L::Felicitations ?>',
                text: '<?php echo $_SESSION['user']['addctrleok']; ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addctrleok']);
                    }

                     ?>




              <?php

                    if(isset($_SESSION['user']['deletesubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['deletesubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: '<?php echo L::Okay ?>',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })


                </script>
                      <?php
                      unset($_SESSION['user']['deletesubjectok']);
                    }

                     ?>


              <?php

                    if(isset($_SESSION['user']['updatesubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['updatesubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: '<?php echo L::Okay ?>',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })


                </script>
                      <?php
                      unset($_SESSION['user']['updatesubjectok']);
                    }

                     ?>
              <?php

                    if(isset($_SESSION['user']['addsubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>


                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['addsubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: '<?php echo L::Okay ?>',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })
                </script>
                      <?php
                      unset($_SESSION['user']['addsubjectok']);
                    }

                     ?>


                     <div class="col-md-12 col-sm-12">
                                   <div class="panel tab-border card-box">
                                       <header class="panel-heading panel-heading-gray custom-tab ">
                                           <ul class="nav nav-tabs">
                                               <li class="nav-item"><a href="#home" data-toggle="tab" class="active"><i class="fa fa-bars"></i> <?php echo L::FamillyListing ?></a>
                                               </li>
                                               <?php
                                                 if($_SESSION['user']['fonctionuser']=="Directeur")
                                                 {

                                                 }else {
                                                   ?>
                                                   <?php
                                                   if($etablissementType==1||$etablissementType==3)
                                                   {
                                                    ?>
                                                    <li class="nav-item"><a href="#about1" data-toggle="tab"><i class="fa fa-plus-circle"></i> <?php echo L::AddcontolsTea  ?></a>
                                                    </li>
                                                    <?php
                                                  }else {
                                                    ?>
                                                    <li class="nav-item"><a href="#about" data-toggle="tab"><i class="fa fa-plus-circle"></i> <?php echo L::FamillyAdding?></a>
                                                    </li>
                                                    <?php
                                                  }
                                                     ?>
                                                   <?php
                                                 }
                                                  ?>




                                           </ul>
                                       </header>
                                       <div class="panel-body">
                                           <div class="tab-content">
                                               <div class="tab-pane active" id="home">
                                                 <div class="row">
                        <div class="col-md-12">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header></header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">

                                    <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                        <thead>
                                            <tr>

                                                <th> <?php echo L::FamillyCode?> </th>
                                                <th> <?php echo L::FamillyMembers?></th>

                                                  <?php
                                                  if($_SESSION['user']['fonctionuser']=="Directeur")
                                                  {

                                                  }else {
                                                    ?>
                                                    <th> <?php echo L::Actions ?> </th>
                                                    <?php
                                                  }
                                                   ?>

                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                            //$matieres

                                            $i=1;
                                              foreach ($familles as $value):
                                           ?>
                                            <tr class="odd gradeX">

                                                <td> <?php echo $value->code_fam;?></td>
                                                <td>
                                                    <?php
                                                    $nbmembers=$etabs->getfamillyMemberNb($value->id_fam,$value->codeEtab_fam);

                                                    if($nbmembers>0)
                                                    {
                                                      ?>
                                                      <span class="label label-success" ><?php echo $nbmembers." ".L::MembresCaps ?></span>
                                                      <?php
                                                    }else {
                                                      ?>
                                                      <span class="label label-danger" ><?php echo L::NoMembers ?></span>
                                                      <?php
                                                    }

                                                    ?>
                                                </td>

                                                <?php
                                                  if($_SESSION['user']['fonctionuser']=="Directeur")
                                                  {

                                                  }else {
                                                    ?>
                                                    <td class="valigntop">

                                                      <a href="#" class="btn btn-warning btn-xs" style="border-radius:3px;" title="<?php echo L::DetailsMembers ?>">
                                                        <i class="fa fa-info-circle"></i>
                                                      </a>
                                                      <a href="#" onclick="MembersFamille('<?php echo $value->code_fam ?>')"  data-toggle="modal" data-target="#largeModel" class="btn btn-success  btn-xs " style="border-radius:3px;" title="<?php echo L::AddMembers ?>">
                                                        <i class="fa fa-plus"></i>
                                                      </a>
                                                      <a href="#" class="btn btn-primary btn-xs" style="border-radius:3px;" title="<?php echo L::ModifyMembers ?>">
                                                        <i class="fa fa-pencil"></i>
                                                      </a>


                                                    </td>
                                                    <?php
                                                  }
                                                 ?>



                                                <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                                <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
                                                <!-- <script src="../assets2/plugins/select2/js/select2.js" ></script>
                                                <script src="../assets2/js/pages/select2/select2-init.js" ></script> -->
                                                <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
                                                <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                                <script>


                                                </script>
                                            </tr>


                                            <?php
                                            $i++;
                                            endforeach;
                                            ?>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                                               </div>

                      <?php
                      if($etablissementType==1||$etablissementType==3)
                      {
                       ?>
                       <div class="tab-pane" id="about1">
                         <?php
                         if($nbsessionOn>0)
                         {
                          ?>
                          <div class="row">
                              <div class="col-md-12 col-sm-12">
                                  <div class="card card-box">
                                      <div class="card-head">
                                          <header></header>

                                      </div>

                                      <div class="card-body" id="bar-parent">
                                          <form  id="FormAddCtrl1" class="form-horizontal" action="../controller/controle.php" method="post">
                                              <div class="form-body">
                                                <div class="form-group row">
                                                        <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-5">
                                                            <select class="form-control input-height" name="classe" id="classe"  style="width:100%" onchange="searchmatiere()">
                                                                <option value=""><?php echo L::Selectclasses ?></option>
                                                                <?php
                                                                $i=1;
                                                                  foreach ($classes as $value):
                                                                  ?>
                                                                  <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                                  <?php
                                                                                                   $i++;
                                                                                                   endforeach;
                                                                                                   ?>

                                                            </select>
                                                    </div>
                                                  </div>
                                                <div class="form-group row">
                                                        <label class="control-label col-md-3"><?php echo L::MatiereMenusingle ?>
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-5">
                                                            <!--input type="text" name="matiere" id="matiere" data-required="1" placeholder="Entrer la classe" class="form-control input-height" /-->
                                                            <select class="form-control input-height" name="matiere" id="matiere"  style="width:100%" onchange='searchprofesseur()'>
                                                                <option value=""><?php echo L::SelectSubjects ?></option>

                                                            </select>
                                                          </div>

                                                 </div>
                                                 <div class="form-group row">
                                                         <label class="control-label col-md-3"><?php echo L::ProfsMenusingle ?>
                                                             <span class="required"> * </span>
                                                         </label>
                                                         <div class="col-md-5">

                                                           <select class="form-control input-height" id="teatcher" name="teatcher" style="width:100%" >
                                                               <option value=""><?php echo L::PleaseselectTeatEnter ?></option>
                                                               <?php
                                                               $i=1;
                                                                 foreach ($teatchers as $value):
                                                                 ?>
                                                                 <option value="<?php echo $value->id_compte?>"><?php echo utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)) ?></option>

                                                                 <?php
                                                                                                  $i++;
                                                                                                  endforeach;
                                                                                                  ?>

                                                           </select>
                                                           <input type="hidden" name="etape" id="etape" value="4"/>
                                                           <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabLocal; ?>"/>
                                                             <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                                        </div>
                                                     </div>

                                                 <div class="form-group row">
                                                         <label class="control-label col-md-3"><?php echo L::LibelleControle ?>
                                                             <span class="required"> * </span>
                                                         </label>
                                                         <div class="col-md-5">
                                                             <input type="text" name="controle" id="controle" data-required="1" placeholder="Entrer la classe" class="form-control input-height" />

                                                           </div>

                                                  </div>
                                                 <div class="form-group row">
                                                         <label class="control-label col-md-3"><?php echo L::Coefs ?>
                                                             <span class="required"> * </span>
                                                         </label>
                                                         <div class="col-md-5">
                                                             <input type="number" min="1" name="coef" id="coef" data-required="1" placeholder="<?php echo L::EnterCoefSubjectControlLib ?>" class="form-control input-height" /> </div>
                                                  </div>
                                                  <div class="form-group row">
                                                    <label class="control-label col-md-3"><?php echo L::DatecontolsTea ?>
                                                        <span class="required"> * </span>
                                                    </label>
                                                        <div class="col-md-5">
                                                            <input type="date"  placeholder="<?php echo L::DatecontolsTea ?>" name="datectrl" id="datectrl"  class="form-control input-height">
                                                              <span class="help-block"><?php echo L::Datesymbole ?></span>
                                                        </div>
                                                    </div>







                            <div class="form-actions">
                                                  <div class="row">
                                                      <div class="offset-md-3 col-md-9">

                                                          <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                                          <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                      </div>
                                                    </div>
                                                 </div>
                          </div>
                                          </form>
                                      </div>
                                  </div>
                              </div>

                          </div>


                          <?php
                        }
                          ?>



                        </div>


    <?php
  }else {
    ?>
    <div class="tab-pane" id="about">
      <?php
      if($nbsessionOn>0)
      {
       ?>
       <div class="row">
           <div class="col-md-12 col-sm-12">
               <div class="card card-box">
                   <div class="card-head">
                       <header></header>

                   </div>

                   <div class="card-body" id="bar-parent">
                       <form  id="FormAddCtrl" class="form-horizontal" action="../controller/familles.php" method="post">
                           <div class="form-body">
                             <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabLocal; ?>"/>
                               <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                          <input type="hidden" name="etape" id="etape" value="1"/>



                              <div class="form-group row">
                                      <label class="control-label col-md-3"><?php echo L::FamillyCode ?>
                                          <span class="required"> * </span>
                                      </label>
                                      <div class="col-md-5">
                                          <input type="text" name="codefamille" id="codefamille" data-required="1" placeholder="<?php echo L::EnterFamillyCode ?>" class="form-control" />

                                        </div>

                               </div>


                               <div class="form-actions">
                               <div class="row">
                                   <div class="offset-md-3 col-md-9">

                                       <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                       <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                   </div>
                                 </div>
                              </div>
       </div>
                       </form>
                   </div>
               </div>
           </div>

       </div>


       <?php
     }
       ?>



     </div>
    <?php
  }
     ?>





                                           </div>
                                       </div>
                                   </div>
                               </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <!-- <script src="../assets2/plugins/jquery/jquery-1.11.1.min.js" ></script> -->
    <script src="../assets2/plugins/jquery/jquery-ui.min.js" ></script>
    <script src="../assets2/plugins/jquery/awesomplete.js" ></script>
    <script src="../assets2/plugins/jquery/prism.js" ></script>

 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>

   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

 <script>

function validerMember()
{
  var stationselect=$("#stationselect").val();
  var famillycode=$("#famillycode").val();
  var codeEtab="<?php echo $codeEtabAssigner ?>";
  var etape=6;

  //nous allons ajouter les membres de la famille

  $.ajax({

    url: '../ajax/famille.php',
    type: 'POST',
    async:true,
    data: 'ids=' +stationselect+ '&etape=' + etape+'&famillycode='+famillycode+'&codeEtab='+codeEtab,
    dataType: 'text',
    success: function (response, statut) {

      window.location.reload();

    }

  });



}

 function calculselectrow()
 {
   var rowCount = $('#tablemembers >tbody >tr').length;
   if(rowCount<=1)
   {
     $("#btnsaving").prop("disabled",true);
   }else {
$("#btnsaving").prop("disabled",false);
   }

   // btnsaving
 }

 function closing()
 {
   location.reload();
 }

 function MembersFamille(codefamille)
 {
   $("#famillycode").val(codefamille);
   $("#stationselect").val("");
 }

 function Addtolist(id,typecompte)
 {

   var codeEtab="<?php echo $codeEtabAssigner ?>";
   var selectionner=$("#stationselect").val();
   var number=selectionner.length;

   // alert(selectionner+" / "+number)

   if(number>0)
   {
     //nous allons verifier si l'utilisateur est deja dans la liste

     var etape=5;
     $.ajax({
       url: '../ajax/famille.php',
       type: 'POST',
       async:true,
       data: 'id=' +id+ '&etape=' + etape+'&selectionner='+selectionner,
       dataType: 'text',
       success: function (response, statut) {

         if(response==0)
         {
           var etape=4;
           $.ajax({

             url: '../ajax/famille.php',
             type: 'POST',
             async:true,
             data: 'compte=' +id+ '&etape=' + etape+'&type='+typecompte+'&codeEtab='+codeEtab,
             dataType: 'text',
             success: function (response, statut) {

               var nom=response.split("*")[0];
               var prenom=response.split("*")[1];
               var email=response.split("*")[2];
               var type=response.split("*")[3];
               var compteid=response.split("*")[4];

               $("#aucuneLinge").slideUp();

               var ligne = "<tr id=\"ligneselect"+id+"\">";
               ligne = ligne + $("#ligne" + id).html();
               ligne = ligne + "<td>"+nom+"</td>";
               ligne = ligne + "<td>"+prenom+"</td>";
               ligne = ligne + "<td>"+email+"</td>";
               ligne = ligne + "<td>"+type+"</td>";
               ligne = ligne + "<td class=\"visible-lg\"><a onclick=\"retirerStation("+id+")\" style=\"color:red;font-weight:normal; cursor:pointer\"><span class=\"fa fa-minus-circle\"></span>&nbsp;<?php echo L::Retirerstation?></a> </td>";
               ligne = ligne + "</tr>";

               $("#tabMemberBody").append(ligne);

               $("#stationselect").val($("#stationselect").val() + "@" + id);

               calculselectrow();



             }

           });
         }

       }
     });

   }else {

     var etape=4;
     $.ajax({

       url: '../ajax/famille.php',
       type: 'POST',
       async:true,
       data: 'compte=' +id+ '&etape=' + etape+'&type='+typecompte+'&codeEtab='+codeEtab,
       dataType: 'text',
       success: function (response, statut) {

         var nom=response.split("*")[0];
         var prenom=response.split("*")[1];
         var email=response.split("*")[2];
         var type=response.split("*")[3];
         var compteid=response.split("*")[4];

         $("#aucuneLinge").slideUp();

         var ligne = "<tr id=\"ligneselect"+id+"\">";
         ligne = ligne + $("#ligne" + id).html();
         ligne = ligne + "<td>"+nom+"</td>";
         ligne = ligne + "<td>"+prenom+"</td>";
         ligne = ligne + "<td>"+email+"</td>";
         ligne = ligne + "<td>"+type+"</td>";
         ligne = ligne + "<td class=\"visible-lg\"><a onclick=\"retirerStation("+id+")\" style=\"color:red;font-weight:normal; cursor:pointer\"><span class=\"fa fa-minus-circle\"></span>&nbsp;<?php echo L::Retirerstation?></a> </td>";
         ligne = ligne + "</tr>";

         $("#tabMemberBody").append(ligne);

         $("#stationselect").val($("#stationselect").val() + "@" + id);

         calculselectrow();



       }

     });

   }




 }



 function retirerStation(id)
 {
   $("#ligneselect"+id).remove();
   $("#stationselect").val($("#stationselect").val().replace(id+"@", ""));

   calculselectrow();
 }

 function searchcompte()
 {
   var compte=$("#account").val();
   var codeEtab="<?php echo $codeEtabAssigner ?>";
   var sessionEtab="<?php echo $libellesessionencours ?>";
   var etape=2;

   $.ajax({
     url: '../ajax/famille.php',
     type: 'POST',
     async:true,
     data: 'compte=' +compte+ '&etape=' + etape+'&sessionEtab='+sessionEtab+'&codeEtab='+codeEtab,
     dataType: 'text',
     success: function (response, statut) {



       // $("#nomserach").html("");
       // $("#nomserach").html(response);

       $("#mylist").html("");
       $("#mylist").html(response);
       $("#nomserach").html("");



     }

   });

 }

 function addFrench()
 {
   var etape=1;
   var lang="fr";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function addEnglish()
 {
   var etape=1;
   var lang="en";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function searchmatiere()
 {
     var codeEtab="<?php echo $codeEtabLocal;?>";
     var classe=$("#classe").val();
     var etape=2;
      $.ajax({

        url: '../ajax/matiere.php',
        type: 'POST',
        async:true,
        data: 'code=' + codeEtab+ '&etape=' + etape+'&classe='+classe,
        dataType: 'text',
        success: function (content, statut) {

          $("#matiere").html("");
          $("#matiere").html(content);

        }
      });
 }

 function searchprofesseur()
 {
   var codeEtab="<?php echo $codeEtabLocal;?>";
   var classe=$("#classe").val();
   var matiere=$("#matiere").val();
   var etape=4;
   $.ajax({

     url: '../ajax/teatcher.php',
     type: 'POST',
     async:true,
     data: 'code=' + codeEtab+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
     dataType: 'text',
     success: function (content, statut) {

       // alert(content);
       $("#teatcher").html("");
       $("#teatcher").html(content);

     }
   });
 }



 jQuery(document).ready(function() {


$("#btnsaving").prop("disabled",true);

$("#classe").select2();
$("#account").select2();
// $("#nomserach").select2();
// $('#nomserach').selectToAutocomplete();


$("#teatcher").select2();
$("#classeEtab").select2();
$("#matiere").select2();
$("#typesess").select2();

$("#FormSearch").validate({

  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
},
highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
},
success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
   rules:{
nomserach:"required"
   },
   messages: {
nomserach:"<?php echo L::Pleaseselectstudentname ?>"
   },
   submitHandler: function(form) {
     var codeEtab="<?php echo $codeEtabLocal;?>";
     var sessionEtab="<?php echo $libellesessionencours;?>";
     var compte=$("#account").val();

     var etape=3;
        $.ajax({
          url: '../ajax/famille.php',
          type: 'POST',
          async:false,
          data: 'nom=' +$("#nomserach").val() + '&etape=' + etape+'&compte='+compte+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab,
          dataType: 'text',
          beforeSend: function (xhr) {

               // $("#imgLoader").css("display", "inline");
               $("#imgLoader").show();
           //     setTimeout(function () {
           //     $("#imgLoader").show();
           // }, 100);

             },
          success: function (content, statut) {
          // $("#nbtotalparent").val(0);
          $("#RowInfosOld").hide();
          $("#imgLoader").hide(2000);
          $("#tabStationBody").html("");
          $("#tabStationBody").html(content);

          }
        });

   }


});

   $("#FormAddCtrl").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

        // matiere:"required",
        // classe:"required",
        // teatcher:"required",
        // coef:"required",
        classe:"required",
        matiere:"required",
        controle:"required",
        coef:"required",
        teatcher:"required",
        datectrl:"required",
        typesess:"required",
        codefamille:"required"


      },
      messages: {
        // matiere:"Merci de renseigner la matière",
        // classe:"<?php echo L::PleaseSelectclasserequired ?>",
        // teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
        // coef:"Merci de renseigner le coefficient de la matière"
        classe:"<?php echo L::PleaseSelectclasserequired ?>",
        matiere:"<?php echo L::PleaseselectSubjects ?>",
        controle:"<?php echo L::Controlsrequired ?>",
        coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
        teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
          datectrl:"<?php echo L::PleaseEnterDateControls ?>",
          typesess:"<?php echo L::PeriodRequired ?>",
          codefamille:"<?php echo L::PleaseEnterFamillyCode ?>"


      },
      submitHandler: function(form) {


// nous allons verifier le code famille
        var etape=1;

         $.ajax({
           url: '../ajax/famille.php',
           type: 'POST',
           async:true,
           data: 'codefamille=' + $("#codefamille").val()+ '&etape=' + etape+'&codeEtab='+$("#codeEtab").val()+'&sessionEtab='+$("#libellesession").val(),
           dataType: 'text',
           success: function (content, statut) {


             if(content==0)
             {
               //cette matière n'existe pas encore pour cette classe

               form.submit();

             }else if(content==1)
             {
               //il est question d'un nouveau professeur pour cette matière
               Swal.fire({
               type: 'warning',
               title: '<?php echo L::WarningLib ?>',
               text: '<?php echo L::FamillyCodeAllredayExist ?>',

               })

             }

           }
         });

             }


           });

           $("#FormAddCtrl1").validate({

             errorPlacement: function(label, element) {
             label.addClass('mt-2 text-danger');
             label.insertAfter(element);
           },
           highlight: function(element, errorClass) {
             $(element).parent().addClass('has-danger')
             $(element).addClass('form-control-danger')
           },
           success: function (e) {
                 $(e).closest('.control-group').removeClass('error').addClass('info');
                 $(e).remove();
             },
              rules:{

                // matiere:"required",
                // classe:"required",
                // teatcher:"required",
                // coef:"required",
                classe:"required",
                matiere:"required",
                controle:"required",
                coef:"required",
                teatcher:"required",
                datectrl:"required",
                typesess:"required"


              },
              messages: {
                // matiere:"Merci de renseigner la matière",
                // classe:"<?php echo L::PleaseSelectclasserequired ?>",
                // teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
                // coef:"Merci de renseigner le coefficient de la matière"


                  classe:"<?php echo L::PleaseSelectclasserequired ?>",
                  matiere:"<?php echo L::PleaseselectSubjects ?>",
                  controle:"<?php echo L::Controlsrequired ?>",
                  coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
                  teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
                    datectrl:"<?php echo L::PleaseEnterDateControls ?>",
                    typesess:"<?php echo L::PeriodRequired ?>"


              },
              submitHandler: function(form) {


        // nous allons verifier un controle similaire n'existe pas
                var etape=1;

                 $.ajax({
                   url: '../ajax/controle.php',
                   type: 'POST',
                   async:true,
                   data: 'matiere=' + $("#matiere").val()+ '&etape=' + etape+'&classe='+$("#classe").val()+'&teatcher='+$("#teatcher").val()+'&codeEtab='+$("#codeEtab").val()+'&coefficient='+$("#coef").val()+'&datectrl='+$("#datectrl").val(),
                   dataType: 'text',
                   success: function (content, statut) {


                     if(content==0)
                     {
                       //cette matière n'existe pas encore pour cette classe

                       form.submit();

                     }else if(content==1)
                     {
                       //il est question d'un nouveau professeur pour cette matière
                       Swal.fire({
                       type: 'warning',
                       title: '<?php echo L::WarningLib ?>',
                       text: '<?php echo L::ControlAllreadyExist ?>',

                       })

                     }

                   }
                 });

                     }


                   });
      });






 </script>
    <!-- end js include path -->
  </body>

</html>
