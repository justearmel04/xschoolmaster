<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement
$schoolsofassign=$etabs->getEtablissementbyCodeEtab($codeEtabAssigner);

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$codesEtab=$schoolsofassign;

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
//
if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}
//nous allons determine le type d'etablissement

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Classes</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="#">Classes</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Toutes les Classes</li>
                            </ol>
                        </div>
                    </div>

                    <?php

                          if(isset($_SESSION['user']['addclasseok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: 'Félicitations',
                      text: '<?php echo $_SESSION['user']['addclasseok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['addclasseok']);
                          }

                           ?>

                    <?php

                          if(isset($_SESSION['user']['updateclasseok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: 'Félicitations',
                      text: '<?php echo $_SESSION['user']['updateclasseok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['updateclasseok']);
                          }

                           ?>



                    <?php

                          if(isset($_SESSION['user']['delclasseok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: 'Félicitations',
                      text: '<?php echo $_SESSION['user']['delclasseok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['delclasseok']);
                          }

                           ?>
					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['updateteaok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: 'Félicitations',
            text: '<?php echo $_SESSION['user']['updateteaok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['updateteaok']);
                }

                 ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>
                 <?php
/*
                  ?>
<div class="row">
  <div class="col-md-12 col-sm-12">
                            <div class="card card-box">
                                <div class="card-head">
                                    <header><?php echo L::Seacher ?></header>

                                </div>
                                <div class="card-body " id="bar-parent">
                                  <form method="post" id="FormSearch">
                                      <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Code Etablissement</label>
                                            <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                                            <select class="form-control " id="codeetab" name="codeetab" onchange="searchlibetab()" style="width:100%">
                                                <option value="">Selectionner un code etablissement</option>
                                                <?php
                                                $i=1;
                                                  foreach ($schoolsofassign as $value):
                                                  ?>
                                                  <option value="<?php echo $value->code_etab?>"><?php echo $value->code_etab?></option>

                                                  <?php
                                                                                   $i++;
                                                                                   endforeach;
                                                                                   ?>

                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Classe</label>
                                            <select class="form-control input-height" id="classex" name="classex" style="width:100%">
                                                <option value=""><?php echo L::Selectclasses ?></option>
                                                <?php
                                                $i=1;
                                                  foreach ($classes as $value):
                                                  ?>
                                                  <option value="<?php echo $value->id_classe?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)) ?></option>

                                                  <?php
                                                                                   $i++;
                                                                                   endforeach;
                                                                                   ?>

                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Nom Etablissement</label>
                                        <select class="form-control " id="libetab" name="libetab" style="width:100%">
                                            <option value="">Selectionner un code etablissement</option>
                                            <?php
                                            $i=1;
                                              foreach ($codesEtab as $value):
                                              ?>
                                              <option value="<?php echo utf8_encode(utf8_decode($value->libelle_etab)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_etab)); ?></option>

                                              <?php
                                                                               $i++;
                                                                               endforeach;
                                                                               ?>

                                        </select>
                                        <input type="hidden" name="search" id="search"/>
                                    </div>


                                </div>
                                      </div>

                                      <button type="submit" class="btn btn-danger"><?php echo L::Seacher ?></button>
                                  </form>
                                </div>
                            </div>
                        </div>
</div>
<?php
  */
 ?>
<div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line">
                               <!--ul class="nav nav-pills nav-pills-rose">
									<li class="nav-item tab-all"><a class="nav-link active show"
										href="#tab1" data-toggle="tab">Liste</a></li>
									<li class="nav-item tab-all"><a class="nav-link" href="#tab2"
										data-toggle="tab">Grille</a></li>
								</ul-->
                                <div class="tab-content">
                                    <div class="tab-pane active fontawesome-demo" id="tab1">
                                        <div class="row">
					                        <div class="col-md-12">
					                            <div class="card  card-box">
					                                <div class="card-head">
					                                    <header></header>
					                                    <div class="tools">
					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
					                                    </div>
					                                </div>
					                                <div class="card-body ">
                                          <div class="pull-right">
                                            <?php
                                               if($_SESSION['user']['fonctionuser']=="Directeur")
                                               {

                                               }else {
                                                 ?>
                                                 <a class="btn btn-primary " href="addclasses.php"><i class="fa fa-plus"></i> Nouvelle Classe</a>
                                                 <?php
                                               }
                                             ?>

                                                <a class="btn btn-warning " href="#"><i class="fa fa-print"></i> Liste Classe</a>

                                          </div>
					                                  <div class="table-scrollable">
                                              <?php
                                              if($etablissementType==1||$etablissementType==3)
                                              {
                                               ?>
					                                    <table class="table table-hover table-checkable order-column full-width" id="example4">
					                                        <thead>
					                                            <tr>

                                                          <!-- <th>Section</th> -->
                                                          <th> N° </th>
					                                                <th> Classe </th>
                                                          <th> Nom de l'enseignant </th>
                                                          <th> NB inscrits </th>
                                                          <?php
                                                           if($_SESSION['user']['fonctionuser']=="Directeur")
                                                           {

                                                           }else {
                                                             ?>
                                                              <th> <?php echo L::Actions?> </th>
                                                             <?php
                                                           }
                                                           ?>

					                                            </tr>
					                                        </thead>
					                                        <tbody>
                                                    <?php
                                                    $i=1;
                                                    if(isset($_POST['search']))
                                                    {
                                                      $content="";
                                                        if($_POST['codeetab']!="")
                                                        {

                                                          if(strlen($_POST['classex'])>0)
                                                          {
                                                            //recherche en fonction de l'id de l'enseignant et code etab
                                                            $classes=$classe->getClassesByschoolCodewithId($_POST['codeetab'],$_POST['classex']);
                                                          }else {
                                                            //recherche tous les enseignants en fonction du code etablissement
                                                            //$teatchers=$teatcher->getAllTeatchers();
                                                            $classes=$classe->getAllClassesByschoolCode($_POST['codeetab']);

                                                          }

                                                          $content="bonjour";

                                                        }else

                                                         {
                                                          $content="bonsoir";
                                                            $classes=$classe->getAllClassesByClasseId($_POST['classex']);
                                                        }
                                                      }

//var_dump($teatchers);
                                                      foreach ($classes as $value):

                                                      ?>
																<tr class="odd gradeX" ondblclick="myFunction(<?php echo $value->id_classe?>)">




																	<!-- <td><?php //echo $classe->DetermineSectionName($value->id_classe,$libellesessionencours);?></td> -->
                                  <td><?php echo $i; ?></td>
                                  <td><?php echo $value->libelle_classe;?></td>

                                  <td><?php
                                  $nbprofesseur=$classe->DeteminerTeatcheraffecterNb($value->id_classe,$libellesessionencours,$value->codeEtab_classe);

                                  if($nbprofesseur==0)
                                  {
                                    ?>
                                    <span class="label label-sm label-danger">Aucun Enseignant</span>
                                    <?php
                                  }else if($nbprofesseur>0)
                                  {
                                    ?>
                                    <span class="label label-sm label-success"><?php echo  $classe->DeteminerTeatcheraffecter($value->id_classe,$libellesessionencours,$value->codeEtab_classe);  ?></span>
                                    <?php

                                  }


                                  ?></td>
																	<td class="left"><?php

                                  echo  $classe->DetermineNumberOfStudentInThisClasse($value->id_classe,$value->codeEtab_classe,$libellesessionencours);?></td>

                                  <?php
                                   if($_SESSION['user']['fonctionuser']=="Directeur")
                                   {

                                   }else {
                                     ?>
                                     <td class="center">

   																		<a href="#"  onclick="modify(<?php echo $value->id_classe?>)" class="btn btn-primary btn-xs">
   																			<i class="fa fa-pencil"></i>
   																		</a>
                                       <a class="btn btn-danger btn-xs" onclick="deleted(<?php echo $value->id_classe?>)">
                                         <i class="fa fa-trash-o "></i>
                                       </a>
                                       <!--a href="showInfosclasse.php?classe=<?php //echo $value->id_classe;?>&codeEtab=<?php //echo $codeEtabAssigner;?>" class="btn btn-warning btn-xs" title="Liste de classe"> <i class="fa fa-eye"></i>  </a-->
                                       <a href="classinfos.php?classe=<?php echo $value->id_classe;?>&codeEtab=<?php echo $codeEtabAssigner;?>" class="btn btn-warning btn-xs" title="Liste de classe"> <i class="fa fa-eye"></i>  </a>
                                       <a href="disciplinesclasses.php?classe=<?php echo $value->id_classe;?>&codeEtab=<?php echo $codeEtabAssigner;?>" class="btn btn-success btn-xs" title="disciplines de classe"> <i class=" fa fa-bar-chart-o"></i> </a>
                                       <?php
                                       if($nbprofesseur==0)
                                       {
                                         ?>
                                         <a href="#" onclick="attributeprof(<?php echo $value->id_classe?>)" class="btn btn-default btn-xs"> <i class="fa fa-plus"></i> </a>
                                         <?php
                                       }
                                        ?>
                                     </td>
                                     <?php
                                   }
                                   ?>

																</tr>
                                <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                <script>

                                function myFunction(idcompte)
                                {
                                  //var url="detailslocal.php?compte="+idcompte;
                                document.location.href="detailsclasses.php?compte="+idcompte;
                                }

                                function attributeprof(idclasse)
                                {
                                  document.location.href="classesattribution.php?compte="+idclasse;
                                }

                                function modify(id)
                                {


                                  Swal.fire({
                    title: '<?php echo L::WarningLib ?>',
                    text: "Voulez vous vraiment modifier cette classe",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<?php echo L::ModifierBtn ?>',
                    cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                  }).then((result) => {
                    if (result.value) {
                      document.location.href="updateclasse.php?compte="+id;
                    }else {

                    }
                  })
                                }

                                function deleted(id)
                                {

                                  Swal.fire({
                    title: '<?php echo L::WarningLib ?>',
                    text: "Voulez vous vraiment supprimer cette classe",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<?php echo L::DeleteLib ?>',
                    cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                  }).then((result) => {
                    if (result.value) {
                      document.location.href="../controller/classe.php?etape=3&compte="+id;
                    }else {

                    }
                  })
                                }

                                </script>


                                <?php
                                                                 $i++;
                                                                 endforeach;
                                                                 ?>

															</tbody>
					                                    </table>
                                              <?php
                                            }else {
                                              ?>
                                              <table class="table table-hover table-checkable order-column full-width" id="example4">
					                                        <thead>
					                                            <tr>

                                                          <!-- <th>Section</th> -->
                                                          <th>N°</th>
					                                                <th> Classe </th>
                                                          <th> NB inscrits </th>
                                                          <?php
                                                          if($_SESSION['user']['fonctionuser'])
                                                          {

                                                          }else {
                                                            ?>
                                                            <th> <?php echo L::Actions?> </th>
                                                            <?php
                                                          }
                                                           ?>

					                                            </tr>
					                                        </thead>
					                                        <tbody>
                                                    <?php
                                                    $i=1;
                                                    if(isset($_POST['search']))
                                                    {
                                                      $content="";
                                                        if($_POST['codeetab']!="")
                                                        {

                                                          if(strlen($_POST['classex'])>0)
                                                          {
                                                            //recherche en fonction de l'id de l'enseignant et code etab
                                                            $classes=$classe->getClassesByschoolCodewithId($_POST['codeetab'],$_POST['classex']);
                                                          }else {
                                                            //recherche tous les enseignants en fonction du code etablissement
                                                            //$teatchers=$teatcher->getAllTeatchers();
                                                            $classes=$classe->getAllClassesByschoolCode($_POST['codeetab']);

                                                          }

                                                          $content="bonjour";

                                                        }else

                                                         {
                                                          $content="bonsoir";
                                                            $classes=$classe->getAllClassesByClasseId($_POST['classex']);
                                                        }
                                                      }

//var_dump($teatchers);
                                                      foreach ($classes as $value):
                                                      ?>
																<tr class="odd gradeX" ondblclick="myFunction(<?php echo $value->id_classe?>)">




																	<!-- <td><?php //echo $classe->DetermineSectionName($value->id_classe,$libellesessionencours);?></td> -->
                                  <td><?php echo $i; ?></td>
                                  <td><?php echo $value->libelle_classe;?></td>

																	<td class="left"><?php echo $classe->DetermineNumberOfStudentInThisClasse($value->id_classe,$value->codeEtab_classe,$libellesessionencours);?></td>
                                  <?php
                                    if($_SESSION['user']['fonctionuser'])
                                    {

                                    }else {
                                      ?>
                                      <td class="center">

    																		<a href="#"  onclick="modify(<?php echo $value->id_classe?>)" class="btn btn-primary btn-xs">
    																			<i class="fa fa-pencil"></i>
    																		</a>
                                        <a class="btn btn-danger btn-xs" onclick="deleted(<?php echo $value->id_classe?>)">
                                          <i class="fa fa-trash-o "></i>
                                        </a>
                                        <a href="showInfosclasse.php?classe=<?php echo $value->id_classe;?>&codeEtab=<?php echo $codeEtabAssigner;?>" class="btn btn-warning btn-xs" title="Liste de classe"> <i class="fa fa-eye"></i>  </a>
                                        <a href="disciplinesclasses.php?classe=<?php echo $value->id_classe;?>&codeEtab=<?php echo $codeEtabAssigner;?>" class="btn btn-success btn-xs" title="disciplines de classe"> <i class=" fa fa-bar-chart-o"></i> </a>
    																	</td>
                                      <?php
                                    }
                                   ?>

																</tr>
                                <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                <script>

                                function myFunction(idcompte)
                                {
                                  //var url="detailslocal.php?compte="+idcompte;
                                document.location.href="detailsclasses.php?compte="+idcompte;
                                }

                                function modify(id)
                                {


                                  Swal.fire({
                    title: '<?php echo L::WarningLib ?>',
                    text: "Voulez vous vraiment modifier cette classe",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<?php echo L::ModifierBtn ?>',
                    cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                  }).then((result) => {
                    if (result.value) {
                      document.location.href="updateclasse.php?compte="+id;
                    }else {

                    }
                  })
                                }

                                function deleted(id)
                                {

                                  Swal.fire({
                    title: '<?php echo L::WarningLib ?>',
                    text: "Voulez vous vraiment supprimer cette classe",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<?php echo L::DeleteLib ?>',
                    cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                  }).then((result) => {
                    if (result.value) {
                      document.location.href="../controller/classe.php?etape=3&compte="+id;
                    }else {

                    }
                  })
                                }

                                </script>


                                <?php
                                                                 $i++;
                                                                 endforeach;
                                                                 ?>

															</tbody>
					                                    </table>
                                              <?php
                                            }
                                               ?>
					                                    </div>
					                                </div>
					                            </div>
					                        </div>
					                    </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>



                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function searchlibetab()
   {

     var etape=2;

     $.ajax({
              url: '../ajax/school.php',
              type: 'POST',
              async:false,
              data: 'code='+ $("#codeetab").val()+'&etape='+etape,
              dataType: 'text',
              success: function (content, statut) {

                $("#libetab").html("");
                $("#libetab").html(content);

                //nous allons chercher la liste des admins local de cet etablissement

                $.ajax({
                         url: '../ajax/classe.php',
                         type: 'POST',
                         async:false,
                         data: 'code='+ $("#codeetab").val()+'&etape='+etape,
                         dataType: 'text',
                         success: function (response, statut) {


                           $("#classex").html("");
                           $("#classex").html(response);
                         }
                       });

              }

            });

   }
   $(document).ready(function() {

     $('#example5').DataTable( {
         "scrollX": true

     } );

     $("#codeetab").select2();

     $("#libetab").select2();
     $("#classex").select2();

   });

   </script>
    <!-- end js include path -->
  </body>

</html>
