<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Teatcher.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$userId=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$matiere=new Matiere();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement
$schoolsofassign=$etabs->getEtablissementbyCodeEtab($codeEtabAssigner);

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$codesEtab=$schoolsofassign;
$codeEtabLocal=$etabs->getcodeEtabByLocalId($userId);
$classes=$classe->getAllClassesbyschoolCode($codeEtabLocal);
$teatchers=$teatcher->getAllTeatchersBySchoolCode($codeEtabLocal);
$matieres=$matiere->getAllMatiereOfThisSchool($codeEtabLocal);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabLocal);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabLocal);




if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabLocal);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabLocal,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabLocal,$libellesessionencours);
}

$etablissementType=$etabs->DetermineTypeEtab($codeEtabLocal);

$matiereslibelles=$etabs->getAllmatierelibellesEtab($libellesessionencours,$codeEtabLocal);

$selectlibidbase=$etabs->getallselectedlibidbase($codeEtabLocal,$libellesessionencours);

// echo $selectlibidbase;
//
// $tabidbase=explode(',',$selectlibidbase);

$nbselectbase=strlen($selectlibidbase);

// echo $nbselectbase;

if($nbselectbase>0)
{
  $allLibelleHeures=$etabs->getLibelleHeure($codeEtabLocal,$libellesessionencours,$selectlibidbase);
}else if($nbselectbase==0)
{
$allLibelleHeures=$etabs->getLibelleHeureAll($codeEtabLocal,$libellesessionencours);
}

$allLibellesHours=$etabs->getHoursAllLibs($codeEtabLocal,$libellesessionencours);

// var_dump($allLibelleHeures);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
    <!-- data tables -->
        <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
  <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title">Gestion des Heures de cours </div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#">Enseigenment</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active">Heures de cours </li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['updateparentok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['updateparentok']; ?>',

                })


                </script>
                      <?php
                      unset($_SESSION['user']['updateparentok']);
                    }

                     ?>


              <?php

                    if(isset($_SESSION['user']['updatesubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['updatesubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'OK',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })


                </script>
                      <?php
                      unset($_SESSION['user']['updatesubjectok']);
                    }

                     ?>
              <?php

                    if(isset($_SESSION['user']['addsubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>


                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['addsubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'OK',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })
                </script>
                      <?php
                      unset($_SESSION['user']['addsubjectok']);
                    }

                     ?>

                     <div class="col-md-12 col-sm-12">
                                   <div class="panel tab-border card-box">
                                       <header class="panel-heading panel-heading-gray custom-tab ">
                                           <ul class="nav nav-tabs">
                                               <li class="nav-item"><a href="#home" data-toggle="tab" class="active"><i class="fa fa-bars"></i> Liste des heures de cours</a>
                                               </li>
                                               <li class="nav-item"><a href="#about" data-toggle="tab"><i class="fa fa-plus-circle"></i> Ajouter heure de cours</a>
                                               </li>
                                               <li class="nav-item"><a href="#about1" data-toggle="tab"><i class="fa fa-plus-circle"></i> Libellé heure de cours</a>
                                               </li>

                                           </ul>
                                       </header>
                                       <div class="panel-body">
                                           <div class="tab-content">
                                               <div class="tab-pane active" id="home">
                                                 <div class="row">
                        <div class="col-md-12">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header></header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">

                                    <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                        <thead>
                                            <tr>

                                                <th> Libellé Heure </th>
                                                <th> Heure de début</th>
                                                <th> Heure de fin </th>

                                                <th> <?php echo L::Actions?> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                            //$matieres

                                            $i=1;
                                              foreach ($allLibellesHours as $value):
                                           ?>
                                            <tr class="odd gradeX">

                                                <td> <span class="label label-sm label-success" style=""><?php echo $value->libelle_heurelib ?></span> </td>
                                                <td style="text-align:center;">
                                                  <?php echo $value->heuredeb_heure	 ?>
                                                </td>
                                                <td style="text-align:center;">
                                                  <?php echo $value->heurefin_heure	 ?>
                                                </td>

                                                <td class="valigntop">
                                                  <a href="updatehours.php?heureid=<?php echo $value->id_heure; ?>" class="btn btn-info  btn-sm " style="border-radius:3px;" title="modifier l'heure de cours'" >
                                                    <i class="fa fa-pencil"></i>
                                                  </a>


                                                  <a href="#"  onclick="deletedHeure(<?php echo $value->id_heure;?>,<?php echo $value->id_heurelib;?>)" class="btn btn-danger  btn-sm " style="border-radius:3px;" title="supprimer l'heure de cours">
                                                    <i class="fa fa-trash-o"></i>
                                                  </a>
                                                </td>
                                            </tr>

                                            <?php
                                            $i++;
                                            endforeach;
                                            ?>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                                               </div>
                                               <div class="tab-pane" id="about">
                                                 <div class="row">
                                                     <div class="col-md-12 col-sm-12">
                                                         <div class="card card-box">
                                                             <div class="card-head">
                                                                 <header></header>

                                                             </div>

                                                             <div class="card-body" id="bar-parent">
                                                                 <form  id="FormAddSubject" class="form-horizontal" action="../controller/hours.php" method="post">
                                                                     <div class="form-body">
                                                                       <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabLocal; ?>"/>
                                                                       <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                                                       <input type="hidden" name="libelleidconcat" id="libelleidconcat" value="">
                                                                       <input type="hidden" name="libelleidconcatbase" id="libelleidconcatbase" value="<?php echo $selectlibidbase; ?>">
                                                                       <input type="hidden" name="concatidH" id="concatidH" value="">
                                                                       <input type="hidden" name="concatidHselected" id="concatidHselected" value="">
                                                                       <input type="hidden" name="selecteionsH" id="selecteionsH" value="">
                                                                       <input type="hidden" name="nbligneH" id="nbligneH" value="0">
                                                                       <input type="hidden" id="etape" name="etape" value="4">


                                                                       <div id="dynamic_field1">

                                                                       </div>
                                                                       <div class="form-group row">
                                                                         <div class="offset-md-3 col-md-9">
                                                                           <button type="button" class="btn btn-primary" id="HeureNew"> <i class="fa fa-plus"></i> Définir une heure </button>

                                                                         </div>

                                                                       </div>






                                                   <div class="form-actions">
                                                                         <div class="row">
                                                                             <div class="offset-md-3 col-md-9">
                                                                                 <button type="submit" id="submit2" disabled="disabled" class="btn btn-info">Enregistrer</button>
                                                                                 <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                             </div>
                                                                           </div>
                                                                        </div>
                                                 </div>
                                                                 </form>
                                                             </div>
                                                         </div>
                                                     </div>

                                                 </div>
                                                </div>
                                                <div class="tab-pane" id="about1">
                                                  <div class="row">
                                                      <div class="col-md-12 col-sm-12">
                                                          <div class="card card-box">
                                                              <div class="card-head">
                                                                  <header></header>

                                                              </div>

                                                              <div class="card-body" id="bar-parent">
                                                                  <form  id="FormAddmatlib" class="form-horizontal" action="../controller/hours.php" method="post">
                                                                      <div class="form-body">

                                                                         <input type="hidden" name="etape" id="etape" value="3"/>
                                                                         <input type="hidden" name="codeEtablib" id="codeEtablib" value="<?php echo $codeEtabAssigner; ?>"/>
                                                                         <input type="hidden" name="libellesessionlib" id="libellesessionlib" value="<?php echo $libellesessionencours; ?>" >
                                                                         <input type="hidden" name="nbLib" id="nbLib" value="0">
                                                                         <input type="hidden" name="concatLibs" id="concatLibs" value="">
                                                                         <div id="dynamic_field">

                                                                         </div>
                                                                         <div class="form-group row">
                                                                           <div class="offset-md-3 col-md-9">
                                                                             <button type="button" class="btn btn-primary" id="LibNew"> <i class="fa fa-plus"></i> Nouveau Libellé </button>

                                                                           </div>

                                                                         </div>







                                                    <div class="form-actions">
                                                                          <div class="row">
                                                                              <div class="offset-md-3 col-md-9">
                                                                                  <button type="submit" id="submit1"class="btn btn-info">Enregistrer</button>
                                                                                  <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                              </div>
                                                                            </div>
                                                                         </div>
                                                  </div>
                                                                  </form>
                                                              </div>
                                                          </div>
                                                      </div>

                                                  </div>
                                                 </div>


                                           </div>
                                       </div>
                                   </div>
                               </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>

   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
   <script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
   <script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
   <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script>

 $('#heuredeb').bootstrapMaterialDatePicker
 ({
   date:false,
   shortTime: false,
   format: 'HH:mm',
   lang: 'fr',
  cancelText: '<?php echo L::AnnulerBtn ?>',
  okText: 'OK',
  clearText: '<?php echo L::Eraser ?>',
  nowText: '<?php echo L::Now ?>'
 });

 $('#heurefin').bootstrapMaterialDatePicker
 ({
   date:false,
   shortTime: false,
   format: 'HH:mm',
   lang: 'fr',
  cancelText: '<?php echo L::AnnulerBtn ?>',
  okText: 'OK',
  clearText: '<?php echo L::Eraser ?>',
  nowText: '<?php echo L::Now ?>'

 });
 function determineteatcher()
 {
   var classe=$("#classe").val();
   var session="<?php echo $libellesessionencours; ?>";
   var codeEtab="<?php echo $codeEtabAssigner; ?>";
   var etape=11;

   $.ajax({
            url: '../ajax/matiere.php',
            type: 'POST',
            async:false,
            data: 'classe='+ classe+'&etape='+etape+'&session='+session+'&codeEtab='+codeEtab,
            dataType: 'text',
            success: function (response, statut) {


              $("#teatcher").html("");
              $("#teatcher").html(response);
            }
          });


 }

 function count(array, value) {
  var counter = 0;
  for(var i=1;i<=array.length;i++) {
    if (array[i] === value) counter++;
  }
  return counter;
}

 function checkRow()
 {
    var nb=$("#nbLib").val();
    if(nb>1)
    {
      var resultat="";
      var myArr = new Array();
      for(i=1;i<=nb;i++)
      {
        var valeur=$("#heurelib"+i).val();
        myArr[i]=valeur;

      }

      var retour=count(myArr, $("#heurelib"+nb).val()) ;

      // alert(retour);

      if(retour==1)
      {
        AddLibRow();
      }else {
        Swal.fire({
        type: 'warning',
        title: '<?php echo L::WarningLib ?>',
        text: "les libellés doivent etre différent",

      })
      }



    }else {
      AddLibRow();
    }
 }


 function deletedLibrow(id)
 {
     var concatLibs=$("#concatLibs").val();
     $("#concatLibs").val($("#concatLibs").val().replace(id+"@", ""));
     recalculLibnb();
     $("#ligneRowName"+id).remove();
     $("#ligneRow"+id).remove();
 }

 function getListeHeureLib(nouveau)
 {
   var concatidbase=$("#libelleidconcatbase").val();
   // var selectedidbase=$("#concatidHselected").val();
   var selectedidbase=$("#selecteionsH").val();
   var codeEtab=$("#codeEtab").val();
   var session=$("#libellesession").val();
   // alert(selectedidbase);
   var etape=2;

   $.ajax({
     url: '../ajax/hours.php',
     type: 'POST',
     async:true,
     data: 'concatidbase='+concatidbase+'&etape='+etape+'&codeEtab='+codeEtab+'&session='+session+'&selectedidbase='+selectedidbase,
     dataType: 'text',
     success: function (content, statut) {

       $("#libelleheure"+nouveau).html(content);

     }});


 }

 function checklastH()
 {
   var concatidH=$("#concatidH").val();
   var longeur=$("#concatidH").val().length;
   var content=0;
   if(longeur>0)
   {
     content=1;
   }
   //return content;

   if(content==0)
   {
     AddligneH();
   }else if(content==1)
   {
     //nous alons verifier si la ligne precedente est renseigner
     //retrouvone le dernier indice
     var etape=3;
     $.ajax({
       url: '../ajax/hours.php',
       type: 'POST',
       async:true,
       data: 'concatidH='+concatidH+'&etape='+etape,
       dataType: 'text',
       success: function (content, statut) {


         var cut = $.trim(content);
         var indice=cut;
         var libelleindice=$("#libelleheure"+indice).val().length;
         var debutindice=$("#heuredeb"+indice).val().length;
         var finindice=$("#heurefin"+indice).val().length;

         if((libelleindice>0)&&(debutindice>0)&&(finindice>0))
         {
           AddligneH();
         }else {
           alert("Merci de renseigner tous les champs");
         }

       }});

   }
 }

function ajouterlibeller(nouveau)
{
  var libelleheure=$("#libelleheure"+nouveau).val();
  var concatidHselected=$("#concatidHselected").val();
  var selecteionsH=$("#selecteionsH").val();
  var longeur=$("#concatidHselected").val().length;
  //alert(libelleheure+" "+longeur);
  if(longeur==0)
  {
    $("#concatidHselected").val(concatidHselected+nouveau+"-"+libelleheure+",");
    $("#selecteionsH").val(selecteionsH+libelleheure+",");
  }else if(longeur>0)
  {
    //nous allons verifier si l'element nouveau+"-"+libelleheure se trouve dans le tableau

    //nous devons retrouver l'ancienne valeur

    var alldata=$("#concatidHselected").val();
    var newdata=nouveau+"-"+libelleheure+",";

    //nous allons voir si nous avons deja cette nouvelle valeur la dans  lot

    var etape1=5;

    $.ajax({
      url: '../ajax/hours.php',
      type: 'POST',
      async:true,
      data: 'alldata='+alldata+'&etape='+etape1+'&nouveau='+nouveau+'&newdata='+newdata,
      dataType: 'text',
      success: function (content, statut) {

        if(content==0)
        {
          $("#concatidHselected").val(concatidHselected+nouveau+"-"+libelleheure+",");
          $("#selecteionsH").val(selecteionsH+libelleheure+",");
        }else if(content==1)
        {
          var etape=4;
          $.ajax({
            url: '../ajax/hours.php',
            type: 'POST',
            async:true,
            data: 'alldata='+alldata+'&etape='+etape+'&nouveau='+nouveau,
            dataType: 'text',
            success: function (content, statut) {

              var valeur=content;

              $("#concatidHselected").val($("#concatidHselected").val().replace(nouveau+"-"+valeur+",", newdata));
              $("#selecteionsH").val($("#selecteionsH").val().replace(valeur+",",libelleheure+","));

            }
          });
        }

      }
    });






  }
}

function deletedrowLibelleH(nouveau)
{
   var concatidH=$("#concatidH").val();
   var selecteionsH=$("#selecteionsH").val();
   var concatidHselected=$("#concatidHselected").val();
   var libelleheure=$("#libelleheure"+nouveau).val();

   //nous allons verifier si le libelle fait parti du lot

   var newdata=nouveau+"-"+libelleheure+",";

   var etape1=5;
   $.ajax({
     url: '../ajax/hours.php',
    type: 'POST',
    async:true,
    data: 'alldata='+concatidHselected+'&etape='+etape1+'&nouveau='+nouveau+'&newdata='+newdata,
    dataType: 'text',
    success: function (content, statut) {


      if(content==0)
      {
        $("#concatidH").val($("#concatidH").val().replace(nouveau+",", ""));



        $("#ligneDefinition"+nouveau).remove();
        $("#ligneRowHeurdeb"+nouveau).remove();
        $("#ligneRowHeurfin"+nouveau).remove();
        $("#ligneRowdeleteH"+nouveau).remove();

        recalculLigneH();

      }else if(content==1)
      {
        $("#concatidH").val($("#concatidH").val().replace(nouveau+",", ""));
        $("#concatidHselected").val($("#concatidHselected").val().replace(nouveau+"-"+libelleheure+",",""));
        $("#selecteionsH").val($("#selecteionsH").val().replace(libelleheure+",",""));



        $("#ligneDefinition"+nouveau).remove();
        $("#ligneRowHeurdeb"+nouveau).remove();
        $("#ligneRowHeurfin"+nouveau).remove();
        $("#ligneRowdeleteH"+nouveau).remove();

        recalculLigneH();
      }

    }
   });




}

 function AddligneH()
 {
   //ajouter une ligne de definition de l'heure



   // var longeur=checklastH();
   //
   // if(longeur==1)
   // {
   //   alert("ancien");
   // }else {
   //   alert("nouveau");
   // }


   var nb=$("#nbligneH").val();
   var nouveau= parseInt(nb)+1;
   var concatidH=$("#concatidH").val();
   $("#concatidH").val(concatidH+nouveau+",");
   recalculLigneH();

   getListeHeureLib(nouveau);



   var ligne="<div class=\"form-group row\" id=\"ligneDefinition"+nouveau+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Libellé Heure "+nouveau+" <span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-5\">";
   ligne=ligne+"<select class=\"form-control\" name=\"libelleheure"+nouveau+"\"  id=\"libelleheure"+nouveau+"\" onchange=\"ajouterlibeller("+nouveau+")\">";

   ligne=ligne+"</select>";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //ligne heure debut

   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowHeurdeb"+nouveau+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Heure Debut<span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-5\">";
   ligne=ligne+"<input type=\"text\"  id=\"heuredeb"+nouveau+"\" name=\"heuredeb"+nouveau+"\" class=\"floating-label mdl-textfield__input\"  placeholder=\"Heure de début\">";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";

   //ligne heure de fin

   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowHeurfin"+nouveau+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Heure Fin<span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-5\">";
   ligne=ligne+"<input type=\"text\"  id=\"heurefin"+nouveau+"\" name=\"heurefin"+nouveau+"\" class=\"floating-label mdl-textfield__input\" placeholder=\"Heure de début\">";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";

   //ligne bouton supprimer
   // ligne=ligne+"<div class=\" row\">";
   ligne=ligne+"<div class=\"pull-right\" id=\"ligneRowdeleteH"+nouveau+"\">";
   ligne=ligne+"<button type=\"button\" id=\"delete"+nouveau+"\" name=\"delete"+nouveau+"\"  onclick=\"deletedrowLibelleH("+nouveau+")\" class=\"btn btn-danger pull-right\"><i class=\"fa fa-trash\"></i></button>";
   ligne=ligne+"</div></br></br>";

   $('#dynamic_field1').append(ligne);

   $('#heurefin'+nouveau).bootstrapMaterialDatePicker
   ({
     date:false,
     shortTime: false,
     format: 'HH:mm',
     lang: 'fr',
    cancelText: '<?php echo L::AnnulerBtn ?>',
    okText: 'OK',
    clearText: '<?php echo L::Eraser ?>',
    nowText: '<?php echo L::Now ?>'

   });

   $('#heuredeb'+nouveau).bootstrapMaterialDatePicker
   ({
     date: false,
     shortTime: false,
     format: 'HH:mm',
     lang: 'fr',
    cancelText: '<?php echo L::AnnulerBtn ?>',
    okText: 'OK',
    clearText: '<?php echo L::Eraser ?>',
    nowText: '<?php echo L::Now ?>'

   });

   $('#heurefin'+nouveau).rules( "add", {
       required: true,
       messages: {
       required: "<?php echo L::RequiredChamp ?>"
}
     });

     $('#heuredeb'+nouveau).rules( "add", {
         required: true,
         messages: {
         required: "<?php echo L::RequiredChamp ?>"
 }
       });

       $('#libelleheure'+nouveau).rules( "add", {
           required: true,
           messages: {
           required: "<?php echo L::RequiredChamp ?>"
   }
         });


   $("#libelleheure"+nouveau).select2();



 }

 function AddLibRow()
 {
   var nb=$("#nbLib").val();
   var nouveau= parseInt(nb)+1;
   var concatLibs=$("#concatLibs").val();
   $("#concatLibs").val(concatLibs+nouveau+"@");
   recalculLibnb();

   var ligne="<div class=\"form-group row\" id=\"ligneRowName"+nouveau+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Libellé Heure "+nouveau+" <span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-5\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"heurelib"+nouveau+"\"  id=\"heurelib"+nouveau+"\" >";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //bouton de suppression
   ligne=ligne+"<div class=\"pull-right\" id=\"ligneRow"+nouveau+"\">";
   ligne=ligne+"<button type=\"button\" id=\"deleteLib"+nouveau+"\" name=\"deleteLib"+nouveau+"\"  onclick=\"deletedLibrow("+nouveau+")\" class=\"btn btn-danger pull-right\"><i class=\"fa fa-trash\"></i></button>";
   ligne=ligne+"</div></br></br>";

$('#dynamic_field').append(ligne);

$('#heurelib'+nouveau).rules( "add", {
    required: true,
    messages: {
    required: "Merci de renseigner le libellé de l'heure de cours"
}
  });

 }

 function recalculLigneH()
 {
   var concatidH=$("#concatidH").val();
   var tab=concatidH.split(",");
   var nbtab=tab.length;
   var nbtabnew=parseInt(nbtab)-1;
   $("#nbligneH").val(nbtabnew);
   if((nbtabnew==0))
   {
       desactivatebtn1();
   }else if((nbtabnew>0)){
     activatebtn1();
   }

 }

 function recalculLibnb()
 {
   var concatLibs=$("#concatLibs").val();
   var tab=concatLibs.split("@");
   var nbtab=tab.length;
   var nbtabnew=parseInt(nbtab)-1;
   $("#nbLib").val(nbtabnew);

   if((nbtabnew==0))
   {
       desactivatebtn();
   }else if((nbtabnew>0)){
     activatebtn();
   }
 }


 function activatebtn1()
 {
   $("#submit2").prop("disabled",false);
 }

 function activatebtn()
 {
   $("#submit1").prop("disabled",false);
 }

 function desactivatebtn1()
 {
   $("#submit2").prop("disabled",true);
 }

 function desactivatebtn()
 {
 $("#submit1").prop("disabled",true);
 }

 jQuery(document).ready(function() {

   // AddLibRow();

$("#submit1").prop("disabled",true);

   $('#LibNew').click(function(){
         //AddLibRow();
         checkRow();

    });

    $("#HeureNew").click(function(){
      // AddligneH();
      checklastH();
    });


$("#teatcher").select2();
$("#matiere").select2();

<?php
if($etablissementType==1||$etablissementType==3)
{
  ?>
$("#classe").select2();
  <?php
}else {
  ?>
  $("#classe").select2({

    tags: true,

  tokenSeparators: [',', ' ']

  });
  <?php
}
 ?>




 $("#FormAddSubject").validate({
   errorPlacement: function(label, element) {
   label.addClass('mt-2 text-danger');
   label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
   $(element).parent().addClass('has-danger')
   $(element).addClass('form-control-danger')
   },
   success: function (e) {
       $(e).closest('.control-group').removeClass('error').addClass('info');
       $(e).remove();
   },
   rules:{
     heuredeb:"required",
     heurefin:"required",
     libelleheure:"required"
   },
   messages:{
     heuredeb:"Merci de renseigner l'heure de début",
     heurefin:"Merci de renseigner l'heure de fin",
     libelleheure:"Merci de selectionner le libellé de l'heure"
   }

 });


  $("#FormAddmatlib").validate({

    errorPlacement: function(label, element) {
    label.addClass('mt-2 text-danger');
    label.insertAfter(element);
  },
  highlight: function(element, errorClass) {
    $(element).parent().addClass('has-danger')
    $(element).addClass('form-control-danger')
  },
  success: function (e) {
        $(e).closest('.control-group').removeClass('error').addClass('info');
        $(e).remove();
    },
      rules:{
        heurelib:"required"
      },
      messages:{
        heurelib:"Merci de renseigner le libellé de l'heure de cours"
      },
        submitHandler: function(form) {
          //nous allons verifié si cet libelle de l'heure de cours existe dejà ou non dans le système

          var etape=1;
          var codeEtab="<?php echo $codeEtabAssigner; ?>";
          var session="<?php  echo $libellesessionencours;?>";

          $.ajax({
            url: '../ajax/hours.php',
            type: 'POST',
            async:true,
            data: 'libelleheure='+heurelib+'&etape='+etape+'&codeEtab='+codeEtab+'&session='+session,
            dataType: 'text',
            success: function (content, statut) {

              // alert(content);
              if(content==0)
              {
                //nouveau libelle a ajouter
                form.submit();
              }else if(content>0)
              {
                //cet libelle existe deja  dans le sysyème

                Swal.fire({
                type: 'warning',
                title: '<?php echo L::WarningLib ?>',
                text: "Cet libellé existe deja dans le système",

              })
              }
            }
          });

        }

  });



      });






 </script>
    <!-- end js include path -->
  </body>

</html>
