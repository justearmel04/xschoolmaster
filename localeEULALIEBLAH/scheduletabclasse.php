<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Teatcher.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Salle.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');
require_once('../controller/functions.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$codeEtab=$_GET['codeEtab'] ;
$idClasse=$_GET['classe'];
$session=$_GET['session'];

//
$etabs=new Etab();
$student=new Student();
$classe=new Classe();
$parentX=new ParentX();
$matieres=new Matiere();
$sallex=new Salle();
$dataday=$etabs->getAllweeksLimit();

$libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
$libelleclasse=$classe->getInfosofclassesbyId($idClasse,$session);
$logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
$etablissementType=$etabs->DetermineTypeEtab($codeEtab);


$students=$student->getAllStudentOfClassesId($idClasse,$session);

//nous allons faire nos différents calculs

//le nombre d'eleve de cette classe

$nbclassestudents=$classe->getAllStudentOfThisClassesNb($idClasse,$session);

//le nombre de fille affecté

$nbaffectefilles=$classe->getAllStudentFilleAffectOfThisClassesNb($idClasse,$session);

//nombre de fille non Affecté

$nbnonaffectefilles=$classe->getAllStudentFilleNAffectOfThisClassesNb($idClasse,$session);

//nombre de mec affectés

$nbaffectemecs=$classe->getAllStudentMecAffectOfThisClassesNb($idClasse,$session);

//nombre de mec non affectés

$nbnonaffectemecs=$classe->getAllStudentMecNAffectOfThisClassesNb($idClasse,$session);

//nombre de fille redoublante

$nbredoubfilles=$classe->getAllStudentFilleRedtOfThisClassesNb($idClasse,$session);

//nombre non redoublante fille

$nbnredoubfilles=$classe->getAllStudentFilleNRedtOfThisClassesNb($idClasse,$session);

//nombre de mec redoublant

$nbredoubmecs=$classe->getAllStudentMecRedtOfThisClassesNb($idClasse,$session);

//nombre non redoublant mec

$nbnredoubmecs=$classe->getAllStudentMecNRedtOfThisClassesNb($idClasse,$session);

$allLibellesHours=$etabs->getHoursAllLibs($codeEtab,$session);

$allteatchers=$parentX->getAllTeatcherOfThisClasseSchool($codeEtab,$idClasse);


require('fpdf/fpdf.php');

class PDF extends FPDF
{
function Footer()
{
    // Positionnement à 1,5 cm du bas
    $this->SetY(-15);
    // Police Arial italique 8
    $this->SetFont('Arial','I',8);
    // Numéro et nombre de pages
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    // $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
}

// Tableau simple
function BasicTable($header, $data)
{
    // En-tête
    foreach($header as $col)
        $this->Cell(40,7,$col,1);
    $this->Ln();
    // Données
    foreach($data as $row)
    {
        foreach($row as $col)
            $this->Cell(40,6,$col,1);
        $this->Ln();
    }
}
}

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->SetMargins(3,0);
$pdf->SetFont('Times','B',  16);
$pdf->AddPage("L");
$pdf->Ln(10);
$pdf->Image("../logo_etab/".$codeEtab."/".$logoEtab,5,5,25,25);
$pdf->Ln(12);
$pdf -> SetX(60);
$pdf-> SetFont('Times','B',  15);
$pdf->Cell(176,5,$libelleEtab,0,0,'C');
$pdf->Ln(8);
$pdf -> SetX(60);
$pdf-> SetFont('Times','',  13);
$pdf->Cell(176,5,L::EmploisdutempsClasses.' : ' .$libelleclasse,0,0,'C');
$pdf->Ln(8);
$pdf -> SetX(60);
$pdf-> SetFont('Times','',  11);
$pdf->Cell(176,5, utf8_decode(L::ScolaryyearMenu).' : '.$session,0,0,'C');
$pdf->Ln(10);
// $pdf -> SetX(20);
// $pdf-> SetFont('Times','',  11);
// $pdf->Cell(176,5,$libelleclasse,0,0,'C');
// $pdf->Ln(20);

$pdf->SetFillColor(230,230,0);
$pdf->SetLineWidth(.3);
$pdf->SetFont('Times','B',12);
$pdf->Cell(47,8,L::Times,1,0,'C');
$tabLibday=array();
$i=0;
foreach ($dataday as $value) :
$tabLibday[$i]=$value->short_days;
if($_SESSION['user']['lang']=="fr")
{
$pdf->Cell(47,8,$value->libelle_days,1,0,'C');
}else {
$pdf->Cell(47,8,$value->libelleen_days,1,0,'C');
}

$i++;
endforeach;
$pdf->Ln();
$pdf->SetFillColor(230,230,0);
$pdf->SetLineWidth(.3);
$pdf->SetFont('Times','B',12);

$nblibday=count($tabLibday);
// echo $nblibday;
  foreach ($allLibellesHours as $value1):
    $idheure=$value1->id_heure;
    $debut=returnHours($value1->heuredeb_heure);
    $fin=returnHours($value1->heurefin_heure);
    $concathours=$debut." - ".$fin;
    $pdf->Cell(47,8,$concathours,1,0,'C');
    for($j=0;$j<$nblibday;$j++)
    {

  $nbmatiereday=$etabs->getnumberofmatierethisday($idheure,$tabLibday[$j],$codeEtab,$session,$_GET['classe']);
    // echo $nbmatiereday;
  if($nbmatiereday>0)
  {
    $datas=$etabs->getMatierethisday($idheure,$tabLibday[$j],$codeEtab,$session,$_GET['classe']);
    $tabdatas=explode("*",$datas);
    $libsalle= $sallex->getLibelleSallesByschoolCodewithId($codeEtab,$tabdatas[2]);
    $pdf->SetFillColor(230,230,0);
    $pdf->SetLineWidth(.3);
    $pdf->SetFont('Times','B',12);
    $pdf->Cell(47,8,raccoursisseur($tabdatas[0]).'('.$libsalle.')' ,1,0,'C');
  }else {
    $pdf->Cell(47,8,"",1,0,'C');
  }

    }
    $pdf->Ln();
  endforeach;

  $pdf->Ln();
  $pdf->AddPage("L");
  $pdf->Ln(10);
  $pdf->Image("../logo_etab/".$codeEtab."/".$logoEtab,5,5,25,25);
  $pdf->Ln(10);
  $pdf -> SetX(60);
  $pdf-> SetFont('Times','B',  15);
  $pdf->Cell(176,5,$libelleEtab,0,0,'C');
  $pdf->Ln(8);
  $pdf -> SetX(60);
  $pdf-> SetFont('Times','',  13);
  $pdf->Cell(176,5,L::EmploisdutempsListProfs.' : ' .$libelleclasse,0,0,'C');
  $pdf->Ln(8);

  $pdf->SetFillColor(230,230,0);
  $pdf->SetLineWidth(.3);
  $pdf->SetFont('Times','B',12);
  $pdf -> SetX(10);
  $pdf->Cell(8,8,utf8_decode(L::Nums),1,0,'C');
  $pdf->Cell(90,8,utf8_decode(L::NamestudentTab),1,0,'C');
  $pdf->Cell(70,8,L::ContactsParentTabcaps,1,0,'C');
  $pdf->Cell(70,8,L::MatiereBoldCaps,1,0,'C');
  $pdf->Cell(40,8,L::CoefsCaps,1,0,'C');
  $pdf->Ln();

  $pdf->SetFont('Times','B',10);
  $pdf->SetFillColor(230,230,0);

  $i=1;

  foreach ($allteatchers as $value):
      $pdf -> SetX(10);
      $pdf->Cell(8,8,$i,1,0,'C');
      $pdf->Cell(90,8,$value->nom_compte." ".$value->prenom_compte,1,0,'C');
      $pdf->Cell(70,8,$value->tel_compte." ".$value->id_cours,1,0,'C');
      // $pdf->Cell(70,8,$value->id_cours." ".$value->codeEtab,1,0,'C');
      $pdf->Cell(70,8,$matieres->getMatiereLibelleByIdMat($value->id_cours,$value->codeEtab),1,0,'C');
      $pdf->Cell(40,8,$matieres->getMatcoef($value->id_cours,$value->codeEtab,$value->session_disp),1,0,'C');
      $pdf->Ln();
      $i++;
  endforeach;


$pdf->Output();



 ?>
