<?php
session_start();
require_once('../class/User.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();
$user=new User();
$classe=new Classe();
$etabs=new Etab();
$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$parent=new ParentX();
$parents=$parent->getAllparentInfobyId($_GET['compte']);
$tabparents=explode("*",$parents);
$alletab=$etabs->getAllEtab();

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title"><?php echo L::AddParents ?></div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#"><?php echo L::ParentsMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active"><?php echo L::AddParents ?></li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addparok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: '<?php echo L::Felicitations ?>',
  text: "<?php echo $_SESSION['user']['addparok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::AddNews ?>',
  cancelButtonText: '<?php echo L::nnulerBtn ?>',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="index.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addparok']);
                    }

                     ?>

              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <div class="card card-box">
                          <div class="card-head">
                              <header><?php echo L::GeneralInfosParentTabsingle ?></header>

                          </div>

                          <div class="card-body" id="bar-parent">
                              <form  id="FormAddLocalAd" class="form-horizontal" action="../controller/parent.php" method="post" enctype="multipart/form-data">
                                  <div class="form-body">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3"><?php echo L::Account ?>
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-5">
                                          <input type="text" class="form-control input-height" name="comptead" id="comptead" value="PARENT" readonly></div>
                                          <input type="hidden" name="etape" id="etape" value="2"/>
                                          <input type="hidden" name="compte" id="compte" value="<?php echo $_GET['compte']?>"/>
                                          <input type="hidden" name="session" id="session" value="<?php echo $libellesessionencours; ?>">
                                          <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner; ?>">
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3"><?php echo L::CniAccount ?>

                                        </label>
                                        <div class="col-md-5">
                                            <input name="cniTea" id="cniTea" type="text" value="<?php echo $tabparents[6];?>" placeholder="<?php echo L::EnterCniAccount ?> " class="form-control input-height" /> </div>
                                    </div>

                                  <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::Name ?>
                                              <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-5">
                                              <input type="text" value="<?php echo $tabparents[0];?>" name="nomTea" id="nomTea" data-required="1" placeholder="<?php echo L::EnterName ?>" class="form-control input-height" /> </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::PreName ?>
                                              <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-5">
                                              <input type="text" value="<?php echo $tabparents[1];?>" name="prenomTea" id="prenomTea" data-required="1" placeholder="<?php echo L::Enterparentprename ?>" class="form-control input-height" /> </div>
                                      </div>

                                      <div class="form-group row">
                                        <label class="control-label col-md-3"><?php echo L::BirthstudentTab ?>

                                        </label>
                                            <div class="col-md-5">
                                                <input type="date" value="<?php echo $tabparents[2];?>" placeholder="<?php echo L::EnterBirthstudentTab ?>" name="datenaisTea" id="datenaisTea" data-mask="99/99/9999" class="form-control input-height">
                                                  <span class="help-block"><?php echo L::Datesymbole ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                                <label class="control-label col-md-3"><?php echo L::SexestudentTab ?>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <select class="form-control input-height" name="sexeTea" id="sexeTea">

                                                        <option <?php if($tabparents[3]=="M"){echo "selected";}?> value="M"><?php echo L::SexeM ?></option>
                                                        <option <?php if($tabparents[3]=="F"){echo "selected";}?>  value="F"><?php echo L::SexeF ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::PhonestudentTab ?>

                                          </label>
                                          <div class="col-md-5">
                                              <input name="contactTea"  value="<?php echo $tabparents[4];?>"id="contactTea" type="text" placeholder="<?php echo L::EnterPhoneNumber ?>" class="form-control input-height" /> </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::PersonalEmail ?>

                                          </label>
                                          <div class="col-md-5">
                                              <div class="input-group">
                                                  <span class="input-group-addon">
                                                          <i class="fa fa-envelope"></i>
                                                      </span>
                                                  <input type="text" class="form-control input-height" value="<?php echo $tabparents[7];?>" name="emailTea" id="emailTea" placeholder="<?php echo L::EnterPersonalEmail ?>"> </div>
                                          </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::ProfessionsParentTab ?>

                                          </label>
                                          <div class="col-md-5">
                                              <input name="fonctionTea" id="fonctionTea" value="<?php echo $tabparents[5];?>" type="text" placeholder="<?php echo L::EnterProfessionsParentTab ?> " class="form-control input-height" /> </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::Nationalite ?>

                                          </label>
                                          <div class="col-md-5">
                                              <input name="nation" id="nation" type="text" value="<?php echo $tabparents[10];?>" placeholder="<?php echo L::EnterNationalites ?> " class="form-control input-height" /> </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::HabitationLocation ?>

                                          </label>
                                          <div class="col-md-5">
                                              <input name="lieuH" id="lieuH" type="text" placeholder="<?php echo L::EnterHabitationLocation ?> " value="<?php echo $tabparents[11];?>" class="form-control input-height" /> </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::ChildNb ?>

                                          </label>
                                          <div class="col-md-5">
                                              <input name="nbchild" id="nbchild" type="number" min=0 placeholder="<?php echo L::EnterChildNb  ?> " value="<?php echo $tabparents[12];?>" class="form-control input-height" /> </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::ChildNbscolarisy ?>

                                          </label>
                                          <div class="col-md-5">
                                              <input name="nbchildsco" id="nbchildsco" type="number" value="<?php echo $tabparents[13];?>" min=0 placeholder="<?php echo L::EnterChildNbscolarisy ?> " class="form-control input-height" /> </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::ProfessianalAdress ?>

                                          </label>
                                          <div class="col-md-5">
                                              <input name="adrespro" id="adrespro" type="text"  placeholder="<?php echo L::EnterProfessianalAdress ?> " value="<?php echo $tabparents[14];?>"  class="form-control input-height" /> </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::Society ?>

                                          </label>
                                          <div class="col-md-5">
                                              <input name="societe" id="societe" type="text"  placeholder="<?php echo L::EnterSociety ?>" value="<?php echo $tabparents[15];?>" class="form-control input-height" /> </div>
                                      </div>






                <div class="form-actions">
                                      <div class="row">
                                          <div class="offset-md-3 col-md-9">
                                            <button class="btn btn-success" type="submit"><?php echo L::ModifierBtn ?></button>
                                            <!--a class="btn btn-info" onclick="modify(<?php //echo $_GET['compte']?>)">Modifier</a-->
                                            <a class="btn btn-danger" href="parents.php" ><?php echo L::AnnulerBtn ?></a>
                                          </div>
                                        </div>
                                     </div>
              </div>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <!-script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script-->
   <script src="../assets2/js/pages/material_select/getmdl-select.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script>

 function addFrench()
 {
   var etape=1;
   var lang="fr";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function addEnglish()
 {
   var etape=1;
   var lang="en";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 jQuery(document).ready(function() {

   $("#FormAddLocalAd").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

        confirmTea:{
          'required': {
              depends: function (element) {
                  return ($('#passTea').val() !='');

              }
          },
            minlength: 6,
            equalTo:'#passTea'
        },
        // fonctionTea:"required",
        // cniTea:"required",

        loginTea:"required",
        // emailTea: {
        //            required: true,
        //            email: true
        //        },
        // contactTea:"required",
        // datenaisTea:"required",
        prenomTea:"required",
        nomTea:"required",
        gradeTea:"required",
        dateEmbTea:"required",
        // sexeTea:"required"



      },
      messages: {
        confirmTea:{
            required:"<?php echo L::Confirmcheck ?>",
            minlength:"<?php echo L::Confirmincheck ?>",
            equalTo: "<?php echo L::ConfirmSamecheck ?>"
        },
        passTea: {
            required:"<?php echo L::Passcheck ?>",
            minlength:"<?php echo L::Confirmincheck ?>"
        },
        loginTea:"<?php echo L::Logincheck ?>",
        emailTea:"<?php echo L::PleaseEnterEmailAdress ?>",
        contactTea:"<?php echo L::PleaseEnterPhoneNumber ?>",
        datenaisTea:"<?php echo L::PleaseEnterPhonestudentTab ?>",
        prenomTea:"<?php echo L::PleaseEnterPrename ?>",
        nomTea:"<?php echo L::PleaseEnterName ?> ",
        fonctionTea:"<?php echo L::PleaseEnterFonction ?>",
        gradeTea:"<?php echo L::EnterGrade ?>",
        dateEmbTea:"<?php echo L::EnterDateEmbauche ?>",
        sexeTea:"<?php echo L::PleaseEnterSexe ?>",
        cniTea:"<?php echo L::PleaseEnterCniAccount ?>"
      },
      submitHandler: function(form) {
        //verifier si ce compte n'existe pas encore dans la base de données
        form.submit();

      }


   });


 });
 </script>
    <!-- end js include path -->
  </body>

</html>
