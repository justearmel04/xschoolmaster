
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>


</head>
<body>



<div class=" container spacer col-md-6 col-xs-12 col-md-offset-3">
    <div class="panel panel-default">
        <div class="panel-headin" style="background-color:#009fe3; font-size:20px;color: white;"> Informations  du parent ou du tuteur de l'élève  </div>


        <div class="panel-body">


           <form>


        <div class="row">
				<div class="form-group col-xs-6">
					<label for="prenom"> Nom *  </label>
					<input type="text" class="form-control" name="nom" />	
				</div> 

			
				<div class="form-group col-xs-6">
					<label for="prenom"> Prénom *  </label>
					<input type="text" class="form-control" name="prenom" />	
				</div>	


		 </div>

		 
         <div class="form-group">
                <label class="control-label"> Pays de résidence * </label>
                        
                <select class="form-control" name="pays" required="required">    
                    <option value=""> Cameroun </option>
                    <option value=""> Congo Brazzaville </option>
                    <option value=""> Cote d'Ivoire </option>
                    <option value=""> Burkina Faso </option> 
                    <option value=""> Gabon </option>    
                    <option value=""> Tchad </option>    
                    <option value=""> Niger </option> 
                    <option value=""> RD Congo </option>             
                </select>            
              

          </div>



		<div class="row">
				<div class="form-group col-xs-6">
					<label > Téléphone * </label>
					<input type="text" class="form-control" name="telephone"/>
				</div>

				<div class="form-group col-xs-6">
					<label > Fonction </label>
				   <input type="text" class="form-control" name="fonction"/>

				</div>		
		 </div>



		<div class="row">
				<div class="form-group col-xs-6">
					<label > Email * </label>
					   
					<input type="email" class="form-control" name="mail" />
				</div>

				<div class="form-group col-xs-6">
					<label > CNI</label>
				   <input type="text" class="form-control" name="cni"/>

				</div>		
		</div>


		 <div class="row">
				<div class="form-group col-xs-6">
					<label > Date de naissance  </label>
					<input type="date" class="form-control glyphicon glyphicon-calendar" name="date"/>
				</div>

				<div class="form-group col-xs-6">
				   <label > Sexe  </label>
				   <select class="form-control" name="sexe" required="required">    
	                 
	                    <option value="M"> Masculin </option>
	                    <option value="F"> Feminin  </option>          
                  </select>

				</div>		
		 </div>


        <div class="form-group">
                   <label class="control-label"> Login * </label>
                   <input type="text" class="form-control" name="login"/>
		</div>


		<div class="row">
				<div class="form-group col-xs-6">
					<label > Mot de passe * </label>
					<input type="password" class="form-control" name="pass"/>
				</div>

				<div class="form-group col-xs-6">
					<label > Confirmation du mot de passe * </label>
				   <input type="password" class="form-control" name="confirmpass"/>

				</div>		
		   </div>

                <div>
                   <button type="button" onclick="SWal.fire();" class="btn btn-info" style="width: 20%"> submit </button>


                    <a  class="btn btn-danger suivant " style="width:20%;"> Annuler</a>
                </div>

            </form>

        </div>
    </div>
</div>


<script type="text/javascript">

		$("#submit").click(function(){

			alert("OK");
		}

			)

</script>


</body>
</html>