<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Admin.php');
require_once('../class/Classe.php');
require_once('../class/Teatcher.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Student.php');

$session= new Sessionacade();
$student=New Student();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$teatcher=new Teatcher();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$admin=new Admin();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

if(isset($_GET['classe']))
{
  $idclasse=@$_GET['classe'];
}

if(isset($_POST['classe']))
{
    $idclasse=@$_POST['classe'];
}

if(isset($_GET['codeEtab']))
{
  $compteEtab=@$_GET['codeEtab'];
}

if(isset($_POST['codeEtab']))
{
    $compteEtab=@$_POST['codeEtab'];
}
$nbsessionOn=$session->getNumberSessionEncoursOn($compteEtab);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($compteEtab);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
}

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();

$libelleclasse=$classe->getInfosofclassesbyId($idclasse,$libellesessionencours);
//le nombre de professeur enseignant dans cette classex
$teatchernb=$teatcher->getNbOfTeatcherClasseSchool($idclasse,$compteEtab,$libellesessionencours);
//liste des professeurs enseignant dans cette classe
$teatcherofschool=$teatcher->getAllTeatcherOfThisClasseSchool($idclasse,$compteEtab);

// $classesOfThisSchool=$etabs->get
$studentOfSchool=$student->getAllstudentofthisclassesSession($idclasse,$libellesessionencours);


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Gestion Eleves - Classe : <?php echo $libelleclasse;?> </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li></i>&nbsp;<a class="parent-item" href="index.html">Gestion Eleves</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Classe : <?php echo $libelleclasse;?></li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->
					      </div>
						</div>
					<!-- end widget -->

          <?php

                if(isset($_SESSION['user']['updateteaok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: 'Félicitations',
            text: '<?php echo $_SESSION['user']['updateteaok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['updateteaok']);
                }

                 ?>
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


          <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                  <div class="card-head">
                    <header></header>
                  </div>
                  <div class="card-body ">
                        <div class = "mdl-tabs mdl-js-tabs">
                           <div class = "mdl-tabs__tab-bar tab-left-side">
                              <a href = "#tab4-panel" class = "mdl-tabs__tab is-active">Elèves</a>
                              <!--a href = "#tab5-panel" class = "mdl-tabs__tab">Notes</a>
                              <a href = "#tab6-panel" class = "mdl-tabs__tab">Présences</a-->

                           </div>


<div class = "mdl-tabs__panel is-active p-t-20" id = "tab4-panel">
                             <div class="row">
                               <div class="col-md-12 col-sm-12">
                                                         <div class="card card-box">
                                                             <div class="card-head">
                                                                 <header>Rechercher</header>

                                                             </div>
                                                             <div class="card-body " id="bar-parent">
                                                               <form method="post" id="FormSearchStudent" action="showInfosclasse.php">
                                                                   <div class="row">

                                                                 <div class="col-md-6 col-sm-6">
                                                                 <!-- text input -->
                                                                 <div class="form-group">
                                                                     <label>Nom & Prénoms Elève</label>
                                                                     <select class="form-control " id="idstudent" name="idstudent" style="width:100%">
                                                                         <option value="">Selectionner un Elève</option>
                                                                         <?php
                                                                         $i=1;
                                                                           foreach ($studentOfSchool as $namevalue):
                                                                           ?>
                                                                           <option value="<?php echo utf8_encode(utf8_decode($namevalue->id_compte)); ?>" <?php if(isset($_POST['idstudent'])&&($_POST['idstudent']==$namevalue->id_compte)){echo "selected";} ?>><?php echo utf8_encode(utf8_decode($namevalue->nom_eleve." ".$namevalue->prenom_eleve)); ?></option>

                                                                           <?php
                                                                                                            $i++;
                                                                                                            endforeach;
                                                                                                            ?>

                                                                     </select>
                                                                     <input type="hidden" name="search1" id="search1"/>
                                                                     <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $compteEtab; ?>">
                                                                     <input type="hidden" name="classe" id="classe" value="<?php echo $idclasse; ?>">
                                                                     <input type="hidden" name="compte" id="compte" value="<?php echo $compteEtab;?>"/>
                                                                 </div>


                                                             </div>
                                                             <div class="col-md-4 col-sm-4">
                                                               <div class="form-group">
                                                               <button type="submit" class="btn btn-danger btn-md" style="margin-top:28px;">Rechercher</button>
                                                              </div>
                                                             </div>
                                                                   </div>


                                                               </form>
                                                             </div>
                                                         </div>
                                                     </div>
                             </div>
                             <div class="row">
                                                     <div class="col-md-12">
                                                         <div class="tabbable-line">
                                                            <ul class="nav nav-pills nav-pills-rose">
                                              <li class="nav-item tab-all" style="display:none"><a class="nav-link active show"
                                                href="#tab1" data-toggle="tab">Liste</a></li>

                                            </ul>
                                                             <div class="tab-content">
                                                                 <div class="tab-pane active fontawesome-demo" id="tab1">
                                                                     <div class="row">
                                                              <div class="col-md-12">
                                                                  <div class="card  card-box">
                                                                      <div class="card-head">
                                                                          <header><!--a class="btn btn-primary" href="admission.php?codeEtab=<?php //echo $compteEtab?>"><i class="fa fa-plus"></i> Nouveau Eleve</a--></header>

                                                                          <div class="tools">
                                                                              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                                                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                                                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                                                          </div>

                                                                      </div>

                                                                      <div class="card-body ">
                                                                        <div class="table-scrollable">
                                                                          <?php
                                                                          if(isset($_POST['search1']))
                                                                          {
                                                                            $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
                                                                            $classe=htmlspecialchars(addslashes($_POST['classe']));
                                                                            $idstudent=htmlspecialchars(addslashes($_POST['idstudent']));

                                                                            $sessionencours=$session->getSessionEncours($codeEtab);
                                                                            $tabsessionencours=explode("*",$sessionencours);
                                                                            $sessionEtab=$tabsessionencours[0];

                                                                            $studentOfSchool=$student->getAllstudentofthisclassesSessionUnique($classe,$sessionEtab,$idstudent);

                                                                          }
                                                                           ?>

                                                                          <table class="table table-hover table-checkable order-column full-width" id="example4">
                            					                                        <thead>
                            					                                            <tr>
                                                                                    <th> Photo </th>
                            					                                                <th style="width:150px;"> Matricule </th>
                            					                                                <th> Nom & Prénoms</th>
                                                                                       <th>Email</th>
                            					                                                 <th> <?php echo L::Actions?> </th>
                            					                                            </tr>
                            					                                        </thead>
                            					                                        <tbody>
                                                                                <?php
                                                                                $i=1;

                            //var_dump($teatchers);
                                                                                  foreach ($studentOfSchool as $value):
                                                                                  ?>
                            																<tr class="odd gradeX" ondblclick="myFunction(<?php echo $value->id_compte?>)">

                                                              <td class="patient-img">
                                                                <?php
                                                                if(strlen($value->photo_compte)>0)
                                                                {
                                                                  $lien="../photo/Students/".$value->matricule_eleve."/".$value->photo_compte;
                                                                }else {
                                                                  $lien="../photo/user5.jpg";
                                                                }

                                                                 ?>
                            																			<img src="<?php echo $lien?>" alt="">
                            																	</td>

                            																	<td><?php
                                                              //echo $value->codeEtab_classe;

                                                              echo $value->matricule_eleve;

                                                              ?></td>

                                                              <td>
                                                                <?php
                                                                $statut=$value->statut_classe;

                                                                echo $value->nom_eleve." ".$value->prenom_eleve;

                                                                ?>
                                                            </td>
                            																	<td class="left"><?php echo $value->email_eleve;?></td>
                                                              <td class="center">

                            																		<a href="#"  onclick="modify(<?php echo $value->id_compte?>)" class="btn btn-tbl-edit btn-xs">
                            																			<i class="fa fa-pencil"></i>
                            																		</a>
                                                                <a class="btn btn-tbl-delete btn-xs" onclick="deleted(<?php echo $value->id_compte?>)">
                                                                  <i class="fa fa-trash-o "></i>
                                                                </a>

                            																	</td>
                            																</tr>
                                                            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                                            <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                                            <script>

                                                            function myFunction(idcompte)
                                                            {
                                                              var codeEtab="<?php echo $compteEtab; ?>";
                                                              //var url="detailslocal.php?compte="+idcompte;
                                                            document.location.href="detailsstudent.php?compte="+idcompte+'&codeEtab='+codeEtab;
                                                            }

                                                            function modify(id)
                                                            {

                                                              var codeEtab="<?php echo $compteEtab; ?>";
                                                              Swal.fire({
                                                title: 'Attention !',
                                                text: "Voulez vous vraiment modifier les informations de cet elève",
                                                type: 'warning',
                                                showCancelButton: true,
                                                confirmButtonColor: '#3085d6',
                                                cancelButtonColor: '#d33',
                                                confirmButtonText: 'Modifier',
                                                cancelButtonText: 'Annuler',
                                              }).then((result) => {
                                                if (result.value) {
                                                  document.location.href="updatestudent.php?compte="+id+'&codeEtab='+codeEtab;
                                                }else {

                                                }
                                              })
                                                            }

                                                            function deleted(id)
                                                            {
                                                              var classe="<?php echo $idclasse; ?>";
                                                              var codeEtab="<?php echo $compteEtab; ?>";
                                                              Swal.fire({
                                                title: 'Attention !',
                                                text: "Voulez vous vraiment supprimer cet elève",
                                                type: 'warning',
                                                showCancelButton: true,
                                                confirmButtonColor: '#3085d6',
                                                cancelButtonColor: '#d33',
                                                confirmButtonText: 'Supprimer',
                                                cancelButtonText: 'Annuler',
                                              }).then((result) => {
                                                if (result.value) {
                                                  document.location.href="../controller/admission.php?etape=3&compte="+id+"&classe="+classe+"&codeEtab="+codeEtab;
                                                }else {

                                                }
                                              })
                                                            }

                                                            </script>


                                                            <?php
                                                                                             $i++;
                                                                                             endforeach;
                                                                                             ?>

                            															</tbody>
                            					                                    </table>
                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                                 </div>

                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                           </div>
                           <div class = "mdl-tabs__panel p-t-20" id = "tab5-panel">
                             <div class="row">
                                                     <div class="col-md-12">
                                                         <div class="tabbable-line">
                                                            <!--ul class="nav nav-pills nav-pills-rose">
                                              <li class="nav-item tab-all"><a class="nav-link active show"
                                                href="#tab1" data-toggle="tab">Liste</a></li>
                                              <li class="nav-item tab-all"><a class="nav-link" href="#tab2"
                                                data-toggle="tab">Grille</a></li>
                                            </ul-->
                                                             <div class="tab-content">
                                                                 <div class="tab-pane active fontawesome-demo" id="tab1">
                                                                     <div class="row">
                                                              <div class="col-md-12">
                                                                  <div class="card  card-box">
                                                                      <div class="card-head">
                                                                          <header><!--a class="btn btn-primary" href="addclasses.php?codeEtab=<?php //echo $compteEtab?>"><i class="fa fa-plus"></i> Nouvelle classe</a--></header>
                                                                          <div class="tools">
                                                                              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                                                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                                                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                                                          </div>
                                                                      </div>
                                                                      <div class="card-body ">
                                                                        <div class="table-scrollable">

                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                                 </div>

                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                           </div>
                           <div class = "mdl-tabs__panel p-t-20" id = "tab6-panel">
                             <div class="row">
                                                     <div class="col-md-12">
                                                         <div class="tabbable-line">

                                                             <div class="tab-content">
                                                                 <div class="tab-pane active fontawesome-demo" id="tab1">
                                                                     <div class="row">
                                                              <div class="col-md-12">
                                                                  <div class="card  card-box">
                                                                      <div class="card-head">
                                                                          <header><!--a class="btn btn-primary" href="addsalles.php?codeEtab=<?php //echo $compteEtab?>"><i class="fa fa-plus"></i> Nouvelle salle</a--></header>
                                                                          <div class="tools">
                                                                              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                                                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                                                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                                                          </div>
                                                                      </div>
                                                                      <div class="card-body ">
                                                                        <div class="table-scrollable">

                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                                 </div>

                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                          </div>
                          <div class = "mdl-tabs__panel p-t-20" id = "tab8-panel">
                            <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="tabbable-line">
                                                           <!--ul class="nav nav-pills nav-pills-rose">
                                              <li class="nav-item tab-all"><a class="nav-link active show"
                                                href="#tab1" data-toggle="tab">Liste</a></li>
                                              <li class="nav-item tab-all"><a class="nav-link" href="#tab2"
                                                data-toggle="tab">Grille</a></li>
                                            </ul-->
                                                            <div class="tab-content">
                                                                <div class="tab-pane active fontawesome-demo" id="tab1">
                                                                    <div class="row">
                                                              <div class="col-md-12">
                                                                  <div class="card  card-box">
                                                                      <div class="card-head">
                                                                          <header><a class="btn btn-primary" href="addmatiereclasse.php?codeEtab=<?php echo $compteEtab?>&classe=<?php echo $idclasse;?>"><i class="fa fa-plus"></i> Nouvelle Matière</a></header>
                                                                          <div class="tools">
                                                                              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                                                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                                                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                                                          </div>
                                                                      </div>
                                                                      <div class="card-body ">
                                                                        <div class="table-scrollable">

                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                          </div>
                        </div>
                  </div>
                </div>
              </div>

          </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
   <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
     <script src="../assets2/js/pages/table/table_data.js" ></script>
     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   //
    $("#idstudent").select2();
   $(document).ready(function() {



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
