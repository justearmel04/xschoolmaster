<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/Classe.php');
require_once('../class/Teatcher.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$alletab=$etabs->getAllEtab();

if(isset($_GET['codeEtab']))
{
  $codeEtabchool=$_GET['codeEtab'];
}

if(isset($_POST['codeEtab']))
{
  $codeEtabchool=$_POST['codeEtab'];
}

$alletabschool=$etabs->getEtablissementbyCodeEtab($codeEtabchool);
$allclassechool=$classe->getAllClassesOfThisSchool($codeEtabchool);
$allprofschool=$teatcher->getAllTeatchersByschoolCode($codeEtabchool);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabchool);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabchool);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
}

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title">Ajouter Matière</div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#">Matière</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active">Ajouter  matière</li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addsalleok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: 'Félicitations !',
  text: "<?php echo $_SESSION['user']['addsalleok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::AddNews ?>',
  cancelButtonText: 'Annuler',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="salles.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addsalleok']);
                    }

                     ?>

              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <div class="card card-box">
                          <div class="card-head">
                              <header>Informations Matière</header>

                          </div>

                          <div class="card-body" id="bar-parent">
                              <form  id="FormAddMatiere" class="form-horizontal" action="../controller/matiere.php" method="post" >
                                  <div class="form-body">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Etablissement
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-5">
                                          <select class="form-control input-height" id="codeEtab" name="codeEtab" style="width:100%" >
                                              <option value="">Selectionner un etablissement</option>
                                              <?php
                                              $i=1;
                                                foreach ($alletabschool as $value):
                                                ?>
                                                <option value="<?php echo $value->code_etab; ?>"><?php echo utf8_encode(utf8_decode($value->libelle_etab)); ?></option>

                                                <?php
                                                                                 $i++;
                                                                                 endforeach;
                                                                                 ?>

                                          </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Classe
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-5">
                                          <select class="form-control input-height" id="classe" name="classe" style="width:100%" >
                                              <option value="">Selectionner une classe</option>
                                              <?php
                                              $i=1;
                                                foreach ($allclassechool as $value):
                                                ?>
                                                <option value="<?php echo $value->id_classe; ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                <?php
                                                                                 $i++;
                                                                                 endforeach;
                                                                                 ?>

                                          </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Professeur
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-5">
                                          <select class="form-control input-height" id="teatcher" name="teatcher" style="width:100%" >
                                              <option value="">Selectionner une Professeur</option>
                                              <?php
                                              $i=1;
                                                foreach ($allprofschool as $value):
                                                ?>
                                                <option value="<?php echo $value->id_compte; ?>"><?php echo utf8_encode(utf8_decode($value->nom_compte." ".$value->prenom_compte)); ?></option>

                                                <?php
                                                                                 $i++;
                                                                                 endforeach;
                                                                                 ?>

                                          </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Matière
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-5">
                                          <input type="text" class="form-control input-height" placeholder="Entrer le Libellé de la matière" name="matiere" id="matiere" value="" ></div>
                                          <input type="hidden" name="etape" id="etape" value="1" />
                                          <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Coefficient
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-5">
                                          <input type="number" min="1" class="form-control input-height" placeholder="Entrer le Coefficient de la matière" name="coef" id="coef" value="" ></div>
                                    </div>






                <div class="form-actions">
                                      <div class="row">
                                          <div class="offset-md-3 col-md-9">
                                              <button type="submit" class="btn btn-info">Enregistrer</button>
                                              <button type="button" class="btn btn-danger">Annuler</button>
                                          </div>
                                        </div>
                                     </div>
              </div>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script>
 jQuery(document).ready(function() {

   $("#FormAddMatiere").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

                coef:"required",
                teatcher:"required",
                classe:"required",
                codeEtab:"required",
                matiere:"required"



      },
      messages: {
        coef:"Merci de renseigner le coefficient de la matère",
        teatcher:"Merci de selectionner un Professeur",
        classe:"<?php echo L::PleaseRenseigneClasse?>",
        codeEtab:"Merci de selectionner un etablissement",
        matiere:"Merci de renseigner le libellé de la Matière"
      },
      submitHandler: function(form) {
        //verifier si ce compte n'existe pas encore dans la base de données
           var etape=1;
           $.ajax({
             url: '../ajax/matiere.php',
             type: 'POST',
             async:false,
             data: 'matiere=' + $("#matiere").val()+ '&codeEtab=' + $("#codeEtab").val() + '&etape=' + etape+'&classe='+$("#classe").val()+'&teatcher='+$("#teatcher").val()+'&coefficient='+$("#coef").val(),
             dataType: 'text',
             success: function (content, statut) {

               if(content==0)
               {
                 //le compte n'existe pas dans la base on peut l'ajouter

                 form.submit();
               }else {
                 //le compte existe dejà dans la base de données
                 Swal.fire({
                 type: 'warning',
                 title: 'Attention',
                 text: '<?php echo L::ClasseAllreadyexiste?>',

                 })

               }

             }


           });
      }


   });


 });
 </script>
    <!-- end js include path -->
  </body>

</html>
