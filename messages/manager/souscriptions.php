<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Teatcher.php');
require_once('../class/Classe.php');

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$admin= new Localadmin();
$localadmins= new Localadmin();
$parents=new ParentX();
$teatcher=new Teatcher();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

$admins=$admin->getAllAdminLocal();

$teatchers=$teatcher->getAllTeatchers();

$classes=$classe->getAllclasses();

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$codesEtab=$etabs->getAllcodesEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$teatchers=$teatcher->getAllTeatchers();

$souscriptions=$etabs->getAllsouscriptions();

// var_dump($souscriptions);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Souscriptions</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Souscriptions</li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

                 <?php

                       if(isset($_SESSION['user']['Updateadminok']))
                       {

                         ?>
                         <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                       <?php
                       //echo $_SESSION['user']['addetabok'];
                       ?>
                       <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
                          </a>
                       </div-->
               <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
               <script src="../assets/js/sweetalert2.min.js"></script>

                   <script>
                   Swal.fire({
                   type: 'success',
                   title: 'Félicitations',
                   text: '<?php echo $_SESSION['user']['Updateadminok']; ?>',

                   })
                   </script>
                         <?php
                         unset($_SESSION['user']['Updateadminok']);
                       }

                        ?>


          <div class="row">

            <div class="col-md-12 col-sm-12">
                                       <div class="panel tab-border card-box">
                                           <header class="panel-heading panel-heading-gray custom-tab ">
                                               <ul class="nav nav-tabs">
                                                   <!--li class="nav-item"><a href="#home" data-toggle="tab" >Liste Offres</a>
                                                   </li>
                                                   <li class="nav-item"><a href="#about" data-toggle="tab">Souscriptions En cours</a>
                                                   </li>
                                                   <li class="nav-item"><a href="#profile" data-toggle="tab">Relance Souscriptions</a>
                                                   </li-->
                                                   <li class="nav-item" style="display:none"><a href="#contact" class="active" data-toggle="tab"></a>
                                                   </li>
                                               </ul>
                                           </header>
                                           <div class="panel-body">
                                               <div class="tab-content">
                                                   <div class="tab-pane " id="home">
                                                     <div class="card-body ">
                                                       <div class="pull-right">
                                                         <a href="#" class="btn btn-primary btn-md"> <i class="fa fa-plus"> Nouvelle offre</i> </a>

                                                       </div><br>
                                                       <table id="example1" class="display" style="width:100%;">
                                                           <thead>
                                                               <tr>
                                                                   <th>Parents</th>
                                                                   <th>Souscriptions</th>
                                                                   <th>Daye début</th>
                                                                   <th>Date de fin</th>
                                                                   <th>Actions</th>

                                                               </tr>
                                                           </thead>
                                                           <tbody>
                                                               <tr>
                                                                   <td>Tiger Nixon</td>
                                                                   <td>System Architect</td>
                                                                   <td>Edinburgh</td>
                                                                   <td>61</td>
                                                                    <td>61</td>

                                                               </tr>

                                                           </tbody>
                                                       </table>

                               </div>
                                                   </div>
                                                   <div class="tab-pane" id="about">
                                                     <div class="card-body ">
                                                       <table id="example6" class="display" style="width:100%;">
                                                         <thead>
                                                             <tr>
                                                                 <th>Parents</th>
                                                                 <th>Notifications</th>
                                                                 <th>Montant</th>
                                                                 <th>Nombre Eleve</th>
                                                                 <th>Actions</th>

                                                             </tr>
                                                         </thead>
                                                         <tbody>
                                                             <tr>
                                                                 <td>Tiger Nixon</td>
                                                                 <td>System Architect</td>
                                                                 <td>Edinburgh</td>
                                                                 <td>61</td>
                                                                 <td>61</td>

                                                             </tr>

                                                         </tbody>
                                                       </table>
                               </div>
                                                   </div>
                                                   <div class="tab-pane" id="profile">
                                                     <div class="card-body ">
                                                       <table id="example7" class="display" style="width:100%;">
                                                           <thead>
                                                               <tr>
                                                                   <th>Parents</th>
                                                                   <th>Date Fin Souscription</th>
                                                                   <th>Actions</th>
                                                               </tr>
                                                           </thead>
                                                           <tbody>
                                                               <tr>
                                                                   <td>Tiger Nixon</td>
                                                                   <td>System Architect</td>
                                                                   <td>Edinburgh</td>


                                                               </tr>

                                                           </tbody>
                                                       </table>
                               </div>
                                                   </div>
                                                   <div class="tab-pane active" id="contact">
                                                     <div class="card-body ">
                                                       <table id="example3" class="table table-hover  order-column full-width" style="width:100%;">
                                                           <thead>
                                                               <tr>
                                                                   <th>Transaction Id</th>
                                                                   <th>Numéro Opération</th>
                                                                   <th>Opérateur Mobile</th>
                                                                    <th>Montant Opération</th>
                                                                    <th>Actions</th>
                                                               </tr>
                                                           </thead>
                                                           <tbody>
                                                             <?php foreach ($souscriptions as $value): ?>


                                                               <tr>
                                                                   <td><?php echo $value->transacid_paiab; ?></td>
                                                                   <td><?php echo $value->number_paiab; ?></td>
                                                                   <td><?php echo $etabs->getLibelleOperateur($value->operateur_paiab); ?></td>
                                                                   <td><?php echo $value->montant_paiab." ".$value->devises_paiab; ?></td>
                                                                   <td>
                                                                     <?php
                                                                     if($value->statut_paiab==0)
                                                                     {
                                                                       ?>
                                                                       <a href="#" onclick="validerReception(<?php echo $value->id_paiab; ?>,<?php echo $value->parentid_paiab;?>)" class="btn btn-success btn-md" title="Valider reception des fonds"><i class="fa fa-check-square"></i> </a>
                                                                       <a href="#" onclick="rejeterReception(<?php echo $value->id_paiab; ?>,<?php echo $value->parentid_paiab;?>)"  class="btn btn-danger btn-md" title="rejeter reception des fonds"><i class="fa fa-times"></i> </a>

                                                                       <?php
                                                                     }else  if($value->statut_paiab==1)
                                                                      {
                                                                        ?>
                                                                        <a href="#" onclick="archiver(<?php echo $value->id_paiab; ?>,<?php echo $value->parentid_paiab;?>)" class="btn btn-warning btn-md" title="archiver le paiement"><i class="fa fa-refresh"></i> </a>
                                                                        <?php
                                                                      }
                                                                      ?>
                                                                     </td>

                                                               </tr>
                                                               <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                                               <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                                               <script type="text/javascript">

                                                               function archiver(id,parentid)
                                                               {

                                                                   Swal.fire({
                                                                   title: 'Attention !',
                                                                   text: "Voulez vous vraiment archiver le paiement",
                                                                   type: 'warning',
                                                                   showCancelButton: true,
                                                                   confirmButtonColor: '#3085d6',
                                                                   cancelButtonColor: '#d33',
                                                                   confirmButtonText: 'Confirmer',
                                                                   cancelButtonText: 'Annuler',
                                                                   }).then((result) => {
                                                                   if (result.value) {
                                                                   //confirmation de la reception des fonds nous allons changer le statut de paiementab à 1 et ajouter le parent dans historiqueabonnement
                                                                   var etape=3;
                                                                   $.ajax({
                                                                   url: '../ajax/validation.php',
                                                                   type: 'POST',
                                                                   async:false,
                                                                   data: 'paiementid='+id+'&etape='+etape+'&parentid='+parentid,
                                                                   dataType: 'text',
                                                                   success: function (response, statut) {

                                                                     location.reload();

                                                                   }
                                                                   });

                                                                   }else {

                                                                   }
                                                                   })
                                                               }

                                                               function rejeterReception(id,parentid)
                                                               {
                                                                 Swal.fire({
                                                                 title: 'Attention !',
                                                                 text: "Voulez vous vraiment rejeter la reception des fonds",
                                                                 type: 'warning',
                                                                 showCancelButton: true,
                                                                 confirmButtonColor: '#3085d6',
                                                                 cancelButtonColor: '#d33',
                                                                 confirmButtonText: 'Confirmer',
                                                                 cancelButtonText: 'Annuler',
                                                                 }).then((result) => {
                                                                 if (result.value) {
                                                                 //confirmation de la reception des fonds nous allons changer le statut de paiementab à 1 et ajouter le parent dans historiqueabonnement
                                                                 var etape=2;
                                                                 $.ajax({
                                                                 url: '../ajax/validation.php',
                                                                 type: 'POST',
                                                                 async:false,
                                                                 data: 'paiementid='+id+'&etape='+etape+'&parentid='+parentid,
                                                                 dataType: 'text',
                                                                 success: function (response, statut) {

                                                                   location.reload();

                                                                 }
                                                                 });

                                                                 }else {

                                                                 }
                                                                 })
                                                               }

                                                               function validerReception(id,parentid)
                                                               {
                                                                 Swal.fire({
                                                   title: 'Attention !',
                                                   text: "Voulez vous vraiment confirmer la reception des fonds",
                                                   type: 'warning',
                                                   showCancelButton: true,
                                                   confirmButtonColor: '#3085d6',
                                                   cancelButtonColor: '#d33',
                                                   confirmButtonText: 'Confirmer',
                                                   cancelButtonText: 'Annuler',
                                                 }).then((result) => {
                                                   if (result.value) {
                                                     //confirmation de la reception des fonds nous allons changer le statut de paiementab à 1 et ajouter le parent dans historiqueabonnement
                                                     var etape=1;
                                                     $.ajax({
                                                       url: '../ajax/validation.php',
                                                       type: 'POST',
                                                       async:false,
                                                       data: 'paiementid='+id+'&etape='+etape+'&parentid='+parentid,
                                                       dataType: 'text',
                                                       success: function (response, statut) {

                                                          location.reload();

                                                       }
                                                     });

                                                   }else {

                                                   }
                                                 })
                                                               }



                                                               </script>
                                                               <?php endforeach; ?>

                                                           </tbody>
                                                       </table>
                               </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
          </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- calendar -->
     <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
   <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
     <script src="../assets2/js/pages/table/table_data.js" ></script>
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   $("#example5").DataTable();
   $("#example6").DataTable();
   $("#example7").DataTable();

   $(document).ready(function() {



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
