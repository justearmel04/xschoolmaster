<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/Classe.php');
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}


$alletab=$etabs->getAllEtab();
$etablissemementNb=$etabs->getNumberOfEtablissement();

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title">Ajouter Professeur</div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#"><?php echo L::ProfsMenu?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active">Ajouter Professeur</li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addteaok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: 'Félicitations !',
  text: "<?php echo $_SESSION['user']['addteaok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Ajouter les diplomes',
  cancelButtonText: 'Annuler',
}).then((result) => {
  if (result.value) {
document.location.href="adddiplomes.php?compte="+<?php echo $_GET['compte'] ?>;
  }else {
    document.location.href="index.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addteaok']);
                    }

                     ?>

                     <?php
                     if($etablissemementNb==0)
                     {
                       ?>
                       <div class="alert alert-danger alert-dismissible fade show" role="alert">
                         Aucun Etablissement existe dans le système. Merci de proceder à la création d'un établissement
                      <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                        </a>
                      </div>
                       <?php

                     }else if($etablissemementNb>0)
                     {
                       ?>
                       <div class="row">
                           <div class="col-md-12 col-sm-12">
                               <div class="card card-box">
                                   <div class="card-head">
                                       <header>Informations Professeur</header>
                                        <!--button id = "panel-button"
                                      class = "mdl-button mdl-js-button mdl-button--icon pull-right"
                                      data-upgraded = ",MaterialButton">
                                      <i class = "material-icons">more_vert</i>
                                   </button>
                                   <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                      data-mdl-for = "panel-button">
                                      <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                                      <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                                      <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                                   </ul-->
                                   </div>

                                   <div class="card-body" id="bar-parent">
                                       <form  id="FormAddLocalAd" class="form-horizontal" action="../controller/teatcher.php" method="post" enctype="multipart/form-data">
                                           <div class="form-body">
                                             <div class="row">
                             <div class="col-md-12">

                       <fieldset style="background-color:#007bff;">

                       <center><legend style="color:white;">INFORMATIONS PERSONNELLES ET PROFESSIONNELLES</legend></center>
                       </fieldset>


                       </div>
                           </div>
                           <br/>
                           <div class="row">
                                               <div class="col-md-6">
                           <div class="form-group">
                           <input type="file" id="photoTea" name="photoTea" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="../assets2/img/user/user5.jpg" data-allowed-file-extensions="gif png jpg jpeg pjpeg" />
                           <label for=""><b><?php echo L::Image?><span class="required"> * </span>: </b></label>
                           </div>

                           </div>


                                         </div>
                           <div class="row">
                                               <div class="col-md-6">
                           <div class="form-group">
                           <label for=""><b><?php echo L::Name?><span class="required"> * </span>: </b></label>

                         <input type="text" name="nomTea" id="nomTea" data-required="1" placeholder="Entrer le Nom" class="form-control input-height" />
                           </div>

                           </div>
                           <div class="col-md-6">
                                <div class="form-group">
                                  <label for=""><b><?php echo L::PreName?> : </b></label>
                                   <input type="text" name="prenomTea" id="prenomTea" data-required="1" placeholder="Entrer le prénoms" class="form-control input-height" />
                                 </div>


                            </div>

                                         </div>
                                         <div class="row">
                                                             <div class="col-md-6">
                                         <div class="form-group">
                                         <label for=""><b>Date de naissance<span class="required"> * </span>: </b></label>

                                         <input type="text" placeholder="Entrer la date de naissance" name="datenaisTea" id="datenaisTea" data-mask="99/99/9999" class="form-control input-height">
                                           <span class="help-block">JJ-MM-AAAA</span>
                                         </div>

                                         </div>
                                         <div class="col-md-6">
                                              <div class="form-group">
                                                <label for=""><b><?php echo L::BirthLocation?>: </b></label>
                                                 <input type="text" name="lieunaisTea" id="lieunaisTea" data-required="1" placeholder="Lieu de naissance" class="form-control input-height" />
                                               </div>


                                          </div>

                                                       </div>
                                                       <div class="row">
                                                                           <div class="col-md-6">
                                                       <div class="form-group">
                                                       <label for=""><b>Genre <span class="required"> * </span>: </b></label>

                                                       <select class="form-control input-height" name="sexeTea" id="sexeTea">
                                                           <option selected value="">Selectionner le genre</option>
                                                           <option value="M">Masculin</option>
                                                           <option value="F">Feminin</option>
                                                       </select>
                                                       </div>

                                                       </div>
                                                       <div class="col-md-6">
                                                         <div class="form-group">
                                                         <label for=""><b>Email <span class="required"> * </span>: </b></label>

                                                       <input type="text" class="form-control input-height" name="emailTea" id="emailTea" placeholder="Entrer l'adresse email">
                                                         </div>


                                                        </div>

                                                                     </div>
                                                                     <div class="row">
                                                                       <div class="col-md-6">
                                                   <div class="form-group">
                                                   <label for=""><b>Nationalité <span class="required"> * </span>: </b></label>

                                             <input name="natTea" id="natTea" type="text" placeholder="Entrer la nationalité " class="form-control input-height" />
                                                   </div>

                                                   </div>
                                                                       <div class="col-md-6">
                                                                            <div class="form-group">
                                                                              <label for=""><b><?php echo L::PhonestudentTab?> </b></label>
                                                                             <input name="contactTea" id="contactTea" type="text" placeholder="Entrer le contact " class="form-control input-height" />
                                                                             </div>


                                                                        </div>



                                                                                   </div>
                                                                                   <div class="row">
                                                                                                       <div class="col-md-6">
                                                                                   <div class="form-group">
                                                                                   <label for=""><b>Situation Matrimoniale <span class="required"> * </span>: </b></label>

                                                                                   <select class="form-control input-height" name="situationTea" id="situationTea">

                                                                                       <option selected value="">Selectionner la situation matrimoniale</option>
                                                                                       <option value="CELIBATAIRE">CELIBATAIRE</option>
                                                                                       <option value="MARIE(E)">MARIE(E)</option>
                                                                                       <option value="DIVORCE(E)">DIVORCE(E)</option>
                                                                                       <option value="VEUF(VE)">VEUF(VE)</option>
                                                                                   </select>
                                                                                   </div>

                                                                                   </div>
                                                                                   <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                          <label for=""><b><?php echo L::ChildNumber?></b></label>
                                                                                         <input name="nbchildTea" id="nbchildTea" type="number" min="0" placeholder="Entrer le nombre d'enfant " class="form-control input-height" />
                                                                                         </div>


                                                                                    </div>

                                                                                                 </div>
                                                                                                 <div class="row">
                                                                                 <div class="col-md-12">

                                                                       <fieldset style="background-color:#007bff;">

                                                                       <center><legend style="color:white;">INFORMATIONS LEGALES</legend></center>
                                                                       </fieldset>


                                                                       </div>
                                                                     </div><br/>
                                                                               <div class="row">
                                                                                                   <div class="col-md-6">
                                                                               <div class="form-group">
                                                                               <label for=""><b>CNPS : </b></label>

                                                                             <input type="text" name="cnpsTea" id="cnpsTea" data-required="1" placeholder="Entrer le Numéro CNPS" class="form-control input-height" />
                                                                               </div>

                                                                               </div>
                                                                               <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                      <label for=""><b>Matricule : </b></label>
                                                                                       <input type="text" name="matTea" id="matTea" data-required="1" placeholder="Entrer le matricule" class="form-control input-height" />
                                                                                     </div>


                                                                                </div>

                                                                                             </div>
                                                                                             <div class="row">
                                                                                                                 <!--div class="col-md-6">
                                                                                             <div class="form-group">
                                                                                             <label for=""><b>Mutuelle : </b></label>

                                                                                           <input type="text" name="mutuelTea" id="mutuel" data-required="1" placeholder="Entrer la mutuelle" class="form-control input-height" />
                                                                                             </div>

                                                                                           </div-->
                                                                                             <div class="col-md-6">
                                                                                                  <div class="form-group">
                                                                                                    <label for=""><b>Date Embauche : </b></label>
                                                                                                    <input type="text" placeholder="Entrer la date d'embauche'" name="dateEmbTea" id="dateEmbTea" data-mask="99/99/9999" class="form-control input-height">
                                                                                                      <span class="help-block">JJ-MM-AAAA</span>
                                                                                                   </div>


                                                                                              </div>

                                                                                                           </div>
                                                                                                           <div class="row">
                                                                                                                               <!--div class="col-md-6">
                                                                                                           <div class="form-group">
                                                                                                           <label for=""><b>Salire Brut<span class="required"> * </span>: </b></label>

                                                                                                         <input type="number" min="0" name="brutTea" id="brutTea" data-required="1" placeholder="Entrer la salaire Brut" class="form-control input-height" />
                                                                                                           </div>

                                                                                                         </div-->
                                                                                                           <!--div class="col-md-6">
                                                                                                                <div class="form-group">
                                                                                                                  <label for=""><b>Salaire Net : </b></label>
                                                                                                               <input type="number" min="0" name="netTea" id="netTea" data-required="1" placeholder="Entrer la salaire Net" class="form-control input-height" />
                                                                                                                 </div>


                                                                                                            </div-->

                                                                                                                         </div>
                                                                                                                         <div class="row">
                                                                                                         <div class="col-md-12">

                                                                                               <fieldset style="background-color:#007bff;">

                                                                                               <center><legend style="color:white;">INFORMATIONS DE COMPTE</legend></center>
                                                                                               </fieldset>


                                                                                               </div>
                                                                                             </div><br/>
                                                                                             <?php
                                                                                                    if(isset($_GET['codeEtab']))
                                                                                                    {
                                                                                                      ?>
                                                                                                       <input type="hidden" id="libetab" name="libetab" value="<?php echo $_GET['codeEtab']?>"/>
                                                                                                      <?php
                                                                                                    }else {
                                                                                                      ?>
                                                                                                      <div class="form-group row">
                                                                                                              <label class="control-label col-md-3">Code Etablissement
                                                                                                                  <span class="required"> * </span>
                                                                                                              </label>
                                                                                                              <div class="col-md-5">
                                                                                                                <select class="form-control input-height" id="libetab" name="libetab">
                                                                                                                    <option value="">Selectionner un etablissement</option>
                                                                                                                    <?php
                                                                                                                    $i=1;
                                                                                                                      foreach ($alletab as $value):
                                                                                                                      ?>
                                                                                                                      <option value="<?php echo utf8_encode(utf8_decode($value->code_etab)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_etab)); ?></option>

                                                                                                                      <?php
                                                                                                                                                       $i++;
                                                                                                                                                       endforeach;
                                                                                                                                                       ?>

                                                                                                                </select>
                                                                                                            </div>
                                                                                                          </div>
                                                                                                      <?php
                                                                                                    }
                                                                                             ?>


                                                                                                 <div class="form-group row">
                                                                                                     <label class="control-label col-md-3"><?php echo L::Logincnx?>
                                                                                                         <span class="required">*  </span>
                                                                                                     </label>
                                                                                                     <div class="col-md-5">
                                                                                                         <input name="loginTea" id="loginTea" type="text" placeholder="Entrer le Login " class="form-control input-height" /> </div>
                                                                                                 </div>
                                                                                                 <div class="form-group row">
                                                                                                     <label class="control-label col-md-3"><?php echo L::Passcnx?>
                                                                                                         <span class="required">*  </span>
                                                                                                     </label>
                                                                                                     <div class="col-md-5">
                                                                                                         <input name="passTea" id="passTea" type="password" placeholder="Entrer le Mot de passe " class="form-control input-height" /> </div>
                                                                                                 </div>
                                                                                                 <div class="form-group row">
                                                                                                     <label class="control-label col-md-3"><?php echo L::ConfirmPasscnx?>
                                                                                                         <span class="required">*  </span>
                                                                                                     </label>
                                                                                                     <div class="col-md-5">
                                                                                                         <input name="confirmTea" id="confirmTea" type="password" placeholder="Confirmer le Mot de passe " class="form-control input-height" /> </div>
                                                                                                         <input type="hidden" name="etape" id="etape" value="1"/>
                                                                                                         <input type="hidden" name="comptead" id="comptead" value="ENSEIGNANT"/>
                                                                                                 </div>
                                                                                                 <div class="form-actions">
                                                                                                                       <div class="row">
                                                                                                                           <div class="offset-md-3 col-md-9">
                                                                                                                               <button type="submit" class="btn btn-info">Enregistrer</button>
                                                                                                                               <button type="button" class="btn btn-danger">Annuler</button>
                                                                                                                           </div>
                                                                                                                         </div>
                                                                                                                      </div>

                                           </div>
                                       </form>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <?php

                     }
                     ?>

          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <script src="../assets2/js/pages/material_select/getmdl-select.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script>
 jQuery(document).ready(function() {

   $("#FormAddLocalAd").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{
        lieunaisTea:"required",
natTea:"required",
situationTea:"required",
nbchildTea:"required",
/*cnpsTea:"required",
matTea:"required",
mutuelTea:"required",*/
brutTea:"required",
netTea:"required",
        passTea: {
            required: true,
            minlength: 6
        },
        confirmTea:{
            required: true,
            minlength: 6,
            equalTo:'#passTea'
        },
        fonctionad:"required",

        loginTea:"required",
        emailTea: {
                   required: true,
                   email: true
               },
        contactTea:"required",
        datenaisTea:"required",
        prenomTea:"required",
        nomTea:"required",
        gradeTea:"required",
        dateEmbTea:"required",
        sexeTea:"required",
        libetab:"required"



      },
      messages: {
        lieunaisTea:"Merci de renseigner le lieu de naissanec",
        natTea:"Merci de renseigner la Nationalité",
        situationTea:"Metci de renseigner la situation matrimoniale",
        nbchildTea:"Merci de renseigner le nombre d'enfants",
        cnpsTea:"Merci de renseigner le numéro CNPS",
        matTea:"Merci de renseigner le numéro Matricule",
        mutuelTea:"Merci de renseigner la Mutuelle",
        brutTea:"Merci de renseigner le salaire brut",
        netTea:"Merci de reneigner le salaire net",
        confirmTea:{
            required:"<?php echo L::Confirmcheck?>",
            minlength:"<?php echo L::Confirmincheck?>",
            equalTo: "<?php echo L::ConfirmSamecheck?>"
        },
        passTea: {
            required:"<?php echo L::Passcheck?>",
            minlength:"<?php echo L::Confirmincheck?>"
        },
        loginTea:"Merci de renseigner le Login",
        emailTea:"Merci de renseigner une adresse email",
        contactTea:"Merci de renseigner un contact",
        datenaisTea:"Merci de renseigner la date de naissance",
        prenomTea:"Merci de renseigner le prénom",
        nomTea:"Merci de renseigner le nom ",
        fonctionad:"<?php echo L::PleaseEnterFonction?>",
        gradeTea:"Merci de renseigner le grade",
        dateEmbTea:"Merci de renseigner la date d'embauche",
        sexeTea:"Merci de selectionner le sexe",
        libetab:"<?php echo L::PleaseSelectSchoolEnseignement?>"
      },
      submitHandler: function(form) {
        //verifier si ce compte n'existe pas encore dans la base de données
           var etape=1;
           $.ajax({
             url: '../ajax/teatcher.php',
             type: 'POST',
             async:false,
             data: 'login=' + $("#loginTea").val()+ '&email=' + $("#emailTea").val()+'&codeetab=' + $("#libetab").val()+'&etape=' + etape,
             dataType: 'text',
             success: function (content, statut) {


               if(content==0)
               {
                 //le compte n'existe pas dans la base on peut l'ajouter

                 form.submit();
               }else if(content==1) {
                 //le compte existe dejà dans la base de données
                 Swal.fire({
                 type: 'warning',
                 title: 'Attention',
                 text: 'Cet Enseignant existe dejà dans la base de données et assigner à cet Etablissement',

                 })

               }else if(content==2) {

                 Swal.fire({
   title: 'Attention !',
   text: "Cet Enseignant existe dejà dans la base mais n'enseigne pas dans cet Etablissement",
   type: 'warning',
   showCancelButton: true,
   confirmButtonColor: '#3085d6',
   cancelButtonColor: '#d33',
   confirmButtonText: 'Assigner le',
   cancelButtonText: 'Annuler',
 }).then((result) => {
   if (result.value) {
      $("#etape").val(2);

      form.submit();
   }else {

   }
 })


               }
               /*
               if(content==0)
               {
                 //le compte n'existe pas dans la base on peut l'ajouter

                 form.submit();
               }else {
                 //le compte existe dejà dans la base de données
                 Swal.fire({
                 type: 'warning',
                 title: 'Attention',
                 text: 'Cet Enseignant existe dejà dans la base de données',

                 })

               }*/

             }


           });
      }


   });


 });
 </script>
    <!-- end js include path -->
  </body>

</html>
