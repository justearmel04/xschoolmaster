<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Admin.php');
require_once('../class/Teatcher.php');
require_once('../class/Student.php');
require_once('../class/Classe.php');
require_once('../class/Salle.php');
require_once('../class/Matiere.php');
require_once('../class/Parent.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();


$emailUti=$_SESSION['user']['email'];

$user=new User();
$etabs=new Etab();
$parent=new Parentx();
$localadmins= new Localadmin();
$parents=new ParentX();
$admin=new Admin();
$teatcher=new Teatcher();
$student=new Student();
$classe=new Classe();
$salle=new Salle();
$matiere=new Matiere();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

if(isset($_GET['compte']))
{
  $compteEtab=@$_GET['compte'];
}

if(isset($_POST['compte']))
{
    $compteEtab=@$_POST['compte'];
}



$alletab=$etabs->getAllEtab();
//recuperer la liste des professeurs de cet etablissement
$teatcherofschool=$teatcher->getAllTeatcherOfThisSchool($compteEtab);
//recuperer la liste des eleves de cet etablissement
$studentOfSchool=$student->getAllStudentOfThisSchool($compteEtab);
//recuperer la liste des classes de cet etablissement
$classesOfThisSchool=$classe->getAllClassesOfThisSchool($compteEtab);
//recuperer la liste des salles de cet etablissement
$sallesOfThisSchool=$salle->getAllSallesOfThisSchool($compteEtab);
//recuperer la liste des matiere dispenser dans cet etablissement
$matiereOfThisSchool=$matiere->getAllMatiereOfThisSchool($compteEtab);

$nbsessionOn=$session->getNumberSessionEncoursOn($compteEtab);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($compteEtab);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
}

$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();

$datax=$etabs->getEtabInfosbyCode($compteEtab);
 $tabx=explode("*",$datax);


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Gestion Etablissements : <?php echo $tabx[1];?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Gestion etablissements</li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->
					      </div>
						</div>
            <?php

                  if(isset($_SESSION['user']['addsalleok']))
                  {

                    ?>
                    <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                  <?php
                  //echo $_SESSION['user']['addetabok'];
                  ?>
                  <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                     </a>
                  </div-->
          <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
          <script src="../assets/js/sweetalert2.min.js"></script>

              <script>
              Swal.fire({
              type: 'success',
              title: 'Félicitations',
              text: '<?php echo $_SESSION['user']['addsalleok']; ?>',

              })
              </script>
                    <?php
                    unset($_SESSION['user']['addsalleok']);
                  }

                   ?>

            <?php

                  if(isset($_SESSION['user']['addclasseok']))
                  {

                    ?>
                    <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                  <?php
                  //echo $_SESSION['user']['addetabok'];
                  ?>
                  <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                     </a>
                  </div-->
          <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
          <script src="../assets/js/sweetalert2.min.js"></script>

              <script>
              Swal.fire({
              type: 'success',
              title: 'Félicitations',
              text: '<?php echo $_SESSION['user']['addclasseok']; ?>',

              })
              </script>
                    <?php
                    unset($_SESSION['user']['addclasseok']);
                  }

                   ?>
            <?php

                  if(isset($_SESSION['user']['addsubjectok']))
                  {

                    ?>
                    <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                  <?php
                  //echo $_SESSION['user']['addetabok'];
                  ?>
                  <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                     </a>
                  </div-->
          <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
          <script src="../assets/js/sweetalert2.min.js"></script>

              <script>
              Swal.fire({
              type: 'success',
              title: 'Félicitations',
              text: '<?php echo $_SESSION['user']['addsubjectok']; ?>',

              })
              </script>
                    <?php
                    unset($_SESSION['user']['addsubjectok']);
                  }

                   ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


          <div class="row">

            <div class="col-sm-12">
								<div class="card-box">
									<div class="card-head">
										<header></header>
									</div>
									<div class="card-body ">
						            <div class = "mdl-tabs mdl-js-tabs">
						               <div class = "mdl-tabs__tab-bar tab-left-side">
						                  <a href = "#tab4-panel" class = "mdl-tabs__tab is-active">Professeurs</a>
						                  <a href = "#tab5-panel" class = "mdl-tabs__tab">Elèves</a>
						                  <a href = "#tab6-panel" class = "mdl-tabs__tab">Classes</a>
                              <a href = "#tab7-panel" class = "mdl-tabs__tab">Salles</a>
                              <a href = "#tab8-panel" class = "mdl-tabs__tab">Matières</a>
						               </div>
						               <div class = "mdl-tabs__panel is-active p-t-20" id = "tab4-panel">
                             <div class="row">
                               <div class="col-md-12 col-sm-12">
                                                         <div class="card card-box">
                                                             <div class="card-head">
                                                                 <header>Rechercher</header>

                                                             </div>
                                                             <div class="card-body " id="bar-parent">
                                                               <form method="post" id="FormSearch" action="schoolInfos.php">
                                                                   <div class="row">
                                                                     <div class="col-md-4 col-sm-4">
                                                                     <!-- text input -->
                                                                     <div class="form-group">
                                                                         <label>Matricule Enseignant</label>
                                                                         <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                                                                         <select class="form-control " id="matTeatcher" name="matTeatcher" onchange="searchTeatcher()"  style="width:100%">
                                                                             <option value="">Selectionner un matricule</option>
                                                                             <?php
                                                                             $i=1;
                                                                               foreach ($teatcherofschool as $valueTeatcher):
                                                                               ?>
                                                                               <option value="<?php echo $valueTeatcher->matricule_enseignant;?>"><?php echo $valueTeatcher->matricule_enseignant;?></option>

                                                                               <?php
                                                                                                                $i++;
                                                                                                                endforeach;
                                                                                                                ?>

                                                                         </select>
                                                                     </div>


                                                                 </div>
                                                                 <div class="col-md-4 col-sm-4">
                                                                 <!-- text input -->
                                                                 <div class="form-group">
                                                                     <label>Nom & Prénoms Professeur</label>
                                                                     <select class="form-control " id="idprof" name="idprof" style="width:100%">
                                                                         <option value="">Selectionner un Professeur</option>
                                                                         <?php
                                                                         $i=1;
                                                                           foreach ($teatcherofschool as $namevalue):
                                                                           ?>
                                                                           <option value="<?php echo utf8_encode(utf8_decode($namevalue->id_enseignant)); ?>"><?php echo utf8_encode(utf8_decode($namevalue->nom_enseignant." ".$namevalue->prenom_enseignant)); ?></option>

                                                                           <?php
                                                                                                            $i++;
                                                                                                            endforeach;
                                                                                                            ?>

                                                                     </select>
                                                                     <input type="hidden" name="search" id="search"/>
                                                                     <input type="hidden" name="compte" id="compte" value="<?php echo $compteEtab;?>"/>
                                                                 </div>


                                                             </div>
                                                             <div class="col-md-4 col-sm-4">
                                                               <div class="form-group">
                                                               <button type="submit" class="btn btn-primary" style="margin-top:25px;">Rechercher</button>
                                                              </div>
                                                             </div>
                                                                   </div>


                                                               </form>
                                                             </div>
                                                         </div>
                                                     </div>
                             </div>
                             <div class="row">
                                                     <div class="col-md-12">
                                                         <div class="tabbable-line">
                                                            <ul class="nav nav-pills nav-pills-rose">
                             									<li class="nav-item tab-all"><a class="nav-link active show"
                             										href="#tab1" data-toggle="tab">Liste</a></li>
                             									<li class="nav-item tab-all"><a class="nav-link" href="#tab2"
                             										data-toggle="tab">Grille</a></li>
                             								</ul>
                                                             <div class="tab-content">
                                                                 <div class="tab-pane active fontawesome-demo" id="tab1">
                                                                     <div class="row">
                             					                        <div class="col-md-12">
                             					                            <div class="card  card-box">
                             					                                <div class="card-head">
                             					                                    <header></header>
                             					                                    <div class="tools">
                             					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                             						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                             						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                             					                                    </div>
                             					                                </div>
                             					                                <div class="card-body ">
                                                                        <div class="pull-right">
                                                                           <a class="btn btn-primary " style="border-radius:5px;" href="addteatcher.php?codeEtab=<?php echo $compteEtab?>"><i class="fa fa-plus"></i> Nouveau Professeur</a>
                                                                            <a class="btn btn-warning " style="border-radius:5px;" href="#"><i class="fa fa-print"></i> Liste Professeur</a>

                                                                        </div>
                             					                                  <div class="table-scrollable">
                             					                                    <table class="table table-hover table-checkable order-column full-width" id="example4">
                             					                                        <thead>
                             					                                            <tr>

                             					                                                <th> Nom & Prénoms </th>
                             					                                                <th> Nationalité </th>
                             					                                                <th> Téléphone </th>
                             					                                                <th> Email </th>
                                                                                      <th> <?php echo L::Actions?> </th>
                             					                                            </tr>
                             					                                        </thead>
                             					                                        <tbody>
                                                                                 <?php
                                                                                 $i=1;
                                                                                 if(isset($_POST['search']))
                                                                                 {
                                                                                   $content="";
                                                                                     if($_POST['matTeatcher']!=""&&$_POST['idprof']!="")
                                                                                     {

                                                                                       $teatcherofschool=$teatcher->getTheSpecificTeatcherForSchool($compteEtab,$_POST['matTeatcher'],$_POST['idprof']);

                                                                                       $content="bonjour";

                                                                                     }
                                                                                   }

                             //var_dump($teatchers);
                                                                                   foreach ($teatcherofschool as $value):
                                                                                   ?>
                             																<tr class="odd gradeX" ondblclick="myFunction(<?php echo $value->id_compte?>)">


                             																	<td style="width:30%"><?php echo $value->nom_compte." ".$value->prenom_compte;?></td>

                             																	<td style="width:20%"><?php echo $value->nat_enseignant;?></td>
                             																	<td style="width:20%"><?php echo $value->tel_compte;?></td>
                             																	<td style="width:10%"><?php echo $value->email_compte;?></td>


                                                               <td class="center">

                             																		<a href="#"  onclick="modify(<?php echo $value->id_compte?>)" class="btn btn-tbl-edit btn-xs">
                             																			<i class="fa fa-pencil"></i>
                             																		</a>
                                                                <a class="btn btn-tbl-delete btn-xs" onclick="deleted(<?php echo $value->id_compte?>)">
                                                                  <i class="fa fa-trash-o "></i>
                                                                </a>

                             																	</td>
                             																</tr>
                                                             <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                                             <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                                             <script>

                                                             function myFunction(idcompte)
                                                             {
                                                               //var url="detailslocal.php?compte="+idcompte;
                                                             document.location.href="detailsteatcher.php?compte="+idcompte;
                                                             }

                                                             function modify(id)
                                                             {


                                                               Swal.fire({
                                                 title: 'Attention !',
                                                 text: "Voulez vous vraiment modifier cet Enseignant",
                                                 type: 'warning',
                                                 showCancelButton: true,
                                                 confirmButtonColor: '#3085d6',
                                                 cancelButtonColor: '#d33',
                                                 confirmButtonText: 'Modifier',
                                                 cancelButtonText: 'Annuler',
                                               }).then((result) => {
                                                 if (result.value) {
                                                   document.location.href="updateteatcher.php?compte="+id;
                                                 }else {

                                                 }
                                               })
                                                             }

                                                             function deleted(id)
                                                             {

                                                               Swal.fire({
                                                 title: 'Attention !',
                                                 text: "Voulez vous vraiment supprimer cet Enseignant",
                                                 type: 'warning',
                                                 showCancelButton: true,
                                                 confirmButtonColor: '#3085d6',
                                                 cancelButtonColor: '#d33',
                                                 confirmButtonText: 'Supprimer',
                                                 cancelButtonText: 'Annuler',
                                               }).then((result) => {
                                                 if (result.value) {
                                                   document.location.href="../controller/teatcher.php?etape=3&compte="+id;
                                                 }else {

                                                 }
                                               })
                                                             }

                                                             </script>


                                                             <?php
                                                                                              $i++;
                                                                                              endforeach;
                                                                                              ?>

                             															</tbody>
                             					                                    </table>
                             					                                    </div>
                             					                                </div>
                             					                            </div>
                             					                        </div>
                             					                    </div>
                                                                 </div>
                                                                 <div class="tab-pane" id="tab2">



                                                 					<div class="row">
                                                             <?php
                                                             $i=1;
                                                               foreach ($teatcherofschool as $value):
                                                               ?>
                             					                        <div class="col-md-4" ondblclick="myFunction(<?php echo $value->id_compte?>)">
                             				                                <div class="card card-box">
                             				                                    <div class="card-body no-padding ">
                             				                                    	<div class="doctor-profile">
                                                                             <?php
                                                                               $lienImg="";
                                                                               if(strlen($value->photo_compte)>0)
                                                                               {
                                                                                 $lienImg="../photo/".$value->email_compte."/".$value->photo_compte;
                                                                               }else {
                                                                                 $lienImg="../photo/user5.jpg";
                                                                               }
                                                                              ?>
                                                                                    <img src="<?php echo $lienImg?>" class="doctor-pic" alt="" style="height:100px; width:90px;">
                             					                                        <div class="profile-usertitle">
                             					                                            <div class="doctor-name"><?php echo $value->nom_compte." ".$value->prenom_compte;?> </div>
                             					                                            <div class="name-center"> <?php echo $value->nat_enseignant;?></div>

                             					                                        </div>
                                                                               <p><?php echo $value->email_compte;?></p>
                                                                               <div><p><i class="fa fa-phone"></i><a href="">  <?php echo $value->tel_compte; ?></a></p> </div>
                                                                         <div class="profile-userbuttons">
                                                                              <a href="#" class="btn btn-circle btn-info btn-sm" onclick="modify(<?php echo $value->id_compte ?>)"><i class="fa fa-pencil"></i></a>
                                                                              <a href="#" class="btn btn-circle btn-danger btn-sm" onclick="deleted(<?php echo $value->id_compte?>)"><i class="fa fa-trash"></i></a>
                                                                         </div>
                             				                                        </div>
                             				                                    </div>
                             				                                </div>
                             					                        </div>
                                                               <?php
                                                                                                $i++;
                                                                                                endforeach;
                                                                                                ?>


                                                 					</div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
						               </div>
						               <div class = "mdl-tabs__panel p-t-20" id = "tab5-panel">
                             <div class="row">
                               <div class="col-md-12 col-sm-12">
                                                         <div class="card card-box">
                                                             <div class="card-head">
                                                                 <header>Rechercher</header>

                                                             </div>
                                                             <div class="card-body " id="bar-parent">
                                                               <form method="post" id="FormSearchStudent" action="schoolInfos.php">
                                                                   <div class="row">
                                                                     <div class="col-md-4 col-sm-4">
                                                                     <!-- text input -->
                                                                     <div class="form-group">
                                                                         <label>Classe</label>
                                                                         <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                                                                         <select class="form-control " id="classeId" name="classeId" onchange="searchStudent()"  style="width:100%">
                                                                             <option value="">Selectionner une classe</option>
                                                                             <?php

                                                                             $i=1;
                                                                               foreach ($classesOfThisSchool as $valueClasses):
                                                                               ?>
                                                                               <option value="<?php echo $valueClasses->id_classe;?>"><?php echo $valueClasses->libelle_classe;?></option>

                                                                               <?php
                                                                                                                $i++;
                                                                                                                endforeach;
                                                                                                                ?>

                                                                         </select>
                                                                     </div>


                                                                 </div>
                                                                 <div class="col-md-4 col-sm-4">
                                                                 <!-- text input -->
                                                                 <div class="form-group">
                                                                     <label>Nom & Prénoms Elève</label>
                                                                     <select class="form-control " id="idstudent" name="idstudent" style="width:100%">
                                                                         <option value="">Selectionner un Elève</option>
                                                                         <?php
                                                                         $i=1;
                                                                           foreach ($studentOfSchool as $namevalue):
                                                                           ?>
                                                                           <option value="<?php echo utf8_encode(utf8_decode($namevalue->id_compte)); ?>"><?php echo utf8_encode(utf8_decode($namevalue->nom_eleve." ".$namevalue->prenom_eleve)); ?></option>

                                                                           <?php
                                                                                                            $i++;
                                                                                                            endforeach;
                                                                                                            ?>

                                                                     </select>
                                                                     <input type="hidden" name="search1" id="search1"/>
                                                                     <input type="hidden" name="compte" id="compte" value="<?php echo $compteEtab;?>"/>
                                                                 </div>


                                                             </div>
                                                             <div class="col-md-4 col-sm-4">
                                                               <div class="form-group">
                                                               <button type="submit" class="btn btn-primary" style="margin-top:25px;">Rechercher</button>
                                                              </div>
                                                             </div>
                                                                   </div>


                                                               </form>
                                                             </div>
                                                         </div>
                                                     </div>
                             </div>
                             <div class="row">
                                                     <div class="col-md-12">
                                                         <div class="tabbable-line">
                                                            <ul class="nav nav-pills nav-pills-rose">
                             									<li class="nav-item tab-all"><a class="nav-link active show"
                             										href="#tab1" data-toggle="tab">Liste</a></li>

                             								</ul>
                                                             <div class="tab-content">
                                                                 <div class="tab-pane active fontawesome-demo" id="tab1">
                                                                     <div class="row">
                             					                        <div class="col-md-12">
                             					                            <div class="card  card-box">
                             					                                <div class="card-head">
                             					                                    <header><!--a class="btn btn-primary" href="admission.php?codeEtab=<?php //echo $compteEtab?>"><i class="fa fa-plus"></i> Nouveau Eleve</a--></header>

                             					                                    <div class="tools">
                             					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                             						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                             						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                             					                                    </div>

                             					                                </div>

                             					                                <div class="card-body ">
                             					                                  <div class="table-scrollable">
                                                                          <div class="pull-right">
                                                                              <a class="btn btn-warning " style="border-radius:5px;" href="#"><i class="fa fa-print"></i> Liste Eleves</a>

                                                                          </div>

                             					                                    <table class="table table-hover table-checkable order-column full-width" id="example4">
                             					                                        <thead>
                             					                                            <tr>
                                                                                      <th> Matricule </th>
                             					                                                <th> Nom & Prénoms </th>
                             					                                                <th> Classe </th>
                             					                                                <th> Parent </th>




                             					                                                <th> <?php echo L::Actions?> </th>
                             					                                            </tr>
                             					                                        </thead>
                             					                                        <tbody>
                                                                                 <?php
                                                                                 $i=1;
                                                                                 if(isset($_POST['search1']))
                                                                                 {
                                                                                   $content="";
                                                                                     if($_POST['classeId']!=""&&$_POST['idstudent']!="")
                                                                                     {

                                                                                       $studentOfSchool=$student->getTheSpecificStudentForSchool($compteEtab,$_POST['classeId'],$_POST['idstudent']);

                                                                                       $content="bonjour";

                                                                                     }
                                                                                   }

                             //var_dump($teatchers);
                                                                                   foreach ($studentOfSchool as $value):
                                                                                   ?>
                             																<tr class="odd gradeX" ondblclick="myFunctionStudent(<?php echo $value->id_compte?>)">


                             																	<td style="width:15%"><?php echo $value->matricule_eleve;?></td>
                                                              <td style="width:30%"><?php echo $value->nom_compte." ".$value->prenom_compte;?></td>
                             																	<td style="width:20%"><?php echo $value->libelle_classe;?></td>
                             																	<td style="width:20%"><?php echo $parent->getNamePreOfTheCompte($value->idcompte_parent);?></td>



                                                               <td class="center">

                             																		<a href="#"  onclick="modifyStudent(<?php echo $value->id_compte?>)" class="btn btn-tbl-edit btn-xs">
                             																			<i class="fa fa-pencil"></i>
                             																		</a>
                                                                <a class="btn btn-tbl-delete btn-xs" onclick="deletedStudent(<?php echo $value->id_compte?>)">
                                                                  <i class="fa fa-trash-o "></i>
                                                                </a>

                             																	</td>
                             																</tr>
                                                             <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                                             <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                                             <script>

                                                             function myFunctionStudent(idcompte)
                                                             {
                                                               var codeEtab="<?php echo $compteEtab;?>";
                                                               //var url="detailslocal.php?compte="+idcompte;
                                                             document.location.href="detailsstudent.php?compte="+idcompte+"&codeEtab="+codeEtab;
                                                             }

                                                             function modifyStudent(id)
                                                             {
                                                               var codeEtab="<?php echo $compteEtab;?>";

                                                               Swal.fire({
                                                 title: 'Attention !',
                                                 text: "Voulez vous vraiment modifier cet Elève",
                                                 type: 'warning',
                                                 showCancelButton: true,
                                                 confirmButtonColor: '#3085d6',
                                                 cancelButtonColor: '#d33',
                                                 confirmButtonText: 'Modifier',
                                                 cancelButtonText: 'Annuler',
                                               }).then((result) => {
                                                 if (result.value) {
                                                   document.location.href="updatestudent.php?compte="+id+"&codeEtab="+codeEtab;
                                                 }else {

                                                 }
                                               })
                                                             }

                                                             function deletedStudent(id)
                                                             {
                                                               var codeEtab="<?php echo $compteEtab;?>";
                                                               Swal.fire({
                                                 title: 'Attention !',
                                                 text: "Voulez vous vraiment supprimer cet Elève",
                                                 type: 'warning',
                                                 showCancelButton: true,
                                                 confirmButtonColor: '#3085d6',
                                                 cancelButtonColor: '#d33',
                                                 confirmButtonText: 'Supprimer',
                                                 cancelButtonText: 'Annuler',
                                               }).then((result) => {
                                                 if (result.value) {
                                                   document.location.href="../controller/student.php?etape=3&compte="+id+"&codeEtab="+codeEtab;
                                                 }else {

                                                 }
                                               })
                                                             }

                                                             </script>


                                                             <?php
                                                                                              $i++;
                                                                                              endforeach;
                                                                                              ?>

                             															</tbody>
                             					                                    </table>
                             					                                    </div>
                             					                                </div>
                             					                            </div>
                             					                        </div>
                             					                    </div>
                                                                 </div>

                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
						               </div>
						               <div class = "mdl-tabs__panel p-t-20" id = "tab6-panel">
                             <div class="row">
                                                     <div class="col-md-12">
                                                         <div class="tabbable-line">
                                                            <!--ul class="nav nav-pills nav-pills-rose">
                             									<li class="nav-item tab-all"><a class="nav-link active show"
                             										href="#tab1" data-toggle="tab">Liste</a></li>
                             									<li class="nav-item tab-all"><a class="nav-link" href="#tab2"
                             										data-toggle="tab">Grille</a></li>
                             								</ul-->
                                                             <div class="tab-content">
                                                                 <div class="tab-pane active fontawesome-demo" id="tab1">
                                                                     <div class="row">
                             					                        <div class="col-md-12">
                             					                            <div class="card  card-box">
                             					                                <div class="card-head">
                             					                                    <header></header>
                             					                                    <div class="tools">
                             					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                             						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                             						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                             					                                    </div>
                             					                                </div>
                             					                                <div class="card-body ">
                                                                        <div class="pull-right">
                                                                          <a class="btn btn-primary" style="border-radius:5px;" href="addclasses.php?codeEtab=<?php echo $compteEtab?>"><i class="fa fa-plus"></i> Nouvelle classe</a>
                                                                            <a class="btn btn-warning " style="border-radius:5px;" href="#"><i class="fa fa-print"></i> Liste Classe</a>

                                                                        </div>
                             					                                  <div class="table-scrollable">
                             					                                    <table class="table table-hover table-checkable order-column full-width" id="example4">
                             					                                        <thead>
                             					                                            <tr>

                             					                                                <th> Etablissement </th>
                             					                                                <th> Classe </th>
                             					                                                <th>Date création</th>
                             					                                                 <th> <?php echo L::Actions?> </th>
                             					                                            </tr>
                             					                                        </thead>
                             					                                        <tbody>
                                                                                 <?php
                                                                                 $i=1;
                                                                                 foreach ($classesOfThisSchool as $value):
                                                                                   ?>
                             																<tr class="odd gradeX" ondblclick="myFunctionClasse(<?php echo $value->id_classe?>)">



                             																	<td style="width:30%"><?php
                                                               //echo $value->codeEtab_classe;
                                                               $datax=$etabs->getEtabInfosbyCode($value->codeEtab_classe);
                                                               $tabx=explode("*",$datax);
                                                               echo $tabx[1];

                                                               ?></td>
                             																	<td style="width:30%"><?php echo $value->libelle_classe;?></td>

                             																	<td class="left"><?php echo date_format(date_create($value->datecrea_classe),"d/m/Y");?></td>
                                                               <td class="center">

                             																		<a href="#"  onclick="modifyClasse(<?php echo $value->id_classe?>)" class="btn btn-tbl-edit btn-xs">
                             																			<i class="fa fa-pencil"></i>
                             																		</a>
                                                                <a class="btn btn-tbl-delete btn-xs" onclick="deletedClasse(<?php echo $value->id_classe?>)">
                                                                  <i class="fa fa-trash-o "></i>
                                                                </a>

                             																	</td>
                             																</tr>
                                                             <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                                             <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                                             <script>

                                                             function myFunctionClasse(idcompte)
                                                             {
                                                               //var url="detailslocal.php?compte="+idcompte;
                                                               var codeEtab="<?php echo $compteEtab;?>";
                                                             document.location.href="detailsclasses.php?compte="+idcompte+"&codeEtab="+codeEtab;
                                                             }

                                                             function modifyClasse(id)
                                                             {
                                                               var codeEtab="<?php echo $compteEtab;?>";

                                                               Swal.fire({
                                                 title: 'Attention !',
                                                 text: "Voulez vous vraiment modifier cette classe",
                                                 type: 'warning',
                                                 showCancelButton: true,
                                                 confirmButtonColor: '#3085d6',
                                                 cancelButtonColor: '#d33',
                                                 confirmButtonText: 'Modifier',
                                                 cancelButtonText: 'Annuler',
                                               }).then((result) => {
                                                 if (result.value) {
                                                   document.location.href="updateclasses.php?compte="+id+"&codeEtab="+codeEtab;
                                                 }else {

                                                 }
                                               })
                                                             }

                                                             function deletedClasse(id)
                                                             {
                                                               var codeEtab="<?php echo $compteEtab;?>";
                                                               Swal.fire({
                                                 title: 'Attention !',
                                                 text: "Voulez vous vraiment supprimer cette classe",
                                                 type: 'warning',
                                                 showCancelButton: true,
                                                 confirmButtonColor: '#3085d6',
                                                 cancelButtonColor: '#d33',
                                                 confirmButtonText: 'Supprimer',
                                                 cancelButtonText: 'Annuler',
                                               }).then((result) => {
                                                 if (result.value) {
                                                   document.location.href="../controller/classes.php?etape=3&compte="+id+"&codeEtab="+codeEtab;
                                                 }else {

                                                 }
                                               })
                                                             }

                                                             </script>


                                                             <?php
                                                                                              $i++;
                                                                                              endforeach;
                                                                                              ?>

                             															</tbody>
                             					                                    </table>
                             					                                    </div>
                             					                                </div>
                             					                            </div>
                             					                        </div>
                             					                    </div>
                                                                 </div>

                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
						               </div>
                           <div class = "mdl-tabs__panel p-t-20" id = "tab7-panel">
                             <div class="row">
                                                     <div class="col-md-12">
                                                         <div class="tabbable-line">

                                                             <div class="tab-content">
                                                                 <div class="tab-pane active fontawesome-demo" id="tab1">
                                                                     <div class="row">
                             					                        <div class="col-md-12">
                             					                            <div class="card  card-box">
                             					                                <div class="card-head">
                             					                                    <header></header>
                             					                                    <div class="tools">
                             					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                             						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                             						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                             					                                    </div>
                             					                                </div>
                             					                                <div class="card-body ">
                                                                        <div class="pull-right">
                                                                          <a class="btn btn-primary" style="border-radius:5px;" href="addsalles.php?codeEtab=<?php echo $compteEtab?>"><i class="fa fa-plus"></i> Nouvelle salle</a>
                                                                            <a class="btn btn-warning " style="border-radius:5px;" href="#"><i class="fa fa-print"></i> Liste salle</a>

                                                                        </div>
                             					                                  <div class="table-scrollable">
                             					                                    <table class="table table-hover table-checkable order-column full-width" id="example4">
                             					                                        <thead>
                             					                                            <tr>

                             					                                                <!--th> Etablissement </th-->
                             					                                                <th> Salle </th>
                             					                                                <th>Date création</th>
                             					                                                 <th> <?php echo L::Actions?> </th>
                             					                                            </tr>
                             					                                        </thead>
                             					                                        <tbody>
                                                                                 <?php
                                                                                 $i=1;


                             //var_dump($teatchers);
                                                                                   foreach ($sallesOfThisSchool as $value):
                                                                                   ?>
                             																<tr class="odd gradeX" ondblclick="myFunctionSalle(<?php echo $value->id_salle?>)">



                             																	<!--td><?php
                                                              /* $datax=$etabs->getEtabInfosbyCode($value->codeEtab_salle);
                                                               $tabx=explode("*",$datax);
                                                               echo $tabx[1];*/
                                                               ?></td-->
                             																	<td><?php echo $value->libelle_salle;?></td>

                             																	<td class="left"><?php echo date_format(date_create($value->datecrea_salle),"d/m/Y");?></td>
                                                               <td class="center">

                             																		<a href="#"  onclick="modifySalle(<?php echo $value->id_salle?>)" class="btn btn-tbl-edit btn-xs">
                             																			<i class="fa fa-pencil"></i>
                             																		</a>
                                                                <a class="btn btn-tbl-delete btn-xs" onclick="deletedSalle(<?php echo $value->id_salle?>)">
                             																			<i class="fa fa-trash-o "></i>
                             																		</a>

                             																	</td>
                             																</tr>
                                                             <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                                             <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                                             <script>

                                                             function myFunctionSalle(idcompte)
                                                             {
                                                               //var url="detailslocal.php?compte="+idcompte;
                                                               var codeEtab="<?php echo $compteEtab;?>";
                                                             document.location.href="detailssalles.php?compte="+idcompte+"&codeEtab="+codeEtab;
                                                             }

                                                             function modifySalle(id)
                                                             {
                                                               var codeEtab="<?php echo $compteEtab;?>";

                                                               Swal.fire({
                                                 title: 'Attention !',
                                                 text: "Voulez vous vraiment modifier cette salle",
                                                 type: 'warning',
                                                 showCancelButton: true,
                                                 confirmButtonColor: '#3085d6',
                                                 cancelButtonColor: '#d33',
                                                 confirmButtonText: 'Modifier',
                                                 cancelButtonText: 'Annuler',
                                               }).then((result) => {
                                                 if (result.value) {
                                                   document.location.href="updatesalles.php?compte="+id+"&codeEtab="+codeEtab;
                                                 }else {

                                                 }
                                               })
                                                             }

                                                             function deletedSalle(id)
                                                             {
                                                               var codeEtab="<?php echo $compteEtab;?>";
                                                               Swal.fire({
                                                 title: 'Attention !',
                                                 text: "Voulez vous vraiment supprimer cette salle",
                                                 type: 'warning',
                                                 showCancelButton: true,
                                                 confirmButtonColor: '#3085d6',
                                                 cancelButtonColor: '#d33',
                                                 confirmButtonText: 'Supprimer',
                                                 cancelButtonText: 'Annuler',
                                               }).then((result) => {
                                                 if (result.value) {
                                                   document.location.href="../controller/salles.php?etape=3&compte="+id+"&codeEtab="+codeEtab;
                                                 }else {

                                                 }
                                               })
                                                             }

                                                             </script>


                                                             <?php
                                                                                              $i++;
                                                                                              endforeach;
                                                                                              ?>

                             															</tbody>
                             					                                    </table>
                             					                                    </div>
                             					                                </div>
                             					                            </div>
                             					                        </div>
                             					                    </div>
                                                                 </div>

                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                          </div>
                          <div class = "mdl-tabs__panel p-t-20" id = "tab8-panel">
                            <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="tabbable-line">
                                                           <!--ul class="nav nav-pills nav-pills-rose">
                            									<li class="nav-item tab-all"><a class="nav-link active show"
                            										href="#tab1" data-toggle="tab">Liste</a></li>
                            									<li class="nav-item tab-all"><a class="nav-link" href="#tab2"
                            										data-toggle="tab">Grille</a></li>
                            								</ul-->
                                                            <div class="tab-content">
                                                                <div class="tab-pane active fontawesome-demo" id="tab1">
                                                                    <div class="row">
                            					                        <div class="col-md-12">
                            					                            <div class="card  card-box">
                            					                                <div class="card-head">
                            					                                    <header></header>
                            					                                    <div class="tools">
                            					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                            					                                    </div>
                            					                                </div>
                            					                                <div class="card-body ">
                                                                        <div class="pull-right">
                                                                          <a class="btn btn-primary" style="border-radius:5px;" href="addmatiere.php?codeEtab=<?php echo $compteEtab?>"><i class="fa fa-plus"></i> Nouvelle Matière</a>
                                                                            <a class="btn btn-warning " style="border-radius:5px;" href="#"><i class="fa fa-print"></i> Liste Matière</a>

                                                                        </div>
                            					                                  <div class="table-scrollable">
                            					                                    <table class="table table-hover table-checkable order-column full-width" id="example4">
                            					                                        <thead>
                            					                                            <tr>

                            					                                                <th> Classe </th>
                            					                                                <th> Professeur </th>
                            					                                                <th> Matière</th>
                                                                                       <th>Coefficient</th>
                            					                                                 <th> <?php echo L::Actions?> </th>
                            					                                            </tr>
                            					                                        </thead>
                            					                                        <tbody>
                                                                                <?php
                                                                                $i=1;
                                                                                if(isset($_POST['search']))
                                                                                {
                                                                                  $content="";
                                                                                    if($_POST['codeetab']!="")
                                                                                    {

                                                                                      if(strlen($_POST['classex'])>0)
                                                                                      {
                                                                                        //recherche en fonction de l'id de l'enseignant et code etab
                                                                                        $classes=$classe->getClassesByschoolCodewithId($_POST['codeetab'],$_POST['classex']);
                                                                                      }else {
                                                                                        //recherche tous les enseignants en fonction du code etablissement
                                                                                        //$teatchers=$teatcher->getAllTeatchers();
                                                                                        $classes=$classe->getAllClassesByschoolCode($_POST['codeetab']);

                                                                                      }

                                                                                      $content="bonjour";

                                                                                    }else

                                                                                     {
                                                                                      $content="bonsoir";
                                                                                        $classes=$classe->getAllClassesByClasseId($_POST['classex']);
                                                                                    }
                                                                                  }

                            //var_dump($teatchers);
                                                                                  foreach ($matiereOfThisSchool as $value):
                                                                                  ?>
                            																<tr class="odd gradeX" ondblclick="myFunctionMat(<?php echo $value->id_mat?>)">



                            																	<td><?php echo $value->libelle_classe ?></td>
                            																	<td><?php echo $teatcher->getNameofTeatcherById($value->id_enseignant) ?></td>
                                                              <td><?php echo $value->libelle_mat ?></td>
                            																	<td class="left"><?php echo $value->coef_mat;?></td>
                                                              <td class="center">

                            																		<a href="#"  onclick="modifyMat(<?php echo $value->id_mat;?>)" class="btn btn-tbl-edit btn-xs">
                            																			<i class="fa fa-pencil"></i>
                            																		</a>
                                                                <a class="btn btn-tbl-delete btn-xs" onclick="deletedMat(<?php echo $value->id_mat;?>)">
                                                                  <i class="fa fa-trash-o "></i>
                                                                </a>

                            																	</td>
                            																</tr>
                                                            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                                            <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                                            <script>

                                                            function myFunctionMat(idcompte)
                                                            {
                                                              //var url="detailslocal.php?compte="+idcompte;
                                                              var codeEtab="<?php echo $compteEtab;?>";
                                                            document.location.href="detailsmatiere.php?compte="+idcompte+"&codeEtab="+codeEtab;
                                                            }

                                                            function modifyMat(id)
                                                            {

                                                              var codeEtab="<?php echo $compteEtab;?>";
                                                              Swal.fire({
                                                title: 'Attention !',
                                                text: "Voulez vous vraiment modifier cette classe",
                                                type: 'warning',
                                                showCancelButton: true,
                                                confirmButtonColor: '#3085d6',
                                                cancelButtonColor: '#d33',
                                                confirmButtonText: 'Modifier',
                                                cancelButtonText: 'Annuler',
                                              }).then((result) => {
                                                if (result.value) {
                                                  document.location.href="updatematiere.php?compte="+id+"&codeEtab="+codeEtab;
                                                }else {

                                                }
                                              })
                                                            }

                                                            function deletedMat(id)
                                                            {
                                                              var codeEtab="<?php echo $compteEtab;?>";
                                                              var classe="<?php echo $value->classe_mat;?>";

                                                              Swal.fire({
                                                title: 'Attention !',
                                                text: "Voulez vous vraiment supprimer cette classe",
                                                type: 'warning',
                                                showCancelButton: true,
                                                confirmButtonColor: '#3085d6',
                                                cancelButtonColor: '#d33',
                                                confirmButtonText: 'Supprimer',
                                                cancelButtonText: 'Annuler',
                                              }).then((result) => {
                                                if (result.value) {
                                                  document.location.href="../controller/matiere.php?etape=3&matiere="+id+"&codeEtab="+codeEtab+"&classe="+classe;
                                                }else {

                                                }
                                              })
                                                            }

                                                            </script>


                                                            <?php
                                                                                             $i++;
                                                                                             endforeach;
                                                                                             ?>

                            															</tbody>
                            					                                    </table>
                            					                                    </div>
                            					                                </div>
                            					                            </div>
                            					                        </div>
                            					                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                          </div>
						            </div>
									</div>
								</div>
							</div>

         </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
     <!-- calendar -->
     <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
   <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
     <script src="../assets2/js/pages/table/table_data.js" ></script>
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   $("#matTeatcher").select2();
   $("#idprof").select2();
   $("#classeId").select2();
   $("#idstudent").select2();

   function searchTeatcher()
   {
      //recuperation du matricule de l'enseignants

      var mat=$("#matTeatcher").val();
      //recherche des infos du proffesseur

      var etape=3;

      $.ajax({
        url: '../ajax/teatcher.php',
        type: 'POST',
        async:false,
        data: 'matricule='+mat+'&etape='+etape,
        dataType: 'text',
        success: function (content, statut)
        {
          $("#idprof").html("");
          $("#idprof").html(content);
        }
      });

   }

   function searchStudent()
   {
     //recupération des variables
     var classeId=$("#classeId").val();
     var etape=3;

     $.ajax({
       url: '../ajax/classe.php',
       type: 'POST',
       async:false,
       data: 'classe='+classeId+'&etape='+etape,
       dataType: 'text',
       success: function (content, statut)
       {
         $("#idstudent").html("");
         $("#idstudent").html(content);
       }
     });

   }

   $(document).ready(function() {

$("#FormSearch").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
 },
 highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
 },
 success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{
    matTeatcher:"required",
    idprof:"required",
  },
  messages: {
    matTeatcher:"Merci de selectionner le matricule Professeur",
    idprof:"Merci de selectionner un Professeur",
  },
    submitHandler: function(form) {
      form.submit();
    }

});

$("#FormSearchStudent").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
 },
 highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
 },
 success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{
    classeId:"required",
    idstudent:"required",
  },
  messages: {
    classeId:"Merci de selectionner une Classe",
    idstudent:"Merci de selectionner un Elève",
  },
    submitHandler: function(form) {
      form.submit();
    }

});


   });

   </script>
    <!-- end js include path -->
  </body>

</html>
