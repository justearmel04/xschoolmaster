<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Teatcher.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();




$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$matiere=new Matiere();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$localadmins= new Localadmin();
$parents=new ParentX();
$classe=new Classe();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$_SESSION['user']['codeEtab']=$codeEtabsession;

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $_SESSION['user']['session']=$libellesessionencours;
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];


  $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
}else {
  $nbclasse=0;
}

$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);



if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();

$sessions=$session->getAllSessionsOfThisSchoolCode($codeEtabsession);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabsession);
$semesters=$session->getAllsemesterOfthisSession($sessionencoursid,$codeEtabsession);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
  <!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
  <link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
  <link href="../assets2/css/material_style.css" rel="stylesheet">
  <!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

  <!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />

    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title">Année scolaire</div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active">Année scolaire</li>
                          </ol>
                      </div>
                  </div>

                  <?php

                        if(isset($_SESSION['user']['Updateadminok']))
                        {

                          ?>
                          <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?php
                        //echo $_SESSION['user']['addetabok'];
                        ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                           </a>
                        </div-->
                <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                <script src="../assets/js/sweetalert2.min.js"></script>

                    <script>
                    Swal.fire({
                    type: 'success',
                    title: 'Félicitations',
                    text: '<?php echo $_SESSION['user']['Updateadminok']; ?>',

                    })
                    </script>
                          <?php
                          unset($_SESSION['user']['Updateadminok']);
                        }

                         ?>
                         <?php

                               if(isset($_SESSION['user']['addetabexist']))
                               {

                                 ?>
                                 <div class="alert alert-danger alert-dismissible fade show" role="alert">
                               <?php
                               echo $_SESSION['user']['addetabexist'];
                               ?>
                               <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                               <span aria-hidden="true">&times;</span>
                                  </a>
                               </div>



                                 <?php
                                 unset($_SESSION['user']['addetabexist']);
                               }

                                ?>



                                <?php
                                if($etablissementType==1||$etablissementType==3)
                                {
                                 ?>


                         <div class="row">

                           <div class="col-md-12 col-sm-12">
                                          <div class="panel tab-border card-box">
                                              <header class="panel-heading panel-heading-gray custom-tab ">
                                                  <ul class="nav nav-tabs">
                                                      <li class="nav-item"><a href="#home" data-toggle="tab" class="active">Annéz scolaire en cours </a>
                                                      </li>
                                                      <!--li class="nav-item"><a href="#type" data-toggle="tab">Type Session En cours</a-->
                                                      </li>
                                                      <li class="nav-item"><a href="#about" data-toggle="tab">Ajouter une année scolaire</a>
                                                      </li>

                                                  </ul>
                                              </header>
                                              <div class="panel-body">
                                                  <div class="tab-content">
                                                      <div class="tab-pane active" id="home">
                                                        <div class="col-md-12">
                                          <div class="card  card-box">

                                              <div class="card-body ">

                                                  <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                                      <thead>
                                                          <tr>

                                                              <th> Id </th>
                                                              <th> Année scolaire</th>
                                                              <th> Date début</th>
                                                              <th> Date fin</th>
                                                              <th> Statut</th>
                                                              <th> <?php echo L::Actions?> </th>
                                                          </tr>
                                                      </thead>
                                                      <?php
                                                      if($nbsessionOn>0)
                                                      {
                                                       ?>
                                                      <tbody>
                                                        <?php
                                                        $i=1;
                                                        foreach ($sessions as $value):
                                                         ?>
                                                          <tr class="odd gradeX">

                                                              <td> <?php echo $i ?></td>
                                                              <td>
                                                                  <a href="#"> <?php echo $value->libelle_sess; ?> </a>
                                                              </td>
                                                              <td>

                                                                <?php echo date_format(date_create($value->datedeb_sess), "d-m-Y") ?>
                                                              </td>
                                                              <td>

                                                                <?php echo date_format(date_create($value->datefin_sess), "d-m-Y") ?>
                                                              </td>
                                                              <td>
                                                                <?php
                                                                if($value->encours_sess==0)
                                                                {
                                                                  ?>
                                                         <span class="label label-sm label-danger"> Inactif </span>
                                                                  <?php
                                                                }else if($value->encours_sess==1)
                                                                {
                                                                  ?>
                                                         <span class="label label-sm label-success"> Actif </span>
                                                                  <?php
                                                                }
                                                                 ?>
                                                              </td>
                                                              <td>
                                                                <?php
                                                                if($value->encours_sess==1 &&$value->statut_sess==0)
                                                                {
                                                               ?>
                                                               <!-- <a href="#"  class="btn btn-danger btn-xs" onclick="sessioncloturate(<?php e//cho $value->id_sess;  ?>)" title="Clôturer la session">
                                                                 <i class="fa fa-times-rectangle"></i>
                                                               </a> -->
                                                               <?php
                                                                }
                                                                 ?>

                                                              </td>

                             <div class="modal fade" id="mediumModel<?php echo $value->id_sess?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                             <div class="modal-dialog" role="document">
                                 <div class="modal-content">
                                     <div class="modal-header">
                                         <h4 class="modal-title" id="exampleModalLabel">Modification Session</h4>
                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                             <span aria-hidden="true">&times;</span>
                                         </button>
                                     </div>
                                     <div class="modal-body">
                                       <form>
                                                      <div class="form-group">
                                                          <label for="simpleFormEmail">Session</label>
                                                          <input type="text" onclick="erasedSession(<?php echo $value->id_sess;?>)" class="form-control" id="sessionlib<?php echo $value->id_sess;?>" name="sessionlib<?php echo $value->id_sess;?>" value="<?php echo $value->libelle_sess ?>" placeholder="Merci de renseigner la Session">
                                                          <p id="messageSessionLib<?php echo $value->id_sess;?>"></p>
                                                      </div>
                                                      <div class="form-group">
                                                          <label for="simpleFormPassword">Période</label>
                                                          <select class="form-control" name="typesession<?php echo $value->id_sess;?>" id="typesession<?php echo $value->id_sess;?>">
                                                            <option value="">Merci de selectionner une période</option>
                                                            <option <?php if($value->type_sess==2){echo "selected";} ?> value="2">Semestre</option>
                                                            <option <?php if($value->type_sess==3){echo "selected";} ?> value="3">Trimestre</option>

                                                          </select>
                                                          <input type="hidden" name="etape" id="etape" value="2">
                                                          <input type="hidden" name="oldsessiontype<?php echo $value->id_sess;?>" id="oldsessiontype<?php echo $value->id_sess;?>" value="<?php echo $value->type_sess; ?>">
                                                          <input type="hidden" name="codeEtab<?php echo $value->id_sess;?>" id="codeEtab<?php echo $value->id_sess;?>" value="<?php echo $value->codeEtab_sess;?>">



                                                      </div>

                                                      <a href="#" onclick="checkSession(<?php echo $value->id_sess;?>)" class="btn btn-primary">Modifier</a>
                                                      <a href="#" class="btn btn-danger btn-md" data-dismiss="modal"> Annuler</a>
                                                  </form>
                                     </div>

                                 </div>
                             </div>
                         </div>

                                                          </tr>
                                                          <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                                          <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                                          <script type="text/javascript">

                                                          function erasedSession(id)
                                                          {
                            document.getElementById("messageSessionLib"+id).innerHTML = "";

                                                          }

                                                          function checkSession(id)
                                                          {
                                                            var sessionlib=$("#sessionlib"+id).val();
                                                            var typesession=$("#typesession"+id).val();
                                                            var codeEtab=$("#codeEtab"+id).val();
                                                            var oldsessiontype=$("#oldsessiontype"+id).val();

                                                            if(sessionlib==""||typesession=="")
                                                            {
                                                               if(sessionlib=="")
                                                               {
                                   document.getElementById("messageSessionLib"+id).innerHTML = "<font color=\"red\">Merci de renseigner le libellé de la session SVP !</font>";

                                                               }
                                                            }else {

                                                             //comparer les types de session

                                                             var etape=2;
                                                             $.ajax({
                                                             url: '../ajax/sessions.php',
                                                             type: 'POST',
                                                             async:true,
                                                              data:'etape=' + etape+'&sessionid='+id+'&sessionlib='+sessionlib+'&typesession='+typesession+'&codeEtab='+codeEtab+'&oldsessiontype='+oldsessiontype,
                                                              dataType: 'text',
                                                              success: function (content, statut) {

                                                                //event.preventDefault();

                                                                $("#mediumModel"+id).css("display", "none")

                                                                Swal.fire({
                                                                type: 'success',
                                                                title: 'Félicitations',
                                                                text: 'Session mis à jour avec succès',

                                                              });

                                                                location.reload();
                                                            }

                                                          });


                                                        }
                                                      }

                                                          function modify(id)
                                                          {
                                                            Swal.fire({
                                              title: '<?php echo L::WarningLib ?>',
                                              text: "Voulez vous vraiment modifier la session",
                                              type: 'warning',
                                              showCancelButton: true,
                                              confirmButtonColor: '#2CA8FF',
                                              cancelButtonColor: '#d33',
                                              confirmButtonText: '<?php echo L::ModifierBtn ?>',
                                              cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                            }).then((result) => {
                                              if (result.value) {
                                                // document.location.href="../controller/messages.php?etape=5&notifid="+id;
                                                $('#btn<?php echo $value->id_sess?>').click();


                                              }else {

                                              }
                                            })
                                                          }

                                                          function archiver(id)
                                                          {
                                                            Swal.fire({
                                              title: '<?php echo L::WarningLib ?>',
                                              text: "<?php echo L::DoyouReallyArchivedMessages ?>",
                                              type: 'warning',
                                              showCancelButton: true,
                                              confirmButtonColor: '#2CA8FF',
                                              cancelButtonColor: '#d33',
                                              confirmButtonText: '<?php echo L::Archived ?>',
                                              cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                            }).then((result) => {
                                              if (result.value) {
                                                document.location.href="../controller/messages.php?etape=5&notifid="+id;

                                              }else {

                                              }
                                            })
                                                          }

                                                          function deleted(id)
                                                          {
                                                            // var notificationid="<?php //echo $value->id_notif; ?>";
                                                            // var destinataires="<?php //echo $value->destinataires_notif; ?>";
                                                            // var classes="<?php //echo  $value->classes_notif;?>";
                                                            var etape=4;
                                                            // var codeEtab="<?php  //echo $value->codeEtab_notif;?>";

                                                            Swal.fire({
                                              title: '<?php echo L::WarningLib ?>',
                                              text: "<?php echo L::DoyouReallyDeletedMessages ?>",
                                              type: 'warning',
                                              showCancelButton: true,
                                              confirmButtonColor: '#2CA8FF',
                                              cancelButtonColor: '#d33',
                                              confirmButtonText: 'Supprmer',
                                              cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                            }).then((result) => {
                                              if (result.value) {
                                                document.location.href="../controller/messages.php?etape=4&notifid="+id;

                                              }else {

                                              }
                                            })
                                                          }

                                                          function send(id)
                                                          {

                                                            //nous allons recuperer les elements essentiels pour l'envoi de mail ou sms
                                                            var etape1=1;
                                                            $.ajax({
                                                              url: '../ajax/message.php',
                                                              type: 'POST',
                                                              async:false,
                                                              data: 'notifid='+id+'&etape='+etape1,
                                                              dataType: 'text',
                                                                success: function (content, statut) {


                                                                  var tabcontent=content.split("*");
                                                                  var destinataires=tabcontent[0];
                                                                  var classes=tabcontent[1];
                                                                  var codeEtab=tabcontent[2];
                                                                  var smssender=tabcontent[3];
                                                                  var emailsender=tabcontent[4];
                                                                  var joinfile=tabcontent[5];
                                                                  var file=tabcontent[6];
                                                                  var etape2=3;

                                                                  Swal.fire({
                                                    title: '<?php echo L::WarningLib ?>',
                                                    text: "<?php echo L::DoyouReallySendingThisMessages ?>",
                                                    type: 'warning',
                                                    showCancelButton: true,
                                                    confirmButtonColor: '#2CA8FF',
                                                    cancelButtonColor: '#d33',
                                                    confirmButtonText: '<?php echo L::Sendbutton ?>',
                                                    cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                                  }).then((result) => {
                                                    if (result.value) {
                                                      // document.location.href="../controller/messages/etape=3&notifid="+notificationid+"&destinataires="+destinataires+"&classes="+classes;
                                                      $.ajax({
                                                        url: '../ajax/message.php',
                                                        type: 'POST',
                                                        async:false,
                                                        data: 'notifid='+id+'&etape='+etape2+'&codeEtab='+codeEtab+'&destinataires='+destinataires+'&classes='+classes+'&smssender='+smssender+'&emailsender='+emailsender,
                                                        dataType: 'text',
                                                          success: function (content, statut) {

                                                            var tab=content.split("/");
                                                            var destimails=tab[0];
                                                            var destiphones=tab[1];

                                           document.location.href="../controller/messages.php?etape=3&notifid="+id+"&destimails="+destimails+"&destiphones="+destiphones+"&codeEtab="+codeEtab+"&smssender="+smssender+"&emailsender="+emailsender+"&joinfile="+joinfile+"&file="+file;


                                                          }
                                                      });
                                                    }else {

                                                    }
                                                  })

                                                                }
                                                            });







                                                          }

                                                          </script>

                                                          <?php
                                                          $i++;
                                                          endforeach
                                                           ?>



                                                      </tbody>

                                                      <?php
                                                    }
                                                       ?>
                                                  </table>
                                              </div>
                                          </div>
                                      </div>
                                                      </div>
                                                      <div class="tab-pane" id="about">
                                                        <div class="offset-md-1 col-md-10 col-sm-10">
                                         <div class="card card-box">

                                             <div class="card-body " id="bar-parent">
                                                 <form method="post" action="../controller/sessions.php" id="FormAddmessage" enctype="multipart/form-data" >

                                                     <div class="form-group">
                                                         <label for="titre">Libellé de la session :</label>
                                                         <input type="text" class="form-control col-md-8" id="titre" name="titre" placeholder="Renseigner le libellé de la session">
                                                         <input type="hidden" name="etape" id="etape"  value="3">
                                                         <input type="hidden" name="typesession" id="typesession" value="4">
                                                         <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner; ?>">
                                                     </div>

                                                     <!--div class="form-group">
                                                         <label for="semestre">Semestre / Trimestre :</label>
                                                         <select class="form-control input-height" id="semestre" name="semestre" style="width:50%">

                                                             <option value="2">Semestre</option>
                                                             <option value="3" selected>Trimestre</option>
                                                         </select>
                                                     </div-->

                                                     <div class="form-group">
                                                         <label for="titre">Date début:</label>
                                                         <input type="text" placeholder="Date de début" name="datedeb" id="datedeb"  class="form-control col-md-8">

                                                     </div>

                                                     <div class="form-group">
                                                         <label for="titre">Date Fin:</label>
                                                         <input type="text" placeholder="Date de fin" name="datefin" id="datefin"  class="form-control col-md-8">

                                                     </div>





                                                     <br>
                                                     <?php
                                                     // $libellesessionencours=$tabsessionencours[0];
                                                     // $sessionencoursid=$tabsessionencours[1];
                                                     // $typesessionencours=$tabsessionencours[2];
                                                     //verifier si nous avons un semestre ou un trimestre dont le statut est à 2

                                                     $checkstat=$session->getNumberSessionEncoursOn($codeEtabAssigner);

                                                     if($checkstat==0)
                                                     {
                                                     ?>
                                                     <button type="submit" class="btn btn-primary">Enregistrer</button>
                                                     <?php
                                                   }else {
                                                     ?>
                                                     <button type="submit" class="btn btn-primary" disabled>Enregistrer</button>
                                                     <?php
                                                   }


                                                      ?>

                                                 </form>
                                             </div>
                                         </div>
                                     </div>
                                                      </div>
                                                      <div class="tab-pane" id="type">
                                                        <div class="offset-md-1 col-md-10 col-sm-10">
                                         <!--div class="card card-box"-->

                                             <div class="card-body " id="bar-parent">
                                              <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                                <thead>
                                                    <tr>
                                                      <th> Libellé Session</th>
                                                        <th> Statut</th>
                                                        <th> <?php echo L::Actions?> </th>
                                                    </tr>
                                                </thead>
                                                <?php
                                                if($nbsessionOn>0)
                                                {
                                                 ?>
                                                <tbody>
                                                  <?php
                                                  $i=1;
                                                  foreach ($semesters as $value):
                                                   ?>
                                                  <tr>

                                                    <td><?php echo $value->libelle_semes ?></td>
                                                    <td>
                                                      <?php
                                                      $statut=$value->statut_semes;
                                                      if($statut==1)
                                                      {
                                                        ?>
                                        <span class="label label-sm label-success"> Actif </span>
                                                        <?php
                                                      }else if($statut==2)
                                                      {
                                                        ?>
                                       <span class="label label-sm label-danger"> Terminé </span>
                                                        <?php
                                                      }
                                                       ?>
                                                    </td>
                                                    <td>
                                                      <?php

                                                      if($value->statut_semes!=2)
                                                      {
                                                       ?>
                                                       <?php
                                                       if($statut==1)
                                                       {
                                                        ?>
                                                        <a href="#"  onclick="semesterclose(<?php echo $value->id_semes;?>,<?php echo $value->idsess_semes;?>,<?php echo $value->next_semes ?>)" class="btn btn-danger btn-xs" title="Clôturer">
                                                          <i class="fa fa-times-rectangle"></i>
                                                        </a>
                                                        <?php
                                                      }
                                                         ?>


                                                      <?php
                                                      }
                                                       ?>
                                                    </td>
                                                  </tr>
                                                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                                  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                                  <script type="text/javascript">
                                                  function semesterclose(id,sessionid,suivant)
                                                  {

                                                    Swal.fire({
                                      title: '<?php echo L::WarningLib ?>',
                                      text: "Voulez vous vraiment cloturer ",
                                      type: 'warning',
                                      showCancelButton: true,
                                      confirmButtonColor: '#2CA8FF',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Clôturer',
                                      cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                    }).then((result) => {
                                      if (result.value) {
                                        // document.location.href="../controller/messages.php?etape=4&notifid="+id;

                                        // document.location.href="../controller/sessions.php?etape=2&typesess="+id+"&sessionid="+sessionid+"&suivantid="+suivant;

                                        Swal.fire({
                          title: '<?php echo L::WarningLib ?>',
                          text: " une fois la session cloturée,il n'est plus possible d'effectuer d'actions sur les notes,les présences,les activités parascolaires,le planning.....liés à cette session ",
                          type: 'warning',
                          showCancelButton: true,
                          confirmButtonColor: '#2CA8FF',
                          cancelButtonColor: '#d33',
                          confirmButtonText: 'Clôturer',
                          cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                        }).then((result) => {
                          if (result.value) {
                            // document.location.href="../controller/messages.php?etape=4&notifid="+id;

                            document.location.href="../controller/sessions.php?etape=2&typesess="+id+"&sessionid="+sessionid+"&suivantid="+suivant;

                          }else {

                          }
                        })

                                      }else {

                                      }
                                    })
                                                  }
                                                  </script>

                                                  <?php
                                                  $i++;
                                                  endforeach
                                                   ?>

                                                </tbody>
                                                <?php
                                              }
                                                 ?>

                                                </table>

                                             </div>
                                         <!--/div-->
                                     </div>
                                                      </div>

                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                         </div>

                         <?php
               }else {
                 ?>
                 <div class="row">

                   <div class="col-md-12 col-sm-12">
                                  <div class="panel tab-border card-box">
                                      <header class="panel-heading panel-heading-gray custom-tab ">
                                          <ul class="nav nav-tabs">
                                              <li class="nav-item"><a href="#home" data-toggle="tab" class="active">Année scolaire en cours </a>
                                              </li>
                                              <li class="nav-item"><a href="#type" data-toggle="tab">Période</a>
                                              </li>
                                              <!-- <li class="nav-item"><a href="#about" data-toggle="tab">Ajouter une année scolaire</a>
                                              </li> -->

                                          </ul>
                                      </header>
                                      <div class="panel-body">
                                          <div class="tab-content">
                                              <div class="tab-pane active" id="home">
                                                <div class="col-md-12">
                                  <div class="card  card-box">

                                      <div class="card-body ">

                                          <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                              <thead>
                                                  <tr>

                                                      <!-- <th> Id </th> -->
                                                      <th> Année scolaire</th>
                                                      <th> Période</th>
                                                      <th> Statut</th>
                                                      <!-- <th> <?php echo L::Actions?> </th> -->
                                                  </tr>
                                              </thead>
                                              <?php
                                              if($nbsessionOn>0)
                                              {
                                               ?>
                                              <tbody>
                                                <?php
                                                $i=1;
                                                foreach ($sessions as $value):
                                                 ?>
                                                  <tr class="odd gradeX">

                                                      <!-- <td> <?php //echo $i ?></td> -->
                                                      <td>
                                                          <a href="#"> <?php echo $value->libelle_sess; ?> </a>
                                                      </td>
                                                      <td>
                                                          <?php
                                                           if($value->type_sess==2)
                                                           {
                                                             ?>
                                               <span class="label label-sm label-primary"> Semestre </span>
                                                             <?php
                                                           }else if($value->type_sess==3)
                                                           {
                                                             ?>
                                               <span class="label label-sm label-primary"> Trimestre </span>
                                                             <?php
                                                           }
                                                           ?>
                                                      </td>
                                                      <td>
                                                        <?php
                                                        if($value->encours_sess==0)
                                                        {
                                                          ?>
                                                 <!-- <span class="label label-sm label-danger"> Cloturer </span> -->
                                                          <?php
                                                        }else if($value->encours_sess==1)
                                                        {
                                                          ?>
                                                 <span class="label label-sm label-success"> Actif </span>
                                                          <?php
                                                        }
                                                         ?>
                                                      </td>


                     <div class="modal fade" id="mediumModel<?php echo $value->id_sess?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                     <div class="modal-dialog" role="document">
                         <div class="modal-content">
                             <div class="modal-header">
                                 <h4 class="modal-title" id="exampleModalLabel">Modification Session</h4>
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                     <span aria-hidden="true">&times;</span>
                                 </button>
                             </div>
                             <div class="modal-body">
                               <form>
                                              <div class="form-group">
                                                  <label for="simpleFormEmail">Année scolaire</label>
                                                  <input type="text" onclick="erasedSession(<?php echo $value->id_sess;?>)" class="form-control" id="sessionlib<?php echo $value->id_sess;?>" name="sessionlib<?php echo $value->id_sess;?>" value="<?php echo $value->libelle_sess ?>" placeholder="Merci de renseigner la Session">
                                                  <p id="messageSessionLib<?php echo $value->id_sess;?>"></p>
                                              </div>
                                              <div class="form-group">
                                                  <label for="simpleFormPassword">Période</label>
                                                  <select class="form-control" name="typesession<?php echo $value->id_sess;?>" id="typesession<?php echo $value->id_sess;?>">
                                                    <option value="">Merci de selectionner une période</option>
                                                    <option <?php if($value->type_sess==2){echo "selected";} ?> value="2">Semestre</option>
                                                    <option <?php if($value->type_sess==3){echo "selected";} ?> value="3">Trimestre</option>

                                                  </select>
                                                  <input type="hidden" name="etape" id="etape" value="2">
                                                  <input type="hidden" name="oldsessiontype<?php echo $value->id_sess;?>" id="oldsessiontype<?php echo $value->id_sess;?>" value="<?php echo $value->type_sess; ?>">
                                                  <input type="hidden" name="codeEtab<?php echo $value->id_sess;?>" id="codeEtab<?php echo $value->id_sess;?>" value="<?php echo $value->codeEtab_sess;?>">



                                              </div>

                                              <a href="#" onclick="checkSession(<?php echo $value->id_sess;?>)" class="btn btn-primary">Modifier</a>
                                              <a href="#" class="btn btn-danger btn-md" data-dismiss="modal"> Annuler</a>
                                          </form>
                             </div>

                         </div>
                     </div>
                 </div>

                                                  </tr>
                                                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                                  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                                  <script type="text/javascript">

                                                  function erasedSession(id)
                                                  {
                    document.getElementById("messageSessionLib"+id).innerHTML = "";

                                                  }

                                                  function checkSession(id)
                                                  {
                                                    var sessionlib=$("#sessionlib"+id).val();
                                                    var typesession=$("#typesession"+id).val();
                                                    var codeEtab=$("#codeEtab"+id).val();
                                                    var oldsessiontype=$("#oldsessiontype"+id).val();

                                                    if(sessionlib==""||typesession=="")
                                                    {
                                                       if(sessionlib=="")
                                                       {
                           document.getElementById("messageSessionLib"+id).innerHTML = "<font color=\"red\">Merci de renseigner le libellé de la session SVP !</font>";

                                                       }
                                                    }else {

                                                     //comparer les types de session

                                                     var etape=2;
                                                     $.ajax({
                                                     url: '../ajax/sessions.php',
                                                     type: 'POST',
                                                     async:true,
                                                      data:'etape=' + etape+'&sessionid='+id+'&sessionlib='+sessionlib+'&typesession='+typesession+'&codeEtab='+codeEtab+'&oldsessiontype='+oldsessiontype,
                                                      dataType: 'text',
                                                      success: function (content, statut) {

                                                        //event.preventDefault();

                                                        $("#mediumModel"+id).css("display", "none")

                                                        Swal.fire({
                                                        type: 'success',
                                                        title: 'Félicitations',
                                                        text: 'Session mis à jour avec succès',

                                                      });

                                                        location.reload();
                                                    }

                                                  });


                                                }
                                              }

                                                  function modify(id)
                                                  {
                                                    Swal.fire({
                                      title: '<?php echo L::WarningLib ?>',
                                      text: "Voulez vous vraiment modifier la session",
                                      type: 'warning',
                                      showCancelButton: true,
                                      confirmButtonColor: '#2CA8FF',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: '<?php echo L::ModifierBtn ?>',
                                      cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                    }).then((result) => {
                                      if (result.value) {
                                        // document.location.href="../controller/messages.php?etape=5&notifid="+id;
                                        $('#btn<?php echo $value->id_sess?>').click();


                                      }else {

                                      }
                                    })
                                                  }

                                                  function archiver(id)
                                                  {
                                                    Swal.fire({
                                      title: '<?php echo L::WarningLib ?>',
                                      text: "<?php echo L::DoyouReallyArchivedMessages ?>",
                                      type: 'warning',
                                      showCancelButton: true,
                                      confirmButtonColor: '#2CA8FF',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: '<?php echo L::Archived ?>',
                                      cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                    }).then((result) => {
                                      if (result.value) {
                                        document.location.href="../controller/messages.php?etape=5&notifid="+id;

                                      }else {

                                      }
                                    })
                                                  }

                                                  function deleted(id)
                                                  {
                                                    // var notificationid="<?php //echo $value->id_notif; ?>";
                                                    // var destinataires="<?php //echo $value->destinataires_notif; ?>";
                                                    // var classes="<?php //echo  $value->classes_notif;?>";
                                                    var etape=4;
                                                    // var codeEtab="<?php  //echo $value->codeEtab_notif;?>";

                                                    Swal.fire({
                                      title: '<?php echo L::WarningLib ?>',
                                      text: "<?php echo L::DoyouReallyDeletedMessages ?>",
                                      type: 'warning',
                                      showCancelButton: true,
                                      confirmButtonColor: '#2CA8FF',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Supprmer',
                                      cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                    }).then((result) => {
                                      if (result.value) {
                                        document.location.href="../controller/messages.php?etape=4&notifid="+id;

                                      }else {

                                      }
                                    })
                                                  }

                                                  function send(id)
                                                  {

                                                    //nous allons recuperer les elements essentiels pour l'envoi de mail ou sms
                                                    var etape1=1;
                                                    $.ajax({
                                                      url: '../ajax/message.php',
                                                      type: 'POST',
                                                      async:false,
                                                      data: 'notifid='+id+'&etape='+etape1,
                                                      dataType: 'text',
                                                        success: function (content, statut) {


                                                          var tabcontent=content.split("*");
                                                          var destinataires=tabcontent[0];
                                                          var classes=tabcontent[1];
                                                          var codeEtab=tabcontent[2];
                                                          var smssender=tabcontent[3];
                                                          var emailsender=tabcontent[4];
                                                          var joinfile=tabcontent[5];
                                                          var file=tabcontent[6];
                                                          var etape2=3;

                                                          Swal.fire({
                                            title: '<?php echo L::WarningLib ?>',
                                            text: "<?php echo L::DoyouReallySendingThisMessages ?>",
                                            type: 'warning',
                                            showCancelButton: true,
                                            confirmButtonColor: '#2CA8FF',
                                            cancelButtonColor: '#d33',
                                            confirmButtonText: '<?php echo L::Sendbutton ?>',
                                            cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                          }).then((result) => {
                                            if (result.value) {
                                              // document.location.href="../controller/messages/etape=3&notifid="+notificationid+"&destinataires="+destinataires+"&classes="+classes;
                                              $.ajax({
                                                url: '../ajax/message.php',
                                                type: 'POST',
                                                async:false,
                                                data: 'notifid='+id+'&etape='+etape2+'&codeEtab='+codeEtab+'&destinataires='+destinataires+'&classes='+classes+'&smssender='+smssender+'&emailsender='+emailsender,
                                                dataType: 'text',
                                                  success: function (content, statut) {

                                                    var tab=content.split("/");
                                                    var destimails=tab[0];
                                                    var destiphones=tab[1];

                                   document.location.href="../controller/messages.php?etape=3&notifid="+id+"&destimails="+destimails+"&destiphones="+destiphones+"&codeEtab="+codeEtab+"&smssender="+smssender+"&emailsender="+emailsender+"&joinfile="+joinfile+"&file="+file;


                                                  }
                                              });
                                            }else {

                                            }
                                          })

                                                        }
                                                    });







                                                  }

                                                  </script>

                                                  <?php
                                                  $i++;
                                                  endforeach
                                                   ?>



                                              </tbody>

                                              <?php
                                            }
                                               ?>
                                          </table>
                                      </div>
                                  </div>
                              </div>
                                              </div>
                                              <div class="tab-pane" id="about">
                                                <div class="offset-md-1 col-md-10 col-sm-10">
                                 <div class="card card-box">

                                     <div class="card-body " id="bar-parent">
                                         <form method="post" action="../controller/sessions.php" id="FormAddmessage" enctype="multipart/form-data" >

                                             <div class="form-group">
                                                 <label for="titre">Annés scolaire :</label>
                                                 <input type="text" class="form-control col-md-8" id="titre" name="titre" placeholder="Renseigner le libellé de l'année scolaire'">
                                                 <input type="hidden" name="etape" id="etape"  value="1">
                                                 <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner; ?>">
                                             </div>

                                             <!--div class="form-group">
                                                 <label for="semestre">Semestre / Trimestre :</label>
                                                 <select class="form-control input-height" id="semestre" name="semestre" style="width:50%">

                                                     <option value="2">Semestre</option>
                                                     <option value="3" selected>Trimestre</option>
                                                 </select>
                                             </div-->

                                             <div class="form-group">
                                                 <label for="titre">Date début:</label>
                                                 <input type="text" placeholder="Date de début" name="datedeb" id="datedeb"  class="form-control col-md-8">

                                             </div>

                                             <div class="form-group">
                                                 <label for="titre">Date Fin:</label>
                                                 <input type="text" placeholder="Date de fin" name="datefin" id="datefin"  class="form-control col-md-8">

                                             </div>



                 <!-- Material inline 2 -->
                 <div class="radioicon radioicon-aqua form-check form-check-inline">

                 <input type="radio" name="semestre" id="materialInline2"  value="2"/>
                 <label for="materialInline2"> Semestre</label>

                 </div>

                 <div class="radioicon radioicon-aqua form-check form-check-inline">
                 <input type="radio" name="semestre" id="materialInline3" value="3" checked/>
                 <label for="materialInline3"> Trimestre</label>

                 </div>
                 <br>



                                             <br>
                                             <?php
                                             // $libellesessionencours=$tabsessionencours[0];
                                             // $sessionencoursid=$tabsessionencours[1];
                                             // $typesessionencours=$tabsessionencours[2];
                                             //verifier si nous avons un semestre ou un trimestre dont le statut est à 2

                                             $checkstat=$session->getNumberSessionEncoursOn($codeEtabAssigner);

                                             if($checkstat==0)
                                             {
                                             ?>
                                             <button type="submit" class="btn btn-primary">Enregistrer</button>
                                             <?php
                                           }else {
                                             ?>
                                             <button type="submit" class="btn btn-primary" disabled>Enregistrer</button>
                                             <?php
                                           }


                                              ?>

                                         </form>
                                     </div>
                                 </div>
                             </div>
                                              </div>
                                              <div class="tab-pane" id="type">
                                                <div class="offset-md-1 col-md-10 col-sm-10">
                                 <!--div class="card card-box"-->

                                     <div class="card-body " id="bar-parent">
                                      <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                        <thead>
                                            <tr>
                                              <th> Année scolaire</th>
                                                <th> Statut</th>
                                                <!-- <th> <?php echo L::Actions?> </th> -->
                                            </tr>
                                        </thead>
                                        <?php
                                        if($nbsessionOn>0)
                                        {
                                         ?>
                                        <tbody>
                                          <?php
                                          $i=1;
                                          foreach ($semesters as $value):
                                           ?>
                                          <tr>

                                            <td><?php echo $value->libelle_semes ?></td>
                                            <td>
                                              <?php
                                              $statut=$value->statut_semes;
                                              if($statut==1)
                                              {
                                                ?>
                                <span class="label label-sm label-success"> Actif </span>
                                                <?php
                                              }else if($statut==2)
                                              {
                                                ?>
                               <span class="label label-sm label-danger"> Terminé </span>
                                                <?php
                                              }
                                               ?>
                                            </td>

                                          </tr>
                                          <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                          <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                          <script type="text/javascript">
                                          function semesterclose(id,sessionid,suivant)
                                          {

                                            Swal.fire({
                              title: '<?php echo L::WarningLib ?>',
                              text: "Voulez vous vraiment cloturer ",
                              type: 'warning',
                              showCancelButton: true,
                              confirmButtonColor: '#2CA8FF',
                              cancelButtonColor: '#d33',
                              confirmButtonText: 'Clôturer',
                              cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                            }).then((result) => {
                              if (result.value) {
                                // document.location.href="../controller/messages.php?etape=4&notifid="+id;
                                // document.location.href="../controller/sessions.php?etape=2&typesess="+id+"&sessionid="+sessionid+"&suivantid="+suivant;

                                Swal.fire({
                  title: '<?php echo L::WarningLib ?>',
                  text: "une fois la session cloturée,il n'est plus possible d'effectuer d'actions sur les notes,les présences,les activités parascolaires,le planning..... liés à cette session ",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#2CA8FF',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Clôturer',
                  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                }).then((result) => {
                  if (result.value) {
                    // document.location.href="../controller/messages.php?etape=4&notifid="+id;

                    document.location.href="../controller/sessions.php?etape=2&typesess="+id+"&sessionid="+sessionid+"&suivantid="+suivant;

                  }else {

                  }
                })

                              }else {

                              }
                            })
                                          }
                                          </script>

                                          <?php
                                          $i++;
                                          endforeach
                                           ?>

                                        </tbody>
                                        <?php
                                      }
                                         ?>

                                        </table>

                                     </div>
                                 <!--/div-->
                             </div>
                                              </div>

                                          </div>
                                      </div>
                                  </div>
                              </div>

                 </div>
                 <?php
               }
                          ?>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
  <script src="../assets2/js/dropify.js"></script>
  <script src="../assets2/plugins/select2/js/select2.js" ></script>
  <script src="../assets2/js/pages/select2/select2-init.js" ></script>
  <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
  <script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
  <script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
  <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   var date = new Date();

  var newDate = new Date(date.setTime( date.getTime() + (-6120 * 86400000)));

   $('#datedeb').bootstrapMaterialDatePicker
   ({
   date: true,
   shortTime: false,
   time: false,
   format: 'DD-MM-YYYY',
   lang: 'fr',
   cancelText: '<?php echo L::AnnulerBtn ?>',
   okText: 'OK',
   clearText: '<?php echo L::Eraser ?>',
   nowText: '<?php echo L::Now ?>'

   });

   $('#datefin').bootstrapMaterialDatePicker
   ({
   date: true,
   shortTime: false,
   time: false,
   format: 'DD-MM-YYYY',
   lang: 'fr',
   cancelText: '<?php echo L::AnnulerBtn ?>',
   okText: 'OK',
   clearText: '<?php echo L::Eraser ?>',
   nowText: '<?php echo L::Now ?>'

   });

      // $("#semestre").select2();

      function sessioncloturate(sessionid)
      {
        var etape=4;
        var codeEtab="<?php echo $codeEtabAssigner ?>";
        Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "Voulez vous vraiment cloturer cette session ",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#2CA8FF',
cancelButtonColor: '#d33',
confirmButtonText: 'Clôturer',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
// document.location.href="../controller/messages.php?etape=4&notifid="+id;
// document.location.href="../controller/sessions.php?etape=2&typesess="+id+"&sessionid="+sessionid+"&suivantid="+suivant;



Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "une fois la session cloturée,il n'est plus possible d'effectuer d'actions sur les notes,les présences,les activités parascolaires,le planning..... liés à cette session ",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#2CA8FF',
cancelButtonColor: '#d33',
confirmButtonText: 'Clôturer',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
// document.location.href="../controller/messages.php?etape=4&notifid="+id;

$.ajax({
  url: '../controller/sessions.php',
  type: 'POST',
  async:false,
  data: 'sessionid='+sessionid+'&etape='+etape+'&codeEtab='+codeEtab,
  dataType: 'text',
    success: function (content, statut) {

 location.reload();

      }
});


}else {

}
})

}else {

}
})
      }

   function checksms()
   {


     if($('#smssender').prop('checked') == true){
 			$("#smsvalue").val(1);
 		}
 		else {

 			$("#smsvalue").val(0);
 		}

   }

   function checkmail()
   {


     if($('#emailsender').prop('checked') == true){
      $("#emailvalue").val(1);
    }
    else {

      $("#emailvalue").val(0);
    }



   }



   $("#classeEtab").select2({
     tags: true,
  tokenSeparators: [',', ' ']
   });
   $("#destinataires").select2({
     tags: true,
  tokenSeparators: [',', ' ']
   });
   $('.joinfile').dropify({
       messages: {
           'default': 'Selectionner un fichier joint',
           'replace': 'Remplacer le fichier joint',
           'remove':  'Retirer',
           'error':   'Ooops, Une erreur est survenue.'
       }
   });
   $(document).ready(function() {
$("#FormAddmessage").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
},
highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
},
success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{
    titre:"required",
    message:"required",
    classeEtab:"required",
    destinataires:"required",
    semestre:"required",
    datefin:"required",
    datedeb:"required",
  },
  messages: {
    titre:"Merci de renseigner le libellé de la session",
    message:"Merci de renseigner le message",
    classeEtab:"Merci de selectionner au moins une classe",
    destinataires:"Merci de selectionner au moins un destinataire",
    semestre:"Merci de selectionner une valeur",
    datefin:"Merci de renseigner la date de début",
    datedeb:"Merci de renseigner la date de fin",
  },
  submitHandler: function(form) {

//nous allins verifier si cette session existe deja

var etape=1;
var codeEtab="<?php echo $codeEtabAssigner; ?>";
var titre=$("#titre").val();

$.ajax({
         url: '../ajax/sessions.php',
         type: 'POST',
         async:false,
         data: 'codeEtab='+codeEtab+'&etape='+etape+'&titre='+titre,
         dataType: 'text',
         success: function (content, statut) {


          if(content==0)
          {
            form.submit();
          }else if(content==1)
          {
            Swal.fire({
            type: 'warning',
            title: '<?php echo L::WarningLib ?>',
            text: 'Cette Session Académique existe dejà pour votre etablissement',

            })
          }
         }
       });

//
  }


});


   });

   </script>
    <!-- end js include path -->
  </body>

</html>
