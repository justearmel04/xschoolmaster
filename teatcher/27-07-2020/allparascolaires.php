<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Teatcher.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();




$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$matiere=new Matiere();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$localadmins= new Localadmin();
$parents=new ParentX();
$classe=new Classe();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$_SESSION['user']['codeEtab']=$codeEtabsession;

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $_SESSION['user']['session']=$libellesessionencours;
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];


  $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
}else {
  $nbclasse=0;
}

$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);



if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();

$parascos=$etabs->getAllParascolairesActivityOfThisSchool($codeEtabsession,$libellesessionencours);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
  <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />

    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::ParascolairesActivitiesLists ?> </div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="#"><?php echo L::parascoMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::ParascolairesActivitiesLists ?></li>
                          </ol>
                      </div>
                  </div>

                  <?php

                        if(isset($_SESSION['user']['updateteaok']))
                        {

                          ?>
                          <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?php
                        //echo $_SESSION['user']['addetabok'];
                        ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                           </a>
                        </div-->
                <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                <script src="../assets/js/sweetalert2.min.js"></script>

                    <script>
                    Swal.fire({
                    type: 'success',
                    title: 'Félicitations',
                    text: '<?php echo $_SESSION['user']['updateteaok']; ?>',

                    })
                    </script>
                          <?php
                          unset($_SESSION['user']['updateteaok']);
                        }

                         ?>
                  <!-- end widget -->
                  <?php

                        if(isset($_SESSION['user']['addetabexist']))
                        {

                          ?>
                          <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <?php
                        echo $_SESSION['user']['addetabexist'];
                        ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                           </a>
                        </div>



                          <?php
                          unset($_SESSION['user']['addetabexist']);
                        }

                         ?>


                         <div class="row">
                           <div class="col-md-12 col-sm-12">
                                                     <div class="card card-box">
                                                         <div class="card-head">
                                                             <header><?php echo L::Seacher ?></header>

                                                         </div>
                                                         <div class="card-body " id="bar-parent">
                                                           <form method="post" id="FormSearch">
                                                               <div class="row">
                                                                 <div class="col-md-6 col-sm-6">
                                                                 <!-- text input -->
                                                                 <div class="form-group">
                                                                     <label><?php echo L::ActivitiesDate ?></label>
                                                                     <input type="text" id="dateactivite" name="dateactivite" class="form-control" placeholder="<?php echo L::ActivitiesDate ?>"/>

                                                                 </div>
                                                                 <div class="form-group">
                                                                     <label><?php echo L::ActivitiesCosts ?></label>
                                                                     <input type="text" id="coutactivite" name="coutactivite" class="form-control" placeholder="<?php echo L::ActivitiesCosts ?>"/>

                                                                 </div>

                                                             </div>
                                                             <div class="col-md-6 col-sm-6">
                                                             <!-- text input -->
                                                             <div class="form-group">
                                                                 <label><?php echo L::ActivitiesRespos ?></label>
                                                                 <input type="text" id="respoactivite" name="respoactivite" class="form-control" placeholder="<?php echo L::ActivitiesRespos ?>"/>

                                                                 <input type="hidden" name="search" id="search"/>
                                                             </div>


                                                         </div>
                                                               </div>

                                                               <button type="submit" class="btn btn-danger"><?php echo L::Seacher ?></button>
                                                           </form>
                                                         </div>
                                                     </div>
                                                 </div>
                         </div>

                         <br>
                         <div class="row">
                                                 <div class="col-md-12">
                                                     <div class="tabbable-line">
                                                        <!--ul class="nav nav-pills nav-pills-rose">
                         									<li class="nav-item tab-all"><a class="nav-link active show"
                         										href="#tab1" data-toggle="tab">Liste</a></li>
                         									<li class="nav-item tab-all"><a class="nav-link" href="#tab2"
                         										data-toggle="tab">Grille</a></li>
                         								</ul-->

                         <?php
                           if(isset($_POST['search']))
                           {
                             if(isset($_POST['dateactivite']))
                             {
                               if(isset($_POST['coutactivite']))
                               {
                                 if(isset($_POST['respoactivite']))
                                 {
                                     //dateactivite,coutactivite,responsableactivite
                                 }else
                                 {
                                   //dateactivite,coutactivite
                                 }
                               }else {
                                 if(isset($_POST['respoactivite']))
                                 {
                                     //dateactivite,responsableactivite
                                 }else
                                 {
                                   //dateactivite
                                 }
                               }
                             }else {

                               if(isset($_POST['coutactivite']))
                               {
                                 if(isset($_POST['respoactivite']))
                                 {
                                     //coutactivite,responsableactivite
                                 }else
                                 {
                                   //coutactivite
                                 }
                               }else {
                                 if(isset($_POST['respoactivite']))
                                 {
                                     //responsableactivite
                                 }
                               }


                             }
                           }

                          ?>










                         <div class="row">
                           <?php
                           $i=0;
                           foreach ($parascos as $value):
                            ?>
                         									<div class="col-md-3">
                         										<div class="card">
                         											<div class="card-head card-topline-aqua">
                         												<!-- <header>User Activity</header> -->
                         											</div>
                         											<div class="card-body no-padding height-9" style="height:40%">
                         												<div class="container-fluid">

                         													<div class="row">
                         														<div class="col-md-12">
                         															<!-- <ul class="activity-list"> -->

                         																<!-- <li> -->
                         																	<div class="post-box">
                                                             <!-- <span
                         																			class="text-muted text-small"><i
                         																				class="fa fa-clock-o"
                         																				aria-hidden="true"></i>
                         																			13 minutes ago</span> -->
                         																		<div class="post-img"><img
                         																				src="../assets/img/slider/fullimage1.jpg"
                         																				class="img-responsive" alt=""></div>
                         																		<!-- <div> -->
                         																		<span style="text-align:center"><h5 class=""><?php echo $value->libelle_act;?></h5></span>

                                                                 <ul class="list-group list-group-unbordered">
                         											<li class="list-group-item">
                         												<b><?php echo L::DatedebLib ?></b> <a class="pull-right"><?php echo date_format(date_create($value->datedeb_act), "d-m-Y")." ".$value->heuredeb_act;?></a>
                         											</li>
                         											<li class="list-group-item">
                         												<b><?php echo L::DatefinLib ?></b> <a class="pull-right"><?php echo date_format(date_create($value->datefin_act), "d-m-Y")." ".$value->heurefin_act; ?></a>
                         											</li>
                         											<li class="list-group-item">
                         												<b><?php echo L::CostAvtivity ?></b> <a class="pull-right"><?php
                                                 $montantAct=$value->montant_act;
                                                 if($montantAct==0)
                                                 {
                                                   ?>
                         <span class="label label-sm label-info" style=""> <?php echo L::GratuitAvtivity ?></span>
                                                   <?php
                                                 }else if($montantAct!=0)
                                                 {
                                                   ?>
                         <span class="label label-sm label-info" style=""> <?php echo $montantAct; ?> </span>
                                                   <?php
                                                 }
                                                  ?></a>
                         											</li>
                                               <li class="list-group-item">
                                                 <p>  <?php
                                                   $dataclasses=$value->classes_act;
                                                   $tabclasse=explode("-",$dataclasses);
                                                   $nbclasse=count($tabclasse)-1;
                                                   for($z=0;$z<$nbclasse;$z++)
                                                   {
                                                     ?>
                                                       <span class="label label-sm label-success" style="text-align:center"> <?php
                                                       echo $classe->getInfosofclassesbyId($tabclasse[$z],$libellesessionencours);
                                                       //echo $tabdataclasse[$z];
                                                       ?> </span>
                                                     <?php
                                                   }
                                                   ?></p>
                                               </li>
                         										</ul>
                                             <br>




                         																		<!-- </div> -->
                         																	</div>

                         																<!-- </li> -->

                         															<!-- </ul> -->
                         														</div>
                         													</div>
                         												</div>
                         											</div>
                         										</div>
                         									</div>
                                           <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                           <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
                                           <!-- <script src="../assets2/plugins/select2/js/select2.js" ></script>
                                           <script src="../assets2/js/pages/select2/select2-init.js" ></script> -->
                                           <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
                                           <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                           <script>

                                           function archived(id,notifid)
                                           {
                                             var session="<?php echo $libellesessionencours; ?>";
                                             var codeEtab="<?php echo $codeEtabLocal; ?>";
                                             Swal.fire({
                               title: '<?php echo L::WarningLib ?>',
                               text: "<?php echo L::DoyouReallyArchivedNotifications ?>",
                               type: 'warning',
                               showCancelButton: true,
                               confirmButtonColor: '#2CA8FF',
                               cancelButtonColor: '#d33',
                               confirmButtonText: '<?php echo L::Archived ?>',
                               cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                             }).then((result) => {
                               if (result.value) {
                                  // document.location.href="../controller/parascolaire.php?etape=5&paraid="+id+'&notifid='+notifid+'&session='+session+'&codeEtab='+codeEtab;
                                  document.location.href="../controller/parascolaires.php?etape=5&paraid="+id+'&notifid='+notifid+'&session='+session+'&codeEtab='+codeEtab;

                               }else {

                               }
                             })
                                           }

                                           function sender(id,notifid)
                                           {
                                             var sessionEtab="<?php echo $libellesessionencours  ?>";
                                             var etape1=4;
                                             $.ajax({
                                               url: '../ajax/message.php',
                                               type: 'POST',
                                               async:false,
                                               data: 'notifid='+notifid+'&etape='+etape1,
                                               dataType: 'text',
                                                 success: function (content, statut) {


                                                   var tabcontent=content.split("*");
                                                   var destinataires=tabcontent[0];
                                                   var classes=tabcontent[1];
                                                   var codeEtab=tabcontent[2];
                                                   var smssender=tabcontent[3];
                                                   var emailsender=tabcontent[4];
                                                   var precis=tabcontent[5];
                                                   var eleves=tabcontent[6];
                                                   var etape2=5;

                                                   Swal.fire({
                                     title: '<?php echo L::WarningLib ?>',
                                     text: "<?php echo L::DoyouReallySendingNotifications ?>",
                                     type: 'warning',
                                     showCancelButton: true,
                                     confirmButtonColor: '#2CA8FF',
                                     cancelButtonColor: '#d33',
                                     confirmButtonText: '<?php echo L::Sendbutton ?>',
                                     cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                   }).then((result) => {
                                     if (result.value) {
                                       // document.location.href="../controller/messages/etape=3&notifid="+notificationid+"&destinataires="+destinataires+"&classes="+classes;
                                       $.ajax({
                                         url: '../ajax/message.php',
                                         type: 'POST',
                                         async:false,
                                         data: 'notifid='+id+'&etape='+etape2+'&codeEtab='+codeEtab+'&destinataires='+destinataires+'&classes='+classes+'&smssender='+smssender+'&emailsender='+emailsender+'&sessionEtab='+sessionEtab+'&precis='+precis+'&eleves='+eleves,
                                         dataType: 'text',
                                           success: function (content, statut) {

                                             var tab=content.split("/");
                                             var destimails=tab[0];
                                             var destiphones=tab[1];

                            // document.location.href="../controller/messages.php?etape=3&notifid="+notifid+"&destimails="+destimails+"&destiphones="+destiphones+"&codeEtab="+codeEtab+"&smssender="+smssender+"&emailsender="+emailsender+"&joinfile="+joinfile+"&file="+file+"&paraid="+id;

                            //ce que j'ai commenter en bas
                            document.location.href="../controller/parascolaires.php?etape=4&notifid="+notifid+"&destimails="+destimails+"&destiphones="+destiphones+"&codeEtab="+codeEtab+"&smssender="+smssender+"&emailsender="+emailsender+"&precis="+precis+"&eleves="+eleves+"&paraid="+id;


                                           }
                                       });
                                     }else {

                                     }
                                   })

                                                 }
                                             });
                                           }

                                           function modify(id)
                                           {


                                             Swal.fire({
                               title: '<?php echo L::WarningLib ?>',
                               text: "<?php echo L::DoyouReallyModifyingSubjects ?>",
                               type: 'warning',
                               showCancelButton: true,
                               confirmButtonColor: '#3085d6',
                               cancelButtonColor: '#d33',
                               confirmButtonText: '<?php echo L::ModifierBtn ?>',
                               cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                             }).then((result) => {
                               if (result.value) {
                                 document.location.href="updatesubject.php?compte="+id;
                               }else {

                               }
                             })
                                           }

                                           function deleted(id,notifid)
                                           {
                                             var sessionEtab="<?php echo $libellesessionencours  ?>";
                                             var codeEtab="<?php echo $codeEtabAssigner;?>";
                                             Swal.fire({
                               title: '<?php echo L::WarningLib ?>',
                               text: "<?php echo L::DoyouReallydeletingThisParascoActivity ?>",
                               type: 'warning',
                               showCancelButton: true,
                               confirmButtonColor: '#3085d6',
                               cancelButtonColor: '#d33',
                               confirmButtonText: '<?php echo L::DeleteLib ?>',
                               cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                             }).then((result) => {
                               if (result.value) {
                                 document.location.href="../controller/parascolaires.php?etape=3&paraid="+id+"&codeEtab="+codeEtab+"&notifid="+notifid+'&session='+sessionEtab;
                               }else {

                               }
                             })
                                           }

                                           </script>
                                           <?php
                                           $i++;
                                         endforeach;
                                            ?>


                         								</div>


                                     <div class="row">
                                       <?php
                                       if((count($parascos)>0)&&($i>9))
                                       {
                                         ?>
                                         <div class="col-12 col-md-12 col-lg-12">

                                           <div class="card">

                                             <div class="card-body">
                                               <nav aria-label="...">
                                                 <ul class="pagination">
                                                   <li class="page-item disabled">
                                                     <a class="page-link" href="#" tabindex="-1">Previous</a>
                                                   </li>
                                                   <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                   <li class="page-item active">
                                                     <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                                   </li>
                                                   <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                   <li class="page-item">
                                                     <a class="page-link" href="#">Next</a>
                                                   </li>
                                                 </ul>
                                               </nav>
                                             </div>
                                           </div>
                                         </div>
                                         <?php
                                       }
                                        ?>


                                     </div>


                                                     </div>
                                                 </div>
                                             </div>


                                             <div class="modal fade" id="mediumModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                           					    <div class="modal-dialog modal-lg" role="document">
                                           					        <div class="modal-content">
                                           					            <div class="modal-header">
                                           					                <h4 class="modal-title" id="exampleModalLabel"><?php echo L::DetailsActivities ?></h4>
                                           					                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                           					                    <span aria-hidden="true">&times;</span>
                                           					                </button>
                                           					            </div>
                                           					            <div class="modal-body">
                                                                   <form action="#" id="FormDiplAdd" method="post" >

                                                                     <div class="row">

                                                                       <div class="col-md-6">
                                                                            <div class="form-group">
                                                                              <label for=""><b><?php echo L::Period ?> <span class="required"> * </span>: </b></label>
                                                                               <select class="form-control" id="typesess" name="typesess"  >

                                                                               </select>
                                                                             </div>


                                                                        </div>
                                                                        <div class="col-md-6">
                                                                             <div class="form-group">
                                                                               <label for=""><b><?php echo L::ParaActivityDenomination ?> <span class="required"> * </span>: </b></label>
                                                                                <input type="text" name="libelleactivity" id="libelleactivity" data-required="1" placeholder="<?php echo L::Enterparentprename ?>" class="form-control" />
                                                                              </div>


                                                                         </div>

                                                                       <div class="col-md-6">
                                                                     <div class="form-group">
                                                                     <label for=""><b><?php echo L::responsables ?><span class="required"> * </span>: </b></label>

                                                                   <input type="text" name="respoactivity" id="respoactivity" data-required="1" placeholder="<?php echo L::EnterName ?>" class="form-control" />
                                                                   <input type="hidden" name="parascoid" id="parascoid" value="">
                                                                   <input type="hidden" name="codeEtabparsco" id="codeEtabparsco" value="">
                                                                   <input type="hidden" name="sessionEtabparsco" id="sessionEtabparsco" value="">
                                                                     </div>

                                                                     </div>
                                                                     <div class="col-md-6">
                                                                          <div class="form-group">
                                                                            <label for=""><b><?php echo L::ContactRespoNametAvtivity ?> <span class="required"> * </span>: </b></label>
                                                                             <input type="text" name="contactrespoactivity" id="contactrespoactivity" data-required="1" placeholder="<?php echo L::Enterparentprename ?>" class="form-control" />
                                                                           </div>


                                                                      </div>
                                                                      <div class="col-md-6">
                                                                    <div class="form-group">
                                                                    <label for=""><b><?php echo L::Avtivitystaring ?><span class="required"> * </span>: </b></label>

                                                                  <input type="text" name="lieuactivity" id="lieuactivity" data-required="1" placeholder="<?php echo L::EnterName ?>" class="form-control" />
                                                                    </div>

                                                                    </div>
                                                                    <div class="col-md-6">
                                                                         <div class="form-group">
                                                                           <label for=""><b><?php echo L::ParaActivityType ?> <span class="required"> * </span>: </b></label>
                                                                            <select class="form-control" id="typeactivity" name="typeactivity" onchange="checkobjet()">

                                                                            </select>
                                                                          </div>


                                                                     </div>

                                                                     <div class="col-md-6" id="otherrow">
                                                                          <div class="form-group">
                                                                            <label for=""><b><?php echo L::OtherActivity ?> <span class="required"> * </span>: </b></label>
                                                                              <input type="text" name="otheractivity" id="otheractivity" data-required="1" placeholder="<?php echo L::PrecisyAvtivityType ?>" class="form-control" />
                                                                           </div>


                                                                      </div>

                                                                      <div class="col-md-6">
                                                                           <div class="form-group">
                                                                             <label for=""><b><?php echo L::DestinatairesNotifications ?> <span class="required"> * </span>: </b></label>
                                                                              <select class="form-control" id="destinataires" name="destinataires" onchange="determinedestinataires()" >
                                                                                <option value="" > <?php echo L::SelectDestinatairesNotifications ?> </option>
                                                                                <option value="1"><?php echo L::Alls ?></option>
                                                                                <option value="2"><?php echo L::PrecisNotifications ?></option>

                                                                              </select>
                                                                              <p class="required col-md-12" style="font-size:10px"><?php echo L::TousInfosPreNotifications ?> <br> <?php echo L::TousInfosPostNotifications ?> </p>
                                                                            </div>


                                                                       </div>

                                                                       <div class="col-md-6">
                                                                            <div class="form-group">
                                                                              <label for=""><b><?php echo L::Preciser ?> <span class="required"> * </span>: </b></label>
                                                                              <select class="form-control input-height" multiple="multiple" id="eleves" name="eleves[]" style="width:100%" >
                                                                                 <option value="0" selected>Tous</option>

                                                                             </select>

                                                                             </div>


                                                                        </div>

                                                                     <div class="col-md-12">
                                                                          <div class="form-group">
                                                                            <label for=""><b><?php echo L::Avtivitydescription ?> <span class="required"> * </span>: </b></label>
                                                                              <textarea class="form-control" name="descripactivite" id="descripactivite" rows="5" cols="50" maxlength="160" placeholder="<?php echo L::DescribeAvtivity ?>"></textarea>
                                                                           </div>


                                                                      </div>


                                                                                   </div>

                                                                     </form>
                                           					            </div>

                                           					        </div>
                                           					    </div>
                                           					</div>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
 <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
 <script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
 <script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   // function classeliste()
   // {
   //   //listeEnseignant.php?$codeEtabAssigner=<?php //echo $codeEtabAssigner?>
   //
   //   var url="listeClasse.php?$idClasse=<?php //echo $idClasse?>& $codeEtabAssigner=<?php //echo $codeEtabAssigner?>";
   //
   //   window.open(url, '_blank');
   // }

   function checkobjet()
   {
     var objet=$("#typeactivity").val();
     var tabobjet=objet.split("-");

     var objetid=tabobjet[0];

     if(objetid==6)
     {
         $("#FormDiplAdd #otherrow").show();
     }else {
       $("#FormDiplAdd #otherrow").hide();
     }

   }

   $("#typeactivity").select();
   $("#typesess").select();
   $("#destinataires").select();
   $("#eleves").select2({
     tags: true,
   tokenSeparators: [',', ' ']
   });



   function checkParascolaires(paraid,codeEtab,sessionEtab)
   {
     $("#parascoid").val(paraid);
     $("#codeEtabparsco").val(codeEtab);
     $("#sessionEtabparsco").val(sessionEtab);




   }


   $('#mediumModel').on('shown.bs.modal', function () {

     var parascoid=$("#FormDiplAdd #parascoid").val();
     var codeEtab=$("#FormDiplAdd #codeEtabparsco").val();
     var sessionEtab=$("#FormDiplAdd #sessionEtabparsco").val();
     var etape=1;

         $.ajax({
           url: '../ajax/parascolaires.php',
      type: 'POST',
      async:true,
      data: 'etape=' + etape+'&parascoid='+parascoid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab,
      dataType: 'text',
      success: function (content, statut) {


        $("#FormDiplAdd #respoactivity").val(content.split("*")[0]);
        $("#FormDiplAdd #contactrespoactivity").val(content.split("*")[1]);
        $("#FormDiplAdd #lieuactivity").val(content.split("*")[2]);

        $("#FormDiplAdd #datedebactivity").val(content.split("*")[3]);
        $("#FormDiplAdd #descripactivite").val(content.split("*")[4]);

        $("#FormDiplAdd #typeactivity").html(content.split("*")[6]);

        if(content.split("*")[3]==6)
        {
          $("#FormDiplAdd #otherrow").show();

        }else {
          $("#FormDiplAdd #otherrow").hide();
        }

        $("#FormDiplAdd #typesess").html(content.split("*")[7]);
        $("#FormDiplAdd #libelleactivity").val(content.split("*")[8]);


        if(content.split("*")[9]==0)
        {
          $('#destinataires option[value="1"]').prop('selected', true);
        }else
        {
          $('#destinataires option[value="2"]').prop('selected', true);
        }





      }
         });

   });


   $('#dateactivite').bootstrapMaterialDatePicker
  ({
   date: true,
   shortTime: false,
   time: false,
   format: 'YYYY-MM-DD',
   lang: 'fr',
  cancelText: '<?php echo L::AnnulerBtn ?>',
  okText: '<?php echo L::Okay ?>',
  clearText: '<?php echo L::Eraser ?>',
  nowText: '<?php echo L::Now ?>',
  switchOnClick : true

  });

   function searchlibetab()
   {

     var etape=2;

     $.ajax({
              url: '../ajax/school.php',
              type: 'POST',
              async:false,
              data: 'code='+ $("#codeetab").val()+'&etape='+etape,
              dataType: 'text',
              success: function (content, statut) {

                $("#libetab").html("");
                $("#libetab").html(content);

                //nous allons chercher la liste des admins local de cet etablissement

                $.ajax({
                         url: '../ajax/classe.php',
                         type: 'POST',
                         async:false,
                         data: 'code='+ $("#codeetab").val()+'&etape='+etape,
                         dataType: 'text',
                         success: function (response, statut) {


                           $("#classex").html("");
                           $("#classex").html(response);
                         }
                       });

              }

            });

   }
   $(document).ready(function() {

     $('#example5').DataTable( {
         "scrollX": true

     } );

     $("#codeetab").select2();

     $("#libetab").select2();

   });

$("#FormSearch").validate({


       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
     },
     highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
     },
     success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },
  rules:{
    dateactivite:{
      required:function(element){
              return $("#coutactivite").val()=="" && $("#respoactivite").val()=="" && $("#dateactivite").val()=="";
          }
    }
      },
      messages:{
dateactivite:"<?php echo L::PleaserenseignerActivitydate ?>"
      }


});




   </script>
    <!-- end js include path -->
  </body>

</html>
