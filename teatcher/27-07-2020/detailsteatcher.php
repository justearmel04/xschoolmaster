<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/Teatcher.php');
require_once('../class/Diplome.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();
$classe=new Classe();
$diplome=new Diplome();
$user=new User();
$etabs=new Etab();
$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$teatcher=new Teatcher();

$etabEnseigner=$etabs->getCodeEtabOfEnseignerId($_SESSION['user']['IdCompte']);

$detailsTeach=$teatcher->getTeatcherInfobyId($_GET['compte']);

$tabteatcher=explode("*",$detailsTeach);


$diplomes=$diplome->getDiplomebyTeatcherId($_GET['compte']);

$classesTeatch=$classe->getClassesAndMatiereOfThisTeatcherSchool($etabEnseigner,$_GET['compte']);


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title">Détails Professeur</div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="#"><?php echo L::ProfsMenu?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active">Détails Professeur</li>
                          </ol>
                      </div>
                  </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


                 <div class="row">
                   <div class="col-sm-12">
                 <div class="card-box">
                   <div class="card-head">
                     <header></header>
                   </div>
                   <div class="card-body ">
                         <div class = "mdl-tabs mdl-js-tabs">
                            <div class = "mdl-tabs__tab-bar tab-left-side">
                               <a href = "#tab4-panel" class = "mdl-tabs__tab is-active"><?php echo L::TeatcherInfosPerso?></a>
                               <a href = "#tab5-panel" class = "mdl-tabs__tab">Classes et matieres</a>


                            </div>
                            <div class = "mdl-tabs__panel is-active p-t-20" id = "tab4-panel">
                              <div class="col-md-12 col-sm-12">
                                  <div class="card card-box">
                                      <div class="card-head">
                                          <header></header>

                                      </div>

                                      <div class="card-body" id="bar-parent">
                                          <form  id="FormAddLocalAd" class="form-horizontal" action="../controller/teatcher.php" method="post" enctype="multipart/form-data">
                                              <div class="form-body">
                                                <div class="row">
                                <div class="col-md-12">

            <fieldset style="background-color:#007bff;">

            <center><legend style="color:white;"><?php echo L::TeatcherInfosPerso?></legend></center>
            </fieldset>


            </div>
                              </div>
                              <br/>
                              <div class="row">
                              <div class="col-md-6">
                              <div class="form-group">
                                <?php
                                if(strlen($tabteatcher[9]))
                                {
                                  $lien="../photo/".$tabteatcher[8]."/".$tabteatcher[10];
                                }else {
                                  $lien="../photo/user5.jpg";
                                }
                                ?>
                              <input disabled="disabled" type="file" id="photoTea" name="photoTea" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="<?php echo $lien; ?>" data-allowed-file-extensions="gif png jpg jpeg pjpeg" />
                              <label for=""><b><?php echo L::Image?><span class="required"> * </span>: </b></label>
                              </div>

                              </div>


                                            </div>
                              <div class="row">
                                                  <div class="col-md-6">
                              <div class="form-group">
                              <label for=""><b><?php echo L::Name?><span class="required"> * </span>: </b></label>

                            <input type="text" readonly name="nomTea" id="nomTea" value="<?php echo $tabteatcher[1];?>" data-required="1" placeholder="<?php echo L::EnterName ?>" class="form-control input-height" />
                              </div>

                              </div>
                              <div class="col-md-6">
                                   <div class="form-group">
                                     <label for=""><b><?php echo L::PreName?> : </b></label>
                                      <input type="text" readonly name="prenomTea" id="prenomTea" value="<?php echo $tabteatcher[2];?>" data-required="1" placeholder="<?php echo L::Enterparentprename ?>" class="form-control input-height" />
                                    </div>


                               </div>

                                            </div>
                                            <div class="row">
                                                                <div class="col-md-6">
                                            <div class="form-group">
                                            <label for=""><b>Date de naissance<span class="required"> * </span>: </b></label>

                                            <input type="text" readonly placeholder="Entrer la date de naissance" value="<?php  echo date_format(date_create($tabteatcher[3]),"d/m/Y");?>" name="datenaisTea" id="datenaisTea" data-mask="99/99/9999" class="form-control input-height">
                                              <span class="help-block"><?php echo L::Datesymbole ?></span>
                                            </div>

                                            </div>
                                            <div class="col-md-6">
                                                 <div class="form-group">
                                                   <label for=""><b><?php echo L::BirthLocation?>: </b></label>
                                                    <input type="text" readonly name="lieunaisTea" id="lieunaisTea" data-required="1" value="<?php echo $tabteatcher[11];?>" placeholder="Lieu de naissance" class="form-control input-height" />
                                                  </div>


                                             </div>

                                                          </div>
                                                          <div class="row">
                                                                              <div class="col-md-6">
                                                          <div class="form-group">
                                                          <label for=""><b>Genre <span class="required"> * </span>: </b></label>

                                                          <select class="form-control input-height" readonly name="sexeTea" id="sexeTea">

                                                              <option <?php if($tabteatcher[4]=="M"){echo "selected";} ?> value="M">Masculin</option>
                                                              <option <?php if($tabteatcher[4]=="F"){echo "selected";} ?>  value="F">Feminin</option>
                                                          </select>
                                                          </div>

                                                          </div>
                                                          <div class="col-md-6">
                                                            <div class="form-group">
                                                            <label for=""><b>Email <span class="required"> * </span>: </b></label>

                                                          <input type="text" readonly class="form-control input-height" value="<?php echo $tabteatcher[8];?>"  name="emailTea" id="emailTea" placeholder="Entrer l'adresse email">
                                                            </div>


                                                           </div>

                                                                        </div>
                                                                        <div class="row">
                                                                          <div class="col-md-6">
                                                      <div class="form-group">
                                                      <label for=""><b>Nationalité <span class="required"> * </span>: </b></label>

                                                <input name="natTea" id="natTea" readonly type="text" placeholder="Entrer la nationalité " value="<?php echo $tabteatcher[6];?>" class="form-control input-height" />
                                                      </div>

                                                      </div>
                                                                          <div class="col-md-6">
                                                                               <div class="form-group">
                                                                                 <label for=""><b><?php echo L::PhonestudentTab?> </b></label>
                                                                                <input readonly name="contactTea" id="contactTea" value="<?php echo $tabteatcher[5];?>" type="text" placeholder="Entrer le contact " class="form-control input-height" />
                                                                                </div>


                                                                           </div>



                                                                                      </div>
                                                                                      <div class="row">
                                                                                                          <div class="col-md-6">
                                                                                      <div class="form-group">
                                                                                      <label for=""><b>Situation Matrimoniale <span class="required"> * </span>: </b></label>

                                                                                      <select readonly class="form-control input-height" name="situationTea" id="situationTea">

                                                                                          <option  value=""><?php echo L::SelectMatrimonialeSituation?></option>
                                                                                          <option <?php if($tabteatcher[12]=="CELIBATAIRE"){echo "selected";}?> value="CELIBATAIRE">CELIBATAIRE</option>
                                                                                          <option <?php if($tabteatcher[12]=="MARIE(E)"){echo "selected";}?> value="MARIE(E)">MARIE(E)</option>
                                                                                          <option <?php if($tabteatcher[12]=="DIVORCE(E)"){echo "selected";}?> value="DIVORCE(E)">DIVORCE(E)</option>
                                                                                          <option <?php if($tabteatcher[12]=="VEUF(VE)"){echo "selected";}?> value="VEUF(VE)">VEUF(VE)</option>
                                                                                      </select>
                                                                                      </div>

                                                                                      </div>
                                                                                      <div class="col-md-6">
                                                                                           <div class="form-group">
                                                                                             <label for=""><b><?php echo L::ChildNumber?></b></label>
                                                                                            <input readonly name="nbchildTea" id="nbchildTea" value="<?php echo $tabteatcher[13];?>" type="number" min="0" placeholder="Entrer le nombre d'enfant " class="form-control input-height" />
                                                                                            </div>


                                                                                       </div>

                                                                                                    </div>



                                        </div>
                                    </form>
                                      </div>
                                  </div>
                              </div>
                            </div>
                            <div class = "mdl-tabs__panel p-t-20" id = "tab5-panel">
                              <div class="col-md-12 col-sm-12">
                                  <div class="card card-box">
                                      <div class="card-head">
                                          <header></header>

                                      </div>

                                      <div class="card-body" id="bar-parent">
                                          <form  id="FormAddLocalAd" class="form-horizontal" action="../controller/teatcher.php" method="post" enctype="multipart/form-data">
                                              <div class="form-body">
                                                     <div class="row">
                                                                                    <div class="col-md-12">

                                                                          <fieldset style="background-color:#007bff;">

                                                                          <center><legend style="color:white;">CLASSES ET MATIERES</legend></center>
                                                                          </fieldset>


                                                                          </div>
                                                      </div><br/>
                                                      <div class="row">
                                                        <div class="table-responsive">
                                           <table class="table table-striped custom-table table-hover">
                                               <thead>
                                                   <tr>
                                                       <th>Classes</th>
                                                       <th>Matières</th>

                                                   </tr>
                                               </thead>
                                               <tbody>
                                                 <?php
                                                 $i=1;
                                                 foreach ($classesTeatch as $value):

                                                  ?>
                                                   <tr>
                                                       <td><a href="#"><?php echo $value->libelle_classe; ?></a>
                                                       </td>
                                                       <td ><?php echo $value->libelle_mat ?></td>

                                                   </tr>
                                                   <?php
                                                   $i++;
                                                   endforeach;

                                                    ?>


                                               </tbody>
                                           </table>
                                           </div>

                                                      </div>




                                        </div>
                                    </form>
                                      </div>
                                  </div>
                              </div>
                            </div>


                            </div>
                         </div>
                   </div>
                 </div>
               </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <script src="../assets2/js/pages/material_select/getmdl-select.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }
   $(document).ready(function() {



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
