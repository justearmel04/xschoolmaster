<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Student.php');
require_once('../class/Teatcher.php');
require_once('../controller/functions.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$matiere=new Matiere();
$student=new Student();
$teatcher=new Teatcher();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);

$matieres=$matiere->getAllSubjectTeatchByTeatcherId($_SESSION['user']['IdCompte']);
$controles=$matiere->getAllControleMatiereOfThisTeatcherId($_SESSION['user']['IdCompte']);


//$dataclasses=$classe->getAllClassesByClasseId($classeschoolid);

$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
  $programmes=$etabs->getAllprogrammesOfTeatcherClasses($_SESSION['user']['IdCompte'],$libellesessionencours);

  // $fiches=$etabs->getAllFicesOfTeatcherClasses($_SESSION['user']['IdCompte'],$libellesessionencours);

$fiches=$etabs->getAllcahiersOfteatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);

}else {
  $nbclasse=0;
}


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <!--link href="../assets2/plugins/summernote/summernote.css" rel="stylesheet"-->
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Cahier de texte</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li>&nbsp;<a class="parent-item" href="#">Gestion des Matières</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Cahier de texte</li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">
              <?php

                    if(isset($_SESSION['user']['addprogra']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
            <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['addprogra'] ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addprogra']);
                    }

                     ?>
					      </div>
						</div>
					<!-- end widget -->



          <div class="row">

            <div class="col-sm-12">
								<div class="card-box">
									<div class="card-head">
										<header></header>
									</div>
									<div class="card-body ">
						            <div class = "mdl-tabs mdl-js-tabs">
						               <div class = "mdl-tabs__tab-bar tab-left-side">
						                  <a href = "#tab4-panel" class = "mdl-tabs__tab is-active">Liste fiche</a>
						                  <a href = "#tab5-panel" class = "mdl-tabs__tab">Ajouter Fiche</a>

						               </div>
						               <div class = "mdl-tabs__panel is-active p-t-20" id = "tab4-panel">
                             <div class="row">
                        <div class="col-md-12">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header></header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">

                                    <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                        <thead>
                                            <tr>



                                                <th> Matière </th>
                                                <th> Classe </th>
                                                <th> Sections </th>
                                                <th> Taches </th>
                                                <!--th> Actions </th-->
                                            </tr>
                                        </thead>
                                        <?php
                                        if($nbsessionOn>0)
                                        {
                                         ?>
                                        <tbody>
                                          <?php
                                            $i=1;
                                            foreach ($fiches as $value):

                                             ?>

                                            <tr class="odd gradeX" ondblclick="detailsfiche(<?php echo $value->id_cahier ?>,<?php echo $value->teatcher_cahier  ?>,'<?php echo $value->codeEtab_cahier  ?>')">
                                              <td><?php echo $value->libelle_classe;?></td>
                                              <td><?php echo $value->libelle_mat;?></td>
                                              <td>
                                                 <ul>
                                                <?php
                                                //determiner la liste des sections
                                                 $sectionfiches=$etabs->getAllsectionsOfThisCahier($value->id_cahier,$value->session_cahier,$value->codeEtab_cahier,$value->matiere_cahier,$value->teatcher_cahier);

                                                 foreach ($sectionfiches as $valuesection):
                                                   ?>

                                                   <li><?php echo $valuesection->libelle_secahier; ?></li>

                                                   <?php
                                                 endforeach;

                                                 ?>
                                                  </ul>
                                              </td>
                                              <td>
                                                <ul>
                                                  <?php
                                                  $tachesfiches=$etabs->getAlltachesOfThisCahier($value->id_cahier,$value->session_cahier,$value->codeEtab_cahier,$value->matiere_cahier,$value->teatcher_cahier);

                                                  foreach ($tachesfiches as $valuetaches):
                                                    ?>
                                                    <li><?php echo $valuetaches->libelle_tacheca; ?></li>
                                                    <?php
                                                  endforeach;

                                                   ?>
                                                </ul>
                                              </td>
                                              <!--td></td-->
                                            </tr>
                                            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                            <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                            <script type="text/javascript">

                                            function detailsfiche(fiche,teatcher,codeEtab)
                                            {
                                              document.location.href="detailfiches.php?fiche="+fiche+"&teatcher="+teatcher+"&codeEtab="+codeEtab
                                            }

                                            function deleted(id)
                                            {

                                              Swal.fire({
                                title: 'Attention !',
                                text: "Voulez vous vraiment supprimer ce programme",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Supprimer',
                                cancelButtonText: 'Annuler',
                              }).then((result) => {
                                if (result.value) {
                                  document.location.href="../controller/programme.php?etape=3&programme="+id;
                                }else {

                                }
                              })
                                            }
                                            </script>
                                            <?php
                                            $i++;
                                          endforeach;
                                             ?>

                                        </tbody>
                                        <?php
                                      }
                                         ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
						               </div>
						               <div class = "mdl-tabs__panel p-t-20" id = "tab5-panel">
                             <div class="row">
                                 <div class="col-md-12 col-sm-12">
                                     <div class="card card-box">
                                         <div class="card-head">
                                             <header></header>

                                         </div>

                                         <?php
                                         if($nbsessionOn>0)
                                         {
                                          ?>

                                         <div class="card-body" id="bar-parent">
                                             <form  id="FormAddAcademique" class="form-horizontal" action="../controller/cahier.php" method="post" >
                                                 <div class="form-body">
                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3">Session
                                                           <span class="required">  *</span>
                                                       </label>
                                                       <div class="col-md-5">
                                                         <input type="text" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>" class="form-control" readonly>
                                                       </div>

                                                   </div>
                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3">Enseignant
                                                           <span class="required">  *</span>
                                                       </label>
                                                       <div class="col-md-5">
                                                         <input type="text" name="enseignant" id="enseignant" value="<?php echo $_SESSION['user']['nom']." ".$_SESSION['user']['prenoms']; ?>" class="form-control" readonly>
                                                       </div>

                                                   </div>
                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3">Classe
                                                           <span class="required"> * </span>
                                                       </label>
                                                       <div class="col-md-5">
                                                         <select class="form-control input-height" id="classeEtab" name="classeEtab" style="width:100%;" onchange="searchmatiere(<?php echo $_SESSION['user']['IdCompte']; ?>)" >
                                                             <option value="">Selectionner une classe</option>
                                                             <?php
                                                             $i=1;
                                                               foreach ($classes as $value):
                                                               ?>
                                                               <option  value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                               <?php
                                                                                                $i++;
                                                                                                endforeach;
                                                                                                ?>

                                                         </select>
                                                         <input type="hidden" name="etape" id="etape" value="1" />
                                                         <input type="hidden" id="codeEtab" name="codeEtab" value=""/>

                                                       </div>

                                                   </div>
                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3">Matière
                                                           <span class="required">  *</span>
                                                       </label>
                                                       <div class="col-md-5">
                                                         <select class="form-control input-height" id="matclasse" name="matclasse" style="width:100%;" onchange="searchcodeEtab(<?php echo $_SESSION['user']['IdCompte']; ?>)">
                                                             <option value="">Selectionner une matière</option>


                                                         </select>
                                                       </div>

                                                   </div>

                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3">Section abordées
                                                           <span class="required">  *</span>
                                                       </label>
                                                       <input type="hidden" id="nb" name="nb" value="0">
                                                       <input type="hidden" name="concatsection" id="concatsection" value="">
                                                       <input type="hidden" name="concatnbsection" id="concatnbsection" value="">

                                                       <div class="col-md-6">
                                                         <button type="button" name="add" id="add" class="btn btn-success "> <i class="fa fa-plus"></i>Nouvelle section</button>
                                                        </div>


                                                   </div>

                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3">
                                                           <span class="required"></span>
                                                       </label>
                                                       <div class="col-md-6">
                                                         <table class="table" id="dynamic_field" border=0>  </table>

                                                        </div>

                                                   </div>
                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3">A retenir
                                                           <span class="required">  *</span>
                                                       </label>
                                                       <div class="col-md-6">
                                                         <textarea class="form-control" rows="8" name="descri" id="descri" placeholder=""></textarea>

                                                       </div>

                                                   </div>
                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3">Travail et devoir à faire
                                                           <span class="required">  *</span>
                                                       </label>
                                                       <input type="hidden" id="nbtache" name="nbtache" value="0">
                                                       <input type="hidden" name="concattache" id="concattache" value="">
                                                       <input type="hidden" name="concatnbtache" id="concatnbtache" value="">

                                                       <div class="col-md-6">
                                                         <button type="button" name="addtache" id="addtache" class="btn btn-success "> <i class="fa fa-plus"></i>Nouvelle Tache</button>
                                                        </div>


                                                   </div>
                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3">
                                                           <span class="required"></span>
                                                       </label>
                                                       <div class="col-md-6">
                                                         <table class="table" id="dynamic_field1" border=0>  </table>

                                                        </div>

                                                   </div>
                                                   <!--div class="form-group row">
                                                       <label class="control-label col-md-3">Détails
                                                           <span class="required">  *</span>
                                                       </label>
                                                       <div class="col-md-6">
                                                         <div class="compose-editor">
 		                                                        <div id="summernote"></div>
 		                                                        <input type="file" class="default" multiple>
 		                                                    </div>
                                                       </div>

                                                   </div-->








                               <div class="form-actions">
                                                     <div class="row">
                                                         <div class="offset-md-3 col-md-9">
                                                             <button type="submit" class="btn btn-info">Enregistrer</button>
                                                             <button type="button" class="btn btn-danger">Annuler</button>
                                                         </div>
                                                       </div>
                                                    </div>
                             </div>
                                             </form>
                                         </div>
                                         <?php
                                       }
                                          ?>
                                     </div>
                                 </div>
                             </div>
						               </div>

						            </div>
									</div>
								</div>
							</div>

          </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
  <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
  <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
  <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
  <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
  <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
  <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
  <script src="../assets2/js/pages/table/table_data.js" ></script>
  <script src="../assets2/plugins/select2/js/select2.js" ></script>
  <script src="../assets2/js/pages/select2/select2-init.js" ></script>
  <!--script src="../assets2/plugins/summernote/summernote.js" ></script>
  <script src="../assets2/plugins/summernote/lang/summernote-fr-FR.js" ></script-->
     <!-- calendar -->
     <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
  <script src="../assets2/plugins/moment/moment.min.js" ></script>
  <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
  <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
  <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
  <script src="../assets2/js/dropify.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   /*$('#summernote').summernote({
       placeholder: '',
       tabsize: 2,
       height: 200,
        lang: 'fr-FR'
     });*/

     function addFrench()
     {
       var etape=1;
       var lang="fr";
       $.ajax({
         url: '../ajax/langue.php',
         type: 'POST',
         async:false,
         data: 'etape=' + etape+ '&lang=' +lang,
         dataType: 'text',
         success: function (content, statut) {

     window.location.reload();

         }
       });
     }

     function addEnglish()
     {
       var etape=1;
       var lang="en";
       $.ajax({
         url: '../ajax/langue.php',
         type: 'POST',
         async:false,
         data: 'etape=' + etape+ '&lang=' +lang,
         dataType: 'text',
         success: function (content, statut) {

     window.location.reload();

         }
       });
     }
   $("#fichier").dropify({
     messages: {
         "default": "Merci de selectionner le support",
         "replace": "Modifier le support",
         "remove" : "Supprimer le support",
         "error"  : "Erreur"
     }
   });

$("#matclasse").select2();
$("#classeEtab").select2();

function searchcodeEtab(id)
{
  var classe=$("#classeEtab").val();
  var teatcherId=id;
  var etape=7;
  var matiere=$("#matclasse").val();

  $.ajax({

       url: '../ajax/matiere.php',
       type: 'POST',
       async:true,
       data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
       dataType: 'text',
       success: function (content, statut) {

          $("#FormAddAcademique #codeEtab").val(content);

       }
     });

}

function searchmatiere(id)
{

    var classe=$("#classeEtab").val();
    var teatcherId=id;
    var etape=6;


  $.ajax({

       url: '../ajax/matiere.php',
       type: 'POST',
       async:true,
       data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe,
       dataType: 'text',
       success: function (content, statut) {


         $("#matclasse").html("");
         $("#matclasse").html(content);

       }
     });
}

function deletedTache(id)
{
  var concattache=$("#concattache").val();

  $("#concattache").val($("#concattache").val().replace(id+"@", ""));

   $('#rowTache'+id+'').remove();

   recalcultachenb();
}

function deletedSection(id)
{
  var concatsection=$("#concatsection").val();

  $("#concatsection").val($("#concatsection").val().replace(id+"@", ""));

   $('#rowSection'+id+'').remove();

   recalculsectionnb();
}

function recalcultachenb()
{
  var concattache=$("#concattache").val();

  var tab=concattache.split("@");

  var nbtab=tab.length;

  var nbtabnew=parseInt(nbtab)-1;

  $("#concatnbtache").val(nbtabnew);
}

function recalculsectionnb()
{
  var concatsection=$("#concatsection").val();

  var tab=concatsection.split("@");

  var nbtab=tab.length;

  var nbtabnew=parseInt(nbtab)-1;

  $("#concatnbsection").val(nbtabnew);
}

function AddsectionRow()
{
  var nb=$("#nb").val();
  var nouveau= parseInt(nb)+1;
  $("#nb").val(nouveau);

    var concatsection=$("#concatsection").val();
    $("#concatsection").val(concatsection+nouveau+"@");

    recalculsectionnb();

    $('#dynamic_field').append('<tr id="rowSection'+nouveau+'"><td><input type="text" name="section_'+nouveau+'" id="section_'+nouveau+'" placeholder="Entrer une section" class="form-control objectif_list" /></td><td><button type="button" id="deleteSection'+nouveau+'" id="deleteSection'+nouveau+'"  onclick="deletedSection('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');

    for(var i=1;i<=nouveau;i++)
    {
      $("#section_"+i).rules( "add", {
          required: true,
          messages: {
          required: "Champ obligatoire"
}
        });
    }


}

function AddtacheRow()
{
  var nb=$("#nbtache").val();
  var nouveau= parseInt(nb)+1;
  $("#nbtache").val(nouveau);

    var concattache=$("#concattache").val();
    $("#concattache").val(concattache+nouveau+"@");

    recalcultachenb();

    $('#dynamic_field1').append('<tr id="rowTache'+nouveau+'"><td><input type="text" name="tache_'+nouveau+'" id="tache_'+nouveau+'" placeholder="Entrer une tache" class="form-control objectif_list" /></td><td><button type="button" id="deleteTache'+nouveau+'" id="deleteTache'+nouveau+'"  onclick="deletedTache('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');

    for(var i=1;i<=nouveau;i++)
    {
      $("#tache_"+i).rules( "add", {
          required: true,
          messages: {
          required: "Champ obligatoire"
}
        });
    }


}

   $(document).ready(function() {

     $("#FormAddAcademique").validate({

       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
      },
      success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },
       rules:{

         programme:"required",
         descri:"required",
         classeEtab:"required",
         matclasse:"required",
         fichier:"required"

       },
     messages: {
       programme:"Merci de renseigner le libellé du programme",
       descri:"Merci de renseigner le détails de la fiche ",
       classeEtab:"Merci de selectionner une classe",
       matclasse:"Merci de selectionner une matière",
       fichier:"Merci de selectionner le fichier du programme"
     },
     submitHandler: function(form) {

     form.submit();



     }

     });

     AddsectionRow();

     $('#add').click(function(){

       //creation d'une ligne de section

       AddsectionRow();

     });

     $('#addtache').click(function(){

       //creation d'une ligne de section

       AddtacheRow();

     });






   });

   </script>
    <!-- end js include path -->
  </body>

</html>
