<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Student.php');
require_once('../class/Teatcher.php');
require_once('../controller/functions.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$matiere=new Matiere();
$student=new Student();
$teatcher=new Teatcher();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);

$matieres=$matiere->getAllSubjectTeatchByTeatcherId($_SESSION['user']['IdCompte']);
$controles=$matiere->getAllControleMatiereOfThisTeatcherId($_SESSION['user']['IdCompte']);


//$dataclasses=$classe->getAllClassesByClasseId($classeschoolid);

$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
  $programmes=$etabs->getAllprogrammesOfTeatcherClasses($_SESSION['user']['IdCompte'],$libellesessionencours);

  // $fiches=$etabs->getAllFicesOfTeatcherClasses($_SESSION['user']['IdCompte'],$libellesessionencours);

$fiches=$etabs->getAllcahiersOfteatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);

}else {
  $nbclasse=0;
}


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <style>
    #radioBtn .notActive{
    color: #3276b1;
    background-color: #fff;
    }
    #radioBtn2 .active{
    color: #f0f1f3;
    background-color: #28a745;

    }
    /* #radioBtn2 .notActive{
    color: #f0f1f3;
    background-color: #e8091e;
    }

     */
    </style>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title"><?php echo L::QuizzAdded ?></div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#"><?php echo L::QuizMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active"><?php echo L::QuizzAdded ?></li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addclasseok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: '<?php echo L::Felicitations ?>',
  text: "<?php echo $_SESSION['user']['addclasseok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::AddNews ?>',
  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="classes.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addclasseok']);
                    }

                     ?>

                     <?php
                     if($nbsessionOn==0)
                     {
                       ?>
                       <div class="alert alert-danger alert-dismissible fade show" role="alert">

                       <?php echo L::RequiredScolaireYear ?>

                       <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
                        </a>
                       </div>
                       <?php
                     }
                      ?>


              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <div class="card card-box">
                          <div class="card-head">
                              <header></header>

                          </div>

                          <div class="card-body" id="bar-parent">
                              <form  id="FormAddAcademique" class="form-horizontal" action="../controller/quizs.php" method="post"  >
                                  <div class="form-body">


                                    	<div class="row">
          <div class="col-md-12">
          <span class="label label-lg label-warning"  style="text-align:center;"> <i class="fa fa-info-circle"> </i> <?php echo L::QuizzInfos ?></span>
        </div></br></br>
                                          <div class="col-md-12">
                                            <div class="row">

              <div class="col-md-6">
  							<div class="form-group">
         <label for="classeEtab"><?php echo L::ClasseMenu ?><span class="required">*</span> :</label>
         <select class="form-control input-height" id="classeEtab" name="classeEtab" onchange="searchmatiere(<?php echo $_SESSION['user']['IdCompte']; ?>)">
           <option value=""><?php echo L::Selectclasses ?></option>
           <?php
           $i=1;
             foreach ($classes as $value):
             ?>
             <option  value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

             <?php
           $i++;
          endforeach;
            ?>
         </select>
                   </div>
  							 </div>


  									 <div class="col-md-6">
  	 									<div class="form-group">
  	 			<label for="matclasse"><?php echo L::MatiereMenusingle ?> <span class="required">*</span> :</label>
          <select class="form-control input-height" id="matclasse" name="matclasse" style="width:100%;" onchange="searchcodeEtab(<?php echo $_SESSION['user']['IdCompte']; ?>)">
              <option value=""><?php echo L::SelectSubjects ?></option>


          </select>
          <input type="hidden" name="etape" id="etape" value="1">
          <input type="hidden" name="codeEtab" name="codeEtab" value="<?php echo $codeEtabsession; ?>">
          <input type="hidden" name="sessionEtab" name="sessionEtab" value="<?php echo $libellesessionencours; ?>">
  	                      </div>
  	 									 </div>
                       <div class="col-md-6">
                        <div class="form-group">
            <label for="libellecourse"><?php echo L::QuizzLib ?><span class="required">*</span> :</label>
                   <input type="text" class="form-control" name="libellecourse" id="libellecourse" value="" size="32" maxlength="225" />

                            </div>
                         </div>

                         <div class="col-md-6">
                          <div class="form-group">
              <label for="durationcourse"><?php echo L::QuizzTimes ?><span class="required">*</span> :</label>
                     <input type="text" class="form-control" name="durationcourse" id="durationcourse" value="" size="32" maxlength="225" />

                              </div>
                           </div>
                           <div class="col-md-6">
                           <div class="form-group">
               <label for="datecourse"><?php echo L::QuizzDeadLine ?> <span class="required">*</span> :</label>
                      <input type="text" class="form-control" name="datecourse" id="datecourse" data-mask="99/99/9999" value="" size="32" maxlength="225" />
                       <span class="help-block"><?php echo L::Datesymbolesecond ?></span>
                                </div>
                            </div>
                           <div class="col-md-6">
      		 									<div class="form-group">
      		 			<label for="datecourse"><?php echo L::EcheanceLocksQuiz ?> <span class="required">*</span> :</label>
                <div class="form-group row" id="RowbtnInscrip">

                 <div class="col-sm-7 col-md-7">
                 <div class="input-group">
                 <div id="radioBtn" class="btn-group">
                 <a class="btn  btn-sm active" id="btn1" data-toggle="verouiller" data-title="1" onclick="verouillerOui()"><?php echo L::True ?></a>
                 <a class="btn  btn-sm notActive" id="btn2" data-toggle="verouiller" data-title="0" onclick="verouillerNon()"><?php echo L::False ?></a>
                 </div>
                 <input type="hidden" name="verouiller" id="verouiller" value="1">
                 </div>
                 </div>
                 </div>
                                </div>
      		 									 </div>
  										 <div class="col-md-12">
  		 									<div class="form-group">
  		 			<label for="detailscourse"><?php echo L::QuizInstructionBtn ?> <span class="required">*</span> :</label>

                   <textarea class="form-control" rows="8" name="detailscourse" id="detailscourse" placeholder=""></textarea>
                            </div>
  		 									 </div>
                         <div class="col-md-12">
                         <span class="label label-lg label-warning"  style="text-align:center;"> <i class="fa fa-list"> </i> <?php echo L::QuizListeQuestion ?></span>
                       </div></br></br>
                       <div class="col-md-12">
                         <button type="button" name="addquest" id="addquest" class="btn btn-success " style="border-radius:5px;height:35px" > <i class="fa fa-plus"></i><?php echo L::QuizNewQuestion ?></button>
                        </div></br></br>
                        <div class="col-md-12">
                          <input type="hidden" id="nbquest" name="nbquest" value="0">
                          <input type="hidden" name="concatquest" id="concatquest" value="">
                          <input type="hidden" name="concatnbquest" id="concatnbquest" value="">

                          <input type="hidden" id="nbquesttrueorfalse" name="nbquesttrueorfalse" value="0">
                          <input type="hidden" name="concatquesttrueorfalse" id="concatquesttrueorfalse" value="">
                          <input type="hidden" name="concatnbquesttrueorfalse" id="concatnbquesttrueorfalse" value="">

                          <input type="hidden" id="nbquestmultiple" name="nbquestmultiple" value="0">
                          <input type="hidden" name="concatquestmultiple" id="concatquestmultiple" value="">
                          <input type="hidden" name="concatnbquestmultiple" id="concatnbquestmultiple" value="">




                          <table class="table" id="dynamic_field" border=0>  </table>

                          <div class="row" id="RowQuiz">


                          </div>


                         </div>













  								 </div>
                                          </div>

                                    </div>














              </div>
              <div class="form-actions">
                                    <div class="row">
                                        <div class="offset-md-3 col-md-9">
                                          <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>

                                            <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                        </div>
                                      </div>
                                   </div>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
   	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
       <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
    <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
    <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
    <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <!--script src="../assets2/plugins/summernote/summernote.js" ></script>
    <script src="../assets2/plugins/summernote/lang/summernote-fr-FR.js" ></script-->
       <!-- calendar -->
       <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
    <script src="../assets2/plugins/moment/moment.min.js" ></script>
    <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
    <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
       <!-- Common js-->
   	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
   	<script src="../assets2/js/theme-color.js" ></script>
   	<!-- Material -->
   	<script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
    <script src="../assets2/js/dropify.js"></script>
    <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
    <script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
    <script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>

 <script>
 /*$('#summernote').summernote({
     placeholder: '',
     tabsize: 2,
     height: 200,
      lang: 'fr-FR'
   });*/

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function verouillerOui()
   {
      $("#verouiller").val(1);
     // $("#btn1").removeClass('notActive').addClass('active');
     // $("#btn2").removeClass('active').addClass('notActive');
     $("#radioBtn #btn1").css({"color": "#f0f1f3", "background-color": "#28a745"});
     $("#radioBtn #btn2").css({"color": "#f0f1f3", "background-color": "#e8091e"});
   }

   function verouillerNon()
   {
     $("#verouiller").val(0);
     // $("#btn2").removeClass('notActive').addClass('active');
     // $("#btn1").removeClass('active').addClass('notActive');
     $("#radioBtn #btn2").css({"color": "#f0f1f3", "background-color": "#28a745"});
     $("#radioBtn #btn1").css({"color": "#f0f1f3", "background-color": "#e8091e"});
   }

 $("#fichier3").dropify({
   messages: {
       "default": "Merci de selectionner le support",
       "replace": "Modifier le support",
       "remove" : "Supprimer le support",
       "error"  : "Erreur"
   }
 });

$("#matclasse").select2();
$("#classeEtab").select2();

function searchcodeEtab(id)
{
var classe=$("#classeEtab").val();
var teatcherId=id;
var etape=7;
var matiere=$("#matclasse").val();

$.ajax({

     url: '../ajax/matiere.php',
     type: 'POST',
     async:true,
     data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
     dataType: 'text',
     success: function (content, statut) {

        $("#FormAddAcademique #codeEtab").val(content);

     }
   });

}

function searchmatiere(id)
{

  var classe=$("#classeEtab").val();
  var teatcherId=id;
  var etape=6;


$.ajax({

     url: '../ajax/matiere.php',
     type: 'POST',
     async:true,
     data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe,
     dataType: 'text',
     success: function (content, statut) {


       $("#matclasse").html("");
       $("#matclasse").html(content);

     }
   });
}














function addquest()
{
  var nb=$("#nbquest").val();
  var nouveau= parseInt(nb)+1;
  $("#nbquest").val(nouveau);

  var concatquest=$("#concatquest").val();
  $("#concatquest").val(concatquest+nouveau+"@");

  var concatquest=$("#concatquest").val();
  var tab=concatquest.split("@");

  var nbtab=tab.length;

  var nbtabnew=parseInt(nbtab)-1;

  $("#concatnbquest").val(nbtabnew);


  var ligne="<div class=\"col-md-12\" id=\"ligneRowModQuest"+nouveau+"\">";

  ligne=ligne+"<div class=\"col-md-6\">";
  ligne=ligne+"<div class=\"form-group\">";
  ligne=ligne+"<label for=\"moderep"+nouveau+"\"><?php echo L::QuizModeResponseQuestion ?><span class=\"required\">*</span> :</label>";
  ligne=ligne+"<select class=\"form-control \" id=\"moderep"+nouveau+"\" name=\"moderep"+nouveau+"\" onchange=\"ModeReponse("+nouveau+")\">";
  ligne=ligne+"<option selected value=\"\"><?php echo L::QuizSelectModeResponseQuestion ?></option>";
  ligne=ligne+"<option value=\"1\"><?php echo L::TrueOrFalse ?></option>";
  ligne=ligne+"<option value=\"2\"><?php echo L::Multiplechoice ?></option>";
  ligne=ligne+"</select>";
  ligne=ligne+"</div>";

  ligne=ligne+"</div>";

  ligne=ligne+"<div class=\"col-md-12\" id=\"ligneRowTrueOrFalse"+nouveau+"\">";
  ligne=ligne+"</div>";

  ligne=ligne+"<div class=\"col-md-12\" id=\"ligneRowMultiplechoice"+nouveau+"\">";
  ligne=ligne+"</div>";

  ligne=ligne+"</div>";



  $("#RowQuiz").append(ligne);

  $("#moderep"+nouveau).select2();

  $('#moderep'+nouveau).rules( "add", {
      required: true,
      messages: {
      required: "<?php echo L::QuizSelectModeResponseQuestionRequired ?>"
}
    });


  // if(nbtabnew==1)
  // {
  //
  //
  //
  // }else {
  //
  // }



}

function defaultOne()
{
  $("#radioBtn #btn1").css({"color": "#f0f1f3", "background-color": "#28a745"});
  $("#radioBtn #btn2").css({"color": "#f0f1f3", "background-color": "#e8091e"});
}

function createTrueOrFalse(nouveau)
{
  var concatquesttrueorfalse=$("#concatquesttrueorfalse").val();
  $("#concatquesttrueorfalse").val(concatquesttrueorfalse+nouveau+"@");




    var ligne="<div class=\"row\">";
    ligne=ligne+"<div class=\"col-md-6\">";
    ligne=ligne+"<div class=\"form-group\">";
    ligne=ligne+"<label for=\"libellequestion"+nouveau+"\">Question "+nouveau+"<span class=\"required\">*</span> :</label>";
    ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"libellequestion"+nouveau+"\" id=\"libellequestion"+nouveau+"\" value=\"\" size=\"32\" maxlength=\"225\" />";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";

    ligne=ligne+"<div class=\"col-md-6\">";
    ligne=ligne+"<div class=\"form-group\">";
    ligne=ligne+"<label for=\"pointquestion"+nouveau+"\">Point <span class=\"required\">*</span> :</label>";
    ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"pointquestion"+nouveau+"\" id=\"pointquestion"+nouveau+"\" value=\"\" size=\"32\" maxlength=\"225\" />";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";

    ligne=ligne+"<div class=\"col-md-6\">";
    ligne=ligne+"<div class=\"form-group\">";

    ligne=ligne+"<div class=\"col-sm-7 col-md-7\">";

    ligne=ligne+"<div class=\"input-group\">";

    ligne=ligne+"<div id=\"radioBtn"+nouveau+"\" class=\"btn-group\">";

    ligne=ligne+"<a class=\"btn  btn-sm \" id=\"btn1"+nouveau+"\" data-toggle=\"answer"+nouveau+"\" data-title=\"1\" onclick=\"vrai("+nouveau+")\"><?php echo L::TrueNOM ?></a>&nbsp;";
    ligne=ligne+"<a class=\"btn  btn-sm \" id=\"btn2"+nouveau+"\" data-toggle=\"answer"+nouveau+"\" data-title=\"0\" onclick=\"faux("+nouveau+")\"><?php echo L::FalseNom ?></a>";

    ligne=ligne+"</div>";

    ligne=ligne+"<input type=\"hidden\" name=\"answer"+nouveau+"\" id=\"answer"+nouveau+"\"  value=\"1\">";

    ligne=ligne+"</div>";

    ligne=ligne+"</div>";

    ligne=ligne+"</div>";
    ligne=ligne+"</div>";



    ligne=ligne+"</div>";

    $("#ligneRowTrueOrFalse"+nouveau).append(ligne);

    // $("#btn1"+nouveau).removeClass('notActive').addClass('active');
    // $("#btn2"+nouveau).removeClass('active').addClass('notActive');

    // $("#radioBtn"+nouveau+" .notActive").css({"color": "#f0f1f3", "background-color": "#e8091e"});
    // $("#radioBtn"+nouveau+ " .active").css({"color": "#f0f1f3", "background-color": "#28a745"});

    activated(nouveau);


    $('#moderep'+nouveau).rules( "add", {
        required: true,
        messages: {
        required: "<?php echo L::QuizSelectModeResponseQuestionRequired ?>"
    }
      });

      $('#libellequestion'+nouveau).rules( "add", {
          required: true,
          messages: {
          required: "<?php echo L::PleaseAddresponserequired ?>"
      }
        });

        $('#pointquestion'+nouveau).rules( "add", {
            required: true,
            messages: {
            required: "<?php echo L::PleaseAddPointresponserequired ?>"
        }
          });



}



function createMultiple(nouveau)
{
  var concatquestmultiple=$("#concatquestmultiple").val();
  $("#concatquestmultiple").val(concatquestmultiple+nouveau+"@");

  var ligne="<div class=\"row\">";

  ligne=ligne+"<div class=\"col-md-6\">";
  ligne=ligne+"<div class=\"form-group\">";
  ligne=ligne+"<label for=\"libellequestion"+nouveau+"\">Question "+nouveau+"<span class=\"required\">*</span> :</label>";
  ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"libellequestion"+nouveau+"\" id=\"libellequestion"+nouveau+"\" value=\"\" size=\"32\" maxlength=\"225\" />";
  ligne=ligne+"</div>";
  ligne=ligne+"</div>";


  ligne=ligne+"<div class=\"col-md-6\">";
  ligne=ligne+"<div class=\"form-group\">";
  ligne=ligne+"<label for=\"pointquestion"+nouveau+"\">Point <span class=\"required\">*</span> :</label>";
  ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"pointquestion"+nouveau+"\" id=\"pointquestion"+nouveau+"\" value=\"\" size=\"32\" maxlength=\"225\" />";
  ligne=ligne+"</div>";
  ligne=ligne+"</div>";


  ligne=ligne+"<div class=\"col-md-6\">";
  ligne=ligne+"<div class=\"form-group\">";
  ligne=ligne+"<label for=\"propositionNbquestion"+nouveau+"\">Proposition de réponse (Nombre) <span class=\"required\">*</span> :</label>";
  ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"propositionNbquestion"+nouveau+"\" id=\"propositionNbquestion"+nouveau+"\" value=\"\" size=\"32\" maxlength=\"225\" onchange=\"addpropositions("+nouveau+")\" />";
  ligne=ligne+"<input type=\"hidden\" id=\"nboldproposition"+nouveau+"\" name=\"nboldproposition"+nouveau+"\" value=\"0\">";
  ligne=ligne+"</div>";
  ligne=ligne+"</div>";

  ligne=ligne+"</div>";

  ligne=ligne+"<div class=\"row\">";
  ligne=ligne+"<div class=\"col-md-12\">";
  ligne=ligne+"<table class=\"table\" id=\"fields_proposition"+nouveau+"\" border=0></table>";
  ligne=ligne+"</div>";
  ligne=ligne+"</div>";

  $("#ligneRowMultiplechoice"+nouveau).append(ligne);


  $('#libellequestion'+nouveau).rules( "add", {
      required: true,
      messages: {
      required: "Merci de renseigner la question"
}
    });

    $('#pointquestion'+nouveau).rules( "add", {
        required: true,
        messages: {
        required: "Merci de renseigner le nombre de point pour cette qestion"
  }
      });

      $('#propositionNbquestion'+nouveau).rules( "add", {
          required: true,
          messages: {
          required: "Merci de renseigner le nombre de proposition de réponse"
    }
        });



}

function addpropositions(nouveau)
{
  var propositionNb=$("#propositionNbquestion"+nouveau).val();


  if(propositionNb==0||propositionNb=="")
  {
    var nboldproposition=$("#nboldproposition"+nouveau).val();

    for(var i=1;i<=nboldproposition;i++)
    {

      $("#LigneProposition"+nouveau+""+i).remove();

    }
  }else if(propositionNb>0)
  {
    var nboldproposition=$("#nboldproposition"+nouveau).val();

    for(var i=1;i<=nboldproposition;i++)
    {

      $("#LigneProposition"+nouveau+""+i).remove();

    }
  }


  var ligne="";

  for(var i=1;i<=propositionNb;i++)
  {
    ligne=ligne+"<tr id=\"LigneProposition"+nouveau+""+i+"\">";

    ligne=ligne+"<td>";
    ligne=ligne+"<input type=\"checkbox\" id=\"chk_proposition"+nouveau+""+i+"\" name=\"chk_proposition"+nouveau+""+i+"\" value=\"0\" onclick=\"CheckProp("+nouveau+","+i+")\"/>";
    ligne=ligne+"</td>";

    ligne=ligne+"<td>";
      ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"libelle_proposition"+nouveau+""+i+"\" value=\"\" />";
    ligne=ligne+"</td>";

    ligne=ligne+"</tr>";
  }

    $("#fields_proposition"+nouveau).append(ligne);

    $("#nboldproposition"+nouveau).val(propositionNb);

    $('#libelle_proposition'+nouveau).rules( "add", {
        required: true,
        messages: {
        required: "Merci de renseigner la proposition de reponse"
  }
      });

  // alert(propositionNb);
}

function CheckProp(nouveau,i)
{
  if( $('#chk_proposition'+nouveau+''+i).is(':checked') ){
    $('#chk_proposition'+nouveau+''+i).val(1);
} else {
    $('#chk_proposition'+nouveau+''+i).val(0);
}
}


function vrai(nouveau)
{

  activated(nouveau);
  $("#answer"+nouveau).val(1);
  // alert('bonjour');
  // $("#btn1"+nouveau).css({"color": "#f0f1f3", "background-color": "#28a745"});
  // $("#btn2"+nouveau).css({"color": "#f0f1f3", "background-color": "#e8091e"});

  // $("#radioBtn"+nouveau+" #btn1"+nouveau).css({"color": "#f0f1f3", "background-color": "#28a745"});
  // $("#radioBtn"+nouveau+ "#btn2"+nouveau).css({"color": "#f0f1f3", "background-color": "#e8091e"});
}

function faux(nouveau)
{

desactivated(nouveau);
$("#answer"+nouveau).val(0);
  // alert('bonsoir');
  // $("#btn2"+nouveau).css({"color": "#f0f1f3", "background-color": "#28a745"});
  // $("#btn1"+nouveau).css({"color": "#f0f1f3", "background-color": "#e8091e"});

  // $("#radioBtn"+nouveau+" #btn2"+nouveau).css({"color": "#f0f1f3", "background-color": "#28a745"});
  // $("#radioBtn"+nouveau+ " #btn1"+nouveau).css({"color": "#f0f1f3", "background-color": "#e8091e"});

}

function deletetrueorfalseAndcreateMultiple(nouveau)
{


    // createMultiple(nouveau);

    var concatquestmultiple=$("#concatquestmultiple").val();
    $("#concatquestmultiple").val(concatquestmultiple+nouveau+"@");

    var ligne="<div class=\"row\">";

    ligne=ligne+"<div class=\"col-md-6\">";
    ligne=ligne+"<div class=\"form-group\">";
    ligne=ligne+"<label for=\"libellequestion"+nouveau+"\">Question "+nouveau+"<span class=\"required\">*</span> :</label>";
    ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"libellequestion"+nouveau+"\" id=\"libellequestion"+nouveau+"\" value=\"\" size=\"32\" maxlength=\"225\" />";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";


    ligne=ligne+"<div class=\"col-md-6\">";
    ligne=ligne+"<div class=\"form-group\">";
    ligne=ligne+"<label for=\"pointquestion"+nouveau+"\">Point <span class=\"required\">*</span> :</label>";
    ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"pointquestion"+nouveau+"\" id=\"pointquestion"+nouveau+"\" value=\"\" size=\"32\" maxlength=\"225\" />";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";


    ligne=ligne+"<div class=\"col-md-6\">";
    ligne=ligne+"<div class=\"form-group\">";
    ligne=ligne+"<label for=\"propositionNbquestion"+nouveau+"\">Proposition de réponse (Nombre) <span class=\"required\">*</span> :</label>";
    ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"propositionNbquestion"+nouveau+"\" id=\"propositionNbquestion"+nouveau+"\" value=\"\" size=\"32\" maxlength=\"225\" onchange=\"addpropositions("+nouveau+")\" />";
    ligne=ligne+"<input type=\"hidden\" id=\"nboldproposition"+nouveau+"\" name=\"nboldproposition"+nouveau+"\" value=\"0\">";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";

    ligne=ligne+"</div>";

    ligne=ligne+"<div class=\"row\">";
    ligne=ligne+"<div class=\"col-md-12\">";
    ligne=ligne+"<table class=\"table\" id=\"fields_proposition"+nouveau+"\" border=0></table>";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";

    $("#ligneRowMultiplechoice"+nouveau).append(ligne);

    var concatquesttrueorfalse=$("#concatquesttrueorfalse").val();
    $("#concatquesttrueorfalse").val($("#concatquesttrueorfalse").val().replace(nouveau+"@", ""));
    $("#ligneRowTrueOrFalse"+nouveau).remove();


    $('#libellequestion'+nouveau).rules( "add", {
        required: true,
        messages: {
        required: "Merci de renseigner la question"
  }
      });

      $('#pointquestion'+nouveau).rules( "add", {
          required: true,
          messages: {
          required: "Merci de renseigner le nombre de point pour cette qestion"
    }
        });

        $('#propositionNbquestion'+nouveau).rules( "add", {
            required: true,
            messages: {
            required: "Merci de renseigner le nombre de proposition de réponse"
      }
          });


}

function deleteMultipleAndcreateTrueOrFalse(nouveau)
{


    // createTrueOrFalse(nouveau);

    var concatquesttrueorfalse=$("#concatquesttrueorfalse").val();
    $("#concatquesttrueorfalse").val(concatquesttrueorfalse+nouveau+"@");




      var ligne="<div class=\"row\">";
      ligne=ligne+"<div class=\"col-md-6\">";
      ligne=ligne+"<div class=\"form-group\">";
      ligne=ligne+"<label for=\"libellequestion"+nouveau+"\">Question "+nouveau+"<span class=\"required\">*</span> :</label>";
      ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"libellequestion"+nouveau+"\" id=\"libellequestion"+nouveau+"\" value=\"\" size=\"32\" maxlength=\"225\" />";
      ligne=ligne+"</div>";
      ligne=ligne+"</div>";

      ligne=ligne+"<div class=\"col-md-6\">";
      ligne=ligne+"<div class=\"form-group\">";
      ligne=ligne+"<label for=\"pointquestion"+nouveau+"\">Point <span class=\"required\">*</span> :</label>";
      ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"pointquestion"+nouveau+"\" id=\"pointquestion"+nouveau+"\" value=\"\" size=\"32\" maxlength=\"225\" />";
      ligne=ligne+"</div>";
      ligne=ligne+"</div>";

      ligne=ligne+"<div class=\"col-md-6\">";
      ligne=ligne+"<div class=\"form-group\">";

      ligne=ligne+"<div class=\"col-sm-7 col-md-7\">";

      ligne=ligne+"<div class=\"input-group\">";

      ligne=ligne+"<div id=\"radioBtn"+nouveau+"\" class=\"btn-group\">";

      ligne=ligne+"<a class=\"btn  btn-sm \" id=\"btn1"+nouveau+"\" data-toggle=\"answer"+nouveau+"\" data-title=\"1\" onclick=\"vrai("+nouveau+")\">VRAI</a>&nbsp;";
      ligne=ligne+"<a class=\"btn  btn-sm \" id=\"btn2"+nouveau+"\" data-toggle=\"answer"+nouveau+"\" data-title=\"0\" onclick=\"faux("+nouveau+")\">FAUX</a>";

      ligne=ligne+"</div>";

      ligne=ligne+"<input type=\"hidden\" name=\"answer"+nouveau+"\" id=\"answer"+nouveau+"\"  value=\"1\">";

      ligne=ligne+"</div>";

      ligne=ligne+"</div>";

      ligne=ligne+"</div>";
      ligne=ligne+"</div>";



      ligne=ligne+"</div>";

      $("#ligneRowTrueOrFalse"+nouveau).append(ligne);

      var concatquestmultiple=$("#concatquestmultiple").val();
      $("#concatquestmultiple").val($("#concatquestmultiple").val().replace(nouveau+"@", ""));
      $("#ligneRowMultiplechoice"+nouveau).remove();

      // $("#btn1"+nouveau).removeClass('notActive').addClass('active');
      // $("#btn2"+nouveau).removeClass('active').addClass('notActive');

      // $("#radioBtn"+nouveau+" .notActive").css({"color": "#f0f1f3", "background-color": "#e8091e"});
      // $("#radioBtn"+nouveau+ " .active").css({"color": "#f0f1f3", "background-color": "#28a745"});

      activated(nouveau);


      $('#moderep'+nouveau).rules( "add", {
          required: true,
          messages: {
          required: "Merci de selectionner le mode de réponse"
      }
        });

        $('#libellequestion'+nouveau).rules( "add", {
            required: true,
            messages: {
            required: "Merci de renseigner la question"
        }
          });

          $('#pointquestion'+nouveau).rules( "add", {
              required: true,
              messages: {
              required: "Merci de renseigner le nombre de point pour cette réponse"
          }
            });

}

function ModeReponse(nouveau)
{
  var mode=$("#moderep"+nouveau).val();

    if(mode==1)
    {
      // alert("vrai ou faux");

      var concatquesttrueorfalse=$("#concatquesttrueorfalse").val();
      var concatquestmultiple=$("#concatquestmultiple").val();
      var concat=concatquesttrueorfalse+concatquestmultiple;

      var tabtrueorfalse=concatquesttrueorfalse.split("@");
      var tabMulti=concatquestmultiple.split("@");

      var tab=concat.split("@");

      var nbtab=concat.length;

      var nbtabnew=parseInt(nbtab)-1;

      var nb=0;

      // alert(nbtab);

      if(nbtab==0)
      {
        createTrueOrFalse(nouveau);
      }else {
        for(var i=0;i<nbtab;i++)
        {
          if(tab[i]==nouveau)
          {
            nb++;
          }
        }
        // alert(nb);
        if(nb>0)
        {
          //nous allons verifier dans le premier tableau

          var nbtabtrue=tabtrueorfalse.length;
          var nbtabtruenew=parseInt(nbtabtrue)-1;
          var nbtrue=0;

          for(var i=0;i<nbtabtrue;i++)
          {
            if(tabtrueorfalse[i]==nouveau)
            {
              nbtrue++;
            }
          }

          if(nbtrue>0)
          {
            //supprimer le trueorfalse
            alert("bonjour");

          }else {
            alert("supprimer le choix multiple et creer le trueorfalse");
            deleteMultipleAndcreateTrueOrFalse(nouveau);


          }

        }else {
          createTrueOrFalse(nouveau);
        }


      }




    }else if(mode==2)
    {
      // alert("choix multiple");

      var concatquesttrueorfalse=$("#concatquesttrueorfalse").val();
      var concatquestmultiple=$("#concatquestmultiple").val();
      var concat=concatquesttrueorfalse+concatquestmultiple;

      var tabtrueorfalse=concatquesttrueorfalse.split("@");
      var tabMulti=concatquestmultiple.split("@");
      var tab=concat.split("@");

      var nbtab=concat.length;

      var nbtabnew=parseInt(nbtab)-1;

      var nb=0;

      // alert(nbtab);

      if(nbtab==0)
      {
        createMultiple(nouveau);
      }else {
        for(var i=0;i<nbtab;i++)
        {
          if(tab[i]==nouveau)
          {
            nb++;
          }
        }

        if(nb>0)
        {
            //nous allons verifier dans le premier tableau

            var nbtabtrue=tabtrueorfalse.length;
            var nbtabtruenew=parseInt(nbtabtrue)-1;
            var nbtrue=0;

            for(var i=0;i<nbtabtrue;i++)
            {
              if(tabtrueorfalse[i]==nouveau)
              {
                nbtrue++;
              }
            }

            if(nbtrue>0)
            {
                alert("supprimer le trueorfalse et creer le multiple");

                deletetrueorfalseAndcreateMultiple(nouveau);



            }else {
              //supprimer le choix multiple

              alert("bonsoir");
            }


        }else {
          createMultiple(nouveau);
        }
      }
      }

    }



$('#durationcourse').bootstrapMaterialDatePicker
({
  date: false,
  shortTime: false,
  format: 'HH:mm',
  lang: 'fr',
 cancelText: 'Annuler',
 okText: 'OK',
 clearText: 'Effacer',
 nowText: 'Maintenant'

});


function activated(nouveau)
{
  $("#radioBtn"+nouveau+" #btn1"+nouveau).css({"color": "#f0f1f3", "background-color": "#28a745"});
  $("#radioBtn"+nouveau+ " #btn2"+nouveau).css({"color": "#f0f1f3", "background-color": "#e8091e"});
}

function desactivated(nouveau)
{
  $("#radioBtn"+nouveau+" #btn2"+nouveau).css({"color": "#f0f1f3", "background-color": "#28a745"});
  $("#radioBtn"+nouveau+ " #btn1"+nouveau).css({"color": "#f0f1f3", "background-color": "#e8091e"});
}

 $(document).ready(function() {




   $("#FormAddAcademique").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
    },
    highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
    },
    success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
     rules:{

       programme:"required",
       descri:"required",
       classeEtab:"required",
       matclasse:"required",
       fichier:"required",
       durationcourse:"required",
       detailscourse:"required",
       datecourse:"required",
       libellecourse:"required"

     },
   messages: {
     programme:"Merci de renseigner le libellé du programme",
     descri:"Merci de renseigner le détails de la fiche ",
     classeEtab:"Merci de selectionner une classe",
     matclasse:"Merci de selectionner une matière",
     fichier:"Merci de selectionner le fichier du programme",
     durationcourse:"Merci de renseigner la durée du quiz",
     detailscourse:"Merci de renseigner les instructions du quiz",
     datecourse:"Merci de renseigner la date limite du quiz",
     libellecourse:"Merci de renseigner le libellé du quiz"
   },
   submitHandler: function(form) {

   form.submit();



   }

   });

addquest();
defaultOne();

   $('#addquest').click(function(){

     //creation d'une ligne de section

     addquest();

   });

   $('#addcomp').click(function(){

     //creation d'une ligne de section

     AddcompRow();

   });

   $('#addtache').click(function(){

     //creation d'une ligne de section

     AddtachesRow();

   });






 });

 </script>
    <!-- end js include path -->
  </body>

</html>
