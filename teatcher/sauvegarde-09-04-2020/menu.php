<div class="white-sidebar-color sidebar-container">

 				<div class="sidemenu-container navbar-collapse collapse fixed-menu">

	                <div id="remove-scroll" class="left-sidemenu">

	                    <ul class="sidemenu  page-header-fixed slimscroll-style" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">

	                        <li class="sidebar-toggler-wrapper hide">

	                            <div class="sidebar-toggler">

	                                <span></span>

	                            </div>

	                        </li>

	                        <li class="sidebar-user-panel">

	                            <div class="user-panel">

	                                <div class="pull-left image">

	                                    <img src="<?php echo $lienphoto?>" class="img-circle user-img-circle" alt="User Image" />

	                                </div>

	                                <div class="pull-left info">

	                                    <p> <?php echo $tablogin[0];?></p>

	                                    <small>

                                        <?php



                                          if($tablogin[1]=="Admin_globale")

                                          {

                                            echo "Administrateur";

                                          }else if($tablogin[1]=="Admin_locale")

                                          {

                                            echo "Admin Locale";

                                          }else if($tablogin[1]=="Teatcher")

                                          {

                                            echo "Professeur";

                                          }else if($tablogin[1]=="Parent")

                                          {

                                            echo "Parent";

                                          }

                                        ?>

                                      </small>

	                                </div>

	                            </div>

	                        </li>

                          <li class="nav-item active open">

	                            <a href="index.php" class="nav-link nav-toggle">

	                                <!-- <i class="material-icons">dashboard</i> -->

	                                <span class="title">Dashboard</span>

	                                <span class="selected"></span>



	                            </a>



	                        </li>



                          <li class="nav-item">

                            <a href="javascript:;" class="nav-link nav-toggle">

                                <!-- <i class="material-icons">dashboard</i> -->

                                <span class="title">Elèves</span>

                                <span class="selected"></span>

                               <span class="arrow open"></span>



                            </a>

                             <ul class="sub-menu" style="margin-left:-30px;">

                               <?php

                               if($nbclasse>0)

                               {

                                ?>





                                 <li class="nav-item">

                                     <a href="javascript:;" class="nav-link  nav-toggle">

                                         Classes

                                       </a>

                                       <ul class="sub-menu" style="margin-left:5px;">

                                         <?php

                                         //recuperer la liste des classe enseigner

                                         $specificlasses=$classe->getTeatcherClassesId($_SESSION['user']['IdCompte']);

                                         $j=1;

                                         foreach ($specificlasses as $value):

                                          ?>

                                         <li class="nav-item">

                                             <a href="showInfosclasse.php?classe=<?php echo $value->id_classe;?>&codeEtab=<?php echo $value->codeEtab?>" class="nav-link">

                                                 <?php echo $value->libelle_classe;?>

                                               </a>

                                         </li>

                                         <?php



                                         $j++;

                                         endforeach;

                                          ?>

                                       </ul>

                                 </li>

                                 <li class="nav-item">

                                     <a href="javascript:;" class="nav-link  nav-toggle">

                                         Présence Journalière

                                       </a>

                                       <ul class="sub-menu" style="margin-left:5px;">

                                         <?php

                                         //recuperer la liste des classe enseigner

                                         $specificlasses=$classe->getTeatcherClassesId($_SESSION['user']['IdCompte']);

                                         $j=1;

                                         foreach ($specificlasses as $value):

                                          ?>

                                         <li class="nav-item">

                                             <a href="dailyattendance.php?classe=<?php echo $value->id_classe;?>&codeEtab=<?php echo $value->codeEtab?>" class="nav-link">

                                                 <?php echo $value->libelle_classe;?>

                                               </a>

                                         </li>

                                         <?php



                                         $j++;

                                         endforeach;

                                          ?>

                                       </ul>

                                 </li>



                                 <li class="nav-item">

                                     <a href="javascript:;" class="nav-link  nav-toggle">

                                         Récap Présence

                                       </a>

                                       <ul class="sub-menu" style="margin-left:5px;">

                                         <?php

                                         //recuperer la liste des classe enseigner

                                         $specificlasses=$classe->getTeatcherClassesId($_SESSION['user']['IdCompte']);

                                         $j=1;

                                         foreach ($specificlasses as $value):

                                          ?>

                                         <li class="nav-item">

                                             <a href="recapattendance.php?classe=<?php echo $value->id_classe;?>&codeEtab=<?php echo $value->codeEtab?>" class="nav-link">

                                                 <?php echo $value->libelle_classe;?>

                                               </a>

                                         </li>

                                         <?php



                                         $j++;

                                         endforeach;

                                          ?>

                                       </ul>

                                 </li>



                                <?php

                              }else

                              {

                                ?>

                                <li class="nav-item">

                                    <a href="#" class="nav-link">

                                        Aucune classe

                                      </a>

                                </li>

                                <?php



                              }

                                 ?>







                             </ul>



	                        </li>

                          <!--li class="nav-item">

                            <a href="routines.php" class="nav-link">

                                <i class="material-icons">dashboard</i>

                                <span class="title">EMPLOI DU TEMPS</span>



                            </a>





	                        </li-->

                          <!--li class="nav-item">

                            <a href="controles.php" class="nav-link">

                                <i class="material-icons">dashboard</i>

                                <span class="title">CONTROLES</span>



                            </a>





	                        </li-->



                          <li class="nav-item">

                            <a href="javascript:;" class="nav-link nav-toggle">

                                <!-- <i class="material-icons">dashboard</i> -->

                                <span class="title">Enseignement</span>

                                <span class="selected"></span>

                               <span class="arrow open"></span>



                            </a>

                             <ul class="sub-menu" style="margin-left:-30px;">

			      


                               <li class="nav-item">

                                   <a href="programmes.php" class="nav-link">

                                       Syllabus

                                     </a>

                               </li>

                               <li class="nav-item">

                                   <a href="cahiers.php" class="nav-link">

                                       Cahier de texte

                                     </a>

                               </li>

                               <li class="nav-item">

                                   <a href="routines.php" class="nav-link">

                                      Emploi de temps

                                     </a>

                               </li>

				<li class="nav-item">

                                   <a href="online.php" class="nav-link">

                                       Formations en ligne 

                                     </a>

                               </li>




                             </ul>



                          </li>



                          <li class="nav-item  ">

                              <a href="#" class="nav-link nav-toggle">



                                  <span class="title">Evaluations et notes</span>

                                  <span class="selected"></span>

                                 <span class="arrow open"></span>

                              </a>

                              <ul class="sub-menu" style="margin-left:-50px">



                                <li class="nav-item  ">

                                    <a href="addnotes.php" class="nav-link "> <span class="title">Consigner une Note</span>

                                    </a>

                                </li>

                                <li class="nav-item  ">

                                      <a href="controles.php" class="nav-link "> <span class="title">Contrôles</span>

                                      </a>

                                  </li>

                                  <!--li class="nav-item  ">

	                                    <a href="notes.php" class="nav-link "> <span class="title">Toutes les Notes</span>

	                                    </a>

	                                </li-->

                                  <li class="nav-item  " class="nav-link nav-toggle">

                                      <a href="#" class="nav-link nav-toggle" >Récap des notes</a>

                                    <ul class="sub-menu" style="">

                                      <li class="nav-item" ><a href="notes.php">Classe</a></li>

                                      <li class="nav-item"><a href="noteseleves.php">Eleves</a> </li>



                                    </ul>

	                                    <!--a href="recapattendance.php" class="nav-link "> <span class="title">Récap des absences</span>

	                                    </a-->

	                                </li>





                              </ul>

                          </li>





                          <!--li class="nav-item">

                            <a href="javascript:;" class="nav-link nav-toggle">



                                <span class="title">Evaluations et notes</span>

                                <span class="selected"></span>

                               <span class="arrow open"></span>



                            </a>

                             <ul class="sub-menu" style="margin-left:-30px;">

                               <li class="nav-item">

                                   <a href="controles.php" class="nav-link">

                                       Controles

                                     </a>

                               </li>

                               <li class="nav-item">

                                   <a href="notes.php" class="nav-link">

                                       Toutes les Notes

                                     </a>

                               </li>

                               <li class="nav-item">

                                   <a href="addnotes.php" class="nav-link">

                                      Ajouter Note

                                     </a>

                               </li>



                             </ul>



                          </li-->







                          <!--li class="nav-item">

                            <a href="programmes.php" class="nav-link">

                                <i class="material-icons">dashboard</i>

                                <span class="title">PROGRAMMES ACADEMIQUES</span>



                            </a>





	                        </li-->



                          <!--li class="nav-item">

                            <a href="cahiers.php" class="nav-link">



                                <span class="title"><?php echo L::CahierMenu  ?></span>



                            </a>





	                        </li-->









                          <li class="nav-item">

	                            <a href="teatchers.php" class="nav-link nav-toggle">

	                                <!-- <i class="material-icons">dashboard</i> -->

	                                <span class="title">Professeurs</span>

	                                <span class="selected"></span>



	                            </a>



	                        </li>

                          <li class="nav-item  ">

	                            <a href="#" class="nav-link nav-toggle">

	                                <span class="title">Notification</span>

                                  <span class="selected"></span>

                                 <span class="arrow open"></span>

	                            </a>

	                            <ul class="sub-menu" style="margin-left:-40px;">



	                                <li class="nav-item  ">

	                                    <a href="addmessages.php" class="nav-link "> <span class="title">Nouvelle notification</span>

	                                    </a>

	                                </li>

                                  <li class="nav-item  ">

                                      <a href="allmessages.php" class="nav-link "> <span class="title">Liste notification</span>

                                      </a>

                                  </li>





	                            </ul>

	                        </li>

                          <li class="nav-item">

	                            <a href="rating.php" class="nav-link nav-toggle">

	                                <!-- <i class="material-icons">dashboard</i> -->

	                                <span class="title">MOYENNE</span>

	                                <span class="selected"></span>



	                            </a>



	                        </li>

































                          <!--li class="nav-item ">

	                            <a href="exams.php" class="nav-link nav-toggle">

	                                <i class="material-icons">hotel</i>

	                                <span class="title">Examens de contrôles</span>

	                                <span class="selected"></span>



	                            </a>



	                        </li-->

                          <!--li class="nav-item  ">

	                            <a href="#" class="nav-link nav-toggle"> <i class="material-icons">hotel</i>

	                                <span class="title">Examens de contrôles</span> <span class="arrow"></span>

	                            </a>

	                            <ul class="sub-menu">

	                                <li class="nav-item  ">

	                                    <a href="#" class="nav-link "> <span class="title">Toutes les Examens de contrôles</span>

	                                    </a>

	                                </li>

	                                <li class="nav-item  ">

	                                    <a href="#" class="nav-link "> <span class="title">Ajouter Examen de contrôle</span>

	                                    </a>

	                                </li>



	                            </ul>

	                        </li-->



                          <!--li class="nav-item  ">

	                            <a href="#" class="nav-link nav-toggle"> <i class="material-icons">hotel</i>

	                                <span class="title">Absences</span> <span class="arrow"></span>

	                            </a>

	                            <ul class="sub-menu">

	                                <li class="nav-item  ">

	                                    <a href="#" class="nav-link "> <span class="title">Toutes les Absences</span>

	                                    </a>

	                                </li>

	                                <li class="nav-item  ">

	                                    <a href="#" class="nav-link "> <span class="title">Ajouter Absence</span>

	                                    </a>

	                                </li>



	                            </ul>

	                        </li-->



                          <!--li class="nav-item ">

                              <a href="attendances.php" class="nav-link nav-toggle">

                                  <i class="material-icons">hotel</i>

                                  <span class="title">Présences</span>

                                  <span class="selected"></span>



                              </a>



                          </li-->



                          <!--li class="nav-item  ">

	                            <a href="#" class="nav-link nav-toggle"> <i class="material-icons">hotel</i>

	                                <span class="title">Emploi du temps</span> <span class="arrow"></span>

	                            </a>

	                            <ul class="sub-menu">

	                                <li class="nav-item  ">

	                                    <a href="#" class="nav-link "> <span class="title">Toutes les Emplois du temps</span>

	                                    </a>

	                                </li>

	                                <li class="nav-item  ">

	                                    <a href="#" class="nav-link "> <span class="title">Ajouter Emploi du temps</span>

	                                    </a>

	                                </li>



	                            </ul>

	                        </li-->



                          <!--li class="nav-item ">

                              <a href="times.php" class="nav-link nav-toggle">

                                  <i class="material-icons">hotel</i>

                                  <span class="title">Emploi du temps</span>

                                  <span class="selected"></span>



                              </a>



                          </li-->







                </div>

            </div>

