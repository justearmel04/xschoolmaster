<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Teatcher.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Matiere.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classMat=new Matiere();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$localadmins= new Localadmin();
$parents=new ParentX();
$classe=new Classe();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
}

$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);
$nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();

if(isset($_GET['classe']))
{
  $lessonclasse=htmlspecialchars(addslashes($_GET['classe']));
}

if(isset($_POST['classe']))
{
  $lessonclasse=htmlspecialchars(addslashes($_POST['classe']));
}

if(isset($_GET['codeEtab']))
{
  $lessonEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
}

if(isset($_POST['codeEtab']))
{
  $lessonEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
}

//determiner la liste des matières dispenser dans cet etablissement

$matieresclasses=$etabs->getAllsubjectofclassesbyIdclasses($lessonclasse,$lessonEtab,$libellesessionencours);

$fichesmatieres=$etabs->getSpecificMatieresOfProgrammesClasses($lessonclasse,$lessonEtab,$libellesessionencours);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::Lessonsreceive ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::Lessonsreceive ?></li>
                          </ol>
                      </div>
                  </div>

                  <div class="state-overview">
        						<div class="row">

        					        <!-- /.col -->

        					        <!-- /.col -->

        					        <!-- /.col -->

        					        <!-- /.col -->
        					      </div>
        						</div>
        					<!-- end widget -->
                  <?php

                        if(isset($_SESSION['user']['addetabexist']))
                        {

                          ?>
                          <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <?php
                        echo $_SESSION['user']['addetabexist'];
                        ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                           </a>
                        </div>



                          <?php
                          unset($_SESSION['user']['addetabexist']);
                        }

                         ?>


                  <div class="row">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                      <div class="card card-topline-green">
                           <div class="card-head">
                               <header><?php echo L::ManagementreceivesLessons ?> </header>
                               <div class="tools">
                                   <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>

                               </div>
                           </div>
                           <div class="card-body ">
                             <?php

                              ?>
                             <form method="post" id="FormLessons" action="lessonsreceives.php">
                                 <div class="row">
                                   <div class="col-md-6 col-sm-6">
                                     <div class="form-group" style="margin-top:8px;">
                                         <label><?php echo L::MatiereMenusingle ?></label>
                                         <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                                         <select class="form-control input-height" id="matclasse" name="matclasse" style="width:100%;" >
                                             <option value=""><?php echo L::SelectSubjects ?></option>
                                             <?php
                                             foreach ($matieresclasses as  $value):
                                               ?>
                                               <option <?php if(isset($_POST['matclasse'])&&($_POST['matclasse']==$value->id_mat )){echo "selected";} ?> value="<?php echo $value->id_mat ?>"><?php echo utf8_encode(utf8_decode($value->libelle_mat)); ?></option>
                                               <?php
                                             endforeach;
                                              ?>

                                         </select>
                                     </div>
                                   <!-- text input -->



                               </div>
                               <div class="col-md-6 col-sm-6">
                               <!-- text input -->
                                </div>
                                   <div class="col-md-6 col-sm-6">
                                   <!-- text input -->
                                   <div class="form-group" style="margin-top:8px;">
                                       <label><?php echo L::PeriodeDu ?></label>
                                       <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                                       <input type="date" name="datedu" id="datedu" class="form-control" value="">
                                   </div>




                               </div>

                           <div class="col-md-6 col-sm-6">
                           <!-- text input -->
                           <div class="form-group" style="margin-top:8px;">
                               <label><?php echo L::PeriodAu ?> </label>
                               <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                               <input type="date" class="form-control" name="dateau" id="dateau" value="">
                           </div>


                       </div>

                               <div class="col-md-3 col-sm-3">
                               <!-- text input -->
                               <!--div class="form-group">
                                   <label style="margin-top:3px;">Date</label>
                                   <input type="text" id="datepre" name ="datepre" class="floating-label mdl-textfield__input"  placeholder="Date présence">
                                   <input type="hidden" name="search" id="search" />
                               </div-->
                               <input type="hidden" name="search" id="search" />
                               <input type="hidden" name="classe" id="classe" value="<?php echo $lessonclasse;  ?>">
                               <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $lessonEtab; ?>" />
                               <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                               <button type="submit" class="btn btn-danger"  style="width:200px;height:35px;margin-top:35px;text-align:center;border-radius:5px;"><i class="fa fa-search"></i> <?php echo L::Seacher ?></button>


                           </div>


                                 </div>


                             </form>
                           </div>
                       </div>
                                </div>

                  </div>

                  <div class="row">
                    <div class="col-md-12">
                                    <div class="card card-box">
                                        <div class="card-head">
                                            <header></header>
                                            <div class="tools">
                                                <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
        	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>

                                            </div>
                                        </div>
                                        <div class="card-body ">
                                            <table id="example5" class="table table-hover  order-column full-width" style="width:100%;">

                                                    <?php

                                                    if(isset($_POST['search']))
                                                    {
                                                        if(isset($_POST['matclasse'])&& ($_POST['matclasse']>0))
                                                        {
                                                          if(isset($_POST['datedu'])&& isset($_POST['dateau']) && strlen($_POST['datedu'])>0 && strlen($_POST['dateau'])>0)
                                                          {
                                                              //rechercher par matiere et par periode
                                                              // echo "recherche avec matiere et période";
                                                              $fichesmatieres=$etabs->getLessonsbyMatiereidAndPeriode($_POST['classe'],$_POST['codeEtab'],$_POST['libellesession'],$_POST['matclasse'],$_POST['datedu'],$_POST['dateau']);

                                                          }else {
                                                            //rechercher par matiere
                                                            // echo "recherche avec matiere seulement";
                                                            $fichesmatieres=$etabs->getLessonsbyMatiereidOnly($_POST['matclasse'],$_POST['classe'],$_POST['codeEtab'],$_POST['libellesession']);
                                                          }
                                                        }
                                                    }

                                                    $nb=count($fichesmatieres);

                                                    if($nb>0)
                                                    {
                                                      foreach ($fichesmatieres as $value):
                                                        $lessons=$etabs->getLessonsbyMatiere($value->mat_fiche,$lessonclasse,$lessonEtab,$libellesessionencours);
                                                        ?>
                                                        <tr>
                                                        <td colspan="2"><?php echo $classMat->getMatiereLibelleByIdMat($value->mat_fiche,$lessonEtab); ?></td>

                                                        </tr>

                                                        <?php
                                                        foreach ($lessons as $valuelessons):

                                                          ?>
                                                         <tr>
                                                           <td><?php echo utf8_encode(utf8_decode($valuelessons->libelle_sousfic)); ?></td>
                                                           <td><?php echo $valuelessons->date_fiche; ?></td>

                                                         </tr>
                                                         <?php

                                                        endforeach;
                                                         ?>

                                                        <?php

                                                      endforeach;


                                                    }else {
                                                      echo L::Nolessons;
                                                    }


                                                     ?>


                                            </table>
                                        </div>
                                    </div>
                                </div>

                  </div>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
  <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
  <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- calendar -->
     <!-- data tables -->
    <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
  <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
    <script src="../assets2/js/pages/table/table_data.js" ></script>
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
  <script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }
   $(document).ready(function() {

$("#matclasse").select2();

   $("#FormLessons").validate({
     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
     rules:{

       matclasse:"required",
       datedu:{
         'required': {
             depends: function (element) {
                 return ($('#dateau').val() !='');

             }
         }
       },
       dateau:{
         'required': {
             depends: function (element) {
                 return ($('#datedu').val() !='');

             }
         }
       }

     },
     messages: {
       matclasse:"<?php echo L::PleaseselectSubjects ?>",
       datedu:"<?php echo L::PleaseEnterParascoActivityDateStart ?>",
       dateau:"<?php echo L::PleaseEnterParascoActivityDateEnd ?>"
     },
     submitHandler: function(form) {
       form.submit();
     }
   });



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
