<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Teatcher.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');
require_once('../class/Student.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();




$session= new Sessionacade();
$student= new Student();
$emailUti=$_SESSION['user']['email'];
$matiere=new Matiere();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$localadmins= new Localadmin();
$parents=new ParentX();
$classe=new Classe();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$_SESSION['user']['codeEtab']=$codeEtabsession;

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $_SESSION['user']['session']=$libellesessionencours;
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];


  $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
}else {
  $nbclasse=0;
}

$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);



if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();

$studentInfos=$student->getAllInformationsOfStudent($_GET['idcompte'],$libellesessionencours);
$tabStudent=explode("*",$studentInfos);

$allparents=$parents->ParentInfostudent($_GET['idcompte']);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
  <!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--bootstrap -->
  <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
  <link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
  <link href="../assets2/css/material_style.css" rel="stylesheet">
  <!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

  <!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title">Liste des Parents d'eleve <?php $nomcomplet=$tabStudent[2]." ".$tabStudent[3]; echo tronquer($nomcomplet,25);//echo tronquer();  ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><a class="parent-item" href="index.php">Eleves</a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item active" href="#">Liste des parents</a>
                              </li>
                              <!-- <li class="active">Tous les Parents</li> -->
                          </ol>
                      </div>
                  </div>

                  <?php

                        if(isset($_SESSION['user']['updateparentok']))
                        {

                          ?>
                          <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?php
                        //echo $_SESSION['user']['addetabok'];
                        ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                           </a>
                        </div-->
                <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                <script src="../assets/js/sweetalert2.min.js"></script>

                    <script>
                    Swal.fire({
                    type: 'success',
                    title: 'Félicitations',
                    text: '<?php echo $_SESSION['user']['updateparentok']; ?>',

                    })
                    </script>
                          <?php
                          unset($_SESSION['user']['updateparentok']);
                        }

                         ?>
        					<!-- end widget -->
                  <?php

                        if(isset($_SESSION['user']['addetabexist']))
                        {

                          ?>
                          <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <?php
                        echo $_SESSION['user']['addetabexist'];
                        ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                           </a>
                        </div>



                          <?php
                          unset($_SESSION['user']['addetabexist']);
                        }

                         ?>


                         <div class="row">
                                                 <div class="col-md-12">
                                                     <div class="tabbable-line">
                       <a class="btn btn-danger xs " href="classinfos.php?classe=<?php echo $_GET['classe'] ?>&codeEtab=<?php echo $_GET['codeEtab'] ?>"  ><i class="fa fa-arrow-circle-left"></i>Retour <?php //echo $_SERVER['HTTP_REFERER']; ?></a>

                                                        <ul class="nav nav-pills nav-pills-rose">
                                                          <li class="nav-item tab-all"></li>
                                          <li class="nav-item tab-all"><a class="nav-link "
                                            href="#tab1" data-toggle="tab" style="display:none">Liste</a></li>
                                          <li style="display:none" class="nav-item tab-all"><a class="nav-link active show" href="#tab2"
                                            data-toggle="tab">Grille</a></li>
                                        </ul>
                                                         <div class="tab-content">
                                                             <div class="tab-pane  fontawesome-demo" id="tab1">
                                                                 <div class="row">
                                                          <div class="col-md-12">
                                                              <div class="card  card-box">
                                                                  <div class="card-head">
                                                                      <header></header>
                                                                      <div class="tools">
                                                                          <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                                                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                                                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                                                      </div>
                                                                  </div>
                                                                  <div class="card-body ">
                                                                    <div class="pull-right">
                                                                       <a class="btn btn-primary " style="border-radius:5px;" href="addparent.php"><i class="fa fa-plus"></i> Nouveau Parent</a>
                                                                        <a class="btn btn-warning " style="border-radius:5px;" href="#"><i class="fa fa-print"></i> Liste Parent</a>

                                                                    </div>
                                                                    <div class="table-scrollable">
                                                                      <table class="table table-hover table-checkable order-column full-width" id="example4">
                                                                          <thead>
                                                                              <tr>

                                                                                  <th style="width:200px;"> Nom & Prénoms </th>
                                                                                  <th> Profession</th>
                                                                                  <th> Email </th>
                                                                                   <th> Téléphone </th>

                                                                                  <th> <?php echo L::Actions?> </th>
                                                                              </tr>
                                                                          </thead>
                                                                          <tbody>
                                                                             <?php

                                                                             if(isset($_POST['studentid']))
                                                                             {
                                                                                if(isset($_POST['parentname'])&&(strlen($_POST['parentname'])>0))
                                                                                {
                                                                                  // echo "bonjour";
                                                                                  //rechercher les parents de cet eleve avec cet identifiant
                                                                                  $allparents=$student->getParentStudentSelected($_POST['studentid'],$_POST['parentname']);
                                                                                }else {
                                                                                  //rechercher tous les parents de cet eleve
                                                                                  // echo "bonsoir";
                                                                                    $allparents=$student->getParentStudentSelectedAll($_POST['studentid']);
                                                                                }
                                                                             }

                                                                             // var_dump($allparents);
                                                                             $i=1;
                                                                               foreach ($allparents as $value):
                                                                               ?>
                                                        <tr class="odd gradeX" ondblclick="myFunction(<?php echo $value->id_compte?>)">

                                                          <td><?php echo utf8_encode(utf8_decode($value->nom_compte." ".$value->prenom_compte))?></td>


                                                          <td><?php echo utf8_encode(utf8_decode($value->fonction_compte))?></td>

                                                           <td class="left"><?php echo utf8_encode(utf8_decode($value->email_compte))?></td>
                                                          <td class="left"><?php echo utf8_encode(utf8_decode($value->tel_compte))?></td>
                                                           <td class="center">

                                                             <a href="#"  onclick="modify(<?php echo $value->id_compte?>)" class="btn btn-info btn-circle ">
                                                               <i class="fa fa-pencil"></i>
                                                             </a>


                                                             <a class="btn btn-danger btn-circle  " onclick="deleted(<?php echo $value->id_compte?>)">
                                                               <i class="fa fa-trash-o "></i>
                                                             </a>


                                                           </td>
                                                        </tr>
                                                         <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                                         <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                                         <script>

                                                         function compteMod(id)
                                                         {
                                                           Swal.fire({
                                             title: '<?php echo L::WarningLib ?>',
                                             text: "Voulez vous vraiment modifier les paramètres de connexion du Parent",
                                             type: 'warning',
                                             showCancelButton: true,
                                             confirmButtonColor: '#3085d6',
                                             cancelButtonColor: '#d33',
                                             confirmButtonText: '<?php echo L::ModifierBtn ?>',
                                             cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                           }).then((result) => {
                                             if (result.value) {
                                               document.location.href="modconfigparent.php?compte="+id;
                                             }else {

                                             }
                                           })
                                                         }

                                                         function compte(id)
                                                         {
                                                           Swal.fire({
                                             title: '<?php echo L::WarningLib ?>',
                                             text: "Voulez vous vraiment configurer les paramètres de connexion du Parent",
                                             type: 'warning',
                                             showCancelButton: true,
                                             confirmButtonColor: '#3085d6',
                                             cancelButtonColor: '#d33',
                                             confirmButtonText: 'Configurer',
                                             cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                           }).then((result) => {
                                             if (result.value) {
                                               document.location.href="configparent.php?compte="+id;
                                             }else {

                                             }
                                           })
                                                         }

                                                         function myFunction(idcompte)
                                                         {
                                                           //var url="detailslocal.php?compte="+idcompte;
                                                         document.location.href="detailsparents.php?compte="+idcompte;
                                                         }

                                                         function modify(id)
                                                         {


                                                           Swal.fire({
                                             title: '<?php echo L::WarningLib ?>',
                                             text: "Voulez vous vraiment modifier cet Parent",
                                             type: 'warning',
                                             showCancelButton: true,
                                             confirmButtonColor: '#3085d6',
                                             cancelButtonColor: '#d33',
                                             confirmButtonText: '<?php echo L::ModifierBtn ?>',
                                             cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                           }).then((result) => {
                                             if (result.value) {
                                               document.location.href="updateparent.php?compte="+id;
                                             }else {

                                             }
                                           })
                                                         }

                                                         function deleted(id)
                                                         {

                                                           Swal.fire({
                                             title: '<?php echo L::WarningLib ?>',
                                             text: "Voulez vous vraiment supprimer cet Parent",
                                             type: 'warning',
                                             showCancelButton: true,
                                             confirmButtonColor: '#3085d6',
                                             cancelButtonColor: '#d33',
                                             confirmButtonText: '<?php echo L::DeleteLib ?>',
                                             cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                           }).then((result) => {
                                             if (result.value) {
                                               document.location.href="../controller/parent.php?etape=3&compte="+id;
                                             }else {

                                             }
                                           })
                                                         }

                                                         function actived(id)
                                                         {
                                                           Swal.fire({
                                             title: '<?php echo L::WarningLib ?>',
                                             text: "Voulez vous vraiment activer cet Parent",
                                             type: 'warning',
                                             showCancelButton: true,
                                             confirmButtonColor: '#3085d6',
                                             cancelButtonColor: '#d33',
                                             confirmButtonText: '<?php echo L::ModifierBtn ?>',
                                             cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                           }).then((result) => {
                                             if (result.value) {
                                               document.location.href="../controller/parent.php?etape=4&compte="+id;
                                             }else {

                                             }
                                           })
                                                         }

                                                         </script>
                                                         <?php

                                                                                          $i++;
                                                                                          endforeach;
                                                                                          ?>

                                                      </tbody>
                                                                      </table>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                             </div>
                                                             <div class="tab-pane active" id="tab2">



                                                      <div class="row">
                                                         <?php
                                                         $i=1;
                                                           foreach ($allparents as $value):
                                                           ?>
                                                          <div class="col-md-4" ondblclick="myFunction(<?php echo $value->id_compte?>)">
                                                                <div class="card card-box">
                                                                    <div class="card-body no-padding ">
                                                                      <div class="doctor-profile">
                                                                         <?php
                                                                           $lienImg="";
                                                                           if(strlen($value->photo_compte)>0)
                                                                           {
                                                                             $lienImg="../photo/".$value->email_compte."/".$value->photo_compte;
                                                                           }else {
                                                                             $lienImg="../photo/user5.jpg";
                                                                           }
                                                                           $libellesexe="";
                                                                           $sexeparent=$value->sexe_parent;
                                                                           if($sexeparent=="M")
                                                                           {
                                                                             $libellesexe="Mr";
                                                                           }else if($sexeparent=="F")
                                                                           {
                                                                             $libellesexe="Mme";
                                                                           }
                                                                          ?>
                                                                                <img src="<?php echo $lienImg?>" class="doctor-pic" alt="" style="height:100px; width:90px;">
                                                                          <div class="profile-usertitle">
                                                                               <div class="doctor-name"><?php echo $libellesexe." ".$value->nom_compte." ".$value->prenom_compte;?> </div>
                                                                               <div class="name-center"> <?php echo $value->fonction_compte;?></div>
                                                                          </div>

                                                                           <p><?php echo $value->email_compte;?></p>
                                                                                 <div><p><i class="fa fa-phone"></i><a href="">  <?php echo $value->tel_parent; ?></a></p> </div>
                                                                           <div class="profile-userbuttons">
                                                                                <!-- <a href="#" class="btn btn-circle btn-info btn-sm" onclick="modify(<?php //echo $value->id_compte ?>)"><i class="fa fa-pencil"></i></a>
                                                                                <a href="#" class="btn btn-circle btn-danger btn-sm" onclick="deleted(<?php //echo $value->id_compte?>)"><i class="fa fa-trash"></i></a> -->
                                                                           </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                          </div>
                                                           <?php
                                                                                            $i++;
                                                                                            endforeach;
                                                                                            ?>


                                                      </div>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
  <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
  <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function searchparentname()
   {

     var etape=4;
     var codeEtab="<?php echo $codeEtabAssigner; ?>";

     $.ajax({
              url: '../ajax/parent.php',
              type: 'POST',
              async:false,
              data: 'code='+codeEtab+'&etape='+etape+'&idcompte='+$("#studentid").val(),
              dataType: 'text',
              success: function (content, statut) {

                $("#parentname").html("");
                $("#parentname").html(content);

                //nous allons chercher la liste des admins local de cet etablissement

              /*  $.ajax({
                         url: '../ajax/localadmin.php',
                         type: 'POST',
                         async:false,
                         data: 'code='+ $("#codeetab").val()+'&etape='+etape,
                         dataType: 'text',
                         success: function (response, statut) {


                           $("#adminlo").html("");
                           $("#adminlo").html(response);
                         }
                       });*/

              }

            });

   }
   $(document).ready(function() {

     $('#example5').DataTable( {
         "scrollX": true

     } );

     $("#codeetab").select2();

     $("#libetab").select2();

     $("#parentname").select2();

     $("#studentid").select2();

     $("#FormSearch").validate({

            errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
          },
          highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger')
            $(element).addClass('form-control-danger')
          },
          success: function (e) {
                $(e).closest('.control-group').removeClass('error').addClass('info');
                $(e).remove();
            },
              rules:{
                studentid:"required"
              },
              messages:{
                studentid:"Merci de selectionner un eleve"
              }
     });




   });

   </script>
    <!-- end js include path -->
  </body>

</html>
