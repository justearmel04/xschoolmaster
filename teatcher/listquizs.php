<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Student.php');
require_once('../class/Teatcher.php');
require_once('../controller/functions.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$matiere=new Matiere();
$student=new Student();
$teatcher=new Teatcher();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);

$matieres=$matiere->getAllSubjectTeatchByTeatcherId($_SESSION['user']['IdCompte']);
$controles=$matiere->getAllControleMatiereOfThisTeatcherId($_SESSION['user']['IdCompte']);


//$dataclasses=$classe->getAllClassesByClasseId($classeschoolid);

$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
  $programmes=$etabs->getAllprogrammesOfTeatcherClasses($_SESSION['user']['IdCompte'],$libellesessionencours);

  // $fiches=$etabs->getAllFicesOfTeatcherClasses($_SESSION['user']['IdCompte'],$libellesessionencours);

$fiches=$etabs->getAllcahiersOfteatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);

}else {
  $nbclasse=0;
}

//la liste des cours de ce professeur

$courses=$etabs->getAllTeatcherquizs($compteuserid,$codeEtabsession,$libellesessionencours);

// var_dump($courses);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}

#radioBtn .notActive{
color: #3276b1;
background-color: #fff;
}

#radioBtn1 .notActive{
color: #3276b1;
background-color: #fff;
}

//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title">Liste des quizs</div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="#">Quizs</a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active">Liste des quizs</li>
                          </ol>
                      </div>
                  </div>

                  <?php

                        if(isset($_SESSION['user']['addclasseok']))
                        {

                          ?>
                          <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?php
                        //echo $_SESSION['user']['addetabok'];
                        ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                           </a>
                        </div-->
      <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
      <script src="../assets/js/sweetalert2.min.js"></script>

                    <script>
                    Swal.fire({
                    type: 'success',
                    title: 'Félicitations',
                    text: '<?php echo $_SESSION['user']['addclasseok'] ?>',

                    })
                    </script>
                          <?php
                          unset($_SESSION['user']['addclasseok']);
                        }

                         ?>

                         <?php
                         if($nbsessionOn==0)
                         {
                           ?>
                           <div class="alert alert-danger alert-dismissible fade show" role="alert">

                           Vous devez definir la Sessionn scolaire

                           <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                            </a>
                           </div>
                           <?php
                         }
                          ?>
                          <div class="col-md-12">
                            <div class="pull-right">
                              <a class="btn btn-primary " href='addquiz.php'><i class="fa fa-plus"></i> <?php echo L::NewsQuizsMenu ?> </a>

                            </div>
                          </div> <br><br>

                          <?php

                          if(count($courses)>0)
                          {
                            ?>

                          <div class="row">
                              <div class="col-md-12 col-sm-12">
                                  <div class="card card-box">
                                      <div class="card-head">
                                          <header></header>

                                      </div>


                                        <div class="card-body" id="bar-parent">
                                            <div class="row">

                                              <?php
                                              foreach ($courses as  $value):
                                                ?>
                                                <div class="col-lg-3 col-md-3 col-12 col-sm-6">
                              										<div class="blogThumb">
                              											<div class="thumb-center"><img class="img-responsive" alt="user"
                              													src="../photo/course2.jpg"></div>
                              											<div class="course-box">

                                                      <div class="" style="text-align:center">
                                                        <h4>
                                                          <?php
                                                          if($value->statut_quiz==0)
                                                          {
                                                            ?>
      <span class="label label-lg label-warning"  style="text-align:center;">  <?php echo $value->libelle_quiz ?></span>
                                                            <?php
                                                          }else {
                                                            ?>
      <span class="label label-lg label-success"  style="text-align:center;">  <?php echo $value->libelle_quiz ?></span>
                                                            <?php
                                                          }
                                                           ?>

                                                        </h4>
                                                      </div>
                                                      <?php

                                                      $tabdate=explode("-",$value->datelimite_quiz);
                                                       ?>
                              												<!-- <div class="text-muted"><span class="m-r-10"><?php //echo $tabdate[2]." ".obtenirLibelleMois($tabdate[1])." ".$tabdate[0] ?></span> -->
                              													<!-- <a class="course-likes m-l-10" href="#"><i
                              															class="fa fa-heart-o"></i> 654</a> -->
                              												<!-- </div> -->
                                                      <p><span><i class="fa fa-calendar"></i> Date limite : <?php echo $tabdate[2]." ".obtenirLibelleMois($tabdate[1])." ".$tabdate[0] ?></span></p>
                              												<p><span><i class="fa fa-clock-o"></i> Durée : <?php echo returnHours($value->duree_quiz) ?></span></p>
                              												<p><span><i class=" fa fa-university"></i> Classe : <?php echo $value->libelle_classe   ?></span></p>
                                                      <p><span><i class="fa fa-graduation-cap"></i> Matière : <?php echo $value->libelle_mat ?></span></p>
                                                      <!-- <p><span><i class="fa fa-graduation-cap"></i> Students: 200+</span></p> -->
                                                      <a href="detailquizs.php?course=<?php echo $value->id_quiz ?>&classeid=<?php echo $value->classe_quiz ?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-info">DETAILS</a>
                              												<!-- <button type="button"
                              													class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-info">DETAILS</button> -->
                              											</div>
                                                    <div class="row list-separated profile-stat">
                                                        <div class="col-md-7 col-sm-7 col-7">
                                                          <?php
                                                          if($value->statut_quiz==0)
                                                          {
                                                            ?>
                                                            <p><span class=" pull-right" style="color:#FFB64D"> NON PUBLIE </span></p>
                                                            <?php
                                                          }else {
                                                            ?>
                                                            <p><span class="pull-right" style="color:#2ed8b6"> PUBLIE</span></p>
                                                            <?php
                                                          }
                                                           ?>

                                                        </div>

                                                    </div>
                              										</div>
                              									</div>
                                                <?php
                                              endforeach;
                                               ?>



                                            </div>
                                        </div>



                                  </div>
                              </div>
                          </div>
                          <?php
                        }else {
                          ?>
                          <div class="alert alert-danger alert-dismissible fade show" role="alert">

                          Aucun quiz à ce jour

                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                           </a>
                          </div>
                          <?php
                        }
                         ?>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
   	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
       <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
    <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
    <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
    <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <!--script src="../assets2/plugins/summernote/summernote.js" ></script>
    <script src="../assets2/plugins/summernote/lang/summernote-fr-FR.js" ></script-->
       <!-- calendar -->
       <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
    <script src="../assets2/plugins/moment/moment.min.js" ></script>
    <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
    <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
       <!-- Common js-->
   	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
   	<script src="../assets2/js/theme-color.js" ></script>
   	<!-- Material -->
   	<script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
    <script src="../assets2/js/dropify.js"></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>

 <script>
 /*$('#summernote').summernote({
     placeholder: '',
     tabsize: 2,
     height: 200,
      lang: 'fr-FR'
   });*/

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

 $("#fichier3").dropify({
   messages: {
       "default": "Merci de selectionner le support",
       "replace": "Modifier le support",
       "remove" : "Supprimer le support",
       "error"  : "Erreur"
   }
 });

$("#matclasse").select2();
$("#classeEtab").select2();

function searchcodeEtab(id)
{
var classe=$("#classeEtab").val();
var teatcherId=id;
var etape=7;
var matiere=$("#matclasse").val();

$.ajax({

     url: '../ajax/matiere.php',
     type: 'POST',
     async:true,
     data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
     dataType: 'text',
     success: function (content, statut) {

        $("#FormAddAcademique #codeEtab").val(content);

     }
   });

}

function searchmatiere(id)
{

  var classe=$("#classeEtab").val();
  var teatcherId=id;
  var etape=6;


$.ajax({

     url: '../ajax/matiere.php',
     type: 'POST',
     async:true,
     data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe,
     dataType: 'text',
     success: function (content, statut) {


       $("#matclasse").html("");
       $("#matclasse").html(content);

     }
   });
}

function deletedTache(id)
{
var concattache=$("#concattache").val();

$("#concattache").val($("#concattache").val().replace(id+"@", ""));

 $('#rowTache'+id+'').remove();

 recalcultachenb();
}

function deletedComp(id)
{
var concatcomp=$("#concatcomp").val();

$("#concatcomp").val($("#concatcomp").val().replace(id+"@", ""));

 $('#rowComp'+id+'').remove();

 // recalculsectionnb();

 var concatcomp=$("#concatcomp").val();

 var tab=concatcomp.split("@");

 var nbtab=tab.length;

 var nbtabnew=parseInt(nbtab)-1;

 $("#concatnbcomp").val(nbtabnew);

}

function deletedTaches(id)
{
var concattaches=$("#concattaches").val();

$("#concattaches").val($("#concattaches").val().replace(id+"@", ""));

 $('#rowTaches'+id+'').remove();

 // recalculsectionnb();

 var concattaches=$("#concattaches").val();

 var tab=concattaches.split("@");

 var nbtab=tab.length;

 var nbtabnew=parseInt(nbtab)-1;

 $("#concatnbtaches").val(nbtabnew);

}


function deletedSection(id)
{
var concatsection=$("#concatsection").val();

$("#concatsection").val($("#concatsection").val().replace(id+"@", ""));

 $('#rowSection'+id+'').remove();

 // recalculsectionnb();

 var concatsection=$("#concatsection").val();

 var tab=concatsection.split("@");

 var nbtab=tab.length;

 var nbtabnew=parseInt(nbtab)-1;

 $("#concatnbsection").val(nbtabnew);

}

function recalcultachenb()
{
var concattache=$("#concattache").val();

var tab=concattache.split("@");

var nbtab=tab.length;

var nbtabnew=parseInt(nbtab)-1;

$("#concatnbtache").val(nbtabnew);
}

function recalculsectionnb()
{

}


function AddtachesRow()
{
var nb=$("#nbtaches").val();
var nouveau= parseInt(nb)+1;
$("#nbtaches").val(nouveau);

  var concattaches=$("#concattaches").val();
  $("#concattaches").val(concattaches+nouveau+"@");

  var concattaches=$("#concattaches").val();

  var tab=concattaches.split("@");

  var nbtab=tab.length;

  var nbtabnew=parseInt(nbtab)-1;

  $("#concatnbtaches").val(nbtabnew);

  // recalculsectionnb();

  if(nbtabnew==1)
  {
    $('#dynamic_field2').append('<tr id="rowTaches'+nouveau+'"><td><input type="text" name="taches_'+nouveau+'" id="taches_'+nouveau+'" placeholder="<?php echo L::EnterNewExercice ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteTaches'+nouveau+'" id="deleteTaches'+nouveau+'"  onclick="deletedTaches('+nouveau+')"  class="btn btn-danger btn_remove" disabled="disabled">X</button></td></tr>');
  }else {
    $('#dynamic_field2').append('<tr id="rowTaches'+nouveau+'"><td><input type="text" name="taches_'+nouveau+'" id="taches_'+nouveau+'" placeholder="<?php echo L::EnterNewExercice ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteTaches'+nouveau+'" id="deleteTaches'+nouveau+'"  onclick="deletedTaches('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');
  }



  for(var i=0;i<nbtabnew;i++)
  {
    var indice=tab[i];
    // alert(indice);
    $("#taches_"+indice).rules( "add", {
        required: true,
        messages: {
        required: "<?php echo L::AddNewexerciceplease ?>"
}
      });
  }


}

function AddcompRow()
{
var nb=$("#nbcomp").val();
var nouveau= parseInt(nb)+1;
$("#nbcomp").val(nouveau);

  var concatcomp=$("#concatcomp").val();
  $("#concatcomp").val(concatcomp+nouveau+"@");

  var concatcomp=$("#concatcomp").val();

  var tab=concatcomp.split("@");

  var nbtab=tab.length;

  var nbtabnew=parseInt(nbtab)-1;

  $("#concatnbcomp").val(nbtabnew);

  // recalculsectionnb();

  if(nbtabnew==1)
  {
    $('#dynamic_field1').append('<tr id="rowComp'+nouveau+'"><td><input type="text" name="comp_'+nouveau+'" id="comp_'+nouveau+'" placeholder="<?php echo L::AddCompViseeRequired ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteComp'+nouveau+'" id="deleteComp'+nouveau+'"  onclick="deletedComp('+nouveau+')"  class="btn btn-danger btn_remove" disabled="disabled">X</button></td></tr>');
  }else {
    $('#dynamic_field1').append('<tr id="rowComp'+nouveau+'"><td><input type="text" name="comp_'+nouveau+'" id="comp_'+nouveau+'" placeholder="<?php echo L::AddCompViseeRequired ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteComp'+nouveau+'" id="deleteComp'+nouveau+'"  onclick="deletedComp('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');
  }



  for(var i=0;i<nbtabnew;i++)
  {
    var indice=tab[i];
    // alert(indice);
    $("#comp_"+indice).rules( "add", {
        required: true,
        messages: {
        required: "<?php echo L::AddCompetenceViseeplease ?>"
}
      });
  }


}

function AddsectionRow()
{
var nb=$("#nb").val();
var nouveau= parseInt(nb)+1;
$("#nb").val(nouveau);

  var concatsection=$("#concatsection").val();
  $("#concatsection").val(concatsection+nouveau+"@");

  var concatsection=$("#concatsection").val();

  var tab=concatsection.split("@");

  var nbtab=tab.length;

  var nbtabnew=parseInt(nbtab)-1;

  $("#concatnbsection").val(nbtabnew);

  // recalculsectionnb();

  if(nbtabnew==1)
  {
    $('#dynamic_field').append('<tr id="rowSection'+nouveau+'"><td><input type="text" name="section_'+nouveau+'" id="section_'+nouveau+'" placeholder="<?php echo L::EnterSection ?>" class="form-control objectif_list" /></td><td><button type="button" id="deleteSection'+nouveau+'" id="deleteSection'+nouveau+'"  onclick="deletedSection('+nouveau+')"  class="btn btn-danger btn_remove" disabled="disabled">X</button></td></tr>');
  }else {
    $('#dynamic_field').append('<tr id="rowSection'+nouveau+'"><td><input type="text" name="section_'+nouveau+'" id="section_'+nouveau+'" placeholder="<?php echo L::EnterSection ?>" class="form-control objectif_list" /></td><td><button type="button" id="deleteSection'+nouveau+'" id="deleteSection'+nouveau+'"  onclick="deletedSection('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');
  }



  for(var i=0;i<nbtabnew;i++)
  {
    var indice=tab[i];
    // alert(indice);
    $("#section_"+indice).rules( "add", {
        required: true,
        messages: {
        required: "<?php echo L::AddSectionplease ?>"
}
      });
  }


}

function AddtacheRow()
{
var nb=$("#nbtache").val();
var nouveau= parseInt(nb)+1;
$("#nbtache").val(nouveau);

  var concattache=$("#concattache").val();
  $("#concattache").val(concattache+nouveau+"@");

  recalcultachenb();

  $('#dynamic_field1').append('<tr id="rowTache'+nouveau+'"><td><input type="text" name="tache_'+nouveau+'" id="tache_'+nouveau+'" placeholder="Entrer une tache" class="form-control objectif_list" /></td><td><button type="button" id="deleteTache'+nouveau+'" id="deleteTache'+nouveau+'"  onclick="deletedTache('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');

  for(var i=1;i<=nouveau;i++)
  {
    $("#tache_"+i).rules( "add", {
        required: true,
        messages: {
        required: "<?php echo L::RequiredChamp ?>"
}
      });
  }


}

 $(document).ready(function() {

   $("#FormAddAcademique").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
    },
    highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
    },
    success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
     rules:{

       programme:"required",
       descri:"required",
       classeEtab:"required",
       matclasse:"required",
       fichier:"required",
       durationcourse:"required",
       detailscourse:"required",
       datecourse:"required"

     },
   messages: {
     programme:"Merci de renseigner le libellé du programme",
     descri:"Merci de renseigner le détails de la fiche ",
     classeEtab:"<?php echo L::PleaseSelectclasseOnerequired ?>",
     matclasse:"Merci de <?php echo L::SelectSubjects ?>",
     fichier:"Merci de selectionner le fichier du programme",
     durationcourse:"<?php echo L::DurationcourseRequired ?>",
     detailscourse:"<?php echo L::DetailscourseRequired ?>",
     datecourse:"<?php echo L::DatecourseRequired ?>"
   },
   submitHandler: function(form) {

   form.submit();



   }

   });

   AddsectionRow();
    AddcompRow();

   $('#add').click(function(){

     //creation d'une ligne de section

     AddsectionRow();

   });

   $('#addcomp').click(function(){

     //creation d'une ligne de section

     AddcompRow();

   });

   $('#addtache').click(function(){

     //creation d'une ligne de section

     AddtachesRow();

   });






 });

 </script>
    <!-- end js include path -->
  </body>

</html>
