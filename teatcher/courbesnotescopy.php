<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Teatcher.php');
require_once('../class/Matiere.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$matiere=new Matiere();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$student=new Student();
$localadmins= new Localadmin();
$parents=new ParentX();
$classe=new Classe();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$_SESSION['user']['codeEtab']=$codeEtabsession;

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $_SESSION['user']['session']=$libellesessionencours;
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
$typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);

  $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
}else {
  $nbclasse=0;
}

$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);



if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->

	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
     <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>


	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                      <div class="page-bar">
                          <div class="page-title-breadcrumb">
                              <div class=" pull-left">
                                  <div class="page-title"><?php echo L::CourbesNotes ?></div>
                              </div>
                              <ol class="breadcrumb page-breadcrumb pull-right">
                                  <li><!--i class="fa fa-home"></i-->&nbsp;<a class="parent-item" href="index.php"><?php echo L::EvalnotesMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                  </li>
                                  <li class="active"><?php echo L::CourbesNotes ?></li>
                              </ol>
                          </div>
                      </div>
                      <div class="state-overview">
            						<div class="row">
                          <?php

                                if(isset($_SESSION['user']['addattendailyok']))
                                {

                                  ?>
                                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                                <?php
                                //echo $_SESSION['user']['addetabok'];
                                ?>
                                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                   </a>
                                </div-->
                        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                        <script src="../assets/js/sweetalert2.min.js"></script>

                            <script>
                            Swal.fire({
                            type: 'success',
                            title: 'Félicitations',
                            text: '<?php echo $_SESSION['user']['addattendailyok']; ?>',

                            })
                            </script>
                                  <?php
                                  unset($_SESSION['user']['addattendailyok']);
                                }

                                 ?>
            					      </div>
            						</div>
            					<!-- end widget -->
                      <?php

                            if(isset($_SESSION['user']['addetabexist']))
                            {

                              ?>
                              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <?php
                            echo $_SESSION['user']['addetabexist'];
                            ?>
                            <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                               </a>
                            </div>



                              <?php
                              unset($_SESSION['user']['addetabexist']);
                            }

                             ?>

            <br/>

            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card card-topline-green">
                     <div class="card-head">
                         <header></header>
                         <div class="tools">
                             <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
              <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
              <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                         </div>
                     </div>
                     <div class="card-body ">
                       <form method="post" id="FormSearch" action="courbesnotes.php">
                           <div class="row">
                             <div class="col-md-6 col-sm-6">
                               <div class="form-group" style="margin-top:8px;">
                                   <label>Classe</label>
                                   <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                                   <select class="form-control input-height" id="classeEtab" name="classeEtab" style="width:100%;" onchange="selectStudent()">
                                       <option value=""><?php echo L::Selectclasses ?></option>
                                       <?php
                                       $i=1;
                                         foreach ($classes as $value):
                                         ?>
                                         <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                         <?php
                                                                          $i++;
                                                                          endforeach;
                                                                          ?>

                                   </select>
                               </div>
                             <!-- text input -->



                         </div>
                         <div class="col-md-6 col-sm-6">
                           <div class="form-group" style="margin-top:8px;">
                               <label><?php echo L::MatiereMenusingle ?></label>
                               <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                               <select class="form-control " id="matclasse" name="matclasse" style="width:100%;">
                                   <option value=""><?php echo L::SelectSubjects ?></option>


                               </select>
                           </div>
                         <!-- text input -->



                     </div>
                     <div class="col-md-6 col-sm-6">
                       <div class="form-group" style="margin-top:8px;">
                           <label><?php echo L::Period ?></label>
                           <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                           <select class="form-control " id="periode" name="periode" style="width:100%;">
                               <option value=""><?php echo L::SelectedPeriod ?></option>
                               <?php
                               $i=1;
                                 foreach ($typesemestre as $value):
                                 ?>
                                 <option value="<?php echo $value->id_semes?>" selected><?php echo utf8_encode(utf8_decode($value->libelle_semes)) ?></option>

                                 <?php
                                                                  $i++;
                                                                  endforeach;
                                                                  ?>

                           </select>
                       </div>
                     <!-- text input -->



                 </div>
                         <div class="col-md-6 col-sm-6">
                           <div class="form-group" style="margin-top:8px;">
                               <label>Elève</label>
                               <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                               <select class="form-control input-height" id="studentclasse" name="studentclasse" style="width:100%;">
                                   <option value="">Selectionner un élève</option>


                               </select>
                               <input type="hidden" name="codeEtab" id="codeEtab"  value="<?php echo $codeEtabsession ?>">
                               <input type="hidden" name="sessionEtab" id="sessionEtab" value="<?php echo $libellesessionencours ?>" />
                                <input type="hidden" name="search" id="search" />
                           </div>
                         <!-- text input -->



                     </div>



                     <button type="submit" class="btn btn-success"  style="width:150px;height:35px;margin-top:37px;"><?php echo L::Displaying ?></button>

                           </div>


                       </form>
                     </div>
                 </div>
                          </div>

            </div>
  <!-- element à faire apparaitre au clique du bouton rechercher -->
  <div class="row" style="" id="affichage">
    <?php
        if(isset($_POST['search']))
        {

          if(isset($_POST['studentclasse'])&& (strlen($_POST['studentclasse'])>0))
          {

            //nous avons selectionner un eleve
            $datastudent=$student->getAllInformationsOfStudent($_POST['studentclasse'],$_POST['sessionEtab']);
            $tabdatasutent=explode("*",$datastudent);
            $matriculestudent=$tabdatasutent[1];



            $tabStutentName=array();
            $tabStudentAbsent=array();
            $tabStudentPresent=array();


            if(isset($_POST['matclasse'])&& (strlen($_POST['matclasse'])>0))
            {
              // echo "Labas";
              //nous avons spécifier une matière

              $tabmatiere=explode("-",$_POST['matclasse']);
              $matiereid=$tabmatiere[0];

              //nous allons chercher la liste des notes obtenues par cet eleve

              $datanotes=$student->getNotesOfstudents($_POST['studentclasse'],$matiereid,$_POST['codeEtab'],$_POST['sessionEtab'],$_POST['classeEtab']);
              $j=1;
              foreach ($datanotes as $value):
                $typenote=$value->type_notes;
                $idtypenote=$value->idtype_notes;

                if($typenote==1)
                {
                  //il est question d'un controle
                  $nbcontrole=$student->getcontrolesInfosNb($idtypenote,$matiereid,$_POST['codeEtab'],$_POST['sessionEtab'],$_POST['periode']);
                  if($nbcontrole>0)
                  {
                    $datacontroles=$student->getcontrolesInfos($idtypenote,$matiereid,$_POST['codeEtab'],$_POST['sessionEtab'],$_POST['periode']);
                    foreach ($datacontroles as  $values):
                      $tabStutentName[$j]=$values->libelle_ctrl;
                      // $tabStudentAbsent[$j]=$nbabsencedays;
                      $tabStudentPresent[$j]=$values->valeur_notes;
                    endforeach;
                  }

                }else if($typenote==2)
                {
                  // il est question d'un examen
                  $nbexamens=$student->getexamenInfosNb($idtypenote,$matiereid,$_POST['codeEtab'],$_POST['sessionEtab'],$_POST['periode']);
                  if($nbexamens>0)
                  {
                    $datacontroles=$student->getexamenInfos($idtypenote,$matiereid,$_POST['codeEtab'],$_POST['sessionEtab'],$_POST['periode']);
                    foreach ($datacontroles as  $values):
                      $tabStutentName[$j]=$values->libelle_exam;
                      // $tabStudentAbsent[$j]=$nbabsencedays;
                      $tabStudentPresent[$j]=$values->valeur_notes;
                    endforeach;

                  }
                }

                $j++;
              endforeach;

             $outp = "";

             for($k=1;$k<$j;$k++)
             {
               $libelle=$tabStutentName[$k];
               $notevalue=$tabStudentPresent[$k];
               if ($outp != "") {$outp .= ",";}
               $outp .= '{y:"'.$libelle. '",';
               $outp .= 'a:'.$notevalue. '}';
             }

             echo $outp;

            }

            ?>

            <div class="offset-md-4 col-md-4"  id="affichage1">
              <div class="card" style="">
              <div class="card-body">
                <h5 class="card-title"></h5>
                <h4 style="font-style: italic;font-weight: bold;text-align:center;font-size:font-size: large;"><?php echo L::CourbesNotes ?></h4>
                <p class="card-text" style="text-align:center;font-weight: bold;"><?php echo $tabdatasutent[2]." ".$tabdatasutent[3]; ?></p>
                <p class="card-text" style="text-align:center;"> <i class="fa fa-pie-chart fa-3x"></i> </p>

              </div>
            </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="affichage2">
                                    <div class="card card-topline-green">
                                        <div class="card-head">
                                            <header></header>
                                            <div class="tools">
                                              <button class="btn btn-md btn-warning" id="print-chart-btn1"><i class="fa fa-print"></i> <?php echo L::Printer ?> </button>
                                            </div>
                                        </div>
                                        <div class="card-body" id="chartjs_pie_parent">

                                          <?php
                                           var_dump($tabStutentName);
                                           var_dump($tabStudentPresent);
                                           // $tabStutentName=array();
                                           // $tabStudentAbsent=array();
                                           // $tabStudentPresent=array();
                                           ?>

                                          <div class="row">
                                               <canvas id="chartjs_classe" height="120"></canvas>
                                          </div>



                                </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                           <div class="card  card-box">
                               <div class="card-head">
                                   <header>LINE CHART</header>
                                   <div class="tools">
                                       <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                     <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                     <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                   </div>
                               </div>
                               <div class="card-body " id="chartjs_line_parent">
                                   <div class="row">
                                       <canvas id="chartjs_line"></canvas>
                                   </div>
                               </div>
                           </div>
                       </div>

            <?php

          }

                }
            ?>



            </div>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
  <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
      <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
    <script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
    <script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>

    <!-- morris chart -->
        <script src="../assets2/plugins/morris/morris.min.js" ></script>
        <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <!--Chart JS-->
    <script src="../assets2/plugins/chart-js/Chart.bundle.js" ></script>
    <script src="../assets2/plugins/chart-js/utils.js" ></script>
    <!-- <script src="../assets2/js/pages/chart/chartjs/chartjs-data.js" ></script> -->
    <!-- <script src="../assets2/js/pages/chart/morris/morris_chart_data.js" ></script> -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
  <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <script src="../assets2/js/jspdf.min.js" ></script>
     <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
  <?php
  if(isset($_POST))
  {
    ?>

  <?php
  }
  ?>

  var date = new Date();
  var newDate = new Date(date.setTime( date.getTime() + (0 * 86400000)));

  $('#datedeb').bootstrapMaterialDatePicker
  ({
   date:true,
   shortTime: false,
   time:false,
   maxDate:newDate,
   format :'DD-MM-YYYY',
   lang: 'fr',
  cancelText: '<?php echo L::AnnulerBtn ?>',
  okText: '<?php echo L::Okay ?>',
  clearText: '<?php echo L::Eraser ?>',
  nowText: '<?php echo L::Now ?>'

  });

  $('#datefin').bootstrapMaterialDatePicker
  ({
   date:true,
   shortTime: false,
   time:false,
   maxDate:newDate,
   format :'DD-MM-YYYY',
   lang: 'fr',
  cancelText: '<?php echo L::AnnulerBtn ?>',
  okText: '<?php echo L::Okay ?>',
  clearText: '<?php echo L::Eraser ?>',
  nowText: '<?php echo L::Now ?>'

  });

  function selectStudent()
  {
  var classe=$("#classeEtab").val();
  var codeEtab="<?php echo $codeEtabsession ?>";
  var session="<?php echo $libellesessionencours; ?>";
  var etape=3;

  $.ajax({
           url: '../ajax/admission.php',
           type: 'POST',
           async:false,
           data: 'codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&classe='+classe,
           dataType: 'text',
           success: function (response, statut) {


             $("#studentclasse").html("");
             $("#studentclasse").html(response);


             var etape1=5;

             $.ajax({

               url: '../ajax/matiere.php',
               type: 'POST',
               async:true,
                data:'etape=' + etape1+'&classe='+classe+'&code='+codeEtab,
                dataType: 'text',
                success: function (content, statut) {

                  $("#matclasse").html("");
                  $("#matclasse").html(content);

                }

             });

           }
         });

  }

   $('#example5').DataTable( {
       "scrollX": true

   } );
   $("#classeEtab").select2();
   $("#studentclasse").select2();
   $("#month").select2();
   $("#yearsession").select2();
   $("#periode").select2();
   $("#matclasse").select2();


   $('#datepre').bootstrapMaterialDatePicker
   ({
     date: true,
     time: false,
     format: 'DD/MM/YYYY',
     lang: 'fr',
     minDate : new Date(),
    cancelText: '<?php echo L::AnnulerBtn ?>',
    okText: '<?php echo L::Okay ?>',
    clearText: '<?php echo L::Eraser ?>',
    nowText: '<?php echo L::Now ?>'

   });
   $(document).ready(function() {

  $("#FormSearch").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
  },
  highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
  },
  success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    classeEtab:"required",
    month:"required",
    yearsession:"required",
    datefin:"required",
    datedeb:"required",
    periode:"required",
    matclasse:"required",

    studentclasse:"required"



  },
  messages: {
    classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
    month:"<?php echo L::PleaseSelectAnMonth ?>",
    yearsession:"Merci de selection l'année de session",
    datefin:"<?php echo L::PleaseEnterDatedebLib ?>",
    datedeb:"<?php echo L::PleaseEnterDatefinLib ?>",
    periode:"<?php echo L::PeriodRequired ?>",
    matclasse:"Merci de <?php echo L::SelectSubjects ?>",

    studentclasse:"<?php echo L::PleaseSelectOneStudent ?>"

  },
  submitHandler: function(form) {
    form.submit();
  }
  });

  $('#print-chart-btn').on('click', function() {
  var canvas = document.querySelector("#chartjs_student");
  var canvas_img = canvas.toDataURL("image/png",1.0); //JPEG will not match background color
  var pdf = new jsPDF('landscape','in', 'letter'); //orientation, units, page size
  pdf.addImage(canvas_img, 'png', .5, 1.75, 10, 5); //image, type, padding left, padding top, width, height
  pdf.autoPrint(); //print window automatically opened with pdf
  var blob = pdf.output("bloburl");
  window.open(blob);
  });

  $('#print-chart-btn1').on('click', function() {
  var canvas = document.querySelector("#chartjs_classe");
  var canvas_img = canvas.toDataURL("image/png",1.0); //JPEG will not match background color
  var pdf = new jsPDF('landscape','in', 'letter'); //orientation, units, page size
  pdf.addImage(canvas_img, 'png', .5, 1.75, 10, 5); //image, type, padding left, padding top, width, height
  pdf.autoPrint(); //print window automatically opened with pdf
  var blob = pdf.output("bloburl");
  window.open(blob);
  });


  <?php
  // if(isset($_POST['studentclasse'])&& (strlen($_POST['studentclasse'])>0))
  // {
  ?>


  <?php
  // }else {
  ?>
  var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var color = Chart.helpers.color;
    var barChartData = {
        labels: [ <?php

          @$compte=count($tabStutentName);
                          for($i=1;$i<=@$compte;$i++)
                          {
                            echo '"'.@$tabStutentName[$i].'",';
                          }
          ?>],
        datasets: [
        //{
        //     label: 'Absent',
        //     backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
        //     borderColor: window.chartColors.red,
        //     borderWidth: 1,
        //     data: [
        //       <?php
        //       @$compte1=count($tabStudentAbsent);
        //                         for($i=1;$i<=@$compte1;$i++)
        //                         {
        //                           echo '"'.@$tabStudentAbsent[$i].'",';
        //                         }
        //         ?>
        //     ]
        // },
         {
            label: 'Notes',
            backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
            borderColor: window.chartColors.blue,
            borderWidth: 1,
            data: [
              <?php
              @$compte2=count($tabStudentPresent);
                                for($i=1;$i<=@$compte2;$i++)
                                {
                                  echo '"'.@$tabStudentPresent[$i].'",';
                                }
                ?>
            ]
        }]

    };

        var ctx = document.getElementById("chartjs_classe").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'line',
            data: barChartData,
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },
                title: {
                    display: true

                }
            }
        });
  <?php
  // }
  ?>



    //eleves

    var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var config = {
        type: 'line',
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [{
                label: "New Students",
                backgroundColor: window.chartColors.red,
                borderColor: window.chartColors.red,
                data: [
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor()
                ],
                fill: false,
            }
        },
        options: {
            responsive: true,
            title:{
                display:true,
                text:''
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Month'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Students'
                    }
                }]
            }
        }
    };
    var ctx = document.getElementById("chartjs_line").getContext("2d");
    window.myLine = new Chart(ctx, config);




  });


   </script>
    <!-- end js include path -->
  </body>

  </html>
