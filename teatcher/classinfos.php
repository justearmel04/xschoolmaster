<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Teatcher.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Student.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();
$student=new Student();
$emailUti=$_SESSION['user']['email'];

$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$localadmins= new Localadmin();
$parents=new ParentX();
$classe=new Classe();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);



$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

$etablissementType=$etabs->DetermineTypeEtab($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];


  $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
}else {
  $nbclasse=0;
}

$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);



if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();

$students=$student->getAllStudentOfClassesId($_GET['classe'],$libellesessionencours);


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"> <?php echo $classe->getInfosofclassesbyId($_GET['classe'],$libellesessionencours); ?> - <?php echo L::InfosclassesEleves ?> </div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="#"><?php echo L::studMenu ?>s</a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::GeneralInfostudents ?></li>
                          </ol>
                      </div>
                  </div>

                  <?php

                        if(isset($_SESSION['user']['updateteaok']))
                        {

                          ?>
                          <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?php
                        //echo $_SESSION['user']['addetabok'];
                        ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                           </a>
                        </div-->
                <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                <script src="../assets/js/sweetalert2.min.js"></script>

                    <script>
                    Swal.fire({
                    type: 'success',
                    title: 'Félicitations',
                    text: '<?php echo $_SESSION['user']['updateteaok']; ?>',

                    })
                    </script>
                          <?php
                          unset($_SESSION['user']['updateteaok']);
                        }

                         ?>
                  <!-- end widget -->
                  <?php

                        if(isset($_SESSION['user']['addetabexist']))
                        {

                          ?>
                          <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <?php
                        echo $_SESSION['user']['addetabexist'];
                        ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                           </a>
                        </div>



                          <?php
                          unset($_SESSION['user']['addetabexist']);
                        }

                         ?>




                         <div class="row">
                                                 <div class="col-md-12">
                                                     <div class="tabbable-line">
                                                        <!--ul class="nav nav-pills nav-pills-rose">
                                          <li class="nav-item tab-all"><a class="nav-link active show"
                                            href="#tab1" data-toggle="tab">Liste</a></li>
                                          <li class="nav-item tab-all"><a class="nav-link" href="#tab2"
                                            data-toggle="tab">Grille</a></li>
                                        </ul-->
                                                         <div class="tab-content">
                                                             <div class="tab-pane active fontawesome-demo" id="tab1">
                                                                 <div class="row">
                                                          <div class="col-md-12">
                                                              <div class="card  card-box">
                                                                  <div class="card-head">
                                                                      <header></header>
                                                                      <div class="tools">
                                                                          <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                                                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                                                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                                                      </div>
                                                                  </div>
                                                                  <div class="card-body ">
                                                                     <div class="pull-right">

                                                                       <?php
                                                                       if($etablissementType==2)
                                                                       {
                                                                        ?>
                                                                       <a class="btn btn-warning btn-md " href="#"  onclick="classeliste()" style="border-radius:5px;"><i class="fa fa-print"></i><?php echo L::ClassesListsingle ?></a>
                                                                       <?php
                                                                     }else if($etablissementType==1)
                                                                     {
                                                                       ?>
                                                                       <a class="btn btn-warning btn-md " href="#"  onclick="classelistesmall()" style="border-radius:5px;"><i class="fa fa-print"></i><?php echo L::ClassesListsingle ?></a>
                                                                       <?php
                                                                     }else if($etablissementType==4)
                                                                     {
                                                                       ?>
                                                                       <a class="btn btn-warning btn-md " href="#"  onclick="classelistesmallbig()" style="border-radius:5px;"><i class="fa fa-print"></i><?php echo L::ClassesListsingle ?></a>
                                                                       <?php
                                                                     }else if($etablissementType==3)
                                                                     {
                                                                       ?>
                                                                         <a class="btn btn-warning btn-md " href="#"  onclick="classelistesmallbig()" style="border-radius:5px;"><i class="fa fa-print"></i><?php echo L::ClassesListsingle ?></a>
                                                                       <?php
                                                                     }
                                                                        ?>
                                                                     </div>

                                                                    <div class="table-scrollable">
                                                                       <?php

                                                                        ?>
                                                                      <table class="table table-hover table-checkable order-column full-width" id="example45">
                                                                          <thead>
                                                                              <tr>
                                                                                <th style="display:none"> <?php echo L::ClassesMenu?> </th>
                                                                                 <th class="noExport"> <?php echo L::Pictures ?> </th>
                                                                                  <th> <?php echo L::MatriculestudentTab ?> </th>

                                                                                  <th> <?php echo L::NamestudentTab ?></th>
                                                                                    <th><?php echo L::EmailstudentTab ?></th>
                                                                                   <th class="noExport"> <?php echo L::Actions ?> </th>
                                                                              </tr>
                                                                          </thead>
                                                                          <tbody>
                                                                             <?php
                                                                             $i=1;

                         //var_dump($teatchers);
                                                                               foreach ($students as $value):
                                                                               ?>
                                                        <tr class="odd gradeX" ondblclick="myFunction(<?php echo $value->id_compte?>)">
                                                          <td style="display:none"><?php echo $value->libelle_classe ?></td>
                                                           <td class="patient-img">
                                                             <?php

                                                             if(strlen($value->photo_compte)>0)
                                                             {
                                                               $lien="../photo/Students/".$value->matricule_eleve."/".$value->photo_compte;
                                                             }else {
                                                               $lien="../photo/user5.jpg";
                                                             }

                                                              ?>
                                                              <img src="<?php echo $lien?>" alt="">
                                                          </td>

                                                          <td style="width:150px;"><?php
                                                           //echo $value->codeEtab_classe;

                                                           echo $value->matricule_eleve;

                                                           ?></td>

                                                           <td>
                                                             <?php
                                                             $statut=$value->statut_classe;

                                                             echo $value->nom_eleve." ".$value->prenom_eleve;

                                                             ?>
                                                         </td>
                                                          <td class="left"><?php echo $value->email_eleve;?></td>
                                                           <td class="center">


                                                             <!--a href="recapnotesstudent.php?idcompte=<?php //echo $value->id_compte?>&classe=<?php //echo $_GET['classe'];?>&codeEtab=<?php //echo $codeEtabAssigner; ?>" class="btn btn-warning btn-xs" title="Recap des notes"> <i class="fa fa-eye" ></i> </a-->

                                                               <a href="recapnotesstudent.php?idcompte=<?php echo $value->id_compte ?>&classe=<?php echo $_GET['classe'] ?>&codeEtab=<?php echo $value->codeEtab_classe ?>" class="btn btn-primary btn-xs" title="<?php echo L::RecapNotesMenu ?>"> <i class="fa fa-eye" ></i> </a>
                                                             <a title="<?php echo L::ParentsLists ?>" class="btn btn-warning btn-xs" href="parentstudents.php?idcompte=<?php echo $value->id_compte ?>&classe=<?php echo $value->id_classe  ?>&codeEtab=<?php echo $value->codeEtab_classe ?>"><i class="fa fa-users"></i></a>
                                                             <a href="recapattendanceclassestudent.php?idcompte=<?php echo $value->id_compte ?>&classe=<?php echo $_GET['classe'] ?>&codeEtab=<?php echo $value->codeEtab_classe ?>" class="btn btn-success btn-xs" title="<?php echo L::RecapsMenu ?>"> <i class="fa fa-history" ></i> </a>
                                                             <!--a href="disciplinesclasses.php?classe=<?php //echo $value->idclasse_inscrip;?>&codeEtab=<?php //echo $codeEtabAssigner;?>" class="btn btn-success btn-xs" title="disciplines de classe"> <i class=" fa fa-bar-chart-o"></i> </a-->

                                                          </td>
                                                        </tr>
                                                         <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                                         <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                                         <script>

                                                         function myFunction(idcompte)
                                                         {
                                                           //var url="detailslocal.php?compte="+idcompte;
                                                         document.location.href="detailsstudent.php?compte="+idcompte;
                                                         }





                                                         </script>


                                                         <?php
                                                                                          $i++;
                                                                                          endforeach;
                                                                                          ?>

                                                      </tbody>
                                                                      </table>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                             </div>

                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
 <script src="../cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" ></script>
 	<script src="../cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
 	<script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" ></script>
 	<script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" ></script>
 	<script src="../cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function classeliste()
   {
     //listeEnseignant.php?$codeEtabAssigner=<?php //echo $codeEtabAssigner?>

     var url="listedeclasse.php?idClasse=<?php  echo $_GET['classe']?>&codeEtab=<?php echo $_GET['codeEtab']?>&session=<?php echo $libellesessionencours ?>";

     window.open(url, '_blank');



   }

   function classelistesmall()
   {
     var url="listedeclasses.php?idClasse=<?php  echo $_GET['classe']?>&codeEtab=<?php echo $_GET['codeEtab']?>&session=<?php echo $libellesessionencours ?>";

     window.open(url, '_blank');
   }

   function classelistesmallbig()
   {
     var url="listeclasses.php?idClasse=<?php  echo $_GET['classe']?>&codeEtab=<?php echo $_GET['codeEtab']?>&session=<?php echo $libellesessionencours ?>";

     window.open(url, '_blank');
   }

   function searchlibetab()
   {

     var etape=2;

     $.ajax({
              url: '../ajax/school.php',
              type: 'POST',
              async:false,
              data: 'code='+ $("#codeetab").val()+'&etape='+etape,
              dataType: 'text',
              success: function (content, statut) {

                $("#libetab").html("");
                $("#libetab").html(content);

                //nous allons chercher la liste des admins local de cet etablissement

                $.ajax({
                         url: '../ajax/classe.php',
                         type: 'POST',
                         async:false,
                         data: 'code='+ $("#codeetab").val()+'&etape='+etape,
                         dataType: 'text',
                         success: function (response, statut) {


                           $("#classex").html("");
                           $("#classex").html(response);
                         }
                       });

              }

            });

   }
   $(document).ready(function() {

     $('#example5').DataTable( {
         "scrollX": true

     } );

     $('#example45').DataTable( {

       "scrollX": true,
       "language": {
           "lengthMenu": "_MENU_  ",
           "zeroRecords": "Aucune correspondance",
           "info": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
           "infoEmpty": "Aucun enregistrement disponible",
           "infoFiltered": "(filtered from _MAX_ total records)",
           "sEmptyTable":"Aucune donnée disponible dans le tableau",
            "sSearch":"Rechercher :",
            "oPaginate": {
       "sFirst":    "Premier",
       "sLast":     "Dernier",
       "sNext":     "Suivant",
       "sPrevious": "Précédent"
     }
     },

         dom: 'Bfrtip',
         buttons: [
             // 'copyHtml5',

             // 'excelHtml5',
             {
               extend: 'excelHtml5',
               title: 'Data export',
               exportOptions: {
                             columns: "thead th:not(.noExport)"
                         }
             }
             // 'csvHtml5',
             // 'pdfHtml5'
         ]
     } );

     $("#codeetab").select2();

     $("#libetab").select2();

   });

   </script>
    <!-- end js include path -->
  </body>

</html>
