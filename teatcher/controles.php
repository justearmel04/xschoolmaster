<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Teatcher.php');
require_once('../controller/functions.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}


$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$localadmins= new Localadmin();
$parents=new ParentX();
$matiere=new Matiere();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);

$matieres=$matiere->getAllSubjectTeatchByTeatcherId($_SESSION['user']['IdCompte']);
$controles=$matiere->getAllControleMatiereOfThisTeatcherId($_SESSION['user']['IdCompte']);
//$dataclasses=$classe->getAllClassesByClasseId($classeschoolid);
$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$etablissementType=$etabs->DetermineTypeEtab($codeEtabsession);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($etablissementType==1||$etablissementType==3)
{
  if($nbsessionOn>0)
  {
    //recuperer la session en cours
    $sessionencours=$session->getSessionEncours($codeEtabsession);
    $tabsessionencours=explode("*",$sessionencours);
    $libellesessionencours=$tabsessionencours[0];
    $sessionencoursid=$tabsessionencours[1];
    $typesessionencours=$tabsessionencours[2];

    // $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);

    $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
  }else {
    $nbclasse=0;
  }
}else {
  if($nbsessionOn>0)
  {
    //recuperer la session en cours
    $sessionencours=$session->getSessionEncours($codeEtabsession);
    $tabsessionencours=explode("*",$sessionencours);
    $libellesessionencours=$tabsessionencours[0];
    $sessionencoursid=$tabsessionencours[1];
    $typesessionencours=$tabsessionencours[2];

    $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);

    $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
  }else {
    $nbclasse=0;
  }
}

//$controles=$matiere->getAllControleMatiereOfThisTeatcherId($_SESSION['user']['IdCompte']);
// $controles=$matiere->getAllEvaluationsOfThisSchool($codeEtabsession,$libellesessionencours);
$controles=$matiere->getAllEvaluationsOfThisSchoolTeatcherId($_SESSION['user']['IdCompte'],$codeEtabsession,$libellesessionencours);


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
  <!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
  <!--bootstrap -->
  <!--bootstrap -->
  <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
        <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
  <!-- Material Design Lite CSS -->
  <link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
  <link href="../assets2/css/material_style.css" rel="stylesheet">
  <!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::ManagingcontolsTea ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">

                              <li><a class="parent-item" href="controles.php"><?php echo L::ControlsMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::ManagingcontolsTea ?></li>
                          </ol>
                      </div>
                  </div>
          <!-- start widget -->
          <?php

                if(isset($_SESSION['user']['addctrleok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
          <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
          <script src="../assets/js/sweetalert2.min.js"></script>

            <script>

            Swal.fire({
            type: 'success',
            title: 'Félicitations',
            text: '<?php echo $_SESSION['user']['addctrleok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['addctrleok']);
                }

                 ?>




          <?php

                if(isset($_SESSION['user']['deletesubjectok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

            <script>

            Swal.fire({
          title: '<?php echo L::Felicitations ?>',
          text: "<?php echo $_SESSION['user']['deletesubjectok']; ?>",
          type: 'success',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'OK',

          }).then((result) => {
          if (result.value) {
          window.location.reload();
          }
          })


            </script>
                  <?php
                  unset($_SESSION['user']['deletesubjectok']);
                }

                 ?>


          <?php

                if(isset($_SESSION['user']['updatesubjectok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

            <script>

            Swal.fire({
          title: '<?php echo L::Felicitations ?>',
          text: "<?php echo $_SESSION['user']['updatesubjectok']; ?>",
          type: 'success',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'OK',

          }).then((result) => {
          if (result.value) {
          window.location.reload();
          }
          })


            </script>
                  <?php
                  unset($_SESSION['user']['updatesubjectok']);
                }

                 ?>
          <?php

                if(isset($_SESSION['user']['addsubjectok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

            <script>


            Swal.fire({
          title: '<?php echo L::Felicitations ?>',
          text: "<?php echo $_SESSION['user']['addsubjectok']; ?>",
          type: 'success',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'OK',

          }).then((result) => {
          if (result.value) {
          window.location.reload();
          }
          })
            </script>
                  <?php
                  unset($_SESSION['user']['addsubjectok']);
                }

                 ?>


  <br/><br/>

  <div class="col-md-12 col-sm-12">
                <div class="panel tab-border card-box">
                    <header class="panel-heading panel-heading-gray custom-tab ">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a href="#home" data-toggle="tab" class="active"><i class="fa fa-bars"></i> Liste des évaluations</a>
                            </li>
                            <?php
                            if($etablissementType==1||$etablissementType==3)
                            {
                             ?>
                             <li class="nav-item"><a href="#about1" data-toggle="tab"><i class="fa fa-plus-circle"></i> Ajouter une évaluation</a>
                             </li>
                             <?php
                           }else {
                             ?>
                             <li class="nav-item"><a href="#about" data-toggle="tab"><i class="fa fa-plus-circle"></i> Ajouter une évaluation</a>
                             </li>
                             <?php
                           }
                              ?>



                        </ul>
                    </header>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="home">
                              <div class="row">

                                <?php
                                  foreach ($controles as $value):
                                      $type=$value->type_evaltions;
                                      if($type=="CONTROLES")
                                      {
                                    ?>
                                    <div class="col-lg-3 col-md-6 col-12 col-sm-6">
                                     <div class="card">
                                       <div class="card-head card-topline-aqua">
                                         <!-- <header>User Activity</header> -->
                                       </div>
                                       <div class="card-body no-padding height-9" style="height:40%">
                                         <div class="container-fluid">

                                           <div class="row">
                                             <div class="col-md-12">
                                               <!-- <ul class="activity-list"> -->

                                                 <!-- <li> -->
                                                   <div class="post-box">
                                                      <!-- <span
                                                       class="text-muted text-small"><i
                                                         class="fa fa-clock-o"
                                                         aria-hidden="true"></i>
                                                       13 minutes ago</span> -->
                                                     <div class="post-img"><img
                                                         src="../assets/img/slider/fullimage1.jpg"
                                                         class="img-responsive" alt=""></div>
                                                     <!-- <div> -->
                                                     <span style="text-align:center"><h5 class=""><?php echo $value->libelle_evaltions;?></h5></span>

                                                          <ul class="list-group list-group-unbordered">
                                                            <?php
                                                            $type=$value->type_evaltions;
                                                            if($type=="CONTROLES")
                                                            {

                                                              $datas=$etabs->getAllControlesDetails($value->codeEtab_evaltions,$value->session_evaltions,$value->idtype_evaltions);
                                                               foreach ($datas as $values):
                                                                 ?>
                                                                 <li class="list-group-item">
                                                                     <p><span><i class="fa fa-graduation-cap"></i> <?php echo L::MatiereMenusingle ?> : <?php echo $values->libelle_mat ?></span></p>
                                                                     <?php
                                                                     /*
                                                                      ?>
                                                                     <p style="margin-top:-9px;"><span><i class="fa ffa fa-user-o"></i> <?php echo L::ProfsMenusingle ?> </span></p>
                                                                     <p style="margin-top:-12px;"><span style="font-size:12px;"><?php echo $teatcher->getNameofTeatcherById($values->teatcher_mat) ?></span> </p>
                                                                       <?php
                                                                       */
                                                                        ?>

                                                                       <p style="margin-top:-15px;"><span ><i class="fa fa-superscript"></i> Coefficient : <?php echo $values->coef_ctrl;?></span></p>
                                                                       <p style="margin-top:-15px;"><span><i class="fa fa-calendar"></i> Date : <?php echo date_format(date_create($values->date_ctrl),"d/m/Y");?></span></p>
                                                                   <!-- </li>
                                                                   <li class="list-group-item"> -->
                                                                     <p>  <?php

                                                                       $dataclasses=$value->classes_evaltions;

                                                                       // for($z=0;$z<$nbclasse;$z++)
                                                                       // {
                                                                         ?>
                                                                           <span class="label label-sm label-success" style="text-align:center"> <?php
                                                                           echo $classe->getInfosofclassesbyId($value->classes_evaltions,$libellesessionencours);
                                                                           //echo $tabdataclasse[$z];
                                                                           ?> </span>
                                                                         <?php
                                                                       // }
                                                                       ?></p>
                                                                   </li>
                                                                 <li class="list-group-item">

                                                               <?php
                                                               $nbcontrolenotes=$etabs->DetermineNumberOfcontrolenote($values->id_ctrl,$values->classe_ctrl,$values->mat_ctrl,$values->codeEtab_ctrl,$values->session_ctrl);
                                                               if($nbcontrolenotes==0)
                                                               {
                                                                 ?>
                                                                 <a href="#"  data-toggle="modal" data-target="#exampleModal"class="btn btn-info  btn-xs " style="border-radius:3px;" onclick="modifying(<?php echo $values->id_ctrl ?>,'<?php echo $values->codeEtab_ctrl ?>','<?php echo $values->session_ctrl ?>')">
                                                                   <i class="fa fa-pencil"></i>
                                                                 </a>
                                                                 <a href="#"  onclick="deleted(<?php echo $value->id_evaltions ?>,<?php echo $values->id_mat;?>,<?php echo $values->id_ctrl; ?>,<?php echo $values->classe_ctrl ?>,'<?php echo $values->codeEtab_ctrl ?>','<?php echo $values->session_ctrl ?>')" class="btn btn-danger  btn-xs " style="border-radius:3px;">
                                                                   <i class="fa fa-trash-o"></i>

                                                                 </a>
                                                                 <?php
                                                               }else {
                                                               ?>
                                                               <a href="#"  onclick="archived(<?php echo $values->id_mat;?>,<?php echo $values->id_ctrl; ?>,<?php echo $values->classe_ctrl ?>)" class="btn btn-warning  btn-xs " style="border-radius:3px;">
                                                                 <i class="fa fa-refresh"></i>
                                                               </a>
                                                               <?php
                                                               }

                                                                ?>

                                                                   </li>
                                                                 <?php
                                                               endforeach;

                                                            }else if($type=="DEVOIRS")
                                                            {

                                                            }else if($type=="QUIZS")
                                                            {

                                                            }else if($type=="EXAMENS")
                                                            {
                                                              //nous allons recuperer les informations de l'examen

                                                            }
                                                             ?>




                                     </ul>
                                      <br>

                                      <?php
                                      /*
                                       ?>
                                      <div class="row">
                                        <?php
                                        $statutpara=$value->statut_act;
                                        if($statutpara==1)
                                        {
                                          ?>
                                          <div class="" style="text-align:center">
                                            <p>

                                              <a href="#" data-toggle="modal" data-target="#mediumModel"  class="btn btn-warning  btn-sm " style="border-radius:3px;" title="détails de l'activité" onclick="checkParascolaires(<?php echo $value->id_act;?>,'<?php echo $value->codeEtab_act ?>','<?php echo $value->session_act ?>')">
                                                <i class="fa fa-info-circle"></i>
                                              </a>
                                              <a href="#"  onclick="sender(<?php echo $value->id_act;?>,<?php echo $value->idnotif_act;?>)" class="btn btn-success  btn-sm " style="border-radius:3px;" title="envoyer la notification">
                                                <i class="fa fa-send-o"></i>
                                              </a>
                                              <a href="updateactivity.php?paraid=<?php echo $value->id_act ?>&codeEtab=<?php echo $value->codeEtab_act ?>" class="btn btn-info  btn-sm " style="border-radius:3px;" title="modifier la notification">
                                                <i class="fa fa-pencil"></i>
                                              </a>

                                              <a href="#"  onclick="deleted(<?php echo $value->id_act;?>,<?php echo $value->idnotif_act;?>)" class="btn btn-danger  btn-sm " style="border-radius:3px;" title="supprimer l'activité">
                                                <i class="fa fa-trash-o"></i>
                                              </a>

                                            </p>
                                          </div>



                                          <?php
                                        }else {
                                          ?>
                                          <a href="#"  onclick="archived(<?php echo $value->id_act;?>,<?php echo $value->idnotif_act;?>)" class="btn btn-primary  btn-md " style="border-radius:3px;" title="archiver l'activité">
                                            <i class="fa fa-circle-o-notch"></i>
                                          </a>

                                          <?php
                                        }
                                         ?>

                                     </div>

                                     <?php
                                     */
                                      ?>


                                                   </div>


                                             </div>
                                           </div>
                                         </div>
                                       </div>
                                     </div>
                                   </div>
                                    <?php
                                  }
                                  endforeach;
                                 ?>


                              </div>
                            </div>

   <?php
   if($etablissementType==1||$etablissementType==3)
   {
    ?>
    <div class="tab-pane" id="about1">
      <?php
      if($nbsessionOn>0)
      {
       ?>
       <div class="row">
           <div class="col-md-12 col-sm-12">
             <form  id="FormAddCtrl1" class="form-horizontal" action="../controller/controle.php" method="post">
                 <div class="form-body">
                   <div class="form-group row">
                           <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                               <span class="required"> * </span>
                           </label>
                           <div class="col-md-5">
                               <select class="form-control " name="classe" id="classe"  style="width:100%" onchange="searchmatiere()">
                                   <option value=""><?php echo L::Selectclasses ?></option>
                                   <?php
                                   $i=1;
                                     foreach ($classes as $value):
                                     ?>
                                     <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                     <?php
                                                                      $i++;
                                                                      endforeach;
                                                                      ?>

                               </select>
                       </div>
                     </div>
                   <div class="form-group row">
                           <label class="control-label col-md-3"><?php echo L::MatiereMenusingle ?>
                               <span class="required"> * </span>
                           </label>
                           <div class="col-md-5">
                               <!--input type="text" name="matiere" id="matiere" data-required="1" placeholder="Entrer la classe" class="form-control " /-->
                               <select class="form-control " name="matiere" id="matiere"  style="width:100%" onchange='searchprofesseur()'>
                                   <option value=""><?php echo L::SelectSubjects ?></option>

                               </select>
                               <input type="hidden" id="teatcher" name="teatcher" value="<?php echo $_SESSION['user']['IdCompte']; ?>">
                               <input type="hidden" name="etape" id="etape" value="4"/>
                               <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabsession; ?>"/>
                                 <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">

                             </div>

                    </div>


                    <div class="form-group row">
                            <label class="control-label col-md-3"><?php echo L::LibelleControle ?>
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-5">
                                <input type="text" name="controle" id="controle" data-required="1" placeholder="Entrer la classe" class="form-control " />

                              </div>

                     </div>
                    <div class="form-group row">
                            <label class="control-label col-md-3"><?php echo L::Coefs ?>
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-5">
                                <input type="number" min="1" name="coef" id="coef" data-required="1" placeholder="<?php echo L::EnterCoefSubjectControlLib ?>" class="form-control " /> </div>
                     </div>
                     <div class="form-group row">
                       <label class="control-label col-md-3"><?php echo L::DatecontolsTea ?>
                           <span class="required"> * </span>
                       </label>
                           <div class="col-md-5">
                               <input type="date"  placeholder="<?php echo L::DatecontolsTea ?>" name="datectrl" id="datectrl"  class="form-control ">
                                 <span class="help-block"><?php echo L::Datesymbole ?></span>
                           </div>
                       </div>







<div class="form-actions">
                     <div class="row">
                         <div class="offset-md-3 col-md-9">

                             <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                             <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                         </div>
                       </div>
                    </div>
</div>
             </form>
           </div>

       </div>


       <?php
     }
       ?>



     </div>


<?php
}else {
?>
<div class="tab-pane" id="about">
<?php
if($nbsessionOn>0)
{
?>
<div class="row">
<div class="col-md-12 col-sm-12">
<form  id="FormAddCtrl" class="form-horizontal" action="../controller/evaluations.php" method="post">
<div class="form-body">

  <div class="form-group row" id="rowclasseselect">
          <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>(s)
              <span class="required"> * </span>
          </label>
          <div class="col-md-5">
            <input type="hidden" name="evaltype" id="evaltype"  value="1">
              <select class="form-control " name="classeselect[]" id="classeselect" multiple="multiple"  style="width:100%" >
                  <option value=""><?php echo L::Selectclasses ?></option>
                  <?php
                  $i=1;
                    foreach ($classes as $value):
                    ?>
                    <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                    <?php
                                                     $i++;
                                                     endforeach;
                                                     ?>

              </select>
      </div>
    </div>

<div class="form-group row" id="rowclasse">
        <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
            <span class="required"> * </span>
        </label>
        <div class="col-md-5">
            <select class="form-control " name="classe" id="classe"  style="width:100%" onchange="searchmatiere()">
                <option value=""><?php echo L::Selectclasses ?></option>
                <?php
                $i=1;
                  foreach ($classes as $value):
                  ?>
                  <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                  <?php
                                                   $i++;
                                                   endforeach;
                                                   ?>

            </select>
    </div>
  </div>
<div class="form-group row" id="rowmatiere">
        <label class="control-label col-md-3"><?php echo L::MatiereMenusingle ?>
            <span class="required"> * </span>
        </label>
        <div class="col-md-5">
            <!--input type="text" name="matiere" id="matiere" data-required="1" placeholder="Entrer la classe" class="form-control " /-->
            <select class="form-control " name="matiere" id="matiere"  style="width:100%" onchange='searchprofesseur()'>
                <option value=""><?php echo L::SelectSubjects ?></option>

            </select>
            <input type="hidden" id="teatcher" name="teatcher" value="<?php echo $_SESSION['user']['IdCompte']; ?>">
          </div>

 </div>

     <input type="hidden" name="etape" id="etape" value="1"/>
     <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabsession; ?>"/>
       <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
     <div class="form-group row" id="rowperiode">
             <label class="control-label col-md-3">Période
                 <span class="required"> * </span>
             </label>
             <div class="col-md-5">

               <select class="form-control " id="typesess" name="typesess" style="width:100%" >
                   <option value=""><?php echo L::SelectedPeriod ?></option>
                   <?php
                   $i=1;
                     foreach ($typesemestre as $value):
                     ?>
                     <option value="<?php echo $value->id_semes?>"><?php echo utf8_encode(utf8_decode($value->libelle_semes)) ?></option>

                     <?php
                                                      $i++;
                                                      endforeach;
                                                      ?>

               </select>

            </div>
         </div>
 <div class="form-group row" id="rowlibelle">
         <label class="control-label col-md-3">Libellé évaluation
             <span class="required"> * </span>
         </label>
         <div class="col-md-5">
             <input type="text" name="controle" id="controle" data-required="1" placeholder="Entrer le libellé de l'évaluation" class="form-control " />

           </div>

  </div>
 <div class="form-group row" id="rowcoef">
         <label class="control-label col-md-3"><?php echo L::Coefs ?>
             <span class="required"> * </span>
         </label>
         <div class="col-md-5">
             <input type="text" min="1" name="coef" id="coef" data-required="1" placeholder="Entrer le coefficient de l'évaluation" class="form-control " /> </div>
  </div>
  <div class="form-group row" id="rowdateponctuel">
    <label class="control-label col-md-3"><?php echo L::DatecontolsTea ?>
        <span class="required"> * </span>
    </label>
        <div class="col-md-5">
            <input type="text"  placeholder="Entrer de l'évaluation" name="datectrl" id="datectrl"  class="form-control ">

        </div>
    </div>
    <div class="form-group row" id="rowdatedeb">
      <label class="control-label col-md-3">Date Début
          <span class="required"> * </span>
      </label>
          <div class="col-md-5">
            <input type="text"  placeholder="Entrer de l'évaluation" name="datedeb" id="datedeb"  class="form-control ">

          </div>
      </div>
      <div class="form-group row" id="rowdatefin">
        <label class="control-label col-md-3">Date Fin
            <span class="required"> * </span>
        </label>
            <div class="col-md-5">
              <input type="text"  placeholder="Entrer de l'évaluation" name="datefin" id="datefin"  class="form-control ">


            </div>
        </div>







<div class="form-actions">
  <div class="row">
      <div class="offset-md-3 col-md-9">

          <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
          <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
      </div>
    </div>
 </div>
</div>
</form>
</div>

</div>


<?php
}
?>



</div>
<?php
}
?>





                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog modal-lg" role="document">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <h4 class="modal-title" id="exampleModalLabel"><?php echo L::ModificationControle ?></h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                      </button>
                                  </div>
                                  <div class="modal-body">
                                    <form  id="FormUpdateControle" class="form-horizontal">
                                        <div class="form-body">
                                          <div class="form-group row">
                                                  <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                                      <span class="required"> * </span>
                                                  </label>
                                                  <div class="col-md-5">
                                                      <select class="form-control " name="classemod" id="classemod"  style="width:171%" disabled>
                                                          <option value=""><?php echo L::Selectclasses ?></option>


                                                      </select>

                                              </div>
                                            </div>
                                            <div class="form-group row">
                                                    <label class="control-label col-md-3"><?php echo L::MatiereMenusingle ?>
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-5">
                                                        <!--input type="text" name="matiere" id="matiere" data-required="1" placeholder="Entrer la classe" class="form-control " /-->
                                                        <select class="form-control " name="matieremod" id="matieremod"  style="width:171%" disabled>
                                                            <option value=""><?php echo L::SelectSubjects ?></option>


                                                        </select>

                                                      </div>

                                             </div>
                                             <div class="form-group row">
                                                     <label class="control-label col-md-3"><?php echo L::ProfsMenusingle ?>
                                                         <span class="required"> * </span>
                                                     </label>
                                                     <div class="col-md-5">

                                                       <select class="form-control " id="teatchermod" name="teatchermod" style="width:171%" disabled>
                                                           <option value=""><?php echo L::TeatcherSelected ?></option>


                                                       </select>



                                                    </div>
                                                 </div>
                                                 <div class="form-group row">
                                                         <label class="control-label col-md-3"><?php echo L::LibelleControle ?>
                                                             <span class="required"> * </span>
                                                         </label>
                                                         <div class="col-md-5">
                                                             <input type="text" name="controlemod" id="controlemod" value="" data-required="1" placeholder="<?php echo L::EnterControlLib ?>" class="form-control " style="width:171%" />
                                                             <input type="hidden" name="etape" id="etape" value="2"/>
                                                             <input type="hidden" name="codeEtabmod" id="codeEtabmod" value=""/>
                                                             <input type="hidden" name="idctrlmod" id="idctrlmod" value=""/>
                                                             <input type="hidden" name="sessionEtabmod" id="sessionEtabmod" value=""/>

                                                           </div>

                                                  </div>

                                                  <div class="form-group row">
                                                          <label class="control-label col-md-3"><?php echo L::Coefs ?>
                                                              <span class="required"> * </span>
                                                          </label>
                                                          <div class="col-md-5">
                                                              <input type="text" min="1" value="" name="coefmod" id="coefmod" data-required="1" placeholder="Entrer le coefficient du contrôle " class="form-control " style="width:171%"/>

                                                            </div>
                                                   </div>
                                                   <div class="form-group row">
                                                     <label class="control-label col-md-3"><?php echo L::DatecontolsTea ?>
                                                         <span class="required"> * </span>
                                                     </label>
                                                         <div class="col-md-5">
                                                             <input type="text"  placeholder="<?php echo L::DatecontolsTea ?>" value="" name="datectrlmod" id="datectrlmod" d class="form-control " style="width:171%">


                                                         </div>
                                                     </div>




                      <div class="form-actions">
                                            <div class="row">
                                                <div class="offset-md-3 col-md-9">

                                                      <button type="submit"  class="btn btn-info">Modifier</button>
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                                                </div>
                                              </div>
                                           </div>
                    </div>
                                    </form>
                                  </div>

                              </div>
                          </div>
                      </div>


                      <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel1">Modification Examen</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                              <form  id="FormUpdateExam" class="form-horizontal">
                                                  <div class="form-body">
                                                    <div class="form-group row">
                                                            <label class="control-label col-md-3">Examen
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-8">
                                                                <input type="text" name="exammod" id="exammod"  data-required="1" value="" placeholder="Entrer la classe" class="form-control " />

                                                               </div>
                                                     </div>
                                                     <div class="form-group row">
                                                       <label class="control-label col-md-3">Date Début
                                                           <span class="required"> * </span>
                                                       </label>
                                                           <div class="col-md-8">
                                                               <input type="text"  placeholder="Entrer la date de début" name="datedebmodexa" id="datedebmodexa"  value="" class="form-control ">

                                                           </div>
                                                       </div>
                                                       <div class="form-group row">
                                                         <label class="control-label col-md-3">Date Fin
                                                             <span class="required"> * </span>
                                                         </label>
                                                             <div class="col-md-8">
                                                                 <input type="text" placeholder="Entrer la date de Fin" name="datefinmodexa" id="datefinmodexa" value=""  class="form-control ">

                                                                 <input type="hidden" name="etape" id="etape" value="2"/>
                                                                 <input type="hidden" name="codeEtabmodexa" id="codeEtabmodexa" value=""/>
                                                                 <input type="hidden" name="sessionEtabmodexa" id="sessionEtabmodexa" value=""/>
                                                                 <input type="hidden" name="idexa" id="idexa" value=""/>
                                                             </div>
                                                         </div>


                                <div class="form-actions">
                                                      <div class="row">
                                                          <div class="offset-md-3 col-md-9">
                                                              <button type="submit"  class="btn btn-info">Modifier</button>
                                                              <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                                                          </div>
                                                        </div>
                                                     </div>
                              </div>
                                              </form>
                                            </div>

                                        </div>
                                    </div>
                                </div>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>

   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <script src="../assets/js/formatter/jquery.formatter.min.js"></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

 <script>

 function modifying(controleid,codeEtab,sessionEtab)
 {
   $("#idctrlmod").val(controleid);
   $("#codeEtabmod").val(codeEtab);
   $("#sessionEtabmod").val(sessionEtab);
 }

 function modifyingExam(controleid,codeEtab,sessionEtab)
 {
   $("#FormUpdateExam #idexa").val(controleid);
   $("#FormUpdateExam #codeEtabmodexa").val(codeEtab);
   $("#FormUpdateExam #sessionEtabmodexa").val(sessionEtab);


 }

 $('#exampleModal1').on('shown.bs.modal', function () {
   var controleid=$("#FormUpdateExam #idexa").val();
   var codeEtab=$("#FormUpdateExam #codeEtabmodexa").val();
   var sessionEtab=$("#FormUpdateExam #sessionEtabmodexa").val();
   var etape=3;

   // alert(controleid);

   $.ajax({
     url: '../ajax/evaluations.php',
         type: 'POST',
         async:true,
         data: 'etape=' + etape+'&controleid='+controleid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab,
         dataType: 'text',
         success: function (content, statut) {

           $("#FormUpdateExam #exammod").val(content.split("*")[0]);
           $("#FormUpdateExam #datedebmodexa").val(content.split("*")[1]);
           $("#FormUpdateExam #datefinmodexa").val(content.split("*")[2]);

         }
   });



 });

$('#exampleModal').on('shown.bs.modal', function () {

var controleid=$("#FormUpdateControle #idctrlmod").val();
var codeEtab=$("#FormUpdateControle #codeEtabmod").val();
var sessionEtab=$("#FormUpdateControle #sessionEtabmod").val();
var etape=1;


$.ajax({
  url: '../ajax/evaluations.php',
      type: 'POST',
      async:true,
      data: 'etape=' + etape+'&controleid='+controleid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab,
      dataType: 'text',
      success: function (content, statut) {

  $("#FormUpdateControle #controlemod").val(content.split("*")[1]);
  $("#FormUpdateControle #coefmod").val(content.split("*")[6]);
  $("#FormUpdateControle #datectrlmod").val(content.split("*")[2]);

  // var matiereid=content.split("*")[4];
  // var teatcherid=content.split("*")[5];
  // var classeid=content.split("*")[3];

  //nous allons chercher la classe , la matière et le nom de l'enseignant
var etape=2;
  $.ajax({
    url: '../ajax/evaluations.php',
        type: 'POST',
        async:true,
        data: 'etape=' + etape+'&controleid='+controleid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab,
        dataType: 'text',
        success: function (content, statut) {


          $("#FormUpdateControle #classemod").html(content.split("*")[0]);
          $("#FormUpdateControle #matieremod").html(content.split("*")[1]);
          $("#FormUpdateControle #teatchermod").html(content.split("*")[2]);

    //nous allons chercher la classe , la matière et le nom de l'enseignant




        }
  });


      }
});



});

function deletedExamen(examenid,evalid,codeEtab,sessionEtab)
{


  var etape=7;
  // var controleid="<?php //echo $value->id_ctrl;?>";
  Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "Voulez vous vraiment supprimer cet examen",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::DeleteLib ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
// document.location.href="../controller/controle.php?etape=3&matiere="+id+"&classe="+classe+"&codeEtab="+codeEtab+"&controleid="+controleid;

$.ajax({

 url: '../ajax/evaluations.php',
 type: 'POST',
 async:true,
 data: 'codeEtab=' + codeEtab+ '&etape=' + etape+'&examenid='+examenid+'&sessionEtab='+sessionEtab+'&evalid='+evalid,
 dataType: 'text',
 success: function (content, statut) {

location.reload();

 }
});

}else {

}
})
}

 function deleted(evalid,matiereid,controleid,classeid,codeEtab,sessionEtab)
 {


   var etape=6;
   // var controleid="<?php //echo $value->id_ctrl;?>";
   Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "Voulez vous vraiment supprimer ce contrôle",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::DeleteLib ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
// document.location.href="../controller/controle.php?etape=3&matiere="+id+"&classe="+classe+"&codeEtab="+codeEtab+"&controleid="+controleid;

$.ajax({

  url: '../ajax/evaluations.php',
  type: 'POST',
  async:true,
  data: 'codeEtab=' + codeEtab+ '&etape=' + etape+'&classeid='+classeid+'&sessionEtab='+sessionEtab+'&matiereid='+matiereid+'&controleid='+controleid+'&evalid='+evalid,
  dataType: 'text',
  success: function (content, statut) {

location.reload();

  }
});

}else {

}
})
 }

 function searching()
 {
   var evaluation=$("#evaltype").val();

   if(evaluation==2)
   {
 $("#rowlibelle").show();
 $("#rowperiode").show();
 $("#rowdatedeb").show();
 $("#rowdatefin").show();
 $("#rowclasseselect").show();

 $("#rowmatiere").hide();
 $("#rowteatcher").hide();
 $("#rowcoef").hide();
 $("#rowdateponctuel").hide();
 $("#rowclasse").hide();

   }else {
     $("#rowclasse").show();
     $("#rowmatiere").show();
     $("#rowteatcher").show();
     $("#rowperiode").show();
     $("#rowlibelle").show();
     $("#rowcoef").show();
     $("#rowdateponctuel").show();

     $("#rowdatedeb").hide();
     $("#rowdatefin").hide();
     $("#rowclasseselect").hide();
   }

 }

 function searchmatiere()
 {
     var codeEtab="<?php echo $codeEtabsession;?>";
     var sessionEtab="<?php echo $libellesessionencours ?>";
     var classe=$("#classe").val();
     var teatcherid=$("#teatcher").val();
     var etape=19;
      $.ajax({

        url: '../ajax/matiere.php',
        type: 'POST',
        async:true,
        data: 'code=' + codeEtab+ '&etape=' + etape+'&classe='+classe+'&teatcherid='+teatcherid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab,
        dataType: 'text',
        success: function (content, statut) {

          $("#matiere").html("");
          $("#matiere").html(content);

        }
      });
 }

 function searchprofesseur()
 {
   var codeEtab="<?php echo $codeEtabsession;?>";
   var classe=$("#classe").val();
   var matiere=$("#matiere").val();
   var etape=4;
   $.ajax({

     url: '../ajax/teatcher.php',
     type: 'POST',
     async:true,
     data: 'code=' + codeEtab+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
     dataType: 'text',
     success: function (content, statut) {

       // alert(content);
       // $("#teatcher").html("");
       // $("#teatcher").html(content);

     }
   });
 }

 jQuery(document).ready(function() {

$("#datectrl").formatter({pattern:"{{99}}-{{99}}-{{9999}}"});
$("#datedeb").formatter({pattern:"{{99}}-{{99}}-{{9999}}"});
$("#datefin").formatter({pattern:"{{99}}-{{99}}-{{9999}}"});
$("#coef").formatter({pattern:"{{999}}"});
$("#datectrlmod").formatter({pattern:"{{99}}-{{99}}-{{9999}}"});
$("#coefmod").formatter({pattern:"{{999}}"});

$("#datedebmodexa").formatter({pattern:"{{99}}-{{99}}-{{9999}}"});
$("#datefinmodexa").formatter({pattern:"{{99}}-{{99}}-{{9999}}"});


$("#classe").select2();
// $("#teatcher").select2();
$("#classeEtab").select2();
$("#matiere").select2();
$("#typesess").select2();
// $("#evaltype").select2();
$("#classeselect").select2({
  tags: true,
tokenSeparators: [',', ' ']
});

$("#rowclasse").hide();
$("#rowmatiere").hide();
$("#rowteatcher").hide();
$("#rowperiode").hide();
$("#rowlibelle").hide();
$("#rowcoef").hide();
$("#rowdateponctuel").hide();

$("#rowdatedeb").hide();
$("#rowdatefin").hide();
$("#rowclasseselect").hide();

searching();

$("#FormUpdateExam").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
},
highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
},
success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },  rules:{
    exammod:"required",
    datedebmodexa:"required",
    datefinmodexa:"required"
  },messages: {
    exammod:"Merci de renseigner le libellé de l'examen",
    datedebmodexa:"Merci de renseigner la date de début",
    datefinmodexa:"Merci de renseigner la date de fin"
  },submitHandler: function(form) {


    var examid=$("#FormUpdateExam #idexa").val();
    var codeEtab=$("#FormUpdateExam #codeEtabmodexa").val();
    var sessionEtab=$("#FormUpdateExam #sessionEtabmodexa").val();

    var exammod=$("#FormUpdateExam #exammod").val();
    var datedebmodexa=$("#FormUpdateExam #datedebmodexa").val();
    var datefinmodexa=$("#FormUpdateExam #datefinmodexa").val();
    var etape=4;

    // alert("nokkk");

    $.ajax({

      url: '../ajax/evaluations.php',
      type: 'POST',
      async:true,
      data: 'codeEtab=' + codeEtab+ '&etape=' + etape+'&sessionEtab='+sessionEtab+'&examid='+examid+'&exammod='+exammod+'&datedebmodexa='+datedebmodexa+'&datefinmodexa='+datefinmodexa,
      dataType: 'text',
      success: function (content, statut) {

        location.reload();


      }
    });


  }
});


$("#FormUpdateControle").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
},
highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
},
success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },  rules:{
    controlemod:"required",
    coefmod:"required",
    datectrlmod:"required"
  },messages: {
    controlemod:"Merci de renseigner le libellé du contrôle",
    coefmod:"Merci de renseigner le coefficient",
    datectrlmod:"Merci de renseigner la date du contrôle"
  },submitHandler: function(form) {


var codeEtab=$("#FormUpdateControle #codeEtabmod").val();
var sessionEtab=$("#FormUpdateControle #sessionEtabmod").val();
var controleid=$("#FormUpdateControle #idctrlmod").val();
var libellecontrole=$("#FormUpdateControle #controlemod").val();
var coefcontrole=$("#FormUpdateControle #coefmod").val();
var datecontrole=$("#FormUpdateControle #datectrlmod").val();
var etape=5;

// alert("nokkk");

$.ajax({

  url: '../ajax/evaluations.php',
  type: 'POST',
  async:true,
  data: 'codeEtab=' + codeEtab+ '&etape=' + etape+'&sessionEtab='+sessionEtab+'&controleid='+controleid+'&libellecontrole='+libellecontrole+'&coefcontrole='+coefcontrole+'&datecontrole='+datecontrole,
  dataType: 'text',
  success: function (content, statut) {

    location.reload();


  }
});



  }


});





   $("#FormAddCtrl").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

        // matiere:"required",
        // classe:"required",
        // teatcher:"required",
        // coef:"required",
        classe:"required",
        matiere:"required",
        controle:"required",
        coef:"required",
        teatcher:"required",
        datectrl:"required",
        typesess:"required"


      },
      messages: {
        // matiere:"Merci de renseigner la matière",
        // classe:"<?php echo L::PleaseSelectclasserequired ?>",
        // teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
        // coef:"Merci de renseigner le coefficient de la matière"
        classe:"<?php echo L::PleaseSelectclasserequired ?>",
        matiere:"<?php echo L::PleaseselectSubjects ?>",
        controle:"<?php echo L::Controlsrequired ?>",
        coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
        teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
          datectrl:"<?php echo L::PleaseEnterDateControls ?>",
          typesess:"Merci de selectionner le type de session"


      },
      submitHandler: function(form) {


// nous allons verifier un controle similaire n'existe pas
        var etape=1;
        var evaltype=$("#evaltype").val();

        if(evaltype==1)
        {
          $.ajax({
            url: '../ajax/controle.php',
            type: 'POST',
            async:true,
            data: 'matiere=' + $("#matiere").val()+ '&etape=' + etape+'&classe='+$("#classe").val()+'&teatcher='+$("#teatcher").val()+'&codeEtab='+$("#codeEtab").val()+'&coefficient='+$("#coef").val()+'&datectrl='+$("#datectrl").val()+'&libelle='+$("#controle").val(),
            dataType: 'text',
            success: function (content, statut) {


              if(content==0)
              {
                //cette matière n'existe pas encore pour cette classe

                form.submit();

              }else if(content==1)
              {
                //il est question d'un nouveau professeur pour cette matière
                Swal.fire({
                type: 'warning',
                title: '<?php echo L::WarningLib ?>',
                text: '<?php echo L::ControlAllreadyExist ?>',

                })

              }

            }
          });

        }else {

          var etape=1;

          $.ajax({
            url: '../ajax/examen.php',
            type: 'POST',
            async:true,
            data: 'examen=' + $("#examen").val()+ '&etape=' + etape+'&datedeb='+$("#datedeb").val()+'&datefin='+$("#datefin").val()+'&codeEtab='+$("#codeEtab").val(),
            dataType: 'text',
            success: function (content, statut) {

                if(content==0)
                {
                  form.submit();
                }else if(content==1)
                {

                  Swal.fire({
                  type: 'warning',
                  title: '<?php echo L::WarningLib ?>',
                  text: "Cet Examen existe dejà dans le sysème",

                })
                }

            }
          });

        }





         // $.ajax({
         //   url: '../ajax/controle.php',
         //   type: 'POST',
         //   async:true,
         //   data: 'matiere=' + $("#matiere").val()+ '&etape=' + etape+'&classe='+$("#classe").val()+'&teatcher='+$("#teatcher").val()+'&codeEtab='+$("#codeEtab").val()+'&coefficient='+$("#coef").val()+'&datectrl='+$("#datectrl").val(),
         //   dataType: 'text',
         //   success: function (content, statut) {
         //
         //
         //     if(content==0)
         //     {
         //       //cette matière n'existe pas encore pour cette classe
         //
         //       form.submit();
         //
         //     }else if(content==1)
         //     {
         //       //il est question d'un nouveau professeur pour cette matière
         //       Swal.fire({
         //       type: 'warning',
         //       title: '<?php echo L::WarningLib ?>',
         //       text: '<?php echo L::ControlAllreadyExist ?>',
         //
         //       })
         //
         //     }
         //
         //   }
         // });

             }


           });

           $("#FormAddCtrl1").validate({

             errorPlacement: function(label, element) {
             label.addClass('mt-2 text-danger');
             label.insertAfter(element);
           },
           highlight: function(element, errorClass) {
             $(element).parent().addClass('has-danger')
             $(element).addClass('form-control-danger')
           },
           success: function (e) {
                 $(e).closest('.control-group').removeClass('error').addClass('info');
                 $(e).remove();
             },
              rules:{

                // matiere:"required",
                // classe:"required",
                // teatcher:"required",
                // coef:"required",
                classe:"required",
                matiere:"required",
                controle:"required",
                coef:"required",
                teatcher:"required",
                datectrl:"required",
                typesess:"required",
                datedeb:"required",
                datefin:"required"


              },
              messages: {
                // matiere:"Merci de renseigner la matière",
                // classe:"<?php echo L::PleaseSelectclasserequired ?>",
                // teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
                // coef:"Merci de renseigner le coefficient de la matière"
                classe:"<?php echo L::PleaseSelectclasserequired ?>",
                matiere:"<?php echo L::PleaseselectSubjects ?>",
                controle:"<?php echo L::Controlsrequired ?>",
                coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
                teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
                datectrl:"<?php echo L::PleaseEnterDateControls ?>",
                typesess:"Merci de selectionner le type de session",
                datedeb:"Merci de renseigner la date de début",
                datefin:"Merci de renseigner la date de fin"


              },
              submitHandler: function(form) {


        // nous allons verifier un controle similaire n'existe pas
                var etape=1;

                 $.ajax({
                   url: '../ajax/controle.php',
                   type: 'POST',
                   async:true,
                   data: 'matiere=' + $("#matiere").val()+ '&etape=' + etape+'&classe='+$("#classe").val()+'&teatcher='+$("#teatcher").val()+'&codeEtab='+$("#codeEtab").val()+'&coefficient='+$("#coef").val()+'&datectrl='+$("#datectrl").val(),
                   dataType: 'text',
                   success: function (content, statut) {


                     if(content==0)
                     {
                       //cette matière n'existe pas encore pour cette classe

                       form.submit();

                     }else if(content==1)
                     {
                       //il est question d'un nouveau professeur pour cette matière
                       Swal.fire({
                       type: 'warning',
                       title: '<?php echo L::WarningLib ?>',
                       text: '<?php echo L::ControlAllreadyExist ?>',

                       })

                     }

                   }
                 });

                     }


                   });
      });






 </script>
    <!-- end js include path -->
  </body>

</html>
