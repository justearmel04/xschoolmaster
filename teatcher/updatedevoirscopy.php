<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Student.php');
require_once('../class/Teatcher.php');
require_once('../controller/functions.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$matiere=new Matiere();
$student=new Student();
$teatcher=new Teatcher();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);

$matieres=$matiere->getAllSubjectTeatchByTeatcherId($_SESSION['user']['IdCompte']);
$controles=$matiere->getAllControleMatiereOfThisTeatcherId($_SESSION['user']['IdCompte']);


//$dataclasses=$classe->getAllClassesByClasseId($classeschoolid);

$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
  $programmes=$etabs->getAllprogrammesOfTeatcherClasses($_SESSION['user']['IdCompte'],$libellesessionencours);

  // $fiches=$etabs->getAllFicesOfTeatcherClasses($_SESSION['user']['IdCompte'],$libellesessionencours);

$fiches=$etabs->getAllcahiersOfteatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);

}else {
  $nbclasse=0;
}

$courseid=$_GET['courseid'];
$classeid=$_GET['classeid'];

// $courses=$etabs->getAllTeatchercourses($compteuserid,$codeEtabsession,$libellesessionencours);
$coursesdetails=$etabs->getAlldevoirsdetails($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);

foreach ($coursesdetails as  $datacourses):
  $descricourses=$datacourses->instructions_dev;
  $durationcourses=$datacourses->datelimite_dev;
  $teatchercourses=$datacourses->nom_compte;
  $datercourses=$datacourses->datelimite_dev;
  $namecourses=$datacourses->libelle_dev;
  $classecourses=$datacourses->libelle_classe;
  $classeidcourses=$datacourses->id_classe;
  $matiereidcourses=$datacourses->id_mat;
  $libellematcourses=$datacourses->libelle_mat;
  $statutcourses=$datacourses->statut_dev;
  $filescourses=$datacourses->support_dev;
  $verouillerdevoir=$datacourses->verouiller_dev;
  $destinataires=$datacourses->destinataires_dev;

endforeach;




// Nous allons recuperer les sections du cours

$coursesSections=$etabs->getAllcoursesSection($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);

$coursescompetences=$etabs->getAllcoursesComp($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);

$courseshomeworks=$etabs->getAllcoursesHomeworks($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);
$devoirsupports=$etabs->getAllsuppourtDev($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);

// var_dump($devoirsupports);
// var_dump($coursescompetences);

 ?>

<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
  <!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
  <!--bootstrap -->
  <!--bootstrap -->
  <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
  <!-- Material Design Lite CSS -->
  <link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
  <link href="../assets2/css/material_style.css" rel="stylesheet">
  <!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/jquery.fileuploader.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>

    <style media="screen">
    #radioBtn .notActive{
    color: #3276b1;
    background-color: #fff;
    }

    #radioBtn2 .notActive{
    color: #f0f1f3;
    background-color: #e8091e;
    }

    #radioBtn2 .active{
    color: #f0f1f3;
    background-color: #28a745;

    }
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::Updatedevoirsbtn ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="#"><?php echo L::DevoirsMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::Updatedevoirsbtn ?></li>
                          </ol>
                      </div>
                  </div>
                  <?php

                        if(isset($_SESSION['user']['addclasseok']))
                        {

                          ?>
                          <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?php
                        //echo $_SESSION['user']['addetabok'];
                        ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                           </a>
                        </div-->
      <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
      <script src="../assets/js/sweetalert2.min.js"></script>

                    <script>
                    Swal.fire({
                    type: 'success',
                    title: 'Félicitations',
                    text: '<?php echo $_SESSION['user']['addclasseok'] ?>',

                    })
                    </script>
                          <?php
                          unset($_SESSION['user']['addclasseok']);
                        }

                         ?>

                         <?php
                         if($nbsessionOn==0)
                         {
                           ?>
                           <div class="alert alert-danger alert-dismissible fade show" role="alert">

                           <?php echo L::RequiredScolaireYear ?>

                           <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                            </a>
                           </div>
                           <?php
                         }
                          ?>


                           <div class="row">
                               <div class="col-md-12 col-sm-12">
                                   <div class="card card-box">
                                       <div class="card-head">
                                           <header></header>

                                       </div>

                                       <div class="card-body" id="bar-parent">
                                           <form  id="FormAddAcademique" class="form-horizontal" action="../controller/devoirs.php" method="post" enctype="multipart/form-data" >
                                               <div class="form-body">


                                                 	<div class="row">
                       <div class="col-md-12">
                       <span class="label label-lg label-warning"  style="text-align:center;"> <i class="fa fa-info-circle"> </i> <?php echo L::DevoirsInfos ?></span>
                     </div></br></br>
                                                       <div class="col-md-12">
                                                         <div class="row">

                           <div class="col-md-6">
               							<div class="form-group">
                      <label for="classeEtab"><?php echo L::ClasseMenu ?><span class="required">*</span> :</label>
                      <select class="form-control input-height" id="classeEtab" name="classeEtab" onchange="searchmatiere(<?php echo $_SESSION['user']['IdCompte']; ?>)">
                        <option value=""><?php echo L::Selectclasses ?></option>
                        <?php
                        $i=1;
                          foreach ($classes as $value):
                          ?>
                          <option <?php if($classeid==$value->id_classe){ echo "selected";} ?> value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                          <?php
                        $i++;
                       endforeach;
                         ?>
                      </select>
                                </div>
               							 </div>


               									 <div class="col-md-6">
               	 									<div class="form-group">
               	 			<label for="matclasse"><?php echo L::MatiereMenusingle ?> <span class="required">*</span> :</label>
                       <select class="form-control" id="matclasse" name="matclasse" style="width:100%;" onchange="searchcodeEtab(<?php echo $_SESSION['user']['IdCompte']; ?>)">
                           <option value=""><?php echo L::SelectSubjects ?></option>


                       </select>
                       <input type="hidden" name="etape" id="etape" value="2">
                       <input type="hidden" name="codeEtab" name="codeEtab" value="<?php echo $codeEtabsession; ?>">
                       <input type="hidden" name="sessionEtab" name="sessionEtab" value="<?php echo $libellesessionencours; ?>">
                       <input type="hidden" name="courseid" name="courseid" value="<?php echo $courseid; ?>">
                       <input type="hidden" name="oldfilename"id="oldfilename" value="<?php echo $filescourses;  ?>">
               	                      </div>
               	 									 </div>
                                    <div class="col-md-6">
                                     <div class="form-group">
                         <label for="libellecourse"><?php echo L::DevoirsLibs ?><span class="required">*</span> :</label>
                                <input type="text" class="form-control" name="libellecourse" id="libellecourse" value="<?php echo utf8_decode(utf8_encode($namecourses)); ?>" size="32" maxlength="225" />

                                         </div>
                                      </div>
               										 <div class="col-md-6">
               		 									<div class="form-group">
               		 			<label for="datecourse"><?php echo L::DateLimitDevoirs ?> <span class="required">*</span> :</label>
               		             <input type="text" class="form-control" name="datecourse" id="datecourse" data-mask="99/99/9999" value="<?php echo date_format(date_create($datercourses), "d/m/Y");?>" size="32" maxlength="225" />
                                <span class="help-block"><?php echo L::Datesymbolesecond ?></span>
                                         </div>
               		 									 </div>
                                      <div class="col-md-6">
                          							<div class="form-group">
                                 <label for="destiEtab"><?php echo L::Destinataires ?><span class="required">*</span> :</label>
                                 <?php
                                 $tabdestinataires=explode("@",$destinataires);
                                 ?>
                                 <select class="form-control" multiple="multiple" id="destiEtab" name="destiEtab[]">

                                   <option
                                   <?php
                                   if (in_array("ELEVE", $tabdestinataires)) {
                                     	echo "selected";
                                     }
                                    ?>
                                     value="ELEVE"><?php echo L::EleveCaps ?></option>
                                   <option
                                   <?php
                                   if (in_array("PARENT", $tabdestinataires)) {
                                     	echo "selected";
                                     }
                                    ?>
                                    value="PARENT"><?php echo L::ParentCaps ?></option>
                                 </select>
                                 <span class="help-block" style="color:red;"> <i class="fa fa-warning" style="color:red;"></i> <?php echo L::SelectOneDestinataires ?></span>
                                           </div>
                          							 </div>
                                        <div class="col-md-6">
                                        <div class="form-group">
                            <label for="datecourse"><?php echo L::EcheanceLocks ?>  <span class="required">*</span> :</label>
                             <div class="form-group row" id="RowbtnInscrip">

                              <div class="col-sm-7 col-md-7">
                              <div class="input-group">
                              <div id="radioBtn2" class="btn-group">
                              <a class="btn  btn-sm <?php if($verouillerdevoir==1){echo "active";}else{echo "notActive";} ?> " id="btn21" data-toggle="verouiller" data-title="1" onclick="verouillerOui()"><?php echo L::True ?></a>
                              <a class="btn  btn-sm <?php if($verouillerdevoir==0){echo "active";}else{echo "notActive";} ?> " id="btn22" data-toggle="verouiller" data-title="0" onclick="verouillerNon()"><?php echo L::False ?></a>
                              </div>
                              <input type="hidden" name="verouiller" id="verouiller" value="<?php echo $verouillerdevoir;  ?>">
                              </div>
                              </div>
                              </div>
                                             </div>
                                         </div>
                                         <div class="col-md-12">
                    		 									<div class="form-group">
                    		 			<label for="detailscourse"><?php echo L::HomeworksInstructions ?> <span class="required">*</span> :</label>

                                     <textarea class="form-control" rows="8" name="detailscourse" id="detailscourse" placeholder=""><?php echo $descricourses; ?></textarea>
                                              </div>
                    		 									 </div>


                                      <div class="col-md-12">
                                      <span class="label label-lg label-warning"  style="text-align:center;"> <i class="fa fa-list"> </i> <?php echo L::Homeworksupports ?></span>
                                    </div></br></br>
                                    <div class="col-md-12">
                                      <button type="button" data-toggle="modal" data-target="#mediumModel3" class="btn btn-success pull-right" style="border-radius:5px;height:35px"> <i class="fa fa-plus"></i><?php echo L::Newsupports ?></button>
                                     </div></br></br>
                                     <div class="col-md-12">
                                     <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="">
                                       <thead>
                                           <tr>

                                             <th style="text-align:center"> <?php echo L::SupportsCaps ?> </th>
                                             <th style="text-align:center"> <?php echo mb_strtoupper(L::Actions) ?> </th>
                                           </tr>
                                       </thead>
                                       <tbody>
                                         <?php

                                         $i=1;
                                         foreach ($devoirsupports as  $value):
                                           $lien="";
                                           if(strlen($value->fichier_support)>0)
                                           {
                                           $lien="../devoirs/Etablissements/".$codeEtabsession."/".str_replace(' ', '_',$libellematcourses)."/".str_replace(' ', '_',$classecourses)."/".$value->fichier_support;
                                          }else {
                                             $lien="../photo/images_files1.jpg";
                                           }
                                           ?>
                                           <tr>

                                             <td style="text-align:center"><?php echo mb_strtolower(L::LibelleSupport) ?> <?php echo $i ?></td>
                                             <td style="text-align:center">
                                               <a  target="_blank" href="<?php echo $lien ?>"  class="btn btn-warning btn_remove btn-md"> <i class="fa fa-file"></i> </a>
                                               <button type="button" onclick="supsupport(<?php echo $value->id_support;?>)"   class="btn btn-danger btn_remove btn-md">X</button>
                                             </td>
                                           </tr>
                                           <?php
                                           $i++;
                                         endforeach;
                                          ?>
                                       </tbody>

                                     </table>

                                      </div>
                                    <!-- <div class="col-md-12">
                                     <div class="form-group">

                         <?php
                         //$lien="";
                         //if(strlen($filescourses)>0)
                         {
                         //  $lien="../devoirs/Etablissements/".$codeEtabsession."/".str_replace(' ', '_',$libellematcourses)."/".str_replace(' ', '_',$classecourses)."/".$filescourses;
                             // $lien="../devoirs/Etablissements/".$codeEtabsession."/".$classecourses."/".$datercourses."/".$libellematcourses."/".$filescourses;
                         //}else {
                         //  $lien="../photo/images_files1.jpg";
                         }

                          ?>
                               <div class="col-md-6">
                                   <input type="file" id="fichier3" name="fichier3" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="<?php echo $lien; ?>" data-allowed-file-extensions="pdf doc docx ppt pptx" />
                               </div>
                                         </div>
                                      </div> -->




               								 </div>
                                                       </div>

                                                 </div>














                           </div>
                           <div class="form-actions">
                                                 <div class="row">
                                                     <div class="offset-md-3 col-md-9">
                                                       <button type="submit" class="btn btn-info"><?php echo L::ModifierBtn ?></button>

                                                         <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                     </div>
                                                   </div>
                                                </div>
                                           </form>
                                           <div class="modal fade" id="mediumModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
             					    <div class="modal-dialog" role="document">
             					        <div class="modal-content">
             					            <div class="modal-header">
             					                <h4 class="modal-title" id="exampleModalLabel"><?php echo L::AddNewSecionsCaps ?></h4>
             					                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             					                    <span aria-hidden="true">&times;</span>
             					                </button>
             					            </div>
             					            <div class="modal-body">
             					               <form class="" action="../controller/courses.php" method="post" id="FormSectioncourses">
                                        <div class="col-md-12">
                                         <div class="form-group">
                             <label for="libellesection"><?php echo L::SectionsLib ?><span class="required">*</span> :</label>
                                    <input type="text" class="form-control" name="libellesection" id="libellesection" value="" size="32" maxlength="225" />

                                             </div>
                                          </div>
                                          <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="offset-md-3 col-md-9">
                                                                      <button type="submit" class="btn btn-info"><?php echo L::AddMenu ?></button>

                                                                        <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                    </div>
                                                                  </div>
                                                               </div>

                                      </form>
             					            </div>

             					        </div>
             					    </div>
             					</div>


                       <div class="modal fade" id="mediumModel1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
             <div class="modal-dialog" role="document">
             <div class="modal-content">
               <div class="modal-header">
                   <h4 class="modal-title" id="exampleModalLabel"><?php echo L::AddNewcompetenceCaps ?></h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
                   </button>
               </div>
               <div class="modal-body">
                  <form class="" action="../controller/courses.php" method="post" id="FormCompcourses">
                    <div class="col-md-12">
                     <div class="form-group">
             <label for="libellecomp"><?php echo L::CompetencesLib ?><span class="required">*</span> :</label>
                <input type="text" class="form-control" name="libellecomp" id="libellecomp" value="" size="32" maxlength="225" />

                         </div>
                      </div>
                      <div class="form-actions">
                                            <div class="row">
                                                <div class="offset-md-3 col-md-9">
                                                  <button type="submit" class="btn btn-info"><?php echo L::AddMenu ?></button>

                                                    <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                </div>
                                              </div>
                                           </div>

                  </form>
               </div>

             </div>
             </div>
             </div>

             <div class="modal fade" id="mediumModel3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
             <div class="modal-dialog" role="document">
             <div class="modal-content">
             <div class="modal-header">
             <h4 class="modal-title" id="exampleModalLabel"><?php echo L::AddNewSupport ?></h4>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
             </button>
             </div>
             <div class="modal-body">
             <form class="" action="../controller/devoirs.php" method="post" id="FormHomecourses2" enctype="multipart/form-data">
             <div class="col-md-12">
             <div class="form-group">
               <input type="hidden" name="etape" id="etape" value="5">
               <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $libellesessionencours ?>">
               <input type="hidden" name="sessionEtab" id="sessionEtab" value="<?php echo $codeEtabsession ?>">
               <input type="hidden" name="classeEtab" id="classeEtab" value="<?php echo $classeid ?>">
               <input type="hidden" name="devoirsid" id="devoirsid" value="<?php echo $courseid ?>">

                 <input type="file" id="files" name="files" onchange="checkFile()"/>
             <span class="fa fa-warning" style="color:red;" id="blocsms" style="display:none"> <i class="fa fa-success" style="color:red;"></i> <?php echo L::PleaseselectOneFile ?>r</span>

               </div>
             </div>
             <div class="form-actions">
                                  <div class="row">
                                      <div class="offset-md-3 col-md-9">
                                        <button type="submit" class="btn btn-info"><?php echo L::AddMenu ?></button>

                                          <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                      </div>
                                    </div>
                                 </div>

             </form>
             </div>

             </div>
             </div>
             </div>






                                       </div>
                                   </div>
                               </div>
                           </div>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
   	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
       <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
    <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
    <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
    <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <!--script src="../assets2/plugins/summernote/summernote.js" ></script>
    <script src="../assets2/plugins/summernote/lang/summernote-fr-FR.js" ></script-->
       <!-- calendar -->
       <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
    <script src="../assets2/plugins/moment/moment.min.js" ></script>
    <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
    <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
       <!-- Common js-->
   	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
   	<script src="../assets2/js/theme-color.js" ></script>
   	<!-- Material -->
   	<script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
    <script src="../assets2/js/jquery.fileuploader.min.js" ></script>
    <script src="../assets2/js/dropify.js"></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>

 <script>
 /*$('#summernote').summernote({
     placeholder: '',
     tabsize: 2,
     height: 200,
      lang: 'fr-FR'
   });*/

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function supsupport(id)
   {
     var classeid="<?php echo $classeid; ?>";
     var teatcherid="<?php echo $compteuserid; ?>";
     var matiereid="<?php echo $matiereidcourses; ?>";
     var courseid="<?php echo $courseid; ?>";
     var codeEtab="<?php echo $codeEtabsession ?>";
     var sessionEtab="<?php echo $libellesessionencours ?>";

     Swal.fire({
   title: '<?php echo L::WarningLib ?>',
   text: "<?php echo L::DoyouDeletedDevoirssupport ?>",
   type: 'warning',
   showCancelButton: true,
   confirmButtonColor: '#3085d6',
   cancelButtonColor: '#d33',
   confirmButtonText: '<?php echo L::DeleteLib ?>',
   cancelButtonText: '<?php echo L::AnnulerBtn ?>',
   }).then((result) => {
   if (result.value) {

     var etape=2;

     $.ajax({

          url: '../ajax/devoirs.php',
          type: 'POST',
          async:true,
          data: 'teatcherid=' + teatcherid+ '&etape=' + etape+'&classe='+classeid+'&matiere='+matiereid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&courseid='+courseid+'&homeworkid='+id,
          dataType: 'text',
          success: function (content, statut) {

             // $("#FormAddAcademique #codeEtab").val(content);

             location.reload();

          }
        });

   }else {

   }
   })


   }

   function checkFile()
   {
     var files=$("#files").val();
     var nb=$("#files").val().length;

     alert("Bonjour");

     if(nb==0)
     {
       $("#blocsms").show();
     }else {
       $("#blocsms").hide();

     }

   }

   function verouillerOui()
   {
      $("#verouiller").val(1);
     $("#btn21").removeClass('notActive').addClass('active');
     $("#btn22").removeClass('active').addClass('notActive');
   }

   function verouillerNon()
   {
     $("#verouiller").val(0);
     $("#btn22").removeClass('notActive').addClass('active');
     $("#btn21").removeClass('active').addClass('notActive');
   }

   function searchmatiereInit()
   {

     var classe="<?php echo $classeid; ?>";
     var teatcherId="<?php echo $compteuserid; ?>";
     var matiereid="<?php echo $matiereidcourses; ?>";
     var etape=17;


   $.ajax({

        url: '../ajax/matiere.php',
        type: 'POST',
        async:true,
        data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe+'&matiereid='+matiereid,
        dataType: 'text',
        success: function (content, statut) {


          $("#matclasse").html("");
          $("#matclasse").html(content);

        }
      });
   }
 $("#fichier3").dropify({
   messages: {
       "default": "Merci de selectionner le support",
       "replace": "Modifier le support",
       "remove" : "Supprimer le support",
       "error"  : "Erreur"
   }
 });

$("#matclasse").select2();
$("#classeEtab").select2();
$("#destiEtab").select2({
  placeholder: "Selectionner le(s) destinataire(s)",
  allowClear: true,
  tags: true,
  tokenSeparators: [',', ' ']
});

function searchcodeEtab(id)
{
var classe=$("#classeEtab").val();
var teatcherId=id;
var etape=7;
var matiere=$("#matclasse").val();

$.ajax({

     url: '../ajax/matiere.php',
     type: 'POST',
     async:true,
     data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
     dataType: 'text',
     success: function (content, statut) {

        $("#FormAddAcademique #codeEtab").val(content);

     }
   });

}

function searchmatiere(id)
{

  var classe=$("#classeEtab").val();
  var teatcherId=id;
  var etape=6;


$.ajax({

     url: '../ajax/matiere.php',
     type: 'POST',
     async:true,
     data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe,
     dataType: 'text',
     success: function (content, statut) {


       $("#matclasse").html("");
       $("#matclasse").html(content);

     }
   });
}











 $(document).ready(function() {

   searchmatiereInit();
   $('input[name="files"]').fileuploader({
        addMore: true,
        // captions: 'en'
    });

    $("#blocsms").hide();

    $("#FormHomecourses2").validate({

           errorPlacement: function(label, element) {
           label.addClass('mt-2 text-danger');
           label.insertAfter(element);
          },
          highlight: function(element, errorClass) {
           $(element).parent().addClass('has-danger')
           $(element).addClass('form-control-danger')
          },
          success: function (e) {
               $(e).closest('.control-group').removeClass('error').addClass('info');
               $(e).remove();
           },
           rules:{
             files:"required"
           },
           messages:{
             files:"Merci de renseigner l'exercice"
           },
           submitHandler: function(form) {

             var classeid="<?php echo $classeid; ?>";
             var teatcherid="<?php echo $compteuserid; ?>";
             var matiereid="<?php echo $matiereidcourses; ?>";
             var courseid="<?php echo $courseid; ?>";
             var codeEtab="<?php echo $codeEtabsession ?>";
             var sessionEtab="<?php echo $libellesessionencours ?>";
             var etape=7;

             var files=$("#files").val();
             var nb=$("#files").val().length;

             alert(nb);

             if(nb==0)
             {
               $("#blocsms").show();
             }else {
               $("#blocsms").hide();

             }


             // $.ajax({
             //
             //      url: '../ajax/courses.php',
             //      type: 'POST',
             //      async:true,
             //      data: 'teatcherid=' + teatcherid+ '&etape=' + etape+'&classe='+classeid+'&matiere='+matiereid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&courseid='+courseid+'&libellehome='+libellehome,
             //      dataType: 'text',
             //      success: function (content, statut) {
             //
             //         // $("#FormAddAcademique #codeEtab").val(content);
             //
             //         location.reload();
             //
             //      }
             //    });



           }
    });


   $("#FormAddAcademique").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
    },
    highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
    },
    success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
     rules:{

       programme:"required",
       descri:"required",
       classeEtab:"required",
       matclasse:"required",
       fichier:"required",
       durationcourse:"required",
       detailscourse:"required",
       datecourse:"required",
       libellecourse:"required"

     },
   messages: {
     programme:"Merci de renseigner le libellé du programme",
     descri:"Merci de renseigner le détails de la fiche ",
     classeEtab:"<?php echo L::PleaseSelectclasseOnerequired ?>",
     matclasse:"<?php echo L::SubjectSelectedrequired ?>",
     fichier:"Merci de selectionner le fichier du programme",
     durationcourse:"<?php echo L::DurationcourseRequired ?>",
     detailscourse:"<?php echo L::InstructionsDevoirsRequired ?>",
     datecourse:"<?php echo L::DateLimitDevoirsRequired ?>",
     libellecourse:"<?php echo L::DevoirsLibsRequired ?>"
   },
   submitHandler: function(form) {

   form.submit();



   }

   });

   // AddsectionRow();
   //  AddcompRow();







 });

 </script>
    <!-- end js include path -->
  </body>

</html>
