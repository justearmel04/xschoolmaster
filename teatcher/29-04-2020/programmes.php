<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Teatcher.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];

$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$localadmins= new Localadmin();
$parents=new ParentX();
$classe=new Classe();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];


  $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
}else {
  $nbclasse=0;
}

$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);



if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();

$programmes=$etabs->getAllprogrammesOfThisTeatcher($_SESSION['user']['IdCompte'],$libellesessionencours);


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Programmes Academiques</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Programmes academiques</li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

                 <?php

                       if(isset($_SESSION['user']['addprogra']))
                       {

                         ?>
                         <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                       <?php
                       //echo $_SESSION['user']['addetabok'];
                       ?>
                       <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
                          </a>
                       </div-->
               <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
               <script src="../assets/js/sweetalert2.min.js"></script>

                   <script>
                   Swal.fire({
                   type: 'success',
                   title: 'Félicitations',
                   text: '<?php echo $_SESSION['user']['addprogra'] ?>',

                   })
                   </script>
                         <?php
                         unset($_SESSION['user']['addprogra']);
                       }

                        ?>


                 <div class="row">
          <div class="col-md-12">
              <div class="card  card-box">
                  <div class="card-head">
                      <header></header>

                  </div>
                  <div class="card-body ">
                    <div class="pull-right">
                      <a href="wizard.php" class="btn btn-warning btn-md"><i class="fa fa-plus"></i>Programmes </a>
                    </div>
                    <div class="table-scrollable">
                      <table class="table table-hover table-checkable order-column full-width" id="example4">
                          <thead>
                              <tr>
                                  <th>Classe</th>
                                  <th> Matiere </th>
                                  <th> Description </th>
                                  <th> Objectif </th>
                                  <th> Compétences visées</th>
                                  <!--th> Action </th-->
                              </tr>
                          </thead>
                          <tbody>

                            <?php
                            // var_dump($programmes);
                            foreach ($programmes as $value):
                             ?>
        <tr class="odd gradeX" ondblclick="detailsyllabus( <?php echo $value->id_syllab?>,<?php echo $value->idteatcher_syllab?>,'<?php echo $value->codeEtab_syllab ?>')">

          <td><?php echo $value->libelle_classe;?></td>
          <td><?php echo $value->libelle_mat;?></td>
          <td>
        <?php echo $value->descri_syllab; ?>
          </td>
          <td>
            <ul>
              <?php
              $objectifs=$etabs->getallObjectifsOfthisSyllabus($value->id_syllab,$value->idteatcher_syllab,$value->idmatiere_syllab,$value->session_syllab);

              foreach ($objectifs as $value) :
              ?>
              <li><?php echo $value->libelle_syllabob ?></li>
              <?php
              endforeach;
               ?>
            </ul>
          </td>
          <td><ul>
            <?php
            $competences=$etabs->getallCompentencesOfthisSyllabus($value->id_syllab,$value->idteatcher_syllab,$value->idmatiere_syllab,$value->session_syllab);

            foreach ($competences as $value) :
            ?>
            <li><?php echo $value->libelle_syllabcomp ?></li>
            <?php
            endforeach;
             ?>
          </ul></td>
          <!--td class="center">
            <a href="edit_staff.html" class="btn btn-tbl-edit btn-xs">
              <i class="fa fa-pencil"></i>
            </a>
            <a class="btn btn-tbl-delete btn-xs">
              <i class="fa fa-trash-o "></i>
            </a>
          </td-->
        </tr>
        <script type="text/javascript">

        function detailsyllabus(programme,teatcher,codeEtab)
        {
          document.location.href="detailsyllabus.php?programme="+programme+"&teatcher="+teatcher+"&codeEtab="+codeEtab
        }

        </script>
        <?php
      endforeach;
         ?>
      </tbody>
                      </table>
                      </div>
                  </div>
              </div>
          </div>
      </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- data tables -->
    <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 	<script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
    <script src="../assets2/js/pages/table/table_data.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
	<script src="../assets2/js/theme-color.js" ></script>
	<!-- Material -->
	<script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   $(document).ready(function() {



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
