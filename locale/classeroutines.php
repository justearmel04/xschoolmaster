<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement
$schoolsofassign=$etabs->getEtablissementbyCodeEtab($codeEtabAssigner);

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$codesEtab=$schoolsofassign;

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
//
if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}
//nous allons determine le type d'etablissement

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
  <!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--bootstrap -->
  <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
  <link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
  <link href="../assets2/css/material_style.css" rel="stylesheet">
  <!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

  <!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title">Liste des classes</div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="#">Elèves</a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active">Liste des classes</li>
                          </ol>
                      </div>
                  </div>


					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['addclasseok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: 'Félicitations',
            text: '<?php echo $_SESSION['user']['addclasseok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['addclasseok']);
                }

                 ?>

          <?php

                if(isset($_SESSION['user']['updateclasseok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: 'Félicitations',
            text: '<?php echo $_SESSION['user']['updateclasseok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['updateclasseok']);
                }

                 ?>



          <?php

                if(isset($_SESSION['user']['delclasseok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: 'Félicitations',
            text: '<?php echo $_SESSION['user']['delclasseok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['delclasseok']);
                }

                 ?>
<!-- start widget -->
<?php

      if(isset($_SESSION['user']['updateteaok']))
      {

        ?>
        <!--div class="alert alert-success alert-dismissible fade show" role="alert">
      <?php
      //echo $_SESSION['user']['addetabok'];
      ?>
      <a href="#" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
         </a>
      </div-->
<link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
<script src="../assets/js/sweetalert2.min.js"></script>

  <script>
  Swal.fire({
  type: 'success',
  title: 'Félicitations',
  text: '<?php echo $_SESSION['user']['updateteaok']; ?>',

  })
  </script>
        <?php
        unset($_SESSION['user']['updateteaok']);
      }

       ?>
<!-- end widget -->
<?php

      if(isset($_SESSION['user']['addetabexist']))
      {

        ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <?php
      echo $_SESSION['user']['addetabexist'];
      ?>
      <a href="#" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
         </a>
      </div>



        <?php
        unset($_SESSION['user']['addetabexist']);
      }

       ?>

       <div class="row">
         <!-- <div class="row" style="background-color:red"> -->
           <div class="col-md-4">
             <!-- <a class="btn btn-primary " style="border-radius:5px;" href="addlocaladmin.php"><i class="fa fa-plus"></i> Nouveau administrateur local</a> -->

           </div>
           <div class="col-md-4">
             <!-- <a class="btn btn-primary " style="border-radius:5px;" href="addlocaladmin.php"><i class="fa fa-plus"></i> Nouveau administrateur local</a> -->

           </div>
           <div class="col-md-4">
             <a class="btn btn-primary pull-right" style="border-radius:5px;" href="routinesadd.php"><i class="fa fa-plus"></i> Emploi du temps</a>

           </div>

         <!-- </div> -->
       </br></br>
         <div class="row">


         <?php
         if($etablissementType==1||$etablissementType==3)
         {
          ?>

         <?php
       }else {
         ?>


               <?php
               $i=1;
               if(isset($_POST['search']))
               {
                 $content="";
                   if($_POST['codeetab']!="")
                   {

                     if(strlen($_POST['classex'])>0)
                     {
                       //recherche en fonction de l'id de l'enseignant et code etab
                       $classes=$classe->getClassesByschoolCodewithId($_POST['codeetab'],$_POST['classex']);
                     }else {
                       //recherche tous les enseignants en fonction du code etablissement
                       //$teatchers=$teatcher->getAllTeatchers();
                       $classes=$classe->getAllClassesByschoolCode($_POST['codeetab']);

                     }

                     $content="bonjour";

                   }else

                    {
                     $content="bonsoir";
                       $classes=$classe->getAllClassesByClasseId($_POST['classex']);
                   }
                 }

// var_dump($classes);
                 foreach ($classes as $value):
                 ?>
                 <div class="col-md-3 col-sm-4" ondblclick="myFunction(<?php echo $value->id_classe?>)">
                      <div class="card card-box">
                          <div class="card-body no-padding ">
                            <div class="doctor-profile">
                               <?php
                                 $lienImg="";
                                 $lienImg="../assets2/img/salles.jpg";
                                ?>
                            <img src="<?php echo $lienImg?>" class="" alt="" style="border-radius:5px;width:100%;padding:0px;margin:0px">
                                <div class="profile-usertitle">
                                    <div class="doctor-name" style="cursor:pointer;text-decoration:none;color:#71d40f"><a href="routines.php?classe=<?php echo $value->id_classe ?>"><?php echo $value->libelle_classe;?></a></div>

                                </div>
                                <div class="user-btm-box">
									<div class="row">
										<div class="col-md-4 col-sm-4 text-center">
											<p class="text-purple"><i class="fa fa-female fa-2x "></i></p>
											<h3><?php
                      $nbredoubfilles=$classe->getAllStudentFilleRedtOfThisClassesNb($value->id_classe,$value->session_classe);
                      $nbnredoubfilles=$classe->getAllStudentFilleNRedtOfThisClassesNb($value->id_classe,$value->session_classe);
                      $sommefemmes=$nbnredoubfilles+$nbredoubfilles;
                      echo $sommefemmes;
                       ?></h3>
										</div>
										<div class="col-md-4 col-sm-4 text-center">
											<p class="text-success"><i class="fa fa-child fa-2x "></i></p>
											<h3><?php
                      $nbredoubmecs=$classe->getAllStudentMecRedtOfThisClassesNb($value->id_classe,$value->session_classe);
                      $nbnredoubmecs=$classe->getAllStudentMecNRedtOfThisClassesNb($value->id_classe,$value->session_classe);
                      $sommehommes=$nbredoubmecs+$nbnredoubmecs;
                      echo $sommehommes;
                       ?></h3>
										</div>
										<div class="col-md-4 col-sm-4 text-center">
											<p class="text-warning"><i class="fa fa-group fa-2x "></i></p>
											<h3><?php echo $classe->getAllStudentOfThisClassesNb($value->id_classe,$value->session_classe); ?></h3>
										</div>
									</div>

								</div>



                                <div class="profile-userbuttons">


                                </div>
                              </div>
                          </div>
                      </div>
                </div>


<?php
                            $i++;
                            endforeach;
                            ?>


         <?php
       }
          ?>
          </div>
      </div>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function searchlibetab()
   {

     var etape=2;

     $.ajax({
              url: '../ajax/school.php',
              type: 'POST',
              async:false,
              data: 'code='+ $("#codeetab").val()+'&etape='+etape,
              dataType: 'text',
              success: function (content, statut) {

                $("#libetab").html("");
                $("#libetab").html(content);

                //nous allons chercher la liste des admins local de cet etablissement

                $.ajax({
                         url: '../ajax/classe.php',
                         type: 'POST',
                         async:false,
                         data: 'code='+ $("#codeetab").val()+'&etape='+etape,
                         dataType: 'text',
                         success: function (response, statut) {


                           $("#classex").html("");
                           $("#classex").html(response);
                         }
                       });

              }

            });

   }
   $(document).ready(function() {

     $('#example5').DataTable( {
         "scrollX": true

     } );

     $("#codeetab").select2();

     $("#libetab").select2();
     $("#classex").select2();

   });

   </script>
    <!-- end js include path -->
  </body>

</html>
