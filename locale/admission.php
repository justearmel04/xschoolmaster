<?php
session_start();
require_once('../class/User.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$etabs=new Etab();
$etab=new Etab();
$parent=new ParentX();
$user=new User();
$classe=new Classe();
$emailUti=$_SESSION['user']['email'];
$userId=$_SESSION['user']['IdCompte'];
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$codeEtabLocal=$etab->getcodeEtabByLocalId($userId);
$classes=$classe->getAllClassesbyschoolCode($codeEtabLocal);
$parents=$parent->getAllParentSchool($codeEtabLocal);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabLocal);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabLocal);


if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabLocal);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabLocal,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabLocal,$libellesessionencours);
}


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title">Créer une fiche</div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="#">Elèves</a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active">Créer une fiche</li>
                          </ol>
                      </div>
                  </div>

					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['addStudok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
<link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
<script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
title: '<?php echo L::Felicitations ?>',
text: "<?php echo $_SESSION['user']['addStudok']; ?>",
type: 'success',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::AddNews ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {

}else {
document.location.href="index.php";
}
})
            </script>
                  <?php
                  unset($_SESSION['user']['addStudok']);
                }

                 ?>

                 <div class="row">
                   <?php
                   if($nbsessionOn==0)
                   {
                    ?>
                   <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="card card-topline-green">
                          <div class="card-head">
                              <header>Informations</header>
                              <div class="tools">
                                  <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
          <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
          <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                              </div>
                          </div>
                          <div class="card-body ">
                            <p>
                              Afin de pouvoir faire l'admission de nouveaux élèves nous vous invitons à mentionner la session en cours dans l'onglet SESSIONS


                            </p>
                          </div>
                      </div>
                               </div>
                               <?php
                             }
                                ?>
                     <div class="col-md-12 col-sm-12">
                         <div class="card card-box">
                             <div class="card-head">
                                 <header>Informations Elève</header>
                                  <!--button id = "panel-button"
                                class = "mdl-button mdl-js-button mdl-button--icon pull-right"
                                data-upgraded = ",MaterialButton">
                                <i class = "material-icons">more_vert</i>
                             </button>
                             <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                data-mdl-for = "panel-button">
                                <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                                <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                                <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                             </ul-->
                             </div>

                             <div class="card-body" id="bar-parent">
                                 <!-- <form  id="FormAddStudent" class="form-horizontal" action="../controller/admission.php" method="post" enctype="multipart/form-data"> -->
                                 <form  id="FormAddStudent" class="form-horizontal" action="../controller/preinscription.php" method="post" enctype="multipart/form-data">

                                     <div class="form-body">
                                       <?php
                                       if($etablissementType==1||$etablissementType==3)
                                       {
                                         ?>
                                         <input type="hidden" name="statuseleve" id="statuseleve" value="0">
                                         <?php
                                       }else {
                                         ?>

                                         <div class="form-group row">
                                           <label class="control-label col-md-3">
                                           <span class="required">  </span>
                                           </label>
                                           <div class="col-sm-7 col-md-7">
                                           <div class="input-group">
                                           <div id="radioBtn2" class="btn-group">
                                           <a class="btn btn-primary btn-sm active" id="btn21" data-toggle="inscript" data-title="1" onclick="inscription()">INSCRIPTION</a>
                                           <a class="btn btn-primary btn-sm notActive" id="btn22" data-toggle="inscript" data-title="2" onclick="reinscription()">REINSCRIPTION</a>
                                           </div>
                                           <input type="hidden" name="inscript" id="inscript" value="1">
                                           </div>
                                           </div>

                                         </div>

                                         <div class="form-group row">
                                             <label class="control-label col-md-3">Statut
                                                 <span class="required">*  </span>
                                             </label>
                                             <div class="col-md-6">
                                               <select class="form-control " id="statuseleve" name="statuseleve" style="width:100%">
                                                   <option value="">Selectionner un statut</option>
                                                   <option value="1">Affecté</option>
                                                   <option value="2">Non affecté</option>

                                               </select>
                                               </div>
                                         </div>
                                         <?php
                                       }
                                        ?>
                                        <div class="form-group row">
                                  <!--label for="happy" class="col-sm-4 col-md-4 control-label text-right">Are you happy ?</label-->
                                  <label class="control-label col-md-3">
                                  <span class="required">  </span>
                                  </label>
                                  <div class="col-sm-7 col-md-7">
                                  <div class="input-group">
                                  <div id="radioBtn" class="btn-group">
                                  <a class="btn btn-primary btn-sm active" data-toggle="doublant" data-title="Y" onclick="doublantN()">NRED</a>
                                  <a class="btn btn-primary btn-sm notActive" data-toggle="doublant" data-title="N" onclick="doublant()">RED</a>
                                  </div>
                                  <input type="hidden" name="doublant" id="doublant" value="0">
                                  </div>
                                  </div>
                                  </div>
                                        <div class="form-group row">
       		<!--label for="happy" class="col-sm-4 col-md-4 control-label text-right">Are you happy ?</label-->
           <label class="control-label col-md-3">
               <span class="required">  </span>
           </label>
         	<div class="col-sm-7 col-md-7">
       			<div class="input-group">
       				<div id="radioBtn" class="btn-group">
       					<a class="btn btn-primary btn-sm active" data-toggle="happy" data-title="Y" onclick="manuel()">Matricule manuel</a>
       					<a class="btn btn-primary btn-sm notActive" data-toggle="happy" data-title="N" onclick="automatique()">Matricule automatique</a>
       				</div>
       				<input type="hidden" name="matriculestate" id="matriculestate" value="1">
       			</div>
       		</div>
       	</div>


         <div class="form-group row " id="matriculerowInscript">
                 <label class="control-label col-md-3">Matricule
                     <span class="required"> * </span>
                 </label>
                 <div class="col-md-6">
                     <input type="text" name="matri" id="matri" data-required="1" placeholder="Entrer le Matricule" class="form-control" onchange="checkmatri()"/>

                   </div>
             </div>
             <div class="form-group row " id="matriculerowReinscript">
                     <label class="control-label col-md-3">Matricule
                         <span class="required"> * </span>
                     </label>
                     <div class="col-md-6">
                       <select class="form-control" name="matriselect" id="matriselect" style="width:100%" onchange="checkmatri()">
                         <option value="">Selectionner le matricule d'un élève</option>
                         <?php
                           foreach ($allstudentschools as $value):
                             ?>
                               <option value="<?php echo utf8_encode(utf8_decode($value->matricule_eleve)); ?>"><?php echo utf8_encode(utf8_decode($value->matricule_eleve)); ?></option>
                             <?php
                           endforeach;
                          ?>

                       </select>


                       </div>
                 </div>
                                     <div class="form-group row">
                                             <label class="control-label col-md-3"><?php echo L::Name?>
                                                 <span class="required"> * </span>
                                             </label>
                                             <div class="col-md-6">
                                                 <input type="text" name="nomad" id="nomad" data-required="1" placeholder="<?php echo L::EnterName ?>" class="form-control " /> </div>
                                                 <?php
                                                 if($nbsessionOn>0)
                                                 {
                                                   ?>
                                                   <input type="hidden" name="sessionscolaire" id="sessionscolaire" value="<?php echo $libellesessionencours; ?>">
                                                   <?php
                                                 }
                                                  ?>
                                         </div>
                                         <div class="form-group row">
                                             <label class="control-label col-md-3"><?php echo L::PreName?>
                                                 <span class="required"> * </span>
                                             </label>
                                             <div class="col-md-6">
                                                 <input type="text" name="prenomad" id="prenomad" data-required="1" placeholder="Entrer le prénom" class="form-control " /> </div>
                                                 <input type="hidden" name="etape" id="etape" value="1"/>
                                                 <input type="hidden" name="newStudent" id="newStudent" value="1"/>
                                                 <input type="hidden" name="codeEtab" id="codeEtab"  value="<?php echo $codeEtabLocal;?>"/>
                                                 <input type="hidden" name="nbparent" id="nbparent"  value="0"/>
                                                 <input type="hidden" name="nboldparent" id="nboldparent"  value="0"/>
                                                 <input type="hidden" name="concatparents" id="concatparents"  value=""/>
                                                 <input type="hidden" name="concatoldparents" id="concatoldparents"  value=""/>

                                         </div>


                                         <div class="form-group row">
                                           <label class="control-label col-md-3"><?php echo L::BirthstudentTab?>
                                               <span class="required"> * </span>
                                           </label>
                                               <div class="col-md-6">
                                                   <input type="text" placeholder="<?php echo L::EnterBirthstudentTab?>" name="datenaisad" id="datenaisad" data-mask="99/99/9999" class="form-control ">
                                                     <span class="help-block"><?php echo L::Datesymbole ?></span>
                                               </div>
                                           </div>
                                           <div class="form-group row">
                                                   <label class="control-label col-md-3">Lieu Naissance
                                                       <span class="required"> * </span>
                                                   </label>
                                                   <div class="col-md-6">
                                                       <input type="text" name="lieunais" id="lieunais" data-required="1" placeholder="Entrer le Lieu de Naissance" class="form-control " /> </div>
                                               </div>

                                           <div class="form-group row">
                                               <label class="control-label col-md-3">Genre
                                                   <span class="required">*  </span>
                                               </label>
                                               <div class="col-md-6">
                                                 <select class="form-control " id="sexe" name="sexe" style="width:100%">
                                                     <option value="">Selectionner un Genre</option>
                                                     <option value="M">Masculin</option>
                                                     <option value="F">Feminin</option>

                                                 </select>
                                                 </div>
                                           </div>

                                           <div class="form-group row">
                                               <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                                   <span class="required">*  </span>
                                               </label>
                                               <div class="col-md-6">
                                                 <select class="form-control " id="classeEtab" name="classeEtab" style="width:100%">
                                                     <option value=""><?php echo L::Selectclasses ?></option>
                                                     <?php
                                                     $i=1;
                                                       foreach ($classes as $value):
                                                       ?>
                                                       <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                       <?php
                                                                                        $i++;
                                                                                        endforeach;
                                                                                        ?>

                                                 </select>
                                                 </div>
                                           </div>
                                           <!-- <div class="form-group row">
                                               <label class="control-label col-md-3">Parents
                                                   <span class="required">*  </span>
                                               </label>
                                               <div class="col-md-8">
                                                 <select class="form-control " multiple="multiple" id="parenta" name="parenta[]" style="width:100%">
                                                     <option value="">Selectionner un parent</option>
                                                     <?php
                                                     //$i=1;
                                                       //foreach ($parents as $value):
                                                       ?>
                                                       <option value="<?php //echo utf8_encode(utf8_decode($value->id_compte)); ?>"><?php //echo utf8_encode(utf8_decode($value->nom_parent." - ".$value->prenom_parent)); ?></option>

                                                       <?php
                                                                                       // $i++;
                                                                                        //endforeach;
                                                                                        ?>

                                                 </select>
                                                 </div>
                                           </div> -->
                                           <div id="dynamic_field">

                                           </div>
                                           <div class="form-group row">
                                             <div class="offset-md-3 col-md-9">

                                                 <button type="button" class="btn btn-primary" id="parentNew"> <i class="fa fa-plus"></i> <?php echo L::NewParent ?> </button>
                                                 <button type="button" class="btn btn-primary" id="parentOld" data-toggle="modal" data-target="#exampleModal" > <i class="fa fa-plus"></i> Ancien parent </button>


                                             </div>
                                           </div>
                                           <div class="form-group row">
                                                   <label class="control-label col-md-3">Allergie

                                                   </label>
                                                   <div class="col-md-6">
                                                       <input type="text" name="allergie" id="allergie" data-required="1" placeholder="Entrer une allergie" class="form-control " /> </div>
                                               </div>
                                               <div class="form-group row">
                                                       <label class="control-label col-md-3">Condition physique

                                                       </label>
                                                       <div class="col-md-6">
                                                           <input type="text" name="condphy" id="condphy" data-required="1" placeholder="Entrer la condition physique" class="form-control " /> </div>
                                                   </div>
                                           <div class="form-group row">
                                               <label class="control-label col-md-3"><?php echo L::Pictures?>

                                               </label>
                                               <div class="compose-editor">
                                                 <input type="file" id="photoad" name="photoad" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="../photo/user5.jpg" data-allowed-file-extensions="gif png jpg jpeg pjpeg" />
                                               </div>
                                           </div>



                   <div class="form-actions">
                                         <div class="row">
                                             <div class="offset-md-3 col-md-9">


                                                 <button type="submit" id="submitBtn"class="btn btn-info" >Enregistrer</button>


                                                 <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                             </div>
                                           </div>
                                        </div>
                 </div>
                                 </form>

                                 <div class="modal fade" id="exampleModal"  tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                   <div class="modal-dialog modal-lg" role="document">

                                     <div class="modal-content">

                                       <div class="modal-header">
                                          <h4 class="modal-title" id="exampleModalLabel">Sélectionner un parent</h4>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                          </button>
                                      </div>
                                      <div class="modal-body">

                                        <div id="divrecherchestation">

                                          <p>Critère de recherche</p>

                                          <form class = "" id = "FormSearch">
                                            <div class="row">
                                                                <div class="col-md-6">
                                            <div class="form-group">
                                            <label for=""><b>Nom parent<span class="required">  </span> :</b></label>

                                          <input type="text" name="nomserach" id="nomserach" data-required="1" placeholder="<?php echo L::ParentName; ?>" class="form-control" />
                                            </div>

                                            </div>
                                            <div class="col-md-6">
                                                 <div class="form-group">
                                                   <label for=""><b>Telephone  parent <span class="required">  </span>: </b></label>
                                                    <input type="text" name="telserach" id="telserach" data-required="1" placeholder="Télephone parent" class="form-control" />
                                                  </div>


                                             </div>
                                             <div class="col-md-6">
                                                 <button type="submit" class="btn btn-danger" id="searchbtn"><i class="fa fa-search"></i> Rechercher</button>
                                                 <img src="../photo/ajax-loader.gif" id="imgLoader" style="display:none;" />

                                              </div>


                                                          </div>
                                          </form>
                                          <br>
                                          <table class="table table-striped table-bordered table-hover">
                    <thead style="background-color:#fbba16; color:white; font-weight: bold;">
                    <tr>
                    <th style="text-align:center">Nom</th>
                    <th class="visible-lg" style="text-align:center">Prénoms </th>
                    <th style="text-align:center"> Télephone mobile </th>
                    <th style="text-align:center">Email </th>
                  </tr>
                   </thead>
                   <tbody id="tabStationBody">

                  <tr>

                    <td colspan="4">Aucune Ligne</td>

                  </tr>

                                    </tbody>
                                     </table>

                                        </div>

                                        <div class="row" id="RowInfosOld">
                                          <input type="hidden" id="parentaold" name="parentaold"value="">
                                          <div class="col-md-12">
                                            <div class="form-group" style="display:none">
                                     <label for="benef">Parent<span class="required">*</span> :</label>
                                     <!--select class="form-control input-height"  id="parentaold" name="parentaold" style="width:100%" onchange="selectparentsinfos()">
                                         <option value="">Selectionner un parent</option>
                                         <?php
                                         $i=1;
                                           //foreach ($parents as $value):
                                           ?>
                                           <option value="<?php //echo utf8_encode(utf8_decode($value->id_compte)); ?>"><?php //echo utf8_encode(utf8_decode($value->nom_parent." - ".$value->prenom_parent)); ?></option>

                                           <?php
                                                                            $i++;
                                                                            //endforeach;
                                                                            ?>

                                     </select-->

                                             </div>


                                             </div>

                                             <div class="col-md-12">
                                              <div class="form-group">
                                       <label for="oldparentname">Nom<span class="required">*</span> :</label>
                                <input type="text" name="oldparentname" class="form-control" id="oldparentname" value="" size="32" maxlength="225" />

                                                 </div>
                                               </div>
                                               <div class="col-md-12">
                                                <div class="form-group">
                                         <label for="oldparentprename">Prénoms<span class="required">*</span> :</label>
                                  <input type="text" name="oldparentprename" class="form-control" id="oldparentprename" value="" size="32" maxlength="225" />

                                                   </div>
                                                 </div>
                                                 <div class="col-md-12">
                                                  <div class="form-group">
                                           <label for="oldparentprename">Genre<span class="required">*</span> :</label>
                                    <input type="text" name="oldparentsexe" class="form-control" id="oldparentsexe" value="" size="32" maxlength="225" />

                                                     </div>
                                                   </div>
                                                 <div class="col-md-12">
                                                  <div class="form-group">
                                           <label for="oldparentphone">Téléphone<span class="required">*</span> :</label>
                                    <input type="text" name="oldparentphone" class="form-control" id="oldparentphone" value="" size="32" maxlength="225" />

                                                     </div>
                                                   </div>
                       <!-- <button type="submit" name="KT_Insert1" id="KT_Insert1" class="btn btn-primary pull-right" style="">Valider <i class="icon-check position-right"></i></button> -->


                                        </div>

                                      </div>
                                      <div class="modal-footer">
                                          <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo L::Closebtn  ?></button>
                                          <button type="button" class="btn btn-primary" id="addoldbtn" onclick="oldbtnaction()" disabled><i class="fa fa-plus"></i> Ajouter</button>
                                      </div>

                                     </div>

                                   </div>

                                 </div>
                             </div>
                         </div>
                     </div>

                 </div>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script>
 function addFrench()
 {
   var etape=1;
   var lang="fr";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function addEnglish()
 {
   var etape=1;
   var lang="en";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }


 jQuery(document).ready(function() {



$("#parenta").select2();
// $("#parentaold").select2();
$("#sexe").select2();
$("#classeEtab").select2();

desactivatebtn();

<?php
if($etablissementType==1||$etablissementType==3)
{

}else
{
  ?>
$("#statuseleve").select2();
  <?php
}
 ?>

 $('#parentNew').click(function(){
       AddparentsRow();

     });

     $("#matriculerowReinscript").hide();
     $("#RowbtnDoublant").hide();



     $("#FormSearch").validate({
       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
     },
     highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
     },
     success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },
   rules:{
     nomserach: {
      'required': {
         depends: function (element) {
             return ($("#telserach").val()=="");

         }
     }
   },
   telserach: {
     digits:true,
    'required': {
       depends: function (element) {
           return ($("#nomserach").val()=="");

       }
   }
 }

   },
   messages: {
 nomserach:"Merci de renseigner au moins le nom du parent",
 telserach:{
   required:"Merci de renseigner au moins le numéro de telephone",
   digits:"<?php echo L::DigitsOnly?>"
 }


 },
 submitHandler: function(form) {

   //nous allons rechercher le parent
    $("#submitBtn").attr("disabled",true);
   var nomserach=$("#nomserach").val();
   var telserach=$("#telserach").val();
   var codeEtab="<?php echo $codeEtabLocal; ?>";
   var concatoldparents=$("#concatoldparents").val();
   var etape=6;
      $.ajax({
        url: '../ajax/parent.php',
        type: 'POST',
        async:false,
        data: 'nom=' + nomserach+ '&etape=' + etape+'&telephone='+telserach+'&codeEtab='+codeEtab+'&concatoldparents='+concatoldparents,
        dataType: 'text',
        beforeSend: function (xhr) {

             // $("#imgLoader").css("display", "inline");
             $("#imgLoader").show();
         //     setTimeout(function () {
         //     $("#imgLoader").show();
         // }, 100);

           },
        success: function (content, statut) {
        // $("#nbtotalparent").val(0);
        $("#RowInfosOld").hide();
        $("#imgLoader").hide(2000);
        $("#tabStationBody").html("");
        $("#tabStationBody").html(content);

        }
      });

 }

     });


   $("#FormAddStudent").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{
        lieunais:"required",
        matri:"required",
        parenta:"required",
        classeEtab:"required",
        sexe:"required",
        passad: {
            required: true,
            minlength: 6
        },
        confirmtad:{
            required: true,
            minlength: 6,
            equalTo:'#passad'
        },
        fonctionad:"required",
        statuseleve:"required",

        loginad:"required",
        emailad: {
                   required: true,
                   email: true
               },
        contactad:"required",
        datenaisad:"required",
        prenomad:"required",
        nomad:"required"


      },
      messages: {
        lieunais:"Merci de renseigner le Lieu de naissance",
        matri:"Merci de renseigner le Matricule",
        parenta:"Merci de selectionner le parent",
        classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
        sexe:"Merci de selectionner le Genre",
        statuseleve:"Merci de selectionner le statut de l'élève",
        confirmtad:{
            required:"<?php echo L::Confirmcheck?>",
            minlength:"<?php echo L::Confirmincheck?>",
            equalTo: "<?php echo L::ConfirmSamecheck?>"
        },
        passad: {
            required:"<?php echo L::Passcheck?>",
            minlength:"<?php echo L::Confirmincheck?>"
        },
        loginad:"<?php echo L::Logincheck?>",
        emailad:"<?php echo L::PleaseEnterEmailAdress?>",
        contactad:"<?php echo L::PleaseEnterPhoneNumber?>",
        datenaisad:"<?php echo L::PleaseEnterPhonestudentTab?>",
        prenomad:"<?php echo L::PleaseEnterPrename?>",
        nomad:"<?php echo L::PleaseEnterName?>",
        fonctionad:"Merci de renseigner la fonction"
      },
      submitHandler: function(form) {
        //verifier si ce compte n'existe pas encore dans la base de données
           var etape=1;
           $.ajax({
             url: '../ajax/admission.php',
             type: 'POST',
             async:false,
             data: 'matricule=' + $("#matri").val()+ '&etape=' + etape+'&classe='+$("#classeEtab").val()+'&parent='+$("#parenta").val(),
             dataType: 'text',
             success: function (content, statut) {

               if(content==0)
               {
                 //le compte n'existe pas dans la base on peut l'ajouter

                 form.submit();
               }else if(content==1){
                 //le compte existe dejà dans la base de données
                 Swal.fire({
                 type: 'warning',
                 title: '<?php echo L::WarningLib ?>',
                 text: "Cet Elève existe dejà dans la base de données des elèves incrits pour l'année encours",

               })

             }else if(content==2)
             {
               $("#newStudent").val(0);
               form.submit();
             }

             }


           });
      }


   });


 });

 $('#exampleModal').on('shown.bs.modal', function () {
   // console.log("bonsoir");
   //nous allons initialiser le tableau
    $("#tabStationBody").html("");
    $("#nomserach").val("");
    $("#telserach").val("");
    $("#RowInfosOld").hide();

    var content="<tr><td colspan=\"4\">Aucune Ligne</td><tr>";
    $("#tabStationBody").html(content);

 });

 $("#submitBtn").attr("disabled",true);
 $("#RowInfosOld").hide();

 function oldbtnaction()
 {
   var parentid= $("#parentaold").val();
   var nom=$("#oldparentname").val();
   var prenom=$("#oldparentprename").val();
   var phone=$("#oldparentphone").val();
   var sexe=$("#oldparentsexe").val();

   var concatoldparents=$("#concatoldparents").val();

   var nb=$("#nboldparent").val();
   var nouveau= parseInt(nb)+1;
   // var concatparents=$("#concatparents").val();
   // $("#concatparents").val(concatparents+nouveau+"@");
   $("#concatoldparents").val(concatoldparents+parentid+"@");

   //recalcule nbconcatparent
   recalculoldparentnb();

   var ligne="<div class=\"form-group row\" id=\"ligneEnteteold"+parentid+"\">";
   ligne=ligne+"<span class=\"label label-md label-info\"  style=\"text-align:center;margin-left:400px;font-family: \"Trebuchet MS\", Verdana, sans-serif;font-style: italic;font-variant-ligatures: no-common-ligatures;font-size:large;\">INFORMATIONS ANCIEN PARENT " +nouveau+"</span>";
   ligne=ligne+"</div>";
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowNameold"+parentid+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Nom <span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-6\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"nomparentold"+parentid+"\"  id=\"nomparentold"+parentid+"\" value="+nom+" readonly >";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //prenoms
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPrenameold"+parentid+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Prénoms <span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-6\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"prenomparentold"+parentid+"\"  id=\"prenomparentold"+parentid+"\" value="+prenom+" readonly>";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //sexe
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowSexeold"+parentid+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Genre <span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-6\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"sexeparentold"+parentid+"\"  id=\"sexeparentold"+parentid+"\" value="+sexe+" readonly>";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";

   //telephone
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPhoneold"+parentid+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Téléphone <span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-6\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"phoneparentold"+parentid+"\"  id=\"phoneparentold"+parentid+"\" value="+phone+" readonly>";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //bouton de suppression
   ligne=ligne+"<div class=\"pull-right\" id=\"ligneRowold"+parentid+"\">";
   ligne=ligne+"<button type=\"button\" id=\"delete"+parentid+"\" name=\"delete"+parentid+"\"  onclick=\"deletedoldrow("+parentid+")\" class=\"btn btn-danger pull-right\"><i class=\"fa fa-trash\"></i></button>";
   ligne=ligne+"</div></br></br>";

   $('#dynamic_field').append(ligne);

   // $('#exampleModal').hide();
   $('#exampleModal').modal('hide');

 }

 function selectparentsinfos()
 {
   var etape=4;
   $.ajax({
     url: '../ajax/login.php',
     type: 'POST',
     async:false,
     data: 'parentid=' + $("#parentaold").val()+ '&etape=' + etape,
     dataType: 'text',
     success: function (response, statut) {

       var nomparent=response.split("*")[0];
       var prenomparent=response.split("*")[1];
       var phoneparent=response.split("*")[2];
       var sexeparent=response.split("*")[6];

       $("#oldparentname").val(nomparent);
       $("#oldparentprename").val(prenomparent);
       $("#oldparentphone").val(phoneparent);
       $("#oldparentsexe").val(sexeparent);

       $("#addoldbtn").prop("disabled",false);


     }
   });
 }
function activatebtn()
{
  $("#submit1").prop("disabled",false);

}

function desactivatebtn()
{
  $("#submit1").prop("disabled",true);
}
function afficherInfos(id)
{
  $("#RowInfosOld").hide();

  selectparentsinfosOne(id);
}

function selectparentsinfosOne(id)
{



  // $("#parentaold").val(id);

  var etape=4;

  $.ajax({
    url: '../ajax/login.php',
    type: 'POST',
    async:false,
    data: 'parentid=' +id+ '&etape=' + etape,
    dataType: 'text',
    success: function (response, statut) {

      var nomparent=response.split("*")[0];
      var prenomparent=response.split("*")[1];
      var phoneparent=response.split("*")[2];
      var sexeparent=response.split("*")[6];
      var parentid=response.split("*")[11];

      // alert(parentid);

      $("#oldparentname").val(nomparent);
      $("#oldparentprename").val(prenomparent);
      $("#oldparentphone").val(phoneparent);
      $("#oldparentsexe").val(sexeparent);
      $("#parentaold").val(parentid);

      $("#RowInfosOld").show(1000);

      $("#addoldbtn").prop("disabled",false);


    }
  });
}

function deletedrow(id)
{
  var concatparents=$("#concatparents").val();
  $("#concatparents").val($("#concatparents").val().replace(id+"@", ""));

  $("#ligneEntete"+id).remove();
  $("#ligneRowName"+id).remove();
  $("#ligneRowPrename"+id).remove();
  $("#ligneRowPhone"+id).remove();
  $("#ligneRowSexe"+id).remove();
  $("#ligneRow"+id).remove();

  recalculparentnb();

}

function deletedoldrow(parentid)
{
  var concatparents=$("#concatparents").val();
  var concatoldparents=$("#concatoldparents").val();
  // $("#concatparents").val($("#concatparents").val().replace(id+"@", ""));
  $("#concatoldparents").val($("#concatoldparents").val().replace(parentid+"@", ""));

  $("#ligneEnteteold"+parentid).remove();
  $("#ligneRowNameold"+parentid).remove();
  $("#ligneRowPrenameold"+parentid).remove();
  $("#ligneRowPhoneold"+parentid).remove();
  $("#ligneRowSexeold"+parentid).remove();
  $("#ligneRowold"+parentid).remove();

  recalculparentnb();
}

function recalculparentnb()
{
  //calcul du nombre de nouveau parent
  var concatparents=$("#concatparents").val();
  var tab=concatparents.split("@");
  var nbtab=tab.length;
  var nbtabnew=parseInt(nbtab)-1;
  $("#nbparent").val(nbtabnew);
  //calcul du nombre ancien parent
  var concatoldparents=$("#concatoldparents").val();
  var tabold=concatoldparents.split("@");
  var nbtabold=tabold.length;
  var nbtaboldnew=parseInt(nbtabold)-1;
  $("#nboldparent").val(nbtaboldnew);

  var etape=16;

  $.ajax({
    url: '../ajax/admission.php',
    type: 'POST',
    async:true,
    data: 'concatnew=' +nbtabnew+ '&etape=' + etape+'&concatold='+nbtaboldnew,
    dataType: 'text',
    success: function (content, statut) {

      $("#nbtotalparent").val(content);

      if(content==0)
      {
        $("#submitBtn").attr("disabled",true);
      }else if(content>0){
        $("#submitBtn").attr("disabled",false);
      }

    }
  });

  // if((nbtabnew==0)&&(nbtaboldnew==0))
  // {
  //   //actualiser la page
  //
  //    // location.reload();
  //     // desactivatebtn();
  //
  //     $("#nbtotalparent").val(0);
  // }else {
  //   var total=(parseInt(nbtabold)-1)+parseInt(nbtab)-1;
  //   $("#nbtotalparent").val(total);
  // }
}

function recalculoldparentnb()
{
  //calcul du nombre de nouveau parent
  var concatparents=$("#concatparents").val();
  var tab=concatparents.split("@");
  var nbtab=tab.length;
  var nbtabnew=parseInt(nbtab)-1;
  $("#nbparent").val(nbtabnew);
  //calcul du nombre ancien parent
  var concatoldparents=$("#concatoldparents").val();
  var tabold=concatoldparents.split("@");
  var nbtabold=tabold.length;
  var nbtaboldnew=parseInt(nbtabold)-1;
  $("#nboldparent").val(nbtaboldnew);

  var etape=16;

  $.ajax({
    url: '../ajax/admission.php',
    type: 'POST',
    async:true,
    data: 'concatnew=' +nbtabnew+ '&etape=' + etape+'&concatold='+nbtaboldnew,
    dataType: 'text',
    success: function (content, statut) {

      $("#nbtotalparent").val(content);

      if(content==0)
      {
        $("#submitBtn").attr("disabled",true);
      }else if(content>0){
        $("#submitBtn").attr("disabled",false);
      }

    }
  });

  // if((nbtabnew==0)&&(nbtaboldnew==0))
  // {
  //   //actualiser la page
  //
  //    // location.reload();
  //    desactivatebtn();
  // }else if((nbtabnew>0)||(nbtaboldnew>0)){
  //   activatebtn();
  // }else if((nbtabnew<0)||(nbtaboldnew<0))
  // {
  //   desactivatebtn();
  // }
}

 function telchecked(id)
 {
   // alert(id);
   // var telephone=$("#phoneparent"+id).val();
   var telephone=$("#mobileparent"+id).val();

   var etape=2;
   $.ajax({
     url: '../ajax/login.php',
     type: 'POST',
     async:true,
      data: 'telephone='+telephone+'&etape='+etape,
     dataType: 'text',
     success: function (response, statut) {

       if(response==0)
       {

       }else if(response>0)
       {
         Swal.fire({
                 type: 'warning',
                 title: '<?php echo L::WarningLib ?>',
                 text: "Nous avons dejà un parent avec ce numéro de téléphone dans le système",

                 })

                deletedrow(id);

                var etape=5;
                $.ajax({
                  url: '../ajax/login.php',
                  type: 'POST',
                  async:true,
                   data: 'telephone='+telephone+'&etape='+etape,
                  dataType: 'text',
                  success: function (response, statut) {
                    var parentid=response.split("*")[11];
                    var nom=response.split("*")[0];
                    var prenom=response.split("*")[1];
                    var phone=response.split("*")[2];
                    var sexe=response.split("*")[6];



                    var concatoldparents=$("#concatoldparents").val();

                    var nb=$("#nboldparent").val();
                    var nouveau= parseInt(nb)+1;
                    // var concatparents=$("#concatparents").val();
                    // $("#concatparents").val(concatparents+nouveau+"@");
                    $("#concatoldparents").val(concatoldparents+parentid+"@");

                    //recalcule nbconcatparent
                    recalculoldparentnb();

                    var ligne="<div class=\"form-group row\" id=\"ligneEnteteold"+parentid+"\">";
                    ligne=ligne+"<span class=\"label label-md label-info\"  style=\"text-align:center;margin-left:400px;font-family: \"Trebuchet MS\", Verdana, sans-serif;font-style: italic;font-variant-ligatures: no-common-ligatures;font-size:large;\">INFORMATIONS PARENT " +nouveau+"</span>";
                    ligne=ligne+"</div>";
                    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowNameold"+parentid+"\">";
                    ligne=ligne+"<label class=\"control-label col-md-3\">Nom <span class=\"required\"> * </span></label>";
                    ligne=ligne+"<div class=\"col-md-6\">";
                    ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"nomparentold"+parentid+"\"  id=\"nomparentold"+parentid+"\" value="+nom+" readonly >";
                    ligne=ligne+"</div>";
                    ligne=ligne+"</div>";
                    //prenoms
                    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPrenameold"+parentid+"\">";
                    ligne=ligne+"<label class=\"control-label col-md-3\">Prénoms <span class=\"required\"> * </span></label>";
                    ligne=ligne+"<div class=\"col-md-6\">";
                    ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"prenomparentold"+parentid+"\"  id=\"prenomparentold"+parentid+"\" value="+prenom+" readonly>";
                    ligne=ligne+"</div>";
                    ligne=ligne+"</div>";
                    //sexe
                    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowSexeold"+parentid+"\">";
                    ligne=ligne+"<label class=\"control-label col-md-3\">Genre <span class=\"required\"> * </span></label>";
                    ligne=ligne+"<div class=\"col-md-6\">";
                    ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"sexeparentold"+parentid+"\"  id=\"sexeparentold"+parentid+"\" value="+sexe+" readonly>";
                    ligne=ligne+"</div>";
                    ligne=ligne+"</div>";

                    //telephone
                    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPhoneold"+parentid+"\">";
                    ligne=ligne+"<label class=\"control-label col-md-3\">Téléphone <span class=\"required\"> * </span></label>";
                    ligne=ligne+"<div class=\"col-md-6\">";
                    ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"phoneparentold"+parentid+"\"  id=\"phoneparentold"+parentid+"\" value="+phone+" readonly>";
                    ligne=ligne+"</div>";
                    ligne=ligne+"</div>";
                    //bouton de suppression
                    ligne=ligne+"<div class=\"pull-right\" id=\"ligneRowold"+parentid+"\">";
                    ligne=ligne+"<button type=\"button\" id=\"delete"+parentid+"\" name=\"delete"+parentid+"\"  onclick=\"deletedoldrow("+parentid+")\" class=\"btn btn-danger pull-right\"><i class=\"fa fa-trash\"></i></button>";
                    ligne=ligne+"</div></br></br>";

                    $('#dynamic_field').append(ligne);

                  }
                });

       }

     }});
 }

 function AddparentsRow()
 {
   var nb=$("#nbparent").val();
   var nouveau= parseInt(nb)+1;
   var concatparents=$("#concatparents").val();
   $("#concatparents").val(concatparents+nouveau+"@");

   //recalcule nbconcatparent
   recalculparentnb();

   var ligne="<div class=\"form-group row\" id=\"ligneEntete"+nouveau+"\">";
   // ligne=ligne+"<label class=\"control-label col-md-8\" style=\"color:#e40d22\">INFORMATIONS PARENT "+nouveau+" <span class=\"required\">  </span></label>";
   ligne=ligne+"<span class=\"label label-md label-info\"  style=\"text-align:center;margin-left:400px;font-family: \"Trebuchet MS\", Verdana, sans-serif;font-style: italic;font-variant-ligatures: no-common-ligatures;font-size:large;\">INFORMATIONS PARENT " +nouveau+"</span>";
   ligne=ligne+"</div>";
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowMobilephone"+nouveau+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Tel Mobile <span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-6\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"mobileparent"+nouveau+"\"  id=\"mobileparent"+nouveau+"\" onchange=\"telchecked("+nouveau+")\"  placeholder=\"Renseigner le téléphone mobile du parent\">";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //telephone mobile
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowName"+nouveau+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Nom <span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-6\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"nomparent"+nouveau+"\"  id=\"nomparent"+nouveau+"\" placeholder=\"Renseigner le nom du parent\">";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //prenoms
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPrename"+nouveau+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Prénoms <span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-6\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"prenomparent"+nouveau+"\"  id=\"prenomparent"+nouveau+"\" placeholder=\"Renseigner le prénom du parent\">";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //sexe
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowSexe"+nouveau+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Genre<span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-6\">";
   ligne=ligne+"<select class=\"form-control \" id=\"sexeparent"+nouveau+"\" name=\"sexeparent"+nouveau+"\" style=\"width:100%\">";
   ligne=ligne+"<option value=\"\">Selectionner un Genre</option>";
   ligne=ligne+"<option value=\"M\">Masculin</option>";
   ligne=ligne+"<option value=\"F\">Feminin</option>";
   ligne=ligne+"</select>";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //telephone
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowMetier"+nouveau+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Profession<span class=\"required\">  </span></label>";
   ligne=ligne+"<div class=\"col-md-6\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"metierparent"+nouveau+"\"  id=\"metierparent"+nouveau+"\" placeholder=\"Renseigner la profession du parent\" >";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //profession
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowMetier"+nouveau+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Email<span class=\"required\">  </span></label>";
   ligne=ligne+"<div class=\"col-md-6\">";
   ligne=ligne+"<input type=\"email\" class=\"form-control \" name=\"emailparent"+nouveau+"\"  id=\"emailrparent"+nouveau+"\" placeholder=\"Renseigner l'adresse email du parent\"  >";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //email
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowEmployeur"+nouveau+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Employeur<span class=\"required\">  </span></label>";
   ligne=ligne+"<div class=\"col-md-6\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"employeurparent"+nouveau+"\"  id=\"employeurparent"+nouveau+"\"  placeholder=\"Renseigner l'employeur du parent\">";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //employeur
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPostaleadress"+nouveau+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Adresse postale<span class=\"required\">  </span></label>";
   ligne=ligne+"<div class=\"col-md-6\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"postaleadressparent"+nouveau+"\"  id=\"postaleadressparent"+nouveau+"\"  placeholder=\"Renseigner l'adresse postale du parent\" >";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //adresse postale

   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPhonework"+nouveau+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Tel Bureau<span class=\"required\">  </span></label>";
   ligne=ligne+"<div class=\"col-md-6\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"phoneparent"+nouveau+"\"  id=\"phoneparent"+nouveau+"\"  placeholder=\"Renseigner le téléphone du bureau du parent\" >";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //telephone domicile
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPhonehome"+nouveau+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Tel domicile<span class=\"required\">  </span></label>";
   ligne=ligne+"<div class=\"col-md-6\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"phonehomeparent"+nouveau+"\"  id=\"phonehomeparent"+nouveau+"\"  placeholder=\"Renseigner le téléphone domiciledu parent\"  >";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //bouton de suppression
   ligne=ligne+"<div class=\"pull-right\" id=\"ligneRow"+nouveau+"\">";
   ligne=ligne+"<button type=\"button\" id=\"delete"+nouveau+"\" name=\"delete"+nouveau+"\"  onclick=\"deletedrow("+nouveau+")\" class=\"btn btn-danger pull-right\"><i class=\"fa fa-trash\"></i></button>";
   ligne=ligne+"</div></br></br>";

   $("#sexeparent"+nouveau).select2();

   $('#dynamic_field').append(ligne);

   $('#nomparent'+nouveau).rules( "add", {
       required: true,
       messages: {
       required: "Merci de renseigner le nom du parent"
 }
     });

  //    $('#metierparent'+nouveau).rules( "add", {
  //        required: true,
  //        messages: {
  //        required: "Merci de renseigner la profession  du parent"
  // }
  //      });
  //
  //      $('#employeurparent'+nouveau).rules( "add", {
  //          required: true,
  //          messages: {
  //          required: "Merci de renseigner l'employeur du parent"
  //   }
  //        });

     $('#sexeparent'+nouveau).rules( "add", {
         required: true,
         messages: {
         required: "Merci de renseigner le genre du parent"
  }
       });

     $('#prenomparent'+nouveau).rules( "add", {
         required: true,
         messages: {
         required: "Merci de renseigner le prénom du parent"
  }
       });

       $('#mobileparent'+nouveau).rules( "add", {
           required: true,
           digits:true,
           messages: {
           required: "Merci de renseigner le téléphone du parent",
           digits:"<?php echo L::DigitsOnly?>"
    }
         });

 }


function doublantN()
{
  $("#doublant").val(0);
}

function doublant()
{
$("#doublant").val(1);
}

 function manuel()
 {
   $("#matriculestate").val(1);
   $("#matri").val("");
   $("#matri").attr("disabled",false);
   $("#etape").val(1);
   $("#matriculerow").show();


 }

 function inscription()
 {
   //val ==1
   $("#inscript").val(1);
   $("#newStudent").val(1);
   $("#RowbtnDoublant").hide();
   $("#matriculerowReinscript").hide();

    $("#RowbtnMatricule").show();
    $("#matriculerowInscript").show();
    $("#lastschoolRow").show();

    $("#btn21").removeClass('notActive').addClass('active');
    $("#btn22").removeClass('active').addClass('notActive');

 }

 function reinscription()
 {
   //val ==2
   $("#inscript").val(2);
   $("#newStudent").val(2);
   $("#RowbtnDoublant").show();
   $("#matriculerowReinscript").show();

   $("#RowbtnMatricule").hide();
   $("#matriculerowInscript").hide();
   $("#lastschoolRow").hide();

   $("#btn22").removeClass('notActive').addClass('active');
   $("#btn21").removeClass('active').addClass('notActive');
 }

 function automatique()
 {

   $("#matriculestate").val(2);
   $("#matri").val("");
   $("#matri").attr("disabled",true);
   $("#etape").val(2);
   $("#matriculerow").hide();
 }

 $('#radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);

    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
})


 </script>
    <!-- end js include path -->
  </body>

</html>
