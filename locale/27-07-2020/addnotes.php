<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$matiere=new Matiere();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}


$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$examens=$etabs->getAllexamensOfSchool($codeEtabAssigner);


$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
  $matieres=$matiere->getAllControleMatiereOfThisSchool($codeEtabAssigner,$libellesessionencours);
}



 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
     <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>


	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title">Gestions des notes</div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><!--i class="fa fa-home"></i-->&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="">Evaluations et notes </li>
                              &nbsp;<i class="fa fa-angle-right"></i>
                              <li class="active">Consigner une note </li>
                          </ol>
                      </div>
                  </div>

					<!-- start widget -->
          <div class="state-overview">
						<div class="row">
              <?php

                    if(isset($_SESSION['user']['addattendailyok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
            <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['addattendailyok']; ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addattendailyok']);
                    }

                     ?>
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

<br/>

<div class="row">

  <div class="col-sm-12">
     <div class="card-box">
       <div class="card-head">
         <header></header>
       </div>
       <div class="card-body">
         <form method="post" id="FormAddNotes" action="addnotes.php">
             <div class="row">
               <div class="col-md-6 col-sm-6">
               <!-- text input -->
               <div class="form-group" style="margin-top:8px;">
                   <label>Classe</label>
                   <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                   <select class="form-control " id="classeEtab" name="classeEtab" style="width:100%;" onchange="searchmatiere1()">
                       <option value=""><?php echo L::Selectclasses ?></option>
                       <?php
                       $i=1;
                         foreach ($classes as $value):
                         ?>
                         <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                         <?php
                                                          $i++;
                                                          endforeach;
                                                          ?>

                   </select>
               </div>


           </div>
           <div class="col-md-6 col-sm-6">
           <!-- text input -->
           <div class="form-group" style="margin-top:8px;">
               <label><?php echo L::MatiereMenusingle ?></label>
               <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
               <select class="form-control " id="matclasse" name="matclasse" style="width:100%;"  >
                   <option value=""><?php echo L::SelectSubjects ?></option>


               </select>
           </div>


         </div>

         <div class="col-md-6 col-sm-6">
         <!-- text input -->
         <div class="form-group" style="margin-top:8px;">
             <label>Type Evaluation</label>
             <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
             <select class="form-control " id="evaltype" name="evaltype" style="width:100%;" onchange='searchevaluation()' >
                 <option value="">Selectionner le type d'évaluation</option>
                 <option value="1">Contrôle</option>
                 <!-- <option value="2">Dévoir surveillé</option> -->
                 <option value="2">Examen</option>

             </select>
         </div>


       </div>



         <div class="col-md-6 col-sm-6">
         <!-- text input -->
         <div class="form-group" style="margin-top:8px;">
           <label>Evaluation</label>
           <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
           <select class="form-control " id="libctrl" name="libctrl" style="width:100%;">
               <option value="">Selectionner une évaluation</option>

           </select>
           <input type="hidden" name="search" id="search" value="1"/>
           <input type="hidden" id="notetype" name="notetype" value="">
           <input type="hidden" name="libellesession"  id="libellesession" value="<?php echo $libellesessionencours; ?>">

         </div>


         </div>
           <div class="col-md-3 col-sm-3">

           <button type="submit" class="btn btn-success"  style="width:200px;height:35px;margin-top:35px;text-align:center;">Gerer les Notes</button>


         </div>


             </div>


         </form>
       </div>
     </div>
   </div>
</div>



  <?php
  if(isset($_POST['search'])&&strlen($_POST['search'])>0)
  {
    if(isset($_POST['notetype'])&&($_POST['notetype']==1))
    {

      if(isset($_POST['classeEtab'])&&isset($_POST['libctrl']))
      {
        $students=$student->getAllstudentofthisclassesSession($_POST['classeEtab'],$libellesessionencours);

        $classeInfos=$classe->getInfosofclassesbyId($_POST['classeEtab'],$libellesessionencours);

          $tabInfosx=explode("-",$_POST['libctrl']);

          $controleid=$tabInfosx[0];
          $matierecontroleId=$tabInfosx[1];
          $teatchercontroleid=$tabInfosx[2];
           $controleInfos=$classe->getControleInfosByIdCtrl($controleid,$_POST['classeEtab'],$codeEtabAssigner);
      }

?>
<div class="row">

  <div class="offset-md-4 col-md-4"  id="affichage1">
    <div class="card" style="">
    <div class="card-body">
      <h5 class="card-title"></h5>
      <h4 style="font-style: italic;font-weight: bold;text-align:center;font-size:font-size: large;">Notes de classe</h4>
      <p class="card-text" style="text-align:center;font-weight: bold;"><?php echo $classeInfos; ?></p>
      <p class="card-text" style="text-align:center;">(<?php echo $controleInfos?>)</p>

    </div>
  </div>
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="affichage2">
                          <div class="card card-topline-green">
                              <div class="card-head">
                                  <header>Notes par Elèves</header>
                                  <div class="tools">
                                      <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
              <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
              <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                  </div>
                              </div>
                              <div class="card-body ">

                                <form method="post" action="../controller/notes.php" id="Form">
                          <table class="table table-striped table-bordered table-hover table-checkable order-column full-width"   id="affichage3">
                              <thead>
                                  <tr>
                                      <!--th style="width:5%">
                                          #
                                      </th-->
                                      <th style="width:15%"> Matricule </th>
                                      <th style="width:45%"> Nom & Prénoms </th>
                                      <th style="width:45%"> Note </th>
                                      <th> Observation </th>

                                  </tr>
                              </thead>
                              <tbody>
                                <?php
                                $matricules="";
                                $j=0;
                                $i=1;

                                foreach ($students as $value):

                                  $matricules=$matricules.$value->idcompte_eleve."*";

                                  $nbnotes=$student->DetermineNoteNumbercontroles($value->idcompte_eleve,$_POST['classeEtab'],$matierecontroleId,$controleid,$codeEtabAssigner);
                                  $datastudentsx=$student->getNotescontroleinformations($value->idcompte_eleve,$_POST['classeEtab'],$matierecontroleId,$controleid,$codeEtabAssigner);
                                  if($nbnotes==0)
                                  {
                                    $defaultnotes=0;
                                    $defaultobserv="";
                                ?>
                                  <tr class="odd gradeX">
                                      <!--td>
                                        <?php //echo $i;?>
                                      </td-->
                                      <td> <?php echo $value->matricule_eleve;?></td>
                                      <td>
                                          <a href="#"><?php echo $value->nom_eleve." ".$value->prenom_eleve;?> </a>
                                      </td>
                                      <td>
                                          <input type="number" min=0 max=20 name="note<?php echo $value->idcompte_eleve;?>" id="note<?php echo $value->idcompte_eleve;?>" style="width:100px" onclick="erasedNote(<?php echo $value->idcompte_eleve;?>)" value="<?php echo $defaultnotes; ?>" />
                                          <p id="messageNote<?php echo $value->idcompte_eleve;?>"></p>
                                      </td>
                                      <td>

                                            <textarea name="obser<?php echo $value->idcompte_eleve;?>" id="obser<?php echo $value->idcompte_eleve;?>" rows="3" cols="50" onclick="erasedObserv(<?php echo $value->idcompte_eleve;?>)"></textarea>
                                            <p id="messageObserv<?php echo $value->idcompte_eleve;?>"></p>
                                      </td>

                                  </tr>
                                  <?php
                                }else {
                                  //nous allons recuperer la note et l'observation
                                  $array=json_encode($datastudentsx,true);
                                  $someArray = json_decode($array, true);
                                  ?>
                                  <tr class="odd gradeX">
                                      <!--td>
                                        <?php //echo $i;?>
                                      </td-->
                                      <td> <?php echo $value->matricule_eleve;?></td>
                                      <td>
                                          <a href="#"><?php echo $value->nom_eleve." ".$value->prenom_eleve;?> </a>
                                      </td>
                                      <td>
                                          <input type="number" min=0 max=20 name="note<?php echo $value->idcompte_eleve;?>" id="note<?php echo $value->idcompte_eleve;?>"  value="<?php echo $someArray[0]["valeur_notes"]; ?>" vstyle="width:100px" onclick="erasedNote(<?php echo $value->idcompte_eleve;?>)" readonly />
                                          <p id="messageNote<?php echo $value->idcompte_eleve;?>"></p>
                                      </td>
                                      <td>

                                            <textarea name="obser<?php echo $value->idcompte_eleve;?>" readonly id="obser<?php echo $value->idcompte_eleve;?>" rows="3" cols="50" onclick="erasedObserv(<?php echo $value->idcompte_eleve;?>)"><?php echo $someArray[0]["obser_notes"]; ?></textarea>
                                            <p id="messageObserv<?php echo $value->idcompte_eleve;?>"></p>
                                      </td>

                                  </tr>
                                  <?php
                                }

                                   ?>

                                  <?php
                                     $i++;
                                     $j++;
                                         endforeach;
                                       ?>



                              </tbody>
                          </table>

                          <?php
                          //echo $matricules;
                          $tabMat=explode("*",$matricules);
                          $nb=count($tabMat);



                          ?>
                          <input type="hidden" name="studentmat" id="studentmat" value="<?php echo $matricules;?>"/>
                          <?php
                          if($_POST['notetype']==1)
                          {
                            // controle
                          ?>
                          <input type="hidden" name="etape" id="etape" value="1"/>
                          <?php
                          }else if($_POST['notetype']==2)
                          {
                            ?>
                            <input type="hidden" name="etape" id="etape" value="2"/>
                            <?php
                            // examen
                          }

                           ?>

                          <input type="hidden" name="nbstudent" id="nbstudent" value="<?php echo $j;?>"/>

                          <input type="hidden" name="classeId" id="classeId" value="<?php echo $_POST['classeEtab']?>"/>
                          <input type="hidden" name="typenote" id="typenote" value="<?php echo $_POST['notetype']?>"/>
                          <input type="hidden" name="idtypenote" id="idtypenote" value="<?php echo $controleid?>"/>
                          <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner;?>"/>
                          <input type="hidden" name="matiereid" id="matiereid" value=" <?php echo $matierecontroleId;?> ">
                          <input type="hidden" name="teatcherid" id="teatcherid" value=" <?php echo  $teatchercontroleid;?> ">
                          <input type="hidden" name="sessionlibelle" id="sessionlibelle" value="<?php echo $libellesessionencours; ?>">

                          <center><button type="submit"  onclick="check()" class="btn btn-success"><i class="fa fa-check-circle"></i>Validation Notes</button></center>
                        </form>

                      </div>
                          </div>
                      </div>

</div>
<?php


    }else if(isset($_POST['notetype'])&&($_POST['notetype']==2)){

      if(isset($_POST['classeEtab'])&&isset($_POST['libctrl'])&&isset($_POST['matclasse']))
      {
          //nous devons recupérer la liste des elèves de cette classe

          // $students=$student->getAllstudentofthisclasses($_POST['classeEtab1']);

          $students=$student->getAllstudentofthisclassesSession($_POST['classeEtab'],$libellesessionencours);

          $classeInfos=$classe->getInfosofclassesbyId($_POST['classeEtab'],$libellesessionencours);



            $tabInfosx=explode("-",$_POST['matclasse']);


            $matiereExamId=$tabInfosx[0];
            $teatcherExamid=$tabInfosx[1];

          ///var_dump($students);notetype

           $examenInfos=$classe->getExamInfosByIdExam($_POST['libctrl'],$codeEtabAssigner);





      }

      ?>

      <div class="row">
        <div class="offset-md-4 col-md-4"  id="affichage1">
          <div class="card" style="">
          <div class="card-body">
            <h5 class="card-title"></h5>
            <h4 style="font-style: italic;font-weight: bold;text-align:center;font-size:font-size: large;">Notes de classe</h4>
            <p class="card-text" style="text-align:center;font-weight: bold;"><?php echo $classeInfos; ?></p>
            <p class="card-text" style="text-align:center;">(<?php echo $examenInfos?>)</p>

          </div>
        </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="affichage2">
                                <div class="card card-topline-green">
                                    <div class="card-head">
                                        <header>Notes par Elèves</header>
                                        <div class="tools">
                                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                        </div>
                                    </div>
                                    <div class="card-body ">

                                      <form method="post" action="../controller/notes.php" id="FormE">
                                <table class="table table-striped table-bordered table-hover table-checkable order-column full-width"   id="affichage3">
                                    <thead>
                                        <tr>
                                            <!--th style="width:5%">
                                                #
                                            </th-->
                                            <th style="width:15%"> Matricule </th>
                                            <th style="width:45%"> Nom & Prénoms </th>
                                            <th style="width:45%"> Note </th>
                                            <th> Observation </th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php
                                      $matricules="";
                                      $j=0;
                                      $i=1;

                                      foreach ($students as $value):

                                        $matricules=$matricules.$value->idcompte_eleve."*";

                                        $nbnotes=$student->DetermineNoteNumberexamen($value->idcompte_eleve,$_POST['classeEtab'],$_POST['libctrl'],$_POST['matclasse'],$teatcherExamid,$codeEtabAssigner);
                                        $datastudentsx=$student->getNotesexameninformations($value->idcompte_eleve,$_POST['classeEtab'],$_POST['matclasse'],$_POST['libctrl'],$codeEtabAssigner);


                                         if($nbnotes==0)
                                             {
                                               $defaultnotes=0;
                                               $defaultobserv="";

                                      ?>
                                        <tr class="odd gradeX">
                                            <!--td>
                                              <?php //echo $i;?>
                                            </td-->
                                            <td> <?php echo $value->matricule_eleve;?></td>
                                            <td>
                                                <a href="#"><?php echo $value->nom_eleve." ".$value->prenom_eleve;?> </a>
                                            </td>
                                            <td>
                                                <input type="number" min=0 max=20 name="noteE<?php echo $value->idcompte_eleve;?>" id="noteE<?php echo $value->idcompte_eleve;?>" style="width:100px" onclick="erasedNote(<?php echo $value->idcompte_eleve;?>)" />
                                                <p id="messageNoteE<?php echo $value->idcompte_eleve;?>"></p>
                                            </td>
                                            <td>

                                                  <textarea name="obserE<?php echo $value->idcompte_eleve;?>" id="obserE<?php echo $value->idcompte_eleve;?>" rows="3" cols="50" onclick="erasedObserv(<?php echo $value->idcompte_eleve;?>)"></textarea>
                                                  <p id="messageObservE<?php echo $value->idcompte_eleve;?>"></p>
                                            </td>

                                        </tr>

                                        <?php
                                      }else if($nbnotes>0)
                                      {
                                        $array=json_encode($datastudentsx,true);
                                        $someArray = json_decode($array, true);
                                        ?>
                                        <tr class="odd gradeX">
                                            <!--td>
                                              <?php //echo $i;?>
                                            </td-->
                                            <td> <?php echo $value->matricule_eleve;?></td>
                                            <td>
                                                <a href="#"><?php echo $value->nom_eleve." ".$value->prenom_eleve;?> </a>
                                            </td>
                                            <td>
                                                <input type="number" min=0 max=20 name="noteE<?php echo $value->idcompte_eleve;?>" id="noteE<?php echo $value->idcompte_eleve;?>" style="width:100px" onclick="erasedNote(<?php echo $value->idcompte_eleve;?>)" value="<?php echo $someArray[0]["valeur_notes"]; ?>" readonly />
                                                <p id="messageNoteE<?php echo $value->idcompte_eleve;?>"></p>
                                            </td>
                                            <td>

                                                  <textarea name="obserE<?php echo $value->idcompte_eleve;?>" id="obserE<?php echo $value->idcompte_eleve;?>" rows="3" cols="50" onclick="erasedObserv(<?php echo $value->idcompte_eleve;?>)" readonly><?php echo $someArray[0]["obser_notes"]; ?></textarea>
                                                  <p id="messageObservE<?php echo $value->idcompte_eleve;?>"></p>
                                            </td>

                                        </tr>
                                        <?php
                                      }
                                           $i++;
                                           $j++;
                                               endforeach;
                                             ?>



                                    </tbody>
                                </table>

                                <?php
                                //echo $matricules;
                                $tabMat=explode("*",$matricules);
                                $nb=count($tabMat);



                                ?>
                                <input type="hidden" name="studentmat" id="studentmat" value="<?php echo $matricules;?>"/>
                                <input type="hidden" name="etape" id="etape" value="2"/>

                                <input type="hidden" name="nbstudent" id="nbstudent" value="<?php echo $j;?>"/>

                                <input type="hidden" name="classeId" id="classeId" value="<?php echo $_POST['classeEtab']?>"/>
                                <input type="hidden" name="typenote" id="typenote" value="<?php echo $_POST['notetype']?>"/>
                                 <input type="hidden" name="idtypenote" id="idtypenote" value="<?php echo $_POST['libctrl']?>"/>
                                <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner;?>"/>
                                <input type="hidden" name="matiereid" id="matiereid" value=" <?php echo $matiereExamId;?> ">
                                <input type="hidden" name="teatcherid" id="teatcherid" value=" <?php echo  $teatcherExamid;?> ">
                                <input type="hidden" name="sessionlibelle" id="sessionlibelle" value="<?php echo $libellesessionencours; ?>">

                                <center><button type="submit"  onclick="check1()" class="btn btn-success"><i class="fa fa-check-circle"></i>Validation Notes</button></center>
                              </form>

                            </div>
                                </div>
                            </div>
      </div>
      <?php

    }
  }
   ?>



					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
      <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
<?php
  if(isset($_POST))
  {
    ?>
    function touspresent(idclasse)
    {

       var nb="<?php echo @$nb;?>";
       var matricules="<?php echo @$matricules?>";

       var array=matricules.split('*');

       for(i=1;i<nb;i++)
       {
         //alert(array[i]);
         $("statut"+array[i]).val(1);
         //$("P"+array[i]).prop("checked", true);
         document.getElementById("P"+array[i]).checked = true;

       }
       $("#allpresent").val(1);

    }

    function tousabsent(idclasse)
    {
      var nb="<?php echo @$nb;?>";
      var matricules="<?php echo @$matricules?>";

      var array=matricules.split('*');

      for(i=1;i<nb;i++)
      {
        $("statut"+array[i]).val(0);
        document.getElementById("A"+array[i]).checked = true;

      }
      $("#allpresent").val(0);
    }
<?php
  }
 ?>


   function present(id)
   {
    $("statut"+id).val(1);
   }

   function absent(id)
   {
    $("statut"+id).val(0);
   }

   $('#example5').DataTable( {
       "scrollX": true

   } );
   $("#classeEtab").select2();
   $("#matclasse").select2();
   //$("#matclasse1").select2();
   $("#libctrl").select2();
   $("#classeEtab1").select2();
   $("#matiere1").select2();
   $("#libctrl1").select2();
   $("#evaltype").select2();


   $('#datepre').bootstrapMaterialDatePicker
   ({
     date: true,
     time: false,
     format: 'DD/MM/YYYY',
     lang: 'fr',
     minDate : new Date(),
    cancelText: '<?php echo L::AnnulerBtn ?>',
    okText: '<?php echo L::Okay ?>',
    clearText: '<?php echo L::Eraser ?>',
    nowText: '<?php echo L::Now ?>'

   });

function searchNotesClasses()
{
  var notetype=$("#notetype").val();
  var classeEtab=$("#classeEtab").val();
  var codeEtab="<?php echo $codeEtabAssigner;  ?>";

  //nous allons rechercher la liste des designation de notes par classe

    if(notetype==""||classeEtab=="")
    {
      if(notetype=="")
      {
        Swal.fire({
        type: 'warning',
        title: '<?php echo L::WarningLib ?>',
        text: "Merci de selectionner le type de note",

      })
      }

      if(classeEtab=="")
      {
        Swal.fire({
        type: 'warning',
        title: '<?php echo L::WarningLib ?>',
        text: "Merci de selectionner une classe",

      })
      }
    }else {

        if(notetype==1)
        {
          var etape=1;
        }else if(notetype==2){
          var etape=2;
        }

      $.ajax({

        url: '../ajax/Search.php',
        type: 'POST',
        async:true,
         data: 'notetype=' + notetype+ '&etape=' + etape+'&classeEtab='+classeEtab+'&codeEtab='+codeEtab,
         dataType: 'text',
         success: function (content, statut) {

           $("#libctrl").html("");
           $("#libctrl").html(content);

         }

      });
    }



}

function searchmatiere1()
{
  var classeEtab=$("#classeEtab").val();
  var codeEtab="<?php echo $codeEtabAssigner;  ?>";
  var etape=5;

  $.ajax({

    url: '../ajax/matiere.php',
    type: 'POST',
    async:true,
     data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab,
     dataType: 'text',
     success: function (content, statut) {

       $("#matclasse").html("");
       $("#matclasse").html(content);

     }

  });

}

function searchmatiere2()
{
  var classeEtab=$("#classeEtab1").val();
  var codeEtab="<?php echo $codeEtabAssigner;  ?>";
  var etape=5;

  $.ajax({

    url: '../ajax/matiere.php',
    type: 'POST',
    async:true,
     data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab,
     dataType: 'text',
     success: function (content, statut) {

       $("#matiere1").html("");
       $("#matiere1").html(content);

     }

  });
}

function searchevaluation()
{
  var classeEtab=$("#classeEtab").val();
  var codeEtab="<?php echo $codeEtabAssigner;  ?>";
  var matiere=$("#matclasse").val();
  var evaltype=$("#evaltype").val();

  // alert(evaltype);

   if(evaltype==1)
   {
     // var etape=2;
     $("#notetype").val(1);
     var etape=5;

     $.ajax({

       url: '../ajax/controle.php',
       type: 'POST',
       async:true,
        data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab+'&matiere='+matiere,
        dataType: 'text',
        success: function (content, statut) {

          $("#libctrl").html("");
          $("#libctrl").html(content);

        }

     });
   }else if(evaltype==2)
   {
     $("#notetype").val(2);
     var etape=4;



     $.ajax({

       url: '../ajax/examen.php',
       type: 'POST',
       async:true,
        data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab+'&matiere='+matiere,
        dataType: 'text',
        success: function (content, statut) {

          $("#libctrl").html("");
          $("#libctrl").html(content);

        }

     });
   }

}

function searchcontrole()
{
  var classeEtab=$("#classeEtab").val();
  var codeEtab="<?php echo $codeEtabAssigner;  ?>";
  var matiere=$("#matclasse").val();

  //nous allons chercher la liste des controles de cette classe dont le statut est à 0
  var etape=2;

  $.ajax({

    url: '../ajax/controle.php',
    type: 'POST',
    async:true,
     data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab+'&matiere='+matiere,
     dataType: 'text',
     success: function (content, statut) {

       $("#libctrl").html("");
       $("#libctrl").html(content);

     }

  });

}

function searchexams()
{
  var classeEtab=$("#classeEtab1").val();
  var codeEtab="<?php echo $codeEtabAssigner;  ?>";
  var matiere= $("#matiere1").val();
  var etape=4;

  $.ajax({

    url: '../ajax/examen.php',
    type: 'POST',
    async:true,
     data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab+'&matiere='+matiere,
     dataType: 'text',
     success: function (content, statut) {

       $("#libctrl1").html("");
       $("#libctrl1").html(content);

     }

  });

}

/*
function searchmatiere()
{

  var classeEtab=$("#classeEtab1").val();
  var codeEtab="<?php //echo $codeEtabAssigner;  ?>";
  var etape=4;

  $.ajax({

    url: '../ajax/matiere.php',
    type: 'POST',
    async:true,
     data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab,
     dataType: 'text',
     success: function (content, statut) {

       $("#matiere1").html("");
       $("#matiere1").html(content);

     }

  });

}*/

function autorise()
{
    $("#Form").submit();
}

function autorise1()
{
    $("#FormE").submit();
}

function check()
{
  var tabidstudent=$("#studentmat").val();
  var nbligne=$("#nbstudent").val();
  var tab=$("#studentmat").val().split("*");
  var i;

  // var note=$("#note"+tab[i]).val()

  for(i=0;i<nbligne;i++)
  {
      var note=$("#note"+tab[i]).val();
      var obser=$("#obser"+tab[i]).val();
  event.preventDefault();


      if(note==""||obser=="")
      {
        if(note=="")
        {
          document.getElementById("messageNote"+tab[i]).innerHTML = "<font color=\"red\">Merci de renseigner la Note SVP !</font>";

        }

        if(note=="")
        {
          document.getElementById("messageObserv"+tab[i]).innerHTML = "<font color=\"red\">Merci de renseigner l'observation SVP !</font>";

        }
      }else {
        autorise();
      }


  }
}

function check1()
{
  var tabidstudent=$("#studentmat").val();
  var nbligne=$("#nbstudent").val();
  var tab=$("#studentmat").val().split("*");
  var i;

  // var note=$("#note"+tab[i]).val()

  for(i=0;i<nbligne;i++)
  {
      var note=$("#noteE"+tab[i]).val();
      var obser=$("#obserE"+tab[i]).val();
  event.preventDefault();


      if(note==""||obser=="")
      {
        if(note=="")
        {
          document.getElementById("messageNoteE"+tab[i]).innerHTML = "<font color=\"red\">Merci de renseigner la Note SVP !</font>";

        }

        if(note=="")
        {
          document.getElementById("messageObservE"+tab[i]).innerHTML = "<font color=\"red\">Merci de renseigner l'observation SVP !</font>";

        }
      }else {
        autorise1();
      }


  }
}

function erasedNote(id)
{
document.getElementById("messageNote"+id).innerHTML = "";
}

function erasedObserv(id)
{
document.getElementById("messageObserv"+id).innerHTML = "";
}

function erasedNoteE(id)
{
document.getElementById("messageNoteE"+id).innerHTML = "";
}

function erasedObservE(id)
{
document.getElementById("messageObservE"+id).innerHTML = "";
}

   $(document).ready(function() {

//
$("#FormAddNotes").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
 },
 highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
 },
 success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    classeEtab:"required",
    datepre:"required",
    notetype:"required",
    classeEtab:"required",
    libctrl:"required"



  },
  messages: {
    classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
    datepre:"Merci de choisir la date de présence",
    notetype:"Merci de selectionner le type de note",
    classeEtab:"<?php echo L::PleaseSelectclasseOnerequired ?>",
    libctrl:"Merci de selectionner une désignation"

  },
  submitHandler: function(form) {
    form.submit();
  }
});



$("#FormAttendance").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
  },
  highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
  },
  success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{

    // for(i=0;i<nbligne;i++)
    // {
    //   note+tabidstudent[i]:"required",
    // }


    classeEtab:"required",
    datepre:"required"



  },
  messages: {
    classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
    datepre:"Merci de choisir la date de présence"

  },
  submitHandler: function(form) {
    //form.submit();
    //classeId
//datePresence
    var etape=1;


    $.ajax({

      url: '../ajax/attendance.php',
      type: 'POST',
      async:false,
      data: 'classe=' + $("#classeId").val()+'&datepre='+$("#datePresence").val()+'&etape='+etape,
      dataType: 'text',
      success: function (content, statut)
      {
          if(content==0)
          {
            form.submit();

          }else if(content==1) {
            Swal.fire({
            type: 'warning',
            title: '<?php echo L::WarningLib ?>',
            text: "La présence de cette classe existe dejà dans le système pour cette date",

          })
        }
      }

    });
  }


});


   });

   </script>
    <!-- end js include path -->
  </body>

</html>
