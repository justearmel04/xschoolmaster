<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$student=new Student();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement
$schoolsofassign=$etabs->getEtablissementbyCodeEtab($codeEtabAssigner);

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);





if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);

  $sections=$classe->getAllSectionsOfSchoolAssigned($codeEtabAssigner,$libellesessionencours);

  $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
}

if(isset($_POST['classeid']))
{
  $classeEtab=$_POST['classeid'];
}

if(isset($_GET['classeid']))
{
  $classeEtab=$_GET['classeid'];
}



 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
  <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title">résultat scolaire</div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="classinfos.php?classe=<?php echo $_GET['classeid'] ?>&codeEtab=<?php echo $_GET['codeEtab'] ?>">Classe</a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active">résultat scolaire </li>
                          </ol>
                      </div>
                  </div>


					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['addclasseok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
  title: '<?php echo L::Felicitations ?>',
  text: "<?php echo $_SESSION['user']['addclasseok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::AddNews ?>',
  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
  }).then((result) => {
  if (result.value) {

  }else {
  document.location.href="classes.php";
  }
  })
            </script>
                  <?php
                  unset($_SESSION['user']['addclasseok']);
                }

                 ?>
                 <?php
                 if($nbsessionOn==0)
                 {
                   ?>
                   <div class="alert alert-danger alert-dismissible fade show" role="alert">

                   Vous devez definir la Sessionn scolaire

                   <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
                    </a>
                   </div>
                   <?php
                 }
                  ?>

                  <div class="row">
                      <div class="col-md-12 col-sm-12">
                          <div class="card card-box">
                              <div class="card-head">
                                  <header></header>

                              </div>

                              <div class="card-body" id="bar-parent">
                                  <form  id="FormAddClass" class="form-horizontal" action="resultat.php" method="post" >
                                      <div class="form-body">
                                        <div class="row">
                                                            <div class="col-md-6">
                                        <div class="form-group">
                                        <label for=""><b>Période<span class="required"> * </span>: </b></label>


                                        <select class="form-control " id="libsemes" name="libsemes"  >
                                            <option value="" selected >Selectionner une période</option>
                                            <?php
                                              $i=1;
                                              foreach ($typesemestre as $value):
                                              ?>
                                              <option value="<?php echo $value->id_semes?>"><?php echo utf8_encode(utf8_decode($value->libelle_semes)) ?></option>

                                              <?php
                                                   $i++;
                                                   endforeach;
                                               ?>

                                        </select>
                                        <input type="hidden" id="codeEtab" name="codeEtab" value="<?php echo $codeEtabAssigner;?>"/>
                                        <input type="hidden" name="sessionEtab" id="sessionEtab" value="<?php echo $libellesessionencours; ?>">
                                        <input type="hidden" name="classeid" id="classeid" value="<?php echo $classeEtab; ?>">
                                        <input type="hidden" name="search" id="search" value="">
                                        </div>

                                        </div>
                                        <div class="col-md-6">
                                             <div class="form-group">
                                               <!-- <label for=""><b>Prénoms <span class="required"> * </span>: </b></label> -->
                                              <button type="submit" class="btn btn-primary btn-md" style="width:200px;height:35px;margin-top:30px;text-align:center;">Afficher </button>
                                              </div>


                                         </div>

                                                      </div>









                  </div>
                                  </form>
                              </div>
                          </div>
                      </div>
                  </div>

                  <?php
                  if(isset($_POST['search']))
                  {
                    $students=$student->getAllStudentOfClassesId($classeEtab,$libellesessionencours);
                   ?>
                   <div class="row">
                            <div class="col-md-12">
                                <div class="card card-topline-aqua">
                                    <div class="card-head">
                                        <header></header>
                                        <div class="tools">
                                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                        </div>
                                    </div>
                                    <div class="card-body ">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                                <thead>
                                                    <tr>
                                                        <th>Matricule</th>
                                                        <th>Nom & prénoms</th>
                                                        <th style="text-align:center">Moyenne</th>
                                                        <th style="text-align:center">Actions</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                  <?php
                                                  foreach ($students as $value):
                                                   ?>
                                                    <tr>
                                                        <td><?php echo $value->matricule_eleve ?></td>
                                                        <td><?php echo $value->nom_eleve." ".$value->prenom_eleve; ?></td>
                                                        <td style="text-align:center"><?php echo round($etabs->getstudentratingTrimestreNew($value->idcompte_eleve,$_POST['libsemes'],$classeEtab,$codeEtabAssigner,$libellesessionencours),2); ?></td>
                                                        <td style="text-align:center">
                                                          <a href="detailscolairesult.php?compte=<?php echo $value->idcompte_eleve ?>&semester=<?php echo $_POST['libsemes']; ?>&classeid=<?php echo $classeEtab;  ?>" class="btn btn-xs btn-warning" title="détails"> <i class="fa fa-info-circle"></i> </a>
                                                          <!-- <a href="bulletininvoice.php?compte=<?php //echo $value->idcompte_eleve ?>&semester=<?php //echo $_POST['libsemes']; ?>&classeid=<?php //echo $classeEtab;  ?>" class="btn btn-xs btn-primary" title="bulletin scolaire"> <i class="fa fa-file"></i> </a> -->
                                                          <a target="_blank" href="bulletinpdf.php?compte=<?php echo $value->idcompte_eleve ?>&semester=<?php echo $_POST['libsemes']; ?>&classeid=<?php echo $classeEtab;  ?>&sessionEtab=<?php echo $libellesessionencours;  ?>" class="btn btn-xs btn-primary" title="bulletin scolaire"> <i class="fa fa-file"></i> </a>
                                                        </td>

                                                    </tr>
                                                    <?php
                                                  endforeach;
                                                     ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                  <?php
                }
                   ?>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
  <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script>

   // $("#libsemes").select2();
 jQuery(document).ready(function() {

   $("#FormAddClass").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{
        passad: {
            required: true,
            minlength: 6
        },
        confirmtad:{
            required: true,
            minlength: 6,
            equalTo:'#passad'
        },
        montantscola:{
                required: true,
                digits:true
            },
       montantscolaaff:{
          required: true,
          digits:true
                },
        fonctionad:"required",

        loginad:"required",
        emailad: {
                   required: true,
                   email: true
               },
        contactad:"required",
        datenaisad:"required",
        prenomad:"required",
        classe:"required",
        libetab:"required",
        section:"required",
        libsemes:"required"




      },
      messages: {
        confirmtad:{
            required:"<?php echo L::Confirmcheck?>",
            minlength:"<?php echo L::Confirmincheck?>",
            equalTo: "<?php echo L::ConfirmSamecheck?>"
        },
        montantscola:{
                required:"<?php echo L::PleaseScolaritiesAmountStudent?>",
                digits:"<?php echo L::DigitsOnly?>"
            },
       montantscolaaff:{
         required:"<?php echo L::PleaseScolaritiesAmountAffectedStudent?>",
         digits:"<?php echo L::DigitsOnly?>"
                },
        passad: {
            required:"<?php echo L::Passcheck?>",
            minlength:"<?php echo L::Confirmincheck?>"
        },
        loginad:"<?php echo L::Logincheck?>",
        emailad:"<?php echo L::PleaseEnterEmailAdress?>",
        contactad:"<?php echo L::PleaseEnterPhoneNumber?>",
        datenaisad:"<?php echo L::PleaseEnterPhonestudentTab?>",
        prenomad:"<?php echo L::PleaseEnterPrename?>",
        nomad:"<?php echo L::PleaseEnterName?>",
        fonctionad:"<?php echo L::PleaseEnterFonction?>",
        classe:"<?php echo L::PleaseRenseigneClasse?>",
        libsemes:"Merci de selectionner une période",
        section:"Merci de selectionner la section de cette classe"
      },
      submitHandler: function(form) {

        //nous allons compter voir si nous avons au moins une moyenne pour ce trimestre

        var codeEtab="<?php echo $codeEtabAssigner; ?>";
        var sessionEtab="<?php echo $libellesessionencours; ?>";
        var classeEtab="<?php echo $classeEtab; ?>";
        var trimestre=$("#libsemes").val();

         var etape=1;
            $.ajax({
              url: '../ajax/rating.php',
              type: 'POST',
              async:false,
              data: 'codeEtab=' +codeEtab+ '&sessionEtab=' +sessionEtab+'&classeEtab=' +classeEtab+'&etape=' + etape+'&trimestre='+trimestre,
              dataType: 'text',
              success: function (content, statut) {

                if(content>0)
                {
                  form.submit();
                }else {
                  Swal.fire({
                  type: 'warning',
                  title: '<?php echo L::WarningLib ?>',
                  text: "Il n'existe aucunes moyennes pour cette période",

                  })
                }

              }
            });

      }


   });


 });
 </script>
    <!-- end js include path -->
  </body>

</html>
