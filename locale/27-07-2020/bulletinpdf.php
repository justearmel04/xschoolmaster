<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Teatcher.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');



$student=new Student();
$studentInfos=$student->getAllInformationsOfStudentNew($_GET['compte'],$_GET['sessionEtab']);
foreach ($studentInfos as  $value):
  // $studentparentid=

$matricule_eleve=$value->matricule_eleve;
$email_eleve=$value->email_eleve;
$nom=$value->nom_eleve;
$classe_eleve=$value->id_classe;
$libelleclasse_eleve=utf8_decode(utf8_encode($value->libelle_classe));
$tel=$value->tel_compte;
$datenais=$value->datenais_compte;
$photo=$value->photo_compte;
$prenom_eleve=$value->prenom_compte;
$lieunais_eleve=$value->lieunais_eleve;
$affecter_Id_eleve=$value->affecter_Id_eleve;
$doublant_Id_eleve=$value->doublant_Id_eleve;


endforeach;

$libelleaffecter="";
$libelledoublant="";

if($affecter_Id_eleve==0)
{
  $libelleaffecter="Non";
}else {
$libelleaffecter="Oui";

}

if($doublant_Id_eleve==0)
{
  $libelledoublant="Non";

}else {
  $libelledoublant="Oui";
}


$parents=new ParentX();
$parentInfos=$parents->ParentInfostudent($_GET['compte']);
$etabs=new Etab();
$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$code = $codeEtabAssigner;

try{

      // $bdd = new PDO('mysql:host=localhost;dbname=xschool', 'root', '');
$bdd = new PDO('mysql:host=www.proximity-cm.com;dbname=proximi5_xschool', 'proximi5_xschool', 'Psa@123456');

   }

catch(PDoExeption $e)

{

   $masseErreur='Erreur PDO dans'.$e->getMessage();

    die($masseErreur);

}


$date=date_format(date_create($datenais),"d/m/Y");

$lien="";

if(strlen($photo)>0)
{
$lien="../photo/Students/".$matricule_eleve."/".$photo;
}else {
  $lien="../photo/user9.jpg";
}


if($sexe='M') {

  $sexe="Masculin";

} else {

  $sexe="Feminin";

}

      $ps4=$bdd->prepare("SELECT  logo_etab FROM etablissement where code_etab=?");

      $parametre=array($code);

      $ps4->execute($parametre);

      $donnees4=$ps4->fetch();

     $encours=1;

     $req = $bdd->prepare("SELECT * from sessions where codeEtab_sess=? and encours_sess=?");

     $parametre=array($code,$encours);

     $req->execute($parametre);

     $data=$req->fetch();

      $ps=$bdd->prepare("SELECT  libelle_etab FROM etablissement where code_etab=?");

      $parametre=array($code);

      $ps->execute($parametre);

      $donnees1=$ps->fetch();

      $etabs=new Etab();



      require('fpdf/fpdf.php');

      class PDF extends FPDF
      {
      function Footer()
      {
          // Positionnement à 1,5 cm du bas
          $this->SetY(-15);
          // Police Arial italique 8
          $this->SetFont('Arial','I',8);
          // Numéro et nombre de pages
          $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
          // $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
      }

      // Tableau simple
      function BasicTable($header, $data)
      {
          // En-tête
          foreach($header as $col)
              $this->Cell(40,7,$col,1);
          $this->Ln();
          // Données
          foreach($data as $row)
          {
              foreach($row as $col)
                  $this->Cell(40,6,$col,1);
              $this->Ln();
          }
      }
      }




      $pdf = new PDF();
      $pdf->AliasNbPages();
      $pdf->SetMargins(3,0);
      $pdf->SetFont('Times','B',  16);
      $pdf->AddPage();
      $pdf->Ln(20);

      // $pdf->SetFont('Times','B',  16);
      //
      // $pdf->AddPage();

      $pdf->Image("../logo_etab/".$code."/".$donnees4['logo_etab'],5,5,30);
      $pdf->Ln(20);
      $pdf-> SetFont('Times','B',  14);
      $pdf->Cell(176,5,"BULLETIN DE NOTES :",0,0,'C');
      $pdf->Ln(10);
      $pdf-> SetFont('Times','B', 11);
      $pdf->SetTextColor(0,0,0);
      $pdf->Cell(80,8,$nom." ".$prenom_eleve,0,0,'L');
      $pdf->Cell(90,8,"Matricule : ".$matricule_eleve,0,0,'R');
      $pdf->Ln();
      $pdf->Cell(60,8,"Classe : ".$libelleclasse_eleve,0,0,'L');
      $pdf->Cell(50,8,"Effectif : ".$libelleclasse_eleve,0,0,'R');
      $pdf->Cell(60,8,utf8_decode("Affecté(e) : ").$libelleaffecter,0,0,'R');
      $pdf->Ln();
      $pdf->Cell(72,8, utf8_decode("Née le :").$date .utf8_decode(" à ").$lieunais_eleve,0,0,'L');
      $pdf->Cell(50,8,"Redoublant : ".$libelledoublant,0,0,'C');
      $pdf->Cell(48,8,"Sexe : ".$sexe,0,0,'R');
      $pdf->SetTextColor(0,0,0);
      $pdf->Ln(12);
      $pdf-> SetFont('Times','B',  13);
      $pdf->Cell(176,5,utf8_decode(L::ScolaryyearMenu).':'.' '.$data['libelle_sess'],0,0,'C');
      $pdf->Ln(10);
      $pdf->SetLineWidth(.3);
      $pdf->SetFont('Times','B',12);
      $pdf->Image("$lien",175,50,30);
      $pdf->SetFont('Times','B',9);
      $pdf->SetX(4);
      $pdf->Cell(50,8,utf8_decode("MATIERES"),1,0,'L');
      $pdf->Cell(11,8,utf8_decode("MOY"),1,0,'C');
      $pdf->Cell(11,8,utf8_decode("COEF"),1,0,'C');
      $pdf->Cell(20,8,utf8_decode("MOYCOEF"),1,0,'C');
      $pdf->Cell(11,8,utf8_decode("RANG"),1,0,'C');
      $pdf->Cell(65,8,utf8_decode("PROFESSEURS"),1,0,'C');
      $pdf->Cell(35,8,utf8_decode("MENTIONS"),1,0,'C');
      $pdf->Ln();
      $pdf->SetFont('Times','B',8);
      $datas=$etabs->getAllsubjectofclassesbyIdclasses($_GET['classeid'],$code,$_GET['sessionEtab']);
      foreach ($datas as $value):
        $data=$etabs->getratingChildInMatiere($_GET['classeid'],$code,$_GET['sessionEtab'],$_GET['compte'],$_GET['semester'],$value->id_mat);
        $rating="";
        foreach ($data as $valuedata):
          $rating=$valuedata->rating;
          $ranking=$valuedata->raking_rating;
          $moycoef=$rating*$value->coef_mat;
          $mentionranking=$valuedata->mention_rating;
        endforeach;

        $datagles=$etabs->getratinggeneraleStudent($_GET['classeid'],$code,$_GET['sessionEtab'],$_GET['compte'],$_GET['semester']);
        foreach ($datagles as $valuegle):
          $totalcoefgle=$valuegle->totalcoef_glerating;
          $totalmoycoefgle=$valuegle->totalmoycoef_glerating;
          $rankinggle=$valuegle->ranking_glerating;
          $ratinggle=$valuegle->rating_glerating;
          $mentionratinggle=$valuegle->mention_glerating;
        endforeach;

      $pdf->SetX(4);
      $pdf->Cell(50,8,$value->libelle_mat,1,0,'L');
      $pdf->Cell(11,8,round($rating,2),1,0,'C');
      $pdf->Cell(11,8,$value->coef_mat,1,0,'C');
      $pdf->Cell(20,8,round($moycoef,2),1,0,'C');
      $pdf->Cell(11,8,$ranking,1,0,'C');
      $pdf->Cell(65,8,$etabs->getUtilisateurName($value->teatcher_mat),1,0,'L');
      $pdf->Cell(35,8,$mentionranking,1,0,'C');
      $pdf->Ln();
      endforeach;
      $pdf->SetX(4);
      $pdf->Cell(50,8,"BILAN",1,0,'L');
      $pdf->Cell(11,8,round($ratinggle,2),1,0,'C');
      $pdf->Cell(11,8,$totalcoefgle,1,0,'C');
      $pdf->Cell(20,8,round($totalmoycoefgle,2),1,0,'C');
      $pdf->Cell(11,8,$rankinggle,1,0,'C');
      $pdf->Cell(65,8,'',1,0,'L');
      $pdf->Cell(35,8,$mentionratinggle,1,0,'C');
      $pdf->Ln();


  $pdf->Output();



?>
