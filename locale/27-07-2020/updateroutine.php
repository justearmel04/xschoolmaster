<?php
session_start();
require_once('../class/User.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$etabs=new Etab();
$etab=new Etab();
$parent=new ParentX();
$user=new User();
$classe=new Classe();
$matiere=new Matiere();
$emailUti=$_SESSION['user']['email'];
$userId=$_SESSION['user']['IdCompte'];
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$parents=$parent->getAllParent();

$codeEtabLocal=$etab->getcodeEtabByLocalId($userId);
$classes=$classe->getAllClassesbyschoolCode($codeEtabLocal);

$tabroute=$etab->getRouteInfosByIdRoute($_GET['compte']);
$skuroute=explode("*",$tabroute);

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}

// $matieres=$matiere->getAllSubjectOfClassesByEtab($codeEtabLocal,$libellesessionencours);
$matieres=$matiere->getAllSubjectOfClassesByEtabClasses($codeEtabLocal,$libellesessionencours,$_GET['classeid'])

// $libellehoursroutines=$etabs->

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
	<link href="../assets2/css/material_style.css" rel="stylesheet">
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title">Modification Routine - Classe :</div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#">Emploi du temps</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active">Modifier Routine </li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addroutineok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: '<?php echo L::Felicitations ?>',
  text: "<?php echo $_SESSION['user']['addroutineok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Ajouter une nouvelle routine',
  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="index.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addroutineok']);
                    }

                     ?>

              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <div class="card card-box">
                          <div class="card-head">
                              <header></header>

                          </div>

                          <div class="card-body" id="bar-parent">
                              <form  id="FormAddRoutine" class="form-horizontal" action="../controller/routined.php" method="post" >
                                  <div class="form-body">

                                    <div class="form-group row">
                                            <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-5">
                                                <select class="form-control input-height" name="classe" id="classe" onchange="searchsubject()" style="width:100%" disabled="disabled">
                                                    <option value=""><?php echo L::Selectclasses ?></option>
                                                    <?php
                                                    $i=1;
                                                      foreach ($classes as $value):
                                                      ?>
                                                      <option <?php if($skuroute[0]==$value->id_classe){echo "selected";}?> value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                      <?php
                                                                                       $i++;
                                                                                       endforeach;
                                                                                       ?>

                                                </select>
                                        </div>
                                      </div>
                                        <div class="form-group row">
                                                <label class="control-label col-md-3"><?php echo L::MatiereMenusingle ?>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">

                                                      <select class="form-control input-height" name="matiere" id="matiere"  style="width:100%">
                                                        <option value=""><?php echo L::SelectSubjects ?></option>
                                                        <?php
                                                        $i=1;
                                                          foreach ($matieres as $value):
                                                          ?>
                                                          <option <?php if($skuroute[1]==$value->id_mat){echo "selected";}?> value="<?php echo utf8_encode(utf8_decode($value->id_mat)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_mat)); ?></option>

                                                          <?php
                                                                                           $i++;
                                                                                           endforeach;
                                                                                           ?>
                                                      </select>
                                                   </div>
                                            </div>
                                  <div class="form-group row">
                                          <label class="control-label col-md-3">Jour
                                              <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-5">
                                            <select class="form-control input-height" name="day" id="day"  style="width:100%" onchange="searchvalideHours()" disabled="disabled">
                                              <option value="">Merci de selectionner un jour</option>
                                              <option <?php if($skuroute[2]=="LUN"){echo "selected";}?> value="LUN">Lundi</option>
                                              <option <?php if($skuroute[2]=="MAR"){echo "selected";}?> value="MAR">Mardi</option>
                                              <option <?php if($skuroute[2]=="MER"){echo "selected";}?> value="MER">Mercredi</option>
                                              <option <?php if($skuroute[2]=="JEU"){echo "selected";}?> value="JEU">Jeudi</option>
                                              <option <?php if($skuroute[2]=="VEN"){echo "selected";}?> value="VEN">Vendredi</option>
                                              <option <?php if($skuroute[2]=="SAM"){echo "selected";}?> value="SAM">Samedi</option>
                                              <option <?php if($skuroute[2]=="DIM"){echo "selected";}?> value="DIM">Dimanche</option>

                                            </select>
                                          </div>
                                      </div>

                                      <div class="form-group row">
                                              <label class="control-label col-md-3">Libelle Heure
                                                  <span class="required"> * </span>
                                              </label>
                                              <div class="col-md-5">
                                                <select class="form-control input-height" name="libhours" id="libhours"  style="width:100%" onchange="displayHours()" disabled="disabled">
                                                  <option value="<?php echo $skuroute[6]; ?>"><?php echo $etabs->getLibelleHoursbylibhoursid($skuroute[6]); ?></option>


                                                </select>
                                              </div>
                                          </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3">Heure Debut
                                              <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-5">
                                              <input type="text" id="heuredeb" name ="heuredeb" value="<?php echo $skuroute[7];?>" class="floating-label mdl-textfield__input" onchange="check()" placeholder="Heure de début" disabled="disabled">
                                          </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3">Heure Fin
                                              <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-5">
                                            <input type="text" id="heurefin" name ="heurefin" value="<?php echo $skuroute[8];?>" class="floating-label mdl-textfield__input" onchange="check()" placeholder="Heure de Fin" disabled="disabled">
                                            <input type="hidden" id="etape" name ="etape" value="6" >
                                            <input type="hidden" name="classe" id="classe"  value="<?php echo $skuroute[0]; ?>">
                                            <input type="hidden" id="idroutine" name ="idroutine" value="<?php echo $_GET['compte']?>" >
                                            <input type="hidden" id="Etab" name ="Etab" value="<?php echo $codeEtabLocal;?>" >
                                            <input type="hidden" name="jourold" id="jourold" value="<?php echo $skuroute[2]; ?>">
                                            <input type="hidden" name="debhourold" id="debhourold" value="<?php echo $skuroute[3]; ?>">
                                            <input type="hidden" name="finhourold" id="finhourold" value="<?php echo $skuroute[4]; ?>">
                                            <input type="hidden" name="debhournew" id="debhournew" value="">
                                            <input type="hidden" name="finhournew" id="finhournew" value="">
                                            <input type="hidden" name="libellession" id="libellession" value="<?php echo $libellesessionencours; ?>">
                                         </div>
                                      </div>


                <div class="form-actions">
                                      <div class="row">
                                          <div class="offset-md-3 col-md-9">
                                              <button type="submit"  class="btn btn-info">Modifier</button>
                                              <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                          </div>
                                        </div>
                                     </div>
              </div>
                              </form>
                          </div>
                      </div>
                  </div>

              </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <!--script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script-->
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <!-- Material -->
	<script src="../assets2/plugins/material/material.min.js"></script>
	<script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>

 <script>

 $('#heurefin').bootstrapMaterialDatePicker
 ({
   date: false,
   shortTime: false,
   format: 'HH:mm',
   lang: 'fr',
  cancelText: '<?php echo L::AnnulerBtn ?>',
  okText: '<?php echo L::Okay ?>',
  clearText: '<?php echo L::Eraser ?>',
  nowText: '<?php echo L::Now ?>'

 });

 $('#heuredeb').bootstrapMaterialDatePicker
 ({
   date: false,
   shortTime: false,
   format: 'HH:mm',
   lang: 'fr',
  cancelText: '<?php echo L::AnnulerBtn ?>',
  okText: '<?php echo L::Okay ?>',
  clearText: '<?php echo L::Eraser ?>',
  nowText: '<?php echo L::Now ?>'

 });

 function verify()
 {

 }

 function displayHours()
 {
   var etape=7;
   var codeEtab="<?php echo $codeEtabLocal;?>";
   var session="<?php echo $libellesessionencours;?>";
   var libelleheure=$("#libhours").val();
   var nouveau="";
   $.ajax({
     url: '../ajax/hours.php',
     type: 'POST',
     async:true,
     data: 'libelleheure='+libelleheure+'&etape='+etape+'&nouveau='+nouveau+'&codeEtab='+codeEtab+'&session='+session,
     dataType: 'text',
     success: function (content, statut) {
       var heuredeb=content.split("*")[0];
       var heurefin=content.split("*")[1];

       $("#heuredeb").val(heuredeb);
       $("#heurefin").val(heurefin);

       $("#debhournew").val(heuredeb);
       $("#finhournew").val(heurefin);



     }
   });
 }

 function searchvalideHours()
 {
   var day=$("#day").val();
   var codeEtab="<?php echo $codeEtabLocal;?>";
   var session="<?php echo $libellesessionencours;?>";
   var routineid="<?php echo $_GET['compte'];?>";
   var hoursLibroutine="<?php echo $skuroute[6];?>";
   var olddays="<?php echo $skuroute[2]; ?>";
   var etape=9;

   $.ajax({
     url: '../ajax/hours.php',
     type: 'POST',
     async:false,
     data: 'day=' + $("#day").val()+'&codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&routineid='+routineid+'&hoursLibroutine='+hoursLibroutine+'&olddays='+olddays,
     dataType: 'text',
     success: function (content, statut)
     {
       $("libhours").html("");
       $("#libhours").html(content);



     }
   });
 }

 function searchsubject()
 {
   var codeEtab="<?php echo $codeEtabLocal;?>";
   var etape=2;

    $.ajax({

      url: '../ajax/matiere.php',
      type: 'POST',
      async:false,
      data: 'classe=' + $("#classe").val()+'&code='+codeEtab+'&etape='+etape,
      dataType: 'text',
      success: function (content, statut)
      {
        $("#matiere").html("");
        $("#matiere").html(content);
      }

    });
 }
 function difheure(heuredeb,heurefin)
	{
	  var hd=heuredeb.split(":");
	  var hf=heurefin.split(":");

    // var difH=hf[0]-hd[0];

	   hd[0]=eval(hd[0]);
     hd[1]=eval(hd[1]);
     //hd[2]=eval(hd[2]);
	   hf[0]=eval(hf[0]);
     hf[1]=eval(hf[1]);
     // hf[2]=eval(hf[2]);
	   //if(hf[2]<hd[2]){hf[1]=hf[1]-1;hf[2]=hf[2]+60;}
	   // if(hf[1]<hd[1]){hf[0]=hf[0]-1;hf[1]=hf[1]+60;}
	   // if(hf[0]<hd[0]){hf[0]=hf[0]+24;}
	   //return((hf[0]-hd[0]) + ":" + (hf[1]-hd[1])); // + ":" + (hf[2]-hd[2]));
	   return((hf[0]-hd[0])*60 + (hf[1]-hd[1]));
	}

 function check()
 {
   //recupération des variables
   var heuredeb=$('#heuredeb').val();
   var heurefin=$('#heurefin').val();

  if((heuredeb!="") && (heurefin!=""))
  {
    var result=difheure(heuredeb,heurefin);

      if(result<0)
      {
        Swal.fire({
        type: 'error',
        title: '<?php echo L::WarningLib ?>',
        text: "L'heure de debut ne peut être supérieure à l'heure de fin",

      })

      //$('#heuredeb').html("");
      $('#heurefin').val("");

      }
  }




 }
 jQuery(document).ready(function() {




$("#classe").select2();
$("#day").select2();
$("#libhours").select2();
$("#classeEtab").select2();
$("#matiere").select2();


   $("#FormAddRoutine").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

        classe:"required",
        matiere:"required",
        day:"required",
        heuredeb:"required",
        heurefin:"required"


      },
      messages: {
        classe:"<?php echo L::PleaseSelectclasserequired ?>",
        matiere:"<?php echo L::PleaseselectSubjects ?>",
        day:"Merci de selectionner un Jour",
        heuredeb:"Merci de renseigner l'heure de début",
        heurefin:"Merci de renseigner l'heure de Fin"
      },
      submitHandler: function(form) {
        //verifier si nous n'avons pas une routine dans la période dites


        form.submit();



      }


   });


 });
 </script>
    <!-- end js include path -->
  </body>

</html>
