<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$mat=new Matiere();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);


if(strlen($imageprofile)>0)

{

  $lienphoto="../photo/".$emailUti."/".$imageprofile;

}else {

  $lienphoto="../photo/user5.jpg";

}




$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)

{

  //recuperer la session en cours

  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
  $studentInfos=$student->getAllInformationsOfStudentOne($_GET['compte'],$libellesessionencours);
  $datascolarity=$etabs->DetermineScolarityStateOfStudent($codeEtabAssigner,$libellesessionencours,$_GET['compte']);

}

// var_dump($studentInfos);

$tabStudent=array();

foreach ($studentInfos as $personnal):
  $tabStudent[0]= $personnal->id_compte;
  $tabStudent[1]=$personnal->matricule_eleve;
  $tabStudent[2]= $personnal->nom_eleve;
  $tabStudent[3]=$personnal->prenom_eleve;
  $tabStudent[4]= $personnal->datenais_eleve;
  $tabStudent[5]=$personnal->lieunais_eleve;
  $tabStudent[6]= $personnal->sexe_eleve;
  $tabStudent[7]=$personnal->email_eleve;
  $tabStudent[8]=$personnal->email_eleve;
  $tabStudent[9]= $personnal->libelle_classe;
  $tabStudent[10]=$personnal->codeEtab_classe;
  $tabStudent[11]= $personnal->photo_compte;
  $tabStudent[12]=$personnal->tel_compte;
  $tabStudent[13]= $personnal->login_compte;
  $tabStudent[14]=$personnal->codeEtab_inscrip;
  $tabStudent[15]= $personnal->id_classe;
  $tabStudent[16]=$personnal->allergie_eleve;
  $tabStudent[17]=$personnal->condphy_eleve;
endforeach;

// $tabStudent=explode("*",$studentInfos);
// $studentparentid=$tabStudent[8];
$infosparents=$parents->ParentInfostudent($_GET['compte']);

$allabsencesLast=$etabs->getListeAttendanceLast($tabStudent[1],$tabStudent[15],$codeEtabAssigner,$libellesessionencours);

//nous allons compter le nombre d'antecedents medicaux

$medicalesAnte=$etabs->getAllMedicalesAnteOfChild($_GET['compte']);

$nbmedicalesAnte=count($medicalesAnte);

$datasallergies=$etabs->getAllMedicalesAllergiesOfChild($_GET['compte']);

$nballergies=count($datasallergies);

$datasinfantmaladies=$etabs->getAllMedicalesInfantDiseaseOfChild($_GET['compte']);

$nbinfantiles=count($datasinfantmaladies);

$datasmedicals=$etabs->getFormMedicalInfosChild($_GET['compte']);
$nbmedicales=count($datasmedicals);

$arrayM=json_encode($datasmedicals,true);
$someArrayM = json_decode($arrayM, true);

//nous allons chercher la moyenne du trimestre en cour pour cet eleve

foreach ($typesemestre as $valuesemestre):
  $semetreEncoursId=$valuesemestre->id_semes;
endforeach;

//nous allons compter le nombre de notes

@$moyenneEncours=$etabs->getstudentratingTrimestreNew($_GET['compte'],$semetreEncoursId,$tabStudent[15],$codeEtabAssigner,$libellesessionencours);

// var_dump($datasallergies);

// echo $nballergies;
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />

  <!-- icons -->

    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

  <!--bootstrap -->

   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />

  <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

  <!-- data tables -->

   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>

    <!-- Material Design Lite CSS -->

  <link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >

  <link href="../assets2/css/material_style.css" rel="stylesheet">

  <!-- morris chart -->

    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />



  <!-- Theme Styles -->

    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />

    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />

    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />

    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>



  <!-- favicon -->

    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">

                      <div class="page-title-breadcrumb">

                          <div class=" pull-left">

                              <div class="page-title">Fiche Elève</div>

                          </div>

                          <ol class="breadcrumb page-breadcrumb pull-right">

                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>

                              </li>

                              <li class="">Elèves</li>&nbsp;<i class="fa fa-angle-right"></i>

                              <li class="active">Fiche Elève</li>

                          </ol>

                      </div>

                  </div>

					<!-- start widget -->
          <?php





                if(isset($_SESSION['user']['addteaok']))
                {
// echo "dddddddd";
                  ?>

<link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
<script src="../assets/js/sweetalert2.min.js"></script>

            <script>

            Swal.fire({
title: '<?php echo L::Felicitations ?>',
text: "<?php echo $_SESSION['user']['addteaok']; ?>",
type: 'success',
showCancelButton: false,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
// confirmButtonText: 'Ajouter les diplomes',
cancelButtonText: 'OK',
}).then((result) => {
if (result.value) {
// document.location.href="adddiplomes.php?compte="+<?php //echo $_GET['compte'] ?>;
}else {
// document.location.href="index.php";
}
})
            </script>
                  <?php
                  unset($_SESSION['user']['addteaok']);
                }

                 ?>

        <!-- end widget -->

        <?php



              if(isset($_SESSION['user']['addetabexist']))

              {



                ?>

                <div class="alert alert-danger alert-dismissible fade show" role="alert">

              <?php

              echo $_SESSION['user']['addetabexist'];

              ?>

              <a href="#" class="close" data-dismiss="alert" aria-label="Close">

              <span aria-hidden="true">&times;</span>

                 </a>

              </div>







                <?php

                unset($_SESSION['user']['addetabexist']);

              }



               ?>


               <div class="row">



                 <div class="col-sm-4">

                   <div class="card ">

                                         <div class="card-body no-padding height-9">

                                             <div class="row">

                                                 <div class="profile-userpic">

                                                   <?php

                                                       if($tabStudent[11]!="")

                                                       {

                                                         $lien="../photo/Students/".$tabStudent[1]."/".$tabStudent[11];

                                                       }else {

                                                         $lien="../photo/user5.jpg";

                                                       }

                                                    ?>

                                                     <img src="<?php echo $lien;?>" class="img-responsive" alt=""> </div>

                                             </div>

                                             <div class="profile-usertitle">

                                                 <div class="profile-usertitle-name"><?php echo $tabStudent[2]." ".$tabStudent[3];?></div>

                                                 <div class="profile-usertitle-job"> <?php echo $tabStudent[1];?></div>

                                             </div>

                                             <ul class="list-group list-group-unbordered">

                                                 <li class="list-group-item">
                                                   <?php
                                                   if($moyenneEncours<5)
                                                   {
                                                     ?>
     <b>Moyenne </b> <a class=" btn btn-xs btn-danger pull-right" href="detailscolairesult.php?compte=<?php echo $_GET['compte']?>&semester=<?php echo $semetreEncoursId; ?>&classeid=<?php echo $tabStudent[15]; ?>"> <?php echo round($moyenneEncours,2); ?></a>
                                                     <?php
                                                   }else if($moyenneEncours>=5 && $moyenneEncours<10)
                                                   {
                                                     ?>
     <b>Moyenne </b> <a class=" btn btn-xs btn-warning pull-right" href="detailscolairesult.php?compte=<?php echo $_GET['compte']?>&semester=<?php echo $semetreEncoursId; ?>&classeid=<?php echo $tabStudent[15]; ?>"> <?php echo round($moyenneEncours,2); ?></a>
                                                     <?php
                                                   }else if($moyenneEncours>=10 && $moyenneEncours<14)
                                                   {
                                                     ?>
     <b>Moyenne </b> <a class=" btn btn-xs btn-primary pull-right" href="detailscolairesult.php?compte=<?php echo $_GET['compte']?>&semester=<?php echo $semetreEncoursId; ?>&classeid=<?php echo $tabStudent[15]; ?>"> <?php echo round($moyenneEncours,2); ?></a>
                                                     <?php
                                                   }else if($moyenneEncours>=14)
                                                   {
                                                     ?>
     <b>Moyenne </b> <a class=" btn btn-xs btn-success pull-right" href="detailscolairesult.php?compte=<?php echo $_GET['compte']?>&semester=<?php echo $semetreEncoursId; ?>&classeid=<?php echo $tabStudent[15]; ?>"> <?php echo round($moyenneEncours,2); ?></a>
                                                     <?php
                                                   }
                                                    ?>



                                                 </li>

                                                 <li class="list-group-item">

                                                     <b>Conduite </b> <a class="pull-right"></a>

                                                 </li>



                                             </ul>

                                             <!-- END SIDEBAR USER TITLE -->

                                             <!-- SIDEBAR BUTTONS -->

                                             <div class="profile-userbuttons">

          <a  target="_blank" class="btn btn-circle btn-warning btn-sm" href="listePersonnelEleve.php?compte=<?php echo $_GET['compte']?>&sessionEtab=<?php echo $libellesessionencours; ?>"> <i class="fa fa-print"></i>Imprimer la  fiche Eleve
                             </a>


              <!--button type="button" class="btn btn-circle red btn-sm">Message</button-->

                                             </div>

                                             <!-- END SIDEBAR BUTTONS -->

                                         </div>

                                     </div>

                 </div>

                 <div class="col-sm-8">

                     <div class="profile-content">

                       <div class="row">
                         <div class="profile-tab-box">
                           <div class="p-l-20">
                             <ul class="nav ">
                               <li class="nav-item tab-all"><a
                                 class="nav-link active show" href="#tab1" data-toggle="tab"><?php echo L::GeneralInfoTab ?></a></li>
                               <li class="nav-item tab-all p-l-20"><a class="nav-link"
                                 href="#tab2" data-toggle="tab">Paiements</a></li>
                               <li class="nav-item tab-all p-l-20"><a class="nav-link"
                                 href="#tab3" data-toggle="tab">Notes</a></li>
                                 <li class="nav-item tab-all p-l-20"><a class="nav-link"
                                   href="#tab4" data-toggle="tab">Absences</a></li>
                                   <li class="nav-item tab-all p-l-20"><a class="nav-link"
                                     href="#tab5" data-toggle="tab"><?php echo L::Parameters ?></a></li>

                             </ul>
                           </div>
                         </div>
                         <div class="white-box">

                           <div class="tab-content">
                             <div class="tab-pane active fontawesome-demo" id="tab1">
                               <div id="biography" >

                                 <div class="row">
                                   <div class="col-md-12">
                                   <span class="label label-md label-info" style="text-align:center">Informations Eleve</span>
                                   </div>

                                 </div>
                                 </br>
                                 <div class="table-responsive">

                                   <table class="table table-striped custom-table table-hover" >

                                       <thead>



                                       </thead>

                                       <tbody>

                                         <tr>

                                             <td><a href="#">Matricule :</a>

                                             </td>

                                             <td ><?php echo $tabStudent[1];?></td>



                                         </tr>

                                           <tr>

                                               <td><a href="#"><?php echo L::NamestudentTab ?> :</a>

                                               </td>

                                               <td ><?php echo $tabStudent[2]." ".$tabStudent[3];?></td>



                                           </tr>



                                           <tr>

                                               <td><a href="#"> Classe : </a>

                                               </td>

                                               <td ><?php echo $tabStudent[9];?></td>



                                           </tr>
                                           <?php
                                           if(strlen($tabStudent[7])>0)
                                           {
                                            ?>
                                           <tr>

                                               <td><a href="#"> <?php echo L::EmailstudentTab ?>: </a>

                                               </td>

                                               <td ><?php echo $tabStudent[7];?></td>



                                           </tr>
                                           <?php
                                         }
                                            ?>

                                            <?php
                                            if(strlen($tabStudent[12])>0)
                                            {
                                             ?>
                                           <tr>

                                               <td><a href="#"><?php echo L::PhonestudentTab ?> :</a>

                                               </td>

                                               <td ><?php echo $tabStudent[12];?></td>



                                           </tr>
                                           <?php
                                         }
                                            ?>
                                           <tr>

                                               <td><a href="#"> <?php echo L::SexestudentTab ?>: </a>

                                               </td>

                                               <td ><?php

                                               $sexe=$tabStudent[6];

                                               if($sexe=="M")

                                               {

                                                 echo "Masculin";

                                               }else {

                                                 echo "Féminin";

                                               }

                                               ?></td>



                                           </tr>

                                           <tr>

                                             <td><a href="#">Date de naissance :</a>

                                               </td>

                                               <td ><?php echo date_format(date_create($tabStudent[4]),"d/m/Y");?></td>



                                           </tr>



                                       </tbody>

                                   </table>

                                 </div></br>
                                 <div class="row">
                                   <div class="col-md-12">
                                   <span class="label label-md label-info" style="text-align:center">Informations Parents</span>
                                   </div>
                                 </div></br>
                                   <div class="table-responsive">
                                     <table class="table table-striped custom-table table-hover">

                                         <thead>
                                           <th>Nom & Prénoms </th>
                                           <th>Email </th>
                                           <th>Contact </th>
                                           <th>Profession</th>


                                         </thead>

                                         <tbody>
                                           <?php
                                           foreach ($infosparents as $value):
                                            ?>

                                             <tr ondblclick="redirectparent(<?php echo $value->id_compte  ?>)">

                                                 <td><?php echo $value->nom_compte." ".$value->prenom_compte; ?></td>
                                                 <td><?php echo $value->email_compte; ?></td>
                                                 <td ><?php echo $value->tel_compte; ?></td>
                                                 <td ><?php echo $value->fonction_compte; ?></td>

                                             </tr>

                                           <?php
                                         endforeach;
                                            ?>
                                         </tbody>

                                     </table>
                                   </div></br>
                                   <?php
                                   if($nbmedicales>0)
                                   {
                                     ?>
                                     <div class="row">
                                       <div class="col-md-12">
                                       <span class="label label-md label-info" style="text-align:center">Informations Médicales</span>
                                       </div>
                                     </div>
                                     </br>
                                       <div class="table-responsive">

                                         <table class="table table-striped custom-table table-hover" >

                                             <thead>



                                             </thead>

                                             <tbody>

                                               <tr>

                                                   <td><a href="#">Groupe sanguin :</a>

                                                   </td>

                                                   <td ><?php echo @$someArrayM[0]["blood_medical"];?></td>



                                               </tr>

                                                 <tr>

                                                     <td><a href="#">Clinique/Hopital :</a>

                                                     </td>

                                                     <td ><?php echo @$someArrayM[0]["hopital_medical"];?></td>



                                                 </tr>



                                                 <tr>

                                                     <td><a href="#"> Numéro matricule Enfant / clinique : </a>

                                                     </td>

                                                     <td ><?php echo @$someArrayM[0]["pinchildhopital_medical"];?></td>



                                                 </tr>
                                                 <?php
                                                 if(strlen(@$someArrayM[0]["doctor_medical"])>0)
                                                 {
                                                  ?>
                                                 <tr>

                                                     <td><a href="#"> Medecin traitant : </a>

                                                     </td>

                                                     <td ><?php echo @$someArrayM[0]["doctor_medical"];?></td>



                                                 </tr>
                                                 <?php
                                               }
                                                  ?>

                                                  <?php
                                                  if(strlen(@$someArrayM[0]["phonedoctor_medical"])>0)
                                                  {
                                                   ?>
                                                 <tr>

                                                     <td><a href="#"><?php echo L::PhonestudentTab ?> :</a>

                                                     </td>

                                                     <td ><?php echo @$someArrayM[0]["phonedoctor_medical"];?></td>



                                                 </tr>
                                                 <?php
                                               }
                                                  ?>

                                                  <tr>

                                                      <td><a href="#">En cas de maladie :</a>

                                                      </td>

                                                      <td ><?php
                                                      if(@$someArrayM[0]["localisationsick_medical"]==1)
                                                      {
                                                        ?>
       <span class="label label-md label-danger" style="text-align:center">Réconduire l’enfant à son domicile</span>
                                                        <?php
                                                      }else {
                                                        ?>
       <span class="label label-md label-danger" style="text-align:center">Conduire l’enfant à l’adresse suivante</span>
                                                        <?php
                                                      }

                                                      ?></td>



                                                  </tr>
                                                  <tr>

                                                      <td colspan="2"><a href="#">Adresse géographique :</a>

                                                      </td>

                                                  </tr>
                                                  <tr>

                                                      <td colspan="2">
                                                       <span class="label label-md label-success" style="text-align:center"><?php echo utf8_decode(@$someArrayM[0]["adresgeolocalsick_medical"]);  ?></span>

                                                      </td>

                                                  </tr>





                                             </tbody>

                                         </table>

                                       </div></br>
                                       <div class="table-responsive">
                                         <ol>
                                         <?php
                                         if(@$nbmedicalesAnte>0)
                                         {
                                          ?>

                                           <li>ANTECEDENTS MEDICAUX</li>


                                         <table class="table table-striped custom-table table-hover">

                                             <thead>
                                               <th>ANTECEDENTS MEDICAUX </th>
                                               <th>Enfant </th>
                                               <th>Père </th>
                                               <th>Mère</th>


                                             </thead>

                                             <tbody>
                                               <?php
                                               foreach (@$medicalesAnte as $value):
                                                ?>

                                                 <tr>

                                                     <td><span class="label label-sm label-danger" style="text-align:center"><?php echo utf8_encode(@$value->libelle_desease); ?></span></td>
                                                     <td><span class="label label-sm label-danger" style="text-align:center"><?php if(@$value->childstate_ante==1){echo "OUI";}else{echo "NON";} ?></span></td>
                                                     <td ><span class="label label-sm label-danger" style="text-align:center"><?php if(@$value->fatherstate_ante==1){echo "OUI";}else{echo "NON";} ?></span></td>
                                                     <td ><span class="label label-sm label-danger" style="text-align:center"><?php if(@$value->motherstate_ante==1){echo "OUI";}else{echo "NON";} ?></td>






                                                 </tr>

                                               <?php
                                             endforeach;
                                                ?>
                                             </tbody>

                                         </table>

                                         <?php
                                          }
                                          ?>
                                          <?php
                                          if(@$nballergies>0)
                                          {
                                            $array=json_encode(@$datasallergies,true);
                                            $someArray = json_decode(@$array, true);
                                            $donnees=@$someArray[0]["allergies_medical"];
                                            $donnees=substr($donnees, 0, -1);

                                            @$tabdonnees=explode(",",$donnees);
                                            @$nbtabdonnees=count($tabdonnees);
                                           ?>

                                            <li>ALLERGIES/ALLERGIES</li>

                                            <table class="table table-striped custom-table table-hover">



                                                <tbody>
                                                 <?php
                                                 for($i=0;$i<@$nbtabdonnees;$i++)
                                                 {


                                                  ?>

                                                    <tr>

                                                        <td><span class="label label-sm label-danger" style="text-align:center"><?php echo $etabs->getLibelleOfDiseaseById($tabdonnees[$i]); ?></span></td>


                                                    </tr>
                                                    <?php
                                                  }
                                                     ?>

                                                </tbody>

                                            </table>
                                            <?php
                                          }
                                             ?>
                                             <?php
                                             if(@$nbinfantiles>0)
                                             {
                                               @$array=json_encode(@$datasinfantmaladies,true);
                                               @$someArray = json_decode(@$array, true);
                                               @$donnees=@$someArray[0]["deseases_medical"];
                                               @$donnees=substr(@$donnees, 0, -1);

                                               @$tabdonnees=explode(",",@$donnees);
                                               @$nbtabdonnees=count(@$tabdonnees);
                                              ?>
                                             <li>MALADIES INFANTILES/CHILDHOOD DISEASES</li>
                                             <table class="table table-striped custom-table table-hover">



                                                 <tbody>
                                                  <?php
                                                  for($i=0;$i<@$nbtabdonnees;$i++)
                                                  {


                                                   ?>

                                                     <tr>

                                                         <td><span class="label label-sm label-danger" style="text-align:center"><?php echo $etabs->getLibelleOfDiseaseById(@$tabdonnees[$i]); ?></span></td>


                                                     </tr>
                                                     <?php
                                                   }
                                                      ?>

                                                 </tbody>

                                             </table>
                                             <?php
                                             }
                                              ?>
                                               </ol>


                                       </div></br>
                                       <div class="row">
                                         <div class="col-md-12">
                                         <span class="label label-md label-info" style="text-align:center">Personne à prévenir</span>
                                         </div>
                                       </div></br>
                                       <div class="table-responsive">

                                         <table class="table table-striped custom-table table-hover" >

                                             <thead>



                                             </thead>

                                             <tbody>
                                               <?php
                                               if(strlen(@$someArrayM[0]["guardian_medical"])>0)
                                               {
                                                ?>
                                               <tr>

                                                   <td><a href="#"><?php echo L::NamestudentTab ?> :</a>

                                                   </td>

                                                   <td ><?php echo utf8_decode(@$someArrayM[0]["guardian_medical"]);?></td>



                                               </tr>
                                               <?php
                                             }
                                                ?>

                                               <?php
                                               if(strlen(@$someArrayM[0]["telBguardian_medical"])>0)
                                               {
                                                ?>

                                                 <tr>

                                                     <td><a href="#">Tel / Bureau:</a>

                                                     </td>

                                                     <td ><?php echo @$someArrayM[0]["telBguardian_medical"];?></td>



                                                 </tr>

                                                 <?php
                                               }
                                                  ?>

                                                 <?php
                                                 if(strlen(@$someArrayM[0]["telMobguardian_medical"])>0)
                                                 {
                                                  ?>

                                                 <tr>

                                                     <td><a href="#"> Cell : </a>

                                                     </td>

                                                     <td ><?php echo @$someArrayM[0]["telMobguardian_medical"];?></td>



                                                 </tr>
                                                 <?php
                                               }
                                                  ?>
                                                 <?php
                                                 if(strlen(@$someArrayM[0]["domicileguardian_medical"])>0)
                                                 {
                                                  ?>
                                                 <tr>

                                                     <td><a href="#"> Domicile : </a>

                                                     </td>

                                                     <td ><?php echo @$someArrayM[0]["domicileguardian_medical"];?></td>



                                                 </tr>
                                                 <?php
                                               }
                                                  ?>

                                                   </tbody>

                                         </table>

                                       </div></br>

                                     <?php
                                   }else {
                                     ?>

                                         <!-- <a href="#" class="btn btn-success btn-md"><i class="fa fa-plus"></i> informations médicales</a> -->

                                     <?php
                                   }
                                    ?>



                               </div>

                             </div>

                             <div class="tab-pane" id="tab2">
                               <div class="container-fluid">

                                 <div class="row">
                                     <div class="full-width p-l-20">
                                         <div class="panel">
                                         <div class="pull-right">
                                             <a href="scolaritestudent.php?student=<?php echo $_GET['compte'] ?>&classe=<?php echo $tabStudent[15]; ?>" class="btn btn-primary btn-md"><i class="fa fa-plus"></i> Faire un versement</a>
                                         </div>
                                           <br><br>


                                           <div class="row">
                                             <div class="col-md-12">
                                             <span class="label label-md label-info" style="text-align:center">LISTE DES VERSEMENTS SCOLARITES</span>
                                             </div>

                                           </div></br></br>
                                           <?php
                                           //liste des versement concernant la scolarité

                                           $datasScolarites=$etabs->getAllScolaritesVersmentChild($codeEtabAssigner,$libellesessionencours,$_GET['compte'],$tabStudent[15]);
                                           // var_dump($datasInscriptions);
                                           $nbdatascolarites=count($datasScolarites);

                                           if($nbdatascolarites==0)
                                           {
                                             ?>
                                             <div class="alert alert-danger alert-dismissible fade show" role="alert">

                                          Aucun Paiement des frais de scolarités à ce jour

                                           <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                                           <span aria-hidden="true">&times;</span>

                                              </a>

                                           </div>
                                             <?php
                                           }else {
                                             ?>
                                             <table id="example1" style="width:100%;">

                                                 <thead>

                                                     <tr>

                                                         <th>Versements </th>

                                                         <th>Date</th>

                                                         <th>Montant Versé</th>

                                                         <th>Reste a payer</th>

                                                       </tr>


                                                 </thead>

                                                 <tbody>

                                                   <?php

                                                   $j=1;

                                                   foreach ($datasScolarites as $value):

                                                    ?>

                                                     <tr>

                                                         <td><span class="label label-sm label-info"><?php echo $value->code_versement; ?></span></td>

                                                         <td><?php echo date_format(date_create($value->date_versement),"d-m-Y")?></td>

                                                         <td><?php echo $value->montant_versement. " ".$value->devise_versement;?></td>

                                                         <td><?php

                                                         $resteapayer=$value->solde_versement;

                                                         if($resteapayer==0)

                                                         {

                                                           ?>

                                                           <span class="label label-sm label-success">Soldé</span>

                                                           <?php

                                                         }else if($resteapayer>0)

                                                         {

                                                           ?>

                                                            <span class="label label-sm label-danger"><?php echo $value->solde_versement. " ".$value->devise_versement;?></span>

                                                           <?php

                                                         }

                                                         ?></td>



                                                     </tr>

                                                   <?php

                                                   $j++;

                                                   endforeach;

                                                    ?>



                                                 </tbody>

                                             </table>
                                             <br>
                                             <?php
                                           }
                                            ?>
                                            <?php
                                            //nous allons verifier si cet etablissement dispose de cantine
                                            $stateschoolcantines=$etabs->getCantineOption($codeEtabAssigner);
                                            if($stateschoolcantines==1)
                                            {
                                              ?>
                                            </br></br>  <div class="row">
                                                <div class="col-md-12">
                                                <span class="label label-md label-info" style="text-align:center">LISTE DES VERSEMENTS CANTINES</span>
                                                </div>

                                              </div></br></br>

                                              <?php
                                              //liste des versement concernant la cantine

                                              $datasCantines=$etabs->getAllCantinesVersmentChild($codeEtabAssigner,$libellesessionencours,$_GET['compte'],$tabStudent[15]);
                                              // var_dump($datasInscriptions);
                                              $nbdatascantines=count($datasCantines);

                                              if($nbdatascantines==0)
                                              {
                                                ?>
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">

                                             Aucun Paiement des frais de cantine à ce jour

                                              <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                                              <span aria-hidden="true">&times;</span>

                                                 </a>

                                              </div>
                                                <?php
                                              }else {
                                                ?>
                                                <table id="example1" style="width:100%;">

                                                    <thead>

                                                        <tr>

                                                            <th>Versements </th>

                                                            <th>Date</th>

                                                            <th>Montant Versé</th>

                                                            <!-- <th>Reste a payer</th> -->



                                                    </thead>

                                                    <tbody>

                                                      <?php

                                                      $j=1;

                                                      foreach ($datasCantines as $value):

                                                       ?>

                                                        <tr>

                                                            <td><span class="label label-sm label-info"><?php echo $value->code_versement; ?></span></td>

                                                            <td><?php echo date_format(date_create($value->date_versement),"d-m-Y")?></td>

                                                            <td><?php echo $value->montant_versement. " ".$value->devise_versement;?></td>





                                                        </tr>

                                                      <?php

                                                      $j++;

                                                      endforeach;

                                                       ?>



                                                    </tbody>

                                                </table>
                                                <br>
                                                <?php
                                              }
                                               ?>
                                              <?php
                                            }
                                             ?>

                                           <?php
                                           //compter le nombre AES DE CET ETABLISSEMENT
                                           $dataschoolAES=$etabs->getAllParascolairesActivityOfThisSchool($codeEtabAssigner,$libellesessionencours);
                                           $nbschoolAES=count($dataschoolAES);

                                           if($nbschoolAES>0)
                                           {
                                             ?>
                                           </br></br> <div class="row">
                                              <div class="col-md-12">
                                              <span class="label label-md label-info" style="text-align:center">LISTE DES VERSEMENTS AES</span>
                                              </div>

                                            </div></br></br>
                                            <?php
                                            //liste des versement concernant les frais AES

                                            $datasAes=$etabs->getAllAesVersmentChild($codeEtabAssigner,$libellesessionencours,$_GET['compte'],$tabStudent[15]);
                                            // var_dump($datasInscriptions);
                                            $nbdatasAes=count($datasAes);

                                            if($nbdatasAes==0)
                                            {
                                              ?>
                                              <div class="alert alert-danger alert-dismissible fade show" role="alert">

                                           Aucun Paiement des frais AES à ce jour

                                            <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                                            <span aria-hidden="true">&times;</span>

                                               </a>

                                            </div>
                                              <?php
                                            }else {
                                              ?>
                                              <table id="example1" style="width:100%;">

                                                  <thead>

                                                      <tr>

                                                          <th>Versements </th>

                                                          <th>Date</th>

                                                          <th>Montant Versé</th>

                                                          <!-- <th>Reste a payer</th> -->



                                                  </thead>

                                                  <tbody>

                                                    <?php

                                                    $j=1;

                                                    foreach ($datasCantines as $value):

                                                     ?>

                                                      <tr>

                                                          <td><span class="label label-sm label-info"><?php echo $value->code_versement; ?></span></td>

                                                          <td><?php echo date_format(date_create($value->date_versement),"d-m-Y")?></td>

                                                          <td><?php echo $value->montant_versement. " ".$value->devise_versement;?></td>





                                                      </tr>

                                                    <?php

                                                    $j++;

                                                    endforeach;

                                                     ?>



                                                  </tbody>

                                              </table>
                                              <br>
                                              <?php
                                            }

                                             ?>
                                             <?php
                                           }

                                            ?>



                                         </div>
                                     </div>

                               </div>

                               </div>

                             </div>

                             <div class="tab-pane" id="tab3">

                               <div class="container-fluid">

                                 <div class="row">
                                     <div class="full-width p-l-20">
                                         <div class="panel">

                                           <br><br>
                                           <div class="row">
                                             <div class="col-md-12">
                                             <span class="label label-md label-info" style="text-align:center">LISTE DES NOTES D'EXAMENS</span>
                                             </div>

                                           </div></br></br>
                                           <?php
                                           //compter le nombre de versement concernant l'inscription de cet eleve
                                           $numberOfExamen=$student->getNumberOfExamNoteOfStudentThisSessions($codeEtabAssigner,$libellesessionencours,$_GET['compte']);

                                           if($numberOfExamen==0)
                                           {
                                             ?>
                                             <div class="alert alert-danger alert-dismissible fade show" role="alert">

                                          Aucune Note d'examen à ce jour

                                           <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                                           <span aria-hidden="true">&times;</span>

                                              </a>

                                           </div>
                                             <?php
                                           }else {
                                             ?>
                                             <table id="example1" style="width:100%;">

                                                 <thead>

                                                     <tr>

                                                         <th>Examens </th>

                                                         <th>Note</th>

                                                         <th>Observation</th>



                                                 </thead>

                                                 <tbody>

                                                   <?php

                                                   $j=1;
                                                   $examenalls=$student->getExameNotesOfStudentThisSession($codeEtabAssigner,$libellesessionencours,$_GET['compte']);
                                                   foreach ($examenalls as $value):

                                                    ?>

                                                     <tr>

                                                         <td><span class="label label-sm label-success"> <?php echo $value->libelle_exam ?></span></td>

                                                         <td><?php echo $value->valeur_notes; ?></td>

                                                         <td><?php echo $value->obser_notes; ?></td>



                                                     </tr>

                                                   <?php

                                                   $j++;

                                                   endforeach;

                                                    ?>



                                                 </tbody>

                                             </table>
                                             <?php
                                           }
                                            ?>
                                          </br></br> <div class="row">
                                             <div class="col-md-12">
                                             <span class="label label-md label-info" style="text-align:center">LISTE DES NOTES DE CONTROLES</span>
                                             </div>

                                           </div></br></br>
                                           <?php
                                           //liste des versement concernant la scolarité

                                         $numberOfControle=$student->getNumberOfControleNoteOfStudentThisSessions($codeEtabAssigner,$libellesessionencours,$_GET['compte']);

                                           if($numberOfControle==0)
                                           {
                                             ?>
                                             <div class="alert alert-danger alert-dismissible fade show" role="alert">

                                          Aucune Note de contrôle à ce jour

                                           <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                                           <span aria-hidden="true">&times;</span>

                                              </a>

                                           </div>
                                             <?php
                                           }else {
                                             ?>
                                             <table id="example1" style="width:100%;">

                                             <thead>

                                              <tr>
                                                  <th>Matière</th>
                                                  <th>Controle </th>
                                                  <th>Note</th>
                                                  <th>Observation</th>
                                                </tr>




                                             </thead>

                                             <tbody>

                                             <?php

                                             $j=1;
                                             $controlealls=$student->getControleNotesOfStudentLimitedThisSessions($codeEtabAssigner,$libellesessionencours,$_GET['compte']);
                                             foreach ($controlealls as $value):

                                             ?>

                                              <tr>
                                                 <td><span class="label label-sm label-success"> <?php echo $mat->getMatiereLibelleByIdMat($value->mat_ctrl,$value->codeEtab_ctrl); ?></span></td>
                                                  <td><span class="label label-sm label-success"> <?php echo $value->libelle_ctrl; ?></span></td>

                                                  <td><?php echo $value->valeur_notes; ?></td>

                                                  <td><?php echo $value->obser_notes; ?></td>



                                              </tr>

                                             <?php

                                             $j++;

                                             endforeach;

                                             ?>



                                             </tbody>

                                             </table>
                                             <?php
                                           }
                                            ?>
                                         </br></br>



                                         </div>
                                     </div>

                               </div>

                               </div>

                             </div>
                             <div class="tab-pane  fontawesome-demo" id="tab4">

                               <div id="biography" >

                                 <div class="row">

                                   <div class="row">
                                     <div class="col-md-12">
                                     <span class="label label-md label-info" style="text-align:center">LISTE DES ABSENCES</span>
                                     </div>
                                   </div>

                                 </div></br>
                                 <?php
                                 $nbabseces=count($allabsencesLast);
                                 if($nbabseces==0)
                                 {
                                   ?>

                                   <?php

                                 }else {
                                   ?>
                                   <!-- <div class="table-responsive"> -->

                                       <table class="table table-striped custom-table table-hover">

                                           <thead>
                                             <th>Date:</th>
                                             <th>Matière :</th>
                                             <!-- <th>Coefficient:</th> -->
                                             <th>Heure début :</th>
                                             <th>Heure fin:</th>



                                           </thead>

                                           <tbody>
                                             <?php
                                             foreach ($allabsencesLast as $value):
                                              ?>

                                               <tr>

                                                   <td><?php echo $value->date_presence; ?></td>
                                                   <td><?php echo $etabs->getMatiereLibelleByIdMat($value->matiere_presence,$value->codeEtab_presence); ?></td>
                                                   <!-- <td ><?php //echo $value->tel_compte; ?></td> -->
                                                   <td ><?php echo $value->heuredeb_heure; ?></td>
                                                   <td ><?php echo $value->heurefin_heure; ?></td>






                                               </tr>

                                             <?php
                                           endforeach;
                                              ?>
                                           </tbody>

                                       </table>
                                       <?php
                                     }
                                      ?>



                               </div>

                             </div>

                             <div class="tab-pane  fontawesome-demo" id="tab5">
                               <div id="biography" >

                                 <div class="row">

                                   <div class="row">
                                     <div class="col-md-12">
                                     <span class="label label-md label-info" style="text-align:center">Information de connexion</span>
                                     </div>
                                   </div>

                                 </div></br>
                                 <form class="form-control" action="../controller/compte.php" method="post" id="Formcnx">
                                   <div class="form-body">

                                     <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::Logincnx?>
                                              <span class="required">*  </span>
                                          </label>
                                          <div class="col-md-6">
                                              <input name="loginTea" id="loginTea" type="text" placeholder="<?php if(strlen($tabStudent[13])>0) { echo $tabStudent[13]; }else { echo "Entrer le Login";}?>"  value="<?php echo $tabStudent[13];?> " class="form-control" />

                                                </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::Passcnx?>
                                              <span class="required">*  </span>
                                          </label>
                                          <div class="col-md-6">
                                              <input name="passTea" id="passTea" type="password" placeholder="Entrer le Mot de passe " class="form-control " /> </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::ConfirmPasscnx?>
                                              <span class="required">*  </span>
                                          </label>
                                          <div class="col-md-6">
                                              <input name="confirmTea" id="confirmTea" type="password" placeholder="Confirmer le Mot de passe " class="form-control " /> </div>
                                              <input type="hidden" name="etape" id="etape" value="1"/>
                                               <input type="hidden" name="idcompte" id="idcompte" value="<?php echo $_GET['compte'];?>"/>
                                               <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner;?>"/>

                                      </div>
                                      <div class="form-actions">
                                                            <div class="row">
                                                                <div class="offset-md-3 col-md-9">
                                                                    <button class="btn btn-success" type="submit">Modifier</button>
                                                                    <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                </div>
                                                              </div>
                                                           </div>

                                   </div>

                                 </form>


                               </div>
                             </div>



                           </div>

                         </div>

                       </div>

                   </div>

                 </div>


                              </div>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>

 	<script src="../assets2/plugins/popper/popper.min.js" ></script>

     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>

 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>

     <!-- bootstrap -->

     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>

     <!-- calendar -->

     <script src="../assets2/plugins/moment/moment.min.js" ></script>

     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>

     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>

     <!-- Common js-->

 	<script src="../assets2/js/app.js" ></script>
  <script src="../assets2/js/pages/validation/form-validation.js" ></script>

     <script src="../assets2/js/layout.js" ></script>

 	<script src="../assets2/js/theme-color.js" ></script>

 	<!-- Material -->


  <script src="../assets2/plugins/material/material.min.js"></script>

  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>

  <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>







    <!-- morris chart -->

    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>

    <script src="../assets2/plugins/morris/raphael-min.js" ></script>

    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->



   <script>


function redirectparent(id)
{
  // var lien="detailsparents.php?compte="+id;
  window.location.href ="detailsparents.php?compte="+id
}


   function generatefichepdf(idcompte)

   {

     var codeEtab="<?php echo $codeEtabAssigner ?>";

      var etape=2;

       $.ajax({

         url: '../ajax/admission.php',

         type: 'POST',

         async:false,

         data: 'compte=' +idcompte+ '&etape=' + etape+'&codeEtab='+ codeEtab,

         dataType: 'text',

         success: function (content, statut) {



          window.open(content, '_blank');



         }

       });

   }



jQuery(document).ready(function() {


$("#Formcnx").validate({

  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
  },
  highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
  },
  success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
   rules:{
     loginTea:"required",
     passTea: {
         required: true,
         minlength: 6
     },
     confirmTea:{
         required: true,
         minlength: 6,
         equalTo:'#passTea'
     }
   },
   messages: {
     loginTea:"Merci de renseigner le login",
     confirmTea:{
         required:"<?php echo L::Confirmcheck?>",
         minlength:"<?php echo L::Confirmincheck?>",
         equalTo: "<?php echo L::ConfirmSamecheck?>"
     },
     passTea: {
         required:"<?php echo L::Passcheck?>",
         minlength:"<?php echo L::Confirmincheck?>"
     }
   },
   submitHandler: function(form) {
     //verifier si ce compte n'existe pas encore dans la base de données
    // form.submit();

    var login=$("#loginTea").val();
    var pass=$("#passTea").val();
    var studentid="<?php echo $_GET['compte'] ?>";
    var etape=1;

    $.ajax({
      url: '../ajax/compte.php',
      type: 'POST',
      async:false,
      data: 'login=' +login+ '&etape=' + etape+'&pass='+pass+'&studentid='+studentid,
      dataType: 'text',
      success: function (response, statut) {

        if(response==0)
        {
          form.submit();
        }else {
          if(response==1)
          {
            Swal.fire({
            type: 'warning',
            title: '<?php echo L::WarningLib ?>',
            text: "Un utilisateur existe dejà dans le système avec ces paramètres",

            })
          }else if(response==2){

            Swal.fire({
            type: 'warning',
            title: '<?php echo L::WarningLib ?>',
            text: "Un utilisateur existe dejà dans le système avec ces paramètres",

            })

          }
        }

      }
    });


   }

});



 });



   </script>

    <!-- end js include path -->

  </body>



</html>
