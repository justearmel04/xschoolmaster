<?php
session_start();
require_once('../class/User.php');
require_once('../class/Parent.php');
require_once('../class/Etablissement.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Student.php');
require_once('../class/Classe.php');

$session= new Sessionacade();
$etabs=new Etab();
$user=new User();
$classe=new Classe();
$student=new Student();
// $parentsx=new ParentX();
$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$parent=new ParentX();
$parents=$parent->getAllparentInfobyIdNew($_GET['compte']);
// $tabparents=explode("*",$parents);
$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$classes=$classe->getAllClassesbyschoolCode($codeEtabAssigner);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}

foreach ($parents as $value):
  $nomLocal=$value->nom_compte;
  $prenomLocal=$value->prenom_compte;
  $datenaisLocal=$value->datenais_compte;
  $telephoneLocal=$value->tel_compte;
  $emailLocal=$value->email_compte;
  $fonctionLocal=$value->fonction_compte;
  $loginLocal=$value->login_compte;
  $photoLocal=$value->photo_compte;
  $sexeLocal=$value->sexe_compte;
  $nomcompletLocal=$value->nom_compte." ".$prenomLocal;
  $nationaliteLocal=$value->nationalite_parent;
  $LieuHLocal=$value->lieuH_parent;
  $situationLocal=$value->situation_parent;
  $cniLocal=$value->cni_parent;
  $societeLocal=$value->societe_parent;
  $adresseproLocal=$value->adressepro_parent;
  $BurophoneLocal=$value->phoneBuro_parent;
  $lieunaisLocal=$value->lieunais_compte;
  $nbchieldLocal=$value->nbchild_parent;
  $nbchieldscoLocal=$value->nbchidsco_parent;



endforeach;

$parentlyStudent=$parent->getDifferentStudentByParentId($_GET['compte']);

// var_dump($parentlyStudent);


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

  <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title">Détails Parent</div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="#"><?php echo L::ParentsMenu?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active">Détails Parent</li>
                          </ol>
                      </div>
                  </div>
                  <?php

                        if(isset($_SESSION['user']['addparok']))
                        {

                          ?>
                          <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?php
                        //echo $_SESSION['user']['addetabok'];
                        ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                           </a>
                        </div-->
      <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
      <script src="../assets/js/sweetalert2.min.js"></script>

                    <script>
                    Swal.fire({
      title: '<?php echo L::Felicitations ?>',
      text: "<?php echo $_SESSION['user']['addparok']; ?>",
      type: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '<?php echo L::AddNews ?>',
      cancelButtonText: '<?php echo L::AnnulerBtn ?>',
    }).then((result) => {
      if (result.value) {

      }else {
        document.location.href="index.php";
      }
    })
                    </script>
                          <?php
                          unset($_SESSION['user']['addparok']);
                        }

                         ?>

                         <div class="row">

                           <div class="col-lg-4 col-md-12 col-sm-12 col-12">
                  <div class="white-box border-gray">
                    <div class="user-bg">
                      <div class="overlay-box" style="background-color:#007bff;">
                        <div class="user-content">
                          <?php

                              if(strlen($photoLocal)>0 && strlen($emailLocal)>0)

                              {

                                $lien="../photo/".$emailLocal."/".$photoLocal;

                              }else {

                                $lien="../photo/user5.jpg";

                              }

                           ?>
                          <a href="javascript:void(0)"><img alt="img" class="thumb-lg img-circle"
                              src="<?php echo $lien; ?>"></a>
                          <h4 class="text-white"><?php echo $nomcompletLocal; ?></h4>
                          <h5 class="text-white"> <i class="fa fa-envelope-o"></i> <?php echo $emailLocal; ?></h5>
                        </div>
                      </div>
                    </div>
                    <div class="user-btm-box">
                      <?php
                      if(strlen($telephoneLocal)>0)
                      {
                        ?>
                        <div class="col-md-12  m-b-0 text-center">
                          <div class="stat-item">
                            <h6> <i class="fa fa-phone"></i> <?php echo L::ContactsParentTab ?> </h6> <b><i class="ti-mobile"></i> <?php echo $telephoneLocal; ?></b>
                          </div>
                        </div>
                        <?php
                      }
                       ?>

                    </div>
                  </div>
                </div>

                           <div class="col-md-8 col-sm-12">

                               <div class="profile-content">

                                 <div class="row">
                                   <div class="profile-tab-box">
                                     <div class="p-l-20">
                                       <ul class="nav ">
                                         <li class="nav-item tab-all"><a
                                           class="nav-link active show" href="#tab1" data-toggle="tab"><?php echo L::GeneralInfoTab ?></a></li>
                                          <li class="nav-item tab-all p-l-20"><a class="nav-link"
                                           href="#tab2" data-toggle="tab">Paiements</a></li>
                                         <li class="nav-item tab-all p-l-20"><a class="nav-link"
                                           href="#tab3" data-toggle="tab">Enfants</a></li>

                                             <li class="nav-item tab-all p-l-20"><a class="nav-link"
                                               href="#tab5" data-toggle="tab"><?php echo L::Parameters ?></a></li>

                                       </ul>
                                     </div>
                                   </div>
                                   <div class="white-box">

                                     <div class="tab-content">
                                       <div class="tab-pane active fontawesome-demo" id="tab1">
                                         <div id="biography" >

                                           <div class="row">
                                             <div class="col-md-12">
                                             <span class="label label-md label-info"  style="text-align:center"> <i class="fa fa-info-circle"></i> Informations parent</span>  <span class="label label-md label-success"  style="text-align:center;color:white"><a href="#"  style="color:white" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-pencil"></i>Modifier les informations</a></span>
                                             </div>

                                           </div>
                                           </br>
                                           <div class="table-responsive">

                                             <table class="table table-striped custom-table table-hover" >

                                                 <thead>



                                                 </thead>

                                                 <tbody>

                                                   <tr>

                                                       <td><a href="#">CNI :</a>

                                                       </td>

                                                       <td ><?php echo $cniLocal;?></td>



                                                   </tr>
                                                   <tr>

                                                       <td><a href="#">Nationalité :</a>

                                                       </td>

                                                       <td ><?php echo $nationaliteLocal;?></td>



                                                   </tr>

                                                     <tr>

                                                         <td><a href="#"><?php echo L::NamestudentTab ?> :</a>

                                                         </td>

                                                         <td ><?php echo $nomcompletLocal;?></td>



                                                     </tr>


                                                     <?php
                                                     // if(strlen($tabStudent[7])>0)
                                                     // {
                                                      ?>
                                                     <tr>

                                                         <td><a href="#"> <?php echo L::EmailstudentTab ?>: </a>

                                                         </td>

                                                         <td ><?php echo $emailLocal;?></td>



                                                     </tr>
                                                     <?php
                                                   // }
                                                      ?>

                                                      <?php
                                                      // if(strlen($tabStudent[12])>0)
                                                      // {
                                                       ?>

                                                     <?php
                                                   // }
                                                      ?>
                                                     <tr>

                                                         <td><a href="#"> <?php echo L::SexestudentTab ?>: </a>

                                                         </td>

                                                         <td ><?php

                                                         $sexe=$sexeLocal;

                                                         if($sexe=="M")

                                                         {

                                                           echo "Masculin";

                                                         }else {

                                                           echo "Féminin";

                                                         }

                                                         ?></td>



                                                     </tr>
                                                     <tr>

                                                         <td><a href="#"><?php echo L::PhonestudentTab ?> :</a>

                                                         </td>

                                                         <td ><?php  echo $telephoneLocal;?></td>



                                                     </tr>
                                                     <tr>

                                                         <td><a href="#">Lieu d'habitation :</a>

                                                         </td>

                                                         <td ><?php echo $LieuHLocal;?></td>



                                                     </tr>
                                                     <tr>

                                                         <td><a href="#">Situation Matrimoniale :</a>

                                                         </td>

                                                         <td ><?php echo $situationLocal;?></td>



                                                     </tr>
                                                     <tr>

                                                         <td><a href="#"><?php echo L::Fonction ?> :</a>

                                                         </td>

                                                         <td ><?php  echo $fonctionLocal;?></td>



                                                     </tr>


                                                       <tr>

                                                           <td><a href="#">Employeur :</a>

                                                           </td>

                                                           <td ><?php  echo $societeLocal;?></td>



                                                       </tr>
                                                       <tr>

                                                           <td><a href="#"> Adresse Professionnelle:</a>

                                                           </td>

                                                           <td ><?php  echo $adresseproLocal;?></td>



                                                       </tr>
                                                       <tr>

                                                           <td><a href="#"> Téléphone Bureau:</a>

                                                           </td>

                                                           <td ><?php  echo $BurophoneLocal;?></td>



                                                       </tr>






                                                 </tbody>

                                             </table>

                                           </div></br>


                                             <div class="modal fade" id="exampleModal"  tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                               <div class="modal-dialog modal-lg" role="document">

                                                 <div class="modal-content">

                                                   <div class="modal-header">
                                                      <h4 class="modal-title" id="exampleModalLabel">Modification Parent</h4>
                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                      </button>
                                                  </div>
                                                  <div class="modal-body">

                                                    <div id="divrecherchestation">



                                                      <form class = "" id = "FormUpdateParent" action="../controller/parent.php" method="post" >
                                                        <div class="row">
                                                          <div class="col-md-12">
                                                          <div class="form-group">

                                                          <div class="compose-editor col-md-6">
                                                            <input type="file" id="photoTea" name="photoTea" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="<?php echo $lien ?>" data-allowed-file-extensions="gif png jpg jpeg pjpeg" />
                                                          </div>

                                                          </div>


                                                          </div>

                                                        </div>
                                                        <div class="row">

                                                                            <div class="col-md-6">
                                                        <div class="form-group">
                                                        <label for=""><b>CNI<span class="required">  </span> :</b></label>

                                                      <input name="cniTea" id="cniTea" type="text" placeholder="Entrer le numéro de CNI " value="<?php echo $cniLocal ?>" class="form-control " />
                                                        </div>

                                                        </div>
                                                        <div class="col-md-6">
                                                             <div class="form-group">
                                                               <label for=""><b>Nationalité <span class="required">  </span>: </b></label>
                                                                 <input name="nationTea" id="nationTea" type="text" placeholder="Entrer la nationalité " value="<?php echo $nationaliteLocal ?>" class="form-control " />
                                                              </div>


                                                         </div>
                                                         <div class="col-md-6">
                                     <div class="form-group">
                                     <label for=""><b>Nom<span class="required">  </span> :</b></label>

                                   <input type="text" name="nomTea" id="nomTea" data-required="1" placeholder="<?php echo L::EnterName ?>"  value="<?php echo $nomLocal ?>"  class="form-control " />
                                     </div>

                                     </div>
                                     <div class="col-md-6">
                                          <div class="form-group">
                                            <label for=""><b>Prénoms <span class="required">  </span>: </b></label>
                                              <input type="text" name="prenomTea" id="prenomTea" data-required="1" placeholder="<?php echo L::Enterparentprename ?>" value="<?php echo $prenomLocal ?>"  class="form-control " />
                                           </div>


                                      </div>
                                      <div class="col-md-6">
                                           <div class="form-group">
                                             <label for=""><b>Genre <span class="required">  </span>: </b></label>
                                             <select class="form-control " name="sexeTea" id="sexeTea">
                                                <option value="" <?php if($sexeLocal==""){echo "selected";}else{} ?> >Selectionner un genre</option>
                                                 <option  <?php if($sexeLocal=="M"){echo "selected";}else{} ?> value="M">Masculin</option>
                                                 <option <?php if($sexeLocal=="F"){echo "selected";}else{} ?>value="F">Feminin</option>
                                             </select>
                                            </div>


                                       </div>
                                      <div class="col-md-6">
                  <div class="form-group">
                  <label for=""><b>Date de naissance<span class="required">  </span> :</b></label>

              <input type="text" placeholder="Entrer la date de naissance" name="datenaisTea" id="datenaisTea" data-mask="99/99/9999" value="<?php echo date_format(date_create($datenaisLocal),"d/m/Y")   ?>" class="form-control ">
                  </div>

                  </div>
                  <div class="col-md-6">
                       <div class="form-group">
                         <label for=""><b>Lieu de naissance <span class="required">  </span>: </b></label>
                          <input type="text" name="lieunaisTea" id="lieunaisTea" data-required="1" placeholder="Lieu de naissance" value="<?php echo $lieunaisLocal  ?>" class="form-control" />
                        </div>


                   </div>

                   <div class="col-md-6">
        <div class="form-group">
        <label for=""><b>Email<span class="required">  </span> :</b></label>

        <input name="emailTea" id="emailTea" type="text" placeholder="Entrer le numéro de téléphone " value="<?php echo $emailLocal  ?>" class="form-control " />
        <span class="help-block" style="color:red;display:none" id="blocsms"> <i class="fa fa-warning" style="color:red;"></i> Vous devez renseigner une adresse email lorsque vous charger une image</span>
        </div>

        </div>
        <div class="col-md-6">
        <div class="form-group">
        <label for=""><b>Téléphone <span class="required">  </span>: </b></label>
        <input name="contactTea" id="contactTea" type="text" regex="/^[0-9]{3}$/i" placeholder="Entrer le numéro de téléphone " value="<?php echo $telephoneLocal  ?>" class="form-control" />
        </div>


        </div>
        <div class="col-md-6">
        <div class="form-group">
        <label for=""><b>Situation Matrimoniale<span class="required">  </span> :</b></label>

        <select class="form-control " name="situationTea" id="situationTea">

        <option <?php if($situationLocal==""){echo "selected";}else{} ?> value="">Selectionner la situation matrimoniale</option>
        <option  <?php if($situationLocal=="CELIBATAIRE"){echo "selected";}else{} ?> value="CELIBATAIRE">CELIBATAIRE</option>
        <option <?php if($situationLocal=="MARIE(E)"){echo "selected";}else{} ?> value="MARIE(E)">MARIE(E)</option>
        <option <?php if($situationLocal=="DIVORCE(E)"){echo "selected";}else{} ?> value="DIVORCE(E)">DIVORCE(E)</option>
        <option <?php if($situationLocal=="VEUF(VE)"){echo "selected";}else{} ?> value="VEUF(VE)">VEUF(VE)</option>
        </select>
        <input type="hidden" name="session" id="session" value="<?php echo $libellesessionencours; ?>">
        <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner; ?>">
        <input type="hidden" name="oldphoto" id="oldphoto" value="<?php echo $photoLocal; ?>">
        <input type="hidden" name="etape" id="etape" value="7">

        </div>

        </div>
        <div class="col-md-6">
        <div class="form-group">
        <label for=""><b>Nombre d'enfant<span class="required">  </span>: </b></label>
        <input name="nbchieldTea" id="nbchieldTea" type="text" regex="/^[0-9]{3}$/i" placeholder="Entrer le numéro de téléphone " value="<?php echo $nbchieldLocal  ?>" class="form-control" />
        </div>


        </div>
        <div class="col-md-6">
        <div class="form-group">
        <label for=""><b>Nombre d'enfant scolarisé <span class="required">  </span>: </b></label>
        <input name="nbchieldscoTea" id="nbchieldscoTea" type="text" regex="/^[0-9]{3}$/i" placeholder="Entrer le numéro de téléphone " value="<?php echo $nbchieldscoLocal  ?>" class="form-control" />
        </div>


        </div>

        <div class="col-md-6">
        <div class="form-group">
        <label for=""><b>Lieu d'habitation<span class="required">  </span> :</b></label>

        <input name="lieuHTea" id="lieuHTea" type="text" placeholder="Entrer le lieu d'habitation " value="<?php echo $LieuHLocal  ?>" class="form-control " />
        </div>

        </div>
        <div class="col-md-6">
        <div class="form-group">
        <label for=""><b>Adresse Postale<span class="required">  </span> :</b></label>

        <input type="text" name="adrespostaleTea" id="adrespostaleTea" data-required="1" placeholder="Adresse postale Parent" class="form-control" />
        </div>

        </div>
        <div class="col-md-6">
        <div class="form-group">
        <label for=""><b>Employeur <span class="required">  </span>: </b></label>
        <input type="text" name="employeurTea" id="employeurTea" data-required="1" placeholder="Employeur" value="<?php echo $societeLocal  ?>" class="form-control" />
        </div>


        </div>
        <div class="col-md-6">
        <div class="form-group">
        <label for=""><b>Fonction <span class="required">  </span>: </b></label>
        <input name="fonctionTea" id="fonctionTea" type="text" placeholder="Entrer la fonction " value="<?php echo $fonctionLocal  ?>" class="form-control " />
        </div>


        </div>
        <div class="col-md-6">
        <div class="form-group">
        <label for=""><b>Téléphone Bureau <span class="required">  </span>: </b></label>
        <input type="tel" name="telburoTe" id="telburoTea" data-required="1" placeholder="Télephone Bureau" value="<?php echo $BurophoneLocal ?>" class="form-control" />
        </div>


        </div>
        <div class="col-md-6">
        <div class="form-group">
        <label for=""><b>Adresse Professionnelle<span class="required">  </span>: </b></label>
        <input name="adresproTea" id="adresproTea" type="text"  placeholder="Entrer une adresse professionnelle " value="<?php echo $adresseproLocal ?>" class="form-control " />
        </div>


        </div>

        <div class="form-actions" style="margin-left:20px;">
                          <div class="row">
                              <div class="col-md-12">
                                 <button type="submit" class="btn btn-success" id="searchbtn" style="border-radius:5px"><i class="fa fa-pencil"></i> Modifier</button>
                                  <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                              </div>
                            </div>
                         </div>



                                                                      </div>
                                                      </form>
                                                      <br>


                                                    </div>



                                                  </div>


                                                 </div>

                                               </div>

                                             </div>




                                         </div>

                                       </div>

                                       <div class="tab-pane" id="tab2">
                                         <div class="container-fluid">
                                           <div class="full-width p-l-20">
                                             <div class="row">

                                               <div class="col-md-12">
                                               <span class="label label-md label-info" style="text-align:center">HISTORIQUE SCOLARITES</span>
                                               </div>
                                            </div><br><br>
                                            <div class="table-responsive">
                                            <table class="table table-striped custom-table table-hover">
                                              <thead>
                                                <tr>
                                                  <th>Versements </th>
                                                  <th>Date</th>
                                                  <th>Montant Versé</th>
                                                  <th>Reste a payer</th>
                                                </tr>

                                                  </thead>
                                             <tbody>
                                               <?php
                                               foreach ($parentlyStudent as $valuestudent):
                                                $datas=$student->getAllInformationsOfStudentNew($valuestudent->idcompte_eleve,$libellesessionencours);
                                                  foreach ($datas as $values):
                                                    $libelleclasse=$values->libelle_classe;
                                                    $idclasse=$values->id_classe;
                                                  endforeach;
                                                 ?>
                                                 <tr>
                                                   <td colspan="4" style="text-align:center;background-color:#1a88ff"><?php echo $valuestudent->nom_eleve." ".$valuestudent->prenom_eleve; ?></td>
                                                 </tr>
                                                 <?php
                                                   $datasScolarites=$etabs->getAllScolaritesVersmentChild($codeEtabAssigner,$libellesessionencours,$valuestudent->idcompte_eleve,$idclasse);
                                                   $nbdatascolarites=count($datasScolarites);
                                                    if($nbdatascolarites==0)
                                                    {
                                                      ?>
                                                      <tr>
                                                        <td colspan="4"><span class="label label-md label-warning" style="text-align:center">AUCUN VERSEMENT</span></td>
                                                      </tr>

                                                      <?php
                                                    }else {
                                                        foreach ($datasScolarites as $value):
                                                          ?>
                                                          <tr>

                                                              <td><span class="label label-sm label-info"><?php echo $value->code_versement; ?></span></td>

                                                              <td><?php echo date_format(date_create($value->date_versement),"d-m-Y")?></td>

                                                              <td><?php echo $value->montant_versement. " ".$value->devise_versement;?></td>

                                                              <td><?php

                                                              $resteapayer=$value->solde_versement;

                                                              if($resteapayer==0)

                                                              {

                                                                ?>

                                                                <span class="label label-sm label-success">Soldé</span>

                                                                <?php

                                                              }else if($resteapayer>0)

                                                              {

                                                                ?>

                                                                 <span class="label label-sm label-danger"><?php echo $value->solde_versement. " ".$value->devise_versement;?></span>

                                                                <?php

                                                              }

                                                              ?></td>



                                                          </tr>
                                                          <?php
                                                        endforeach;

                                                    }
                                                  ?>


                                                 <?php
                                               endforeach;
                                                ?>
                                            </tbody>

                                              </table>
                                            </div>
                                           </div>



                                         </div>

                                       </div>

                                       <div class="tab-pane" id="tab3">

                                         <div class="container-fluid">
                                           <div class="full-width p-l-20">

                                             <div class="row">

                                               <div class="col-md-12">
                                               <span class="label label-md label-info" style="text-align:center">LISTE DES ENFANTS</span>
                                               </div>
                                            </div><br><br>


                                            <div class="table-responsive">
                                            <table class="table table-striped custom-table table-hover">
                                              <thead>
                                                    <th>Matricule</th>
                                                    <th>Nom & Prénoms </th>
                                                    <th>Classe </th>
                                                  </thead>
                                             <tbody>
                                               <?php
                                               foreach ($parentlyStudent as $valuestudent):
                                                $datas=$student->getAllInformationsOfStudentNew($valuestudent->idcompte_eleve,$libellesessionencours);
                                                  foreach ($datas as $values):
                                                    $libelleclasse=$values->libelle_classe;
                                                  endforeach;
                                                 ?>
                                                 <tr>
                                                <td><?php echo $valuestudent->matricule_eleve; ?></td>
                                                  <td><?php echo $valuestudent->nom_eleve." ".$valuestudent->prenom_eleve; ?></td>
                                                  <td ><?php echo $libelleclasse ?></td>

                                                  </tr>
                                                 <?php
                                               endforeach;
                                                ?>
                                            </tbody>

                                              </table>
                                            </div>



                                           </div>


                                         </div>

                                       </div>
                                       <div class="tab-pane  fontawesome-demo" id="tab4">

                                         <div id="biography" >

                                         </div>

                                       </div>

                                       <div class="tab-pane  fontawesome-demo" id="tab5">
                                         <div id="biography" >

                                           <div class="row">

                                             <div class="row">
                                               <div class="col-md-12">
                                               <span class="label label-md label-info" style="text-align:center">Information du connexion</span>
                                               </div>
                                             </div>

                                           </div></br>
                                           <form class="form-control" action="../controller/compte.php" method="post" id="Formcnx">
                                             <div class="form-body">

                                               <div class="form-group row">
                                                    <label class="control-label col-md-3"><?php echo L::Logincnx?>
                                                        <span class="required">*  </span>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <input name="loginTea" id="loginTea" type="text" placeholder="<?php if(strlen($loginLocal)>0) { echo $loginLocal; }else { echo "Entrer le Login";}?>"  value="<?php echo $loginLocal;?> " class="form-control" />

                                                          </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="control-label col-md-3"><?php echo L::Passcnx?>
                                                        <span class="required">*  </span>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <input name="passTea" id="passTea" type="password" placeholder="Entrer le Mot de passe " class="form-control " /> </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="control-label col-md-3"><?php echo L::ConfirmPasscnx?>
                                                        <span class="required">*  </span>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <input name="confirmTea" id="confirmTea" type="password" placeholder="Confirmer le Mot de passe " class="form-control " /> </div>
                                                        <input type="hidden" name="etape" id="etape" value="3"/>
                                                         <input type="hidden" name="idcompte" id="idcompte" value="<?php echo $_GET['compte'];?>"/>
                                                         <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner;?>"/>

                                                </div>
                                                <div class="form-actions">
                                                                      <div class="row">
                                                                          <div class="offset-md-3 col-md-9">
                                                                              <button class="btn btn-success" type="submit">Modifier</button>
                                                                              <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                          </div>
                                                                        </div>
                                                                     </div>

                                             </div>

                                           </form>


                                         </div>
                                       </div>



                                     </div>

                                   </div>

                                 </div>

                             </div>

                           </div>


                                        </div>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <script src="../assets2/js/pages/material_select/getmdl-select.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>

   <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
 	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
 	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script>

 var date = new Date();
var newDate = new Date(date.setTime( date.getTime() + (0 * 86400000)));

 // $('#datenaisTea').bootstrapMaterialDatePicker
 // ({
 //   date:true,
 //   shortTime: false,
 //   time:false,
 //   maxDate:newDate,
 //   format :'DD-MM-YYYY',
 //   lang: 'fr',
 //  cancelText: '<?php echo L::AnnulerBtn ?>',
 //  okText: 'OK',
 //  clearText: '<?php echo L::Eraser ?>',
 //  nowText: '<?php echo L::Now ?>'
 //
 // });

 function modify(id)
 {
   Swal.fire({
 title: '<?php echo L::WarningLib ?>',
 text: "Voulez-vous vraiment modifier le compte Parent",
 type: 'warning',
 showCancelButton: true,
 confirmButtonColor: '#3085d6',
 cancelButtonColor: '#d33',
 confirmButtonText: '<?php echo L::ModifierBtn ?>',
 cancelButtonText: '<?php echo L::AnnulerBtn ?>',
 }).then((result) => {
 if (result.value) {
 document.location.href="updateparent.php?compte="+id;
 }else {

 }
 })
 }
 jQuery(document).ready(function() {

   $("#FormConnexion").validate({
     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
     rules:{
       loginTea:"required",
       passTea:{
           required: true,
           minlength: 6
       },
       confirmTea:{
           required: true,
           minlength: 6,
           equalTo:'#passTea'
       }
     },
     messages:{
       confirmTea:{
           required:"<?php echo L::Confirmcheck?>",
           minlength:"<?php echo L::Confirmincheck?>",
           equalTo: "<?php echo L::ConfirmSamecheck?>"
       },
       passTea: {
           required:"<?php echo L::Passcheck?>",
           minlength:"<?php echo L::Confirmincheck?>"
       },
       loginTea:"Merci de renseigner le Login"
     }
   });

   $("#FormPersonnelle").validate({
     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
       rules:{
         nomTea:"required",
         prenomTea:"required",

       },
       messages:{
         nomTea:"Merci de renseigner le Nom du parent",
         prenomTea:"Merci de renseigner le Prénom du parent"
       }

   });

   $("#FormUpdateParent").validate({
     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
     rules:{
       nomTea:"required",
       prenomTea:"required",
       sexeTea:"required",
       nbchieldTea:{
          digits:true,
           maxlength:3
       },
       nbchieldscoTea:{
          digits:true,
           maxlength:3
       }

     },
     messages:{
       nomTea:"Merci de renseigner le nom du parent",
       prenomTea:"Merci de renseigner le prénom du parent",
       sexeTea:"Merci de selectionner le genre du parent",
       nbchieldTea:{
          digits:"<?php echo L::DigitsOnly?>",
           maxlength:"Le nombre de caractère ne doit excedé trois(3)"
       },
       nbchieldscoTea:{
          digits:"<?php echo L::DigitsOnly?>",
           maxlength:"Le nombre de caractère ne doit excedé trois(3)"
       }
     },
     submitHandler: function(form) {

       var photoTea=$("#photoTea").val();
       var caracteres=$("#photoTea").val().length;
       var emailTea=$("#emailTea").val();
       if(emailTea=="")
       {
         if(caracteres>0)
         {
            $("#blocsms").css("display", "inline");
         }else {
           $("#blocsms").css("display", "none");
           form.submit();
         }
       }else {
         form.submit();
       }



     }

   });


   $("#FormAddLocalAd").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{
        passTea: {
            required: true,
            minlength: 6
        },
        confirmTea:{
            required: true,
            minlength: 6,
            equalTo:'#passTea'
        },
        fonctionTea:"required",
        cniTea:"required",

        loginTea:"required",
        emailTea: {
                   required: true,
                   email: true
               },
        contactTea:"required",
        datenaisTea:"required",
        prenomTea:"required",
        nomTea:"required",
        gradeTea:"required",
        dateEmbTea:"required",
        sexeTea:"required"



      },
      messages: {
        confirmTea:{
            required:"<?php echo L::Confirmcheck?>",
            minlength:"<?php echo L::Confirmincheck?>",
            equalTo: "<?php echo L::ConfirmSamecheck?>"
        },
        passTea: {
            required:"<?php echo L::Passcheck?>",
            minlength:"<?php echo L::Confirmincheck?>"
        },
        loginTea:"Merci de renseigner le Login",
        emailTea:"Merci de renseigner une adresse email",
        contactTea:"Merci de renseigner un contact",
        datenaisTea:"Merci de renseigner la date de naissance",
        prenomTea:"Merci de renseigner le prénom",
        nomTea:"Merci de renseigner le nom ",
        fonctionTea:"Merci de renseigner la fonction",
        gradeTea:"Merci de renseigner le grade",
        dateEmbTea:"Merci de renseigner la date d'embauche",
        sexeTea:"Merci de selectionner le sexe",
        cniTea:"Merci de renseigner le numéro CNI"
      },
      submitHandler: function(form) {
        //verifier si ce compte n'existe pas encore dans la base de données
           var etape=1;
           $.ajax({
             url: '../ajax/parent.php',
             type: 'POST',
             async:false,
             data: 'login=' + $("#loginTea").val()+ '&email=' + $("#emailTea").val() + '&cni=' + $("#cniTea").val() + '&etape=' + etape,
             dataType: 'text',
             success: function (content, statut) {




               if(content==0)
               {
                 //le compte n'existe pas dans la base on peut l'ajouter

                 form.submit();
               }else if(content==1) {
                 //Un Parent existe dejà avec cette CNI
                 Swal.fire({
                 type: 'warning',
                 title: '<?php echo L::WarningLib ?>',
                 text: 'Un Parent existe dejà avec cette CNI',

                 })

               }else if(content==2) {
                 //le compte existe dejà dans la base de données
                 Swal.fire({
                 type: 'warning',
                 title: '<?php echo L::WarningLib ?>',
                 text: 'Un Parent existe dejà avec cette adresse email',

                 })

               }else if(content==3) {
                 //le compte existe dejà dans la base de données
                 Swal.fire({
                 type: 'warning',
                 title: '<?php echo L::WarningLib ?>',
                 text: 'Cette adresse email est dejà utilisée dans le système',

                 })

               }

             }


           });
      }


   });


 });
 </script>
    <!-- end js include path -->
  </body>

</html>
