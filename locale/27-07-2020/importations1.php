<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement
$schoolsofassign=$etabs->getEtablissementbyCodeEtab($codeEtabAssigner);

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$codesEtab=$schoolsofassign;

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
//
if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}
//nous allons determine le type d'etablissement

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
 <head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<title><?php echo L::Titlepage?></title>
<meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
<meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

<!-- google font -->
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
<!-- icons -->
<link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
<!--bootstrap -->
<!--bootstrap -->
<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<!-- Material Design Lite CSS -->
<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
<link href="../assets2/css/material_style.css" rel="stylesheet">
<!-- Theme Styles -->
<link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
<link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
<link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
<link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
<link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <style>
    #radioBtn .notActive{
    color: #3276b1;
    background-color: #fff;
    }

    #radioBtn1 .notActive{
    color: #3276b1;
    background-color: #fff;
    }
    </style>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title">Importations des données</div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#">Etalissement</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active">Importations</li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addetabok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: '<?php echo L::Felicitations ?>',
  text: "<?php echo $_SESSION['user']['addetabok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::AddNews ?>',
  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="index.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addetabok']);
                    }

                     ?>

              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <div class="card card-box">
                          <div class="card-head">
                              <header></header>
                               <!--button id = "panel-button"
                             class = "mdl-button mdl-js-button mdl-button--icon pull-right"
                             data-upgraded = ",MaterialButton">
                             <i class = "material-icons">more_vert</i>
                          </button>
                          <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                             data-mdl-for = "panel-button">
                             <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                             <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                             <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                          </ul-->
                          </div>

                          <div class="card-body" id="bar-parent">
                              <form  id="FormAddEtab" class="form-horizontal" action="../controller/importation.php" method="post" enctype="multipart/form-data">
                                  <div class="form-body">


                                      <!-- <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                              <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-5">
                                            <select class="form-control" name="classeid" id="classeid" style="width:100%;" >
                                                <option value=""><?php echo L::Selectclasses ?></option>


                                            </select>
                                         </div>
                                      </div> -->


                                      <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::File?> d'importation

                                          </label>
                                          <div class="compose-editor col-md-5">
                                            <input type="file" id="fichier1" name="fichier1" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="../photo/images_files4.jpg" data-allowed-file-extensions="csv" data-errors-position="outside" data-show-errors="true" />
                                            <input type="hidden" name="etape" id="etape" value="3"/>
                                            <input type="hidden" name="pays" id="pays" value="<?php echo $etabs->getpaysidofschool($codeEtabAssigner) ?>"/>
                                            <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner."*".$libellesessionencours ?>"/>

                                        </div>
                                      </div>

                <div class="form-actions">
                                      <div class="row">
                                          <div class="offset-md-3 col-md-9">
                                              <button type="submit" class="btn btn-info" id="submitbtn">Enregistrer</button>
                                              <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                          </div>
                                        </div>
                                     </div>
              </div>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

 <script>

 $('#radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);

    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
})

$('#radioBtn1 a').on('click', function(){
   var sel = $(this).data('title');
   var tog = $(this).data('toggle');
   $('#'+tog).prop('value', sel);

   $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
   $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
})

function checktypeEtab()
{
  var typeetab=$("#typeetab").val();
  if(typeetab==5)
  {
    $("#precisionRow").show();
    $("#submitbtn").attr("disabled",true);
    $('#etape').val(3);
  }else {
$("#precisionRow").hide();
$("#submitbtn").attr("disabled",false);
$('#etape').val(1);
  }
}

 function cantineN()
 {
   $("#cantine").val(0);
 }

 function cantine()
 {
 $("#cantine").val(1);
 }

 function transportcar()
 {
$("#transportcar").val(1);
 }

 function transportcarN()
 {
$("#transportcar").val(0);
 }

 function compter()
 {
   var typeetab=$("#typeetab").val();
   var precision=$("#precision").val();
   if($("#precision").val().length>0)
   {
     $("#submitbtn").attr("disabled",false);
   }else {
     $("#submitbtn").attr("disabled",true);
   }

//    if(typeetab==5)
//    {
// //$("#submitbtn").attr("disabled",true);
// document.getElementById("submitbtn").disabled = true;
//    }else {
//  // $("#submitbtn").attr("disabled",false);
//  document.getElementById("submitbtn").disabled = false;
//    }
 }

 // $("#pays").select2();
 $("#classeid").select2();
 // $("#codeEtab").select2({
 //   tags: true,
 // tokenSeparators: [',', ' ']
 // });

 function selectEtab()
 {
   var pays=$("#pays").val();
   var etape=3;
   $.ajax({
     url: '../ajax/school.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&pays=' + $("#pays").val(),
     dataType: 'text',
     success: function (content, statut) {

       $("#codeEtab").html("");
       $("#codeEtab").html(content);

     }
   });
 }

 function selectclasseEtab()
 {
    var pays=$("#pays").val();
    var codeEtab=$("#codeEtab").val();
    var etape=14;
    $.ajax({
      url: '../ajax/classe.php',
      type: 'POST',
      async:false,
      data: 'etape=' + etape+ '&pays=' + $("#pays").val()+'&codeEtab='+codeEtab,
      dataType: 'text',
      success: function (content, statut) {

      $("#classeid").html("");
      $("#classeid").html(content);


      }
    });

 }


 jQuery(document).ready(function() {

   selectclasseEtab();

   $("#precisionRow").hide();

   $("#FormAddEtab").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

        codeEtab:"required",
        classeid:"required",
        pays:"required",
        fichier1:"required"

      },
      messages: {
        codeEtab:"Merci de selectionner un etablissement",
        classeid:"Merci de selectionner une classe",
        pays:"Merci de selectionner un pays",
        fichier1:"Merci de selectionner un fichier"
       },

       submitHandler: function(form) {

         form.submit();

       }


   });


 });
 </script>
    <!-- end js include path -->
  </body>

</html>
