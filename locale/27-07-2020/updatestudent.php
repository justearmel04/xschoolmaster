<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

if(isset($_GET['codeEtab']))
{
  $compteEtab=@$_GET['codeEtab'];
}

$nbsessionOn=$session->getNumberSessionEncoursOn($compteEtab);
$etablissementType=$etabs->DetermineTypeEtab($compteEtab);


if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($compteEtab);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
}


$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);

$studentInfos=$student->getAllInformationsOfStudent($_GET['compte'],$libellesessionencours);
$tabStudent=explode("*",$studentInfos);
$studentparentid=$tabStudent[8];

// $parentInfos=$parents->getParentInfosbyId($studentparentid);
// $tabParent=explode("*",$parentInfos);


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Modification Elève</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="">Gestions Elèves</li>&nbsp;<i class="fa fa-angle-right"></i>
                                <li class="active">Informations Elèves</li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->
					      </div>
						</div>
            <?php

                  if(isset($_SESSION['user']['updateroutineok']))
                  {

                    ?>
                    <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                  <?php
                  //echo $_SESSION['user']['addetabok'];
                  ?>
                  <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                     </a>
                  </div-->
          <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
          <script src="../assets/js/sweetalert2.min.js"></script>

              <script>
              Swal.fire({
              type: 'success',
              title: 'Félicitations',
              text: "<?php echo $_SESSION['user']['updateroutineok']; ?>",

            })
              </script>
                    <?php
                    unset($_SESSION['user']['updateroutineok']);
                  }

                   ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


          <div class="row">
            <?php
            /*
             ?>
            <div class="col-sm-4">
              <div class="card ">
                                    <div class="card-body no-padding height-9">
                                        <div class="row">
                                            <div class="profile-userpic">
                                              <?php
                                                  if($tabStudent[11]!="")
                                                  {
                                                    $lien="../photo/Students/".$tabStudent[1]."/".$tabStudent[11];
                                                  }else {
                                                    $lien="../photo/user5.jpg";
                                                  }
                                               ?>
                                                <img src="<?php echo $lien;?>" class="img-responsive" alt=""> </div>
                                        </div>
                                        <div class="profile-usertitle">
                                            <div class="profile-usertitle-name"><?php echo $tabStudent[2]." ".$tabStudent[3];?></div>
                                            <div class="profile-usertitle-job"> <?php echo $tabStudent[1];?></div>
                                        </div>
                                        <ul class="list-group list-group-unbordered">
                                            <li class="list-group-item">
                                                <b>Moyenne Trimestre</b> <a class="pull-right"></a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>Conduite Trimestre</b> <a class="pull-right"></a>
                                            </li>

                                        </ul>
                                        <!-- END SIDEBAR USER TITLE -->
                                        <!-- SIDEBAR BUTTONS -->
                                        <div class="profile-userbuttons">
                                            <button type="button" class="btn btn-circle btn-warning btn-sm"><i class="fa fa-print"></i> Fiche </button>

                                        </div>
                                        <!-- END SIDEBAR BUTTONS -->
                                    </div>
                                </div>
            </div>
            <?php
*/
             ?>
            <div class="col-sm-12">
            								<div class="card-box">
            									<div class="card-head">
            										<header>INFORMATIONS</header>
            									</div>
            									<div class="card-body ">
            						            <div class = "mdl-tabs mdl-js-tabs">
            						               <div class = "mdl-tabs__tab-bar tab-left-side">
            						                  <a href = "#tab4-panel" class = "mdl-tabs__tab is-active ">Basic</a>
            						                  <!--a href = "#tab5-panel" class = "mdl-tabs__tab ">Parent</a>
            						                  <a href = "#tab6-panel" class = "mdl-tabs__tab ">Examens</a>
                                          <a href = "#tab7-panel" class = "mdl-tabs__tab " >Contrôles</a>
                                          <a href = "#tab8-panel" class = "mdl-tabs__tab " >Statistiques</a>
                                          <a href = "#tab9-panel" class = "mdl-tabs__tab " >Paiements</a-->
            						               </div>
            						               <div class = "mdl-tabs__panel is-active p-t-20" id = "tab4-panel">
                                         <div class="card-body "style="margin-left:-22px;" id="bar-parent">
                                           <form  id="FormAddStudent" class="form-horizontal" action="../controller/admission.php" method="post" enctype="multipart/form-data">
                                               <div class="form-body">

                                                 <div class="form-group row">
                                                         <label class="control-label col-md-3">Matricule
                                                             <span class="required"> * </span>
                                                         </label>
                                                         <div class="col-md-5">
                                                             <input type="text" name="matri" id="matri"  value=" <?php echo $tabStudent[1];?>" data-required="1" placeholder="Entrer le Matricule" class="form-control input-height" disabled />
                                                             <input type="hidden" name="etape" id="etape" value="2"/>
                                                             <input type="hidden" name="newStudent" id="newStudent" value="1"/>
                                                             <input type="hidden" name="codeEtab" id="codeEtab"  value="<?php echo $tabStudent[14];?>"/>
                                                             <input type="hidden" name="idcompte" value="<?php echo $tabStudent[0]; ?>">
                                                             <input type="hidden" name="oldfile" value="<?php echo $tabStudent[11]; ?>">
                                                             <input type="hidden" name="matricileStudent" value="<?php echo $tabStudent[1] ?>">


                                                           </div>
                                                     </div>
                                               <div class="form-group row">
                                                       <label class="control-label col-md-3"><?php echo L::Name?>
                                                           <span class="required"> * </span>
                                                       </label>
                                                       <div class="col-md-5">
                                                           <input type="text"  name="nomad" id="nomad" value="<?php echo $tabStudent[2];?>" data-required="1" placeholder="<?php echo L::EnterName ?>" class="form-control input-height" /> </div>
                                                   </div>
                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3"><?php echo L::PreName?>
                                                           <span class="required"> * </span>
                                                       </label>
                                                       <div class="col-md-5">
                                                           <input type="text" name="prenomad" value="<?php echo $tabStudent[3];?>" id="prenomad" data-required="1" placeholder="<?php echo L::Enterparentprename ?>" class="form-control input-height" /> </div>
                                                   </div>

                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                                           <span class="required">*  </span>
                                                       </label>
                                                       <div class="col-md-5">
                                                         <select class="form-control input-height" id="classeEtab" name="classeEtab" style="width:100%" disabled>
                                                             <option selected value=""><?php echo $tabStudent[9];?></option>

                                                         </select>
                                                         </div>
                                                   </div>

                                                   <div class="form-group row">
                                                     <label class="control-label col-md-3"><?php echo L::BirthstudentTab?>
                                                         <span class="required"> * </span>
                                                     </label>
                                                         <div class="col-md-5">
                                                             <input type="text" placeholder="Entrer la date de naissance" value="<?php echo date_format(date_create($tabStudent[4]),"d/m/Y");?>" name="datenaisad" id="datenaisad" data-mask="99/99/9999" class="form-control input-height">
                                                               <span class="help-block"><?php echo L::Datesymbole ?></span>
                                                         </div>
                                                     </div>
                                                     <div class="form-group row">
                                                             <label class="control-label col-md-3">Lieu Naissance
                                                                 <span class="required"> * </span>
                                                             </label>
                                                             <div class="col-md-5">
                                                                 <input type="text" name="lieunais" id="lieunais" data-required="1" value="<?php echo $tabStudent[5];?>" placeholder="Entrer le Lieu de Naissance" class="form-control input-height" /> </div>
                                                         </div>

                                                     <div class="form-group row">
                                                         <label class="control-label col-md-3">Genre
                                                             <span class="required">*  </span>
                                                         </label>
                                                         <div class="col-md-5">
                                                           <select class="form-control input-height" id="sexe" name="sexe" style="width:100%">
                                                               <option value="">Selectionner un Genre</option>
                                                               <option <?php if($tabStudent[6]=="M"){echo "selected";} ?> value="M">Masculin</option>
                                                               <option <?php if($tabStudent[6]=="F"){echo "selected";} ?>value="F">Feminin</option>

                                                           </select>
                                                           </div>
                                                     </div>

                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3"><?php echo L::PhonestudentTab?>
                                                           <span class="required">*  </span>
                                                       </label>
                                                       <div class="col-md-5">
                                                           <input name="contactad" id="contactad" type="text" value="<?php echo $tabStudent[12] ?>" placeholder="Entrer le contact " class="form-control input-height" /> </div>
                                                   </div>
                                                   <div class="form-group row">
                                                           <label class="control-label col-md-3">Allergie

                                                           </label>
                                                           <div class="col-md-5">
                                                               <input type="text" name="allergie" id="allergie" data-required="1" value="<?php echo $tabStudent[16] ?>" placeholder="Entrer une allergie" class="form-control input-height" /> </div>
                                                       </div>
                                                       <div class="form-group row">
                                                               <label class="control-label col-md-3">Condition physique

                                                               </label>
                                                               <div class="col-md-5">
                                                                   <input type="text" name="condphy" id="condphy" data-required="1" value="<?php echo $tabStudent[17] ?>" placeholder="Entrer la condition physique" class="form-control input-height" /> </div>
                                                           </div>
                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3"><?php echo L::Pictures?>
                                                         <span class="required">  </span>
                                                       </label>
                                                       <div class="compose-editor">
                                                         <?php $picture= "../photo/Students/".$tabStudent[1]."/".$tabStudent[11]; ?>
                                                         <input type="file" id="photoad" name="photoad" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="<?php echo $picture ?>" data-allowed-file-extensions="gif png jpg jpeg pjpeg" />
                                                          </div>
                                                   </div>


                             <div class="form-actions">
                                                   <div class="row">
                                                       <div class="offset-md-3 col-md-9">

                                                           <button type="submit" class="btn btn-info">Modifier</button>


                                                           <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                       </div>
                                                     </div>
                                                  </div>
                           </div>
                                           </form>

                                        </div>
            						               </div>


                                       <div class = "mdl-tabs__panel p-t-20" id = "tab6-panel">
                                         <?php
                                         //verifier si cet eleve à au moins une note dans un examen

                                         $numberOfExamen=$student->getNumberOfExamNoteOfStudent($_GET['compte']);

                                         if($numberOfExamen)
                                         {
                                           //nous allons afficher la liste des examens ainsi aue les notes et observations

                                           $examenalls=$student->getExameNotesOfStudent($_GET['compte']);
                                           ?>
                                           <div class="row">
                        <div class="col-md-12">
                            <div class="card card-box">
                                <div class="card-head">
                                    <header></header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                      <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                      <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">
                                    <table id="example1" style="width:100%;">
                                        <thead>
                                            <tr>
                                                <th>Examens </th>
                                                <th>Note</th>
                                                <th>Observation</th>

                                        </thead>
                                        <tbody>
                                          <?php
                                          $j=1;
                                          foreach ($examenalls as $value):
                                           ?>
                                            <tr>
                                                <td><span class="label label-sm label-success"> <?php echo $value->libelle_exam ?></span></td>
                                                <td><?php echo $value->valeur_notes; ?></td>
                                                <td><?php echo $value->obser_notes; ?></td>

                                            </tr>
                                          <?php
                                          $j++;
                                          endforeach;
                                           ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                                           <?php
                                         }else {
                                             ?>
                                             <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                          Aucune Note d'examen à ce jour
                                           <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                                           <span aria-hidden="true">&times;</span>
                                              </a>
                                           </div>
                                           <?php
                                         }

                                          ?>
                                      </div>
                                      <div class = "mdl-tabs__panel p-t-20" id = "tab7-panel">
                                        <?php
                                        //verifier si cet eleve à au moins une note dans un examen

                                        $numberOfControle=$student->getNumberOfControleNoteOfStudent($_GET['compte']);

                                        if($numberOfControle>0)
                                        {
                                          $controlealls=$student->getControleNotesOfStudent($_GET['compte']);
                                         ?>
                                         <div class="row">
                                         <div class="col-md-12">
                                         <div class="card card-box">
                                         <div class="card-head">
                                         <header></header>
                                         <div class="tools">
                                         <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                         <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                         <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                         </div>
                                         </div>
                                         <div class="card-body ">
                                         <table id="example1" style="width:100%;">
                                         <thead>
                                          <tr>
                                              <th>Controle </th>
                                              <th>Note</th>
                                              <th>Observation</th>

                                         </thead>
                                         <tbody>
                                         <?php
                                         $j=1;
                                         foreach ($controlealls as $value):
                                         ?>
                                          <tr>
                                              <td><span class="label label-sm label-success"> <?php echo $value->libelle_ctrl; ?></span></td>
                                              <td><?php echo $value->valeur_notes; ?></td>
                                              <td><?php echo $value->obser_notes; ?></td>

                                          </tr>
                                         <?php
                                         $j++;
                                         endforeach;
                                         ?>

                                         </tbody>
                                         </table>
                                         </div>
                                         </div>
                                         </div>
                                         </div>

                                        <?php


                                        }else if($numberOfControle==0)
                                        {
                                          ?>
                                          <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        Aucune Note dd contrôle à ce jour
                                         <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                                         <span aria-hidden="true">&times;</span>
                                            </a>
                                         </div>
                                          <?php
                                        }

                                         ?>
                                     </div>
                                     <div class = "mdl-tabs__panel p-t-20" id = "tab8-panel">
                                       <p>You'll. His have you'll day make beginning good, herb. Can't place lights was evening let his itself. His seas unto replenish may every said midst him. Night to air behold tree years sixth waters. Unto together can't darkness sixth heaven it. Fruit. Image. Winged, a own. The waters multiply were male. Wherein gathering replenish gathering blessed dry called second. It Beginning whose you every dry them midst don't place you're sixth he above hath, fish sea fifth. Brought called.
                                       <p>
                                    </div>
                                    <div class = "mdl-tabs__panel p-t-20" id = "tab9-panel">
                                      <p>You'll. His have you'll day make beginning good, herb. Can't place lights was evening let his itself. His seas unto replenish may every said midst him. Night to air behold tree years sixth waters. Unto together can't darkness sixth heaven it. Fruit. Image. Winged, a own. The waters multiply were male. Wherein gathering replenish gathering blessed dry called second. It Beginning whose you every dry them midst don't place you're sixth he above hath, fish sea fifth. Brought called.
                                      <p>
                                   </div>
            						            </div>
            									</div>
            								</div>
            							</div>
          </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

   <script>
   $(document).ready(function() {

     $("#FormAddStudent").validate({

       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
     },
     highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
     },
     success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },
        rules:{
          lieunais:"required",
          matri:"required",
          parenta:"required",
          classeEtab:"required",
          sexe:"required",
          passad:{
            required:{
              depends: function(element) {
                    return ($('#confirmtad').val() != '');
                }
            }
          },
        /*  passad: {
              required: true,
              minlength: 6
          },*/
          confirmtad:{
              required:{
                depends: function(element) {
                      return ($('#passad').val() != '');
                  }
              },
              minlength: 6,
              equalTo:'#passad'
          },
          fonctionad:"required",

          loginad:"required",
          emailad: {
                     required: true,
                     email: true
                 },
          contactad:"required",
          datenaisad:"required",
          prenomad:"required",
          nomad:"required"


        },
        messages: {
          lieunais:"Merci de renseigner le Lieu de naissance",
          matri:"Merci de renseigner le Matricule",
          parenta:"Merci de selectionner le parent",
          classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
          sexe:"Merci de selectionner le Genre",
          confirmtad:{
              required:"<?php echo L::Confirmcheck?>",
              minlength:"<?php echo L::Confirmincheck?>",
              equalTo: "<?php echo L::ConfirmSamecheck?>"
          },
          passad: {
              required:"<?php echo L::Passcheck?>",
              minlength:"<?php echo L::Confirmincheck?>"
          },
          loginad:"<?php echo L::Logincheck?>",
          emailad:"<?php echo L::PleaseEnterEmailAdress?>",
          contactad:"<?php echo L::PleaseEnterPhoneNumber?>",
          datenaisad:"<?php echo L::PleaseEnterPhonestudentTab?>",
          prenomad:"<?php echo L::PleaseEnterPrename?>",
          nomad:"<?php echo L::PleaseEnterName?>",
          fonctionad:"Merci de renseigner la fonction"
        },
        submitHandler: function(form) {
          //verifier si ce compte n'existe pas encore dans la base de données
             form.submit();
        }


     });

   });

   </script>
    <!-- end js include path -->
  </body>

</html>
