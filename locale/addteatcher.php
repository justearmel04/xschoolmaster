<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
  <!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
  <link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
  <link href="../assets2/css/material_style.css" rel="stylesheet">
  <!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

  <!-- Theme Styles -->
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title">Ajouter Professeur</div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="#"><?php echo L::ProfsMenu?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active">Ajouter Professeur</li>
                          </ol>
                      </div>
                  </div>

                  <?php

                        if(isset($_SESSION['user']['addteaok']))
                        {

                          ?>
                          <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?php
                        //echo $_SESSION['user']['addetabok'];
                        ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                           </a>
                        </div-->
          <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
          <script src="../assets/js/sweetalert2.min.js"></script>

                    <script>
                    Swal.fire({
          title: '<?php echo L::Felicitations ?>',
          text: "<?php echo $_SESSION['user']['addteaok']; ?>",
          type: 'success',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ajouter les diplomes',
          cancelButtonText: '<?php echo L::AnnulerBtn ?>',
          }).then((result) => {
          if (result.value) {
          document.location.href="adddiplomes.php?compte="+<?php echo $_GET['compte'] ?>;
          }else {
          document.location.href="index.php";
          }
          })
                    </script>
                          <?php
                          unset($_SESSION['user']['addteaok']);
                        }

                         ?>

                         <div class="row">
                             <div class="col-md-12 col-sm-12">
                                 <div class="card card-box">
                                     <div class="card-head">
                                         <header>Informations Professeur</header>
                                          <!--button id = "panel-button"
                                        class = "mdl-button mdl-js-button mdl-button--icon pull-right"
                                        data-upgraded = ",MaterialButton">
                                        <i class = "material-icons">more_vert</i>
                                     </button>
                                     <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                        data-mdl-for = "panel-button">
                                        <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                                        <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                                        <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                                     </ul-->
                                     </div>

                                     <div class="card-body" id="bar-parent">
                                         <form  id="FormAddLocalAd" class="form-horizontal" action="../controller/teatcher.php" method="post" enctype="multipart/form-data">
                                             <div class="form-body">
                                               <div class="row">
                               <div class="col-md-12">

                         <fieldset style="background-color:#007bff;">

                         <center><legend style="color:white;"><?php echo L::TeatcherInfosPerso?></legend></center>
                         </fieldset>


                         </div>
                             </div>
                             <br/>
                             <div class="row">
                                                 <div class="col-md-6">
                             <div class="form-group">
                             <input type="file" id="photoTea" name="photoTea" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="../photo/user5.jpg" data-allowed-file-extensions="gif png jpg jpeg pjpeg" />

                             <label for=""><b>Image: </b></label>
                             </div>

                             </div>


                                           </div>
                             <div class="row">
                                                 <div class="col-md-6">
                             <div class="form-group">
                             <label for=""><b><?php echo L::Name?><span class="required"> * </span>: </b></label>

                           <input type="text" name="nomTea" id="nomTea" data-required="1" placeholder="<?php echo L::EnterName ?>" class="form-control" />
                             </div>

                             </div>
                             <div class="col-md-6">
                                  <div class="form-group">
                                    <label for=""><b>Prénoms <span class="required"> * </span>: </b></label>
                                     <input type="text" name="prenomTea" id="prenomTea" data-required="1" placeholder="<?php echo L::Enterparentprename ?>" class="form-control" />
                                   </div>


                              </div>

                                           </div>
                                           <div class="row">
                                                               <div class="col-md-6">
                                           <div class="form-group">
                                           <label for=""><b><?php echo L::BirthstudentTab?>: </b></label>

                                           <input type="text" placeholder="Entrer la date de naissance" name="datenaisTea" id="datenaisTea" data-mask="99/99/9999" class="form-control ">
                                             <span class="help-block"><?php echo L::Datesymbole ?></span>
                                           </div>

                                           </div>
                                           <div class="col-md-6">
                                                <div class="form-group">
                                                  <label for=""><b><?php echo L::BirthLocation?>: </b></label>
                                                   <input type="text" name="lieunaisTea" id="lieunaisTea" data-required="1" placeholder="Lieu de naissance" class="form-control " />
                                                 </div>


                                            </div>

                                                         </div>
                                                         <div class="row">
                                                                             <div class="col-md-6">
                                                         <div class="form-group">
                                                         <label for=""><b><?php echo L::Gender?> : </b></label>

                                                         <select class="form-control input-height" name="sexeTea" id="sexeTea">
                                                             <option selected value="">Selectionner le genre</option>
                                                             <option value="M">Masculin</option>
                                                             <option value="F">Feminin</option>
                                                         </select>
                                                         </div>

                                                         </div>
                                                         <div class="col-md-6">
                                                           <div class="form-group">
                                                           <label for=""><b><?php echo L::EmailstudentTab?> : </b></label>

                                                         <input type="email" class="form-control " name="emailTea" id="emailTea" placeholder="Entrer l'adresse email">
                                                         <span class="help-block" style="color:red;display:none" id="blocsms"> <i class="fa fa-warning" style="color:red;"></i> Vous devez renseigner une adresse email lorsque vous charger une image</span>
                                                           </div>


                                                          </div>

                                                                       </div>
                                                                       <div class="row">
                                                                         <div class="col-md-6">
                                                     <div class="form-group">
                                                     <label for=""><b><?php echo L::Nationalite?> : </b></label>

                                               <input name="natTea" id="natTea" type="text" placeholder="Entrer la nationalité " class="form-control " />
                                                     </div>

                                                     </div>
                                                                         <div class="col-md-6">
                                                                              <div class="form-group">
                                                                                <label for=""><b><?php echo L::PhonestudentTab?> </b></label>
                                                                               <input name="contactTea" id="contactTea" type="text" placeholder="Entrer le contact " class="form-control" />
                                                                               </div>


                                                                          </div>



                                                                                     </div>
                                                                                     <div class="row">
                                                                                                         <div class="col-md-6">
                                                                                     <div class="form-group">
                                                                                     <label for=""><b><?php echo L::MatrimonialeSituation?> : </b></label>

                                                                                     <select class="form-control " name="situationTea" id="situationTea">

                                                                                         <option selected value="">Selectionner la situation matrimoniale</option>
                                                                                         <option value="CELIBATAIRE">CELIBATAIRE</option>
                                                                                         <option value="MARIE(E)">MARIE(E)</option>
                                                                                         <option value="DIVORCE(E)">DIVORCE(E)</option>
                                                                                         <option value="VEUF(VE)">VEUF(VE)</option>
                                                                                     </select>
                                                                                     </div>

                                                                                     </div>
                                                                                     <div class="col-md-6">
                                                                                          <div class="form-group">
                                                                                            <label for=""><b><?php echo L::ChildNumber?></b></label>
                                                                                           <input name="nbchildTea" id="nbchildTea" type="number" min="0" placeholder="Entrer le nombre d'enfant " class="form-control" />
                                                                                           </div>


                                                                                      </div>
                                                                                      <div class="col-md-6">
                                                                  <div class="form-group">
                                                                  <label for=""><b>Type enseignant : </b></label>

                                                                  <select class="form-control " name="typeTea" id="typeTea">

                                                                      <option selected value="">Selectionner le type de l'enseignant</option>
                                                                      <option value="1">TITULAIRE</option>
                                                                      <option value="2">INTERIMAIRE</option>
                                                                  </select>
                                                                  </div>

                                                                  </div>
                                                                  <?php
                                                                    if($etablissementType==1||$etablissementType==3)
                                                                    {
                                                                   ?>
                                                                   <div class="col-md-6">
                                               <div class="form-group">
                                               <label for=""><b>Classe affectation : </b></label>

                                               <select class="form-control input-height" id="classeEtab" name="classeEtab" style="width:100%">
                                                   <option value=""><?php echo L::Selectclasses ?></option>
                                                   <?php
                                                   $i=1;
                                                     foreach ($classes as $value):
                                                     ?>
                                                     <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                     <?php
                                                                                      $i++;
                                                                                      endforeach;
                                                                                      ?>

                                               </select>
                                               </div>

                                               </div>
                                                                   <?php
                                                                 }
                                                                    ?>
                                                                                                   </div>
                                                                                                   <div class="row">
                                                                                   <div class="col-md-12">

                                                                         <fieldset style="background-color:#007bff;">

                                                                         <center><legend style="color:white;">INFORMATIONS PROFESSIONNELLES</legend></center>
                                                                         </fieldset>


                                                                         </div>
                                                                       </div><br/>
                                                                                 <div class="row">
                                                                                                     <div class="col-md-6">
                                                                                 <div class="form-group">
                                                                                 <label for=""><b>CNPS : </b></label>

                                                                               <input type="text" name="cnpsTea" id="cnpsTea" data-required="1" placeholder="Entrer le Numéro CNPS" class="form-control" />
                                                                                 </div>

                                                                                 </div>
                                                                                 <div class="col-md-6">
                                                                                      <div class="form-group">
                                                                                        <label for=""><b>Matricule : </b></label>
                                                                                         <input type="text" name="matTea" id="matTea" data-required="1" placeholder="Entrer le matricule" class="form-control" />
                                                                                       </div>


                                                                                  </div>

                                                                                               </div>
                                                                                               <div class="row">
                                                                                                                   <div class="col-md-6">
                                                                                               <div class="form-group">
                                                                                               <label for=""><b>Mutuelle : </b></label>

                                                                                             <input type="text" name="mutuelTea" id="mutuel" data-required="1" placeholder="Entrer la mutuelle" class="form-control" />
                                                                                               </div>

                                                                                               </div>
                                                                                               <div class="col-md-6">
                                                                                                    <div class="form-group">
                                                                                                      <label for=""><b>Date Embauche : </b></label>
                                                                                                      <input type="text" placeholder="Entrer la date d'embauche'" name="dateEmbTea" id="dateEmbTea" data-mask="99/99/9999" class="form-control">
                                                                                                        <span class="help-block"><?php echo L::Datesymbole ?></span>
                                                                                                     </div>


                                                                                                </div>

                                                                                                             </div>
                                                                                                             <div class="row">
                                                                                                                                 <!--div class="col-md-6">
                                                                                                             <div class="form-group">
                                                                                                             <label for=""><b>Salire Brut<span class="required"> * </span>: </b></label>

                                                                                                           <input type="number" min="0" name="brutTea" id="brutTea" data-required="1" placeholder="Entrer la salaire Brut" class="form-control input-height" />
                                                                                                             </div>

                                                                                                           </div-->
                                                                                                             <!--div class="col-md-6">
                                                                                                                  <div class="form-group">
                                                                                                                    <label for=""><b>Salaire Net : </b></label>
                                                                                                                 <input type="number" min="0" name="netTea" id="netTea" data-required="1" placeholder="Entrer la salaire Net" class="form-control input-height" />
                                                                                                                   </div>


                                                                                                              </div-->

                                                                                                                           </div>
                                                                                                                           <div class="row">
                                                                                                           <div class="col-md-12">

                                                                                                 <fieldset style="background-color:#007bff;">

                                                                                                 <center><legend style="color:white;">INFORMATIONS DE COMPTE</legend></center>
                                                                                                 </fieldset>


                                                                                                 </div>
                                                                                               </div><br/>


                                                                                                   <div class="form-group row">
                                                                                                       <label class="control-label col-md-3"><?php echo L::Logincnx?>

                                                                                                       </label>
                                                                                                       <div class="col-md-5">
                                                                                                           <input name="loginTea" id="loginTea" type="text" placeholder="Entrer le Login " class="form-control" />
                                                                                                           <input type="hidden" id="libetab" name="libetab" value="<?php echo $codeEtabAssigner?>"/>
                                                                                                           <input type="hidden" name="typeetablissement" id="typeetablissement" value="<?php echo $etablissementType; ?>">
                                                                                                           <input type="hidden" name="netTea" id="netTea" value="0">
                                                                                                           <input type="hidden" name="brutTea" id="brutTea" value="0">
                                                                                                           <input type="hidden" name="session" id="session" value="<?php echo $libellesessionencours; ?>">
                                                                                                          </div>
                                                                                                   </div>
                                                                                                   <div class="form-group row">
                                                                                                       <label class="control-label col-md-3"><?php echo L::Passcnx?>

                                                                                                       </label>
                                                                                                       <div class="col-md-5">
                                                                                                           <input name="passTea" id="passTea" type="password" placeholder="Entrer le Mot de passe " class="form-control " /> </div>
                                                                                                   </div>
                                                                                                   <div class="form-group row">
                                                                                                       <label class="control-label col-md-3"><?php echo L::ConfirmPasscnx?>

                                                                                                       </label>
                                                                                                       <div class="col-md-5">
                                                                                                           <input name="confirmTea" id="confirmTea" type="password" placeholder="Confirmer le Mot de passe " class="form-control" /> </div>
                                                                                                           <input type="hidden" name="etape" id="etape" value="1"/>
                                                                                                           <input type="hidden" name="comptead" id="comptead" value="ENSEIGNANT"/>
                                                                                                   </div>
                                                                                                   <div class="form-actions">
                                                                                                                         <div class="row">
                                                                                                                             <div class="offset-md-3 col-md-9">
                                                                                                                                 <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                                                                                                                 <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                                                                             </div>
                                                                                                                           </div>
                                                                                                                        </div>

                                             </div>
                                         </form>
                                     </div>
                                 </div>
                             </div>
                         </div>

					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <script src="../assets2/js/pages/material_select/getmdl-select.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

    <script>
    function addFrench()
    {
      var etape=1;
      var lang="fr";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

    function addEnglish()
    {
      var etape=1;
      var lang="en";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }
    $("#sexeTea").select2();
    $("#situationTea").select2();
    $("#typeTea").select2();
    $("#classeEtab").select2();



    jQuery(document).ready(function() {

      $("#FormAddLocalAd").validate({

        errorPlacement: function(label, element) {
        label.addClass('mt-2 text-danger');
        label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
        $(element).parent().addClass('has-danger')
        $(element).addClass('form-control-danger')
      },
      success: function (e) {
            $(e).closest('.control-group').removeClass('error').addClass('info');
            $(e).remove();
        },
         rules:{

       classeEtab:"required",
       typeTea:"required",
           passTea: {
             required:function(element){
                     return $("#confirmTea").val()!="" && $("#loginTea").val()!="";
                 },
               minlength: 6
           },
           confirmTea:{
               required:function(element){
                       return $("#passTea").val()!="" && $("#loginTea").val()!="";
                   },
               minlength: 6,
               equalTo:'#passTea'
           },

           loginTea: {
    required: function(element){
            return $("#passTea").val()!="" && $("#confirmTea").val()!="";
        }
},

           contactTea:{
                   required: true,
                   digits:true,
                   minlength: 8,
                   maxlength:12,
               },

           prenomTea:"required",
           nomTea:"required",

           sexeTea:"required",
           libetab:"required"




         },
         messages: {
           lieunaisTea:"Merci de renseigner le lieu de naissanec",
           natTea:"Merci de renseigner la Nationalité",
           situationTea:"Metci de renseigner la situation matrimoniale",
           nbchildTea:"Merci de renseigner le nombre d'enfants",
           cnpsTea:"Merci de renseigner le numéro CNPS",
           matTea:"Merci de renseigner le numéro Matricule",
           mutuelTea:"Merci de renseigner la Mutuelle",
           brutTea:"Merci de renseigner le salaire brut",
           netTea:"Merci de reneigner le salaire net",
           confirmTea:{
               required:"<?php echo L::Confirmcheck?>",
               minlength:"<?php echo L::Confirmincheck?>",
               equalTo: "<?php echo L::ConfirmSamecheck?>"
           },
           passTea: {
               required:"<?php echo L::Passcheck?>",
               minlength:"<?php echo L::Confirmincheck?>"
           },
           loginTea:"Merci de renseigner le Login",
           emailTea:"Merci de renseigner une adresse email",
           contactTea:{
             required: "<?php echo L::PleaseEnterPhoneNumber?>",
             minlength: "<?php echo L::PhoneNumberCaracteres?>",
             digits: "<?php echo L::PleaseEnterDigitsOnly?>",
             maxlength: "<?php echo L::NumbercaractersMax?>",
           },
           datenaisTea:"Merci de renseigner la date de naissance",
           prenomTea:"Merci de renseigner le prénom",
           nomTea:"Merci de renseigner le nom ",
           fonctionad:"<?php echo L::PleaseEnterFonction?>",
           gradeTea:"Merci de renseigner le grade",
           dateEmbTea:"Merci de renseigner la date d'embauche",
           sexeTea:"Merci de selectionner le sexe",
           libetab:"<?php echo L::PleaseSelectSchoolEnseignement?>",
           classeEtab:"Merci de selectionner la classe d'affectation",
           typeTea:"Merci de selectionner le type d'enseignant ",
         },
         submitHandler: function(form) {
           var photoTea=$("#photoTea").val();
           var caracteres=$("#photoTea").val().length;
           var emailTea=$("#emailTea").val();

           if(emailTea=="")
           {
               if(caracteres>0)
               {
                 $("#blocsms").css("display", "inline");
               }else {
                 $("#blocsms").css("display", "none");

                 var etape=1;
                 $.ajax({
                   url: '../ajax/teatcher.php',
                   type: 'POST',
                   async:false,
                   data: 'login=' + $("#loginTea").val()+ '&email=' + $("#emailTea").val()+'&codeetab=' + $("#libetab").val()+'&etape=' + etape,
                   dataType: 'text',
                   success: function (content, statut) {


                     if(content==0)
                     {
                       //le compte n'existe pas dans la base on peut l'ajouter

                       form.submit();
                     }else if(content==1) {
                       //le compte existe dejà dans la base de données
                       form.submit();
                       // Swal.fire({
                       // type: 'warning',
                       // title: '<?php echo L::WarningLib ?>',
                       // text: 'Cet Enseignant existe dejà dans la base de données',
                       //
                       // })

                     }else if(content==2) {

                       Swal.fire({
         title: '<?php echo L::WarningLib ?>',
         text: "Cet Enseignant existe dejà dans la base mais n'enseigne pas dans cet Etablissement",
         type: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Assigner le',
         cancelButtonText: '<?php echo L::AnnulerBtn ?>',
       }).then((result) => {
         if (result.value) {
            $("#etape").val(2);

            form.submit();
         }else {

         }
       })


                     }


                   }


                 });

               }
           }else {
             var etape=1;
             $.ajax({
               url: '../ajax/teatcher.php',
               type: 'POST',
               async:false,
               data: 'login=' + $("#loginTea").val()+ '&email=' + $("#emailTea").val()+'&codeetab=' + $("#libetab").val()+'&etape=' + etape,
               dataType: 'text',
               success: function (content, statut) {


                 if(content==0)
                 {
                   //le compte n'existe pas dans la base on peut l'ajouter

                   form.submit();
                 }else if(content==1) {
                   //le compte existe dejà dans la base de données
                   form.submit();
                   // Swal.fire({
                   // type: 'warning',
                   // title: '<?php echo L::WarningLib ?>',
                   // text: 'Cet Enseignant existe dejà dans la base de données',
                   //
                   // })

                 }else if(content==2) {

                   Swal.fire({
     title: '<?php echo L::WarningLib ?>',
     text: "Cet Enseignant existe dejà dans la base mais n'enseigne pas dans cet Etablissement",
     type: 'warning',
     showCancelButton: true,
     confirmButtonColor: '#3085d6',
     cancelButtonColor: '#d33',
     confirmButtonText: 'Assigner le',
     cancelButtonText: '<?php echo L::AnnulerBtn ?>',
   }).then((result) => {
     if (result.value) {
        $("#etape").val(2);

        form.submit();
     }else {

     }
   })


                 }


               }


             });
           }



         }


      });


    });
    </script>
    <!-- end js include path -->
  </body>

</html>
