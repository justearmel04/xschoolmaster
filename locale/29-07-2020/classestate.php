<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$student=new Student();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}


$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$sections=$classe->getAllsesctionsSchool($codeEtabAssigner,$libellesessionencours);




 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Application de communication écoles et parents</title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Etat Financier</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Etat par classe</li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->

					<div class="state-overview">

						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


          <div class="row">

            <div class="col-md-12 col-sm-12">
                                      <div class="card card-box">
                                          <div class="card-head">
                                              <header><?php echo L::Seacher ?></header>

                                          </div>
                                          <div class="card-body " id="bar-parent">
                                            <form method="post" id="FormSearch">
                                                <div class="row">
                                                  <div class="col-md-6 col-sm-6">
                                                  <!-- text input -->
                                                  <div class="form-group">
                                                      <label>Section</label>
                                                      <select class="form-control input-height" id="section" name="section" style="width:100%" onchange="searchclassesection()">
                                                          <option value="">Selectionner une section</option>
                                                          <?php
                                                          $i=1;
                                                            foreach ($sections as $value):
                                                            ?>
                                                            <option value="<?php echo $value->id_section?>"><?php echo utf8_encode(utf8_decode($value->libelle_section)) ?></option>

                                                            <?php
                                                                                             $i++;
                                                                                             endforeach;
                                                                                             ?>

                                                      </select>
                                                  </div>



                                              </div>
                                              <div class="col-md-6 col-sm-6">
                                              <!-- text input -->

                                              <div class="form-group">
                                                  <label>Classe</label>
                                                  <select class="form-control input-height" id="classex" name="classex" style="width:100%">
                                                      <option value=""><?php echo L::Selectclasses ?></option>


                                                  </select>
                                                  <input type="hidden" name="search" id="search"  value="">
                                                  <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner ?>">
                                                  <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours ?>">
                                              </div>


                                          </div>
                                                </div>

                                                <button type="submit" class="btn btn-danger"><i class="fa fa-search"></i> Rechercher</button>
                                            </form>
                                          </div>
                                      </div>
                                  </div>
          </div>


          <div class="row">

            <?php

            if(isset($_POST['search']))
            {
              //recupération des variables

              $codeEtabsearch=htmlspecialchars(addslashes($_POST['codeEtab']));
              $libellesessionsearch=htmlspecialchars(addslashes($_POST['libellesession']));
              $classesearch=htmlspecialchars(addslashes($_POST['classex']));
              $section=htmlspecialchars(addslashes($_POST['section']));

              //nous allons afficher la liste des eleves de cette classe

              $students=$student->getAllstudentofthisclassesSession($classesearch,$libellesessionencours);


              ?>
              <div class="col-md-12">
                  <div class="card  card-box">
                      <div class="card-head">
                          <header></header>
                          <div class="tools">
                              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                          </div>
                      </div>
                      <div class="card-body ">
                        <div class="pull-right">

                              <a class="btn btn-warning " href="#" onclick="genereEtat(<?php echo $classesearch; ?>,'<?php echo $codeEtabsearch; ?>','<?php echo $libellesessionsearch;  ?>',<?php echo $section; ?>)"><i class="fa fa-print"></i> Etat scolarités</a>

                        </div>

                        <div class="table-scrollable">
                          <table class="table table-hover table-checkable order-column full-width" id="example4">
                              <thead>
                                  <tr>
                                     <th>Matricule</th>
                                      <th>Nom & Prénoms</th>
                                      <th> Montant versé </th>
                                       <th> Reste à payer</th>
                                  </tr>
                              </thead>
                              <tbody>
                                <?php
                                $sommemontantvers=0;
                                $sommeresteapayer=0;
                                $devise="";
                                foreach ($students as $value):
                                 ?>
                                <tr>
                                  <td><?php echo $value->matricule_eleve;?></td>
                                   <td><?php echo $value->nom_eleve." ".$value->prenom_eleve;?></td>
                                   <td><?php
                                   //nous allons compter le nombre de versement

                                     $versementNb=$student->getNumberOfversementStudent($value->id_compte,$classesearch,$codeEtabsearch,$libellesessionsearch);
                                     //echo $versementNb;
                                     if($versementNb==0)
                                     {
                                       ?>
                                       <span class="label label-sm label-danger"> Aucun versement </span>
                                       <?php
                                     }else if($versementNb>0)
                                     {
                                       $datavers=$student->getSommeversement($value->id_compte,$classesearch,$codeEtabsearch,$libellesessionsearch,$section);
                                       $tabdata=explode("*",$datavers);
                                       $sommemontantvers=$sommemontantvers+$tabdata[0];
                                       $devise=$tabdata[1];

                                       ?>
                                       <span class="label label-sm label-success"> <?php echo $tabdata[0]." ".$tabdata[1]; ?> </span>
                                       <?php
                                     }
                                   //echo $student->getSommeversement($value->id_compte,$classesearch,$codeEtabsearch,$libellesessionsearch,$section);

                                    ?></td>
                                   <td>
                                     <?php
                                     if($versementNb==0)
                                     {
                                       //nous allons afficher le montant de scolarité de la section

                                       $sectiondetails=$student->getScolaritefraisSection($codeEtabsearch,$libellesessionsearch,$section);
                                       $datasectiondetails=explode("*",$sectiondetails);
                                       $sommeresteapayer=$sommeresteapayer+$datasectiondetails[0];

                                       ?>
                                        <span class="label label-sm label-success"> <?php echo $datasectiondetails[0]." ".$datasectiondetails[1]; ?> </span>
                                       <?php

                                     }else if($versementNb>0)
                                     {
                                       //retrouver le dernier Enregistrement du versement

                                       $datavers=$student->SelectInformationsOfLastVersement($codeEtabsearch,$classesearch,$libellesessionsearch,$value->id_compte);
                                       $tabdatavers=explode("*",$datavers);
                                       $sommeresteapayer=$sommeresteapayer+$tabdatavers[0];
                                       $content=$tabdatavers[0]." ".$tabdatavers[1];
                                       ?>
                                         <span class="label label-sm label-success"> <?php echo $content; ?> </span>
                                       <?php
                                     }
                                      ?>
                                   </td>
                               </tr>
                               <?php
                             endforeach;
                                ?>
                             </tbody>
                             <tfoot>
                               <tr>
                                   <th colspan="2"><span style="">Total</span></th>

                                   <th><?php echo number_format($sommemontantvers, 0, ',', ' ')." ".$devise; ?></th>
                                   <th><?php echo number_format($sommeresteapayer, 0, ',', ' ')." ".$devise; ?></th>

                               </tr>
                           </tfoot>
                          </table>
                          </div>
                      </div>
                  </div>
              </div>
              <?php

            }
             ?>



          </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
  <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
<script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
  <script src="../assets2/js/pages/table/table_data.js" ></script>
   <script src="../assets2/plugins/material/material.min.js"></script>
   <script src="../assets2/plugins/select2/js/select2.js" ></script>
   <script src="../assets2/js/pages/select2/select2-init.js" ></script>
   <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
   <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }
   $("#section").select2();
   $("#classex").select2();

   function genereEtat(classesearch,codeEtabsearch,libellesessionsearch,section)
   {
     var etape=1;
     $.ajax({
       url: '../ajax/etat.php',
       type: 'POST',
       async:false,
       data: 'codeEtab='+codeEtabsearch+'&etape='+etape+'&session='+libellesessionsearch+'&section='+section+'&classex='+classesearch,
       dataType: 'text',
       success: function (response, statut) {

           window.open(response, '_blank');

       }
     });

   }

   function searchclassesection()
   {
     var section=$("#section").val();
     var session="<?php  echo $libellesessionencours?>";
     var codeEtab="<?php  echo $codeEtabAssigner;?>";
     var etape=7;

     $.ajax({
       url: '../ajax/classe.php',
       type: 'POST',
       async:false,
       data: 'codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&section='+section,
       dataType: 'text',
       success: function (response, statut) {


         $("#classex").html("");
         $("#classex").html(response);
       }
     });


   }
   $(document).ready(function() {
$("#FormSearch").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
},
highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
},
success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{
    classex:"required",
    section:"required"
  },
  messages: {
    classex:"Merci de selectionner une classe",
    section:"Merci de selectionner une section"
  }
});


   });

   </script>
    <!-- end js include path -->
  </body>

</html>
