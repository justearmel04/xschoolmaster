<?php
session_start();
require_once('../class/User.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$etabs=new Etab();
$etab=new Etab();
$parent=new ParentX();
$user=new User();
$classe=new Classe();
$emailUti=$_SESSION['user']['email'];
$userId=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfile($emailUti);
$logindata=$user->getLoginProfile($emailUti);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$parents=$parent->getAllParent();

$codeEtabLocal=$etab->getcodeEtabByLocalId($userId);
$classes=$classe->getAllClassesbyschoolCode($codeEtabLocal);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Application de communication écoles et parents</title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title">Inscription Elève</div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#">Gestions Elèves</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active">Inscription</li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addStudok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: '<?php echo L::Felicitations ?>',
  text: "<?php echo $_SESSION['user']['addStudok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::AddNews ?>',
  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="index.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addStudok']);
                    }

                     ?>

              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="card card-topline-green">
                       <div class="card-head">
                           <header>Informations</header>
                           <div class="tools">
                               <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
       <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
       <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                           </div>
                       </div>
                       <div class="card-body ">
                         <p>
                           La promotion d'un élève de la classe actuelle à la classe suivante créera une inscription de cet élève à la session suivante. Assurez-vous de sélectionner les options de classe correctes dans le menu de sélection avant de faire la promotion, si vous ne voulez pas promouvoir un élève à la classe suivante, veuillez sélectionner cette option. Cela ne fera pas passer l'étudiant à la classe suivante, mais cela créera une inscription à la session suivante, mais dans la même classe.

                           </p>
                       </div>
                   </div>
                            </div>
                  <div class="col-md-12 col-sm-12">
                      <div class="card card-box">
                          <div class="card-head">
                              <header>Gestion Promotion</header>

                          </div>

                          <div class="card-body" id="bar-parent">
                              <form  id="FormAddStudent" class="form-horizontal" action="../controller/admission.php" method="post" enctype="multipart/form-data">
                                  <div class="form-body">
                                    <?php
                                    /*
                                     ?>
                                    <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Text</label>
                                            <input type="text" class="form-control" placeholder="Enter ...">
                                        </div>
                                      </div>
                                      <div class="col-md-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Text Disabled</label>
                                            <input type="text" class="form-control" placeholder="Enter ..." disabled>
                                        </div>
                                      </div>
                                      <div class="col-md-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Text Readonly</label>
                                            <input type="text" class="form-control" placeholder="Enter ..." readonly>
                                        </div>
                                      </div>

                                      <div class="col-md-3 col-sm-3">

                                        <div class="form-group">
                                            <label>Textarea</label>
                                            <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                        </div>


                                    </div>

                                </div>
                                <?php
                                */
                                 ?>

                                  </div>
                              </form>
                          </div>
                      </div>
                  </div>

              </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script>
 function addFrench()
 {
   var etape=1;
   var lang="fr";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function addEnglish()
 {
   var etape=1;
   var lang="en";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }
 jQuery(document).ready(function() {
$("#parenta").select2();
$("#sexe").select2();
$("#classeEtab").select2();

   $("#FormAddStudent").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{
        lieunais:"required",
        matri:"required",
        parenta:"required",
        classeEtab:"required",
        sexe:"required",
        passad: {
            required: true,
            minlength: 6
        },
        confirmtad:{
            required: true,
            minlength: 6,
            equalTo:'#passad'
        },
        fonctionad:"required",

        loginad:"required",
        emailad: {
                   required: true,
                   email: true
               },
        contactad:"required",
        datenaisad:"required",
        prenomad:"required",
        nomad:"required"


      },
      messages: {
        lieunais:"Merci de renseigner le Lieu de naissance",
        matri:"Merci de renseigner le Matricule",
        parenta:"Merci de selectionner le parent",
        classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
        sexe:"Merci de selectionner le Genre",
        confirmtad:{
            required:"Merci de confirmer le mot de passe",
            minlength:"Le mot de passe doit contenir au moins 6 caractères",
            equalTo: "Merci de renseigner des mots de passe identique"
        },
        passad: {
            required:"Merci de renseigner le mot de passe",
            minlength:"Le mot de passe doit contenir au moins 6 caractères"
        },
        loginad:"Merci de renseigner le Login",
        emailad:"Merci de renseigner une adresse email",
        contactad:"Merci de renseigner un contact",
        datenaisad:"Merci de renseigner la date de naissance",
        prenomad:"Merci de renseigner le prénom",
        nomad:"Merci de renseigner le nom ",
        fonctionad:"Merci de renseigner la fonction"
      },
      submitHandler: function(form) {
        //verifier si ce compte n'existe pas encore dans la base de données
           var etape=1;
           $.ajax({
             url: '../ajax/admission.php',
             type: 'POST',
             async:false,
             data: 'matricule=' + $("#matri").val()+ '&etape=' + etape+'&classe='+$("#classeEtab").val()+'&parent='+$("#parenta").val(),
             dataType: 'text',
             success: function (content, statut) {

               if(content==0)
               {
                 //le compte n'existe pas dans la base on peut l'ajouter

                 form.submit();
               }else if(content==1){
                 //le compte existe dejà dans la base de données
                 Swal.fire({
                 type: 'warning',
                 title: '<?php echo L::WarningLib ?>',
                 text: "Cet Elève existe dejà dans la base de données des elèves incrits pour l'année encours",

               })

             }else if(content==2)
             {
               $("#newStudent").val(0);
               form.submit();
             }

             }


           });
      }


   });


 });
 </script>
    <!-- end js include path -->
  </body>

</html>
