<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Student.php');
require_once('../controller/functions.php');

$session= new Sessionacade();
$student= new Student();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
// $allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);

//recuperation du code etablissement
$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);


$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}

$allinscriptions=$student->getallinscriptionsnotvalide($codeEtabAssigner,$libellesessionencours);



 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <style>
    #radioBtn .notActive{
  color: #3276b1;
  background-color: #fff;
}
    </style>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title">Liste des incriptions</div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><a class="parent-item" href="index.php">Comptabilités</a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="#">Inscriptions</a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active">Liste des inscriptions</li>
                          </ol>
                      </div>
                  </div>
					<!-- start widget -->

					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['updateparentok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: 'Félicitations',
            text: '<?php echo $_SESSION['user']['updateparentok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['updateparentok']);
                }

                 ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>



                 <div class="row">
                                         <div class="col-md-12">
                                             <div class="tabbable-line">
                                                <ul class="nav nav-pills nav-pills-rose">
                 									<!--li class="nav-item tab-all"><a class="nav-link active show"
                 										href="#tab1" data-toggle="tab">Liste</a></li-->
                 									<!--li class="nav-item tab-all"><a class="nav-link" href="#tab2"
                 										data-toggle="tab">Grille</a></li-->
                 								</ul>
                                                 <div class="tab-content">
                                                     <div class="tab-pane active fontawesome-demo" id="tab1">
                                                         <div class="row">
                 					                        <div class="col-md-12">
                 					                            <div class="card  card-box">
                 					                                <div class="card-head">
                 					                                    <header></header>
                 					                                    <div class="tools">
                 					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                 						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                 						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                 					                                    </div>
                 					                                </div>
                 					                                <div class="card-body ">
                                                            <div class="pull-right">
                                                               <!-- <a class="btn btn-primary " style="border-radius:5px;" href="addparent.php"><i class="fa fa-plus"></i> Nouveau Parent</a>
                                                                <a class="btn btn-warning " style="border-radius:5px;" href="#"><i class="fa fa-print"></i> Liste Parent</a> -->

                                                            </div>
                 					                                  <div class="table-scrollable">
                 					                                    <table class="table table-hover table-checkable order-column full-width" id="example4">
                 					                                        <thead>
                 					                                            <tr>
                                                                        <th> Matricule</th>
                 					                                                <th> Nom & Prénoms </th>
                 					                                                <th> Classe</th>
                                                                          <?php
                                                                          if($etablissementType==2)
                                                                          {
                                                                           ?>
                 					                                                <th> Statut </th>
                                                                          <?php
                                                                        }
                                                                           ?>
                                                                          <th> <?php echo L::Actions?> </th>
                 					                                            </tr>
                 					                                        </thead>
                 					                                        <tbody>
                                                                    <?php
                                                                    foreach ($allinscriptions as  $value):
                                                                     ?>
                                                                    <tr>
                                                                      <td><?php echo $value->matricule_eleve; ?></td>
                                                                      <td><?php echo $value->nom_compte." ".$value->prenom_compte; ?></td>
                                                                      <td><?php echo $value->libelle_classe; ?></td>
                                                                      <?php
                                                                      if($etablissementType==2)
                                                                      {
                                                                        $statut="";
                                                                        if($value->affecter_Id_eleve==0)
                                                                        {
                                                                          $statut="AFF";
                                                                        }else
                                                                        {
                                                                          $statut="NAFF";
                                                                        }
                                                                       ?>
                                                                      <td><?php echo $statut; ?></td>
                                                                      <?php
                                                                    }
                                                                       ?>
                                                                      <td>
                                                                        <a href="#" class="btn btn-success btn-xs" title="valider inscription" data-backdrop="static" data-toggle="modal" data-target="#fluidModel<?php echo $value->id_inscrip ?>"><i class="fa fa-check"></i> </a>
                                                                        <div class="modal fade" data-backdrop="static" id="fluidModel<?php echo $value->id_inscrip ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					    <div class="modal-dialog" role="document">
					        <div class="modal-content">
					            <div class="modal-header">
					                <h4 class="modal-title" id="exampleModalLabel">Validation inscription</h4>
					                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					                    <span aria-hidden="true">&times;</span>
					                </button>
					            </div>
					            <div class="modal-body">
					               <form id="incriptionForm<?php echo $value->id_inscrip ?>" class="" action="">
                           <div class="row">
                             <div class="col-md-6">
																		<div class="form-group">
											       <label for="nom<?php echo $value->id_inscrip ?>">Nom<span class="required">*</span> :</label>
											<input type="text" name="nom<?php echo $value->id_inscrip ?>" class="form-control" id="nom<?php echo $value->id_inscrip ?>" value="<?php echo $value->nom_compte; ?>" size="32" maxlength="225" readonly />

																		 </div>

                           </div>
                           <div class="col-md-6">
                                  <div class="form-group">
                           <label for="prenom<?php echo $value->id_inscrip ?>">Prénoms<span class="required">*</span> :</label>
                    <input type="text" name="prenom<?php echo $value->id_inscrip ?>" class="form-control" id="prenom<?php echo $value->id_inscrip ?>" value="<?php echo $value->prenom_compte; ?>" size="32" maxlength="225" readonly />

                                   </div>

                         </div>
                         <div class="col-md-6">
                                <div class="form-group">
                         <label for="datenais<?php echo $value->id_inscrip ?>">Date naissance<span class="required">*</span> :</label>
                  <input type="text" name="datenais<?php echo $value->id_inscrip ?>" class="form-control" id="datenais<?php echo $value->id_inscrip ?>" value="<?php echo date_format(date_create($value->datenais_eleve), "d/m/Y"); ?>" readonly size="32" maxlength="225" />

                                 </div>

                       </div>
                       <div class="col-md-6">
                              <div class="form-group">
                       <label for="lieunais<?php echo $value->id_inscrip ?>">Lieu naissance<span class="required">*</span> :</label>
                <input type="text" name="lieunais<?php echo $value->id_inscrip ?>" class="form-control" id="lieunais<?php echo $value->id_inscrip ?>" readonly value="<?php echo $value->lieunais_eleve; ?>" size="32" maxlength="225" />

                               </div>

                     </div>
                     <div class="col-md-6">
                            <div class="form-group">
                     <label for="classe<?php echo $value->id_inscrip ?>">Classe<span class="required">*</span> :</label>
              <input type="text" name="classe<?php echo $value->id_inscrip ?>" class="form-control" id="classe<?php echo $value->id_inscrip ?>" readonly value="<?php echo $value->libelle_classe; ?>" size="32" maxlength="225" />

                             </div>

                   </div>
                   <?php
                   if($etablissementType==2)
                   {
                    ?>
                    <div class="col-md-6">
                           <div class="form-group">
                    <label for="statut<?php echo $value->id_inscrip ?>">Statut<span class="required">*</span> :</label>
             <input type="text" name="statut<?php echo $value->id_inscrip ?>" class="form-control" id="statut<?php echo $value->id_inscrip ?>" value="<?php echo $statut; ?>" size="32" maxlength="225" />

                            </div>

                  </div>
                    <?php
                      if($statut=="AFF")
                      {
                        ?>
                        <div class="col-md-6">
                               <div class="form-group">
                        <label for="montant<?php echo $value->id_inscrip ?>">Montant inscription<span class="required">*</span> :</label>
                 <input type="text" name="montant<?php echo $value->id_inscrip ?>" class="form-control" id="montant<?php echo $value->id_inscrip ?>" value="<?php echo $value->scolariteaff_classe?>" size="32" maxlength="225" readonly />

                                </div>

                      </div>
                        <?php
                      }else {
                        ?>
                        <div class="col-md-6">
                               <div class="form-group">
                        <label for="montant<?php echo $value->id_inscrip ?>">Montant inscription<span class="required">*</span> :</label>
                 <input type="text" name="montant<?php echo $value->id_inscrip ?>" class="form-control" id="montant<?php echo $value->id_inscrip ?>" value="<?php echo $value->scolarite_classe?>" size="32" maxlength="225" readonly/>

                                </div>

                      </div>
                        <?php
                      }


                   }else {
                     ?>
                     <div class="col-md-6">
                            <div class="form-group">
                     <label for="montant<?php echo $value->id_inscrip ?>">Montant inscription<span class="required">*</span> :</label>
              <input type="text" name="montant<?php echo $value->id_inscrip ?>" class="form-control" id="montant<?php echo $value->id_inscrip ?>" value="<?php echo $value->scolarite_classe?>" size="32" maxlength="225" readonly />

                             </div>

                   </div>
                     <?php
                   }
                     ?>




                   <div class="col-md-6">
                          <div class="form-group">
                   <label for="montantv<?php echo $value->id_inscrip ?>">Montant verser<span class="required">*</span> :</label>
            <input type="text" name="montantv<?php echo $value->id_inscrip ?>" class="form-control" id="montantv<?php echo $value->id_inscrip ?>" value="" size="32" maxlength="225" onchange="checker(<?php echo $value->id_inscrip ?>)" />

                           </div>

                 </div>
                 <div class="col-md-6">
                        <div class="form-group">
                 <label for="modev<?php echo $value->id_inscrip ?>">Mode de paiement<span class="required">*</span> :</label>
                 <!-- <select class="form-control" name="modev<?php //echo $value->id_inscrip ?>" id="modev<?php echo $value->id_inscrip ?>">
                   <option value="">Selectionner un mode de paiement</option>
                   <option value="1">Chèque</option>
                   <option value="2">Espèce</option>
                 </select> -->
                 <div class="col-sm-7 col-md-7">
                 <div class="input-group">
                 <div id="radioBtn" class="btn-group">
                 <a class="btn btn-primary btn-sm active" data-toggle="mode<?php echo $value->id_inscrip ?>" onclick="espece(<?php echo $value->id_inscrip ?>)" id="btn1<?php echo $value->id_inscrip ?>" data-title="Y" >ESPECE</a>
                 <a class="btn btn-primary btn-sm notActive" data-toggle="mode<?php echo $value->id_inscrip ?>" onclick="cheque(<?php echo $value->id_inscrip ?>)" id="btn2<?php echo $value->id_inscrip ?>"  data-title="N" >CHEQUE</a>
                 </div>
                 <input type="hidden" name="mode<?php echo $value->id_inscrip ?>" id="mode<?php echo $value->id_inscrip ?>" value="1">
                 </div>
                 </div>


                         </div>

               </div>
               <!-- <div class="form-actions"> -->
                 <!-- <div class="row"> -->
                   <div class="offset-md-7 col-md-9">
                     <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
                     <!-- <button type="button" onclick="validationForm(<?php //echo $value->id_inscrip ?>)" class="btn btn-success" >Valider</button> -->
                     <button type="submit" class="btn btn-success" id="validebtn<?php echo $value->id_inscrip ?>" disabled>Valider</button>
                   </div>
                   </div>

                 <!-- </div> -->

               <!-- </div> -->

                         </form>



					    </div>
					</div>
                                                                      </td>
                                                                    </tr>
                                                                    <style >
                                                                    #radioBtn .notActive{
                                                                  color: #3276b1;
                                                                  background-color: #fff;
                                                                }


                                                                    </style>
                                                                    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                                                    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
                                                                    <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
                                                                    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                                                    <!-- <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script> -->
                                                                    <script type="text/javascript">
                                                                    function SeparateurMillier(a, b) {
                                                                  a = '' + a;
                                                                  b = b || ' ';
                                                                  var c = '',
                                                                      d = 0;
                                                                  while (a.match(/^0[0-9]/)) {
                                                                    a = a.substr(1);
                                                                  }
                                                                  for (var i = a.length-1; i >= 0; i--) {
                                                                    c = (d != 0 && d % 3 == 0) ? a[i] + b + c : a[i] + c;
                                                                    d++;
                                                                  }
                                                                  return c;
                                                                }
                                                                    function checker(id)
                                                                    {
                                                                      //recuperer le montant de la scolarité
                                                                      var etape=6;
                                                                      var scolarite=$("#montant"+id).val();
                                                                      var montantverser=$("#montantv"+id).val();
                                                                         $.ajax({
                                                                           url: '../ajax/login.php',
                                                                           type: 'POST',
                                                                           async:false,
                                                                           data: 'scolarite=' +scolarite+ '&etape=' + etape+'&montantverser='+montantverser,
                                                                           dataType: 'text',
                                                                           success: function (response, statut) {
                                                                             if(response==0)
                                                                             {
                                                          $("#validebtn"+id).prop("disabled",false);
                                                                             }else if(response==1)
                                                                             {
                                                          $("#validebtn"+id).prop("disabled",true);
                                                                             }
                                                                             $("#montantv"+id).removeClass('error').addClass('info');
                                                                             // $("#montantv"+id).val(SeparateurMillier(montantverser));


                                                                           }
                                                                         });




                                                                    }
                                                                    function espece(id)
                                                                    {
                                                                      $("#btn1"+id).addClass("active");
                                                                      $("#btn2"+id).removeClass("active");
                                                                      $("#btn2"+id).addClass("notActive");
                                                                      $("#mode").val(1);
                                                                    }
                                                                    function cheque(id)
                                                                    {
                                                                      $("#btn2"+id).addClass("active");
                                                                      $("#btn1"+id).removeClass("active");
                                                                      $("#btn1"+id).addClass("notActive");
                                                                      $("#mode").val(2);
                                                                    }

                                                                    function validationForm(id)
                                                                    {
                                                                      var montant="montant"+id;
                                                                      $("#incriptionForm"+id).validate({
                                                                        errorPlacement: function(label, element) {
                                                                        label.addClass('mt-2 text-danger');
                                                                        label.insertAfter(element);
                                                                      },
                                                                      highlight: function(element, errorClass) {
                                                                        $(element).parent().addClass('has-danger')
                                                                        $(element).addClass('form-control-danger')
                                                                      },
                                                                      success: function (e) {
                                                                            $(e).closest('.control-group').removeClass('error').addClass('info');
                                                                            $(e).remove();
                                                                        },
                                                                        rules:{
                                                                          montant:"required"
                                                                        },
                                                                        messages:{
                                                                          montant:"Merci de renseigner le montant verser"
                                                                        }
                                                                      });
                                                                    }

                                                                    $("#incriptionForm<?php echo $value->id_inscrip ?>").validate({
                                                                      errorPlacement: function(label, element) {
                                                  								 label.addClass('mt-2 text-danger');
                                                  								 label.insertAfter(element);
                                                  							 },
                                                  							 highlight: function(element, errorClass) {
                                                  								 $(element).parent().addClass('has-danger')
                                                  								 $(element).addClass('form-control-danger')
                                                  							 },
                                                  							 success: function (e) {
                                                  										 $(e).closest('.control-group').removeClass('error').addClass('info');
                                                  										 $(e).remove();
                                                  								 },
                                                                   rules:{

                                            									 montantv<?php echo $value->id_inscrip ?>:{
                                                          required: true,
                                                          digits:true,
                                                          max:$("#montant<?php echo $value->id_inscrip ?>").val(),
                                                          min:1

                                                      }
                                            								 },
                                            								 messages:{

                                            									 montantv<?php echo $value->id_inscrip ?>:{
                                                          required:"Merci de renseigner le montant décaisser",
                                                          digits:"<?php echo L::DigitsOnly?>",
                                                          max:"le montant versé ne peut être supérieur au de la scolarité",
                                                          min:"Le montant verser doit être supérieur à 0"

                                                      }
                                            				},
                                                                    });

                                                                    // incriptionForm


                                                                    </script>
                                                                    <?php
                                                                  endforeach;
                                                                     ?>
                 															                   </tbody>
                 					                                    </table>
                 					                                    </div>
                 					                                </div>
                 					                            </div>
                 					                        </div>
                 					                    </div>
                                                     </div>
                                                     <div class="tab-pane" id="tab2">



                                     					<div class="row">
                                                 <?php
                                                 $i=1;
                                                   foreach ($allparents as $value):
                                                   ?>
                 					                        <div class="col-md-4" ondblclick="myFunction(<?php echo $value->id_compte?>)">
                 				                                <div class="card card-box">
                 				                                    <div class="card-body no-padding ">
                 				                                    	<div class="doctor-profile">
                                                                 <?php
                                                                   $lienImg="";
                                                                   if(strlen($value->photo_compte)>0)
                                                                   {
                                                                     $lienImg="../photo/".$value->email_compte."/".$value->photo_compte;
                                                                   }else {
                                                                     $lienImg="../photo/user5.jpg";
                                                                   }
                                                                  ?>
                                                                        <img src="<?php echo $lienImg?>" class="doctor-pic" alt="" style="height:100px; width:90px;">
                 					                                        <div class="profile-usertitle">
                 					                                             <div class="doctor-name"><?php echo $value->nom_compte." ".$value->prenom_compte;?> </div>
                 					                                             <div class="name-center"> <?php echo $value->fonction_compte;?></div>
                 					                                        </div>

                                                                   <p><?php echo $value->email_compte;?></p>
                                                                         <div><p><i class="fa fa-phone"></i><a href="">  <?php echo $value->tel_parent; ?></a></p> </div>
                                                                   <div class="profile-userbuttons">
                                                                        <a href="#" class="btn btn-circle btn-info btn-sm" onclick="modify(<?php echo $value->id_compte ?>)"><i class="fa fa-pencil"></i></a>
                                                                        <a href="#" class="btn btn-circle btn-danger btn-sm" onclick="deleted(<?php echo $value->id_compte?>)"><i class="fa fa-trash"></i></a>
                                                                   </div>
                 				                                        </div>
                 				                                    </div>
                 				                                </div>
                 					                        </div>
                                                   <?php
                                                                                    $i++;
                                                                                    endforeach;
                                                                                    ?>


                                     					</div>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
  <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
  <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function searchparentname()
   {

     var etape=4;
     var codeEtab="<?php echo $codeEtabAssigner; ?>";

     $.ajax({
              url: '../ajax/parent.php',
              type: 'POST',
              async:false,
              data: 'code='+codeEtab+'&etape='+etape+'&idcompte='+$("#studentid").val(),
              dataType: 'text',
              success: function (content, statut) {

                $("#parentname").html("");
                $("#parentname").html(content);

                //nous allons chercher la liste des admins local de cet etablissement

              /*  $.ajax({
                         url: '../ajax/localadmin.php',
                         type: 'POST',
                         async:false,
                         data: 'code='+ $("#codeetab").val()+'&etape='+etape,
                         dataType: 'text',
                         success: function (response, statut) {


                           $("#adminlo").html("");
                           $("#adminlo").html(response);
                         }
                       });*/

              }

            });

   }
   $(document).ready(function() {

     $('#example5').DataTable( {
         "scrollX": true

     } );

     $("#codeetab").select2();

     $("#libetab").select2();

     $("#parentname").select2();

     $("#studentid").select2();

     $("#FormSearch").validate({

            errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
          },
          highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger')
            $(element).addClass('form-control-danger')
          },
          success: function (e) {
                $(e).closest('.control-group').removeClass('error').addClass('info');
                $(e).remove();
            },
              rules:{
                studentid:"required"
              },
              messages:{
                studentid:"Merci de selectionner un eleve"
              }
     });




   });

   </script>
    <!-- end js include path -->
  </body>

</html>
