<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/Teatcher.php');
require_once('../class/Diplome.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();
$classe=new Classe();
$diplome=new Diplome();
$user=new User();
$etabs=new Etab();
$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}


$teatcher=new Teatcher();

$detailsTeach=$teatcher->getTeatcherInfobyId($_GET['compte']);

$tabteatcher=explode("*",$detailsTeach);

$alletab=$etabs->getAllEtab();

$diplomes=$diplome->getDiplomebyTeatcherId($_GET['compte']);

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title">Détails Professeur</div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#"><?php echo L::ProfsMenu?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active">Détails Professeur</li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addteaok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: 'Félicitations !',
  text: "<?php echo $_SESSION['user']['addteaok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Ajouter les diplomes',
  cancelButtonText: 'Annuler',
}).then((result) => {
  if (result.value) {
document.location.href="adddiplomes.php?compte="+<?php echo $_GET['compte'] ?>;
  }else {
    document.location.href="index.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addteaok']);
                    }

                     ?>

              <div class="row">
                <div class="col-sm-12">
              <div class="card-box">
                <div class="card-head">
                  <header></header>
                </div>
                <div class="card-body ">
                      <div class = "mdl-tabs mdl-js-tabs">
                         <div class = "mdl-tabs__tab-bar tab-left-side">
                            <a href = "#tab4-panel" class = "mdl-tabs__tab is-active"><?php echo L::TeatcherInfosPerso?></a>
                            <a href = "#tab5-panel" class = "mdl-tabs__tab">informations PROFESSIONNELLES</a>
                            <a href = "#tab6-panel" class = "mdl-tabs__tab">diplomes</a>
                            <a href = "#tab7-panel" class = "mdl-tabs__tab"><?php echo L::AccountInfos?></a>
                         </div>
                         <div class = "mdl-tabs__panel is-active p-t-20" id = "tab4-panel">
                           <div class="col-md-12 col-sm-12">
                               <div class="card card-box">
                                   <div class="card-head">
                                       <header></header>

                                   </div>

                                   <div class="card-body" id="bar-parent">
                                       <form  id="FormAddLocalAd" class="form-horizontal" action="../controller/teatcher.php" method="post" enctype="multipart/form-data">
                                           <div class="form-body">
                                             <div class="row">
                             <div class="col-md-12">

         <fieldset style="background-color:#007bff;">

         <center><legend style="color:white;"><?php echo L::TeatcherInfosPerso?></legend></center>
         </fieldset>


         </div>
                           </div>
                           <br/>
                           <div class="row">
                           <div class="col-md-6">
                           <div class="form-group">
                             <?php
                             if(strlen($tabteatcher[9])>0)
                             {
                               $lien="../photo/".$tabteatcher[8]."/".$tabteatcher[10];
                             }else {
                               $lien="../photo/user5.jpg";
                             }
                             ?>
                           <input disabled="disabled" type="file" id="photoTea" name="photoTea" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="<?php echo $lien; ?>" data-allowed-file-extensions="gif png jpg jpeg pjpeg" />
                           <label for=""><b><?php echo L::Image?><span class="required"> * </span>: </b></label>
                           </div>

                           </div>


                                         </div>
                           <div class="row">
                                               <div class="col-md-6">
                           <div class="form-group">
                           <label for=""><b><?php echo L::Name?><span class="required"> * </span>: </b></label>

                         <input type="text" readonly name="nomTea" id="nomTea" value="<?php echo $tabteatcher[1];?>" data-required="1" placeholder="Entrer le Nom" class="form-control input-height" />
                           </div>

                           </div>
                           <div class="col-md-6">
                                <div class="form-group">
                                  <label for=""><b><?php echo L::PreName?> : </b></label>
                                   <input type="text" readonly name="prenomTea" id="prenomTea" value="<?php echo $tabteatcher[2];?>" data-required="1" placeholder="Entrer le prénoms" class="form-control input-height" />
                                 </div>


                            </div>

                                         </div>
                                         <div class="row">
                                                             <div class="col-md-6">
                                         <div class="form-group">
                                         <label for=""><b>Date de naissance<span class="required"> * </span>: </b></label>

                                         <input type="text" readonly placeholder="Entrer la date de naissance" value="<?php  echo date_format(date_create($tabteatcher[3]),"d/m/Y");?>" name="datenaisTea" id="datenaisTea" data-mask="99/99/9999" class="form-control input-height">
                                           <span class="help-block">JJ-MM-AAAA</span>
                                         </div>

                                         </div>
                                         <div class="col-md-6">
                                              <div class="form-group">
                                                <label for=""><b><?php echo L::BirthLocation?>: </b></label>
                                                 <input type="text" readonly name="lieunaisTea" id="lieunaisTea" data-required="1" value="<?php echo $tabteatcher[11];?>" placeholder="Lieu de naissance" class="form-control input-height" />
                                               </div>


                                          </div>

                                                       </div>
                                                       <div class="row">
                                                                           <div class="col-md-6">
                                                       <div class="form-group">
                                                       <label for=""><b>Genre <span class="required"> * </span>: </b></label>

                                                       <select class="form-control input-height" readonly name="sexeTea" id="sexeTea">

                                                           <option <?php if($tabteatcher[4]=="M"){echo "selected";} ?> value="M">Masculin</option>
                                                           <option <?php if($tabteatcher[4]=="F"){echo "selected";} ?>  value="F">Feminin</option>
                                                       </select>
                                                       </div>

                                                       </div>
                                                       <div class="col-md-6">
                                                         <div class="form-group">
                                                         <label for=""><b>Email <span class="required"> * </span>: </b></label>

                                                       <input type="text" readonly class="form-control input-height" value="<?php echo $tabteatcher[8];?>"  name="emailTea" id="emailTea" placeholder="Entrer l'adresse email">
                                                         </div>


                                                        </div>

                                                                     </div>
                                                                     <div class="row">
                                                                       <div class="col-md-6">
                                                   <div class="form-group">
                                                   <label for=""><b>Nationalité <span class="required"> * </span>: </b></label>

                                             <input name="natTea" id="natTea" readonly type="text" placeholder="Entrer la nationalité " value="<?php echo $tabteatcher[6];?>" class="form-control input-height" />
                                                   </div>

                                                   </div>
                                                                       <div class="col-md-6">
                                                                            <div class="form-group">
                                                                              <label for=""><b><?php echo L::PhonestudentTab?> </b></label>
                                                                             <input readonly name="contactTea" id="contactTea" value="<?php echo $tabteatcher[5];?>" type="text" placeholder="Entrer le contact " class="form-control input-height" />
                                                                             </div>


                                                                        </div>



                                                                                   </div>
                                                                                   <div class="row">
                                                                                                       <div class="col-md-6">
                                                                                   <div class="form-group">
                                                                                   <label for=""><b>Situation Matrimoniale <span class="required"> * </span>: </b></label>

                                                                                   <select readonly class="form-control input-height" name="situationTea" id="situationTea">

                                                                                       <option  value=""><?php echo L::SelectMatrimonialeSituation?></option>
                                                                                       <option <?php if($tabteatcher[12]=="CELIBATAIRE"){echo "selected";}?> value="CELIBATAIRE">CELIBATAIRE</option>
                                                                                       <option <?php if($tabteatcher[12]=="MARIE(E)"){echo "selected";}?> value="MARIE(E)">MARIE(E)</option>
                                                                                       <option <?php if($tabteatcher[12]=="DIVORCE(E)"){echo "selected";}?> value="DIVORCE(E)">DIVORCE(E)</option>
                                                                                       <option <?php if($tabteatcher[12]=="VEUF(VE)"){echo "selected";}?> value="VEUF(VE)">VEUF(VE)</option>
                                                                                   </select>
                                                                                   </div>

                                                                                   </div>
                                                                                   <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                          <label for=""><b><?php echo L::ChildNumber?></b></label>
                                                                                         <input readonly name="nbchildTea" id="nbchildTea" value="<?php echo $tabteatcher[13];?>" type="number" min="0" placeholder="Entrer le nombre d'enfant " class="form-control input-height" />
                                                                                         </div>


                                                                                    </div>

                                                                                                 </div>


                                       <div class="form-actions">
                                                             <div class="row">
                                                                 <div class="offset-md-3 col-md-9">
                                                                   <a class="btn btn-info" href="updateteatcher.php?compte=<?php echo $_GET['compte']?>" >Modifier</a>
                                                                   <a class="btn btn-danger" href="teatchers.php" >Annuler</a>
                                                                 </div>
                                                               </div>
                                                            </div>
                                     </div>
                                 </form>
                                   </div>
                               </div>
                           </div>
                         </div>
                         <div class = "mdl-tabs__panel p-t-20" id = "tab5-panel">
                           <div class="col-md-12 col-sm-12">
                               <div class="card card-box">
                                   <div class="card-head">
                                       <header></header>

                                   </div>

                                   <div class="card-body" id="bar-parent">
                                       <form  id="FormAddLocalAd" class="form-horizontal" action="../controller/teatcher.php" method="post" enctype="multipart/form-data">
                                           <div class="form-body">
                                                  <div class="row">
                                                                                 <div class="col-md-12">

                                                                       <fieldset style="background-color:#007bff;">

                                                                       <center><legend style="color:white;">INFORMATIONS PROFESSIONNELLES</legend></center>
                                                                       </fieldset>


                                                                       </div>
                                                                     </div><br/>
                                                                               <div class="row">
                                                                                                   <div class="col-md-6">
                                                                               <div class="form-group">
                                                                               <label for=""><b>CNPS<span class="required"> * </span>: </b></label>

                                                                             <input type="text" readonly name="cnpsTea" id="cnpsTea" value="<?php echo $tabteatcher[14];?>" data-required="1" placeholder="Entrer le Numéro CNPS" class="form-control input-height" />
                                                                               </div>

                                                                               </div>
                                                                               <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                      <label for=""><b>Matricule : </b></label>
                                                                                       <input type="text" readonly name="matTea" id="matTea" value="<?php echo $tabteatcher[15];?>" data-required="1" placeholder="Entrer le matricule" class="form-control input-height" />
                                                                                     </div>


                                                                                </div>

                                                                                             </div>
                                                                                             <div class="row">
                                                                                                                 <div class="col-md-6">
                                                                                             <div class="form-group">
                                                                                             <label for=""><b>Mutuelle<span class="required"> * </span>: </b></label>

                                                                                           <input type="text" readonly name="mutuelTea" id="mutuelTea" value="<?php echo $tabteatcher[16];?>" data-required="1" placeholder="Entrer la mutuelle" class="form-control input-height" />
                                                                                             </div>

                                                                                             </div>
                                                                                             <div class="col-md-6">
                                                                                                  <div class="form-group">
                                                                                                    <label for=""><b>Date Embauche : </b></label>
                                                                                                    <input type="text" readonly placeholder="Entrer la date d'embauche'" name="dateEmbTea" value="<?php  echo date_format(date_create($tabteatcher[3]),"d/m/Y");?>"  id="dateEmbTea" data-mask="99/99/9999" class="form-control input-height">
                                                                                                      <span class="help-block">JJ-MM-AAAA</span>
                                                                                                   </div>


                                                                                              </div>

                                                                                                           </div>
                                                                                                           <div class="row">
                                                                                                                               <div class="col-md-6">
                                                                                                           <div class="form-group">
                                                                                                           <label for=""><b>Salire Brut<span class="required"> * </span>: </b></label>

                                                                                                         <input type="number" readonly min="1" name="brutTea" value="<?php echo $tabteatcher[17];?>" id="brutTea" data-required="1" placeholder="Entrer la salaire Brut" class="form-control input-height" />
                                                                                                           </div>

                                                                                                           </div>
                                                                                                           <div class="col-md-6">
                                                                                                                <div class="form-group">
                                                                                                                  <label for=""><b>Salaire Net : </b></label>
                                                                                                               <input type="number" readonly min="1" name="netTea" id="netTea" value="<?php echo $tabteatcher[18];?>" data-required="1" placeholder="Entrer la salaire Net" class="form-control input-height" />
                                                                                                                 </div>


                                                                                                            </div>

                                                                                                                         </div>










                                       <div class="form-actions">
                                                             <div class="row">
                                                                 <div class="offset-md-3 col-md-9">
                                                                   <a class="btn btn-info" href="updateteatcher.php?compte=<?php echo $_GET['compte']?>" >Modifier</a>
                                                                   <a class="btn btn-danger" href="teatchers.php" >Annuler</a>
                                                                 </div>
                                                               </div>
                                                            </div>
                                     </div>
                                 </form>
                                   </div>
                               </div>
                           </div>
                         </div>
                         <div class = "mdl-tabs__panel p-t-20" id = "tab6-panel">
                           <div class="col-md-12 col-sm-12">
                               <div class="card card-box">
                                   <div class="card-head">
                                       <header></header>

                                   </div>

                                   <div class="card-body" id="bar-parent">
                                       <form  id="FormAddLocalAd" class="form-horizontal" action="../controller/teatcher.php" method="post" enctype="multipart/form-data">
                                           <div class="form-body">
                                                </div>


                                                                                                 <div class="row">
                                                                                 <div class="col-md-12">

                                                                       <fieldset style="background-color:#007bff;">

                                                                       <center><legend style="color:white;">DIPLOMES</legend></center>
                                                                       </fieldset>


                                                                       </div>
                                                                     </div><br/>






         																	<div class="row">
         																	<table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                           <thead>
                                               <tr>


                                                   <th> <?php echo L::DegreeDesignation?> </th>
                                                   <th> <?php echo L::Ecole?> </th>
                                                   <th> <?php echo L::Speciality?> </th>
                                                   <th> <?php echo L::Level?> </th>
                                                   <th> <?php echo L::DateObtain?> </th>
                                                   <th> <?php echo L::File?> </th>

                                               </tr>
                                           </thead>
                                           <tbody>
                                             <?php
                                             $i=1;
                                               foreach ($diplomes as $value):
                                               ?>
                                               <tr class="odd gradeX">

                                                 <td>
                                                   <?php echo $value->libelle_diplo;?>
                                                 </td>
                                                   <td>
                                     <?php echo $value->ecole_diplo;?>
                                                   </td>
                                                   <td>
                                                       <span class="label label-sm label-default"><?php echo $value->specialite_diplo;?>  </span>
                                                   </td>
                                                   <td>
                                     <?php echo $value->niveau_diplo;?>
                                                   </td>
                                                   <td>
                                                     <?php echo date_format(date_create($value->dateobt_diplo),"d/m/Y");?>
                                                   </td>
                                                     <td>
                                                       <?php
                                                       if(strlen($value->fichier_diplo)>0)
                                                       {
                                                         $emailTea=$diplome->getTeatcherMail($_GET['compte']);
                                                         $lien="../diplomes/professeur/".$emailTea."/".$value->fichier_diplo;
                                                       ?>
                                                     <center>  <a href="<?php echo $lien;?>" target="_blank" class="btn btn-success" title="<?php echo L::SeeFile?>"><span class="fa fa-eye"></span></a></center>
                                                       <?php
                                                       }else {
                                                         ?>
                                                     <center>  <a href="#" target="_blank" class="btn btn-danger" title="<?php echo L::SeeFile?>"><span class="fa fa-times"></span></a></center>
                                                         <?php
                                                       }
                                                        ?>
                                                     </td>

                                               </tr>
                                               <?php
                                                                                $i++;
                                                                                endforeach;
                                                                                ?>


                                           </tbody>
                                       </table>
         																	</div>






                                       <div class="form-actions">
                                                             <div class="row">
                                                                 <div class="offset-md-3 col-md-9">
                                                                   <a class="btn btn-info" href="updateteatcher.php?compte=<?php echo $_GET['compte']?>" >Modifier</a>
                                                                   <a class="btn btn-danger" href="teatchers.php" >Annuler</a>
                                                                 </div>
                                                               </div>
                                                            </div>
                                     </div>
                                 </form>
                                   </div>
                               </div>
                           </div>
                           <div class = "mdl-tabs__panel p-t-20" id = "tab7-panel">
                             <div class="col-md-12 col-sm-12">
                                 <div class="card card-box">
                                     <div class="card-head">
                                         <header></header>
                                          <!--button id = "panel-button"
                                        class = "mdl-button mdl-js-button mdl-button--icon pull-right"
                                        data-upgraded = ",MaterialButton">
                                        <i class = "material-icons">more_vert</i>
                                     </button>
                                     <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                        data-mdl-for = "panel-button">
                                        <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                                        <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                                        <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                                     </ul-->
                                     </div>

                                     <div class="card-body" id="bar-parent">
                                         <form  id="Formcompte" class="form-horizontal" action="../controller/teatcher.php" method="post">
                                             <div class="form-body">

                                                 <div class="row">
                                               <div class="col-md-12">
                                                 <fieldset style="background-color:#007bff;">

                                               <center><legend style="color:white;">INFORMATIONS DE COMPTE</legend></center>
                                                 </fieldset>


                                                                                                 </div>
                                                                                               </div><br/>
                                                                                                  <div class="form-group row">
                                                                                                       <label class="control-label col-md-3"><?php echo L::Logincnx?>
                                                                                                           <span class="required">*  </span>
                                                                                                       </label>
                                                                                                       <div class="col-md-5">
                                                                                                           <input name="loginTea" id="loginTea" type="text" placeholder="Entrer le Login " value="<?php echo $tabteatcher[19];?>" class="form-control input-height" /> </div>
                                                                                                   </div>
                                                                                                   <div class="form-group row">
                                                                                                       <label class="control-label col-md-3"><?php echo L::Passcnx?>
                                                                                                           <span class="required">*  </span>
                                                                                                       </label>
                                                                                                       <div class="col-md-5">
                                                                                                           <input name="passTea" id="passTea" type="password" placeholder="Entrer le Mot de passe " class="form-control input-height" /> </div>
                                                                                                   </div>
                                                                                                   <div class="form-group row">
                                                                                                       <label class="control-label col-md-3"><?php echo L::ConfirmPasscnx?>
                                                                                                           <span class="required">*  </span>
                                                                                                       </label>
                                                                                                       <div class="col-md-5">
                                                                                                           <input name="confirmTea" id="confirmTea" type="password" placeholder="Confirmer le Mot de passe " class="form-control input-height" /> </div>
                                                                                                           <input type="hidden" name="etape" id="etape" value="4"/>
                                                                                                           <input type="hidden" name="idcompte" id="idcompte" value="<?php echo $_GET['compte']; ?>">
                                                                                                           <input type="hidden" name="comptead" id="comptead" value="ENSEIGNANT"/>
                                                                                                   </div>
                                                                                                   <div class="form-actions">
                                                                                                                         <div class="row">
                                                                                                                             <div class="offset-md-3 col-md-9">
                                                                                                                                 <!-- <a class="btn btn-info"  >Modifier</a> -->
                                                                                                                                 <button type="submit" name="button" class="btn btn-info">Modifier</button>
                                                                                                                                 <button type="button" class="btn btn-danger">Annuler</button>
                                                                                                                             </div>
                                                                                                                           </div>
                                                                                                                        </div>

                                             </div>
                                         </form>
                                     </div>
                                 </div>
                             </div>
                             </div>
                         </div>
                      </div>
                </div>
              </div>
            </div>
              </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <script src="../assets2/js/pages/material_select/getmdl-select.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script>

 function modify(id)
 {
   Swal.fire({
 title: 'Attention !',
 text: "Voulez-vous vraiment modifier ces informations",
 type: 'warning',
 showCancelButton: true,
 confirmButtonColor: '#3085d6',
 cancelButtonColor: '#d33',
 confirmButtonText: 'Modifier',
 cancelButtonText: 'Annuler',
 }).then((result) => {
 if (result.value) {
 document.location.href="updateteatcher.php?compte="+id;
 }else {

 }
 })
 }

 jQuery(document).ready(function() {

   $("#FormAddLocalAd").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{
        lieunaisTea:"required",
natTea:"required",
situationTea:"required",
nbchildTea:"required",
cnpsTea:"required",
matTea:"required",
mutuelTea:"required",
brutTea:"required",
netTea:"required",
        passTea: {
            required: true,
            minlength: 6
        },
        confirmTea:{
            required: true,
            minlength: 6,
            equalTo:'#passTea'
        },
        fonctionad:"required",

        loginTea:"required",
        emailTea: {
                   required: true,
                   email: true
               },
        contactTea:"required",
        datenaisTea:"required",
        prenomTea:"required",
        nomTea:"required",
        gradeTea:"required",
        dateEmbTea:"required",
        sexeTea:"required",
        libetab:"required"



      },
      messages: {
        lieunaisTea:"Merci de renseigner le lieu de naissanec",
        natTea:"Merci de renseigner la Nationalité",
        situationTea:"Metci de renseigner la situation matrimoniale",
        nbchildTea:"Merci de renseigner le nombre d'enfants",
        cnpsTea:"Merci de renseigner le numéro CNPS",
        matTea:"Merci de renseigner le numéro Matricule",
        mutuelTea:"Merci de renseigner la Mutuelle",
        brutTea:"Merci de renseigner le salaire brut",
        netTea:"Merci de reneigner le salaire net",
        confirmTea:{
            required:"<?php echo L::Confirmcheck?>",
            minlength:"<?php echo L::Confirmincheck?>",
            equalTo: "<?php echo L::ConfirmSamecheck?>"
        },
        passTea: {
            required:"<?php echo L::Passcheck?>",
            minlength:"<?php echo L::Confirmincheck?>"
        },
        loginTea:"Merci de renseigner le Login",
        emailTea:"Merci de renseigner une adresse email",
        contactTea:"Merci de renseigner un contact",
        datenaisTea:"Merci de renseigner la date de naissance",
        prenomTea:"Merci de renseigner le prénom",
        nomTea:"Merci de renseigner le nom ",
        fonctionad:"<?php echo L::PleaseEnterFonction?>",
        gradeTea:"Merci de renseigner le grade",
        dateEmbTea:"Merci de renseigner la date d'embauche",
        sexeTea:"Merci de selectionner le sexe",
        libetab:"<?php echo L::PleaseSelectSchoolEnseignement?>"
      },
      submitHandler: function(form) {
        //verifier si ce compte n'existe pas encore dans la base de données
           var etape=1;
           $.ajax({
             url: '../ajax/teatcher.php',
             type: 'POST',
             async:false,
             data: 'login=' + $("#loginTea").val()+ '&email=' + $("#emailTea").val()+'&codeetab=' + $("#libetab").val()+'&etape=' + etape,
             dataType: 'text',
             success: function (content, statut) {


               if(content==0)
               {
                 //le compte n'existe pas dans la base on peut l'ajouter

                 form.submit();
               }else if(content==1) {
                 //le compte existe dejà dans la base de données
                 Swal.fire({
                 type: 'warning',
                 title: 'Attention',
                 text: 'Cet Enseignant existe dejà dans la base de données et assigner à cet Etablissement',

                 })

               }else if(content==2) {

                 Swal.fire({
   title: 'Attention !',
   text: "Cet Enseignant existe dejà dans la base mais n'enseigne pas dans cet Etablissement",
   type: 'warning',
   showCancelButton: true,
   confirmButtonColor: '#3085d6',
   cancelButtonColor: '#d33',
   confirmButtonText: 'Assigner le',
   cancelButtonText: 'Annuler',
 }).then((result) => {
   if (result.value) {
      $("#etape").val(2);

      form.submit();
   }else {

   }
 })


               }
               /*
               if(content==0)
               {
                 //le compte n'existe pas dans la base on peut l'ajouter

                 form.submit();
               }else {
                 //le compte existe dejà dans la base de données
                 Swal.fire({
                 type: 'warning',
                 title: 'Attention',
                 text: 'Cet Enseignant existe dejà dans la base de données',

                 })

               }*/

             }


           });
      }


   });


   $("#Formcompte").validate({
     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
    },
    highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
    },
    success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
     rules:{
       loginTea:"required",
       passTea: {
           required: true,
           minlength: 6
       },
       confirmTea:{
           required: true,
           minlength: 6,
           equalTo:'#passTea'
       }

     },
     messages:{

       confirmTea:{
           required:"<?php echo L::Confirmcheck?>",
           minlength:"<?php echo L::Confirmincheck?>",
           equalTo: "<?php echo L::ConfirmSamecheck?>"
       },
       passTea: {
           required:"<?php echo L::Passcheck?>",
           minlength:"<?php echo L::Confirmincheck?>"
       },
       loginTea:"Merci de renseigner le Login"
     }
   });



 });
 </script>
    <!-- end js include path -->
  </body>

</html>
