<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$student=new Student();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement
$schoolsofassign=$etabs->getEtablissementbyCodeEtab($codeEtabAssigner);

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();

$codesEtab=$schoolsofassign;

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
//
if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
    $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
    $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}

$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$sections=$classe->getAllsectionofthisschool($codeEtabAssigner,$libellesessionencours);

$studentInfos=$student->getAllInformationsOfStudentOne($_GET['compte'],$libellesessionencours);
$tabStudent=array();

foreach ($studentInfos as $personnal):
  $tabStudent[0]= $personnal->id_compte;
  $tabStudent[1]=$personnal->matricule_eleve;
  $tabStudent[2]= $personnal->nom_eleve;
  $tabStudent[3]=$personnal->prenom_eleve;
  $tabStudent[4]= $personnal->datenais_eleve;
  $tabStudent[5]=$personnal->lieunais_eleve;
  $tabStudent[6]= $personnal->sexe_eleve;
  $tabStudent[7]=$personnal->email_eleve;
  $tabStudent[8]=$personnal->email_eleve;
  $tabStudent[9]= $personnal->libelle_classe;
  $tabStudent[10]=$personnal->codeEtab_classe;
  $tabStudent[11]= $personnal->photo_compte;
  $tabStudent[12]=$personnal->tel_compte;
  $tabStudent[13]= $personnal->login_compte;
  $tabStudent[14]=$personnal->codeEtab_inscrip;
  $tabStudent[15]= $personnal->id_classe;
  $tabStudent[16]=$personnal->allergie_eleve;
  $tabStudent[17]=$personnal->condphy_eleve;

endforeach;

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />


	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>



  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Bulletin</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="#">Resultat scolaire</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Bulletin</li>
                            </ol>
                        </div>
                    </div>

                    <?php

                          if(isset($_SESSION['user']['addclasseok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: 'Félicitations',
                      text: '<?php echo $_SESSION['user']['addclasseok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['addclasseok']);
                          }

                           ?>

                    <?php

                          if(isset($_SESSION['user']['updateclasseok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: 'Félicitations',
                      text: '<?php echo $_SESSION['user']['updateclasseok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['updateclasseok']);
                          }

                           ?>



                    <?php

                          if(isset($_SESSION['user']['delclasseok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: 'Félicitations',
                      text: '<?php echo $_SESSION['user']['delclasseok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['delclasseok']);
                          }

                           ?>
					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['updateteaok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: 'Félicitations',
            text: '<?php echo $_SESSION['user']['updateteaok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['updateteaok']);
                }

                 ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

                 <div class="row">
                 						<div class="col-md-12">
                 							<div class="white-box">

                 								<div class="row">
                 									<div class="col-md-12">
                 										<div class="pull-left">
                 											<address>
                                        <?php
                                        $imagelogo=$etabs->getEtabLogobyCodeEtab($codeEtabAssigner);
                                        $lien="../logo_etab/".$codeEtabAssigner."/".$imagelogo;

                                         ?>
                 												<img src="<?php echo $lien; ?>" alt="logo"
                 													class="logo-default" style="width:50%" />
                 												<!--p class="text-muted m-l-5">
                 													Aditya University, <br> Opp. Town Hall, <br>
                 													Sardar Patel Road, <br> Ahmedabad - 380015
                 												</p-->
                 											</address>

                                      <br><br>
                 										</div>



                 									</div>
                                  <div class="col-md-12">

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card card-topline-gray">
                                        <div class="card-head" style="text-align:center">
                                            <header>BULLETIN DE NOTES</header>

                                        </div>
                                        <div class="card-body ">
                                          <div class="row">
                                            <div class="col-md-6">
                                              <span><?php echo $tabStudent[2]." ".$tabStudent[3] ?></span>
                                            </div>
                                            <div class="col-md-6">
                                              <span class="pull-right"> Matricule : <?php echo $tabStudent[1] ?></span>
                                            </div>

                                          </div>
                                          <div class="row">
                                            <div class="col-md-4">
                                              <span>Classe : <?php echo $tabStudent[9] ?></span><br>
                                              <span>Née le : <?php echo date_format(date_create($tabStudent[4]),"d/m/Y");?> à <?php echo $tabStudent[5]; ?></span>
                                            </div>
                                            <div class="col-md-4">
                                              <span>Effectif : <?php echo $tabStudent[9] ?></span><br>
                                              <span>Rédoublant(e) : <?php echo $tabStudent[9] ?></span><br>
                                            </div>
                                            <div class="col-md-4 ">
                                              <span class="pull-right">Affecté(e) : <?php echo $tabStudent[9] ?></span><br>
                                              <span class="pull-right">Sexe : <?php echo $tabStudent[9] ?></span><br>
                                            </div>

                                          </div>
                                        </div>
                                    </div>
                                </div>


                                  </div>

                 									<div class="col-md-12">
                 										<div class="table-responsive m-t-40">
                 											<table class="table table-hover">
                 												<thead>
                 													<tr>
                 														<th class="text-left">MATIERES</th>
                 														<th class="text-center">MOY</th>
                 														<th class="text-center">COEF</th>
                                            <th class="text-center">MOYCOEF</th>
                 														<th class="text-center">RANG</th>
                 														<th class="text-center">PROFESSEURS</th>
                 														<th class="text-center">MENTION</th>
                 													</tr>
                 												</thead>
                 												<tbody>
                                          <?php
                                          $datas=$etabs->getAllsubjectofclassesbyIdclasses($_GET['classeid'],$codeEtabAssigner,$libellesessionencours);
                                          foreach ($datas as $value):
                                            $data=$etabs->getratingChildInMatiere($_GET['classeid'],$codeEtabAssigner,$libellesessionencours,$_GET['compte'],$_GET['semester'],$value->id_mat);
                                            $rating="";
                                            foreach ($data as $valuedata):
                                              $rating=$valuedata->rating;
                                              $ranking=$valuedata->raking_rating;
                                              $moycoef=$rating*$value->coef_mat;
                                              $mentionranking=$valuedata->mention_rating;
                                            endforeach;

                                            $datagles=$etabs->getratinggeneraleStudent($_GET['classeid'],$codeEtabAssigner,$libellesessionencours,$_GET['compte'],$_GET['semester']);
                                            foreach ($datagles as $valuegle):
                                              $totalcoefgle=$valuegle->totalcoef_glerating;
                                              $totalmoycoefgle=$valuegle->totalmoycoef_glerating;
                                              $rankinggle=$valuegle->ranking_glerating;
                                              $ratinggle=$valuegle->rating_glerating;
                                              $mentionratinggle=$valuegle->mention_glerating;
                                            endforeach;

                                           ?>
                 													<tr>
                 														<td class="text-left"><?php echo $value->libelle_mat; ?></td>
                 														<td class="text-center"><?php echo round($rating,2); ?></td>
                                            <td class="text-center"><?php echo $value->coef_mat ?></td>
                                            <td class="text-center"><?php echo round($moycoef,2); ?></td>
                 														<td class="text-center"><?php echo $ranking; ?></td>
                 														<td class="text-center"><?php echo $etabs->getUtilisateurName($value->teatcher_mat) ?></td>
                 														<td class="text-center"><?php echo $mentionranking ?></td>
                 													</tr>
                                          <?php
                                        endforeach;
                                           ?>
                                           <tfoot>
                                             <tr>
                                               <td class="text-left">BILAN</td>
                                               <td class="text-center"><?php echo round($ratinggle,2) ?></td>
                                               <td class="text-center"><?php echo $totalcoefgle ?></td>
                                               <td class="text-center"><?php echo round($totalmoycoefgle,2) ?></td>
                                               <td class="text-center"><?php echo $rankinggle ?></td>
                                               <td class="text-center"></td>
                                               <td class="text-center"></td>
                                             </tr>
                                           </tfoot>

                 												</tbody>
                 											</table>
                 										</div>
                 									</div>
                 									<div class="col-md-12">
                 										
                 										<div class="clearfix"></div>
                 										<hr>
                 										<div class="text-right">
                 											<button class="btn btn-danger" type="submit"> Proceed to payment </button>
                 											<button onclick="javascript:window.print();"
                 												class="btn btn-default btn-outline" type="button"> <span><i
                 														class="fa fa-print"></i> Print</span> </button>
                 										</div>
                 									</div>
                 								</div>
                 							</div>
                 						</div>
                 					</div>



                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>


   $("#section").select2();
   $("#libetab").select2();
   $("#classex").select2();

   function searchclassesection()
   {
     var etape=10;
     var codeEtab="<?php echo $codeEtabAssigner ?>";
     var session="<?php echo $libellesessionencours ?>";
      $.ajax({
        url: '../ajax/classe.php',
        type: 'POST',
        async:false,
        data: 'section='+ $("#section").val()+'&etape='+etape+'&session='+session+'&codeEtab='+codeEtab,
        dataType: 'text',
        success: function (content, statut) {

          $("#classex").html("");
          $("#classex").html(content);

        }
      });
   }

   function searchlibetab()
   {

     var etape=2;

     $.ajax({
              url: '../ajax/school.php',
              type: 'POST',
              async:false,
              data: 'code='+ $("#codeetab").val()+'&etape='+etape,
              dataType: 'text',
              success: function (content, statut) {

                $("#libetab").html("");
                $("#libetab").html(content);

                //nous allons chercher la liste des admins local de cet etablissement

                $.ajax({
                         url: '../ajax/classe.php',
                         type: 'POST',
                         async:false,
                         data: 'code='+ $("#codeetab").val()+'&etape='+etape,
                         dataType: 'text',
                         success: function (response, statut) {


                           $("#classex").html("");
                           $("#classex").html(response);
                         }
                       });

              }

            });

   }
   $(document).ready(function() {

     $('#example5').DataTable( {
         "scrollX": true

     } );



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
