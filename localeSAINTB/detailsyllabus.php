<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Teatcher.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Classe.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];

$user=new User();
$etabs=new Etab();
$admin= new Localadmin();
$classe=new Classe();
$localadmins= new Localadmin();
$parents=new ParentX();
$teatcher=new Teatcher();

$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

$admins=$admin->getAllAdminLocal();
// $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
if(strlen($_SESSION['user']['codeEtab'])>0)
{
  $codeEtabAssigner=$_SESSION['user']['codeEtab'];
}else {
  $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
}



if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$codesEtab=$etabs->getAllcodesEtab();
$codesEtab=$etabs->getAllcodesEtabBycodeEtab($codeEtabAssigner);
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$teatchers=$teatcher->getAllTeatchers();

$teatchers=$teatcher->getAllTeatchersBySchoolCode($codeEtabAssigner);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);

}
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);

$notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

// $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);
$allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

$syllabusInfos=$etabs->getSyllabusInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

$array=json_encode($syllabusInfos,true);
$someArray = json_decode($array, true);

$courseid=$_GET['programme'];
$classeid=$_GET['classeid'];

// $courses=$etabs->getAllTeatchercourses($compteuserid,$codeEtabsession,$libellesessionencours);
$coursesdetails=$etabs->getSyllabusInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);
// $coursesdetails=$etabs->getAllcoursesdetails($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);

// var_dump($coursesdetails);


foreach ($coursesdetails as  $datacourses):
  $descricourses=$datacourses->descri_syllab;

  $teatchercourses=$datacourses->nom_compte;
  $classecourses=$datacourses->libelle_classe;
  $classeidcourses=$datacourses->id_classe;
  $matiereidcourses=$datacourses->id_mat;
  $libellematcourses=$datacourses->libelle_mat;
  $teatcheridcourses=$datacourses->id_compte;


endforeach;


$nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
 <html lang="en">
 <!-- BEGIN HEAD -->

 <head>
     <meta charset="utf-8" />
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta content="width=device-width, initial-scale=1" name="viewport" />
     <title><?php echo L::Titlesite ?></title>
     <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
     <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

     <!-- google font -->
     <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
 	<!-- icons -->
     <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
 	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
 	<!--bootstrap -->
    <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
 	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <!-- data tables -->
    <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
     <!-- Material Design Lite CSS -->
 	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
 	<link href="../assets2/css/material_style.css" rel="stylesheet">
   <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
 	<!-- morris chart -->
     <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
     <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
     <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

 	<!-- Theme Styles -->
     <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
     <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
     <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
     <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
     <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

 	<!-- favicon -->
     <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
  </head>
  <!-- END HEAD -->
 <body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
     <div class="page-wrapper">
         <!-- start header -->
 		<?php
 include("header.php");
     ?>
         <!-- end header -->
         <!-- start page container -->
         <div class="page-container">
  			<!-- start sidebar menu -->
  			<?php
 				include("menu.php");
 			?>
 			 <!-- end sidebar menu -->
 			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title"><?php echo L::DetailsSylabus ?></div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" ><?php echo L::syllabusplanning ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active"><?php echo L::DetailsSylabus ?></li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addclasseok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['addclasseok'] ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addclasseok']);
                    }

                     ?>

                     <?php
                     if($nbsessionOn==0)
                     {
                       ?>
                       <div class="alert alert-danger alert-dismissible fade show" role="alert">

                       Vous devez definir la Sessionn scolaire

                       <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
                        </a>
                       </div>
                       <?php
                     }
                      ?>


                      <div class="row">
           <div class="col-md-12">
             <!-- BEGIN PROFILE SIDEBAR -->

             <div class="profile-sidebar">
               <div class="card card-topline-aqua">
                 <div class="card-body no-padding height-9">
                   <div class="row">
                     <div class="course-picture">
                       <img src="../photo/course2.jpg" class="img-responsive"
                         alt=""> </div>
                   </div>
                   <div class="profile-usertitle">
                     <div class="profile-usertitle-name"> <?php //echo utf8_decode(utf8_encode($namecourses));?> </div>
                   </div>
                   <!-- END SIDEBAR USER TITLE -->
                 </div>
               </div>
               <div class="card">
                 <div class="card-head card-topline-aqua">
                   <header></header>
                 </div>
                 <div class="card-body no-padding height-9">

                   <ul class="list-group list-group-unbordered">

                     <li class="list-group-item">
                       <b><i class="fa fa-university"></i> <?php echo L::ClasseMenu  ?> </b>
                       <div class="profile-desc-item pull-right"><?php echo utf8_decode(utf8_encode($classecourses)); ?></div>
                     </li>
                      <li class="list-group-item">
                        <b><i class="fa fa-user-circle-o"></i> <?php echo L::ProfsMenusingle  ?></b>
                        <div class="profile-desc-item pull-right"><?php echo utf8_decode(utf8_encode($teatchercourses)); ?>  </div>
                      </li>
                     <li class="list-group-item">
                       <b><i class="fa fa-graduation-cap"></i> <?php echo L::MatiereMenusingle; ?></b>
                       <div class=" pull-right"  ><?php echo utf8_decode(utf8_encode($libellematcourses)); ?>  </div>
                     </li>
                     <li class="list-group-item">
                       <b><i class="fa fa-calendar"></i> Date</b>
                       <div class="profile-desc-item pull-right">
                          <?php
                        //  $tabdate=explode("-",$datercourses);

                        //  echo $tabdate[2]." ".obtenirLibelleMois($tabdate[1])." ".$tabdate[0];
                          ?>
                        </div>
                     </li>
                   </ul>
                 <br>

                    <div class="col-md-12" style="text-align:center">
                      <a href="updatesyllabus.php?programmeid=<?php echo $_GET['programme'] ?>&classeid=<?php echo $classeidcourses; ?>&teatcherid=<?php echo $teatcheridcourses ?>" class="btn btn-md btn-primary" style="border-radius:5px;"> <i class="fa fa-pencil"></i> Modifier le syllabus </a>
                    </div>


                 </div>
               </div>
             </div>
             <!-- END BEGIN PROFILE SIDEBAR -->
             <!-- BEGIN PROFILE CONTENT -->
             <div class="profile-content">

                <div class="row">
                  <div class="col-md-12 col-lg-12">
    <div class="card card-topline-aqua">
      <div class="card-head">
        <header><?php echo mb_strtoupper(L::GeneralInfoTab)  ?></header>
        <div class="tools">
          <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
          <a class="t-collapse btn-color fa fa-chevron-down"
            href="javascript:;"></a>
          <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
        </div>
      </div>

      <div class="card-body ">
         <span class="label label-lg label-warning"  style="text-align:center;">  DESCRIPTION DU SYLLABUS </span>

        <hr>
        <!-- <div class="col-md-12"> -->
<div class="form-group">

<textarea class="form-control descri" rows="5" placeholder="Entrer la description du cours" disabled="disabled"><?php echo $descricourses; ?></textarea>

</div>
<span class="label label-lg label-warning"  style="text-align:center;">  OBJECTIFS DU SYLLABUS </span>

<hr>
<?php
  $objectifs=$etabs->getSyllabusObjectifsInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

 ?>
 <ul>
   <?php
   foreach ($objectifs as  $value):
     ?>
     <li><?php echo utf8_decode(utf8_encode($value->libelle_syllabob )); ?></li>
     <?php
   endforeach;
    ?>

 </ul>
 <span class="label label-lg label-warning"  style="text-align:center;">  CONTENU DU SYLLABUS </span>

 <hr>
 <?php
   $themes=$etabs->getSyllabusThemesInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

  ?>
  <ul>
    <?php
    foreach ($themes as  $value):
      ?>
      <li><?php echo utf8_decode(utf8_encode($value->libelle_syllabth)); ?></li>
      <?php
    endforeach;
     ?>

  </ul>
  <span class="label label-lg label-warning"  style="text-align:center;">  PREREQUIS NECESSAIRES DU SYLLABUS </span>

  <hr>
  <?php
    $Requis=$etabs->getSyllabusRequisInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

   ?>
   <ul>
     <?php
     foreach ($Requis as  $value):
       ?>
       <li><?php echo utf8_decode(utf8_encode($value->libelle_syllabreq)); ?></li>
       <?php
     endforeach;
      ?>

   </ul>
   <span class="label label-lg label-warning"  style="text-align:center;">  COMPETENCES VISEES DU SYLLABUS </span>

   <hr>
   <?php
      $Comp=$etabs->getSyllabusCompInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

    ?>
    <ul>
      <?php
      foreach ($Comp as  $value):
        ?>
        <li><?php echo utf8_decode(utf8_encode($value->libelle_syllabcomp)); ?></li>
        <?php
      endforeach;
       ?>

    </ul>

<!-- </div> -->
      </div>
    </div>
  </div>

  <div class="col-md-12 col-lg-12">
  <div class="card card-topline-aqua">
  <div class="card-head">
  <header>DOCUMENTS DU COURS</header>
  <div class="tools">
  <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
  <a class="t-collapse btn-color fa fa-chevron-down"
  href="javascript:;"></a>
  <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
  </div>
  </div>

  <div class="card-body ">

  <span class="label label-lg label-warning"  style="text-align:center;">DOCUMENTS OBLIGATOIRES </span>

  <hr>
  <?php
    $Docs=$etabs->getSyllabusDocInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

  ?>
  <ul>
  <?php
  foreach ($Docs as  $value):
  ?>
  <li><?php echo utf8_decode(utf8_encode($value->libelle_syllabdoc )); ?></li>
  <?php
  endforeach;
  ?>

  </ul>
  <span class="label label-lg label-warning"  style="text-align:center;">  DOCUMENTS FACULTATIFS </span>

  <hr>
  <?php
  $Docfacs=$etabs->getSyllabusDocfacInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

  ?>
  <ul>
  <?php
  foreach ($Docfacs as  $value):
  ?>
  <li><?php echo utf8_decode(utf8_encode($value->libelle_syllabdoc)); ?></li>
  <?php
  endforeach;
  ?>

  </ul>



  <!-- </div> -->
  </div>
  </div>
  </div>

  <div class="col-md-12 col-lg-12">
  <div class="card card-topline-aqua">
  <div class="card-head">
  <header>REGLES DE FONCTIONNEMENT DU COURS</header>
  <div class="tools">
  <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
  <a class="t-collapse btn-color fa fa-chevron-down"
  href="javascript:;"></a>
  <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
  </div>
  </div>

  <div class="card-body ">


  <?php
    $Regles=$etabs->getSyllabusRegleInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

  ?>
  <ul>
  <?php
  foreach ($Regles as  $value):
  ?>
  <li><?php echo utf8_decode(utf8_encode($value->libelle_syllabregle)); ?></li>
  <?php
  endforeach;
  ?>

  </ul>




  <!-- </div> -->
  </div>
  </div>
  </div>





                </div>


             </div>


              <div class="col-md-12 col-lg-12">
              <div class="card card-topline-aqua">
              <div class="card-head">
              <header>CALENDRIER DES COURS</header>
              <div class="tools">
              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
              <a class="t-collapse btn-color fa fa-chevron-down"
              href="javascript:;"></a>
              <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
              </div>
              </div>

              <div class="card-body ">


              <?php
               $Calendars=$etabs->getSyllabusCalendarInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

              ?>
              <table  class="table table-hover table-checkable order-column full-width" >
                 <thead>
                     <tr>

                          <th style="text-align:center;">Date</th>
                         <th style="text-align:center;">No Séance </th>
                          <th style="text-align:center;">Contenu pédagogique </th>
                          <th style="text-align:center;">Travail préalable </th>
                          <!--th> Actions </th-->
                     </tr>
                 </thead>
                 <tbody id="tabStationCalendar">
                   <?php foreach ($Calendars as $value): ?>
                  <tr>
                  <td style="text-align:center;"><?php echo $value->date_syllabcal?></td>
                  <td style="text-align:center;"><?php echo $value->seance_syllabcal ?></td>
                  <td style="text-align:center;"><?php echo $value->contenu_syllabcal?></td>
                  <td style="text-align:center;"><?php echo $value->prealable_syllabcal ?></td>

                  </tr>
                   <?php endforeach; ?>
                 </tbody>

             </table>




              <!-- </div> -->
              </div>
              </div>
              </div>

              <div class="col-md-12 col-lg-12">
              <div class="card card-topline-aqua">
              <div class="card-head">
              <header>MODE EVALUATION</header>
              <div class="tools">
              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
              <a class="t-collapse btn-color fa fa-chevron-down"
              href="javascript:;"></a>
              <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
              </div>
              </div>

              <div class="card-body ">


              <?php
                $Calendars=$etabs->getSyllabusModeEvalInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

              ?>
              <table class="table table-hover table-checkable order-column full-width">
                  <thead>
                      <tr>

                          <th style="text-align:center;width:200px">Date</th>
                          <th style="text-align:center;">Type d’évaluation </th>
                          <th style="text-align:center;">Compétences visées </th>
                          <th style="text-align:center;">Pondération (%)</th>
                          <!--th style="text-align:center;">Actions</th-->

                      </tr>
                  </thead>
                  <tbody id="tabStationEvaluation">
                    <?php foreach ($Calendars as $value): ?>
                   <tr>
                   <td style="text-align:center;"><?php echo $value->date_syllabeval?></td>
                   <td style="text-align:center;"><?php echo $value->type_syllabeval ?></td>
                   <td style="text-align:center;"><?php echo $value->competence_syllabeval?></td>
                   <td style="text-align:center;"><?php echo $value->ponderation_syllabeval ?></td>

                   </tr>
                    <?php endforeach; ?>
                  </tbody>

              </table>




              <!-- </div> -->
              </div>
              </div>
              </div>

             <!-- END PROFILE CONTENT -->
           </div>
         </div>
          </div>
      </div>
             </div>
             <!-- end page content -->
             <!-- start chat sidebar -->
             <div class="chat-sidebar-container" data-close-on-body-click="false">
             <div class="chat-sidebar">
               <ul class="nav nav-tabs">
                 <li class="nav-item">
                   <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                       class="material-icons">
                       chat</i>Chat
                     <!-- <span class="badge badge-danger">4</span> -->
                   </a>
                 </li>
               </ul>
               <div class="tab-content">
                 <!-- Start User Chat -->
                 <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                   id="quick_sidebar_tab_1"> -->
                   <div class="chat-sidebar-chat "
                     >
                   <div class="chat-sidebar-list">
                     <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                       data-wrapper-class="chat-sidebar-list">
                       <div class="chat-header">
                         <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                       </div>
                       <ul class="media-list list-items">
                         <?php

                         foreach ($onlineUsers as  $valueUsers):
                           $tofuser=$valueUsers->photo_compte;
                           if(strlen($tofuser)>0)
                           {
                             $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                           }else {
                             $lientofuser="../photo/user5.jpg";
                           }
                           if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                           {
                             ?>
                             <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                 width="35" height="35" alt="...">
                               <i class="online dot red"></i>
                               <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                 <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                 <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                               </div>
                             </li>
                             <?php
                           }
                           ?>

                           <?php
                         endforeach;
                          ?>

                       </ul>
                       <div class="chat-header">
                         <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                       </div>
                       <ul class="media-list list-items">
                         <?php

                         foreach ($offlineUsers as  $valueUsers):
                           $tofuser=$valueUsers->photo_compte;
                           if(strlen($tofuser)>0)
                           {
                             $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                           }else {
                             $lientofuser="../photo/user5.jpg";
                           }
                           ?>
                           <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                               width="35" height="35" alt="...">
                             <i class="offline dot"></i>
                             <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                               <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                               <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                             </div>
                           </li>
                           <?php
                         endforeach;
                          ?>
                       </ul>
                     </div>
                   </div>
                 </div>
                 <!-- End User Chat -->
               </div>
             </div>
             </div>
             <!-- end chat sidebar -->
         </div>
         <!-- end page container -->
         <!-- start footer -->
         <div class="page-footer">
             <div class="page-footer-inner"> 2019 &copy;
             <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
             </div>
             <div class="scroll-to-top">
                 <i class="material-icons">eject</i>
             </div>
         </div>
         <!-- end footer -->
     </div>
     <!-- start js include path -->
     <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
    	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
    	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
        <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
     <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
     <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
     <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
     <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
     <script src="../assets2/js/pages/table/table_data.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <!--script src="../assets2/plugins/summernote/summernote.js" ></script>
     <script src="../assets2/plugins/summernote/lang/summernote-fr-FR.js" ></script-->
        <!-- calendar -->
        <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
        <!-- Common js-->
    	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
    	<script src="../assets2/js/theme-color.js" ></script>
    	<!-- Material -->
    	<script src="../assets2/plugins/material/material.min.js"></script>
     <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
     <script src="../assets2/js/dropify.js"></script>
     <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>

  <script>
  /*$('#summernote').summernote({
      placeholder: '',
      tabsize: 2,
      height: 200,
       lang: 'fr-FR'
    });*/

    function addFrench()
    {
      var etape=1;
      var lang="fr";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

    function addEnglish()
    {
      var etape=1;
      var lang="en";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

 function publicationcourse(courseid,classeid,teatcherid,codeEtab,sessionEtab,matiereid)
 {
   Swal.fire({
 title: '<?php echo L::WarningLib ?>',
 text: "<?php echo L::DoyouPublishcourse ?>",
 type: 'warning',
 showCancelButton: true,
 confirmButtonColor: '#3085d6',
 cancelButtonColor: '#d33',
 confirmButtonText: '<?php echo L::Published ?>',
 cancelButtonText: '<?php echo L::AnnulerBtn ?>',
 }).then((result) => {
 if (result.value) {

   var etape=1;

   $.ajax({

        url: '../ajax/courses.php',
        type: 'POST',
        async:true,
        data: 'teatcherid=' + teatcherid+ '&etape=' + etape+'&classe='+classeid+'&matiere='+matiereid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&courseid='+courseid,
        dataType: 'text',
        success: function (content, statut) {

           // $("#FormAddAcademique #codeEtab").val(content);

           location.reload();

        }
      });

 }else {

 }
 })
 }

  $("#fichier3").dropify({
    messages: {
        "default": "Merci de selectionner le support",
        "replace": "Modifier le support",
        "remove" : "Supprimer le support",
        "error"  : "Erreur"
    }
  });

 $("#matclasse").select2();
 $("#classeEtab").select2();

 function searchcodeEtab(id)
 {
 var classe=$("#classeEtab").val();
 var teatcherId=id;
 var etape=7;
 var matiere=$("#matclasse").val();

 $.ajax({

      url: '../ajax/matiere.php',
      type: 'POST',
      async:true,
      data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
      dataType: 'text',
      success: function (content, statut) {

         $("#FormAddAcademique #codeEtab").val(content);

      }
    });

 }

 function searchmatiere(id)
 {

   var classe=$("#classeEtab").val();
   var teatcherId=id;
   var etape=6;


 $.ajax({

      url: '../ajax/matiere.php',
      type: 'POST',
      async:true,
      data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe,
      dataType: 'text',
      success: function (content, statut) {


        $("#matclasse").html("");
        $("#matclasse").html(content);

      }
    });
 }

 function deletedTache(id)
 {
 var concattache=$("#concattache").val();

 $("#concattache").val($("#concattache").val().replace(id+"@", ""));

  $('#rowTache'+id+'').remove();

  recalcultachenb();
 }

 function deletedComp(id)
 {
 var concatcomp=$("#concatcomp").val();

 $("#concatcomp").val($("#concatcomp").val().replace(id+"@", ""));

  $('#rowComp'+id+'').remove();

  // recalculsectionnb();

  var concatcomp=$("#concatcomp").val();

  var tab=concatcomp.split("@");

  var nbtab=tab.length;

  var nbtabnew=parseInt(nbtab)-1;

  $("#concatnbcomp").val(nbtabnew);

 }

 function deletedTaches(id)
 {
 var concattaches=$("#concattaches").val();

 $("#concattaches").val($("#concattaches").val().replace(id+"@", ""));

  $('#rowTaches'+id+'').remove();

  // recalculsectionnb();

  var concattaches=$("#concattaches").val();

  var tab=concattaches.split("@");

  var nbtab=tab.length;

  var nbtabnew=parseInt(nbtab)-1;

  $("#concatnbtaches").val(nbtabnew);

 }


 function deletedSection(id)
 {
 var concatsection=$("#concatsection").val();

 $("#concatsection").val($("#concatsection").val().replace(id+"@", ""));

  $('#rowSection'+id+'').remove();

  // recalculsectionnb();

  var concatsection=$("#concatsection").val();

  var tab=concatsection.split("@");

  var nbtab=tab.length;

  var nbtabnew=parseInt(nbtab)-1;

  $("#concatnbsection").val(nbtabnew);

 }

 function recalcultachenb()
 {
 var concattache=$("#concattache").val();

 var tab=concattache.split("@");

 var nbtab=tab.length;

 var nbtabnew=parseInt(nbtab)-1;

 $("#concatnbtache").val(nbtabnew);
 }

 function recalculsectionnb()
 {

 }


 function AddtachesRow()
 {
 var nb=$("#nbtaches").val();
 var nouveau= parseInt(nb)+1;
 $("#nbtaches").val(nouveau);

   var concattaches=$("#concattaches").val();
   $("#concattaches").val(concattaches+nouveau+"@");

   var concattaches=$("#concattaches").val();

   var tab=concattaches.split("@");

   var nbtab=tab.length;

   var nbtabnew=parseInt(nbtab)-1;

   $("#concatnbtaches").val(nbtabnew);

   // recalculsectionnb();

   if(nbtabnew==1)
   {
     $('#dynamic_field2').append('<tr id="rowTaches'+nouveau+'"><td><input type="text" name="taches_'+nouveau+'" id="taches_'+nouveau+'" placeholder="<?php echo L::EnterNewExercice ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteTaches'+nouveau+'" id="deleteTaches'+nouveau+'"  onclick="deletedTaches('+nouveau+')"  class="btn btn-danger btn_remove" disabled="disabled">X</button></td></tr>');
   }else {
     $('#dynamic_field2').append('<tr id="rowTaches'+nouveau+'"><td><input type="text" name="taches_'+nouveau+'" id="taches_'+nouveau+'" placeholder="<?php echo L::EnterNewExercice ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteTaches'+nouveau+'" id="deleteTaches'+nouveau+'"  onclick="deletedTaches('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');
   }



   for(var i=0;i<nbtabnew;i++)
   {
     var indice=tab[i];
     // alert(indice);
     $("#taches_"+indice).rules( "add", {
         required: true,
         messages: {
         required: "<?php echo L::AddNewexerciceplease ?>"
 }
       });
   }


 }

 function AddcompRow()
 {
 var nb=$("#nbcomp").val();
 var nouveau= parseInt(nb)+1;
 $("#nbcomp").val(nouveau);

   var concatcomp=$("#concatcomp").val();
   $("#concatcomp").val(concatcomp+nouveau+"@");

   var concatcomp=$("#concatcomp").val();

   var tab=concatcomp.split("@");

   var nbtab=tab.length;

   var nbtabnew=parseInt(nbtab)-1;

   $("#concatnbcomp").val(nbtabnew);

   // recalculsectionnb();

   if(nbtabnew==1)
   {
     $('#dynamic_field1').append('<tr id="rowComp'+nouveau+'"><td><input type="text" name="comp_'+nouveau+'" id="comp_'+nouveau+'" placeholder="<?php echo L::AddCompViseeRequired ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteComp'+nouveau+'" id="deleteComp'+nouveau+'"  onclick="deletedComp('+nouveau+')"  class="btn btn-danger btn_remove" disabled="disabled">X</button></td></tr>');
   }else {
     $('#dynamic_field1').append('<tr id="rowComp'+nouveau+'"><td><input type="text" name="comp_'+nouveau+'" id="comp_'+nouveau+'" placeholder="<?php echo L::AddCompViseeRequired ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteComp'+nouveau+'" id="deleteComp'+nouveau+'"  onclick="deletedComp('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');
   }



   for(var i=0;i<nbtabnew;i++)
   {
     var indice=tab[i];
     // alert(indice);
     $("#comp_"+indice).rules( "add", {
         required: true,
         messages: {
         required: "<?php echo L::AddCompetenceViseeplease ?>"
 }
       });
   }


 }

 function AddsectionRow()
 {
 var nb=$("#nb").val();
 var nouveau= parseInt(nb)+1;
 $("#nb").val(nouveau);

   var concatsection=$("#concatsection").val();
   $("#concatsection").val(concatsection+nouveau+"@");

   var concatsection=$("#concatsection").val();

   var tab=concatsection.split("@");

   var nbtab=tab.length;

   var nbtabnew=parseInt(nbtab)-1;

   $("#concatnbsection").val(nbtabnew);

   // recalculsectionnb();

   if(nbtabnew==1)
   {
     $('#dynamic_field').append('<tr id="rowSection'+nouveau+'"><td><input type="text" name="section_'+nouveau+'" id="section_'+nouveau+'" placeholder="<?php echo L::EnterSection ?>" class="form-control objectif_list" /></td><td><button type="button" id="deleteSection'+nouveau+'" id="deleteSection'+nouveau+'"  onclick="deletedSection('+nouveau+')"  class="btn btn-danger btn_remove" disabled="disabled">X</button></td></tr>');
   }else {
     $('#dynamic_field').append('<tr id="rowSection'+nouveau+'"><td><input type="text" name="section_'+nouveau+'" id="section_'+nouveau+'" placeholder="<?php echo L::EnterSection ?>" class="form-control objectif_list" /></td><td><button type="button" id="deleteSection'+nouveau+'" id="deleteSection'+nouveau+'"  onclick="deletedSection('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');
   }



   for(var i=0;i<nbtabnew;i++)
   {
     var indice=tab[i];
     // alert(indice);
     $("#section_"+indice).rules( "add", {
         required: true,
         messages: {
         required: "<?php echo L::AddSectionplease ?>"
 }
       });
   }


 }

 function AddtacheRow()
 {
 var nb=$("#nbtache").val();
 var nouveau= parseInt(nb)+1;
 $("#nbtache").val(nouveau);

   var concattache=$("#concattache").val();
   $("#concattache").val(concattache+nouveau+"@");

   recalcultachenb();

   $('#dynamic_field1').append('<tr id="rowTache'+nouveau+'"><td><input type="text" name="tache_'+nouveau+'" id="tache_'+nouveau+'" placeholder="Entrer une tache" class="form-control objectif_list" /></td><td><button type="button" id="deleteTache'+nouveau+'" id="deleteTache'+nouveau+'"  onclick="deletedTache('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');

   for(var i=1;i<=nouveau;i++)
   {
     $("#tache_"+i).rules( "add", {
         required: true,
         messages: {
         required: "<?php echo L::RequiredChamp ?>"
 }
       });
   }


 }

  $(document).ready(function() {

    $("#FormAddAcademique").validate({

      errorPlacement: function(label, element) {
      label.addClass('mt-2 text-danger');
      label.insertAfter(element);
     },
     highlight: function(element, errorClass) {
      $(element).parent().addClass('has-danger')
      $(element).addClass('form-control-danger')
     },
     success: function (e) {
          $(e).closest('.control-group').removeClass('error').addClass('info');
          $(e).remove();
      },
      rules:{

        programme:"required",
        descri:"required",
        classeEtab:"required",
        matclasse:"required",
        fichier:"required",
        durationcourse:"required",
        detailscourse:"required",
        datecourse:"required"

      },
    messages: {
      programme:"Merci de renseigner le libellé du programme",
      descri:"Merci de renseigner le détails de la fiche ",
      classeEtab:"<?php echo L::PleaseSelectclasseOnerequired ?>",
      matclasse:"Merci de <?php echo L::SelectSubjects ?>",
      fichier:"Merci de selectionner le fichier du programme",
      durationcourse:"<?php echo L::DurationcourseRequired ?>",
      detailscourse:"<?php echo L::DetailscourseRequired ?>",
      datecourse:"<?php echo L::DatecourseRequired ?>"
    },
    submitHandler: function(form) {

    form.submit();



    }

    });

    // AddsectionRow();
    //  AddcompRow();

    $('#add').click(function(){

      //creation d'une ligne de section

      AddsectionRow();

    });

    $('#addcomp').click(function(){

      //creation d'une ligne de section

      AddcompRow();

    });

    $('#addtache').click(function(){

      //creation d'une ligne de section

      AddtachesRow();

    });






  });

  </script>
     <!-- end js include path -->
   </body>

 </html>
