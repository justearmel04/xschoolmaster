<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Teatcher.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../controller/functions.php');
require_once('../class/ChiffresEnLettres.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$codeEtab=$_GET['codeEtab'] ;
$idClasse=$_GET['classeEtab'];
$session=$_GET['sessionEtab'];

$etabs=new Etab();
$student=new Student();
$classe=new Classe();
$lettre=new ChiffreEnLettre();

$libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
$libelleclasse=$classe->getInfosofclassesbyId($idClasse,$session);
$logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
$etablissementType=$etabs->DetermineTypeEtab($codeEtab);


$students=$student->getAllStudentOfClassesId($idClasse,$session);

//nous allons faire nos différents calculs

//le nombre d'eleve de cette classe


$nbclassestudents=$classe->getAllStudentOfThisClassesNb($idClasse,$session);

//le nombre de fille affecté

$nbaffectefilles=$classe->getAllStudentFilleAffectOfThisClassesNb($idClasse,$session);

//nombre de fille non Affecté

$nbnonaffectefilles=$classe->getAllStudentFilleNAffectOfThisClassesNb($idClasse,$session);

//nombre de mec affectés

$nbaffectemecs=$classe->getAllStudentMecAffectOfThisClassesNb($idClasse,$session);

//nombre de mec non affectés

$nbnonaffectemecs=$classe->getAllStudentMecNAffectOfThisClassesNb($idClasse,$session);

//nombre de fille redoublante

$nbredoubfilles=$classe->getAllStudentFilleRedtOfThisClassesNb($idClasse,$session);

//nombre non redoublante fille

$nbnredoubfilles=$classe->getAllStudentFilleNRedtOfThisClassesNb($idClasse,$session);

//nombre de mec redoublant

$nbredoubmecs=$classe->getAllStudentMecRedtOfThisClassesNb($idClasse,$session);

//nombre non redoublant mec

$nbnredoubmecs=$classe->getAllStudentMecNRedtOfThisClassesNb($idClasse,$session);

$montantpaiebaseclasses=$student->getmontantpaiebaseclasse($idClasse,$session);

foreach ($montantpaiebaseclasses as  $amountvalue) :

$montantinscriptionsclasses=$amountvalue->inscriptionmont_classe;
// $montantreinscriptionsclasses=$amountvalue->
$montantscolaritesclasses=$amountvalue->scolarite_classe;

endforeach;


 $versementStudent=$student->getStudentversementInfosNew($codeEtab,$session,$_GET['compte'],$_GET['versementid']);

 foreach ($versementStudent as  $valueVersement):
   $deposant=$valueVersement->deposant_versement;
   $motif=$valueVersement->motif_versement;
   $mode=$valueVersement->mode_versement;
   $montant=$valueVersement->montant_versement;
   $solde=$valueVersement->solde_versement;
   $code=$valueVersement->code_versement;
   $dateversement=$valueVersement->date_versement;
   $beneficiaire=$valueVersement->nom_eleve." ".$valueVersement->prenom_eleve;
   $beneficiairematri=$valueVersement->matricule_eleve;
   $beneficiaireclasse=$valueVersement->libelle_classe;
   $scolaritesmontant=$valueVersement->scolarite_classe;
   $scolaritesnet=$montant+$solde;
   $motifid=$valueVersement->motifid_versement;
   $libelleactivities="";

   if($motif=="AES")
   {
     $datasactivities=$etabs->getActivitiesDetails($motifid,$codeEtab,$session);
     foreach ($datasactivities as $valueactivities):
       $libelleactivities=$valueactivities->libelle_act;
     endforeach;
   }

 endforeach;



 $libellemode="";

 if($mode==1)
 {
  $libellemode="CHEQUES";
 }else if($mode==2)
 {
    $libellemode="ESPECES";

 }else if($mode==3)
 {
   $libellemode="TPE";
 }else if($mode==4)
 {
   $libellemode="Virement Bancaire";
 }


require('fpdf/fpdf.php');

class PDF extends FPDF
{



function Footer()
{
    // Positionnement à 1,5 cm du bas
    $this->SetY(-15);
    // Police Arial italique 8
    $this->SetFont('Arial','I',8);
    // Numéro et nombre de pages
    // $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    // $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
}

// Tableau simple
function BasicTable($header, $data)
{
    // En-tête
    foreach($header as $col)
        $this->Cell(40,7,$col,1);
    $this->Ln();
    // Données
    foreach($data as $row)
    {
        foreach($row as $col)
            $this->Cell(40,6,$col,1);
        $this->Ln();
    }
}
}

$pdf = new PDF('L','mm',array(150,200));


$pdf->AddPage('L', 'A5');
$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(true, 10);
$pdf->SetFont('Arial', '', 12);
$pdf->SetTopMargin(10);
$pdf->SetLeftMargin(10);
$pdf->SetRightMargin(10);

$pdf->Ln(17);
$pdf -> SetX(2);
$pdf->Image("../logo_etab/".$codeEtab."/".$logoEtab,12,4,35,35);


/* --- Cell --- */
$pdf->SetTextColor(65,83,162);
$pdf->SetXY(54, 16);
$pdf->SetFont('Times', 'B', 14);
$pdf->Cell(142, 13, 'INTERNATIONAL BILINGUAL SCHOOLS OF AFRICA', 'TB', 1, 'C', false);
$pdf->SetTextColor(0);
/* --- Cell --- */
$pdf->SetXY(151, 32);
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(47, 10, "DATE : ".date_format(date_create($dateversement),"d/m/Y"), 0, 1, 'L', false);



// $pdf->SetMargins(3,0);
// $pdf->SetFont('Times','B',  16);

// $pdf -> SetX(5);
// $pdf-> SetFont('Times','B',14);
// $pdf->Cell(125,-24,"INTERNATIONAL BILINGUAL",0,0,'C');
// $pdf-> SetFont('Times','B',12);
// $pdf->Cell(75,-24,"ANNEE SCOLAIRE : ".$session,0,0,'C');
// $pdf->Ln(6);
// $pdf-> SetFont('Times','B',14);
// $pdf->Cell(125,-24,"SCHOOLS OF AFRICA",0,0,'C');
// $pdf-> SetFont('Times','B',12);
// $pdf->Cell(75,-24,"DATE : ".date_format(date_create($dateversement),"d/m/Y"),0,0,'C');
// $pdf->Ln(6);
// $pdf-> SetFont('Times','B',14);
// $pdf->Cell(125,-24,"RIVIERA",0,0,'C');
// $pdf->SetFont('Times','B',  15);
// $pdf->Cell(75,-24,"B.P.F :".$montant." #",0,0,'C');
// $pdf->Ln(3);
// $pdf -> SetX(5);
// $pdf-> SetFont('Times','B',  14);
// $pdf->Cell(176,5,"Matricule : ".$beneficiairematri,0,0,'C');
// $pdf->Ln(6);
// $pdf->Cell(165,5,utf8_decode("N° Famille :"),0,0,'C');
// $pdf->Ln(10);
// $pdf -> SetX(5);
// $pdf-> SetFont('Times','B', 16);
if($motif=="SCOLARITES")
{
 /* $pdf -> SetX(38);
  $pdf->Cell(80,5,utf8_decode("REÇU - INSCRIPTION"),0,0,'C');*/
  /* --- Cell --- */
  $pdf->SetXY(61, 39);
  $pdf->SetFontSize(20);
  $pdf->SetTextColor(78,183,72);
  $pdf->Cell(89, 12, utf8_decode("REÇU - SCOLARITE") , 0, 1, 'C', false);
  /* --- Cell --- */
  $pdf->SetXY(95, 50);
  $pdf->SetFontSize(14);
  $pdf->SetTextColor(0,0,0);
  $pdf->Cell(40, 9, number_format($solde,0,',',' ') .' '.'FCFA', 1, 1, 'C', false);
  /* --- Cell --- */
  $pdf->SetXY(73, 52);
  $pdf->SetFont('Arial', 'B', 14);
  $pdf->Cell(0, 5, 'Montant:', 0, 1, 'L', false);
  $pdf->SetXY(141, 45);
   $pdf->SetFontSize(16);
  $pdf->SetTextColor(255,0,0);
  $tabcode=explode("VER",$code);
  $versementcode=utf8_decode("N° ".$tabcode[1]);
  $pdf->Cell(0, 0,$versementcode,0,0,'C');
  $pdf->SetXY(61, 85);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("12");
  $pdf-> SetFont('Arial','B', 14);
  $pdf->Cell(40,-39,utf8_decode('NOM ET PRENOMS '),0,0,'C');
  $pdf-> SetFont('','B', 12);
  $tabbeneficiaire=explode(" ",$beneficiaire);
  $nbtabbenef=count($tabbeneficiaire);

  if(strlen($beneficiaire)<=30)
   {
    $pdf -> SetX("60");
     $pdf-> SetFont('Arial','', 14);
     $pdf->Cell(93,-39,":"." ".utf8_decode(ucwords(strtolower($beneficiaire))),0,0,'L');
   }else {
     $pdf-> SetFont('Arial','I', 14);
     $case=$tabbeneficiaire[0]." ".$tabbeneficiaire[1];
     if(strlen($case)>30)
     {
       $case = substr($beneficiaire, 0, 30);
       $pdf->Cell(93,5,utf8_decode($case),0,0,'L');
     }else {
       $pdf->Cell(93,5,utf8_decode($case),0,0,'L');
     }
   }
  //  $pdf-> SetFont('Arial','B', 14);
  // $pdf->Cell(13,5,utf8_decode('Classe '),0,0,'C');
  // // if(strlen($beneficiaireclasse)>15)
  // {
  //   $tabclassebenef=explode('/',$beneficiaireclasse);
  //   $nbclassebenef=count($tabclassebenef);
  //   if($nbclassebenef>0)
  //   {
  //     $pdf-> SetFont('Times','I', 14);
  //     $pdf->Cell(47,5,utf8_decode($tabclassebenef[0]),0,0,'L');
  //   }else {
  //     $caseclasse= substr($beneficiaireclasse, 0, 15);
  //     $pdf-> SetFont('Times','I', 14);
  //     $pdf->Cell(47,5,utf8_decode($caseclasse),0,0,'L');
  //   }

  // }else {
  //   $pdf-> SetFont('Times','I', 14);
  //   $pdf->Cell(47,5,utf8_decode($beneficiaireclasse),0,0,'L');
  // }
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("13");
  $pdf-> SetFont('Arial','B', 14);
  $pdf->Cell(40,-37,utf8_decode('MONTANT(en lettres)'),0,0,'C');
  $pdf-> SetFont('Arial','', 14);
  $pdf -> SetX("60");
  $pdf->Cell(10,-37,":"." ".utf8_decode(ucwords($lettre->Conversion($solde)).' Francs CFA'),0,0,'L');
  // $pdf-> SetFont('Times','B', 14);
  // $pdf->Cell(40,5,utf8_decode('Réduction '),0,0,'L');
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("8");
  $pdf-> SetFont('Arial','B', 14);
  $pdf->Cell(48,-35,utf8_decode('AVANCE'),0,0,'L');
  $pdf -> SetX("60");
  $pdf-> SetFont('Arial','', 14);
  $montantnette=$solde+$montant;
  $resteapayer=$montant-$solde;
  $pdf->Cell(145,-35,":"." ".utf8_decode(ucwords($lettre->Conversion($solde)).'Francs CFA'),0,0,'L');
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("8");
  $pdf-> SetFont('Arial','B', 14);
  $pdf->Cell(48,-33,utf8_decode('RESTE A PAYER'),0,0,'L');
  $pdf -> SetX("60");
  $pdf-> SetFont('Arial','', 14);
  $montantnette=$solde+$montant;
  $pdf->Cell(145,-33,":"." ".utf8_decode(ucwords($lettre->Conversion($resteapayer)).'Francs CFA'),0,0,'L');
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("8");
  $pdf-> SetFont('Arial','B', 14);
  $pdf->Cell(24,-31,utf8_decode('VERSEMENT'),0,0,'L');
  $pdf-> SetFont('Arial','', 14);
  $pdf -> SetX("60");
  $pdf->Cell(0,-31,":"." ".ucfirst(utf8_decode($libellemode)) ,0,0,'L');
 /* $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(42,5,utf8_decode('Mode de règlement '),0,0,'L');
  $pdf-> SetFont('Times','I', 12);
  $pdf->Cell(38,5,utf8_decode($libellemode),0,0,'L');*/

  $pdf->Ln(10);


}else if($motif=="TRANSPORTS")
{

/* $pdf -> SetX(38);
  $pdf->Cell(80,5,utf8_decode("REÇU - INSCRIPTION"),0,0,'C');*/
  /* --- Cell --- */
  $pdf->SetXY(61, 39);
  $pdf->SetFontSize(20);
  $pdf->SetTextColor(78,183,72);
  $pdf->Cell(89, 12, utf8_decode("REÇU - TRANSPORT") , 0, 1, 'C', false);
  /* --- Cell --- */
  $pdf->SetXY(95, 50);
  $pdf->SetFontSize(14);
  $pdf->SetTextColor(0,0,0);
  $pdf->Cell(40, 9, number_format($montant,0,',',' ') .' '.'FCFA', 1, 1, 'C', false);
  /* --- Cell --- */
  $pdf->SetXY(73, 52);
  $pdf->SetFont('Arial', 'B', 14);
  $pdf->Cell(0, 5, 'Montant:', 0, 1, 'L', false);
  $pdf->SetXY(141, 45);
   $pdf->SetFontSize(16);
  $pdf->SetTextColor(255,0,0);
  $tabcode=explode("VER",$code);
  $versementcode=utf8_decode("N° ".$tabcode[1]);
  $pdf->Cell(0, 0,$versementcode,0,0,'C');
  $pdf->SetXY(61, 85);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("12");
  $pdf-> SetFont('Arial','B', 14);
  $pdf->Cell(40,-27,utf8_decode('NOM ET PRENOMS '),0,0,'C');
  $pdf-> SetFont('','B', 12);
  $tabbeneficiaire=explode(" ",$beneficiaire);
  $nbtabbenef=count($tabbeneficiaire);

  if(strlen($beneficiaire)<=30)
   {
    $pdf -> SetX("60");
     $pdf-> SetFont('Arial','', 14);
     $pdf->Cell(93,-27,":"." ".utf8_decode(ucwords(strtolower($beneficiaire))),0,0,'L');
   }else {
     $pdf-> SetFont('Arial','I', 14);
     $case=$tabbeneficiaire[0]." ".$tabbeneficiaire[1];
     if(strlen($case)>30)
     {
       $case = substr($beneficiaire, 0, 30);
       $pdf->Cell(93,-27,utf8_decode($case),0,0,'L');
     }else {
       $pdf->Cell(93,-27,utf8_decode($case),0,0,'L');
     }
   }
  //  $pdf-> SetFont('Arial','B', 14);
  // $pdf->Cell(13,5,utf8_decode('Classe '),0,0,'C');
  // // if(strlen($beneficiaireclasse)>15)
  // {
  //   $tabclassebenef=explode('/',$beneficiaireclasse);
  //   $nbclassebenef=count($tabclassebenef);
  //   if($nbclassebenef>0)
  //   {
  //     $pdf-> SetFont('Times','I', 14);
  //     $pdf->Cell(47,5,utf8_decode($tabclassebenef[0]),0,0,'L');
  //   }else {
  //     $caseclasse= substr($beneficiaireclasse, 0, 15);
  //     $pdf-> SetFont('Times','I', 14);
  //     $pdf->Cell(47,5,utf8_decode($caseclasse),0,0,'L');
  //   }

  // }else {
  //   $pdf-> SetFont('Times','I', 14);
  //   $pdf->Cell(47,5,utf8_decode($beneficiaireclasse),0,0,'L');
  // }
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("13");
  $pdf-> SetFont('Arial','B', 14);
  $pdf->Cell(40,-20,utf8_decode('MONTANT(en lettres)'),0,0,'C');
  $pdf-> SetFont('Arial','', 14);
  $pdf -> SetX("60");
  $pdf->Cell(10,-20,":"." ".utf8_decode(ucwords($lettre->Conversion($montant)).' Francs CFA'),0,0,'L');
  // $pdf-> SetFont('Times','B', 14);
  // $pdf->Cell(40,5,utf8_decode('Réduction '),0,0,'L');
  $pdf->Ln(10);
  // $pdf->SetTextColor(10,5,8);
  // $pdf -> SetX("8");
  // $pdf-> SetFont('Arial','B', 14);
  // $pdf->Cell(48,-35,utf8_decode('AVANCE'),0,0,'L');
  // $pdf -> SetX("60");
  // $pdf-> SetFont('Arial','', 14);
  // $montantnette=$solde+$montant;
  // $resteapayer=$montant-$solde;
  // $pdf->Cell(145,-35,":"." ".utf8_decode(ucwords($lettre->Conversion($solde)).'Francs CFA'),0,0,'L');
  // $pdf->Ln(10);
  // $pdf->SetTextColor(10,5,8);
  // $pdf -> SetX("8");
  // $pdf-> SetFont('Arial','B', 14);
  // $pdf->Cell(48,-33,utf8_decode('RESTE A PAYER'),0,0,'L');
  // $pdf -> SetX("60");
  // $pdf-> SetFont('Arial','', 14);
  // $montantnette=$solde+$montant;
  // $pdf->Cell(145,-33,":"." ".utf8_decode(ucwords($lettre->Conversion($resteapayer)).'Francs CFA'),0,0,'L');
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("8");
  $pdf-> SetFont('Arial','B', 14);
  $pdf->Cell(24,-35,utf8_decode('VERSEMENT'),0,0,'L');
  $pdf-> SetFont('Arial','', 14);
  $pdf -> SetX("60");
  $pdf->Cell(0,-35,":"." ".ucfirst(utf8_decode($libellemode)) ,0,0,'L');
 /* $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(42,5,utf8_decode('Mode de règlement '),0,0,'L');
  $pdf-> SetFont('Times','I', 12);
  $pdf->Cell(38,5,utf8_decode($libellemode),0,0,'L');*/

  $pdf->Ln(10);
}else if($motif=="INSCRIPTIONS")
{
 /* $pdf -> SetX(38);
  $pdf->Cell(80,5,utf8_decode("REÇU - INSCRIPTION"),0,0,'C');*/
  /* --- Cell --- */
  $pdf->SetXY(61, 39);
  $pdf->SetFontSize(20);
  $pdf->SetTextColor(78,183,72);
  $pdf->Cell(89, 12, utf8_decode("REÇU - INSCRIPTION") , 0, 1, 'L', false);
  /* --- Cell --- */
  $pdf->SetXY(95, 50);
  $pdf->SetFontSize(14);
  $pdf->SetTextColor(0,0,0);
  $pdf->Cell(40, 9, number_format($solde,0,',',' ') .' '.'FCFA', 1, 1, 'C', false);
  /* --- Cell --- */
  $pdf->SetXY(73, 52);
  $pdf->SetFont('Arial', 'B', 14);
  $pdf->Cell(0, 5, 'Montant:', 0, 1, 'L', false);
  $pdf->SetXY(141, 45);
   $pdf->SetFontSize(16);
  $pdf->SetTextColor(255,0,0);
  $tabcode=explode("VER",$code);
  $versementcode=utf8_decode("N° ".$tabcode[1]);
  $pdf->Cell(0, 0,$versementcode,0,0,'C');
  $pdf->SetXY(61, 85);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("12");
  $pdf-> SetFont('Arial','B', 14);
  $pdf->Cell(40,-39,utf8_decode('NOM ET PRENOMS '),0,0,'C');
  $pdf-> SetFont('','B', 12);
  $tabbeneficiaire=explode(" ",$beneficiaire);
  $nbtabbenef=count($tabbeneficiaire);

  if(strlen($beneficiaire)<=30)
   {
    $pdf -> SetX("60");
     $pdf-> SetFont('Arial','', 14);
     $pdf->Cell(93,-39,":"." ".utf8_decode(ucwords(strtolower($beneficiaire))),0,0,'L');
   }else {
     $pdf-> SetFont('Arial','I', 14);
     $case=$tabbeneficiaire[0]." ".$tabbeneficiaire[1];
     if(strlen($case)>30)
     {
       $case = substr($beneficiaire, 0, 30);
       $pdf->Cell(93,5,utf8_decode($case),0,0,'L');
     }else {
       $pdf->Cell(93,5,utf8_decode($case),0,0,'L');
     }
   }
  //  $pdf-> SetFont('Arial','B', 14);
  // $pdf->Cell(13,5,utf8_decode('Classe '),0,0,'C');
  // // if(strlen($beneficiaireclasse)>15)
  // {
  //   $tabclassebenef=explode('/',$beneficiaireclasse);
  //   $nbclassebenef=count($tabclassebenef);
  //   if($nbclassebenef>0)
  //   {
  //     $pdf-> SetFont('Times','I', 14);
  //     $pdf->Cell(47,5,utf8_decode($tabclassebenef[0]),0,0,'L');
  //   }else {
  //     $caseclasse= substr($beneficiaireclasse, 0, 15);
  //     $pdf-> SetFont('Times','I', 14);
  //     $pdf->Cell(47,5,utf8_decode($caseclasse),0,0,'L');
  //   }

  // }else {
  //   $pdf-> SetFont('Times','I', 14);
  //   $pdf->Cell(47,5,utf8_decode($beneficiaireclasse),0,0,'L');
  // }
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("13");
  $pdf-> SetFont('Arial','B', 14);
  $pdf->Cell(40,-37,utf8_decode('MONTANT(en lettres)'),0,0,'C');
  $pdf-> SetFont('Arial','', 14);
  $pdf -> SetX("60");
  $pdf->Cell(10,-37,":"." ".utf8_decode(ucwords($lettre->Conversion($solde)).' Francs CFA'),0,0,'L');
  // $pdf-> SetFont('Times','B', 14);
  // $pdf->Cell(40,5,utf8_decode('Réduction '),0,0,'L');
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("8");
  $pdf-> SetFont('Arial','B', 14);
  $pdf->Cell(48,-35,utf8_decode('AVANCE'),0,0,'L');
  $pdf -> SetX("60");
  $pdf-> SetFont('Arial','', 14);
  $montantnette=$solde+$montant;
  $resteapayer=$montant-$solde;
  $pdf->Cell(145,-35,":"." ".utf8_decode(ucwords($lettre->Conversion($solde)).'Francs CFA'),0,0,'L');
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("8");
  $pdf-> SetFont('Arial','B', 14);
  $pdf->Cell(48,-33,utf8_decode('RESTE A PAYER'),0,0,'L');
  $pdf -> SetX("60");
  $pdf-> SetFont('Arial','', 14);
  $montantnette=$solde+$montant;
  $pdf->Cell(145,-33,":"." ".utf8_decode(ucwords($lettre->Conversion($resteapayer)).'Francs CFA'),0,0,'L');
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("8");
  $pdf-> SetFont('Arial','B', 14);
  $pdf->Cell(24,-31,utf8_decode('VERSEMENT'),0,0,'L');
  $pdf-> SetFont('Arial','', 14);
  $pdf -> SetX("60");
  $pdf->Cell(0,-31,":"." ".ucfirst(utf8_decode($libellemode)) ,0,0,'L');
 /* $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(42,5,utf8_decode('Mode de règlement '),0,0,'L');
  $pdf-> SetFont('Times','I', 12);
  $pdf->Cell(38,5,utf8_decode($libellemode),0,0,'L');*/

  $pdf->Ln(10);


}else if($motif=="AES")
{

  /* $pdf -> SetX(38);
  $pdf->Cell(80,5,utf8_decode("REÇU - INSCRIPTION"),0,0,'C');*/
  /* --- Cell --- */
  $pdf->SetXY(61, 39);
  $pdf->SetFontSize(20);
  $pdf->SetTextColor(78,183,72);
  $pdf->Cell(89, 12, utf8_decode("REÇU - AES") , 0, 1, 'C', false);
  /* --- Cell --- */
  $pdf->SetXY(95, 50);
  $pdf->SetFontSize(14);
  $pdf->SetTextColor(0,0,0);
  $pdf->Cell(40, 9, number_format($montant,0,',',' ') .' '.'FCFA', 1, 1, 'C', false);
  /* --- Cell --- */
  $pdf->SetXY(73, 52);
  $pdf->SetFont('Arial', 'B', 14);
  $pdf->Cell(0, 5, 'Montant:', 0, 1, 'L', false);
  $pdf->SetXY(141, 45);
   $pdf->SetFontSize(16);
  $pdf->SetTextColor(255,0,0);
  $tabcode=explode("VER",$code);
  $versementcode=utf8_decode("N° ".$tabcode[1]);
  $pdf->Cell(0, 0,$versementcode,0,0,'C');
  $pdf->SetXY(61, 85);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("12");
  $pdf-> SetFont('Arial','B', 14);
  $pdf->Cell(40,-27,utf8_decode('NOM ET PRENOMS '),0,0,'C');
  $pdf-> SetFont('','B', 12);
  $tabbeneficiaire=explode(" ",$beneficiaire);
  $nbtabbenef=count($tabbeneficiaire);

  if(strlen($beneficiaire)<=30)
   {
    $pdf -> SetX("60");
     $pdf-> SetFont('Arial','', 14);
     $pdf->Cell(93,-27,":"." ".utf8_decode(ucwords(strtolower($beneficiaire))),0,0,'L');
   }else {
     $pdf-> SetFont('Arial','I', 14);
     $case=$tabbeneficiaire[0]." ".$tabbeneficiaire[1];
     if(strlen($case)>30)
     {
       $case = substr($beneficiaire, 0, 30);
       $pdf->Cell(93,-27,utf8_decode($case),0,0,'L');
     }else {
       $pdf->Cell(93,-27,utf8_decode($case),0,0,'L');
     }
   }
  //  $pdf-> SetFont('Arial','B', 14);
  // $pdf->Cell(13,5,utf8_decode('Classe '),0,0,'C');
  // // if(strlen($beneficiaireclasse)>15)
  // {
  //   $tabclassebenef=explode('/',$beneficiaireclasse);
  //   $nbclassebenef=count($tabclassebenef);
  //   if($nbclassebenef>0)
  //   {
  //     $pdf-> SetFont('Times','I', 14);
  //     $pdf->Cell(47,5,utf8_decode($tabclassebenef[0]),0,0,'L');
  //   }else {
  //     $caseclasse= substr($beneficiaireclasse, 0, 15);
  //     $pdf-> SetFont('Times','I', 14);
  //     $pdf->Cell(47,5,utf8_decode($caseclasse),0,0,'L');
  //   }

  // }else {
  //   $pdf-> SetFont('Times','I', 14);
  //   $pdf->Cell(47,5,utf8_decode($beneficiaireclasse),0,0,'L');
  // }
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("13");
  $pdf-> SetFont('Arial','B', 14);
  $pdf->Cell(40,-20,utf8_decode('MONTANT(en lettres)'),0,0,'C');
  $pdf-> SetFont('Arial','', 14);
  $pdf -> SetX("60");
  $pdf->Cell(10,-20,":"." ".utf8_decode(ucwords($lettre->Conversion($montant)).' Francs CFA'),0,0,'L');
  // $pdf-> SetFont('Times','B', 14);
  // $pdf->Cell(40,5,utf8_decode('Réduction '),0,0,'L');
  $pdf->Ln(10);
  // $pdf->SetTextColor(10,5,8);
  // $pdf -> SetX("8");
  // $pdf-> SetFont('Arial','B', 14);
  // $pdf->Cell(48,-35,utf8_decode('AVANCE'),0,0,'L');
  // $pdf -> SetX("60");
  // $pdf-> SetFont('Arial','', 14);
  // $montantnette=$solde+$montant;
  // $resteapayer=$montant-$solde;
  // $pdf->Cell(145,-35,":"." ".utf8_decode(ucwords($lettre->Conversion($solde)).'Francs CFA'),0,0,'L');
  // $pdf->Ln(10);
  // $pdf->SetTextColor(10,5,8);
  // $pdf -> SetX("8");
  // $pdf-> SetFont('Arial','B', 14);
  // $pdf->Cell(48,-33,utf8_decode('RESTE A PAYER'),0,0,'L');
  // $pdf -> SetX("60");
  // $pdf-> SetFont('Arial','', 14);
  // $montantnette=$solde+$montant;
  // $pdf->Cell(145,-33,":"." ".utf8_decode(ucwords($lettre->Conversion($resteapayer)).'Francs CFA'),0,0,'L');
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("8");
  $pdf-> SetFont('Arial','B', 14);
  $pdf->Cell(24,-35,utf8_decode('POUR'),0,0,'L');
  $pdf-> SetFont('Arial','', 14);
  $pdf -> SetX("60");
  $pdf->Cell(0,-35,":"." ".ucfirst(utf8_decode($libelleactivities)) ,0,0,'L');
 /* $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(42,5,utf8_decode('Mode de règlement '),0,0,'L');
  $pdf-> SetFont('Times','I', 12);
  $pdf->Cell(38,5,utf8_decode($libellemode),0,0,'L');*/

  $pdf->Ln(10);
}
$pdf-> SetFont('Arial','BU', 13);
$pdf->Ln(8);
$pdf->Cell(80,-36,'REMETTANT',0,0,'C');
$pdf->Cell(110,-36,utf8_decode("SERVICE COMPTABILITE"),0,0,'C');















$pdf->Output();



// $pdf->AddPage('P', 'A4');
// $pdf->SetAutoPageBreak(true, 10);
// $pdf->SetFont('Arial', '', 12);
// $pdf->SetTopMargin(10);
// $pdf->SetLeftMargin(10);
// $pdf->SetRightMargin(10);


// /* --- Cell --- */
// $pdf->SetTextColor(65,83,162);
// $pdf->SetXY(54, 16);
// $pdf->SetFont('Times', 'B', 14);
// $pdf->Cell(142, 13, 'INTERNATIONAL BILINGUAL SCHOOLS OF AFRICA', 'TB', 1, 'C', false);
// $pdf->SetTextColor(0);
// /* --- Cell --- */
// $pdf->SetXY(151, 32);
// $pdf->SetFont('Arial', 'B', 12);
// $pdf->Cell(47, 10, 'DATE: 22/05/1999', 0, 1, 'L', false);

// /* --- Cell --- */
// $pdf->SetXY(95, 70);
// $pdf->SetFontSize(14);
// $pdf->Cell(48, 9, '50000', 1, 1, 'L', false);
// /* --- Cell --- */
// $pdf->SetXY(73, 72);
// $pdf->SetFont('Times', 'B', 14);
// $pdf->Cell(0, 5, 'Montant:', 0, 1, 'L', false);




 ?>
