
<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Teatcher.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$student=new Student();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$teatcher=new Teatcher();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

if(strlen($_SESSION['user']['codeEtab'])>0)
{
  $codeEtabAssigner=$_SESSION['user']['codeEtab'];
}else {
  $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
}
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//nous allons chercher la liste des etablissements du groupe

$allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
$agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$libellesessionencours="";

  if($etablissementType==1||$etablissementType==3)
  {
    if($nbsessionOn>0){
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];

    }

  }else {
    if($nbsessionOn>0)
    {
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];
      $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
      $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
    }
  }

  if(strlen($_SESSION['user']['codeEtab'])>0)
  {


  }else {
    $_SESSION['user']['codeEtab']=$codeEtabAssigner;
  }

  $_SESSION['user']['session']=$libellesessionencours;

   // var_dump($allcodeEtabs);

   $allstudentschools=$student->getAllStudentOfThisSchool($codeEtabAssigner);

  $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

  $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);
  $studentInfos=$student->getAllInformationsOfStudentOne($_GET['compte'],$libellesessionencours);
  $datascolarity=$etabs->DetermineScolarityStateOfStudent($_SESSION['user']['codeEtab'],$libellesessionencours,$_GET['compte']);

  $tabStudent=array();

  foreach ($studentInfos as $personnal):
    $tabStudent[0]= $personnal->id_compte;
    $tabStudent[1]=$personnal->matricule_eleve;
    $tabStudent[2]= $personnal->nom_eleve;
    $tabStudent[3]=$personnal->prenom_eleve;
    $tabStudent[4]= $personnal->datenais_eleve;
    $tabStudent[5]=$personnal->lieunais_eleve;
    $tabStudent[6]= $personnal->sexe_eleve;
    $tabStudent[7]=$personnal->email_eleve;
    $tabStudent[8]=$personnal->email_eleve;
    $tabStudent[9]= $personnal->libelle_classe;
    $tabStudent[10]=$personnal->codeEtab_classe;
    $tabStudent[11]= $personnal->photo_compte;
    $tabStudent[12]=$personnal->tel_compte;
    $tabStudent[13]= $personnal->login_compte;
    $tabStudent[14]=$personnal->codeEtab_inscrip;
    $tabStudent[15]= $personnal->id_classe;
    $tabStudent[16]=$personnal->allergie_eleve;
    $tabStudent[17]=$personnal->condphy_eleve;
    $tabStudent[18]=$personnal->contrat_inscrip;
    $tabStudent[19]=$personnal->session_inscrip;
    $tabStudent[20]=$personnal->numclient_eleve;
    $tabStudent[21]=$personnal->carnet_inscrip;


  endforeach;

  $infosparents=$parents->ParentInfostudent($_GET['compte']);

  $allabsencesLast=$etabs->getListeAttendanceLast($tabStudent[1],$tabStudent[15],$_SESSION['user']['codeEtab'],$libellesessionencours);

  //nous allons compter le nombre d'antecedents medicaux

  $medicalesAnte=$etabs->getAllMedicalesAnteOfChild($_GET['compte']);

  $nbmedicalesAnte=count($medicalesAnte);

  $datasallergies=$etabs->getAllMedicalesAllergiesOfChild($_GET['compte']);

  $nballergies=count($datasallergies);

  $datasinfantmaladies=$etabs->getAllMedicalesInfantDiseaseOfChild($_GET['compte']);

  $nbinfantiles=count($datasinfantmaladies);

  $datasmedicals=$etabs->getFormMedicalInfosChild($_GET['compte']);
  $nbmedicales=count($datasmedicals);

  $arrayM=json_encode($datasmedicals,true);
  $someArrayM = json_decode($arrayM, true);

  $nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);

  $onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
  $offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);

 ?>

<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->



<head>

    <meta charset="utf-8" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta content="width=device-width, initial-scale=1" name="viewport" />

<title><?php echo L::Titlesite ?></title>

    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">

    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">



    <!-- google font -->

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />

	<!-- icons -->

    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<!--bootstrap -->

   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />

	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

  <!-- data tables -->

   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>

    <!-- Material Design Lite CSS -->

	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >

	<link href="../assets2/css/material_style.css" rel="stylesheet">

	<!-- morris chart -->

    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />



	<!-- Theme Styles -->

    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />

    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />

    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />

    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>



	<!-- favicon -->

    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />

    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}

#radioBtn .notActive{
color: #3276b1;
background-color: #fff;
}

#radioBtn1 .notActive{
color: #3276b1;
background-color: #fff;
}

//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
.app-file-list {
  border: 1px solid #ebebeb;
}

.app-file-list .app-file-icon {
  background-color: #f5f5f5;
  padding: 2rem;
  text-align: center;
  font-size: 2rem;
  border-bottom: 1px solid #ebebeb;
  border-top-right-radius: 8px;
  border-top-left-radius: 8px;
}

.app-file-list:hover {
  border-color: #d7d7d7;
}
.dropdown-menu-right{right:0;left:auto}
body.dark .app-file-list {
  border-color: #454c66;
}

.baseline .baseline-list .baseline-info {
	margin-top: 10px;
	margin-bottom: 10px;
	margin-left: 20px;
}

.baseline-info {
	border-color: #36a2f5 !important;
}
.baseline-border {
	border-left: 1px solid #e5ebf8;
}
.baseline, .baseline .baseline-list {
	position: relative;
	border-color: #e5ebf8;
}
.baseline .baseline-list {
	padding-bottom: 1px;
}
.baseline, .baseline .baseline-list {
	position: relative;
	border-color: #e5ebf8;
}
.baseline .baseline-list:before {
	display: table;
	content: " ";
}
.baseline .baseline-list:after {
	display: table;
	position: absolute;
	top: 18px;
	left: 0;
	width: 12px;
	height: 12px;
	margin-left: -6px;
	content: "";
	border-width: 1px;
	border-style: solid;
	border-color: inherit;
	border-radius: 10px;
	background-color: #fff;
	box-shadow: 0 0 0 3px #e5ebf8 inset;
}

.baseline-primary:after {
	box-shadow: 0 0 0 3px #a768f3 inset !important;
}
.baseline-success:after {
	box-shadow: 0 0 0 3px #34bfa3 inset !important;
}
.baseline-primary {
	border-color: #a768f3 !important;
}
.baseline-success {
	border-color: #34bfa3 !important;
}
.baseline-warning:after {
	box-shadow: 0 0 0 3px #eac459 inset !important;
}
.baseline-info:after {
	box-shadow: 0 0 0 3px #36a2f5 inset !important;
}
.baseline-warning {
	border-color: #eac459 !important;
}
    </style>

 </head>

 <!-- END HEAD -->

<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">

    <div class="page-wrapper">

        <!-- start header -->

		<?php

include("header.php");

    ?>

        <!-- end header -->

        <!-- start page container -->

        <div class="page-container">

 			<!-- start sidebar menu -->

 			<?php

				include("menu.php");

			?>

			 <!-- end sidebar menu -->

			<!-- start page content -->

            <div class="page-content-wrapper">

                <div class="page-content">

                    <div class="page-bar">

                        <div class="page-title-breadcrumb">

                            <div class=" pull-left">

                                <div class="page-title"><?php echo L::FichestudMenu ?></div>

                            </div>

                            <ol class="breadcrumb page-breadcrumb pull-right">

                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>

                                </li>

                                <li class=""> <a href="classinfos.php?classe=<?php echo $tabStudent[15] ?>&codeEtab=<?php echo $_SESSION['user']['codeEtab'] ?>"><?php echo L::studMenu ?></a> </li>&nbsp;<i class="fa fa-angle-right"></i>

                                <li class="active"><?php echo L::FichestudMenu ?></li>

                            </ol>

                        </div>

                    </div>

					<!-- start widget -->

					<div class="state-overview">



						</div>

            <?php

            if(isset($_SESSION['user']['FileError']))
            {
              ?>
              <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
              <script src="../assets/js/sweetalert2.min.js"></script>

                            <script>
              Swal.fire({
                           type: 'warning',
                           title: '<?php echo L::WarningLib ?>',
                           text: "<?php echo $_SESSION['user']['FileError'] ?>",

                         })
                         </script>
              <?php
                unset($_SESSION['user']['FileError']);
            }



                  if(isset($_SESSION['user']['addteaok']))
                  {
// echo "dddddddd";
                    ?>

<link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
<script src="../assets/js/sweetalert2.min.js"></script>

              <script>

              Swal.fire({
  title: '<?php echo L::Felicitations ?>',
  text: "<?php echo $_SESSION['user']['addteaok']; ?>",
  type: 'success',
  showCancelButton: false,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  // confirmButtonText: 'Ajouter les diplomes',
  cancelButtonText: '<?php echo L::Okay ?>',
  }).then((result) => {
  if (result.value) {
  // document.location.href="adddiplomes.php?compte="+<?php //echo $_GET['compte'] ?>;
  }else {
  // document.location.href="index.php";
  }
  })
              </script>
                    <?php
                    unset($_SESSION['user']['addteaok']);
                  }

                   ?>

					<!-- end widget -->

          <?php



                if(isset($_SESSION['user']['addetabexist']))

                {



                  ?>

                  <div class="alert alert-danger alert-dismissible fade show" role="alert">

                <?php

                echo $_SESSION['user']['addetabexist'];

                ?>

                <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                <span aria-hidden="true">&times;</span>

                   </a>

                </div>







                  <?php

                  unset($_SESSION['user']['addetabexist']);

                }



                 ?>


                 <?php

                       if(isset($_SESSION['user']['addStudok']))
                       {

                         ?>
                         <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                       <?php
                       //echo $_SESSION['user']['addetabok'];
                       ?>
                       <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
                          </a>
                       </div-->
               <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
               <script src="../assets/js/sweetalert2.min.js"></script>

                   <script>
                   Swal.fire({
                   type: 'success',
                   title: '<?php echo L::Felicitations ?>',
                   text: '<?php echo $_SESSION['user']['addStudok'] ?>',

                   })
                   </script>
                         <?php
                         unset($_SESSION['user']['addStudok']);
                       }

                        ?>


          <div class="row">



            <div class="col-sm-4 col-md-4">

              <div class="card ">

                                    <div class="card-body no-padding height-9">

                                        <div class="row">

                                            <div class="profile-userpic">

                                              <?php
                                                $fileExiste=0;

                                                  if($tabStudent[11]!="")

                                                  {

                                                    $lien="../photo/Students/".$tabStudent[1]."/".$tabStudent[11];
                                                    $fileExiste=1;

                                                  }else {

                                                    $lien="../photo/user5.jpg";

                                                  }
                                                  //style="width:30%;height:100%"

                                               ?>

                                                <img src="<?php echo $lien;?>" class="img-responsive" alt=""  ondblclick="changepictures()"> </div>

                                        </div>

                                        <div class="profile-usertitle">

                                            <div class="profile-usertitle-name"><?php echo $tabStudent[2]." ".$tabStudent[3];?></div>

                                            <div class="profile-usertitle-job"> <?php echo $tabStudent[1];?></div>


                                        </div>

                                        <form class="" action="../controller/admission.php" method="post" id="FormFile" enctype="multipart/form-data" >
                                          <input type="file" name="fileInput" value="" id="fileInput" onchange="UploadPictures()" style="display:none">
                                          <input type="hidden" name="matriculefile"  id="matriculefile" value="<?php echo $tabStudent[1] ?>">
                                          <input type="hidden" name="studentIdfile"  id="studentIdfile" value="<?php echo $tabStudent[0] ?>">
                                          <input type="hidden" name="studentNamefile"  id="studentNamefile" value="<?php echo $tabStudent[2] ?>">
                                          <input type="hidden" name="codeEtabfile"  id="codeEtabfile" value="<?php echo $_SESSION['user']['codeEtab'] ?>">
                                          <input type="hidden" name="sessionEtabfile"  id="sessionEtabfile" value="<?php echo $libellesessionencours ?>">
                                          <input type="hidden" name="fileExiste"  id="fileExiste" value="<?php echo $fileExiste ?>">
                                          <input type="hidden" name="etape"  id="etape" value="3">
                                          <input type="hidden" name="fileExistename" id="fileExistename" value="<?php echo $tabStudent[11] ?>">
                                        </form>

                                        <ul class="list-group list-group-unbordered">

                                            <li class="list-group-item">

                                                <b><?php echo L::AcquisEvaluation?></b> <a class="pull-right"></a>

                                            </li>

                                            <li class="list-group-item">

                                                <b><?php echo L::Barometers ?></b> <a class="pull-right"></a>

                                            </li>



                                        </ul>

                                        <!-- END SIDEBAR USER TITLE -->

                                        <!-- SIDEBAR BUTTONS -->

                                        <div class="profile-userbuttons">

			<a  target="_blank" class="btn btn-circle btn-warning btn-sm" href="listePersonnelEleve.php?compte=<?php echo $_GET['compte']?>&sessionEtab=<?php echo $libellesessionencours; ?>"> <i class="fa fa-print"></i><?php echo L::PrintFiche ?>
                        </a>


					<!--button type="button" class="btn btn-circle red btn-sm">Message</button-->

                                        </div>
                                        <div class="profile-userbuttons">

      <a  class="btn btn-circle btn-success btn-sm" href="#"> <i class="fa fa-pencil"></i><?php echo L::Writetoteatcher ?>
      </a>
                                  </div>
                                  <div class="profile-userbuttons">

<a   class="btn btn-circle btn-primary btn-sm" href="#"> <i class="fa fa-book"></i><?php echo L::Readteatcher ?>
</a>
                            </div>

                                        <!-- END SIDEBAR BUTTONS -->

                                    </div>

                                </div>
                                <div class="col-sm-12">

                                  <div class="card ">

                                                        <div class="card-body no-padding height-9">

                                                            <div class="row">



                                                            </div>

                                                            <div class="profile-usertitle">

                                                                <div class="profile-usertitle-name"><?php echo L::Devoirdemaison?></div>

                                                            </div>

                                                            <ul class="list-group list-group-unbordered">

                                                              <!-- <?php
                                                              if($nbparascolairepaiement>0)
                                                              {
                                                                $parascolairepaiementdata=$etabs->getNumberPaiementparascolairesdatas($tabStudent[0],$tabStudent[14],$tabStudent[18],$tabStudent[15]);

                                                                foreach ($parascolairepaiementdata as  $value):
                                                                  $aesid=$value->motifid_versement;
                                                                  $datas=$etabs->getparascolaireMessages($aesid);
                                                                  foreach ($datas as  $values):
                                                                    $libelle=$values->libelle_act;
                                                                  endforeach;
                                                                  ?>
                                                                  <li class="list-group-item">

                                                                      <b><?php echo L::$libelle ?></b> <a class="pull-right"></a>

                                                                  </li>
                                                                  <?php
                                                                endforeach;
                                                                }
                                                               ?> -->






                                                            </ul>

                                                            <!-- END SIDEBAR USER TITLE -->

                                                            <!-- SIDEBAR BUTTONS -->



                                                            <!-- END SIDEBAR BUTTONS -->

                                                        </div>

                                                    </div>

                                </div>
                                <?php
                                // echo $tabStudent[0]."/".$tabStudent[14]."/".$tabStudent[18]."/".$tabStudent[15];
                                $nbparascolairepaiement=$etabs->getNumberPaiementparascolaires($tabStudent[0],$tabStudent[14],$tabStudent[18],$tabStudent[15]);
                                // if($nbparascolairepaiement==0)
                                // {
                                  ?>
                                  <div class="col-sm-12">

                                    <div class="card ">

                                                          <div class="card-body no-padding height-9">

                                                              <div class="row">



                                                              </div>

                                                              <div class="profile-usertitle">

                                                                  <div class="profile-usertitle-name"><?php echo L::AesScream?></div>

                                                              </div>

                                                              <ul class="list-group list-group-unbordered">

                                                                <?php
                                                                  $parascolairepaiementdata=$etabs->getNumberPaiementparascolairesdatas($tabStudent[0],$tabStudent[14],$tabStudent[18],$tabStudent[15]);

                                                                  foreach ($parascolairepaiementdata as  $value):
                                                                    $aesid=$value->motifid_versement;
                                                                    $datas=$etabs->getparascolaireMessages($aesid);
                                                                    foreach ($datas as  $values):
                                                                      $libelle=$values->libelle_act;
                                                                    endforeach;
                                                                    ?>
                                                                    <li class="list-group-item">

                                                                        <b><?php echo L::$libelle ?></b> <a class="pull-right"></a>

                                                                    </li>
                                                                    <?php
                                                                  endforeach;
                                                                 ?>






                                                              </ul>

                                                              <!-- END SIDEBAR USER TITLE -->

                                                              <!-- SIDEBAR BUTTONS -->

                                                              <div class="profile-userbuttons">


                                                              </div>

                                                              <!-- END SIDEBAR BUTTONS -->

                                                          </div>

                                                      </div>

                                  </div>
                                  <?php
                                // }
                                 ?>
                                 <div class="col-sm-12">

                                   <div class="card ">

                                                         <div class="card-body no-padding height-9">

                                                             <div class="row">



                                                             </div>

                                                             <div class="profile-usertitle">

                                                                 <div class="profile-usertitle-name"><?php echo strtoupper(L::TeatcherCapsingle)?></div>

                                                             </div>

                                                             <ul class="list-group list-group-unbordered">

                                                               <?php
                                                                 $allteatchers=$teatcher->getAllTeatcherOfClasses($tabStudent[10],$tabStudent[19],$tabStudent[15]);
                                                                 // var_dump($allteatchers);
                                                                 foreach ($allteatchers as  $value):

                                                                   ?>
                                                                   <li class="list-group-item">

                                                                       <b><?php echo $value->nom_compte." ".$value->prenom_compte ?></b> <a class="pull-right"></a>

                                                                   </li>
                                                                   <?php
                                                                 endforeach;
                                                                ?>






                                                             </ul>

                                                             <!-- END SIDEBAR USER TITLE -->

                                                             <!-- SIDEBAR BUTTONS -->

                                                             <div class="profile-userbuttons">


                                                             </div>

                                                             <!-- END SIDEBAR BUTTONS -->

                                                         </div>

                                                     </div>

                                 </div>

            </div>

            <div class="col-sm-8">

                <div class="profile-content">

                  <div class="row">
                    <div class="profile-tab-box">
                      <div class="p-l-20">
                        <ul class="nav ">
                          <li class="nav-item tab-all"><a
                            class="nav-link active show" href="#tab1" data-toggle="tab"><?php echo L::GeneralInfoTab ?></a></li>
                            <?php
                            if($_SESSION['user']['fonctionuser']=="Marketing"||$_SESSION['user']['fonctionuser']=="Intendance"||$_SESSION['user']['fonctionuser']=="CC"||$_SESSION['user']['fonctionuser']=="CAES"||$_SESSION['user']['fonctionuser']=="CD")
                            {

                            }else
                            {
                             ?>
                            <li class="nav-item tab-all p-l-20"><a class="nav-link"
                              href="#tab2" data-toggle="tab"><?php echo L::Paiements ?></a></li>
                              <?php
                            }
                               ?>

<?php
if($_SESSION['user']['fonctionuser']=="Comptable")
{

}else {
  ?>
  <li class="nav-item tab-all p-l-20"><a class="nav-link"
    href="#tab3" data-toggle="tab"><?php echo L::Pedagogie ?></a></li>
    <li class="nav-item tab-all p-l-20"><a class="nav-link"
      href="#tab4" data-toggle="tab"><?php echo L::AbsMenu ?></a></li>
  <?php
}
 ?>

                              <?php
                                    if($_SESSION['user']['fonctionuser']=="Coordinnateur"|| $_SESSION['user']['fonctionuser']=="Administrateur"|| $_SESSION['user']['fonctionuser']=="Superviseur")
                                    {
                                      ?>
                                      <li class="nav-item tab-all p-l-20"><a class="nav-link"
                                        href="#tab5" data-toggle="tab"><?php echo L::Parameters ?></a></li>
                                      <?php
                                    }
                               ?>


                        </ul>
                      </div>
                    </div>
                    <div class="white-box">

                      <div class="tab-content">
                        <div class="tab-pane active fontawesome-demo" id="tab1">
                          <div id="biography" >

                            <div class="row">
                              <div class="col-md-4">
                              <span class="label label-md label-info" style="text-align:center;color:white"><?php echo L::GeneralInfostudentTab ?></span>
                              </div>
                              <div class="col-md-4">
                                <?php
                                if(strlen($tabStudent[18])>0)
                                {
                                  $fileExiste=1;
                                  ?>
                                  <a href="#" class="label label-md label-warning"> <i class="fa fa-pencil"></i> <?php echo L::ParentalContrat ?> </a>
                                  <form class="" action="../controller/admission.php" method="post" id="FormFile2"  >
                                    <input type="file" name="fileInput" value="" id="fileInput" onchange="UploadContratsMod()" style="display:none">
                                    <input type="hidden" name="matriculefile"  id="matriculefile" value="<?php echo $tabStudent[1] ?>">
                                    <input type="hidden" name="studentIdfile"  id="studentIdfile" value="<?php echo $tabStudent[0] ?>">
                                    <input type="hidden" name="studentNamefile"  id="studentNamefile" value="<?php echo $tabStudent[2] ?>">
                                    <input type="hidden" name="codeEtabfile"  id="codeEtabfile" value="<?php echo $_SESSION['user']['codeEtab']; ?>">
                                    <input type="hidden" name="sessionEtabfile"  id="sessionEtabfile" value="<?php echo $libellesessionencours ?>">
                                    <input type="hidden" name="fileExiste2"  id="fileExiste2" value="<?php echo $fileExiste ?>">
                                    <input type="hidden" name="etape"  id="etape" value="5">
                                    <input type="hidden" name="fileExistename" id="fileExistename" value="<?php echo $tabStudent[18] ?>">
                                  </form>
                                  <?php
                                }else {
                                  $fileExiste=0;
                                ?>
                                <a href="#" class="label label-md label-info" style="color:white" onclick="changecontrats()"> <i class="fa fa-plus"></i> <?php echo L::ParentalContrat ?> </a>

                                <form class="" action="../controller/admission.php" method="post" id="FormFile1"  enctype="multipart/form-data">
                                  <input type="file" name="fileInput1" value="" id="fileInput1" onchange="UploadContrats()" style="display:none">
                                  <input type="hidden" name="matriculefile"  id="matriculefile" value="<?php echo $tabStudent[1] ?>">
                                  <input type="hidden" name="studentIdfile"  id="studentIdfile" value="<?php echo $tabStudent[0] ?>">
                                  <input type="hidden" name="studentNamefile"  id="studentNamefile" value="<?php echo $tabStudent[2] ?>">
                                  <input type="hidden" name="codeEtabfile"  id="codeEtabfile" value="<?php echo $_SESSION['user']['codeEtab']; ?>">
                                  <input type="hidden" name="sessionEtabfile"  id="sessionEtabfile" value="<?php echo $libellesessionencours ?>">
                                  <input type="hidden" name="fileExiste"  id="fileExiste" value="<?php echo $fileExiste ?>">
                                  <input type="hidden" name="etape"  id="etape" value="4">
                                  <input type="hidden" name="fileExistename" id="fileExistename" value="<?php echo $tabStudent[18] ?>">
                                </form>
                                <?php
                                }

                                ?>
                              </div>
                              <div class="col-md-4">

                                <?php
                                if(strlen($tabStudent[21])>0)
                                {
                                    $fileExisteCarnet=1;
                                  ?>
                                    <a href="#" class="label label-md label-warning"> <i class="fa fa-pencil"></i> <?php echo L::CarnetSante ?> </a>
                                  <?php
                                }else {
                                  $fileExisteCarnet=0;
                                  ?>
                                    <a href="#" class="label label-md label-info" style="color:white" onclick="changecarnet()"> <i class="fa fa-plus"></i> <?php echo L::CarnetSante ?> </a>
                                    <form class="" action="../controller/admission.php" method="post" id="FormFile2"  >
                                      <input type="file" name="fileInput2" value="" id="fileInput2" onchange="UploadCarnets()" style="display:none">
                                      <input type="hidden" name="matriculefile"  id="matriculefile" value="<?php echo $tabStudent[1] ?>">
                                      <input type="hidden" name="studentIdfile"  id="studentIdfile" value="<?php echo $tabStudent[0] ?>">
                                      <input type="hidden" name="studentNamefile"  id="studentNamefile" value="<?php echo $tabStudent[2] ?>">
                                      <input type="hidden" name="codeEtabfile"  id="codeEtabfile" value="<?php echo $_SESSION['user']['codeEtab']; ?>">
                                      <input type="hidden" name="sessionEtabfile"  id="sessionEtabfile" value="<?php echo $libellesessionencours ?>">
                                      <input type="hidden" name="fileExiste"  id="fileExiste" value="<?php echo $fileExiste ?>">
                                      <input type="hidden" name="etape"  id="etape" value="6">
                                      <input type="hidden" name="fileExistename" id="fileExistename" value="<?php echo $tabStudent[21] ?>">
                                    </form>
                                  <?php
                                }

                                 ?>

                              </div>






                              <div class="col-md-4">

                              </div>

                            </div>
                            </br>
                            <div class="modal fade" id="largeModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              					    <div class="modal-dialog modal-xs" role="document">
              					        <div class="modal-content">
              					            <div class="modal-header">
              					                <h4 class="modal-title" id="exampleModalLabel"><?php echo L::ModClientNumbers ?></h4>
              					                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              					                    <span aria-hidden="true">&times;</span>
              					                </button>
              					            </div>
              					            <div class="modal-body">
                                      <div id="divrecherchestation">
                                        <div class="table-scrollable">

                              </div>
                              <form class="" action="#" id="Modclientnumber">
                                <div class="form-group row">
                                    <label class="control-label col-md-12"><?php echo L::ClientNumbers ?>
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-12">
                                      <input type="text" class="form-control" id="clientnumbermod" name="clientnumbermod" value="<?php echo $tabStudent[20]  ?>" placeholder="<?php echo L::EnterClientNumbers ?>">

                                    </div>

                                </div>
                                     <input type="hidden" name="studentid" id="studentid" value="<?php echo $_GET['compte'] ?>">
                                     <input type="hidden" name="etape" id="etape" value="">

                                     <div class="">

                                       <button type="submit" class="btn btn-primary btn-md" name="button"> <i class="fa fa-check"></i> <?php echo L::ModifierBtn; ?> </button>

                                     </div>


                              </form>

                                      </div>
              					            </div>
              					            <div class="modal-footer">
              					                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closing()"><?php echo L::Closebtn  ?></button>
              					                <button type="button" style="display:none" class="btn btn-primary" onclick="validerMember()" id="btnsaving"><?php echo L::Saving ?></button>
              					            </div>
              					        </div>
              					    </div>
              					</div>
                            <div class="modal fade" id="largeModel1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              					    <div class="modal-dialog modal-xs" role="document">
              					        <div class="modal-content">
              					            <div class="modal-header">
              					                <h4 class="modal-title" id="exampleModalLabel"><?php echo L::AddClientNumbers ?></h4>
              					                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              					                    <span aria-hidden="true">&times;</span>
              					                </button>
              					            </div>
              					            <div class="modal-body">
                                      <div id="divrecherchestation">

                              <form class="" action="#" id="addclientnumber">
                                <div class="form-group row">
                                    <label class="control-label col-md-12"><?php echo L::ClientNumbers ?>
                                        <span class="required">  </span>
                                    </label>
                                    <div class="col-md-12">
                                      <input type="text" class="form-control" id="clientnumberadd" name="clientnumberadd" placeholder="<?php echo L::EnterClientNumbers ?>">

                                    </div>

                                </div>
                                     <input type="hidden" name="studentid" id="studentid" value="<?php echo $_GET['compte'] ?>">
                                     <input type="hidden" name="etape" id="etape" value="">
                                     <div class="">

                                       <button type="submit" class="btn btn-primary btn-md" name="button"> <i class="fa fa-check"></i> <?php echo L::Validate; ?> </button>

                                     </div>

                              </form>

                                      </div>
              					            </div>
              					            <div class="modal-footer">
              					                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closing()"><?php echo L::Closebtn  ?></button>
              					                <button type="button" style="display:none" class="btn btn-primary" onclick="validerMember()" id="btnsaving"><?php echo L::Saving ?></button>
              					            </div>
              					        </div>
              					    </div>
              					</div>
                            <div class="table-responsive">

                              <table class="table table-striped custom-table table-hover" >

                                  <thead>



                                  </thead>

                                  <tbody>
                                    <?php
                                    if($_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Accountant")
                                    {
                                     ?>
                                    <tr>

                                        <td><a href="#"><?php echo L::ClientNumbers ?> :</a>

                                        </td>

                                        <td >
                                          <?php
                                          if(strlen($tabStudent[20])>0)
                                          {
                                            echo $tabStudent[20];
                                            ?>
                                            <a href="#" class="btn btn-xs btn-primary pull-right"  data-toggle="modal" data-target="#largeModel"> <i class="fa fa-pencil"></i> </a
                                            <?php
                                          }else {
                                            ?>
                                            <a href="#" class="btn btn-xs btn-primary pull-right"  data-toggle="modal" data-target="#largeModel1"> <i class="fa fa-plus"></i> </a>
                                            <?php
                                          }
                                           ?>
                                        </td>



                                    </tr>
                                    <?php
                                  }
                                     ?>

                                    <tr>

                                        <td><a href="#"><?php echo L::MatriculestudentTab ?> :</a>

                                        </td>

                                        <td ><?php echo $tabStudent[1];?></td>



                                    </tr>

                                      <tr>

                                          <td><a href="#"><?php echo L::NamestudentTab ?> :</a>

                                          </td>

                                          <td ><?php echo $tabStudent[2]." ".$tabStudent[3];?></td>



                                      </tr>



                                      <tr>

                                          <td><a href="#"> <?php echo L::ClassestudentTab  ?>: </a>

                                          </td>

                                          <td ><?php echo $tabStudent[9];?></td>



                                      </tr>
                                      <?php
                                      if(strlen($tabStudent[7])>0)
                                      {
                                       ?>
                                      <tr>

                                          <td><a href="#"> <?php echo L::EmailstudentTab ?> : </a>

                                          </td>

                                          <td ><?php echo $tabStudent[7];?></td>



                                      </tr>
                                      <?php
                                    }
                                       ?>

                                       <?php
                                       if(strlen($tabStudent[12])>0)
                                       {
                                        ?>
                                      <tr>

                                          <td><a href="#"><?php echo L::PhonestudentTab ?> :</a>

                                          </td>

                                          <td ><?php echo $tabStudent[12];?></td>



                                      </tr>
                                      <?php
                                    }
                                       ?>
                                      <tr>

                                          <td><a href="#"> <?php echo L::SexestudentTab ?> : </a>

                                          </td>

                                          <td ><?php

                                          $sexe=$tabStudent[6];

                                          if($sexe=="M")

                                          {

                                            echo L::SexeM;

                                          }else {

                                            echo L::SexeF;

                                          }

                                          ?></td>



                                      </tr>

                                      <tr>

                                        <td><a href="#"><?php echo L::BirthstudentTab ?> :</a>

                                          </td>

                                          <td ><?php echo date_format(date_create($tabStudent[4]),"d/m/Y");?></td>



                                      </tr>



                                  </tbody>

                              </table>

                            </div></br>
                            <div class="row">
                              <div class="col-md-12">
                              <span class="label label-md label-info" style="text-align:center"><?php echo L::GeneralInfosParentTab ?></span>
                              </div>
                            </div></br>
                              <div class="table-responsive">
                                <table class="table table-striped custom-table table-hover">

                                    <thead>
                                      <th><?php echo L::NamestudentTab ?> </th>
                                      <th><?php echo L::EmailstudentTab ?> </th>
                                      <th><?php echo L::ContactsParentTab ?> </th>
                                      <th><?php echo L::ProfessionsParentTab ?></th>


                                    </thead>

                                    <tbody>
                                      <?php
                                      foreach ($infosparents as $value):
                                       ?>

                                        <tr>

                                            <td><?php echo $value->nom_compte." ".$value->prenom_compte; ?></td>
                                            <td><?php echo $value->email_compte; ?></td>
                                            <td ><?php echo $value->tel_compte; ?></td>
                                            <td ><?php echo $value->fonction_compte; ?></td>






                                        </tr>

                                      <?php
                                    endforeach;
                                       ?>
                                    </tbody>

                                </table>
                              </div></br>
                              <?php
                              if($nbmedicales>0)
                              {
                                ?>
                                <div class="row">
                                  <div class="col-md-12">
                                  <span class="label label-md label-info" style="text-align:center"><?php echo L::MedicalInfosTab ?></span>
                                  </div>
                                </div>
                                </br>
                                  <div class="table-responsive">

                                    <table class="table table-striped custom-table table-hover" >

                                        <thead>



                                        </thead>

                                        <tbody>

                                          <tr>

                                              <td><a href="#"><?php echo L::BloodInfosTab ?> :</a>

                                              </td>

                                              <td ><?php echo @$someArrayM[0]["blood_medical"];?></td>



                                          </tr>

                                            <tr>

                                                <td><a href="#"><?php echo L::HospitalnameTab ?> :</a>

                                                </td>

                                                <td ><?php echo @$someArrayM[0]["hopital_medical"];?></td>



                                            </tr>



                                            <tr>

                                                <td><a href="#"> <?php echo L::HospitalchildnumberTab ?> : </a>

                                                </td>

                                                <td ><?php echo @$someArrayM[0]["pinchildhopital_medical"];?></td>



                                            </tr>
                                            <?php
                                            if(strlen(@$someArrayM[0]["doctor_medical"])>0)
                                            {
                                             ?>
                                            <tr>

                                                <td><a href="#"> <?php echo L::HospitaldoctorTab ?> : </a>

                                                </td>

                                                <td ><?php echo @$someArrayM[0]["doctor_medical"];?></td>



                                            </tr>
                                            <?php
                                          }
                                             ?>

                                             <?php
                                             if(strlen(@$someArrayM[0]["phonedoctor_medical"])>0)
                                             {
                                              ?>
                                            <tr>

                                                <td><a href="#"><?php echo L::PhonestudentTab ?> :</a>

                                                </td>

                                                <td ><?php echo @$someArrayM[0]["phonedoctor_medical"];?></td>



                                            </tr>
                                            <?php
                                          }
                                             ?>

                                             <tr>

                                                 <td><a href="#"><?php echo L::DiseasecaseTab ?> :</a>

                                                 </td>

                                                 <td ><?php
                                                 if(@$someArrayM[0]["localisationsick_medical"]==1)
                                                 {
                                                   ?>
  <span class="label label-md label-danger" style="text-align:center"><?php echo L::LeadToHomeTab ?></span>
                                                   <?php
                                                 }else {
                                                   ?>
  <span class="label label-md label-danger" style="text-align:center"><?php echo L::LeadToHomeToAdress ?></span>
                                                   <?php
                                                 }

                                                 ?></td>



                                             </tr>
                                             <tr>

                                                 <td colspan="2"><a href="#"><?php echo L::GeoAdress ?> :</a>

                                                 </td>

                                             </tr>
                                             <tr>

                                                 <td colspan="2">
                                                  <span class="label label-md label-success" style="text-align:center"><?php echo utf8_decode(@$someArrayM[0]["adresgeolocalsick_medical"]);  ?></span>

                                                 </td>

                                             </tr>





                                        </tbody>

                                    </table>

                                  </div></br>
                                  <div class="table-responsive">
                                    <ol>
                                    <?php
                                    if(@$nbmedicalesAnte>0)
                                    {
                                     ?>

                                      <li><?php echo L::HistoriqDiseases ?></li>


                                    <table class="table table-striped custom-table table-hover">

                                        <thead>
                                          <th><?php echo L::HistoriqDiseases ?> </th>
                                          <th><?php echo L::Child ?> </th>
                                          <th><?php echo L::Father ?> </th>
                                          <th><?php echo L::Mother ?></th>


                                        </thead>

                                        <tbody>
                                          <?php
                                          foreach (@$medicalesAnte as $value):
                                           ?>

                                            <tr>

                                                <td><span class="label label-sm label-danger" style="text-align:center"><?php echo utf8_encode(@$value->libelle_desease); ?></span></td>
                                                <td><span class="label label-sm label-danger" style="text-align:center"><?php if(@$value->childstate_ante==1){echo L::True;}else{echo L::False;} ?></span></td>
                                                <td ><span class="label label-sm label-danger" style="text-align:center"><?php if(@$value->fatherstate_ante==1){echo L::True;}else{echo  L::False;;} ?></span></td>
                                                <td ><span class="label label-sm label-danger" style="text-align:center"><?php if(@$value->motherstate_ante==1){echo L::True;;}else{echo  L::False;} ?></td>






                                            </tr>

                                          <?php
                                        endforeach;
                                           ?>
                                        </tbody>

                                    </table>

                                    <?php
                                     }
                                     ?>
                                     <?php
                                     if(@$nballergies>0)
                                     {
                                       $array=json_encode(@$datasallergies,true);
                                       $someArray = json_decode(@$array, true);
                                       $donnees=@$someArray[0]["allergies_medical"];
                                       $donnees=substr($donnees, 0, -1);

                                       @$tabdonnees=explode(",",$donnees);
                                       @$nbtabdonnees=count($tabdonnees);
                                      ?>

                                       <li><?php echo L::Allergies ?></li>

                                       <table class="table table-striped custom-table table-hover">



                                           <tbody>
                                            <?php
                                            if(strlen($someArray[0]["allergies_medical"])>0 && $someArray[0]["allergies_medical"]!="" )
                                            {


                                            for($i=0;$i<@$nbtabdonnees;$i++)
                                            {


                                             ?>

                                               <tr>

                                                   <td><span class="label label-sm label-danger" style="text-align:center"><?php echo @$etabs->getLibelleOfDiseaseById($tabdonnees[$i]); ?></span></td>


                                               </tr>
                                               <?php
                                             }
                                             }
                                                ?>

                                           </tbody>

                                       </table>
                                       <?php
                                     }
                                        ?>
                                        <?php
                                        if(@$nbinfantiles>0)
                                        {
                                          @$array=json_encode(@$datasinfantmaladies,true);
                                          @$someArray = json_decode(@$array, true);
                                          @$donnees=@$someArray[0]["deseases_medical"];
                                          @$donnees=substr(@$donnees, 0, -1);

                                          @$tabdonnees=explode(",",@$donnees);
                                          @$nbtabdonnees=count(@$tabdonnees);
                                         ?>
                                        <li><?php echo L::ChildDeseases ?></li>
                                        <table class="table table-striped custom-table table-hover">



                                            <tbody>
                                             <?php
                                             if(strlen($someArray[0]["deseases_medical"])>0 && $someArray[0]["deseases_medical"]!="" )
                                             {
                                             for($i=0;$i<@$nbtabdonnees;$i++)
                                             {


                                              ?>

                                                <tr>

                                                    <td><span class="label label-sm label-danger" style="text-align:center"><?php echo @$etabs->getLibelleOfDiseaseById(@$tabdonnees[$i]); ?></span></td>


                                                </tr>
                                                <?php
                                              }
                                              }
                                                 ?>

                                            </tbody>

                                        </table>
                                        <?php
                                        }
                                         ?>
                                          </ol>


                                  </div></br>
                                  <div class="row">
                                    <div class="col-md-12">
                                    <span class="label label-md label-info" style="text-align:center"><?php echo L::InformPerson ?></span>
                                    </div>
                                  </div></br>
                                  <div class="table-responsive">

                                    <table class="table table-striped custom-table table-hover" >

                                        <thead>



                                        </thead>

                                        <tbody>
                                          <?php
                                          if(strlen(@$someArrayM[0]["guardian_medical"])>0)
                                          {
                                           ?>
                                          <tr>

                                              <td><a href="#"><?php echo L::NamestudentTab ?> :</a>

                                              </td>

                                              <td ><?php echo utf8_decode(@$someArrayM[0]["guardian_medical"]);?></td>



                                          </tr>
                                          <?php
                                        }
                                           ?>

                                          <?php
                                          if(strlen(@$someArrayM[0]["telBguardian_medical"])>0)
                                          {
                                           ?>

                                            <tr>

                                                <td><a href="#"><?php echo L::PhoneBuro ?>:</a>

                                                </td>

                                                <td ><?php echo @$someArrayM[0]["telBguardian_medical"];?></td>



                                            </tr>

                                            <?php
                                          }
                                             ?>

                                            <?php
                                            if(strlen(@$someArrayM[0]["telMobguardian_medical"])>0)
                                            {
                                             ?>

                                            <tr>

                                                <td><a href="#"> <?php echo L::PhoneCell ?> : </a>

                                                </td>

                                                <td ><?php echo @$someArrayM[0]["telMobguardian_medical"];?></td>



                                            </tr>
                                            <?php
                                          }
                                             ?>
                                            <?php
                                            if(strlen(@$someArrayM[0]["domicileguardian_medical"])>0)
                                            {
                                             ?>
                                            <tr>

                                                <td><a href="#"> <?php echo L::DomicileH ?> : </a>

                                                </td>

                                                <td ><?php echo @$someArrayM[0]["domicileguardian_medical"];?></td>



                                            </tr>
                                            <?php
                                          }
                                             ?>

                                              </tbody>

                                    </table>

                                  </div></br>
                                  <?php
                                  $carnetstudent=$someArrayM[0]["carnet_medical"];
                                  if(strlen($carnetstudent)>0)
                                  {
                                    $lien="../photo/Carnets/" .$tabStudent[1]."/".$carnetstudent;
                                    ?>

                                    <div class="row">

                                      <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                                    <div class="card app-file-list">
                                                        <div class="app-file-icon">
                                                          <?php
                                                          $filename=$carnetstudent;
                                                          $tabfiles=explode('.',$filename);
                                                          $typefiles=$tabfiles[1];


                                                          if($typefiles=="csv"||$typefiles=="xlsx"||$typefiles=="xls")
                                                          {
                                                            ?>
                                                            <i class="fa fa-file-excel-o text-success"></i>
                                                            <?php

                                                          }else if($typefiles=="docx"||$typefiles=="docx")
                                                          {
                                                            ?>
                                                            <i class="fa fa-file-text-o text-primary"></i>
                                                            <?php
                                                          }else if($typefiles=="pdf")
                                                          {
                                                            ?>
                                                            <i class="fa fa-file-pdf-o text-warning"></i>
                                                            <?php

                                                          }else {
                                                            ?>
                                                            <i class="fa fa-file-text-o text-warning"></i>
                                                            <?php
                                                          }

                                                           ?>



                                                        </div>
                                                        <div class="p-2 small">
                                                          <?php
                                                          if($typefiles=="csv"||$typefiles=="xlsx"||$typefiles=="xls")
                                                          {
                                                            ?>

                                                            <div style="text-align:center"> <span class="label label-success" ><?php echo $carnetstudent ?></span> </div>
                                                            <?php

                                                          }else if($typefiles=="docx"||$typefiles=="docx")
                                                          {
                                                            ?>

                                                            <div style="text-align:center"> <span class="label label-primary" ><?php echo $carnetstudent ?></span> </div>
                                                            <?php
                                                          }else if($typefiles=="pdf")
                                                          {
                                                            ?>
                                                            <div style="text-align:center"> <span class="label label-warning" ><?php echo $carnetstudent ?></span> </div>
                                                            <?php

                                                          }else {
                                                            ?>

                                                            <div style="text-align:center"> <span class="label label-warning" ><?php echo $carnetstudent ?></span> </div>
                                                            <?php
                                                          }
                                                           ?>
                                                           <br>

                                                            <div class="text-muted" style="text-align:center"><?php echo L::carnetCaps ?></div>

                                                            <div class="pull-right">

                                                              <a target="_blank" href="<?php echo $lien ?>" class="btn btn-xs btn-warning"> <i class="fa fa-download"></i> </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                    <!-- </div> -->

                                    <?php
                                  }

                                  if(strlen($tabStudent[18])>0)
                                  {
                                    $lien="../photo/Contrats/" . $tabStudent[1]."/".$tabStudent[18];
                                    ?>

                                    <!-- <div class="row"> -->

                                      <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                                    <div class="card app-file-list">
                                                        <div class="app-file-icon">
                                                          <?php
                                                          $filename=$tabStudent[18];
                                                          $tabfiles=explode('.',$filename);
                                                          $typefiles=$tabfiles[1];


                                                          if($typefiles=="csv"||$typefiles=="xlsx"||$typefiles=="xls")
                                                          {
                                                            ?>
                                                            <i class="fa fa-file-excel-o text-success"></i>
                                                            <?php

                                                          }else if($typefiles=="docx"||$typefiles=="docx")
                                                          {
                                                            ?>
                                                            <i class="fa fa-file-text-o text-primary"></i>
                                                            <?php
                                                          }else if($typefiles=="pdf")
                                                          {
                                                            ?>
                                                            <i class="fa fa-file-pdf-o text-warning"></i>
                                                            <?php

                                                          }else {
                                                            ?>
                                                            <i class="fa fa-file-text-o text-warning"></i>
                                                            <?php
                                                          }

                                                           ?>



                                                        </div>
                                                        <div class="p-2 small">
                                                          <?php
                                                          if($typefiles=="csv"||$typefiles=="xlsx"||$typefiles=="xls")
                                                          {
                                                            ?>

                                                            <div style="text-align:center"> <span class="label label-success" ><?php echo $tabStudent[18]; ?></span> </div>
                                                            <?php

                                                          }else if($typefiles=="docx"||$typefiles=="docx")
                                                          {
                                                            ?>

                                                            <div style="text-align:center"> <span class="label label-primary" ><?php echo $tabStudent[18]; ?></span> </div>
                                                            <?php
                                                          }else if($typefiles=="pdf")
                                                          {
                                                            ?>
                                                            <div style="text-align:center"> <span class="label label-warning" ><?php echo $tabStudent[18]; ?></span> </div>
                                                            <?php

                                                          }else {
                                                            ?>

                                                            <div style="text-align:center"> <span class="label label-warning" ><?php echo $tabStudent[18]; ?></span> </div>
                                                            <?php
                                                          }
                                                           ?>
                                                           <br>

                                                            <div class="text-muted" style="text-align:center"><?php echo L::ParentalContrat ?></div>

                                                            <div class="pull-right">

                                                              <a target="_blank" href="<?php echo $lien ?>" class="btn btn-xs btn-warning"> <i class="fa fa-download"></i> </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                    </div>


                                    <?php
                                  }
                                   ?>


                                <?php
                              }else {
                                ?>

                                    <a href="medicalformadd.php?compte=<?php echo $tabStudent[0] ?>" class="btn btn-success btn-md"><i class="fa fa-plus"></i> <?php echo L::MedicalInfosTab ?></a>

                                <?php
                              }
                               ?>



                          </div>

                        </div>

                        <div class="tab-pane" id="tab2">
                          <div class="container-fluid">

                            <div class="row">
                                <div class="full-width p-l-20">
                                    <div class="panel">
                                      <?php
                                        if($_SESSION['user']['fonctionuser']=="Coordinnateur"|| $_SESSION['user']['fonctionuser']=="Administrateur"|| $_SESSION['user']['fonctionuser']=="Superviseur"||$_SESSION['user']['fonctionuser']=="Comptable")
                                        {
                                          ?>
                                          <div class="pull-right">
                                              <a href="scolaritestudent.php?student=<?php echo $_GET['compte'] ?>&classe=<?php echo $tabStudent[15]; ?>" class="btn btn-primary btn-md"><i class="fa fa-plus"></i> <?php echo L::AddVersements ?>t</a>
                                          </div>
                                          <?php
                                        }
                                       ?>

                                      <br><br>
                                      <div class="row">
                                        <div class="col-md-12">
                                        <span class="label label-md label-info" style="text-align:center"><?php echo L::HistoriqdepensesInscri ?></span>
                                        </div>

                                      </div></br></br>
                                      <?php
                                      //compter le nombre de versement concernant l'inscription de cet eleve
                                      $datasInscriptions=$etabs->getAllInscriptionVersmentChild($_SESSION['user']['codeEtab'],$libellesessionencours,$_GET['compte'],$tabStudent[15]);
                                      // var_dump($datasInscriptions);
                                      $nbdatainscription=count($datasInscriptions);
                                      if($nbdatainscription==0)
                                      {
                                        ?>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">

                                     <?php echo L::NoInscrivers ?>

                                      <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                                      <span aria-hidden="true">&times;</span>

                                         </a>

                                      </div>
                                        <?php
                                      }else {
                                        ?>
                                        <table id="example1" style="width:100%;">

                                            <thead>

                                                <tr>

                                                    <th><?php echo L::Versements ?> </th>

                                                    <th><?php echo L::LittleDateVersements ?></th>

                                                    <th><?php echo L::AmountVerser ?></th>

                                                    <th><?php echo L::AmountReste ?></th>



                                            </thead>

                                            <tbody>

                                              <?php

                                              $j=1;

                                              foreach ($datasInscriptions as $value):

                                               ?>

                                                <tr>

                                                    <td><span class="label label-sm label-info"><?php echo $value->code_versement; ?></span></td>

                                                    <td><?php echo date_format(date_create($value->date_versement),"d-m-Y")?></td>

                                                    <td><?php echo $value->montant_versement. " ".$value->devise_versement;?></td>

                                                    <td><?php

                                                    $resteapayer=$value->solde_versement;

                                                    if($resteapayer==0)

                                                    {

                                                      ?>

                                                      <span class="label label-sm label-success"><?php echo L::AmountSolder ?></span>

                                                      <?php

                                                    }else if($resteapayer>0)

                                                    {

                                                      ?>

                                                       <span class="label label-sm label-danger"><?php echo $value->solde_versement. " ".$value->devise_versement;?></span>

                                                      <?php

                                                    }

                                                    ?></td>



                                                </tr>

                                              <?php

                                              $j++;

                                              endforeach;

                                               ?>



                                            </tbody>

                                        </table>
                                        <?php
                                      }
                                       ?>
                                     </br></br> <div class="row">
                                        <div class="col-md-12">
                                        <span class="label label-md label-info" style="text-align:center"><?php echo L::Historiqdepenses ?></span>
                                        </div>

                                      </div></br></br>
                                      <?php
                                      //liste des versement concernant la scolarité

                                      $datasScolarites=$etabs->getAllScolaritesVersmentChild($_SESSION['user']['codeEtab'],$libellesessionencours,$_GET['compte'],$tabStudent[15]);
                                      // var_dump($datasInscriptions);
                                      $nbdatascolarites=count($datasScolarites);

                                      if($nbdatascolarites==0)
                                      {
                                        ?>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">

                                    <?php echo L::Nodepenses ?>

                                      <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                                      <span aria-hidden="true">&times;</span>

                                         </a>

                                      </div>
                                        <?php
                                      }else {
                                        ?>
                                        <table id="example1" style="width:100%;">

                                            <thead>

                                                <tr>

                                                    <th><?php echo L::Versements ?> </th>

                                                    <th><?php echo L::LittleDateVersements ?></th>

                                                    <th><?php echo L::AmountVerser ?></th>

                                                    <th><?php echo L::AmountReste ?></th>



                                            </thead>

                                            <tbody>

                                              <?php

                                              $j=1;

                                              foreach ($datasScolarites as $value):

                                               ?>

                                                <tr>

                                                    <td><span class="label label-sm label-info"><?php echo $value->code_versement; ?></span></td>

                                                    <td><?php echo date_format(date_create($value->date_versement),"d-m-Y")?></td>

                                                    <td><?php echo $value->montant_versement. " ".$value->devise_versement;?></td>

                                                    <td><?php

                                                    $resteapayer=$value->solde_versement;

                                                    if($resteapayer==0)

                                                    {

                                                      ?>

                                                      <span class="label label-sm label-success"><?php echo L::AmountSolder ?></span>

                                                      <?php

                                                    }else if($resteapayer>0)

                                                    {

                                                      ?>

                                                       <span class="label label-sm label-danger"><?php echo $value->solde_versement. " ".$value->devise_versement;?></span>

                                                      <?php

                                                    }

                                                    ?></td>



                                                </tr>

                                              <?php

                                              $j++;

                                              endforeach;

                                               ?>



                                            </tbody>

                                        </table>
                                        <?php
                                      }
                                       ?>
                                    <!-- </br></br>  <div class="row">
                                        <div class="col-md-12">
                                        <span class="label label-md label-info" style="text-align:center">HISTORIQUE DES VERSEMENTS CANTINES</span>
                                        </div>

                                      </div></br></br> -->

                                      <?php
                                      //liste des versement concernant la cantine

                                      $datasCantines=$etabs->getAllCantinesVersmentChild($_SESSION['user']['codeEtab'],$libellesessionencours,$_GET['compte'],$tabStudent[15]);
                                      // var_dump($datasInscriptions);
                                      $nbdatascantines=count($datasCantines);

                                      if($nbdatascantines==0)
                                      {
                                        ?>
                                        <!-- <div class="alert alert-danger alert-dismissible fade show" role="alert">

                                     Aucun Paiement des frais de cantine à ce jour

                                      <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                                      <span aria-hidden="true">&times;</span>

                                         </a>

                                      </div> -->
                                        <?php
                                      }else {
                                        ?>
                                        <!-- <table id="example1" style="width:100%;">

                                            <thead>

                                                <tr>

                                                    <th>Versements </th>

                                                    <th>Date</th>

                                                    <th>Montant Versé</th>





                                            </thead>

                                            <tbody>

                                              <?php

                                            //  $j=1;

                                            //  foreach ($datasCantines as $value):

                                               ?>

                                                <tr>

                                                    <td><span class="label label-sm label-info"><?php //echo $value->code_versement; ?></span></td>

                                                    <td><?php //echo date_format(date_create($value->date_versement),"d-m-Y")?></td>

                                                    <td><?php //echo $value->montant_versement. " ".$value->devise_versement;?></td>





                                                </tr>

                                              <?php

                                            //  $j++;

                                            //  endforeach;

                                               ?>



                                            </tbody>

                                        </table> -->
                                        <?php
                                      }
                                       ?>
                                     </br></br> <div class="row">
                                        <div class="col-md-12">
                                        <span class="label label-md label-info" style="text-align:center"><?php echo L::HistoriqAESVers ?></span>
                                        </div>

                                      </div></br></br>
                                      <?php
                                      //liste des versement concernant les frais AES

                                      $datasAes=$etabs->getAllAesVersmentChild($_SESSION['user']['codeEtab'],$libellesessionencours,$_GET['compte'],$tabStudent[15]);
                                      // var_dump($datasInscriptions);
                                      $nbdatasAes=count($datasAes);

                                      if($nbdatasAes==0)
                                      {
                                        ?>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">

                                     <?php echo L::NoAESVers ?>

                                      <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                                      <span aria-hidden="true">&times;</span>

                                         </a>

                                      </div>
                                        <?php
                                      }else {
                                        ?>
                                        <table id="example1" style="width:100%;">

                                            <thead>

                                                <tr>

                                                    <th><?php echo L::Versements ?> </th>

                                                    <th><?php echo L::LittleDateVersements ?></th>

                                                    <th><?php echo L::AmountVerser ?></th>

                                                    <!-- <th>Reste a payer</th> -->



                                            </thead>

                                            <tbody>

                                              <?php

                                              $j=1;

                                              foreach ($datasCantines as $value):

                                               ?>

                                                <tr>

                                                    <td><span class="label label-sm label-info"><?php echo $value->code_versement; ?></span></td>

                                                    <td><?php echo date_format(date_create($value->date_versement),"d-m-Y")?></td>

                                                    <td><?php echo $value->montant_versement. " ".$value->devise_versement;?></td>





                                                </tr>

                                              <?php

                                              $j++;

                                              endforeach;

                                               ?>



                                            </tbody>

                                        </table>
                                        <?php
                                      }

                                       ?>

                                    </div>
                                </div>

                          </div>

                          </div>

                        </div>

                        <div class="tab-pane" id="tab3">

                          <div class="container-fluid">

                            <div class="row">
                                <div class="full-width p-l-20">
                                    <div class="panel">


                                      <div class="row">
                                        <div class="col-md-12" style="text-align:center">
                                        <span class="label label-md md-12 label-info" style="text-align:center"><?php echo L::BulletinName ?></span>
                                        </div>

                                      </div></br>

                                      <div class="profile-userbuttons">

    <a   class="btn btn-circle btn-primary btn-sm" href="#"> <i class="fa fa-file-text-o"></i><?php echo L::firstsemester ?>
    </a>
    <a   class="btn btn-circle btn-primary btn-sm" href="#"> <i class="fa fa-file-text-o"></i><?php echo L::secondsemester ?>
    </a>
    <a   class="btn btn-circle btn-primary btn-sm" href="#"> <i class="fa fa-file-text-o"></i><?php echo L::thirstsemester ?>
    </a>
                                </div>
                                </br></br>



                                    </div>
                                </div>

                          </div>

                          </div>

                        </div>
                        <div class="tab-pane  fontawesome-demo" id="tab4">

                          <div id="biography" >

                            <div class="row">

                              <div class="row">
                                <div class="col-md-12">
                                <span class="label label-md label-info" style="text-align:center"><?php echo L::HistoriqAttendance ?></span>
                                </div>
                              </div>

                            </div></br>
                            <?php
                            $nbabseces=count($allabsencesLast);
                            if($nbabseces==0)
                            {
                              ?>

                              <?php

                            }else {
                              ?>
                              <!-- <div class="table-responsive"> -->

                                  <table class="table table-striped custom-table table-hover">

                                      <thead>
                                        <th><?php echo L::LittleDateVersements ?>:</th>
                                        <th><?php echo L::MatiereMenusingle ?> :</th>
                                        <!-- <th>Coefficient:</th> -->
                                        <th><?php echo L::HoursStart ?> :</th>
                                        <th><?php echo L::HourEnd ?>:</th>



                                      </thead>

                                      <tbody>
                                        <?php
                                        foreach ($allabsencesLast as $value):
                                         ?>

                                          <tr>

                                              <td><?php echo $value->date_presence; ?></td>
                                              <td><?php echo $etabs->getMatiereLibelleByIdMat($value->matiere_presence,$value->codeEtab_presence); ?></td>
                                              <!-- <td ><?php //echo $value->tel_compte; ?></td> -->
                                              <td ><?php echo $value->heuredeb_heure; ?></td>
                                              <td ><?php echo $value->heurefin_heure; ?></td>






                                          </tr>

                                        <?php
                                      endforeach;
                                         ?>
                                      </tbody>

                                  </table>
                                  <?php
                                }
                                 ?>



                          </div>

                        </div>

                        <div class="tab-pane  fontawesome-demo" id="tab5">
                          <div id="biography" >

                            <div class="row">

                              <div class="row">
                                <div class="col-md-12">
                                <span class="label label-md label-info" style="text-align:center"><?php echo L::InformationsLogin ?></span>
                                </div>
                              </div>

                            </div></br>
                            <form class="form-control" action="../controller/compte.php" method="post" id="Formcnx">
                              <div class="form-body">

                                <div class="form-group row">
                                     <label class="control-label col-md-3"><?php echo L::Logincnx ?>
                                         <span class="required">*  </span>
                                     </label>
                                     <div class="col-md-6">
                                         <input name="loginTea" id="loginTea" type="text" placeholder="<?php echo L::LoginEnter ?> " <?php echo $tabStudent[13];?> class="form-control t" /> </div>
                                 </div>
                                 <div class="form-group row">
                                     <label class="control-label col-md-3"><?php echo L::Passcnx ?>
                                         <span class="required">*  </span>
                                     </label>
                                     <div class="col-md-6">
                                         <input name="passTea" id="passTea" type="password" placeholder="<?php echo L::PasswordEnter ?> " class="form-control " /> </div>
                                 </div>
                                 <div class="form-group row">
                                     <label class="control-label col-md-3"><?php echo L::ConfirmPasscnx ?>
                                         <span class="required">*  </span>
                                     </label>
                                     <div class="col-md-6">
                                         <input name="confirmTea" id="confirmTea" type="password" placeholder="<?php echo L::ConfirmPassword ?> " class="form-control " /> </div>
                                         <input type="hidden" name="etape" id="etape" value="1"/>
                                          <input type="hidden" name="idcompte" id="idcompte" value="<?php echo $_GET['compte'];?>"/>
                                          <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $_SESSION['user']['codeEtab'];?>"/>

                                 </div>
                                 <div class="form-actions">
                                                       <div class="row">
                                                           <div class="offset-md-3 col-md-9">
                                                               <button class="btn btn-success" type="submit"><?php echo L::ModifierBtn ?></button>
                                                               <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                           </div>
                                                         </div>
                                                      </div>

                              </div>

                            </form>


                          </div>
                        </div>



                      </div>

                    </div>

                  </div>

              </div>

            </div>


            							</div>



          </div>





                     <!-- start new patient list -->



                    <!-- end new patient list -->



                </div>

            </div>

            <!-- end page content -->

            <!-- start chat sidebar -->

            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>

            <!-- end chat sidebar -->

        </div>

        <!-- end page container -->

        <!-- start footer -->

        <div class="page-footer">

            <div class="page-footer-inner"> 2019 &copy;

            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>

            </div>

            <div class="scroll-to-top">

                <i class="material-icons">eject</i>

            </div>

        </div>

        <!-- end footer -->



    <!-- start js include path -->

    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
  <script src="../assets2/js/dropify.js"></script>
  <script src="../assets2/plugins/select2/js/select2.js" ></script>
  <script src="../assets2/js/pages/select2/select2-init.js" ></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
  <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>







    <!-- morris chart -->

    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>

    <script src="../assets2/plugins/morris/raphael-min.js" ></script>

    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->



   <script>
   // function SetcodeEtab(codeEtab)
   // {
   //   var etape=3;
   //   $.ajax({
   //     url: '../ajax/sessions.php',
   //     type: 'POST',
   //     async:false,
   //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
   //     dataType: 'text',
   //     success: function (content, statut) {
   //
   // window.location.reload();
   //
   //     }
   //   });
   // }
   function changecarnet()
   {
     $("#fileInput2").click();
   }
   function changecontrats()
   {
      $("#fileInput1").click();
   }

   function changepictures()
   {
     $("#fileInput").click();
   }

   function UploadContratsMod()
   {
     Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::DoyouUploaderThisContrats ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::Uploader ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {


  $("#FormFile1").submit();

}else {

}
})
   }

   function UploadCarnets()
   {
     Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::DoyouUploadCarnetSante ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::Uploader ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {


  $("#FormFile2").submit();

}else {

}
})
   }

   function UploadContrats()
   {
     Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::DoyouUploaderThisContrats ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::Uploader ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {


  $("#FormFile1").submit();

}else {

}
})
   }

   function UploadPictures()
   {
     Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::DoyouUploaderThisImage ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::Uploader ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {


  $("#FormFile").submit();

}else {

}
})
   }


   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }


   function generatefichepdf(idcompte)

   {

     var codeEtab="<?php echo $_SESSION['user']['codeEtab'] ?>";

      var etape=2;

       $.ajax({

         url: '../ajax/admission.php',

         type: 'POST',

         async:false,

         data: 'compte=' +idcompte+ '&etape=' + etape+'&codeEtab='+ codeEtab,

         dataType: 'text',

         success: function (content, statut) {



          window.open(content, '_blank');



         }

       });

   }



jQuery(document).ready(function() {


  $("#addclientnumber").validate({

    errorPlacement: function(label, element) {
    label.addClass('mt-2 text-danger');
    label.insertAfter(element);
  },
  highlight: function(element, errorClass) {
    $(element).parent().addClass('has-danger')
    $(element).addClass('form-control-danger')
  },
  success: function (e) {
        $(e).closest('.control-group').removeClass('error').addClass('info');
        $(e).remove();
    },
     rules:{

       clientnumberadd:"required"



     },
     messages: {

       clientnumberadd:"<?php echo L::PleaseAddClientNumbers?>"

     },
     submitHandler: function(form) {

       var etape=8;
       var studentid="<?php echo $_GET['compte'] ?>";
       var numclient=$("#addclientnumber #clientnumberadd").val();

       $.ajax({
         url: '../ajax/compte.php',
         type: 'POST',
         async:false,
         data: 'studentid=' +studentid+ '&etape=' + etape+'&numclient='+numclient,
         dataType: 'text',
         success: function (response, statut) {

window.location.reload();

         }
       });


     }


  });

  $("#Modclientnumber").validate({

    errorPlacement: function(label, element) {
    label.addClass('mt-2 text-danger');
    label.insertAfter(element);
  },
  highlight: function(element, errorClass) {
    $(element).parent().addClass('has-danger')
    $(element).addClass('form-control-danger')
  },
  success: function (e) {
        $(e).closest('.control-group').removeClass('error').addClass('info');
        $(e).remove();
    },
     rules:{

       clientnumbermod:"required"



     },
     messages: {

       clientnumbermod:"<?php echo L::PleaseAddClientNumbers;?>"

     },
     submitHandler: function(form) {

       var etape=9;
       var studentid="<?php echo $_GET['compte'] ?>";
       var numclient=$("#Modclientnumber #clientnumbermod").val();

       $.ajax({
         url: '../ajax/compte.php',
         type: 'POST',
         async:false,
         data: 'studentid=' +studentid+ '&etape=' + etape+'&numclient='+numclient,
         dataType: 'text',
         success: function (response, statut) {

window.location.reload();

         }
       });
     }


  });

$("#Formcnx").validate({

  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
  },
  highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
  },
  success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
   rules:{
     loginTea:"required",
     passTea: {
         required: true,
         minlength: 6
     },
     confirmTea:{
         required: true,
         minlength: 6,
         equalTo:'#passTea'
     }
   },
   messages: {
     loginTea:"<?php echo L::Logincheck ?>",
     confirmTea:{
         required:"<?php echo L::Confirmcheck ?>",
         minlength:"<?php echo L::Confirmincheck ?>",
         equalTo: "<?php echo L::ConfirmSamecheck ?>"
     },
     passTea: {
         required:"<?php echo L::Passcheck ?>",
         minlength:"<?php echo L::Confirmincheck ?>"
     }
   },
   submitHandler: function(form) {
     //verifier si ce compte n'existe pas encore dans la base de données
    // form.submit();

    var login=$("#loginTea").val();
    var pass=$("#passTea").val();
    var studentid="<?php echo $_GET['compte'] ?>";
    var etape=1;

    $.ajax({
      url: '../ajax/compte.php',
      type: 'POST',
      async:false,
      data: 'login=' +login+ '&etape=' + etape+'&pass='+pass+'&studentid='+studentid,
      dataType: 'text',
      success: function (response, statut) {



      }
    });


   }

});



 });



   </script>

    <!-- end js include path -->

  </body>



</html>
