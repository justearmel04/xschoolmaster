<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

if(strlen($_SESSION['user']['codeEtab'])>0)
{
  $codeEtabAssigner=$_SESSION['user']['codeEtab'];
}else {
  $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
}
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//nous allons chercher la liste des etablissements du groupe

$allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
$agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$libellesessionencours="";

  if($etablissementType==1||$etablissementType==3)
  {
    if($nbsessionOn>0){
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];

    }

  }else {
    if($nbsessionOn>0)
    {
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];
      $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
      $typesemestrealls=$session->getAllsemestrebyIdsession($sessionencoursid);
      $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
    }
  }

  if(strlen($_SESSION['user']['codeEtab'])>0)
  {

  }else {
    $_SESSION['user']['codeEtab']=$codeEtabAssigner;
  }

  $_SESSION['user']['session']=$libellesessionencours;


  $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

  $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


  // var_dump($allcodeEtabs);
  $nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
// echo $libellesessionencours;
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);

$parascolairesAdd=$etabs->getExtrascolairesAdd($_SESSION['user']['codeEtab'],$libellesessionencours);


// var_dump($parascolairesAdd);




 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
    <!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!--bootstrap -->
    <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

    <!-- data tables -->
    <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>

    <!-- Material Design Lite CSS -->
    <link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
    <link href="../assets2/css/material_style.css" rel="stylesheet">

    <!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- wizard -->
    	<link rel="stylesheet" href="../assets2/css/pages/steps.css">

    <!-- Theme Styles -->
      <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
      <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="../assets2/css/pages/inbox.min.css" rel="stylesheet" type="text/css" />
    <!-- <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet"> -->

      <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <!-- Theme Styles -->

    <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />





 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::NewExtrascolaire ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="allparascolaires.php"><?php echo L::ExtraActivities ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::NewparaActMenu ?></li>
                          </ol>
                      </div>
                  </div>
                  <?php

                        if(isset($_SESSION['user']['addprogra']))
                        {

                          ?>
                          <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?php
                        //echo $_SESSION['user']['addetabok'];
                        ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                           </a>
                        </div-->
                        <div class="alert alert-danger" role="alert">
          <?php echo $_SESSION['user']['addprogra'] ?>
        </div>


                          <?php
                          unset($_SESSION['user']['addprogra']);
                        }

                         ?>
					<!-- start widget -->
          <div class="inbox">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-topline-gray">
                                <div class="card-body no-padding height-9">
									<div class="row">
			                            <!--div class="col-md-3">
			                                <div class="inbox-sidebar">
			                                    <a href="email_compose.html" data-title="Compose" class="btn red compose-btn btn-block m-0">
			                                        <i class="fa fa-edit"></i> Compose </a>
			                                    <ul class="inbox-nav inbox-divider">
			                                        <li class="active"><a href="#"><i
															class="fa fa-inbox"></i> Inbox <span
															class="label mail-counter-style label-danger pull-right">2</span></a>
			                                        </li>
			                                        <li><a href="#"><i
															class="fa fa-envelope"></i> Sent Mail</a>
			                                        </li>
			                                        <li><a href="#"><i
															class="fa fa-briefcase"></i> Important</a>
			                                        </li>
			                                        <li><a href="#"><i
															class="fa fa-star"></i> Starred </a>
			                                        </li>
			                                        <li><a href="#"><i
															class=" fa fa-external-link"></i> Drafts <span
															class="label mail-counter-style label-info pull-right">30</span></a>
			                                        </li>
			                                        <li><a href="#"><i
															class=" fa fa-trash-o"></i> Trash</a>
			                                        </li>
			                                    </ul>
			                                    <ul class="nav nav-pills nav-stacked labels-info inbox-divider">
			                                        <li>
			                                            <h4>Labels</h4>
			                                        </li>
			                                        <li><a href="#"><i
															class="fa fa-tags text-info"></i>  Work</a>
			                                        </li>
			                                        <li>
			                                            <a href="#">
			                                                <i class=" fa fa-tags text-warning"></i> Design
			                                            </a>
			                                        </li>
			                                        <li>
			                                            <a href="#">
			                                                <i class=" fa fa-tags text-danger text-success"></i> Family
			                                            </a>
			                                        </li>
			                                        <li>
			                                            <a href="#">
			                                                <i class=" fa fa-tags text-purple"></i> Friends
			                                            </a>
			                                        </li>
			                                        <li>
			                                            <a href="#">
			                                                <i class=" fa fa-tags "></i> Office
			                                            </a>
			                                        </li>
			                                    </ul>
			                                    <ul class="nav nav-pills nav-stacked labels-info inbox-divider ">
			                                        <li>
			                                            <h4>Buddy online</h4>
			                                        </li>
			                                        <li>
			                                            <a href="#">
			                                                <i class=" fa fa-comments text-success"></i> Jhone Doe
			                                               <span class="online-status">I do not think</span>
			                                            </a>
			                                        </li>
			                                        <li>
			                                            <a href="#">
			                                                <i class=" fa fa-comments text-danger"></i> Sumon
			                                                <span class="online-status">Busy with coding</span>
			                                            </a>
			                                        </li>
			                                        <li>
			                                            <a href="#">
			                                                <i class=" fa fa-comments text-purple "></i> Anjelina Joli
			                                                <span class="online-status">I out of control</span>
			                                            </a>
			                                        </li>
			                                        <li>
			                                            <a href="#">
			                                                <i class=" fa fa-comments text-success "></i> Jonathan Smith
			                                                <span class="online-status">I am not here</span>
			                                            </a>
			                                        </li>
			                                        <li>
			                                            <a href="#">
			                                                <i class=" fa fa-comments text-info "></i> Tawseef
			                                                <span class="online-status">I do not think</span>
			                                            </a>
			                                        </li>
			                                    </ul>
			                                </div>
			                            </div-->



			                            <div class="col-md-8 col-sm-12 col-lg-8">
                                    <?php
                                    if(isset($_SESSION['user']['messages']))
                                    {
                                      ?>
                                      <div class="alert alert-danger" role="alert">
                                      <?php echo $_SESSION['user']['messages']; ?>
                                    </div>
                                      <?php
                                      unset($_SESSION['user']['messages']);
                                      // unset($_SESSION['user']['rappelid']);
                                    }
                                     ?>

			                                <div class="inbox-body">
		                                    <div class="inbox-header">
		                                        <div class="mail-option">
		                                            <!-- <div class="btn-group margin-top-20 ">
		                                                <button class="btn btn-primary btn-sm margin-right-10"><i class="fa fa-check"></i> Send</button>
		                                                <button class="btn btn-sm btn-default margin-right-10"><i class="fa fa-times"></i> Discard</button>
		                                                <button class="btn btn-sm btn-default margin-right-10">Draft</button>
		                                            </div> -->
		                                        </div>
		                                    </div>
                                        <form class="" action="../controller/summernotes.php" method="post" id="FormAddExam" enctype="multipart/form-data" >

                                              <div class="form-group">
                                                 <label class="control-label col-md-11"><?php echo L::DenominationfrenchActivity?>
                                                     <span class="required"> * </span>
                                                 </label>
                                                 <div class="col-md-12">
                                                   <input  placeholder="<?php echo L::ParaActivityDenomination ?>" name="denomination" id="denomination"  value="<?php if(isset($_SESSION['user']['denomination'])){echo $_SESSION['user']['denomination'];} ?>" class="form-control">
                                                   <input type="hidden" name="etape" id="etape"  value="1">
                                                   <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $_SESSION['user']['codeEtab']; ?>">
                                                   <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                                   <input type="hidden" name="gratuitcheck" id="gratuitcheck" value="0">
                                                   <input type="hidden" name="paiecheck" id="paiecheck" value="1">
                                                  </div>
                                                 </div>

                                                 <div class="form-group">
                                                    <label class="control-label col-md-11"><?php echo L::DenominationenglishActivity ?>
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-12">
                                                      <input  placeholder="<?php echo L::ParaActivityDenomination ?>" name="denominationen" id="denominationen"  value="<?php if(isset($_SESSION['user']['denominationen'])){echo $_SESSION['user']['denominationen'];} ?>" class="form-control">

                                                     </div>
                                                    </div>


                                <div class="form-group ">
                                      <label class="control-label col-md-12"><?php echo L::Trimestrialmontant ?>
                                          <span class="required"> * </span>
                                      </label>
                                          <div class="col-md-12">
                                              <input type="text"  name="montantAct" id="montantAct" data-required="1" placeholder="<?php echo L::AmountEnterAvtivity ?>"  value="<?php if(isset($_SESSION['user']['montantAct'])){echo $_SESSION['user']['montantAct'];} ?>" class="form-control" >

                                          </div>
                                </div>
                                <div class="form-group ">
                                      <label class="control-label col-md-12"><?php echo L::Annualmontant ?>
                                          <span class="required"> * </span>
                                      </label>
                                          <div class="col-md-12">
                                              <input type="text"  name="montantActannual" id="montantActannual" data-required="1" placeholder="<?php echo L::AmountEnterAvtivity ?>"  value="<?php if(isset($_SESSION['user']['montantActannual'])){echo $_SESSION['user']['montantAct'];} ?>" class="form-control" >

                                          </div>
                                </div>
                                <div class="form-group" id="rowprecision">
                                   <label class="control-label col-md-12"> <?php echo L::destinatairestypes ?>
                                       <span class="required"> * </span>
                                   </label>
                                   <div class="col-md-12">
                                     <select class="form-control " id="precis" name="precis" style="width:100%" onchange="selectionprecis()" >
                                        <option value="" > <?php echo L::	selectdestinatairestypes ?> </option>
                                        <option value="1" selected ><?php echo L::NoPrecisNotifications ?></option>
                                        <option value="2"><?php echo L::PrecisNotifications ?></option>

                                    </select>
                                     </div>
                            </div>

                            <div class="form-group" id="rowprecisioneleves">
                               <label class="control-label col-md-12"> <?php echo L::studMenu ?>
                                   <span class="required"> * </span>
                               </label>
                               <div class="col-md-12">
                                 <select class="form-control " multiple="multiple" id="eleves" name="eleves[]" style="width:100%"  >



                                </select>
                                 </div>
                        </div>
                        <div class="form-group" id="rowprecisiondestinataires">

                           <div class="col-md-12">
                             <select class="form-control " id="precisiondestinataires" multiple="multiple" name="precisiondestinataires[]" style="width:100%"  >


                            </select>
                             </div>
                    </div>


		                                    <div class="inbox-body no-pad">
		                                        <div class="mail-list">

		                                            <div class="compose-mail">
                                                  <span style="text-align:center"><?php echo L::Avtivitydescription ?></span>
		                                                <!-- <form method="post"> -->
                                                    <div class="compose-editor">

		                                                        <!-- <div id="summernote"></div> -->
                                                            <textarea class="form-control" name="summernote" id="summernote" style="height: 400px;"></textarea>
		                                                        <input type="file" class="default" id="fichier" name="fichier"  multiple>
		                                                    </div>
		                                                    <div class="btn-group margin-top-20 " style="text-align:center">
				                                                <button class="btn btn-success btn-sm margin-right-10" onclick="changeEtape(9)"><i class="fa fa-check"></i> <?php echo L::Saving ?></button>
				                                                <!-- <button class="btn btn-sm btn-default margin-right-10"><i class="fa fa-times"></i> Discard</button> -->
		                                                		<button class="btn btn-sm btn-primary margin-right-10" onclick="changeEtape(3)"style="display:none"><?php echo L::SaveAndsendAfterNotifications ?></button>
		                                            		    </div>
		                                                <!-- </form> -->
		                                            </div>
		                                        </div>
		                                    </div>
                                      </form>
		                                </div>
			                            </div>
                                  <div class="col-sm-12 col-md-4 col-lg-4">

                                    <div class="card ">

                                                          <div class="card-body no-padding height-9">
                                                            <ul class="list-group list-group-unbordered">
                                                              <li class="list-group-item" style="text-align:center">

                                                                  <b><?php echo L::ExtraActivitiesliste ?></b> <a class="pull-right"></a>

                                                              </li>


                                                              </ul>

                                                              <!-- END SIDEBAR USER TITLE -->

                                                              <!-- SIDEBAR BUTTONS -->

                                                              <?php
                                                            foreach ($parascolairesAdd as $values):

                                                              ?>

                                                              <div class="profile-userbuttons col-md-12">

                                                                <?php

                                                                if($_SESSION['user']['lang']=="fr")
                                                                {
                                                                  ?>
                                                                  <a   class="btn  btn-success btn-sm " data-toggle="modal" data-target="#largeModel1"  onclick="detailspara(<?php echo $values->id_act ?>)"><?php echo $values->libelle_act;?></a>

                                                                  <?php

                                                                }else {
                                                                  ?>
                                                                    <a   class="btn  btn-success btn-sm " data-toggle="modal" data-target="#largeModel1" onclick="detailsparaen(<?php echo $values->id_act ?>)"><?php echo $values->libelleen_act;?></a>
                                                                  <?php
                                                                }
                                                                 ?>






                                <!--button type="button" class="btn btn-circle red btn-sm">Message</button-->

                                                              </div>

                                                              <?php


                                                            endforeach;


                                                               ?>





                                                              <!-- END SIDEBAR BUTTONS -->

                                                          </div>

                                                      </div>

                                                      <div class="modal fade" id="largeModel1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                      <div class="modal-dialog modal-md" role="document">
                                                          <div class="modal-content">
                                                              <div class="modal-header">
                                                                  <h4 class="modal-title" id="exampleModalLabel"><?php echo L::DetailsActivities ?></h4>
                                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                      <span aria-hidden="true">&times;</span>
                                                                  </button>
                                                              </div>
                                                              <div class="modal-body">
                                                                <div id="divrecherchestation">

                                                                  <div class="row">



                                                                      <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                                                                        <div class="blogThumb">
                                                                          <div class="thumb-center"><img class="img-responsive" id="imageinfo" alt="user"
                                                                              src="../photo/course2.jpg"></div>
                                                                          <div class="course-box">

                                                                            <div class="" style="text-align:center">
                                                                              <h4>

                                                                                      <!-- <span class="label label-lg label-warning" id="spaninfo"  style="text-align:center;">fffffffffff</span> -->


                                                                              </h4>
                                                                            </div>
                                                                          </div>

                                                                            <div class="col-md-12">
                                                                             <div class="form-group">
                                                                 <label for="libellecourse1"><?php if($_SESSION['user']['lang']=="fr"){echo L::DenominationfrenchActivity;}else{echo L::DenominationenglishActivity;} ?><span class="required">*</span> :</label>
                                                                        <input type="text" class="form-control" name="libellecourse1" id="libellecourse1" value="" size="32" maxlength="225" />

                                                                                 </div>
                                                                              </div>
                                                                              <div class="col-md-12">
                                                                               <div class="form-group">
                                                                   <label for="libellecourse2"><?php echo L::Trimestrialmontant ?><span class="required">*</span> :</label>
                                                                          <input type="text" class="form-control" name="libellecourse2" id="libellecourse2" value="" size="32" maxlength="225" />

                                                                                   </div>
                                                                                </div>
                                                                          <div class="col-md-12">
                                                                           <div class="form-group">
                                                               <label for="libellecourse3"><?php echo L::Annualmontant ?><span class="required">*</span> :</label>
                                                                      <input type="text" class="form-control" name="libellecourse3" id="libellecourse3" value="" size="32" maxlength="225" />

                                                                               </div>
                                                                            </div>
                                                                          <div class="form-group">


                                                                          <textarea class="form-control descri" name="comment" id="comment" rows="5" placeholder="" ></textarea>

                                                                          </div>

                                                                        </div>
                                                                      </div>




                                                                  </div>

                                                                </div>
                                                              </div>
                                                              <div class="modal-footer">
                                                                  <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closebtn"><?php echo L::Closebtn  ?></button>
                                                                  <button type="button" style="display:none" class="btn btn-primary" onclick="validerMember()" id="btnsaving"><?php echo L::Saving ?></button>
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>


                                  </div>

			                        </div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- corps -->

            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
    <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
	<script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js"></script>
    <!-- bootstrap -->
<script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
<script src="../assets2/plugins/select2/js/select2.js" ></script>
<script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <!-- wizard -->
  <script src="../assets2/plugins/steps/jquery.steps.js" ></script>
 <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
 <script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
 <script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
     <!-- calendar -->
  <script src="../assets2/plugins/moment/moment.min.js" ></script>
  <script src="../assets2/js/app.js" ></script>
  <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/jquery-dateformat.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
  <script src="../assets/js/formatter/jquery.formatter.min.js"></script>
  <script src="../assets2/plugins/summernote/summernote.js" ></script>


  <script type="text/javascript">

  function detailspara(id)
  {
    var codeEtab="<?php echo $_SESSION['user']['codeEtab'] ?>";
    var sessionEtab="<?php echo $libellesessionencours ?>";
    var etape=2;
    $.ajax({
       url: '../ajax/parascolaires.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&codeEtab=' +codeEtab+'&sessionEtab='+sessionEtab+'&activities='+id,
       dataType: 'text',
       success: function (content, statut) {

         var libelle=content.split("*")[0];
         var trimestre=content.split("*")[1];
         var annuel=content.split("*")[2];
         var description=content.split("*")[3];
         var file=content.split("*")[4];
         var fichier=content.split("*")[5];

         $("#divrecherchestation #libellecourse1").val(libelle);
         $("#divrecherchestation #libellecourse2").val(trimestre);
         $("#divrecherchestation #libellecourse3").val(annuel);
         // $("#divrecherchestation #comment").val(description);
          $("#divrecherchestation #comment").summernote('code', description);

          if(file==1)
          {
            $("#imageinfo").attr('src',fichier);
          }


   // window.location.reload();

       }
     });
  }


  function detailsparaen(id)
  {
    var codeEtab="<?php echo $_SESSION['user']['codeEtab'] ?>";
    var sessionEtab="<?php echo $libellesessionencours ?>";
    var etape=3;
    $.ajax({
       url: '../ajax/parascolaires.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&codeEtab=' +codeEtab+'&sessionEtab='+sessionEtab+'&activities='+id,
       dataType: 'text',
       success: function (content, statut) {

   // window.location.reload();

       }
     });
  }

  // function SetcodeEtab(codeEtab)
  // {
  //   var etape=3;
  //   $.ajax({
  //     url: '../ajax/sessions.php',
  //     type: 'POST',
  //     async:false,
  //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
  //     dataType: 'text',
  //     success: function (content, statut) {
  //
  // window.location.reload();
  //
  //     }
  //   });
  // }
  function addFrench()
  {
    var etape=1;
    var lang="fr";
    $.ajax({
      url: '../ajax/langue.php',
      type: 'POST',
      async:false,
      data: 'etape=' + etape+ '&lang=' +lang,
      dataType: 'text',
      success: function (content, statut) {

  window.location.reload();

      }
    });
  }

  function addEnglish()
  {
    var etape=1;
    var lang="en";
    $.ajax({
      url: '../ajax/langue.php',
      type: 'POST',
      async:false,
      data: 'etape=' + etape+ '&lang=' +lang,
      dataType: 'text',
      success: function (content, statut) {

  window.location.reload();

      }
    });
  }

  function changeEtape(id)
  {

    // var sauvegarde=$("#sauvegarde").val();

    // const code = $('[name="summernote"]').summernote('code');
    // const code1=$('#summernote').val();
    //
    // alert(code+ " "+code1);

    $("#etape").val(id);
  }


  function checkobjet()
  {
    var objet=$("#typeactivite").val();
    var tabobjet=objet.split("-");

    var objetid=tabobjet[0];

    if(objetid==6)
    {
        // createOther();
        $("#rowother").show();
    }else {
      // deleteOther();
      $("#rowother").hide();
    }

  }

  function restauration()
  {
    // $("#rowclasseEtab").hide();
    // $("#rowprecisioneleves").hide();
    $("#rowprecisiondestinataires").hide();
  }

  function selectiondestinataires()
  {
    restauration();
    var destinataires=$("#destinataires").val();
    if(destinataires=="Admin_locale")
    {
      // $("#rowclasseEtab").hide();
    }else {
      // $("#rowclasseEtab").show();
    }
  }

  function createdestinatairesTeatchers()
  {
    var classeselected=$("#classeEtab").val();
    var session=$("#libellesession").val();
    var codeEtab="<?php echo $_SESSION['user']['codeEtab'] ?>";
    var etape=32;
    $.ajax({
      url: '../ajax/admission.php',
      type: 'POST',
      async:true,
      data: 'classe=' +classeselected+ '&etape=' + etape+'&session='+session+'&codeEtab='+codeEtab,
      dataType: 'text',
      success: function (content, statut) {

        $("#precisiondestinataires").html("");
        $("#precisiondestinataires").html(content);



      }
    });
  }

  function createdestinatairesRespos()
  {
    // var classeselected=$("#classeEtab").val();
    var session=$("#libellesession").val();
    var codeEtab="<?php echo $_SESSION['user']['codeEtab'] ?>";
    var etape=33;
    $.ajax({
      url: '../ajax/admission.php',
      type: 'POST',
      async:true,
      data: 'etape=' + etape+'&session='+session+'&codeEtab='+codeEtab,
      dataType: 'text',
      success: function (content, statut) {

        $("#precisiondestinataires").html("");
        $("#precisiondestinataires").html(content);



      }
    });
  }

  function createdestinatairesEleves()
  {

    //nous allons chercher la liste des eleves de ces classes

    var classeselected=$("#classeEtab").val();
    var session=$("#libellesession").val();
    var codeEtab="<?php echo $_SESSION['user']['codeEtab'] ?>";
    var etape=31;
    $.ajax({
      url: '../ajax/admission.php',
      type: 'POST',
      async:true,
      data: 'classe=' +classeselected+ '&etape=' + etape+'&session='+session+'&codeEtab='+codeEtab,
      dataType: 'text',
      success: function (content, statut) {

        $("#eleves").html("");
        $("#eleves").html(content);



      }
    });


  }




  function selectionprecis()
  {
    var precis=$("#precis").val();
    var destinataires=$("#destinataires").val();

    if(precis==2)
    {
       if(destinataires=="Parent")
       {
         //liste des eleves de cette classe
         $("#rowprecisioneleves").show();

         //nous allons chercher la liste des élèves de ces classes
         createdestinatairesEleves();


       }else if(destinataires=="Teatcher")
       {
         $("#rowprecisioneleves").hide();
         $("#rowprecisiondestinataires").show();

         createdestinatairesTeatchers();

       }else if(destinataires=="Admin_locale")
       {
         $("#rowprecisioneleves").hide();
         $("#rowprecisiondestinataires").show();
         createdestinatairesRespos();
         //liste des responsables de l'établissement
       }
    }else if(precis==1)
    {
      $("#rowprecisioneleves").hide();
      $("#rowprecisiondestinataires").hide();
    }

    // if(destinataires=="Parent" && precis==2)
    // {
    //   //liste des enfants de cette classe
    //   // $("#rowclasseEtab").hide();
    //   $("#rowprecisioneleves").show();
    // }else if(destinataires=="Teatcher" && precis==1)
    // {
    //   //liste des enseignants de cette classe
    // }else if(destinataires=="Admin_locale" && precis==1)
    // {
    //   //liste des responsables de l'établissement
    // }

  }

   jQuery(document).ready(function() {

     $('input').keyup(function() {
               this.value = this.value.toLocaleUpperCase();
           });

           var myImg = document.getElementById('imageinfo');
                 if(myImg && myImg.style) {
                   myImg.style.height = '200px';
                   myImg.style.width = '300px';
                 }

     $("#montantAct").formatter({pattern:"{{999999999}}"});

     $("#contactrespo").formatter({pattern:"{{99}}{{99}}{{99}}{{99}}"});


     var form = $("#FormAddExam").show();

     form.validate({
       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
     },
     highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
     },
     success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },

       rules:{
         classeEtab:"required",
         matiere:"required",
         classe:"required",
         teatcher:"required",
         coef:"required",
         examen:"required",
         datedeb:"required",
         datefin:"required",
         typesess:"required",
         montantAct:"required",
         montantActannual:"required",
         libelactivity:"required",
         message:"required",
         denomination:"required",
         denominationen:"required",
         typeactivite:"required",
         locationactivite:"required",
         descripactivite:"required",
         respoactivite:"required",
         contactrespo:"required",
         destinataires:"required",
         eleves:"required",
         dateobjet:"required",
         objet:"required",
         commentaire:"required",
         // summernote:{
         //   'required':{
         //     depends:function(element){
         //       // const code = $('#summernote').summernote('code');
         //       // return ($('#summernote').val()=='<p><br></p>');
         //       return ($('#summernote').summernote('code') == "" || $('#summernote').summernote('code')== "<p><br></p>");
         //     }
         //   }
         // }





       },
       messages: {
         classeEtab:"<?php echo L::PleaseSelectaumoinsClasse ?>",
         matiere:"<?php echo L::PleaseEnterMatiere ?>",
         classe:"<?php echo L::PleaseSelectclasserequired ?>",
         teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
         coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
         examen:"<?php echo L::PleaseEnterExamLib ?>",
         datedeb:"<?php echo L::PleaseEnterParascoActivityDateStart ?>",
         datefin:"<?php echo L::PleaseEnterParascoActivityDateEnd ?>",
         typesess:"<?php echo L::PeriodRequired ?>",
         montantAct:"<?php echo L::PleaseEnterTrimestrialmontant ?>",
         montantActannual:"<?php echo L::PleaseEnterAnnualmontant ?>",
         libelactivity:"<?php echo L::PleaseEnterParascoActivityName ?>",
         message:"<?php echo L::PleaseEnterActivityMesaage ?>",
         denomination:"<?php echo L::PleaseEnterActivityName ?>",
         denominationen:"<?php echo L::PleaseEnterActivityName ?>",
         typeactivite:"<?php echo L::PleaseEnterActivityType ?>",
         locationactivite:"<?php echo L::PleaseEnterActivityLocation ?>",
         descripactivite:"<?php echo L::PleaseEnterActivityDescription ?>",
         respoactivite:"<?php echo L::PleaseEnterRespoNameMessage ?>",
         contactrespo:"<?php echo L::PleaseEnterContactRespoMessage ?>",
         destinataires:"<?php echo L::PleaseSelectaumoinsDestinatairesMessage ?>",
         eleves:"<?php echo L::PleaseSelectStudents ?>",
         dateobjet:"<?php echo L::ControlsDaterequired ?>",
         objet:"<?php echo L::PleaseSelectObjetMessage ?>",
         commentaire:"<?php echo L::PleaseEnterCommentMessage ?>",
         // summernote:"Merci de renseigner le message"


       }
     });

     // $("#rowclasseEtab").hide();
     $("#rowprecisioneleves").hide();
     $("#rowprecision").hide();

     $("#objet").select2();
     $("#typesess").select2();
     $("#typeactivite").select2();

     $("#rowother").hide();
     $("#rowprecisiondestinataires").hide();
     // $("#destinataires").select2();
     $("#precis").select2();
     $("#classeEtab").select2({
       tags: true,
     tokenSeparators: [',', ' ']
     });
     $("#precisiondestinataires").select2({
       tags: true,
     tokenSeparators: [',', ' ']
     });

     $("#destinataires").select2({
       tags: true,
     tokenSeparators: [',', ' ']
     });

     $("#eleves").select2({
       tags: true,
     tokenSeparators: [',', ' ']
     });
     $('[name="content"]')
        .summernote({
          placeholder: '',
          tabsize: 2,
          height: 200,
          fontSize: 20,

      toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        // ['insert', ['link', 'picture']],
        ['height', ['height']]
      ]
        });
 $('#summernote').summernote({
       placeholder: '',
       tabsize: 2,
       height: 200,
       fontSize: 20,

       toolbar: [
         // [groupName, [list of button]]
         ['style', ['bold', 'italic', 'underline', 'clear']],
         ['font', ['strikethrough', 'superscript', 'subscript']],
         ['fontsize', ['fontsize']],
         ['color', ['color']],
         ['para', ['ul', 'ol', 'paragraph']],
         ['insert', ['link', 'picture', 'video']],
         ['height', ['height']]
       ]

     });

     $('#comment').summernote({
           placeholder: '',
           tabsize: 2,
           height: 200,
           fontSize: 20,

           toolbar: [
             // [groupName, [list of button]]
             // ['style', ['bold', 'italic', 'underline', 'clear']],
             // ['font', ['strikethrough', 'superscript', 'subscript']],
             // ['fontsize', ['fontsize']],
             // ['color', ['color']],
             // ['para', ['ul', 'ol', 'paragraph']],
             // ['insert', ['link', 'picture', 'video']],
             // ['height', ['height']]
           ]

         });



     $('#heuredeb').bootstrapMaterialDatePicker
   ({
     date: true,
     shortTime: false,
     format: 'YYYY-MM-DD HH:mm',
     lang: 'fr',
     cancelText: '<?php echo L::AnnulerBtn ?>',
     okText: '<?php echo L::Okay ?>',
     clearText: '<?php echo L::Eraser ?>',
     nowText: '<?php echo L::Now ?>'

});

$('#heurefin').bootstrapMaterialDatePicker
 ({
   date: true,
   shortTime: false,
   format: 'YYYY-MM-DD HH:mm',
   lang: 'fr',
  cancelText: '<?php echo L::AnnulerBtn ?>',
  okText: '<?php echo L::Okay ?>',
  clearText: '<?php echo L::Eraser ?>',
  nowText: '<?php echo L::Now ?>'

});



   });

</script>
    <!-- end js include path -->
  </body>

</html>
