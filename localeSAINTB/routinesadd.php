<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

if(strlen($_SESSION['user']['codeEtab'])>0)
{
  $codeEtabAssigner=$_SESSION['user']['codeEtab'];
}else {
  $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
}
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//nous allons chercher la liste des etablissements du groupe

$allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
$agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$libellesessionencours="";

  if($etablissementType==1||$etablissementType==3)
  {
    if($nbsessionOn>0){
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];

    }

  }else {
    if($nbsessionOn>0)
    {
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];
      $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
      $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
    }
  }

  if(strlen($_SESSION['user']['codeEtab'])>0)
  {

  }else {
    $_SESSION['user']['codeEtab']=$codeEtabAssigner;
  }

  $_SESSION['user']['session']=$libellesessionencours;

  $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

  $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);



  $HoursLibs=$etabs->getselectionHeureAll($_SESSION['user']['codeEtab'],$libellesessionencours);

  // var_dump($allcodeEtabs);
  $nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
// echo $libellesessionencours;
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
	<link href="../assets2/css/material_style.css" rel="stylesheet">
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title"><?php echo L::AddEmploisduTemps ?></div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#"><?php echo L::EnsigneMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          <li><a class="parent-item" href="#"><?php echo L::EmploisduTemps?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active"><?php echo L::AddMenu ?></li>
                      </ol>
                  </div>
              </div>

              <?php

                    if(isset($_SESSION['user']['addprogra']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
              <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
              <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: '<?php echo L::Felicitations ?>',
                text: '<?php echo $_SESSION['user']['addprogra'] ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addprogra']);
                    }

                     ?>

              <?php

                    if(isset($_SESSION['user']['addroutineok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: '<?php echo L::Felicitations ?>',
  text: "<?php echo $_SESSION['user']['addroutineok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::AddcourseNew ?>',
  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="index.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addroutineok']);
                    }

                     ?>

                     <?php
                     if($nbsessionOn>0)
                     {
                      ?>

              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <div class="card card-box">
                          <div class="card-head">
                              <header></header>

                          </div>

                          <div class="card-body" id="bar-parent">
                              <form  id="FormAddRoutine" class="form-horizontal" action="../controller/routined.php" method="post" >
                                  <div class="form-body">

                                    <div class="form-group row">
                                            <label class="control-label col-md-3"><?php echo L::ScolaryyearMenu ?>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-5">
                                <input type="text" class="form-control " name="sessioncourante" id="sessioncourante" value="<?php echo $libellesessionencours; ?>" readonly>
                                        </div>
                                      </div>

                                    <div class="form-group row">
                                            <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-5">
                                                <!--select class="form-control " name="classe" id="classe" onchange="searchsubject()" style="width:100%"-->
                                                  <select class="form-control " name="classe" id="classe" onchange="selectclasse()" style="width:100%">
                                                    <option value=""><?php echo L::Selectclasses ?></option>
                                                    <?php
                                                    $i=1;
                                                      foreach ($classes as $value):
                                                      ?>
                                                      <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                      <?php
                                                                                       $i++;
                                                                                       endforeach;
                                                                                       ?>

                                                </select>
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                              <label class="control-label col-md-3"><?php echo L::Libjour ?>
                                                  <span class="required"> * </span>
                                              </label>
                                              <div class="col-md-5">
                                                <select class="form-control " name="day" id="day"  style="width:100%">
                                                  <option value=""><?php echo L::PleaseSelectLibjour ?></option>
                                                  <option value="LUN"><?php echo L::Dayslibs1 ?></option>
                                                  <option value="MAR"><?php echo L::Dayslibs2 ?></option>
                                                  <option value="MER"><?php echo L::Dayslibs3 ?></option>
                                                  <option value="JEU"><?php echo L::Dayslibs4 ?></option>
                                                  <option value="VEN"><?php echo L::Dayslibs5 ?></option>
                                                  <option value="SAM"><?php echo L::Dayslibs6 ?></option>
                                                  <option value="DIM"><?php echo L::Dayslibs7 ?></option>

                                                </select>
                                                <input type="hidden" id="etape" name="etape" value="4" >
                                                <input type="hidden" name="seletionne" id="seletionne" value="">
                                                <input type="hidden" name="concatroutines" id="concatroutines" value="">
                                                <input type="hidden" name="nbconcatroutines" id="nbconcatroutines" value="">
                                                <input type="hidden" name="contentmatiere" id="contentmatiere" value="">
                                                <input type="hidden" name="Hourselected" id="Hourselected" value="">
                                                <input type="hidden" name="concatHourselected" id="concatHourselected" value="">

                                                <input type="hidden" name="nbroute" id="nbroute" value="0">
                                                <input type="hidden" id="Etab" name ="Etab" value="<?php echo $_SESSION['user']['codeEtab'];?>" >
                                                <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                                <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $_SESSION['user']['codeEtab']; ?>">
                                              </div>
                                          </div>
                                          <div id="dynamic_field">

                                          </div>


                <div class="form-actions">
                                      <div class="row">
                                          <div class="offset-md-3 col-md-9">

                                              <button type="button" class="btn btn-primary" id="routineNew"> <i class="fa fa-plus"></i> <?php echo L::Newcourses ?> </button>
                                          </div>
                                        </div><br><br>
                                        <div class="row">
                                            <div class="offset-md-3 col-md-9">
                                                <button type="submit" id="submit1"  class="btn btn-info" disabled><?php echo L::AddAndAssignNewDays ?></button>
                                                <button type="submit"  id="submit2" class="btn btn-danger" disabled><?php echo L::AddAndDisplayingAlls ?></button>
                                            </div>
                                          </div>
                                     </div>
              </div>
                              </form>
                          </div>
                      </div>
                  </div>

              </div>
              <?php
            }else {
              ?>
              <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="card card-topline-green">
                       <div class="card-head">
                           <header><?php echo L::Infos ?></header>
                           <div class="tools">
                               <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
       <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
       <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                           </div>
                       </div>
                       <div class="card-body ">
                         <p>

                             <?php echo L::RequiredScolaireYear ?>

                         </p>
                       </div>
                   </div>
                            </div>

              </div>
              <?php
            }
               ?>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <!--script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script-->
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <!-- Material -->
	<script src="../assets2/plugins/material/material.min.js"></script>
	<script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>

 <script>
 // function SetcodeEtab(codeEtab)
 // {
 //   var etape=3;
 //   $.ajax({
 //     url: '../ajax/sessions.php',
 //     type: 'POST',
 //     async:false,
 //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
 //     dataType: 'text',
 //     success: function (content, statut) {
 //
 // window.location.reload();
 //
 //     }
 //   });
 // }
 function addFrench()
 {
   var etape=1;
   var lang="fr";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function addEnglish()
 {
   var etape=1;
   var lang="en";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function activatebtn()
 {
   $("#submit1").prop("disabled",false);

   $("#submit2").prop("disabled",false);
 }

 function deletedrow(id)
 {

   var concatroutines=$("#concatroutines").val();

   $("#concatroutines").val($("#concatroutines").val().replace(id+"@", ""));

   // $("#concatroutines").val(concatroutines+nouveau+"@");

   $("#ligneRowMat"+id).remove();
   $("#ligneRowHeurdeb"+id).remove();
   $("#ligneRowHeurfin"+id).remove();
   $("#ligneRow"+id).remove();

   recalculroutinenb();




    //$("#ligneRow"+id).remove();
 }

 function selectclasse()
 {

   var classe=$("#classe").val();
   var etape=10;






   //alert("bonjour");

   $.ajax({
     url: '../ajax/matiere.php',
             type: 'POST',
             async:false,
             data: 'classe=' + $("#classe").val()+ '&etape=' + etape+'&session='+$("#sessioncourante").val()+'&codeEtab='+$("#codeEtab").val(),
             dataType: 'text',
   success: function (content, statut) {

     //$("#classe").val(content);

     //var contentclasse=content;

  //    alert(classe);
    $("#seletionne").val(classe);
  //
   // $("#classe").val(content);

     //nous allons mettre les matieres de cette classe dans la variable contentmatiere


     var etape1=2;

      $.ajax({

        url: '../ajax/matiere.php',
        type: 'POST',
        async:false,
        data: 'classe=' + $("#seletionne").val()+'&code='+$("#codeEtab").val()+'&etape='+etape1,
        dataType: 'text',
        success: function (content1, statut)
        {

          // $("#contentmatiere").val(content1);
          //alert(content1);

         $("#contentmatiere").val(content1);


        }

     });

     // contentmatiere



   }


 });

}

function recalculroutinenb()
{

  var concatroutines=$("#concatroutines").val();

  var tab=concatroutines.split("@");

  var nbtab=tab.length;

  var nbtabnew=parseInt(nbtab)-1;

  $("#nbconcatroutines").val(nbtabnew);

  if(nbtabnew==0)
  {
    //actualiser la page

     location.reload();
  }else if(nbtabnew>0){
    activatebtn();
  }



}

function getListeHeureLib(nouveau)
{
  var codeEtab=$("#codeEtab").val();
  var session=$("#libellesession").val();
  var Hourselected=$("#Hourselected").val();
  var etape=8;

  $.ajax({
    url: '../ajax/hours.php',
    type: 'POST',
    async:true,
    data: 'Hourselected='+Hourselected+'&etape='+etape+'&codeEtab='+codeEtab+'&session='+session,
    dataType: 'text',
    success: function (content, statut) {

      $("#heureLibelles"+nouveau).html(content);

    }});
}

function ajouterlibeller(nouveau)
{
  var libelleheure=$("#heureLibelles"+nouveau).val();
  var Hourselected=$("#Hourselected").val();
  var concatHourselected=$("#concatHourselected").val();
  var longeur=$("#Hourselected").val().length;

  if(longeur==0)
  {
    $("#concatHourselected").val(concatHourselected+nouveau+"-"+libelleheure+",");
    $("#Hourselected").val(Hourselected+libelleheure+",");
    searcher(libelleheure,nouveau);
  }else if(longeur>0)
  {
    var alldata=$("#concatHourselected").val();
    var newdata=nouveau+"-"+libelleheure+",";

    var etape1=5;

    $.ajax({
      url: '../ajax/hours.php',
      type: 'POST',
      async:true,
      data: 'alldata='+alldata+'&etape='+etape1+'&nouveau='+nouveau+'&newdata='+newdata,
      dataType: 'text',
      success: function (content, statut) {

        if(content==0)
        {
          $("#concatHourselected").val(concatHourselected+nouveau+"-"+libelleheure+",");
          $("#Hourselected").val(Hourselected+libelleheure+",");
          searcher(libelleheure,nouveau);
        }else if(content==1)
        {
          var etape=4;
          $.ajax({
            url: '../ajax/hours.php',
            type: 'POST',
            async:true,
            data: 'alldata='+alldata+'&etape='+etape+'&nouveau='+nouveau,
            dataType: 'text',
            success: function (content, statut) {

              var valeur=content;

              $("#concatHourselected").val($("#concatHourselected").val().replace(nouveau+"-"+valeur+",", newdata));
              $("#Hourselected").val($("#Hourselected").val().replace(valeur+",",libelleheure+","));

              searcher(libelleheure,nouveau);

            }
          });
        }



      }

  });

}
}

function searcher(libelleheure,nouveau)
{
  var etape=7;
  var codeEtab=$("#codeEtab").val();
  var session=$("#libellesession").val();
  $.ajax({
    url: '../ajax/hours.php',
    type: 'POST',
    async:true,
    data: 'libelleheure='+libelleheure+'&etape='+etape+'&nouveau='+nouveau+'&codeEtab='+codeEtab+'&session='+session,
    dataType: 'text',
    success: function (content, statut) {
      var heuredeb=content.split("*")[0];
      var heurefin=content.split("*")[1];
      // var id=content.split("*")[2];
      // var HeureIds=$("#HeureIds").val();
      // var HeureIdconcat=$("#HeureIdconcat").val();

      $("#heuredeb"+nouveau).val(heuredeb);
      $("#heurefin"+nouveau).val(heurefin);

      // $("#HeureIds").val(HeureIds+id+",");
      // $("#HeureIdconcat").val(HeureIdconcat+nouveau+"-"+id+",");

      // $("#concatHourselected").val(concatHourselected+nouveau+"-"+libelleheure+",");
      // $("#Hourselected").val(Hourselected+libelleheure+",");



    }
  });
}

 function AddroutinesRow()
 {
   //nous allons verifier si la classe est Selectionner  classeseleted

    var contentmatiere=$("#contentmatiere").val();


   var nb=$("#nbroute").val();
   var nouveau= parseInt(nb)+1;

  var concatroutines=$("#concatroutines").val();

  $("#concatroutines").val(concatroutines+nouveau+"@");

  //recalcule nbconcatroutine
recalculroutinenb();
getListeHeureLib(nouveau);

   //ligne matiere
    var ligne="<div class=\"form-group row\" id=\"ligneRowMat"+nouveau+"\">";
    ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::MatiereMenusingle ?> <span class=\"required\"> * </span></label>";
    ligne=ligne+"<div class=\"col-md-5\">";
    ligne=ligne+"<select class=\"form-control \" name=\"matiere"+nouveau+"\"  id=\"matiere"+nouveau+"\"  style=\"width:100%\">";
    ligne=ligne+contentmatiere;
    ligne=ligne+"</select>";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";

    //ligne heure debut

    //ligne Libelle heure de cours

    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowHeurdeCours"+nouveau+"\">";
    ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::LibelleHeure ?> <span class=\"required\"> * </span></label>";
    ligne=ligne+"<div class=\"col-md-5\">";
    ligne=ligne+"<select class=\"form-control \" name=\"heureLibelles"+nouveau+"\"  id=\"heureLibelles"+nouveau+"\"  style=\"width:100%\" onchange=\"ajouterlibeller("+nouveau+")\" >";
    // ligne=ligne+contentmatiere;
    ligne=ligne+"</select>";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";

    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowHeurdeb"+nouveau+"\">";
    ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::HourStart ?><span class=\"required\"> * </span></label>";
    ligne=ligne+"<div class=\"col-md-5\">";
    ligne=ligne+"<input type=\"text\"  id=\"heuredeb"+nouveau+"\" name=\"heuredeb"+nouveau+"\" class=\"floating-label mdl-textfield__input\"  placeholder=\"<?php echo L::EnterHourStart ?>\" disabled=\"disabled\">";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";

    //ligne heure de fin

    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowHeurfin"+nouveau+"\">";
    ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::HourEnd ?><span class=\"required\"> * </span></label>";
    ligne=ligne+"<div class=\"col-md-5\">";
    ligne=ligne+"<input type=\"text\"  id=\"heurefin"+nouveau+"\" name=\"heurefin"+nouveau+"\" class=\"floating-label mdl-textfield__input\" placeholder=\"<?php echo L::EnterHourEnd ?>\" disabled=\"disabled\">";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";

    //ligne bouton supprimer
    // ligne=ligne+"<div class=\" row\">";
    ligne=ligne+"<div class=\"pull-right\" id=\"ligneRow"+nouveau+"\">";
    ligne=ligne+"<button type=\"button\" id=\"delete"+nouveau+"\" name=\"delete"+nouveau+"\"  onclick=\"deletedrow("+nouveau+")\" class=\"btn btn-danger pull-right\"><i class=\"fa fa-trash\"></i></button>";
    ligne=ligne+"</div></br></br>";
    // ligne=ligne+"</div>";



    $('#dynamic_field').append(ligne);




    // alert(contentmatiere);

     // $("#matiere"+nouveau).html(contentmatiere);

    $('#heurefin'+nouveau).bootstrapMaterialDatePicker
    ({
      date:false,
      shortTime: false,
      format: 'HH:mm',
      lang: 'fr',
     cancelText: '<?php echo L::AnnulerBtn ?>',
     okText: '<?php echo L::Okay ?>',
     clearText: '<?php echo L::Eraser ?>',
     nowText: '<?php echo L::Now ?>'

    });

    $('#heuredeb'+nouveau).bootstrapMaterialDatePicker
    ({
      date: false,
      shortTime: false,
      format: 'HH:mm',
      lang: 'fr',
      cancelText: '<?php echo L::AnnulerBtn ?>',
      okText: '<?php echo L::Okay ?>',
      clearText: '<?php echo L::Eraser ?>',
      nowText: '<?php echo L::Now ?>'

    });

    $("#matiere"+nouveau).select2();
    $("#heureLibelles"+nouveau).select2();


    $("#nbroute").val(nouveau);

    $('#heurefin'+nouveau).rules( "add", {
        required: true,
        messages: {
        required: "<?php echo L::RequiredChamp ?>"
}
      });

      $('#heuredeb'+nouveau).rules( "add", {
          required: true,
          messages: {
          required: "<?php echo L::RequiredChamp ?>"
  }
        });

        $("#matiere"+nouveau).rules( "add", {
            required: true,
            messages: {
            required: "<?php echo L::RequiredChamp ?>"
    }
          });

 }



 function verify()
 {

 }

 function searchsubject()
 {
   var codeEtab="<?php echo $_SESSION['user']['codeEtab'];?>";
   var etape=2;

    $.ajax({

      url: '../ajax/matiere.php',
      type: 'POST',
      async:false,
      data: 'classe=' + $("#classe").val()+'&code='+codeEtab+'&etape='+etape,
      dataType: 'text',
      success: function (content, statut)
      {
        $("#matiere").html("");
        $("#matiere").html(content);
      }

    });
 }
 function difheure(heuredeb,heurefin)
	{
	  var hd=heuredeb.split(":");
	  var hf=heurefin.split(":");

    // var difH=hf[0]-hd[0];

	   hd[0]=eval(hd[0]);
     hd[1]=eval(hd[1]);
     //hd[2]=eval(hd[2]);
	   hf[0]=eval(hf[0]);
     hf[1]=eval(hf[1]);
     // hf[2]=eval(hf[2]);
	   //if(hf[2]<hd[2]){hf[1]=hf[1]-1;hf[2]=hf[2]+60;}
	   // if(hf[1]<hd[1]){hf[0]=hf[0]-1;hf[1]=hf[1]+60;}
	   // if(hf[0]<hd[0]){hf[0]=hf[0]+24;}
	   //return((hf[0]-hd[0]) + ":" + (hf[1]-hd[1])); // + ":" + (hf[2]-hd[2]));
	   return((hf[0]-hd[0])*60 + (hf[1]-hd[1]));
	}

 function check()
 {
   //recupération des variables
   var heuredeb=$('#heuredeb').val();
   var heurefin=$('#heurefin').val();

  if((heuredeb!="") && (heurefin!=""))
  {
    var result=difheure(heuredeb,heurefin);

      if(result<0)
      {
        Swal.fire({
        type: 'error',
        title: '<?php echo L::WarningLib ?>',
        text: "<?php echo L::RequiredHoursDebAndEnd ?>",

      })

      //$('#heuredeb').html("");
      $('#heurefin').val("");

      }
  }




 }
 jQuery(document).ready(function() {

   $('#submit1').click(function(){

$('#etape').val(4);

       });

       $('#submit2').click(function(){

    $('#etape').val(5);

           });

   $('#routineNew').click(function(){
         AddroutinesRow();


       });

// AddroutinesRow();


$("#classe").select2();
$("#day").select2();
$("#classeEtab").select2();
// $("#matiere").select2();


   $("#FormAddRoutine").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

        classe:"required",
        matiere:"required",
        day:"required",
        heuredeb:"required",
        heurefin:"required"


      },
      messages: {
        classe:"<?php echo L::PleaseSelectclasserequired ?>",
        matiere:"<?php echo L::PleaseselectSubjects ?>",
        day:"<?php echo L::PleaseSelectLibjour ?>",
        heuredeb:"<?php echo L::PleaseEnterHourStart ?>",
        heurefin:"<?php echo L::PleaseEnterHourEnd ?>"
      },
      submitHandler: function(form) {
        //verifier si nous n'avons pas une routine dans la période dites

        form.submit();

//         var etape=3;
//         var heuredeb=$('#heuredeb').val();
//         var heurefin=$('#heurefin').val();
//         var codeEtab="<?php //echo $codeEtabLocal;?>";
//         //alert(heuredeb);
//         $.ajax({
//           url: '../ajax/matiere.php',
//           type: 'POST',
//           async:false,
//           data: 'classe=' + $("#classe").val()+ '&etape=' + etape+'&matiere='+$("#matiere").val()+'&day='+$("#day").val()+'&heuredeb='+heuredeb+'&heurefin='+heurefin+'&codeEtab='+codeEtab,
//           dataType: 'text',
//           success: function (content, statut) {
//
//
//
//           if(content==0)
//             {
//               //le compte n'existe pas dans la base on peut l'ajouter
//
//               form.submit();
//             }else if(content==1){
//             //il est question de la même matiere qui est deja présente dans le système et a cette meme période
//               Swal.fire({
//               type: 'warning',
//               title: '<?php echo L::WarningLib ?>',
//               text: "Cette matière existe deja pour cette période de cours",
//
//             })
//
//           }else if(content==2)
//           {
//             //il est question d'une nouvelle matière or nous avons une matière a cette période
//             var etape=4;
//             var heuredeb=$('#heuredeb').val();
//             var heurefin=$('#heurefin').val();
//             var codeEtab="<?php //echo $codeEtabLocal;?>";
//             var classe=$("#classe").val();
//             var matiere=$("#matiere").val();
//             Swal.fire({
// title: '<?php echo L::WarningLib ?>',
// text: "Une Matière est dejà prévu à cette heure de cours",
// type: 'success',
// showCancelButton: true,
// confirmButtonColor: '#3085d6',
// cancelButtonColor: '#d33',
// confirmButtonText: '<?php echo L::ModifierBtn ?>',
// cancelButtonText: '<?php echo L::AnnulerBtn ?>',
// }).then((result) => {
// if (result.value) {
// document.location.href="../controller/routine.php?etape="+etape+"&heuredeb="+heuredeb+"&heurefin="+heurefin+"&codeEtab="+codeEtab+"&classe="+classe+"&matiere="+matiere;
// }else {
//
// }
// })
//             // $("#newStudent").val(0);
//             // form.submit();
//           }
//
//           }
//
//
//         });


      }


   });


 });
 </script>
    <!-- end js include path -->
  </body>

</html>
