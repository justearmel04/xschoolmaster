<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$student=new Student();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

if(strlen($_SESSION['user']['codeEtab'])>0)
{
  $codeEtabAssigner=$_SESSION['user']['codeEtab'];
}else {
  $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
}
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//nous allons chercher la liste des etablissements du groupe

$allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
$agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$libellesessionencours="";

  if($etablissementType==1||$etablissementType==3)
  {
    if($nbsessionOn>0){
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];

    }

  }else {
    if($nbsessionOn>0)
    {
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];
      $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
      $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
    }
  }

  if(strlen($_SESSION['user']['codeEtab'])>0)
  {
    if(strlen($_GET['codeEtab'])>0)
    {
      $_SESSION['user']['codeEtab']=$_GET['codeEtab'];
    }

  }else {
    $_SESSION['user']['codeEtab']=$codeEtabAssigner;
  }

  $_SESSION['user']['session']=$libellesessionencours;

   // var_dump($allcodeEtabs);

   $allstudentschools=$student->getAllStudentOfThisSchool($codeEtabAssigner);

  $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

  $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);

  $students=$student->getAllStudentOfClassesId($_GET['classe'],$libellesessionencours);

  // $classesEtab=$classe->


  $nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
  $onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
  $offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::Listedeseleves. " ".$classe->getInfosofclassesbyId($_GET['classe'],$libellesessionencours);  ?> </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="listeall.php"><?php echo L::studMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::Listedeseleves ?></li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['updateteaok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: '<?php echo L::Felicitations  ?>',
            text: '<?php echo $_SESSION['user']['updateteaok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['updateteaok']);
                }

                 ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>
                 <div class="row">
                   <?php

                   if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
                   {



                   }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
                   {

                   }

                    ?>
                 </div>


<div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line">
                               <!--ul class="nav nav-pills nav-pills-rose">
									<li class="nav-item tab-all"><a class="nav-link active show"
										href="#tab1" data-toggle="tab">Liste</a></li>
									<li class="nav-item tab-all"><a class="nav-link" href="#tab2"
										data-toggle="tab">Grille</a></li>
								</ul-->
                                <div class="tab-content">
                                    <div class="tab-pane active fontawesome-demo" id="tab1">
                                        <div class="row">
					                        <div class="col-md-12">
					                            <div class="card  card-box">
					                                <div class="card-head">
					                                    <header></header>
					                                    <div class="tools">
					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
					                                    </div>
					                                </div>
					                                <div class="card-body ">
                                            <div class="pull-right">
                                              <?php
                                                    if($_SESSION['user']['fonctionuser']=="Coordinnateur"|| $_SESSION['user']['fonctionuser']=="Administrateur"|| $_SESSION['user']['fonctionuser']=="Superviseur")
                                                    {
                                              ?>
                                                <a class="btn btn-primary btn-md " href="admission.php" style="border-radius:5px;"><i class="fa fa-plus"></i> <?php echo L::CreateFicheMenu ?></a>
                                              <?php
                                                  }
                                               ?>

                                              <?php
                                              if($etablissementType==2)
                                              {
                                               ?>
                                              <a class="btn btn-warning btn-md " href="#"  onclick="classeliste()" style="border-radius:5px;"><i class="fa fa-print"></i> <?php echo L::ClassesListsingle ?></a>
                                              <?php
                                            }else if($etablissementType==1)
                                            {
                                              ?>
                                              <a class="btn btn-warning btn-md " href="#"  onclick="classelistesmall()" style="border-radius:5px;"><i class="fa fa-print"></i> <?php echo L::ClassesListsingle ?></a>
                                              <?php
                                            }else if($etablissementType==4)
                                            {
                                              ?>
                                              <a class="btn btn-warning btn-md " href="#"  onclick="classelistesmallbig()" style="border-radius:5px;"><i class="fa fa-print"></i> <?php echo L::ClassesListsingle ?></a>
                                              <?php
                                            }else if($etablissementType==3||$etablissementType==5)
                                            {
                                              ?>
                                                <a class="btn btn-warning btn-md " href="#"  onclick="classelistesmallbig()" style="border-radius:5px;"><i class="fa fa-print"></i> <?php echo L::ClassesListsingle ?></a>
                                              <?php
                                            }
                                               ?>
                                            </div>

					                                  <div class="table-scrollable">
                                              <?php

                                               ?>
					                                    <table class="table table-hover table-checkable order-column full-width" id="example45">
					                                        <thead>
					                                            <tr>
                                                         <th> <?php echo L::Pictures ?> </th>
					                                                <th> <?php echo L::MatriculestudentTab ?> </th>
					                                                <th> <?php echo L::NamestudentTab ?></th>
                                                           <th><?php echo L::EmailstudentTab ?></th>
					                                                 <th class="noExport"> <?php echo L::Actions ?> </th>
					                                            </tr>
					                                        </thead>
					                                        <tbody>
                                                    <?php
                                                    $i=1;

$scolatitesaveser=0;
$scolaritesverser=0;
$inscriptionaverser=0;
$inscriptionverser=0;
//var_dump($teatchers);
                                                      foreach ($students as $value):
                                                        $scolatitesaveser=$scolatitesaveser+$value->scolarite_classe;
                                                        $inscriptionaverser=$inscriptionaverser+$value->inscriptionmont_classe;
                                                        // echo $value->inscriptionmont_classe;
                                                      ?>
																<tr class="odd gradeX" ondblclick="myFunction(<?php echo $value->id_compte?>)">
                                  <?php
                                  //rechercher le total des verements scolarites de cet eleves
                                    $datasInscriptions=$etabs->getAllInscriptionVersmentChild($_SESSION['user']['codeEtab'],$libellesessionencours,$value->id_compte,$value->id_classe);
                                    $nbdatainscription=count($datasInscriptions);
                                    $sommeinscri=0;
                                      if($nbdatainscription==0)
                                      {
                                        $inscriptionverser=$inscriptionverser+0;
                                      }else if($nbdatainscription>0){

                                          foreach ($datasInscriptions as $valueinscrip):
                                            $sommeinscri=$sommeinscri+$valueinscrip->montant_versement;
                                          endforeach;
                                      }

                                      $inscriptionverser=$inscriptionverser+$sommeinscri;

                                      $datasScolarites=$etabs->getAllScolaritesVersmentChild($_SESSION['user']['codeEtab'],$libellesessionencours,$value->id_compte,$value->id_classe);
                                      // var_dump($datasInscriptions);
                                      $nbdatascolarites=count($datasScolarites);
                                      $sommeiscola=0;
                                      if($nbdatascolarites==0)
                                      {
                                        $scolaritesverser=$scolaritesverser+0;
                                      }else if($nbdatascolarites>0){

                                          foreach ($datasScolarites as $valuescola):
                                            $sommeiscola=$sommeiscola+$valuescola->montant_versement;
                                          endforeach;
                                      }

                                      $scolaritesverser=$scolaritesverser+$sommeiscola;
                                   ?>

                                  <td class="patient-img">
                                    <?php

                                    if(strlen($value->photo_compte)>0)
                                    {
                                      $lien="../photo/Students/".$value->matricule_eleve."/".$value->photo_compte;
                                    }else {
                                      $lien="../photo/user5.jpg";
                                    }

                                     ?>
																			<img src="<?php echo $lien?>" alt="">
																	</td>

																	<td style="width:150px;"><?php
                                  //echo $value->codeEtab_classe;

                                  echo $value->matricule_eleve;

                                  ?></td>

                                  <td>
                                    <?php
                                    $statut=$value->statut_classe;

                                    echo $value->nom_eleve." ".$value->prenom_eleve;

                                    ?>
                                </td>
																	<td class="left"><?php echo $value->email_eleve;?></td>
                                  <td class="center">

                                    <?php

                                          if($_SESSION['user']['fonctionuser']=="Coordinnateur"|| $_SESSION['user']['fonctionuser']=="Administrateur"|| $_SESSION['user']['fonctionuser']=="Directeur"|| $_SESSION['user']['fonctionuser']=="BDM"|| $_SESSION['user']['fonctionuser']=="CC"|| $_SESSION['user']['fonctionuser']=="DG")
                                          {
                                            ?>

                                            																		<a href="#"  onclick="modify(<?php echo $value->id_compte?>,'<?php echo $value->codeEtab_classe;  ?>')" class="btn btn-primary btn-xs" title="<?php echo L::ModifierBtn ?>">
                                            																			<i class="fa fa-pencil"></i>
                                            																		</a>
                                                                                <!--a href="recapnotesstudent.php?idcompte=<?php //echo $value->id_compte?>&classe=<?php //echo $_GET['classe'];?>&codeEtab=<?php //echo $codeEtabAssigner; ?>" class="btn btn-warning btn-xs" title="Recap des notes"> <i class="fa fa-eye" ></i> </a-->
                                              <?php
                                              if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="DG")
                                              {

                                              }
                                               ?>

                                            <?php
                                          }

                                     ?>

                                     <a title="<?php echo L::Desactiver ?>" class="btn btn-danger btn-xs" onclick="deleted(<?php echo $value->id_compte?>)">
                                       <i class="fa fa-power-off "></i>
                                     </a>

                                    <a title="<?php echo L::ParentsLists ?>" class="btn btn-warning btn-xs" href="parentstudents.php?idcompte=<?php echo $value->id_compte ?>&classe=<?php echo $value->id_classe  ?>&codeEtab=<?php echo $value->codeEtab_classe ?>"><i class="fa fa-users"></i></a>

                                    <!--a href="disciplinesclasses.php?classe=<?php //echo $value->idclasse_inscrip;?>&codeEtab=<?php //echo $codeEtabAssigner;?>" class="btn btn-success btn-xs" title="disciplines de classe"> <i class=" fa fa-bar-chart-o"></i> </a-->

																	</td>
																</tr>
                                <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                <script>

                                function myFunction(idcompte)
                                {
                                  //var url="detailslocal.php?compte="+idcompte;
                                document.location.href="detailsstudent.php?compte="+idcompte;
                                }

                                function modify(id,codeEtab)
                                {


                                  Swal.fire({
                    title: '<?php echo L::WarningLib ?>',
                    text: "<?php echo L::DoyouReallyModifyingFiche ?>",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<?php echo L::ModifierBtn ?>',
                    cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                  }).then((result) => {
                    if (result.value) {
                      document.location.href="updatestudent.php?compte="+id+"&codeEtab="+codeEtab;
                    }else {

                    }
                  })
                                }

                                function deleted(id)
                                {

                                  var classe="<?php echo $_GET['classe']; ?>";
                                  var codeEtab="<?php echo $_SESSION['user']['codeEtab']; ?>";

                                  Swal.fire({
                    title: '<?php echo L::WarningLib ?>',
                    text: "<?php echo L::DoyouReallyDesavtivatedFiche ?>",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<?php echo L::Desactiver ?>',
                    cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                  }).then((result) => {
                    if (result.value) {
                      //document.location.href="../controller/student.php?etape=3&compte="+id;
  document.location.href="../controller/admission.php?etape=3&compte="+id+"&classe="+classe+"&codeEtab="+codeEtab;

                    }else {

                    }
                  })

                                }

                                </script>


                                <?php
                                                                 $i++;
                                                                 endforeach;
                                                                 ?>

															</tbody>
                              <!--tfoot>
                                <tr>
                                  <td></td>
                                  <td colspan="2" style="width:300px">MONTANT SCOLARITES A VERSER</td>
                                  <td colspan="2" ><?php //echo $scolatitesaveser ?></td>

                                </tr>
                                <tr>
                                  <td></td>
                                  <td colspan="2" style="width:300px" >MONTANT SCOLARITES  VERSER</td>
                                  <td colspan="2" ><?php //echo $scolaritesverser; ?></td>

                                </tr>
                                <tr>
                                  <td></td>
                                  <td colspan="2" style="width:300px" >MONTANT INSCRIPTION A VERSER</td>
                                  <td colspan="2" ><?php //echo $inscriptionaverser ?></td>

                                </tr>
                                <tr>
                                  <td></td>
                                  <td colspan="2" style="width:300px" >MONTANT INSCRIPTION  VERSER</td>
                                  <td colspan="2" ><?php //echo $inscriptionverser ?></td>

                                </tr>
                              </tfoot-->
					                                    </table>
					                                    </div>
					                                </div>
					                            </div>
					                        </div>
					                    </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>



                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
 <script src="../cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" ></script>
 	<script src="../cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
 	<script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" ></script>
 	<script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" ></script>
 	<script src="../cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   // function SetcodeEtab(codeEtab)
   // {
   //   var etape=3;
   //   $.ajax({
   //     url: '../ajax/sessions.php',
   //     type: 'POST',
   //     async:false,
   //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
   //     dataType: 'text',
   //     success: function (content, statut) {
   //
   // window.location.reload();
   //
   //     }
   //   });
   // }

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function classeliste()
   {
     //listeEnseignant.php?$codeEtabAssigner=<?php //echo $codeEtabAssigner?>

     var url="listedeclasse.php?idClasse=<?php  echo $_GET['classe']?>&codeEtab=<?php echo $_SESSION['user']['codeEtab']?>&session=<?php echo $libellesessionencours ?>";

     window.open(url, '_blank');



   }

   function classelistesmall()
   {
     var url="listedeclasses.php?idClasse=<?php  echo $_GET['classe']?>&codeEtab=<?php echo $_SESSION['user']['codeEtab']?>&session=<?php echo $libellesessionencours ?>";

     window.open(url, '_blank');
   }

   function classelistesmallbig()
   {
     var url="listeclasses.php?idClasse=<?php  echo $_GET['classe']?>&codeEtab=<?php echo $_SESSION['user']['codeEtab']?>&session=<?php echo $libellesessionencours ?>";

     window.open(url, '_blank');
   }

   function searchlibetab()
   {

     var etape=2;

     $.ajax({
              url: '../ajax/school.php',
              type: 'POST',
              async:false,
              data: 'code='+ $("#codeetab").val()+'&etape='+etape,
              dataType: 'text',
              success: function (content, statut) {

                $("#libetab").html("");
                $("#libetab").html(content);

                //nous allons chercher la liste des admins local de cet etablissement

                $.ajax({
                         url: '../ajax/classe.php',
                         type: 'POST',
                         async:false,
                         data: 'code='+ $("#codeetab").val()+'&etape='+etape,
                         dataType: 'text',
                         success: function (response, statut) {


                           $("#classex").html("");
                           $("#classex").html(response);
                         }
                       });

              }

            });

   }
   $(document).ready(function() {

     $('#example5').DataTable( {
         "scrollX": true

     } );

     $('#example45').DataTable( {

       "scrollX": true,
       "language": {
           "lengthMenu": "_MENU_  ",
           "zeroRecords": "Aucune correspondance",
           "info": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
           "infoEmpty": "Aucun enregistrement disponible",
           "infoFiltered": "(filtered from _MAX_ total records)",
           "sEmptyTable":"Aucune donnée disponible dans le tableau",
            "sSearch":"Rechercher :",
            "oPaginate": {
       "sFirst":    "Premier",
       "sLast":     "Dernier",
       "sNext":     "Suivant",
       "sPrevious": "Précédent"
     }
     },

         dom: 'Bfrtip',
         buttons: [
             // 'copyHtml5',

             // 'excelHtml5',
             {
               extend: 'excelHtml5',
               title: 'Data export',
               exportOptions: {
                             columns: "thead th:not(.noExport)"
                         }
             }
             // 'csvHtml5',
             // 'pdfHtml5'
         ]
     } );

     $("#codeetab").select2();

     $("#libetab").select2();

   });

   </script>
    <!-- end js include path -->
  </body>

</html>
