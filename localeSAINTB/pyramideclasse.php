<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$student=new Student();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

if(strlen($_SESSION['user']['codeEtab'])>0)
{
  $codeEtabAssigner=$_SESSION['user']['codeEtab'];
}else {
  $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
}
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//nous allons chercher la liste des etablissements du groupe

$allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
$agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$libellesessionencours="";

  if($etablissementType==1||$etablissementType==3)
  {
    if($nbsessionOn>0){
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];

    }

  }else {
    if($nbsessionOn>0)
    {
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];
      $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
      $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
    }
  }

  if(strlen($_SESSION['user']['codeEtab'])>0)
  {

  }else {
    $_SESSION['user']['codeEtab']=$codeEtabAssigner;
  }

  $_SESSION['user']['session']=$libellesessionencours;

   // var_dump($allcodeEtabs);

   $allstudentschools=$student->getAllStudentOfThisSchool($codeEtabAssigner);

  $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

  $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);

  // $classesEtab=$classe->

  $classebynieveau=$classe->getAllclassebyNiveau($_SESSION['user']['codeEtab'],$_SESSION['user']['session']);
  $datenaisstudents=$classe->getAlldatenaisStudent($_SESSION['user']['codeEtab'],$_SESSION['user']['session']);

  $nbclassebyniveau=count($classebynieveau);
  $nbdatenaisalls=count($datenaisstudents);

  // $classesEtab=$classe->

  $concatdatenais="";
  $concatniveau="";

  foreach ($datenaisstudents as $value):
    $concatdatenais=$concatdatenais.$value->year."*";
  endforeach;

  foreach ($classebynieveau as $values):
    $concatniveau=$concatniveau.$values->niveau_classe."*";
  endforeach;

  // $concatniveau="*".$concatniveau;
  $concatniveau="SIXIEME*CINQUIEME*QUATRIEME*TROISIEME*TOTALFIRST*SECONDE*PREMIERE*TERMINAL*TOTALSECONDE*TOTALGENE";
  $concatpremiercycle="SIXIEME*CINQUIEME*QUATRIEME*TROISIEME";
  $concatdeuxiemecycle="SECONDE*PREMIERE*TERMINAL";
  $concatdatenais="*".$concatdatenais;

// echo $concatniveau;

  // $concatniveau=substr($concatniveau, 0, -1);

  // $concatdatenais=substr($concatdatenais, 0, -1);

  $tabdatenais=explode("*",$concatdatenais);
  $tabniveau=explode("*",$concatniveau);

  $nbniveaux=count($tabniveau);


  $nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
  $onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
  $offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::ClassesMenu ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="#"><?php echo L::ClassesMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::AllClassesMenu ?></li>
                            </ol>
                        </div>
                    </div>

                    <?php

                          if(isset($_SESSION['user']['addclasseok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: '<?php echo L::Felicitations ?>',
                      text: '<?php echo $_SESSION['user']['addclasseok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['addclasseok']);
                          }

                           ?>

                    <?php

                          if(isset($_SESSION['user']['updateclasseok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: '<?php echo L::Felicitations ?>',
                      text: '<?php echo $_SESSION['user']['updateclasseok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['updateclasseok']);
                          }

                           ?>



                    <?php

                          if(isset($_SESSION['user']['delclasseok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: '<?php echo L::Felicitations ?>',
                      text: '<?php echo $_SESSION['user']['delclasseok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['delclasseok']);
                          }

                           ?>
					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['updateteaok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: '<?php echo L::Felicitations ?>',
            text: '<?php echo $_SESSION['user']['updateteaok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['updateteaok']);
                }

                 ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>
<div class="row" style="display:none">
  <div class="col-md-12 col-sm-12">
                            <div class="card card-box">
                                <div class="card-head">
                                    <header><?php echo L::Seacher ?></header>

                                </div>
                                <div class="card-body " id="bar-parent">
                                  <form method="post" id="FormSearch">
                                      <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                        <!-- text input -->

                                        <div class="form-group">
                                            <label><?php echo L::ASections ?></label>
                                            <select class="form-control input-height" id="section" name="section" style="width:100%" onchange="searchclassesection()">
                                                <option value=""><?php echo L::SelectSectionss ?></option>
                                                <?php
                                                $i=1;
                                                  foreach ($sections as $value):
                                                  ?>
                                                  <option value="<?php echo $value->id_section;?>"><?php echo utf8_encode(utf8_decode($value->libelle_section)); ?></option>

                                                  <?php
                                                                                   $i++;
                                                                                   endforeach;
                                                                                   ?>

                                            </select>
                                            <input type="hidden" name="search" id="search"/>
                                            <input type="hidden" id="codeetab" name="codeetab" value="<?php echo $_SESSION['user']['codeEtab']; ?>">

                                        </div>

                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label><?php echo L::ClasseMenu ?></label>
                                        <select class="form-control input-height" id="classex" name="classex" style="width:100%">
                                            <option value=""><?php echo L::Selectclasses ?></option>


                                        </select>
                                          </div>


                                </div>
                                      </div>

                                      <button type="submit" class="btn btn-danger"><i class="fa fa-search"></i> <?php echo L::Seacher ?></button>
                                  </form>
                                </div>
                            </div>
                        </div>
</div>


<div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line">
                               <!--ul class="nav nav-pills nav-pills-rose">
									<li class="nav-item tab-all"><a class="nav-link active show"
										href="#tab1" data-toggle="tab">Liste</a></li>
									<li class="nav-item tab-all"><a class="nav-link" href="#tab2"
										data-toggle="tab">Grille</a></li>
								</ul-->
                                <div class="tab-content">
                                    <div class="tab-pane active fontawesome-demo" id="tab1">
                                        <div class="row">
					                        <div class="col-md-12">
					                            <div class="card  card-box">
					                                <div class="card-head">
					                                    <header></header>
					                                    <div class="tools">
					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
					                                    </div>
					                                </div>
					                                <div class="card-body ">

					                                  <div class="table-scrollable">
                                              <div class="table-responsive text-nowrap">
        <!--Table-->
        <table class="table table-striped" id="example">

          <!--Table head-->
          <thead>
            <tr>
              <th>Niveau</th>
              <th>Classe pédagigiques</th>
              <th>Base</th>
              <th>G</th>
              <th>F</th>
              <th>T</th>
              <th>G</th>
              <th>F</th>
              <th>T</th>


            </tr>
          </thead>
          <!--Table head-->

          <!--Table body-->
          <tbody>
            <?php
            $somme=0;
            $sommefirstcycle=0;
            $sommesecondcycle=0;
            $k=1;
            //²nous allons compter le nombre de classe au premier cycle
            $premiercycleclasse=$etabs->getlisteclassepremiercycle($_SESSION['user']['codeEtab'],$_SESSION['user']['session']);
            $deuxiemecycleclasse=$etabs->getlisteclassedeuxiemecycle($_SESSION['user']['codeEtab'],$_SESSION['user']['session']);
            $nbpremier=count($premiercycleclasse);
            $nbdeuxieme=count($deuxiemecycleclasse);
            echo $nbpremier." / ".$nbdeuxieme;
            for($i=0;$i<$nbniveaux;$i++)
            {
              if($tabniveau[$i]=="TOTALFIRST"||$tabniveau[$i]=="TOTALSECONDE"||$tabniveau[$i]=="TOTALGENE")
              {
                  if($tabniveau[$i]=="TOTALFIRST")
                  {
                    ?>
                    <tr>
                      <td ><?php echo getclasseNiveau($tabniveau[$i]); ?></td>
                      <td >
                        <?php

                         ?>
                      </td>
                      <td ><?php echo $sommefirstcycle; ?></td>
                      <td >0</td>
                      <td >0</td>
                      <td >0</td>
                      <td >0</td>
                      <td >0</td>
                      <td >0</td>
                    </tr>
                    <?php
                  }else if($tabniveau[$i]=="TOTALSECONDE")
                  {
                    ?>
                    <tr>
                      <td ><?php echo getclasseNiveau($tabniveau[$i]); ?></td>
                      <td >
                        <?php

                         ?>
                      </td>
                      <td ><?php echo $sommesecondcycle; ?></td>
                      <td >0</td>
                      <td >0</td>
                      <td >0</td>
                      <td >0</td>
                      <td >0</td>
                      <td >0</td>
                    </tr>
                    <?php
                  }else if($tabniveau[$i]=="TOTALGENE")
                  {
                    ?>
                    <tr>
                      <td ><?php echo getclasseNiveau($tabniveau[$i]); ?></td>
                      <td >
                        <?php

                         ?>
                      </td>
                      <td ><?php echo $sommesecondcycle+$sommefirstcycle; ?></td>
                      <td >0</td>
                      <td >0</td>
                      <td >0</td>
                      <td >0</td>
                      <td >0</td>
                      <td >0</td>
                    </tr>
                    <?php
                  }
                ?>

                <?php
              }else {
                ?>
                <tr>
                  <td ><?php echo getclasseNiveau($tabniveau[$i]); ?></td>
                  <td >
                  <?php
                  if($tabniveau[$i]=="SIXIEME"||$tabniveau[$i]=="CINQUIEME"||$tabniveau[$i]=="QUATRIEME"||$tabniveau[$i]=="TROISIEME")
                  {

                    $datas=$etabs->getClassesbyNiveau($_SESSION['user']['codeEtab'],$_SESSION['user']['session'],$tabniveau[$i]);
                    $content="";
                    foreach ($datas as $value):
                      $content=$content.$value->libelle_classe.",";
                    endforeach;
                    $content=substr($content, 0, -1);
                    echo $content;
                    $sommefirstcycle=$sommefirstcycle+count($datas);

                  }else if($tabniveau[$i]=="SECONDE"||$tabniveau[$i]=="PREMIERE"||$tabniveau[$i]=="TERMINAL") {

                    $datas=$etabs->getClassesbyNiveau($_SESSION['user']['codeEtab'],$_SESSION['user']['session'],$tabniveau[$i]);
                    $content="";
                    foreach ($datas as $value):
                      $content=$content.$value->libelle_classe.",";
                    endforeach;
                    $content=substr($content, 0, -1);
                    echo $content;
                      $sommesecondcycle=$sommesecondcycle+count($datas);

                  }

                   ?>
                  </td>
                  <td >
                    <?php

                    echo count($datas);
                   ?></td>
                  <td ><?php
                  $garcons=$etabs->getAllMenClassebyNiveau($_SESSION['user']['codeEtab'],$_SESSION['user']['session'],$tabniveau[$i]);
                  // var_dump($garcons);
                  $nbgarcons=count($garcons);
                  echo $nbgarcons;
                   ?></td>
                  <td >
                    <?php
                    $filles=$etabs->getAllFilleClassebyNiveau($_SESSION['user']['codeEtab'],$_SESSION['user']['session'],$tabniveau[$i]);
                    // var_dump($filles);
                    $nbfilles=count($filles);
                    echo $nbfilles;
                     ?>
                  </td>
                  <td >0</td>
                  <td >0</td>
                  <td >0</td>
                  <td >0</td>
                </tr>
                <?php
              }
              ?>


                <?php

                //nous allons compter le nombre de classe par niveau

                $classebyniveau=$etabs->getClassesbyNiveau($_SESSION['user']['codeEtab'],$_SESSION['user']['session'],$tabniveau[$i]);
                $nbclassebyniveau=count($classebyniveau);
                // echo $nbclassebyniveau;
                $concatids="";
                foreach ($classebyniveau as  $value):
                  $concatids=$concatids.$value->id_classe."*";
                endforeach;
                $concatids=substr($concatids, 0, -1);
                $tabconcatids=explode("*",$concatids);
                $nbconcatids=count($tabconcatids);
                $cumul=0;
                $cumulgenerale=0;
                for($j=0;$j<=$nbconcatids;$j++)
                {


                }

                 ?>



              <!-- </tr> -->
              <?php
              $k++;
            }
             ?>


          </tbody>

          <!--Table body-->


        </table>
        <!--Table-->
      </div>
</section>
<!--Section: Live preview-->
					                                    </div>
					                                </div>
					                            </div>
					                        </div>
					                    </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>



                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
 <script src="../cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" ></script>
 	<script src="../cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
 	<script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" ></script>
 	<script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" ></script>
 	<script src="../cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   // function SetcodeEtab(codeEtab)
   // {
   //   var etape=3;
   //   $.ajax({
   //     url: '../ajax/sessions.php',
   //     type: 'POST',
   //     async:false,
   //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
   //     dataType: 'text',
   //     success: function (content, statut) {
   //
   // window.location.reload();
   //
   //     }
   //   });
   // }
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }


   $("#section").select2();
   $("#libetab").select2();
   $("#classex").select2();

   function searchclassesection()
   {
     var etape=10;
     var codeEtab="<?php echo $_SESSION['user']['codeEtab'] ?>";
     var session="<?php echo $libellesessionencours ?>";
      $.ajax({
        url: '../ajax/classe.php',
        type: 'POST',
        async:false,
        data: 'section='+ $("#section").val()+'&etape='+etape+'&session='+session+'&codeEtab='+codeEtab,
        dataType: 'text',
        success: function (content, statut) {

          $("#classex").html("");
          $("#classex").html(content);

        }
      });
   }

   function searchlibetab()
   {

     var etape=2;

     $.ajax({
              url: '../ajax/school.php',
              type: 'POST',
              async:false,
              data: 'code='+ $("#codeetab").val()+'&etape='+etape,
              dataType: 'text',
              success: function (content, statut) {

                $("#libetab").html("");
                $("#libetab").html(content);

                //nous allons chercher la liste des admins local de cet etablissement

                $.ajax({
                         url: '../ajax/classe.php',
                         type: 'POST',
                         async:false,
                         data: 'code='+ $("#codeetab").val()+'&etape='+etape,
                         dataType: 'text',
                         success: function (response, statut) {


                           $("#classex").html("");
                           $("#classex").html(response);
                         }
                       });

              }

            });

   }
   $(document).ready(function() {

     $('#example5').DataTable( {
         "scrollX": true

     } );

     $('#example45').DataTable( {

       "scrollX": true,
       "language": {
           "lengthMenu": "_MENU_  ",
           "zeroRecords": "Aucune correspondance",
           "info": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
           "infoEmpty": "Aucun enregistrement disponible",
           "infoFiltered": "(filtered from _MAX_ total records)",
           "sEmptyTable":"Aucune donnée disponible dans le tableau",
            "sSearch":"Rechercher :",
            "oPaginate": {
       "sFirst":    "Premier",
       "sLast":     "Dernier",
       "sNext":     "Suivant",
       "sPrevious": "Précédent"
     }
     },

         dom: 'Bfrtip',
         buttons: [
             // 'copyHtml5',

             // 'excelHtml5',
             {
               extend: 'excelHtml5',
               title: 'Data export',
               exportOptions: {
                             columns: "thead th:not(.noExport)"
                         }
             }
             // 'csvHtml5',
             // 'pdfHtml5'
         ]
     } );



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
