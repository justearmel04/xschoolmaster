<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Teatcher.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$matiere=new Matiere();
$teatcher=new Teatcher();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

if(strlen($_SESSION['user']['codeEtab'])>0)
{
  $codeEtabAssigner=$_SESSION['user']['codeEtab'];
}else {
  $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
}
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//nous allons chercher la liste des etablissements du groupe

$allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
$agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$libellesessionencours="";

  if($etablissementType==1||$etablissementType==3)
  {
    if($nbsessionOn>0){
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];

    }

  }else {
    if($nbsessionOn>0)
    {
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];
      $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
      $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
    }
  }

  if(strlen($_SESSION['user']['codeEtab'])>0)
  {

  }else {
    $_SESSION['user']['codeEtab']=$codeEtabAssigner;
  }

  $_SESSION['user']['session']=$libellesessionencours;

  $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

  $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


$matiereslibelles=$etabs->getAllmatierelibellesEtab($libellesessionencours,$_SESSION['user']['codeEtab']);
$matieres=$matiere->GetAllMatiereEtabs($_SESSION['user']['codeEtab'],$libellesessionencours);
$teatchers=$teatcher->getAllTeatchersBySchoolCode($_SESSION['user']['codeEtab']);
$nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
  // var_dump($allcodeEtabs);

// echo $libellesessionencours;
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
  <!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
  <!--bootstrap -->
  <!--bootstrap -->
  <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
  <!-- Material Design Lite CSS -->
  <link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
  <link href="../assets2/css/material_style.css" rel="stylesheet">
  <!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <link rel="stylesheet" href="../assets2/css/pages/steps.css">

 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                        <div class="page-title"><?php echo L::MatiereGestions ?></div>

                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="matieres.php"><?php echo L::MatiereMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active"><?php echo L::MatiereGestions ?></li>

                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['deletesubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['deletesubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: '<?php echo L::Okay ?>',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })


                </script>
                      <?php
                      unset($_SESSION['user']['deletesubjectok']);
                    }

                     ?>
                     <?php
                     if(isset($_SESSION['user']['messages']))
                     {
                       ?>
                       <div class="alert alert-success" role="alert">
                       <?php echo $_SESSION['user']['messages']; ?>
                     </div>
                       <?php
                       unset($_SESSION['user']['messages']);
                       // unset($_SESSION['user']['rappelid']);
                     }
                      ?>

              <?php

                    if(isset($_SESSION['user']['updatesubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['updatesubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: '<?php echo L::Okay ?>',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })


                </script>
                      <?php
                      unset($_SESSION['user']['updatesubjectok']);
                    }

                     ?>
              <?php

                    if(isset($_SESSION['user']['addsubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>


                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['addsubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: '<?php echo L::Okay ?>',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })
                </script>
                      <?php
                      unset($_SESSION['user']['addsubjectok']);
                    }

                     ?>

                     <div class="col-md-12 col-sm-12">
                                   <div class="panel tab-border card-box">
                                       <header class="panel-heading panel-heading-gray custom-tab ">
                                           <ul class="nav nav-tabs">

                           <li class="nav-item"><a href="#home" data-toggle="tab" class="active"><i class="fa fa-bars"></i> <?php echo L::SubjectsListes ?></a>
                           </li>



                                                   <li class="nav-item"><a href="#about" data-toggle="tab"><i class="fa fa-plus-circle"></i> <?php echo L::AddaSubjects ?></a>
                                                   </li>
                                                   <li class="nav-item"><a href="#about1" data-toggle="tab"><i class="fa fa-plus-circle"></i><?php echo L::LibelleSubjects ?> </a>
                                                   </li>





                                           </ul>
                                       </header>
                                       <div class="panel-body">
                                           <div class="tab-content">
                                               <div class="tab-pane active" id="home">
                                                 <div class="row">
                        <div class="col-md-12">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header></header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">

                                    <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                        <thead>
                                            <tr>

                                                <th> <?php echo L::ClasseMenu ?> </th>
                                                <th> <?php echo L::MatiereMenusingle ?></th>
                                                <th> <?php echo L::ProfsMenusingle ?> </th>
                                                <?php
                                                   if($_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Accountant")
                                                   {

                                                   }else {
                                                     ?>
                                                     <th> <?php echo L::Actions ?> </th>
                                                     <?php
                                                   }
                                                 ?>

                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                          // var_dump($matieres);

                                            $i=1;
                                              foreach ($matieres as $value):
                                           ?>
                                            <tr class="odd gradeX">

                                                <td> <?php echo $value->libelle_classe;?></td>
                                                <td>
                                                    <?php echo $value->libelle_mat;?>
                                                </td>
                                                <td>
                                                  <?php
                                                  if($value->teatcher_mat>0)
                                                  {
                                                    ?>
                                                    <label class="label label-primary"><?php echo $etabs->getUtilisateurName($value->teatcher_mat);?></label>
                                                    <?php
                                                  }else {
                                                    ?>
                                                    <label class="label label-danger"><?php echo L::NoTeatchers ?></label>
                                                    <?php
                                                  }
                                                   ?>
                                                </td>

                                                <?php
                                                // if($_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Accountant")
                                                // {
                                                //
                                                // }else {
                                                  ?>
                                                  <td class="valigntop">
                                                    <?php
                                                    // if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="CC"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="CD")
                                                    // {
                                                      ?>
                                                      <a href="#"  data-toggle="modal" data-target="#exampleModal"class="btn btn-info  btn-xs " style="border-radius:3px;">
                                                        <i class="fa fa-pencil"></i>
                                                      </a>
                                                      <?php
                                                    // }
                                                     ?>

                                                    <!--a href="#"  onclick="modify(<?php //echo $value->id_mat;?>)" class="btn btn-info  btn-md " style="border-radius:3px;">
                                                      <i class="fa fa-pencil"></i>
                                                    </a-->
                                                    <?php

                                                      // if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG")
                                                      // {
                                                        ?>

                                                        <a href="#"  onclick="deleted(<?php echo $value->id_mat;?>,<?php echo $value->teatcher_mat?>,<?php echo $value->classe_mat ?>)" class="btn btn-danger  btn-xs " style="border-radius:3px;">
                                                          <i class="fa fa-trash-o"></i>
                                                        </a>

                                                        <?php
                                                      // }

                                                     ?>


                                                  </td>
                                                  <?php
                                                // }
                                                 ?>


                                            </tr>


                                            <?php
                                            $i++;
                                            endforeach;
                                            ?>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                                               </div>
                                               <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                               					    <div class="modal-dialog" role="document">
                                               					        <div class="modal-content">
                                               					            <div class="modal-header">
                                               					                <h4 class="modal-title" id="exampleModalLabel"><?php echo L::ModificationSubjects ?></h4>
                                               					                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                               					                    <span aria-hidden="true">&times;</span>
                                               					                </button>
                                               					            </div>
                                               					            <div class="modal-body">
                                                                       <form  id="FormUpdateSubject" class="form-horizontal" action="../controller/matiere.php" method="post">
                                                                           <div class="form-body">
                                                                             <div class="form-group row">
                                                                                     <label class="control-label col-md-3"><?php echo L::MatiereMenusingle ?>
                                                                                         <span class="required"> * </span>
                                                                                     </label>
                                                                                     <div class="col-md-8">
                                                                                         <input type="text" name="matiere" id="matiere"  data-required="1" value="" placeholder="" class="form-control " />
                                                                                         <p id="messageMat"></p>
                                                                                        </div>
                                                                              </div>
                                                                              <div class="form-group row">
                                                                                      <label class="control-label col-md-3"><?php echo L::Coefs ?>
                                                                                          <span class="required"> * </span>
                                                                                      </label>
                                                                                      <div class="col-md-8">
                                                                                          <input type="number" name="coef" id="coef" data-required="1" value="" placeholder="" class="form-control " />
                                                                                          <p id="messageCoef"></p>
                                                                                         </div>
                                                                               </div>

                                                                             <div class="form-group row">
                                                                                     <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                                                                         <span class="required"> * </span>
                                                                                     </label>
                                                                                     <div class="col-md-8">
                                                                                         <select class="form-control " name="classe" id="classe"   style="width:100%">
                                                                                             <option value=""><?php echo L::Selectclasses ?></option>
                                                                                             <?php
                                                                                             $i=1;
                                                                                               foreach ($classes as $valueClasse):
                                                                                               ?>
                                                                                               <option value="<?php echo utf8_encode(utf8_decode($valueClasse->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($valueClasse->libelle_classe)); ?></option>

                                                                                               <?php
                                                                                                                                $i++;
                                                                                                                                endforeach;
                                                                                                                                ?>

                                                                                         </select>
                                                                                         <input type="hidden" name="etape" id="etape" value="2"/>
                                                                                         <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $_SESSION['user']['codeEtab']; ?>"/>
                                                                                         <input type="hidden" name="idmat" id="idmat" value=""/>
                                                                                         <p id="messageClasse"></p>
                                                                                 </div>
                                                                               </div>






                                                         <div class="form-actions">
                                                                               <div class="row">
                                                                                   <div class="offset-md-3 col-md-9">
                                                                                       <button type="submit"  class="btn btn-info"><?php echo L::ModifierBtn ?></button>
                                                                                       <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo L::AnnulerBtn ?></button>
                                                                                   </div>
                                                                                 </div>
                                                                              </div>
                                                       </div>
                                                                       </form>
                                               					            </div>

                                               					        </div>
                                               					    </div>
                                               					</div>


                                                  <div class="tab-pane" id="about">
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-12">
                                                            <div class="card card-box">
                                                                <div class="card-head">
                                                                    <header></header>

                                                                </div>

                                                                <div class="card-body" id="bar-parent">
                                                                    <form  id="FormAddSubject" class="form-horizontal" action="../controller/matiere.php" method="post">
                                                                        <div class="form-body">
                                                                          <div class="form-group row">
                                                                                  <label class="control-label col-md-3"><?php echo L::MatiereMenusingle ?>
                                                                                      <span class="required"> * </span>
                                                                                  </label>
                                                                                  <div class="col-md-5">
                                                                                    <select class="form-control " name="matiere" id="matiere"  style="width:100%">
                                                                                        <option value=""><?php echo L::SelectSubjects ?></option>
                                                                                        <?php
                                                                                        $i=1;
                                                                                          foreach ($matiereslibelles as $value):
                                                                                          ?>
                                                                                          <option value="<?php echo utf8_encode(utf8_decode($value->libelle_matlib)); ?>"><?php echo utf8_decode($value->libelle_matlib); ?></option>

                                                                                          <?php
                                                                                                                           $i++;
                                                                                                                           endforeach;
                                                                                                                           ?>

                                                                                    </select>
                                                                                      <!--input type="text" name="matiere" id="matiere" data-required="1" placeholder="Entrer la classe" class="form-control " /-->

                                                                                      <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $_SESSION['user']['codeEtab']; ?>"/>
                                                                                      <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">

                                                                                    </div>
                                                                           </div>
                                                                           <div class="form-group row">
                                                                                   <label class="control-label col-md-3"><?php echo L::Coefs ?>ss
                                                                                       <span class="required"> * </span>
                                                                                   </label>
                                                                                   <div class="col-md-5">
                                                                                       <input type="number"  name="coef" id="coef" data-required="1" placeholder="<?php echo L::EnterCoefSubjectControlLib ?>" class="form-control " /> </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                    <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                                                                        <span class="required"> * </span>
                                                                                    </label>
                                                                                    <div class="col-md-5">
                                                                                        <select class="form-control "  name="classe" id="classe"  style="width:100%">
                                                                                            <option value=""><?php echo L::Selectclasses ?></option>
                                                                                            <?php
                                                                                            $i=1;
                                                                                              foreach ($classes as $value):
                                                                                              ?>
                                                                                              <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                                                              <?php
                                                                                                                               $i++;
                                                                                                                               endforeach;
                                                                                                                               ?>

                                                                                        </select>
                                                                                         <input type="hidden" name="etape" id="etape" value="6"/>
                                                                                </div>
                                                                              </div>

                                                                            <!-- <div class="form-group row">
                                                                                  <label class="control-label col-md-3"><?php //echo L::ProfsMenusingle ?>
                                                                                      <span class="required"> * </span>
                                                                                  </label>
                                                                                  <div class="col-md-5">

                                                                                    <select class="form-control " id="teatcher" name="teatcher" style="width:100%">
                                                                                        <option value=""><?php //echo L::PleaseselectTeatEnter ?></option>
                                                                                        <?php
                                                                                        $i=1;
                                                                                        //  foreach ($teatchers as $value):
                                                                                          ?>
                                                                                          <option value="<?php //echo $value->id_compte?>"><?php e//cho utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)) ?></option>

                                                                                          <?php
                                                                                                                           //$i++;
                                                                                                                          // endforeach;
                                                                                                                           ?>

                                                                                    </select>
                                                                                    </div>
                                                                              </div> -->






                                                      <div class="form-actions">
                                                                            <div class="row">
                                                                                <div class="offset-md-3 col-md-9">
                                                                                    <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                                                                    <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                                </div>
                                                                              </div>
                                                                           </div>
                                                    </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                   </div>

                                                <div class="tab-pane" id="about1">
                                                  <div class="row">
                                                      <div class="col-md-12 col-sm-12">
                                                          <div class="card card-box">
                                                              <div class="card-head">
                                                                  <header></header>

                                                              </div>

                                                              <div class="card-body" id="bar-parent">
                                                                  <form  id="FormAddmatlib" class="form-horizontal" action="../controller/matiere.php" method="post">
                                                                      <div class="form-body">
                                                                        <div class="form-group row">
                                                                                <label class="control-label col-md-3"><?php
                                                                                 echo L::LibelleSubjects;


                                                                                 ?>
                                                                                    <span class="required"> * </span>
                                                                                </label>
                                                                                <div class="col-md-5">
                                                                                    <input type="text" name="matierelib" id="matierelib" data-required="1" placeholder="<?php   if($_SESSION['user']['primaire']==1){  echo L::EnterLibellecCompetence;}else {echo L::EnterLibelleSubjects;}?>" class="form-control" />
                                                                                    <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $_SESSION['user']['codeEtab']; ?>"/>
                                                                                      <input type="hidden" name="etape" id="etape" value="5"/>
                                                                                      <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>"/>
                                                                                    <!-- input type="hidden" name="libellesession" id="libellesession" value="<?php //echo $libellesessionencours; ?>" -->

                                                                                  </div>
                                                                         </div>







                                                    <div class="form-actions">
                                                                          <div class="row">
                                                                              <div class="offset-md-3 col-md-9">
                                                                                  <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                                                                  <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                              </div>
                                                                            </div>
                                                                         </div>
                                                  </div>
                                                                  </form>
                                                              </div>
                                                          </div>
                                                      </div>

                                                  </div>
                                                 </div>


                                           </div>
                                       </div>
                                   </div>
                               </div>



          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
   <script src="../assets/js/formatter/jquery.formatter.min.js"></script>

 <script>

 function deleted(id,teatcherid,classe)
 {
   // var classe="<?php //echo $value->classe_mat;?>";
   var codeEtab="<?php echo $_SESSION['user']['codeEtab'];?>";
   var sessionEtab="<?php echo $libellesessionencours;?>";
   Swal.fire({
 title: '<?php echo L::WarningLib ?>',
 text: "<?php echo L::DououReallyDeletingThisSubject ?>",
 type: 'warning',
 showCancelButton: true,
 confirmButtonColor: '#3085d6',
 cancelButtonColor: '#d33',
 confirmButtonText: '<?php echo L::DeleteLib ?>',
 cancelButtonText: '<?php echo L::AnnulerBtn ?>',
 }).then((result) => {
 if (result.value) {
 var etape=20;
 $.ajax({

 url: '../ajax/matiere.php',
 type: 'POST',
 async:false,
 data: 'classe=' +classe+'&codeEtab='+codeEtab+'&etape='+etape+'&sessionEtab='+sessionEtab+'&teatcherid='+teatcherid+'&matiere='+id,
 dataType: 'text',
 success: function (content, statut)
 {
 // $("#matiere").html("");
 window.location.reload();
 }

 });

 // document.location.href="../controller/matiere.php?etape=3&matiere="+id+"&classe="+classe+"&codeEtab="+codeEtab;
 }else {

 }
 })
 }

function GetTeatcherclasses()
{
  var classe=$("#classe").val();
  var codeEtab="<?php echo $_SESSION['user']['codeEtab']; ?>";
  var sessionEtab="<?php echo $libellesessionencours ?>";

  var etape=12;

  $.ajax({
    url: '../ajax/teatcher.php',
    type: 'POST',
    async:false,
    data: 'etape=' + etape+ '&codeEtab=' +codeEtab+ '&sessionEtab=' +sessionEtab+ '&classe=' +classe,
    dataType: 'text',
    success: function (content, statut) {

// window.location.reload();
$("#teatcher").html("");
$("#teatcher").html(content);

    }
  });


}

 // function SetcodeEtab(codeEtab)
 // {
 //   var etape=3;
 //   $.ajax({
 //     url: '../ajax/sessions.php',
 //     type: 'POST',
 //     async:false,
 //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
 //     dataType: 'text',
 //     success: function (content, statut) {
 //
 // window.location.reload();
 //
 //     }
 //   });
 // }
 function addFrench()
 {
   var etape=1;
   var lang="fr";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function addEnglish()
 {
   var etape=1;
   var lang="en";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function determineteatcher()
 {
   var classe=$("#classe").val();
   var session="<?php echo $libellesessionencours; ?>";
   var codeEtab="<?php echo $_SESSION['user']['codeEtab']; ?>";
   var etape=11;

   $.ajax({
            url: '../ajax/matiere.php',
            type: 'POST',
            async:false,
            data: 'classe='+ classe+'&etape='+etape+'&session='+session+'&codeEtab='+codeEtab,
            dataType: 'text',
            success: function (response, statut) {


              $("#teatcher").html("");
              $("#teatcher").html(response);
            }
          });


 }

 jQuery(document).ready(function() {


$("#FormUpdateSubject #coef").formatter({pattern:"{{999}}"});
$("#FormAddSubject #coef").formatter({pattern:"{{999}}"});



// $("#teatcher").select2();
$("#FormUpdateSubject #matiere").select2();
$("#FormAddSubject #matiere").select2();

<?php
if($etablissementType==1||$etablissementType==3)
{
  ?>
$("#classe").select2();
  <?php
}else {
  ?>
  $("#classe").select2({

    tags: true,

  tokenSeparators: [',', ' ']

  });
  <?php
}
 ?>
//

$("#FormUpdateSubject").validate({

  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
},
highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
},
success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
   rules:{

     matiere:"required",
     classe:"required",
     teatcher:"required",
     coef:"required"


   },
   messages: {
     matiere:"<?php echo L::PleaseEnterMatiere ?>",
     classe:"<?php echo L::PleaseSelectclasserequired ?>",
     coef:"<?php echo L::SubjectcoefSelectedrequired ?>"

   },
   submitHandler: function(form) {




          }


        });

  $("#FormAddmatlib").validate({

    errorPlacement: function(label, element) {
    label.addClass('mt-2 text-danger');
    label.insertAfter(element);
  },
  highlight: function(element, errorClass) {
    $(element).parent().addClass('has-danger')
    $(element).addClass('form-control-danger')
  },
  success: function (e) {
        $(e).closest('.control-group').removeClass('error').addClass('info');
        $(e).remove();
    },
      rules:{
        matierelib:"required"
      },
      messages:{
        <?php
        if($_SESSION['user']['primaire']==1)
        {
          ?>
          matierelib:"<?php echo L::LibelleComprequired ?>"
          <?php
        }else {
          ?>
          matierelib:"<?php echo L::PleaseEnterLibelleSubjects ?>"
          <?php
        }
         ?>

      },
        submitHandler: function(form) {
          //nous allons verifié si cet libelle de matiere n'existe pas deja au niveau de la base

          var etape=13;

          $.ajax({
            url: '../ajax/matiere.php',
            type: 'POST',
            async:true,
            data: 'matiere=' + $("#matierelib").val()+ '&etape=' + etape+'&codeEtab='+$("#FormAddmatlib #codeEtab").val()+'&session='+$("#FormAddmatlib #libellesession").val(),
            dataType: 'text',
            success: function (content, statut) {

              // alert(content);
              if(content==0)
              {
                //nouveau libelle a ajouter
                form.submit();
              }else if(content>0)
              {
                //cet libelle existe deja  dans le sysyème

                Swal.fire({
                type: 'warning',
                title: '<?php echo L::WarningLib ?>',
                text: "<?php echo L::LibelleSubjectsAllreadyExist ?>",

              })
              }
            }
          });
        }

  });


  $("#FormAddSubject1").validate({

    errorPlacement: function(label, element) {
    label.addClass('mt-2 text-danger');
    label.insertAfter(element);
  },
  highlight: function(element, errorClass) {
    $(element).parent().addClass('has-danger')
    $(element).addClass('form-control-danger')
  },
  success: function (e) {
        $(e).closest('.control-group').removeClass('error').addClass('info');
        $(e).remove();
    },
     rules:{

       matiere:"required",
       classe:"required",
       teatcher:"required",
       coef:{
     required: true,
     number: true,
     // range:[0.5,10]
   }


     },
     messages: {
       matiere:"<?php echo L::PleaseselectCompetence ?>",
       classe:"<?php echo L::PleaseSelectclasserequired ?>",
       teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
       coef:{
     required:"<?php echo L::SubjectcoefSelectedrequired ?>",
     // number:"Merci de renseigner un nombre",
     // min:"Merci de renseigner le coefficient de la matière"
   }

     },
     submitHandler: function(form) {


       var etape=1;

        $.ajax({
          url: '../ajax/matiere.php',
          type: 'POST',
          async:true,
          data: 'matiere=' + $("#matiere").val()+ '&etape=' + etape+'&classe='+$("#classe").val()+'&teatcher='+$("#teatcher").val()+'&codeEtab='+$("#codeEtab").val()+'&coefficient='+$("#coef").val(),
          dataType: 'text',
          success: function (content, statut) {


            if(content==0)
            {
              //cette matière n'existe pas encore pour cette classe

              form.submit();

            }else if(content==1)
            {
              //il est question d'un nouveau professeur pour cette matière
              Swal.fire({
  title: '<?php echo L::WarningLib ?>',
  text: "<?php echo L::CompetencesAllreadyDispense ?>",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::ModifierBtn ?>',
  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
  }).then((result) => {
  if (result.value) {

  //nous allons modifier la valeur de etape pour 4
  $("#FormAddSubject1 #etape").val(4);
  $("#FormAddSubject1").submit();

  }else {

  }
  })

            }else if(content==2)
            {
              // il s'agit du meme professeur pour cette matiere

              Swal.fire({
              type: 'warning',
              title: '<?php echo L::WarningLib ?>',
              text: "<?php echo L::ThisSubjectAllreadyExist ?>",

            })
            }

          }
        });

            }


          });


   $("#FormAddSubject").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

        matiere:"required",
        classe:"required",
        teatcher:"required",
        coef:{
      required: true,
      number: true,
      // range:[0.5,10]
    }


      },
      messages: {
        matiere:"<?php echo L::PleaseEnterMatiere ?>",
        classe:"<?php echo L::PleaseSelectclasserequired ?>",
        teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
        coef:{
      required:"<?php echo L::SubjectcoefSelectedrequired ?>",
      // number:"Merci de renseigner un nombre",
      // min:"Merci de renseigner le coefficient de la matière"
    }

      },
      submitHandler: function(form) {


        var etape=1;

         $.ajax({
           url: '../ajax/matiere.php',
           type: 'POST',
           async:true,
           data: 'matiere=' + $("#matiere").val()+ '&etape=' + etape+'&classe='+$("#classe").val()+'&teatcher='+$("#teatcher").val()+'&codeEtab='+$("#codeEtab").val()+'&coefficient='+$("#coef").val(),
           dataType: 'text',
           success: function (content, statut) {


             if(content==0)
             {
               //cette matière n'existe pas encore pour cette classe

               form.submit();

             }else if(content==1)
             {
               //il est question d'un nouveau professeur pour cette matière
               Swal.fire({
 title: '<?php echo L::WarningLib ?>',
 text: "<?php echo L::SubjectsAllreadyDispense ?>",
 type: 'warning',
 showCancelButton: true,
 confirmButtonColor: '#3085d6',
 cancelButtonColor: '#d33',
 confirmButtonText: '<?php echo L::ModifierBtn ?>',
 cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
 if (result.value) {

//nous allons modifier la valeur de etape pour 4
$("#FormAddSubject #etape").val(4);
$("#FormAddSubject").submit();

 }else {

 }
})

             }else if(content==2)
             {
               // il s'agit du meme professeur pour cette matiere

               Swal.fire({
               type: 'warning',
               title: '<?php echo L::WarningLib ?>',
               text: "<?php echo L::ThisSubjectAllreadyExist ?>",

             })
             }

           }
         });

             }


           });
      });






 </script>
    <!-- end js include path -->
  </body>

</html>
