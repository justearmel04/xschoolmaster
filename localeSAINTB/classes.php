<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

if(strlen($_SESSION['user']['codeEtab'])>0)
{
  $codeEtabAssigner=$_SESSION['user']['codeEtab'];
}else {
  $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
}
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//nous allons chercher la liste des etablissements du groupe

$allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
$agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$libellesessionencours="";

  if($etablissementType==1||$etablissementType==3)
  {
    if($nbsessionOn>0){
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];

    }

  }else {
    if($nbsessionOn>0)
    {
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];
      $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
      $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
    }
  }

  if(strlen($_SESSION['user']['codeEtab'])>0)
  {

  }else {
    $_SESSION['user']['codeEtab']=$codeEtabAssigner;
  }

  $_SESSION['user']['session']=$libellesessionencours;

  $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

  $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


  // var_dump($allcodeEtabs);
  $nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
// echo $libellesessionencours;
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::ClassesMenu ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="#"><?php echo L::ClassesMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::AllClassesMenu ?></li>
                            </ol>
                        </div>
                    </div>

                    <?php

                          if(isset($_SESSION['user']['addclasseok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: '<?php echo L::Felicitations ?>',
                      text: '<?php echo $_SESSION['user']['addclasseok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['addclasseok']);
                          }

                           ?>

                    <?php

                          if(isset($_SESSION['user']['updateclasseok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: '<?php echo L::Felicitations ?>',
                      text: '<?php echo $_SESSION['user']['updateclasseok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['updateclasseok']);
                          }

                           ?>



                    <?php

                          if(isset($_SESSION['user']['delclasseok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: '<?php echo L::Felicitations ?>',
                      text: '<?php echo $_SESSION['user']['delclasseok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['delclasseok']);
                          }

                           ?>
					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['updateteaok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: '<?php echo L::Felicitations ?>',
            text: '<?php echo $_SESSION['user']['updateteaok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['updateteaok']);
                }

                 ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>
                 <?php
/*
                  ?>
<div class="row">
  <div class="col-md-12 col-sm-12">
                            <div class="card card-box">
                                <div class="card-head">
                                    <header><?php echo L::Seacher ?></header>

                                </div>
                                <div class="card-body " id="bar-parent">
                                  <form method="post" id="FormSearch">
                                      <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Code Etablissement</label>
                                            <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                                            <select class="form-control " id="codeetab" name="codeetab" onchange="searchlibetab()" style="width:100%">
                                                <option value="">Selectionner un code etablissement</option>
                                                <?php
                                                $i=1;
                                                  foreach ($schoolsofassign as $value):
                                                  ?>
                                                  <option value="<?php echo $value->code_etab?>"><?php echo $value->code_etab?></option>

                                                  <?php
                                                                                   $i++;
                                                                                   endforeach;
                                                                                   ?>

                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Classe</label>
                                            <select class="form-control input-height" id="classex" name="classex" style="width:100%">
                                                <option value=""><?php echo L::Selectclasses ?></option>
                                                <?php
                                                $i=1;
                                                  foreach ($classes as $value):
                                                  ?>
                                                  <option value="<?php echo $value->id_classe?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)) ?></option>

                                                  <?php
                                                                                   $i++;
                                                                                   endforeach;
                                                                                   ?>

                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Nom Etablissement</label>
                                        <select class="form-control " id="libetab" name="libetab" style="width:100%">
                                            <option value="">Selectionner un code etablissement</option>
                                            <?php
                                            $i=1;
                                              foreach ($codesEtab as $value):
                                              ?>
                                              <option value="<?php echo utf8_encode(utf8_decode($value->libelle_etab)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_etab)); ?></option>

                                              <?php
                                                                               $i++;
                                                                               endforeach;
                                                                               ?>

                                        </select>
                                        <input type="hidden" name="search" id="search"/>
                                    </div>


                                </div>
                                      </div>

                                      <button type="submit" class="btn btn-danger"><?php echo L::Seacher ?></button>
                                  </form>
                                </div>
                            </div>
                        </div>
</div>
<?php
  */
 ?>
<div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line">
                               <!--ul class="nav nav-pills nav-pills-rose">
									<li class="nav-item tab-all"><a class="nav-link active show"
										href="#tab1" data-toggle="tab">Liste</a></li>
									<li class="nav-item tab-all"><a class="nav-link" href="#tab2"
										data-toggle="tab">Grille</a></li>
								</ul-->
                                <div class="tab-content">
                                    <div class="tab-pane active fontawesome-demo" id="tab1">
                                        <div class="row">
					                        <div class="col-md-12">
					                            <div class="card  card-box">
					                                <div class="card-head">
					                                    <header></header>
					                                    <div class="tools">
					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
					                                    </div>
					                                </div>
					                                <div class="card-body ">
                                          <div class="pull-right">

<a class="btn btn-primary " href="addclasses.php"><i class="fa fa-plus"></i> <?php echo L::ClassesNewMenu ?></a>




                                                <a class="btn btn-warning " href="#"><i class="fa fa-print"></i> <?php echo L::ClassesListsingle ?></a>

                                          </div>
					                                  <div class="table-scrollable">

                                              <table class="table table-hover table-checkable order-column full-width" id="example4">
					                                        <thead>
					                                            <tr>

                                                          <!-- <th>Section</th> -->
                                                          <th><?php echo L::Nums ?></th>
					                                                <th> <?php echo L::ClasseMenu ?> </th>
                                                          <th> <?php echo L::inscritsNb ?> </th>
                                                          <?php
                                                          if($_SESSION['user']['primaire']==1)
                                                          {
                                                           ?>
                                                          <th><?php echo L::EnseignantNb ?></th>
                                                          <?php
                                                        }
                                                           ?>
                                                          <?php

                                                            ?>
                                                              <th> <?php echo L::Actions ?> </th>
                                                            <?php

                                                            ?>

                                                            <?php

                                                           ?>

					                                            </tr>
					                                        </thead>
					                                        <tbody>
                                                    <?php
                                                    $i=1;
                                                    if(isset($_POST['search']))
                                                    {
                                                      $content="";
                                                        if($_POST['codeetab']!="")
                                                        {

                                                          if(strlen($_POST['classex'])>0)
                                                          {
                                                            //recherche en fonction de l'id de l'enseignant et code etab
                                                            $classes=$classe->getClassesByschoolCodewithId($_SESSION['user']['codeEtab'],$_POST['classex']);
                                                          }else {
                                                            //recherche tous les enseignants en fonction du code etablissement
                                                            //$teatchers=$teatcher->getAllTeatchers();
                                                            $classes=$classe->getAllClassesByschoolCode($_SESSION['user']['codeEtab']);

                                                          }

                                                          $content="bonjour";

                                                        }else

                                                         {
                                                          $content="bonsoir";
                                                            $classes=$classe->getAllClassesByClasseId($_POST['classex']);
                                                        }
                                                      }

//var_dump($teatchers);
                                                      foreach ($classes as $value):
                                                      ?>
																<tr class="odd gradeX" >




																	<!-- <td><?php //echo $classe->DetermineSectionName($value->id_classe,$libellesessionencours);?></td> -->
                                  <td><?php echo $i; ?></td>
                                  <td><?php echo $value->libelle_classe;?></td>


																	<td class="left"><?php echo $classe->DetermineNumberOfStudentInThisClasse($value->id_classe,$value->codeEtab_classe,$libellesessionencours);?></td>
                                  <?php
                                  if($_SESSION['user']['primaire']==1)
                                  {
                                    ?>
<td><?php echo $classe->getnumberOfTeatcherEnseigner($value->id_classe,$value->codeEtab_classe,$libellesessionencours); ?></td>
                                    <?php
                                  }
                                   ?>


                                      <td class="center">
                                        <a href="#"  data-toggle="modal" data-target="#largeModel2" onclick="UpdateThisclasse(<?php echo $value->id_classe ?>,'<?php echo $value->libelle_classe ?>','<?php echo $value->codeEtab_classe ?>','<?php echo $value->session_classe ?>')" class="btn btn-primary btn-xs">
                                          <i class="fa fa-pencil"></i>
                                        </a>
                                            <!-- <a href="#" class="btn btn-success btn-xs" title="<?php //echo L::EnseignantAssignated ?>" data-toggle="modal" data-target="#largeModel" onclick="assignatedThisclasse(<?php echo $value->id_classe ?>,'<?php echo $value->libelle_classe ?>','<?php echo $value->codeEtab_classe ?>','<?php echo $value->session_classe ?>')"><i class="fa fa-plus"></i> </a> -->



                                        <!-- <a class="btn btn-danger btn-xs" onclick="deleted(<?php echo $value->id_classe?>)">
                                          <i class="fa fa-trash-o "></i>
                                        </a> -->
                                        <a href="classinfos.php?classe=<?php echo $value->id_classe;?>&codeEtab=<?php echo $_SESSION['user']['codeEtab'];?>" class="btn btn-warning btn-xs" title="<?php echo L::ClassesListsingle ?>"> <i class="fa fa-eye"></i>  </a>
                                        <!-- <a href="disciplinesclasses.php?classe=<?php //echo $value->id_classe;?>&codeEtab=<?php //echo $_SESSION['user']['codeEtab'];?>" class="btn btn-success btn-xs" title="<?php echo L::DisciplinesClasses ?>"> <i class=" fa fa-bar-chart-o"></i> </a> -->
    																	</td>
                                      <?php

                                   ?>

																</tr>
                                <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                <script>

                                function myFunction(idcompte)
                                {
                                  //var url="detailslocal.php?compte="+idcompte;
                                document.location.href="detailsclasses.php?compte="+idcompte;
                                }

                                function modify(id)
                                {


                                  Swal.fire({
                    title: '<?php echo L::WarningLib ?>',
                    text: "<?php echo L::DoyouReallyModifyingClasses ?>",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<?php echo L::ModifierBtn ?>',
                    cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                  }).then((result) => {
                    if (result.value) {
                      document.location.href="updateclasse.php?compte="+id;
                    }else {

                    }
                  })
                                }

                                function deleted(id)
                                {

                                  Swal.fire({
                    title: '<?php echo L::WarningLib ?>',
                    text: "<?php echo L::DoyouReallydeletedClasses ?>",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<?php echo L::DeleteLib ?>',
                    cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                  }).then((result) => {
                    if (result.value) {
                      document.location.href="../controller/classe.php?etape=3&compte="+id;
                    }else {

                    }
                  })
                                }

                                </script>


                                <?php
                                                                 $i++;
                                                                 endforeach;
                                                                 ?>

															</tbody>
					                                    </table>

					                                    </div>
					                                </div>
					                            </div>
					                        </div>

                                  <div class="modal fade" id="largeModel1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    					    <div class="modal-dialog modal-lg" role="document">
                    					        <div class="modal-content">
                    					            <div class="modal-header">
                    					                <h4 class="modal-title" id="exampleModalLabel"><?php echo L::ListeEnseignantAssignated ?></h4>
                    					                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    					                    <span aria-hidden="true">&times;</span>
                    					                </button>
                    					            </div>
                    					            <div class="modal-body">
                                            <div id="divrecherchestation">



                                         <table class="table table-striped table-bordered table-hover" id="tablemembers">
                    <thead style="background-color:#28a745; color:white; font-weight: bold;">
                    <tr>
                    <th style="width:250px;"><?php echo L::Name ?></th>
                    <th style="width:270px;" class="visible-lg"><?php echo L::PreName ?> </th>
                    <th style="text-align:center"> <?php echo L::TelMobiles ?> </th>

                    <!-- <th style=""><?php //echo L::Actions ?> </th> -->
                    </tr>
                    </thead>
                    <tbody id="tabMemberBody">

                    <tr id="aucuneLinge">

                    <td colspan="3"><?php echo L::NoLigne ?></td>

                    </tr>

                                   </tbody>
                                    </table>
                                    <form class="" action="#">


                                    </form>

                                            </div>
                    					            </div>
                    					            <div class="modal-footer">
                    					                <button type="button" class="btn btn-secondary" data-dismiss="modal" ><?php echo L::Closebtn  ?></button>
                    					                <!-- <button type="button" class="btn btn-primary"  id="btnsaving"><?php //echo L::Saving ?></button> -->
                    					            </div>
                    					        </div>
                    					    </div>
                    					</div>

                                  <div class="modal fade" id="largeModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog modal-xs" role="document">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <h4 class="modal-title" id="exampleModalLabel"><?php echo L::EnseignantAssignated ?></h4>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                              </button>
                                          </div>
                                          <div class="modal-body">
                                            <div id="divrecherchestation">
                                              <div class="table-scrollable">

                                    </div>
                                    <form class="" action="#" id="Assigneteatcher">
                                      <div class="form-group row">
                                          <label class="control-label col-md-12"><?php echo L::ClasseMenu ?>
                                              <span class="required">  </span>
                                          </label>
                                          <div class="col-md-12">
                                            <input type="text" class="form-control" id="libelleclasse" name="libelleclasse" value="" placeholder="<?php echo L::EnterClientNumbers ?>" readonly>
                                            <input type="hidden" name="idclasse" id="idclasse" value="">
                                            <input type="hidden" name="codeEtabclasse" id="codeEtabclasse" value="">
                                            <input type="hidden" name="sessionEtabclasse" id="sessionEtabclasse" value="">

                                          </div>

                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-12"><?php echo L::TeatcherCapsingle ?>
                                              <span class="required">  </span>
                                          </label>
                                          <div class="col-md-12">
                                            <select class="form-control " id="teatcherid" name="teatcherid" style="width:100%">
                                                <option value=""><?php echo L::SeleectAnEnseignant ?></option>


                                            </select>

                                          </div>

                                      </div>


                                           <div class="">

                                             <button type="submit" class="btn btn-primary btn-md" name="button"> <i class="fa fa-check"></i> <?php echo L::AssignatedLib; ?> </button>

                                           </div>


                                    </form>

                                            </div>
                                          </div>
                                          <div class="modal-footer">
                                              <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closing()"><?php echo L::Closebtn  ?></button>
                                              <button type="button" style="display:none" class="btn btn-primary" onclick="validerMember()" id="btnsaving"><?php echo L::Saving ?></button>
                                          </div>
                                      </div>
                                  </div>
                                  </div>


                                  <div class="modal fade" id="largeModel2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog modal-xs" role="document">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <h4 class="modal-title" id="exampleModalLabel"><?php echo L::ClasseModification ?></h4>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                              </button>
                                          </div>
                                          <div class="modal-body">
                                            <div id="divrecherchestation">
                                              <div class="table-scrollable">

                                    </div>
                                    <form class="" action="#" id="Updateclasse">
                                      <div class="form-group row">
                                          <label class="control-label col-md-12"><?php echo L::ClasseMenu ?>
                                              <span class="required">  </span>
                                          </label>
                                          <div class="col-md-12">
                                            <input type="text" class="form-control" id="libelleclasse" name="libelleclasse" value="" placeholder="">
                                            <input type="hidden" name="idclasse" id="idclasse" value="">
                                            <input type="hidden" name="codeEtabclasse" id="codeEtabclasse" value="<?php echo $_SESSION['user']['codeEtab'];?>">
                                            <input type="hidden" name="sessionEtabclasse" id="sessionEtabclasse" value="<?php echo $libellesessionencours; ?>">

                                          </div>

                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-12"><?php echo L::Level ?>
                                              <span class="required">  </span>
                                          </label>
                                          <div class="col-md-12">
                                            <select class="form-control" name="typeclasse" id="typeclasse" style="text-align:center;width:100%">
                                              <option value=""><?php echo L::selectLevel ?></option>
                                              <option value="1-SIXIEME-6e"><?php echo L::Sixieme ?></option>
                                              <option value="1-CINQUIEME-5e"><?php echo L::Cinqieme ?></option>
                                              <option value="1-QUATRIEME-4e"><?php echo L::Quatrieme ?></option>
                                              <option value="1-TROISIEME-3e"><?php echo L::Troisieme ?></option>
                                              <option value="2-SECONDE-2A"><?php echo L::SecondeA ?></option>
                                              <option value="2-SECONDE-2C"><?php echo L::SecondeC ?></option>
                                              <option value="2-PREMIERE-1A"><?php echo L::PremiereA ?></option>
                                              <option value="2-PREMIERE-1D"><?php echo L::PremiereD ?></option>
                                              <option value="2-TERMINAL-TA"><?php echo L::TerminalA ?></option>
                                              <option value="2-TERMINAL-TD"><?php echo L::TerminalD ?></option>
                                            </select>

                                          </div>

                                      </div>

                                      <div class="form-group row">
                                          <label class="control-label col-md-12"><?php echo L::InscriptionDues ?>
                                              <span class="required">  </span>
                                          </label>
                                          <div class="col-md-12">
                                            <input type="text" class="form-control" id="montantinscrip" name="montantinscrip" placeholder="<?php echo L::EnterInscriptionDues ?>">

                                          </div>

                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-12"><?php echo L::ScolarityDues ?>
                                              <span class="required">  </span>
                                          </label>
                                          <div class="col-md-12">
                                          <input type="text" class="form-control" id="montantscola" name="montantscola" placeholder="<?php echo L::EnterScolarityDues ?>">

                                          </div>

                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-12"><?php echo L::AffecterScolarites ?>
                                              <span class="required">  </span>
                                          </label>
                                          <div class="col-md-12">
                                          <input type="text" class="form-control" id="montantscolaaff" name="montantscolaaff" placeholder="<?php echo L::EnterAffecterScolarites ?>">

                                          </div>

                                      </div>



                                           <div class="">

                                             <button type="submit" class="btn btn-primary btn-md" name="button"> <i class="fa fa-check"></i> <?php echo L::ModifierBtn; ?> </button>

                                           </div>


                                    </form>

                                            </div>
                                          </div>
                                          <div class="modal-footer">
                                              <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closing()"><?php echo L::Closebtn  ?></button>
                                              <button type="button" style="display:none" class="btn btn-primary" onclick="validerMember()" id="btnsaving"><?php echo L::Saving ?></button>
                                          </div>
                                      </div>
                                  </div>
                                  </div>


					                    </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>



                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
<script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
  <script src="../assets2/js/pages/table/table_data.js" ></script>
  <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
  <script src="../assets2/js/dropify.js"></script>
  <script src="../assets2/plugins/select2/js/select2.js" ></script>
  <script src="../assets2/js/pages/select2/select2-init.js" ></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
  <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>





    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   function UpdateThisclasse(idclasse,libelleclasse,codeEtab,sessionEtab)
   {
     $("#Updateclasse #libelleclasse").val(libelleclasse);
     $("#Updateclasse #idclasse").val(idclasse);
     $("#Updateclasse #codeEtabclasse").val(codeEtab);
     $("#Updateclasse #sessionEtabclasse").val(sessionEtab);
     var etape=16;
     $.ajax({
       url: '../ajax/classe.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&codeEtab=' +codeEtab+ '&classeid=' +idclasse+'&sessionEtab='+sessionEtab,
       dataType: 'text',
       success: function (content, statut) {

         var inscription=content.split("*")[0];
         var scolarite=content.split("*")[1];
         var scolariteaff=content.split("*")[2];
         var typeselection=content.split("*")[3];

         $("#Updateclasse #montantinscrip").val(inscription);
         $("#Updateclasse #montantscola").val(scolarite);
         $("#Updateclasse #montantscolaaff").val(scolariteaff);
         $("#Updateclasse #typeclasse").html("");
         $("#Updateclasse #typeclasse").html(typeselection);


   // $("#Assigneteatcher #teatcherid").html("");
   // $("#Assigneteatcher #teatcherid").html(content);


       }
     });
   }
   function assignatedThisclasse(idclasse,libelleclasse,codeEtab,sessionEtab)
   {
     $("#Assigneteatcher #libelleclasse").val(libelleclasse);
     $("#Assigneteatcher #idclasse").val(idclasse);
     $("#Assigneteatcher #codeEtabclasse").val(codeEtab);
     $("#Assigneteatcher #sessionEtabclasse").val(sessionEtab);

     // alert(idclasse);

     //nous allons rechercher la liste des enseignants
     var etape=9;
     $.ajax({
       url: '../ajax/teatcher.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&codeEtab=' +codeEtab+ '&classeid=' +idclasse,
       dataType: 'text',
       success: function (content, statut) {

   $("#Assigneteatcher #teatcherid").html("");
   $("#Assigneteatcher #teatcherid").html(content);


       }
     });

   }

   function ListassignatedTetcher(idclasse,libelleclasse,codeEtab,sessionEtab)
   {
     var etape=11;
     $.ajax({
       url: '../ajax/teatcher.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&codeEtab=' +codeEtab+ '&classeid=' +idclasse+ '&sessionEtab=' +sessionEtab,
       dataType: 'text',
       success: function (content, statut) {

   // $("#Assigneteatcher #teatcherid").html("");
   // $("#Assigneteatcher #teatcherid").html(content);

   $("#aucuneLinge").slideUp();
    $("#tabMemberBody").append(content);


       }
     });
   }

   // function SetcodeEtab(codeEtab)
   // {
   //   var etape=3;
   //   $.ajax({
   //     url: '../ajax/sessions.php',
   //     type: 'POST',
   //     async:false,
   //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
   //     dataType: 'text',
   //     success: function (content, statut) {
   //
   // window.location.reload();
   //
   //     }
   //   });
   // }
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function searchlibetab()
   {

     var etape=2;

     $.ajax({
              url: '../ajax/school.php',
              type: 'POST',
              async:false,
              data: 'code='+ $("#codeetab").val()+'&etape='+etape,
              dataType: 'text',
              success: function (content, statut) {

                $("#libetab").html("");
                $("#libetab").html(content);

                //nous allons chercher la liste des admins local de cet etablissement

                $.ajax({
                         url: '../ajax/classe.php',
                         type: 'POST',
                         async:false,
                         data: 'code='+ $("#codeetab").val()+'&etape='+etape,
                         dataType: 'text',
                         success: function (response, statut) {


                           $("#classex").html("");
                           $("#classex").html(response);
                         }
                       });

              }

            });

   }
   $(document).ready(function() {
 $("#typeclasse").select2();
     $('#example5').DataTable( {
         "scrollX": true

     } );

     $("#codeetab").select2();

     $("#libetab").select2();
     $("#classex").select2();
     $("#teatcherid").select2();


     $("#Updateclasse").validate({

       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
     },
     highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
     },
     success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },
        rules:{

          libelleclasse:"required",
          typeclasse:"required",
          montantinscrip:"required",
          montantscola:"required",
          montantscolaaff:"required"


        },
        messages: {

          libelleclasse:"<?php echo L::PleaseEnterclasseLib;?>",
          typeclasse:"<?php echo L::PleaseselectLevel ?>",
          montantinscrip:"<?php echo L::PleaseAddMontAffecterScolarites ?>",
          montantscola:"<?php echo L::PleaseAddMontAffecterScolarites ?>",
          montantscolaaff:"<?php echo L::PleaseAddMontAffecterScolarites ?>"

        },
        submitHandler: function(form) {

          // alert("bonjou");

          var etape=17;
          //
          // var teatcherid=$("#Assigneteatcher #teatcherid").val();
          var idclasse=$("#Updateclasse #idclasse").val();
          var codeEtab=$("#Updateclasse #codeEtabclasse").val();
          var sessionEtab=$("#Updateclasse #sessionEtabclasse").val();
          var libelleclasse=$("#Updateclasse #libelleclasse").val();
          var typeclasse=$("#Updateclasse #typeclasse").val();
          var montantinscrip=$("#Updateclasse #montantinscrip").val();
          var montantscola=$("#Updateclasse #montantscola").val();
          var montantscolaaff=$("#Updateclasse #montantscolaaff").val();
     //
            // var numclient=$("#Modclientnumber #clientnumbermod").val();

          $.ajax({
            url: '../ajax/classe.php',
            type: 'POST',
            async:false,
            data: 'idclasse=' +idclasse+ '&etape=' + etape+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&libelleclasse='+libelleclasse+'&typeclasse='+typeclasse+'&montantinscrip='+montantinscrip+'&montantscola='+montantscola+'&montantscolaaff='+montantscolaaff,
            dataType: 'text',
            success: function (response, statut) {

     window.location.reload();

            }
          });
        }


     });


     $("#Assigneteatcher").validate({

       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
     },
     highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
     },
     success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },
        rules:{

          teatcherid:"required"



        },
        messages: {

          teatcherid:"<?php echo L::PleaseSelectAnEnseignant;?>"

        },
        submitHandler: function(form) {

          // alert("bonjou");

          var etape=10;

          var teatcherid=$("#Assigneteatcher #teatcherid").val();
          var idclasse=$("#Assigneteatcher #idclasse").val();
          var codeEtab=$("#Assigneteatcher #codeEtabclasse").val();
          var sessionEtab=$("#Assigneteatcher #sessionEtabclasse").val();
   //
   //        // var numclient=$("#Modclientnumber #clientnumbermod").val();
   //
          $.ajax({
            url: '../ajax/teatcher.php',
            type: 'POST',
            async:false,
            data: 'teatcherid=' +teatcherid+ '&etape=' + etape+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&classeid='+idclasse,
            dataType: 'text',
            success: function (response, statut) {

   window.location.reload();

            }
          });
        }


     });



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
