<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');
require_once('../class/Student.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$student=new Student();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

if(strlen($_SESSION['user']['codeEtab'])>0)
{
  $codeEtabAssigner=$_SESSION['user']['codeEtab'];
}else {
  $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
}
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//nous allons chercher la liste des etablissements du groupe

$allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
$agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$libellesessionencours="";

  if($etablissementType==1||$etablissementType==3)
  {
    if($nbsessionOn>0){
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];

    }

  }else {
    if($nbsessionOn>0)
    {
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];
      $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
      $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
    }
  }

  if(strlen($_SESSION['user']['codeEtab'])>0)
  {

  }else {
    $_SESSION['user']['codeEtab']=$codeEtabAssigner;
  }

  $_SESSION['user']['session']=$libellesessionencours;





   // var_dump($allcodeEtabs);

  $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

  $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);



    $typesemestrealls=$session->getAllsemestrebyIdsession($sessionencoursid);

  $nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
  // var_dump($allcodeEtabs);
  $onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
  $offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
// echo $libellesessionencours;

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
     <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>


	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::AddnotesMenuEvaluation?> </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><!--i class="fa fa-home"></i-->&nbsp;<a class="parent-item" href="index.php"><?php echo L::Evaluations  ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::AddnotesMenuEvaluation ?> </li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">
              <?php

                    if(isset($_SESSION['user']['addattendailyok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
            <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: '<?php echo L::Felicitations ?>',
                text: '<?php echo $_SESSION['user']['addattendailyok']; ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addattendailyok']);
                    }

                     ?>
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

<br/>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="card card-topline-green">
         <div class="card-head">
             <header></header>
             <div class="tools">
                 <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
  <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
  <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
             </div>
         </div>
         <div class="card-body ">
           <form method="post" id="FormAddNotes" action="addnotes.php">
               <div class="row">
                 <div class="col-md-6 col-sm-6">
                 <!-- text input -->
                 <div class="form-group" style="margin-top:8px;">
                     <label><?php echo L::ClasseMenu ?></label>
                     <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                     <select class="form-control input-height" id="classeEtab" name="classeEtab" style="width:100%;" onchange="searchmatiere1()">
                         <option value=""><?php echo L::Selectclasses ?></option>
                         <?php
                         $i=1;
                           foreach ($classes as $value):
                       $nbclassestudents=$classe->getAllStudentOfThisClassesNb($value->id_classe,$value->session_classe);
                       if($nbclassestudents>0)
                       {
                         ?>
<option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>
                         <?php
                       }
                           ?>


                           <?php
                                                            $i++;
                                                            endforeach;
                                                            ?>

                     </select>
                 </div>


             </div>
             <div class="col-md-6 col-sm-6">
             <!-- text input -->
             <div class="form-group" style="margin-top:8px;">
               <label><?php echo L::MatiereMenusingle ?></label>

                 <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                 <select class="form-control input-height" id="matclasse" name="matclasse" style="width:100%;" onchange='searchcontrole()' >

                   <option value=""><?php echo L::SelectSubjects ?></option>



                 </select>
             </div>


         </div>

         <div class="col-md-6 col-sm-6">
         <!-- text input -->
         <div class="form-group" style="margin-top:8px;">
             <label><?php echo L::Trimestre ?></label>
             <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->

             <select class="form-control input-height" id="typesess" name="typesess" style="width:100%" >
                 <option value=""><?php echo L::selectTrimestre ?></option>
                 <?php
                 $i=1;
                   foreach ($typesemestrealls as $value):
                   ?>
                   <option value="<?php echo $value->id_semes?>" ><?php echo utf8_encode(utf8_decode($value->libelle_semes)) ?></option>

                   <?php
                                                    $i++;
                                                    endforeach;
                                                    ?>

             </select>
         </div>


     </div>

         <div class="col-md-6 col-sm-6">
         <!-- text input -->
         <div class="form-group" style="margin-top:8px;">
             <label><?php echo L::Evaluation ?></label>
             <input type="text" id="libctrl" name="libctrl"  value="" style="width:100%;">
             <!-- input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..." -->

             <input type="hidden" name="search" id="search" value="1"/>
             <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $_SESSION['user']['codeEtab'] ?>"/>
             <input type="hidden" id="notetype" name="notetype" value="1">
             <input type="hidden" name="libellesession"  id="libellesession" value="<?php echo $libellesessionencours; ?>">
         </div>


         </div>


             <div class="col-md-3 col-sm-3">
             <!-- text input -->
             <!--div class="form-group">
                 <label style="margin-top:3px;">Date</label>
                 <input type="text" id="datepre" name ="datepre" class="floating-label mdl-textfield__input"  placeholder="Date présence">
                 <input type="hidden" name="search" id="search" />
             </div-->
             <button type="submit" class="btn btn-success"  style="width:200px;height:35px;margin-top:35px;text-align:center;"><?php echo L::ConsignerNotes ?></button>


         </div>


               </div>


           </form>
         </div>
     </div>
              </div>

</div>

<div class="row" style="" id="affichage">

  <?php
      if(isset($_POST['search'])&&($_POST['search']==1))
      {
          if(isset($_POST['notetype'])&&isset($_POST['classeEtab'])&&isset($_POST['libctrl']))
          {
              //nous devons recupérer la liste des elèves de cette classe

              // $students=$student->getAllstudentofthisclasses($_POST['classeEtab']);
              $students=$student->getAllstudentofthisclassesSession($_POST['classeEtab'],$libellesessionencours);

              $classeInfos=$classe->getInfosofclassesbyId($_POST['classeEtab'],$libellesessionencours);

              $controleInfos=$_POST['libctrl'];

          }
          ?>

          <div class="offset-md-4 col-md-4"  id="affichage1">
            <div class="card" style="">
            <div class="card-body">
              <h5 class="card-title"></h5>
              <h4 style="font-style: italic;font-weight: bold;text-align:center;font-size:font-size: large;"><?php echo L::NotesClassing ?></h4>
              <p class="card-text" style="text-align:center;font-weight: bold;"><?php echo $classeInfos; ?></p>
              <p class="card-text" style="text-align:center;">(<?php //echo $controleInfos?>)</p>

            </div>
          </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="affichage2">
                                  <div class="card card-topline-green">
                                      <div class="card-head">
                                          <header><?php echo L::NotesClassingbystudent ?></header>
                                          <div class="tools">
                                              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                      <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                      <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                          </div>
                                      </div>
                                      <div class="card-body ">

                                        <form method="post" action="../controller/notes.php" id="Form" >
                                          <!-- <input type="text" name="testname" id="testname" value=""> -->
                                  <table class="table table-striped table-bordered table-hover table-checkable order-column full-width"   id="affichage3">
                                      <thead>
                                          <tr>
                                              <!--th style="width:5%">
                                                  #
                                              </th-->
                                              <th style="width:30%;text-align:center"> <?php echo L::MatriculestudentTab ?> </th>
                                              <th style="width:45%;text-align:center"> <?php echo L::NamestudentTab ?> </th>
                                              <th style="width:45%;text-align:center"> <?php echo L::Notelibelle ?> </th>


                                          </tr>
                                      </thead>
                                      <tbody>
                                        <?php
                                        $matricules="";
                                        $j=0;
                                        $i=1;

                                        foreach ($students as $value):

                                          $matricules=$matricules.$value->idcompte_eleve."*";
                                        ?>
                                          <tr class="odd gradeX">
                                              <!--td>
                                                <?php //echo $i;?>
                                              </td-->
                                              <td> <?php echo $value->matricule_eleve;?></td>
                                              <td>
                                                  <a href="#"><?php echo $value->nom_eleve." ".$value->prenom_eleve;?> </a>
                                              </td>
                                              <td style="text-align:center">
                                                  <input type="number" min=0 max=100 name="note<?php echo $value->idcompte_eleve;?>" id="note<?php echo $value->idcompte_eleve;?>" style="width:100px;text-align:center" onclick="erasedNote(<?php echo $value->idcompte_eleve;?>)" />
                                                  <p id="messageNote<?php echo $value->idcompte_eleve;?>"></p>
                                              </td>
                                              <!-- <td>

                                                    <textarea name="obser<?php //echo $value->idcompte_eleve;?>" id="obser<?php //echo $value->idcompte_eleve;?>" rows="3" cols="50" onclick="erasedObserv(<?php //echo $value->idcompte_eleve;?>)"></textarea>
                                                    <p id="messageObserv<?php //echo $value->idcompte_eleve;?>"></p>
                                              </td> -->

                                          </tr>

                                          <?php
                                             $i++;
                                             $j++;
                                                 endforeach;
                                               ?>



                                      </tbody>
                                  </table>

                                  <?php
                                  //echo $matricules;
                                  $tabMat=explode("*",$matricules);
                                  $nb=count($tabMat);
                                  $matclasse=$_POST['matclasse'];

                                  $tabmatclasse=explode("-",$matclasse);

                                  ?>
                                  <input type="hidden" name="studentmat" id="studentmat" value="<?php echo $matricules;?>"/>
                                  <!-- <input type="hidden" name="etape" id="etape" value="8"/> -->
                                  <input type="hidden" name="etape" id="etape" value="9"/>

                                  <input type="hidden" name="nbstudent" id="nbstudent" value="<?php echo $j;?>"/>

                                  <input type="hidden" name="classeId" id="classeId" value="<?php echo $_POST['classeEtab']?>"/>
                                  <input type="hidden" name="typenote" id="typenote" value="<?php echo $_POST['notetype']?>"/>
                                  <input type="hidden" name="libellenote" id="libellenote" value="<?php echo $_POST['libctrl']?>"/>
                                  <input type="hidden" name="idtypenote" id="idtypenote" value="1"/>
                                  <input type="hidden" name="typesession" id="typesession" value="<?php echo $_POST['typesess'] ?>">
                                  <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $_POST['codeEtab'];?>"/>
                                  <input type="hidden" name="matiereid" id="matiereid" value=" <?php echo $tabmatclasse[0];?> ">
                                  <input type="hidden" name="teatcherid" id="teatcherid" value=" <?php echo  $tabmatclasse[1];?> ">
                                  <input type="hidden" name="sessionlibelle" id="sessionlibelle" value="<?php echo $_POST['libellesession']; ?>">
                                  <center><button type="submit"  onclick="check()" class="btn btn-success"><i class="fa fa-check-circle"></i><?php echo L::Gradevalider ?></button></center>
                                </form>

                              </div>
                                  </div>
                              </div>


          <?php
      }
   ?>

</div>

<!-- fin affichage du bouton rechercher -->

                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <!-- <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script> -->
   <!-- <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script> -->
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>

   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <!-- <script src="../assets2/js/pages/validation/form-validation.js" ></script> -->
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script src="../assets/js/formatter/jquery.formatter.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   // function SetcodeEtab(codeEtab)
   // {
   //   var etape=3;
   //   $.ajax({
   //     url: '../ajax/sessions.php',
   //     type: 'POST',
   //     async:false,
   //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
   //     dataType: 'text',
   //     success: function (content, statut) {
   //
   // window.location.reload();
   //
   //     }
   //   });
   // }

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }





   $('#example5').DataTable( {
       "scrollX": true

   } );
   $("#classeEtab").select2();
   $("#matclasse").select2();
   $("#typesess").select2();
   // $("#libctrl").select2();
   $("#classeEtab1").select2();
   $("#matiere1").select2();
   // $("#libctrl1").select2();



function searchNotesClasses()
{
  var notetype=$("#notetype").val();
  var classeEtab=$("#classeEtab").val();
  var codeEtab="<?php echo $_SESSION['user']['codeEtab'];  ?>";

  //nous allons rechercher la liste des designation de notes par classe

    if(notetype==""||classeEtab=="")
    {
      if(notetype=="")
      {
        Swal.fire({
        type: 'warning',
        title: '<?php echo L::WarningLib ?>',
        text: "<?php echo L::PleaseSelectnotetyperequired ?>",

      })
      }

      if(classeEtab=="")
      {
        Swal.fire({
        type: 'warning',
        title: '<?php echo L::WarningLib ?>',
        text: "<?php echo L::PleaseSelectclasseOnerequired ?>",

      })
      }
    }else {

        if(notetype==1)
        {
          var etape=1;
        }else if(notetype==2){
          var etape=2;
        }

      $.ajax({

        url: '../ajax/Search.php',
        type: 'POST',
        async:true,
         data: 'notetype=' + notetype+ '&etape=' + etape+'&classeEtab='+classeEtab+'&codeEtab='+codeEtab,
         dataType: 'text',
         success: function (content, statut) {

           // $("#libctrl").html("");
           // $("#libctrl").html(content);

         }

      });
    }



}

function searchmatiere1()
{
  var classeEtab=$("#classeEtab").val();
  var codeEtab="<?php echo $_SESSION['user']['codeEtab'];  ?>";
  var etape=5;

  $.ajax({

    url: '../ajax/matiere.php',
    type: 'POST',
    async:true,
     data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab,
     dataType: 'text',
     success: function (content, statut) {

       $("#matclasse").html("");
       $("#matclasse").html(content);

     }

  });

}

function searchmatiere2()
{
  var classeEtab=$("#classeEtab1").val();
  var codeEtab="<?php echo $_SESSION['user']['codeEtab'];  ?>";
  var etape=5;

  $.ajax({

    url: '../ajax/matiere.php',
    type: 'POST',
    async:true,
     data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab,
     dataType: 'text',
     success: function (content, statut) {

       $("#matiere1").html("");
       $("#matiere1").html(content);

     }

  });
}

function searchcontrole()
{
  var classeEtab=$("#classeEtab").val();
  var codeEtab="<?php echo $_SESSION['user']['codeEtab'];  ?>";
  var matiere=$("#matclasse").val();

  //nous allons chercher la liste des controles de cette classe dont le statut est à 0
  var etape=2;

  $.ajax({

    url: '../ajax/controle.php',
    type: 'POST',
    async:true,
     data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab+'&matiere='+matiere,
     dataType: 'text',
     success: function (content, statut) {

       $("#libctrl").html("");
       $("#libctrl").html(content);

     }

  });

}

function erasedNote(id)
{
document.getElementById("messageNote"+id).innerHTML = "";
}

function erasedObserv(id)
{
document.getElementById("messageObserv"+id).innerHTML = "";
}

function erasedNoteE(id)
{
document.getElementById("messageNoteE"+id).innerHTML = "";
}

function erasedObservE(id)
{
document.getElementById("messageObservE"+id).innerHTML = "";
}

function autorise()
{
    $("#Form").submit();
}

function check1()
{
  var tabidstudent=$("#studentmat").val();
  var nbligne=$("#nbstudent").val();
  var tab=$("#studentmat").val().split("*");
  var i;

  // var note=$("#note"+tab[i]).val()

  for(i=0;i<nbligne;i++)
  {
      var note=$("#note"+tab[i]).val();
      // var obser=$("#obserE"+tab[i]).val();
  event.preventDefault();



        if(note=="")
        {
          document.getElementById("messageNoteE"+tab[i]).innerHTML = "<font color=\"red\"><?php echo L::PleaseAddNotes ?> </font>";

        }else {

        }





  }
}

$("#btnsubmit").click(function(){

  var studentmat=$("#studentmat").val();
  var tab=studentmat.split("*");

  var nbtab=tab.length-1;

  // alert(studentmat+" "+nbtab);

for(var i=0;i<nbtab;i++)
{
  var code=tab[i];
  // $('#Form #note'+code).formatter({pattern:"{{999}}"});

  $('#Form #note'+code).rules( "add", {
           // required: true,
           required: {
          depends: function (element) {
              return ($('#Form #note'+code).val()=="");

          }
      },
       max:100,
       min:0,
          messages: {
          required: "<?php echo L::PleaseAddNotes ?>",
          max:"<?php echo L::NoteMaxexeceded ?>",
          min:"<?php echo L::NoteMinUnder ?>"
   }
       });


}


});


   $(document).ready(function() {

     $( "#Form input[name*='note']" ).formatter({pattern:"{{999}}"});


     $("#FormAddNotes").validate({
       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
      },
      success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },
       rules:{


         classeEtab:"required",
         datepre:"required",
         notetype:"required",
         classeEtab:"required",
         libctrl:"required",
         matclasse:"required",
         typesess:"required"



       },
       messages: {
         classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
         datepre:"<?php echo L::PleaseSelectdateprerequired ?>",
         notetype:"<?php echo L::PleaseSelectnotetyperequired ?>",
         libctrl:"<?php echo L::PleaseEnterLibellenote ?>",
         matclasse:"<?php echo L::SubjectSelectedrequired ?>",

        typesess:"<?php echo L::pleaseselecttrimestre ?>"

       },
       submitHandler: function(form) {
         form.submit();
       }
     });


$("#Form").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
  },
  highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
  },
  success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{
    // testname:"required",
  },
  messages: {
// testname:"Merci de renseigner le champ"

  },
  submitHandler: function(form) {

    form.submit();


  }


});


   });

   </script>
    <!-- end js include path -->
  </body>

</html>
