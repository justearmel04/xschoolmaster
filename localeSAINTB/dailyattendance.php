<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$student=new Student();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

if(strlen($_SESSION['user']['codeEtab'])>0)
{
  $codeEtabAssigner=$_SESSION['user']['codeEtab'];
}else {
  $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
}
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//nous allons chercher la liste des etablissements du groupe

$allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
$agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$libellesessionencours="";

  if($etablissementType==1||$etablissementType==3)
  {
    if($nbsessionOn>0){
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];

    }

  }else {
    if($nbsessionOn>0)
    {
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];
      $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
      $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
    }
  }

  if(strlen($_SESSION['user']['codeEtab'])>0)
  {

  }else {
    $_SESSION['user']['codeEtab']=$codeEtabAssigner;
  }

  $_SESSION['user']['session']=$libellesessionencours;


  $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

  $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);



  // var_dump($allcodeEtabs);
  $nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
// echo $libellesessionencours;
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
     <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>


	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::Consignerlesabsences ?> :</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i><?php echo L::Homestartindex ?>&nbsp;<i class="fa fa-angle-right"></i><a class="parent-item" href="#"><?php echo L::AbsMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::Consignerlesabsences ?></li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">
              <?php

                    if(isset($_SESSION['user']['addattendailyok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
            <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: '<?php echo L::Felicitations ?>',
                text: '<?php echo $_SESSION['user']['addattendailyok']; ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addattendailyok']);
                    }

                     ?>
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

<br/>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="card card-topline-green">
                   <div class="card-head">
                       <header></header>
                       <div class="tools">
                           <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                       </div>
                   </div>
                   <div class="card-body ">
                     <form method="post" id="FormSearch" action="dailyattendance.php">
                         <div class="row">
                           <div class="col-md-6 col-sm-6">
                           <!-- text input -->
                           <div class="form-group">
                               <label style="margin-top:3px;"><?php echo L::LittleDateVersements ?></label>
                               <input type="text" id="datepre" name ="datepre" class="floating-label mdl-textfield__input"  placeholder="<?php echo L::Datepre ?>">
                               <input type="hidden" name="search" id="search" />
                           </div>


                       </div>

                           <div class="col-md-6 col-sm-6">
                           <!-- text input -->
                           <div class="form-group" style="margin-top:8px;">
                               <label><?php echo L::ClasseMenu ?></label>
                               <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                               <select class="form-control " id="classeEtab" name="classeEtab" style="width:100%;" onchange="searchingMatiere('<?php echo $_SESSION['user']['codeEtab']; ?>')">
                                   <option value=""><?php echo L::Selectclasses ?></option>
                                   <?php
                                   $i=1;
                                     foreach ($classes as $value):
                                          $nbclassestudents=$classe->getAllStudentOfThisClassesNb($value->id_classe,$value->session_classe);
                                          if($nbclassestudents>0)
                                          {
                                              ?>
 <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>
                                              <?php
                                          }
                                     ?>


                                     <?php
                                                                      $i++;
                                                                      endforeach;
                                                                      ?>

                               </select>
                           </div>


                       </div>
                       <!-- <div class="col-md-4 col-sm-4">

                       <div class="form-group" style="margin-top:8px;">
                           <label><?php echo L::MatiereMenusingle ?></label>

                           <select class="form-control " id="matiereEtab" name="matiereEtab" style="width:100%;" onchange="selectedHours()">
                               <option value=""><?php echo L::SelectSubjects ?></option>
                            </select>
                       </div>


                   </div> -->
                   <!-- <div class="col-md-4 col-sm-4">

                   <div class="form-group" style="margin-top:8px;">
                       <label><?php echo L::CoursesHoursLib ?></label>

                       <select class="form-control " id="HeureLibEtab" name="HeureLibEtab" style="width:100%;" onchange="Addhiddencaps()">
                           <option value=""><?php echo L::SelectCoursesHoursLib ?></option>
                        </select>
                        <input type="hidden" name="datedebhours" id="datedebhours" value="">
                        <input type="hidden" name="datefinhours" id="datefinhours" value="">
                   </div>


               </div> -->

                   <?php
                   if($nbsessionOn>0)
                   {
                     ?>
                     <button type="submit" class="btn btn-success" onclick="affichage()" style="width:150px;height:35px;margin-top:30px;"><?php echo L::ConsignerAbsence ?></button>
                     <?php
                   }else if($nbsessionOn==0)
                   {
                     ?>
<button type="submit" class="btn btn-success" onclick="affichage()" style="width:150px;height:35px;margin-top:30px;" disabled ><?php echo L::ConsignerAbsence ?></button>
                     <?php
                   }
                    ?>


                         </div>


                     </form>
                   </div>
               </div>
                        </div>

          </div>
<!-- element à faire apparaitre au clique du bouton rechercher -->
<div class="row" style="" id="affichage">
  <?php
      if(isset($_POST['search']))
      {
          if(isset($_POST['classeEtab'])&&isset($_POST['datepre']))
          {
              //nous devons recupérer la liste des elèves de cette classe

              //$students=$student->getAllstudentofthisclasses($_POST['classeEtab']);

              $students=$student->getAllstudentofthisclassesSession($_POST['classeEtab'],$libellesessionencours);

              $classeInfos=$classe->getInfosofclassesbyId($_POST['classeEtab'],$libellesessionencours);

              ///var_dump($students);
              // $matierepost=explode('-',$_POST['matiereEtab']);

          }
          ?>

          <div class="offset-md-4 col-md-4"  id="affichage1">
            <div class="card" style="">
            <div class="card-body">
              <h5 class="card-title"></h5>
              <h4 style="font-style: italic;font-weight: bold;text-align:center;font-size:font-size: large;"><?php echo L::DailyPresencesClasse ?></h4>
              <p class="card-text" style="text-align:center;font-weight: bold;"><?php echo $classeInfos; ?></p>
              <p class="card-text" style="text-align:center;"><?php echo $_POST['datepre']?></p>

            </div>
          </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="affichage2">
                                  <div class="card card-topline-green">
                                      <div class="card-head">
                                          <header><?php echo L::StatusPresences ?></header>
                                          <div class="tools">
                                              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                      <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                      <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                          </div>
                                      </div>
                                      <div class="card-body ">
                                        <div class="pull-right"><a class="btn btn-primary " onclick="touspresent(<?php echo $_POST['classeEtab'];?>)" ><?php echo L::MarqueAllpresence ?></a> <a class="btn btn-primary " onclick="tousabsent(<?php echo $_POST['classeEtab'];?>)"><?php echo L::MarqueAllAbsences ?></a></div>
                                        <form method="post" action="../controller/attendance.php" id="FormAttendance">
                                          <br>
                                  <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example3"  >
                                      <thead>
                                          <tr>
                                              <!-- <th style="width:5%">
                                                  #
                                              </th> -->
                                              <!-- <th style="width:15%"> <?php //echo L::MatriculestudentTab ?> </th> -->
                                              <th style="width:35%"><?php echo L::NamestudentTab ?> </th>
                                              <th> <?php echo L::Etat ?> </th>
                                              <th> <?php echo L::RetardLibelle ?> </th>
                                              <!-- <th> <?php //echo L::RetardLibelle ?> </th> -->

                                          </tr>
                                      </thead>
                                      <tbody>
                                        <?php
                                        $matricules="";
                                        $j=0;
                                        $i=1;

                                        foreach ($students as $value):

                                          $matricules=$matricules."*".$value->matricule_eleve;
                                        ?>
                                          <tr class="odd gradeX">
                                              <!-- <td>
                                                <?php echo $i;?>
                                              </td> -->
                                              <!-- <td> <?php //echo $value->matricule_eleve;?></td> -->
                                              <td class="left">
                                                <span class="label label-primary"><?php echo $value->matricule_eleve." - ".$value->nom_eleve." ".$value->prenom_eleve;?></span>
                                                  <!-- <a href="#"> </a> -->
                                              </td>
                                              <td>
                                                <label class="radio-inline">
                                          <input type="radio" value="1" name="optradio<?php echo $value->matricule_eleve;?>" id="P<?php echo $value->matricule_eleve;?>" onclick="present('<?php echo $value->matricule_eleve;?>')" checked> <?php echo L::Presenter ?>
                                        </label>&nbsp;
                                        <label class="radio-inline">
                                          <input type="radio" value="0" onclick="absent('<?php echo $value->matricule_eleve;?>')" id="A<?php echo $value->matricule_eleve;?>" name="optradio<?php echo $value->matricule_eleve;?>"> <?php echo L::Absent ?>
                                        </label>&nbsp;
                                        <label class="radio-inline">
                                          <input type="radio" value="2" onclick="Retard('<?php echo $value->matricule_eleve;?>')" id="R<?php echo $value->matricule_eleve;?>" name="optradio<?php echo $value->matricule_eleve;?>"> <?php echo L::RetardLibelle ?>
                                        </label>&nbsp;
                                        <input type="hidden" name="statut<?php echo $value->matricule_eleve;?>" id="statut<?php echo $value->matricule_eleve;?>" value="1"/>

                                              </td>
                                              <td>
                                                <input type="text" class="heure" name="heure<?php echo $value->matricule_eleve;?>" id="heure<?php echo $value->matricule_eleve;?>" value="" disabled>
                                                <input type="hidden" name="observation<?php echo $value->matricule_eleve;?>" id="observation<?php echo $value->matricule_eleve;?>"value="">

                                                <input type="hidden" name="statutcomment<?php echo $value->matricule_eleve;?>" id="statutcomment<?php echo $value->matricule_eleve;?>" value="0">
                                                <a href="#" title="<?php echo L::Addobservations ?>" style="display:none" id="btnadd<?php echo $value->matricule_eleve;?>" class="btn btn-success btn-xs" onclick="commentaires('<?php echo $value->matricule_eleve;?>')" data-toggle="modal" data-target="#largeModel1" ><i class="fa fa-plus"></i> </a>
                                                <a href="#" style="display:none" title="<?php echo L::Detailsingle ?>" id="btndetails<?php echo $value->matricule_eleve;?>" class="btn btn-warning btn-xs" onclick="detailscommentaires('<?php echo $value->matricule_eleve;?>')" data-toggle="modal" data-target="#largeModel2" ><i class="fa fa-info-circle"></i> </a>
                                                <a href="#" style="display:none" title="<?php echo L::ModifierBtn ?>" id="btnmod<?php echo $value->matricule_eleve;?>" class="btn btn-primary btn-xs" onclick="modifycommenatires('<?php echo $value->matricule_eleve;?>')" data-toggle="modal" data-target="#largeModel3"><i class="fa fa-pencil"></i> </a>
                                              </td>
                                              <!-- <td></td> -->

                                          </tr>

                                          <?php
                                             $i++;
                                             $j++;
                                                 endforeach;
                                               ?>



                                      </tbody>
                                  </table>

                                  <?php
                                  //echo $matricules;
                                  $tabMat=explode("*",$matricules);
                                  $nb=count($tabMat);



                                  ?>
                                  <!-- <input type="hidden" name="matiereid" id="matiereid" value="<?php// echo $matierepost[0]; ?>"> -->
                                  <!-- <input type="hidden" name="profid" id="profid" value="<?php //echo $matierepost[1]; ?>"> -->
                                  <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $_SESSION['user']['codeEtab']; ?>">
                                  <input type="hidden" name="studentmat" id="studentmat" value="<?php echo $matricules;?>"/>
                                  <input type="hidden" name="etape" id="etape" value="3"/>
                                  <input type="hidden" name="nbstudent" id="nbstudent" value="<?php echo $nb;?>"/>
                                  <input type="hidden" name="allpresent" id="allpresent" value=""/>
                                  <input type="hidden" name="classeId" id="classeId" value="<?php echo $_POST['classeEtab']?>"/>
                                  <input type="hidden" name="datePresence" id="datePresence" value="<?php echo $_POST['datepre']?>"/>
                                  <!-- <input type="hidden" name="LibelleHeurePresence" id="LibelleHeurePresence" value="<?php //echo $_POST['HeureLibEtab']?>"/> -->
                                  <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                  <center><button type="submit"  class="btn btn-success"><i class="fa fa-check-circle"></i><?php echo L::ValidationStauts ?></button></center>
                                </form>

                              </div>
                                  </div>
                              </div>


          <?php
      }
   ?>



          </div>

<!-- fin affichage du bouton rechercher -->

                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>

            <div class="modal fade" id="largeModel1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel"><?php echo L::Addobservations ?></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                      <div id="divrecherchestation">

              <form class="" action="#" id="commentairesForm">
                <div class="form-group">
                  <input type="hidden" name="commentaireStudentMatri" id="commentaireStudentMatri" value="">
                <textarea class="form-control descri" name="comment" id="comment" rows="5" placeholder="" ></textarea>

                </div>
                <div class="form-group">
                  <button type="submit" name="button" class="btn btn-success btn-md"><i class="fa fa-plus"></i> <?php echo L::AddMenu ?> </button>
                </div>

<!-- <input type="hidden" name="stationselect" id="stationselect" value="">
<input type="hidden" name="famillycode" id="famillycode" value=""> -->

              </form>

                      </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closebtn"><?php echo L::Closebtn  ?></button>
                        <button type="button" style="display:none" class="btn btn-primary" onclick="validerMember()" id="btnsaving"><?php echo L::Saving ?></button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="largeModel2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel"><?php echo L::Detailsobservations ?></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  <div id="divrecherchestation">

          <form class="" action="#" id="commentairesFormdetails">
            <div class="form-group">
              <input type="hidden" name="commentaireStudentMatri" id="commentaireStudentMatri" value="">
            <textarea class="form-control descri" name="comment" id="comment" rows="5" placeholder="" ></textarea>

            </div>


<!-- <input type="hidden" name="stationselect" id="stationselect" value="">
<input type="hidden" name="famillycode" id="famillycode" value=""> -->

          </form>

                  </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closebtn"><?php echo L::Closebtn  ?></button>
                    <button type="button" style="display:none" class="btn btn-primary" onclick="validerMember()" id="btnsaving"><?php echo L::Saving ?></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="largeModel3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?php echo L::Modobservations ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div id="divrecherchestation">

      <form class="" action="#" id="commentairesFormMod">
        <div class="form-group">
          <input type="hidden" name="commentaireStudentMatri" id="commentaireStudentMatri" value="">
        <textarea class="form-control descri" name="comment" id="comment" rows="5" placeholder="" ></textarea>

        </div>
        <div class="form-group">
          <button type="submit" name="button" class="btn btn-success btn-md"><i class="fa fa-pencil"></i> <?php echo L::ModifierBtn ?> </button>
        </div>

<!-- <input type="hidden" name="stationselect" id="stationselect" value="">
<input type="hidden" name="famillycode" id="famillycode" value=""> -->

      </form>

              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closebtn3"><?php echo L::Closebtn  ?></button>
                <button type="button" style="display:none" class="btn btn-primary" onclick="validerMember()" id="btnsaving"><?php echo L::Saving ?></button>
            </div>
        </div>
    </div>
</div>

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
      <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
<script src="../assets/js/formatter/jquery.formatter.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function modifycommenatires(matri)
   {
     // alert(matri);
     $("#commentairesFormMod #commentaireStudentMatri").val(matri);
     // var comment=$("#commentairesFormMod #comment").val();
     var observation=$("#observation"+matri).val();
     $("#commentairesFormMod #comment").val(observation);
     // alert(comment);
     // $("#commentairesFormMod #comment").val(comment);
     // $("#observation"+matri).val(comment);
   }

   function detailscommentaires(matri)
   {
     $("#commentaireStudentMatri").val(matri);
     var comment=$("#observation"+matri).val();

     $("#commentairesFormdetails #comment").val(comment);
   }

   function commentaires(matri)
   {
     $("#commentaireStudentMatri").val(matri);
   }
   // function SetcodeEtab(codeEtab)
   // {
   //   var etape=3;
   //   $.ajax({
   //     url: '../ajax/sessions.php',
   //     type: 'POST',
   //     async:false,
   //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
   //     dataType: 'text',
   //     success: function (content, statut) {
   //
   // window.location.reload();
   //
   //     }
   //   });
   // }
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

<?php
  if(isset($_POST))
  {
    ?>
    function touspresent(idclasse)
    {

       var nb="<?php echo @$nb;?>";
       var matricules="<?php echo @$matricules?>";

       var array=matricules.split('*');

       for(i=1;i<nb;i++)
       {
         //alert(array[i]);
         $("statut"+array[i]).val(1);
         //$("P"+array[i]).prop("checked", true);
         document.getElementById("P"+array[i]).checked = true;

       }
       $("#allpresent").val(1);
       $("#heure"+id).attr("disabled",true);

    }

    function tousabsent(idclasse)
    {
      var nb="<?php echo @$nb;?>";
      var matricules="<?php echo @$matricules?>";

      var array=matricules.split('*');

      for(i=1;i<nb;i++)
      {
        $("statut"+array[i]).val(0);
        document.getElementById("A"+array[i]).checked = true;

      }
      $("#allpresent").val(0);
      $("#heure"+id).attr("disabled",true);
    }
<?php
  }
 ?>

 function selectedHours()
 {
   var classe=$("#classeEtab").val();
   var codeEtab="<?php echo $_SESSION['user']['codeEtab']; ?>";
   var session="<?php echo $libellesessionencours; ?>";
   var matiere=$("#matiereEtab").val();
   var datechoice=$("#datepre").val();
   var etape=10;

   if(datechoice=="")
   {
     Swal.fire({
     type: 'warning',
     title: '<?php echo L::WarningLib ?>',
     text: "<?php echo L::PresenceAllreadyExiste ?>",

   })
   }else {
     $.ajax({
       url: '../ajax/hours.php',
       type: 'POST',
       async:false,
       data: 'classe=' +classe+'&code='+codeEtab+'&etape='+etape+'&session='+session+'&matiere='+matiere+'&datechoice='+datechoice,
       dataType: 'text',
       success: function (content, statut)
       {
         $("#HeureLibEtab").html("");
          $("#HeureLibEtab").html(content);

       }
     });
   }



 }

 function Addhiddencaps()
 {
   var libelleheure=$("#HeureLibEtab").val();
   var codeEtab="<?php echo $_SESSION['user']['codeEtab']; ?>";
   var session="<?php echo $libellesessionencours; ?>";
   var datechoice=$("#datepre").val();
   var nouveau="";
   var etape=7;

   if(datechoice=="")
   {
     Swal.fire({
     type: 'warning',
     title: '<?php echo L::WarningLib ?>',
     text: "<?php echo L::PresenceAllreadyExiste ?>",

   })
   }else {
     $.ajax({
       url: '../ajax/hours.php',
       type: 'POST',
       async:true,
       data: 'libelleheure='+libelleheure+'&etape='+etape+'&nouveau='+nouveau+'&codeEtab='+codeEtab+'&session='+session+'&datechoice='+datechoice,
       dataType: 'text',
       success: function (content, statut) {
         var heuredeb=content.split("*")[0];
         var heurefin=content.split("*")[1];

         $("#datedebhours").val(heuredeb);
         $("#datefinhours").val(heurefin);

       }
     });
   }



 }

 function searchingMatiere(codeEtab)
 {
   var classe=$("#classeEtab").val();
   var datechoice=$("#datepre").val();
   var etape=14;

   if(datechoice=="")
   {
     Swal.fire({
     type: 'warning',
     title: '<?php echo L::WarningLib ?>',
     text: "<?php echo L::PleaseEnterDatePre ?>",

   })
   }else {
     $.ajax({
       url: '../ajax/matiere.php',
       type: 'POST',
       async:false,
       data: 'classe=' +classe+'&code='+codeEtab+'&etape='+etape+'&datechoice='+datechoice,
       dataType: 'text',
       success: function (content, statut)
       {
         $("#matiereEtab").html("");
          $("#matiereEtab").html(content);

       }
     });
   }



 }


   function present(id)
   {
    $("statut"+id).val(1);
$("#heure"+id).attr("disabled",true);
$("#btnadd"+id).hide();
$("#btndetails"+id).hide();
$("#btnmod"+id).hide();
$("#observation"+id).val("");
$("#statutcomment"+id).val(0);
$('#heure'+id).rules( "remove" );
   }

   function absent(id)
   {
    $("statut"+id).val(0);
    $("#heure"+id).attr("disabled",true);
    $("#btnadd"+id).hide();
    $("#btndetails"+id).hide();
    $("#btnmod"+id).hide();
    $("#observation"+id).val("");
    $("#statutcomment"+id).val(0);
    $('#heure'+id).rules( "remove" );
   }

   function Retard(id)
   {
       $("statut"+id).val(2);
       $("#heure"+id).attr("disabled",false);
       $("#btnadd"+id).show();
       $('#heure'+id).rules( "add", {
           required: true,
           messages: {
           required: "<?php echo L::RequiredChamp ?>"
   }
         });

   }

   $('#example5').DataTable( {
       "scrollX": true

   } );
   $("#classeEtab").select2();
  $("#matiereEtab").select2();
  $("#HeureLibEtab").select2();
   $('#datepre').bootstrapMaterialDatePicker
   ({
     date: true,
     time: false,
     format: 'DD/MM/YYYY',
     lang: 'fr',
     // minDate : new Date(),
    cancelText: '<?php echo L::AnnulerBtn ?>',
    okText: '<?php echo L::Okay ?>',
    clearText: '<?php echo L::Eraser ?>',
    nowText: '<?php echo L::Now ?>'

   });
   $(document).ready(function() {

     $(".heure").formatter({pattern:"{{99}}:{{99}}:{{99}}"});

//

$("#commentairesFormMod").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
 },
 highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
 },
 success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    comment:"required"




  },
  messages: {
    comment:"<?php echo L::PleaseEnterobservations ?>",


  },
  submitHandler: function(form) {

    var comment=$("#commentairesFormMod #comment").val();
    var matri=$("#commentairesFormMod #commentaireStudentMatri").val();
    // alert(comment+" ggggg"+matri);
      $("#observation"+matri).val(comment);

    $("#FormAttendance #statutcomment"+matri).val(1);
    $("#btnadd"+matri).hide();
    $("#btndetails"+matri).show();
    $("#btnmod"+matri).show();

    $("#closebtn3").click();

    // alert("bonjour");

  }
});


$("#commentairesForm").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
 },
 highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
 },
 success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    comment:"required"




  },
  messages: {
    comment:"<?php echo L::PleaseEnterobservations ?>",


  },
  submitHandler: function(form) {

    var comment=$("#commentairesForm #comment").val();
    var matri=$("#commentairesForm #commentaireStudentMatri").val();
    $("#observation"+matri).val(comment);
    $("#statutcomment"+matri).val(1);
    $("#btnadd"+matri).hide();
    $("#btndetails"+matri).show();
    $("#btnmod"+matri).show();
    $("#comment").val("");
    $("#closebtn").click();

  }
});

$("#FormSearch").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
 },
 highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
 },
 success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    classeEtab:"required",
    datepre:"required",
    matiereEtab:"required",
    HeureLibEtab:"required"



  },
  messages: {
    classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
    datepre:"<?php echo L::PleaseSelectdateprerequired ?>",
    matiereEtab:"<?php echo L::SubjectSelectedrequired ?>",
    HeureLibEtab:"<?php echo L::PleaseEntersLibelleHours ?>"

  },
  submitHandler: function(form) {
    form.submit();
    //nous allons verifier si nous n'avons pas deja fait les présences en ce jour et pour cette heure de cours

    // var datepre=$("#datepre").val();
    // var matiereEtab=$("#matiereEtab").val();
    // var HeureLibEtab=$("#HeureLibEtab").val();
    // var classeEtab=$("#classeEtab").val();
    // var codeEtab="<?php echo $_SESSION['user']['codeEtab']; ?>";
    // var session="<?php echo $libellesessionencours; ?>";
    // var etape=11;
    //
    // $.ajax({
    //   url: '../ajax/hours.php',
    //   type: 'POST',
    //   async:false,
    //   data: 'classe=' +classeEtab+'&codeEtab='+codeEtab+'&etape='+etape+'&matiereEtab='+matiereEtab+'&HeureLibEtab='+HeureLibEtab+'&datepre='+datepre+'&session='+session,
    //   dataType: 'text',
    //   success: function (content, statut)
    //   {
    //
    //     if(content==0)
    //     {
    //       form.submit();
    //     }else {
    //       Swal.fire({
    //       type: 'warning',
    //       title: '<?php echo L::WarningLib ?>',
    //       text: "<?php echo L::DatePreExistforLibelleHours ?>",
    //
    //     })
    //     }
    //
    //     // $("#matiereEtab").html("");
    //     //  $("#matiereEtab").html(content);
    //
    //   }
    // });
  }
});


$("#FormAttendance").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
  },
  highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
  },
  success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    classeEtab:"required",
    datepre:"required"



  },
  messages: {
    classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
    datepre:"<?php echo L::PleaseSelectdateprerequired ?>"

  },
  submitHandler: function(form) {
    //form.submit();
    //classeId
//datePresence
    var etape=1;

form.submit();

    // $.ajax({
    //
    //   url: '../ajax/attendance.php',
    //   type: 'POST',
    //   async:false,
    //   data: 'classe=' + $("#classeId").val()+'&datepre='+$("#datePresence").val()+'&etape='+etape,
    //   dataType: 'text',
    //   success: function (content, statut)
    //   {
    //       if(content==0)
    //       {
    //         form.submit();
    //
    //       }else if(content==1) {
    //         Swal.fire({
    //         type: 'warning',
    //         title: '<?php// echo L::WarningLib ?>',
    //         text: "La présence de cette classe existe dejà dans le système pour cette date",
    //
    //       })
    //     }
    //   }
    //
    // });
  }


});


   });

   </script>
    <!-- end js include path -->
  </body>

</html>
