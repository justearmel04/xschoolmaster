<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$parent=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

if(strlen($_SESSION['user']['codeEtab'])>0)
{
  $codeEtabAssigner=$_SESSION['user']['codeEtab'];
}else {
  $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
}
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//nous allons chercher la liste des etablissements du groupe

$allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
$agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$libellesessionencours="";

  if($etablissementType==1||$etablissementType==3)
  {
    if($nbsessionOn>0){
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];

    }

  }else {
    if($nbsessionOn>0)
    {
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];
      $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
      $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
    }
  }

  if(strlen($_SESSION['user']['codeEtab'])>0)
  {

  }else {
    $_SESSION['user']['codeEtab']=$codeEtabAssigner;
  }

  $_SESSION['user']['session']=$libellesessionencours;

  $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

  $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


  $parents=$parent->getAllparentInfobyIdNew($_GET['compte']);
  foreach ($parents as $value):
    $nomLocal=$value->nom_compte;
    $prenomLocal=$value->prenom_compte;
    $datenaisLocal=$value->datenais_compte;
    $telephoneLocal=$value->tel_compte;
    $emailLocal=$value->email_compte;
    $fonctionLocal=$value->fonction_compte;
    $loginLocal=$value->login_compte;
    $photoLocal=$value->photo_compte;
    $sexeLocal=$value->sexe_compte;
    $nomcompletLocal=$value->nom_compte." ".$prenomLocal;
    $nationaliteLocal=$value->nationalite_parent;
    $LieuHLocal=$value->lieuH_parent;
    $situationLocal=$value->situation_parent;
    $cniLocal=$value->cni_parent;
    $societeLocal=$value->societe_parent;
    $adresseproLocal=$value->adressepro_parent;
    $BurophoneLocal=$value->phoneBuro_parent;
    $lieunaisLocal=$value->lieunais_compte;
    $nbchieldLocal=$value->nbchild_parent;
    $nbchieldscoLocal=$value->nbchidsco_parent;



  endforeach;

  $parentlyStudent=$parent->getDifferentStudentByParentId($_GET['compte']);
  $nbnotificationstandby=$parent->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parent->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
  // var_dump($allcodeEtabs);
  $onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
  $offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
// echo $libellesessionencours;

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

  <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title"><?php echo L::DetailsParent ?></div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="parents.php"><?php echo L::ParentsMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active"><?php echo L::DetailsParent ?></li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addparok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
//                 Swal.fire({
//   title: '<?php //echo L::Felicitations ?>',
//   text: "<?php //echo $_SESSION['user']['addparok']; ?>",
//   type: 'success',
//   showCancelButton: true,
//   confirmButtonColor: '#3085d6',
//   cancelButtonColor: '#d33',
//   confirmButtonText: '<?php// echo L::AddNews ?>',
//   cancelButtonText: '<?php //echo L::AnnulerBtn ?>',
// }).then((result) => {
//   if (result.value) {
//
//   }else {
//     document.location.href="index.php";
//   }
// })

Swal.fire({
type: 'success',
title: '<?php echo L::Felicitations ?>',
text: '<?php echo $_SESSION['user']['addparok'] ?>',

})

                </script>
                      <?php
                      unset($_SESSION['user']['addparok']);
                    }

                     ?>

                     <div class="row">

                       <div class="col-lg-4 col-md-12 col-sm-12 col-12">
              <div class="white-box border-gray">
                <div class="user-bg">
                  <div class="overlay-box" style="background-color:#007bff;">
                    <div class="user-content">
                      <?php

                          if(strlen($photoLocal)>0 && strlen($emailLocal)>0)

                          {

                            $lien="../photo/".$emailLocal."/".$photoLocal;

                          }else {

                            $lien="../photo/user5.jpg";

                          }

                       ?>
                      <a href="javascript:void(0)"><img alt="img" class="thumb-lg img-circle"
                          src="<?php echo $lien; ?>"></a>
                      <h4 class="text-white"><?php echo $nomcompletLocal; ?></h4>
                      <h5 class="text-white"> <i class="fa fa-envelope-o"></i> <?php echo $emailLocal; ?></h5>
                    </div>
                  </div>
                </div>
                <div class="user-btm-box">
                  <?php
                  if(strlen($telephoneLocal)>0)
                  {
                    ?>
                    <div class="col-md-12  m-b-0 text-center">
                      <div class="stat-item">
                        <h6> <i class="fa fa-phone"></i> <?php echo L::ContactsParentTab ?> </h6> <b><i class="ti-mobile"></i> <?php echo $telephoneLocal; ?></b>
                      </div>
                    </div>
                    <?php
                  }
                   ?>

                </div>
              </div>
            </div>

                       <div class="col-md-8 col-sm-12">

                           <div class="profile-content">

                             <div class="row">
                               <div class="profile-tab-box">
                                 <div class="p-l-20">
                                   <ul class="nav ">
                                     <li class="nav-item tab-all"><a
                                       class="nav-link active show" href="#tab1" data-toggle="tab"><?php echo L::GeneralInfoTab ?></a></li>
                                      <li class="nav-item tab-all p-l-20"><a class="nav-link"
                                       href="#tab2" data-toggle="tab"><?php echo L::Paiements ?></a></li>
                                     <li class="nav-item tab-all p-l-20"><a class="nav-link"
                                       href="#tab3" data-toggle="tab"><?php echo L::Enfants ?></a></li>

                                         <li class="nav-item tab-all p-l-20"><a class="nav-link"
                                           href="#tab5" data-toggle="tab"><?php echo L::Parameters ?></a></li>

                                   </ul>
                                 </div>
                               </div>
                               <div class="white-box">

                                 <div class="tab-content">
                                   <div class="tab-pane active fontawesome-demo" id="tab1">
                                     <div id="biography" >

                                       <div class="row">
                                         <div class="col-md-12">
                                         <span class="label label-md label-info"  style="text-align:center"> <i class="fa fa-info-circle"></i> <?php echo L::GeneralInfosParentTabsingle ?></span>  <span class="label label-md label-success"  style="text-align:center;color:white"><a href="#"  style="color:white" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-pencil"></i><?php echo L::ModifyingInfos ?></a></span>
                                         </div>

                                       </div>
                                       </br>
                                       <div class="table-responsive">

                                         <table class="table table-striped custom-table table-hover" >

                                             <thead>



                                             </thead>

                                             <tbody>

                                               <tr>

                                                   <td><a href="#"><?php echo L::CniAccount ?> :</a>

                                                   </td>

                                                   <td ><?php echo $cniLocal;?></td>



                                               </tr>
                                               <tr>

                                                   <td><a href="#"><?php echo L::EnterNationalite ?> :</a>

                                                   </td>

                                                   <td ><?php echo $nationaliteLocal;?></td>



                                               </tr>

                                                 <tr>

                                                     <td><a href="#"><?php echo L::NamestudentTab ?> :</a>

                                                     </td>

                                                     <td ><?php echo $nomcompletLocal;?></td>



                                                 </tr>


                                                 <?php
                                                 // if(strlen($tabStudent[7])>0)
                                                 // {
                                                  ?>
                                                 <tr>

                                                     <td><a href="#"> <?php echo L::EmailstudentTab ?> : </a>

                                                     </td>

                                                     <td ><?php echo $emailLocal;?></td>



                                                 </tr>
                                                 <?php
                                               // }
                                                  ?>

                                                  <?php
                                                  // if(strlen($tabStudent[12])>0)
                                                  // {
                                                   ?>

                                                 <?php
                                               // }
                                                  ?>
                                                 <tr>

                                                     <td><a href="#"> <?php echo L::SexestudentTab ?> : </a>

                                                     </td>

                                                     <td ><?php

                                                     $sexe=$sexeLocal;

                                                     if($sexe=="M")

                                                     {

                                                       echo L::SexeM;

                                                     }else {

                                                       echo L::SexeF;

                                                     }

                                                     ?></td>



                                                 </tr>
                                                 <tr>

                                                     <td><a href="#"><?php echo L::PhonestudentTab ?> :</a>

                                                     </td>

                                                     <td ><?php  echo $telephoneLocal;?></td>



                                                 </tr>
                                                 <tr>

                                                     <td><a href="#"><?php echo L::HabitationLocation ?> :</a>

                                                     </td>

                                                     <td ><?php echo $LieuHLocal;?></td>



                                                 </tr>
                                                 <tr>

                                                     <td><a href="#"><?php echo L::MatrimonialeSituation ?> :</a>

                                                     </td>

                                                     <td ><?php echo $situationLocal;?></td>



                                                 </tr>
                                                 <tr>

                                                     <td><a href="#"><?php echo L::Fonction ?> :</a>

                                                     </td>

                                                     <td ><?php  echo $fonctionLocal;?></td>



                                                 </tr>


                                                   <tr>

                                                       <td><a href="#"><?php echo L::Employeur ?> :</a>

                                                       </td>

                                                       <td ><?php  echo $societeLocal;?></td>



                                                   </tr>
                                                   <tr>

                                                       <td><a href="#"> <?php echo L::ProfessianalAdress ?>:</a>

                                                       </td>

                                                       <td ><?php  echo $adresseproLocal;?></td>



                                                   </tr>
                                                   <tr>

                                                       <td><a href="#"> <?php echo L::TelBureau ?>:</a>

                                                       </td>

                                                       <td ><?php  echo $BurophoneLocal;?></td>



                                                   </tr>






                                             </tbody>

                                         </table>

                                       </div></br>


                                         <div class="modal fade" id="exampleModal"  tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                           <div class="modal-dialog modal-lg" role="document">

                                             <div class="modal-content">

                                               <div class="modal-header">
                                                  <h4 class="modal-title" id="exampleModalLabel"><?php echo L::ModificationParent ?></h4>
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                  </button>
                                              </div>
                                              <div class="modal-body">

                                                <div id="divrecherchestation">



                                                  <form class = "" id = "FormUpdateParent" action="../controller/parent.php" method="post" enctype="multipart/form-data" >
                                                    <div class="row">
                                                      <div class="col-md-12">
                                                      <div class="form-group">

                                                      <div class="compose-editor col-md-6">
                                                        <input type="file" id="photoTea" name="photoTea" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="<?php echo $lien ?>" data-allowed-file-extensions="gif png jpg jpeg pjpeg" />
                                                      </div>

                                                      </div>


                                                      </div>

                                                    </div>
                                                    <div class="row">

                                                                        <div class="col-md-6">
                                                    <div class="form-group">
                                                    <label for=""><b><?php echo L::CniAccount ?><span class="required">  </span> :</b></label>

                                                  <input name="cniTea" id="cniTea" type="text" placeholder="<?php echo L::EnterCniAccount ?> " value="<?php echo $cniLocal ?>" class="form-control " />
                                                    </div>

                                                    </div>
                                                    <div class="col-md-6">
                                                         <div class="form-group">
                                                           <label for=""><b><?php echo L::EnterNationalite ?> <span class="required">  </span>: </b></label>
                                                             <input name="nationTea" id="nationTea" type="text" placeholder="<?php echo L::EnterNationalites ?> " value="<?php echo utf8_encode($nationaliteLocal) ?>" class="form-control " />
                                                          </div>


                                                     </div>
                                                     <div class="col-md-6">
                                 <div class="form-group">
                                 <label for=""><b><?php echo L::Name ?><span class="required">  </span> :</b></label>

                               <input type="text" name="nomTea" id="nomTea" data-required="1" placeholder="<?php echo L::EnterName ?>"  value="<?php echo utf8_encode($nomLocal) ?>"  class="form-control " />
                                 </div>

                                 </div>
                                 <div class="col-md-6">
                                      <div class="form-group">
                                        <label for=""><b><?php echo L::PreName ?> <span class="required">  </span>: </b></label>
                                          <input type="text" name="prenomTea" id="prenomTea" data-required="1" placeholder="<?php echo L::Enterparentprename ?>" value="<?php echo utf8_encode($prenomLocal) ?>"  class="form-control " />
                                       </div>


                                  </div>
                                  <div class="col-md-6">
                                       <div class="form-group">
                                         <label for=""><b><?php echo L::Gender ?> <span class="required">  </span>: </b></label>
                                         <select class="form-control " name="sexeTea" id="sexeTea">
                                            <option value="" <?php if($sexeLocal==""){echo "selected";}else{} ?> ><?php echo L::SelectGender ?></option>
                                             <option  <?php if($sexeLocal=="M"){echo "selected";}else{} ?> value="M"><?php echo L::SexeM ?></option>
                                             <option <?php if($sexeLocal=="F"){echo "selected";}else{} ?>value="F"><?php echo L::SexeF ?></option>
                                         </select>
                                        </div>


                                   </div>
                                  <div class="col-md-6">
              <div class="form-group">
              <label for=""><b><?php echo L::BirthstudentTab ?><span class="required">  </span> :</b></label>

          <input type="text" placeholder="<?php echo L::EnterBirthstudentTab ?>" name="datenaisTea" id="datenaisTea"  value="<?php echo date_format(date_create($datenaisLocal),"d-m-Y")   ?>" class="form-control ">
              </div>

              </div>
              <div class="col-md-6">
                   <div class="form-group">
                     <label for=""><b><?php echo L::BirthLocation ?> <span class="required">  </span>: </b></label>
                      <input type="text" name="lieunaisTea" id="lieunaisTea" data-required="1" placeholder="<?php echo L::BirthLocation ?>" value="<?php echo utf8_encode($lieunaisLocal)  ?>" class="form-control" />
                    </div>


               </div>

               <div class="col-md-6">
       <div class="form-group">
       <label for=""><b><?php echo L::EmailstudentTab ?><span class="required">  </span> :</b></label>

       <input name="emailTea" id="emailTea" type="text" placeholder="<?php echo L::EnterEmailAdress ?> " value="<?php echo utf8_encode($emailLocal)  ?>" class="form-control " />
       <span class="help-block" style="color:red;display:none" id="blocsms"> <i class="fa fa-warning" style="color:red;"></i> <?php echo L::EmailRequiredWithImageCharge ?></span>
       </div>

       </div>
       <div class="col-md-6">
       <div class="form-group">
       <label for=""><b><?php echo L::PhonestudentTab ?> <span class="required">  </span>: </b></label>
       <input name="contactTea" id="contactTea" type="text"  placeholder="<?php echo L::EnterPhoneNumber ?> " value="<?php echo utf8_encode($telephoneLocal)  ?>" class="form-control" />
       </div>


       </div>
       <div class="col-md-6">
       <div class="form-group">
       <label for=""><b><?php echo L::MatrimonialeSituation ?><span class="required">  </span> :</b></label>

       <select class="form-control " name="situationTea" id="situationTea">

       <option <?php if($situationLocal==""){echo "selected";}else{} ?> value=""><?php echo L::SelectMatrimonialeSituation ?></option>
       <option  <?php if($situationLocal=="CELIBATAIRE"){echo "selected";}else{} ?> value="CELIBATAIRE"><?php echo L::Celib ?></option>
       <option <?php if($situationLocal=="MARIE(E)"){echo "selected";}else{} ?> value="MARIE(E)"><?php echo L::Mariee ?></option>
       <option <?php if($situationLocal=="DIVORCE(E)"){echo "selected";}else{} ?> value="DIVORCE(E)"><?php echo L::Divorcee ?></option>
       <option <?php if($situationLocal=="VEUF(VE)"){echo "selected";}else{} ?> value="VEUF(VE)"><?php echo L::Veufvee ?></option>
       </select>
       <input type="hidden" name="session" id="session" value="<?php echo $libellesessionencours; ?>">
       <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner; ?>">
       <input type="hidden" name="oldphoto" id="oldphoto" value="<?php echo $photoLocal; ?>">
       <input type="hidden" name="compteid" id="compteid" value="<?php echo $_GET['compte'] ?>">
       <input type="hidden" name="etape" id="etape" value="7">

       </div>

       </div>
       <div class="col-md-6">
       <div class="form-group">
       <label for=""><b><?php echo L::ChildNb  ?><span class="required">  </span>: </b></label>
       <input name="nbchieldTea" id="nbchieldTea" type="text"  placeholder="<?php echo L::EnterChildNb ?> " value="<?php echo $nbchieldLocal  ?>" class="form-control" />
       </div>


       </div>
       <div class="col-md-6">
       <div class="form-group">
       <label for=""><b><?php echo L::ChildNbscolarisy ?> <span class="required">  </span>: </b></label>
       <input name="nbchieldscoTea" id="nbchieldscoTea" type="text"  placeholder="<?php echo L::EnterChildNbscolarisy ?> " value="<?php echo $nbchieldscoLocal  ?>" class="form-control" />
       </div>


       </div>

       <div class="col-md-6">
       <div class="form-group">
       <label for=""><b><?php echo L::HabitationLocation ?><span class="required">  </span> :</b></label>

       <input name="lieuHTea" id="lieuHTea" type="text" placeholder="<?php echo L::EnterHabitationLocation ?>" value="<?php echo $LieuHLocal  ?>" class="form-control " />
       </div>

       </div>
       <div class="col-md-6">
       <div class="form-group">
       <label for=""><b><?php echo L::Adressepostales ?><span class="required">  </span> :</b></label>

       <input type="text" name="adrespostaleTea" id="adrespostaleTea" data-required="1" placeholder="<?php echo L::AdressepostaleParent ?>" class="form-control" />
       </div>

       </div>
       <div class="col-md-6">
       <div class="form-group">
       <label for=""><b><?php echo L::Employeur ?> <span class="required">  </span>: </b></label>
       <input type="text" name="employeurTea" id="employeurTea" data-required="1" placeholder="<?php echo L::Employeur ?>" value="<?php echo utf8_encode($societeLocal)  ?>" class="form-control" />
       </div>


       </div>
       <div class="col-md-6">
       <div class="form-group">
       <label for=""><b><?php echo L::Fonction ?> <span class="required">  </span>: </b></label>
       <input name="fonctionTea" id="fonctionTea" type="text" placeholder="<?php echo L::EnterFonction ?> " value="<?php echo utf8_encode($fonctionLocal)  ?>" class="form-control " />
       </div>


       </div>
       <div class="col-md-6">
       <div class="form-group">
       <label for=""><b><?php echo L::TelBureau ?> <span class="required">  </span>: </b></label>
       <input type="tel" name="telburoTea" id="telburoTea" data-required="1" placeholder="<?php echo L::TelBureau ?>" value="<?php echo $BurophoneLocal ?>" class="form-control" />
       </div>


       </div>
       <div class="col-md-6">
       <div class="form-group">
       <label for=""><b><?php echo L::ProfessianalAdress ?><span class="required">  </span>: </b></label>
       <input name="adresproTea" id="adresproTea" type="text"  placeholder="<?php echo L::EnterProfessianalAdress ?> " value="<?php echo utf8_encode($adresseproLocal) ?>" class="form-control " />
       </div>


       </div>

       <div class="form-actions" style="margin-left:20px;">
                      <div class="row">
                          <div class="col-md-12">
                             <button type="submit" class="btn btn-success" id="searchbtn" style="border-radius:5px"><i class="fa fa-pencil"></i> <?php echo L::ModifierBtn ?></button>
                              <button type="button" class="btn btn-danger"  data-dismiss="modal" aria-label="Close"><?php echo L::AnnulerBtn ?></button>
                          </div>
                        </div>
                     </div>



                                                                  </div>
                                                  </form>
                                                  <br>


                                                </div>



                                              </div>


                                             </div>

                                           </div>

                                         </div>




                                     </div>

                                   </div>

                                   <div class="tab-pane" id="tab2">
                                     <div class="container-fluid">
                                       <div class="full-width p-l-20">
                                         <div class="row">

                                           <div class="col-md-12">
                                           <span class="label label-md label-info" style="text-align:center"><?php echo L::Historiqdepenses ?></span>
                                           </div>
                                        </div><br><br>
                                        <div class="table-responsive">
                                        <table class="table table-striped custom-table table-hover">
                                          <thead>
                                            <tr>
                                              <th><?php echo L::Versements ?> </th>
                                              <th><?php echo L::LittleDateVersements ?></th>
                                              <th><?php echo L::AmountVerser ?></th>
                                              <th><?php echo L::AmountReste ?></th>
                                            </tr>

                                              </thead>
                                         <tbody>
                                           <?php
                                           foreach ($parentlyStudent as $valuestudent):
                                            $datas=$student->getAllInformationsOfStudentNew($valuestudent->idcompte_eleve,$libellesessionencours);
                                              foreach ($datas as $values):
                                                $libelleclasse=$values->libelle_classe;
                                                $idclasse=$values->id_classe;
                                              endforeach;
                                             ?>
                                             <tr>
                                               <td colspan="4" style="text-align:center;background-color:#1a88ff"><?php echo $valuestudent->nom_eleve." ".$valuestudent->prenom_eleve; ?></td>
                                             </tr>
                                             <?php
                                               $datasScolarites=$etabs->getAllScolaritesVersmentChild($codeEtabAssigner,$libellesessionencours,$valuestudent->idcompte_eleve,$idclasse);
                                               $nbdatascolarites=count($datasScolarites);
                                                if($nbdatascolarites==0)
                                                {
                                                  ?>
                                                  <tr>
                                                    <td colspan="4"><span class="label label-md label-warning" style="text-align:center"><?php echo L::Aucunversement ?></span></td>
                                                  </tr>

                                                  <?php
                                                }else {
                                                    foreach ($datasScolarites as $value):
                                                      ?>
                                                      <tr>

                                                          <td><span class="label label-sm label-info"><?php echo $value->code_versement; ?></span></td>

                                                          <td><?php echo date_format(date_create($value->date_versement),"d-m-Y")?></td>

                                                          <td><?php echo $value->montant_versement. " ".$value->devise_versement;?></td>

                                                          <td><?php

                                                          $resteapayer=$value->solde_versement;

                                                          if($resteapayer==0)

                                                          {

                                                            ?>

                                                            <span class="label label-sm label-success"><?php echo L::AmountSolder ?></span>

                                                            <?php

                                                          }else if($resteapayer>0)

                                                          {

                                                            ?>

                                                             <span class="label label-sm label-danger"><?php echo $value->solde_versement. " ".$value->devise_versement;?></span>

                                                            <?php

                                                          }

                                                          ?></td>



                                                      </tr>
                                                      <?php
                                                    endforeach;

                                                }
                                              ?>


                                             <?php
                                           endforeach;
                                            ?>
                                        </tbody>

                                          </table>
                                        </div>
                                       </div>



                                     </div>

                                   </div>

                                   <div class="tab-pane" id="tab3">

                                     <div class="container-fluid">
                                       <div class="full-width p-l-20">

                                         <div class="row">

                                           <div class="col-md-12">
                                           <span class="label label-md label-info" style="text-align:center"><?php echo L::EnfantsListing ?></span>
                                           </div>
                                        </div><br><br>


                                        <div class="table-responsive">
                                        <table class="table table-striped custom-table table-hover">
                                          <thead>
                                                <th><?php echo L::MatriculestudentTab ?></th>
                                                <th><?php echo L::NamestudentTab ?> </th>
                                                <th><?php echo L::ClassestudentTab ?> </th>
                                              </thead>
                                         <tbody>
                                           <?php
                                           foreach ($parentlyStudent as $valuestudent):
                                            $datas=$student->getAllInformationsOfStudentNew($valuestudent->idcompte_eleve,$libellesessionencours);
                                              foreach ($datas as $values):
                                                $libelleclasse=$values->libelle_classe;
                                              endforeach;
                                             ?>
                                             <tr>
                                            <td><?php echo $valuestudent->matricule_eleve; ?></td>
                                              <td><?php echo $valuestudent->nom_eleve." ".$valuestudent->prenom_eleve; ?></td>
                                              <td ><?php echo $libelleclasse ?></td>

                                              </tr>
                                             <?php
                                           endforeach;
                                            ?>
                                        </tbody>

                                          </table>
                                        </div>



                                       </div>


                                     </div>

                                   </div>
                                   <div class="tab-pane  fontawesome-demo" id="tab4">

                                     <div id="biography" >

                                     </div>

                                   </div>

                                   <div class="tab-pane  fontawesome-demo" id="tab5">
                                     <div id="biography" >

                                       <div class="row">

                                         <div class="row">
                                           <div class="col-md-12">
                                           <span class="label label-md label-info" style="text-align:center"><?php echo L::InformationsLogin ?></span>
                                           </div>
                                         </div>

                                       </div></br>
                                       <form class="form-control" action="../controller/compte.php" method="post" id="Formcnx">
                                         <div class="form-body">

                                           <div class="form-group row">
                                                <label class="control-label col-md-3"><?php echo L::Logincnx ?>
                                                    <span class="required">*  </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <input name="loginTea" id="loginTea" type="text" placeholder="<?php if(strlen($loginLocal)>0) { echo $loginLocal; }else { echo "Entrer le Login";}?>"  value="<?php echo $loginLocal;?> " class="form-control" />

                                                      </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3"><?php echo L::Passcnx ?>
                                                    <span class="required">*  </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <input name="passTea" id="passTea" type="password" placeholder="<?php echo L::PasswordEnter ?> " class="form-control " /> </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3"><?php echo L::ConfirmPasscnx ?>
                                                    <span class="required">*  </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <input name="confirmTea" id="confirmTea" type="password" placeholder="<?php echo L::ConfirmPassword ?> " class="form-control " /> </div>
                                                    <input type="hidden" name="etape" id="etape" value="3"/>
                                                     <input type="hidden" name="idcompte" id="idcompte" value="<?php echo $_GET['compte'];?>"/>
                                                     <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner;?>"/>

                                            </div>
                                            <div class="form-actions">
                                                                  <div class="row">
                                                                      <div class="offset-md-3 col-md-9">
                                                                          <button class="btn btn-success" type="submit"><?php echo L::ModifierBtn ?></button>
                                                                          <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                      </div>
                                                                    </div>
                                                                 </div>

                                         </div>

                                       </form>


                                     </div>
                                   </div>



                                 </div>

                               </div>

                             </div>

                         </div>

                       </div>


                                    </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <script src="../assets2/js/pages/material_select/getmdl-select.js" ></script>
   <script src="../assets/js/formatter/jquery.formatter.min.js"></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>

   <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
 	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
 	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script>
 // function SetcodeEtab(codeEtab)
 // {
 //   var etape=3;
 //   $.ajax({
 //     url: '../ajax/sessions.php',
 //     type: 'POST',
 //     async:false,
 //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
 //     dataType: 'text',
 //     success: function (content, statut) {
 //
 // window.location.reload();
 //
 //     }
 //   });
 // }
 function addFrench()
 {
   var etape=1;
   var lang="fr";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function addEnglish()
 {
   var etape=1;
   var lang="en";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 var date = new Date();
var newDate = new Date(date.setTime( date.getTime() + (0 * 86400000)));

 // $('#datenaisTea').bootstrapMaterialDatePicker
 // ({
 //   date:true,
 //   shortTime: false,
 //   time:false,
 //   maxDate:newDate,
 //   format :'DD-MM-YYYY',
 //   lang: 'fr',
 //  cancelText: '<?php //echo L::AnnulerBtn ?>',
 //  okText: '<?php //echo L::Okay ?>',
 //  clearText: '<?php //echo L::Eraser ?>',
 //  nowText: '<?php //echo L::Now ?>'
 //
 // });

 function modify(id)
 {
   Swal.fire({
 title: '<?php echo L::WarningLib ?>',
 text: "<?php echo L::DoyouReallyModifyingThisParentAccount ?>",
 type: 'warning',
 showCancelButton: true,
 confirmButtonColor: '#3085d6',
 cancelButtonColor: '#d33',
 confirmButtonText: '<?php echo L::ModifierBtn ?>',
 cancelButtonText: '<?php echo L::AnnulerBtn ?>',
 }).then((result) => {
 if (result.value) {
 document.location.href="updateparent.php?compte="+id;
 }else {

 }
 })
 }
 jQuery(document).ready(function() {

   $("#datenaisTea").formatter({pattern:"{{99}}-{{99}}-{{9999}}"});
$("#contactTea").formatter({pattern:"{{99}}{{99}}{{99}}{{99}}"});
$("#nbchieldTea").formatter({pattern:"{{999}}"});
$("#nbchieldscoTea").formatter({pattern:"{{999}}"});
$("#telburoTea").formatter({pattern:"{{99}}{{99}}{{99}}{{99}}"});


   $("#FormConnexion").validate({
     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
     rules:{
       loginTea:"required",
       passTea:{
           required: true,
           minlength: 6
       },
       confirmTea:{
           required: true,
           minlength: 6,
           equalTo:'#passTea'
       }
     },
     messages:{
       confirmTea:{
           required:"<?php echo L::Confirmcheck ?>",
           minlength:"<?php echo L::Confirmincheck ?>",
           equalTo: "<?php echo L::ConfirmSamecheck ?>"
       },
       passTea: {
           required:"<?php echo L::Passcheck ?>",
           minlength:"<?php echo L::Confirmincheck ?>"
       },
       loginTea:"<?php echo L::Logincheck ?>"
     }
   });

   $("#FormPersonnelle").validate({
     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
       rules:{
         nomTea:"required",
         prenomTea:"required"
       },
       messages:{
         nomTea:"<?php echo L::renseignerNameParent ?>",
         prenomTea:"<?php echo L::renseignerPreNameParent ?>"
       }

   });


   $("#FormAddLocalAd").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{
        passTea: {
            required: true,
            minlength: 6
        },
        confirmTea:{
            required: true,
            minlength: 6,
            equalTo:'#passTea'
        },
        fonctionTea:"required",
        cniTea:"required",

        loginTea:"required",
        emailTea: {
                   required: true,
                   email: true
               },
        contactTea:"required",
        datenaisTea:"required",
        prenomTea:"required",
        nomTea:"required",
        gradeTea:"required",
        dateEmbTea:"required",
        sexeTea:"required"



      },
      messages: {
        confirmTea:{
            required:"<?php echo L::Confirmcheck ?>",
            minlength:"<?php echo L::Passmincheck ?>",
            equalTo: "<?php echo L::ConfirmSamecheck ?>"
        },
        passTea: {
            required:"<?php echo L::Passcheck ?>",
            minlength:"<?php echo L::Passmincheck ?>"
        },
        loginTea:"<?php echo L::Logincheck ?>",
        emailTea:"<?php echo L::PleaseEnterEmailAdress ?>",
        contactTea:"<?php echo L::PleaseEnterPhoneNumber ?>",
        datenaisTea:"<?php echo L::PleaseEnterPhonestudentTab ?>",
        prenomTea:"<?php echo L::PleaseEnterPrename ?>",
        nomTea:"<?php echo L::PleaseEnterName ?> ",
        fonctionTea:"<?php echo L::PleaseEnterFonction ?>",
        gradeTea:"<?php echo L::EnterGrade ?>",
        dateEmbTea:"<?php echo L::EnterDateEmbauche ?>",
        sexeTea:"<?php echo L::PleaseEnterSexe ?>",
        cniTea:"<?php echo L::PleaseEnterCniAccount ?>"
      },
      submitHandler: function(form) {
        //verifier si ce compte n'existe pas encore dans la base de données
           var etape=1;
           $.ajax({
             url: '../ajax/parent.php',
             type: 'POST',
             async:false,
             data: 'login=' + $("#loginTea").val()+ '&email=' + $("#emailTea").val() + '&cni=' + $("#cniTea").val() + '&etape=' + etape,
             dataType: 'text',
             success: function (content, statut) {




               if(content==0)
               {
                 //le compte n'existe pas dans la base on peut l'ajouter

                 form.submit();
               }else if(content==1) {
                 //Un Parent existe dejà avec cette CNI
                 Swal.fire({
                 type: 'warning',
                 title: '<?php echo L::WarningLib ?>',
                 text: '<?php echo L::AparentHaveThisCniAccountl ?>',

                 })

               }else if(content==2) {
                 //le compte existe dejà dans la base de données
                 Swal.fire({
                 type: 'warning',
                 title: '<?php echo L::WarningLib ?>',
                 text: '<?php echo L::AparentHaveThisAdressEmail ?>',

                 })

               }else if(content==3) {
                 //le compte existe dejà dans la base de données
                 Swal.fire({
                 type: 'warning',
                 title: '<?php echo L::WarningLib ?>',
                 text: '<?php echo L::ThisEmailAdressAllreadyExistes ?>',

                 })

               }

             }


           });
      }


   });


 });
 </script>
    <!-- end js include path -->
  </body>

</html>
