<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/Tache.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

// echo $_SESSION['user']['fonctionuser'];
if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$tachex = new Tache();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

if(strlen($_SESSION['user']['codeEtab'])>0)
{
  $codeEtabAssigner=$_SESSION['user']['codeEtab'];
}else {
  $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
}
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//nous allons chercher la liste des etablissements du groupe

$allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
$agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$libellesessionencours="";

  if($etablissementType==1||$etablissementType==3)
  {
    if($nbsessionOn>0){
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];

    }

  }else {
    if($nbsessionOn>0)
    {
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];
      $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
      $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
    }
  }

  if(strlen($_SESSION['user']['codeEtab'])>0)
  {

  }else {
    $_SESSION['user']['codeEtab']=$codeEtabAssigner;
  }

  $_SESSION['user']['session']=$libellesessionencours;





   // var_dump($allcodeEtabs);

  $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

  $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);



$allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);

$lasttachesassignated=$tachex->gettacherassignerTouserlast($_SESSION['user']['IdCompte']);

$lasttachesassignatedby=$tachex->gettacherassignerTouserlastby($_SESSION['user']['IdCompte']);

$nbnotificationstandby=$parents->getParentnotificationstandbyNb($compteuserid);

//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($compteuserid);


  // var_dump($datastat);

// echo $libellesessionencours."/".$_SESSION['user']['codeEtab'];

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <link href="../assets2/css/style1.css" rel="stylesheet" type="text/css" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::dashb ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::dashb ?></li>
                            </ol>
                        </div>

                    </div>




					<!-- start widget -->

          <div class="row ">
            <!-- <div class="col-md-12 ">
          <img src="../assets/img/logo/eduksvg.svg" alt="" style="width:20%" class="pull-right">
            </div> -->

            <div class="col-md-12 ">
              <div class="alert blue-bgcolor alert-dismissible fade show" role="alert">
              <?php echo L::Bienvenues ?> <?php echo $etabs->getEtabLibellebyCodeEtab($_SESSION['user']['codeEtab']); ?>
              <a href="#" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
               </a>
              </div>
            </div>

          </div>

					<div class="state-overview">

						</div>


                 <div class="row">
                   <div class="col-md-12 col-sm-12">

                     <div class="state-overview">

                       <div class="row">

                              <!-- /.col -->
                             <div class="col-lg-4 col-sm-4">
                            <div class="overview-panel orange">
                              <div class="symbol">
                                <i class="material-icons f-left">group</i>
                              </div>
                              <div class="value white">
                                <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php if(strlen($libellesessionencours)>0){echo $user->getNbofStudentforThisAllschoolSession($compteuserid,$libellesessionencours);}else{echo "0";} ?>"><?php echo $user->getNbofStudentforThisAllschoolSession($compteuserid,$libellesessionencours);?></p>
                                <p> <a href="studentalls.php" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::EleveMenu) ?></a> </p>
                              </div>
                            </div>
                          </div>
                              <!-- /.col -->
                              <div class="col-lg-4 col-sm-4">
                            <div class="overview-panel deepPink-bgcolor">
                              <div class="symbol">
                                <i class="material-icons f-left">school</i>
                              </div>
                              <div class="value white">
                                <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php echo $tabstat[2];?>"><?php echo $user->getNumberofTeatcherforThisAllschool($compteuserid);?></p>
                                <p> <a href="teatchers.php" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::TeatcherCapsingle) ?></a> </p>
                              </div>
                            </div>
                          </div>
                              <!-- /.col -->
                              <div class="col-lg-4 col-sm-4">
                            <div class="overview-panel blue-bgcolor">
                              <div class="symbol">
                                  <i class="material-icons f-left">person</i>
                              </div>
                              <div class="value white">
                                <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php echo $tabstat[3];?>"><?php echo $user->getNumberofParentforThisAllschool($compteuserid)?></p>
                              <!-- <p><?php //echo strtoupper(L::ParentsMenu) ?></p> -->
                              <p> <a href="parents.php" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::ParentsMenu) ?></a></p>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-4 col-sm-4">
                    <div class="overview-panel purple">
                      <div class="symbol">
                        <i class="fa fa-book usr-clr"></i>
                      </div>
                      <div class="value white">
                        <p class="sbold addr-font-h1" data-counter="counterup" data-value="23"><?php echo $etabs->getAllfamillynumberEtabs($compteuserid) ?></p>
                        <p><a href="matieres.php" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::MatiereBold);  ?></a> </p>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-4 col-sm-4">
            <div class="overview-panel green">
              <div class="symbol">
                <i class="fa fa-users usr-clr"></i>
              </div>
              <div class="value white">
                <p class="sbold addr-font-h1" data-counter="counterup" data-value="23"><?php echo $etabs->getNumberAssignatedEtabs($compteuserid) ?></p>
                <p><a href="users.php" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::UsersMenu);  ?></a> </p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-sm-4">
     <div class="overview-panel yellow">
      <div class="symbol">
        <i class="fa fa-institution usr-clr"></i>
      </div>
      <div class="value white">
        <p class="sbold addr-font-h1" data-counter="counterup" data-value="23"><?php echo $etabs->getNumberAllclassesEtab($compteuserid,$libellesessionencours); ?></p>
        <p><a href="#" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::ClassesMenu);  ?></a> </p>
      </div>
     </div>
     </div>

                            </div>

                     </div>

                   </div>
                   <div class="col-md-4">
                       <div class="card card-box">
                           <div class="card-head">
                               <header><?php echo L::EleveCaps ?></header>
                               <!--div class="tools">
                                   <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                 <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                 <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                               </div-->
                           </div>
                           <div class="card-body " id="chartjs_doughnut_parent">
                               <div class="row">
                                   <canvas id="chartjs_doughnut" height="320"></canvas>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="col-md-8">

                     <?php

                      $transactionNb=$etabs->getNumberOfTransactions($_SESSION['user']['codeEtab'],$libellesessionencours);

                      // if($transactionNb>0)
                      // {
                         $lasttransactions=$etabs->getAllLastTransactions($_SESSION['user']['codeEtab'],$libellesessionencours);
                      ?>
                      <div class="card  card-box">
                            <div class="card-head">
                                <header><?php echo L::transactionstories ?></header>
                                <div class="tools">
                                    <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                  <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                  <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                </div>
                            </div>
                            <div class="card-body no-padding height-9">
                                <div class="row">
                                    <div class="noti-information notification-menu">
                                        <div class="notification-list mail-list not-list small-slimscroll-style">
                                          <?php

                                          foreach ($lasttransactions as $value):
                                            ?>
                                            <a href="javascript:;" class="single-mail"> <span class="icon bg-b-green box-shadow-1"> <i class="fa fa-money"></i>
                    </span> <span class="text-purple"><?php echo $value->code_versement ?></span> <span class="label label-info" style="margin-left:20px;"> <?php echo $value->motif_versement ?></span>  <span style="margin-left:20px;" ><?php echo $student->getNameInfos($value->ideleve_versement) ?></span> <span class="label label-warning pull-right"> <?php echo $value->montant_versement ?> </span>
                                                <span class="notificationtime">
                                                    <small><?php echo date_format(date_create($value->date_versement),"d/m/Y") ?></small>
                                                </span>
                                            </a>
                                            <?php
                                          endforeach;

                                           ?>


                                        </div>

                                          <!--div class="full-width text-center p-t-10" style="text-align:center" >
                                            <a href="transactions.php" class="btn green btn-outline btn-circle margin-0"><?php// echo L::ViewAll ?></a>

                                          </div-->


                                    </div>

                                </div>
                                <div class="full-width text-center p-t-10" style="text-align:center;" >
                                  <a href="transactions.php" class="btn green btn-outline btn-circle margin-0"><?php echo L::ViewAll ?></a>

                                </div>
                            </div>
                        </div>
                      <?php

                       ?>

                   </div>



                    <div class="offset-md-4 col-md-8 col-sm-12 col-12">

               </div>

                    <?php
                  // }
                  ?>


                 </div>



                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <!-- start chat sidebar -->

                        <div class="chat-sidebar-container" data-close-on-body-click="false">
                        <div class="chat-sidebar">
                          <ul class="nav nav-tabs">
                            <li class="nav-item">
                              <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                                  class="material-icons">
                                  chat</i>Chat
                                <!-- <span class="badge badge-danger">4</span> -->
                              </a>
                            </li>
                          </ul>
                          <div class="tab-content">
                            <!-- Start User Chat -->
                            <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                              id="quick_sidebar_tab_1"> -->
                              <div class="chat-sidebar-chat "
                                >
                              <div class="chat-sidebar-list">
                                <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                                  data-wrapper-class="chat-sidebar-list">
                                  <div class="chat-header">
                                    <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                                  </div>
                                  <ul class="media-list list-items">
                                    <?php

                                    foreach ($onlineUsers as  $valueUsers):
                                      $tofuser=$valueUsers->photo_compte;
                                      if(strlen($tofuser)>0)
                                      {
                                        $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                                      }else {
                                        $lientofuser="../photo/user5.jpg";
                                      }
                                      if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                                      {
                                        ?>
                                        <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                            width="35" height="35" alt="...">
                                          <i class="online dot red"></i>
                                          <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                            <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                            <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                                          </div>
                                        </li>
                                        <?php
                                      }
                                      ?>

                                      <?php
                                    endforeach;
                                     ?>

                                  </ul>
                                  <div class="chat-header">
                                    <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                                  </div>
                                  <ul class="media-list list-items">
                                    <?php

                                    foreach ($offlineUsers as  $valueUsers):
                                      $tofuser=$valueUsers->photo_compte;
                                      if(strlen($tofuser)>0)
                                      {
                                        $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                                      }else {
                                        $lientofuser="../photo/user5.jpg";
                                      }
                                      ?>
                                      <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                          width="35" height="35" alt="...">
                                        <i class="offline dot"></i>
                                        <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                          <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                          <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                                        </div>
                                      </li>
                                      <?php
                                    endforeach;
                                     ?>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <!-- End User Chat -->
                          </div>
                        </div>
                      </div>
                        <!-- end chat sidebar -->
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
  <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- morris chart -->

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>

     <script src="../assets2/plugins/chart-js/Chart.bundle.js" ></script>
     <script src="../assets2/plugins/chart-js/utils.js" ></script>
     <!-- <script src="../assets2/js/pages/chart/chartjs/chartjs-data.js" ></script> -->
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>

  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>



   function terminer(idcompte,idtache)
   {
     // alert(idcompte);
     var etape=1;

     Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::Haveyoufinishtasks ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::True ?>',
cancelButtonText: '<?php echo L::False ?>',
}).then((result) => {
if (result.value) {

  $.ajax({
    url: '../ajax/taches.php',
    type: 'POST',
    async:false,
    data: 'etape=' + etape+ '&idcompte=' +idcompte+'&idtache='+idtache,
    dataType: 'text',
    success: function (content, statut) {

    // location.reload();

    }
  });

}else {

}
})
   }

   function addmessages(senderid,receiverid)
   {

     var etape=1;
     $.ajax({
       url: '../ajax/chat.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&senderid=' +senderid+'&receiverid='+receiverid,
       dataType: 'text',
       success: function (content, statut) {

       document.location.href="chats.php";

       }
     });
   }

   // function SetcodeEtab(codeEtab)
   // {
   //   var etape=3;
   //   $.ajax({
   //     url: '../ajax/sessions.php',
   //     type: 'POST',
   //     async:false,
   //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
   //     dataType: 'text',
   //     success: function (content, statut) {
   //
   // window.location.reload();
   //
   //     }
   //   });
   // }
   //
   // function SetAllcodeEtab()
   // {
   //   var etape=4;
   //   $.ajax({
   //     url: '../ajax/sessions.php',
   //     type: 'POST',
   //     async:false,
   //     data: 'etape=' + etape,
   //     dataType: 'text',
   //     success: function (content, statut) {
   //
   // window.location.reload();
   //
   //     }
   //   });
   // }

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function loader()
   {
     var etape=1;
     var codeEtab="<?php echo $_SESSION['user']['codeEtab'];?>";
     var session="<?php echo $libellesessionencours ; ?>";

     $.ajax({
       url: '../ajax/load.php',
       type: 'POST',
       async:false,
       data: 'codeEtab=' +codeEtab+ '&etape='+etape+'&session='+session,
       dataType: 'text',
       success: function (content, statut) {

         var data=content;

         // callback(data);

       }
     });

   }

   function Testing()
   {
     var etape=6;
     var codeEtab="<?php echo $_SESSION['user']['codeEtab'];?>";
     var session="<?php echo $libellesessionencours ; ?>";
     var idcompte="<?php echo $_SESSION['user']['IdCompte'] ?>";

     $.ajax({
       url: '../ajax/chat.php',
       type: 'POST',
       async:false,
       data: 'codeEtab=' +codeEtab+ '&etape='+etape+'&session='+session+'&idcompte='+idcompte,
       dataType: 'text',
       success: function (content, statut) {

         // var data=content;

         var tabcontent=content.split("*");
     	   // var hf=heurefin.split(":");

         if(tabcontent[0]==1)
         {
           var heure=tabcontent[1];
           var etape=7;
           $.ajax({
             url: '../ajax/chat.php',
             type: 'POST',
             async:false,
             data: 'codeEtab=' +codeEtab+ '&etape='+etape+'&session='+session+'&idcompte='+idcompte+'&heure='+heure,
             dataType: 'text',
             success: function (content, statut) {

               // var data=content;

               var tabconcat=content.split("*");

               location.reload();

               // if(tabconcat[0]==1)
               // {
               //   Swal.fire({
               //   type: 'error',
               //   title: '<?php //echo L::WarningLib ?>',
               //   text: "<?php //echo @$_SESSION['user']['addtaches']; ?>",
               //
               // })

               //nous allons mettre le statut de la relance a 1

               // var etape=8;
               // var tacheid=tabconcat[1];
               //
               // $.ajax({
               //   url: '../ajax/chat.php',
               //   type: 'POST',
               //   async:false,
               //   data: 'tacheid=' +tacheid+ '&etape='+etape,
               //   dataType: 'text',
               //   success: function (content, statut) {
               //
               //
               //
               //   }
               // });

               // }

               // callback(data);

             }
           });
         }

         // callback(data);

       }
     });
   }

   setInterval(Testing,1*2*5000);

   $(document).ready(function() {

     // alert(session);

     var MONTHS = ['<?php echo L::JanvLib?>','<?php echo L::FevLib ?>','<?php echo L::MarsLib ?>','<?php echo L::AvriLib ?>','<?php echo L::MaiLib ?>','<?php echo L::JuneLib ?>','<?php echo L::JulLib ?>','<?php echo L::AoutLib ?>','<?php echo L::SeptLib?>','<?php echo L::OctobLib?>','<?php echo L::NovbLib?>','<?php echo L::DecemLib?>'];
        var config = {
            type: 'line',
            data: {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [{
                    label: "Paiements Attendus",
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor()
                    ],
                    fill: false,
                }, {
                    label: "Paiements reçus",
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor()
                    ],
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Mois'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: ''
                        }
                    }]
                }
            }
        };
        var ctx = document.getElementById("chartjs_line").getContext("2d");
        window.myLine = new Chart(ctx, config);

     // var randomScalingFactor = function() {
     //       return Math.round(Math.random() * 100);
     //   };

       var config = {
           type: 'doughnut',
           data: {
               datasets: [{
                   data: [
                       <?php echo $etabs->getnumberOfwomenchild($_SESSION['user']['codeEtab'],$libellesessionencours); ?>,
                       // randomScalingFactor(),
                       // randomScalingFactor(),
                       // randomScalingFactor(),
                       <?php echo $etabs->getnumberOfmenchild($_SESSION['user']['codeEtab'],$libellesessionencours); ?>,
                   ],
                   backgroundColor: [
                       window.chartColors.red,
                       // window.chartColors.orange,
                       // window.chartColors.yellow,
                       // window.chartColors.green,
                       window.chartColors.blue,
                   ],
                   label: 'Dataset 1'
               }],
               labels: [
                   "<?php echo L::Filles ?>",
                   // "Orange",
                   // "Yellow",
                   // "Green",
                   "<?php echo L::Garcons ?>"
               ]
           },
           options: {
               responsive: true,
               legend: {
                   position: 'top',
               },
               title: {
                   display: true,
                   text: ''
               },
               animation: {
                   animateScale: true,
                   animateRotate: true
               }
           }
       };

           var ctx = document.getElementById("chartjs_doughnut").getContext("2d");
           window.myDoughnut = new Chart(ctx, config);


   });


   </script>
    <!-- end js include path -->
  </body>

</html>
