<?php
session_start();
require_once('../controller/functions.php');
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$userId=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$classeShool=new Classe();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();

//recuperaion des variables

$codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
$classeid=htmlspecialchars(addslashes($_GET['classeEtab']));
$session=htmlspecialchars(addslashes($_GET['session']));
$datedeb=htmlspecialchars(addslashes($_GET['datedeb']));
$nb=htmlspecialchars(addslashes($_GET['nb']));

$libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
$logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
$lienlogo="../logo_etab/".$codeEtab."/".$logoEtab;
$contacts="";
$infosEtab=$etabs->getEtabInfosbyCode($codeEtab);
$tabEtab=explode('*',$infosEtab);

 if($tabEtab[3]!="")
 {
   $contacts=$tabEtab[2]." / ".$tabEtab[3];
 }else {
   $contacts=$tabEtab[2];
 }

 $datefin=date("d-m-Y", strtotime("+".$nb."day", strtotime($datedeb)));


 $dataday=$etabs->getAllweeks();
 $libelleclasse=$classeShool->getInfosofclassesbyId($classeid,$session);
 $students=$student->getAllstudentofthisclassesSession($classeid,$session);
 $libelledoc="RECAPITULATIF DES ABSENCES";

require('fpdf/fpdf.php');

class PDF extends FPDF
{
function Footer()
{
    // Positionnement à 1,5 cm du bas
    $this->SetY(-15);
    // Police Arial italique 8
    $this->SetFont('Arial','I',8);
    // Numéro et nombre de pages
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    // $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
}
}

$pdf = new PDF('L','mm','A4');
$pdf->AliasNbPages();
$pdf->SetFont('Times','B',  16);
$pdf->AddPage();
$pdf->Ln(20);
$pdf->Image("../logo_etab/".$codeEtab."/".$logoEtab,5,5,25,25);
$pdf->Ln(20);
$pdf-> SetFont('Times','B',  15);
$pdf->Cell(176,5,$libelledoc,0,0,'C');
$pdf->Ln(10);
$pdf->SetFillColor(230,230,0);
$pdf->SetLineWidth(.3);
$pdf->SetFont('Times','B',10);
// $pdf->Cell(30,8,'ELEVES',1,0,'C');
for($x=0;$x<=$nb;$x++)
{
  $deadLine = date("d-m", strtotime("+".$x."day", strtotime($datedeb)));
  $pdf->Cell(4,8,$deadLine,1,0,'C');
}



$pdf->Output();


 ?>
