<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}
$classeselect="";
$studentselect="";
if(isset($_GET))
{

  $classeselect=htmlspecialchars(addslashes(@$_GET['classe']));
  $studentselect=htmlspecialchars(addslashes(@$_GET['idcompte']));
}else if(isset($_POST))
{

  $classeselect=htmlspecialchars(addslashes(@$_POST['classeEtab']));
  $studentselect=htmlspecialchars(addslashes(@$_POST['eleveid']));
}


$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
     <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>


	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Récapitulatif des notes </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><!--i class="fa fa-home"></i-->&nbsp;<a class="parent-item" href="index.php">Evaluations & Notes</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Récapitulatif des notes  </li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">
              <?php

                    if(isset($_SESSION['user']['addattendailyok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
            <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['addattendailyok']; ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addattendailyok']);
                    }

                     ?>
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

<br/>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="card card-topline-green">
                   <div class="card-head">
                       <header>Récapitulatif des notes</header>
                       <div class="tools">
                           <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                       </div>
                   </div>
                   <div class="card-body ">
                     <form method="post" id="FormNotes" action="recapnoteseleves.php">
                         <div class="row">

                       <div class="col-md-6 col-sm-6">
                       <!-- text input -->
                       <div class="form-group" style="margin-top:8px;">
                           <label>Matière</label>
                           <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                           <select class="form-control input-height" id="matclasse" name="matclasse" style="width:100%;" >
                               <option value="">Selectionner une matière</option>


                           </select>
                            <input type="hidden" id="classeEtab" name="classeEtab" class="form-control" value="<?php echo $classeselect; ?>">
                       </div>


                   </div>
                           <div class="col-md-6 col-sm-6">
                           <!-- text input -->
                           <div class="form-group" style="margin-top:8px;">
                               <label>Type Note</label>
                               <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                               <select class="form-control input-height" id="notetype" name="notetype" style="width:100%;" onchange="searchDesignation()">
                                   <option value="">Selectionner un type de Note</option>

                                     <option <?php if(isset($_POST['search'])&&($_POST['notetype']==1)){echo "selected";} ?> value="1">CONTROLE</option>
                                     <option <?php if(isset($_POST['search'])&&($_POST['notetype']==2)){echo "selected";} ?> value="2">EXAMEN</option>


                               </select>
                           </div>




                       </div>

                   <div class="col-md-6 col-sm-6">
                   <!-- text input -->
                   <div class="form-group" style="margin-top:8px;">
                       <label>Désignation</label>
                       <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                       <select class="form-control input-height" id="libctrl" name="libctrl" style="width:100%;">
                           <option value="">Selectionner une désignation</option>

                       </select>
                       <input type="hidden" id="eleveid" name="eleveid" class="form-control" value="<?php echo $studentselect; ?>">
                   </div>


               </div>


                       <div class="col-md-3 col-sm-3">
                       <!-- text input -->
                       <!--div class="form-group">
                           <label style="margin-top:3px;">Date</label>
                           <input type="text" id="datepre" name ="datepre" class="floating-label mdl-textfield__input"  placeholder="Date présence">
                           <input type="hidden" name="search" id="search" />
                       </div-->
                       <input type="hidden" name="search" id="search" />
                       <button type="submit" class="btn btn-success"  style="width:200px;height:35px;margin-top:35px;text-align:center;">Afficher les Notes</button>


                   </div>


                         </div>


                     </form>
                   </div>
               </div>
                        </div>

          </div>
<!-- element à faire apparaitre au clique du bouton rechercher -->
<div class="row" style="" id="affichage">
  <?php
      if(isset($_POST['search']))
      {
          if(isset($_POST['notetype'])&&isset($_POST['classeEtab'])&&isset($_POST['libctrl'])&&isset($_POST['eleveid']))
          {
              //nous devons recupérer la liste des elèves de cette classe ainsi que leur note

              // $students=$student->getAllstudentofthisclasses($_POST['classeEtab']);

              // $students=$student->getAllstudentofthisclassesSession($_POST['classeEtab'],$libellesessionencours);
              $students=$student->getAllstudentofthisclassesSessionUnique($_POST['classeEtab'],$libellesessionencours,$_POST['eleveid']);

              // echo $_POST['notetype'];

              // if($_POST['notetype']==1)
              // {
              //   //cas d'un controle
              // $data=explode("-",$_POST['libctrl']);
              // $eleveid=$_POST['eleveid'];
              // $controleid=$data[0];
              // $matiereid=$data[1];
              // $teatcherid=$data[2];
              //   $studentsX=$student->getAllstudentNotesofthisclassespecifiq($_POST['classeEtab'],$matiereid,$controleid,$teatcherid,$codeEtabAssigner,$eleveid);
              // }else if($_POST['notetype']==2){
              //   //cas d'un examen
              //   $eleveid=$_POST['eleveid'];
              //   $data=explode("-",$_POST['libctrl']);
              //   $examid=$data[0];
              //   $matiereid=$data[1];
              //   $teatcherid=$data[2];
              //   //$examid=$_POST['libctrl'];
              // $studentsX=$student->getAllstudentNotesExamofthisclassespecifiq($_POST['classeEtab'],$examid,$codeEtabAssigner,$eleveid);
              // }


              $classeInfos=$classe->getInfosofclassesbyId($_POST['classeEtab'],$libellesessionencours);

              // var_dump($studentsX);

          }
          ?>

          <div class="offset-md-4 col-md-4"  id="affichage1">
            <div class="card" style="">
            <div class="card-body">
              <h5 class="card-title"></h5>
              <h4 style="font-style: italic;font-weight: bold;text-align:center;font-size:font-size: large;">Note de Classe</h4>
              <p class="card-text" style="text-align:center;font-weight: bold;"><?php echo $classeInfos; ?></p>
              <p class="card-text" style="text-align:center;"></p>

            </div>
          </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="affichage2">
                                  <div class="card card-topline-green">
                                      <div class="card-head">
                                          <header>Récapitulatif des notes   <?php //echo $classeInfos; ?></header>
                                          <div class="tools">
                                              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                      <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                      <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                          </div>
                                      </div>
                                      <div class="card-body ">

                                        <form method="post" action="#" id="FormAttendance">
                                  <table class="table table-striped table-bordered table-hover table-checkable order-column full-width"   id="affichage3">
                                      <thead>
                                          <tr>
                                            <th>
                                                    <label class="rt-chkbox rt-chkbox-single rt-chkbox-outline">
                                                        <input type="checkbox" class="group-checkable" value="1" id="allcheckbox" name="allcheckbox" checked onclick="checkboxclick()" />
                                                        <span></span>
                                                    </label>
                                                </th>
                                            <!--th style="width:15%"> Matricule </th-->
                                            <th style="width:50%"> Nom & Prénoms </th>
                                            <th style="width:35%;text-align:center"> Note </th>
                                            <th style="text-align:center"> Observation </th>


                                          </tr>
                                      </thead>
                                      <tbody>
                                        <?php
                                        $matricules="";
                                        $j=0;
                                        $i=1;

                                        foreach ($students as $value):

                                          $matricules=$matricules."*".$value->matricule_eleve;
                                        ?>
                                        <tr class="odd gradeX">
                                            <!--td>
                                              <?php //echo $i;?>
                                            </td-->
                                            <td>
                                                      <label class="rt-chkbox rt-chkbox-single rt-chkbox-outline">
                                                          <input type="checkbox" class="checkboxes" id="checkbox<?php echo $value->matricule_eleve;?>" name="checkbox<?php echo $value->matricule_eleve;?>" value="1" checked onclick="studentcheckbox('<?php echo $value->matricule_eleve;  ?>')" />
                                                          <span></span>
                                                      </label>
                                                  </td>
                                            <td style="text-align:center">
                                                <a href="#"><?php echo $value->nom_eleve." ".$value->prenom_eleve;?> </a>
                                            </td>
                                            <td style="text-align:center">
                                              <?php
                                              //nous allons voir si l'eleve à une note dans cette matiere

                                              if($_POST['notetype']==1)
                                              {
                                                //cas d'un controle
                                              $data=explode("-",$_POST['libctrl']);
                                              $controleid=$data[0];
                                              $matiereid=$data[1];
                                              $teatcherid=$data[2];
                                                // $studentsX=$student->getAllstudentNotesofthisclasses($_POST['classeEtab'],$matiereid,$controleid,$teatcherid,$codeEtabAssigner);

                                                //determiner le nombre de note pour ce controle

                                                $nbnotes=$student->DetermineNoteNumbercontroles($value->idcompte_eleve,$_POST['classeEtab'],$matiereid,$controleid,$codeEtabAssigner);
                                                $datastudentsx=$student->getNotescontroleinformations($value->idcompte_eleve,$_POST['classeEtab'],$matiereid,$controleid,$codeEtabAssigner);
                                              }else if($_POST['notetype']==2){
                                                //cas d'un examen
                                                $data=explode("-",$_POST['libctrl']);
                                                $examid=$data[0];
                                                $matiereid=$data[1];
                                                $teatcherid=$data[2];

                                                //$examid=$_POST['libctrl'];
                                              // $studentsX=$student->getAllstudentNotesExamofthisclasses($_POST['classeEtab'],$examid,$codeEtabAssigner);

                                              //determiner le nombre de note pour ce controle

                                              $nbnotes=$student->DetermineNoteNumberexamens($value->idcompte_eleve,$_POST['classeEtab'],$matiereid,$examid,$codeEtabAssigner);
                                              $datastudentsx=$student->getNotesexameninformations($value->idcompte_eleve,$_POST['classeEtab'],$matiereid,$examid,$codeEtabAssigner);
                                              }


                                               ?>

                                               <?php
                                                if($nbnotes==0)
                                                {
                                                  ?>
  <input style='text-align:center' type="number" min=0 max=20 name="noteE<?php echo $value->idcompte_eleve;?>" id="noteE<?php echo $value->idcompte_eleve;?>" value="<?php echo $nbnotes; ?>" style="width:100px" onclick="erasedNote(<?php echo $value->idcompte_eleve;?>)" readonly/>
                                                  <?php

                                                }else if($nbnotes>0)
                                                {
      //recuperer la note et l'observation
      $array=json_encode($datastudentsx,true);
      $someArray = json_decode($array, true);

                                                ?>
    <input style='text-align:center' type="number" min=0 max=20 name="noteE<?php echo $value->idcompte_eleve;?>" id="noteE<?php echo $value->idcompte_eleve;?>" value="<?php echo $someArray[0]["valeur_notes"]; ?>" style="width:100px" onclick="erasedNote(<?php echo $value->idcompte_eleve;?>)" readonly/>

                                                <?php
                                                }
                                                ?>


                                                <p id="messageNoteE<?php echo $value->idcompte_eleve;?>"></p>
                                            </td>
                                            <td>
                                              <?php
                                              if($nbnotes==0)
                                              {
                                                ?>
  <textarea  name="obserE<?php echo $value->idcompte_eleve;?>" id="obserE<?php echo $value->idcompte_eleve;?>" rows="3" cols="50" onclick="erasedObserv(<?php echo $value->idcompte_eleve;?>)" readonly></textarea>
                                                <?php
                                              }else
                                              {
                                                ?>
  <textarea  name="obserE<?php echo $value->idcompte_eleve;?>" id="obserE<?php echo $value->idcompte_eleve;?>" rows="3" cols="50" onclick="erasedObserv(<?php echo $value->idcompte_eleve;?>)" readonly><?php echo $someArray[0]["obser_notes"]; ?></textarea>
                                                <?php
                                              }
                                               ?>


                                                  <p id="messageObservE<?php echo $value->idcompte_eleve;?>"></p>
                                            </td>
                                            <!-- td>
                                              <a href="updatenotes.php?noteid=<?php //echo $value->id_notes ?>&tyepenote=<?php //echo $value->idcompte_eleve;?>&eleveid=<?php //echo $value->type_notes;?>"  class="btn btn-warning btn-xs">
                                                <i class="material-icons">edit</i>
                                              </a>
                                               <!-- <a href="#"  class="btn btn-warning btn-xs" data-toggle="modal" data-target="#largeModel">
                                                <i class="material-icons">edit</i>
                                              </a> -->

                                            <!-- </td-->

          <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
          <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
          <!-- <script src="../assets2/plugins/select2/js/select2.js" ></script>
          <script src="../assets2/js/pages/select2/select2-init.js" ></script> -->
          <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
          <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
          <script type="text/javascript">

          function check(id)
          {

            var note=$("#note"+id).val();
            var obser=$("#obserE"+id).val();

              if(note==""||obser=="")
              {
                  if(note=="")
                  {
  document.getElementById("messageCont"+id).innerHTML = "<font color=\"red\">Merci de renseigner la Note SVP !</font>";
                  }

                  if(obser=="")
                  {
document.getElementById("messageObservE"+id).innerHTML = "<font color=\"red\">Merci de renseigner l'observation SVP !</font>";
                  }
                  //event.preventDefault();
              }else {
                //document.location.href="../controller/notes.php?etape=3";
                //authorise();
                alert("bonjour");
                $("#UpdateNote"+id).submit();
              }




          }

          function erasedNote(id)
          {
            document.getElementById("messageCont"+id).innerHTML = "";
          }

          function erasedObser(id)
          {
            document.getElementById("messageObservE"+id).innerHTML = "";
          }
          function authorise()
          {
            $("#UpdateNote<?php echo $someArray[0]["valeur_notes"];?>").submit();
          }

          </script>

                                        </tr>

                                          <?php
                                             $i++;
                                             $j++;
                                                 endforeach;
                                               ?>



                                      </tbody>
                                  </table>

                                  <?php
                                  //echo $matricules;
                                  $tabMat=explode("*",$matricules);
                                  $nb=count($tabMat);



                                  ?>
                                  <input type="hidden" name="studentmat" id="studentmat" value="<?php echo $matricules;?>"/>
                                  <input type="hidden" name="retirestudentmat" id="retirestudentmat" value="">
                                  <input type="hidden" name="studentmataafiificher" id="studentmataafiificher" value="<?php echo $matricules;?>">
                                  <input type="hidden" name="etape" id="etape" value="1"/>
                                  <input type="hidden" name="nbstudent" id="nbstudent" value="<?php echo $nb;?>"/>
                                  <input type="hidden" name="allpresent" id="allpresent" value=""/>
                                  <input type="hidden" name="classeId" id="classeId" value="<?php echo $_POST['classeEtab']?>"/>
                                  <input type="hidden" name="datePresence" id="datePresence" value="<?php echo $_POST['datepre']?>"/>
                                  <center>
                                    <button type="button"  class="btn btn-primary btn-xs"> <i class="material-icons f-left">print</i> Imprimer </button>
                                    <?php
                                    if($_POST['notetype']==1)
                                    {
                                      ?>
  <!--a href="updatenoteclasseseleves.php?classe=<?php  //echo $_POST['classeEtab'] ?>&controle=<?php //echo $controleid ?>&matiere=<?php //echo $matiereid ?>&teatcher=<?php //echo $teatcherid ?>"  class="btn btn-warning btn-xs"> <i class="material-icons f-left">edit</i> Modifier </a-->
  <a href="#" onclick="updatecontroleeleves('<?php echo $matricules;?>',<?php  echo $_POST['classeEtab'] ?>,<?php echo $controleid ?>,<?php echo $matiereid ?>,<?php echo $teatcherid ?>)"  class="btn btn-warning btn-xs"> <i class="material-icons f-left">edit</i> Modifier </a>
                                      <?php
                                    }else if($_POST['notetype']==2)
                                    {
                                      ?>
                                        <!--a href="updatenoteclasseseleves.php?classe=<?php  //echo $_POST['classeEtab'] ?>&examen=<?php //echo $examid ?>&matiere=<?php //echo $matiereid ?>&teatcher=<?php //echo $teatcherid ?>"  class="btn btn-warning btn-xs"> <i class="material-icons f-left">edit</i> Modifier </a-->
  <a href="#"  onclick="updateexameneleves('<?php echo $matricules;?>',<?php  echo $_POST['classeEtab'] ?>,<?php echo $examid ?>,<?php echo $matiereid ?>,<?php echo $teatcherid ?>)" class="btn btn-warning btn-xs"> <i class="material-icons f-left">edit</i> Modifier </a>
                                      <?php

                                    }

                                     ?>

                                  </center>
                                </form>

                              </div>
                                  </div>
                              </div>


          <?php
      }
   ?>



          </div>

<!-- fin affichage du bouton rechercher -->

                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
      <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
<?php
  if(isset($_POST))
  {
    ?>
    function touspresent(idclasse)
    {

       var nb="<?php echo @$nb;?>";
       var matricules="<?php echo @$matricules?>";

       var array=matricules.split('*');

       for(i=1;i<nb;i++)
       {
         //alert(array[i]);
         $("statut"+array[i]).val(1);
         //$("P"+array[i]).prop("checked", true);
         document.getElementById("P"+array[i]).checked = true;

       }
       $("#allpresent").val(1);

    }

    function updatecontroleeleves(matricules,classe,controleid,matiereid,teatcherid )
    {
      // alert(matricules);

      var etape=3;
      //determiner la les ids des eleves

      $.ajax({

        url: '../ajax/designation.php',
        type: 'POST',
        async:true,
         data: 'datas=' +matricules+ '&etape=' + etape,
         dataType: 'text',
         success: function (content, statut) {

             // var lien="updatenoteclasses.php?classe="+classe+"&controle="+controleid+"&matiere="+matiereid+"&teatcher="+teatcherid+"&matriculedelete="+content;
             var lien="updatenoteclasseseleves.php?classe="+classe+"&controle="+controleid+"&matiere="+matiereid+"&teatcher="+teatcherid+"&matriculedelete="+content;
             document.location.href=lien;
         }

      });
    }

    function updateexameneleves(matricules,classe,examid,matiereid,teatcherid)
    {
      // alert(matricules);

      var etape=3;
      //determiner la les ids des eleves

      $.ajax({

        url: '../ajax/designation.php',
        type: 'POST',
        async:true,
         data: 'datas=' +matricules+ '&etape=' + etape,
         dataType: 'text',
         success: function (content, statut) {

             // var lien="updatenoteclasses.php?classe="+classe+"&controle="+controleid+"&matiere="+matiereid+"&teatcher="+teatcherid+"&matriculedelete="+content;
             var lien="updatenoteclasseseleves.php?classe="+classe+"&examid="+examid+"&matiere="+matiereid+"&teatcher="+teatcherid+"&matriculedelete="+content;
             document.location.href=lien;
         }

      });
    }

    function tousabsent(idclasse)
    {
      var nb="<?php echo @$nb;?>";
      var matricules="<?php echo @$matricules?>";

      var array=matricules.split('*');

      for(i=1;i<nb;i++)
      {
        $("statut"+array[i]).val(0);
        document.getElementById("A"+array[i]).checked = true;

      }
      $("#allpresent").val(0);
    }
<?php
  }
 ?>


   function present(id)
   {
    $("statut"+id).val(1);
   }

   function absent(id)
   {
    $("statut"+id).val(0);
   }

   $('#example5').DataTable( {
       "scrollX": true

   } );

   $("#notetype").select2();
   $("#libctrl").select2();
   $("#matclasse").select2();


   $('#datepre').bootstrapMaterialDatePicker
   ({
     date: true,
     time: false,
     format: 'DD/MM/YYYY',
     lang: 'fr',
     minDate : new Date(),
    cancelText: 'Annuler',
    okText: 'OK',
    clearText: 'Effacer',
    nowText: 'Maintenant'

   });

   function searchDesignation()
   {
     var classeEtab="<?php echo $classeselect;  ?>";
     var codeEtab="<?php echo $codeEtabAssigner;  ?>";
     var matiere=$("#matclasse").val();
     var typenote=$("#notetype").val();
     var etape=1;
     //nous allons verifier si nous avons des notes pour ce controle ou examen
     $.ajax({

       url: '../ajax/designation.php',
       type: 'POST',
       async:true,
        data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab+'&matiere='+matiere+'&typenote='+typenote,
        dataType: 'text',
        success: function (content, statut) {

          $("#libctrl").html("");
          $("#libctrl").html(content);

        }

     });

   }

   function searchmatiere()
   {
     var classeEtab="<?php echo $classeselect;  ?>";
     var codeEtab="<?php echo $codeEtabAssigner;  ?>";
     var session ="<?php echo $libellesessionencours;  ?>";
     var etape=2;
     var etape1=3;

     $.ajax({

       url: '../ajax/matiere.php',
       type: 'POST',
       async:true,
        data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab,
        dataType: 'text',
        success: function (content, statut) {

          $("#matclasse").html("");
          $("#matclasse").html(content);

          //nous allons chercher la liste des eleves ce cette classe

          // $.ajax({
          //
          //   url: '../ajax/admission.php',
          //   type: 'POST',
          //   async:true,
          //    data:'etape=' + etape1+'&classe='+classeEtab+'&codeEtab='+codeEtab+'&session='+session,
          //    dataType: 'text',
          //    success: function (content, statut) {
          //
          //
          //      $("#eleveid").html("");
          //      $("#eleveid").html(content);
          //
          //      //nous allons chercher la liste des eleves ce cette classe
          //
          //
          //
          //    }
          //
          // });


        }

     });

   }

function searchNotesClasses()
{
  var notetype=$("#notetype").val();
  var classeEtab="<?php echo $classeselect;  ?>";
  var codeEtab="<?php echo $codeEtabAssigner;  ?>";

  //nous allons rechercher la liste des designation de notes par classe

    if(notetype==""||classeEtab=="")
    {
      if(notetype=="")
      {
        Swal.fire({
        type: 'warning',
        title: 'Attention',
        text: "Merci de selectionner le type de note",

      })
      }

      if(classeEtab=="")
      {
        Swal.fire({
        type: 'warning',
        title: 'Attention',
        text: "Merci de selectionner une classe",

      })
      }
    }else {

        if(notetype==1)
        {
          var etape=1;
        }else if(notetype==2){
          var etape=2;
        }

      $.ajax({

        url: '../ajax/notesSearch.php',
        type: 'POST',
        async:true,
         data: 'notetype=' + notetype+ '&etape=' + etape+'&classeEtab='+classeEtab+'&codeEtab='+codeEtab,
         dataType: 'text',
         success: function (content, statut) {

           $("#libctrl").html("");
           $("#libctrl").html(content);

         }

      });
    }



}

   $(document).ready(function() {

    searchmatiere();

$("#FormNotes").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
 },
 highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
 },
 success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    classeEtab:"required",
    datepre:"required",
    notetype:"required",
    classeEtab:"required",
    libctrl:"required",
    matclasse:"required",
    eleveid:"required"



  },
  messages: {
    classeEtab:"Merci de selectionner la classe",
    datepre:"Merci de choisir la date de présence",
    notetype:"Merci de selectionner le type de note",
    classeEtab:"Merci de selectionner une classe",
    libctrl:"Merci de selectionner une désignation",
    matclasse:"Merci de selectionner une matière",
    eleveid:"Merci de selectionner un eleve"

  },
  submitHandler: function(form) {
    form.submit();
  }
});


$("#FormAttendance").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
  },
  highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
  },
  success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    classeEtab:"required",
    datepre:"required"



  },
  messages: {
    classeEtab:"Merci de selectionner la classe",
    datepre:"Merci de choisir la date de présence"

  },
  submitHandler: function(form) {
    //form.submit();
    //classeId
//datePresence
    var etape=1;


    $.ajax({

      url: '../ajax/attendance.php',
      type: 'POST',
      async:false,
      data: 'classe=' + $("#classeId").val()+'&datepre='+$("#datePresence").val()+'&etape='+etape,
      dataType: 'text',
      success: function (content, statut)
      {
          if(content==0)
          {
            form.submit();

          }else if(content==1) {
            Swal.fire({
            type: 'warning',
            title: 'Attention',
            text: "La présence de cette classe existe dejà dans le système pour cette date",

          })
        }
      }

    });
  }


});


   });

   </script>
    <!-- end js include path -->
  </body>

</html>
