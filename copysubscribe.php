<?php
session_start();

require_once('single/Etablissement.php');
// require_once('class/LocalAdmin.php');
// require_once('class/Parent.php');
// require_once('class/Classe.php');
// require_once('class/Teatcher.php');
// require_once('class/Sessionsacade.php');

// $session= new Sessionacade();
//
// $emailUti=$_SESSION['user']['email'];
// $classe=new Classe();
// $user=new User();
$etabs=new Etab();
// $teatcher=new Teatcher();
// $localadmins= new Localadmin();
// $parents=new ParentX();



//le nombre des eleves de cet etablissement



$alletab=$etabs->getAllEtab();
$pays=$etabs->getAllCountriesOfSystem();
$alletab=$etabs->getAllEtab();
// $locals=$localadmins->getAllAdminLocal();
// $allparents=$parents->getAllParent();




 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- Material Design Lite CSS -->
    <link rel="stylesheet" href="assets2/css/pages/steps.css">
	<link href="assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="assets2/css/material_style.css" rel="stylesheet">

	<!-- Theme Styles -->
    <link href="assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
<div class="page-wrapper">
  <div class="container">
    <div class="row" style="margin-top:15px;" id="programmepdf">

      <div class="col-sm-12 col-md-12">
                             <div class="card-box">
                                 <div class="card-head">
                                     <header>Formulaire d'abonnement aux Notifications XSCHOOL</header>
                                 </div>
                                 <div class="card-body ">
                                   <form class="" action="#" method="post" id="subscribeform">

                                     <h3>Enregistrement Parent</h3>
                                     <fieldset>
 									        <div class=" col-md-12 panel-headin" style="background-color:#009fe3; font-size:20px;color: white;text-align:center;"> Informations Personnelles du parent ou du tuteur de l'élève  </div>
                        <br>
                        <div class="row">
                                            <div class="col-md-6">
                        <div class="form-group">
                        <label for=""><b><?php echo L::Name?><span class="required"> * </span>: </b></label>

                      <input type="text" name="nomTea" id="nomTea" data-required="1" placeholder="Entrer le Nom" class="form-control input-height " />
                        </div>

                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                               <label for=""><b><?php echo L::PreName?> : </b></label>
                                <input type="text" name="prenomTea" id="prenomTea" data-required="1" placeholder="Entrer le prénoms" class="form-control input-height" />
                              </div>


                         </div>

                                      </div>
                                      <div class="row">
                                                          <div class="col-md-6">
                                      <div class="form-group">
                                      <label for=""><b>Pays de résidence <span class="required"> * </span>: </b></label>

                                      <select class="form-control input-height" name="pays" id="pays" style="width:100%;" >
                                          <option value="">Selectionner une Pays</option>
                                          <?php
                                          $i=1;
                                            foreach ($pays as $value):
                                            ?>
                                            <option value="<?php echo utf8_encode(utf8_decode($value->id_pays)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_pays)); ?></option>

                                            <?php
                                                                             $i++;
                                                                             endforeach;
                                                                             ?>

                                      </select>
                                      </div>

                                      </div>

                                      <div class="col-md-6">
                  <div class="form-group">
                  <label for=""><b>Téléphone  <span class="required"> * </span>: </b></label>

                  <input type="text" name="telTea" id="telTea" data-required="1" placeholder="Téléphone" class="form-control input-height" />
                  </div>

                  </div>


                                                    </div>
                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                    <div class="form-group">
                                                    <label for=""><b>Date de naissance<span class="required"> * </span>: </b></label>

                                                    <input type="date" placeholder="Entrer la date de naissance" name="datenaisTea" id="datenaisTea" class="form-control input-height ">

                                                    </div>

                                                    </div>
                                                    <div class="col-md-6">
                                                         <div class="form-group">
                                                           <label for=""><b>Fonction: </b></label>
                                                            <input type="text" name="fonctionTea" id="fonctionTea" data-required="1" placeholder="Fonction" class="form-control input-height" />
                                                          </div>


                                                     </div>

                                                                  </div>
                                                                  <div class="row">
                                                                                      <div class="col-md-6">
                                                                  <div class="form-group">
                                                                  <label for=""><b>Genre <span class="required"> * </span>: </b></label>

                                                                  <select class="form-control input-height" name="sexeTea" id="sexeTea">
                                                                      <option selected value="">Selectionner le genre</option>
                                                                      <option value="M">Masculin</option>
                                                                      <option value="F">Feminin</option>
                                                                  </select>
                                                                  </div>

                                                                  </div>
                                                                  <div class="col-md-6">
                                                                    <div class="form-group">
                                                                    <label for=""><b>Email <span class="required"> * </span>: </b></label>

                                                                  <input type="email" class="form-control input-height " name="emailTea" id="emailTea" placeholder="Entrer l'adresse email">
                                                                    </div>


                                                                   </div>

                                                                                </div>
                                                                                <div class=" col-md-12 panel-headin" style="background-color:#009fe3; font-size:20px;color: white;text-align:center;"> Informations de compte  </div>
                                                                                <br/>


                                                                                    <div class="form-group row">
                                                                                        <label class="control-label col-md-3"><?php echo L::Logincnx?>
                                                                                            <span class="required">*  </span>
                                                                                        </label>
                                                                                        <div class="col-md-5">
                                                                                            <input name="loginTea" id="loginTea" type="text" placeholder="Entrer le Login " class="form-control input-height" />

                                                                                           </div>
                                                                                    </div>
                                                                                    <div class="form-group row">
                                                                                        <label class="control-label col-md-3"><?php echo L::Passcnx?>
                                                                                            <span class="required">*  </span>
                                                                                        </label>
                                                                                        <div class="col-md-5">
                                                                                            <input name="passTea" id="passTea" type="password" placeholder="Entrer le Mot de passe " class="form-control input-height"  /> </div>
                                                                                    </div>
                                                                                    <div class="form-group row">
                                                                                        <label class="control-label col-md-3"><?php echo L::ConfirmPasscnx?>
                                                                                            <span class="required">*  </span>
                                                                                        </label>
                                                                                        <div class="col-md-5">
                                                                                            <input name="confirmTea" id="confirmTea" type="password" placeholder="Confirmer le Mot de passe " class="form-control input-height" /> </div>
                                                                                            <input type="hidden" name="etape" id="etape" value="1"/>

                                                                                    </div>
                                     </fieldset>
                                        <h3>Choix Elève</h3>
                                        <fieldset>
                                          <div class=" col-md-12 panel-headin" style="background-color:#009fe3; font-size:20px;color: white;text-align:center;"> Informations Elève  </div>
                                          <br/>
                                          <div class="row">
                                                              <div class="col-md-6">
                                          <div class="form-group">
                                          <label for=""><b>Matricule<span class="required"> * </span>: </b></label>

                                        <input type="text"  name="matricule" id="matricule" data-required="1" placeholder="Matricule" class="form-control input-height" />
                                          </div>

                                          </div>
                                          <div class="col-md-6">
                                               <div class="form-group">
                                                 <label for=""><b>Etablissement : </b></label>

                                              <!--select class="form-control input-height" id="etablissement" name="etablissement" style="width:100%" onchange="determinatestudent()"-->
                                                <select class="form-control input-height" id="etablissement" name="etablissement" style="width:100%" >

                                                  <option>Selectionner un etablissement</option>
                                                  <?php
                                                  $i=1;
                                                    foreach ($alletab as $value):
                                                    ?>
                                                    <option value="<?php echo utf8_encode(utf8_decode($value->code_etab)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_etab)); ?></option>

                                                    <?php
                                                                                     $i++;
                                                                                     endforeach;
                                                                                     ?>

                                              </select>
                                                </div>


                                           </div>

                                                        </div>
                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                        <div class="form-group">
                                                        <label for=""><b>Nom & Prénoms<span class="required"> * </span>: </b></label>

                                                      <input type="text"  name="nomcomplet" id="nomcomplet" data-required="1" placeholder="" class="form-control input-height" readonly/>
                                                        </div>

                                                        </div>
                                                        <div class="col-md-6">
                                                             <div class="form-group">
                                                               <label for=""><b>Classe<span class="required"> * </span>: </b></label>
                                                            <input type="text"  name="classeTea" id="classeTea" data-required="1" placeholder="" class="form-control input-height" readonly/>
                                                              </div>


                                                         </div>
                                                         <div class="col-md-4">
                                                           <!--button id="addbutton"  class="btn btn-md btn-warning" data-toggle="modal" data-target="#largeModel"><i class="fa fa-plus"></i> AJOUTER ENFANT </button-->
                                                           <a  id="addbutton" class="btn btn btn-warning btn-sm " data-toggle="modal" data-target="#largeModel" data-animation="zoomInUp">

      <span class="fa fa-plus-circle"></span>
			AJOUTER ENFANT

</a>

<div class="modal fade" id="largeModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel">Selectionner vos enfants</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                      <div id="divrecherchestation">
                        <p>  Rechercher</p>

                        <form class="form-inline">

                          <div class="form-group">

                            <div class="row">

                              <div class="col-lg-4">
                                <input type="text"  name="matriculecountry" id="matriculecountry" data-required="1" placeholder="Matricule" class="form-control" />
                              </div>
                              <div class="col-lg-6">
                                <select class="form-control " id="etablissementcontry" name="etablissementcountry" style="width:100%" >

                                  <option>Selectionner un etablissement</option>
                                  <?php
                                  $i=1;
                                    foreach ($alletab as $value):
                                    ?>
                                    <option value="<?php echo utf8_encode(utf8_decode($value->code_etab)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_etab)); ?></option>

                                    <?php
                                                                     $i++;
                                                                     endforeach;
                                                                     ?>

                                </select>
                              </div>
                              <div class="col-lg-2">
                                <button type="button" id="btnRechercherStation" class="btn btn-md btn-danger" style="margin-left:-5px;">
                                 <span class="fa fa-search"></span>
                               </button>
                              </div>



                            </div>



                          </div>
                        </form>
                         <br/>
                         <table class="table table-striped">
                     <thead style="background-color:#009fe3; color:white; font-weight: bold;">
                     <tr>
                     <th style="">Nom </th>
                     <th class="visible-lg">Prénoms  </th>
                     <th> Classe</th>
                     <th style="">Etablissemnt</th>
                   </tr>
                    </thead>
                    <tbody id="tabStationBody">

                   <tr>

                     <td colspan="4">Aucun Elève </td>

                   </tr>

                                     </tbody>
                                      </table>
                    <div id="divafficherinfosstation" class="">

                      <div class="panel panel-default">
                        <div class="panel-heading">Informations elève</div>


                      </div>

                    </div>


                      </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>

                                                         </div>

                                                       </div> <br>
                                                                      <div class="row">




                              					                        <div class="col-md-12">
                              					                            <div class="card  card-box">
                              					                                <div class="card-head">
                              					                                    <header></header>
                              					                                    <div class="tools">
                              					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                              						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                              						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                              					                                    </div>
                              					                                </div>
                              					                                <div class="card-body ">

                              					                                  <div class="table-scrollable">
                              					                                    <table class="table table-hover table-checkable order-column full-width" id="example4">
                              					                                        <thead>
                              					                                            <tr>

                                                                                        <th>Matricule</th>
                              					                                                <th>Nom & prénoms </th>
                                                                                        <th>Etablissement </th>
                                                                                        <th>Classe </th>
                                                                                         <th> <?php echo L::Actions?> </th>
                              					                                            </tr>
                              					                                        </thead>
                              					                                        <tbody>

                              															                    </tbody>
                              					                                    </table>
                              					                                    </div>
                              					                                </div>
                              					                            </div>
                              					                        </div>
                              					                    </div>
                                        </fieldset>
                                        <h3>Choix abonnement</h3>
                                        <fieldset>
ssssssss
                                        </fieldset>

                                   </form>

                                 </div>
                             </div>
                         </div>

    </div>
  </div>

</div>

    <!-- start js include path -->
    <script src="assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="assets2/plugins/popper/popper.min.js" ></script>
     <script src="assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
  <script src="assets2/plugins/jquery-validation/js/jquery.validate.min.js"></script>

     <!-- bootstrap -->
     <script src="assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- wizard -->
        <script src="assets2/plugins/steps/jquery.steps.js" ></script>
        <script src="assets2/js/pages/steps/steps-data.js" ></script>

 	<script src="assets2/js/app.js" ></script>
     <script src="assets2/js/layout.js" ></script>
 	<script src="assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="assets/js/sweetalert2.min.js"></script>
  <script type="text/javascript" src="assets/js/libs/form-validator/jquery.validate.min.js"></script>


    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
$("#addbutton").disabled =true;
   function determinatestudent(){

    var matricule=$("#matricule").val();
    var codeEtab=$("#etablissement").val();
    var etape=1;

   $.ajax({

         url: 'ajax/determination.php',
         type: 'POST',
         async:true,
          data: 'codeEtab='+codeEtab+'&etape='+etape+'&matricule='+matricule,
         dataType: 'text',
         success: function (response, statut) {

          if(response==0)
        {
          //cet eleve n'existe pas ou n'est pas inscrit pour la session encours

          Swal.fire({
          type: 'warning',
          title: 'Attention',
          text: "cet eleve n'existe pas ou n'est pas inscrit pour la session encours",

          })
        }else if(response!=0) {
          var tabdata=response.split("*");
          //nous pouvons recuperer de facon individuelle les variables nom,prenoms et classe

          var nom=tabdata[0];
          var prenom=tabdata[1];
          var classe=tabdata[2];




     $("#nomcomplet").val(nom+" "+prenom);

     //$("#prenom").html(prenom);

     $("#classeTea").val(classe);

     $("#addbutton").prop('disabled', false);

        }

         }
       });
   }

   $(document).ready(function() {
     "use strict";
     var wizard = $("#wizard_test").steps();

     var form = $("#subscribeform").show();

     form.validate({
       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
     },
     highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
     },
     success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },
       rules:{
         // nomTea:"required",
         // prenomTea:"required",
         //  pays:"required",
         //  telTea:"required",
         //  datenaisTea:"required",
         //  fonctionTea:"required",
         //  sexeTea:"required",
         //  emailTea:"required",
         //  loginTea:"required",
         //  passTea: {
         //      required: true,
         //      minlength: 6
         //  },
         //  confirmTea:{
         //      required: true,
         //      minlength: 6,
         //      equalTo:'#passTea'
         //  }
       },
       messages:{
         nomTea:"Merci de renseigner le nom",
         prenomTea:"Merci de renseigner le prénoms",
          pays:"Merci de selectionner le pays",
          telTea:"Merci de re renseigner le numéro de telphone",
          datenaisTea:"Merci de renseigner la date de naissance",
          fonctionTea:"Merci de renseigner la fonction",
          sexeTea:"Merci de selectionner le genre",
          emailTea: {
                     required:"Merci de renseigner l'adresse email",
                     email:"Merci de renseigner une adresse email valide"
                 },
          loginTea:"Merci de renseigner le Login",
          confirmTea:{
              required:"<?php echo L::Confirmcheck?>",
              minlength:"<?php echo L::Confirmincheck?>",
              equalTo: "<?php echo L::ConfirmSamecheck?>"
          },
          passTea: {
              required:"<?php echo L::Passcheck?>",
              minlength:"<?php echo L::Confirmincheck?>"
          }
       }
     });

     form.steps({
         headerTag: "h3",
         bodyTag: "fieldset",
         transitionEffect: "slideLeft",
         onStepChanging: function (event, currentIndex, newIndex)
          {
                      // Allways allow previous action even if the current form is not valid!
               if (currentIndex > newIndex)
               {
                   return true;
               }

               // Forbid next action on "Warning" step if the user is to young
               if (newIndex === 3 && Number($("#age-2").val()) < 18)
               {
                   return false;
               }
               // Needed in some cases if the user went back (clean up)
               if (currentIndex < newIndex)
               {
                   // To remove error styles
                   form.find(".body:eq(" + newIndex + ") label.error").remove();
                   form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
               }
               form.validate().settings.ignore = ":disabled,:hidden";
               return form.valid();


          }


     });




   });

   </script>
    <!-- end js include path -->
  </body>

</html>
