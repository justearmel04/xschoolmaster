<?php
session_start();
require_once('intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('langcache');
$i18n->setFilePath('intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
 ?>
<!DOCTYPE html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title> Application de communication ecoles et parents </title>

<meta name="description" content=" Xschool'ing est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle    t  con ue pour r pondre   des probl mes que nous observons et pour suivre de pr s l' volution de nos enfants, alors commen ons maintenant.">

<meta name="keywords" content=" Application du domaine éducatif, Application de communication parents- établissments , Application mobile  et Desktop pour Etablissement">

<meta name="author" content="Xschool'ing | xschool'ing.com">

 <meta property="og:url" content="" />
 <meta property="og:title" content="Xschool'ing - Application de communication ecoles et parents" />
 <meta property="og:description" content="Application du domaine éducatif, Application de communication parents établissment , Application android et Desktop pour Etablissement." />

<!-- Mobile Specific -->

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<!-- CSS -->
<link href="" type="image/x-icon" rel="icon" /><link href="" type="image/x-icon" rel="shortcut icon" />
	<!--link rel="stylesheet" type="text/css" href="xchoolink/css/v2/home/component43a0.css?v3" />
	<link rel="stylesheet" type="text/css" href="xchoolink/css/v2/home/bootstrap.min1b26.css?v2" />
	<link rel="stylesheet" type="text/css" href="xchoolink/css/v2/home/animate1b26.css?v2" />
	<link rel="stylesheet" type="text/css" href="xchoolink/css/v2/home/simple-line-icons1b26.css?v2" />
	<link rel="stylesheet" type="text/css" href="xchoolink/css/v2/home/icomoon-soc-icons1b26.css?v2" />
	<link rel="stylesheet" type="text/css" href="xchoolink/css/v2/home/magnific-popup1b26.css?v2" />
	 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
	  <script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>

	<link rel="stylesheet" type="text/css" href="xchoolink/css/v2/home/style43a0.css?v3" />
	<link rel="stylesheet" type="text/css" href="xchoolink/css/v2/home/style-gold1b26.css?v2" />
	<link rel="stylesheet" type="text/css" href="xchoolink/css/v2/home/custom43a0.css?v3" />
<script type="text/javascript" src="xchoolink/js/vendor/jquery-1.11.0.min1b26.js?v2"></script-->
<!--[if lt IE 10]>
<link rel="stylesheet" type="text/css" href="xchoolink/css/ie.css" /><![endif]-->
<!--[if lt IE 9]>

	<!--link rel="stylesheet" type="text/css" href="xchoolink/css/vendor/html5shiv.js.css" /-->
<![endif]-->
	<style>
    #preloader2 {position: fixed;top: 0;left: 0;right: 0;bottom: 0;background-color: #fff;
        /* change if the mask should have another color then white */
        z-index:9999;
        /* makes sure it stays on top */}
    #status {width: 64px;height: 64px;position: absolute;left: 50%;
        /* centers the loading animation horizontally one the screen */
        top: 50%;
        /* centers the loading animation vertically one the screen */
        background-image: url(assets2/img/preloader1b26.gif?v2);
        /* path to your loading animation */
        background-repeat: no-repeat;
        background-position: center;
        margin: -100px 0 0 -100px;
        /* is width and height divided by two */
    }
</style>
</head>
<body data-spy="scroll" data-target=".navMenuCollapse">
<!--[if lt IE 7]>
    <<![endif]-->
<div id="preloader2">
    <div id="status">&nbsp;</div>
</div>

<div id="wrap" class="wrapper_all">
	<div id="home"></div>

 <nav class="navbar navbar-fixed-top navbar-slide">
    	<script type="text/javascript">


</script>
	<div class="container_fluid header_custom_tab">
								<a class="navbar-brand goto" href="#wrap" style="padding-right:0px;">
					<img src="" height="30" width="200" alt="Xchool'ing" />				</a>
								<a class="contact-btn icon-envelope" data-toggle="modal" data-target="#modalContact" title=" Cliquer ici pour commander une démo par mail "></a>
				<button class="navbar-toggle menu-collapse-btn collapsed" data-toggle="collapse" data-target=".navMenuCollapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<div class="collapse navbar-collapse navMenuCollapse">
					<ul class="nav" style="padding-left:0px;">
									<li><a href="#home" style=" font-size:15px;"><?php echo L::Homestartindex ?></a> </li>
						<li><a href="#features" style=" font-size:12px;"><?php echo L::Fonct?></a> </li>


						<li><a href="#online" style=" font-size:12px;"> <?php echo L::cour_en_ligne?> </a> </li>

						<li><a href="#benefits2" style=" font-size:12px;"> <?php echo L::pourquoi?>  </a></li>




							<li class="dropdown language-switch">
                <?php

                  if($_SESSION['user']['lang']=="fr")
                  {
                    ?>
                    <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <img
                        src="assets2/img/flags/french_flag.jpg" style="width:17px;"  class="position-left" alt=""> Francais <span
                        class="fa fa-angle-down"></span>
                    </a>
                    <?php
                  }else if($_SESSION['user']['lang']=="en")
                  {
                    ?>
                    <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <img
                        src="assets2/img/flags/gb.png" style="width:17px;"  class="position-left" alt=""> English <span
                        class="fa fa-angle-down"></span>
                    </a>

                    <?php
                  }

                 ?>

							<ul class="dropdown-menu">
								<li onclick="addFrench()">
									<a class="french"><img src="assets2/img/flags/french_flag.jpg" alt="" style="width:17px;" > Francais</a>
								</li>

								<li onclick="addEnglish()">
									<a class="english"><img src="assets2/img/flags/gb.png" alt="" > English</a>
								</li>


							</ul>
						</li>


					<ul class="reg-log-common">


					<li class="reg-buttns">
						<div class="morph-button morph-button-modal morph-button-modal-2 morph-button-fixed">

						  <button type="button" class="actual-reg-btn"> <a href="signup.php" style="margin-top:-10px;padding-left:7px; font-size:12px;"> <?php echo L::membres?> </a>

						 </button>

						</div>
					</li>

					<li class="reg-buttns">
						<div class="morph-button morph-button-modal morph-button-modal-2 morph-button-fixed">

						  <button type="button" class="actual-reg-btn"> <a href="subscribe.php" style="margin-top:-10px;padding-left:10px;font-size:12px;"> <?php echo L::Souscrire?> </a>

						 </button>

						</div>
					</li>
					</ul>



					</ul>
				</div>
			</div>
   </nav>


    <!-- INTRO BEGIN -->
		<header id="full-intro" class="intro-block bg-color-blue" >
		<div class="ray ray-vertical y-100 x-50 ray-rotate-135 laser-blink hidden-sm hidden-xs" ></div>
		<div class="ray ray-horizontal y-25 x-0 ray-rotate-45 laser-blink hidden-sm hidden-xs" ></div>
		<div class="container">
			<div class="row">

				<div class="col-sm-7 col-sm-offset-1 pull-right main_banner_div">



 					<h1 class="slogan"><p class="slogan_text_p"><span style="font-size:80px; color:#71D40F;"><?php echo L::texte?></span> <br><span data-type="School-Parent" class="type" style="color:#324085"><?php echo L::texte1?></span><br> <?php echo L::texte2?></p></h1>
					<div class="download_app_icons">
						<a class="download-btn-alt ios-btn" href="https://play.google.com/store/search?q=xschoolink&c=apps" target="_blank">
							<i class="icon soc-icon-apple"></i> <?php echo L::tel1?>  <b> <?php echo L::tel2?></b>
						</a>
						<a class="download-btn-alt android-btn" href="https://play.google.com/store/apps/details?id=cm.ftg.xschool" target="_blank">
							<i class="icon soc-icon-android"></i><?php echo L::tel1?> <b> <?php echo L::tel3?></b>
						</a>
					</div>


				</div>
				<div class="col-sm-4 featured hidden-sm hidden-xs">
					<img src="xchoolink/timthumb8387.png?src=https://www.skoolbeep.com/img//v2/home/intro_screen_fee.png&amp;w=400&amp;h=690&amp;a=c" height="690" width="400" data-wow-delay="0.5s" class="intro-screen wow bounceInUp" alt="" />				</div>
			</div>
		</div>
		<div class="block-bg" data-stellar-ratio="0.4"></div>
	</header>
	<!-- INTRO END -->

	<!-- FEATURES BEGIN -->
	<section id="features" class="img-block-3col features_main">
		<div class="container">
			<div class="title">
				<h2> <?php echo L::Fonct?> </h2>
				<p> <?php echo L::tel4?></p>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<ul class="item-list-right item-list-big">
						<li class="wow fadeInLeft">
							<i class="icon icon-screen-desktop" style="color: red;"></i>
							<h3 style="color:#324085;"><?php echo L::tel5?> </h3>
							<p style="padding-right: 50px;"> <i class="fas fa-location-arrow" style="color: red;"></i> &nbsp; <?php echo L::tel7?> </p>
							<p style="padding-right: 53px;"> <i class="fas fa-location-arrow" style="color: red;"></i> &nbsp;   <?php echo L::tel8?></p>
							<p style="padding-right: 95px;"> <i class="fas fa-location-arrow" style="color: red;"></i> &nbsp;  <?php echo L::tel9?></p>
							<p style="padding-right: 85px;"> <i class="fas fa-location-arrow" style="color: red;"></i> &nbsp; <?php echo L::t1?></p>
							<p style="padding-right: 62px;"> <i class="fas fa-location-arrow" style="color: red;"></i> &nbsp; <?php echo L::t2?> </p>
							<p style="padding-right: 53px;"><i class="fas fa-location-arrow" style="color: red;"></i> &nbsp;  <?php echo L::t3?> </p>
							<p style="padding-right: 60px;"> <i class="fas fa-location-arrow" style="color: red;"></i> &nbsp;  <?php echo L::t4?></p>

						</li>


						<li class="wow fadeInLeft">
							<i class="icon icon-calendar" style="color: red;"></i>
							<h3 style="color:#324085;"><?php echo L::a?></h3>
							<p> <?php echo L::a1?></p>
						</li>

					</ul>
				</div>
				<div class="col-sm-4 col-sm-push-4">
					<ul class="item-list-left item-list-big">
						<li class="wow fadeInRight">
							<i class="icon icon-map" style="color: red;"></i>
							<h3 style="color:#324085;"> <?php echo L::tel6?> </h3>
							<p style=""><i class="fas fa-long-arrow-alt-right" style="color:red"></i> <?php echo L::c1?> </p>
							<p style=""> <i class="fas fa-long-arrow-alt-right" style="color:red"></i>  <?php echo L::c?></p>
							<p style=""> <i class="fas fa-long-arrow-alt-right" style="color:red"></i> <?php echo L::c2?></p>
							<p style=" "> <i class="fas fa-long-arrow-alt-right" style="color:red"></i>   <?php echo L::c3?></p>
							<p style=""> <i class="fas fa-long-arrow-alt-right" style="color:red"></i> <?php echo L::c4?></p>
							<p style=""><i class="fas fa-long-arrow-alt-right" style="color:red"></i>   <?php echo L::c5?></p>

						</li>
						<li class="wow fadeInRight">
						<i class="icon icon-rocket" style="color: red;"></i>
							<h3 style="color:#324085;"><?php echo L::a2?></h3>
							<p><?php echo L::a3?></p>
						</li>

					</ul>
				</div>
				<div class="col-sm-4 col-sm-pull-4">
					<div class="animation-box wow bounceIn">
						<img src="" height="192" width="48" alt="Xchool'ing" class="highlight-left wow" />						<img src="xchoolink/img/v2/home/li.png" height="192" width="48" alt="Xchool'ing" class="highlight-right wow" />						<img src="xchool'ink/timthumbcad0.png?src=https://www.skoolbeep.com/img/v2/home/features_screen.png&amp;w=300&amp;h=581" height="581" width="300" alt="Xchool'ing" class="screen" />					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- FEATURES END -->

		<!-- FEATURES BEGIN -->
	<section id="innovations" class="" style="background-color:#324085;">
		<div class="container">
			<div class="title">
				<h1 style="color: white;font-weight: bold;font-family:italic">Xschool’ink</h1><br>
				<p style="color: white; "><?php echo L::b1?> <br>.</p>
			</div>
			<img src="xchool'ink/pencher.png?src=https://www.skoolbeep.com/img/v2/home/innovation_screen.png&amp;w=800&amp;h=387" height="387" width="800" alt="Xchool'ing" class="screen wow bounceInUp" />		</div>
		<div id="ray1" class="ray ray-horizontal"></div>
		<div id="ray2" class="ray ray-horizontal"></div>
		<div id="ray3" class="ray ray-horizontal"></div>
		<div id="ray4" class="ray ray-horizontal"></div>
	</section>
	<!-- FEATURES END -->

	<!-- BENEFITS1 BEGIN -->
	<section id="benefits1" class="img-block-2col benifits_main" style="background-color: #F8F8F8">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="title">

					</div>
					<ul class="item-list-left">
						<li>
							<i class="icon icon-picture color" style="color: red;"></i>
							<h3 style="color:#324085;"><?php echo L::ben1?></h3>

							<p style=""><i class="icon-control-forward " style="color:#324085;"></i> &nbsp; &nbsp; <?php echo L::ben2?></p>
							<p style=""> <i class="icon-control-forward " style="color:#324085;"></i> &nbsp; &nbsp; <?php echo L::ben3?></p>
							<p style=""> <i class="icon-control-forward "style="color:#324085;"></i> &nbsp; &nbsp; <?php echo L::ben4?></p>
							<p style=" "> <i class="icon-control-forward " style="color:#324085"></i> &nbsp; &nbsp;  <?php echo L::ben5?></p>
							<p style=""> <i class="icon-control-forward " style="color:#324085;"></i> &nbsp; &nbsp; <?php echo L::ben6?></p>
							<p style=""><i class="icon-control-forward " style="color:#324085;"></i>   &nbsp; &nbsp; <?php echo L::ben7?> </p>

						</li>
						<li>
							<i class="icon icon-equalizer color" style="color: red;"></i>
							<h3 style="color:#324085;">  <?php echo L::bec?> </h3>
							<p style=""><i class="fas fa-check" style="color: red;"></i> &nbsp; &nbsp; <?php echo L::bec1?> </p>
						   <p style=""> <i class="fas fa-check" style="color: red;"></i> &nbsp; &nbsp;  <?php echo L::bec2?></p>
							<p style=""> <i class="fas fa-check" style="color: red;"></i> &nbsp; &nbsp; <?php echo L::bec3?></p>
							<p style=" "> <i class="fas fa-check" style="color: red;"></i> &nbsp; &nbsp; <?php echo L::bec4?></p>
							<p style=""> <i class="fas fa-check" style="color: red;"></i> &nbsp; &nbsp;<?php echo L::bec5?></p>
							<p style=""> <i class="fas fa-check" style="color: red;"></i> &nbsp; &nbsp; <?php echo L::bec6?> </p>
							<p style=""><i class="fas fa-check" style="color: red;"></i> &nbsp; &nbsp; <?php echo L::bec7?></p>
						</li>

					</ul>
				</div>
				<div class="col-md-5 col-md-offset-1 col-sm-6 right_benifit_phone">
					<div class="screen-couple-right wow fadeInRightBig">
						<div class="flare">
							<img src="xchool'ink/img/v2/home/flare_base.png" height="374" width="328" alt="Xchool'ing" class="base wow" />							<img src="xchool'ink/img/v2/home/flare_base.png" height="110" width="95" alt="Xchool'ing" class="shapes wow" />						</div>
						<img src="xchool'ink/img/v2/home/screen_couple_above.png" height="484" width="250" alt="Xchool'ing" class="screen above" />						<img src="xchool'ink/img/v2/home/screen_couple_beyond.png" data-wow-delay="0.5s" height="407" width="210" alt="Xchool'ing" class="screen beyond wow fadeInRight" />
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="online" class="img-block-2col benifits_main">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="title">
						<h2> <?php echo L::cour?> </h2>
					</div>
					<ul class="item-list-left">
						<li>
							<i class="icon icon-layers  " style="color: red;"></i>
							<h3 style="color:#324085;"> <?php echo L::cour1?> </h3>

							<p style=""><i class="icon-star   " style="color:#324085;"></i>&nbsp; &nbsp;   <?php echo L::cour2?> </p>
							<p style=""><i class="icon-star  " style="color:#324085;"></i> &nbsp; &nbsp;  <?php echo L::cour3?> </p>
							<p style=""><i class="icon-star " style="color:#324085;"></i> &nbsp; &nbsp;  <?php echo L::cour4?></p>
							<p style=""><i class="icon-star " style="color:#324085;"></i> &nbsp; &nbsp;  <?php echo L::cour5?> </p>
							<p style=""><i class="icon-star  " style="color:#324085;"></i>&nbsp; &nbsp;   <?php echo L::cour6?> </p>

						</li>
						<li>
							<i class=" icon fas fa-graduation-cap" style="color: red;"></i>
							<h3 style="color:#324085;"> <?php echo L::courele?> </h3>
							<p style=""><i class="fas fa-location-arrow" style="color: red;"></i> &nbsp; &nbsp; <?php echo L::courele1?> </p>
						   <p style=""> <i class="fas fa-location-arrow" style="color: red;"></i>&nbsp; &nbsp; <?php echo L::courele2?> </p>
						   <p style=""> <i class="fas fa-location-arrow" style="color: red;"></i>&nbsp; &nbsp; <?php echo L::courele3?>  </p>
						   <p style=""> <i class="fas fa-location-arrow" style="color: red;"></i>&nbsp; &nbsp; <?php echo L::courele4?>  </p>

						</li>

					</ul>
				</div>
				<div class="col-md-5 col-md-offset-1 col-sm-6 right_benifit_phone">
					<div  class="wow fadeInRightBig animated">

						<img src="xchool'ink/raw1.jpg" height="484" width="800" alt="Xchool'ing"  style="padding-right: 270px" />
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- BENEFITS1 END -->
	<!-- BENEFITS1 END -->
	<!-- BENEFITS2 BEGIN -->
<section id="benefits2" class="" style="background-color:#324085;">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-sm-push-6">
					<div class="title">
						<h2 style="color: white;"> <?php echo L::pkoi?>  </h2>
					</div>



				<h4 style="color:gold;"><i class="icon icon-screen-smartphone"></i><i class="icon icon-screen-desktop"></i> <?php echo L::pkoi1?> </h4>
						<p style="color:white">  <?php echo L::pkoi2?>
						</p>
				   <h4 style="color:gold;"> <i class="icon icon-magic-wand"></i> <?php echo L::pkoi3?>
				   </h4>
				           <p style="color:white">
						   <?php echo L::pkoi4?>.
						   </p>
					<h4 style="color:gold;" > <i class="far fa-save" ></i> <?php echo L::pkoi5?>

					</h4>
						   <p style="color:white"> <?php echo L::pkoi6?>
						   </p>

					<h4 style="color:gold;"> <i class="icon icon-envelope"></i> <?php echo L::pkoi7?></h4>
							<p style="color:white"><?php echo L::pkoi8?>
							</p>
					<h4 style="color:gold;"> <i class="icon icon-settings"></i> <?php echo L::pkoi9?></h4>

							<p style="color:white"><?php echo L::pkoi10?>
							</p>
					<h4 style="color:gold;"><i class="icon icon-lock"></i> <?php echo L::pkoi11?></h4>

							<p style="color:white">
								<?php echo L::pkoi12?>
							</p>

							<div class="brochure_div"><a class="brochure_link" target="_blank" download href="brochure/brochurexschoolink.pdf"><span></span></a></div>

				</div>
				<div class="col-sm-6 col-sm-pull-6">
					<div class="screen-couple-left wow fadeInLeftBig">
						<div class="fog fog-top wow"></div>
						<div class="fog fog-bottom wow"></div>
						<img src="xchool'ink/timthumb314d.png?src=https://www.skoolbeep.com/img/v2/home/screen_couple_above_v2.png&amp;w=250&amp;h=484" height="484" width="250" alt="Xchool'ing" class="screen above" />						<img src="xchool'ink/img/v2/home/screen_couple_beyond.png" data-wow-delay="0.5s" height="407" width="210" alt="Xchool'ing" class="screen beyond wow fadeInRight" />					</div>
				</div>

			</div>
		</div>
	</section>
	<!-- BENEFITS2 END -->
	<!-- SCREENSHOTS BEGIN -->
	<section id="customers" class="bg-color2">
		<div class="container-fluid wow fadeIn">
			<h2> <?php echo L::cap?> </h2>
			<div id="screenshots-slider" class="owl-carousel">
				<a class="item" href="xchool'ink/img/v2/home/schoolmenu.png" title="School Home"><img src="xchool'ink/timthumbd731.png?src=https://www.skoolbeep.com/img/v2/home/schoolmenu.png&amp;w=321&amp;h=571" alt="School Home" width="321" height="571" /></a>
				<a class="item" href="xchool'ink/img/v2/home/groupmessage.jpg" title="Group Message"><img src="xchool'ink/timthumb6750.jpg?src=https://www.skoolbeep.com/img/v2/home/groupmessage.jpg&amp;w=321&amp;h=571" alt="Group Message" width="321" height="571"/></a>
				<a class="item" href="xchool'ink/img/v2/home/gallery.jpg" title="Gallery"><img src="xchool'ink/timthumb12a4.jpg?src=https://www.skoolbeep.com/img/v2/home/gallery.jpg&amp;w=321&amp;h=571" alt="Gallery" width="321" height="571"/></a>
				<a class="item" href="xchool'ink/img/v2/home/calendar.jpg" title="Calendar"><img src="xchoolink/timthumb7a1d.jpg?src=https://www.skoolbeep.com/img/v2/home/calendar.jpg&amp;w=321&amp;h=571" alt="Calendar" width="321" height="571"/></a>
				<a class="item" href="xchoolink/img/v2/home/livetracking.jpg" title="Transport"><img src="xchoolink/timthumb1416.jpg?src=https://www.skoolbeep.com/img/v2/home/livetracking.jpg&amp;w=321&amp;h=571" alt="Transport" width="321" height="571"/></a>
			</div>
		</div>
	</section>
	<!-- SCREENSHOTS END -->
	<!-- FACTS BEGIN -->
	<section id="facts">
		<div class="container">
			<ul class="facts-list">
				<li class="wow bounceIn">
					<i class="icon icon-badge"></i>
					<h3 class="wow">900+</h3>
					<h4><?php echo L::cap1?> </h4>
				</li>
				<li class="wow bounceIn" data-wow-delay="0.4s">
					<i class="icon icon-graduation"></i>
					<h3 class="wow">500,000+</h3>
					<h4><?php echo L::cap2?> </h4>
				</li>
				<li class="wow bounceIn" data-wow-delay="0.8s">
					<i class="icon icon-user"></i>
					<h3 class="wow">200,000+</h3>
					<h4> <?php echo L::cap3?>  </h4>
				</li>
				<li class="wow bounceIn" data-wow-delay="1.2s">
					<i class="icon icon-clock"></i>
					<h3 class="wow"> 4+ </h3>
					<h4><?php echo L::cap4?> </h4>
				</li>
			</ul>
		</div>
	</section>
	<!-- FACTS END -->

	<!-- TESTIMONIALS BEGIN -->
	 <section   style="background-color:#324085; color: white ;">
		<div class="container-fluid"  >
			<h2 id="testimonials" style="color: white;"> <?php echo L::tem?>  </h2>
			<div id="testimonials-slider" class="owl-carousel">
				<div class="item container">
					<div class="talk"> Notre école a opté pour la solution  <span style="color: red"> Xshool'ink
					 </span> et dépuis que nous l'utilisons , la gestion de notre école se fait  beaucoup plus simplement  et de façon  autonome.</div>
					<img src="xchoolink/img/v2/home/customer_photo.jpg" height="60" width="60" alt="Xchool'ing" class="photo" />					<div class="name"> Dr Njoya Merlin </div>
					<div class="ocupation"> Patron d'école</div>
				</div>
				<div class="item container">
					<div class="talk"> Grace à <span style="color: red"> Xshool'ink
					 </span>en tant que parent d'élève j'ai la possibilité de controler les activités scolaires de mon enfant grace aux notifications que je reçois.</div>
					<img src="xchoolink/img/v2/home/customer_photo.jpg" height="60" width="60" alt="Xchool'ing" class="photo" />					<div class="name">Pr Jhoson Assoh </div>
					<div class="ocupation"> Parent d'élève </div>
				</div>
				<div class="item container">
					<div class="talk"> <span style="color: red"> Xshool'ink
					 </span> diminue considérablement notre temps de travail <span style="color: red">
					 </span> et nous permet d'échanger avec les parents en temps réel.</div>
					<img src="xchoolink/img/v2/home/customer_photo.jpg" height="60" width="60" alt="Xchool'ing" class="photo" />					<div class="name"> M. Talla  Patrick  S</div>
					<div class="ocupation"> Enseignant </div>
				</div>
				<div class="item container">
					<div class="talk">Notre école a opter pour la solutution  <span style="color: red"> Xshool'ink
					 </span> et dépuis que nous l'utilisons , la gestion de notre école se fait  beaucoup plus simplement  et de façon  autonome.</div>
					<img src="xchoolink/img/v2/home/customer_photo.jpg" height="60" width="60" alt="Xchool'ing" class="photo" />					<div class="name">MBE Justin </div>
					<div class="ocupation">Patron d'école </div>
				</div>
			</div>
		</div>
		<div class="block-bg"></div>
	</section>
	<!-- TESTIMONIALS END -->

	<!-- DOWNLOAD BEGIN -->


	<!-- DOWNLOAD END -->     <!-- FOOTER BEGIN -->
	<footer id="footer">
       <div class="container">
<div class="row">
	<div class="col-md-6 footer-left">
		<p class="copyright">

			<a href=""> © 2019 All rights reserved - PROXIMITY SA </a> <br>

		</p>
			 <a> (+237) 233 43 45 74 - info@proximity-cm.com </a> <br>

			 <a> 76 AVENUE DE L'INDÉPENDANCE, BP 4791 DOUALA CAMEROUN </a>

                          <ul class="soc-list footer_links">
				 <li><a href="confidentiality.php" target="_blank">Terms of Use</a></li>

			 </ul>


		<!-- <p class="madewithlove">
			Powered by <a href="https://www.2basetechnologies.com" target="_blank"  alt="mobile application development company">2Base Technologies Pvt. Ltd</a>
		</p> -->
	</div>

<div class="col-md-6 footer-right">
	<ul class="soc-list social wow flipInX">
		<li><a href="https://twitter.com/ProximitySA" target="_blank"><i class="icon soc-icon-twitter"></i></a></li>
		<li><a href="https://www.facebook.com/Proximity-SA-168936656574928" target="_blank"><i class="icon soc-icon-facebook"></i></a></li>
		<!-- <li><a href="https://plus.google.com/113125530968548562855/" target="_blank"><i class="icon soc-icon-googleplus"></i></a></li> -->
		<li><a href="https://www.linkedin.com/company/proximity-sa/" target="_blank"><i class="icon soc-icon-linkedin"></i></a></li>
	</ul>
</div>
</div>
</div>
<!--<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63066262-1', 'auto');
  ga('send', 'pageview');

</script><script type="text/javascript">

// var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
// (function(){
// var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
// s1.async=true;
// s1.src='/js/v2/tawkto.js';
// s1.charset='UTF-8';
// s1.setAttribute('crossorigin','*');
// s0.parentNode.insertBefore(s1,s0);
// })();


var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5a8aa3f7d7591465c707ca41/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})(); -->

</script>
  	</footer>
	<!-- FOOTER END -->
</div>
<!-- MODALS BEGIN-->
<!--  modal message box begin here-->
<div class="modal fade" id="modalMessage" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3 class="modal-title"></h3>
		</div>
	</div>
</div>





<!-- contact modal-->
<div class="modal fade" id="modalContact" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3 class="modal-title"> Commander une démo </h3>
			<form action="https://www.skoolbeep.com/pages/contact_us" id="contact_form" class="user_contact_from" method="post" accept-charset="utf-8">
				<div style="display:none;">
					<input type="hidden" name="_method" value="POST"/>
				</div>


				<div class="col-md-12 contact_address">

					<div class="form-group">

 						<select class="form-control" id="exampleFormControlSelect1">

                                  <option> Votre Pays de residence * </option>

                                  <option> Cameroun </option>

                                  <option> Cote d'ivoire </option>

                                  <option> Burkina Faso</option>

                                  <option> Congo braville </option>

                                  <option> Niger </option>

                                  <option> Gabon </option>



                           </select>

 					</div>


			   </div>

			   <div class="col-md-6 contact_address">

					<div class="form-group">

 						<input name="" placeholder=" Nom  de votre établissement * "  type="text" id=""/>

 					</div>

			   </div>

			   <div class="col-md-6 contact_address">

					<div class="form-group">

 						<input name="" placeholder=" Nom du responsable * "  type="text" id=""/>

 					</div>

			   </div>

			    <div class="col-md-6 contact_address">

					<div class="form-group">

 						<input name="" placeholder=" Ville de résidence * "  type="text" id=""/>

 					</div>

			    </div>

			    <div class="col-md-6 contact_address">

					<div class="form-group">

 						<input name="" placeholder=" Effectif de l'établissement * "  type="text" id=""/>

 					</div>

			    </div>

			    <div class="col-md-6 contact_address">

					<div class="form-group">

 						<input name="" placeholder=" Addresse Email * "  type="text" id=""/>

 					</div>

			    </div>

			    <div class="col-md-6 contact_address">

					<div class="form-group">

 						<input name="" placeholder=" Téléphone * "  type="text" id=""/>

 				    </div>

			    </div>
			    <div class="col-md-12 contact_address">

					<div class="form-group">

 						<textarea name="" placeholder="Laissez nous un message * " cols="1" rows="1" class="form-control" id="PageMessage"></textarea>

 				    </div> <br>

 				    <div class="submit">

						<input  type="submit" class="blue_btn" id="contact_submit" value="Envoyer" style="background-color:#324085;"/>


					</div>

			    </div>

			 </form>


			</div>
	</div>
</div>
<!-- MODALS END-->


	<script type="text/javascript" src="xchoolink/js/bootstrap.min1b26.js?v2"></script>
	<script type="text/javascript" src="xchoolink/js/v2/home/owl.carousel.min1b26.js?v2"></script>
	<script type="text/javascript" src="xchoolink/js/v2/home/classie1b26.js?v2"></script>

	<script type="text/javascript" src="xchoolink/js/v2/home/modernizr.custom1b26.js?v2"></script>
	<script type="text/javascript" src="xchoolink/js/v2/home/uiMorphingButton_fixed1b26.js?v2"></script>
 <script>
       $(function () {
           jcf.replaceAll();
       });
   </script>
 <script src="xchoolink/js/v2/jcf.js"></script>
   <script src="xchoolink/js/v2/jcf.checkbox.js"></script>
<script type="text/javascript">
// $(function() {
//     $(".rslides").responsiveSlides({
// 		pager: true,
// 		});
//     if($('#flashMessage').length){
// 	    var onclick1 = "$('#errorMsg').slideUp(500).delay(7000)";
// 		jQuery('header').prepend('<div id="errorMsg" class="multimsg" style="position: absolute; top: 0px; display: block;z-index: 10;"><div class="flash flash_success">'+$('#flashMessage').html()+'</div><a href="javascript:void(0);" onclick='+onclick1+' class="msg_close" alt=""><span></span></a></div>');
// 		$("#errorMsg").fadeIn()
// 		.css({top:-100,position:'fixed'})
// 		.animate({top:0}, 800, function() {
// 		    //callback
// 			setTimeout(function() {
// 				$("#errorMsg").slideUp(500).delay(7000);
// 			}, 5000);
// 		});
//     }
//   });

</script>

	<script type="text/javascript" src="xchoolink/js/v2/home/wow.min1b26.js?v2"></script>
	<script type="text/javascript" src="xchoolink/js/v2/home/jquery.smooth-scroll.min1b26.js?v2"></script>
	<script type="text/javascript" src="xchoolink/js/v2/home/jquery.superslides.min1b26.js?v2"></script>
	<script type="text/javascript" src="xchoolink/js/v2/home/placeholders.jquery.min1b26.js?v2"></script>
	<script type="text/javascript" src="xchoolink/js/v2/home/jquery.magnific-popup.min1b26.js?v2"></script>

	<script type="text/javascript" src="xchoolink/js/v2/home/jquery.stellar.min1b26.js?v2"></script>
	<script type="text/javascript" src="xchoolink/js/v2/home/retina.min1b26.js?v2"></script>
	<script type="text/javascript" src="xchoolink/js/v2/home/jquery.validate.min1b26.js?v2"></script>
	<script type="text/javascript" src="xchoolink/js/v2/home/custom6134.js?1575534592"></script>
	<script type="text/javascript" src="xchoolink/js/v2/home/jquery.slimscroll.min1b26.js?v2"></script>

<!-- Preloader -->
<script type="text/javascript">
    //<![CDATA[
        $(window).load(function() { // makes sure the whole site is loaded
            $('#status').fadeOut(); // will first fade out the loading animation
            $('#preloader2').delay(0).fadeOut('slow'); // will fade out the white DIV that covers the website.
            $('body').delay(0).css({'overflow':'visible'});
        })
    //]]>

function addFrench()
{
  var etape=1;
  var lang="fr";
  $.ajax({
    url: 'ajax/langue.php',
    type: 'POST',
    async:false,
    data: 'etape=' + etape+ '&lang=' +lang,
    dataType: 'text',
    success: function (content, statut) {

window.location.reload();

    }
  });
}

function addEnglish()
{
  var etape=1;
  var lang="en";
  $.ajax({
    url: 'ajax/langue.php',
    type: 'POST',
    async:false,
    data: 'etape=' + etape+ '&lang=' +lang,
    dataType: 'text',
    success: function (content, statut) {

window.location.reload();

    }
  });
}

</script>
<!--[if lte IE 9]>
	<script src="xchoolink/js/v2/home/respond.min.js"></script>
<![endif]-->
</body>


</html>
