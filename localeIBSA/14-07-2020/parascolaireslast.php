<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Teatcher.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$userId=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$matiere=new Matiere();
$localadmins= new Localadmin();
$parents=new ParentX();
$imageprofile=$user->getImageProfile($emailUti);
$logindata=$user->getLoginProfile($emailUti);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement
$schoolsofassign=$etabs->getEtablissementbyCodeEtab($codeEtabAssigner);

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$codesEtab=$schoolsofassign;
$codeEtabLocal=$etabs->getcodeEtabByLocalId($userId);
$classes=$classe->getAllClassesbyschoolCode($codeEtabLocal);
$teatchers=$teatcher->getAllTeatchersBySchoolCode($codeEtabLocal);
$matieres=$matiere->getAllMatiereOfThisSchool($codeEtabLocal);

$examens=$etabs->getAllExamensOfThisSchool($codeEtabLocal);



$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabLocal);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabLocal);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];

  $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);

  $parascos=$etabs->getAllParascolairesOfThisSchool($codeEtabLocal,$libellesessionencours);
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabLocal,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabLocal,$libellesessionencours);
}



// session_start();
// require_once('../class/User.php');
// require_once('../class/Parent.php');
// require_once('../class/Classe.php');
//
// require_once('../class/Teatcher.php');
// require_once('../class/Matiere.php');
// $etab=new Etab();
// $parent=new ParentX();
// $user=new User();
// $classe=new Classe();

// $emailUti=$_SESSION['user']['email'];
//
// $imageprofile=$user->getImageProfile($emailUti);
// $logindata=$user->getLoginProfile($emailUti);
// $tablogin=explode("*",$logindata);
// $datastat=$user->getStatis();
// $tabstat=explode("*",$datastat);
// $classeId=$_GET['classe'];
//
//
// if(strlen($imageprofile)>0)
// {
//   $lienphoto="../photo/".$emailUti."/".$imageprofile;
// }else {
//   $lienphoto="../photo/user5.jpg";
// }
//
// $parents=$parent->getAllParent();
//

 // var_dump($parascos);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
  <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
<!--bootstrap -->
<!--bootstrap -->
<link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<!-- Material Design Lite CSS -->
<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
<link href="../assets2/css/material_style.css" rel="stylesheet">
<!-- Theme Styles -->
  <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
<link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
<link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title">Activités Parascolaires :</div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#">Matière</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active">Activités Parascolaires  :</li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['Updateadminok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
            <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['Updateadminok']; ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['Updateadminok']);
                    }

                     ?>
              <?php

                    if(isset($_SESSION['user']['deletesubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
              title: 'Félicitations !',
              text: "<?php echo $_SESSION['user']['deletesubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'OK',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })


                </script>
                      <?php
                      unset($_SESSION['user']['deletesubjectok']);
                    }

                     ?>


              <?php

                    if(isset($_SESSION['user']['updateExamok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: "<?php echo $_SESSION['user']['updateExamok'];?>",

              })


                </script>
                      <?php
                      unset($_SESSION['user']['updateExamok']);
                    }

                     ?>
              <?php

                    if(isset($_SESSION['user']['addsubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>


                Swal.fire({
              title: 'Félicitations !',
              text: "<?php echo $_SESSION['user']['addsubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'OK',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })
                </script>
                      <?php
                      unset($_SESSION['user']['addsubjectok']);
                    }

                     ?>

                     <div class="col-md-12 col-sm-12">
                                   <div class="panel tab-border card-box">
                                       <header class="panel-heading panel-heading-gray custom-tab ">
                                           <ul class="nav nav-tabs">
                                               <li class="nav-item"><a href="#home" data-toggle="tab" class="active"><i class="fa fa-bars"></i> Liste Activités Parascolaires </a>
                                               </li>
                                               <li class="nav-item"><a href="#about" data-toggle="tab"><i class="fa fa-plus-circle"></i> Nouvelle Activité Parascolaire </a>
                                               </li>

                                           </ul>
                                       </header>
                                       <div class="panel-body">
                                           <div class="tab-content">
                                               <div class="tab-pane active" id="home">
                                                 <div class="row">
                        <div class="col-md-12">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header></header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">

                                    <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                        <thead>
                                            <tr>

                                                <th> Activités </th>
                                                <th> Date Début</th>
                                                <th> Date Fin </th>
                                                <th> Classes </th>
                                                <th> Coût </th>
                                                <th> <?php echo L::Actions?> </th>
                                            </tr>
                                        </thead>
                                        <?php
                                        if($nbsessionOn>0)
                                        {
                                         ?>
                                        <tbody>
                                          <?php
                                            //$matieres

                                            $i=1;
                                              foreach ($parascos as $value):
                                           ?>
                                            <tr class="odd gradeX">

                                                <td style="width:200px;"> <?php echo $value->libelle_para;?></td>
                                                <td>
                                                    <?php echo date_format(date_create($value->debut_para),"d/m/Y");?>
                                                </td>
                                                <td>
                                                  <?php echo date_format(date_create($value->fin_para),"d/m/Y");?>
                                                </td>
                                                <td>
                                              <?php
                                              $dataclasse=$value->classes_para;
                                              $tabdataclasse=explode("-",$dataclasse);
                                              $nbtabdataclasse=count($tabdataclasse);
                                              $limitdataclasse=$nbtabdataclasse-1;
                                              for($z=0;$z<$limitdataclasse;$z++)
                                              {
                                                ?>
                                                  <span class="label label-sm label-success" style=""> <?php
                                                  echo $classe->getInfosofclassesbyId($tabdataclasse[$z],$libellesessionencours);
                                                  //echo $tabdataclasse[$z];
                                                  ?> </span>
                                                <?php
                                              }
                                                ?>
                                                </td>
                                                <td>
                                                  <?php
                                                  $montantAct=$value->montant_para;
                                                  if($montantAct==0)
                                                  {
                                                    ?>
  <span class="label label-sm label-info" style=""> Gratuit</span>
                                                    <?php
                                                  }else if($montantAct!=0)
                                                  {
                                                    ?>
<span class="label label-sm label-info" style=""> <?php echo $montantAct; ?> </span>
                                                    <?php
                                                  }
                                                   ?>
                                                </td>

                                                <td class="valigntop">

                                                  <!--a href="#"  onclick="modify(<?php //echo $value->id_mat;?>)" class="btn btn-info  btn-md " style="border-radius:3px;">
                                                    <i class="fa fa-pencil"></i>
                                                  </a-->

                                                  <?php
                                                  $statutpara=$value->statut_para;

                                                  if($statutpara==1)
                                                  {
                                                    //activité créer
                                                    ?>
                                                    <a href="#"  onclick="sender(<?php echo $value->id_para;?>,<?php echo $value->idnotif_para;?>)" class="btn btn-success  btn-md " style="border-radius:3px;" title="envoyer la notification">
                                                      <i class="fa fa-send-o"></i>
                                                    </a>
                                                    <a href="upadteparascolaire.php?paraid=<?php echo $value->id_para ?>&codeEtab=<?php echo $value->codeEtab_para ?>" class="btn btn-info  btn-md " style="border-radius:3px;">
                                                      <i class="fa fa-pencil"></i>
                                                    </a>

                                                    <a href="#"  onclick="deleted(<?php echo $value->id_para;?>,<?php echo $value->idnotif_para;?>)" class="btn btn-danger  btn-md " style="border-radius:3px;" title="supprimer l'activité">
                                                      <i class="fa fa-trash-o"></i>
                                                    </a>
                                                    <?php
                                                  }else if($statutpara==2)
                                                  {
                                                    //notification activité envoyé
                                                    ?>
                                                    <a href="#"  onclick="archived(<?php echo $value->id_para;?>,<?php echo $value->idnotif_para;?>)" class="btn btn-primary  btn-md " style="border-radius:3px;" title="sarchiver l'activité">
                                                      <i class="fa fa-circle-o-notch"></i>
                                                    </a>
                                                    <?php
                                                  }


                                                   ?>


                                                </td>
                                            </tr>


                                            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                            <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
                                            <!-- <script src="../assets2/plugins/select2/js/select2.js" ></script>
                                            <script src="../assets2/js/pages/select2/select2-init.js" ></script> -->
                                            <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
                                            <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                            <script>

                                            function affichemontantM(id)
                                            {
                                              alert(id);
                                            //   document.querySelector("#montantdiv"+id).style.display="block";
                                            //   $("#paiecheck"+id).val(0);
                                            //   $("#gratuitcheck"+id).val(1);
                                            }

                                            function affectergratM(id)
                                            {
                                              alert(id);
                                              // $("#paiecheck"+id).val(1);
                                              // $("#gratuitcheck"+id).val(0);
                                              // document.querySelector("#montantdiv"+id).style.display="none";
                                            }

                                            function gratAffecter(id)
                                            {
                                              alert(id);
                                            }

                                            function gratAffecter(id)
                                            {
                                              alert(id);
                                            }

                                            function soumettre()
                                            {
                                              $("#FormUpdateExam<?php echo $value->id_para;?>").submit();
                                            }

                                            function myFunction(idcompte)
                                            {
                                              //var url="detailslocal.php?compte="+idcompte;
                                            document.location.href="detailsadmin.php?compte="+idcompte;
                                            }

                                            function erasedExam(id)
                                            {
                                              document.getElementById("messageExam"+id).innerHTML = "";

                                            }

                                            function erasedCoef(id)
                                            {
                                              document.getElementById("messageCoef"+id).innerHTML = "";
                                            }

                                            function erasedClasse(id)
                                            {
                                              document.getElementById("messageClasse"+id).innerHTML = "";
                                            }

                                            function erasedTeatcher(id)
                                            {
                                              document.getElementById("messageTeatcher"+id).innerHTML = "";
                                            }

                                              function archived(id,notifid)
                                              {
                                                var session="<?php echo $libellesessionencours; ?>";
                                                var codeEtab="<?php echo $codeEtabLocal; ?>";
                                                Swal.fire({
                                  title: 'Attention !',
                                  text: "Voulez vous vraiment archiver la notification de cette activité",
                                  type: 'warning',
                                  showCancelButton: true,
                                  confirmButtonColor: '#2CA8FF',
                                  cancelButtonColor: '#d33',
                                  confirmButtonText: 'Archiver',
                                  cancelButtonText: 'Annuler',
                                }).then((result) => {
                                  if (result.value) {
                                     document.location.href="../controller/parascolaire.php?etape=5&paraid="+id+'&notifid='+notifid+'&session='+session+'&codeEtab='+codeEtab;

                                  }else {

                                  }
                                })
                                              }

                                            function sender(id,notifid)
                                            {
                                              var sessionEtab="<?php echo $libellesessionencours  ?>";
                                              var etape1=1;
                                              $.ajax({
                                                url: '../ajax/message.php',
                                                type: 'POST',
                                                async:false,
                                                data: 'notifid='+notifid+'&etape='+etape1,
                                                dataType: 'text',
                                                  success: function (content, statut) {


                                                    var tabcontent=content.split("*");
                                                    var destinataires=tabcontent[0];
                                                    var classes=tabcontent[1];
                                                    var codeEtab=tabcontent[2];
                                                    var smssender=tabcontent[3];
                                                    var emailsender=tabcontent[4];
                                                    var joinfile=tabcontent[5];
                                                    var file=tabcontent[6];
                                                    var etape2=3;

                                                    Swal.fire({
                                      title: 'Attention !',
                                      text: "Voulez vous vraiment envoyer la notification de cette activité",
                                      type: 'warning',
                                      showCancelButton: true,
                                      confirmButtonColor: '#2CA8FF',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Envoyer',
                                      cancelButtonText: 'Annuler',
                                    }).then((result) => {
                                      if (result.value) {
                                        // document.location.href="../controller/messages/etape=3&notifid="+notificationid+"&destinataires="+destinataires+"&classes="+classes;
                                        $.ajax({
                                          url: '../ajax/message.php',
                                          type: 'POST',
                                          async:false,
                                          data: 'notifid='+id+'&etape='+etape2+'&codeEtab='+codeEtab+'&destinataires='+destinataires+'&classes='+classes+'&smssender='+smssender+'&emailsender='+emailsender+'&sessionEtab='+sessionEtab,
                                          dataType: 'text',
                                            success: function (content, statut) {

                                              var tab=content.split("/");
                                              var destimails=tab[0];
                                              var destiphones=tab[1];

                             // document.location.href="../controller/messages.php?etape=3&notifid="+notifid+"&destimails="+destimails+"&destiphones="+destiphones+"&codeEtab="+codeEtab+"&smssender="+smssender+"&emailsender="+emailsender+"&joinfile="+joinfile+"&file="+file+"&paraid="+id;
                             document.location.href="../controller/parascolaire.php?etape=4&notifid="+notifid+"&destimails="+destimails+"&destiphones="+destiphones+"&codeEtab="+codeEtab+"&smssender="+smssender+"&emailsender="+emailsender+"&joinfile="+joinfile+"&file="+file+"&paraid="+id;


                                            }
                                        });
                                      }else {

                                      }
                                    })

                                                  }
                                              });
                                            }


                                            function modify(id)
                                            {


                                              Swal.fire({
                                title: 'Attention !',
                                text: "Voulez vous vraiment modifier cette matière",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Modifier',
                                cancelButtonText: 'Annuler',
                              }).then((result) => {
                                if (result.value) {
                                  document.location.href="updatesubject.php?compte="+id;
                                }else {

                                }
                              })
                                            }

                                            function deleted(id,notifid)
                                            {
                                              var sessionEtab="<?php echo $libellesessionencours  ?>";
                                              var codeEtab="<?php echo $codeEtabAssigner;?>";
                                              Swal.fire({
                                title: 'Attention !',
                                text: "Voulez vous vraiment supprimer cette activité parascolaire",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Supprimer',
                                cancelButtonText: 'Annuler',
                              }).then((result) => {
                                if (result.value) {
                                  document.location.href="../controller/parascolaire.php?etape=3&paraid="+id+"&codeEtab="+codeEtab+"&notifid="+notifid+'&session='+sessionEtab;
                                }else {

                                }
                              })
                                            }

                                            function check(id)
                                            {
                                              //recuperation des variables
                                              var exam=$("#exam"+id).val();
                                              var datedeb=$("#datedeb"+id).val();
                                              var datefin=$("#datefin"+id).val();

                                              event.preventDefault();

                                              if(exam==""||datedeb==""||datefin=="")
                                              {
                                                if(exam=="")
                                                {
                                                   document.getElementById("messageExam"+id).innerHTML = "<font color=\"red\">Merci de renseigner le libellé de l'examen!</font>";
                                                }

                                                if(datedeb=="")
                                                {
                                                  document.getElementById("messageDeb"+id).innerHTML = "<font color=\"red\">Merci de renseigner la date de début !</font>";
                                                }

                                                if(datefin=="")
                                                {
                                                  document.getElementById("messageFin"+id).innerHTML = "<font color=\"red\">Merci de renseigner la date de fin !</font>";
                                                }






                                            }else {

                                                  soumettre();
                                            }

                                            }



















                                            </script>

                                            <?php
                                            $i++;
                                            endforeach;
                                            ?>


                                        </tbody>
                                        <?php
                                      }
                                         ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                                               </div>
                                               <div class="tab-pane" id="about">
                                                 <div class="row">
                                                     <div class="col-md-12 col-sm-12">
                                                         <div class="card card-box">
                                                             <div class="card-head">
                                                                 <header></header>

                                                             </div>
                                                             <?php
                                                             if($nbsessionOn>0)
                                                             {
                                                              ?>

                                                             <div class="card-body" id="bar-parent">
                                                                 <form  id="FormAddExam" class="form-horizontal" action="../controller/parascolaire.php" method="post" enctype="multipart/form-data">
                                                                     <div class="form-body">
                                                                       <div class="form-group row">
                                                                               <label class="control-label col-md-3">Libellé Activité
                                                                                   <span class="required"> * </span>
                                                                               </label>
                                                                               <div class="col-md-5">
                                                                                   <input type="text" name="libelactivity" id="libelactivity" data-required="1" placeholder="Entrer le libellé de l'activité" class="form-control input-height" />
                                                                                 </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                                <label class="control-label col-md-3">Type session
                                                                                    <span class="required"> * </span>
                                                                                </label>
                                                                                <div class="col-md-5">

                                                                                  <select class="form-control input-height" id="typesess" name="typesess" style="width:100%" >
                                                                                      <option value="">Selectionner un Type session</option>
                                                                                      <?php
                                                                                      $i=1;
                                                                                        foreach ($typesemestre as $value):
                                                                                        ?>
                                                                                        <option value="<?php echo $value->id_semes?>"><?php echo utf8_encode(utf8_decode($value->libelle_semes)) ?></option>

                                                                                        <?php
                                                                                                                         $i++;
                                                                                                                         endforeach;
                                                                                                                         ?>

                                                                                  </select>

                                                                               </div>
                                                                            </div>

                                                                        <div class="form-group row">
                                                                          <label class="control-label col-md-3">Date Début
                                                                              <span class="required"> * </span>
                                                                          </label>
                                                                              <div class="col-md-5">
                                                                                  <input type="date"  placeholder="Entrer la date de début" name="datedeb" id="datedeb"  value="" class="form-control input-height">

                                                                              </div>
                                                                          </div>
                                                                          <div class="form-group row">
                                                                            <label class="control-label col-md-3">Date Fin
                                                                                <span class="required"> * </span>
                                                                            </label>
                                                                                <div class="col-md-5">
                                                                                    <input type="date" placeholder="Entrer la date de Fin" name="datefin" id="datefin" value=""  class="form-control input-height">

                                                                                    <input type="hidden" name="etape" id="etape" value="1"/>
                                                                                    <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabLocal; ?>"/>
                                                                                    <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                                                                    <input type="hidden" name="gratuitcheck" id="gratuitcheck" value="1">
                                                                                      <input type="hidden" name="paiecheck" id="paiecheck" value="0">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">

                                                                                <label class="control-label col-md-3">Message
                                                                                    <span class="required"> * </span>
                                                                                </label>
                                                                                <div class="col-md-5">
                                                                                  <textarea class="form-control" name="message" id="message" rows="8" cols="80" placeholder="Renseigner le Message"></textarea>

                                                                                </div>

                                                                            </div>
                                                                            <div class="form-group row">
                                                                              <label class="control-label col-md-3"><?php echo L::File?> joint
                                                                                  <span class="required">  </span>
                                                                              </label>
                                                                                <div class="col-md-5">
                                                                                  <input type="file" id="joinfile" name="joinfile" class="joinfile"  data-show-loader="true" data-max-file-size="1mb" data-default-file="../assets2/img/user/user5.jpg" data-allowed-file-extensions="gif png jpg jpeg pjpeg pdf docx xlsx xls doc" />

                                                                                </div>

                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label class="control-label col-md-3">Classe(s) concernée(s)
                                                                                    <span class="required"> * </span>
                                                                                </label>
                                                                                <div class="col-md-5">
                                                                                  <select class="form-control input-height" multiple="multiple" id="classeEtab" name="classeEtab[]" style="width:100%" onchange="erased()">
                                                                                      <option value="" >Selectionner  classe(s)</option>
                                                                                      <?php
                                                                                      $i=1;
                                                                                        foreach ($classes as $value):
                                                                                        ?>
                                                                                        <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                                                        <?php
                                                                                                                         $i++;
                                                                                                                         endforeach;
                                                                                                                         ?>

                                                                                  </select>
                                                                                  <p id="messageselectclasse"></p>
                                                                                  </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label class="control-label col-md-3">Destinataire(s) :
                                                                                    <span class="required">  </span>
                                                                                </label>
                                                                                <div class="col-md-5">
                                                                                  <select class="form-control input-height" multiple="multiple" id="destinataires" name="destinataires[]" style="width:100%" onchange="erased1()">
                                                                                      <option value="" >Selectionner le(s) destinataire(s)</option>
                                                                                      <option value="Parent">PARENTS</option>
                                                                                      <option value="Student">ELEVES</option>
                                                                                      <option value="Teatcher">PROFESSEURS</option>
                                                                                      <option value="Admin_locale">ADMINISTRATION</option>

                                                                                  </select>
                                                                                  <p id="messageselectdesti"></p>
                                                                                  </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                              <div class="col-md-3">

                                                                              </div>
                                                                              <div class="col-md-6">
                                                                                <div class="radio radio-yellow inline">
                                                                                  <input id="radiobg1" name="radio2" type="radio"  checked="checked" onclick="affectergrat()">

                                                                                  <label for="radiobg1">
                                                                                      Gratuit
                                                                                  </label>
                                                                                </div>
                                                                                <div class="radio radio-yellow inline">
                                                                                    <input id="radiobg2" name="radio2" type="radio"  onclick="affichemontant()">

                                                                                    <label for="radiobg2">
                                                                                        Payant
                                                                                    </label>
                                                                                </div>
                                                                              </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                              <div class="col-md-3">

                                                                              </div>
                                                                                    <div class="col-md-5" style="display:none" id='montantdiv'>
                                                                                        <input type="number" min=1 name="montantAct" id="montantAct" data-required="1" placeholder="Entrer le montant de l'activité" class="form-control input-height" />
                                                                                      </div>
                                                                             </div>
                                                                            <div class="form-group row">
                                                                              <div class="col-md-3">

                                                                              </div>

                                                                                <div class="custom-control custom-checkbox custom-control-inline " >
                                            <input type="checkbox" class="custom-control-input" id="smssender" name="smssender" onclick='checksms()'>
                                            <input type="hidden" name="smsvalue" id="smsvalue"  value="0">
                                            <label class="custom-control-label" for="smssender">Notification SMS</label>

                                                                                </div>


                                                                                <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" id="emailsender" name="emailsender" onclick='checkmail()'>
                                            <input type="hidden" name="emailvalue" id="emailvalue"  value="0">
                                            <label class="custom-control-label" for="emailsender">Notification Email</label>

                                                                                </div>





                                                                            </div>
                                                                            <div class="row">
                                                                              <div class="col-md-3">

                                                                              </div>
                                                                              <div class="col-md-6">
                                                                                <p id="messagesnotificationchoice"></p>
                                                                              </div>


                                                                            </div>




                                                   <div class="form-actions">
                                                                         <div class="row">
                                                                             <div class="offset-md-3 col-md-9">

                                                                                 <button type="button" class="btn btn-info" onclick="check()">Enregistrer</button>
                                                                                 <button type="button" class="btn btn-danger">Annuler</button>
                                                                             </div>
                                                                           </div>
                                                                        </div>
                                                 </div>
                                                                 </form>
                                                             </div>
                                                             <?php
                                                           }
                                                              ?>
                                                         </div>
                                                     </div>

                                                 </div>
                                                </div>


                                           </div>
                                       </div>
                                   </div>
                               </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
  <script src="../assets2/js/dropify.js"></script>
  <script src="../assets2/plugins/select2/js/select2.js" ></script>
  <script src="../assets2/js/pages/select2/select2-init.js" ></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
  <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script>

 function checksms()
 {


   if($('#smssender').prop('checked') == true){
    $("#smsvalue").val(1);

  document.getElementById("messagesnotificationchoice").innerHTML = "";
  }
  else {
    $("#smsvalue").val(0);
    if($("#emailvalue").val()==0)
    {
      document.getElementById("messagesnotificationchoice").innerHTML = "<font color=\"red\">Merci de choisir un type d'envoi de notification</font>";
    }


  }

 }

 function checkmail()
 {


   if($('#emailsender').prop('checked') == true){
    $("#emailvalue").val(1);
    document.getElementById("messagesnotificationchoice").innerHTML = "";
  }
  else {

    $("#emailvalue").val(0);

    if($("#smsvalue").val()==0)
    {
      document.getElementById("messagesnotificationchoice").innerHTML = "<font color=\"red\">Merci de choisir un type d'envoi de notification</font>";
    }
  }



 }

 function erased1()
 {

   document.getElementById("messageselectdesti").innerHTML = "";
 }

 function erased()
 {
   document.getElementById("messageselectclasse").innerHTML = "";
 }

 function check()
 {
   var classe=$("#classeEtab").val();
   var desti=$("#destinataires").val();
   var smsvalue=$("#smsvalue").val();
   var emailvalue=$("#emailvalue").val();

   if(classe!="" && desti!="")
   {
     //$("#FormAddExam").submit();

     if(smsvalue==0 && emailvalue==0)
     {
       document.getElementById("messagesnotificationchoice").innerHTML = "<font color=\"red\">Merci de choisir un type d'envoi de notification</font>";
     }else {
       $("#FormAddExam").submit();
     }

   }else if(classe=="" || desti=="") {

     if(classe=="" )
     {
       document.getElementById("messageselectclasse").innerHTML = "<font color=\"red\">Merci de selectionner au moins une classe</font>";

     }

     if(desti=="")
     {
       document.getElementById("messageselectdesti").innerHTML = "<font color=\"red\">Merci de selectionner au moins un destinataire</font>";

     }

   }
 }

function affectergrat()
{
  $("#paiecheck").val(0);
  $("#gratuitcheck").val(1);
  document.querySelector("#montantdiv").style.display="none";


}

function affichemontant()
{
  document.querySelector("#montantdiv").style.display="block";
  $("#paiecheck").val(1);
  $("#gratuitcheck").val(0);
}
$("#classe").select2();
$("#teatcher").select2();
// $("#classeEtab").select2();
$("#classeEtab").select2({
  tags: true,
tokenSeparators: [',', ' ']
});
$("#destinataires").select2({

  tags: true,
tokenSeparators: [',', ' ']
});
$('.joinfile').dropify({
    messages: {
        'default': 'Selectionner un fichier joint',
        'replace': 'Remplacer le fichier joint',
        'remove':  'Retirer',
        'error':   'Ooops, Une erreur est survenue.'
    }
});


$("#typesess").select2();

 jQuery(document).ready(function() {







   $("#FormAddExam").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{
        classeEtab:"required",
        matiere:"required",
        classe:"required",
        teatcher:"required",
        coef:"required",
        examen:"required",
        datedeb:"required",
        datefin:"required",
        typesess:"required",
        montantAct:"required",
        libelactivity:"required",
        message:"required"



      },
      messages: {
        classeEtab:"Merci de selectionner au moins une classe",
        matiere:"Merci de renseigner la matière",
        classe:"Merci de selectionner la classe",
        teatcher:"Merci de selectionner un Professeur",
        coef:"Merci de renseigner le coefficient de la matière",
        examen:"Merci de renseigner le libellé de l'examen",
        datedeb:"Merci de renseigner la date de début",
        datefin:"Merci de renseigner la date de fin",
        typesess:"Merci de selectionner le type de session",
        montantAct:"Merci de renseignant le montant de l'activité parascolaire",
        libelactivity:"Merci de renseigner le libellé de l'activité parascolaire",
        message:"Merci de renseigner le message"


      },
      submitHandler: function(form) {


        var etape=1;

        $.ajax({
          url: '../ajax/examen.php',
          type: 'POST',
          async:true,
          data: 'examen=' + $("#examen").val()+ '&etape=' + etape+'&datedeb='+$("#datedeb").val()+'&datefin='+$("#datefin").val()+'&codeEtab='+$("#codeEtab").val(),
          dataType: 'text',
          success: function (content, statut) {

              if(content==0)
              {
                form.submit();
              }else if(content==1)
              {

                Swal.fire({
                type: 'warning',
                title: 'Attention',
                text: "Cet Examen existe dejà dans le sysème",

              })
              }

          }
        });


         /*$.ajax({
           url: '../ajax/matiere.php',
           type: 'POST',
           async:true,
           data: 'matiere=' + $("#matiere").val()+ '&etape=' + etape+'&classe='+$("#classe").val()+'&teatcher='+$("#teatcher").val()+'&codeEtab='+$("#codeEtab").val()+'&coefficient='+$("#coef").val(),
           dataType: 'text',
           success: function (content, statut) {


             if(content==0)
             {
               //cette matière n'existe pas encore pour cette classe

               form.submit();

             }else if(content==1)
             {
               //il est question d'un nouveau professeur pour cette matière
               Swal.fire({
 title: 'Attention !',
 text: "Cette Matière est dejà dispenser par un Professeur",
 type: 'warning',
 showCancelButton: true,
 confirmButtonColor: '#3085d6',
 cancelButtonColor: '#d33',
 confirmButtonText: 'Modifier le Professeur',
 cancelButtonText: 'Annuler',
}).then((result) => {
 if (result.value) {

//nous allons modifier la valeur de etape pour 4
$("#FormAddSubject #etape").val(4);
$("#FormAddSubject").submit();

 }else {

 }
})

             }else if(content==2)
             {
               // il s'agit du meme professeur pour cette matiere

               Swal.fire({
               type: 'warning',
               title: 'Attention',
               text: "Cette Matière existe deja dans le système",

             })
             }

           }
         });*/

             }


           });
      });






 </script>
    <!-- end js include path -->
  </body>

</html>
