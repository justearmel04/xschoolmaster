<?php
session_start();
require_once('../controller/functions.php');
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$userId=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);

$codeEtabLocal=$etabs->getcodeEtabByLocalId($userId);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabLocal);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabLocal);


if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabLocal);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabLocal,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabLocal,$libellesessionencours);
}


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
     <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>


	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Récap des Présences - Classe :</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><!--i class="fa fa-home"></i-->&nbsp;<a class="parent-item" href="index.html">Présences</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Récap présences</li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">
              <?php

                    if(isset($_SESSION['user']['addattendailyok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
            <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['addattendailyok']; ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addattendailyok']);
                    }

                     ?>
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

<br/>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="card card-topline-green">
                   <div class="card-head">
                       <header>Récapitulatif des Présences</header>
                       <div class="tools">
                           <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                       </div>
                   </div>
                   <div class="card-body ">
                     <form method="post" id="FormSearch" action="recapattendance.php">
                         <div class="row">
                           <div class="col-md-3 col-sm-3">
                           <!-- text input -->
                           <div class="form-group" style="margin-top:8px;">
                               <label>Classe</label>
                               <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                               <select class="form-control input-height" id="classeEtab" name="classeEtab" style="width:100%;">
                                   <option value="">Selectionner une classe</option>
                                   <?php
                                   $i=1;
                                     foreach ($classes as $value):
                                     ?>
                                     <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                     <?php
                                                                      $i++;
                                                                      endforeach;
                                                                      ?>

                               </select>
                           </div>


                       </div>
                       <div class="col-md-3 col-sm-3">
                       <!-- text input -->
                       <div class="form-group" style="margin-top:8px;">
                           <label>Mois</label>
                           <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                           <select class="form-control input-height" id="month" name="month" style="width:100%;">
                               <option selected value="">Selectionner un mois</option>
                               <option  value="JANVIER-1">JANVIER</option>
                               <option  value="FEVRIER-2">FEVRIER</option>
                               <option  value="MARS-3">MARS</option>
                               <option  value="AVRIL-4">AVRIL</option>
                               <option  value="MAI-5">MAI</option>
                               <option  value="JUIN-6">JUIN</option>
                               <option  value="JUILLET-7">JUILLET</option>
                               <option  value="AOUT-8">AOUT</option>
                               <option  value="SEPTEMBRE-9">SEPTEMBRE</option>
                               <option  value="OCTOBRE-10">OCTOBRE</option>
                               <option  value="NOVEMBRE-11">NOVEMBRE</option>
                               <option  value="DECEMBRE-12">DECEMBRE</option>


                           </select>
                           <input type="hidden" name="search" id="search" />
                       </div>


                   </div>
                   <div class="col-md-3 col-sm-3">
                   <!-- text input -->
                   <div class="form-group" style="margin-top:8px;">
                       <label>Année de session</label>
                       <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                       <select class="form-control input-height" id="yearsession" name="yearsession" style="width:100%;">
                           <option selected value="">Selectionner un mois</option>
                           <option  value="<?php echo date('Y');?>"><?php echo date('Y');?></option>
                           <option  value="<?php echo date("Y")+1;?>"><?php echo date("Y")+1;?></option>



                       </select>
                   </div>


               </div>
                   <button type="submit" class="btn btn-success"  style="width:150px;height:35px;margin-top:33px;">Afficher le Récap</button>

                         </div>


                     </form>
                   </div>
               </div>
                        </div>

          </div>
<!-- element à faire apparaitre au clique du bouton rechercher -->
<div class="row" style="" id="affichage">
  <?php
      if(isset($_POST['search']))
      {
          if(isset($_POST['classeEtab'])&&isset($_POST['month'])&&isset($_POST['yearsession']))
          {
              //nous devons recupérer la liste des jours du mois selectionner

               //$num = cal_days_in_month(CAL_GREGORIAN, retranscrireMois($mois), $annee);

               $moisconcat=$_POST['month'];
               $annee=$_POST['yearsession'];
               $tabmoisconcat=explode("-",$moisconcat);
               $mois=$tabmoisconcat[0];
               $numbermois=$tabmoisconcat[1];
               $num = cal_days_in_month(CAL_GREGORIAN,$numbermois, $annee);

               $concat=$annee."-".regiveMois($numbermois)."-";

               $infosclasses=$classe->getInfosofclassesbyId($_POST['classeEtab'],$libellesessionencours);

               //nous allons chercher la liste des eleves de cette classe
               // $students=$student->getAllstudentofthisclasses($_POST['classeEtab']);
               $students=$student->getAllstudentofthisclassesSession($_POST['classeEtab'],$libellesessionencours);


               //echo $num;
              ///var_dump($students);

          }
          ?>

          <div class="offset-md-4 col-md-4"  id="affichage1">
            <div class="card" style="">
            <div class="card-body">
              <h5 class="card-title"></h5>
              <h4 style="font-style: italic;font-weight: bold;text-align:center;font-size:font-size: large;">Récapitulatif des présences Classe</h4>
              <p class="card-text" style="text-align:center;font-weight: bold;"><?php echo @$infosclasses; ?></p>
              <p class="card-text" style="text-align:center;"><?php echo @$mois." ".$annee?></p>

            </div>
          </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="affichage2">
                                  <div class="card card-topline-green">
                                      <div class="card-head">
                                          <header>Liste des présences</header>
                                          <div class="tools">
                                              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                      <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                      <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                          </div>
                                      </div>
                                      <div class="card-body ">
                                        <div class="pull-right">
                                          <a href="#" class="btn btn-md btn-warning" onclick="recapattendance('<?php echo $moisconcat ?>','<?php echo $annee  ?>',<?php echo $_POST['classeEtab']  ?>,'<?php echo $libellesessionencours ?>','<?php echo $codeEtabLocal  ?>')"><i class="fa fa-print"></i>Imprimer </a>
                                        </div>

                                  <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example5"  id="affichage3">
                                      <thead>
                                          <tr>
                                              <!--th style="width:15%;height:10px;padding:3%;margin:15%"-->
                                                <th style="padding:20px;">
                                                  # ELEVES \ JOURS
                                              </th>
                                              <?php
                                                for($x=1;$x<=$num;$x++)
                                                {
                                                ?>
                                                <th style="">
                                                    <?php echo $x; ?>
                                                </th>
                                              <?php
                                                }
                                              ?>


                                          </tr>
                                      </thead>
                                      <tbody>
                                        <?php
                                        $i=1;

                                        foreach ($students as $value):
                                         ?>
                                        <tr>
                                          <td style=""><?php  echo $value->nom_eleve." ".$value->prenom_eleve?></td>
                                          <?php
                                            for($x=1;$x<=$num;$x++)
                                            {
                                            ?>
                                            <td style="">
                                                <?php
                                                  if(strlen($x)==1)
                                                  {
                                                    $x="0".$x;
                                                  }
                                                 $data=$concat.$x;
                                                $matricule=$value->matricule_eleve;
                                                 $nombre=$student->getNbAttendance($matricule,$data,$_POST['classeEtab']);

                                                  if($nombre==0)
                                                  {
                                                    echo $nombre;
                                                  }else {
                                                    $number=$student->getstatutAttendance($matricule,$data,$_POST['classeEtab']);
                                                    $array=json_encode($number,true);
                                                    $someArray = json_decode($array, true);
                                                    echo $someArray[0]["statut_presence"];
                                                    //var_dump($number[0]['statut_presence']);
                                                  }

                                                  ?>
                                            </td>
                                          <?php
                                            }
                                          ?>
                                       </tr>
                                       <?php
                                       $i++;

                                           endforeach;
                                        ?>

                                      </tbody>
                                  </table>



                              </div>
                                  </div>
                              </div>


          <?php
              }
          ?>



          </div>

<!-- fin affichage du bouton rechercher -->

                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
      <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

<?php
  if(isset($_POST))
  {
    ?>

<?php
  }
 ?>


function recapattendance(moisconcat,annee,classeEtab,session,codeEtab)
{
  var etape=4;
  $.ajax({
    url: '../ajax/etat.php',
    type: 'POST',
    async:false,
    data: 'codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&classeEtab='+classeEtab+'&moisconcat='+moisconcat+'&annee='+annee,
    dataType: 'text',
    success: function (response, statut) {

        window.open(response, '_blank');

    }
  });
}

   $('#example5').DataTable( {
       "scrollX": true

   } );
   $("#classeEtab").select2();
   $("#month").select2();
   $("#yearsession").select2();
   $('#datepre').bootstrapMaterialDatePicker
   ({
     date: true,
     time: false,
     format: 'DD/MM/YYYY',
     lang: 'fr',
     minDate : new Date(),
    cancelText: 'Annuler',
    okText: 'OK',
    clearText: 'Effacer',
    nowText: 'Maintenant'

   });
   $(document).ready(function() {

//
$("#FormSearch").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
 },
 highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
 },
 success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    classeEtab:"required",
    month:"required",
    yearsession:"required"



  },
  messages: {
    classeEtab:"Merci de selectionner la classe",
    month:"Merci de selectionner un mois",
    yearsession:"Merci de selection l'année de session"

  },
  submitHandler: function(form) {
    form.submit();
  }
});




   });

   </script>
    <!-- end js include path -->
  </body>

</html>
