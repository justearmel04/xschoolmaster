<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];

$user=new User();
$etabs=new Etab();
$student=new Student();
$localadmins= new Localadmin();
$parents=new ParentX();
$classe=new Classe();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);
$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();

$codeEtabclasseSession=$classe->getCodeEtabOfClassesByClasseId($_GET['classe']);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabclasseSession);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabclasseSession);


if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabclasseSession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabclasseSession,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabclasseSession,$libellesessionencours);
}

// $studentclasses=$student->getAllstudentofthisclasses($_GET['classe']);
$studentclasses=$student->getAllstudentofthisclassesSession($_GET['classe'],$libellesessionencours);

$nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::ClasseMenu ?> : <?php echo $classe->getInfosofclassesbyId($_GET['classe'],$libellesessionencours) ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li>&nbsp;<a class="parent-item" href="#"><?php echo L::Gestionstudents ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::ClassesMenu ?> : <?php echo $classe->getInfosofclassesbyId($_GET['classe'],$libellesessionencours) ?></li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


          <div class="row">
            <div class="col-md-12 col-sm-12">
                            <div class="borderBox light bordered">
                                <div class="borderBox-title tabbable-line">
                                    <div class="caption">
                                        <span class="caption-subject font-dark bold uppercase"><?php echo L::GestionstudentsCaps ?></span>
                                    </div>
                                    <ul class="nav nav-tabs">

                                        <li class="nav-item">
                                            <a href="#borderBox_tab2" data-toggle="tab"> <?php echo L::Presences ?> </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#borderBox_tab1" data-toggle="tab" class="active"> <?php echo L::studMenu ?> </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="borderBox-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="borderBox_tab1">
                                          <div class="row">
                        <div class="col-md-12">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header></header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">
                                  <div class="pull-right">

                                        <a class="btn btn-warning" onclick="listeclasse(<?php echo $_GET['classe'] ?>,<?php echo $_GET['codeEtab'] ?>,<?php echo $libellesessionencours; ?>)" href="#"><i class="fa fa-print"></i> Liste de Classe</a>

                                  </div>
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                        <thead>
                                            <tr>
                                              <th> <?php echo L::Pictures ?></th>
                                              <th> <?php echo L::MatriculestudentTab ?> </th>
                                              <th> <?php echo L::NamestudentTab ?> </th>
                                              <th> <?php echo L::EmailstudentTab ?> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                          $j=1;
                                          foreach ($studentclasses as $value):

                                           ?>
                                            <tr class="odd gradeX" ondblclick="myFunction(<?php echo $value->id_compte?>)">
                                              <td class="patient-img">
                                                <?php
                                                if(strlen($value->photo_compte)>0)
                                                {
                                                  $lien="../photo/Students/".$value->matricule_eleve."/".$value->photo_compte;
                                                }else {
                                                  $lien="../photo/user5.jpg";
                                                }
                                                 ?>
                                                  <img src="<?php echo $lien?>" alt="">
                                              </td>

                                                <td> <?php echo $value->matricule_eleve ?> </td>

                                                <td>
                                                    <a href="#"> <?php echo $value->nom_eleve." ".$value->prenom_eleve?> </a>
                                                </td>
                                                <td>
                                                    <?php echo $value->email_eleve;?>
                                                </td>

                                            </tr>
                                            <script>

                                            function myFunction(idcompte)
                                            {
                                              //var url="detailslocal.php?compte="+idcompte;
                                            document.location.href="detailsstudent.php?compte="+idcompte;
                                            }
                                            </script>


                                            <?php
                                            $j++;
                                            endforeach;
                                             ?>



                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


                                        </div>
                                        <div class="tab-pane" id="borderBox_tab2">
                                          <div class="row">
                        <div class="col-md-12">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header><?php echo L::PresenceDu ?> <?php echo date("d/m/Y") ?></header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">
                                  <?php
                                  //nous allons verifier si nous avons deja la présence pour cette date

                                    $datepresence=date("Y-m-d");

                                    $check=$student->presencesExistByClassesAndDate($_GET['classe'],$datepresence);

                                    if($check>0)
                                    {

                                    }else if($check==0)
                                    {
                                      ?>
                                      <form method="post" action="../controller/attendance.php" id="FormAttendance">
                                    <a class="btn btn-primary " onclick="touspresent(<?php echo $_GET['classe'];?>)" ><?php echo L::MarqueAllpresence ?></a> <a class="btn btn-primary " onclick="tousabsent(<?php echo $_GET['classe'];?>)"><?php echo L::MarqueAllAbsences ?></a> <br>

                                    <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4" style="margin-top:10px;">
                                        <thead>
                                            <tr>

                                              <th style="width:5%">
                                                  #
                                              </th>
                                              <th style="width:15%"> <?php echo L::MatriculestudentTab  ?> </th>
                                              <th style="width:35%"> <?php echo L::NamestudentTab ?> </th>
                                              <th> <?php echo L::Etat ?> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php

                                          $matricules="";
                                          $j=0;
                                          $i=1;
                                          foreach ($studentclasses as $value):
                                            $matricules=$matricules."*".$value->matricule_eleve;
                                           ?>
                                            <tr class="odd gradeX">

                                                <td>   <?php echo $i;?> </td>
                                                <td>
                                                  <?php echo $value->matricule_eleve;?>
              																	</td>
                                                <td>
                                                    <a href="#"><?php echo $value->nom_eleve." ".$value->prenom_eleve;?> </a>
                                                </td>
                                                <td>
                                                  <label class="radio-inline">
                                            <input type="radio" value="1" name="optradio<?php echo $value->matricule_eleve;?>" id="P<?php echo $value->matricule_eleve;?>" onclick="present('<?php echo $value->matricule_eleve;?>')" checked> <?php echo L::Presenter ?>
                                          </label>&nbsp;
                                          <label class="radio-inline">
                                            <input type="radio" value="0" onclick="absent('<?php echo $value->matricule_eleve;?>')" id="A<?php echo $value->matricule_eleve;?>" name="optradio<?php echo $value->matricule_eleve;?>"> <?php echo L::Absent ?>
                                          </label>&nbsp;
                                          <input type="hidden" name="statut<?php echo $value->matricule_eleve;?>" id="statut<?php echo $value->matricule_eleve;?>" value="1"/>

                                                </td>

                                            </tr>



                                            <?php
                                            $i++;
                                            $j++;
                                            endforeach;
                                             ?>



                                        </tbody>
                                    </table>
                                    <?php
                                    //echo $matricules;
                                    $tabMat=explode("*",$matricules);
                                    $nb=count($tabMat);

                                    ?>

                                    <input type="hidden" name="studentmat" id="studentmat" value="<?php echo $matricules;?>"/>
                                    <input type="hidden" name="etape" id="etape" value="1"/>
                                    <input type="hidden" name="nbstudent" id="nbstudent" value="<?php echo $nb;?>"/>
                                    <input type="hidden" name="allpresent" id="allpresent" value=""/>
                                    <input type="hidden" name="classeId" id="classeId" value="<?php echo $_GET['classe']?>"/>
                                    <input type="hidden" name="datePresence" id="datePresence" value="<?php echo date("d/m/Y");?>"/>
                                    <center><button type="button" onclick="check()" class="btn btn-success"><i class="fa fa-check-circle"></i><?php echo L::ValidationStauts ?></button></center>
                                  </form >
                                      <?php
                                    }

                                   ?>


                                </div>
                            </div>
                        </div>
                    </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

          </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
   <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
     <script src="../assets2/js/pages/table/table_data.js" ></script>
     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function listeclasse(classeid,codeEtab,session)
   {

   }

   function check()
   {
     var etape=1;



     $.ajax({

       url: '../ajax/attendance.php',
       type: 'POST',
       async:false,
       data: 'classe=' + $("#classeId").val()+'&datepre='+$("#datePresence").val()+'&etape='+etape,
       dataType: 'text',
       success: function (content, statut)
       {
         alert(content);
           if(content==0)
           {
             $("#FormAttendance").submit();

           }else if(content==1) {
             Swal.fire({
             type: 'warning',
             title: '<?php echo L::WarningLib ?>',
             text: "<?php echo L::PresenceAllreadyExiste ?>",

           })
         }
       }

     });
   }
   function touspresent(idclasse)
   {

      var nb="<?php echo @$nb;?>";
      var matricules="<?php echo @$matricules?>";

      var array=matricules.split('*');

      for(i=1;i<nb;i++)
      {
        //alert(array[i]);
        $("statut"+array[i]).val(1);
        //$("P"+array[i]).prop("checked", true);
        document.getElementById("P"+array[i]).checked = true;

      }
      $("#allpresent").val(1);

   }

   function tousabsent(idclasse)
   {
     var nb="<?php echo @$nb;?>";
     var matricules="<?php echo @$matricules?>";

     var array=matricules.split('*');

     for(i=1;i<nb;i++)
     {
       $("statut"+array[i]).val(0);
       document.getElementById("A"+array[i]).checked = true;

     }
     $("#allpresent").val(0);
   }

   function present(id)
   {
    $("statut"+id).val(1);
   }

   function absent(id)
   {
    $("statut"+id).val(0);
   }


   $(document).ready(function() {



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
