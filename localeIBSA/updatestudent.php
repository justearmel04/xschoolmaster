<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');
require_once('../class/Student.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

  if($_SESSION['user']['groupe']==1)
  {
    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="DAF"||$_SESSION['user']['fonctionuser']=="Intendance"||$_SESSION['user']['fonctionuser']=="Marketing"||$_SESSION['user']['fonctionuser']=="CAES"||$_SESSION['user']['fonctionuser']=="CD"||$_SESSION['user']['fonctionuser']=="CC")
    {
      // $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);

      //nous allons chercher la liste des etablissements du groupe

      $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
        }

        if(strlen($_SESSION['user']['codeEtab'])>0)
        {

        }else {
          $_SESSION['user']['codeEtab']=$codeEtabAssigner;
        }

        $_SESSION['user']['session']=$libellesessionencours;





         // var_dump($allcodeEtabs);

        $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

        $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


    }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
    {
      // $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);
      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
          $_SESSION['user']['session']=$libellesessionencours;

          if(strlen($_SESSION['user']['codeEtab'])>0)
          {

          }else {
            $_SESSION['user']['codeEtab']=$codeEtabAssigner;
          }

          $notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

          // $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
          $classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);
          $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);
        }

    }
  }

  $studentInfos=$student->getAllInformationsOfStudent($_GET['compte'],$libellesessionencours);
  $tabStudent=explode("*",$studentInfos);
  $studentparentid=$tabStudent[8];

  $nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
  $onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
  $offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
  <link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
     <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>


	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::StudentsModifications ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class=""><?php echo L::StudentsGestions ?></li>&nbsp;<i class="fa fa-angle-right"></i>
                              <li class="active"><?php echo L::GeneralInfostudents ?></li>
                          </ol>
                      </div>
                  </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">
              <?php

                    if(isset($_SESSION['user']['updateroutineok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
            <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: '<?php echo L::Felicitations ?>',
                text: "<?php echo $_SESSION['user']['updateroutineok']; ?>",

              })
                </script>
                      <?php
                      unset($_SESSION['user']['updateroutineok']);
                    }

                     ?>
  					<!-- end widget -->
            <?php

                  if(isset($_SESSION['user']['addetabexist']))
                  {

                    ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  <?php
                  echo $_SESSION['user']['addetabexist'];
                  ?>
                  <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                     </a>
                  </div>



                    <?php
                    unset($_SESSION['user']['addetabexist']);
                  }

                   ?>
<br/>

                </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-sm-12">
                  <div class="card card-box">
                      <div class="card-head">
                          <header><?php echo mb_strtoupper(L::Infos); ?></header>

                      </div>


                      <div class="card-body" id="bar-parent">
                        <form  id="FormAddStudent" class="form-horizontal" action="../controller/admission.php" method="post" enctype="multipart/form-data">
                            <div class="form-body">

                              <div class="form-group row">
                                      <label class="control-label col-md-3"><?php echo L::MatriculestudentTab ?>
                                          <span class="required"> * </span>
                                      </label>
                                      <div class="col-md-5">
                                          <input type="text" name="matri" id="matri"  value=" <?php echo $tabStudent[1];?>" data-required="1" placeholder="<?php echo L::EnterMatriculestudentTab ?>" class="form-control " disabled />
                                          <input type="hidden" name="etape" id="etape" value="2"/>
                                          <input type="hidden" name="newStudent" id="newStudent" value="1"/>
                                          <input type="hidden" name="codeEtab" id="codeEtab"  value="<?php echo $tabStudent[14];?>"/>
                                          <input type="hidden" name="idcompte" value="<?php echo $tabStudent[0]; ?>">
                                          <input type="hidden" name="oldfile" value="<?php echo $tabStudent[11]; ?>">
                                          <input type="hidden" name="matricileStudent" value="<?php echo $tabStudent[1] ?>">


                                        </div>
                                  </div>
                            <div class="form-group row">
                                    <label class="control-label col-md-3"><?php echo L::Name ?>
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-5">
                                        <input type="text"  name="nomad" id="nomad" value="<?php echo $tabStudent[2];?>" data-required="1" placeholder="<?php echo L::EnterName ?>" class="form-control " /> </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-md-3"><?php echo L::PreName ?>
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-5">
                                        <input type="text" name="prenomad" value="<?php echo $tabStudent[3];?>" id="prenomad" data-required="1" placeholder="<?php echo L::Enterparentprename ?>" class="form-control " /> </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                        <span class="required">*  </span>
                                    </label>
                                    <div class="col-md-5">
                                      <select class="form-control " id="classeEtab" name="classeEtab" style="width:100%" disabled>
                                          <option selected value=""><?php echo $tabStudent[9];?></option>

                                      </select>
                                      </div>
                                </div>

                                <div class="form-group row">
                                  <label class="control-label col-md-3"><?php echo L::BirthstudentTab ?>
                                      <span class="required"> * </span>
                                  </label>
                                      <div class="col-md-5">
                                          <input type="text" placeholder="<?php echo L::EnterBirthstudentTab ?>" value="<?php echo date_format(date_create($tabStudent[4]),"d/m/Y");?>" name="datenaisad" id="datenaisad" data-mask="99/99/9999" class="form-control ">
                                            <span class="help-block"><?php echo L::Datesymbolesecond ?></span>
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::BirthLocation ?>
                                              <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-5">
                                              <input type="text" name="lieunais" id="lieunais" data-required="1" value="<?php echo $tabStudent[5];?>" placeholder="<?php echo L::EnterBirthLocation ?>" class="form-control " /> </div>
                                      </div>

                                  <div class="form-group row">
                                      <label class="control-label col-md-3"><?php echo L::Gender ?>
                                          <span class="required">*  </span>
                                      </label>
                                      <div class="col-md-5">
                                        <select class="form-control " id="sexe" name="sexe" style="width:100%">
                                            <option value=""><?php echo L::SelectGender ?></option>
                                            <?php
                                            if($tabStudent[6]=="M")
                                            {
                                              ?>
                                              <option  selected value="M"><?php echo L::SexeM?></option>
                                              <option value="F"><?php echo L::SexeF ?></option>
                                              <?php
                                            }else if($tabStudent[6]=="F") {
                                            ?>
                                            <option  value="M"><?php echo L::SexeM?></option>
                                            <option  selected value="F"><?php echo L::SexeF ?></option>
                                            <?php
                                            }
                                             ?>


                                        </select>
                                        </div>
                                  </div>

                                <div class="form-group row">
                                    <label class="control-label col-md-3"><?php echo L::PhonestudentTab ?>
                                        <span class="required">*  </span>
                                    </label>
                                    <div class="col-md-5">
                                        <input name="contactad" id="contactad" type="text" value="<?php echo $tabStudent[12] ?>" placeholder="<?php echo L::EnterPhoneNumber ?> " class="form-control " /> </div>
                                </div>
                                <div class="form-group row">
                                        <label class="control-label col-md-3"><?php echo L::Allergiesingle ?>

                                        </label>
                                        <div class="col-md-5">
                                            <input type="text" name="allergie" id="allergie" data-required="1" value="<?php echo $tabStudent[16] ?>" placeholder="<?php echo L::EnterAllergiesingle ?>" class="form-control " /> </div>
                                    </div>
                                    <div class="form-group row">
                                            <label class="control-label col-md-3"><?php echo L::PhysiqueConditions ?>

                                            </label>
                                            <div class="col-md-5">
                                                <input type="text" name="condphy" id="condphy" data-required="1" value="<?php echo $tabStudent[17] ?>" placeholder="<?php echo L::EnterPhysiqueConditions ?>" class="form-control " /> </div>
                                        </div>
                                <div class="form-group row">
                                    <label class="control-label col-md-3"><?php echo L::Pictures ?>
                                      <span class="required">  </span>
                                    </label>
                                    <div class="compose-editor">
                                      <?php $picture= "../photo/Students/".$tabStudent[1]."/".$tabStudent[11]; ?>
                                      <input type="file" id="photoad" name="photoad" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="<?php echo $picture ?>" data-allowed-file-extensions="gif png jpg jpeg pjpeg" />
                                       </div>
                                </div>


          <div class="form-actions">
                                <div class="row">
                                    <div class="offset-md-3 col-md-9">

                                        <button type="submit" class="btn btn-info"><?php echo L::ModifierBtn ?></button>


                                        <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                    </div>
                                  </div>
                               </div>
        </div>
                        </form>
                      </div>
                  </div>
              </div>

            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
      <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
       <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
  <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
  <script src="../assets2/js/dropify.js"></script>

 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>


   <script>

   // function SetcodeEtab(codeEtab)
   // {
   //   var etape=3;
   //   $.ajax({
   //     url: '../ajax/sessions.php',
   //     type: 'POST',
   //     async:false,
   //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
   //     dataType: 'text',
   //     success: function (content, statut) {
   //
   // window.location.reload();
   //
   //     }
   //   });
   // }

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }
   // $("#libetab").select2();

jQuery(document).ready(function() {

$("#sexe").select2();
$("#fonctionad").select2();

  $("#FormAddStudent").validate({

    errorPlacement: function(label, element) {
    label.addClass('mt-2 text-danger');
    label.insertAfter(element);
  },
  highlight: function(element, errorClass) {
    $(element).parent().addClass('has-danger')
    $(element).addClass('form-control-danger')
  },
  success: function (e) {
        $(e).closest('.control-group').removeClass('error').addClass('info');
        $(e).remove();
    },
     rules:{
       lieunais:"required",
       matri:"required",
       parenta:"required",
       classeEtab:"required",
       sexe:"required",
       passad:{
         required:{
           depends: function(element) {
                 return ($('#confirmtad').val() != '');
             }
         }
       },
     /*  passad: {
           required: true,
           minlength: 6
       },*/
       confirmtad:{
           required:{
             depends: function(element) {
                   return ($('#passad').val() != '');
               }
           },
           minlength: 6,
           equalTo:'#passad'
       },
       fonctionad:"required",

       loginad:"required",
       emailad: {
                  required: true,
                  email: true
              },
       contactad:"required",
       datenaisad:"required",
       prenomad:"required",
       nomad:"required"


     },
     messages: {
       lieunais:"<?php echo L::PleaseEnterBirthLocation ?>",
       matri:"<?php echo L::PleaseEnterMatriculeTea ?>",
       parenta:"<?php echo L::PleaseSelectAParent ?>",
       classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
       sexe:"<?php echo L::PleaseSelectGende ?>",
       confirmtad:{
           required:"<?php echo L::Confirmcheck ?>",
           minlength:"<?php echo L::Confirmincheck ?>",
           equalTo: "<?php echo L::ConfirmSamecheck ?>"
       },
       passad: {
           required:"<?php echo L::Passcheck ?>",
           minlength:"<?php echo L::Confirmincheck ?>"
       },
       loginad:"<?php echo L::Logincheck ?>",
       emailad:"<?php echo L::PleaseEnterEmailAdress ?>",
       contactad:"<?php echo L::PleaseEnterPhoneNumber ?>",
       datenaisad:"<?php echo L::PleaseEnterPhonestudentTab ?>",
       prenomad:"<?php echo L::PleaseEnterPrename ?>",
       nomad:"<?php echo L::PleaseEnterName ?> ",
       fonctionad:"<?php echo L::PleaseEnterFonction ?>"
     },
     submitHandler: function(form) {
       //verifier si ce compte n'existe pas encore dans la base de données
          form.submit();
     }


  });

});

   </script>
    <!-- end js include path -->
  </body>

</html>
