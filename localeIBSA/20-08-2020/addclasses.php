<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

// echo $_SESSION['user']['fonctionuser'];
if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

  if($_SESSION['user']['groupe']==1)
  {
    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
    {
      // echo "bonsoir";
      $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);

      //nous allons chercher la liste des etablissements du groupe

      $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
        }

        if(strlen($_SESSION['user']['codeEtab'])>0)
        {

        }else {
          $_SESSION['user']['codeEtab']=$codeEtabAssigner;
        }

        $_SESSION['user']['session']=$libellesessionencours;





         // var_dump($allcodeEtabs);

        $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

        $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


    }else if($_SESSION['user']['fonctionuser']=="Coordonnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
    {
      // echo "bonjour";
      $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);
      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
          $_SESSION['user']['session']=$libellesessionencours;

          if(strlen($_SESSION['user']['codeEtab'])>0)
          {

          }else {
            $_SESSION['user']['codeEtab']=$codeEtabAssigner;
          }

          $notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

          // $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
          $classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);
          $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

          // var_dump($classes);
        }

    }
  }

  // var_dump($allcodeEtabs);

// echo $libellesessionencours;

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <style>
    #radioBtn .notActive{
    color: #3276b1;
    background-color: #fff;
    }

    #radioBtn1 .notActive{
    color: #3276b1;
    background-color: #fff;
    }
    </style>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title"><?php echo L::Addaclasse ?></div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#"><?php echo L::ClasseMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active"><?php echo L::Addaclasse ?></li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addclasseok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: '<?php echo L::Felicitations ?>',
  text: "<?php echo $_SESSION['user']['addclasseok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::AddNews ?>',
  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="classes.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addclasseok']);
                    }

                     ?>

                     <?php
                     if($nbsessionOn==0)
                     {
                       ?>
                       <div class="alert alert-danger alert-dismissible fade show" role="alert">

                       <?php echo L::RequiredScolaireYear ?>

                       <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
                        </a>
                       </div>
                       <?php
                     }
                      ?>


              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <div class="card card-box">
                          <div class="card-head">
                              <header><?php echo L::ClasseInfos ?></header>

                          </div>

                          <div class="card-body" id="bar-parent">
                              <form  id="FormAddClass" class="form-horizontal" action="../controller/classe.php" method="post" >
                                  <div class="form-body">

                                    <!--div class="form-group row">
                                        <label class="control-label col-md-3">Section
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-5">
                                          <select class="form-control input-height" id="section" name="section" style="width:100%">
                                              <option value="">Selectionner une section</option>
                                              <?php
                                              //$i=1;
                                                //foreach ($sections as $value):
                                                ?>
                                                <option value="<?php //echo $value->id_section?>"><?php //echo utf8_encode(utf8_decode($value->libelle_section)) ?></option>

                                                <?php
                                                                                 //$i++;
                                                                                 //endforeach;
                                                                                 ?>

                                          </select>
                                        </div>

                                    </div-->
                                    <div class="form-group row">
                                        <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-5">
                                          <input type="text" class="form-control" placeholder="Entrer le Nom de la classe" name="classe" id="classe" value="" ></div>
                                          <input type="hidden" name="etape" id="etape" value="5" />
                                          <input type="hidden" name="nbcantine" id="nbcantine" value="0">
                                          <input type="hidden" id="libetab" name="libetab" value="<?php echo $codeEtabAssigner;?>"/>
                                          <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                          <input type="hidden" name="typeEtab" id="typeEtab" value="<?php echo $etablissementType; ?>">
                                    </div>
                                    <div class="form-group row" style="display:none">
                                        <label class="control-label col-md-3"><?php echo L::ClasseType ?>
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-5">
                                          <select class="form-control" name="typeclasse" id="typeclasse" style="text-align:center">
                                            <option value=""><?php echo L::SelecetClasseType ?></option>
                                            <option value="PREK">PRE K</option>
                                            <option value="K1">K1</option>
                                            <option value="K2">K2</option>
                                            <option value="K3">K3</option>
                                            <option value="GRADE1">GRADE1</option>
                                            <option value="GRADE2">GRADE2</option>
                                            <option value="GRADE3">GRADE3</option>
                                            <option value="GRADE4">GRADE4</option>
                                            <option value="GRADE5">GRADE5</option>
                                            <option value="GRADE6">GRADE6</option>
                                            <option value="GRADE7">GRADE7</option>
                                            <option value="GRADE8">GRADE8</option>
                                            <option value="GRADE9">GRADE9</option>
                                          </select>

                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3"><?php echo L::InscriptionDues ?>
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-5">
                                          <input type="text" class="form-control" id="montantinscrip" name="montantinscrip" placeholder="<?php echo L::EnterInscriptionDues ?>">

                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3"><?php echo L::ScolarityDues ?>
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-5">
                                          <input type="text" class="form-control" id="montantscola" name="montantscola" placeholder="<?php echo L::EnterScolarityDues ?>">

                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3"><?php echo L::ReinscriptionFees ?>
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-5">
                                          <input type="text" class="form-control" id="montantreins" name="montantreins" placeholder="<?php echo L::ReinscriptionFeesEnter ?>">

                                        </div>

                                    </div>
                                    <div class="form-group row" style="display:none">
                                        <label class="control-label col-md-3"><?php echo L::AESDues ?>
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-5">
                                          <input type="text" class="form-control" id="montantAES" name="montantAES" placeholder="<?php echo L::EnterAESDues ?>">

                                        </div>

                                    </div>
                                    <?php
                                    //nous allons verifier si cet etablissemnt gere la cantine scolaire
                                    $cantineoption=$etabs->getCantineOption($codeEtabAssigner);

                                    if($cantineoption==1)
                                    {
                                      ?>
                                      <fieldset>
                                        <!-- <legend style="margin-left:280px">FRAIS DE CANTINE</legend> -->
                                        <!-- <div class="col-md-12">
                                        <span class="label label-md label-info"  style="text-align:center;margin-left:360px">FRAIS DE CANTINE</span>
                                      </div></br> -->
                                        <!-- <div class="form-group row">
                                            <label class="control-label col-md-3">Frais mensuel
                                                <span class="required">  </span>
                                            </label>
                                            <div class="col-md-5">
                                              <input type="text" class="form-control" id="montantMenscant" name="montantMenscant" placeholder="Entrer les frais mensuels de la cantine">

                                            </div>

                                        </div> -->
                                        <input type="hidden" class="form-control" id="montantMenscant" name="montantMenscant"  value="0" placeholder="Entrer les frais mensuels de la cantine">

                                        <!-- <div class="form-group row">
                                            <label class="control-label col-md-3">Frais trimestriel
                                                <span class="required">  </span>
                                            </label>
                                            <div class="col-md-5">
                                              <input type="text" class="form-control" id="montantTrimcant" name="montantTrimcant" placeholder="Entrer les frais trimestriels de la cantine">

                                            </div>

                                        </div> -->
                                        <input type="hidden" class="form-control" id="montantTrimcant" name="montantTrimcant" value="0" placeholder="Entrer les frais trimestriels de la cantine">

                                        <!-- <div class="form-group row">
                                            <label class="control-label col-md-3">Frais annuel
                                                <span class="required">  </span>
                                            </label>
                                            <div class="col-md-5">
                                              <input type="text" class="form-control" id="montantAnncant" name="montantAnncant" placeholder="Entrer les frais annuels de la cantine">

                                            </div>

                                        </div> -->
                                        <input type="hidden" class="form-control" id="montantAnncant" name="montantAnncant" value="0" placeholder="Entrer les frais annuels de la cantine">
                                        <!-- <div class="form-group row">
                                          <label class="control-label col-md-3">
                                          <span class="required"> </span>
                                          </label>
                                          <label class="col-sm-7 col-md-7">Les frais de cantine sont-ils obligatoires ?
                                          <span class="required"> </span>
                                          </label>
                                        </div> -->
                                        <div class="form-group row">

                                  <label class="control-label col-md-3">
                                  <span class="required"> </span>
                                  </label>
                                  <div class="col-sm-7 col-md-7">
                                  <div class="input-group">
                                  <div id="radioBtn" class="btn-group">
                                  <!-- <a class="btn btn-primary btn-sm active" data-toggle="cantineorder" data-title="0" onclick="cantineorderN()">NON</a>
                                  <a class="btn btn-primary btn-sm notActive" data-toggle="cantineorder" data-title="1" onclick="cantineorder()">OUI</a> -->
                                  </div>
                                  <input type="hidden" name="cantineorder" id="cantineorder" value="0">
                                  </div>
                                  </div>
                                  </div>
                                      </fieldset>
                                      <?php
                                    }

                                     ?>
                                    <div id="dynamic_field">

                                    </div>
                                    <!-- <div class="row">
                                        <div class="offset-md-3 col-md-9">

                                            <button type="button" class="btn btn-primary" id="cantineBtnAdd"> <i class="fa fa-plus"></i> Ajouter les frais de cantine </button>
                                        </div>
                                      </div> -->






                <div class="form-actions">
                                      <div class="row">
                                          <div class="offset-md-3 col-md-9">
                                            <?php
                                              if($nbsessionOn>0)
                                              {
                                                ?>
                              <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                                <?php
                                              }else {
                                                ?>
                              <button type="submit" class="btn btn-info" disabled><?php echo L::Saving ?></button>
                                                <?php
                                              }
                                             ?>

                                              <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                          </div>
                                        </div>
                                     </div>
              </div>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <!-- Material -->
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script>
 function SetcodeEtab(codeEtab)
 {
   var etape=3;
   $.ajax({
     url: '../ajax/sessions.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function addFrench()
 {
   var etape=1;
   var lang="fr";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function addEnglish()
 {
   var etape=1;
   var lang="en";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 $('#radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);

    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
 })

 function cantineorder()
 {

   $("#cantineorder").val(1);

   $('#montantMenscant').rules( "add", {
       required: true,
       messages: {
       required: "Merci de renseigner les frais mensuels"
   }
     });

     $('#montantTrimcant').rules( "add", {
         required: true,
         messages: {
         required: "Merci de renseigner les frais trimestriels"
     }
       });


       $('#montantAnncant').rules( "add", {
           required: true,
           messages: {
           required: "Merci de renseigner les frais annuels"
       }
         });

 }

 function cantineorderN()
 {

 $("#cantineorder").val(0);

$('#montantMenscant').rules('remove');
$('#montantTrimcant').rules('remove');
$('#montantAnncant').rules('remove');

 }

function AddcantinesRow()
{
  var nb=$("#nbcantine").val();
  var nouveau= parseInt(nb)+1;

  if(nouveau==1)
  {

      var ligne="<fieldset>";
      ligne=ligne+"<legend style=\"margin-left:280px\">FRAIS DE CANTINE</legend>";
      ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowMens"+nouveau+"\">";
      ligne=ligne+"<label class=\"control-label col-md-3\">Paiement Mensuel <span class=\"required\"> * </span></label>";
      ligne=ligne+"<div class=\"col-md-5\">";
      ligne=ligne+"<input type=\"text\"  id=\"mensuelmont"+nouveau+"\" name=\"mensuelmont"+nouveau+"\" class=\"floating-label mdl-textfield__input\"  placeholder=\"Renseigner les frais mensuels\">";
      ligne=ligne+"</div>";
      ligne=ligne+"</div>";

      ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowTrim"+nouveau+"\">";
      ligne=ligne+"<label class=\"control-label col-md-3\">Paiement trimestriel<span class=\"required\"> * </span></label>";
      ligne=ligne+"<div class=\"col-md-5\">";
      ligne=ligne+"<input type=\"text\"  id=\"trimestremont"+nouveau+"\" name=\"trimestremont"+nouveau+"\" class=\"floating-label mdl-textfield__input\"  placeholder=\"Renseigner les frais trimestriels\">";
      ligne=ligne+"</div>";
      ligne=ligne+"</div>";

      ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowAnn"+nouveau+"\">";
      ligne=ligne+"<label class=\"control-label col-md-3\">Paiement annuel<span class=\"required\"> * </span></label>";
      ligne=ligne+"<div class=\"col-md-5\">";
      ligne=ligne+"<input type=\"text\"  id=\"annuelmont"+nouveau+"\" name=\"annuelmont"+nouveau+"\" class=\"floating-label mdl-textfield__input\"  placeholder=\"Renseigner les frais annuels\">";
      ligne=ligne+"</div>";
      ligne=ligne+"</div>";

      ligne=ligne+"</fieldset>";

      $('#dynamic_field').append(ligne);

      $('#cantineBtnAdd').hide();

  }else {

  }

}



 jQuery(document).ready(function() {

   $("#typeclasse").select2();

   $('#cantineBtnAdd').click(function(){
            AddcantinesRow();
   });

   $("#FormAddClass").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{
        passad: {
            required: true,
            minlength: 6
        },
        confirmtad:{
            required: true,
            minlength: 6,
            equalTo:'#passad'
        },
        montantscola:{
                required: true,
                digits:true
            },
       montantscolaaff:{
          required: true,
          digits:true
                },
         montantinscrip:{
           required: true,
           digits:true
         },
        fonctionad:"required",
        typeclasse:"required",

        loginad:"required",
        emailad: {
                   required: true,
                   email: true
               },

        contactad:"required",
        datenaisad:"required",
        prenomad:"required",
        classe:"required",
        libetab:"required",
        section:"required"


      },
      messages: {
        confirmtad:{
            required:"<?php echo L::Confirmcheck?>",
            minlength:"<?php echo L::Confirmincheck?>",
            equalTo: "<?php echo L::ConfirmSamecheck?>"
        },
        montantscola:{
                required:"<?php echo L::PleaseEnterScolarityDues ?>",
                digits:"<?php echo L::DigitsOnly ?>"
            },
            montantinscrip:{
                required:"<?php echo L::PleaseEnterInscriptionDues ?> ",
                digits:"<?php echo L::DigitsOnly ?>"
            },
       montantscolaaff:{
         required:"<?php echo L::PleaseScolaritiesAmountAffectedStudent?>",
         digits:"<?php echo L::DigitsOnly?>"
                },
        passad: {
            required:"<?php echo L::Passcheck?>",
            minlength:"<?php echo L::Confirmincheck?>"
        },
        loginad:"<?php echo L::Logincheck?>",
        emailad:"<?php echo L::PleaseEnterEmailAdress?>",
        contactad:"<?php echo L::PleaseEnterPhoneNumber?>",
        datenaisad:"<?php echo L::PleaseEnterPhonestudentTab?>",
        prenomad:"<?php echo L::PleaseEnterPrename?>",
        nomad:"<?php echo L::PleaseEnterName?>",
        fonctionad:"<?php echo L::PleaseEnterFonction?>",
        typeclasse:"<?php echo L::SelecetClasseType ?>",
        classe:"<?php echo L::PleaseEnterclasseLib ?>",
        libetab:"<?php echo L::PleaseEtabselect?>",
        section:"Merci de selectionner la section de cette classe"
      },
      submitHandler: function(form) {
        //verifier si ce compte n'existe pas encore dans la base de données
           var etape=1;
           // var sectionid=$("#section").val();
           var session=$("#libellesession").val();
           var codeEtab="<?php echo $codeEtabAssigner;?>";
           $.ajax({
             url: '../ajax/classe.php',
             type: 'POST',
             async:false,
             data: 'classe=' + $("#classe").val()+ '&libetab='+codeEtab +'&etape=' + etape+'&session='+session,
             // data: 'classe=' + $("#classe").val()+ '&libetab='+codeEtab +'&etape=' + etape+'&section='+sectionid+'&session='+session,
             dataType: 'text',
             success: function (content, statut) {

               if(content==0)
               {
                 //le compte n'existe pas dans la base on peut l'ajouter

                 form.submit();
               }else {
                 //le compte existe dejà dans la base de données
                 Swal.fire({
                 type: 'warning',
                 title: '<?php echo L::WarningLib ?>',
                 text: '<?php echo L::ClasseAllreadyexiste ?>',

                 })

               }

             }


           });
      }


   });


 });
 </script>
    <!-- end js include path -->
  </body>

</html>
