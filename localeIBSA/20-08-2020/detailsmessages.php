<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
$agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$libellesessionencours="";

  if($etablissementType==1||$etablissementType==3)
  {
    if($nbsessionOn>0){
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];
      // $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
      // $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      // $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
    }

  }else {
    if($nbsessionOn>0)
    {
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];
      $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
      $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
    }
  }

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);

$_SESSION['user']['session']=$libellesessionencours;

$_SESSION['user']['codeEtab']=$codeEtabAssigner;

$notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

$destinataires=$etabs->getMessagesdestinaires($_GET['msg'],$codeEtabAssigner,$libellesessionencours);

// var_dump($destinataires);

//recuperation des informations du message

$messagesInfos=$etabs->getMessagesInformationsDetails($_GET['msg'],$codeEtabAssigner,$libellesessionencours);
$commentairesmessage="";
foreach ($messagesInfos as $value):
$commentairesmessage=$value->commentaire_msg;
endforeach;



// $notifications=$etabs->getAllMessagesLast($codeEtabAssigner,$libellesessionencours);




 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <style media="screen">
    /* .f30 {
font-size: 30px;
}
.text-primary {
color: #a768f3 !important;
}
.app-header .d-user span, .f12 {
font-size: 12px;
}

.wb-icon-box {
display: inline-block;
width: 50px;
height: 50px;
text-align: center;
line-height: 60px;
}
.f30 {
font-size: 30px;
}
.task-d-list {
margin-bottom: 0;
}
.custom-checkbox .custom-control-indicator {
border-radius: .25rem;
}
.custom-control-indicator {
position: absolute;
top: .25rem;
left: 0;
display: block;
width: 1rem;
height: 1rem;
pointer-events: none;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
background-color: #ddd;
background-repeat: no-repeat;
background-position: center center;
background-size: 50% 50%;
}
.task-d-list li:hover .task-list-action {
opacity: 1;
}
.task-d-list li .task-list-action {
opacity: 0;
}
.task-d-list li {
margin-bottom: 14px;
}
.quick-links-grid {
display: inline-block;
width: 100%;
}
.quick-links-grid .ql-grid-item {
display: inherit;
width: 49.3%;
padding: 20px 5px;
text-align: center;
vertical-align: middle;
text-decoration: none;
color: #45567c;
}
.quick-links-grid .ql-grid-item i {
-webkit-transition: all .3s;
transition: all .3s;
text-align: center;
color: #d2d6eb;
font-size: 32px;
}
.quick-links-grid .ql-grid-item .ql-grid-title {
display: block;
margin: 10px 0 0;
text-align: center;
font-size: 12px;
font-weight: 400;
line-height: 1;
}
.f60 {
font-size: 60px;
}
.table-vertical-middle tr td {
vertical-align: middle;
}
.table td, .table th {
padding: 0.6 rem 1rem;
}
.border, .table td, .table th {
border-color: #e5e9ec !important;
}
.table tfoot th, .table thead th {
vertical-align: bottom;
border-bottom: none;
}
.table thead th {
border-top: none;
}
.card .card-header .card-title {
	margin-bottom: 0;
	color: #53505f;
	font-size: 18px;
	font-weight: 500;
}
.lobicard-custom-icon > .card-header .dropdown, .card-shadow .card-header .dropdown, .lobicard-custom-control .card-header .dropdown {
	display: inline-block;
	float: right;
	position: relative;
}
.lobicard-custom-icon > .card-header .dropdown .tools, .card-shadow .card-header .dropdown .tools, .lobicard-custom-control .card-header .dropdown .tools {
	margin: 0px;
}
.card-shadow {
	border: none;
	box-shadow: 0 1px 10px 1px rgba(115,108,203,.1);
}
.card .card-header {
	padding: 1rem;
	border-bottom: 1px solid #e5e9ec;
	background: #fff;
}
.nav-pills-sm .nav-link {
	padding: .4em 1em;
	font-size: 12px;
}
.nav-pill-custom .nav-link, .nav-pills-sm .nav-link {
	border-radius: 30px !important;
}
.lobicard-custom-control .card-header .dropdown .tools {
	margin: 0px;
}
.lobicard-custom-control .card-header .dropdown .tools a {
	color: #fff;
} */
    </style>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::MessagesDetails ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li>&nbsp;<a class="parent-item" href="allmessages.php"><?php echo L::NotificationMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::MessagesDetails ?></li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
          <div class="row ">


              </div>
					<div class="state-overview">

						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


                 <div class="row">
       						<div class="col-md-12">
       							<!-- BEGIN PROFILE SIDEBAR -->


       								<div class="card">
       									<div class="card-head card-topline-aqua">
       										<!-- <header>About Course</header> -->
       									</div>
       									<div class="card-body no-padding height-9">
       										<div class="profile-desc">
       											<?php echo $commentairesmessage; ?>
       										</div>
       										</div>
       								</div>

       						</div>
                  <div class="col-md-12">
                            <div class="card card-box">
                                <div class="card-head">
                                    <header><?php echo L::DestinatairesLists ?></header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">
                                    <table id="example1" class="display" style="width:100%;">
                                        <thead>
                                            <tr>
                                                <th><?php echo L::Name ?></th>
                                                <th><?php echo L::PreName ?></th>
                                                <th><?php echo L::Etat ?></th>
                                                <th><?php echo L::Lecturedate ?></th>
                                                <th><?php echo L::Actions ?></th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                          foreach ($destinataires as $value):
                                            $compteid=$value->idcompte_lec;
                                            $datas=$etabs->getcompteInfos($compteid);
                                            foreach ($datas as $values):
                                              $nom=$values->nom_compte;
                                              $prenom=$values->prenom_compte;
                                            endforeach;
                                            $statut=$value->lecture_lec;
                                           ?>
                                            <tr>
                                                <td><?php echo $nom ?></td>
                                                <td><?php echo $prenom ?></td>
                                                <td>
                                                  <?php
                                                  if($statut==0)
                                                  {
                                                    ?>
                                                  <span class="label label-sm label-danger" style=""><?php echo L::LectureNone; ?></span>
                                                    <?php
                                                  }else {
                                                    ?>
                                               <span class="label label-sm label-success" style=""><?php echo L::LectureYes; ?></span>
                                                    <?php
                                                  }
                                                   ?>
                                                </td>
                                                <td>
                                                  <?php
                                                  if($statut>0)
                                                  {
                                                    ?>
                                                    <span><?php echo date_format(date_create($value->datecrea_lec),"d/m/Y"); ?></span>
                                                    <?php
                                                  }
                                                   ?>
                                                </td>
                                                <td>
                                                  <?php
                                                  if($statut==0)
                                                  {
                                                    ?>
                                                    <a href="#" class="btn btn-warning btn-xs" title="<?php echo L::Relance ?>"> <i class="fa fa-refresh"></i> </a>
                                                    <?php
                                                  }
                                                   ?>
                                                </td>

                                            </tr>
                                            <?php
                                          endforeach;
                                             ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
       					</div>




                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
  <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- morris chart -->

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>

     <script src="../assets2/plugins/chart-js/Chart.bundle.js" ></script>
     <script src="../assets2/plugins/chart-js/utils.js" ></script>
     <!-- <script src="../assets2/js/pages/chart/chartjs/chartjs-data.js" ></script> -->
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
   <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
     <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   function SetcodeEtab(codeEtab)
   {
     var etape=3;
     $.ajax({
       url: '../ajax/sessions.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }



   $(document).ready(function() {

     $('#example1').DataTable( {
         "scrollX": true

     } );



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
