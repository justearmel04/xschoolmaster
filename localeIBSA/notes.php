<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Student.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

  if($_SESSION['user']['groupe']==1)
  {
    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="DAF"||$_SESSION['user']['fonctionuser']=="Intendance"||$_SESSION['user']['fonctionuser']=="Marketing"||$_SESSION['user']['fonctionuser']=="CAES"||$_SESSION['user']['fonctionuser']=="CD"||$_SESSION['user']['fonctionuser']=="CC")
    {
      // $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);

      //nous allons chercher la liste des etablissements du groupe

      $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
        }

        if(strlen($_SESSION['user']['codeEtab'])>0)
        {

        }else {
          $_SESSION['user']['codeEtab']=$codeEtabAssigner;
        }

        $_SESSION['user']['session']=$libellesessionencours;





         // var_dump($allcodeEtabs);

        $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

        $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


    }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
    {
      // $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);
      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
          $_SESSION['user']['session']=$libellesessionencours;

          if(strlen($_SESSION['user']['codeEtab'])>0)
          {

          }else {
            $_SESSION['user']['codeEtab']=$codeEtabAssigner;
          }

          $notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

          // $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
          $classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);
          $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);
        }

    }
  }
  $typesemestrealls=$session->getAllsemestrebyIdsession($sessionencoursid);

  // var_dump($allcodeEtabs);
  $nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
// echo $libellesessionencours;
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
     <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>


	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::RecapNotesMenucompletEval ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><!--i class="fa fa-home"></i-->&nbsp;<a class="parent-item" href="index.php"><?php echo L::Evaluations ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::RecapNotesMenucompletEval ?>  </li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">
              <?php

                    if(isset($_SESSION['user']['addattendailyok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
            <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: '<?php echo L::Felicitations ?>',
                text: '<?php echo $_SESSION['user']['addattendailyok']; ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addattendailyok']);
                    }

                     ?>
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

<br/>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="card card-topline-green">
                   <div class="card-head">
                       <header><?php echo L::RecapNotesMenucompletEval?></header>
                       <div class="tools">
                           <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                       </div>
                   </div>
                   <div class="card-body ">
                     <form method="post" id="FormNotes" action="notes.php">
                         <div class="row">
                           <div class="col-md-6 col-sm-6">
                           <!-- text input -->
                           <div class="form-group" style="margin-top:8px;">
                               <label><?php echo L::ClasseMenu ?></label>
                               <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                               <select class="form-control input-height" id="classeEtab" name="classeEtab" style="width:100%;" onchange="searchmatiere()" >
                                   <option value=""><?php echo L::Selectclasses ?></option>
                                   <?php
                                   $i=1;
                                     foreach ($classes as $value):
                                          $nbclassestudents=$classe->getAllStudentOfThisClassesNb($value->id_classe,$value->session_classe);
                                          if($nbclassestudents>0)
                                          {
                                            ?>
 <option <?php if(isset($_POST['search'])&&($_POST['classeEtab']==$value->id_classe)){echo "selected";} ?> value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>
                                            <?php
                                          }
                                     ?>


                                     <?php
                                                                      $i++;
                                                                      endforeach;
                                                                      ?>

                               </select>
                           </div>


                       </div>
                       <div class="col-md-6 col-sm-6">
                       <!-- text input -->
                       <div class="form-group" style="margin-top:8px;">
                         <label><?php echo L::MatiereMenusingle ?></label>
                           <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                           <select class="form-control input-height" id="matclasse" name="matclasse" style="width:100%;" onchange="searchDesignation()" >
                             <option value=""><?php echo L::SelectSubjects ?></option>


                           </select>
                           <input type="hidden" name="notetype" id="notetype" value="1">
                       </div>


                   </div>

                   <div class="col-md-6 col-sm-6">
                   <!-- text input -->
                   <div class="form-group" style="margin-top:8px;">
                       <label><?php echo L::Trimestre ?></label>
                       <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->

                       <select class="form-control input-height" id="typesess" name="typesess" style="width:100%" >
                           <option value=""><?php echo L::selectTrimestre ?></option>
                           <?php
                           $i=1;
                             foreach ($typesemestrealls as $value):
                             ?>
                             <option value="<?php echo $value->id_semes?>" ><?php echo utf8_encode(utf8_decode($value->libelle_semes)) ?></option>

                             <?php
                                                              $i++;
                                                              endforeach;
                                                              ?>

                       </select>
                   </div>


               </div>


                   <!-- <div class="col-md-6 col-sm-6">

                   <div class="form-group" style="margin-top:8px;">
                     <label><?php echo L::Evaluation ?></label>
                     <input type="text" id="libctrl" name="libctrl"  value="" style="width:100%;">
                   </div>


               </div> -->

                       <div class="col-md-3 col-sm-3">
                       <!-- text input -->
                       <!--div class="form-group">
                           <label style="margin-top:3px;">Date</label>
                           <input type="text" id="datepre" name ="datepre" class="floating-label mdl-textfield__input"  placeholder="Date présence">
                           <input type="hidden" name="search" id="search" />
                       </div-->
                       <input type="hidden" name="search" id="search" />
                       <button type="submit" class="btn btn-success" onclick="affichage()" style="width:200px;height:35px;margin-top:35px;text-align:center;"><?php echo L::DescribNotes ?></button>


                   </div>


                         </div>


                     </form>
                   </div>
               </div>
                        </div>

          </div>
<!-- element à faire apparaitre au clique du bouton rechercher -->
<div class="row" style="" id="affichage">
  <?php
      if(isset($_POST['search']))
      {
          // if(isset($_POST['notetype'])&&isset($_POST['classeEtab'])&&isset($_POST['libctrl']))
          // {


              //nous devons recupérer la liste des elèves de cette classe ainsi que leur note

              $students=$student->getAllstudentofthisclassesSession($_POST['classeEtab'],$libellesessionencours);


            $datas=$student->getAllsnotesofStudentinThisMatieres($_POST['classeEtab'],$_SESSION['user']['codeEtab'],$libellesessionencours,$_POST['matclasse'],$_POST['typesess']);



              $classeInfos=$classe->getInfosofclassesbyId($_POST['classeEtab'],$libellesessionencours);

              // var_dump($datas);


          ?>

          <div class="offset-md-4 col-md-4"  id="affichage1">
            <div class="card" style="">
            <div class="card-body">
              <h5 class="card-title"></h5>
              <h4 style="font-style: italic;font-weight: bold;text-align:center;font-size:font-size: large;"><?php echo L::NotesClassingevaluation ?></h4>
              <p class="card-text" style="text-align:center;font-weight: bold;"><?php echo $classeInfos; ?></p>
              <p class="card-text" style="text-align:center;"></p>

            </div>
          </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="affichage2">
                                  <div class="card card-topline-green">
                                      <div class="card-head">
                                          <header><?php //echo L::ListNotesClasse ?> <?php //echo $classeInfos; ?></header>
                                          <div class="tools">
                                              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                      <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                      <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                          </div>
                                      </div>
                                      <div class="card-body ">

                                        <form method="post" action="#" id="FormAttendance">
                                  <table class="table table-striped table-bordered table-hover table-checkable order-column full-width"  id="affichage3">
                                    <thead>
                                        <tr>
                                          <!-- <th>
                                                  <label class="rt-chkbox rt-chkbox-single rt-chkbox-outline">
                                                      <input type="checkbox" class="group-checkable" value="1" id="allcheckbox" name="allcheckbox" checked onclick="checkboxclick()" />
                                                      <span></span>
                                                  </label>
                                              </th> -->
                                          <!--th style="width:15%"> Matricule </th-->
                                          <th style="width:40%"> <?php echo L::NamestudentTab ?> </th>

                                          <th style="text-align:center;width:30%"> <?php echo L::Evaluation ?> </th>
                                          <th style="width:20%;text-align:center"> <?php echo L::Notelibelle ?> </th>


                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php
                                      $matricules="";
                                      $j=0;
                                      $i=1;

                                      foreach ($datas as $value):

                                        $matricules=$matricules."*".$value->matricule_eleve;
                                      ?>
                                      <tr class="odd gradeX">
                                          <!--td>
                                            <?php //echo $i;?>
                                          </td-->
                                          <!-- <td>
                                                    <label class="rt-chkbox rt-chkbox-single rt-chkbox-outline">
                                                        <input type="checkbox" class="checkboxes" id="checkbox<?php echo $value->matricule_eleve;?>" name="checkbox<?php echo $value->matricule_eleve;?>" value="1" checked onclick="studentcheckbox('<?php echo $value->matricule_eleve;  ?>')" />
                                                        <span></span>
                                                    </label>
                                                </td> -->
                                          <td style="text-align:center">
                                              <a href="#"><?php echo $value->nom_eleve." ".$value->prenom_eleve;?> </a>
                                          </td>

                                          <td>

                                          <span class="label label-primary label-md"><?php echo $value->libelle_notes ?></span>



                                                <p id="messageObservE<?php echo $value->idcompte_eleve;?>"></p>
                                          </td>
                                          <td style="text-align:center">

                                            <?php echo $value->valeur_notes;?>

<!-- <input style='text-align:center' type="number" min=0 max=20 name="noteE<?php echo $value->idcompte_eleve;?>" id="noteE<?php echo $value->idcompte_eleve;?>" value="<?php echo $value->valeur_notes;?>" style="width:100px" onclick="erasedNote(<?php echo $value->idcompte_eleve;?>)" readonly/> -->



                                              <p id="messageNoteE<?php echo $value->idcompte_eleve;?>"></p>
                                          </td>
                                          <!-- td>
                                            <a href="updatenotes.php?noteid=<?php //echo $value->id_notes ?>&tyepenote=<?php //echo $value->idcompte_eleve;?>&eleveid=<?php //echo $value->type_notes;?>"  class="btn btn-warning btn-xs">
                                              <i class="material-icons">edit</i>
                                            </a>
                                             <!-- <a href="#"  class="btn btn-warning btn-xs" data-toggle="modal" data-target="#largeModel">
                                              <i class="material-icons">edit</i>
                                            </a> -->

                                          <!-- </td-->

        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
        <!-- <script src="../assets2/plugins/select2/js/select2.js" ></script>
        <script src="../assets2/js/pages/select2/select2-init.js" ></script> -->
        <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
        <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
        <script type="text/javascript">

        function check(id)
        {

          var note=$("#note"+id).val();
          var obser=$("#obserE"+id).val();

            if(note==""||obser=="")
            {
                if(note=="")
                {
document.getElementById("messageCont"+id).innerHTML = "<font color=\"red\"><?php echo L::PleaseAddNotes ?></font>";
                }

                if(obser=="")
                {
document.getElementById("messageObservE"+id).innerHTML = "<font color=\"red\"><?php echo L::PleaseAddObservations ?></font>";
                }
                //event.preventDefault();
            }else {
              //document.location.href="../controller/notes.php?etape=3";
              //authorise();
              // alert("bonjour");
              $("#UpdateNote"+id).submit();
            }




        }

        function erasedNote(id)
        {
          document.getElementById("messageCont"+id).innerHTML = "";
        }

        function erasedObser(id)
        {
          document.getElementById("messageObservE"+id).innerHTML = "";
        }
        function authorise()
        {
          $("#UpdateNote<?php echo $someArray[0]["valeur_notes"];?>").submit();
        }

        </script>

                                      </tr>

                                        <?php
                                           $i++;
                                           $j++;
                                               endforeach;
                                             ?>



                                    </tbody>
                                  </table>

                                  <?php
                                  //echo $matricules;
                                  $tabMat=explode("*",$matricules);
                                  $nb=count($tabMat);



                                  ?>
                                  <input type="hidden" name="studentmat" id="studentmat" value="<?php echo $matricules;?>"/>
                                  <input type="hidden" name="retirestudentmat" id="retirestudentmat" value="">
                                  <input type="hidden" name="studentmataafiificher" id="studentmataafiificher" value="<?php echo $matricules;?>">
                                  <input type="hidden" name="etape" id="etape" value="1"/>
                                  <input type="hidden" name="nbstudent" id="nbstudent" value="<?php echo $nb;?>"/>
                                  <input type="hidden" name="allpresent" id="allpresent" value=""/>
                                  <input type="hidden" name="classeId" id="classeId" value="<?php echo $_POST['classeEtab']?>"/>
                                  <input type="hidden" name="datePresence" id="datePresence" value="<?php echo $_POST['datepre']?>"/>
                                  <center>
                                    <!-- <button type="button"  class="btn btn-primary btn-xs"> <i class="material-icons f-left">print</i> <?php echo L::Printer ?> </button> -->
                                    <?php
                                    // if($_POST['notetype']==1)
                                    // {
                                      ?>
  <!--a href="updatenoteclasses.php?classe=<?php  //echo $_POST['classeEtab'] ?>&controle=<?php //echo $controleid ?>&matiere=<?php //echo $matiereid ?>&teatcher=<?php //echo $teatcherid ?>"  class="btn btn-warning btn-xs" id="btnupdate"> <i class="material-icons f-left">edit</i> Modifier </a-->

  <!-- <a href="#"  class="btn btn-warning btn-xs" id="btnupdate" onclick="updatecontrole(<?php echo $_POST['classeEtab'];  ?>,<?php echo $controleid  ?>,<?php echo $matiereid  ?>,<?php echo $teatcherid  ?>)"> <i class="material-icons f-left">edit</i> <?php echo L::ModifierBtn ?> </a> -->

                                      <?php
                                    // }else if($_POST['notetype']==2)
                                    // {
                                      ?>
                                        <!--a href="updatenoteclasses.php?classe=<?php  //echo $_POST['classeEtab'] ?>&examen=<?php //echo $examid ?>&matiere=<?php //echo $matiereid ?>&teatcher=<?php //echo $teatcherid ?>" id="btnupdate" class="btn btn-warning btn-xs"> <i class="material-icons f-left">edit</i> Modifier </a-->
  <!-- <a href="#" id="btnupdate" class="btn btn-warning btn-xs" onclick='updateexamen(<?php echo $_POST['classeEtab'];  ?>,<?php echo $examid ?>,<?php echo $matiereid ?>,<?php echo $teatcherid ?>)'> <i class="material-icons f-left">edit</i> <?php echo L::ModifierBtn ?> </a> -->
                                      <?php

                                    // }

                                     ?>

                                  </center>
                                </form>

                              </div>
                                  </div>
                              </div>


          <?php
      }
   ?>



          </div>

<!-- fin affichage du bouton rechercher -->

                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
      <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   // function SetcodeEtab(codeEtab)
   // {
   //   var etape=3;
   //   $.ajax({
   //     url: '../ajax/sessions.php',
   //     type: 'POST',
   //     async:false,
   //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
   //     dataType: 'text',
   //     success: function (content, statut) {
   //
   // window.location.reload();
   //
   //     }
   //   });
   // }
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

<?php
  if(isset($_POST))
  {
    ?>
    function touspresent(idclasse)
    {

       var nb="<?php echo @$nb;?>";
       var matricules="<?php echo @$matricules?>";

       var array=matricules.split('*');

       for(i=1;i<nb;i++)
       {
         //alert(array[i]);
         $("statut"+array[i]).val(1);
         //$("P"+array[i]).prop("checked", true);
         document.getElementById("P"+array[i]).checked = true;

       }
       $("#allpresent").val(1);

    }

    function tousabsent(idclasse)
    {
      var nb="<?php echo @$nb;?>";
      var matricules="<?php echo @$matricules?>";

      var array=matricules.split('*');

      for(i=1;i<nb;i++)
      {
        $("statut"+array[i]).val(0);
        document.getElementById("A"+array[i]).checked = true;

      }
      $("#allpresent").val(0);
    }
<?php
  }
 ?>

 function updatecontrole(classe,controleid,matiereid,teatcherid)
 {
    var matriculedelete=$("#retirestudentmat").val();
    var matriculeadd=$("#studentmataafiificher").val();
    var lien="updatenoteclasses.php?classe="+classe+"&controle="+controleid+"&matiere="+matiereid+"&teatcher="+teatcherid+"&matriculedelete="+matriculedelete;
    var etape=3;
    //determiner la les ids des eleves

    $.ajax({

      url: '../ajax/designation.php',
      type: 'POST',
      async:true,
       data: 'datas=' + matriculeadd+ '&etape=' + etape,
       dataType: 'text',
       success: function (content, statut) {

           var lien="updatenoteclasses.php?classe="+classe+"&controle="+controleid+"&matiere="+matiereid+"&teatcher="+teatcherid+"&matriculedelete="+content;
           document.location.href=lien;
       }

    });

    // document.location.href=lien;

 }

 function updateexamen(classe,examid,matiereid,teatcherid )
 {
   var matriculedelete=$("#retirestudentmat").val();
   var matriculeadd=$("#studentmataafiificher").val();
   var lien="updatenoteclasses.php?classe="+classe+"&examen="+examid+"&matiere="+matiereid+"&teatcher="+teatcherid+"&matriculedelete="+matriculedelete;

   var etape=3;
   //determiner la les ids des eleves

   $.ajax({

     url: '../ajax/designation.php',
     type: 'POST',
     async:true,
      data: 'datas=' + matriculeadd+ '&etape=' + etape,
      dataType: 'text',
      success: function (content, statut) {

        var lien="updatenoteclasses.php?classe="+classe+"&examen="+examid+"&matiere="+matiereid+"&teatcher="+teatcherid+"&matriculedelete="+content;

          document.location.href=lien;
      }

   });
 }

 function ajoutermatricule(mat)
 {
   var matricules=$("#retirestudentmat").val();
   var contactmatricules=matricules+"*"+mat;
   $("#retirestudentmat").val(contactmatricules);
   $("#studentmataafiificher").val($("#studentmataafiificher").val().replace("*"+mat,""));
 }

 function retirermatricule(mat)
 {
    var matricules=$("#retirestudentmat").val();
    var addmat=$("#studentmataafiificher").val();
    var concataddmat=addmat+"*"+mat;
    var contactmatricules=matricules+"*"+mat;
    $("#retirestudentmat").val($("#retirestudentmat").val().replace("*"+mat,""));
    $("#studentmataafiificher").val(concataddmat);
 }

 function studentcheckbox(mat)
 {
   var checkbox=$("#checkbox"+mat).val();

   if(checkbox==0)
   {
     // alert("coché");
     $("#checkbox"+mat).val(1);
     //retirer le matricule a la liste
     retirermatricule(mat);
   }else if(checkbox==1)
   {
     // alert("décoché");
     $("#checkbox"+mat).val(0);
     //ajouter le matricule a la liste

     ajoutermatricule(mat);
   }
 }

    function checkboxclick()
    {
      var allcheckbox=$("#allcheckbox").val();
      var etape=9;

      if(allcheckbox==0)
      {
        // alert("coché");
        $("#allcheckbox").val(1);
        //la liste des eleves(matricules)
        var matricules=$("#studentmat").val();
        var tabstudent=matricules.split("*");

        $.ajax({

          url: '../ajax/determination.php',
          type: 'POST',
          async:true,
           data: 'datas=' + matricules+ '&etape=' + etape,
           dataType: 'text',
           success: function (content, statut) {

             var taille=content;
            for(var i=1;i<taille;i++)
            {
                $("#checkbox"+tabstudent[i]).attr('checked', true);
                $("#checkbox"+tabstudent[i]).val(1);
                $("#retirestudentmat").val("");
            }

           }

        });

      }else if(allcheckbox==1)
      {
        // alert("décoché");
        $("#allcheckbox").val(0);
        //la liste des eleves(matricules)
        var matricules=$("#studentmat").val();
        var tabstudent=matricules.split("*");
        $.ajax({

          url: '../ajax/determination.php',
          type: 'POST',
          async:true,
           data: 'datas=' + matricules+ '&etape=' + etape,
           dataType: 'text',
           success: function (content, statut) {

             var taille=content;
            for(var i=1;i<taille;i++)
            {
                $("#checkbox"+tabstudent[i]).attr('checked', false);
                $("#checkbox"+tabstudent[i]).val(0);
                $("#retirestudentmat").val(matricules);
                $("#btnupdate").prop('disabled', false);

            }

           }

        });
      }
    }

   function present(id)
   {
    $("statut"+id).val(1);
   }

   function absent(id)
   {
    $("statut"+id).val(0);
   }

   $('#example5').DataTable( {
       "scrollX": true

   } );
   $("#classeEtab").select2();
   // $("#notetype").select2();
    $("#typesess").select2();
   // $("#libctrl").select2();
   $("#matclasse").select2();
   $('#datepre').bootstrapMaterialDatePicker
   ({
     date: true,
     time: false,
     format: 'DD/MM/YYYY',
     lang: 'fr',
     minDate : new Date(),
    cancelText: '<?php echo L::AnnulerBtn ?>',
    okText: '<?php echo L::Okay ?>',
    clearText: '<?php echo L::Eraser ?>',
    nowText: '<?php echo L::Now ?>'

   });

   function searchDesignation()
   {
     var classeEtab=$("#classeEtab").val();
     var codeEtab="<?php echo $_SESSION['user']['codeEtab'];  ?>";
     var matiere=$("#matclasse").val();
     // var typenote=$("#notetype").val();
     var typenote=1;;
     var etape=1;
     //nous allons verifier si nous avons des notes pour ce controle ou examen
     $.ajax({

       url: '../ajax/designation.php',
       type: 'POST',
       async:true,
        data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab+'&matiere='+matiere+'&typenote='+typenote,
        dataType: 'text',
        success: function (content, statut) {

          $("#libctrl").html("");
          $("#libctrl").html(content);

        }

     });

   }

   function searchmatiere()
   {
     var classeEtab=$("#classeEtab").val();
     var codeEtab="<?php echo $_SESSION['user']['codeEtab'];  ?>";
     var etape=2;

     $.ajax({

       url: '../ajax/matiere.php',
       type: 'POST',
       async:true,
        data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab,
        dataType: 'text',
        success: function (content, statut) {

          $("#matclasse").html("");
          $("#matclasse").html(content);

          // searchDesignation();

        }

     });

   }

function searchNotesClasses()
{
  // var notetype=$("#notetype").val();
  var notetype=1;
  var classeEtab=$("#classeEtab").val();
  var codeEtab="<?php echo $_SESSION['user']['codeEtab'];  ?>";

  //nous allons rechercher la liste des designation de notes par classe

    if(notetype==""||classeEtab=="")
    {
      if(notetype=="")
      {
        Swal.fire({
        type: 'warning',
        title: '<?php echo L::WarningLib ?>',
        text: "<?php echo L::PleaseSelectnotetyperequired ?>",

      })
      }

      if(classeEtab=="")
      {
        Swal.fire({
        type: 'warning',
        title: '<?php echo L::WarningLib ?>',
        text: "<?php echo L::PleaseSelectclasseOnerequired ?>",

      })
      }
    }else {

        if(notetype==1)
        {
          var etape=1;
        }else if(notetype==2){
          var etape=2;
        }

      $.ajax({

        url: '../ajax/notesSearch.php',
        type: 'POST',
        async:true,
         data: 'notetype=' + notetype+ '&etape=' + etape+'&classeEtab='+classeEtab+'&codeEtab='+codeEtab,
         dataType: 'text',
         success: function (content, statut) {

           $("#libctrl").html("");
           $("#libctrl").html(content);

         }

      });
    }



}

   $(document).ready(function() {

//
$("#FormNotes").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
 },
 highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
 },
 success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    classeEtab:"required",
    datepre:"required",
    notetype:"required",
    classeEtab:"required",
    libctrl:"required",
    matclasse:"required",
    typesess:"required"



  },
  messages: {
    classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
    datepre:"<?php echo L::PleaseSelectdateprerequired ?>",
    notetype:"<?php echo L::PleaseSelectnotetyperequired ?>",
    // classeEtab:"<?php //echo L::	PleaseSelectclasseOnerequired ?>",
    libctrl:"<?php echo L::DesignationSelectedrequired ?>",
    matclasse:"<?php echo L::SubjectSelectedrequired ?>",
     typesess:"<?php echo L::pleaseselecttrimestre ?>"

  },
  submitHandler: function(form) {
    form.submit();
  }
});


$("#FormAttendance").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
  },
  highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
  },
  success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    classeEtab:"required",
    datepre:"required"



  },
  messages: {
    classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
    datepre:"<?php echo L::PleaseSelectdateprerequired ?>"

  },
  submitHandler: function(form) {
    //form.submit();
    //classeId
//datePresence
    var etape=1;


    $.ajax({

      url: '../ajax/attendance.php',
      type: 'POST',
      async:false,
      data: 'classe=' + $("#classeId").val()+'&datepre='+$("#datePresence").val()+'&etape='+etape,
      dataType: 'text',
      success: function (content, statut)
      {
          if(content==0)
          {
            form.submit();

          }else if(content==1) {
            Swal.fire({
            type: 'warning',
            title: '<?php echo L::WarningLib ?>',
            text: "<?php echo L::PresenceAllreadyExiste ?>",

          })
        }
      }

    });
  }


});


   });

   </script>
    <!-- end js include path -->
  </body>

</html>
