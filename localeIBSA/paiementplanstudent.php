<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$student=new Student();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

  if($_SESSION['user']['groupe']==1)
  {
    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="DAF"||$_SESSION['user']['fonctionuser']=="Intendance"||$_SESSION['user']['fonctionuser']=="Marketing"||$_SESSION['user']['fonctionuser']=="CAES"||$_SESSION['user']['fonctionuser']=="CD"||$_SESSION['user']['fonctionuser']=="CC")
    {
      // $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);

      //nous allons chercher la liste des etablissements du groupe

      $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
        }

        if(strlen($_SESSION['user']['codeEtab'])>0)
        {
          if(strlen($_GET['codeEtab'])>0)
          {
            $_SESSION['user']['codeEtab']=$_GET['codeEtab'];
          }

        }else {
          $_SESSION['user']['codeEtab']=$codeEtabAssigner;
        }

        $_SESSION['user']['session']=$libellesessionencours;

         // var_dump($allcodeEtabs);

         $allstudentschools=$student->getAllStudentOfThisSchool($codeEtabAssigner);

        $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

        $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);

        $students=$student->getAllStudentOfClassesId($_GET['classe'],$libellesessionencours);

        // $classesEtab=$classe->

    }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
    {
      // $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);
      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
          $_SESSION['user']['session']=$libellesessionencours;

          if(strlen($_SESSION['user']['codeEtab'])>0)
          {
            $_SESSION['user']['codeEtab']=$_GET['codeEtab'];
          }else {
            $_SESSION['user']['codeEtab']=$codeEtabAssigner;
          }

          $notifications=$etabs->getAllMessagesending($_SESSION['user']['codeEtab'],$libellesessionencours);

          // $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);

            $classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);

          $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);
          $allstudentschools=$student->getAllStudentOfThisSchool($_SESSION['user']['codeEtab']);
            $students=$student->getAllStudentOfClassesId($_GET['classe'],$libellesessionencours);
        }

    }
  }

  $nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
  $onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
  $offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::Listedeseleves. " ".$classe->getInfosofclassesbyId($_GET['classe'],$libellesessionencours);  ?> </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="paiementplans.php"><?php echo L::ClassesMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::Listedeseleves ?></li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['updateteaok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: '<?php echo L::Felicitations  ?>',
            text: '<?php echo $_SESSION['user']['updateteaok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['updateteaok']);
                }

                 ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>



<div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line">
                               <!--ul class="nav nav-pills nav-pills-rose">
									<li class="nav-item tab-all"><a class="nav-link active show"
										href="#tab1" data-toggle="tab">Liste</a></li>
									<li class="nav-item tab-all"><a class="nav-link" href="#tab2"
										data-toggle="tab">Grille</a></li>
								</ul-->
                                <div class="tab-content">
                                    <div class="tab-pane active fontawesome-demo" id="tab1">
                                        <div class="row">
					                        <div class="col-md-12">
					                            <div class="card  card-box">
					                                <div class="card-head">
					                                    <header></header>
					                                    <div class="tools">
					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
					                                    </div>
					                                </div>


					                                  <div class="table-scrollable">
                                              <?php

                                               ?>
					                                    <table class="table table-hover table-checkable order-column full-width" id="example45">
					                                        <thead>
					                                            <tr>
                                                         <!-- <th> <?php //echo L::Pictures ?> </th> -->
					                                                <th> <?php echo L::MatriculestudentTab ?> </th>
					                                                <th> <?php echo L::NamestudentTab ?></th>
                                                           <th><?php echo L::SexestudentTab ?></th>
					                                                 <th class="noExport"> <?php echo L::Actions ?> </th>
					                                            </tr>
					                                        </thead>
					                                        <tbody>
                                                    <?php
                                                    $i=1;

$scolatitesaveser=0;
$scolaritesverser=0;
$inscriptionaverser=0;
$inscriptionverser=0;
// var_dump($students);
                                                      foreach ($students as $value):
                                                        $scolatitesaveser=$scolatitesaveser+$value->scolarite_classe;
                                                        $inscriptionaverser=$inscriptionaverser+$value->inscriptionmont_classe;
                                                        // echo $value->inscriptionmont_classe;
                                                      ?>
																<tr class="odd gradeX" >
                                  <?php
                                  //rechercher le total des verements scolarites de cet eleves
                                    $datasInscriptions=$etabs->getAllInscriptionVersmentChild($_SESSION['user']['codeEtab'],$libellesessionencours,$value->id_compte,$value->id_classe);
                                    $nbdatainscription=count($datasInscriptions);
                                    $sommeinscri=0;
                                      if($nbdatainscription==0)
                                      {
                                        $inscriptionverser=$inscriptionverser+0;
                                      }else if($nbdatainscription>0){

                                          foreach ($datasInscriptions as $valueinscrip):
                                            $sommeinscri=$sommeinscri+$valueinscrip->montant_versement;
                                          endforeach;
                                      }

                                      $inscriptionverser=$inscriptionverser+$sommeinscri;

                                      $datasScolarites=$etabs->getAllScolaritesVersmentChild($_SESSION['user']['codeEtab'],$libellesessionencours,$value->id_compte,$value->id_classe);
                                      // var_dump($datasInscriptions);
                                      $nbdatascolarites=count($datasScolarites);
                                      $sommeiscola=0;
                                      if($nbdatascolarites==0)
                                      {
                                        $scolaritesverser=$scolaritesverser+0;
                                      }else if($nbdatascolarites>0){

                                          foreach ($datasScolarites as $valuescola):
                                            $sommeiscola=$sommeiscola+$valuescola->montant_versement;
                                          endforeach;
                                      }

                                      $scolaritesverser=$scolaritesverser+$sommeiscola;
                                   ?>


																	<td style="width:150px;"><?php
                                  //echo $value->codeEtab_classe;

                                  echo $value->matricule_eleve;

                                  ?></td>

                                  <td>
                                    <?php
                                    $statut=$value->statut_classe;

                                    echo $value->nom_eleve." ".$value->prenom_eleve;

                                    ?>
                                </td>
																	<td class="left"><?php echo $value->sexe_compte;?></td>
                                  <td class="center">

                                    <?php
                                    //verifier si cet élève a un plan de paiement defini

                                    // echo $value->id_compte." /".$value->codeEtab_inscrip." /".$value->codeEtab_inscrip." /".$value->session_inscrip;

                                    $plans=$etabs->getplanstudentnumberNb($value->id_compte,$value->codeEtab_inscrip,$value->session_inscrip);
                                    // var_dump($plans);
                                     $nbplans="";
                                     foreach ($plans as $valueplan):
                                       $nbplans=$valueplan->plan_inscrip;
                                     endforeach;
                                     // echo $nbplans;
                                    if($nbplans==0)
                                    {
                                      ?>

                              <a href="#" data-toggle="modal" data-target="#largeModel" onclick="choosestudent(<?php echo $value->id_compte ?>,'<?php echo $value->codeEtab_inscrip ?>','<?php echo $value->session_inscrip ?>')"  class="btn btn-success btn-xs"> <i class="fa fa-plus"></i> </a>

                                      <?php
                                    }else {
                                      ?>
<a href="#" data-toggle="modal" data-target="#largeModel1" onclick="detailsplans(<?php echo $value->id_compte ?>,'<?php echo $value->codeEtab_inscrip ?>','<?php echo $value->session_inscrip ?>')" class="btn btn-warning btn-xs"> <i class="fa fa-info-circle"></i> </a>
                                      <?php
                                    }
                                     ?>



                                    <!-- <a href="#" class="btn btn-primary btn-xs"> <i class="fa fa-pencil"></i> </a> -->



																	</td>
																</tr>



                                <?php
                                                                 $i++;
                                                                 endforeach;
                                                                 ?>

															</tbody>

					                                    </table>
					                                    </div>
					                                </div>
					                            </div>
					                        </div>
					                    </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="largeModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xs" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="exampleModalLabel"><?php echo L::Paiementplan ?></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                              <div id="divrecherchestation">
                                <div class="table-scrollable">

                      </div>
                      <form class="" method="post" action="../controller/modalites.php" id="Modclientnumber">
                        <div class="form-group row">
                            <label class="control-label col-md-12"><?php echo L::Paiementplan ?>
                                <span class="required">  </span>
                            </label>
                            <div class="col-md-12">
                              <select class="form-control" name="paiementplan" id="paiementplan" style="width:100%" onchange="determinemodalites()">
                                <option value=""><?php echo L::PaiementplanSelectOne ?></option>
                                <option value="A"><?php echo L::PlanA ?></option>
                                <option value="B"><?php echo L::PlanB ?></option>
                                <option value="C"><?php echo L::PlanC ?></option>
                                <option value="D"><?php echo L::PlanD ?></option>
                              </select>

                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-12"><?php echo L::ScolariteBuyerBy ?>
                                <span class="required">  </span>
                            </label>
                            <div class="col-md-12">
                              <select class="form-control" name="payeur" id="payeur" style="width:100%" onchange="determinepayeur()">
                                <option value=""><?php echo L::PleasePayeurselect ?></option>
                                <option value="FAMILLE"><?php echo L::Moimem ?></option>
                                <option value="EMPLOYEUR"><?php echo L::EmployeurBuyer ?></option>

                              </select>

                            </div>

                        </div>
                        <div class="form-group row" id="employeurRow">
                            <label class="control-label col-md-12"><?php echo L::EmployeurLib ?>
                                <span class="required">  </span>
                            </label>
                            <div class="col-md-12">
                            <input type="text" class="form-control" name="employeurName" id="employeurName" value="">

                            </div>

                        </div>
                        <div class="form-group row" id="modalitesRow">
                            <label class="control-label col-md-12"><?php echo L::Modalitepaiementnumber ?>
                                <span class="required">  </span>
                            </label>
                            <div class="col-md-12">
                              <input type="text" class="form-control" name="nbmodalities" id="nbmodalities" value="" onchange="createline()">

                            </div>

                        </div>
                        <div id="dynamic_field">

                        </div>
                             <input type="hidden" name="studentid" id="studentid" value="">
                             <input type="hidden" name="codeEtab" id="codeEtab" value="">
                             <input type="hidden" name="sessionEtab" id="sessionEtab" value="">
                             <input type="hidden" name="classeid" id="classeid" value="<?php echo $_GET['classe']; ?>">
                             <input type="hidden" name="etape" id="etape" value="1">

                             <div class="">

                               <button type="submit" class="btn btn-primary btn-md" name="button"> <i class="fa fa-check"></i> <?php echo L::Validate; ?> </button>

                             </div>


                      </form>

                              </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closing()"><?php echo L::Closebtn  ?></button>
                                <button type="button" style="display:none" class="btn btn-primary" onclick="validerMember()" id="btnsaving"><?php echo L::Saving ?></button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="largeModel1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="exampleModalLabel"><?php echo L::Paiementplan ?></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                          <div id="divrecherchestation">
                            <div class="table-scrollable">
                              <table class="table table-striped table-bordered table-hover">
        <thead style="background-color:#28a745; color:white; font-weight: bold;">
        <tr>
        <th style=""><?php echo L::DateEcheance ?></th>
        <th class=""><?php echo L::MontantEcheance ?> </th>
        <th> <?php echo L::Etat ?> </th>
        <th style=""><?php echo L::payeur ?> </th>
        <!-- th style=""><?php //echo L::Actions ?> </th> -->
      </tr>
       </thead>
       <tbody id="tabStationBody">

      <tr id="RowInfosOld">

        <td colspan="4"><?php echo L::NoLigne ?></td>

      </tr>

                        </tbody>
                         </table>
                  </div>


                          </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closing()"><?php echo L::Closebtn  ?></button>
                            <button type="button" style="display:none" class="btn btn-primary" onclick="validerMember()" id="btnsaving"><?php echo L::Saving ?></button>
                        </div>
                    </div>
                </div>
            </div>

                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
 <script src="../cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" ></script>
 	<script src="../cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
 	<script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" ></script>
 	<script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" ></script>
 	<script src="../cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
<script src="../assets/js/formatter/jquery.formatter.min.js"></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   // function SetcodeEtab(codeEtab)
   // {
   //   var etape=3;
   //   $.ajax({
   //     url: '../ajax/sessions.php',
   //     type: 'POST',
   //     async:false,
   //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
   //     dataType: 'text',
   //     success: function (content, statut) {
   //
   // window.location.reload();
   //
   //     }
   //   });
   // }

   function detailsplans(idcompte,codeEtab,sessionEtab)
   {

     var etape=1;
        $.ajax({
          url: '../ajax/modalites.php',
          type: 'POST',
          async:false,
          data:'etape=' + etape+'&compte='+idcompte+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab,
          dataType: 'text',
          beforeSend: function (xhr) {

               // $("#imgLoader").css("display", "inline");
               $("#imgLoader").show();
           //     setTimeout(function () {
           //     $("#imgLoader").show();
           // }, 100);

             },
          success: function (content, statut) {
          // $("#nbtotalparent").val(0);
          $("#RowInfosOld").hide();
          $("#imgLoader").hide(2000);
          $("#tabStationBody").html("");
          $("#tabStationBody").html(content);

          }
        });
   }

function choosestudent(studentid,codeEtab,sessionEtab)
{
  $("#Modclientnumber #studentid").val(studentid);
  $("#Modclientnumber #codeEtab").val(codeEtab);
  $("#Modclientnumber #sessionEtab").val(sessionEtab);
}

function determinepayeur()
{
  var payeur=$("#payeur").val();
  if(payeur=="FAMILLE")
  {
$("#employeurRow").hide();
  }else {
$("#employeurRow").show();
  }
}

   function determinemodalites()
   {
     var paiementplan=$("#paiementplan").val();
     if(paiementplan=="D")
     {
       $("#modalitesRow").show();
     }else if(paiementplan=="A")
     {
       $("#dynamic_field").empty();
       $("#modalitesRow").hide();
       $("#nbmodalities").val(1);
      createRow(1);
       //nbmodalities
     }else if(paiementplan=="B")
     {
       $("#dynamic_field").empty();
       $("#modalitesRow").hide();
       $("#nbmodalities").val(3);
       createRow(3);
     }else if(paiementplan=="C")
     {
       $("#dynamic_field").empty();
       $("#modalitesRow").hide();
       $("#nbmodalities").val(10);
       createRow(1);
     }else if(paiementplan=="D")
     {
       $("#dynamic_field").empty();
       $("#modalitesRow").show();
       $("#nbmodalities").val(0);
     }
   }

   function createline()
   {
     var nb=$("#nbmodalities").val();
     if(nb>0)
     {
       $("#dynamic_field").empty();
       createRow(nb);
     }else {
       $("#dynamic_field").empty();
       Swal.fire({
                     type: 'warning',
                     title: '<?php echo L::WarningLib ?>',
                     text: "<?php echo L::Nbmodalitessuperieur ?>",

                   })
     }
   }

   function createRow(nb)
   {

     for(var i=1;i<=nb;i++)
     {
       var ligne="<div class=\"form-group row\" id=\"modalites"+i+"\">";
       ligne=ligne+"<label class=\"control-label col-md-12\"><?php echo L::DateEcheance ?> <span class=\"required\"> * </span></label>";
       ligne=ligne+"<div class=\"col-md-12\">";
       ligne=ligne+"<input type=\"text\"  id=\"date"+i+"\" name=\"date"+i+"\" class=\"floating-label mdl-textfield__input dateLib\"   >";
       ligne=ligne+"</div>";
       ligne=ligne+"</div>";

       ligne=ligne+"<div class=\"form-group row\">";
       ligne=ligne+"<label class=\"control-label col-md-12\"><?php echo L::MontantEcheance ?> <span class=\"required\"> * </span></label>";
       ligne=ligne+"<div class=\"col-md-12\">";
       ligne=ligne+"<input type=\"text\"  id=\"montant"+i+"\" name=\"montant"+i+"\" class=\"floating-label mdl-textfield__input montantLib\"   >";
       ligne=ligne+"</div>";
       ligne=ligne+"</div>";

$("#dynamic_field").append(ligne);
$('#montant'+i).rules( "add", {
    required: true,
    messages: {
    required: "<?php echo L::PleaseEnterMontantEcheance ?>"
}
  });

  $("#date"+i).rules( "add", {
      required: true,
      messages: {
      required: "<?php echo L::PleaseEnterdateEcheance ?>"
}
    });

    $("#date"+i).formatter({pattern:"{{99}}/{{99}}/{{9999}}"});
    $("#montant"+i).formatter({pattern:"{{999999999999}}"});


     }


   }

   function createRowC()
   {

   }

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function classeliste()
   {
     //listeEnseignant.php?$codeEtabAssigner=<?php //echo $codeEtabAssigner?>

     var url="listedeclasse.php?idClasse=<?php  echo $_GET['classe']?>&codeEtab=<?php echo $_SESSION['user']['codeEtab']?>&session=<?php echo $libellesessionencours ?>";

     window.open(url, '_blank');



   }

   function classelistesmall()
   {
     var url="listedeclasses.php?idClasse=<?php  echo $_GET['classe']?>&codeEtab=<?php echo $_SESSION['user']['codeEtab']?>&session=<?php echo $libellesessionencours ?>";

     window.open(url, '_blank');
   }

   function classelistesmallbig()
   {
     var url="listeclasses.php?idClasse=<?php  echo $_GET['classe']?>&codeEtab=<?php echo $_SESSION['user']['codeEtab']?>&session=<?php echo $libellesessionencours ?>";

     window.open(url, '_blank');
   }

   function searchlibetab()
   {

     var etape=2;

     $.ajax({
              url: '../ajax/school.php',
              type: 'POST',
              async:false,
              data: 'code='+ $("#codeetab").val()+'&etape='+etape,
              dataType: 'text',
              success: function (content, statut) {

                $("#libetab").html("");
                $("#libetab").html(content);

                //nous allons chercher la liste des admins local de cet etablissement

                $.ajax({
                         url: '../ajax/classe.php',
                         type: 'POST',
                         async:false,
                         data: 'code='+ $("#codeetab").val()+'&etape='+etape,
                         dataType: 'text',
                         success: function (response, statut) {


                           $("#classex").html("");
                           $("#classex").html(response);
                         }
                       });

              }

            });

   }
   $(document).ready(function() {

     // $(".dateLib").formatter({pattern:"{{99}}/{{99}}/{{9999}}"});
     // $(".montantLib").formatter({pattern:"{{999999999999}}"});

     $("#modalitesRow").hide();
     $("#employeurRow").hide();

     $("#paiementplan").select2();
     $("#payeur").select2();


     $('#example5').DataTable( {
         "scrollX": true

     } );

     $("#Modclientnumber").validate({



       errorPlacement: function(label, element) {

       label.addClass('mt-2 text-danger');

       label.insertAfter(element);

     },

     highlight: function(element, errorClass) {

       $(element).parent().addClass('has-danger')

       $(element).addClass('form-control-danger')



     },

     success: function (e) {

           $(e).closest('.control-group').removeClass('error').addClass('info');

           $(e).remove();

       },

        rules:{



          // matiere:"required",

          // classe:"required",

          // teatcher:"required",

          // coef:"required",

          paiementplan:"required",

          employeurName:"required",

          nbmodalities:"required",
          payeur:"required",
          deposant:"required",
          dupaie:"required",
          trajet:"required",
          transports:"required",
          deposantphone:"required",



          montvers:{

            'required': {

                depends: function (element) {

                    return ($('#montapayer').val() !=''|| $('#montrest').val() !='' );



                }

            }

          },





        },

        messages: {



            paiementplan:"<?php echo L::PleasePaiementplanSelect ?>",
            employeurName:"<?php echo L::PleaseEnterEmployeurName ?>",
            nbmodalities:"<?php echo L::PleaseEntermodalitiesNumber ?>",
            payeur:"<?php echo L::PleaseChooseTheBuyer ?>",
            deposant:"<?php echo L::PleaseEnterdeposantName ?>",
            modepaie:"<?php echo L::PleaseSelectPaiementMode ?>",
            dupaie:"<?php echo L::RequiredChamp ?>",
            trajet:"<?php echo L::Pleaseselecttrajet ?>",
            transports:"<?php echo L::PleaseTransportationFeesSelect ?>",
            deposantphone:"<?php echo L::Pleasephonedeposant ?>"





        },

        submitHandler: function(form) {





  // nous allons verifier un controle similaire n'existe pas



    form.submit();





               }





             });

     $('#example45').DataTable( {

       "scrollX": true,
       "language": {
           "lengthMenu": "_MENU_  ",
           "zeroRecords": "Aucune correspondance",
           "info": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
           "infoEmpty": "Aucun enregistrement disponible",
           "infoFiltered": "(filtered from _MAX_ total records)",
           "sEmptyTable":"Aucune donnée disponible dans le tableau",
            "sSearch":"Rechercher :",
            "oPaginate": {
       "sFirst":    "Premier",
       "sLast":     "Dernier",
       "sNext":     "Suivant",
       "sPrevious": "Précédent"
     }
     },

         dom: 'Bfrtip',
         buttons: [
             // 'copyHtml5',

             // 'excelHtml5',
             {
               extend: 'excelHtml5',
               title: 'Data export',
               exportOptions: {
                             columns: "thead th:not(.noExport)"
                         }
             }
             // 'csvHtml5',
             // 'pdfHtml5'
         ]
     } );

     $("#codeetab").select2();

     $("#libetab").select2();

   });

   </script>
    <!-- end js include path -->
  </body>

</html>
