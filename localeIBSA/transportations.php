<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

  if($_SESSION['user']['groupe']==1)
  {
    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="DAF"||$_SESSION['user']['fonctionuser']=="Intendance"||$_SESSION['user']['fonctionuser']=="Marketing"||$_SESSION['user']['fonctionuser']=="CAES"||$_SESSION['user']['fonctionuser']=="CD"||$_SESSION['user']['fonctionuser']=="CC")
    {
      // $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);

      //nous allons chercher la liste des etablissements du groupe

      $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
        }

        if(strlen($_SESSION['user']['codeEtab'])>0)
        {

        }else {
          $_SESSION['user']['codeEtab']=$codeEtabAssigner;
        }

        $_SESSION['user']['session']=$libellesessionencours;





         // var_dump($allcodeEtabs);

        $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

        $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


    }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
    {
      // $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);
      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
          $_SESSION['user']['session']=$libellesessionencours;

          if(strlen($_SESSION['user']['codeEtab'])>0)
          {

          }else {
            $_SESSION['user']['codeEtab']=$codeEtabAssigner;
          }

          $notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

          // $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
          $classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);
          $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);
        }

    }
  }

  $transportations=$etabs->getAlltransportationSession($_SESSION['user']['codeEtab'],$libellesessionencours);
  $nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
  // var_dump($allcodeEtabs);

// echo $libellesessionencours;
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title"><?php echo L::TransportationsManagements ?></div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">

                          <li><a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><?php echo L::TP ?>&nbsp;<i class="fa fa-angle-right"></i></li>
                          <li class="active"><?php echo L::Parameters ?></li>
                      </ol>
                  </div>
              </div>

              <?php

                    if(isset($_SESSION['user']['addctrleok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
              <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
              <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
                type: 'success',
                title: '<?php echo L::Felicitations ?>',
                text: '<?php echo $_SESSION['user']['addctrleok']; ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addctrleok']);
                    }

                     ?>




              <?php

                    if(isset($_SESSION['user']['deletesubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['deletesubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: '<?php echo L::Okay ?>',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })


                </script>
                      <?php
                      unset($_SESSION['user']['deletesubjectok']);
                    }

                     ?>


              <?php

                    if(isset($_SESSION['user']['updatesubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['updatesubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: '<?php echo L::Okay ?>',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })


                </script>
                      <?php
                      unset($_SESSION['user']['updatesubjectok']);
                    }

                     ?>
              <?php

                    if(isset($_SESSION['user']['addsubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>


                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['addsubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: '<?php echo L::Okay ?>',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })
                </script>
                      <?php
                      unset($_SESSION['user']['addsubjectok']);
                    }

                     ?>


                     <div class="col-md-12 col-sm-12">
                                   <div class="panel tab-border card-box">
                                       <header class="panel-heading panel-heading-gray custom-tab ">
                                           <ul class="nav nav-tabs">
                                               <li class="nav-item"><a href="#home" data-toggle="tab" class="active"><i class="fa fa-bars"></i> <?php echo L::TPListesfees ?></a>
                                               </li>
                                               <?php
                                                 if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="Coordinateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Intendance")
                                                  {
                                                   ?>
                                                   <?php
                                                   if($etablissementType==1||$etablissementType==3)
                                                   {
                                                    ?>
                                                    <li class="nav-item"><a href="#about1" data-toggle="tab"><i class="fa fa-plus-circle"></i> <?php echo L::AddcontolsTea ?></a>
                                                    </li>
                                                    <?php
                                                  }else {
                                                    ?>
                                                    <li class="nav-item"><a href="#about" data-toggle="tab"><i class="fa fa-plus-circle"></i> <?php echo L::AddTPfees ?></a>
                                                    </li>
                                                    <!-- <li class="nav-item"><a href="#about2" data-toggle="tab"><i class="fa fa-plus-circle"></i> <?php //echo L::TypeTransportation ?></a> -->
                                                    </li>
                                                    <?php
                                                  }
                                                     ?>
                                                   <?php
                                                 }
                                                  ?>




                                           </ul>
                                       </header>
                                       <div class="panel-body">
                                           <div class="tab-content">
                                               <div class="tab-pane active" id="home">
                                                 <div class="row">
                        <div class="col-md-12">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header></header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">

                                    <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example45">
                                        <thead>
                                            <tr>
                                               <th style="width:400px"> <?php echo L::TypeTransportation ?> </th>
                                                <th> <?php echo L::Frais ?> </th>
                                                <th> <?php echo L::MontantSection ?></th>

                                                  <?php
                                                  if($_SESSION['user']['fonctionuser']=="Directeur")
                                                  {
                                                    ?>
                                                    <th class="noExport" style="display:none"> <?php echo L::Actions ?> </th>
                                                    <?php
                                                  }else {
                                                    ?>
                                                    <th class="noExport"> <?php echo L::Actions ?> </th>
                                                    <?php
                                                  }
                                                   ?>

                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                            //$matieres

                                            $i=1;
                                              foreach ($transportations as $value):
                                           ?>
                                            <tr class="odd gradeX">
                                                <td class="left">
                                                  <?php

                                                  if($value->Mode_trans=="TRAJETSIMPLE")
                                                  {
                                                    ?>
                                                    <span class="label label-primary mabel-xs"><?php echo L::Trajet1; ?></span>
                                                    <?php
                                                  }else   if($value->Mode_trans=="TRAJETMIXTE")
                                                  {
                                                    ?>
                                                    <span class="label label-primary"><?php echo L::Trajet2; ?></span>
                                                    <?php
                                                  }

                                                   ?>
                                                </td>
                                                <td> <?php
                                                if($value->type_trans=="MENSUEL")
                                                {
                                                  echo L::Mensualities;
                                                }else if($value->type_trans=="ANNUEL")
                                                {
                                                  echo L::Annuialities ;
                                                }

                                                ?></td>
                                                <td>
                                                    <?php echo $value->frais_trans;?>
                                                </td>
                                                <td class="valigntop">

                                                  <?php

                                                  if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="Coordinateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Intendance")

                                                    {
                                                      ?>
                                                      <a href="#"  data-toggle="modal" data-target="#exampleModal"class="btn btn-info  btn-xs " onclick="ActionBtn(<?php echo $value->id_trans ?>,'<?php echo $value->type_trans ?>','<?php echo $value->codeEtab_trans ?>','<?php echo $value->sessionEtab_trans ?>',<?php echo $value->frais_trans;?>,'<?php echo $value->devises_trans ?>')">
                                                        <i class="fa fa-pencil"></i>
                                                        </a>
                                                      <?php
                                                    }

                                                        ?>




                                                </td>


                                            </tr>


                                            <?php
                                            $i++;
                                            endforeach;
                                            ?>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                                               </div>

                      <?php
                      if($etablissementType==1||$etablissementType==3)
                      {
                       ?>
                       <div class="tab-pane" id="about1">
                         <?php
                         if($nbsessionOn>0)
                         {
                          ?>
                          <div class="row">
                              <div class="col-md-12 col-sm-12">
                                  <div class="card card-box">
                                      <div class="card-head">
                                          <header></header>

                                      </div>

                                      <div class="card-body" id="bar-parent">
                                          <form  id="FormAddCtrl1" class="form-horizontal" action="../controller/controle.php" method="post">
                                              <div class="form-body">
                                                <div class="form-group row">
                                                        <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-5">
                                                            <select class="form-control input-height" name="classe" id="classe"  style="width:100%" onchange="searchmatiere()">
                                                                <option value=""><?php echo L::Selectclasses ?></option>
                                                                <?php
                                                                $i=1;
                                                                  foreach ($classes as $value):
                                                                  ?>
                                                                  <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                                  <?php
                                                                                                   $i++;
                                                                                                   endforeach;
                                                                                                   ?>

                                                            </select>
                                                    </div>
                                                  </div>
                                                <div class="form-group row">
                                                        <label class="control-label col-md-3"><?php echo L::MatiereMenusingle ?>
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-5">
                                                            <!--input type="text" name="matiere" id="matiere" data-required="1" placeholder="Entrer la classe" class="form-control input-height" /-->
                                                            <select class="form-control input-height" name="matiere" id="matiere"  style="width:100%" onchange='searchprofesseur()'>
                                                                <option value=""><?php echo L::SelectSubjects ?></option>

                                                            </select>
                                                          </div>

                                                 </div>
                                                 <div class="form-group row">
                                                         <label class="control-label col-md-3"><?php echo L::ProfsMenusingle ?>
                                                             <span class="required"> * </span>
                                                         </label>
                                                         <div class="col-md-5">

                                                           <select class="form-control input-height" id="teatcher" name="teatcher" style="width:100%" >
                                                               <option value=""><?php echo L::PleaseselectTeatEnter ?></option>
                                                               <?php
                                                               $i=1;
                                                                 foreach ($teatchers as $value):
                                                                 ?>
                                                                 <option value="<?php echo $value->id_compte?>"><?php echo utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)) ?></option>

                                                                 <?php
                                                                                                  $i++;
                                                                                                  endforeach;
                                                                                                  ?>

                                                           </select>
                                                           <input type="hidden" name="etape" id="etape" value="4"/>
                                                           <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $_SESSION['user']['codeEtab']; ?>"/>
                                                             <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                                        </div>
                                                     </div>

                                                 <div class="form-group row">
                                                         <label class="control-label col-md-3"><?php echo L::LibelleControle ?>
                                                             <span class="required"> * </span>
                                                         </label>
                                                         <div class="col-md-5">
                                                             <input type="text" name="controle" id="controle" data-required="1" placeholder="Entrer la classe" class="form-control input-height" />

                                                           </div>

                                                  </div>
                                                 <div class="form-group row">
                                                         <label class="control-label col-md-3"><?php echo L::Coefs ?>
                                                             <span class="required"> * </span>
                                                         </label>
                                                         <div class="col-md-5">
                                                             <input type="number" min="1" name="coef" id="coef" data-required="1" placeholder="<?php echo L::EnterCoefSubjectControlLib ?>" class="form-control input-height" /> </div>
                                                  </div>
                                                  <div class="form-group row">
                                                    <label class="control-label col-md-3"><?php echo L::DatecontolsTea ?>
                                                        <span class="required"> * </span>
                                                    </label>
                                                        <div class="col-md-5">
                                                            <input type="date"  placeholder="<?php echo L::DatecontolsTea ?>" name="datectrl" id="datectrl"  class="form-control input-height">
                                                              <span class="help-block"><?php echo L::Datesymbole ?></span>
                                                        </div>
                                                    </div>







                            <div class="form-actions">
                                                  <div class="row">
                                                      <div class="offset-md-3 col-md-9">

                                                          <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                                          <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                      </div>
                                                    </div>
                                                 </div>
                          </div>
                                          </form>
                                      </div>
                                  </div>
                              </div>

                          </div>


                          <?php
                        }
                          ?>



                        </div>


    <?php
  }else {
    ?>
    <div class="tab-pane" id="about">
      <?php
      if($nbsessionOn>0)
      {
       ?>
       <div class="row">
           <div class="col-md-12 col-sm-12">
               <div class="card card-box">
                   <div class="card-head">
                       <header></header>

                   </div>

                   <div class="card-body" id="bar-parent">
                       <form  id="FormAddCtrl" class="form-horizontal" action="../controller/transportation.php" method="post">
                           <div class="form-body">

                             <div class="form-group row">
                                     <label class="control-label col-md-3"><?php echo L::TypeTransportation ?>
                                         <span class="required"> * </span>
                                     </label>
                                     <div class="col-md-8">
                                       <select class="form-control " name="transportype" id="transportype" style="width:61%">
                                          <option value="" selected><?php echo L::selecttrajet ?></option>
                                           <option  value="TRAJETSIMPLE"><?php echo L::Trajet1?></option>
                                           <option value="TRAJETMIXTE"><?php echo L::Trajet2 ?></option>
                                       </select>
                                       </div>

                              </div>


                             <div class="form-group row">
                                     <label class="control-label col-md-3"><?php echo L::Mensualfees ?>
                                         <span class="required"> * </span>
                                     </label>
                                     <div class="col-md-5">
                                   <input type="text" name="fraismensuel" id="fraismensuel" data-required="1" placeholder="<?php echo L::EnterMensualfees ?>" class="form-control " />

                                       </div>

                              </div>
                              <div class="form-group row" style="display:none">
                                      <label class="control-label col-md-3"><?php echo L::Trimesterfees ?>
                                          <span class="required"> * </span>
                                      </label>
                                      <div class="col-md-5">
                                    <input type="text" name="fraistrimes" id="fraistrimes" data-required="1" placeholder="<?php echo L::EnterTrimesterfees ?>" class="form-control " />

                                        </div>

                               </div>
                               <div class="form-group row">
                                       <label class="control-label col-md-3"><?php echo L::Semesterfees ?>
                                           <span class="required"> * </span>
                                       </label>
                                       <div class="col-md-5">
                                     <input type="text" name="fraisannuel" id="fraisannuel" data-required="1" placeholder="<?php echo L::EnterSemesterfees ?>" class="form-control " />
                                     <input type="hidden" name="etape" id="etape" value="3">
                                     <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $_SESSION['user']['codeEtab'] ?>">
                                     <input type="hidden" name="sessionEtab" id="sessionEtab" value="<?php echo $libellesessionencours ?>">
                                         </div>

                                </div>
                                <div class="form-group row">
                                        <label class="control-label col-md-3"><?php echo L::DeviseMontantSection ?>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-5">
                                      <input type="text" name="devises" id="devises" data-required="1" placeholder="<?php echo L::DeviseMontantSectionEnter ?>" class="form-control " />

                                          </div>

                                 </div>





         <div class="form-actions">
                               <div class="row">
                                   <div class="offset-md-3 col-md-9">

                                       <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                       <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                   </div>
                                 </div>
                              </div>
       </div>
                       </form>
                   </div>
               </div>
           </div>

       </div>


       <?php
     }
       ?>



     </div>
    <?php
  }
     ?>





                                           </div>
                                       </div>
                                   </div>
                               </div>
          </div>


          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="exampleModalLabel"><?php echo L::FraisMod ?></h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                  <form  id="FormUpdate" class="form-horizontal" action="../controller/transportation.php" method="post">
                                      <div class="form-body">
                                      <div class="row">
                                        <div class="col-md-12">
                                        <div class="form-group">
                                        <label for=""><b><?php echo L::Frais ?><span class="required">  </span> :</b></label>

                                        <input name="typefrais1" id="typefrais1" type="text" placeholder="<?php echo L::EnterCniAccount ?> " value="" class="form-control " readonly />
                                        <input name="typefrais" id="typefrais" type="hidden" placeholder="<?php echo L::EnterCniAccount ?> " value="" class="form-control " readonly />
                                        </div>

                                        </div>

                                        <div class="col-md-12">
                                        <div class="form-group">
                                        <label for=""><b><?php echo L::MontantSection ?><span class="required">  </span> :</b></label>
                                        <input type="hidden" name="etape" id="etape" value="2">
                                        <input type="hidden" name="transid" id="transid" value="">
                                        <input type="hidden" name="codeEtabfrais" id="codeEtabfrais" value="">
                                        <input type="hidden" name="sessionEtabfrais" id="sessionEtabfrais" value="">
                                        <input name="montfrais" id="montfrais" type="text" placeholder="<?php echo L::EnterFraisMont ?> " value="" class="form-control " />
                                        </div>

                                        </div>

                                        <div class="col-md-12">
                                        <div class="form-group">
                                        <label for=""><b><?php echo L::DeviseMontantSection ?><span class="required">  </span> :</b></label>

                                        <input name="devisefrais" id="devisefrais" type="text" placeholder="<?php echo L::DeviseMontantSectionEnter ?> " value="" class="form-control " />
                                        </div>

                                        </div>

                                        </div>





                    <div class="form-actions">
                                          <div class="row">
                                              <div class="offset-md-3 col-md-9">

                                                    <button type="submit" onclick="" class="btn btn-info"><?php echo L::ModifierBtn ?></button>
                                                  <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo L::AnnulerBtn ?></button>
                                              </div>
                                            </div>
                                         </div>
                  </div>
                                  </form>
                                </div>

                            </div>
                        </div>
                    </div>

      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  	<script src="../assets2/plugins/popper/popper.min.js" ></script>
      <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
  	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
      <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
     <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
  <!-- bootstrap -->
      <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
      <!-- counterup -->
      <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
      <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
      <!-- Common js-->
  	<script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
      <script src="../assets2/js/layout.js" ></script>
      <script src="../assets2/js/theme-color.js" ></script>
      <!-- material -->
      <!-- data tables -->
     <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
   <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" ></script>
   	<script src="../cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
   	<script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" ></script>
   	<script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" ></script>
   	<script src="../cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" ></script>
     <script src="../assets2/js/pages/table/table_data.js" ></script>
      <script src="../assets2/plugins/material/material.min.js"></script>
      <script src="../assets2/plugins/select2/js/select2.js" ></script>
      <script src="../assets2/js/pages/select2/select2-init.js" ></script>
      <script src="../assets/js/formatter/jquery.formatter.min.js"></script>
      <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
      <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

      <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>

 <script>
 // function SetcodeEtab(codeEtab)
 // {
 //   var etape=3;
 //   $.ajax({
 //     url: '../ajax/sessions.php',
 //     type: 'POST',
 //     async:false,
 //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
 //     dataType: 'text',
 //     success: function (content, statut) {
 //
 // window.location.reload();
 //
 //     }
 //   });
 // }
function ActionBtn(idtrans,typetrans,codeEtab,sessionEtab,frais,devises)
{

  var libelletypefrais="";
  if(typetrans=="MENSUEL")
  {
    $("#FormUpdate #typefrais1").val("<?php echo L::Mensualities ?>");
  }else {
  $("#FormUpdate #typefrais1").val("<?php echo L::Annuialities ?>");
  }

  $("#FormUpdate #typefrais").val(typetrans);
  $("#FormUpdate #montfrais").val(frais);
  $("#FormUpdate #devisefrais").val(devises);
  $("#FormUpdate #codeEtabfrais").val(codeEtab);
  $("#FormUpdate #sessionEtabfrais").val(sessionEtab);
  $("#FormUpdate #transid").val(idtrans);

}


 function addFrench()
 {
   var etape=1;
   var lang="fr";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function addEnglish()
 {
   var etape=1;
   var lang="en";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function searchmatiere()
 {
     var codeEtab="<?php echo $_SESSION['user']['codeEtab'];?>";
     var classe=$("#classe").val();
     var etape=2;
      $.ajax({

        url: '../ajax/matiere.php',
        type: 'POST',
        async:true,
        data: 'code=' + codeEtab+ '&etape=' + etape+'&classe='+classe,
        dataType: 'text',
        success: function (content, statut) {

          $("#matiere").html("");
          $("#matiere").html(content);

        }
      });
 }

 function searchprofesseur()
 {
   var codeEtab="<?php echo $_SESSION['user']['codeEtab'];?>";
   var classe=$("#classe").val();
   var matiere=$("#matiere").val();
   var etape=4;
   $.ajax({

     url: '../ajax/teatcher.php',
     type: 'POST',
     async:true,
     data: 'code=' + codeEtab+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
     dataType: 'text',
     success: function (content, statut) {

       // alert(content);
       $("#teatcher").html("");
       $("#teatcher").html(content);

     }
   });
 }


function AddtransportRow()
{

}


 jQuery(document).ready(function() {

   $('#example45').DataTable( {

     "scrollX": true,
     "language": {
         "lengthMenu": "_MENU_  ",
         "zeroRecords": "Aucune correspondance",
         "info": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
         "infoEmpty": "Aucun enregistrement disponible",
         "infoFiltered": "(filtered from _MAX_ total records)",
         "sEmptyTable":"Aucune donnée disponible dans le tableau",
          "sSearch":"Rechercher :",
          "oPaginate": {
     "sFirst":    "Premier",
     "sLast":     "Dernier",
     "sNext":     "Suivant",
     "sPrevious": "Précédent"
   }
   },

       dom: 'Bfrtip',

         buttons: [
             // 'copyHtml5',

              // 'excelHtml5',
             {
               extend: 'excelHtml5',
               title: 'Data export',
               exportOptions: {
                             columns: "thead th:not(.noExport)"
                         }
             }
             // 'csvHtml5',
             // 'pdfHtml5'
         ]



   } );

   $("#fraisannuel").formatter({pattern:"{{999999999999}}"});
   $("#fraistrimes").formatter({pattern:"{{999999999999}}"});
   $("#fraismensuel").formatter({pattern:"{{999999999999}}"});


   $('#routineNew').click(function(){
         AddtransportRow();


       });



$("#classe").select2();
$("#teatcher").select2();
$("#classeEtab").select2();
$("#matiere").select2();
$("#typesess").select2();
$("#transportype").select2();



   $("#FormUpdate").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

        // matiere:"required",
        // classe:"required",
        // teatcher:"required",
        // coef:"required",
        montfrais:"required",
        devisefrais:"required",
        fraismensuel:"required",
        devises:"required",
        coef:"required",
        teatcher:"required",
        datectrl:"required",
        typesess:"required",
        transportype:"required"


      },
      messages: {
        // matiere:"Merci de renseigner la matière",
        // classe:"<?php echo L::PleaseSelectclasserequired ?>",
        // teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
        // coef:"Merci de renseigner le coefficient de la matière"
        classe:"<?php echo L::PleaseSelectclasserequired ?>",
        matiere:"<?php echo L::PleaseselectSubjects ?>",
        controle:"<?php echo L::Controlsrequired ?>",
        coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
        teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
          datectrl:"<?php echo L::PleaseEnterDateControls ?>",
          typesess:"<?php echo L::PeriodRequired ?>",
          fraisannuel:"<?php echo L::PleaseEnterSemesterfees ?>",
          fraistrimes:"<?php echo L::PleaseEnterTrimesterfees ?>",
          fraismensuel:"<?php echo L::PleaseEnterMensualfees ?>",
          devises:"<?php echo L::PleaseDeviseMontantSectionEnter ?>",
          montfrais:"<?php echo L::PleaseEnterFraisMont ?>",
          devisefrais:"<?php echo L::PleaseDeviseMontantSectionEnter ?>",
          transportype:"<?php echo L::Pleaseselecttrajet ?>"


      },
      submitHandler: function(form) {


// nous allons verifier un controle similaire n'existe pas
        form.submit();

             }


           });

           $("#FormAddCtrl1").validate({

             errorPlacement: function(label, element) {
             label.addClass('mt-2 text-danger');
             label.insertAfter(element);
           },
           highlight: function(element, errorClass) {
             $(element).parent().addClass('has-danger')
             $(element).addClass('form-control-danger')
           },
           success: function (e) {
                 $(e).closest('.control-group').removeClass('error').addClass('info');
                 $(e).remove();
             },
              rules:{

                // matiere:"required",
                // classe:"required",
                // teatcher:"required",
                // coef:"required",
                classe:"required",
                matiere:"required",
                controle:"required",
                coef:"required",
                teatcher:"required",
                datectrl:"required",
                typesess:"required"


              },
              messages: {
                // matiere:"Merci de renseigner la matière",
                // classe:"<?php echo L::PleaseSelectclasserequired ?>",
                // teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
                // coef:"Merci de renseigner le coefficient de la matière"


                  classe:"<?php echo L::PleaseSelectclasserequired ?>",
                  matiere:"<?php echo L::PleaseselectSubjects ?>",
                  controle:"<?php echo L::Controlsrequired ?>",
                  coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
                  teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
                    datectrl:"<?php echo L::PleaseEnterDateControls ?>",
                    typesess:"<?php echo L::PeriodRequired ?>"


              },
              submitHandler: function(form) {


        // nous allons verifier un controle similaire n'existe pas
                var etape=1;

                 $.ajax({
                   url: '../ajax/controle.php',
                   type: 'POST',
                   async:true,
                   data: 'matiere=' + $("#matiere").val()+ '&etape=' + etape+'&classe='+$("#classe").val()+'&teatcher='+$("#teatcher").val()+'&codeEtab='+$("#codeEtab").val()+'&coefficient='+$("#coef").val()+'&datectrl='+$("#datectrl").val(),
                   dataType: 'text',
                   success: function (content, statut) {


                     if(content==0)
                     {
                       //cette matière n'existe pas encore pour cette classe

                       form.submit();

                     }else if(content==1)
                     {
                       //il est question d'un nouveau professeur pour cette matière
                       Swal.fire({
                       type: 'warning',
                       title: '<?php echo L::WarningLib ?>',
                       text: '<?php echo L::ControlAllreadyExist ?>',

                       })

                     }

                   }
                 });

                     }


                   });



                   $("#FormAddCtrl").validate({

                     errorPlacement: function(label, element) {
                     label.addClass('mt-2 text-danger');
                     label.insertAfter(element);
                   },
                   highlight: function(element, errorClass) {
                     $(element).parent().addClass('has-danger')
                     $(element).addClass('form-control-danger')
                   },
                   success: function (e) {
                         $(e).closest('.control-group').removeClass('error').addClass('info');
                         $(e).remove();
                     },
                     rules:{

                       // matiere:"required",
                       // classe:"required",
                       // teatcher:"required",
                       // coef:"required",
                       montfrais:"required",
                       devisefrais:"required",
                       fraismensuel:"required",
                       devises:"required",
                       coef:"required",
                       teatcher:"required",
                       datectrl:"required",
                       typesess:"required",
                       transportype:"required",
                       fraisannuel:"required"


                     },
                     messages: {
                       // matiere:"Merci de renseigner la matière",
                       // classe:"<?php echo L::PleaseSelectclasserequired ?>",
                       // teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
                       // coef:"Merci de renseigner le coefficient de la matière"
                       classe:"<?php echo L::PleaseSelectclasserequired ?>",
                       matiere:"<?php echo L::PleaseselectSubjects ?>",
                       controle:"<?php echo L::Controlsrequired ?>",
                       coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
                       teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
                         datectrl:"<?php echo L::PleaseEnterDateControls ?>",
                         typesess:"<?php echo L::PeriodRequired ?>",
                         fraisannuel:"<?php echo L::PleaseEnterSemesterfees ?>",
                         fraistrimes:"<?php echo L::PleaseEnterTrimesterfees ?>",
                         fraismensuel:"<?php echo L::PleaseEnterMensualfees ?>",
                         devises:"<?php echo L::PleaseDeviseMontantSectionEnter ?>",
                         montfrais:"<?php echo L::PleaseEnterFraisMont ?>",
                         devisefrais:"<?php echo L::PleaseDeviseMontantSectionEnter ?>",
                         transportype:"<?php echo L::Pleaseselecttrajet ?>",
                         fraisannuel:"<?php echo L::PleaseEnterSemesterfees ?>"


                     },
                      submitHandler: function(form) {


                   // nous allons verifier un controle similaire n'existe pas

                   //nous alons verifier si les frais non pas deja été ajouter cette annéé pour le modev

                   var codeEtab=$("#codeEtab").val();
                   var sessionEtab=$("#sessionEtab").val();
                   var transportype=$("#transportype").val();
                   var etape=1;

                   $.ajax({
                     url: '../ajax/transportation.php',
                     type: 'POST',
                     async:false,
                     data: 'etape=' + etape+ '&codeEtab=' +codeEtab+'&sessionEtab='+sessionEtab+'&transportype='+transportype,
                     dataType: 'text',
                     success: function (content, statut) {

                       if(content==1)
                       {
                         Swal.fire({
                         type: 'warning',
                         title: '<?php echo L::WarningLib ?>',
                         text: '<?php echo L::TpfeesAllreadyExist ?>',

                         })
                       }else {
                         form.submit();
                       }

                     }
                   });
                             }


                           });


      });






 </script>
    <!-- end js include path -->
  </body>

</html>
