<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Teatcher.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Classe.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];

$user=new User();
$etabs=new Etab();
$admin= new Localadmin();
$localadmins= new Localadmin();
$parents=new ParentX();
$classe=new Classe();
$teatcher=new Teatcher();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);


$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);



if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

if($_SESSION['user']['groupe']==1)
{
  if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
  {
    $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
    $datastat=$user->getStatisById($codeEtabAssigner);
    $tabstat=explode("*",$datastat);

    //nous allons chercher la liste des etablissements du groupe

    $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

    // var_dump($allcodeEtabs);

    $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
    $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
    $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
    $libellesessionencours="";

      if($etablissementType==1||$etablissementType==3)
      {
        if($nbsessionOn>0){
          //recuperer la session en cours
          $sessionencours=$session->getSessionEncours($codeEtabAssigner);
          $tabsessionencours=explode("*",$sessionencours);
          $libellesessionencours=$tabsessionencours[0];
          $sessionencoursid=$tabsessionencours[1];
          $typesessionencours=$tabsessionencours[2];

        }

      }else {
        if($nbsessionOn>0)
        {
          //recuperer la session en cours
          $sessionencours=$session->getSessionEncours($codeEtabAssigner);
          $tabsessionencours=explode("*",$sessionencours);
          $libellesessionencours=$tabsessionencours[0];
          $sessionencoursid=$tabsessionencours[1];
          $typesessionencours=$tabsessionencours[2];
          $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
          $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
          $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
        }
      }

      $_SESSION['user']['session']=$libellesessionencours;

      $_SESSION['user']['codeEtab']=$codeEtabAssigner;

      $teatchers=$teatcher->getAllTeatchersBySchoolCodeEtabs($compteuserid);

      $admins=$admin->getAllAdminLocalByEtabs($codeEtabAssigner,$compteuserid);

      // var_dump($admins);


       // var_dump($allcodeEtabs);




  }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
  {
    $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
    $datastat=$user->getStatisById($codeEtabAssigner);
    $tabstat=explode("*",$datastat);
    $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
    $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
    $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
    $libellesessionencours="";

      if($etablissementType==1||$etablissementType==3)
      {
        if($nbsessionOn>0){
          //recuperer la session en cours
          $sessionencours=$session->getSessionEncours($codeEtabAssigner);
          $tabsessionencours=explode("*",$sessionencours);
          $libellesessionencours=$tabsessionencours[0];
          $sessionencoursid=$tabsessionencours[1];
          $typesessionencours=$tabsessionencours[2];

        }

      }else {
        if($nbsessionOn>0)
        {
          //recuperer la session en cours
          $sessionencours=$session->getSessionEncours($codeEtabAssigner);
          $tabsessionencours=explode("*",$sessionencours);
          $libellesessionencours=$tabsessionencours[0];
          $sessionencoursid=$tabsessionencours[1];
          $typesessionencours=$tabsessionencours[2];
          $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
          $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
          $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
        }
        $_SESSION['user']['session']=$libellesessionencours;

        $_SESSION['user']['codeEtab']=$codeEtabAssigner;

        $teatchers=$teatcher->getAllTeatchersBySchoolCode($codeEtabAssigner);

        $admins=$admin->getAllAdminLocalByUserId($codeEtabAssigner,$_SESSION['user']['IdCompte']);




      }

  }
}


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::ManagementUsers ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="#"><?php echo L::UsersMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::ManagementUsers ?></li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['addlocalok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
          <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
          <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: '<?php echo L::Felicitations ?>',
            text: '<?php echo $_SESSION['user']['addlocalok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['addlocalok']);
                }

                 ?>

          <?php

                if(isset($_SESSION['user']['updatelocalok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: '<?php echo L::Felicitations ?>',
            text: '<?php echo $_SESSION['user']['updatelocalok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['updatelocalok']);
                }

                 ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>
<div class="row">
  <div class="col-md-12 col-sm-12">
                            <div class="card card-box">
                                <div class="card-head">
                                    <header><?php echo L::Seacher ?></header>

                                </div>
                                <div class="card-body " id="bar-parent">
                                  <?php

                                  if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
                                  {
                                    ?>
                                    <form method="post" id="FormSearch1">
                                        <div class="row">
                                          <div class="col-md-6 col-sm-6">
                                          <!-- text input -->
                                            <!-- <input type="hidden" id="codeetab" name="codeetab" class="form-control" value="<?php //echo $codeEtabAssigner; ?>"> -->
                                            <div class="form-group">
                                                <label><?php echo L::Etabses ?></label>
                                                <select class="form-control " id="Etabs" name="Etabs" onchange="searchusersname()" style="width:100%">
                                                    <option value=""><?php echo L::SelectOneEtabs ?></option>
                                                    <?php
                                                    $i=1;
                                                      foreach ($allcodeEtabs as $value):
                                                      ?>
                                                      <option value="<?php echo $value->codeEtab_assign?>"><?php echo $etabs->getEtabNamebyCodeEtab($value->codeEtab_assign)?></option>

                                                      <?php
                                                                                       $i++;
                                                                                       endforeach;
                                                                                       ?>
                                                       <option value="ALL" selected><?php echo L::AllEtabses ?></option>

                                                </select>
                                            </div>

                                      </div>
                                          <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label><?php echo L::UsersMenu ?></label>
                                                <select class="form-control input-height" id="adminlo" name="adminlo" style="width:100%">
                                                    <option value=""><?php echo L::SelectUsers ?></option>
                                                    <?php
                                                    $i=1;
                                                      foreach ($admins as $value):
                                                      ?>
                                                      <option value="<?php echo $value->id_compte?>"><?php echo utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)) ?></option>

                                                      <?php
                                                                                       $i++;
                                                                                       endforeach;
                                                                                       ?>

                                                </select>
                                                <input type="hidden" name="search" id="search" value="1"/>
                                            </div>
                                          </div>


                                        </div>
                                        <input type="hidden" name="search" id="search" value="1" />
                                        <button type="submit" class="btn btn-danger"><?php echo L::Seacher ?></button>
                                    </form>
                                    <?php
                                  }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
                                  {
                                    ?>
                                    <form method="post" id="FormSearch">
                                        <div class="row">
                                          <div class="col-md-6 col-sm-6">
                                          <!-- text input -->
                                            <input type="hidden" id="codeetab" name="codeetab" class="form-control" value="<?php echo $codeEtabAssigner; ?>">
                                          <div class="form-group">
                                              <label><?php echo L::UsersMenu ?> </label>
                                              <select class="form-control input-height" id="adminlo" name="adminlo" style="width:100%">
                                                  <option><?php echo L::SelectUsers ?></option>
                                                  <?php
                                                  $i=1;
                                                    foreach ($admins as $value):
                                                    ?>
                                                    <option value="<?php echo $value->id_compte?>"><?php echo utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)) ?></option>

                                                    <?php
                                                                                     $i++;
                                                                                     endforeach;
                                                                                     ?>

                                              </select>
                                              <input type="hidden" name="search" id="search" value="0" />
                                          </div>

                                      </div>

                                        </div>

                                        <button type="submit" class="btn btn-danger"><?php echo L::Seacher ?></button>
                                    </form>
                                    <?php
                                  }

                                   ?>

                                </div>
                            </div>
                        </div>
</div>
<div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line">
                               <ul class="nav nav-pills nav-pills-rose" style="display:none">
									<li class="nav-item tab-all"><a class="nav-link active show"
										href="#tab1" data-toggle="tab"><?php echo L::List ?></a></li>
									<li class="nav-item tab-all"><a class="nav-link" href="#tab2"
										data-toggle="tab"><?php echo L::Grille ?></a></li>
								</ul>
                                <div class="tab-content">
                                    <div class="tab-pane active fontawesome-demo" id="tab1">
                                        <div class="row">
					                        <div class="col-md-12">
					                            <div class="card  card-box">
					                                <div class="card-head">
					                                    <header></header>
					                                    <div class="tools">
					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
					                                    </div>
					                                </div>
					                                <div class="card-body ">
                                              <div class="pull-right">
                                                <?php
                                                  if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Superviseur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Coordinnateur")
                                                  {

                                                  }else {
                                                    ?>
                                                      <a class="btn btn-primary " style="border-radius:5px;" href="addusers.php"><i class="fa fa-plus"></i> <?php echo L::NewUsers ?></a>
                                                    <?php
                                                  }
                                                 ?>

                                                <a class="btn btn-warning " style="border-radius:5px;" onclick="localsliste()" href="#"><i class="fa fa-print"></i> <?php echo L::ListUsers ?></a>
                                              </div>
					                                  <div class="table-scrollable">
					                                    <table class="table table-hover table-checkable order-column full-width" id="example45">
					                                        <thead>
					                                            <tr>

					                                                <th> <?php echo L::NamestudentTab ?>  </th>

                                                          <th> <?php echo L::Fonction ?></th>
					                                                <th> <?php echo L::PhonestudentTab ?></th>
					                                                <th> <?php echo L::EmailstudentTab ?> </th>


                                                          <?php
                                                          if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Superviseur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Coordinnateur")
                                                            {

                                                            }else {
                                                              ?>
                                                              <th class="noExport"> <?php echo L::Actions ?> </th>
                                                              <?php
                                                            }
                                                           ?>

					                                            </tr>
					                                        </thead>
					                                        <tbody>
                                                    <?php
                                                    $i=1;

                                                    if(isset($_POST["search"]))
                                                    {

                                                      if($_POST["search"]==0)
                                                      {
                                                        if(strlen($_POST['adminlo'])>0)
                                                        {
                                                          //recherche un admin en particulier
                                                          $admins=$admin->getAllAdminLocalBysearch($_POST['adminlo']);
                                                        }else {
                                                          //rechercher tous les admins d'un etablissement
                                                          $admins=$admin->getAllAdminLocalBysearchCode($_POST['codeetab']);
                                                        }
                                                      }else if($_POST["search"]==1)
                                                      {
                                                        if($_POST['Etabs']=="All")
                                                        {

                                                        }else {
                                                          if(strlen($_POST['adminlo'])>0)
                                                          {
                                                            //recherche un admin en particulier
                                                            $admins=$admin->getAllAdminLocalBysearchEtabs($_POST['adminlo'],$_POST['Etabs']);
                                                          }else {
                                                            //rechercher tous les admins d'un etablissement
                                                            $admins=$admin->getAllAdminLocalBysearchCode($_POST['Etabs']);
                                                          }
                                                        }
                                                      }


                                                    }


                                                      foreach ($admins as $value):
                                                      ?>
																<tr class="odd gradeX" ondblclick="myFunction(<?php echo $value->id_compte?>)">

																	<td class="" style="width:300px;"><?php echo $value->nom_compte." ".$value->prenom_compte;?></td>

                                  <td class="left"><?php echo $value->fonction_compte?></td>
																	<td class="left"><?php echo $value->tel_compte?></td>
																	<td class="left"><?php echo $value->email_compte?></td>

                                  <?php
                                    if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Superviseur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Coordinnateur")
                                    {

                                    }else {
                                      ?>
                                      <td class="center">

    																		<a href="updatelocal.php?compte=<?php echo $value->id_compte?>"   class="btn btn-info btn-xs">
    																			<i class="fa fa-pencil"></i>
    																		</a>
                                        <a class="btn btn-danger btn-xs" onclick="deleted(<?php echo $value->id_compte?>)">
                                          <i class="fa fa-trash-o "></i>
                                        </a>
                                        <!--a class="btn btn-success btn-md" onclick="actived(<?php //echo $value->id_compte?>)">
                                          <i class="fa fa-check-square-o "></i>
                                        </a-->

    																	</td>
                                      <?php
                                    }
                                   ?>


																</tr>
                                <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                <script>

                                function myFunction(idcompte)
                                {
                                  //var url="detailslocal.php?compte="+idcompte;
                                document.location.href="detailslocal.php?compte="+idcompte;
                                }

                                function actived($id)
                                {
                                  Swal.fire({
                    title: '<?php echo L::WarningLib ?>',
                    text: "<?php echo L::DoyouReallyActivateThisLocalAdmin  ?>",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<?php echo L::ModifierBtn ?>',
                    cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                  }).then((result) => {
                    if (result.value) {
                      document.location.href="../controller/localadmin.php?etape=3&compte="+id;
                    }else {

                    }
                  })
                                }

                                function modify(id)
                                {


                                  Swal.fire({
                    title: '<?php echo L::WarningLib ?>',
                    text: "<?php echo L::DoyouReallyModifingThisLocalAdmin ?>",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<?php echo L::ModifierBtn ?>',
                    cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                  }).then((result) => {
                    if (result.value) {
                      document.location.href="updatelocal.php?compte="+id;
                    }else {

                    }
                  })
                                }

                                function deleted(id)
                                {

                                  Swal.fire({
                    title: '<?php echo L::WarningLib ?>',
                    text: "<?php echo L::DoyouReallyDeletingThisLocalAdmin ?>",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<?php echo L::DeleteLib ?>',
                cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                  }).then((result) => {
                    if (result.value) {
                      document.location.href="../controller/localadmin.php?etape=3&compte="+id;
                    }else {

                    }
                  })
                                }

                                </script>
                                <?php
                                                                 $i++;
                                                                 endforeach;
                                                                 ?>

															</tbody>
					                                    </table>
					                                    </div>
					                                </div>
					                            </div>
					                        </div>
					                    </div>
                                    </div>
                                    <div class="tab-pane" id="tab2">



                    					<div class="row">
                                <?php
                                $i=1;
                                  foreach ($admins as $value):
                                  ?>
					                        <div class="col-md-4" ondblclick="myFunction(<?php echo $value->id_compte?>)">
				                                <div class="card card-box">
				                                    <div class="card-body no-padding ">
				                                    	<div class="doctor-profile">
                                                <?php
                                                  $lienImg="";
                                                  if(strlen($value->photo_compte)>0)
                                                  {
                                                    $lienImg="../photo/".$value->email_compte."/".$value->photo_compte;
                                                  }else {
                                                    $lienImg="../photo/user5.jpg";
                                                  }
                                                 ?>
				                                                <img src="<?php echo $lienImg?>" class="doctor-pic" alt="" style="height:100px; width:90px;">
					                                        <div class="profile-usertitle">
					                                            <div class="doctor-name"><?php echo $value->nom_compte." ".$value->prenom_compte;?></div>
					                                            <div class="name-center"> <?php echo $value->fonction_compte;?> </div>
					                                        </div>


				                                                <p><?php echo $value->email_compte;?></p>
				                                                <?php
                                                        if(strlen($value->tel_compte)>0)
                                                        {
                                                          ?>
                                                          <div><p><i class="fa fa-phone"></i><a href="">  <?php echo $value->tel_compte; ?></a></p> </div>
                                                          <?php
                                                        }
                                                         ?>
					                                        <div class="profile-userbuttons">
					                                             <a href="#" class="btn btn-circle btn-info btn-sm" onclick="modify(<?php echo $value->id_compte ?>)"><i class="fa fa-pencil"></i></a>
                                                       <a href="#" class="btn btn-circle btn-danger btn-sm" onclick="deleted(<?php echo $value->id_compte?>,'<?php echo $codeEtabAssigner ?>')"><i class="fa fa-trash"></i></a>
					                                        </div>
				                                        </div>
				                                    </div>
				                                </div>
					                        </div>
                                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                  <script>

                                  function myFunction(idcompte)
                                  {
                                    //var url="detailslocal.php?compte="+idcompte;
                                  document.location.href="detailslocal.php?compte="+idcompte;
                                  }

                                  function modify(id)
                                  {


                                    Swal.fire({
                      title: '<?php echo L::WarningLib ?>',
                      text: "<?php echo L::DoyouReallyModifingThisLocalAdmin ?>",
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: '<?php echo L::ModifierBtn ?>',
                      cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                    }).then((result) => {
                      if (result.value) {
                        document.location.href="updatelocal.php?compte="+id;
                      }else {

                      }
                    })
                                  }

                                  function deleted(id,codeEtab)
                                  {

                                    Swal.fire({
                    title: '<?php echo L::WarningLib ?>',
                      text: "<?php echo L::DoyouReallyDeletingThisLocalAdmin ?>",
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: '<?php echo L::DeleteLib ?>',
                      cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                    }).then((result) => {
                      if (result.value) {
                        document.location.href="../controller/localadmin.php?etape=3&compte="+id+"codeEtab="+codeEtab;
                      }else {

                      }
                    })
                                  }

                                  </script>
                                  <?php
                                                                   $i++;
                                                                   endforeach;
                                                                   ?>


                    					</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
 <script src="../cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" ></script>
   <script src="../cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
   <script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" ></script>
   <script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" ></script>
   <script src="../cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   function SetcodeEtab(codeEtab)
   {
     var etape=3;
     $.ajax({
       url: '../ajax/sessions.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function localsliste()
   {
     //listeEnseignant.php?$codeEtabAssigner=<?php //echo $codeEtabAssigner?>

     var url="listeAdminlocale.php?$codeEtabAssigner=<?php echo $codeEtabAssigner?>";

     window.open(url, '_blank');
   }

   function searchusersname()
   {
     var codeEtab=$("#Etabs").val();
     var compteid="<?php echo $compteuserid ?>";
     if(codeEtab=="ALL")
     {

     }else
     {
       var etape=4;
        $.ajax({
          url: '../ajax/localadmin.php',
          type: 'POST',
          async:false,
        data: 'codeEtab='+codeEtab+'&etape='+etape+'&compteid='+compteid,
          dataType: 'text',
          success: function (content, statut) {

            $("#adminlo").html("");
            $("#adminlo").html(content);

          }
        });

     }
   }

   function searchlibetab()
   {

     var etape=2;

     $.ajax({
              url: '../ajax/school.php',
              type: 'POST',
              async:false,
              data: 'code='+ $("#codeetab").val()+'&etape='+etape,
              dataType: 'text',
              success: function (content, statut) {

                $("#libetab").html("");
                $("#libetab").html(content);

                //nous allons chercher la liste des admins local de cet etablissement
                var idcompte="<?php echo $_SESSION['user']['IdCompte'];?>";

                $.ajax({
                         url: '../ajax/localadmin.php',
                         type: 'POST',
                         async:false,
                         data: 'code='+ $("#codeetab").val()+'&etape='+etape+'&idcompte='+idcompte,
                         dataType: 'text',
                         success: function (response, statut) {


                           $("#adminlo").html("");
                           $("#adminlo").html(response);
                         }
                       });

              }

            });

   }
   $(document).ready(function() {

     $('#example5').DataTable( {
         "scrollX": true

     } );

     $('#example45').DataTable( {

       "scrollX": true,
       "language": {
           "lengthMenu": "_MENU_  ",
           "zeroRecords": "Aucune correspondance",
           "info": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
           "infoEmpty": "Aucun enregistrement disponible",
           "infoFiltered": "(filtered from _MAX_ total records)",
           "sEmptyTable":"Aucune donnée disponible dans le tableau",
            "sSearch":"Rechercher :",
            "oPaginate": {
       "sFirst":    "Premier",
       "sLast":     "Dernier",
       "sNext":     "Suivant",
       "sPrevious": "Précédent"
     }
     },

         dom: 'Bfrtip',
         buttons: [
             // 'copyHtml5',

             // 'excelHtml5',
             {
               extend: 'excelHtml5',
               title: 'Data export',
               exportOptions: {
                             columns: "thead th:not(.noExport)"
                         }
             }
             // 'csvHtml5',
             // 'pdfHtml5'
         ]
     } );

     // $("#codeetab").select2();
     //
     // $("#libetab").select2();
     $("#adminlo").select2();
     $("#Etabs").select2();

   });

   </script>
    <!-- end js include path -->
  </body>

</html>
