<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}



 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::AddParent ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="#"><?php echo L::ParentsMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::AddParent ?></li>
                          </ol>
                      </div>
                  </div>
                  <?php

                        if(isset($_SESSION['user']['addparok']))
                        {

                          ?>
                          <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?php
                        //echo $_SESSION['user']['addetabok'];
                        ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                           </a>
                        </div-->
      <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
      <script src="../assets/js/sweetalert2.min.js"></script>

                    <script>
                    Swal.fire({
      title: '<?php echo L::Felicitations ?>',
      text: "<?php echo $_SESSION['user']['addparok']; ?>",
      type: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '<?php echo L::AddNews ?>',
      cancelButtonText: '<?php echo L::AnnulerBtn ?>',
    }).then((result) => {
      if (result.value) {

      }else {
        document.location.href="index.php";
      }
    })
                    </script>
                          <?php
                          unset($_SESSION['user']['addparok']);
                        }

                         ?>
					<!-- start widget -->

					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


                 <div class="row">
                     <div class="col-md-12 col-sm-12">
                         <div class="card card-box">
                             <div class="card-head">
                                 <header><?php echo L::GeneralInfosParentTab ?></header>
                                  <!--button id = "panel-button"
                                class = "mdl-button mdl-js-button mdl-button--icon pull-right"
                                data-upgraded = ",MaterialButton">
                                <i class = "material-icons">more_vert</i>
                             </button>
                             <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                data-mdl-for = "panel-button">
                                <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                                <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                                <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                             </ul-->
                             </div>

                             <div class="card-body" id="bar-parent">

                               <form class="repeater" id="FormAddLocalAd"  method="post" action="../controller/saisies.php">
                                 <input type="text" name="nbtab" id="nbtab" value="1">
                                 <input type="text" name="etape" id="etape" value="1">
                                 <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner;?>"/>
                                 <input type="hidden" name="sessionEtab" id="sessionEtab" value="<?php echo $libellesessionencours; ?>">

                                 <input type="text" name="nomTea1" id="nomTea" value="">
                                            <div data-repeater-list="group-a">
                                                <div data-repeater-item class="row">
                                                  <div class="col-md-6 col-sm-6">
                                                    <div  class="form-group">
                                                        <label for=""><?php echo L::CniAccount ?></label>
                                                      <input name="cniTea" id="cniTea" type="text" placeholder="<?php echo L::EnterCniAccount ?> " class="form-control " />
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 col-sm-6">
                                                    <div  class="form-group">
                                                      <label ><?php echo L::Name ?></label>
                                                      <input type="text" name="nomTea" id="nomTea" data-required="1" placeholder="<?php echo L::EnterName ?>" class="form-control " />
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 col-sm-6">
                                                    <div  class="form-group">
                                                        <label for=""><?php echo L::PreName ?></label>
                                                        <input type="text" name="prenomTea" id="prenomTea" data-required="1" placeholder="<?php echo L::Enterparentprename ?>" class="form-control " />
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 col-sm-6">
                                                    <div  class="form-group">
                                                      <label for=""><?php echo L::BirthstudentTab ?></label>
                                                      <input type="text" placeholder="<?php echo L::EnterBirthstudentTab ?>" name="datenaisTea" id="datenaisTea" data-mask="99/99/9999" class="form-control ">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 col-sm-6">
                                                    <div  class="form-group">
                                                        <label for=""><?php echo L::SexestudentTab ?></label>
                                                        <select class="form-control " name="sexeTea" id="sexeTea">

                                                            <option selected value="M"><?php echo L::SexeM ?></option>
                                                            <option value="F"><?php echo L::SexeF ?></option>
                                                        </select>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 col-sm-6">
                                                    <div  class="form-group">
                                                      <label for=""><?php echo L::PhonestudentTab ?></label>
                                                      <input name="contactTea" id="contactTea" type="text"  placeholder="<?php echo L::EnterPhoneNumber ?> " class="form-control " />
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 col-sm-6">
                                                    <div  class="form-group">
                                                        <label for=""><?php echo L::PersonalEmail ?></label>
                                                        <input name="emailTea" id="emailTea" type="text" placeholder="<?php echo L::EnterPhoneNumber ?> " class="form-control " />
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 col-sm-6">
                                                    <div  class="form-group">
                                                      <label for=""><?php echo L::ProfessionsParentTab ?></label>
                                                      <input name="fonctionTea" id="fonctionTea" type="text" placeholder="<?php echo L::EnterProfessionsParentTab ?> " class="form-control " />
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 col-sm-6">
                                                    <div  class="form-group">
                                                        <label for="name"><?php echo L::Nationalite ?></label>
                                                        <input name="nation" id="nation" type="text" placeholder="<?php echo L::EnterNationalite ?> " class="form-control " />
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 col-sm-6">
                                                    <div  class="form-group">
                                                      <label for=""><?php echo L::HabitationLocation ?></label>
                                                      <input name="lieuH" id="lieuH" type="text" placeholder="<?php echo L::EnterHabitationLocation ?> " class="form-control " />
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 col-sm-6">
                                                    <div  class="form-group">
                                                        <label for=""><?php echo L::ChildNb ?></label>
                                                        <input name="nbchild" id="nbchild" type="text"  placeholder="<?php echo L::EnterChildNb ?> " class="form-control " />
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 col-sm-6">
                                                    <div  class="form-group">
                                                      <label for=""><?php echo L::ChildNbscolarisy ?></label>
                                                       <input name="nbchildsco" id="nbchildsco" type="text"  placeholder="<?php echo L::EnterChildNbscolarisy ?> " class="form-control " />
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 col-sm-6">
                                                    <div  class="form-group">
                                                        <label for=""><?php echo L::ProfessianalAdress ?></label>
                                                        <input name="adrespro" id="adrespro" type="text"  placeholder="<?php echo L::EnterProfessianalAdress ?> " class="form-control " />
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 col-sm-6">
                                                    <div  class="form-group">
                                                      <label for=""><?php echo L::Society ?></label>
                                                      <input name="societe" id="societe" type="text"  placeholder="<?php echo L::EnterSociety ?>" class="form-control " />
                                                    </div>
                                                  </div>


                                                    <div class="col-lg-2 align-self-center">
                                                        <input data-repeater-delete type="button" onclick='deleted()' class="btn btn-primary btn-block" value="Delete"/>
                                                    </div>
                                                </div>

                                            </div>
                                            <input data-repeater-create type="button" style="margin-top:5px" onclick='added()' class="btn btn-success mo-mt-2" value="Add"/>

                                            <div class="form-group">
                                                <div class="offset-md-3 col-md-9">
                                                    <button type="submit" class="btn btn-info" id="btnsubmit">Submit</button>
                                                    <button type="button" class="btn btn-default">Cancel</button>
                                                </div>
                                            </div>

                                        </form>

                             </div>
                         </div>
                     </div>
                 </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <script src="../assets2/js/pages/material_select/getmdl-select.js" ></script>
   <script src="../assets/js/formatter/jquery.formatter.min.js"></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
   <script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
   <script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>

   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
<script src="../assets2/plugins/jquery-repeater/jquery.repeater.min.js"></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script>
 function SetcodeEtab(codeEtab)
 {
   var etape=3;
   $.ajax({
     url: '../ajax/sessions.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }
function added()
{
  var nb=$("#nbtab").val();
  var nouveau= parseInt(nb)+1;
  $("#nbtab").val(nouveau);

  // $('[name*="nomTea"]').each(function() {
  //
  //     $(this).rules('add', {
  //         required: true,
  //         messages: {
  //             required: "Merci de renseigner le nom du parent"
  //         }
  //     });
  // });
  //
  // $('[name*="prenomTea"]').each(function() {
  //
  //     $(this).rules('add', {
  //         required: true,
  //         messages: {
  //             required: "Merci de renseigner le prénom du parent"
  //         }
  //     });
  // });


  for(i=0;i<nouveau;i++)
  {
    var nomparent="group-a["+i+"][nomTea]";
    alert(nomparent);


        $('[name*="'+nomparent+'"]').rules('add', {
            required: true,
            messages: {
                required: "Merci de renseigner le nom du parent"
            }
        });

  }

}

function deleted()
{
  var nb=$("#nbtab").val();
  var nouveau= parseInt(nb)-1;
  $("#nbtab").val(nouveau);

  if(nouveau==0)
  {
    $("#btnsubmit").attr("disabled", true);
  }else {
    $("#btnsubmit").attr("disabled", false);
  }
}

 function addFrench()
 {
   var etape=1;
   var lang="fr";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function addEnglish()
 {
   var etape=1;
   var lang="en";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 $("#sexeTea").select2();
 var date = new Date();

var newDate = new Date(date.setTime( date.getTime() + (-6120 * 86400000)));

 $('#datenaisTea').bootstrapMaterialDatePicker
 ({
 date: true,
 shortTime: false,
 time: false,
 maxDate:newDate ,
 // minDate:newDate ,
 // format: 'YYYY-MM-DD',
 format: 'DD-MM-YYYY',
 lang: 'fr',
 cancelText: '<?php echo L::AnnulerBtn ?>',
 okText: '<?php echo L::Okay ?>',
 clearText: '<?php echo L::Eraser ?>',
 nowText: '<?php echo L::Now ?>'

 });

 jQuery(document).ready(function() {

   'use strict';

   $('.repeater').repeater({
       defaultValues: {
           'textarea-input': 'foo',
           'text-input': 'bar',
           'select-input': 'B',
           'checkbox-input': ['A', 'B'],
           'radio-input': 'B'
       },
       show: function () {
           $(this).slideDown();
       },
       hide: function (deleteElement) {
           if(confirm('Are you sure you want to delete this element?')) {
             Swal.fire({
             type: 'success',
             title: 'Félicitations',
             text: 'bonsoir',

             })
               $(this).slideUp(deleteElement);
           }
       },
       ready: function (setIndexes) {
         // alert('bonsoir');
       }
   });

   window.outerRepeater = $('.outer-repeater').repeater({
       defaultValues: { 'text-input': 'outer-default' },
       show: function () {
           console.log('outer show');
           $(this).slideDown();
       },
       hide: function (deleteElement) {
           console.log('outer delete');
           $(this).slideUp(deleteElement);
       },
       repeaters: [{
           selector: '.inner-repeater',
           defaultValues: { 'inner-text-input': 'inner-default' },
           show: function () {
               console.log('inner show');
               $(this).slideDown();
           },
           hide: function (deleteElement) {
               console.log('inner delete');
               $(this).slideUp(deleteElement);
           }
       }]
   });

   $("#datenaisTea").formatter({pattern:"{{99}}-{{99}}-{{9999}}"});
   $("#contactTea").formatter({pattern:"{{99}}{{99}}{{99}}{{99}}"});
   $("#nbchild").formatter({pattern:"{{999}}"});
   $("#nbchildsco").formatter({pattern:"{{999}}"});

   // $('#FormAddLocalAd').submit(function(e) {
   //
   //    e.preventDefault();
   //
   //    var contact=$("#contactTea").val();
   //    $(".error").remove();
   //
   //     var regEx = /^[0-9]{9}$ /;
   //
   //     if (!contact.match(regEx)) {
   //         $('#contactTea').after('<span class="error" style="color:red"><?php echo L::PleaseEnterPhoneNumber ?></span>');
   //
   //     }else {
   //       e.sumit();
   //     }
   //
   // });

   $("#FormAddLocalAd").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{
        passTea: {
            required: true,
            minlength: 6
        },
        confirmTea:{
            required: true,
            minlength: 6,
            equalTo:'#passTea'
        },
        // fonctionTea:"required",
        // cniTea:"required",

        loginTea:"required",
        // emailTea: {
        //            required: true,
        //            email: true
        //        },
        contactTea:{
          required: true

        },
        // datenaisTea:"required",
        prenomTea:"required",
        nomTea:"required",
        gradeTea:"required",
        dateEmbTea:"required",
        sexeTea:"required"



      },
      messages: {
        confirmTea:{
            required:"<?php echo L::Confirmcheck ?>",
            minlength:"<?php echo L::Confirmincheck ?>",
            equalTo: "<?php echo L::ConfirmSamecheck ?>"
        },
        passTea: {
            required:"<?php echo L::Passcheck ?>",
            minlength:"<?php echo L::Confirmincheck ?>"
        },
        loginTea:"<?php echo L::Logincheck ?>",
        emailTea:"<?php echo L::PleaseEnterEmailAdress ?>",
        contactTea:"<?php echo L::PleaseEnterPhoneNumber ?>",
        datenaisTea:"<?php echo L::PleaseEnterPhonestudentTab ?>",
        prenomTea:"<?php echo L::PleaseEnterPrename ?>",
        nomTea:"<?php echo L::PleaseEnterName ?> ",
        fonctionTea:"<?php echo L::PleaseEnterFonction ?>",
        gradeTea:"<?php echo L::EnterGrade ?>",
        dateEmbTea:"<?php echo L::EnterDateEmbauche ?>",
        sexeTea:"<?php echo L::PleaseEnterSexe ?>",
        // cniTea:"Merci de renseigner le numéro CNI"
      },
      submitHandler: function(form) {
        //verifier si ce compte n'existe pas encore dans la base de données
           var nb=$("#nbtab").val();
           // console.log(group-a);


           if(nb==1)
           {
             alert("une saisie");
           }else {
             alert("multi saisie");
           }

           // form.submit();
      }


   });


 });
 </script>
    <!-- end js include path -->
  </body>

</html>
