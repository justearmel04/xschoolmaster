<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/Tache.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

// echo $_SESSION['user']['fonctionuser'];
if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$tachex = new Tache();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

  if($_SESSION['user']['groupe']==1)
  {
    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="DAF"||$_SESSION['user']['fonctionuser']=="Intendance"||$_SESSION['user']['fonctionuser']=="Marketing"||$_SESSION['user']['fonctionuser']=="CAES"||$_SESSION['user']['fonctionuser']=="CD"||$_SESSION['user']['fonctionuser']=="CC")
    {
      // echo "bonsoir";
      // $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);

      //nous allons chercher la liste des etablissements du groupe

      $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
        }

        if(strlen($_SESSION['user']['codeEtab'])>0)
        {

        }else {
          $_SESSION['user']['codeEtab']=$codeEtabAssigner;
        }

        $_SESSION['user']['session']=$libellesessionencours;





         // var_dump($allcodeEtabs);

        $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

        $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


    }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
    {
      // echo "bonjour";
      // $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);
      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
          $_SESSION['user']['session']=$libellesessionencours;

          if(strlen($_SESSION['user']['codeEtab'])>0)
          {

          }else {
            $_SESSION['user']['codeEtab']=$codeEtabAssigner;
          }

          $notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

          // $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
          $classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);
          $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

          // var_dump($classes);
        }

    }
  }

$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);

$lasttachesassignated=$tachex->gettacherassignerTouserlast($_SESSION['user']['IdCompte']);

$lasttachesassignatedby=$tachex->gettacherassignerTouserlastby($_SESSION['user']['IdCompte']);

$nbnotificationstandby=$parents->getParentnotificationstandbyNb($compteuserid);

//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($compteuserid);

$courses=$etabs->getAllTeatcherdevoirsEtab($codeEtabAssigner,$libellesessionencours);


  // var_dump($lasttachesassignated);

// echo $libellesessionencours."/".$_SESSION['user']['codeEtab'];

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <link href="../assets2/css/style1.css" rel="stylesheet" type="text/css" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
              <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title"><?php echo L::DevoisLists ?></div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                            </li>
                            <li><a class="parent-item" href="devoirs.php"><?php echo L::DevoirsMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                            </li>
                            <li class="active"><?php echo L::DevoisLists ?></li>
                        </ol>
                    </div>
                </div>


        <!-- start widget -->
        <?php

              if(isset($_SESSION['user']['addclasseok']))
              {

                ?>
                <!--div class="alert alert-success alert-dismissible fade show" role="alert">
              <?php
              //echo $_SESSION['user']['addetabok'];
              ?>
              <a href="#" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
                 </a>
              </div-->
<link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
<script src="../assets/js/sweetalert2.min.js"></script>

          <script>
          Swal.fire({
          type: 'success',
          title: '<?php echo L::Felicitations ?>',
          text: '<?php echo $_SESSION['user']['addclasseok'] ?>',

          })
          </script>
                <?php
                unset($_SESSION['user']['addclasseok']);
              }

               ?>

               <?php
               if($nbsessionOn==0)
               {
                 ?>
                 <div class="alert alert-danger alert-dismissible fade show" role="alert">

                 <?php echo L::RequiredScolaireYear ?>

                 <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
                  </a>
                 </div>
                 <?php
               }
                ?>

                <?php

                if(count($courses)>0)
                {
                  ?>

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="card card-box">
                            <div class="card-head">
                                <header></header>

                            </div>


                              <div class="card-body" id="bar-parent">
                                <!-- <div class="row pull-right"> -->
                                <?php
                                if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="CC"||$_SESSION['user']['fonctionuser']=="CD")
                                {
                                  ?>
                                  <a href="adddevoirs.php" class="btn btn-primary btn-md pull-right"> <i class="fa fa-plus"></i> <?php echo L::Addadevoirs ?> </a>
                                  <?php
                                }
                                 ?>

                                <!-- </div>  -->
                                <br><br>
                                  <div class="row">

                                    <?php
                                    foreach ($courses as  $value):
                                      ?>
                                      <div class="col-lg-4 col-md-4 col-12 col-sm-6">
                                        <div class="blogThumb">
                                          <div class="thumb-center"><img class="img-responsive" alt="user"
                                              src="../photo/course2.jpg"></div>
                                          <div class="course-box">

                                            <div class="" style="text-align:center">
                                              <h4>
                                                <?php
                                                  if($value->statut_dev==0)
                                                  {
                                                    ?>
                                                      <span class="label label-lg label-warning"  style="text-align:center;">  <?php echo $value->libelle_dev ?></span>
                                                    <?php
                                                  }else {
                                                    ?>
                                                    <span class="label label-lg label-success"  style="text-align:center;">  <?php echo $value->libelle_dev ?></span>
                                                    <?php
                                                  }
                                                 ?>

                                              </h4>
                                            </div>
                                            <?php

                                            $tabdate=explode("-",$value->datecrea_dev);
                                            $tabnew=explode(" ",$tabdate[2]);
                                             ?>
                                            <div class="text-muted"><span class="m-r-10"><?php echo $tabnew[0]." ".obtenirLibelleMois($tabdate[1])." ".$tabdate[0] ?></span>
                                              <!-- <a class="course-likes m-l-10" href="#"><i
                                                  class="fa fa-heart-o"></i> 654</a> -->
                                            </div>
                                            <p><span><i class="fa fa-calendar"></i> <?php echo L::DateLimit ?> : <?php echo date_format(date_create($value->datelimite_dev),"d/m/Y");?></span></p>
                                            <p><span><i class="fa fa-university"> </i><?php echo L::ClasseMenu ?> : <?php echo $value->libelle_classe   ?></span></p>
                                            <p><span><i class="fa fa-graduation-cap"></i> <?php echo L::MatiereMenusingle;  ?> : <?php echo $value->libelle_mat ?></span></p>
                                            <!-- <p><span><i class="fa fa-graduation-cap"></i> Students: 200+</span></p> -->
                                            <a href="detaildevoirs.php?course=<?php echo $value->id_dev ?>&classeid=<?php echo $value->classe_dev ?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-info"><?php echo L::Details ?></a>
                                            <!-- <button type="button"
                                              class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-info">DETAILS</button> -->
                                          </div>
                                          <div class="row list-separated profile-stat">
                                              <div class="col-md-7 col-sm-7 col-7">
                                                <?php
                                                if($value->statut_dev==0)
                                                {
                                                  ?>
                                                  <p><span class=" pull-right" style="color:#FFB64D"> <?php echo L::PublishingNOK ?> </span></p>
                                                  <?php
                                                }else {
                                                  ?>
                                                  <p><span class="pull-right" style="color:#2ed8b6"> <?php echo L::PublishingOK ?></span></p>
                                                  <?php
                                                }
                                                 ?>

                                              </div>

                                          </div>
                                        </div>
                                      </div>
                                      <?php
                                    endforeach;
                                     ?>



                                  </div>
                              </div>



                        </div>
                    </div>
                </div>
                <?php
              }else {
                ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">

                <?php echo L::Nocourseday ?>

                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                 </a>
                </div>
                <?php
              }
               ?>
        <!-- end widget -->
        <!-- chart start -->



              </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <!-- start chat sidebar -->

                        <div class="chat-sidebar-container" data-close-on-body-click="false">
                        <div class="chat-sidebar">
                          <ul class="nav nav-tabs">
                            <li class="nav-item">
                              <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                                  class="material-icons">
                                  chat</i>Chat
                                <!-- <span class="badge badge-danger">4</span> -->
                              </a>
                            </li>
                          </ul>
                          <div class="tab-content">
                            <!-- Start User Chat -->
                            <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                              id="quick_sidebar_tab_1"> -->
                              <div class="chat-sidebar-chat "
                                >
                              <div class="chat-sidebar-list">
                                <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                                  data-wrapper-class="chat-sidebar-list">
                                  <div class="chat-header">
                                    <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                                  </div>
                                  <ul class="media-list list-items">
                                    <?php

                                    foreach ($onlineUsers as  $valueUsers):
                                      $tofuser=$valueUsers->photo_compte;
                                      if(strlen($tofuser)>0)
                                      {
                                        $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                                      }else {
                                        $lientofuser="../photo/user5.jpg";
                                      }
                                      if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                                      {
                                        ?>
                                        <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                            width="35" height="35" alt="...">
                                          <i class="online dot red"></i>
                                          <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                            <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                            <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                                          </div>
                                        </li>
                                        <?php
                                      }
                                      ?>

                                      <?php
                                    endforeach;
                                     ?>

                                  </ul>
                                  <div class="chat-header">
                                    <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                                  </div>
                                  <ul class="media-list list-items">
                                    <?php

                                    foreach ($offlineUsers as  $valueUsers):
                                      $tofuser=$valueUsers->photo_compte;
                                      if(strlen($tofuser)>0)
                                      {
                                        $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                                      }else {
                                        $lientofuser="../photo/user5.jpg";
                                      }
                                      ?>
                                      <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                          width="35" height="35" alt="...">
                                        <i class="offline dot"></i>
                                        <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                          <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                          <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                                        </div>
                                      </li>
                                      <?php
                                    endforeach;
                                     ?>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <!-- End User Chat -->
                          </div>
                        </div>
                      </div>
                        <!-- end chat sidebar -->
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
  <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- morris chart -->

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>

     <script src="../assets2/plugins/chart-js/Chart.bundle.js" ></script>
     <script src="../assets2/plugins/chart-js/utils.js" ></script>
     <!-- <script src="../assets2/js/pages/chart/chartjs/chartjs-data.js" ></script> -->
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>

  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>



   function terminer(idcompte,idtache)
   {
     // alert(idcompte);
     var etape=1;

     Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::Haveyoufinishtasks ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::True ?>',
cancelButtonText: '<?php echo L::False ?>',
}).then((result) => {
if (result.value) {

  $.ajax({
    url: '../ajax/taches.php',
    type: 'POST',
    async:false,
    data: 'etape=' + etape+ '&idcompte=' +idcompte+'&idtache='+idtache,
    dataType: 'text',
    success: function (content, statut) {

    // location.reload();

    }
  });

}else {

}
})
   }

   function addmessages(senderid,receiverid)
   {

     var etape=1;
     $.ajax({
       url: '../ajax/chat.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&senderid=' +senderid+'&receiverid='+receiverid,
       dataType: 'text',
       success: function (content, statut) {

       document.location.href="chats.php";

       }
     });
   }

   // function SetcodeEtab(codeEtab)
   // {
   //   var etape=3;
   //   $.ajax({
   //     url: '../ajax/sessions.php',
   //     type: 'POST',
   //     async:false,
   //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
   //     dataType: 'text',
   //     success: function (content, statut) {
   //
   // window.location.reload();
   //
   //     }
   //   });
   // }

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function loader()
   {
     var etape=1;
     var codeEtab="<?php echo $_SESSION['user']['codeEtab'];?>";
     var session="<?php echo $libellesessionencours ; ?>";

     $.ajax({
       url: '../ajax/load.php',
       type: 'POST',
       async:false,
       data: 'codeEtab=' +codeEtab+ '&etape='+etape+'&session='+session,
       dataType: 'text',
       success: function (content, statut) {

         var data=content;

         // callback(data);

       }
     });

   }

   $(document).ready(function() {

     // alert(session);

     var MONTHS = ['<?php echo L::JanvLib?>','<?php echo L::FevLib ?>','<?php echo L::MarsLib ?>','<?php echo L::AvriLib ?>','<?php echo L::MaiLib ?>','<?php echo L::JuneLib ?>','<?php echo L::JulLib ?>','<?php echo L::AoutLib ?>','<?php echo L::SeptLib?>','<?php echo L::OctobLib?>','<?php echo L::NovbLib?>','<?php echo L::DecemLib?>'];
        var config = {
            type: 'line',
            data: {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [{
                    label: "Paiements Attendus",
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor()
                    ],
                    fill: false,
                }, {
                    label: "Paiements reçus",
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor()
                    ],
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Mois'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: ''
                        }
                    }]
                }
            }
        };
        var ctx = document.getElementById("chartjs_line").getContext("2d");
        window.myLine = new Chart(ctx, config);

     // var randomScalingFactor = function() {
     //       return Math.round(Math.random() * 100);
     //   };

       var config = {
           type: 'doughnut',
           data: {
               datasets: [{
                   data: [
                       <?php echo $etabs->getnumberOfwomenchild($_SESSION['user']['codeEtab'],$libellesessionencours); ?>,
                       // randomScalingFactor(),
                       // randomScalingFactor(),
                       // randomScalingFactor(),
                       <?php echo $etabs->getnumberOfmenchild($_SESSION['user']['codeEtab'],$libellesessionencours); ?>,
                   ],
                   backgroundColor: [
                       window.chartColors.red,
                       // window.chartColors.orange,
                       // window.chartColors.yellow,
                       // window.chartColors.green,
                       window.chartColors.blue,
                   ],
                   label: 'Dataset 1'
               }],
               labels: [
                   "<?php echo L::Filles ?>",
                   // "Orange",
                   // "Yellow",
                   // "Green",
                   "<?php echo L::Garcons ?>"
               ]
           },
           options: {
               responsive: true,
               legend: {
                   position: 'top',
               },
               title: {
                   display: true,
                   text: ''
               },
               animation: {
                   animateScale: true,
                   animateRotate: true
               }
           }
       };

           var ctx = document.getElementById("chartjs_doughnut").getContext("2d");
           window.myDoughnut = new Chart(ctx, config);


   });


   </script>
    <!-- end js include path -->
  </body>

</html>
