<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

  if($_SESSION['user']['groupe']==1)
  {
    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="DAF"||$_SESSION['user']['fonctionuser']=="Intendance"||$_SESSION['user']['fonctionuser']=="Marketing"||$_SESSION['user']['fonctionuser']=="CAES"||$_SESSION['user']['fonctionuser']=="CD"||$_SESSION['user']['fonctionuser']=="CC")
    {
      // $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);

      //nous allons chercher la liste des etablissements du groupe

      $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
        }

        if(strlen($_SESSION['user']['codeEtab'])>0)
        {

        }else {
          $_SESSION['user']['codeEtab']=$codeEtabAssigner;
        }

        $_SESSION['user']['session']=$libellesessionencours;





         // var_dump($allcodeEtabs);

        $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

        $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


    }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
    {
      // $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);
      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
          $_SESSION['user']['session']=$libellesessionencours;

          if(strlen($_SESSION['user']['codeEtab'])>0)
          {

          }else {
            $_SESSION['user']['codeEtab']=$codeEtabAssigner;
          }

          $notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

          // $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
          $classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);
          $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);
        }

    }
  }

  $nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
  // var_dump($allcodeEtabs);

// echo $libellesessionencours;
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);

$datasinfos=$etabs->getMessagesInformationsender($_GET['msg'],$_SESSION['user']['codeEtab'],$libellesessionencours,$_SESSION['user']['IdCompte']);
foreach ($datasinfos as  $value):
  $messageslibelle=$value->commentaire_msg;
endforeach;
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
    <!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!--bootstrap -->
    <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

    <!-- data tables -->
    <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>

    <!-- Material Design Lite CSS -->
    <link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
    <link href="../assets2/css/material_style.css" rel="stylesheet">

    <!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- wizard -->
    	<link rel="stylesheet" href="../assets2/css/pages/steps.css">

    <!-- Theme Styles -->
      <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
      <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="../assets2/css/pages/inbox.min.css" rel="stylesheet" type="text/css" />

      <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <!-- Theme Styles -->

    <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />





 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::ModifyingNotif ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="#"><?php echo L::NotificationMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::ModifyingNotif ?></li>
                          </ol>
                      </div>
                  </div>
                  <?php

                        if(isset($_SESSION['user']['addprogra']))
                        {

                          ?>
                          <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?php
                        //echo $_SESSION['user']['addetabok'];
                        ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                           </a>
                        </div-->
                        <div class="alert alert-danger" role="alert">
          <?php echo $_SESSION['user']['addprogra'] ?>
        </div>


                          <?php
                          unset($_SESSION['user']['addprogra']);
                        }

                         ?>
					<!-- start widget -->
          <div class="inbox">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-topline-gray">
                                <div class="card-body no-padding height-9">
									<div class="row">
			                            <!--div class="col-md-3">
			                                <div class="inbox-sidebar">
			                                    <a href="email_compose.html" data-title="Compose" class="btn red compose-btn btn-block m-0">
			                                        <i class="fa fa-edit"></i> Compose </a>
			                                    <ul class="inbox-nav inbox-divider">
			                                        <li class="active"><a href="#"><i
															class="fa fa-inbox"></i> Inbox <span
															class="label mail-counter-style label-danger pull-right">2</span></a>
			                                        </li>
			                                        <li><a href="#"><i
															class="fa fa-envelope"></i> Sent Mail</a>
			                                        </li>
			                                        <li><a href="#"><i
															class="fa fa-briefcase"></i> Important</a>
			                                        </li>
			                                        <li><a href="#"><i
															class="fa fa-star"></i> Starred </a>
			                                        </li>
			                                        <li><a href="#"><i
															class=" fa fa-external-link"></i> Drafts <span
															class="label mail-counter-style label-info pull-right">30</span></a>
			                                        </li>
			                                        <li><a href="#"><i
															class=" fa fa-trash-o"></i> Trash</a>
			                                        </li>
			                                    </ul>
			                                    <ul class="nav nav-pills nav-stacked labels-info inbox-divider">
			                                        <li>
			                                            <h4>Labels</h4>
			                                        </li>
			                                        <li><a href="#"><i
															class="fa fa-tags text-info"></i>  Work</a>
			                                        </li>
			                                        <li>
			                                            <a href="#">
			                                                <i class=" fa fa-tags text-warning"></i> Design
			                                            </a>
			                                        </li>
			                                        <li>
			                                            <a href="#">
			                                                <i class=" fa fa-tags text-danger text-success"></i> Family
			                                            </a>
			                                        </li>
			                                        <li>
			                                            <a href="#">
			                                                <i class=" fa fa-tags text-purple"></i> Friends
			                                            </a>
			                                        </li>
			                                        <li>
			                                            <a href="#">
			                                                <i class=" fa fa-tags "></i> Office
			                                            </a>
			                                        </li>
			                                    </ul>
			                                    <ul class="nav nav-pills nav-stacked labels-info inbox-divider ">
			                                        <li>
			                                            <h4>Buddy online</h4>
			                                        </li>
			                                        <li>
			                                            <a href="#">
			                                                <i class=" fa fa-comments text-success"></i> Jhone Doe
			                                               <span class="online-status">I do not think</span>
			                                            </a>
			                                        </li>
			                                        <li>
			                                            <a href="#">
			                                                <i class=" fa fa-comments text-danger"></i> Sumon
			                                                <span class="online-status">Busy with coding</span>
			                                            </a>
			                                        </li>
			                                        <li>
			                                            <a href="#">
			                                                <i class=" fa fa-comments text-purple "></i> Anjelina Joli
			                                                <span class="online-status">I out of control</span>
			                                            </a>
			                                        </li>
			                                        <li>
			                                            <a href="#">
			                                                <i class=" fa fa-comments text-success "></i> Jonathan Smith
			                                                <span class="online-status">I am not here</span>
			                                            </a>
			                                        </li>
			                                        <li>
			                                            <a href="#">
			                                                <i class=" fa fa-comments text-info "></i> Tawseef
			                                                <span class="online-status">I do not think</span>
			                                            </a>
			                                        </li>
			                                    </ul>
			                                </div>
			                            </div-->

			                            <div class="col-md-9">
			                                <div class="inbox-body">
		                                    <div class="inbox-header">
		                                        <div class="mail-option">
		                                            <!-- <div class="btn-group margin-top-20 ">
		                                                <button class="btn btn-primary btn-sm margin-right-10"><i class="fa fa-check"></i> Send</button>
		                                                <button class="btn btn-sm btn-default margin-right-10"><i class="fa fa-times"></i> Discard</button>
		                                                <button class="btn btn-sm btn-default margin-right-10">Draft</button>
		                                            </div> -->
		                                        </div>
		                                    </div>
                                        <form class="" action="../controller/summernotes.php" method="post" id="FormAddExam" >

                                            <div class="form-group">
                                               <!-- <label class="control-label col-md-12"> <?php echo L::ObjetNotifications ?>
                                                   <span class="required"> * </span>
                                               </label> -->
                                               <div class="col-md-12">
                                                   <!-- <select class="form-control input-height" id="objet" name="objet" style="width:100%" onchange="checkobjet()" >
                                                      <option value=""> <?php echo L::SelectObjetNotifications ?> </option>
                                                      <option value="1-Reminder"><?php echo L::ReminderNotifications ?> </option>
                                                      <option value="2-Conference"><?php echo L::ConferenceNotifications ?> </option>
                                                      <option value="3-Devoir non rendu"> <?php echo L::DevoirNogiveNotifications ?> </option>
                                                      <option value="4-Absence"> <?php echo L::AbsenceNotifications ?> </option>
                                                      <option value="5-Résultat d’évaluation"> <?php echo L::EvaluationResultNotifications ?> </option>
                                                      <option value="6-Discipline"> <?php echo L::DisciplneNotifications ?> </option>
                                                      <option value="7-Participation en classe"> <?php echo L::ParticipationNotifications ?> </option>
                                                      <option value="8-Autre"> <?php echo L::OthersNotifications ?>  </option>

                                                  </select> -->

                                                  <input type="hidden" name="etape" id="etape"  value="1">
                                                  <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $_SESSION['user']['codeEtab']; ?>">
                                                  <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                                  <input type="hidden" name="messageid" id="messageid" value="<?php echo $_GET['msg'] ?>">
                                                 </div>
                                        </div>
                                        <div class="form-group" id="rowother">

                                           <div class="col-md-12">
                                               <input  placeholder="<?php echo L::PrecisObjetNotifications ?>" name="otherobjet" id="otherobjet"  value="" class="form-control" style="width:100%">
                                             </div>
                                    </div>


                                        <!-- <div class="form-group">
                                           <label class="control-label col-md-12"> <?php echo L::DestinatairesNotifications ?>
                                               <span class="required"> * </span>
                                           </label>
                                           <div class="col-md-12">
                                             <select class="form-control input-height" id="destinataires" name="destinataires" style="width:100%" onchange="selectiondestinataires()" >
                                                <option value="" selected> <?php echo L::SelectDestinatairesNotifications ?> </option>
                                                <option value="Parent"  ><?php echo L::ParentCaps  ?></option>
                                                <option value="Teatcher"><?php echo L::ProfsMenuCaps ?></option>
                                                <option value="Admin_locale"><?php echo L::responsablesCaps ?></option>

                                            </select>
                                             </div>
                                        </div> -->

                                    <div class="form-group" id="rowclasseEtab">
                                       <label class="control-label col-md-12"> <?php echo L::ClasseconcerneNotifications ?>
                                           <span class="required"> * </span>
                                       </label>
                                       <div class="col-md-12">
                                         <select class="form-control input-height" multiple="multiple" id="classeEtab" name="classeEtab[]" style="width:100%">

                                             <?php
                                             $i=1;
                                               foreach ($classes as $value):
                                               ?>
                                               <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                               <?php
                                                                                $i++;
                                                                                endforeach;
                                                                                ?>

                                         </select>
                                         </div>
                                </div>
                                <!-- <div class="form-group" id="rowprecision">
                                   <label class="control-label col-md-12"> <?php echo L::destinatairestypes ?>
                                       <span class="required"> * </span>
                                   </label>
                                   <div class="col-md-12">
                                     <select class="form-control input-height" id="precis" name="precis" style="width:100%" onchange="selectionprecis()" >
                                        <option value="" selected> <?php echo L::	selectdestinatairestypes ?> </option>
                                        <option value="1" selected ><?php echo L::NoPrecisNotifications ?></option>
                                        <option value="2"><?php echo L::PrecisNotifications ?></option>

                                    </select>
                                     </div>
                            </div> -->

                            <div class="form-group" id="rowprecisioneleves">
                               <label class="control-label col-md-12"> <?php echo L::studMenu ?>
                                   <span class="required"> * </span>
                               </label>
                               <div class="col-md-12">
                                 <select class="form-control input-height" multiple="multiple" id="eleves" name="eleves[]" style="width:100%"  >



                                </select>
                                 </div>
                        </div>
                        <div class="form-group" id="rowprecisiondestinataires">

                           <div class="col-md-12">
                             <select class="form-control input-height" id="precisiondestinataires" multiple="multiple" name="precisiondestinataires[]" style="width:100%"  >


                            </select>
                             </div>
                    </div>


		                                    <div class="inbox-body no-pad">
		                                        <div class="mail-list">
		                                            <div class="compose-mail">
		                                                <!-- <form method="post"> -->



		                                                    <div class="compose-editor">
		                                                        <!-- <div id="summernote"></div> -->
                                                            <textarea class="form-control" name="summernote" id="summernote" style="height: 400px;"><?php echo $messageslibelle ?></textarea>
		                                                        <!-- <input type="file" class="default" id="fichier" name="fichier"  multiple> -->
		                                                    </div>
		                                                    <div class="btn-group margin-top-20 " style="text-align:center">
				                                                <button class="btn btn-success btn-sm margin-right-10" onclick="changeEtape(7)"><i class="fa fa-check"></i> <?php echo L::ModifierBtn ?></button>
				                                                <!-- <button class="btn btn-sm btn-default margin-right-10"><i class="fa fa-times"></i> Discard</button> -->
		                                                		<!-- <button class="btn btn-sm btn-primary margin-right-10" onclick="changeEtape(1)"><?php //echo L::SaveAndsendAfterNotifications ?></button> -->
		                                            		    </div>
		                                                <!-- </form> -->
		                                            </div>
		                                        </div>
		                                    </div>
                                      </form>
		                                </div>
			                            </div>
			                        </div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- corps -->

            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
    <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
	<script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js"></script>
    <!-- bootstrap -->
<script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
<script src="../assets2/plugins/select2/js/select2.js" ></script>
<script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <!-- wizard -->
  <script src="../assets2/plugins/steps/jquery.steps.js" ></script>
 <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
 <script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
 <script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
     <!-- calendar -->
  <script src="../assets2/plugins/moment/moment.min.js" ></script>
  <script src="../assets2/js/app.js" ></script>
  <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/jquery-dateformat.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
  <script src="../assets/js/formatter/jquery.formatter.min.js"></script>
  <script src="../assets2/plugins/summernote/summernote.js" ></script>


  <script type="text/javascript">

  // function SetcodeEtab(codeEtab)
  // {
  //   var etape=3;
  //   $.ajax({
  //     url: '../ajax/sessions.php',
  //     type: 'POST',
  //     async:false,
  //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
  //     dataType: 'text',
  //     success: function (content, statut) {
  //
  // window.location.reload();
  //
  //     }
  //   });
  // }
  function addFrench()
  {
    var etape=1;
    var lang="fr";
    $.ajax({
      url: '../ajax/langue.php',
      type: 'POST',
      async:false,
      data: 'etape=' + etape+ '&lang=' +lang,
      dataType: 'text',
      success: function (content, statut) {

  window.location.reload();

      }
    });
  }

  function addEnglish()
  {
    var etape=1;
    var lang="en";
    $.ajax({
      url: '../ajax/langue.php',
      type: 'POST',
      async:false,
      data: 'etape=' + etape+ '&lang=' +lang,
      dataType: 'text',
      success: function (content, statut) {

  window.location.reload();

      }
    });
  }

  function changeEtape(id)
  {

    // var sauvegarde=$("#sauvegarde").val();

    // const code = $('[name="summernote"]').summernote('code');
    // const code1=$('#summernote').val();
    //
    // alert(code+ " "+code1);

    $("#etape").val(id);
  }


  function checkobjet()
  {
    var objet=$("#objet").val();
    var tabobjet=objet.split("-");

    var objetid=tabobjet[0];

    if(objetid==8)
    {
        // createOther();
        $("#rowother").show();
    }else {
      // deleteOther();
      $("#rowother").hide();
    }

  }

  function restauration()
  {
    $("#rowclasseEtab").hide();
    $("#rowprecisioneleves").hide();
    $("#rowprecisiondestinataires").hide();
  }

  function selectiondestinataires()
  {
    restauration();
    var destinataires=$("#destinataires").val();
    if(destinataires=="Admin_locale")
    {
      $("#rowclasseEtab").hide();
    }else {
      $("#rowclasseEtab").show();
    }
  }

  function createdestinatairesTeatchers()
  {
    var classeselected=$("#classeEtab").val();
    var session=$("#libellesession").val();
    var codeEtab="<?php echo $_SESSION['user']['codeEtab'] ?>";
    var etape=32;
    $.ajax({
      url: '../ajax/admission.php',
      type: 'POST',
      async:true,
      data: 'classe=' +classeselected+ '&etape=' + etape+'&session='+session+'&codeEtab='+codeEtab,
      dataType: 'text',
      success: function (content, statut) {

        $("#precisiondestinataires").html("");
        $("#precisiondestinataires").html(content);



      }
    });
  }

  function createdestinatairesRespos()
  {
    // var classeselected=$("#classeEtab").val();
    var session=$("#libellesession").val();
    var codeEtab="<?php echo $_SESSION['user']['codeEtab'] ?>";
    var etape=33;
    $.ajax({
      url: '../ajax/admission.php',
      type: 'POST',
      async:true,
      data: 'etape=' + etape+'&session='+session+'&codeEtab='+codeEtab,
      dataType: 'text',
      success: function (content, statut) {

        $("#precisiondestinataires").html("");
        $("#precisiondestinataires").html(content);



      }
    });
  }

  function createdestinatairesEleves()
  {

    //nous allons chercher la liste des eleves de ces classes

    var classeselected=$("#classeEtab").val();
    var session=$("#libellesession").val();
    var codeEtab="<?php echo $_SESSION['user']['codeEtab'] ?>";
    var etape=31;
    $.ajax({
      url: '../ajax/admission.php',
      type: 'POST',
      async:true,
      data: 'classe=' +classeselected+ '&etape=' + etape+'&session='+session+'&codeEtab='+codeEtab,
      dataType: 'text',
      success: function (content, statut) {

        $("#eleves").html("");
        $("#eleves").html(content);



      }
    });


  }


  function selectionprecis()
  {
    var precis=$("#precis").val();
    var destinataires=$("#destinataires").val();

    if(precis==2)
    {
       if(destinataires=="Parent")
       {
         //liste des eleves de cette classe
         $("#rowprecisioneleves").show();

         //nous allons chercher la liste des élèves de ces classes
         createdestinatairesEleves();


       }else if(destinataires=="Teatcher")
       {
         $("#rowprecisioneleves").hide();
         $("#rowprecisiondestinataires").show();

         createdestinatairesTeatchers();

       }else if(destinataires=="Admin_locale")
       {
         $("#rowprecisioneleves").hide();
         $("#rowprecisiondestinataires").show();
         createdestinatairesRespos();
         //liste des responsables de l'établissement
       }
    }else if(precis==1)
    {
      $("#rowprecisioneleves").hide();
      $("#rowprecisiondestinataires").hide();
    }

    // if(destinataires=="Parent" && precis==2)
    // {
    //   //liste des enfants de cette classe
    //   // $("#rowclasseEtab").hide();
    //   $("#rowprecisioneleves").show();
    // }else if(destinataires=="Teatcher" && precis==1)
    // {
    //   //liste des enseignants de cette classe
    // }else if(destinataires=="Admin_locale" && precis==1)
    // {
    //   //liste des responsables de l'établissement
    // }

  }

   jQuery(document).ready(function() {


     var form = $("#FormAddExam").show();

     form.validate({
       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
     },
     highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
     },
     success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },

       rules:{
         classeEtab:"required",
         matiere:"required",
         classe:"required",
         teatcher:"required",
         coef:"required",
         examen:"required",
         datedeb:"required",
         datefin:"required",
         typesess:"required",
         montantAct:"required",
         libelactivity:"required",
         message:"required",
         denomination:"required",
         typeactivite:"required",
         locationactivite:"required",
         descripactivite:"required",
         respoactivite:"required",
         contactrespo:"required",
         destinataires:"required",
         eleves:"required",
         dateobjet:"required",
         objet:"required",
         commentaire:"required",
         // summernote:{
         //   'required':{
         //     depends:function(element){
         //       // const code = $('#summernote').summernote('code');
         //       // return ($('#summernote').val()=='<p><br></p>');
         //       return ($('#summernote').summernote('code') == "" || $('#summernote').summernote('code')== "<p><br></p>");
         //     }
         //   }
         // }





       },
       messages: {
         classeEtab:"<?php echo L::PleaseSelectaumoinsClasse ?>",
         matiere:"<?php echo L::PleaseEnterMatiere ?>",
         classe:"<?php echo L::PleaseSelectclasserequired ?>",
         teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
         coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
         examen:"<?php echo L::PleaseEnterExamLib ?>",
         datedeb:"<?php echo L::PleaseEnterParascoActivityDateStart ?>",
         datefin:"<?php echo L::PleaseEnterParascoActivityDateEnd ?>",
         typesess:"<?php echo L::PeriodRequired ?>",
         montantAct:"<?php echo L::PleaseEnterParascoActivityAmount ?>",
         libelactivity:"<?php echo L::PleaseEnterParascoActivityName ?>",
         message:"<?php echo L::PleaseEnterActivityMesaage ?>",
         denomination:"<?php echo L::PleaseEnterActivityName ?>",
         typeactivite:"<?php echo L::PleaseEnterActivityType ?>",
         locationactivite:"<?php echo L::PleaseEnterActivityLocation ?>",
         descripactivite:"<?php echo L::PleaseEnterActivityDescription ?>",
         respoactivite:"<?php echo L::PleaseEnterRespoNameMessage ?>",
         contactrespo:"<?php echo L::PleaseEnterContactRespoMessage ?>",
         destinataires:"<?php echo L::PleaseSelectaumoinsDestinatairesMessage ?>",
         eleves:"<?php echo L::PleaseSelectStudents ?>",
         dateobjet:"<?php echo L::ControlsDaterequired ?>",
         objet:"<?php echo L::PleaseSelectObjetMessage ?>",
         commentaire:"<?php echo L::PleaseEnterCommentMessage ?>",
         // summernote:"Merci de renseigner le message"


       }
     });

     $("#rowclasseEtab").hide();
     $("#rowprecisioneleves").hide();
     $("#objet").select2();
     $("#rowother").hide();
     $("#rowprecisiondestinataires").hide();
     $("#destinataires").select2();
     $("#precis").select2();
     $("#classeEtab").select2({
       tags: true,
     tokenSeparators: [',', ' ']
     });
     $("#precisiondestinataires").select2({
       tags: true,
     tokenSeparators: [',', ' ']
     });

     $("#eleves").select2({
       tags: true,
     tokenSeparators: [',', ' ']
     });
     $('[name="content"]')
        .summernote({
          placeholder: '',
          tabsize: 2,
          height: 200,
          fontSize: 20,

      toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']]
      ]
        });
 $('#summernote').summernote({
       placeholder: '',
       tabsize: 2,
       height: 200,
       fontSize: 20,

   toolbar: [
     // [groupName, [list of button]]
     ['style', ['bold', 'italic', 'underline', 'clear']],
     ['font', ['strikethrough', 'superscript', 'subscript']],
     ['fontsize', ['fontsize']],
     ['color', ['color']],
     ['para', ['ul', 'ol', 'paragraph']],
     ['height', ['height']]
   ]

     });



   });

</script>
    <!-- end js include path -->
  </body>

</html>
