<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/Teatcher.php');
require_once('../class/Diplome.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();
$classe=new Classe();
$diplome=new Diplome();
$user=new User();
$etabs=new Etab();
$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}


$teatcher=new Teatcher();

$detailsTeach=$teatcher->getTeatcherInfobyId($_GET['compte']);

$tabteatcher=explode("*",$detailsTeach);

$alletab=$etabs->getAllEtab();

$diplomes=$diplome->getDiplomebyTeatcherId($_GET['compte']);

$teatcherid=$_GET['compte'];
$childid=$_GET['child'];

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
//
if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}

$data=$teatcher->getTeatcherchildInfos($teatcherid,$childid);
$array=json_encode($data,true);
$someArray = json_decode($array, true);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
  <!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
  <!--bootstrap -->
  <!--bootstrap -->
  <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
  <!-- Material Design Lite CSS -->
  <link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
  <link href="../assets2/css/material_style.css" rel="stylesheet">
  <!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <link rel="stylesheet" href="../assets2/css/pages/steps.css">
    <style>
    #radioBtn .notActive{
  color: #3276b1;
  background-color: #fff;
}
#radioBtn1 .notActive{
color: #3276b1;
background-color: #fff;
}
#radioBtn2 .notActive{
color: #3276b1;
background-color: #fff;
}

#radioBtn3 .notActive{
color: #3276b1;
background-color: #fff;
}
    </style>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title"><?php echo L::ModifyingInfos ?></div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#"><?php echo L::ProfsMenusingle ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active"><?php echo L::Modifications ?></li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addStudok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: '<?php echo L::Felicitations ?>',
  text: "<?php echo $_SESSION['user']['addStudok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::AddNews ?>',
  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="index.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addStudok']);
                    }

                     ?>


              <div class="row">
                    	<div class="col-sm-12">
                             <div class="card-box">
                                 <div class="card-head">
                                     <header></header>
                                 </div>
                                 <div class="card-body ">
                                   <form class="" action="../controller/teatcher.php" method="post" id="FormAddStudent" enctype="multipart/form-data">

                                     <div class="form-group row">
                                             <label class="control-label col-md-4"><?php echo L::NamestudentTab ?>
                                                 <span class="required"> * </span>
                                             </label>
                                             <div class="col-md-6">
                                                 <input type="text" name="namecomplet" id="namecomplet" data-required="1" placeholder="<?php echo L::EnterChildCompletName ?>" value="<?php echo $someArray[0]["nom_childt"]; ?>" class="form-control" /> </div>
                                         </div>
                                         <div class="form-group row">
                                             <label class="control-label col-md-4"><?php echo L::BirthstudentTab ?>
                                                 <span class="required"> * </span>
                                             </label>
                                             <div class="col-md-6">
                                               <input type="text" placeholder="<?php echo L::EnterChildDateNaiss ?>" name="datenaischild" id="datenaischild" data-mask="99/99/9999" class="form-control " value="<?php echo date_format(date_create($someArray[0]["datenais_childt"]),"d/m/Y"); ?>">
                                                 <span class="help-block"><?php echo L::Datesymbolesecond ?></span></div>
                                               </div>
                                         <div class="form-group row">
                                             <label class="control-label col-md-4"><?php echo L::BirthLocation ?>
                                                 <span class="required"> * </span>
                                             </label>
                                             <div class="col-md-6">
                                                 <input type="text" name="lieunaischild" id="lieunaischild" data-required="1" placeholder="<?php echo L::EnterBirthLocation ?>" class="form-control " value="<?php echo $someArray[0]["lieunais_childt"]; ?>" /> </div>
                                         </div>
                                         <div class="form-group row">
                                             <label class="control-label col-md-4"><?php echo L::Gender ?>
                                                 <span class="required"> * </span>
                                             </label>
                                             <div class="col-md-6">
                                               <select class="form-control" name="sexechild" id="sexechild" style="width:100%">
                                                 <option value=""><?php echo L::PleaseSelectGender ?></option>
                                                 <option <?php if($someArray[0]["sexe_childt"]=="M"){echo "selected";} ?> value="M"><?php echo L::SexeM ?></option>
                                                 <option <?php if($someArray[0]["sexe_childt"]=="F"){echo "selected";} ?> value="F"><?php echo L::SexeF ?></option>
                                               </select>
                                             </div>
                                         </div>


                                               <div class="form-group row">
                                                   <label class="control-label col-md-4"><?php echo L::File ?>
                                                       <span class="required">  </span>
                                                   </label>
                                                   <div class="col-md-6">

                                                     <?php
                                                     $filesons=$teatcher->getFileOfSons($someArray[0]["id_childt"]);
                                                     $lienextrait="";
                                                     if(strlen($filesons)>0)
                                                     {
                                                       $libelledossier=str_replace('\\',"",$teatcher->getNameofTeatcherById($teatcherid))."/Enfants/";
                                                       $dossier="../Dossiers/professeurs/";
                                                       $dossier1="../Dossiers/professeurs/".$libelledossier;

                                                       if(strlen($filesons)>0)
                                                       {
                                                         $lienextrait=$dossier1.$filesons;
                                                       }
                                                     }
                                                      ?>


                                                   <input type="file" id="fichierExt" name="fichierExt" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="<?php echo $lienextrait; ?>" data-allowed-file-extensions="pdf doc docx" />

                                                   <input type="hidden" name="etape" id="etape" value="7"/>
                                                   <input type="hidden" name="idcompe" id="idcompte" value="<?php echo $_GET['compte']?>"/>
                                                   <input type="hidden" name="childid" id="childid" value="<?php echo $someArray[0]["id_childt"]; ?>"/>
                                                   <input type="hidden" name="codeEtab_upson" id="codeEtab_upson" value="<?php echo $codeEtabAssigner?>"/>
                                                   <input type="hidden" name="oldextrait" id="oldextrait" value="<?php echo strlen($filesons);?>"/>
                                                   <input type="hidden" name="oldfilelien" id="oldextrait" value="<?php echo $filesons;?>"/>
                                                 </div>
                                                     </div>
                                                     <div class="form-actions">
                                                                           <div class="row">
                                                                               <div class="offset-md-3 col-md-9">
                                                                                   <button type="submit" class="btn btn-info"><?php echo L::ModifierBtn ?></button>
                                                                                   <button type="button" class="btn btn-danger"><?php echo L::Closebtn ?></button>
                                                                               </div>
                                                                             </div>
                                                                          </div>

                                   </form>


                                 </div>
                             </div>
                         </div>
                    </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <!-- <script src="../assets2/plugins/steps/jquery.steps.js" ></script> -->


  <script>

  function addFrench()
  {
    var etape=1;
    var lang="fr";
    $.ajax({
      url: '../ajax/langue.php',
      type: 'POST',
      async:false,
      data: 'etape=' + etape+ '&lang=' +lang,
      dataType: 'text',
      success: function (content, statut) {

  window.location.reload();

      }
    });
  }

  function addEnglish()
  {
    var etape=1;
    var lang="en";
    $.ajax({
      url: '../ajax/langue.php',
      type: 'POST',
      async:false,
      data: 'etape=' + etape+ '&lang=' +lang,
      dataType: 'text',
      success: function (content, statut) {

  window.location.reload();

      }
    });
  }


  // $(document).ready(function() {
    jQuery(document).ready(function() {

    "use strict";
    // var wizard = $("#wizard_test").steps();
    var i=1;
    // determinedestinataires();

    $('#exampleModal').on('shown.bs.modal', function () {
      // console.log("bonsoir");
      //nous allons initialiser le tableau
       $("#tabStationBody").html("");
       $("#nomserach").val("");
       $("#telserach").val("");
       $("#RowInfosOld").hide();

       var content="<tr><td colspan=\"4\"><?php echo L::NoLigne ?></td><tr>";
       $("#tabStationBody").html(content);

    });

$("#submitBtn").attr("disabled",true);
$("#RowInfosOld").hide();



    desactivatebtn();
    getInfantiles();
    getAllergies();


    // var form = $("#FormAddStudent").show();
    var form = $("#FormAddStudent");
    var form1=$("#FormSearch");

    form1.validate({
      errorPlacement: function(label, element) {
      label.addClass('mt-2 text-danger');
      label.insertAfter(element);
    },
    highlight: function(element, errorClass) {
      $(element).parent().addClass('has-danger')
      $(element).addClass('form-control-danger')
    },
    success: function (e) {
          $(e).closest('.control-group').removeClass('error').addClass('info');
          $(e).remove();
      },
  rules:{
    nomserach: {
     'required': {
        depends: function (element) {
            return ($("#telserach").val()=="");

        }
    }
  },
  telserach: {
    digits:true,
   'required': {
      depends: function (element) {
          return ($("#nomserach").val()=="");

      }
  }
}

  },
  messages: {
nomserach:"<?php echo L::EnterParentsNameMinus ?>",
telserach:{
  required:"<?php echo L::EnterParentsNameMinusPhone ?>",
  digits:"<?php echo L::DigitsOnly ?>"
}


},
submitHandler: function(form) {

  //nous allons rechercher le parent
   $("#submitBtn").attr("disabled",true);
  var nomserach=$("#nomserach").val();
  var telserach=$("#telserach").val();
  var codeEtab="<?php echo $codeEtabLocal; ?>";
  var concatoldparents=$("#concatoldparents").val();
  var etape=6;
     $.ajax({
       url: '../ajax/parent.php',
       type: 'POST',
       async:false,
       data: 'nom=' + nomserach+ '&etape=' + etape+'&telephone='+telserach+'&codeEtab='+codeEtab+'&concatoldparents='+concatoldparents,
       dataType: 'text',
       beforeSend: function (xhr) {

            // $("#imgLoader").css("display", "inline");
            $("#imgLoader").show();
        //     setTimeout(function () {
        //     $("#imgLoader").show();
        // }, 100);

          },
       success: function (content, statut) {
       // $("#nbtotalparent").val(0);
       $("#RowInfosOld").hide();
       $("#imgLoader").hide(2000);
       $("#tabStationBody").html("");
       $("#tabStationBody").html(content);

       }
     });

}

    });

    form.validate({
      errorPlacement: function(label, element) {
      label.addClass('mt-2 text-danger');
      label.insertAfter(element);
    },
    highlight: function(element, errorClass) {
      $(element).parent().addClass('has-danger')
      $(element).addClass('form-control-danger')
    },
    success: function (e) {
          $(e).closest('.control-group').removeClass('error').addClass('info');
          $(e).remove();
      },

      rules:{
        // lastschool:"required",
        guardian:"required",
        telBguardian:"required",
        telMobguardian:"required",
        domicileguardian:"required",
        adresgeodetails:"required",
        blood:"required",
        cantineEtab:"required",
        matri:"required",
        matriselect:"required",
        matri: {
				 'required': {
 						depends: function (element) {
 								return ($("#inscript").val()==1);

 						}
 				}
			},
        matriselect: {
				 'required': {
 						depends: function (element) {
 								return ($("#inscript").val()==2);

 						}
 				}
			},
        nomad:"required",
        prenomad:"required",
        datenaisad:"required",
        lieunais:"required",
        sexe:"required",
        classeEtab:"required"






      },
      messages: {
        // lastschool:"Merci de renseigner la derni",
        guardian:"<?php echo L::PleaseEnterPreventPersonne ?>",
        telBguardian:"<?php echo L::PleaseEnterTelBureau ?>",
        telMobguardian:"<?php echo L::PleaseEnterTelMobile ?>",
        domicileguardian:"<?php echo L::PleaseEnterTelDomicile ?>",
        adresgeodetails:"<?php echo L::PleaseEnterGeoAdress ?>",
        blood:"<?php echo L::PleaseEnterBloodInfosTab ?>",
        cantineEtab:"<?php echo L::PleaseEntercantMenu ?>",
        matri:"<?php echo L::PleaseEnterMatriculestudentTab ?>",
        matriselect:"<?php echo L::PleaseSelectrMatriculestudentTab ?>",
        nomad:"<?php echo L::PleaseEnterStudentName ?>",
        prenomad:"<?php echo L::PleaseEnterStudentPreName ?>",
        datenaisad:"<?php echo L::PleaseEnterStudentDatenaiss ?>",
        lieunais:"<?php echo L::PleaseEnterStudentBirthLocation ?>",
        sexe:"<?php echo L::PleaseEnterStudentGender ?>",
        classeEtab:"<?php echo L::PleaseSelectaumoinsClasse ?>",
        matiere:"<?php echo L::PleaseEnterMatiere ?>"



      }
    });
// form.steps({
//   headerTag: "h3",
//   bodyTag: "fieldset",
//   transitionEffect: "slideLeft",
//   onStepChanging: function (event, currentIndex, newIndex)
//   {
//       // Allways allow previous action even if the current form is not valid!
//       if (currentIndex > newIndex)
//       {
//           return true;
//       }
//
//       if ($("#nbtotalparent").val()==0)
//       {
//         // Swal.fire({
//         //         type: 'warning',
//         //         title: '<?php echo L::WarningLib ?>',
//         //         text: "Vous devez ajouter au moins un parent à cet élève",
//         //
//         //         })
//         //   return false;
//       }
//
//       // Forbid next action on "Warning" step if the user is to young
//       // if (newIndex === 3 && Number($("#age-2").val()) < 18)
//       // {
//       //     return false;
//       // }
//       // Needed in some cases if the user went back (clean up)
//       if (currentIndex < newIndex)
//       {
//           // To remove error styles
//           form.find(".body:eq(" + newIndex + ") label.error").remove();
//           form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
//       }
//       form.validate().settings.ignore = ":disabled,:hidden";
//       return form.valid();
//   },
//   onStepChanged: function (event, currentIndex, priorIndex)
//   {
//       // Used to skip the "Warning" step if the user is old enough.
//       //nbparent
//
//       if (currentIndex === 1 && $("#nbparent").val()==0)
//       {
//         Swal.fire({
//                 type: 'warning',
//                 title: '<?php echo L::WarningLib ?>',
//                 text: "Vous devez ajouter au moins un parent à cet élève",
//
//                 })
//           return false;
//       }
//
//       // if (currentIndex === 2 && Number($("#age-2").val()) >= 18)
//       // {
//       //     form.steps("next");
//       // }
//       // // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
//       // if (currentIndex === 2 && priorIndex === 3)
//       // {
//       //     form.steps("previous");
//       // }
//   },
//   onFinishing: function (event, currentIndex)
//   {
//       form.validate().settings.ignore = ":disabled";
//       return form.valid();
//   },
//   onFinished: function (event, currentIndex)
//   {
//       //alert("Submitted!");
//       form.submit();
//   }
// });


    //  form.steps({
    //      headerTag: "h3",
    //      bodyTag: "fieldset",
    //      transitionEffect: "slideLeft",
    //      onStepChanging: function (event, currentIndex, newIndex)
    //       {
    //                   // Allways allow previous action even if the current form is not valid!
    //                   // var classeselected=$("#classeEtab").val();
    //                   // // alert(classeselected);
    //                   //
    //                   // if(classeselected=="")
    //                   // {
    //                   //   $("#classeEtab").rules( "add", {
    //                   //       required: true,
    //                   //       messages: {
    //                   //       required: "Merci de selectionner au moins une classe"
    //                   // }
    //                   //     });
    //                   //   // return false;
    //                   // }
    //            if (currentIndex > newIndex)
    //            {
    //                return true;
    //            }
    //
    //            // Forbid next action on "Warning" step if the user is to young
    //            // if (newIndex === 2 && Number($("#nbselect").val()) < 0)
    //            // {
    //            //     return false;
    //            // }
    //            // Needed in some cases if the user went back (clean up)
    //            if (currentIndex < newIndex)
    //            {
    //                // To remove error styles
    //                form.find(".body:eq(" + newIndex + ") label.error").remove();
    //                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
    //            }
    //            form.validate().settings.ignore = ":disabled,:hidden";
    //            return form.valid();
    //
    //
    //       },
    // onStepChanged: function (event, currentIndex, priorIndex)
    // {
    //     // Used to skip the "Warning" step if the user is old enough.
    //     // if (currentIndex === 2 && Number($("#nbselect").val()) >0)
    //     // {
    //     //     form.steps("next");
    //     // }
    //
    // },
    // onFinishing: function (event, currentIndex)
    // {
    //     form.validate().settings.ignore = ":disabled";
    //     return form.valid();
    // },
    // onFinished: function (event, currentIndex)
    // {
    //   form.submit();
    //
    // }
    //
    //
    //  });



   //  form.steps({
   //      headerTag: "h3",
   //      bodyTag: "fieldset",
   //      transitionEffect: "slideLeft",
   //      onStepChanging: function (event, currentIndex, newIndex)
   //       {
   //                   // Allways allow previous action even if the current form is not valid!
   //                   // var classeselected=$("#classeEtab").val();
   //                   // // alert(classeselected);
   //                   //
   //                   // if(classeselected=="")
   //                   // {
   //                   //   $("#classeEtab").rules( "add", {
   //                   //       required: true,
   //                   //       messages: {
   //                   //       required: "Merci de selectionner au moins une classe"
   //                   // }
   //                   //     });
   //                   //   // return false;
   //                   // }
   //            if (currentIndex > newIndex)
   //            {
   //                return true;
   //            }
   //
   //            // Forbid next action on "Warning" step if the user is to young
   //            // if (newIndex === 2 && Number($("#nbselect").val()) < 0)
   //            // {
   //            //     return false;
   //            // }
   //            // Needed in some cases if the user went back (clean up)
   //            if (currentIndex < newIndex)
   //            {
   //                // To remove error styles
   //                form.find(".body:eq(" + newIndex + ") label.error").remove();
   //                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
   //            }
   //            form.validate().settings.ignore = ":disabled,:hidden";
   //            return form.valid();
   //
   //
   //       },
   // onStepChanged: function (event, currentIndex, priorIndex)
   // {
   //     // Used to skip the "Warning" step if the user is old enough.
   //     // if (currentIndex === 2 && Number($("#nbselect").val()) >0)
   //     // {
   //     //     form.steps("next");
   //     // }
   //
   // },
   // onFinishing: function (event, currentIndex)
   // {
   //     form.validate().settings.ignore = ":disabled";
   //     return form.valid();
   // },
   // onFinished: function (event, currentIndex)
   // {
   //   form.submit();
   //
   // }
   //
   //
   //  });

    $("#parenta").select2();
    // $("#parentaold").select2();
    $("#sexe").select2();
    $("#classeEtab").select2();
    $("#cantineEtab").select2();
    $("#matriselect").select2();


    $("#deseasesinfant").select2({
      tags: true,
    tokenSeparators: [',', ' ']
    });
    $("#allergiestudents").select2({
      tags: true,
    tokenSeparators: [',', ' ']
    });

    $('#parentNew').click(function(){
          AddparentsRow();

        });

        $('#MedicalNew').click(function(){
              AddMedicalsRow();

            });

$("#matriculerowReinscript").hide();
$("#RowbtnDoublant").hide();


  });

  function afficherInfos(id)
  {
    $("#RowInfosOld").hide();

    selectparentsinfosOne(id);
  }



  function recalculallergiesnb()
  {
    var concatnballergies=$("#concatnballergies").val();

    var tab=concatnballergies.split("@");

    var nbtab=tab.length;

    var nbtabnew=parseInt(nbtab)-1;

    $("#nbconcatnballergie").val(nbtabnew);
  }

  function recalculinfantilesnb()
  {
    var concatnbinfantilesD=$("#concatnbinfantilesD").val();

    var tab=concatnbinfantilesD.split("@");

    var nbtab=tab.length;

    var nbtabnew=parseInt(nbtab)-1;

    $("#nbconcatnbinfantilesD").val(nbtabnew);
  }

  function addAllergies()
  {
    var nb=$("#nballergies").val();
    var nouveau= parseInt(nb)+1;
    $("#nballergies").val(nouveau);
    var concatallergies=$("#concatnballergies").val();
    $("#concatnballergies").val(concatallergies+nouveau+"@");

    recalculallergiesnb();

    $('#fields_allergies').append('<tr id="rowAllerg'+nouveau+'"><td><input type="text" name="Allergies_'+nouveau+'" id="Allergies_'+nouveau+'" placeholder="<?php echo L::EnterAllergiesingle ?>" class="form-control" /></td><td><button type="button" id="deleteAllerg'+nouveau+'" id="deleteAllerg'+nouveau+'"  onclick="deletedAllergies('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');

    for(var i=1;i<=nouveau;i++)
    {
      $("#Allergies_"+i).rules( "add", {
          required: true,
          messages: {
          required: "<?php echo L::PleaseEnterAllergiesingle ?>"
}
        });
      }
  }

  function deletedAllergies(id)
  {
    var concatnballergies=$("#concatnballergies").val();

    $("#concatnballergies").val($("#concatnballergies").val().replace(id+"@", ""));

     $('#rowAllerg'+id+'').remove();

    recalculallergiesnb();
  }

  function deleteInfantiles(id)
  {
    var concatnbinfantilesD=$("#concatnbinfantilesD").val();

    $("#concatnbinfantilesD").val($("#concatnbinfantilesD").val().replace(id+"@", ""));

     $('#rowInfantiles'+id+'').remove();

    recalculinfantilesnb();
  }

  function addInfantiles()
  {
    var nb=$("#nbinfantilesD").val();
    var nouveau= parseInt(nb)+1;
    $("#nbinfantilesD").val(nouveau);
    var concatnbinfantilesD=$("#concatnbinfantilesD").val();
    $("#concatnbinfantilesD").val(concatnbinfantilesD+nouveau+"@");
    recalculinfantilesnb();

    $('#fields_infantiles').append('<tr id="rowInfantiles'+nouveau+'"><td><input type="text" name="Infantiles_'+nouveau+'" id="Infantiles_'+nouveau+'" placeholder="<?php echo L::EnterInfantilesMaladies ?>" class="form-control" /></td><td><button type="button" id="deleteInfantiles'+nouveau+'" id="deleteInfantiles'+nouveau+'"  onclick="deleteInfantiles('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');

    for(var i=1;i<=nouveau;i++)
    {
      $("#Infantiles_"+i).rules( "add", {
          required: true,
          messages: {
          required: "<?php echo L::PleaseEnterInfantilesMaladies ?>"
}
        });
    }
  }

  function doublantN()
  {
    $("#doublant").val(0);

    $("#btn1").removeClass('notActive').addClass('active');
    $("#btn2").removeClass('active').addClass('notActive');
  }

  function doublant()
  {
  $("#doublant").val(1);
  $("#btn2").removeClass('notActive').addClass('active');
  $("#btn1").removeClass('active').addClass('notActive');
  }

  function checkmatri()
  {
    var matri=$("#matri").val();
    var codeEtab="<?php echo $codeEtabLocal ?>";
    var session="<?php echo $libellesessionencours ?>";
    var etape=11;
    $.ajax({
      url: '../ajax/admission.php',
      type: 'POST',
      async:true,
      data: 'matricule=' +matri+ '&etape=' + etape+'&session='+session+'&codeEtab='+codeEtab,
      dataType: 'text',
      success: function (content, statut) {

        // alert(content);

        if(content==1)
        {
          //nous allons rechercher les informations de l'élève

          $("#newStudent").val(2);

          var etape=12;
            $.ajax({
              url: '../ajax/admission.php',
              type: 'POST',
              async:true,
              data: 'matricule=' +matri+ '&etape=' + etape+'&session='+session+'&codeEtab='+codeEtab,
              dataType: 'text',
              success: function (content, statut) {

                Swal.fire({
  title: '<?php echo L::WarningLib ?>',
  text: content,
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::ImportDatas ?>',
  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
  if (result.value) {
// alert("oui");

var etape=13;
$.ajax({
  url: '../ajax/admission.php',
  type: 'POST',
  async:true,
  data: 'matricule=' +matri+ '&etape=' + etape+'&session='+session+'&codeEtab='+codeEtab,
  dataType: 'text',
  success: function (response, statut) {

    var nom=response.split("*")[0];
    var prenom=response.split("*")[1];
    var datenais=response.split("*")[2];
    var lieunais=response.split("*")[3];
    var sexeselect=response.split("*")[4];


    // alert(nom);

    $("#nomad").val(nom);
    $("#prenomad").val(prenom);
    $("#datenaisad").val(datenais);
    $("#lieunais").val(lieunais);
    $('#sexe option[value="'+sexeselect+'"]').attr('selected',true);

    //nous allons chercher les informations sur les parents de cet eleves

    var etape=14;

    $.ajax({
      url: '../ajax/admission.php',
      type: 'POST',
      async:true,
      data: 'matricule=' +matri+ '&etape=' + etape+'&session='+session+'&codeEtab='+codeEtab,
      dataType: 'text',
      success: function (response, statut) {



        var tab=response.split('*');

        // alert(tab.length);

        for(var i=0;i<tab.length;i++)
        {
          // alert(tab[i]);
          var parentstudentid=tab[i];

          var etape=15;

          $.ajax({
            url: '../ajax/admission.php',
            type: 'POST',
            async:true,
            data: 'parentstudentid='+parentstudentid+ '&etape=' + etape,
            dataType: 'text',
            success: function (response, statut) {



              var parentid=response.split("*")[11];
              var nom=response.split("*")[0];
              var prenom=response.split("*")[1];
              var phone=response.split("*")[2];
              var sexe=response.split("*")[6];

              Onloadoldparent(parentid,nom,prenom,phone,sexe);

            }
          });

        }

      }
    });



  }
});


  }else {
    //document.location.href="index.php";
    $("#matri").val("");
    $("#matri").focus();
  }
})

              }
            });
        }else {
          $("#newStudent").val(1);
        }

      }
    });
    // alert(matri);
    //nous allons verifier si cet matricule existe deja dans le système pour cet établissement
  }

  function Onloadoldparent(parentid,nom,prenom,phone,sexe)
  {
    var concatoldparents=$("#concatoldparents").val();

    var nb=$("#nboldparent").val();
    var nouveau= parseInt(nb)+1;
    // var concatparents=$("#concatparents").val();
    // $("#concatparents").val(concatparents+nouveau+"@");
    $("#concatoldparents").val(concatoldparents+parentid+"@");

    //recalcule nbconcatparent
    recalculoldparentnb();

    var ligne="<div class=\"form-group row\" id=\"ligneEnteteold"+parentid+"\">";
    ligne=ligne+"<label class=\"control-label col-md-8\" style=\"color:#e40d22\"><?php echo L::OldsParentsInfos ?> "+nouveau+" <span class=\"required\">  </span></label>";
    ligne=ligne+"</div>";
    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowNameold"+parentid+"\">";
    ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::Name ?> <span class=\"required\"> * </span></label>";
    ligne=ligne+"<div class=\"col-md-6\">";
    ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"nomparentold"+parentid+"\"  id=\"nomparentold"+parentid+"\" value="+nom+" readonly >";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";
    //prenoms
    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPrenameold"+parentid+"\">";
    ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::PreName ?> <span class=\"required\"> * </span></label>";
    ligne=ligne+"<div class=\"col-md-6\">";
    ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"prenomparentold"+parentid+"\"  id=\"prenomparentold"+parentid+"\" value="+prenom+" readonly>";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";
    //sexe
    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowSexeold"+parentid+"\">";
    ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::Gender ?> <span class=\"required\"> * </span></label>";
    ligne=ligne+"<div class=\"col-md-6\">";
    ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"sexeparentold"+parentid+"\"  id=\"sexeparentold"+parentid+"\" value="+sexe+" readonly>";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";

    //telephone
    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPhoneold"+parentid+"\">";
    ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::PhonestudentTab ?> <span class=\"required\"> * </span></label>";
    ligne=ligne+"<div class=\"col-md-6\">";
    ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"phoneparentold"+parentid+"\"  id=\"phoneparentold"+parentid+"\" value="+phone+" readonly>";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";
    //bouton de suppression
    ligne=ligne+"<div class=\"pull-right\" id=\"ligneRowold"+parentid+"\">";
    ligne=ligne+"<button type=\"button\" id=\"delete"+parentid+"\" name=\"delete"+parentid+"\"  onclick=\"deletedoldrow("+parentid+")\" class=\"btn btn-danger pull-right\"><i class=\"fa fa-trash\"></i></button>";
    ligne=ligne+"</div></br></br>";

    $('#dynamic_field').append(ligne);
  }

  function Home()
  {
    $("#localisation").val(1);
    $("#btn31").removeClass('notActive').addClass('active');
    $("#btn32").removeClass('active').addClass('notActive');
  }

  function HomeN()
  {
    $("#localisation").val(0);
    $("#btn32").removeClass('notActive').addClass('active');
    $("#btn31").removeClass('active').addClass('notActive');
  }

   function manuel()
   {
     $("#matriculestate").val(1);
     $("#matri").val("");
     $("#matri").attr("disabled",false);
     $("#etape").val(1);
     $("#matriculerow").show();

     $("#btn11").removeClass('notActive').addClass('active');
     $("#btn12").removeClass('active').addClass('notActive');




   }

   function inscription()
   {
     //val ==1
     $("#inscript").val(1);
     $("#newStudent").val(1);
     $("#RowbtnDoublant").hide();
     $("#matriculerowReinscript").hide();

      $("#RowbtnMatricule").show();
      $("#matriculerowInscript").show();
      $("#lastschoolRow").show();

      $("#btn21").removeClass('notActive').addClass('active');
      $("#btn22").removeClass('active').addClass('notActive');

   }

   function reinscription()
   {
     //val ==2
     $("#inscript").val(2);
     $("#newStudent").val(2);
     $("#RowbtnDoublant").show();
     $("#matriculerowReinscript").show();

     $("#RowbtnMatricule").hide();
     $("#matriculerowInscript").hide();
     $("#lastschoolRow").hide();

     $("#btn22").removeClass('notActive').addClass('active');
     $("#btn21").removeClass('active').addClass('notActive');
   }

   function automatique()
   {

     $("#matriculestate").val(2);
     $("#matri").val("");
     $("#matri").attr("disabled",true);
     $("#etape").val(2);
     $("#matriculerow").hide();
     $("#btn12").removeClass('notActive').addClass('active');
     $("#btn11").removeClass('active').addClass('notActive');
   }

   $('#radioBtn a').on('click', function(){
      var sel = $(this).data('title');
      var tog = $(this).data('toggle');
      $('#'+tog).prop('value', sel);

      $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
      $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
  })

  $('#radioBtn1 a').on('click', function(){
     var sel = $(this).data('title');
     var tog = $(this).data('toggle');
     $('#'+tog).prop('value', sel);

     $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
     $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
  })

  $('#radioBtn2 a').on('click', function(){
     var sel = $(this).data('title');
     var tog = $(this).data('toggle');
     $('#'+tog).prop('value', sel);

     $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
     $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
  })

  $('#radioBtn3 a').on('click', function(){
     var sel = $(this).data('title');
     var tog = $(this).data('toggle');
     $('#'+tog).prop('value', sel);

     $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
     $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
  })




      $('#exampleModal').on('hidden.bs.modal', function (e) {
          // window.location.reload(true);
          $("#parentaold").val("");
          $("#oldparentname").val("");
          $("#oldparentprename").val("");
          $("#oldparentphone").val("");

           // $('#parentaold option[value=" "]').prop('selected', true);
           $('#parentaold>option[value=""]').attr('selected', true);

          $("#addoldbtn").prop("disabled",true);
      });

      function oldbtnaction()
      {
        var parentid= $("#parentaold").val();
        var nom=$("#oldparentname").val();
        var prenom=$("#oldparentprename").val();
        var phone=$("#oldparentphone").val();
        var sexe=$("#oldparentsexe").val();

        var concatoldparents=$("#concatoldparents").val();

        var nb=$("#nboldparent").val();
        var nouveau= parseInt(nb)+1;
        // var concatparents=$("#concatparents").val();
        // $("#concatparents").val(concatparents+nouveau+"@");
        $("#concatoldparents").val(concatoldparents+parentid+"@");

        //recalcule nbconcatparent
        recalculoldparentnb();

        var ligne="<div class=\"form-group row\" id=\"ligneEnteteold"+parentid+"\">";
        ligne=ligne+"<label class=\"control-label col-md-8\" style=\"color:#e40d22\"><?php echo L::OldsParentsInfos ?> "+nouveau+" <span class=\"required\">  </span></label>";
        ligne=ligne+"</div>";
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowNameold"+parentid+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::Name ?> <span class=\"required\"> * </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"nomparentold"+parentid+"\"  id=\"nomparentold"+parentid+"\" value="+nom+" readonly >";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //prenoms
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPrenameold"+parentid+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::PreName ?> <span class=\"required\"> * </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"prenomparentold"+parentid+"\"  id=\"prenomparentold"+parentid+"\" value="+prenom+" readonly>";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //sexe
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowSexeold"+parentid+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::Gender ?> <span class=\"required\"> * </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"sexeparentold"+parentid+"\"  id=\"sexeparentold"+parentid+"\" value="+sexe+" readonly>";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";



        //telephone
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPhoneold"+parentid+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::PhonestudentTab ?> <span class=\"required\"> * </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"phoneparentold"+parentid+"\"  id=\"phoneparentold"+parentid+"\" value="+phone+" readonly>";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //bouton de suppression
        ligne=ligne+"<div class=\"pull-right\" id=\"ligneRowold"+parentid+"\">";
        ligne=ligne+"<button type=\"button\" id=\"delete"+parentid+"\" name=\"delete"+parentid+"\"  onclick=\"deletedoldrow("+parentid+")\" class=\"btn btn-danger pull-right\"><i class=\"fa fa-trash\"></i></button>";
        ligne=ligne+"</div></br></br>";

        $('#dynamic_field').append(ligne);

        // $('#exampleModal').hide();
        $('#exampleModal').modal('hide');

      }

    function cantineoption()
    {
      var etape=1;
      var classe=$("#classeEtab").val();
      var session="<?php echo $libellesessionencours; ?>";
      var codeEtab="<?php echo $codeEtabLocal; ?>";



       $.ajax({
         url: '../ajax/cantine.php',
         type: 'POST',
         async:false,
         data: 'classeEtab=' +classe+ '&etape=' + etape+'&codeEtab='+codeEtab+'&session='+session,
         dataType: 'text',
         success: function (response, statut) {
           //nous allons voir si les frais sont obligatoires ou pas

           var etape=2;

          if(response==1)
          {
            //cantine obligaoires

            $.ajax({
              url: '../ajax/cantine.php',
              type: 'POST',
              async:false,
              data: 'classeEtab=' +classe+ '&etape=' + etape+'&codeEtab='+codeEtab+'&session='+session,
              dataType: 'text',
              success: function (response, statut) {
                $("#cantineEtab").html();
                $("#cantineEtab").html(response);


                $('#cantineEtab').rules( "add", {
                    required: true,
                    messages: {
                    required: "<?php echo L::PLeaseSelectCantinesFrais ?>"
             }
                  });

                $("#spancantine").addClass("required" );

                  //nous allons chercher le type de la classe
                  var etape=3;
                    $.ajax({
                      url: '../ajax/cantine.php',
                      type: 'POST',
                      async:false,
                      data: 'classeEtab=' +classe+ '&etape=' + etape+'&codeEtab='+codeEtab+'&session='+session,
                      dataType: 'text',
                      success: function (response, statut) {

                        if(response=="PREK"||response=="K1"||response=="K2"||response=="K3"||response=="GRADE1")
                        {
                          $("#lastschoolRow").hide();
                        }else {
                          $("#lastschoolRow").show();
                          //nous allons voir si nous sommes dans le cas d'un inscription ou réinscription

                          var inscript=$("#inscript").val();

                          // alert(inscript);

                          if(inscript==2)
                          {
                            var etabname=$("#etabname").val();
                            $("#lastschool").val(etabname);


                          }

                        }




                      }
                    });

              }
            });

          }else {
            //cantine non obligatoires

            $.ajax({
              url: '../ajax/cantine.php',
              type: 'POST',
              async:false,
              data: 'classeEtab=' +classe+ '&etape=' + etape+'&codeEtab='+codeEtab+'&session='+session,
              dataType: 'text',
              success: function (response, statut) {

                $("#cantineEtab").html();
                $("#cantineEtab").html(response);

                $('#cantineEtab').rules('remove');

                $("#spancantine").removeClass("required" );

              }
            });

          }

         }

     });

    }

    function selectparentsinfosOne(id)
    {



      // $("#parentaold").val(id);

      var etape=4;

      $.ajax({
        url: '../ajax/login.php',
        type: 'POST',
        async:false,
        data: 'parentid=' +id+ '&etape=' + etape,
        dataType: 'text',
        success: function (response, statut) {

          var nomparent=response.split("*")[0];
          var prenomparent=response.split("*")[1];
          var phoneparent=response.split("*")[2];
          var sexeparent=response.split("*")[6];
          var parentid=response.split("*")[11];

          // alert(parentid);

          $("#oldparentname").val(nomparent);
          $("#oldparentprename").val(prenomparent);
          $("#oldparentphone").val(phoneparent);
          $("#oldparentsexe").val(sexeparent);
          $("#parentaold").val(parentid);

          $("#RowInfosOld").show(1000);

          $("#addoldbtn").prop("disabled",false);


        }
      });
    }

      function selectparentsinfos()
      {
        var etape=4;
        $.ajax({
          url: '../ajax/login.php',
          type: 'POST',
          async:false,
          data: 'parentid=' + $("#parentaold").val()+ '&etape=' + etape,
          dataType: 'text',
          success: function (response, statut) {

            var nomparent=response.split("*")[0];
            var prenomparent=response.split("*")[1];
            var phoneparent=response.split("*")[2];
            var sexeparent=response.split("*")[6];

            $("#oldparentname").val(nomparent);
            $("#oldparentprename").val(prenomparent);
            $("#oldparentphone").val(phoneparent);
            $("#oldparentsexe").val(sexeparent);

            $("#addoldbtn").prop("disabled",false);


          }
        });
      }
     function activatebtn()
     {
       $("#submit1").prop("disabled",false);

     }

     function desactivatebtn()
     {
       $("#submit1").prop("disabled",true);
     }

     function deletedrow(id)
     {
       var concatparents=$("#concatparents").val();
       $("#concatparents").val($("#concatparents").val().replace(id+"@", ""));

       $("#ligneEntete"+id).remove();
       $("#ligneRowName"+id).remove();
       $("#ligneRowPrename"+id).remove();
       $("#ligneRowPhone"+id).remove();
       $("#ligneRowSexe"+id).remove();
       $("#ligneRow"+id).remove();

       recalculparentnb();

     }

     function deletedoldrow(parentid)
     {
       var concatparents=$("#concatparents").val();
       var concatoldparents=$("#concatoldparents").val();
       // $("#concatparents").val($("#concatparents").val().replace(id+"@", ""));
       $("#concatoldparents").val($("#concatoldparents").val().replace(parentid+"@", ""));

       $("#ligneEnteteold"+parentid).remove();
       $("#ligneRowNameold"+parentid).remove();
       $("#ligneRowPrenameold"+parentid).remove();
       $("#ligneRowPhoneold"+parentid).remove();
       $("#ligneRowSexeold"+parentid).remove();
       $("#ligneRowold"+parentid).remove();

       recalculparentnb();
     }

      function recalculparentnb()
      {
        //calcul du nombre de nouveau parent
        var concatparents=$("#concatparents").val();
        var tab=concatparents.split("@");
        var nbtab=tab.length;
        var nbtabnew=parseInt(nbtab)-1;
        $("#nbparent").val(nbtabnew);
        //calcul du nombre ancien parent
        var concatoldparents=$("#concatoldparents").val();
        var tabold=concatoldparents.split("@");
        var nbtabold=tabold.length;
        var nbtaboldnew=parseInt(nbtabold)-1;
        $("#nboldparent").val(nbtaboldnew);

        var etape=16;

        $.ajax({
          url: '../ajax/admission.php',
          type: 'POST',
          async:true,
          data: 'concatnew=' +nbtabnew+ '&etape=' + etape+'&concatold='+nbtaboldnew,
          dataType: 'text',
          success: function (content, statut) {

            $("#nbtotalparent").val(content);

            if(content==0)
            {
              $("#submitBtn").attr("disabled",true);
            }else if(content>0){
              $("#submitBtn").attr("disabled",false);
            }

          }
        });

        // if((nbtabnew==0)&&(nbtaboldnew==0))
        // {
        //   //actualiser la page
        //
        //    // location.reload();
        //     // desactivatebtn();
        //
        //     $("#nbtotalparent").val(0);
        // }else {
        //   var total=(parseInt(nbtabold)-1)+parseInt(nbtab)-1;
        //   $("#nbtotalparent").val(total);
        // }
      }

      function recalculoldparentnb()
      {
        //calcul du nombre de nouveau parent
        var concatparents=$("#concatparents").val();
        var tab=concatparents.split("@");
        var nbtab=tab.length;
        var nbtabnew=parseInt(nbtab)-1;
        $("#nbparent").val(nbtabnew);
        //calcul du nombre ancien parent
        var concatoldparents=$("#concatoldparents").val();
        var tabold=concatoldparents.split("@");
        var nbtabold=tabold.length;
        var nbtaboldnew=parseInt(nbtabold)-1;
        $("#nboldparent").val(nbtaboldnew);

        var etape=16;

        $.ajax({
          url: '../ajax/admission.php',
          type: 'POST',
          async:true,
          data: 'concatnew=' +nbtabnew+ '&etape=' + etape+'&concatold='+nbtaboldnew,
          dataType: 'text',
          success: function (content, statut) {

            $("#nbtotalparent").val(content);

            if(content==0)
            {
              $("#submitBtn").attr("disabled",true);
            }else if(content>0){
              $("#submitBtn").attr("disabled",false);
            }

          }
        });

        // if((nbtabnew==0)&&(nbtaboldnew==0))
        // {
        //   //actualiser la page
        //
        //    // location.reload();
        //    desactivatebtn();
        // }else if((nbtabnew>0)||(nbtaboldnew>0)){
        //   activatebtn();
        // }else if((nbtabnew<0)||(nbtaboldnew<0))
        // {
        //   desactivatebtn();
        // }
      }

      function telchecked(id)
      {
        // alert(id);
        // var telephone=$("#phoneparent"+id).val();
        var telephone=$("#mobileparent"+id).val();

        var etape=2;
        $.ajax({
          url: '../ajax/login.php',
          type: 'POST',
          async:true,
           data: 'telephone='+telephone+'&etape='+etape,
          dataType: 'text',
          success: function (response, statut) {

            if(response==0)
            {

            }else if(response>0)
            {
              Swal.fire({
                      type: 'warning',
                      title: '<?php echo L::WarningLib ?>',
                      text: "<?php echo L::ThisParentAllreadyExist ?>",

                      })

                     deletedrow(id);

                     var etape=5;
                     $.ajax({
                       url: '../ajax/login.php',
                       type: 'POST',
                       async:true,
                        data: 'telephone='+telephone+'&etape='+etape,
                       dataType: 'text',
                       success: function (response, statut) {
                         var parentid=response.split("*")[11];
                         var nom=response.split("*")[0];
                         var prenom=response.split("*")[1];
                         var phone=response.split("*")[2];
                         var sexe=response.split("*")[6];



                         var concatoldparents=$("#concatoldparents").val();

                         var nb=$("#nboldparent").val();
                         var nouveau= parseInt(nb)+1;
                         // var concatparents=$("#concatparents").val();
                         // $("#concatparents").val(concatparents+nouveau+"@");
                         $("#concatoldparents").val(concatoldparents+parentid+"@");

                         //recalcule nbconcatparent
                         recalculoldparentnb();

                         var ligne="<div class=\"form-group row\" id=\"ligneEnteteold"+parentid+"\">";
                         ligne=ligne+"<label class=\"control-label col-md-8\" style=\"color:#e40d22\"><?php echo L::OldsParentsInfos ?> "+nouveau+" <span class=\"required\">  </span></label>";
                         ligne=ligne+"</div>";
                         ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowNameold"+parentid+"\">";
                         ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::Name ?> <span class=\"required\"> * </span></label>";
                         ligne=ligne+"<div class=\"col-md-6\">";
                         ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"nomparentold"+parentid+"\"  id=\"nomparentold"+parentid+"\" value="+nom+" readonly >";
                         ligne=ligne+"</div>";
                         ligne=ligne+"</div>";
                         //prenoms
                         ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPrenameold"+parentid+"\">";
                         ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::PreName ?> <span class=\"required\"> * </span></label>";
                         ligne=ligne+"<div class=\"col-md-6\">";
                         ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"prenomparentold"+parentid+"\"  id=\"prenomparentold"+parentid+"\" value="+prenom+" readonly>";
                         ligne=ligne+"</div>";
                         ligne=ligne+"</div>";
                         //sexe
                         ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowSexeold"+parentid+"\">";
                         ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::Gender ?> <span class=\"required\"> * </span></label>";
                         ligne=ligne+"<div class=\"col-md-6\">";
                         ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"sexeparentold"+parentid+"\"  id=\"sexeparentold"+parentid+"\" value="+sexe+" readonly>";
                         ligne=ligne+"</div>";
                         ligne=ligne+"</div>";

                         //telephone
                         ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPhoneold"+parentid+"\">";
                         ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::PhonestudentTab ?><span class=\"required\"> * </span></label>";
                         ligne=ligne+"<div class=\"col-md-6\">";
                         ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"phoneparentold"+parentid+"\"  id=\"phoneparentold"+parentid+"\" value="+phone+" readonly>";
                         ligne=ligne+"</div>";
                         ligne=ligne+"</div>";
                         //bouton de suppression
                         ligne=ligne+"<div class=\"pull-right\" id=\"ligneRowold"+parentid+"\">";
                         ligne=ligne+"<button type=\"button\" id=\"delete"+parentid+"\" name=\"delete"+parentid+"\"  onclick=\"deletedoldrow("+parentid+")\" class=\"btn btn-danger pull-right\"><i class=\"fa fa-trash\"></i></button>";
                         ligne=ligne+"</div></br></br>";

                         $('#dynamic_field').append(ligne);

                       }
                     });

            }

          }});
      }

      function Addlignestandard()
      {
        var ligne="<tr id=\"lignevide\">";
        ligne=ligne+"<td colspan=\"4\" style=\"text-align:center\"><?php echo L::NoInformations ?></td>";
        ligne=ligne+"</tr>";

        $('#tablebody').append(ligne);

      }

      function deletedlineAnte(nouveau)
      {
        var concatantecedent=$("#concatantecedent").val();
        $("#concatantecedent").val($("#concatantecedent").val().replace(nouveau+"@", ""));
	      $("#ligneAnte"+nouveau).remove();

        var tabconcatligne=$("#concatantecedent").val().split("@");
        var nb=(tabconcatligne.length)-1;
        if(nb==0)
        {
          Addlignestandard();
        }
      }



      function getandecedentRow(nouveau)
      {
        var session="<?php echo $libellesessionencours; ?>";
        var codeEtab="<?php echo $codeEtabLocal; ?>";
        var etape=17;

         $.ajax({
           url: '../ajax/admission.php',
           type: 'POST',
           async:false,
           data: 'session=' +session+ '&etape=' + etape+'&codeEtab='+codeEtab,
           dataType: 'text',
           success: function (response, statut) {
             $("#libelleantecedet"+nouveau).html("");
             $("#libelleantecedet"+nouveau).html(response);
             // $("#heureLibelles"+nouveau).html(content);

           }
         });


      }

      function AddRow()
      {
        //var nb=$("#nbantecedent").val();
        var nb=$("#nbconcatantecedent").val();
      	var nouveau= parseInt(nb)+1;
      	var concatantecedent=$("#concatantecedent").val();
      	$("#concatantecedent").val(concatantecedent+nouveau+"@");
        // recalculantecedentnb();
        getandecedentRow(nouveau);

        var ligne="<tr  id=\"ligneAnte"+nouveau+"\" style=\"cursor:pointer\" ondblclick=\"deletedlineAnte("+nouveau+")\" >";
        ligne=ligne+"<td>";
        // ligne=ligne+"<select class=\"form-control \" id=\"libelleantecedet"+nouveau+"\" name=\"libelleantecedet"+nouveau+"\" style=\"width:100%;text-align:center\">";
        // ligne=ligne+"<option value=\"\">Selectionner un Antécédent Médical</option>";
        // ligne=ligne+"<option value=\"1\">Asthme / Asthma</option>";
        // ligne=ligne+"<option value=\"2\">Drépanocytose / Sickle Cell Disease</option>";
        // ligne=ligne+"<option value=\"3\">Cardiopathie / Heart Disease</option>";
        // ligne=ligne+"<option value=\"4\">Epilepsie / Epilepsy</option>";
        // ligne=ligne+"<option value=\"5\">Hypertension / High Blood Pressure</option>";
        // ligne=ligne+"<option value=\"6\">Rhumatisme / Rheumatism</option>";
        // ligne=ligne+"<option value=\"7\">Diabète / Diabetes</option>";
        // ligne=ligne+"</select>";
          ligne=ligne+"<input type=\"text\" class=\"form-control \" id=\"libelleantecedet"+nouveau+"\" name=\"libelleantecedet"+nouveau+"\" style=\"width:100%;text-align:center\" >";
        ligne=ligne+"</td>";

        ligne=ligne+"<td>";
        ligne=ligne+"<select class=\"form-control \" id=\"childantecedet"+nouveau+"\" name=\"childantecedet"+nouveau+"\" style=\"width:100%;text-align:center\">";
        ligne=ligne+"<option value=\"1\" selected><?php echo L::True ?></option>";
        ligne=ligne+"<option value=\"2\"><?php echo L::False ?></option>";
        ligne=ligne+"</select>";
      	ligne=ligne+"</td>";

        ligne=ligne+"<td>";
        ligne=ligne+"<select class=\"form-control \" id=\"fatherantecedet"+nouveau+"\" name=\"fatherantecedet"+nouveau+"\" style=\"width:100%;text-align:center\">";
        ligne=ligne+"<option value=\"1\" selected><?php echo L::True ?></option>";
        ligne=ligne+"<option value=\"2\"><?php echo L::False ?></option>";
        ligne=ligne+"</select>";
        ligne=ligne+"</td>";

        ligne=ligne+"<td>";
        ligne=ligne+"<select class=\"form-control \" id=\"motherantecedet"+nouveau+"\" name=\"motherantecedet"+nouveau+"\" style=\"width:100%;text-align:center\">";
        ligne=ligne+"<option value=\"1\" selected><?php echo L::True ?></option>";
        ligne=ligne+"<option value=\"2\"><?php echo L::False ?></option>";
        ligne=ligne+"</select>";
        ligne=ligne+"</td>";
        ligne=ligne+"</tr>";

        $("#nbconcatantecedent").val(nouveau);

        $('#tablebody').append(ligne);

      }

      function recalculantecedentnb()
      {

      }

      function AddMedicalsRow()
      {
        //var nb=$("#nbantecedent").val();
        var nb=$("#nbconcatantecedent").val();

        if(nb==0)
          {
          	$("#lignevide").remove();

          	AddRow();

          }else {
          	AddRow();
          }
      }




      function getInfantiles()
      {
        var session="<?php echo $libellesessionencours; ?>";
        var codeEtab="<?php echo $codeEtabLocal; ?>";
        var etape=18;

         $.ajax({
           url: '../ajax/admission.php',
           type: 'POST',
           async:false,
           data: 'session=' +session+ '&etape=' + etape+'&codeEtab='+codeEtab,
           dataType: 'text',
           success: function (response, statut) {
             $("#deseasesinfant").html("");
             $("#deseasesinfant").html(response);
             // $("#heureLibelles"+nouveau).html(content);

           }
         });
      }

      function getAllergies()
      {
        var session="<?php echo $libellesessionencours; ?>";
        var codeEtab="<?php echo $codeEtabLocal; ?>";
        var etape=19;

         $.ajax({
           url: '../ajax/admission.php',
           type: 'POST',
           async:false,
           data: 'session=' +session+ '&etape=' + etape+'&codeEtab='+codeEtab,
           dataType: 'text',
           success: function (response, statut) {
             $("#allergiestudents").html("");
             $("#allergiestudents").html(response);
             // $("#heureLibelles"+nouveau).html(content);

           }
         });
      }

      function AddparentsRow()
      {
        var nb=$("#nbparent").val();
        var nouveau= parseInt(nb)+1;
        var concatparents=$("#concatparents").val();
        $("#concatparents").val(concatparents+nouveau+"@");

        //recalcule nbconcatparent
        recalculparentnb();

        var ligne="<div class=\"form-group row\" id=\"ligneEntete"+nouveau+"\">";
        // ligne=ligne+"<label class=\"control-label col-md-8\" style=\"color:#e40d22\">INFORMATIONS PARENT "+nouveau+" <span class=\"required\">  </span></label>";
        ligne=ligne+"<span class=\"label label-md label-info\"  style=\"text-align:center;margin-left:400px;font-family: \"Trebuchet MS\", Verdana, sans-serif;font-style: italic;font-variant-ligatures: no-common-ligatures;font-size:large;\"><?php echo L::GeneralInfosParentTabCaps ?> " +nouveau+"</span>";
        ligne=ligne+"</div>";
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowMobilephone"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::TelMobile ?> <span class=\"required\"> * </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"mobileparent"+nouveau+"\"  id=\"mobileparent"+nouveau+"\" onchange=\"telchecked("+nouveau+")\"  placeholder=\"<?php echo L::RenseignerParentMobile ?>\">";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //telephone mobile
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowName"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::Name ?> <span class=\"required\"> * </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"nomparent"+nouveau+"\"  id=\"nomparent"+nouveau+"\" placeholder=\"<?php echo L::EnterrenseignerNameParent ?>\">";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //prenoms
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPrename"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::PreName ?> <span class=\"required\"> * </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"prenomparent"+nouveau+"\"  id=\"prenomparent"+nouveau+"\" placeholder=\"<?php echo L::EnterrenseignerPreNameParent ?>\">";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //sexe
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowSexe"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::Gender ?><span class=\"required\"> * </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<select class=\"form-control \" id=\"sexeparent"+nouveau+"\" name=\"sexeparent"+nouveau+"\" style=\"width:100%\">";
        ligne=ligne+"<option value=\"\"><?php echo L::SelectGender ?></option>";
        ligne=ligne+"<option value=\"M\"><?php echo L::SexeM ?></option>";
        ligne=ligne+"<option value=\"F\"><?php echo L::SexeF ?></option>";
        ligne=ligne+"</select>";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //telephone
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowMetier"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::EnterProfessionsParentTab ?><span class=\"required\">  </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"metierparent"+nouveau+"\"  id=\"metierparent"+nouveau+"\" placeholder=\"<?php echo L::renseignerProfessionParent ?>\" >";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //profession
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowMetier"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::EmailstudentTab ?><span class=\"required\">  </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"email\" class=\"form-control \" name=\"emailparent"+nouveau+"\"  id=\"emailrparent"+nouveau+"\" placeholder=\"<?php echo L::renseignerEmailParent ?>\"  >";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //email
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowEmployeur"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::Employeur ?><span class=\"required\">  </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"employeurparent"+nouveau+"\"  id=\"employeurparent"+nouveau+"\"  placeholder=\"<?php echo L::renseignerEmployeurParent ?>\">";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //employeur
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPostaleadress"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::Adressepostales ?><span class=\"required\">  </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"postaleadressparent"+nouveau+"\"  id=\"postaleadressparent"+nouveau+"\"  placeholder=\"<?php echo L::renseignerPostalAdressParent ?>\" >";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //adresse postale

        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPhonework"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::TelBureau ?><span class=\"required\">  </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"phoneparent"+nouveau+"\"  id=\"phoneparent"+nouveau+"\"  placeholder=\"<?php echo L::renseignerTelephoneBureauParent ?>\" >";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //telephone domicile
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPhonehome"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\"><?php echo L::TelDomicile ?><span class=\"required\">  </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"phonehomeparent"+nouveau+"\"  id=\"phonehomeparent"+nouveau+"\"  placeholder=\"<?php echo L::renseignerTelephoneDomicileParent ?>\"  >";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //bouton de suppression
        ligne=ligne+"<div class=\"pull-right\" id=\"ligneRow"+nouveau+"\">";
        ligne=ligne+"<button type=\"button\" id=\"delete"+nouveau+"\" name=\"delete"+nouveau+"\"  onclick=\"deletedrow("+nouveau+")\" class=\"btn btn-danger pull-right\"><i class=\"fa fa-trash\"></i></button>";
        ligne=ligne+"</div></br></br>";

        $("#sexeparent"+nouveau).select2();

        $('#dynamic_field').append(ligne);

        $('#nomparent'+nouveau).rules( "add", {
            required: true,
            messages: {
            required: "<?php echo L::renseignerNameParent ?>"
     }
          });

       //    $('#metierparent'+nouveau).rules( "add", {
       //        required: true,
       //        messages: {
       //        required: "Merci de renseigner la profession  du parent"
       // }
       //      });
       //
       //      $('#employeurparent'+nouveau).rules( "add", {
       //          required: true,
       //          messages: {
       //          required: "Merci de renseigner l'employeur du parent"
       //   }
       //        });

          $('#sexeparent'+nouveau).rules( "add", {
              required: true,
              messages: {
              required: "<?php echo L::renseignerGenreParent ?>"
       }
            });

          $('#prenomparent'+nouveau).rules( "add", {
              required: true,
              messages: {
              required: "<?php echo L::renseignerPreNameParent ?>"
       }
            });

            $('#mobileparent'+nouveau).rules( "add", {
                required: true,
                digits:true,
                messages: {
                required: "<?php echo L::renseignerPhoneParent ?>",
                digits:"<?php echo L::DigitsOnly ?>"
         }
              });

      }

  </script>
  </body>

</html>
