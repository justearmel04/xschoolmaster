<?php
session_start();
require_once('../class/User.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$etabs=new Etab();
$etab=new Etab();
$parent=new ParentX();
$user=new User();
$classe=new Classe();
$matiere=new Matiere();
$emailUti=$_SESSION['user']['email'];
$userId=$_SESSION['user']['IdCompte'];
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$parents=$parent->getAllParent();

$codeEtabLocal=$etab->getcodeEtabByLocalId($userId);
$classes=$classe->getAllClassesbyschoolCode($codeEtabLocal);


$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabLocal);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabLocal);


if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabLocal);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];

  $matieres=$matiere->getAllSubjectOfClassesByEtab($codeEtabLocal,$libellesessionencours);

  $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabLocal,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabLocal,$libellesessionencours);
}



 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Application de communication écoles et parents</title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
	<link href="../assets2/css/material_style.css" rel="stylesheet">
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title">Ajouter Emploi du temps</div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#">Enseignement</a>&nbsp;<i class="fa fa-angle-right"></i>
                          <li><a class="parent-item" href="#">Emploi du temps</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active">Ajouter</li>
                      </ol>
                  </div>
              </div>

              <?php

                    if(isset($_SESSION['user']['addprogra']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
              <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
              <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['addprogra'] ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addprogra']);
                    }

                     ?>

              <?php

                    if(isset($_SESSION['user']['addroutineok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: 'Félicitations !',
  text: "<?php echo $_SESSION['user']['addroutineok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Ajouter une nouvelle routine',
  cancelButtonText: 'Annuler',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="index.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addroutineok']);
                    }

                     ?>

                     <?php
                     if($nbsessionOn>0)
                     {
                      ?>

              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <div class="card card-box">
                          <div class="card-head">
                              <header></header>

                          </div>

                          <div class="card-body" id="bar-parent">
                              <form  id="FormAddRoutine" class="form-horizontal" action="../controller/routined.php" method="post" >
                                  <div class="form-body">

                                    <div class="form-group row">
                                            <label class="control-label col-md-3">Session
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-5">
                                <input type="text" class="form-control input-height" name="sessioncourante" id="sessioncourante" value="<?php echo $libellesessionencours; ?>" readonly>
                                        </div>
                                      </div>

                                    <div class="form-group row">
                                            <label class="control-label col-md-3">Classe
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-5">
                                                <!--select class="form-control input-height" name="classe" id="classe" onchange="searchsubject()" style="width:100%"-->
                                                  <select class="form-control input-height" name="classe" id="classe" onchange="selectclasse()" style="width:100%">
                                                    <option value="">Selectionner une classe</option>
                                                    <?php
                                                    $i=1;
                                                      foreach ($classes as $value):
                                                      ?>
                                                      <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                      <?php
                                                                                       $i++;
                                                                                       endforeach;
                                                                                       ?>

                                                </select>
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                              <label class="control-label col-md-3">Jour
                                                  <span class="required"> * </span>
                                              </label>
                                              <div class="col-md-5">
                                                <select class="form-control input-height" name="day" id="day"  style="width:100%">
                                                  <option value="">Merci de selectionner un jour</option>
                                                  <option value="LUN">Lundi</option>
                                                  <option value="MAR">Mardi</option>
                                                  <option value="MER">Mercredi</option>
                                                  <option value="JEU">Jeudi</option>
                                                  <option value="VEN">Vendredi</option>
                                                  <option value="SAM">Samedi</option>
                                                  <option value="DIM">Dimanche</option>

                                                </select>
                                                <input type="hidden" id="etape" name ="etape" value="1" >
                                                <input type="hidden" name="seletionne" id="seletionne" value="">
                                                <input type="hidden" name="concatroutines" id="concatroutines" value="">
                                                <input type="hidden" name="nbconcatroutines" id="nbconcatroutines" value="">
                                                <input type="hidden" name="contentmatiere" id="contentmatiere" value="">
                                                <input type="hidden" name="nbroute" id="nbroute" value="0">
                                                <input type="hidden" id="Etab" name ="Etab" value="<?php echo $codeEtabLocal;?>" >
                                                <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                                <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabLocal; ?>">
                                              </div>
                                          </div>
                                          <div id="dynamic_field">

                                          </div>


                <div class="form-actions">
                                      <div class="row">
                                          <div class="offset-md-3 col-md-9">

                                              <button type="button" class="btn btn-primary" id="routineNew"> <i class="fa fa-plus"></i> Nouvelle Routine </button>
                                          </div>
                                        </div><br><br>
                                        <div class="row">
                                            <div class="offset-md-3 col-md-9">
                                                <button type="submit" id="submit1"  class="btn btn-info" disabled>Enregistrer et consigner un autre jour</button>
                                                <button type="submit"  id="submit2" class="btn btn-danger" disabled>Enregistrer et afficher l'emploi du temps</button>
                                            </div>
                                          </div>
                                     </div>
              </div>
                              </form>
                          </div>
                      </div>
                  </div>

              </div>
              <?php
            }else {
              ?>
              <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="card card-topline-green">
                       <div class="card-head">
                           <header>Informations</header>
                           <div class="tools">
                               <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
       <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
       <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                           </div>
                       </div>
                       <div class="card-body ">
                         <p>

                             Afin de pouvoir faire ajouter un emploi du temps nous vous invitons à mentionner la session en cours dans l'onglet SESSIONS

                         </p>
                       </div>
                   </div>
                            </div>

              </div>
              <?php
            }
               ?>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <!--script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script-->
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <!-- Material -->
	<script src="../assets2/plugins/material/material.min.js"></script>
	<script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>

 <script>

 function activatebtn()
 {
   $("#submit1").prop("disabled",false);

   $("#submit2").prop("disabled",false);
 }

 function deletedrow(id)
 {

   var concatroutines=$("#concatroutines").val();

   $("#concatroutines").val($("#concatroutines").val().replace(id+"@", ""));

   // $("#concatroutines").val(concatroutines+nouveau+"@");

   $("#ligneRowMat"+id).remove();
   $("#ligneRowHeurdeb"+id).remove();
   $("#ligneRowHeurfin"+id).remove();
   $("#ligneRow"+id).remove();

   recalculroutinenb();




    //$("#ligneRow"+id).remove();
 }

 function selectclasse()
 {

   var classe=$("#classe").val();
   var etape=10;






   //alert("bonjour");

   $.ajax({
     url: '../ajax/matiere.php',
             type: 'POST',
             async:false,
             data: 'classe=' + $("#classe").val()+ '&etape=' + etape+'&session='+$("#sessioncourante").val()+'&codeEtab='+$("#codeEtab").val(),
             dataType: 'text',
   success: function (content, statut) {

     //$("#classe").val(content);

     //var contentclasse=content;

  //    alert(classe);
    $("#seletionne").val(classe);
  //
   // $("#classe").val(content);

     //nous allons mettre les matieres de cette classe dans la variable contentmatiere


     var etape1=2;

      $.ajax({

        url: '../ajax/matiere.php',
        type: 'POST',
        async:false,
        data: 'classe=' + $("#seletionne").val()+'&code='+$("#codeEtab").val()+'&etape='+etape1,
        dataType: 'text',
        success: function (content1, statut)
        {

          // $("#contentmatiere").val(content1);
          //alert(content1);

         $("#contentmatiere").val(content1);


        }

     });

     // contentmatiere



   }


 });

}

function recalculroutinenb()
{

  var concatroutines=$("#concatroutines").val();

  var tab=concatroutines.split("@");

  var nbtab=tab.length;

  var nbtabnew=parseInt(nbtab)-1;

  $("#nbconcatroutines").val(nbtabnew);

  if(nbtabnew==0)
  {
    //actualiser la page

     location.reload();
  }else if(nbtabnew>0){
    activatebtn();
  }



}

 function AddroutinesRow()
 {
   //nous allons verifier si la classe est Selectionner  classeseleted

    var contentmatiere=$("#contentmatiere").val();

   // var classe=$("#classe").val();

   // var classeseleced=$("#selectedclasse").val();

   // if(classe=="")
   // {
   //
   // }else{
   //   if(classe==classeselected)
   //   {
   //
   //   }else {
   //
   //   }
   // }



   var nb=$("#nbroute").val();
   var nouveau= parseInt(nb)+1;

  var concatroutines=$("#concatroutines").val();

  $("#concatroutines").val(concatroutines+nouveau+"@");

  //recalcule nbconcatroutine
recalculroutinenb();

   //ligne matiere
    var ligne="<div class=\"form-group row\" id=\"ligneRowMat"+nouveau+"\">";
    ligne=ligne+"<label class=\"control-label col-md-3\">Matière <span class=\"required\"> * </span></label>";
    ligne=ligne+"<div class=\"col-md-5\">";
    ligne=ligne+"<select class=\"form-control input-height\" name=\"matiere"+nouveau+"\"  id=\"matiere"+nouveau+"\"  style=\"width:100%\">";
    ligne=ligne+contentmatiere;
    ligne=ligne+"</select>";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";

    //ligne heure debut

    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowHeurdeb"+nouveau+"\">";
    ligne=ligne+"<label class=\"control-label col-md-3\">Heure Debut<span class=\"required\"> * </span></label>";
    ligne=ligne+"<div class=\"col-md-5\">";
    ligne=ligne+"<input type=\"text\"  id=\"heuredeb"+nouveau+"\" name=\"heuredeb"+nouveau+"\" class=\"floating-label mdl-textfield__input\"  placeholder=\"Heure de début\">";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";

    //ligne heure de fin

    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowHeurfin"+nouveau+"\">";
    ligne=ligne+"<label class=\"control-label col-md-3\">Heure Fin<span class=\"required\"> * </span></label>";
    ligne=ligne+"<div class=\"col-md-5\">";
    ligne=ligne+"<input type=\"text\"  id=\"heurefin"+nouveau+"\" name=\"heurefin"+nouveau+"\" class=\"floating-label mdl-textfield__input\" placeholder=\"Heure de début\">";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";

    //ligne bouton supprimer
    // ligne=ligne+"<div class=\" row\">";
    ligne=ligne+"<div class=\"pull-right\" id=\"ligneRow"+nouveau+"\">";
    ligne=ligne+"<button type=\"button\" id=\"delete"+nouveau+"\" name=\"delete"+nouveau+"\"  onclick=\"deletedrow("+nouveau+")\" class=\"btn btn-danger pull-right\"><i class=\"fa fa-trash\"></i></button>";
    ligne=ligne+"</div></br></br>";
    // ligne=ligne+"</div>";



    $('#dynamic_field').append(ligne);




    // alert(contentmatiere);

     // $("#matiere"+nouveau).html(contentmatiere);

    $('#heurefin'+nouveau).bootstrapMaterialDatePicker
    ({
      date:false,
      shortTime: false,
      format: 'HH:mm',
      lang: 'fr',
     cancelText: 'Annuler',
     okText: 'OK',
     clearText: 'Effacer',
     nowText: 'Maintenant'

    });

    $('#heuredeb'+nouveau).bootstrapMaterialDatePicker
    ({
      date: false,
      shortTime: false,
      format: 'HH:mm',
      lang: 'fr',
     cancelText: 'Annuler',
     okText: 'OK',
     clearText: 'Effacer',
     nowText: 'Maintenant'

    });

    $("#matiere"+nouveau).select2();


    $("#nbroute").val(nouveau);

    $('#heurefin'+nouveau).rules( "add", {
        required: true,
        messages: {
        required: "Champ obligatoire"
}
      });

      $('#heuredeb'+nouveau).rules( "add", {
          required: true,
          messages: {
          required: "Champ obligatoire"
  }
        });

        $("#matiere"+nouveau).rules( "add", {
            required: true,
            messages: {
            required: "Champ obligatoire"
    }
          });

 }



 function verify()
 {

 }

 function searchsubject()
 {
   var codeEtab="<?php echo $codeEtabLocal;?>";
   var etape=2;

    $.ajax({

      url: '../ajax/matiere.php',
      type: 'POST',
      async:false,
      data: 'classe=' + $("#classe").val()+'&code='+codeEtab+'&etape='+etape,
      dataType: 'text',
      success: function (content, statut)
      {
        $("#matiere").html("");
        $("#matiere").html(content);
      }

    });
 }
 function difheure(heuredeb,heurefin)
	{
	  var hd=heuredeb.split(":");
	  var hf=heurefin.split(":");

    // var difH=hf[0]-hd[0];

	   hd[0]=eval(hd[0]);
     hd[1]=eval(hd[1]);
     //hd[2]=eval(hd[2]);
	   hf[0]=eval(hf[0]);
     hf[1]=eval(hf[1]);
     // hf[2]=eval(hf[2]);
	   //if(hf[2]<hd[2]){hf[1]=hf[1]-1;hf[2]=hf[2]+60;}
	   // if(hf[1]<hd[1]){hf[0]=hf[0]-1;hf[1]=hf[1]+60;}
	   // if(hf[0]<hd[0]){hf[0]=hf[0]+24;}
	   //return((hf[0]-hd[0]) + ":" + (hf[1]-hd[1])); // + ":" + (hf[2]-hd[2]));
	   return((hf[0]-hd[0])*60 + (hf[1]-hd[1]));
	}

 function check()
 {
   //recupération des variables
   var heuredeb=$('#heuredeb').val();
   var heurefin=$('#heurefin').val();

  if((heuredeb!="") && (heurefin!=""))
  {
    var result=difheure(heuredeb,heurefin);

      if(result<0)
      {
        Swal.fire({
        type: 'error',
        title: 'Attention',
        text: "L'heure de debut ne peut être supérieure à l'heure de fin",

      })

      //$('#heuredeb').html("");
      $('#heurefin').val("");

      }
  }




 }
 jQuery(document).ready(function() {

   $('#submit1').click(function(){

$('#etape').val(1);

       });

       $('#submit2').click(function(){

    $('#etape').val(2);

           });

   $('#routineNew').click(function(){
         AddroutinesRow();


       });

// AddroutinesRow();


$("#classe").select2();
$("#day").select2();
$("#classeEtab").select2();
// $("#matiere").select2();


   $("#FormAddRoutine").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

        classe:"required",
        matiere:"required",
        day:"required",
        heuredeb:"required",
        heurefin:"required"


      },
      messages: {
        classe:"Merci de selectionner la classe",
        matiere:"Merci de selectionner la matière",
        day:"Merci de selectionner un Jour",
        heuredeb:"Merci de renseigner l'heure de début",
        heurefin:"Merci de renseigner l'heure de Fin"
      },
      submitHandler: function(form) {
        //verifier si nous n'avons pas une routine dans la période dites

        form.submit();

//         var etape=3;
//         var heuredeb=$('#heuredeb').val();
//         var heurefin=$('#heurefin').val();
//         var codeEtab="<?php //echo $codeEtabLocal;?>";
//         //alert(heuredeb);
//         $.ajax({
//           url: '../ajax/matiere.php',
//           type: 'POST',
//           async:false,
//           data: 'classe=' + $("#classe").val()+ '&etape=' + etape+'&matiere='+$("#matiere").val()+'&day='+$("#day").val()+'&heuredeb='+heuredeb+'&heurefin='+heurefin+'&codeEtab='+codeEtab,
//           dataType: 'text',
//           success: function (content, statut) {
//
//
//
//           if(content==0)
//             {
//               //le compte n'existe pas dans la base on peut l'ajouter
//
//               form.submit();
//             }else if(content==1){
//             //il est question de la même matiere qui est deja présente dans le système et a cette meme période
//               Swal.fire({
//               type: 'warning',
//               title: 'Attention',
//               text: "Cette matière existe deja pour cette période de cours",
//
//             })
//
//           }else if(content==2)
//           {
//             //il est question d'une nouvelle matière or nous avons une matière a cette période
//             var etape=4;
//             var heuredeb=$('#heuredeb').val();
//             var heurefin=$('#heurefin').val();
//             var codeEtab="<?php //echo $codeEtabLocal;?>";
//             var classe=$("#classe").val();
//             var matiere=$("#matiere").val();
//             Swal.fire({
// title: 'Attention !',
// text: "Une Matière est dejà prévu à cette heure de cours",
// type: 'success',
// showCancelButton: true,
// confirmButtonColor: '#3085d6',
// cancelButtonColor: '#d33',
// confirmButtonText: 'Modifier',
// cancelButtonText: 'Annuler',
// }).then((result) => {
// if (result.value) {
// document.location.href="../controller/routine.php?etape="+etape+"&heuredeb="+heuredeb+"&heurefin="+heurefin+"&codeEtab="+codeEtab+"&classe="+classe+"&matiere="+matiere;
// }else {
//
// }
// })
//             // $("#newStudent").val(0);
//             // form.submit();
//           }
//
//           }
//
//
//         });


      }


   });


 });
 </script>
    <!-- end js include path -->
  </body>

</html>
