<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Admin.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];

$Administrateur=new Admin();
$user=new User();
$etabs=new Etab();
$admin= new Localadmin();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

$admins=$admin->getAllAdminLocal();

$Administrateurs=$Administrateur->getAllAdministateur();

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$codesEtab=$etabs->getAllcodesEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Administrateur Globale</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="#">Administrateur Globales</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Tous les Administrateur Globales</li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['Updateadminok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: 'Félicitations',
            text: '<?php echo $_SESSION['user']['Updateadminok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['Updateadminok']);
                }

                 ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>
<div class="row">
  <div class="col-md-12 col-sm-12">
                            <div class="card card-box">
                                <div class="card-head">
                                    <header><?php echo L::Seacher ?></header>

                                </div>
                                <div class="card-body " id="bar-parent">
                                  <form method="post" id="FormSearch">
                                      <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Nom</label>
                                            <input type="text" id="nomparent" name="nomparent" class="form-control" placeholder="Enter ...">

                                        </div>



                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Prénoms </label>
                                        <input type="text" id="prenomparent" name="prenomparent" class="form-control" placeholder="Enter ...">

                                    </div>


                                </div>
                                      </div>

                                      <button type="submit" class="btn btn-danger"><?php echo L::Seacher ?></button>
                                  </form>
                                </div>
                            </div>
                        </div>
</div>
<div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line">
                               <ul class="nav nav-pills nav-pills-rose">
									<li class="nav-item tab-all"><a class="nav-link active show"
										href="#tab1" data-toggle="tab">Liste</a></li>
									<li class="nav-item tab-all"><a class="nav-link" href="#tab2"
										data-toggle="tab">Grille</a></li>
								</ul>
                                <div class="tab-content">
                                    <div class="tab-pane active fontawesome-demo" id="tab1">
                                        <div class="row">
					                        <div class="col-md-12">
					                            <div class="card  card-box">
					                                <div class="card-head">
					                                    <header></header>
					                                    <div class="tools">
					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
					                                    </div>
					                                </div>
					                                <div class="card-body ">
					                                  <div class="table-scrollable">
					                                    <table class="table table-hover table-checkable order-column full-width" id="example4">
					                                        <thead>
					                                            <tr>

					                                                <th> Name </th>
					                                                <th> Prénoms </th>
					                                                <th> Téléphone </th>
					                                                <th> Email</th>
					                                                <th> Statut </th>
					                                                <th> Date création </th>
					                                                <th> Actions </th>
					                                            </tr>
					                                        </thead>
					                                        <tbody>
                                                    <?php
                                                    $i=1;
                                                      foreach ($Administrateurs as $value):
                                                      ?>
																<tr class="odd gradeX" ondblclick="myFunction(<?php echo $value->id_compte?>)">


                                  <td><?php echo $value->nom_compte?></td>
																	<td class="left"><?php echo $value->prenom_compte?></td>
																	<td class="left"><?php echo $value->tel_compte?></td>
																	<td class="left"><?php echo $value->email_compte?></td>
                                      <td>
                                        <?php
                                        $statut=$value->statut_compte;
                                        if($statut==0)
                                        {
                                          ?>
                        <span class="label label-sm label-danger"> Inactif </span>
                                          <?php
                                        }else {
                                          ?>
                       <span class="label label-sm label-success"> Actif </span>
                                          <?php
                                        }
                                        ?>
                                    </td>
																<td class="left"><?php echo date_format(date_create($value->datecrea_compte),"d/m/Y");?></td>
                                  <td class="center">

                                    <a href="#"  onclick="modify(<?php echo $value->id_compte?>)" class="btn btn-tbl-edit btn-xs">
                                      <i class="fa fa-pencil"></i>
                                    </a>
                                    <?php
                                    if($statut!=0)
                                    {
                                     ?>
                                    <a class="btn btn-tbl-delete btn-xs" onclick="deleted(<?php echo $value->id_compte?>)">
                                      <i class="fa fa-trash-o "></i>
                                    </a>
                                    <?php
                                  }
                                     ?>
                                  </td>
																</tr>
                                <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                <script>

                                function myFunction(idcompte)
                                {
                                  //var url="detailslocal.php?compte="+idcompte;
                                document.location.href="detailsadmin.php?compte="+idcompte;
                                }

                                function modify(id)
                                {


                                  Swal.fire({
                    title: '<?php echo L::WarningLib ?>',
                    text: "Voulez vous vraiment modifier cet Administrateur Globale",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<?php echo L::ModifierBtn ?>',
                    cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                  }).then((result) => {
                    if (result.value) {
                      document.location.href="updateadmin.php?compte="+id;
                    }else {

                    }
                  })
                                }

                                function deleted(id)
                                {

                                  Swal.fire({
                    title: '<?php echo L::WarningLib ?>',
                    text: "Voulez vous vraiment supprimer cet Administrateur Globale",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<?php echo L::DeleteLib ?>',
                    cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                  }).then((result) => {
                    if (result.value) {
                      document.location.href="../controller/admin.php?etape=3&compte="+id;
                    }else {

                    }
                  })
                                }

                                </script>
                                <?php
                                                                 $i++;
                                                                 endforeach;
                                                                 ?>

															</tbody>
					                                    </table>
					                                    </div>
					                                </div>
					                            </div>
					                        </div>
					                    </div>
                                    </div>
                                    <div class="tab-pane" id="tab2">



                    					<div class="row">
                                <?php
                                $i=1;
                                  foreach ($Administrateurs as $value):
                                  ?>
					                        <div class="col-md-4" ondblclick="myFunction(<?php echo $value->id_compte?>)">
				                                <div class="card card-box">
				                                    <div class="card-body no-padding ">
				                                    	<div class="doctor-profile">
                                                <?php
                                                  $lienImg="";
                                                  if(strlen($value->photo_compte)>0)
                                                  {
                                                    $lienImg="../photo/".$value->email_compte."/".$value->photo_compte;
                                                  }else {
                                                    $lienImg="../photo/user5.jpg";
                                                  }
                                                 ?>
                                                        <img src="<?php echo $lienImg?>" class="doctor-pic" alt="" style="height:100px; width:90px;">

					                                        <div class="profile-usertitle">
                                                    <div class="doctor-name"><?php echo $value->nom_compte." ".$value->prenom_compte;?></div>
                                                    <div class="name-center"> <?php echo $value->fonction_compte;?> </div>
					                                        </div>

                                                                                                          <p><?php if($statut==0)
                                                                                                          {
                                                                                                            ?>
                                                                                          <span class="label label-sm label-danger"> Inactif </span>
                                                                                                            <?php
                                                                                                          }else {
                                                                                                            ?>
                                                                                         <span class="label label-sm label-danger"> Actif </span>
                                                                                                            <?php
                                                                                                          }
                                                                                                          ?>
                                                                                                        </p>
				                                                <div>
                                                          <p> <?php echo $value->email_compte;?></p>
                                                         <div><p><i class="fa fa-phone"></i><a href="">  <?php echo $value->tel_compte; ?></a></p> </div>
                                                        </div>
					                                        <div class="profile-userbuttons">
                                                    <a href="#" class="btn btn-circle btn-info btn-sm" onclick="modify(<?php echo $value->id_compte ?>)"><i class="fa fa-pencil"></i></a>
                                                    <?php
                                                     if($statut!=0)
                                                     {
                                                    ?>
                                                    <a href="#" class="btn btn-circle btn-danger btn-sm" onclick="deleted(<?php echo $value->id_compte?>)"><i class="fa fa-trash"></i></a>
                                                    <?php
                                                  }
                                                    ?>
					                                        </div>
				                                        </div>
				                                    </div>
				                                </div>
					                        </div>
                                  <?php
                                                                   $i++;
                                                                   endforeach;
                                                                   ?>


                    					</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function searchlibetab()
   {

     var etape=2;

     $.ajax({
              url: '../ajax/school.php',
              type: 'POST',
              async:false,
              data: 'code='+ $("#codeetab").val()+'&etape='+etape,
              dataType: 'text',
              success: function (content, statut) {

                $("#libetab").html("");
                $("#libetab").html(content);

                //nous allons chercher la liste des admins local de cet etablissement

                $.ajax({
                         url: '../ajax/localadmin.php',
                         type: 'POST',
                         async:false,
                         data: 'code='+ $("#codeetab").val()+'&etape='+etape,
                         dataType: 'text',
                         success: function (response, statut) {


                           $("#adminlo").html("");
                           $("#adminlo").html(response);
                         }
                       });

              }

            });

   }
   $(document).ready(function() {

     $('#example5').DataTable( {
         "scrollX": true

     } );

     $("#codeetab").select2();

     $("#libetab").select2();

   });

   </script>
    <!-- end js include path -->
  </body>

</html>
