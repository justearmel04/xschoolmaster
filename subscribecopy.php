<?php
session_start();

require_once('single/Etablissement.php');
// require_once('class/LocalAdmin.php');
// require_once('class/Parent.php');
// require_once('class/Classe.php');
// require_once('class/Teatcher.php');
// require_once('class/Sessionsacade.php');

// $session= new Sessionacade();
//
// $emailUti=$_SESSION['user']['email'];
// $classe=new Classe();
// $user=new User();
$etabs=new Etab();
// $teatcher=new Teatcher();
// $localadmins= new Localadmin();
// $parents=new ParentX();



//le nombre des eleves de cet etablissement



$alletab=$etabs->getAllEtab();
// $locals=$localadmins->getAllAdminLocal();
// $allparents=$parents->getAllParent();




 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- Material Design Lite CSS -->
    <!--link rel="stylesheet" href="assets2/css/pages/steps.css"-->
	<link href="assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="assets2/css/material_style.css" rel="stylesheet">

	<!-- Theme Styles -->
    <link href="assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
<div class="page-wrapper">
  <div class="container">
    <div class="row" style="margin-top:15px;" id="programmepdf">

      <div class="col-sm-12 col-md-12">
                             <div class="card-box">
                                 <div class="card-head">
                                     <header>Formulaire d'abonnement aux Notifications XSCHOOL</header>
                                 </div>
                                 <div class="card-body ">
                                   <form class="" action="#" method="post" id="subscribeform">

                                     <h3>Enregistrement Parent</h3>
                                     <fieldset>
 									        <div class=" col-md-12 panel-headin" style="background-color:#009fe3; font-size:20px;color: white;text-align:center;"> Informations Personnelles du parent ou du tuteur de l'élève  </div>
                        <br>
                        <div class="row">
                                            <div class="col-md-6">
                        <div class="form-group">
                        <label for=""><b><?php echo L::Name?><span class="required"> * </span>: </b></label>

                      <input type="text" name="nomTea" id="nomTea" data-required="1" placeholder="Entrer le Nom" class="form-control input-height" />
                        </div>

                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                               <label for=""><b><?php echo L::PreName?> : </b></label>
                                <input type="text" name="prenomTea" id="prenomTea" data-required="1" placeholder="Entrer le prénoms" class="form-control input-height" />
                              </div>


                         </div>

                                      </div>
                                      <div class="row">
                                                          <div class="col-md-6">
                                      <div class="form-group">
                                      <label for=""><b>Pays de résidence <span class="required"> * </span>: </b></label>

                                      <select class="form-control" name="pays" required="required">
                       <option value=""> Cameroun </option>
                       <option value=""> Congo Brazzaville </option>
                       <option value=""> Cote d'Ivoire </option>
                       <option value=""> Burkina Faso </option>
                       <option value=""> Gabon </option>
                       <option value=""> Tchad </option>
                       <option value=""> Niger </option>
                       <option value=""> RD Congo </option>
                   </select>
                                      </div>

                                      </div>

                                      <div class="col-md-6">
                  <div class="form-group">
                  <label for=""><b>Téléphone  <span class="required"> * </span>: </b></label>

                  <input type="text" name="telTea" id="telTea" data-required="1" placeholder="Téléphone" class="form-control input-height" />
                  </div>

                  </div>


                                                    </div>
                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                    <div class="form-group">
                                                    <label for=""><b>Date de naissance<span class="required"> * </span>: </b></label>

                                                    <input type="date" placeholder="Entrer la date de naissance" name="datenaisTea" id="datenaisTea" class="form-control input-height">

                                                    </div>

                                                    </div>
                                                    <div class="col-md-6">
                                                         <div class="form-group">
                                                           <label for=""><b>Fonction: </b></label>
                                                            <input type="text" name="fonctionTea" id="fonctionTea" data-required="1" placeholder="Fonction" class="form-control input-height" />
                                                          </div>


                                                     </div>

                                                                  </div>
                                                                  <div class="row">
                                                                                      <div class="col-md-6">
                                                                  <div class="form-group">
                                                                  <label for=""><b>Genre <span class="required"> * </span>: </b></label>

                                                                  <select class="form-control input-height" name="sexeTea" id="sexeTea">
                                                                      <option selected value="">Selectionner le genre</option>
                                                                      <option value="M">Masculin</option>
                                                                      <option value="F">Feminin</option>
                                                                  </select>
                                                                  </div>

                                                                  </div>
                                                                  <div class="col-md-6">
                                                                    <div class="form-group">
                                                                    <label for=""><b>Email <span class="required"> * </span>: </b></label>

                                                                  <input type="text" class="form-control input-height" name="emailTea" id="emailTea" placeholder="Entrer l'adresse email">
                                                                    </div>


                                                                   </div>

                                                                                </div>
                                                                                <div class=" col-md-12 panel-headin" style="background-color:#009fe3; font-size:20px;color: white;text-align:center;"> Informations de compte  </div>
                                                                                <br/>


                                                                                    <div class="form-group row">
                                                                                        <label class="control-label col-md-3"><?php echo L::Logincnx?>
                                                                                            <span class="required">*  </span>
                                                                                        </label>
                                                                                        <div class="col-md-5">
                                                                                            <input name="loginTea" id="loginTea" type="text" placeholder="Entrer le Login " class="form-control input-height" />

                                                                                           </div>
                                                                                    </div>
                                                                                    <div class="form-group row">
                                                                                        <label class="control-label col-md-3"><?php echo L::Passcnx?>
                                                                                            <span class="required">*  </span>
                                                                                        </label>
                                                                                        <div class="col-md-5">
                                                                                            <input name="passTea" id="passTea" type="password" placeholder="Entrer le Mot de passe " class="form-control input-height" /> </div>
                                                                                    </div>
                                                                                    <div class="form-group row">
                                                                                        <label class="control-label col-md-3"><?php echo L::ConfirmPasscnx?>
                                                                                            <span class="required">*  </span>
                                                                                        </label>
                                                                                        <div class="col-md-5">
                                                                                            <input name="confirmTea" id="confirmTea" type="password" placeholder="Confirmer le Mot de passe " class="form-control input-height" /> </div>
                                                                                            <input type="hidden" name="etape" id="etape" value="1"/>

                                                                                    </div>
                                     </fieldset>
                                        <h3>Choix Elève</h3>
                                        <fieldset>
                                          <div class=" col-md-12 panel-headin" style="background-color:#009fe3; font-size:20px;color: white;text-align:center;"> Informations Elève  </div>
                                          <br/>
                                          <div class="row">
                                                              <div class="col-md-6">
                                          <div class="form-group">
                                          <label for=""><b>Matricule<span class="required"> * </span>: </b></label>

                                        <input type="text"  name="matricule" id="matricule" data-required="1" placeholder="Matricule" class="form-control input-height" />
                                          </div>

                                          </div>
                                          <div class="col-md-6">
                                               <div class="form-group">
                                                 <label for=""><b>Etablissement : </b></label>
                                              <input type="text" name="etablissement" id="etablissement" data-required="1" placeholder="Entrer la salaire Net" class="form-control input-height" />
                                                </div>


                                           </div>

                                                        </div>
                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                        <div class="form-group">
                                                        <label for=""><b>Nom & Prénoms<span class="required"> * </span>: </b></label>

                                                      <input type="text"  name="nomcomplet" id="nomcomplet" data-required="1" placeholder="" class="form-control input-height" readonly/>
                                                        </div>

                                                        </div>
                                                        <div class="col-md-6">
                                                             <div class="form-group">
                                                               <label for=""><b>Classe: </b></label>
                                                            <input type="text"  name="classeTea" id="classeTea" data-required="1" placeholder="" class="form-control input-height" />
                                                              </div>


                                                         </div>

                                                                      </div>
                                        </fieldset>
                                        <h3>Choix abonnement</h3>
                                        <fieldset>

                                        </fieldset>

                                   </form>

                                 </div>
                             </div>
                         </div>

    </div>
  </div>

</div>

    <!-- start js include path -->
    <script src="assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="assets2/plugins/popper/popper.min.js" ></script>
     <script src="assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
  <script src="assets2/plugins/jquery-validation/js/jquery.validate.min.js"></script>

     <!-- bootstrap -->
     <script src="assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- wizard -->
        <script src="assets2/plugins/steps/jquery.steps.js" ></script>
        <script src="assets2/js/pages/steps/steps-data.js" ></script>

 	<script src="assets2/js/app.js" ></script>
     <script src="assets2/js/layout.js" ></script>
 	<script src="assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="assets2/plugins/material/material.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   $(document).ready(function() {
     "use strict";
     var wizard = $("#wizard_test").steps();

     var form = $("#subscribeform").show();






     form.steps({
         headerTag: "h3",
         bodyTag: "fieldset",
         transitionEffect: "slideLeft"

     });

   });

   </script>
    <!-- end js include path -->
  </body>

</html>
