<?php

session_start();

require_once('intl/i18n.class.php');



if(!isset($_SESSION['user']['lang']))

{

  $_SESSION['user']['lang']="fr";

}



$i18n = new i18n();

$i18n->setCachePath('langcache');

$i18n->setFilePath('intl/lang/lang_{LANGUAGE}.ini'); // language file path

$i18n->setFallbackLang($_SESSION['user']['lang']);

$i18n->setPrefix('L');

$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available

$i18n->setSectionSeperator('_');

$i18n->setMergeFallback(false);

$i18n->init();

 ?>

<!doctype html>

<html class="no-js " lang="en">



<head>

<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php echo L::Titlepage?></title>

<meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">

<meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">



      <!-- Favicon-->

  <link rel="shortcut icon" href="assets/img/favicon/favicon.ico"  />

    <!-- Custom Css -->

    <link rel="stylesheet" href="assets3/plugins/bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" href="assets/css/sweetalert2.min.css"/>

    <link rel="stylesheet" href="assets3/css/main.css">

    <link rel="stylesheet" href="assets3/css/authentication.css">

    <link rel="stylesheet" href="assets3/css/color_skins.css">

</head>



<body class="theme-cyan authentication sidebar-collapse">

<!-- Navbar -->

<nav class="navbar navbar-expand-lg fixed-top navbar-transparent">

    <div class="container">

        <div class="navbar-translate n_logo">

            <a class="navbar-brand" href="#" title="" target="_blank"></a>

            <button class="navbar-toggler" type="button">

                <span class="navbar-toggler-bar bar1"></span>

                <span class="navbar-toggler-bar bar2"></span>

                <span class="navbar-toggler-bar bar3"></span>

            </button>

        </div>

        <div class="navbar-collapse">

            <ul class="navbar-nav">



                <!--li class="nav-item">

                    <a class="nav-link" href="#">Search Result</a>

                </li>

                <li class="nav-item">

                    <a class="nav-link" title="Follow us on Twitter" href="#" target="_blank">

                        <i class="zmdi zmdi-twitter"></i>

                        <p class="d-lg-none d-xl-none">Twitter</p>

                    </a>

                </li>

                <li class="nav-item">

                    <a class="nav-link" title="Like us on Facebook" href="#" target="_blank">

                        <i class="zmdi zmdi-facebook"></i>

                        <p class="d-lg-none d-xl-none">Facebook</p>

                    </a>

                </li>

                <li class="nav-item">

                    <a class="nav-link" title="Follow us on Instagram" href="#" target="_blank">

                        <i class="zmdi zmdi-instagram"></i>

                        <p class="d-lg-none d-xl-none">Instagram</p>

                    </a>

                </li>

                <li class="nav-item">

                    <a class="nav-link btn btn-primary btn-round" href="sign-up.html">SIGN UP</a>

                </li-->

            </ul>

        </div>

    </div>

</nav>

<!-- End Navbar -->

<div class="page-header">

    <div class="page-header-image" style="background-image:url(assets/img/backv.jpg)"></div>

    <div class="container">

        <div class="col-md-12 content-center">

          <?php



                if(isset($_SESSION['user']['addprogra']))

                {



                  ?>

                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">

                <?php

                //echo $_SESSION['user']['addetabok'];

                ?>

                <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                <span aria-hidden="true">&times;</span>

                   </a>

                </div-->

        <!-- <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

        <script src="../assets/js/sweetalert2.min.js"></script>



            <script>

            Swal.fire({

            type: 'success',

            title: 'Félicitations',

            text: '<?php //echo $_SESSION['user']['addprogra'] ?>',



            }) -->

          <!--/script-->



          <div class="alert alert-success" role="alert">

            <?php echo $_SESSION['user']['addprogra']; ?>

          </div>

                  <?php

                  //unset($_SESSION['user']['addprogra']);

                }



                 ?>

            <div class="card-plain">

                <form class="form" id="Formcnx" method="post" action="controller/connexion.php">

                    <div class="header">

                        <div class="logo-container">

                          <!-- <img src="assets/img/logo/logoibsa2.jpeg" alt="" style="width:300%"> -->





                        </div>

                        <h5>CONNEXION</h5>

                    </div>

                    <div class="content">

                        <div class="input-group input-lg">

                            <input type="text" class="form-control" name="login" id="login" placeholder="Entrer votre Login">

                            <span class="input-group-addon">

                                <i class="zmdi zmdi-account-circle"></i>

                            </span>

                        </div>

                        <div class="input-group input-lg">

                            <input type="password"  name="pass" id="pass"placeholder="Entrer votre mot de passe" class="form-control" />

                            <span class="input-group-addon">

                                <i class="zmdi zmdi-lock"></i>

                            </span>

                        </div>

                    </div>

                    <div class="footer text-center">

                        <!--a href="index.html" class="btn l-cyan btn-round btn-lg btn-block waves-effect waves-light">SIGN IN</a-->

                        <button class="btn l-cyan btn-round btn-lg btn-block waves-effect waves-light" type="submit" >se connecter</button>

                        <!--h6 class="m-t-20"><a href="forgot-password.html" class="link">Forgot Password?</a></h6-->

                    </div>

                </form>

            </div>

        </div>

    </div>

    <footer class="footer">

        <div class="container">

            <nav>


            </nav>

        </div>

    </footer>

</div>



<!-- Jquery Core Js -->

<script src="assets3/bundles/libscripts.bundle.js"></script>

<script src="assets3/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->

<script src="assets2/plugins/jquery/jquery.min.js" ></script>

<script type="text/javascript" src="assets/js/sweetalert2.min.js"></script>

<script type="text/javascript" src="assets/js/libs/form-validator/jquery.validate.min.js"></script>

<script>

   $(".navbar-toggler").on('click',function() {

    $("html").toggleClass("nav-open");

});

//=============================================================================

$('.form-control').on("focus", function() {

    $(this).parent('.input-group').addClass("input-group-focus");

}).on("blur", function() {

    $(this).parent(".input-group").removeClass("input-group-focus");

});



</script>

<script>

           showSwal = function(type){

              'use strict';

              if(type === 'basic'){

            swal({

              title:"Erreur",

              text:"Le login saisi n'existe pas dans notre base de données",

              type:'warning'

            })



        }else if(type === 'basic1')

        {

          swal({

            title:"Compte inactif",

            text:"Merci de prendre contact avec le support IT",

            type:'warning'

          })

        }else if(type === 'basic2')

        {

          swal({

            title:"Mot de passe incorrect",

            text:"Merci de réessayer avec le bon Mot de passe",

            type:'warning'

          })

        }else if(type === 'bonsoir')

          {

              swal({

                title: 'Are you sure?',

                text: "You won't be able to revert this!",

                type: 'warning',

                showCancelButton: true,

                confirmButtonColor: '#3085d6',

                cancelButtonColor: '#d33',

                confirmButtonText: 'Great '

              }).then(function () {

              swal(

                'Deleted!',

                'Your file has been deleted.',

                'success'

              )

            })

          }

           }

          </script>

          <script>

           jQuery(document).ready(function() {



             $("#Formcnx").validate({



               errorPlacement: function(label, element) {

               label.addClass('mt-2 text-danger');

               label.insertAfter(element);

             },

             highlight: function(element, errorClass) {

               $(element).parent().addClass('has-danger')

               $(element).addClass('form-control-danger')

             },

             success: function (e) {

                   $(e).closest('.control-group').removeClass('error').addClass('info');

                   $(e).remove();

               },

               rules:{



                 login:{

                    required: true

                    //email: true

                  },

                 pass:{

                  required: true,

                  minlength: 6

                }

               },

               messages: {

                  login:"Merci de renseigner vôtre Login.",

                  pass:{

                required: "Merci de renseigner votre mot de passe",

                minlength: "Votre mot de passe doit contenir au moins 6 caractères"

                }



              },



              submitHandler: function(form) {



                var etape=1;

                $.ajax({



                  url: 'ajax/connexion.php',

                  type: 'POST',

                  async:false,

                  data: 'login=' + $("#login").val()+ '&pass=' + $("#pass").val()+ '&etape=' + etape,

                  dataType: 'text',

                  success: function (content, statut) {



                    if(content==0)

                    {

                      //showSwal('basic');

                      Swal.fire({

                      type: 'warning',

                      title: 'Erreur',

                      text: "Le login saisi n'existe pas dans notre base de données",



                      })

                    }else if(content==1) {

                      //showSwal('basic1');

                      Swal.fire({

                      type: 'warning',

                      title: 'Compte Inactif',

                      text: "Merci de prendre contact avec le support IT",



                      })

                    }else if(content==2)

                    {

                      //showSwal('basic2');

                      Swal.fire({

                      type: 'warning',

                      title: 'Mot de passe incorrect',

                      text: "Merci de réessayer avec le bon Mot de passe",



                      })

                    }else {

                      form.submit();

                    }



                  }

                });



              }



             });

           });

           </script>





</body>



</html>
