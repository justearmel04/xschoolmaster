<?php

// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}


//require_once('../../class1/User.php');

//require_once('../../class1/cnx.php');

//require_once('../../class1/Parent.php');

//require_once('../../class1/Etablissement.php');

require_once('../../class/cnx.php');

require_once('../../class1/functions.php');

$db = new mysqlConnector();

//$users = new User();

//$parents=new ParentX();

//$etabs=new Etab();





if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Recuperation des donnees de la premiere etape : Infos Client

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$codeEtab=$request->codeEtab;
$sessionEtab=$request->sessionEtab;
$matiereid=$request->matiereid;
$libellematiere=$request->matierelib;
$teatcherid=$request->teatcherid;
$classeid=$request->classeid;
$libelleclasse=$request->classelib;
$idtypenote=$request->controleid;
$typesessionNotes=$request->trimestre;
$coefficientNotes=$request->coefficient;

$typenote=1;
$datepiste=date("Y-m-d");
$controlepiste=1;
$reglementpiste=0;
$examenpiste=0;
$actionpiste=1;
$tabNameofstudents="";

$code="SELECT * FROM controle,notes where controle.id_ctrl=notes.idtype_notes and controle.codeEtab_ctrl='$codeEtab' and controle.session_ctrl='$sessionEtab' and controle.typesess_ctrl='$typesessionNotes' and notes.idclasse_notes='$classeid' and controle.mat_ctrl='$matiereid'";
$req=$db->dataBase->prepare($code);
$req->execute([]);
$datas=$req->fetchAll();
$nbnotessemesterclasses=count($datas);

$data=$request->items;

if($nbnotessemesterclasses==0)
  {
	//la première note dans cette matiere

     foreach ($data as  $value):

       $notes=$value->notes;
       $observation=$value->observations;
       $id_eleve=$value->ideleve;
       $matricule=$value->matricule;

       $notescoef=$notes*$coefficientNotes;
       $moyennestudent=$notescoef/$coefficientNotes;

       //nous allons ajouter la note dans rating

$code1="INSERT INTO rating SET session_rating='$sessionEtab',typsession_rating='$typesessionNotes',ideleve_rating='$id_eleve',classe_rating='$classeid',idprof_rating='$teatcherid',matiereid_rating='$matiereid',totalnotes_rating='$notes',totalnotescoef_rating='$notescoef',totalcoef_rating='$coefficientNotes',rating='$moyennestudent',codeEtab_rating='$codeEtab'";
$req1=$db->dataBase->prepare($code1);
$req1->execute([]);

$code2="INSERT INTO  notes SET type_notes='$typenote',idtype_notes='$idtypenote',idclasse_notes='$classeid',idmat_notes='$matiereid',idprof_notes='$teatcherid',ideleve_notes='$id_eleve',codeEtab_notes='$codeEtab',valeur_notes='$notes',obser_notes='$observation',session_notes='$sessionEtab'";
$req2=$db->dataBase->prepare($code2);
$req2->execute([]);


     endforeach;

  }else
  {

  foreach ($data as  $value):

       $notes=$value->notes;
       $observations=$value->observations;
       $id_eleve=$value->ideleve;
       $matricule=$value->matricule;

       $code1="SELECT * FROM rating where ideleve_rating='$id_eleve' and matiereid_rating='$matiereid' and classe_rating='$classeid' and typsession_rating='$typesessionNotes' and session_rating='$sessionEtab'";
       $req1=$db->dataBase->prepare($code1);
       $req1->execute([]);
       $donnees=$req1->fetchAll();
       foreach($donnees as values):
       /* $alltotalnotes=$values->totalnotes_rating;
        $alltotalnotescoef=$values->totalnotescoef_rating;
        $totalcoefnotes=$values->totalcoef_rating;
        $ratingId=$values->id_rating;*/
       endforeach;


  endforeach;



  }







echo json_encode('success');


}

function getratingMention($rating)
      {
        $mention="";
        if($rating<5)
        {
          $mention="Faible";
        }else if($rating>5 && $rating<10 )
        {
          $mention="Insuffisant";
        } else if($rating>=10 && $rating<12 )
        {
          $mention="Passable";
        }else if($rating>=12 && $rating<14 )
        {
          $mention="Assez bien";
        }else if($rating>=14 && $rating<16 )
        {
          $mention="Bien";
        }else if($rating>=16 && $rating<17.5 )
        {
          $mention="Très bien";
        }else if($rating>18)
        {
          $mention="Excellent";
        }

        return $mention;

      }

?>
