<?php
session_start();
require_once('../controller/functions.php');
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Matiere.php');

$session= new Sessionacade();
$matiere=new Matiere();
$emailUti=$_SESSION['user']['email'];
$userId=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$classeShool=new Classe();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();

//recuperaion des variables

$codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
$classeid=htmlspecialchars(addslashes($_GET['classeEtab']));
$session=htmlspecialchars(addslashes($_GET['session']));
$datedeb=htmlspecialchars(addslashes($_GET['datedeb']));
$nb=htmlspecialchars(addslashes($_GET['nb']));

$libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
$logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
$lienlogo="../logo_etab/".$codeEtab."/".$logoEtab;
$contacts="";
$infosEtab=$etabs->getEtabInfosbyCode($codeEtab);
$tabEtab=explode('*',$infosEtab);

 if($tabEtab[3]!="")
 {
   $contacts=$tabEtab[2]." / ".$tabEtab[3];
 }else {
   $contacts=$tabEtab[2];
 }

 $datefin=date("d-m-Y", strtotime("+".$nb."day", strtotime($datedeb)));


 $dataday=$etabs->getAllweeks();
 $libelleclasse=$classeShool->getInfosofclassesbyId($classeid,$session);
 $students=$student->getAllstudentofthisclassesSession($classeid,$session);
 if(isset($_GET['matricule']))
 {
   $libelledoc="RECAPITULATIF DES ABSENCES ";
 }else  if(isset($_GET['matiere']))
 {
    $libelledoc="RECAPITULATIF DES ABSENCES MATIERE ";
 }
 else {
   $libelledoc="RECAPITULATIF DES ABSENCES "." ".$libelleclasse;
 }


require('fpdf/fpdf.php');

class PDF extends FPDF
{
function Footer()
{
    // Positionnement à 1,5 cm du bas
    $this->SetY(-15);
    // Police Arial italique 8
    $this->SetFont('Arial','I',8);
    // Numéro et nombre de pages
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    // $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
}
}

$pdf = new PDF('L','mm','A4');
$pdf->AliasNbPages();
$pdf->SetFont('Times','B',  16);
$pdf->AddPage();
$pdf->Ln(12);
$pdf->Image("../logo_etab/".$codeEtab."/".$logoEtab,5,5,25,25);
$pdf->Ln(10);
$pdf -> SetX(60);
$pdf-> SetFont('Times','B',  15);
$pdf->Cell(176,5,$libelledoc,0,0,'C');
$pdf->Ln(10);
// $pdf->Ln(8);
$pdf -> SetX(60);
$datefin=date("d-m-Y", strtotime("+".$_GET['nb']."day", strtotime($_GET['datedeb'])));
$periode=date_format(date_create($_GET['datedeb']), "d-m-Y")." AU ".$datefin;
$pdf-> SetFont('Times','',  11);
$pdf->Cell(176,5,'PERIODE DU: ' .$periode,0,0,'C');
$pdf->Ln(5);
$pdf -> SetX(60);
$pdf-> SetFont('Times','',  11);
$pdf->Cell(176,5,'Annees scolaire : 2019-2020',0,0,'C');
$pdf->Ln(5);



$pdf->SetFillColor(230,230,0);
$pdf->SetLineWidth(.3);
$pdf->SetFont('Times','B',12);
$pdf->Cell(30,8,'Matricule',1,0,'C');
$pdf->Cell(80,8,'Nom & prenoms',1,0,'C');
$pdf->Cell(68,8,'MATIERE',1,0,'C');
$pdf->Cell(50,8,'HEURE DEBUT',1,0,'C');
$pdf->Cell(50,8,'HEURE FIN',1,0,'C');
$pdf->Ln();

for($x=0;$x<=$_GET['nb'];$x++)
{
  if(isset($_GET['matricule']))
  {

    $dateday= date("Y-m-d", strtotime("+".$x."day", strtotime($_GET['datedeb'])));
    $nbabsebnces=$student->getAbsencesthisdaynbStudent($dateday,$_GET['codeEtab'],$_GET['session'],$_GET['classeEtab'],$_GET['matricule']);

    if($nbabsebnces>0)
    {
      $datas=$student->getlisteabsencesdayStudent($dateday,$_GET['codeEtab'],$_GET['session'],$_GET['classeEtab'],$_GET['matricule']);
      $pdf->SetFont('Times','B',9);
      $pdf->Cell(278,8,date_format(date_create($dateday), "d-m-Y"),1,0,'C');
      $pdf->Ln();
      foreach ($datas as $value):
        $pdf->Cell(30,8,$value->matricule_presence,1,0,'C');
        $pdf->Cell(80,8,$value->nom_eleve." ".$value->prenom_eleve,1,0,'C');
        $pdf->Cell(68,8,$matiere->getMatiereLibelleByIdMat($value->matiere_presence,$value->codeEtab_presence),1,0,'C');
        $pdf->Cell(50,8,returnHours($value->heuredeb_heure),1,0,'C');
        $pdf->Cell(50,8,returnHours($value->heurefin_heure),1,0,'C');
        $pdf->Ln();

      endforeach;
    }

  }else if(isset($_GET['matiere']))
  {

    $dateday= date("Y-m-d", strtotime("+".$x."day", strtotime($_GET['datedeb'])));
    $nbabsebnces=$student->getAbsencesthisdaynbMat($dateday,$_GET['codeEtab'],$_GET['session'],$_GET['classeEtab'],$_GET['matiere']);

    if($nbabsebnces>0)
    {
      $datas=$student->getlisteabsencesdayMat($dateday,$_GET['codeEtab'],$_GET['session'],$_GET['classeEtab'],$_GET['matiere']);
      $pdf->SetFont('Times','B',9);
      $pdf->Cell(278,8,date_format(date_create($dateday), "d-m-Y"),1,0,'C');
      $pdf->Ln();
      foreach ($datas as $value):
        $pdf->Cell(30,8,$value->matricule_presence,1,0,'C');
        $pdf->Cell(80,8,$value->nom_eleve." ".$value->prenom_eleve,1,0,'C');
        $pdf->Cell(68,8,$matiere->getMatiereLibelleByIdMat($value->matiere_presence,$value->codeEtab_presence),1,0,'C');
        $pdf->Cell(50,8,returnHours($value->heuredeb_heure),1,0,'C');
        $pdf->Cell(50,8,returnHours($value->heurefin_heure),1,0,'C');
        $pdf->Ln();

      endforeach;
    }

  }else {
    $dateday= date("Y-m-d", strtotime("+".$x."day", strtotime($_GET['datedeb'])));
    $nbabsebnces=$student->getAbsencesthisdaynb($dateday,$_GET['codeEtab'],$_GET['session'],$_GET['classeEtab']);

    if($nbabsebnces>0)
    {
      $datas=$student->getlisteabsencesday($dateday,$_GET['codeEtab'],$_GET['session'],$_GET['classeEtab']);
      $pdf->SetFont('Times','B',9);
      $pdf->Cell(278,8,date_format(date_create($dateday), "d-m-Y"),1,0,'C');
      $pdf->Ln();
      foreach ($datas as $value):
        $pdf->Cell(30,8,$value->matricule_presence,1,0,'C');
        $pdf->Cell(80,8,$value->nom_eleve." ".$value->prenom_eleve,1,0,'C');
        $pdf->Cell(68,8,$matiere->getMatiereLibelleByIdMat($value->matiere_presence,$value->codeEtab_presence),1,0,'C');
        $pdf->Cell(50,8,returnHours($value->heuredeb_heure),1,0,'C');
        $pdf->Cell(50,8,returnHours($value->heurefin_heure),1,0,'C');
        $pdf->Ln();

      endforeach;
    }
  }



}





$pdf->Output();


 ?>
