<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];

$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$classe=new Classe();
$imageprofile=$user->getImageProfile($emailUti);
$logindata=$user->getLoginProfile($emailUti);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);
// $classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);
// $nbclasse=$classe->getNumberClassesOfTeatcherId($_SESSION['user']['IdCompte']);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$parentlyStudent=$parents->getDifferentStudentByParentId($_SESSION['user']['IdCompte']);

$nbsessionOn=$session->getNumberSessionEncoursOn($_GET['codeEtab']);
$etablissementType=$etabs->DetermineTypeEtab($_GET['codeEtab']);

if($nbsessionOn>0)
{
  $sessionencours=$session->getSessionEncours($_GET['codeEtab']);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $parascos=$etabs->getAllParascolairesOfThisSchool($_GET['codeEtab'],$libellesessionencours);
  $notifications=$etabs->getAllNotificationbySchoolCode($_GET['codeEtab'],$libellesessionencours);
}


// $alletab=$etabs->getAllEtab();
// $locals=$localadmins->getAllAdminLocal();
// $allparents=$parents->getAllParent();


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Notifications</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="#">Notifications</a>
                                </li>

                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


          <div class="row">

            <div class="col-md-12">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header></header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>

                                    </div>
                                </div>
                                <div class="card-body ">

                                    <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                        <thead>
                                          <tr>





                                              <th> Libellé Notification </th>

                                              <th> Classe(s)</th>

                                              <th> Destinataire(s)</th>

                                              <th> Type envoi</th>

                                              <th> Statut</th>

                                              <th> <?php echo L::Actions?> </th>

                                          </tr>
                                        </thead>
                                        <tbody>

                                          <?php



                                          foreach ($notifications as $valuenotif):
                                            $classeselected=substr($valuenotif->classes_msg,0,-1);
                                            $tabclasseselected=explode("-",$classeselected);
                                            $counttable=count($tabclasseselected);

                                            $precis=$valuenotif->precis_msg;

                                            if($precis==0)
                                            {
                                              for($i=0;$i<$counttable;$i++)
                                              {
                                                if($tabclasseselected[$i]==$_GET['classe'])
                                                {
                                                  $dataparas=$etabs->getAllNotificationbySchoolCodesenderparent($valuenotif->id_msg,$_GET['classe'],$valuenotif->session_msg,$valuenotif->codeEtab_msg);
                                                  foreach ($dataparas as $value):
                                                    ?>
                                                    <tr class="odd gradeX">


                                                        <td>

                                                            <a href="#"> <?php echo $value->libelle_notif; ?> </a>

                                                        </td>

                                                        <td>

                                                          <?php

                                                           $dataclasse=$value->classes_notif;

                                                           $tabdataclasse=explode("-",$dataclasse);

                                                           $nbtabdataclasse=count($tabdataclasse);

                                                           $limitdataclasse=$nbtabdataclasse-1;

                                                           // echo $limitdataclasse;



                                                           for($z=0;$z<$limitdataclasse;$z++)

                                                           {

                                                             ?>



                                                               <span class="label label-sm label-success" style=""> <?php echo $classe->getInfosofclassesbyId($tabdataclasse[$z],$libellesessionencours); ?> </span>





                                                            <?php



                                                           }

                                                           ?>



                                                        </td>

                                                        <td>

                                                          <?php

                                                          $datadestinataire=$value->destinataires_notif;

                                                          $tabdatadestinataires=explode("-",$datadestinataire);

                                                          $nbtabdatadestinataires=count($tabdatadestinataires);

                                                          $limitdatadestinataires=$nbtabdatadestinataires-1;



                                                          for($k=0;$k<$limitdatadestinataires;$k++)

                                                          {

                                                            ?>

                                                            <span class="label label-sm label-warning"> <?php echo $tabdatadestinataires[$k]; ?> </span>

                                                           <?php



                                                          }

                                                          ?>

                                                       </td>

                                                       <td>

                                                         <?php

                                                           $envoitype="";

                                                           if($value->sms_notif==0)

                                                           {

                                                             if($value->email_notif==0)

                                                             {



                                                             }else if($value->email_notif==1)

                                                             {

                                                               echo "EMAIL";

                                                             }

                                                           }else if($value->sms_notif==1){

                                                             if($value->email_notif==0)

                                                             {

                                                               echo "SMS";

                                                             }else if($value->email_notif==1)

                                                             {

                                                               echo "SMS / EMAIL";

                                                             }

                                                           }

                                                          ?>

                                                       </td>

                                                         <td>

                                                           <?php

                                                             $statutnotif=$value->statut_notif;



                                                             if($statutnotif==0)

                                                             {

                                                               ?>

                               <button type="button" class="btn btn-circle btn-danger btn-xs m-b-10">En attente</button>

                                                               <?php



                                                             }else if($statutnotif==1)

                                                             {

                                                               ?>

                             <button type="button" class="btn btn-circle btn-success btn-xs m-b-10">EnvoyÃ©</button>

                                                               <?php

                                                             }else if($statutnotif==2)

                                                             {

                                                               ?>



                                                               <?php

                                                             }

                                                          ?>

                                                         </td>

                                                          <td>

                                                            <?php

                                                              $statutnotif=$value->statut_notif;



                                                              if($statutnotif==0)

                                                              {

                                                                ?>

                                                                <a href="#"  title="Envoyer" onclick="send(<?php echo $value->id_notif; ?>,<?php echo $value->para_notif; ?>)" class="btn btn-info btn-circle ">

                                                                  <i class="fa fa-send"></i>

                                                                </a>

                                                                <a class="btn btn-danger btn-circle  " onclick="deleted(<?php echo $value->id_notif; ?>,<?php echo $value->para_notif; ?>,<?php echo $value->scola_notif; ?>)"  title="Supprimer">

                                                                  <i class="fa fa-trash-o "></i>

                                                                </a>

                                                                 <?php



                                                              }else if($statutnotif==1)

                                                              {

                                                                ?>

                                                                <a class="btn btn-warning btn-circle  " title="archiver" onclick="archiver(<?php echo $value->id_notif; ?>,<?php echo $value->para_notif; ?>,<?php echo $value->scola_notif; ?>)">

                                                                  <i class="fa fa-repeat "></i>

                                                                </a>

                                                                <?php

                                                              }else if($statutnotif==2)

                                                              {

                                                                ?>



                                                                <?php

                                                              }

                                                           ?>

                                                          </td>



                                                    </tr>
                                                    <?php
                                                  endforeach;
                                                }
                                              }
                                            }else if($precis==1)
                                            {

                                            }

                                           ?>





                                            <?php

                                            $i++;

                                            endforeach

                                             ?>







                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
          </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- calendar -->
     <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
   <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
    <script src="../assets2/js/pages/table/table_data.js" ></script>
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   $(document).ready(function() {



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
