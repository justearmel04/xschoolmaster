<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Student.php');
require_once('../class/Classe.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$emailUti=$_SESSION['user']['email'];

$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$classe=new Classe();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);
// $classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);
// $nbclasse=$classe->getNumberClassesOfTeatcherId($_SESSION['user']['IdCompte']);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$parentlyStudent=$parents->getDifferentStudentByParentId($_SESSION['user']['IdCompte']);

$concatcodeEtab="";
$concatclasseids="";

foreach ($parentlyStudent as $valuex):
  $datas=$parents->getStudentdatas($valuex->id_compte);

  foreach ($datas as $valuez):
    $concatcodeEtab=$concatcodeEtab.$valuez->codeEtab_inscrip.",";
    $concatclasseids=$concatclasseids.$valuez->idclasse_inscrip.",";
  endforeach;

endforeach;


$concatcodeEtab=substr($concatcodeEtab, 0, -1);
$concatclasseids=substr($concatclasseids, 0, -1);



// $alletab=$etabs->getAllEtab();
// $locals=$localadmins->getAllAdminLocal();
// $allparents=$parents->getAllParent();


 $nbnotificationstandby=$parents->getParentnotificationstandbyNb($compteuserid);
//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($compteuserid);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
     <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>


	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::Reacpsdesabsences ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><!--i class="fa fa-home"></i-->&nbsp;<a class="parent-item" href="index.php"><?php echo L::AbsMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::Reacpsdesabsences ?> </li>
                          </ol>
                      </div>
                  </div>
					<!-- start widget -->
          <div class="state-overview">
            <div class="row">
              <?php

                    if(isset($_SESSION['user']['addattendailyok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
            <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: 'Félicitation',
                text: '<?php echo $_SESSION['user']['addattendailyok']; ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addattendailyok']);
                    }

                     ?>
                </div>
            </div>
          <!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

  <br/>
					<!-- end widget -->

          <div class="row">

            <div class="col-md-12 col-sm-12">

                                      <div class="card card-box">

                                          <div class="card-head">

                                              <header><?php echo L::Seacher ?></header>



                                          </div>

                                          <div class="card-body " id="bar-parent">

                                            <form method="post" id="FormTeatcherSearch" name="recaplistes.php">

                                                <div class="row">

                                                  <div class="col-md-6 col-sm-6">

                                                  <!-- text input -->

                                                  <div class="form-group" style="margin-top:8px;">
                                                      <label><?php echo L::DatedebLib ?></label>
                                                      <input type="text" id="datedeb" name="datedeb" class="form-control" placeholder="<?php echo L::EntersDatedebLib ?>">

                                                      <input type="hidden" name="search" id="search" />
                                                      <input type="hidden" name="tabcodeEtab" id="tabcodeEtab" value="<?php echo $concatcodeEtab; ?>">
                                                      <input type="hidden" name="tabclasses" id="tabclasses" value="<?php echo $concatclasseids; ?>">
                                                      <input type="hidden" name="codeEtab" id="codeEtab" value="">
                                                      <input type="hidden" name="sessionEtab" id="sessionEtab" value="">
                                                  </div>

                                                  <div class="form-group">

                                                      <label><?php echo L::studMenu ?></label>

                                                      <select class="form-control " id="studentid" name="studentid" style="width:100%">

                                                          <option value=""><?php echo L::SelectOneStudent ?></option>

                                                          <?php

                                                          foreach ($parentlyStudent as $value):

                                                           ?>

                                                           <option value="<?php echo $value->id_compte; ?>"><?php echo utf8_encode(utf8_decode($value->nom_compte." ".$value->prenom_compte)) ?></option>

                                                           <?php

                                                         endforeach;

                                                            ?>

                                                      </select>

                                                  </div>



                                              </div>

                                              <div class="col-md-6 col-sm-6">

                                              <!-- text input -->

                                              <div class="form-group" style="margin-top:8px;">
                                                  <label><?php echo L::DatefinLib ?></label>
                                                  <input type="text" id="datefin" name="datefin" class="form-control" placeholder="<?php echo L::EntersDatefinLib ?>">

                                              </div>





                                          </div>

                                                </div>



                                                <button type="submit" class="btn btn-danger"><?php echo L::Seacher ?></button>

                                            </form>

                                          </div>

                                      </div>

                                  </div>

          </div>





          <div class="row" style="" id="affichage">
            <?php
                if(isset($_POST['search']))
                {
                    if(isset($_POST['datedeb'])&&isset($_POST['datefin']))
                    {
                        //nous devons recupérer la liste des jours du mois selectionner
                        $datedeb=$_POST['datedeb'];
                        $datefin=$_POST['datefin'];
                        $datetime1 = date_create($_POST['datedeb']); // Date fixe
                        $datetime2 = date_create($_POST['datefin']); // Date fixe
                        $interval = date_diff($datetime1, $datetime2);
                        $nb= $interval->format('%a');



                        ?>

                        <div class="offset-md-4 col-md-4"  id="affichage1">
                          <div class="card" style="">
                          <div class="card-body">
                            <h5 class="card-title"></h5>
                            <h4 style="font-style: italic;font-weight: bold;text-align:center;font-size:font-size: large;"><?php echo L::Reacpsdesabsences ?></h4>
                            <p class="card-text" style="text-align:center;font-weight: bold;"><?php //echo @$infosclasses; ?></p>
                            <p class="card-text" style="text-align:center;font-weight: bold"><?php echo $_POST['datedeb'].' au '.$_POST['datefin'] ?></p>

                          </div>
                        </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card card-box">
                                <div class="card-head">
                                    <header></header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                      <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                      <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">
                                  <div class="pull-right">
                                    <!-- <a href="#" class="btn btn-md btn-warning" onclick="recapattendanceDay('<?php echo $_POST['datedeb']; ?>','<?php echo $nb;  ?>',<?php echo $_POST['classeEtab']  ?>,'<?php echo $libellesessionencours ?>','<?php echo $codeEtab  ?>','<?php echo $matriculesStudent ?>')"><i class="fa fa-print"></i>Imprimer </a> -->
                                  </div>
                                <div class="item-content table-responsive">
                                  <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                       <thead>
                           <tr>
                               <th><?php echo L::LittleDateVersements ?></th>
                               <th><?php echo L::MatriculestudentTab ?></th>
                               <th><?php echo L::NamestudentTab ?></th>
                               <th><?php echo L::Etat ?></th>
                               <th style="display:none"><?php echo L::Actions ?></th>
                               <!-- <th><?php //echo L::HoursEnd ?></th> -->
                           </tr>
                       </thead>
                       <tbody>

                         <?php

                         if(isset($_POST['studentid'])&&strlen($_POST['studentid'])>0)
                         {
                           for($x=0;$x<=$nb;$x++)
                           {

                           $dateday= date("Y-m-d", strtotime("+".$x."day", strtotime($datedeb)));

                           $datas=$parents->getStudentdatas($_POST['studentid']);

                           $matriculesStudent=$student->getstudentMatriculebyid($_POST['studentid']);

                           foreach ($datas as $valuez):
                            $codeEtab=$valuez->codeEtab_inscrip;
                            $libellesessionencours=$valuez->session_inscrip;
                            $classeid=$valuez->idclasse_inscrip;

                           endforeach;

                           // echo $dateday."codeEtab:".$codeEtab." sessionEtab:".$libellesessionencours." classeid: ".$classeid."matricule: ".$matriculesStudent;

                           $nbabsebnces=$student->getAbsencesthisdaynbStudent($dateday,$codeEtab,$libellesessionencours,$classeid,$matriculesStudent);
                           // echo $nbabsebnces;

                           if($nbabsebnces>0)
                           {
                             $datak=$student->getAbsencesthisdaynbStudents($dateday,$codeEtab,$libellesessionencours,$classeid,$matriculesStudent);
                             // var_dump($datak);
                             foreach ($datak as $value):
                               ?>
                               <tr>
                                 <td><?php echo $value->date_presence ?></td>
                                 <td><?php echo $value->matricule_presence ?></td>
                                 <td><?php echo $value->nom_compte." ".$value->prenom_compte ?></td>
                                 <td>
                                   <?php
                                     if($value->statut_presence==0)
                                     {
                                       echo L::Absent;
                                     }else {
                                       echo L::RetardLibelle;
                                     }
                                    ?>
                                 </td>
                                 <td style="display:none"></td>
                               </tr>
                               <?php
                             endforeach;
                           }
                         }

                         }else {

                           foreach ($parentlyStudent as $values):

                             for($x=0;$x<=$nb;$x++)
                             {
                               $dateday= date("Y-m-d", strtotime("+".$x."day", strtotime($datedeb)));

                               $datas=$parents->getStudentdatas($valuex->id_compte);

                               $matriculesStudent=$student->getstudentMatriculebyid($valuex->id_compte);

                               foreach ($datas as $valuez):
                                $codeEtab=$valuez->codeEtab_inscrip;
                                $libellesessionencours=$valuez->session_inscrip;
                                $classeid=$valuez->idclasse_inscrip;

                               endforeach;

                               // echo $dateday."codeEtab:".$codeEtab." sessionEtab:".$libellesessionencours." classeid: ".$classeid."matricule: ".$matriculesStudent;

                               $nbabsebnces=$student->getAbsencesthisdaynbStudent($dateday,$codeEtab,$libellesessionencours,$classeid,$matriculesStudent);
                               // echo $nbabsebnces;

                               if($nbabsebnces>0)
                               {
                                 $datak=$student->getAbsencesthisdaynbStudents($dateday,$codeEtab,$libellesessionencours,$classeid,$matriculesStudent);
                                 // var_dump($datak);
                                 foreach ($datak as $value):
                                   ?>
                                   <tr>
                                     <td><?php echo $value->date_presence ?></td>
                                     <td><?php echo $value->matricule_presence ?></td>
                                     <td><?php echo $value->nom_compte." ".$value->prenom_compte ?></td>
                                     <td>
                                       <?php
                                         if($value->statut_presence==0)
                                         {
                                           echo L::Absent;
                                         }else {
                                           echo L::RetardLibelle;
                                         }
                                        ?>
                                     </td>
                                     <td style="display:none"></td>
                                   </tr>
                                   <?php
                                 endforeach;
                               }

                             }

                             ?>

                             <?php
                           endforeach;

                         }




                          ?>




                       </tbody>
                   </table>
                                  </div>

                                </div>
                            </div>
                        </div>



                        <?php

                    }
                    ?>
                    <?php
                  }else {
                    $datedeb=date("d-m-Y");
                    $datetime1 = date_create($datedeb); // Date fixe
                    $datefin = date('Y-m-d', strtotime($datedeb. ' - 1 month'));
                    $datetime2 = date_create($datefin); // Date fixe
                    $interval = date_diff($datetime1, $datetime2);
                    $nb= $interval->format('%a');

                    // echo $nb;

                    ?>
                    <div class="offset-md-4 col-md-4"  id="affichage1">
                      <div class="card" style="">
                      <div class="card-body">
                        <h5 class="card-title"></h5>
                        <h4 style="font-style: italic;font-weight: bold;text-align:center;font-size:font-size: large;"><?php echo L::Reacpsdesabsences ?></h4>
                        <p class="card-text" style="text-align:center;font-weight: bold;"><?php //echo @$infosclasses; ?></p>
                        <p class="card-text" style="text-align:center;font-weight: bold"><?php echo $datedeb.' au '.$datefin ?></p>

                      </div>
                    </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card card-box">
                            <div class="card-head">
                                <header></header>
                                <div class="tools">
                                    <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                  <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                  <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                </div>
                            </div>
                            <div class="card-body ">
                              <div class="pull-right">
                                <!-- <a href="#" class="btn btn-md btn-warning" onclick="recapattendanceDay('<?php echo $_POST['datedeb']; ?>','<?php echo $nb;  ?>',<?php echo $_POST['classeEtab']  ?>,'<?php echo $libellesessionencours ?>','<?php echo $codeEtab  ?>','<?php echo $matriculesStudent ?>')"><i class="fa fa-print"></i>Imprimer </a> -->
                              </div>
                            <div class="item-content table-responsive">
                              <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                   <thead>
                       <tr>
                           <th><?php echo L::LittleDateVersements ?></th>
                           <th><?php echo L::MatriculestudentTab ?></th>
                           <th><?php echo L::NamestudentTab ?></th>
                           <th><?php echo L::Etat ?></th>
                           <th style="display:none"><?php echo L::Actions ?></th>
                           <!-- <th><?php //echo L::HoursEnd ?></th> -->
                       </tr>
                   </thead>
                   <tbody>

                     <?php

                     foreach ($parentlyStudent as $values):

                       for($x=0;$x<=$nb;$x++)
                       {
                         $dateday= date("Y-m-d", strtotime("+".$x."day", strtotime($datefin)));

                         $datas=$parents->getStudentdatas($valuex->id_compte);

                         $matriculesStudent=$student->getstudentMatriculebyid($valuex->id_compte);

                         foreach ($datas as $valuez):
                          $codeEtab=$valuez->codeEtab_inscrip;
                          $libellesessionencours=$valuez->session_inscrip;
                          $classeid=$valuez->idclasse_inscrip;

                         endforeach;

                         // echo $dateday."codeEtab:".$codeEtab." sessionEtab:".$libellesessionencours." classeid: ".$classeid."matricule: ".$matriculesStudent;

                         $nbabsebnces=$student->getAbsencesthisdaynbStudent($dateday,$codeEtab,$libellesessionencours,$classeid,$matriculesStudent);
                         // echo $nbabsebnces;

                         if($nbabsebnces>0)
                         {
                           $datak=$student->getAbsencesthisdaynbStudents($dateday,$codeEtab,$libellesessionencours,$classeid,$matriculesStudent);
                           // var_dump($datak);
                           foreach ($datak as $value):
                             ?>
                             <tr>
                               <td><?php echo $value->date_presence ?></td>
                               <td><?php echo $value->matricule_presence ?></td>
                               <td><?php echo $value->nom_compte." ".$value->prenom_compte ?></td>
                               <td>
                                 <?php
                                   if($value->statut_presence==0)
                                   {
                                     echo L::Absent;
                                   }else {
                                     echo L::RetardLibelle;
                                   }
                                  ?>
                               </td>
                               <td style="display:none"></td>
                             </tr>
                             <?php
                           endforeach;
                         }

                       }

                       ?>

                       <?php
                     endforeach;


                      ?>




                   </tbody>
               </table>
                              </div>

                            </div>
                        </div>
                    </div>
                    <?php
                  }
                    ?>



                    </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->


            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
  <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
      <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
    <script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
    <script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
  <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   var date = new Date();
   var newDate = new Date(date.setTime( date.getTime() + (0 * 86400000)));

   $('#datedeb').bootstrapMaterialDatePicker
   ({
     date:true,
     shortTime: false,
     time:false,
     maxDate:newDate,
     format :'DD-MM-YYYY',
     lang: 'fr',
    cancelText: '<?php echo L::AnnulerBtn ?>',
    okText: '<?php echo L::Okay ?>',
    clearText: '<?php echo L::Eraser ?>',
    nowText: '<?php echo L::Now ?>'

   });

   $('#datefin').bootstrapMaterialDatePicker
   ({
     date:true,
     shortTime: false,
     time:false,
     maxDate:newDate,
     format :'DD-MM-YYYY',
     lang: 'fr',
    cancelText: '<?php echo L::AnnulerBtn ?>',
    okText: '<?php echo L::Okay ?>',
    clearText: '<?php echo L::Eraser ?>',
    nowText: '<?php echo L::Now ?>'

   });

   $(document).ready(function() {

$("#studentid").select2();

$("#FormTeatcherSearch").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
 },
 highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
 },
 success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    classeEtab:"required",
    month:"required",
    yearsession:"required",
    datedeb:"required",
    datefin:"required"



  },
  messages: {

    classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
    month:"<?php echo L::PleaseSelectAnMonth ?>",
    yearsession:"Merci de selection l'année de session",
    datedeb:"<?php echo L::PleaseEnterDatedebLib ?>",
    datefin:"<?php echo L::PleaseEnterDatefinLib ?>",
    eleveid:"<?php echo L::PleaseSelectOneStudent ?>"

  },
  submitHandler: function(form) {
    form.submit();
  }
});


   });

   </script>
    <!-- end js include path -->
  </body>

</html>
