<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$emailUti=$_SESSION['user']['email'];

$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$classe=new Classe();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);
// $classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);
// $nbclasse=$classe->getNumberClassesOfTeatcherId($_SESSION['user']['IdCompte']);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$parentlyStudent=$parents->getDifferentStudentByParentId($_SESSION['user']['IdCompte']);


 $nbnotificationstandby=$parents->getParentnotificationstandbyNb($compteuserid);
//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($compteuserid);


// $alletab=$etabs->getAllEtab();
// $locals=$localadmins->getAllAdminLocal();
// $allparents=$parents->getAllParent();


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
 <body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">

     <div class="page-wrapper">

         <!-- start header -->

 		<?php

 include("header.php");

     ?>

         <!-- end header -->

         <!-- start page container -->

         <div class="page-container">

  			<!-- start sidebar menu -->

  			<?php

 				include("menu.php");

 			?>

 			 <!-- end sidebar menu -->

 			<!-- start page content -->

             <div class="page-content-wrapper">

                 <div class="page-content">

                     <div class="page-bar">

                         <div class="page-title-breadcrumb">

                             <div class=" pull-left">

                                 <div class="page-title"><?php echo L::FichestudMenu ?></div>

                             </div>

                             <ol class="breadcrumb page-breadcrumb pull-right">

                                 <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::HomeCaps ?></a>&nbsp;<i class="fa fa-angle-right"></i>

                                 </li>

                                 <li class=""><a href="fiches.php"><?php echo L::EleveMenusingle ?></a></li>&nbsp; <i class="fa fa-angle-right"></i>

                                 <li class="active"><?php echo L::FichestudMenu ?></li>

                             </ol>

                         </div>

                     </div>

 					<!-- start widget -->

 					<div class="state-overview">

 						<div class="row">



 					        <!-- /.col -->



 					        <!-- /.col -->



 					        <!-- /.col -->



 					        <!-- /.col -->

 					      </div>

 						</div>

 					<!-- end widget -->

           <?php



                 if(isset($_SESSION['user']['addetabexist']))

                 {



                   ?>

                   <div class="alert alert-danger alert-dismissible fade show" role="alert">

                 <?php

                 echo $_SESSION['user']['addetabexist'];

                 ?>

                 <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                 <span aria-hidden="true">&times;</span>

                    </a>

                 </div>







                   <?php

                   unset($_SESSION['user']['addetabexist']);

                 }



                  ?>





                  <div class="row">

                    <?php
                    foreach ($parentlyStudent as  $value):

                        foreach ($parentlyStudent as $values):
                          $studentid=$values->id_compte;
                          $photoid=$values->photo_compte;
                          $infos=$parents->getStudentCurrentlyinscription($_SESSION['user']['IdCompte'],$studentid);
                          // var_dump($infos);
                          foreach ($infos as $personnal):
                            $infoscodeEtab=$personnal->codeEtab_inscrip;
                            $infossessionEtab=$personnal->session_inscrip;
                            $infosclasseEtab=$personnal->idclasse_inscrip;

                            $matriculeid=$personnal->matricule_eleve;
                            $nomid= $personnal->nom_eleve;
                            $prenomid=$personnal->prenom_eleve;
                            $datenaisid= $personnal->datenais_eleve;
                            $lieunaisid=$personnal->lieunais_eleve;
                            $genreid= $personnal->sexe_eleve;
                            $emailid=$personnal->email_eleve;
                            $libelleclasseid= $personnal->libelle_classe;
                            $etablissementid=$personnal->libelle_etab;




                          endforeach;





                        endforeach;

                      ?>

                      <div class="col-md-6">
                        <div class="col-sm-12">

                          <div class="card ">

                                                <div class="card-body no-padding height-9">

                                                    <div class="row">

                                                        <div class="profile-userpic">

                                                          <?php

                                                              if($photoid!="")

                                                              {

                                                                $lien="../photo/Students/".$matriculeid."/".$photoid;

                                                              }else {

                                                                $lien="../photo/user5.jpg";

                                                              }

                                                           ?>

                                                            <img src="<?php echo $lien;?>" class="" alt=""> </div>
                                                      </div>
                                                    </div>

                                                    <div class="profile-usertitle">


                                                        <div class="profile-usertitle-name"><?php echo $nomid." ".$prenomid;?></div>

                                                        <div class="profile-usertitle-job"> <?php echo $matriculeid;?></div>

                                                    </div>


                                                    <ul class="list-group list-group-unbordered">

                                            <li class="list-group-item">
                                                <b style="margin-left:10px;"><?php echo L::ClasseMenu ?></b>
                                                <div class="profile-desc-item pull-right"><?php echo $libelleclasseid ?></div>
                                            </li>
                                          </ul>

                                                    <!-- END SIDEBAR USER TITLE -->

                                                    <!-- SIDEBAR BUTTONS -->
                                                    <div class="row">

                                                      <div class="col-md-6">
                                                        <a  target="_blank" class="btn  btn-warning btn-md" href="#"> <i class="fa fa-print"></i>Imprimer la  fiche Eleve
                                                        </a>
                                                      </div>
                                                      <div class="col-md-6">
                                                        <a  target="_blank" class="btn btn-warning btn-md" href="#"> <i class="fa fa-print"></i>Imprimer la  fiche Eleve
                                                        </a>
                                                      </div>


                                                    </div> <br>

                                                    <div class="row">

                                                      <div class="col-md-6">
                                                        <a  target="_blank" class="btn  btn-warning btn-md" href="#"> <i class="fa fa-print"></i>Imprimer la  fiche Eleve
                                                        </a>
                                                      </div>
                                                      <div class="col-md-6">
                                                        <a  target="_blank" class="btn btn-warning btn-md" href="#"> <i class="fa fa-print"></i>Imprimer la  fiche Eleve
                                                        </a>
                                                      </div>


                                                    </div>

                                                    <div class="profile-userbuttons">

            			<a  target="_blank" class="btn btn-circle btn-warning btn-sm" href="listePersonnelEleve.php?compte=<?php echo $_GET['compte']?>&sessionEtab=<?php echo $libellesessionencours; ?>&codeEtab=<?php echo $codeEtabAssigner ?>"> <i class="fa fa-print"></i>Imprimer la  fiche Eleve
                  </a>
                                              </div>
                                              <div class="profile-userbuttons">

            <a  class="btn btn-circle btn-success btn-sm" href="addnotifications.php?compte=<?php echo $_GET['compte']  ?>"> <i class="fa fa-pencil"></i><?php echo L::Writetoteatcher ?>
            </a>
                                        </div>
                                        <div class="profile-userbuttons">

      <a   class="btn btn-circle btn-primary btn-sm" href="notifications.php"> <i class="fa fa-book"></i><?php echo L::Readteatcher ?>
      </a>
                                  </div>

                                                    <!-- END SIDEBAR BUTTONS -->

                                                </div>

                                            </div>

                        </div>






                      </div>

                      <?php
                    endforeach;
                     ?>




                    							</div>





                      <!-- start new patient list -->



                     <!-- end new patient list -->



                 </div>

             </div>

             <!-- end page content -->

             <!-- start chat sidebar -->



             <!-- end chat sidebar -->

         </div>

         <!-- end page container -->

         <!-- start footer -->

         <div class="page-footer">

             <div class="page-footer-inner"> 2019 &copy;

             <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>

             </div>

             <div class="scroll-to-top">

                 <i class="material-icons">eject</i>

             </div>

         </div>

         <!-- end footer -->

     </div>

     <!-- start js include path -->

     <script src="../assets2/plugins/jquery/jquery.min.js" ></script>

  	<script src="../assets2/plugins/popper/popper.min.js" ></script>

      <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>

  	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

      <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>

      <!-- bootstrap -->

      <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>

      <!-- calendar -->

      <script src="../assets2/plugins/moment/moment.min.js" ></script>

      <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>

      <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>

      <!-- Common js-->

  	<script src="../assets2/js/app.js" ></script>

      <script src="../assets2/js/layout.js" ></script>

  	<script src="../assets2/js/theme-color.js" ></script>

  	<!-- Material -->

  	<script src="../assets2/plugins/material/material.min.js"></script>









     <!-- morris chart -->

     <!--script src="../assets2/plugins/morris/morris.min.js" ></script>

     <script src="../assets2/plugins/morris/raphael-min.js" ></script>

     <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->



    <script>


    function addFrench()
    {
      var etape=1;
      var lang="fr";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

    function addEnglish()
    {
      var etape=1;
      var lang="en";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }


    function generatefichepdf(idcompte)

    {

      var codeEtab="<?php echo $codeEtabAssigner ?>";

       var etape=2;

        $.ajax({

          url: '../ajax/admission.php',

          type: 'POST',

          async:false,

          data: 'compte=' +idcompte+ '&etape=' + etape+'&codeEtab='+ codeEtab,

          dataType: 'text',

          success: function (content, statut) {



           window.open(content, '_blank');



          }

        });

    }



    $(document).ready(function() {







    });



    </script>

     <!-- end js include path -->

   </body>

</html>
