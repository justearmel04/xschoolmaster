<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Teatcher.php');
require_once('../controller/functions.php');

$session= new Sessionacade();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$localadmins= new Localadmin();
$parents=new ParentX();
$matiere=new Matiere();
$student=new Student();
$imageprofile=$user->getImageProfile($emailUti);
$logindata=$user->getLoginProfile($emailUti);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);

$matieres=$matiere->getAllSubjectTeatchByTeatcherId($_SESSION['user']['IdCompte']);
$controles=$matiere->getAllControleMatiereOfThisTeatcherId($_SESSION['user']['IdCompte']);

$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
}

$nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);

$programmes=$etabs->getAllprogrammesOfTeatcherClassesAndpgrmeId($_SESSION['user']['IdCompte'],$_GET['programme'],$libellesessionencours);
//$dataclasses=$classe->getAllClassesByClasseId($classeschoolid);
$tabprogramme=explode("*",$programmes);

$fiches=$etabs->getAllfichesOfTeatcherClassesAndpgrmeId($_SESSION['user']['IdCompte'],$_GET['programme']);
$tabfiche=explode("*",$fiches);


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Meilleure application de communication Parents Etablissement</title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Modification Fiche Lecture</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li>&nbsp;<a class="parent-item" href="#">Gestion des Matières</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Modification Fiche Lecture</li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">
              <?php

                    if(isset($_SESSION['user']['addprogra']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
            <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['addprogra'] ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addprogra']);
                    }

                     ?>
					      </div>
						</div>
					<!-- end widget -->



          <div class="row">

            <div class="col-sm-12">
								<div class="card-box">
									<div class="card-head">
										<header></header>
									</div>
									<div class="card-body ">
						            <div class = "mdl-tabs mdl-js-tabs">
						               <div class = "mdl-tabs__tab-bar tab-left-side">

						                  <a href = "#tab5-panel" class = "mdl-tabs__tab is-active">Modifier Fiche Lecture</a>

						               </div>

						               <div class = "mdl-tabs__panel is-active p-t-20" id = "tab5-panel">
                             <div class="row">
                                 <div class="col-md-12 col-sm-12">
                                     <div class="card card-box">
                                         <div class="card-head">
                                             <header></header>

                                         </div>

                                         <div class="card-body" id="bar-parent">
                                             <form  id="FormAddAcademique" class="form-horizontal" action="../controller/fiche.php" method="post"  enctype="multipart/form-data">
                                                 <div class="form-body">

                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                                           <span class="required"> * </span>
                                                       </label>
                                                       <div class="col-md-5">
                                                         <select class="form-control input-height" id="classeEtab" name="classeEtab" style="width:100%;" onchange="searchmatiere(<?php echo $_SESSION['user']['IdCompte']; ?>)" readonly>
                                                             <option value=""><?php echo L::Selectclasses ?></option>
                                                             <?php
                                                             $i=1;
                                                               foreach ($classes as $value):
                                                               ?>
                                                               <option <?php if($tabfiche[2]==$value->id_classe){echo "selected";} ?> value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                               <?php
                                                                                                $i++;
                                                                                                endforeach;
                                                                                                ?>

                                                         </select>
                                                         <input type="hidden" name="classeEtab" id="classeEtab" value="<?php echo $tabfiche[2]; ?>" />
                                                       </div>

                                                   </div>
                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3">Matière
                                                           <span class="required">  *</span>
                                                       </label>
                                                       <div class="col-md-5">
                                                         <select readonly class="form-control input-height" id="matclasse" name="matclasse" style="width:100%;" onchange="searchcodeEtab(<?php echo $_SESSION['user']['IdCompte']; ?>)">
                                                             <option value=""><?php echo L::SelectSubjects ?></option>
                                                             <option value="<?php echo $tabfiche[1]; ?>" selected><?php echo $matiere->getMatiereLibelleByIdMat($tabfiche[1],$tabfiche[4]) ?></option>

                                                         </select>
                                                         <input type="hidden" name="matclasse" id="matclasse" value="<?php echo $tabfiche[1]; ?>" />
                                                       </div>

                                                   </div>
                                                   <div class="row">
                                                     <div class="offset-md-1 col-md-9">
                                                       <div class="table-responsive">
                                           <table class="table table-striped custom-table table-hover">
                                               <thead>
                                                   <tr>
                                                       <th style="width:75%"></th>
                                                       <th></th>
                                                   </tr>
                                               </thead>
                                               <tbody>
                                                 <?php
                                                 $sousfiches=$etabs->getAllSousFichesOfThisFicheId($tabfiche[0]);
                                                 $i=1;
                                                 foreach ($sousfiches as $value):
                                                  ?>
                                                   <tr>
                                                       <td><a href="#">
                                                         <span class="label label-sm label-success"><?php echo trim(htmlspecialchars(addslashes(utf8_encode($value->libelle_sousfic)))); ?></span>
                                                         </a>
                                                       </td>

                                                       <td >

                                                           <a class="btn btn-primary btn-xs" data-toggle="modal" data-target="#largeModel<?php echo $value->id_sousfic ?>" >
                                                               <i class="fa fa-pencil"></i>
                                                           </a>
                                                           <a class="btn btn-danger btn-xs" onclick="deleted(<?php echo $value->id_sousfic;?>)">
                                                               <i class="fa fa-trash-o "></i>
                                                           </a>
                                                       </td>
                                                   </tr>
                                                   <div class="modal fade" id="largeModel<?php echo $value->id_sousfic ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					    <div class="modal-dialog " role="document">
					        <div class="modal-content">
					            <div class="modal-header">
					                <h4 class="modal-title" id="exampleModalLabel">Modification Element Fiche</h4>
					                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					                    <span aria-hidden="true">&times;</span>
					                </button>
					            </div>
					            <div class="modal-body">

                          <form id="Updatesous<?php echo $value->id_sousfic ?>" class="" action="#" method="post">
                            <div class="form-body">
                          <div class="form-group row">
                                    <label class="control-label col-md-3">Element
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-8">
<input onclick="erasedMessage(<?php echo $value->id_sousfic;?>)" type="text" name="desi<?php echo $value->id_sousfic ?>" id="desi<?php echo $value->id_sousfic ?>" value="<?php echo trim(htmlspecialchars(addslashes(utf8_encode($value->libelle_sousfic)))); ?>" class="form-control input-height" />
<p id="messages<?php echo $value->id_sousfic;?>"></p>
                                       </div>

                                </div>


                            <div class="form-actions">
                                <div class="row">
                                    <div class="offset-md-3 col-md-9">
                                        <button type="button"  onclick="check(<?php echo $value->id_sousfic ?>)" class="btn btn-info">Modifier</button>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                                    </div>
                                  </div>
                               </div>
                            </div>
                          </form>
					            </div>

					        </div>
					    </div>
					</div>
                                                    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
                                                   <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                                   <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                                   <script type="text/javascript">
                                                   function check(id)
                                                   {
                                                      var desi=$("#desi"+id).val();
                                                      var ficheid="<?php echo $tabfiche[0]; ?>";
                                                      var sousficheid=' <?php echo $value->id_sousfic;?>';
                                                      var etape=3;
                                                      if(desi=="")
                                                      {
                                                        document.getElementById("messages"+id).innerHTML = "<font color=\"red\">Merci de renseigner ce champ SVP !</font>";
                                                      }else
                                                      {
                                                        //alert("bonjour");
                                                        // $("#FormAddAcademique #Updatesous"+id).submit();

                                                        //recupeartion des variables

                                                        $.ajax({
                                                          url: '../ajax/fiche.php',
                                                          type: 'POST',
                                                          async:true,
                                                          data: 'ficheid=' + ficheid+ '&etape=' + etape+'&desi='+desi+'&sousficheid='+sousficheid,
                                                          dataType: 'text',
                                                          success: function (content, statut) {

                                                          window.location.reload();
                                                          }
                                                        });


                                                      }
                                                   }

                                                   function erasedMessage(id)
                                                   {
                                                       document.getElementById("messages"+id).innerHTML = "";
                                                   }
                                                   function deleted(id)
                                                   {

                                                     Swal.fire({
                                       title: '<?php echo L::WarningLib ?>',
                                       text: "Voulez vous vraiment supprimer cet élement de la Fiche de Lecture",
                                       type: 'warning',
                                       showCancelButton: true,
                                       confirmButtonColor: '#3085d6',
                                       cancelButtonColor: '#d33',
                                       confirmButtonText: '<?php echo L::DeleteLib ?>',
                                       cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                     }).then((result) => {
                                       if (result.value) {
                                         //document.location.href="../controller/programme.php?etape=3&programme="+id;
                                         //nous allons compter le nombre d'ele,ent constituant la sous fiche
                                         var ficheid="<?php echo $tabfiche[0]; ?>";
                                         var classe="<?php echo $tabfiche[2]; ?>";
                                         var matiere="<?php echo $tabfiche[1]; ?>";
                                         var teatcherid="<?php echo $tabfiche[3]; ?>";
                                         var etape=2;
                                         $.ajax({

                                              url: '../ajax/fiche.php',
                                              type: 'POST',
                                              async:true,
                                              data: 'ficheid=' + ficheid+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere+'&teatcherid='+teatcherid,
                                              dataType: 'text',
                                              success: function (content, statut) {



                                                 if(content==0)
                                                 {
                                                   document.location.href="../controller/fiche.php?etape=3&programme="+id+"&ficheid="+ficheid;
                                                 }else if(content==1)
                                                 {
                                                   Swal.fire({
                                                   type: 'warning',
                                                   title: '<?php echo L::WarningLib ?>',
                                                   text: 'Vous ne pouvez pas faire cette action',

                                                   })
                                                 }

                                              }
                                            });
                                       }else {

                                       }
                                     })
                                                   }
                                                   </script>
                                                   <?php
                                                   $i++;
                                                 endforeach;
                                                    ?>

                                               </tbody>
                                           </table>
                                           </div>

                                                     </div>

                                                   </div>



                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3"><?php echo L::File?>
                                                           <span class="required">  *</span>
                                                       </label>
                                                       <div class="col-md-5">
                                                         <?php
                                                         //determiner le lien du fichier
                                                         $libelleclasse=$classe->getInfosofclassesbyId($tabfiche[2],$libellesessionencours);
                                                         $tabdatefiche=explode("-",$tabfiche[6]);
                                                         $yearsfiche=$tabdatefiche[0];

                                                          $lien="../fiches/".$yearsfiche."/".$libelleclasse."/".$tabfiche[5];
                                                          $tabfic=explode(".",$tabfiche[5]);
                                                         $type="";
                                                         if($tabfic[1]=="pdf")
                                                         {
                                                           $type="../assets/img/pdf.png";
                                                         }else if(($tabfic[1]=="doc")||($tabfic[1]=="docx"))
                                                         {
                                                           $type="../assets/img/word.png";
                                                         }
                                                          ?>
                                                      <input type="file" id="fichier" name="fichier" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="<?php echo $lien; ?>" data-allowed-file-extensions="doc docx pdf" />
                                                      <input type="hidden" name="oldfichier" id="oldfichier" value="<?php echo $tabfiche[5]; ?>">
                                                      <input type="hidden" name="ficheid"  id="ficheid"  value="<?php echo $tabfiche[0]; ?>">
                                                      <input type="hidden" name="years" id="years" value="<?php echo $yearsfiche; ?>">
                                                      <input type="hidden" name="classelibelle" id="classelibelle" value="<?php echo $libelleclasse; ?>">
                                                      <input type="hidden" name="etape" id="etape" value="2" />
                                                      <input type="hidden" id="codeEtab" name="codeEtab" value="<?php echo $tabfiche[4]; ?>"/>
                                                       </div>

                                                   </div>






                               <div class="form-actions">
                                                     <div class="row">
                                                         <div class="offset-md-3 col-md-9">
                                                             <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                                             <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                         </div>
                                                       </div>
                                                    </div>
                             </div>
                                             </form>
                                         </div>
                                     </div>
                                 </div>
                             </div>
						               </div>

						            </div>
									</div>
								</div>
							</div>

          </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
  <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
  <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
  <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
  <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
  <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
  <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
  <script src="../assets2/js/pages/table/table_data.js" ></script>
  <script src="../assets2/plugins/select2/js/select2.js" ></script>
  <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <!-- calendar -->
     <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
  <script src="../assets2/plugins/moment/moment.min.js" ></script>
  <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
  <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
  <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
  <script src="../assets2/js/dropify.js"></script>
   <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   $("#fichier").dropify({
     messages: {
         "default": "Merci de selectionner le fichier",
         "replace": "Modifier le fichier",
         "remove" : "Supprimer le fichier",
         "error"  : "Erreur"
     }
   });

$("#matclasse").select2({disabled: true});
$("#classeEtab").select2({disabled: true});

function searchcodeEtab(id)
{
  var classe=$("#classeEtab").val();
  var teatcherId=id;
  var etape=7;
  var matiere=$("#matclasse").val();

  $.ajax({

       url: '../ajax/matiere.php',
       type: 'POST',
       async:true,
       data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
       dataType: 'text',
       success: function (content, statut) {

          $("#FormAddAcademique #codeEtab").val(content);

       }
     });

}

function searchmatiere(id)
{

    var classe=$("#classeEtab").val();
    var teatcherId=id;
    var etape=6;


  $.ajax({

       url: '../ajax/matiere.php',
       type: 'POST',
       async:true,
       data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe,
       dataType: 'text',
       success: function (content, statut) {


         $("#matclasse").html("");
         $("#matclasse").html(content);

       }
     });
}

   $(document).ready(function() {

$("#FormAddAcademique").validate({

  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
 },
 highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
 },
 success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{

    programme:"required",
    descri:"required",
    classeEtab:"required",
    matclasse:"required"


  },
messages: {
  programme:"Merci de renseigner le libellé du programme",
  descri:"Merci de renseigner la description du programme",
  classeEtab:"<?php echo L::PleaseSelectclasseOnerequired ?>",
  matclasse:"Merci de <?php echo L::SelectSubjects ?>"

},
submitHandler: function(form) {

//nous allons verifier si le programme n'existe pas dans la base de données

 var etape=1;
 var teatcher="<?php echo $_SESSION['user']['IdCompte']; ?>";
 $.ajax({
   url: '../ajax/programme.php',
   type: 'POST',
   async:false,
   data: 'programme=' + $("#programme").val()+ '&classeEtab=' + $("#classeEtab").val() + '&matclasse=' + $("#matclasse").val() + '&etape=' + etape+'&codeEtab='+$("#codeEtab").val()+'&teatcher='+teatcher,
   dataType: 'text',
   success: function (content, statut) {
form.submit();
/*
     if(content==0)
     {
       //le compte n'existe pas dans la base on peut l'ajouter

       form.submit();
     }else if(content==1)
     {
       Swal.fire({
       type: 'warning',
       title: '<?php echo L::WarningLib ?>',
       text: 'Ce Programme existe dejà dans le système',

       })
     }*/

   }


 });


}

});

   });

   </script>
    <!-- end js include path -->
  </body>

</html>
