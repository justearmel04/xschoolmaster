<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Teatcher.php');
require_once('../class/Matiere.php');
require_once('../controller/functions.php');
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$teatcher=new Teatcher();
$matiere=new Matiere();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
// $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$codeEtabAssigner=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);
// $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
  $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
}else {
    $nbclasse=0;
}


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
     <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>


	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Courbe des absences</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><!--i class="fa fa-home"></i-->&nbsp;<a class="parent-item" href="index.php"><?php echo L::Presences ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Courbe des absences</li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">
              <?php

                    if(isset($_SESSION['user']['addattendailyok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
            <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['addattendailyok']; ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addattendailyok']);
                    }

                     ?>
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

<br/>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="card card-topline-green">
                   <div class="card-head">
                       <header></header>
                       <div class="tools">
                           <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                       </div>
                   </div>
                   <div class="card-body ">
                     <form method="post" id="FormSearch" action="attendancegraph.php">
                         <div class="row">
                           <div class="col-md-6 col-sm-6">
                           <!-- text input -->
                           <div class="form-group" style="margin-top:8px;">
                               <label><?php echo L::DatedebLib ?></label>
                               <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                               <input type="text" name="datedeb" id="datedeb" class="form-control" placeholder="Entrer la date de début" value="">
                               <input type="hidden" name="search" id="search" />
                               <input type="hidden" name="codeEtab" id="codeEtab"  value="<?php echo $codeEtabAssigner ?>">
                               <input type="hidden" name="sessionEtab" id="sessionEtab" value="<?php echo $libellesessionencours ?>" />
                           </div>



                       </div>
                       <div class="col-md-6 col-sm-6">
                       <!-- text input -->
                       <div class="form-group" style="margin-top:8px;">
                           <label><?php echo L::DatefinLib ?></label>
                           <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                           <input type="text" name="datefin" id="datefin" class="form-control" value="" placeholder="Entrer la date de fin">
                       </div>


                   </div>
                   <div class="col-md-6 col-sm-6">
                     <div class="form-group" style="margin-top:8px;">
                         <label>Classe</label>
                         <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                         <select class="form-control input-height" id="classeEtab" name="classeEtab" style="width:100%;" onchange="selectStudent()">
                             <option value=""><?php echo L::Selectclasses ?></option>
                             <?php
                             $i=1;
                               foreach ($classes as $value):
                               ?>
                               <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                               <?php
                                                                $i++;
                                                                endforeach;
                                                                ?>

                         </select>
                     </div>
                   <!-- text input -->



               </div>
               <div class="col-md-6 col-sm-6">
                 <div class="form-group" style="margin-top:8px;">
                     <label>Elève</label>
                     <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                     <select class="form-control input-height" id="studentclasse" name="studentclasse" style="width:100%;">
                         <option value="">Selectionner un élève</option>


                     </select>
                 </div>
               <!-- text input -->



           </div>
                   <button type="submit" class="btn btn-success"  style="width:150px;height:35px;margin-top:33px;"><?php echo L::Displaying ?></button>

                         </div>


                     </form>
                   </div>
               </div>
                        </div>

          </div>
<!-- element à faire apparaitre au clique du bouton rechercher -->
<div class="row" style="" id="affichage">
  <?php
      if(isset($_POST['search']))
      {
          // if(isset($_POST['classeEtab'])&&isset($_POST['month'])&&isset($_POST['yearsession']))
          // {
          //     //nous devons recupérer la liste des jours du mois selectionner
          //
          //      //$num = cal_days_in_month(CAL_GREGORIAN, retranscrireMois($mois), $annee);
          //
          //      $moisconcat=$_POST['month'];
          //      $annee=$_POST['yearsession'];
          //      $tabmoisconcat=explode("-",$moisconcat);
          //      $mois=$tabmoisconcat[0];
          //      $numbermois=$tabmoisconcat[1];
          //      $num = cal_days_in_month(CAL_GREGORIAN,$numbermois, $annee);
          //
          //      $concat=$annee."-".regiveMois($numbermois)."-";
          //
          //      $infosclasses=$classe->getInfosofclassesbyId($_POST['classeEtab']);
          //
          //      //nous allons chercher la liste des eleves de cette classe
          //      // $students=$student->getAllstudentofthisclasses($_POST['classeEtab']);
          //      $students=$student->getAllstudentofthisclassesSession($_POST['classeEtab'],$libellesessionencours);
          //
          //
          //
          //
          // }

            if(isset($_POST['studentclasse'])&& (strlen($_POST['studentclasse'])>0))
            {
              // echo "sepecifique";
              $datastudent=$student->getAllInformationsOfStudent($_POST['studentclasse'],$_POST['sessionEtab']);
              $tabdatasutent=explode("*",$datastudent);
              $matriculestudent=$tabdatasutent[1];
              // $nombrepresencethisStuedent=$student->DetermineNumberOfAttendanceTimely($matriculestudent,$_POST['classeEtab'],$_POST['sessionEtab'],$_POST['datedeb'],$_POST['datefin'],$_POST['codeEtab']);
              // $nombrepresenceabsentStudent=$student->DetermineAbsenceOfThisStudentTimely($matriculestudent,$_POST['classeEtab'],$_POST['sessionEtab'],$_POST['datedeb'],$_POST['datefin'],$_POST['codeEtab']);
              //
              // $nombrepresencethisStuedentNB=$student->getNBOfAttendanceTimely($matriculestudent,$_POST['classeEtab'],$_POST['sessionEtab'],$_POST['datedeb'],$_POST['datefin'],$_POST['codeEtab']);
              // $nombrepresenceabsentStudentNB=$student->getNBAbsenceOfThisStudentTimely($matriculestudent,$_POST['classeEtab'],$_POST['sessionEtab'],$_POST['datedeb'],$_POST['datefin'],$_POST['codeEtab']);
              // echo $nombrepresencethisStuedent." -".$nombrepresenceabsentStudent;
              $nbjourpresentstudent=$student->getNombrePresentdayStudent($matriculestudent,$_POST['classeEtab'],$_POST['sessionEtab'],dateFormat($_POST['datedeb']),dateFormat($_POST['datefin']),$_POST['codeEtab']);
              $nbjourabsentstudent=$student->getNombreAbsentdayStudent($matriculestudent,$_POST['classeEtab'],$_POST['sessionEtab'],dateFormat($_POST['datedeb']),dateFormat($_POST['datefin']),$_POST['codeEtab']);

            }

          ?>

          <?php

          if(isset($_POST['studentclasse'])&& (strlen($_POST['studentclasse'])>0))
          {
            // echo "sepecifique";
        ?>
        <div class="offset-md-4 col-md-4"  id="affichage1">
          <div class="card" style="">
          <div class="card-body">
            <h5 class="card-title"></h5>
            <h4 style="font-style: italic;font-weight: bold;text-align:center;font-size:font-size: large;"><?php echo L::StudentsgraphesPresences ?></h4>
            <p class="card-text" style="text-align:center;font-weight: bold;"><?php echo $tabdatasutent[2]." ".$tabdatasutent[3]; ?></p>
            <p class="card-text" style="text-align:center;"> <i class="fa fa-pie-chart fa-3x"></i> </p>

          </div>
        </div>
        </div>
        <?php
          }
           ?>

           <?php
           if(isset($_POST['studentclasse'])&& (strlen($_POST['studentclasse'])>0))
           {
            ?>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="affichage2">
                                  <div class="card card-topline-green">
                                      <div class="card-head">
                                          <header></header>
                                          <div class="tools">
                                            <button class="btn btn-md btn-warning" id="print-chart-btn"><i class="fa fa-print"></i> <?php echo L::Printer ?> </button>
                                          </div>
                                      </div>
                                      <div class="card-body" id="chartjs_pie_parent">


                                        <div class="row">
                                             <canvas id="chartjs_student" height="120"></canvas>
                                        </div>



                              </div>
                                  </div>
                              </div>
            <?php
          }
             ?>


          <?php
              }
          ?>



          </div>

<!-- fin affichage du bouton rechercher -->

                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
      <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>


    <!--Chart JS-->
    <script src="../assets2/plugins/chart-js/Chart.bundle.js" ></script>
    <script src="../assets2/plugins/chart-js/utils.js" ></script>
    <script src="../assets2/js/pages/chart/chartjs/chartjs-data.js" ></script>
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <script src="../assets2/js/jspdf.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
<?php
  if(isset($_POST))
  {
    ?>

<?php
  }
 ?>

 var date = new Date();
 var newDate = new Date(date.setTime( date.getTime() + (0 * 86400000)));

 $('#datedeb').bootstrapMaterialDatePicker
 ({
   date:true,
   shortTime: false,
   time:false,
   maxDate:newDate,
   format :'DD-MM-YYYY',
   lang: 'fr',
  cancelText: '<?php echo L::AnnulerBtn ?>',
  okText: 'OK',
  clearText: '<?php echo L::Eraser ?>',
  nowText: '<?php echo L::Now ?>'

 });

 $('#datefin').bootstrapMaterialDatePicker
 ({
   date:true,
   shortTime: false,
   time:false,
   maxDate:newDate,
   format :'DD-MM-YYYY',
   lang: 'fr',
  cancelText: '<?php echo L::AnnulerBtn ?>',
  okText: 'OK',
  clearText: '<?php echo L::Eraser ?>',
  nowText: '<?php echo L::Now ?>'

 });

function selectStudent()
{
  var classe=$("#classeEtab").val();
  var codeEtab="<?php echo $codeEtabAssigner ?>";
  var session="<?php echo $libellesessionencours; ?>";
  var etape=3;

  $.ajax({
           url: '../ajax/admission.php',
           type: 'POST',
           async:false,
           data: 'codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&classe='+classe,
           dataType: 'text',
           success: function (response, statut) {


             $("#studentclasse").html("");
             $("#studentclasse").html(response);
           }
         });

}

   $('#example5').DataTable( {
       "scrollX": true

   } );
   $("#classeEtab").select2();
   $("#studentclasse").select2();
   $("#month").select2();
   $("#yearsession").select2();
   $('#datepre').bootstrapMaterialDatePicker
   ({
     date: true,
     time: false,
     format: 'DD/MM/YYYY',
     lang: 'fr',
     minDate : new Date(),
    cancelText: '<?php echo L::AnnulerBtn ?>',
    okText: 'OK',
    clearText: '<?php echo L::Eraser ?>',
    nowText: '<?php echo L::Now ?>'

   });
   $(document).ready(function() {

$("#FormSearch").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
 },
 highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
 },
 success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    classeEtab:"required",
    month:"required",
    yearsession:"required",
    datefin:"required",
    datedeb:"required",
    studentclasse:"required"



  },
  messages: {
    classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
    month:"<?php echo L::PleaseSelectAnMonth ?>",
    yearsession:"Merci de selection l'année de session",
    datefin:"<?php echo L::PleaseEnterDatedebLib ?>",
    datedeb:"<?php echo L::PleaseEnterDatefinLib ?>",
    studentclasse:"Merci de selectionner un eleve"

  },
  submitHandler: function(form) {
    form.submit();
  }
});

$('#print-chart-btn').on('click', function() {
 var canvas = document.querySelector("#chartjs_student");
 var canvas_img = canvas.toDataURL("image/png",1.0); //JPEG will not match background color
 var pdf = new jsPDF('landscape','in', 'letter'); //orientation, units, page size
 pdf.addImage(canvas_img, 'png', .5, 1.75, 10, 5); //image, type, padding left, padding top, width, height
 pdf.autoPrint(); //print window automatically opened with pdf
 var blob = pdf.output("bloburl");
 window.open(blob);
});

$('#print-chart-btn1').on('click', function() {
 var canvas = document.querySelector("#chartjs_classe");
 var canvas_img = canvas.toDataURL("image/png",1.0); //JPEG will not match background color
 var pdf = new jsPDF('landscape','in', 'letter'); //orientation, units, page size
 pdf.addImage(canvas_img, 'png', .5, 1.75, 10, 5); //image, type, padding left, padding top, width, height
 pdf.autoPrint(); //print window automatically opened with pdf
 var blob = pdf.output("bloburl");
 window.open(blob);
});


<?php
if(isset($_POST['studentclasse'])&& (strlen($_POST['studentclasse'])>0))
{
?>
var randomScalingFactor = function() {
      return Math.round(Math.random() * 100);
  };
var studentsName="<?php echo @$tabdatasutent[2]." ".@$tabdatasutent[3]; ?>";
  var config = {
      type: 'pie',
  data: {
      datasets: [{
          data: [
              <?php echo @$nbjourabsentstudent; ?>,
              <?php echo @$nbjourpresentstudent ?>,

          ],
          backgroundColor: [
              window.chartColors.red,
              window.chartColors.blue,
          ],
          label: 'Dataset 1'
      }],
      labels: [
          "Absent",
          "Present"

      ]
  },
  options: {
      responsive: true,
      title: {
       display: true,
       position:'bottom',
       text: 'Courbe des présences élève  '+studentsName
   }
  }
};

  var ctx = document.getElementById("chartjs_student").getContext("2d");
  window.myPie = new Chart(ctx, config);
<?php
}else {
  ?>
  var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var color = Chart.helpers.color;
    var barChartData = {
        labels: [ <?php
                          for($i=1;$i<=@$j;$i++)
                          {
                            echo '"'.@$tabStutentName[$i].'",';
                          }
          ?>],
        datasets: [{
            label: 'Absent',
            backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
            borderColor: window.chartColors.red,
            borderWidth: 1,
            data: [
              <?php
                                for($i=1;$i<=@$j;$i++)
                                {
                                  echo '"'.@$tabStudentAbsent[$i].'",';
                                }
                ?>
            ]
        }, {
            label: 'Present',
            backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
            borderColor: window.chartColors.blue,
            borderWidth: 1,
            data: [
              <?php
                                for($i=1;$i<=@$j;$i++)
                                {
                                  echo '"'.@$tabStudentPresent[$i].'",';
                                }
                ?>
            ]
        }]

    };

        var ctx = document.getElementById("chartjs_classe").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },
                title: {
                    display: true

                }
            }
        });
  <?php
}
 ?>



    //eleves


});


   </script>
    <!-- end js include path -->
  </body>

</html>
