<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Student.php');
require_once('../class/Teatcher.php');
require_once('../controller/functions.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$matiere=new Matiere();
$student=new Student();
$teatcher=new Teatcher();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);

$matieres=$matiere->getAllSubjectTeatchByTeatcherId($_SESSION['user']['IdCompte']);
$controles=$matiere->getAllControleMatiereOfThisTeatcherId($_SESSION['user']['IdCompte']);


//$dataclasses=$classe->getAllClassesByClasseId($classeschoolid);

$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
  $programmes=$etabs->getAllprogrammesOfTeatcherClasses($_SESSION['user']['IdCompte'],$libellesessionencours);

  // $fiches=$etabs->getAllFicesOfTeatcherClasses($_SESSION['user']['IdCompte'],$libellesessionencours);

$fiches=$etabs->getAllcahiersOfteatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);

}else {
  $nbclasse=0;
}

//la liste des cours de ce professeur

$courseid=$_GET['programmeid'];
$classeid=$_GET['classeid'];

// $courses=$etabs->getAllTeatchercourses($compteuserid,$codeEtabsession,$libellesessionencours);
$coursesdetails=$etabs->getSyllabusInfos($_GET['programmeid'],$_GET['teatcherid'],$codeEtabsession);
// $coursesdetails=$etabs->getAllcoursesdetails($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);

// var_dump($coursesdetails);


foreach ($coursesdetails as  $datacourses):
  $descricourses=$datacourses->descri_syllab;

  $teatchercourses=$datacourses->nom_compte;
  $teatchercoursescomplet=$datacourses->nom_compte." ".$datacourses->prenom_compte;
  $classecourses=$datacourses->libelle_classe;
  $classeidcourses=$datacourses->id_classe;
  $matiereidcourses=$datacourses->id_mat;
  $libellematcourses=$datacourses->libelle_mat;


endforeach;



// Nous allons recuperer les sections du cours

// $coursesSections=$etabs->getAllcoursesSection($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);
//
// $coursescompetences=$etabs->getAllcoursesComp($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);
//
// $courseshomeworks=$etabs->getAllcoursesHomeworks($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);
// var_dump($coursescompetences);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <style>
    #radioBtn .notActive{
    color: #3276b1;
    background-color: #fff;
    }

    #radioBtn1 .notActive{
    color: #3276b1;
    background-color: #fff;
    }
    </style>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title">Modification du syllabus</div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" <?php echo L::SyllabrMenu ?>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active">Modification du syllabus</li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addclasseok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['addclasseok'] ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addclasseok']);
                    }

                     ?>

                     <?php
                     if($nbsessionOn==0)
                     {
                       ?>
                       <div class="alert alert-danger alert-dismissible fade show" role="alert">

                       Vous devez definir la Sessionn scolaire

                       <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
                        </a>
                       </div>
                       <?php
                     }
                      ?>


                      <div class="row">
						<div class="col-md-12">
							<!-- BEGIN PROFILE SIDEBAR -->

							<!-- END BEGIN PROFILE SIDEBAR -->
							<!-- BEGIN PROFILE CONTENT -->
							<div class="profile-content">

                <div class="row">
                  <div class="col-md-12 col-lg-12">
    <div class="card card-topline-aqua">
      <div class="card-head">
        <header></header>
        <div class="tools">
          <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
          <!-- <a class="t-collapse btn-color fa fa-chevron-down"
            href="javascript:;"></a> -->
          <!-- <a class="t-close btn-color fa fa-times" href="javascript:;"></a> -->
        </div>
      </div>

      <div class="card-body ">
        <form class="" action="#" method="post">
          <div class="row">
            <div class="col-md-6">
             <div class="form-group">
  <label for="libellecourse">Année scolaire<span class="required">*</span> :</label>
        <input type="text" class="form-control" name="sessionEtab" id="sessionEtab" value="<?php echo $libellesessionencours; ?>" size="32" maxlength="225"  disabled="disabled"/>

                 </div>
              </div>
              <div class="col-md-6">
               <div class="form-group">
   <label for="libellecourse">Professeur<span class="required">*</span> :</label>
          <input type="text" class="form-control" name="profcompletname" id="profcompletname" value="<?php echo $teatchercoursescomplet; ?>" size="32" maxlength="225" disabled="disabled"/>

                   </div>
                </div>
                <div class="col-md-6">
                 <div class="form-group">
      <label for="libellecourse">Classe<span class="required">*</span> :</label>
            <input type="text" class="form-control" name="libelleclasse" id="libelleclasse" value="<?php echo $classecourses; ?>" size="32" maxlength="225" disabled="disabled" />

                     </div>
                  </div>
                  <div class="col-md-6">
                   <div class="form-group">
       <label for="libellecourse">Matière<span class="required">*</span> :</label>
              <input type="text" class="form-control" name="libellematiere" id="libellematiere" value="<?php echo $libellematcourses; ?>" size="32" maxlength="225"  disabled="disabled"/>

                       </div>
                    </div>



          </div>

         <span class="label label-lg label-warning"  style="text-align:center;">  DESCRIPTION DU SYLLABUS </span>

        <hr>
        <!-- <div class="col-md-12"> -->
<div class="form-group">

<textarea class="form-control descri" rows="5" name="descri" id="descri" placeholder="Entrer la description du cours" ><?php echo $descricourses; ?></textarea>

</div>
<span class="label label-lg label-warning"  style="text-align:center;">  OBJECTIFS DU SYLLABUS </span>

<hr>
<?php
  $objectifs=$etabs->getSyllabusObjectifsInfos($_GET['programmeid'],$_GET['teatcherid'],$codeEtabsession);

 ?>
 <div class="col-md-12">
   <button type="button" data-toggle="modal" data-target="#mediumModel" class="btn btn-primary pull-right" style="border-radius:5px;height:35px"> <i class="fa fa-plus"></i>Nouvelle objectif</button>
  </div></br></br>
 <div class="col-md-12">
 <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="">
   <thead>
       <tr>
         <th> N° </th>
         <th style="text-align:center"> OBJECTIFS </th>
         <th style="text-align:center"> ACTIONS </th>
       </tr>
   </thead>
   <tbody>
 <?php
 $i=1;
 foreach ($objectifs as  $value):
   ?>
   <tr>
     <td><?php echo $i; ?></td>
     <td style="text-align:center"><?php echo utf8_decode(utf8_encode($value->libelle_syllabob)) ?></td>
     <td style="text-align:center"><button type="button" onclick="#" class="btn btn-danger btn_remove">X</button></td>
   </tr>
   <?php
   $i++;
 endforeach;
  ?>
   </tbody>

 </table>

  </div>

 <span class="label label-lg label-warning"  style="text-align:center;">  CONTENU DU SYLLABUS </span>

 <hr>
 <?php
   $themes=$etabs->getSyllabusThemesInfos($_GET['programmeid'],$_GET['teatcherid'],$codeEtabsession);

  ?>
  <div class="col-md-12">
    <button type="button" data-toggle="modal" data-target="#mediumModel1" class="btn btn-primary pull-right" style="border-radius:5px;height:35px"> <i class="fa fa-plus"></i>Nouveau contenu</button>
   </div></br></br>

  <div class="col-md-12">
  <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="">
    <thead>
        <tr>
          <th> N° </th>
          <th style="text-align:center"> CONTENUS </th>
          <th style="text-align:center"> ACTIONS </th>
        </tr>
    </thead>
    <tbody>
  <?php
  $i=1;
  foreach ($themes as  $value):
    ?>
    <tr>
      <td><?php echo $i; ?></td>
      <td style="text-align:center"><?php echo utf8_decode(utf8_encode($value->libelle_syllabth)) ?></td>
      <td style="text-align:center"><button type="button" onclick="#" class="btn btn-danger btn_remove">X</button></td>
    </tr>
    <?php
    $i++;
  endforeach;
   ?>
    </tbody>

  </table>

   </div>

  <span class="label label-lg label-warning"  style="text-align:center;">  PREREQUIS NECESSAIRES DU SYLLABUS </span>

  <hr>
  <?php
    $Requis=$etabs->getSyllabusRequisInfos($_GET['programmeid'],$_GET['teatcherid'],$codeEtabsession);

   ?>
   <div class="col-md-12">
     <button type="button" data-toggle="modal" data-target="#mediumModel2" class="btn btn-primary pull-right" style="border-radius:5px;height:35px"> <i class="fa fa-plus"></i>Nouveau prerequis</button>
    </div></br></br>

   <div class="col-md-12">
   <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="">
     <thead>
         <tr>
           <th> N° </th>
           <th style="text-align:center"> PREREQUIS </th>
           <th style="text-align:center"> ACTIONS </th>
         </tr>
     </thead>
     <tbody>
   <?php
   $i=1;
   foreach ($Requis as  $value):
     ?>
     <tr>
       <td><?php echo $i; ?></td>
       <td style="text-align:center"><?php echo utf8_decode(utf8_encode($value->libelle_syllabreq)) ?></td>
       <td style="text-align:center"><button type="button" onclick="#" class="btn btn-danger btn_remove">X</button></td>
     </tr>
     <?php
     $i++;
   endforeach;
    ?>
     </tbody>

   </table>

    </div>

   <span class="label label-lg label-warning"  style="text-align:center;">  COMPETENCES VISEES DU SYLLABUS </span>

   <hr>
   <?php
      $Comp=$etabs->getSyllabusCompInfos($_GET['programmeid'],$_GET['teatcherid'],$codeEtabsession);

    ?>
    <div class="col-md-12">
      <button type="button" data-toggle="modal" data-target="#mediumModel3" class="btn btn-primary pull-right" style="border-radius:5px;height:35px"> <i class="fa fa-plus"></i>Nouvelle competence</button>
     </div></br></br>
    <div class="col-md-12">
    <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="">
      <thead>
          <tr>
            <th> N° </th>
            <th style="text-align:center"> COMPTENCES </th>
            <th style="text-align:center"> ACTIONS </th>
          </tr>
      </thead>
      <tbody>
    <?php
    $i=1;
    foreach ($Comp as  $value):
      ?>
      <tr>
        <td><?php echo $i; ?></td>
        <td style="text-align:center"><?php echo utf8_decode(utf8_encode($value->libelle_syllabcomp)) ?></td>
        <td style="text-align:center"><button type="button" onclick="#" class="btn btn-danger btn_remove">X</button></td>
      </tr>
      <?php
      $i++;
    endforeach;
     ?>
      </tbody>

    </table>

     </div>

     <span class="label label-lg label-warning"  style="text-align:center;">  DOCUMENTS DE COURS </span>

     <hr>
     <?php

          $Docs=$etabs->getSyllabusAllDocInfos($_GET['programmeid'],$_GET['teatcherid'],$codeEtabsession);

      ?>
      <div class="col-md-12">
      <button type="button" data-toggle="modal" data-target="#mediumModel4" class="btn btn-primary pull-right" style="border-radius:5px;height:35px"> <i class="fa fa-plus"></i>Nouveau document</button>
      </div></br></br>
      <div class="col-md-12">
      <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="">
        <thead>
            <tr>
              <th> N° </th>
              <th style="text-align:center"> DOCUMENTS </th>
              <th style="text-align:center"> TYPES </th>
              <th style="text-align:center"> ACTIONS </th>
            </tr>
        </thead>
        <tbody>
      <?php
      $i=1;
      foreach ($Docs as  $value):
        ?>
        <tr>
          <td><?php echo $i; ?></td>
          <td style="text-align:center"><?php echo utf8_decode(utf8_encode($value->libelle_syllabdoc)) ?></td>
          <td style="text-align:center"></td>
          <td style="text-align:center"><button type="button" onclick="#" class="btn btn-danger btn_remove">X</button></td>
        </tr>
        <?php
        $i++;
      endforeach;
       ?>
        </tbody>

      </table>

       </div>

       <span class="label label-lg label-warning"  style="text-align:center;">  CALENDRIER DES COURS </span>

       <hr>
       <?php

           $Calendars=$etabs->getSyllabusCalendarInfos($_GET['programmeid'],$_GET['teatcherid'],$codeEtabsession);

        ?>
        <div class="col-md-12">
        <button type="button" data-toggle="modal" data-target="#mediumModel5" class="btn btn-primary pull-right" style="border-radius:5px;height:35px"> <i class="fa fa-plus"></i>Nouveau calendrier</button>
        </div></br></br>
        <div class="col-md-12">
        <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="">
          <thead>
              <tr>
                <th> N° </th>
                <th style="text-align:center;">Date</th>
               <th style="text-align:center;">No Séance </th>
                <th style="text-align:center"> ACTIONS </th>
              </tr>
          </thead>
          <tbody>
        <?php
        $i=1;
        foreach ($Calendars as  $value):
          ?>
          <tr>
            <td><?php echo $i; ?></td>
          <td style="text-align:center;"><?php echo $value->date_syllabcal?></td>
            <td style="text-align:center"></td>
            <td style="text-align:center"><button type="button" onclick="#" class="btn btn-danger btn_remove">X</button></td>
          </tr>
          <?php
          $i++;
        endforeach;
         ?>
          </tbody>

        </table>

         </div>

         <span class="label label-lg label-warning"  style="text-align:center;">  REGLES DE FONCTIONNEMENT DU COURS </span>

         <hr>
         <?php

             $Regles=$etabs->getSyllabusRegleInfos($_GET['programmeid'],$_GET['teatcherid'],$codeEtabsession);

          ?>
          <div class="col-md-12">
          <button type="button" data-toggle="modal" data-target="#mediumModel6" class="btn btn-primary pull-right" style="border-radius:5px;height:35px"> <i class="fa fa-plus"></i>Nouvelle regle</button>
          </div></br></br>
          <div class="col-md-12">
          <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="">
            <thead>
                <tr>
                  <th> N° </th>

                 <th style="text-align:center;">REGLES </th>
                  <th style="text-align:center"> ACTIONS </th>
                </tr>
            </thead>
            <tbody>
          <?php
          $i=1;
          foreach ($Regles as  $value):
            ?>
            <tr>
              <td><?php echo $i; ?></td>
            <td style="text-align:center;"><?php echo $value->libelle_syllabregle?></td>

              <td style="text-align:center"><button type="button" onclick="#" class="btn btn-danger btn_remove">X</button></td>
            </tr>
            <?php
            $i++;
          endforeach;
           ?>
            </tbody>

          </table>

           </div>


</form>
      </div>
    </div>
  </div>









                </div>


							</div>






							<!-- END PROFILE CONTENT -->
						</div>
					</div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
   	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
       <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
    <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
    <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
    <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <!--script src="../assets2/plugins/summernote/summernote.js" ></script>
    <script src="../assets2/plugins/summernote/lang/summernote-fr-FR.js" ></script-->
       <!-- calendar -->
       <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
    <script src="../assets2/plugins/moment/moment.min.js" ></script>
    <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
    <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
       <!-- Common js-->
   	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
   	<script src="../assets2/js/theme-color.js" ></script>
   	<!-- Material -->
   	<script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
    <script src="../assets2/js/dropify.js"></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>

 <script>
 /*$('#summernote').summernote({
     placeholder: '',
     tabsize: 2,
     height: 200,
      lang: 'fr-FR'
   });*/

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

function publicationcourse(courseid,classeid,teatcherid,codeEtab,sessionEtab,matiereid)
{
  Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::DoyouPublishcourse ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::Published ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {

  var etape=1;

  $.ajax({

       url: '../ajax/courses.php',
       type: 'POST',
       async:true,
       data: 'teatcherid=' + teatcherid+ '&etape=' + etape+'&classe='+classeid+'&matiere='+matiereid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&courseid='+courseid,
       dataType: 'text',
       success: function (content, statut) {

          // $("#FormAddAcademique #codeEtab").val(content);

          location.reload();

       }
     });

}else {

}
})
}

 $("#fichier3").dropify({
   messages: {
       "default": "Merci de selectionner le support",
       "replace": "Modifier le support",
       "remove" : "Supprimer le support",
       "error"  : "Erreur"
   }
 });

$("#matclasse").select2();
$("#classeEtab").select2();

function searchcodeEtab(id)
{
var classe=$("#classeEtab").val();
var teatcherId=id;
var etape=7;
var matiere=$("#matclasse").val();

$.ajax({

     url: '../ajax/matiere.php',
     type: 'POST',
     async:true,
     data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
     dataType: 'text',
     success: function (content, statut) {

        $("#FormAddAcademique #codeEtab").val(content);

     }
   });

}

function searchmatiere(id)
{

  var classe=$("#classeEtab").val();
  var teatcherId=id;
  var etape=6;


$.ajax({

     url: '../ajax/matiere.php',
     type: 'POST',
     async:true,
     data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe,
     dataType: 'text',
     success: function (content, statut) {


       $("#matclasse").html("");
       $("#matclasse").html(content);

     }
   });
}

function deletedTache(id)
{
var concattache=$("#concattache").val();

$("#concattache").val($("#concattache").val().replace(id+"@", ""));

 $('#rowTache'+id+'').remove();

 recalcultachenb();
}

function deletedComp(id)
{
var concatcomp=$("#concatcomp").val();

$("#concatcomp").val($("#concatcomp").val().replace(id+"@", ""));

 $('#rowComp'+id+'').remove();

 // recalculsectionnb();

 var concatcomp=$("#concatcomp").val();

 var tab=concatcomp.split("@");

 var nbtab=tab.length;

 var nbtabnew=parseInt(nbtab)-1;

 $("#concatnbcomp").val(nbtabnew);

}

function deletedTaches(id)
{
var concattaches=$("#concattaches").val();

$("#concattaches").val($("#concattaches").val().replace(id+"@", ""));

 $('#rowTaches'+id+'').remove();

 // recalculsectionnb();

 var concattaches=$("#concattaches").val();

 var tab=concattaches.split("@");

 var nbtab=tab.length;

 var nbtabnew=parseInt(nbtab)-1;

 $("#concatnbtaches").val(nbtabnew);

}


function deletedSection(id)
{
var concatsection=$("#concatsection").val();

$("#concatsection").val($("#concatsection").val().replace(id+"@", ""));

 $('#rowSection'+id+'').remove();

 // recalculsectionnb();

 var concatsection=$("#concatsection").val();

 var tab=concatsection.split("@");

 var nbtab=tab.length;

 var nbtabnew=parseInt(nbtab)-1;

 $("#concatnbsection").val(nbtabnew);

}

function recalcultachenb()
{
var concattache=$("#concattache").val();

var tab=concattache.split("@");

var nbtab=tab.length;

var nbtabnew=parseInt(nbtab)-1;

$("#concatnbtache").val(nbtabnew);
}

function recalculsectionnb()
{

}


function AddtachesRow()
{
var nb=$("#nbtaches").val();
var nouveau= parseInt(nb)+1;
$("#nbtaches").val(nouveau);

  var concattaches=$("#concattaches").val();
  $("#concattaches").val(concattaches+nouveau+"@");

  var concattaches=$("#concattaches").val();

  var tab=concattaches.split("@");

  var nbtab=tab.length;

  var nbtabnew=parseInt(nbtab)-1;

  $("#concatnbtaches").val(nbtabnew);

  // recalculsectionnb();

  if(nbtabnew==1)
  {
    $('#dynamic_field2').append('<tr id="rowTaches'+nouveau+'"><td><input type="text" name="taches_'+nouveau+'" id="taches_'+nouveau+'" placeholder="<?php echo L::EnterNewExercice ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteTaches'+nouveau+'" id="deleteTaches'+nouveau+'"  onclick="deletedTaches('+nouveau+')"  class="btn btn-danger btn_remove" disabled="disabled">X</button></td></tr>');
  }else {
    $('#dynamic_field2').append('<tr id="rowTaches'+nouveau+'"><td><input type="text" name="taches_'+nouveau+'" id="taches_'+nouveau+'" placeholder="<?php echo L::EnterNewExercice ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteTaches'+nouveau+'" id="deleteTaches'+nouveau+'"  onclick="deletedTaches('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');
  }



  for(var i=0;i<nbtabnew;i++)
  {
    var indice=tab[i];
    // alert(indice);
    $("#taches_"+indice).rules( "add", {
        required: true,
        messages: {
        required: "<?php echo L::AddNewexerciceplease ?>"
}
      });
  }


}

function AddcompRow()
{
var nb=$("#nbcomp").val();
var nouveau= parseInt(nb)+1;
$("#nbcomp").val(nouveau);

  var concatcomp=$("#concatcomp").val();
  $("#concatcomp").val(concatcomp+nouveau+"@");

  var concatcomp=$("#concatcomp").val();

  var tab=concatcomp.split("@");

  var nbtab=tab.length;

  var nbtabnew=parseInt(nbtab)-1;

  $("#concatnbcomp").val(nbtabnew);

  // recalculsectionnb();

  if(nbtabnew==1)
  {
    $('#dynamic_field1').append('<tr id="rowComp'+nouveau+'"><td><input type="text" name="comp_'+nouveau+'" id="comp_'+nouveau+'" placeholder="<?php echo L::AddCompViseeRequired ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteComp'+nouveau+'" id="deleteComp'+nouveau+'"  onclick="deletedComp('+nouveau+')"  class="btn btn-danger btn_remove" disabled="disabled">X</button></td></tr>');
  }else {
    $('#dynamic_field1').append('<tr id="rowComp'+nouveau+'"><td><input type="text" name="comp_'+nouveau+'" id="comp_'+nouveau+'" placeholder="<?php echo L::AddCompViseeRequired ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteComp'+nouveau+'" id="deleteComp'+nouveau+'"  onclick="deletedComp('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');
  }



  for(var i=0;i<nbtabnew;i++)
  {
    var indice=tab[i];
    // alert(indice);
    $("#comp_"+indice).rules( "add", {
        required: true,
        messages: {
        required: "<?php echo L::AddCompetenceViseeplease ?>"
}
      });
  }


}

function AddsectionRow()
{
var nb=$("#nb").val();
var nouveau= parseInt(nb)+1;
$("#nb").val(nouveau);

  var concatsection=$("#concatsection").val();
  $("#concatsection").val(concatsection+nouveau+"@");

  var concatsection=$("#concatsection").val();

  var tab=concatsection.split("@");

  var nbtab=tab.length;

  var nbtabnew=parseInt(nbtab)-1;

  $("#concatnbsection").val(nbtabnew);

  // recalculsectionnb();

  if(nbtabnew==1)
  {
    $('#dynamic_field').append('<tr id="rowSection'+nouveau+'"><td><input type="text" name="section_'+nouveau+'" id="section_'+nouveau+'" placeholder="<?php echo L::EnterSection ?>" class="form-control objectif_list" /></td><td><button type="button" id="deleteSection'+nouveau+'" id="deleteSection'+nouveau+'"  onclick="deletedSection('+nouveau+')"  class="btn btn-danger btn_remove" disabled="disabled">X</button></td></tr>');
  }else {
    $('#dynamic_field').append('<tr id="rowSection'+nouveau+'"><td><input type="text" name="section_'+nouveau+'" id="section_'+nouveau+'" placeholder="<?php echo L::EnterSection ?>" class="form-control objectif_list" /></td><td><button type="button" id="deleteSection'+nouveau+'" id="deleteSection'+nouveau+'"  onclick="deletedSection('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');
  }



  for(var i=0;i<nbtabnew;i++)
  {
    var indice=tab[i];
    // alert(indice);
    $("#section_"+indice).rules( "add", {
        required: true,
        messages: {
        required: "<?php echo L::AddSectionplease ?>"
}
      });
  }


}

function AddtacheRow()
{
var nb=$("#nbtache").val();
var nouveau= parseInt(nb)+1;
$("#nbtache").val(nouveau);

  var concattache=$("#concattache").val();
  $("#concattache").val(concattache+nouveau+"@");

  recalcultachenb();

  $('#dynamic_field1').append('<tr id="rowTache'+nouveau+'"><td><input type="text" name="tache_'+nouveau+'" id="tache_'+nouveau+'" placeholder="Entrer une tache" class="form-control objectif_list" /></td><td><button type="button" id="deleteTache'+nouveau+'" id="deleteTache'+nouveau+'"  onclick="deletedTache('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');

  for(var i=1;i<=nouveau;i++)
  {
    $("#tache_"+i).rules( "add", {
        required: true,
        messages: {
        required: "<?php echo L::RequiredChamp ?>"
}
      });
  }


}

 $(document).ready(function() {

   $("#FormAddAcademique").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
    },
    highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
    },
    success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
     rules:{

       programme:"required",
       descri:"required",
       classeEtab:"required",
       matclasse:"required",
       fichier:"required",
       durationcourse:"required",
       detailscourse:"required",
       datecourse:"required"

     },
   messages: {
     programme:"Merci de renseigner le libellé du programme",
     descri:"Merci de renseigner le détails de la fiche ",
     classeEtab:"<?php echo L::PleaseSelectclasseOnerequired ?>",
     matclasse:"Merci de <?php echo L::SelectSubjects ?>",
     fichier:"Merci de selectionner le fichier du programme",
     durationcourse:"<?php echo L::DurationcourseRequired ?>",
     detailscourse:"<?php echo L::DetailscourseRequired ?>",
     datecourse:"<?php echo L::DatecourseRequired ?>"
   },
   submitHandler: function(form) {

   form.submit();



   }

   });

   // AddsectionRow();
   //  AddcompRow();

   $('#add').click(function(){

     //creation d'une ligne de section

     AddsectionRow();

   });

   $('#addcomp').click(function(){

     //creation d'une ligne de section

     AddcompRow();

   });

   $('#addtache').click(function(){

     //creation d'une ligne de section

     AddtachesRow();

   });






 });

 </script>
    <!-- end js include path -->
  </body>

</html>
