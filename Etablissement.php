<?php

class Etab{

  public $db;
  function __construct() {
    require_once('cnx.php');
    $db = new mysqlConnector();
    $this->db= $db->dataBase;
      }

      function DetermineScolarityStateOfStudent($codeEtabAssigner,$libellesessionencours,$idcompte)
      {
        $req = $this->db->prepare("SELECT * FROM versement where codeEtab_versement=? and session_versement=? and ideleve_versement=? order by versement.id_versement DESC");
        $req->execute([$codeEtabAssigner,$libellesessionencours,$idcompte]);
        return $req->fetchAll();
      }

      function UpdateSolderetardNotificationWithoutClasses($titre,$message,$codeEtab,$libellesession,$smsvalue,$emailvalue,$soldeid,$notifid)
      {
        // $req = $this->db->prepare("UPDATE notification set libelle_notif=? where id_notif=? ");
        // $req->execute([$titre,$notifid]);


        $req = $this->db->prepare("UPDATE notification set libelle_notif=?,message_notif=?,sms_notif=?,email_notif=? where id_notif=? and session_notif=? and codeEtab_notif=?");
        $req->execute([$titre,$message,$smsvalue,$emailvalue,$notifid,$libellesession,$codeEtab]);

        $req1 = $this->db->prepare("UPDATE soldenotification set libelle_soldenot=? where 	id_soldenot=? and codeEtab_soldenot=? and session_soldenot=? and idnotif_soldenot=?");
        $req1->execute([$titre,$soldeid,$codeEtab,$libellesession,$notifid]);

        $_SESSION['user']['Updateadminok']="Notification Solde modifié avec succès";
        if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/soldenotifications.php");

          }
      }

      function UpdateSolderetardNotificationWithClasses($titre,$message,$codeEtab,$libellesession,$smsvalue,$emailvalue,$classes,$soldeid,$notifid)
      {
        $req = $this->db->prepare("UPDATE notification set libelle_notif=?,message_notif=?,sms_notif=?,email_notif=?,	classes_notif=? where id_notif=? and session_notif=? and codeEtab_notif=?");
        $req->execute([$titre,$message,$smsvalue,$emailvalue,$classes,$notifid,$libellesession,$codeEtab]);

        $req1 = $this->db->prepare("UPDATE soldenotification set libelle_soldenot=?,classes_soldenot=? where 	id_soldenot=? and codeEtab_soldenot=? and session_soldenot=? and idnotif_soldenot=?");
        $req1->execute([$titre,$classes,$soldeid,$codeEtab,$libellesession,$notifid]);

        $_SESSION['user']['Updateadminok']="Notification Solde modifié avec succès";
        if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/soldenotifications.php");

          }

      }

      function getsSoldesScolairesOfThisSchool($codeEtabLocal,$libellesessionencours,$paraid)
      {
        $req = $this->db->prepare("SELECT * FROM soldenotification,notification where soldenotification.idnotif_soldenot=notification.id_notif  and soldenotification.codeEtab_soldenot=? and soldenotification.session_soldenot=? and soldenotification.id_soldenot=?");
        $req->execute([$codeEtabLocal,$libellesessionencours,$paraid]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["libelle_soldenot"]."*".$someArray[0]["message_notif"]."*".$someArray[0]["classes_notif"]."*".$someArray[0]["destinataires_notif"];
         $donnees.="*".$someArray[0]["sms_notif"]."*".$someArray[0]["email_notif"]."*".$someArray[0]["id_notif"]."*".$someArray[0]["id_soldenot"];
        // $donnees.="*".$someArray[0]["classes_notif"]."*".$someArray[0]["destinataires_notif"]."*".$someArray[0]["payant_para"];
        // $donnees.="*".$someArray[0]["montant_para"]."*".$someArray[0]["sms_notif"]."*".$someArray[0]["email_notif"]."*".$someArray[0]["date_notif"]."*".$someArray[0]["id_notif"]."*".$someArray[0]["date_notif"];
        return $donnees;
      }

      function DeleteNotificationScolarite($scolaid,$notifid,$statusNotification,$statutsolde)
      {
        $req = $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=?");
        $req->execute([$statusNotification,$notifid]);

        $req1 = $this->db->prepare("UPDATE soldenotification set statut_soldenot=? where idnotif_soldenot=? and id_soldenot=?");
        $req1->execute([$statutsolde,$notifid,$scolaid]);

        $_SESSION['user']['Updateadminok']="Notification supprimer avec succès";
        if($_SESSION['user']['profile'] == "Admin_globale") {

              //header("Location:../manager/index.php");
              header("Location:../manager/messages.php");

          }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/messages.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/messages.php");

              }
      }

      function ArchivedScolaritesoldeScolar($soldeid,$notifid,$session,$codeEtab,$notificationstatus,$notificationsoldesatatus)
      {
        $req= $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$notificationstatus,$notifid,$codeEtab]);

        $reqX= $this->db->prepare("UPDATE  soldenotification set statut_soldenot=? where idnotif_soldenot=? and 	codeEtab_soldenot=? and id_soldenot=?");
        $reqX->execute([$notificationsoldesatatus,$notifid,$codeEtab,$soldeid]);

        $_SESSION['user']['Updateadminok']="Notification Solde archivé avec succès";
        if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/soldenotifications.php");

          }
      }

      function ArchivedScolaritesolde($scolarid,$notifid,$session,$codeEtab,$notificationstatus,$notificationsoldesatatus)
      {
        $req= $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$notificationstatus,$notifid,$codeEtab]);

        $reqX= $this->db->prepare("UPDATE  soldenotification set statut_soldenot=? where idnotif_soldenot=? and 	codeEtab_soldenot=? and id_soldenot=?");
        $reqX->execute([$notificationsoldesatatus,$notifid,$codeEtab,$scolarid]);

        $_SESSION['user']['Updateadminok']="Notification Solde archivé avec succès";
        if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/messages.php");

          }
      }

      function ArchivedParascolaireActivity($paraid,$notifid,$session,$codeEtab,$notificationstatut,$parascolairestatut)
      {
        $req= $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$notificationstatut,$notifid,$codeEtab]);

        $reqX= $this->db->prepare("UPDATE parascolaires set statut_para=? where idnotif_para=? and 	codeEtab_para=? and id_para=?");
        $reqX->execute([$parascolairestatut,$notifid,$codeEtab,$paraid]);

        $_SESSION['user']['updateExamok']="Activité parascolaire archivé avec succès";
        if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/parascolaires.php");

          }

      }

      function UpdateNotificationStatusAndParascolairesParascolar($notifid,$notificationsStatus,$codeEtab,$paraid,$parascolaireStatus)
      {
        $req= $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$notificationsStatus,$notifid,$codeEtab]);

        $reqX= $this->db->prepare("UPDATE  soldenotification set statut_soldenot=? where 	idnotif_soldenot=? and 	codeEtab_soldenot=? and id_soldenot=?");
        $reqX->execute([$parascolaireStatus,$notifid,$codeEtab,$paraid]);

      }

      function UpdateNotificationStatusAndParascolairesPara($notifid,$notificationsStatus,$codeEtab,$paraid,$parascolaireStatus)
      {
        $req= $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$notificationsStatus,$notifid,$codeEtab]);

        $reqX= $this->db->prepare("UPDATE parascolaires set statut_para=? where 	idnotif_para=? and 	codeEtab_para=? and id_para=?");
        $reqX->execute([$parascolaireStatus,$notifid,$codeEtab,$paraid]);

      }

      function UpdateNotificationStatusParasclar($notifid,$notificationsStatus,$codeEtab)
      {
        $req= $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$notificationsStatus,$notifid,$codeEtab]);
      }

      function UpdateNotificationStatusPara($notifid,$notificationsStatus,$codeEtab)
      {
        $req= $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$notificationsStatus,$notifid,$codeEtab]);
      }

      function  DetermineAllScolairesNoitications($codeEtabLocal,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * from soldenotification,notification where soldenotification.idnotif_soldenot=notification.id_notif and soldenotification.codeEtab_soldenot=? and soldenotification.session_soldenot=? and soldenotification.statut_soldenot not in (3,4) order by soldenotification.id_soldenot DESC	");
        $req->execute([$codeEtabLocal,$libellesessionencours]);
        return $req->fetchAll();
      }

      function AddSoldeNotification($titre,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$paranotif,$libellesession,$scolanotif,$idscolanotifications)
      {
        $req = $this->db->prepare("INSERT INTO soldenotification SET libelle_soldenot=?,codeEtab_soldenot=?,session_soldenot=?,classes_soldenot=?,idnotif_soldenot=?,statut_soldenot=?");
        $req->execute([$titre,$codeEtab,$libellesession,$classes,$idscolanotifications,$scolanotif]);
        $_SESSION['user']['addctrleok']="Notification ajouté avec succès";

        if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/soldenotifications.php");

          }
      }

    function  AddNotificationScola($titre,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$paranotif,$libellesession,$scolanotif)
      {
        $req = $this->db->prepare("INSERT INTO notification SET libelle_notif=?,destinataires_notif=?,classes_notif=?,message_notif=?,join_notif=?,statut_notif=?,date_notif=?,codeEtab_notif=?,sms_notif=?,email_notif=?,para_notif=?,session_notif=?,scola_notif=?");
        $req->execute([$titre,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$paranotif,$libellesession,$scolanotif]);
        $idnotif=$this->db->lastInsertId();
        // return $idlastcompte;

        $reqX = $this->db->prepare("INSERT INTO soldenotification SET libelle_soldenot=?,codeEtab_soldenot=?,session_soldenot=?,classes_soldenot=?,idnotif_soldenot=?,statut_soldenot=?");
        $reqX->execute([$titre,$codeEtab,$libellesession,$classes,$idnotif,$scolanotif]);
        $_SESSION['user']['addctrleok']="Notification ajouté avec succès";

        if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/soldenotifications.php");

          }

      }

      function DetermineAllversements($codeEtabLocal,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * from versement where versement.codeEtab_versement=? and versement.session_versement=?");
        $req->execute([$codeEtabLocal,$libellesessionencours]);
        return $req->fetchAll();
      }

      function getCountrydeviseByCodeEtab($codeEtabAssigner)
      {
          $req = $this->db->prepare("SELECT * from etablissement,pays where pays.id_pays=etablissement.pays_etab and etablissement.code_etab=?");
          $req->execute([$codeEtabAssigner]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["devises_pays"];
          return $donnees;
      }

      function getExamenNotesWithoutLibctrl($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession,$libsemes)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,examen,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and examen.session_exam=classe.session_classe and
          notes.idclasse_notes=classe.id_classe and notes.idtype_notes=examen.id_exam and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=examen.session_exam and notes.ideleve_notes=? and notes.idclasse_notes=?
          and notes.idmat_notes=? and examen.typesess_exam=? and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=?  ");

          $req->execute([$idcompte,$classe,$matclasse,$libsemes,$notetype,$codeEtab,$libellesession]);
          return $req->fetchAll();
      }

      function getControleNotesWithoutLibctrl($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession,$libsemes)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
          and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
          and notes.idmat_notes=? and controle.typesess_ctrl=? and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=?  ");

          $req->execute([$idcompte,$classe,$matclasse,$libsemes,$notetype,$codeEtab,$libellesession]);
          return $req->fetchAll();

      }

      function getNoteStudentAllByTypeAndSessionExam($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession,$libsemes)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,examen,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and examen.session_exam=classe.session_classe and
          notes.idclasse_notes=classe.id_classe and notes.idtype_notes=examen.id_exam and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=examen.session_exam and notes.ideleve_notes=? and notes.idtype_notes in (?) and notes.type_notes=? and notes.idmat_notes=? and notes.idclasse_notes=?
          and notes.codeEtab_notes=? and notes.session_notes=? and examen.typesess_exam=?");
          $req->execute([$idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession,$libsemes]);
          return $req->fetchAll();
      }

      function getPreciseNotesExamenStudent($idcompte,$examenid,$notetype,$teatcherid,$matclasse,$classe,$codeEtab,$libellesession,$libsemes)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,examen,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and examen.session_exam=classe.session_classe and
          notes.idclasse_notes=classe.id_classe and notes.idtype_notes=examen.id_exam and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=examen.session_exam and notes.ideleve_notes=? and notes.idtype_notes and notes.type_notes=? and notes.idprof_notes=?
          and notes.idmat_notes=? and notes.idclasse_notes=? and notes.codeEtab_notes=? and notes.session_notes=? and examen.typesess_exam=?  ");
          $req->execute([$idcompte,$examenid,$notetype,$teatcherid,$matclasse,$classe,$codeEtab,$libellesession,$libsemes]);
          return $req->fetchAll();
      }

      function getNoteStudentAllByTypeAndSession($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession,$libsemes)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
          and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl
           and notes.idtype_notes=controle.id_ctrl and notes.ideleve_notes=? and notes.idtype_notes in (?) and notes.type_notes=? and notes.idmat_notes=? and notes.idclasse_notes=? and notes.codeEtab_notes=? and notes.session_notes=? and controle.typesess_ctrl=? ");
           $req->execute([$idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession,$libsemes]);



           return $req->fetchAll();
      }

      function getPreciseNotesControleStudent($idcompte,$controleid,$notetype,$teatcherid,$matclasse,$classe,$codeEtab,$libellesession,$libsemes)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
          and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl
           and notes.idtype_notes=controle.id_ctrl and notes.ideleve_notes=? and notes.idtype_notes=? and notes.type_notes=? and notes.idprof_notes=? and notes.idmat_notes=? and notes.idclasse_notes=? and notes.codeEtab_notes=? and notes.session_notes=? and controle.typesess_ctrl=?");
          $req->execute([$idcompte,$controleid,$notetype,$teatcherid,$matclasse,$classe,$codeEtab,$libellesession,$libsemes]);
          return $req->fetchAll();

      }

      function getStudentExam($idcompte,$classe,$matiere,$semestre,$notetype,$codeEtab,$session)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,examen,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and examen.session_exam=classe.session_classe and
          notes.idclasse_notes=classe.id_classe and notes.idtype_notes=examen.id_exam and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=examen.session_exam and notes.ideleve_notes=? and notes.idclasse_notes=?
          and notes.idmat_notes=? and examen.typesess_exam=? and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=?  ");

          $req->execute([$idcompte,$classe,$matiere,$semestre,$notetype,$codeEtab,$session]);
          return $req->fetchAll();
      }

      function getNumberOfNotesExamStudentByClassesIdAndSubject($idcompte,$classe,$matiere,$semestre,$notetype,$codeEtab,$session)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,examen,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and examen.session_exam=classe.session_classe and
          notes.idclasse_notes=classe.id_classe and notes.idtype_notes=examen.id_exam and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=examen.session_exam and notes.ideleve_notes=? and notes.idclasse_notes=?
          and notes.idmat_notes=? and examen.typesess_exam=? and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=?  ");

          $req->execute([$idcompte,$classe,$matiere,$semestre,$notetype,$codeEtab,$session]);
          $data=$req->fetchAll();
          $nb=count($data);

          return $nb;
      }

      function getStudentControle($idcompte,$classe,$matiere,$semestre,$notetype,$codeEtab,$session)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
          and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
          and notes.idmat_notes=? and controle.typesess_ctrl=? and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=?   ");

          $req->execute([$idcompte,$classe,$matiere,$semestre,$notetype,$codeEtab,$session]);
          return $req->fetchAll();

      }

      function getNumberOfNotesControleStudentByClassesIdAndSubject($idcompte,$classe,$matiere,$semestre,$notetype,$codeEtab,$session)
      {
          $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
            and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and  notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl
            and notes.ideleve_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and controle.typesess_ctrl=? and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=?   ");

            $req->execute([$idcompte,$classe,$matiere,$semestre,$notetype,$codeEtab,$session]);
            $data=$req->fetchAll();
            $nb=count($data);

            return $nb;
      }

      function getLessonsbyMatiereidOnly($matiereid,$lessonclasse,$lessonEtab,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT distinct fiche.mat_fiche  FROM fiche,sousfiche where fiche.id_fiche=sousfiche.idfic_sousfic and fiche.mat_fiche=? and fiche.classe_fiche=? and fiche.codeEtab_fiche=? and fiche.session_fiche=? ");
        $req->execute([$matiereid,$lessonclasse,$lessonEtab,$libellesessionencours]);
        return $req->fetchAll();
      }

      function getLessonsbyMatiereidAndPeriode($classeid,$codeEtab,$libellesession,$matclasse,$datedu,$dateau)
      {
            $req = $this->db->prepare("SELECT distinct fiche.mat_fiche FROM fiche,sousfiche,classe,matiere where fiche.classe_fiche=classe.id_classe and fiche.id_fiche=sousfiche.idfic_sousfic and fiche.mat_fiche=matiere.id_mat and fiche.classe_fiche=? and fiche.codeEtab_fiche=? and fiche.session_fiche=? and fiche.mat_fiche=? and (fiche.date_fiche>=? and fiche.date_fiche<=?)  ");
            $req->execute([$classeid,$codeEtab,$libellesession,$matclasse,$datedu,$dateau]);
            return $req->fetchAll();
      }

      function getLessonsbyMatiere($matiereid,$lessonclasse,$lessonEtab,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT *  FROM fiche,sousfiche where fiche.id_fiche=sousfiche.idfic_sousfic and fiche.mat_fiche=? and fiche.classe_fiche=? and fiche.codeEtab_fiche=? and fiche.session_fiche=? ");
        $req->execute([$matiereid,$lessonclasse,$lessonEtab,$libellesessionencours]);
        return $req->fetchAll();
      }

      function getSpecificMatieresOfProgrammesClasses($lessonclasse,$lessonEtab,$libellesessionencours)
      {
          $req = $this->db->prepare("SELECT distinct fiche.mat_fiche FROM fiche,sousfiche,classe,matiere where fiche.classe_fiche=classe.id_classe and fiche.id_fiche=sousfiche.idfic_sousfic and fiche.mat_fiche=matiere.id_mat and classe.id_classe=? and classe.codeEtab_classe=? and classe.session_classe=?");
          $req->execute([$lessonclasse,$lessonEtab,$libellesessionencours]);
          return $req->fetchAll();
      }

      function getAllprogrammesOfTeatcherSession($teatcherid,$codeEtab,$libellesessionencours)
      {
        //$req = $this->db->prepare("SELECT * FROM programme where idprof_prog=? and codeEtab_prog=? and session_prog=?");
        $req = $this->db->prepare("SELECT * FROM programme,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and programme.idprof_prog=enseignant.idcompte_enseignant and programme.idmat_prog=matiere.id_mat and programme.idclasse_prog=classe.id_classe and enseignant.idcompte_enseignant=? and classe.codeEtab_classe=? and  programme.session_prog=? order by programme.id_prog ASC");
        $req->execute([$teatcherid,$codeEtab,$libellesessionencours]);
        return $req->fetchAll();
      }

      function ActivateNotificationState($idcompte,$session,$statut,$codeEtab)
      {
          $req= $this->db->prepare("UPDATE notificationstate set status_state=? where idparent_state=? and session_state=?");
          $req->execute([$statut,$idcompte,$session]);
          $_SESSION['user']['Updateadminok']="Notification Activé avec succès pour ce compte";

          if($_SESSION['user']['profile'] == "Admin_globale") {

                //header("Location:../manager/index.php");
                header("Location:../manager/souscriptions.php?codeEtab=".$codeEtab);

            }else if($_SESSION['user']['profile'] == "Admin_locale") {

              header("Location:../locale/souscriptions.php?codeEtab=".$codeEtab);

            }else if($_SESSION['user']['profile'] == "Teatcher") {

                header("Location:../teatcher/souscriptions.php?codeEtab=".$codeEtab);

                }

      }

      function desActivateNotificationState($idcompte,$session,$statut,$codeEtab)
      {
        $req= $this->db->prepare("UPDATE notificationstate set status_state=? where idparent_state=? and session_state=?");
        $req->execute([$statut,$idcompte,$session]);
        $_SESSION['user']['Updateadminok']="Notification desactivé avec succès pour ce compte";

        if($_SESSION['user']['profile'] == "Admin_globale") {

              //header("Location:../manager/index.php");
              header("Location:../manager/souscriptions.php?codeEtab=".$codeEtab);

          }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/souscriptions.php?codeEtab=".$codeEtab);

          }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/souscriptions.php?codeEtab=".$codeEtab);

              }

      }

      function getNotificationStateParent($idcompte,$libellesessionencours)
      {
          $req = $this->db->prepare("SELECT status_state FROM notificationstate where idparent_state=? and session_state=?");
          $req->execute([$idcompte,$libellesessionencours]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["status_state"];
          return $donnees;
      }

      function UpdateNotificationStatusAndParascolaires($notifid,$notificationsStatus,$codeEtab,$paraid,$parascolaireStatus)
      {
        $req= $this->db->prepare("UPDATE notification SET statut_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$notificationsStatus,$notifid,$codeEtab]);

        $req1= $this->db->prepare("UPDATE parascolaires SET statut_para=? where idnotif_para=? and id_para=? and codeEtab_para=?");
        $req1->execute([$parascolaireStatus,$notifid,$paraid,$codeEtab]);
      }

      function NotificationAndParascolairesInfosUpdateWithOutPaieWithoutFile($libelactivity,$typesess,$datedeb,$datefin,$codeEtab,$libellesession,$paraid,$message,$montantAct,$smsvalue,$emailvalue,$notifid,$statutFile)
      {
        $payant=0;
        $req= $this->db->prepare("UPDATE parascolaires set libelle_para=?,debut_para=?,fin_para=?,payant_para=?,montant_para=? where id_para=? and idnotif_para=? and codeEtab_para=? and session_para=?");
        $req->execute([$libelactivity,$datedeb,$datefin,$payant,$montantAct,$paraid,$notifid,$codeEtab,$libellesession]);

        //modification dans notifications

        $req= $this->db->prepare("UPDATE notification set libelle_notif=?,message_notif=?,sms_notif=?,email_notif=?,join_notif=? where id_notif=? and codeEtab_notif=? and 	session_notif=?");
        $req->execute([$libelactivity,$message,$smsvalue,$emailvalue,$statutFile,$notifid,$codeEtab,$libellesession]);
        $_SESSION['user']['Updateadminok']="Activité Parascolaire modifiée avec succès";
        if($_SESSION['user']['profile'] == "Admin_globale") {

              //header("Location:../manager/index.php");
              header("Location:../manager/parascolaires.php");

          }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/parascolaires.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/parascolaires.php");

              }
      }

      function NotificationAndParascolairesInfosUpdateWithPaieWithoutFile($libelactivity,$typesess,$datedeb,$datefin,$codeEtab,$libellesession,$paraid,$message,$montantAct,$smsvalue,$emailvalue,$notifid,$statutFile)
      {
        $payant=1;
        $req= $this->db->prepare("UPDATE parascolaires set libelle_para=?,debut_para=?,fin_para=?,payant_para=?,montant_para=? where id_para=? and idnotif_para=? and codeEtab_para=? and session_para=?");
        $req->execute([$libelactivity,$datedeb,$datefin,$payant,$montantAct,$paraid,$notifid,$codeEtab,$libellesession]);

        //modification dans notifications

        $req= $this->db->prepare("UPDATE notification set libelle_notif=?,message_notif=?,sms_notif=?,email_notif=?,join_notif=? where id_notif=? and codeEtab_notif=? and 	session_notif=?");
        $req->execute([$libelactivity,$message,$smsvalue,$emailvalue,$statutFile,$notifid,$codeEtab,$libellesession]);

        $_SESSION['user']['Updateadminok']="Activité Parascolaire modifiée avec succès";
        if($_SESSION['user']['profile'] == "Admin_globale") {

              //header("Location:../manager/index.php");
              header("Location:../manager/parascolaires.php");

          }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/parascolaires.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/parascolaires.php");

              }
      }

      function NotificationAndParascolairesInfosUpdateWithPaie($libelactivity,$typesess,$datedeb,$datefin,$codeEtab,$libellesession,$paraid,$message,$montantAct,$smsvalue,$emailvalue,$notifid,$statutFile,$file)
      {
        $payant=1;
        $req= $this->db->prepare("UPDATE parascolaires set libelle_para=?,debut_para=?,fin_para=?,payant_para=?,montant_para=? where id_para=? and idnotif_para=? and codeEtab_para=? and session_para=?");
        $req->execute([$libelactivity,$datedeb,$datefin,$payant,$montantAct,$paraid,$notifid,$codeEtab,$libellesession]);

        //modification dans notifications

        $req= $this->db->prepare("UPDATE notification set libelle_notif=?,message_notif=?,sms_notif=?,email_notif=?,join_notif=?,fichier_notif=? where id_notif=? and codeEtab_notif=? and 	session_notif=?");
        $req->execute([$libelactivity,$message,$smsvalue,$emailvalue,$statutFile,$file,$notifid,$codeEtab,$libellesession]);

        $_SESSION['user']['Updateadminok']="Activité Parascolaire modifiée avec succès";
        if($_SESSION['user']['profile'] == "Admin_globale") {

              //header("Location:../manager/index.php");
              header("Location:../manager/parascolaires.php");

          }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/parascolaires.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/parascolaires.php");

              }

      }

      function NotificationAndParascolairesInfosUpdateWithOutPaie($libelactivity,$typesess,$datedeb,$datefin,$codeEtab,$libellesession,$paraid,$message,$montantAct,$smsvalue,$emailvalue,$notifid,$statutFile,$file)
      {
        $payant=0;
        $req= $this->db->prepare("UPDATE parascolaires set libelle_para=?,debut_para=?,fin_para=?,payant_para=?,montant_para=? where id_para=? and idnotif_para=? and codeEtab_para=? and session_para=?");
        $req->execute([$libelactivity,$datedeb,$datefin,$payant,$montantAct,$paraid,$notifid,$codeEtab,$libellesession]);

        //modification dans notifications

        $req= $this->db->prepare("UPDATE notification set libelle_notif=?,message_notif=?,sms_notif=?,email_notif=?,join_notif=?,fichier_notif=? where id_notif=? and codeEtab_notif=? and 	session_notif=?");
        $req->execute([$libelactivity,$message,$smsvalue,$emailvalue,$statutFile,$file,$notifid,$codeEtab,$libellesession]);
        $_SESSION['user']['Updateadminok']="Activité Parascolaire modifiée avec succès";
        if($_SESSION['user']['profile'] == "Admin_globale") {

              //header("Location:../manager/index.php");
              header("Location:../manager/parascolaires.php");

          }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/parascolaires.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/parascolaires.php");

              }
      }



      function UpdateDestinatairesNotificationAndParascolaire($destinataires,$paraid,$codeEtab,$libellesession,$notifid)
      {
        $req1= $this->db->prepare("UPDATE parascolaires set classes_para=? where idnotif_para=? and codeEtab_para=? and session_para=? and id_para=?");
        $req1->execute([$classes,$notifid,$codeEtab,$libellesession,$paraid]);
      }

      function UpdateClassesNotificationAndParascolaire($classes,$paraid,$codeEtab,$libellesession,$notifid)
      {
        $req= $this->db->prepare("UPDATE notification set classes_notif=? where id_notif=? and codeEtab_notif=? and session_notif=?");
        $req->execute([$classes,$notifid,$codeEtab,$libellesession]);

        //modification dans Parascolaires
          $req1= $this->db->prepare("UPDATE parascolaires set classes_para=? where idnotif_para=? and codeEtab_para=? and session_para=? and id_para=?");
          $req1->execute([$classes,$notifid,$codeEtab,$libellesession,$paraid]);
      }

      function getParascolairesOfThisSchool($codeEtabLocal,$libellesessionencours,$paraid)
      {
          $req = $this->db->prepare("SELECT * FROM parascolaires,notification where parascolaires.idnotif_para=notification.id_notif and notification.para_notif=1 and parascolaires.codeEtab_para=? and parascolaires.session_para=? and parascolaires.id_para=?");
          $req->execute([$codeEtabLocal,$libellesessionencours,$paraid]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["libelle_para"]."*".$someArray[0]["typesession_para"]."*".$someArray[0]["debut_para"]."*".$someArray[0]["fin_para"];
          $donnees.="*".$someArray[0]["message_notif"]."*".$someArray[0]["join_notif"]."*".$someArray[0]["fichier_notif"];
          $donnees.="*".$someArray[0]["classes_notif"]."*".$someArray[0]["destinataires_notif"]."*".$someArray[0]["payant_para"];
          $donnees.="*".$someArray[0]["montant_para"]."*".$someArray[0]["sms_notif"]."*".$someArray[0]["email_notif"]."*".$someArray[0]["date_notif"]."*".$someArray[0]["id_notif"]."*".$someArray[0]["date_notif"];
          return $donnees;

      }

      function getAllParascolairesOfThisSchool($codeEtabLocal,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM parascolaires where codeEtab_para=? and  session_para=? and  statut_para not in (0,3)");
        $req->execute([$codeEtabLocal,$libellesessionencours]);
        return $req->fetchAll();

      }

      function AddParascolaireWithPaiement($libelactivity,$datedeb,$datefin,$codeEtab,$libellesession,$typesess,$montantAct,$paiecheck,$classes,$idnotif,$statutpara)
      {
        $req = $this->db->prepare("INSERT INTO parascolaires SET 	libelle_para=?,debut_para=?,fin_para=?,codeEtab_para=?,session_para=?,typesession_para=?,montant_para=?,payant_para=?,classes_para=?,idnotif_para=?,statut_para=?");
        $req->execute([$libelactivity,$datedeb,$datefin,$codeEtab,$libellesession,$typesess,$montantAct,$paiecheck,$classes,$idnotif,$statutpara]);
        $_SESSION['user']['deletesubjectok']="Activité parascolaire ajoutée avec succès";

        if($_SESSION['user']['profile'] == "Admin_globale") {

              //header("Location:../manager/index.php");
              header("Location:../manager/parascolaires.php");

          }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/parascolaires.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/parascolaires.php");

              }

      }

      function AddParascolaireWithOutPaiement($libelactivity,$datedeb,$datefin,$codeEtab,$libellesession,$typesess,$paiecheck,$classes,$idnotif,$statutpara)
      {
          $req = $this->db->prepare("INSERT INTO parascolaires SET 	libelle_para=?,debut_para=?,fin_para=?,codeEtab_para=?,session_para=?,typesession_para=?,payant_para=?,classes_para=?,idnotif_para=?,statut_para=?");
          $req->execute([$libelactivity,$datedeb,$datefin,$codeEtab,$libellesession,$typesess,$paiecheck,$classes,$idnotif,$statutpara]);
          $_SESSION['user']['deletesubjectok']="Activité parascolaire ajoutée avec succès";

          if($_SESSION['user']['profile'] == "Admin_globale") {

                //header("Location:../manager/index.php");
                header("Location:../manager/parascolaires.php");

            }else if($_SESSION['user']['profile'] == "Admin_locale") {

              header("Location:../locale/parascolaires.php");

            }else if($_SESSION['user']['profile'] == "Teatcher") {

                header("Location:../teatcher/parascolaires.php");

                }
      }

      function ArchivedNotification($status,$notifid,$session,$codeEtab)
      {
        $req= $this->db->prepare("UPDATE notification SET statut_notif=? where id_notif=? and session_notif=? and codeEtab_notif=?");
        $req->execute([$status,$notifid,$session,$codeEtab]);
        $_SESSION['user']['Updateadminok']="Notification archiver avec succès";
        if($_SESSION['user']['profile'] == "Admin_globale") {

              //header("Location:../manager/index.php");
              header("Location:../manager/messages.php");

          }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/messages.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/messages.php");

              }
      }

      function DeleteNotificationParasco($paraid,$notifid,$statusNotification,$statusparasco)
      {
        $req = $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=?");
        $req->execute([$statusNotification,$notifid]);

        $req1 = $this->db->prepare("UPDATE parascolaires set statut_para=? where idnotif_para=? and id_para=?");
        $req1->execute([$statusparasco,$notifid,$paraid]);

        $_SESSION['user']['Updateadminok']="Notification supprimer avec succès";
        if($_SESSION['user']['profile'] == "Admin_globale") {

              //header("Location:../manager/index.php");
              header("Location:../manager/messages.php");

          }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/messages.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/messages.php");

              }


      }

      function DeleteNotification($notifid,$statusNotification)
      {
        // $req = $this->db->prepare("DELETE FROM notification where id_notif=?");
        // $req->execute([$notifid]);
        $req = $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=?");
        $req->execute([$statusNotification,$notifid]);
        $_SESSION['user']['Updateadminok']="Notification supprimer avec succès";
        if($_SESSION['user']['profile'] == "Admin_globale") {

              //header("Location:../manager/index.php");
              header("Location:../manager/messages.php");

          }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/messages.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/messages.php");

              }
      }

      function UpdateNotificationStatus($notifid,$notificationsStatus,$codeEtab)
      {
        $req= $this->db->prepare("UPDATE notification SET statut_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$notificationsStatus,$notifid,$codeEtab]);
      }

      function SendAddedExamenNotesMailerToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$sessionlibelle,$codeEtab)
      {
        require_once('../PHPMailer/class.phpmailer.php');
        require_once('../PHPMailer/class.smtp.php');
        require_once('../controller/functions.php');
        $client1="justearmel04@gmail.com";
        $client2="fabienekoute@gmail.com";
        $mail = new PHPMailer();
        $mail->isHTML(true);
        $mail->CharSet="UTF-8";
        $mail->isSMTP();
        $mail->SMTPOptions = array (
       'ssl' => array(
       'verify_peer'  => false,
       'verify_peer_name'  => false,
       'allow_self_signed' => true));
       $mail->Host='mail.proximity-cm.com';
       $mail->SMTPAuth = true;
       $mail->Port = 25;
       $mail->SMTPSecure = "tls";
       $mail->Username = "xschool@proximity-cm.com";
       $mail->Password ="123psa@456";
       $mail->From='xschool@proximity-cm.com';
       $mail->FromName=$libelleEtab;
       $mail->AddAddress($client1);
       $mail->AddAddress($client2);
       $mail->AddAddress($destimails);
       $mail->Subject ="Modification de Note";
       $mail->Body = "Bonjour Cher Parent<br>";
       $mail->Body .="La direction de l'établissement ".$libelleEtab." tiens à vous informer de l'ajout d'une nouvelle de note d'examen de votre enfant  ".$nomcompletStudent."<br>";
       $mail->Body .="régulièrement inscrit en classe de ". $libelleclasse ." au cours de ". $libellematiere;
       $mail->Body .="<br>";
       $mail->Body .="Merci de vous connecter à la plateforme afin de consulter <br>";
       $mail->Body .="Cordialement<br>";
       $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
       $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");

       if(!$mail->Send())

       {

       $msg="nok";

       }else
       {

       $msg="ok";

       }

  return $msg;
      }

      function SendAddedControleNotesMailerToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$sessionlibelle,$codeEtab,$libellematiere)
      {
        require_once('../PHPMailer/class.phpmailer.php');
        require_once('../PHPMailer/class.smtp.php');
        require_once('../controller/functions.php');
        $client1="justearmel04@gmail.com";
        $client2="fabienekoute@gmail.com";
        $mail = new PHPMailer();
        $mail->isHTML(true);
        $mail->CharSet="UTF-8";
        $mail->isSMTP();
        $mail->SMTPOptions = array (
       'ssl' => array(
       'verify_peer'  => false,
       'verify_peer_name'  => false,
       'allow_self_signed' => true));
       $mail->Host='mail.proximity-cm.com';
       $mail->SMTPAuth = true;
       $mail->Port = 25;
       $mail->SMTPSecure = "tls";
       $mail->Username = "xschool@proximity-cm.com";
       $mail->Password ="123psa@456";
       $mail->From='xschool@proximity-cm.com';
       $mail->FromName=$libelleEtab;
       $mail->AddAddress($client1);
       $mail->AddAddress($client2);
       $mail->AddAddress($destimails);
       $mail->Subject ="Modification de Note";
       $mail->Body = "Bonjour Cher Parent<br>";
       $mail->Body .="La direction de l'établissement ".$libelleEtab." tiens à vous informer de l'ajout d'une nouvelle de note de contrôle de votre enfant  ".$nomcompletStudent."<br>";
       $mail->Body .="régulièrement inscrit en classe de ". $libelleclasse ." au cours de ". $libellematiere;
       $mail->Body .="<br>";
       $mail->Body .="Merci de vous connecter à la plateforme afin de consulter <br>";
       $mail->Body .="Cordialement<br>";
       $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
       $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");

       if(!$mail->Send())

       {

       $msg="nok";

       }else
       {

       $msg="ok";

       }

  return $msg;

      }

      function SendModificationNotesToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$libellematiere,$codeEtab)
      {
        require_once('../PHPMailer/class.phpmailer.php');
        require_once('../PHPMailer/class.smtp.php');
        require_once('../controller/functions.php');
        $client1="justearmel04@gmail.com";
        $client2="fabienekoute@gmail.com";
        $mail = new PHPMailer();
        $mail->isHTML(true);
        $mail->CharSet="UTF-8";
        $mail->isSMTP();
        $mail->SMTPOptions = array (
       'ssl' => array(
       'verify_peer'  => false,
       'verify_peer_name'  => false,
       'allow_self_signed' => true));
       $mail->Host='mail.proximity-cm.com';
       $mail->SMTPAuth = true;
       $mail->Port = 25;
       $mail->SMTPSecure = "tls";
       $mail->Username = "xschool@proximity-cm.com";
       $mail->Password ="123psa@456";
       $mail->From='xschool@proximity-cm.com';
       $mail->FromName=$libelleEtab;
       $mail->AddAddress($client1);
       $mail->AddAddress($client2);
       $mail->AddAddress($destimails);
       $mail->Subject ="Modification de Note";
       $mail->Body = "Bonjour Cher Parent<br>";
       $mail->Body .="La direction de l'établissement ".$libelleEtab." tiens à vous informer d'une modification de note de votre enfant  ".$nomcompletStudent."<br>";
       $mail->Body .="régulièrement inscrit en classe de ". $libelleclasse ." au cours de ". $libellematiere;
       $mail->Body .="<br>";
       $mail->Body .="Merci de vous connecter à la plateforme afin de consulter <br>";
       $mail->Body .="Cordialement<br>";
       $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
       $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");

       if(!$mail->Send())

       {

       $msg="nok";

       }else
       {

       $msg="ok";

       }

  return $msg;

      }

    function SendNotifiactionWithToDestinataires($destimails,$titremessage,$contenumessage,$libelleEtab,$logoEtab,$codeEtab,$datenotif,$file)
    {
      require_once('../PHPMailer/class.phpmailer.php');
      require_once('../PHPMailer/class.smtp.php');
      require_once('../controller/functions.php');
      $day=date("d-m-Y");
      $client1="justearmel04@gmail.com";
      $client2="fabienekoute@gmail.com";
      $mail = new PHPMailer();
      $mail->isHTML(true);
      $mail->CharSet="UTF-8";
      $mail->isSMTP();
      $mail->SMTPOptions = array (
     'ssl' => array(
     'verify_peer'  => false,
     'verify_peer_name'  => false,
     'allow_self_signed' => true));
     $mail->Host='mail.proximity-cm.com';
     $mail->SMTPAuth = true;
     $mail->Port = 25;
     $mail->SMTPSecure = "tls";
     $mail->Username = "xschool@proximity-cm.com";
     $mail->Password ="123psa@456";
     $mail->From='xschool@proximity-cm.com';
     $mail->FromName=$libelleEtab;
     $mail->AddAddress($client1);
     $mail->AddAddress($client2);
     $tabdesti=explode('*',$destimails);
     $nb=count($tabdesti);

     for($i=0;$i<$nb;$i++){
       $mail->AddAddress($tabdesti[$i]);
     }
     $mail->Subject =$titremessage;
     $mail->Body =$contenumessage;
     $mail->Body .="<br>";
     $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
     $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");
     $mail->addAttachment("../notifications/".$datenotif."/".$file);

     if(!$mail->Send())

     {

     $msg="nok";

     }else
     {

     $msg="ok";

     }

return $msg;



    }

      function TeatcherRoutineUpdateMailer($mailteatcher,$libelleclasse,$libellematieres,$libellejourold,$libelledaymodify,$debhourold,$heuredeb,$finhourold,$heurefin,$libelleEtab,$logoEtab,$codeEtab)
      {
        require_once('../PHPMailer/class.phpmailer.php');
        require_once('../PHPMailer/class.smtp.php');
        require_once('../controller/functions.php');

        $titremessage="Modification Emploi du temps";

        $client1="justearmel04@gmail.com";
        $client2="fabienekoute@gmail.com";
        $mail = new PHPMailer();
        $mail->isHTML(true);
        $mail->CharSet="UTF-8";
        $mail->isSMTP();
        $mail->SMTPOptions = array (
       'ssl' => array(
       'verify_peer'  => false,
       'verify_peer_name'  => false,
       'allow_self_signed' => true));
       $mail->Host='mail.proximity-cm.com';
       $mail->SMTPAuth = true;
       $mail->Port = 25;
       $mail->SMTPSecure = "tls";
       $mail->Username = "xschool@proximity-cm.com";
       $mail->Password ="123psa@456";
       $mail->From='xschool@proximity-cm.com';
       $mail->FromName=$libelleEtab;
       $mail->AddAddress($client1);
       $mail->AddAddress($client2);
       $mail->AddAddress($mailteatcher);
       $mail->Subject =$titremessage;
       $mail->Body = "Bonjour Chers Professeur<br>";
       $mail->Body .="La direction de l'établissement ".$libelleEtab." tiens à vous informer d'une modification dans votre emploi du temps pour les cours de ".$libellematieres;
       $mail->Body .="<br>";
       $mail->Body .="Initialement prévu chaque ".$libellejourold ."  de ".$debhourold . "à ".$finhourold ." pour la classe de " .$libelleclasse . "  qui se déroulerons désormais chaque ".$libelledaymodify . " de ".$heuredeb . " à ".$heurefin  ;
       $mail->Body .="<br>";
       $mail->Body .="La Direction vous remercie de vôtre compréhension";
       $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
       $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");
       if(!$mail->Send())

       {

       $msg="nok";

       }else
       {

       $msg="ok";

       }

  return $msg;

      }

      function SendSyllabusModifiedbyCodeEtab($localMails,$libelleEtab,$logoEtab,$teatcherName,$libellemat,$classelibelle,$codeEtab)
      {
        require_once('../PHPMailer/class.phpmailer.php');
        require_once('../PHPMailer/class.smtp.php');
        require_once('../controller/functions.php');

        $titremessage="Modification  Programme scolaire";
        $client1="justearmel04@gmail.com";
        $client2="fabienekoute@gmail.com";
        $mail = new PHPMailer();
        $mail->isHTML(true);
        $mail->CharSet="UTF-8";
        $mail->isSMTP();
        $mail->SMTPOptions = array (
       'ssl' => array(
       'verify_peer'  => false,
       'verify_peer_name'  => false,
       'allow_self_signed' => true));
       $mail->Host='mail.proximity-cm.com';
       $mail->SMTPAuth = true;
       $mail->Port = 25;
       $mail->SMTPSecure = "tls";
       $mail->Username = "xschool@proximity-cm.com";
       $mail->Password ="123psa@456";
       $mail->From='xschool@proximity-cm.com';
       $mail->FromName=$libelleEtab;
       $mail->AddAddress($client1);
       $mail->AddAddress($client2);
       $tabdesti=explode('*',$localMails);
       $nb=count($tabdesti);

       for($i=0;$i<$nb;$i++){
         $mail->AddAddress($tabdesti[$i]);
       }
       $mail->Subject =$titremessage;
       $mail->Body = "Bonjour Responsable<br>";
       $mail->Body = "Le programme academique du cours ".$libellemat ." dispenser par le professeur ".$teatcherName . " dans la classe de ".$classelibelle . " à bien été modifié ce jour";
       $mail->Body .="<br>";
       $mail->Body .="Merci de prendre connaissance du programme ";
       $mail->Body .="<br>";
       $mail->Body .="Cordialement";
       $mail->Body .="<br>";
       $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
       $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");
       if(!$mail->Send())

       {

       $msg="nok";

       }else
       {

       $msg="ok";

       }

  return $msg;



      }

      function SendSyllabusCreatedbyCodeEtab($codeEtab,$localMails,$libelleEtab,$logoEtab,$libellemat,$libelleclasse,$teatcherName)
      {
        require_once('../PHPMailer/class.phpmailer.php');
        require_once('../PHPMailer/class.smtp.php');
        require_once('../controller/functions.php');

        $titremessage="Ajout de Programme scolaire";

        $client1="justearmel04@gmail.com";
        $client2="fabienekoute@gmail.com";
        $mail = new PHPMailer();
        $mail->isHTML(true);
        $mail->CharSet="UTF-8";
        $mail->isSMTP();
        $mail->SMTPOptions = array (
       'ssl' => array(
       'verify_peer'  => false,
       'verify_peer_name'  => false,
       'allow_self_signed' => true));
       $mail->Host='mail.proximity-cm.com';
       $mail->SMTPAuth = true;
       $mail->Port = 25;
       $mail->SMTPSecure = "tls";
       $mail->Username = "xschool@proximity-cm.com";
       $mail->Password ="123psa@456";
       $mail->From='xschool@proximity-cm.com';
       $mail->FromName=$libelleEtab;
       $mail->AddAddress($client1);
       $mail->AddAddress($client2);
       $tabdesti=explode('*',$localMails);
       $nb=count($tabdesti);

       for($i=0;$i<$nb;$i++){
         $mail->AddAddress($tabdesti[$i]);
       }
       $mail->Subject =$titremessage;
        $mail->Body = "Bonjour Responsable<br>";
        $mail->Body = "Le programme academique du cours ".$libellemat ." dispenser par le professeur ".$teatcherName . " dans la classe de ".$libelleclasse . " à bien été ajouter ce jour";
        $mail->Body .="<br>";
        $mail->Body .="Merci de prendre connaissance du programme ";
        $mail->Body .="<br>";
        $mail->Body .="Cordialement";
        $mail->Body .="<br>";
        $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
        $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");
        if(!$mail->Send())

        {

        $msg="nok";

        }else
        {

        $msg="ok";

        }

   return $msg;

      }

      function ParentsRoutineUpdateMailer($parentsMails,$libelleclasse,$libellematieres,$libellejourold,$libelledaymodify,$debhourold,$heuredeb,$finhourold,$heurefin,$libelleEtab,$logoEtab,$codeEtab)
      {
        require_once('../PHPMailer/class.phpmailer.php');
        require_once('../PHPMailer/class.smtp.php');
        require_once('../controller/functions.php');

        $titremessage="Modification Emploi du temps";

        $client1="justearmel04@gmail.com";
        $client2="fabienekoute@gmail.com";
        $mail = new PHPMailer();
        $mail->isHTML(true);
        $mail->CharSet="UTF-8";
        $mail->isSMTP();
        $mail->SMTPOptions = array (
       'ssl' => array(
       'verify_peer'  => false,
       'verify_peer_name'  => false,
       'allow_self_signed' => true));
       $mail->Host='mail.proximity-cm.com';
       $mail->SMTPAuth = true;
       $mail->Port = 25;
       $mail->SMTPSecure = "tls";
       $mail->Username = "xschool@proximity-cm.com";
       $mail->Password ="123psa@456";
       $mail->From='xschool@proximity-cm.com';
       $mail->FromName=$libelleEtab;
       $mail->AddAddress($client1);
       $mail->AddAddress($client2);
       $tabdesti=explode('*',$parentsMails);
       $nb=count($tabdesti);

       for($i=0;$i<$nb;$i++){
         $mail->AddAddress($tabdesti[$i]);
       }
       $mail->Subject =$titremessage;
       $mail->Body = "Bonjour Chers Parent<br>";
       $mail->Body .="La direction de l'établissement ".$libelleEtab." tiens à vous informer d'une modification dans l'emploi du temps de votre enfant pour les cours de ".$libellematieres;
       $mail->Body .="<br>";
       $mail->Body .="Initialement prévu chaque ".$libellejourold ."  de ".$debhourold . "à ".$finhourold ." pour la classe de " .$libelleclasse . "  qui se déroulerons désormais chaque ".$libelledaymodify . " de ".$heuredeb . " à ".$heurefin  ;
       $mail->Body .="<br>";
       $mail->Body .="La Direction vous remercie de vôtre compréhension";
       $mail->Body .="<br>";
       $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
       $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");
       if(!$mail->Send())

       {

       $msg="nok";

       }else
       {

       $msg="ok";

       }

  return $msg;
      }

      function SendNotifiactionWithoutToDestinataires($destimails,$titremessage,$contenumessage,$libelleEtab,$logoEtab,$codeEtab)
      {
        require_once('../PHPMailer/class.phpmailer.php');
        require_once('../PHPMailer/class.smtp.php');
        require_once('../controller/functions.php');

        $day=date("d-m-Y");
        $client1="justearmel04@gmail.com";
        $client2="fabienekoute@gmail.com";
        $mail = new PHPMailer();
         $mail->isHTML(true);
         $mail->CharSet="UTF-8";
         $mail->isSMTP();
         $mail->SMTPOptions = array (
        'ssl' => array(
        'verify_peer'  => false,
        'verify_peer_name'  => false,
        'allow_self_signed' => true));
        $mail->Host='mail.proximity-cm.com';
        $mail->SMTPAuth = true;
        $mail->Port = 25;
        $mail->SMTPSecure = "tls";
        $mail->Username = "xschool@proximity-cm.com";
        $mail->Password ="123psa@456";
        $mail->From='xschool@proximity-cm.com';
        $mail->FromName=$libelleEtab;
        $mail->AddAddress($client1);
        $mail->AddAddress($client2);
        $tabdesti=explode('*',$destimails);
        $nb=count($tabdesti);

        for($i=0;$i<$nb;$i++){
          $mail->AddAddress($tabdesti[$i]);
        }
        $mail->Subject =$titremessage;
        $mail->Body =$contenumessage;
        $mail->Body .="<br>";
        $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
        $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");

        if(!$mail->Send())

        {

        $msg="nok";

        }else
        {

        $msg="ok";

        }
        return $msg;

      }

      function deleteRoutineById($routineid,$classe)
      {
        $req = $this->db->prepare("DELETE FROM routine where id_route=?");
        $req->execute([$routineid]);
        $_SESSION['user']['updateroutineok']=" Routine supprimer avec succès";

         // header("Location:../manager/matieres.php?classe=".$classe);
         if($_SESSION['user']['profile'] == "Admin_globale") {

               //header("Location:../manager/index.php");
               header("Location:../manager/routines.php?classe=".$classe);

           }else if($_SESSION['user']['profile'] == "Admin_locale") {

             header("Location:../locale/routines.php?classe=".$classe);

             }
      }

      function getIndictatifOfThisSchool($codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement,pays where etablissement.pays_etab=pays.id_pays and etablissement.code_etab=?");
        $req->execute([$codeEtab]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["indicatif_pays"];
        return $donnees;
      }

      function getNotificationInfos($notifid)
      {
          $req = $this->db->prepare("SELECT * FROM notification where id_notif=?");
          $req->execute([$notifid]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["destinataires_notif"]."*".$someArray[0]["classes_notif"]."*".$someArray[0]["codeEtab_notif"]."*".$someArray[0]["sms_notif"]."*".$someArray[0]["email_notif"]."*".$someArray[0]["join_notif"]."*".$someArray[0]["fichier_notif"];
          $donnees.="*".$someArray[0]["libelle_notif"]."*".$someArray[0]["message_notif"]."*".$someArray[0]["date_notif"]."*".$someArray[0]["para_notif"];
          return $donnees;
      }

      function getEmailOfLocaladminOfThisClasses($classeid,$typecompte,$codeEtab)
      {
        $req = $this->db->prepare("SELECT distinct compte.email_compte,compte.tel_compte FROM compte,assigner where assigner.id_adLocal=compte.id_compte and assigner.codeEtab_assign=? and compte.type_compte=?");
        $req->execute([$codeEtab,$typecompte]);
        return $req->fetchAll();
      }

      function getEmailsOfTeatcherOfThisClasses($classeid,$typecompte,$codeEtab)
      {
        $session="2019-2020";
        $req = $this->db->prepare("SELECT distinct compte.email_compte,compte.tel_compte FROM compte,dispenser,classe where dispenser.id_enseignant=compte.id_compte and dispenser.idclasse_disp=classe.id_classe and classe.id_classe=? and dispenser.codeEtab=? and compte.type_compte=?");
        $req->execute([$classeid,$codeEtab,$typecompte]);
        return $req->fetchAll();
      }

      function getEmailsOfStudentOfThisClasses($classeid,$typecompte,$codeEtab)
      {
        //typecompte==Student
        $session="2019-2020";
        $req = $this->db->prepare("SELECT distinct eleve.email_eleve,compte.tel_compte FROM eleve,inscription,classe,compte where inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=eleve.idcompte_eleve and compte.id_compte=eleve.idcompte_eleve and inscription.session_inscrip=? and classe.id_classe=? and classe.codeEtab_classe=? ");
        $req->execute([$session,$classeid,$codeEtab]);
        return $req->fetchAll();

      }

      function getEmailsOfParentOfStudentInThisClasses($classeid,$typecompte,$codeEtab,$session)
      {
        //typecompte==Parent
        //$session="2019-2020";
          $req = $this->db->prepare("SELECT distinct parent.email_parent,parent.tel_parent FROM eleve,inscription,classe,parent,parenter where parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.session_inscrip=?  and classe.id_classe=? and classe.codeEtab_classe=?");
          $req->execute([$session,$classeid,$codeEtab]);
          return $req->fetchAll();
      }

      function getAllNotificationbySchoolCode($codeEtabAssigner,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM notification where codeEtab_notif=? and session_notif=? and statut_notif not in (-1,4) order by id_notif DESC");
        $req->execute([$codeEtabAssigner,$libellesessionencours]);
        return $req->fetchAll();
      }

      function AddNotificationWithoutFi($titre,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$session,$paranotif)
      {
        $req = $this->db->prepare("INSERT INTO notification SET libelle_notif=?,destinataires_notif=?,classes_notif=?,message_notif=?,join_notif=?,statut_notif=?,date_notif=?,codeEtab_notif=?,sms_notif=?,email_notif=?,session_notif=?,para_notif=?");
        $req->execute([$titre,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$session,$paranotif]);
        $idlastcompte=$this->db->lastInsertId();

        return $idlastcompte;
      }

      function AddNotificationWithoutFile($titre,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$paranotif,$libellesession)
      {
        $req = $this->db->prepare("INSERT INTO notification SET libelle_notif=?,destinataires_notif=?,classes_notif=?,message_notif=?,join_notif=?,statut_notif=?,date_notif=?,codeEtab_notif=?,sms_notif=?,email_notif=?,para_notif=?,session_notif=?");
        $req->execute([$titre,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$paranotif,$libellesession]);
        $_SESSION['user']['Updateadminok']="Notification ajouté avec succès";
        if($_SESSION['user']['profile'] == "Admin_globale") {

              //header("Location:../manager/index.php");
              header("Location:../manager/messages.php");

          }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/messages.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/messages.php");

              }

      }

      function UpdateNotificationFile($fichierad,$idnotif,$codeEtab)
      {
        $req= $this->db->prepare("UPDATE notification SET fichier_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$fichierad,$idnotif,$codeEtab]);
      }

      function UpdateNotificationFileName($fichierad,$idnotif,$codeEtab)
      {
        $req= $this->db->prepare("UPDATE notification SET fichier_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$fichierad,$idnotif,$codeEtab]);
        $_SESSION['user']['Updateadminok']="Notification ajouté avec succès";
        if($_SESSION['user']['profile'] == "Admin_globale") {

              //header("Location:../manager/index.php");
              header("Location:../manager/messages.php");

          }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/messages.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/messages.php");

              }

      }

      function AddNotificationWithFile($titre,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$session,$paranotif)
      {
        $req = $this->db->prepare("INSERT INTO notification SET libelle_notif=?,destinataires_notif=?,classes_notif=?,message_notif=?,join_notif=?,statut_notif=?,date_notif=?,codeEtab_notif=?,sms_notif=?,email_notif=?,session_notif=?,para_notif=?");
        $req->execute([$titre,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$session,$paranotif]);
        $idfiche=$this->db->lastInsertId();
        return $idfiche;
      }

      function getEtabLibellebyCodeEtab($codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement where code_etab=?");
        $req->execute([$codeEtab]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["libelle_etab"];
        return $donnees;
      }

      function getEtabLogobyCodeEtab($codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement where code_etab=?");
        $req->execute([$codeEtab]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["logo_etab"];
        return $donnees;
      }

      function getHoursOfSubject($classeId,$matiereid,$profid,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM routine where classe_route=? and matiere_route=? and etab_route=?");
        $req->execute([$classeId,$matiereid,$codeEtab]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["debut_route"]."*".$someArray[0]["fin_route"];
        return $donnees;
      }

      function getCodeEtabOfEnseignerId($IdCompte)
      {
        $req = $this->db->prepare("SELECT * FROM enseigner where id_enseignant=?");
        $req->execute([$IdCompte]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["codeEtab"];
        return $donnees;
      }

      function UpdateFicheLectureWithFile($fichierad,$ficheid,$codeEtab,$classeEtab,$matclasse)
      {
        $req= $this->db->prepare("UPDATE fiche SET support_fiche=? where id_fiche=? and codeEtab_fiche=? and classe_fiche=? and mat_fiche=?");
        $req->execute([$fichierad,$ficheid,$codeEtab,$classeEtab,$matclasse]);

        $_SESSION['user']['addprogra']="Fiche de Lecture modifié avec succès";
        if($_SESSION['user']['profile'] == "Admin_globale") {

              //header("Location:../manager/index.php");
              header("Location:../manager/fiches.php");

          }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/fiches.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/fiches.php");

              }
      }

      function UpdateSousFiches($desi,$ficheid,$sousficheid)
      {
        $req= $this->db->prepare("UPDATE sousfiche SET libelle_sousfic=? where idfic_sousfic=? and 	id_sousfic=?");
        $req->execute([$desi,$ficheid,$sousficheid]);
      }

      function DeletedDousFichesById($sousficheid,$ficheid)
      {
        $req = $this->db->prepare("DELETE FROM sousfiche where id_sousfic=?");
        $req->execute([$sousficheid]);
        $_SESSION['user']['addprogra']="Element supprimer avec succès";
        if($_SESSION['user']['profile'] == "Admin_globale") {

              //header("Location:../manager/index.php");
              header("Location:../manager/updatefiche.php?programme=".$ficheid);

          }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/updatefiche.php?programme=".$ficheid);

          }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/updatefiche.php?programme=".$ficheid);

              }
      }

      function getNumberOfSousFiches($ficheid)
      {
        $req = $this->db->prepare("SELECT * FROM sousfiche where idfic_sousfic=?");
        $req->execute([$ficheid]);
        $data=$req->fetchAll();
        $nb=count($data);

        return $nb;
      }

      function getAllSousFichesOfThisFicheId($sousfiche)
      {
          $req = $this->db->prepare("SELECT * FROM sousfiche where idfic_sousfic=?");
          $req->execute([$sousfiche]);
          return $req->fetchAll();
      }

      function getAllfichesOfTeatcherClassesAndpgrmeId($IdCompte,$idfiche)
      {
        $req = $this->db->prepare("SELECT * FROM fiche,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and fiche.teatcher_fiche=enseignant.idcompte_enseignant and fiche.mat_fiche=matiere.id_mat and fiche.classe_fiche=classe.id_classe and enseignant.idcompte_enseignant=? and fiche.id_fiche=?");
        $req->execute([$IdCompte,$idfiche]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["id_fiche"]."*".$someArray[0]["mat_fiche"]."*".$someArray[0]["classe_fiche"]."*".$someArray[0]["teatcher_fiche"]."*".$someArray[0]["codeEtab_fiche"]."*".$someArray[0]["support_fiche"]."*".$someArray[0]["date_fiche"];
        return $donnees;


      }
      function getAllSousFicheOfThisFicheId($idfiche)
      {
          $req = $this->db->prepare("SELECT * FROM sousfiche where idfic_sousfic=?");
          $req->execute([$idfiche]);
          return $req->fetchAll();
      }

      function getAllFicesOfTeatcherClasses($IdCompte,$session)
      {
        $req = $this->db->prepare("SELECT * FROM fiche,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and fiche.teatcher_fiche=enseignant.idcompte_enseignant and fiche.mat_fiche=matiere.id_mat and fiche.classe_fiche=classe.id_classe and enseignant.idcompte_enseignant=? and fiche.session_fiche=? order by classe.libelle_classe ASC");
        $req->execute([$IdCompte,$session]);
        return $req->fetchAll();
      }

      function AddSousFiche($idfiche,$details)
      {
          $req = $this->db->prepare("INSERT INTO sousfiche SET idfic_sousfic=?,libelle_sousfic=?");
          $req->execute([$idfiche,$details]);
      }

      function UpdateLectureFicheFileName($fichierad,$idfiche,$teatcherid,$codeEtab,$matiereid,$classeEtab)
      {
        $req= $this->db->prepare("UPDATE fiche SET support_fiche=? where id_fiche=? and teatcher_fiche=? and codeEtab_fiche=? and mat_fiche=? and classe_fiche=?");
        $req->execute([$fichierad,$idfiche,$teatcherid,$codeEtab,$matiereid,$classeEtab]);
      }

      function AddLectureFiche($datecrea,$matiereid,$classeEtab,$teatcherid,$codeEtab,$session)
      {
          $req = $this->db->prepare("INSERT INTO fiche SET date_fiche=?,mat_fiche=?,classe_fiche=?,teatcher_fiche=?,codeEtab_fiche=?,session_fiche=?");
          $req->execute([$datecrea,$matiereid,$classeEtab,$teatcherid,$codeEtab,$session]);
          $idfiche=$this->db->lastInsertId();
          return $idfiche;

      }

      function UpdateProgrammeAcademiqueWithOutFile($programmeid,$programme,$descri,$codeEtab)
      {
        $req= $this->db->prepare("UPDATE programme SET libelle_prog=?,descri_prog=? where id_prog=? and codeEtab_prog=?");
        $req->execute([$programme,$descri,$programmeid,$codeEtab]);

      }

      function UpdateProgrammeAcademiqueWithFile($programmeid,$programme,$descri,$fichierad,$codeEtab)
      {
          $req= $this->db->prepare("UPDATE programme SET libelle_prog=?,descri_prog=?,fichier_prog=? where id_prog=? and codeEtab_prog=?");
          $req->execute([$programme,$descri,$fichierad,$programmeid,$codeEtab]);


      }

      function getAllprogrammesOfTeatcherClasses($IdCompte,$session)
      {
        $req = $this->db->prepare("SELECT * FROM programme,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and programme.idprof_prog=enseignant.idcompte_enseignant and programme.idmat_prog=matiere.id_mat and programme.idclasse_prog=classe.id_classe and enseignant.idcompte_enseignant=? and programme.session_prog=? order by classe.libelle_classe ASC");
        $req->execute([$IdCompte,$session]);
        return $req->fetchAll();
      }

      function getAllprogrammesOfTeatcherClassesAndpgrmeId($IdCompte,$programme,$session)
      {
        $req = $this->db->prepare("SELECT * FROM programme,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and programme.idprof_prog=enseignant.idcompte_enseignant and programme.idmat_prog=matiere.id_mat and programme.idclasse_prog=classe.id_classe and enseignant.idcompte_enseignant=? and programme.id_prog=? and programme.session_prog=? ");
        $req->execute([$IdCompte,$programme,$session]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);

        $donnees=$someArray[0]["id_prog"]."*".$someArray[0]["libelle_prog"]."*".$someArray[0]["descri_prog"]."*".$someArray[0]["idprof_prog"]."*".$someArray[0]["idmat_prog"]."*".$someArray[0]["idclasse_prog"];
        $donnees.="*".$someArray[0]["codeEtab_prog"]."*".$someArray[0]["fichier_prog"]."*".$someArray[0]["years_prog"];
        return $donnees;

      }

      function AddprogrammeClasseMatTeatcher($programme,$descri,$teatcherid,$matiereid,$classeEtab,$codeEtab,$statut,$datecrea,$fichierad,$years,$session)
      {
        $req = $this->db->prepare("INSERT INTO programme SET libelle_prog=?,descri_prog=?,idprof_prog=?,idmat_prog=?,idclasse_prog=?,codeEtab_prog=?,statut_prog=?,date_prog=?,fichier_prog=?,years_prog=?,session_prog=?");
        $req->execute([
    $programme,$descri,$teatcherid,$matiereid,$classeEtab,$codeEtab,$statut,$datecrea,$fichierad,$years,$session
        ]);



      }

      function getNumberofPgrmeClasse($classeEtab,$matclasse,$codeEtab,$teatcher)
      {
          $req = $this->db->prepare("SELECT * FROM programme where idclasse_prog=? and idmat_prog=? and codeEtab_prog=? and idprof_prog=?");
          $req->execute([$classeEtab,$matclasse,$codeEtab,$teatcher]);
          $data=$req->fetchAll();
          $nb=count($data);

          return $nb;

      }

      function getAllMatiereTeatchByThisTeatcherOfClasses($teatcherId,$classe)
      {
          $req = $this->db->prepare("SELECT * FROM matiere,classe where matiere.classe_mat=classe.id_classe and matiere.teatcher_mat=? and matiere.classe_mat=? ");
          $req->execute([$teatcherId,$classe]);
          return $req->fetchAll();
      }

      function getNbMatiereTeatchByThisTeatcherOfClasses($teatcherId,$classe,$code)
      {
          $req = $this->db->prepare("SELECT * FROM matiere,classe where matiere.classe_mat=classe.id_classe and matiere.teatcher_mat=? and matiere.classe_mat=? and classe.codeEtab_classe=? ");
          $req->execute([$teatcherId,$classe,$code]);
          $data=$req->fetchAll();
          $nb=count($data);

          return $nb;
      }

      function getAllMatiereTeatchByThisTeatcherOfClassesSchool($teatcherId,$classe,$code)
      {
        $req = $this->db->prepare("SELECT * FROM matiere,classe where matiere.classe_mat=classe.id_classe and matiere.teatcher_mat=? and matiere.classe_mat=? and classe.codeEtab_classe=? ");
        $req->execute([$teatcherId,$classe,$code]);
        return $req->fetchAll();
      }

      function getCodeEtabofMatiereChoosen($teatcherId,$classe,$matiere)
      {
        $req = $this->db->prepare("SELECT * FROM matiere where teatcher_mat=? and classe_mat=? and 	id_mat=?");
        $req->execute([$teatcherId,$classe,$matiere]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);

        $donnees=$someArray[0]["codeEtab_mat"];
        return $donnees;

      }

function getNumberOfTeatcherRoutine($IdCompte)
{
  $req = $this->db->prepare("SELECT * FROM routine,classe,matiere,enseignant,dispenser where routine.matiere_route=matiere.id_mat and routine.classe_route=classe.id_classe and dispenser.id_enseignant=enseignant.idcompte_enseignant and dispenser.id_cours=matiere.id_mat and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=?");
  $req->execute([$IdCompte]);
  $data=$req->fetchAll();
  $nb=count($data);

  return $nb;
}
function getAllnotesOfExamensClasses($classe,$matiere,$teatcher,$examenid,$type)
{
  $req = $this->db->prepare("SELECT * FROM examen,notes,classe,matiere,enseignant,eleve where notes.ideleve_notes=eleve.idcompte_eleve and  notes.idtype_notes=examen.id_exam and notes.idclasse_notes=classe.id_classe and notes.idmat_notes=matiere.id_mat and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=? and notes.idmat_notes=? and notes.idprof_notes=? and notes.idtype_notes=? and notes.type_notes=? order by eleve.nom_eleve ASC");
  $req->execute([$classe,$matiere,$teatcher,$examenid,$type]);
  return $req->fetchAll();
}

function getAllnoteControlesClasses($classe,$matiere,$teatcher,$controleid,$type)
{
    $req = $this->db->prepare("SELECT * FROM controle,notes,classe,matiere,enseignant,eleve where notes.ideleve_notes=eleve.idcompte_eleve and notes.idtype_notes=controle.id_ctrl and notes.idclasse_notes=classe.id_classe and notes.idmat_notes=matiere.id_mat and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=? and notes.idmat_notes=? and notes.idprof_notes=? and notes.idtype_notes=? and notes.type_notes=? order by eleve.nom_eleve ASC ");
    $req->execute([$classe,$matiere,$teatcher,$controleid,$type]);
    return $req->fetchAll();
}

function getStatusOfControleClasseMat($classe,$code,$matiere,$statut)
{
  $req = $this->db->prepare("SELECT * FROM controle where classe_ctrl=? and codeEtab_ctrl=? and mat_ctrl=? and statut_ctrl=?");
   $req->execute([$classe,$code,$matiere,$statut]);
   $data=$req->fetchAll();
   $nb=count($data);

   return $nb;
}

function getStatusOfControleClasseMatTeatcher($classe,$code,$matiere,$statut,$teatcherid)
{
  $req = $this->db->prepare("SELECT * FROM controle where classe_ctrl=? and codeEtab_ctrl=? and mat_ctrl=? and statut_ctrl=? and teatcher_ctrl=?");
   $req->execute([$classe,$code,$matiere,$statut,$teatcherid]);
   $data=$req->fetchAll();
   $nb=count($data);

   return $nb;
}



function getNumberOfNotesSchoolClasses($classe,$code,$matiere,$type)
{
  $req = $this->db->prepare("SELECT * FROM examen,notes where notes.idtype_notes=examen.id_exam and notes.type_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and notes.codeEtab_notes=? ");
  $req->execute([$type,$classe,$matiere,$code]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;

}

function getNumberOfNotesSchoolClassesTeatcher($classe,$code,$matiere,$type,$teatcherid)
{
  $req = $this->db->prepare("SELECT * FROM examen,notes where notes.idtype_notes=examen.id_exam and notes.type_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and notes.codeEtab_notes=? and notes.idprof_notes=? ");
  $req->execute([$type,$classe,$matiere,$code,$teatcherid]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;

}


function getNumberOfNotesSchoolClassesExamen($classe,$code,$matiere,$type,$idExam)
{
  $req = $this->db->prepare("SELECT * FROM examen,notes where notes.idtype_notes=examen.id_exam and notes.type_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and notes.codeEtab_notes=? and examen.id_exam=? ");
  $req->execute([$type,$classe,$matiere,$code,$idExam]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;

}

function getNumberOfNotesSchoolClassesExamenTeatcher($classe,$code,$matiereid,$type,$idExam,$teatcherid)
{
  $req = $this->db->prepare("SELECT * FROM examen,notes where notes.idtype_notes=examen.id_exam and notes.type_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and notes.codeEtab_notes=? and examen.id_exam=? and notes.idprof_notes=?");
  $req->execute([$classe,$code,$matiereid,$type,$idExam,$teatcherid]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;

}

function getAllExamOfThisValue($concatidExamwithoutNotes,$code)
{
  $req = $this->db->prepare("SELECT * FROM examen WHERE codeEtab_exam=? and id_exam in($concatidExamwithoutNotes) ");
  $req->execute([$code]);
  // $req->execute([]);
  return $req->fetchAll();
}

function getAllCountriesOfSystem()
{
    $req = $this->db->prepare("SELECT * FROM pays ");
    $req->execute([]);
    return $req->fetchAll();

}


function getAllExamenOfSchoolClasses($classe,$code,$matiere,$type)
{
  $req = $this->db->prepare("SELECT distinct matiere.id_mat,enseignant.idcompte_enseignant,examen.id_exam,examen.libelle_exam FROM matiere,enseignant,examen,notes where notes.idmat_notes=matiere.id_mat and notes.idprof_notes=enseignant.idcompte_enseignant and  notes.idtype_notes=examen.id_exam and notes.type_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and notes.codeEtab_notes=? ");
  $req->execute([$type,$classe,$matiere,$code]);
  return $req->fetchAll();
}

function getAllExamenOfSchoolClassesTeatcher($classe,$code,$matiere,$type,$teatcherid)
{
  $req = $this->db->prepare("SELECT distinct matiere.id_mat,enseignant.idcompte_enseignant,examen.id_exam,examen.libelle_exam FROM matiere,enseignant,examen,notes where notes.idmat_notes=matiere.id_mat and notes.idprof_notes=enseignant.idcompte_enseignant and  notes.idtype_notes=examen.id_exam and notes.type_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and notes.codeEtab_notes=? and enseignant.idcompte_enseignant=? ");
  $req->execute([$type,$classe,$matiere,$code,$teatcherid]);
  return $req->fetchAll();
}

function getAllControlesOfThisClassesSchool($classe,$code,$matiere,$statut)
{
   $req = $this->db->prepare("SELECT * FROM controle,classe,enseignant,matiere where controle.mat_ctrl=matiere.id_mat and  controle.classe_ctrl=classe.id_classe and controle.teatcher_ctrl=enseignant.idcompte_enseignant and  classe_ctrl=? and codeEtab_ctrl=? and mat_ctrl=? and statut_ctrl=?");
   $req->execute([$classe,$code,$matiere,$statut]);
   return $req->fetchAll();

}

function getAllControlesOfThisClassesSchoolTeatcher($classe,$code,$matiere,$statut,$teatcher)
{
   $req = $this->db->prepare("SELECT * FROM controle,classe,enseignant,matiere where controle.mat_ctrl=matiere.id_mat and  controle.classe_ctrl=classe.id_classe and controle.teatcher_ctrl=enseignant.idcompte_enseignant and  classe_ctrl=? and codeEtab_ctrl=? and mat_ctrl=? and statut_ctrl=? and enseignant.idcompte_enseignant=?");
   $req->execute([$classe,$code,$matiere,$statut,$teatcher]);
   return $req->fetchAll();

}

function getAllexamensOfSchool($codeEtabAssigner)
{
  $req = $this->db->prepare("SELECT * FROM examen where codeEtab_exam=?");
   $req->execute([$codeEtabAssigner]);
   return $req->fetchAll();
}

function getNumberOfEtablissement()
{
  $req = $this->db->prepare("SELECT * FROM etablissement");
   $req->execute([]);
   $data=$req->fetchAll();
   $nb=count($data);

   return $nb;
}

function getAllControleOfThisClassesOfSchool($notetype,$classeEtab,$codeEtab)
{
  $req = $this->db->prepare("SELECT * FROM classe,controle,matiere,enseignant where controle.mat_ctrl=matiere.id_mat and controle.teatcher_ctrl=enseignant.idcompte_enseignant and controle.classe_ctrl=classe.id_classe and classe.codeEtab_classe=? and classe.id_classe=? and controle.statut_ctrl=0");
   $req->execute([$codeEtab,$classeEtab]);
   return $req->fetchAll();
}

function getAllDesignationNotesOfClasses($notetype,$classeEtab,$codeEtab)
{
  //$req = $this->db->prepare("SELECT * FROM classe,notes,matiere,enseignant,controle where controle.statut_ctrl=1 and classe.id_classe=notes.idclasse_notes and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idmat_notes=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.type_notes=1 and  notes.idtype_notes=? and notes.idclasse_notes=? and notes.codeEtab_notes=?  ");
  $req = $this->db->prepare("SELECT distinct id_ctrl,mat_ctrl,teatcher_ctrl,libelle_ctrl FROM controle,classe,matiere,enseignant,notes where controle.statut_ctrl=1 and controle.codeEtab_ctrl=? and controle.classe_ctrl=classe.id_classe and controle.mat_ctrl=matiere.id_mat and controle.teatcher_ctrl=enseignant.idcompte_enseignant and notes.idclasse_notes=classe.id_classe and classe.id_classe=? and notes.type_notes=1");
  $req->execute([$codeEtab,$classeEtab]);
   return $req->fetchAll();
}

function getAllDesignationNotesOfClassesE($notetype,$classeEtab,$codeEtab)
{
  $req = $this->db->prepare("SELECT * FROM classe,notes,matiere,enseignant,examen where  classe.id_classe=notes.idclasse_notes and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idmat_notes=matiere.id_mat and notes.idtype_notes=examen.id_exam and notes.type_notes=2 and  notes.idtype_notes=? and notes.idclasse_notes=? and notes.codeEtab_notes=? ");
  $req->execute([$notetype,$classeEtab,$codeEtab]);
   return $req->fetchAll();
}

function AddExamenSchool($exam,$datedeb,$datefin,$codeEtab,$session,$typesess)
{
  $req = $this->db->prepare("INSERT INTO examen SET libelle_exam=?,du_exam=?,au_exam=?,codeEtab_exam=?,session_exam=?,typesess_exam=?");
  $req->execute([
$exam,$datedeb,$datefin,$codeEtab,$session,$typesess
  ]);

  $_SESSION['user']['updateExamok']="Examen ajouté avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

        //header("Location:../manager/index.php");
        header("Location:../manager/examens.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale") {

      header("Location:../locale/examens.php");

      }

}
function supprimerExamenOfthisSchool($examen,$codeEtab)
{
  $req = $this->db->prepare("DELETE FROM examen where id_exam=? and codeEtab_exam=?");
  $req->execute([
  $examen,$codeEtab
  ]);

  $_SESSION['user']['updateExamok']="Examen supprimer avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

        //header("Location:../manager/index.php");
        header("Location:../manager/examens.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale") {

      header("Location:../locale/examens.php");

      }
}

function getAllExamensOfThisSchool($codeEtabLocal)
{
  $req = $this->db->prepare("SELECT * FROM examen where codeEtab_exam=? ");
  $req->execute([$codeEtabLocal]);
  return $req->fetchAll();

}

function UpdateExamenOfClasses($exam,$datedeb,$datefin,$codeEtab,$idexam)
{
  $req= $this->db->prepare("UPDATE examen SET libelle_exam=?,du_exam=?,au_exam=? where codeEtab_exam=? and id_exam=?");
  $req->execute([
  $exam,$datedeb,$datefin,$codeEtab,$idexam
  ]);

  $_SESSION['user']['updateExamok']="Informations Examen modifié avec succès";
  if($_SESSION['user']['profile'] == "Admin_globale") {

        //header("Location:../manager/index.php");
        header("Location:../manager/examens.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale") {

      header("Location:../locale/examens.php");

      }
}

function AllreadyExistExamens($examen,$datedeb,$datefin,$codeEtab)
{
  $req = $this->db->prepare("SELECT * FROM examen where libelle_exam=? and du_exam=? and au_exam=? and codeEtab_exam=? ");
  $req->execute([$examen,$datedeb,$datefin,$codeEtab]);
  $data=$req->fetchAll();
  $nb=count($data);

  return $nb;
}

function UpdateControleClasseSchool($classe,$matiere,$teatcher,$controle,$codeEtab,$coef,$datectrl,$idctrl)
{
  $req= $this->db->prepare("UPDATE controle SET classe_ctrl=?,mat_ctrl=?,teatcher_ctrl=?,libelle_ctrl=?,coef_ctrl=?,date_ctrl=? where codeEtab_ctrl=? and id_ctrl=?");
  $req->execute([
  $classe,
  $matiere,
  $teatcher,
  $controle,
  $coef,
  $datectrl,
  $codeEtab,
  $idctrl
  ]);

$_SESSION['user']['addctrleok']=L::ControlsModMessageSuccess;
if($_SESSION['user']['profile'] == "Admin_globale") {

      //header("Location:../manager/index.php");
      header("Location:../manager/controles.php");

  }else if($_SESSION['user']['profile'] == "Admin_locale") {

    header("Location:../locale/controles.php");

  }else if($_SESSION['user']['profile'] == "Teatcher") {

      header("Location:../teatcher/controles.php");

      }

}

function deletedControleClassesSchool($controleid,$codeEtab,$classe,$matiere)
{
    $req=$this->db->prepare("DELETE FROM controle where id_ctrl=? and codeEtab_ctrl=? and classe_ctrl=? and mat_ctrl=? ");
    $req->execute([
  $controleid,$codeEtab,$classe,$matiere
    ]);
    $_SESSION['user']['addctrleok']=L::ControlsDelMessageSuccess;

    if($_SESSION['user']['profile'] == "Admin_globale") {

          //header("Location:../manager/index.php");
          header("Location:../manager/controles.php");

      }else if($_SESSION['user']['profile'] == "Admin_locale") {

        header("Location:../locale/controles.php");

      }else if($_SESSION['user']['profile'] == "Teatcher") {

          header("Location:../teatcher/controles.php");

          }
}

function UpdateRoutine($classe,$Etab,$heuredeb,$heurefin,$matiere,$day,$idroutine)
{
    //$req= $this->db->prepare("UPDATE routine SET classe_route=?,debut_route=?,fin_route=?,matiere_route=?,day_route where etab_route=? and id_route=?");
    $req= $this->db->prepare("UPDATE routine SET classe_route=?,debut_route=?,fin_route=?,matiere_route=?,day_route=? where etab_route=? and id_route=?");

    $req->execute([
    $classe,
   $heuredeb,
    $heurefin,
    $matiere,
     $day,
    $Etab,
    $idroutine
    ]);

    // routine modifier avec succès



}

function getRouteInfosByIdRoute($idroute)
{
  $req = $this->db->prepare("SELECT * from routine where id_route=?");
  $req->execute([$idroute]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);

  $donnees=$someArray[0]["classe_route"]."*".$someArray[0]["matiere_route"]."*".$someArray[0]["day_route"]."*".$someArray[0]["debut_route"]."*".$someArray[0]["fin_route"]."*".$someArray[0]["etab_route"];
  return $donnees;
}

function getspecificRoutine($id_days,$short_days,$classe)
{
  $req = $this->db->prepare("SELECT * from daysweek,routine,matiere where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=? and daysweek.short_days=? order by debut_route ASC");
  $req->execute([$classe,$short_days]);
  return $req->fetchAll();
}

function getspecificRoutineTeatcher($id_days,$short_days,$classe,$idteatcher)
{
  $req = $this->db->prepare("SELECT * FROM routine,matiere,enseignant,dispenser,classe where routine.matiere_route=matiere.id_mat and enseignant.idcompte_enseignant=matiere.teatcher_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and dispenser.id_cours=matiere.id_mat and classe.id_classe=routine.classe_route and classe.id_classe=? and routine.day_route=? and enseignant.idcompte_enseignant=? order by debut_route ASC");
  $req->execute([$classe,$short_days,$idteatcher]);
  $data=$req->fetchAll();
$content="";
  foreach ($data as $value) {
    $content.='<div class="btn-group" role="group" aria-label="Button group with nested dropdown">';
    $content.='<div class="btn-group" role="group">';
    $content.='  <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
    $content.='  <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
    $content.=$value->libelle_mat."(".$value->debut_route." - ".$value->fin_route.")";
    $content.='</button>&nbsp; &nbsp;';

  }
  return $content;
}

      function getNumberofRoutinebyIdroute($id_days,$short_days,$classe)
      {
        $req = $this->db->prepare("SELECT * from daysweek,routine where  routine.day_route=daysweek.short_days and routine.classe_route=? and daysweek.short_days=? ");
        $req->execute([$classe,$short_days]);
        $data=$req->fetchAll();
        $nb=count($data);

        return $nb;
      }

      function getAllweeks()
      {
        $req = $this->db->prepare("SELECT * from daysweek");
        $req->execute([]);

        return $req->fetchAll();


      }

      function getAllRoutineByClassesandTheDay($classe,$day)
      {
            $req = $this->db->prepare("SELECT * from routine where classe_route=? and day_route=? order by debut_route ASC");
            $req->execute([
            $classe,
            $day
            ]);

            return $req->fetchAll();
      }

      function AddRoutine($classe,$Etab,$heuredeb,$heurefin,$matiere,$day,$session)
      {
        $req = $this->db->prepare("INSERT INTO routine SET classe_route=?,etab_route=?,debut_route=?,fin_route=?,matiere_route=?,day_route=?,session_route=?");
        $req->execute([
        $classe,
        $Etab,
        $heuredeb,
        $heurefin,
        $matiere,
        $day,
        $session
        ]);

        //routine ajouté avec succès

        $_SESSION['user']['addroutineok']=" Routine ajoutée avec succès";

         // header("Location:../manager/matieres.php?classe=".$classe);
         if($_SESSION['user']['profile'] == "Admin_globale") {

               //header("Location:../manager/index.php");
               header("Location:../manager/addroutines.php");

           }else if($_SESSION['user']['profile'] == "Admin_locale") {

             header("Location:../locale/addroutines.php");

             }
      }

function verifyRoutineExistwithThisSubject($heuredeb,$heurefin,$day,$classe,$codeEtab,$matiere)
{
  $req = $this->db->prepare("SELECT * FROM routine where (debut_route>=? and fin_route<=?) and day_route=? and classe_route=? and etab_route=? and matiere_route=?");
  $req->execute([
    $heuredeb,
    $heurefin,
    $day,
    $classe,
    $codeEtab,
    $matiere
  ]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function verifyRoutineExistInTimePeriode($heuredeb,$heurefin,$day,$classe,$codeEtab)
{
    $req = $this->db->prepare("SELECT * FROM routine where (debut_route>=? and fin_route<=?) and day_route=? and classe_route=? and etab_route=?");
    $req->execute([
      $heuredeb,
      $heurefin,
      $day,
      $classe,
      $codeEtab
  ]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getAllsubjectofclassesbyIdclasses($classe,$code)
{
    $req = $this->db->prepare("SELECT * FROM matiere where classe_mat=? and codeEtab_mat=?");
    $req->execute([
    $classe,
    $code
  ]);
   return $req->fetchAll();
}
function getAllsubjectofclassesbyIdclassesAndTeatcherId($classe,$code)
{
    $req = $this->db->prepare("SELECT * FROM matiere where classe_mat=? and codeEtab_mat=?");
    $req->execute([
    $classe,
    $code
  ]);
   return $req->fetchAll();
}

function UpdateMatiere($matiere,$classe,$teatcher,$codeEtab,$coefficient,$idmat)
{
  $req = $this->db->prepare("UPDATE matiere SET libelle_mat=?,classe_mat=?,teatcher_mat=?,coef_mat=? where codeEtab_mat=? and id_mat=?");
  $req->execute([
  $matiere,
  $classe,
  $teatcher,
  $coefficient,
  $codeEtab,
  $idmat
]);

  //modification dans la table dispenser

  $reqx= $this->db->prepare("UPDATE dispenser SET id_enseignant=?,idclasse_disp=? where codeEtab=? and id_cours=?");
  $reqx->execute([
  $teatcher,
  $classe,
  $codeEtab,
  $idmat
  ]);

  $_SESSION['user']['updatesubjectok']=" Matière modidié avec succès";

   // header("Location:../manager/matieres.php?classe=".$classe);
   if($_SESSION['user']['profile'] == "Admin_globale") {

         //header("Location:../manager/index.php");
         header("Location:../manager/matieres.php?classe=".$classe);

     }else if($_SESSION['user']['profile'] == "Admin_locale") {

       header("Location:../locale/matieres.php?classe=".$classe);

       }

}

function AddMatiere($matiere,$classe,$teatcher,$codeEtab,$coefficient,$session)
{
  $req = $this->db->prepare("INSERT INTO matiere SET libelle_mat=?,classe_mat=?,teatcher_mat=?,	codeEtab_mat=?,coef_mat=?,session_mat=?");
  $req->execute([
  $matiere,
  $classe,
  $teatcher,
  $codeEtab,
  $coefficient,
  $session
]);

//insertion dans la table dispenser
$idcours=$this->db->lastInsertId();

$reqx= $this->db->prepare("INSERT INTO dispenser SET id_enseignant=?,id_cours=?,idclasse_disp=?,codeEtab=?,session_disp=?");
$reqx->execute([
$teatcher,
$idcours,
$classe,
$codeEtab,
$session
]);


  $_SESSION['user']['addsubjectok']="Nouvelle Matière ajoutée avec succès";

   // header("Location:../manager/matieres.php?classe=".$classe);
   if($_SESSION['user']['profile'] == "Admin_globale") {

         //header("Location:../manager/index.php");
         header("Location:../manager/schoolInfos.php?compte=".$codeEtab);

     }else if($_SESSION['user']['profile'] == "Admin_locale") {

       header("Location:../locale/matieres.php");

       }
}

  function DelatedMatiere($idmat,$classe,$codeEtab)
  {
    $req = $this->db->prepare("DELETE FROM dispenser where id_cours=? and codeEtab=? and idclasse_disp=? ");
    $req->execute([$idmat,$codeEtab,$classe]);

    $reqx = $this->db->prepare("DELETE FROM matiere where id_mat=? ");
    $reqx->execute([$idmat]);

    //supprimer la table dispenser


    $_SESSION['user']['addsubjectok']="Matière supprimé avec succès";

     // header("Location:../manager/matieres.php?classe=".$classe);
     if($_SESSION['user']['profile'] == "Admin_globale") {

           //header("Location:../manager/index.php");
           // header("Location:../manager/matieres.php?classe=".$classe);
           header("Location:../manager/schoolInfos.php?compte=".$codeEtab);

       }else if($_SESSION['user']['profile'] == "Admin_locale") {

         header("Location:../locale/matieres.php");

         }

  }

  function ExistControleAllready($matiere,$classe,$teatcher,$codeEtab,$coefficient,$datectrl)
  {
    $req = $this->db->prepare("SELECT * FROM controle where mat_ctrl=? and classe_ctrl=? and teatcher_ctrl=? and codeEtab_ctrl=? and coef_ctrl=? and date_ctrl=? ");
    $req->execute([$matiere,$classe,$teatcher,$codeEtab,$coefficient,$datectrl]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;
  }

  function AddControleClasseSchool($controle,$datectrl,$classe,$matiere,$teatcher,$coef,$codeEtab,$session,$typesess)
  {
    $statut=0;
    $req = $this->db->prepare("INSERT INTO controle SET libelle_ctrl=?,date_ctrl=?,classe_ctrl=?,mat_ctrl=?,teatcher_ctrl=?,coef_ctrl=?,codeEtab_ctrl=?,statut_ctrl=?,session_ctrl=?,typesess_ctrl=?");
    $req->execute([
  $controle,
  $datectrl,
  $classe,
  $matiere,
  $teatcher,
  $coef,
  $codeEtab,
  $statut,
  $session,
  $typesess
  ]);

  $_SESSION['user']['addctrleok']=L::ControlsAddMessageSuccess;

   // header("Location:../manager/matieres.php?classe=".$classe);
   if($_SESSION['user']['profile'] == "Admin_globale") {

         //header("Location:../manager/index.php");
         header("Location:../manager/controles.php");

     }else if($_SESSION['user']['profile'] == "Admin_locale") {

       header("Location:../locale/controles.php");

       }else if($_SESSION['user']['profile'] == "Teatcher") {

         header("Location:../teatcher/controles.php");

         }


  }

      function ExistMatiereAllready($matiere,$classe,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM matiere where libelle_mat=? and classe_mat=? and codeEtab_mat=? ");
        $req->execute([$matiere,$classe,$codeEtab]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function ExistMatiereWithSameProf($matiere,$classe,$codeEtab,$teatcher)
      {
        $req = $this->db->prepare("SELECT * FROM matiere where libelle_mat=? and classe_mat=? and codeEtab_mat=? and teatcher_mat=? ");
        $req->execute([$matiere,$classe,$codeEtab,$teatcher]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function getcodeEtabByLocalId($userId)
      {
        $req = $this->db->prepare("SELECT * FROM assigner  where id_adLocal=?");
         $req->execute([$userId]);
         $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["codeEtab_assign"];
            return $donnees;

      }
      function suppressionClasse($idclasse)
      {
        $req = $this->db->prepare("DELETE FROM matiere where classe_mat=?");
        $req->execute([$idclasse]);

        $reqX = $this->db->prepare("DELETE FROM classe where 	id_classe=?");
        $reqX->execute([$idclasse]);

        $_SESSION['user']['delclasseok']="Classe supprimé avec succès";

        if($_SESSION['user']['profile'] == "Admin_globale") {
      header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
        // header("Location:../manager/addclasses.php");
          }else if($_SESSION['user']['profile'] == "Admin_locale") {

        header("Location:../locale/classes.php");

            }else if($_SESSION['user']['profile'] == "Teatcher") {

                  header("Location:../teatcher/index.php");

              }else if($_SESSION['user']['profile'] == "Student") {

                    header("Location:../student/index.php");

                }else if($_SESSION['user']['profile'] == "Parent") {

                      header("Location:../parent/index.php");

                  }


      }

      function getEtablissementbyCodeEtab($codeetab)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement  where code_etab=?");
         $req->execute([$codeetab]);
         return $req->fetchAll();
      }



      function getAllcodesEtab()
      {
        $req = $this->db->prepare("SELECT etablissement.code_etab,etablissement.libelle_etab FROM etablissement order by id_etab desc ");
         $req->execute();
         return $req->fetchAll();
      }

      function getAllcodesEtabBycodeEtab($codeEtabAssigner)
      {
        $req = $this->db->prepare("SELECT etablissement.code_etab,etablissement.libelle_etab FROM etablissement where etablissement.code_etab=?");
         $req->execute([$codeEtabAssigner]);
         return $req->fetchAll();
      }

      function getEtabInfosbyCode($etab)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement  where code_etab=?");
         $req->execute([$etab]);
         $data=$req->fetchAll();

  $array=json_encode($data,true);
  $someArray = json_decode($array, true);

  $donnees=$someArray[0]["code_etab"]."*".$someArray[0]["libelle_etab"]."*".$someArray[0]["tel_etab"]."*".$someArray[0]["tel1_etab"]."*".$someArray[0]["email_etab"]."*".$someArray[0]["adresse_etab"];
  $donnees.="*".$someArray[0]["logo_etab"];
  return $donnees;
      }

      function getAllEtab()
      {
        $req = $this->db->prepare("SELECT * FROM etablissement order by createby_etab desc  ");
         $req->execute();
         return $req->fetchAll();
      }

      function existEtab($codeetab)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement where code_etab=?");
        $req->execute([$codeetab]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function existEtabCountry($codeetab,$pays)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement where code_etab=? and pays_etab=?");
        $req->execute([$codeetab,$pays]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }



      function getAllAdminLocalBysearchCode($code)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement where code_etab=?  ");
         $req->execute([$code]);
         return $req->fetchAll();
      }

      function getAllAdminLocalBysearchperiode($datedu,$dateau)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement where datecrea_etab>=? and  datecrea_etab<=? ");
         $req->execute([$datedu,$dateau]);
         return $req->fetchAll();
      }


      function getDetailsOfStudent($matricule,$codeEtab,$session)
      {
        $req = $this->db->prepare("SELECT * FROM compte,eleve,classe,inscription where compte.id_compte=eleve.idcompte_eleve and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and eleve.matricule_eleve=? and inscription.codeEtab_inscrip=? and inscription.session_inscrip=?");
        $req->execute([$matricule,$codeEtab,$session]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["nom_eleve"]."*".$someArray[0]["prenom_eleve"]."*".$someArray[0]["libelle_classe"];
        return $donnees;
      }


}

?>
