<?php
session_start();
require_once('../class/Salle.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Etablissement.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();
$etabs=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$titre=htmlspecialchars(addslashes($_POST['titre']));
$codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));

$check=$session->ExisteSessionForThisSchool($titre,$codeEtab);

if($check==0)
{
  $content=0;
}else if($check>0)
{
  $content=1;
}

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
    //recuperation des variables

    $sessionid=htmlspecialchars(addslashes($_POST['sessionid']));
    $sessionlib=htmlspecialchars(addslashes($_POST['sessionlib']));
    $typesession=htmlspecialchars(addslashes($_POST['typesession']));
    $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
    $oldsessiontype=htmlspecialchars(addslashes($_POST['oldsessiontype']));

    $check=$session->UpdateSessionSchool($sessionid,$sessionlib,$typesession,$codeEtab);

    //insertion dans la table semestre

    $content=1;

    if($typesession==2)
    {
      //cas semestre

      for($i=1;$i<=$typesession;$i++)
      {
        $libellesemestre="SEMESTRE ".$i;
        $statut=1;
        //insertion dans la tables semestre
        $session->AddSemestre($libellesemestre,$sessionid,$statut,$codeEtab);
      }

    }else if($typesession==3)
    {
      //cas trimestre

      for($i=1;$i<=$typesession;$i++)
      {
        $libellesemestre="TRIMESTRE ".$i;
        $statut=1;
        //insertion dans la tables semestre
        $session->AddSemestre($libellesemestre,$sessionid,$statut,$codeEtab);
      }

    }

    $_SESSION['user']['Updateadminok']=L::Scolaryearsuccessfullyupdate;

    echo $content;
}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);

  $_SESSION['user']['codeEtab']=$codeEtab;
  $_SESSION['user']['groupeselect']=$codeEtab;

  $primareEtab=$etabs->getPrimaireEtabValue($codeEtab);
  $_SESSION['user']['primaire']=$primareEtab;

  echo $codeEtab;

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);

  $primareEtab=$etabs->getPrimaireEtabValue($codeEtab);
  $_SESSION['user']['primaire']=$primareEtab;

  // $_SESSION['user']['codeEtab']=$codeEtab;
  $_SESSION['user']['groupeselect']="ALL";
}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);

  echo $codeEtab;

 $primareEtab=$etabs->getPrimaireEtabValue($codeEtab);

 // echo $primareEtab;

 $_SESSION['user']['codeEtab']=$codeEtab;
 $_SESSION['user']['groupeselect']=$codeEtab;
  $_SESSION['user']['primaire']=$primareEtab;
  //

}



 ?>
