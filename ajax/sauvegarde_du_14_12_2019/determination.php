<?php

 session_start();

 require_once('../single/Etablissement.php');

 require_once('../single/Sessionsacade.php');

 $session= new Sessionacade();
 $Etabs=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1)){


	  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));

    $matricule=htmlspecialchars(addslashes($_POST['matricule']));


	  $content="";

	  $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtab);


		if($nbsessionOn>0)
		{
		  $sessionencours=$session->getSessionEncours($codeEtab);
		  $tabsessionencours=explode("*",$sessionencours);
		  $libellesessionencours=$tabsessionencours[0];
		  $sessionencoursid=$tabsessionencours[1];
		  $typesessionencours=$tabsessionencours[2];
		}


	//$nbdata=count($datastutent);
	$nbdata=$Etabs->getNbDetailsOfStudent($matricule,$codeEtab,$libellesessionencours);





	if($nbdata>0)
{
	$datastutent=$Etabs->getDetailsOfStudent($matricule,$codeEtab,$libellesessionencours);
  $content=$datastutent;
}else {
  $content=0;
}

echo $content;



}else if(isset($_POST['etape'])&&($_POST['etape']==2)){

  //recuperation des variables
  $pays=htmlspecialchars(addslashes($_POST['pays']));

  $devise=$Etabs->DeviseOfCountry($pays);

  echo $devise;

}else if(isset($_POST['etape'])&&($_POST['etape']==3)){

  //recuperation des variables
  $montanttotale=htmlspecialchars(addslashes($_POST['montanttotale']));
  $montant=htmlspecialchars(addslashes($_POST['montant']));

  $nouveaumontant=$montanttotale+$montant;
  echo $nouveaumontant;

}else if(isset($_POST['etape'])&&($_POST['etape']==4)){

  //recuperation des variables
  $montanttotale=htmlspecialchars(addslashes($_POST['montanttotale']));
  $montant=htmlspecialchars(addslashes($_POST['montant']));

  $nouveaumontant=$montanttotale-$montant;
  if($nouveaumontant<0)
  {
    $nouveaumontant=0;
  }
  echo $nouveaumontant;

}else if(isset($_POST['etape'])&&($_POST['etape']==5)){

  //recuperation des variables
  $pays=htmlspecialchars(addslashes($_POST['pays']));

  $content="";

  $Etabsdata=$Etabs->AllEtablissementOfCountry($pays);

  $nbligne=count($Etabsdata);

  if($nbligne==0)
  {
    $content.="<option value=''>Aucun Etablissement </option>";
  }else {
    //var_dump($datas);
    $content.="<option value='' selected>Selectionner un Etablissement</option>";

    foreach ($Etabsdata as $value):
        $content .= "<option value='".$value->code_etab."' >" . utf8_encode(utf8_decode($value->libelle_etab)). "</option>";
    endforeach;
  }

  echo $content;



}else if(isset($_POST['etape'])&&($_POST['etape']==6)){

  //recuperation des variables

  $pays=htmlspecialchars(addslashes($_POST['pays']));

  $content="";

  $mobileoperators=$Etabs->AllMobileoperatorsOfCountry($pays);

  $nbligne=count($mobileoperators);

  if($nbligne==0)
  {
    $content.="Aucun Opérateur Mobile";
  }else {
    //var_dump($datas);


    foreach ($mobileoperators as $value):
      $lienfileoperator="operateurs/".$value->file_mob;
      $numerocountry=$value->phone_mob;
        $content .= "<div class='col-lg-4'>
<div class='card m-b-30'>
<img class='card-img-top img-fluid' src='".$lienfileoperator."' alt='Card image cap'>
<div class='card-body'>
  <h4 class='card-title font-20 mt-0'>".$numerocountry."</h4>
  <p class='card-text'></p>
  <p class='card-text'>
  </p>
</div>
</div>
</div>";
    endforeach;
  }

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==7)){

  $pays=htmlspecialchars(addslashes($_POST['pays']));

  $content="";

  $mobileoperators=$Etabs->AllMobileoperatorsOfCountry($pays);

  $nbligne=count($mobileoperators);

  if($nbligne==0)
  {
    $content.="<option value=''>Aucun Opérateur mobile </option>";
  }else {
      foreach ($mobileoperators as $value):
          $content .= "<option value='".$value->id_mob."' >" . utf8_encode(utf8_decode($value->libelle_mob)). "</option>";
      endforeach;
  }

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==8)){

  $pays=htmlspecialchars(addslashes($_POST['pays']));

  $content="";

  $listeabonnements=$Etabs->AllAbonnementListeOfCountry($pays);
  $nbligne=count($listeabonnements);

  if($nbligne==0)
  {
    $content.="<option value=''>Aucun Abonnement </option>";
  }else {
    $content.="<option value=''>Selectionner un abonnement </option>";
      foreach ($listeabonnements as $value):
          $content .= "<option value='".$value->id_abn."-".$value->montant_abn."' >" . utf8_encode(utf8_decode($value->libelle_abn)). "</option>";
      endforeach;
  }
  echo $content;
}




 ?>
