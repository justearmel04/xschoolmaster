<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Student.php');
require_once('../class/Etablissement.php');

$session= new Sessionacade();
$classex= new Classe();
$student=new Student();
$etabs=new Etab();


if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recuperation des variables


  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $classe=htmlspecialchars(addslashes($_POST['classex']));
  $section=htmlspecialchars(addslashes($_POST['section']));
  $session=htmlspecialchars(addslashes($_POST['session']));

  $libelleclasse=$classex->getInfosofclassesbyId($classe,$session);

  //$lien=$student->generateficheLocalpdf($compte,$codeEtab);

  $lien=$student->generatescolariteclasselpdf($codeEtab,$classe,$section,$session,$libelleclasse);

  echo $lien;

}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recupération des variables

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $eleveid=htmlspecialchars(addslashes($_POST['eleveid']));
  $session=htmlspecialchars(addslashes($_POST['session']));
  $versementid=htmlspecialchars(addslashes($_POST['versementid']));

  $lien=$student->genereteVersement($codeEtab,$session,$eleveid,$versementid);

  echo $lien;

}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recuperation des variables

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $classeid=htmlspecialchars(addslashes($_POST['classe']));
  $session=htmlspecialchars(addslashes($_POST['session']));

  $lien=$student->generateRoutine($codeEtab,$classeid,$session);

  echo $lien;


}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //recuperation des variables

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $classeid=htmlspecialchars(addslashes($_POST['classeEtab']));
  $session=htmlspecialchars(addslashes($_POST['session']));
  $moisconcat=htmlspecialchars(addslashes($_POST['moisconcat']));
  $annee=htmlspecialchars(addslashes($_POST['annee']));

  $lien=$student->generateRecapattendance($codeEtab,$classeid,$session,$moisconcat,$annee);

echo $lien;
}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //recuperation des variables

    $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
    $session=htmlspecialchars(addslashes($_POST['session']));
    $classeEtab=htmlspecialchars(addslashes($_POST['classeEtab']));
    $typesession=htmlspecialchars(addslashes($_POST['typesession']));

    $libelleclasse=$classex->getInfosofclassesbyId($classeEtab,$session);

    $lien=$student->generateHonortabFile($codeEtab,$classeEtab,$session,$typesession,$libelleclasse);

    echo $lien;


}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  //recuperation des variables

  $message=htmlspecialchars(addslashes($_POST['message']));
  $parascolaire=htmlspecialchars(addslashes($_POST['parascolaire']));
  $precis=htmlspecialchars(addslashes($_POST['precis']));
  $addby=htmlspecialchars(addslashes($_POST['addby']));
  $denomination="";
  $datas=$etabs->getMessagesType($message,$addby);
  $tabdatas=explode("*",$datas);
  $parascolairedata=$tabdatas[0];
  $scolardata=$tabdatas[1];
  $content="";

  if($parascolairedata==1)
  {
    $content=$etabs->getparacolaireDesignation($message);
  }else if($scolardata==1)
  {

  }else if(($parascolairedata==0)&&($scolardata==0))
  {
    if($tabdatas[2]==8)
    {
      $content=$tabdatas[3];
    }else {
      $content=$tabdatas[4];
    }
  }

echo $content."/".$tabdatas[5];
  //
  // if($parascolaire==1)
  // {
  //
  // }else {
  //
  // }



}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{

  //recuperation des variables

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $classeid=htmlspecialchars(addslashes($_POST['classeEtab']));
  $session=htmlspecialchars(addslashes($_POST['session']));
  $datedeb=htmlspecialchars(addslashes($_POST['datedeb']));
  $nb=htmlspecialchars(addslashes($_POST['nb']));

  $lien=$student->generateRecapattendanceDay($codeEtab,$classeid,$session,$datedeb,$nb);

echo $lien;


}

 ?>
