<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Etablissement.php');
require_once('../class/Student.php');
require_once('../controller/functions.php');

$student= new Student();
$session= new Sessionacade();
$etabs= new Etab();
$classex= new Classe();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$codeEtab=htmlspecialchars($_POST['codeEtab']);
$classeEtab=htmlspecialchars($_POST['classeEtab']);
$sessionEtab=htmlspecialchars($_POST['session']);

//nous allons recuperer le required_fraisco

$required=$classex->getRequiredfraiscolairesCantine($classeEtab,$codeEtab,$sessionEtab);

echo $required;


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  $content="";
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $classeEtab=htmlspecialchars($_POST['classeEtab']);
  $sessionEtab=htmlspecialchars($_POST['session']);

  $datas=$classex->getAllCantinesRows($classeEtab,$codeEtab,$sessionEtab);


  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>Aucune Option de cantine</option>";
  }else {
    //var_dump($datas);
    $content.="<option value='' selected>Selectionner un frais de cantine</option>";

    foreach ($datas as $value):
        $content .= "<option value='". $value->id_fraisco ."' >" . utf8_encode(utf8_decode($value->libelle_fraisco))."   ( ".utf8_encode(utf8_decode(prixMill($value->montant_fraisco))). " )</option>";
    endforeach;
  }

  echo $content;


}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //nous allons rechercher le type de classe
  $content="";
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $classeEtab=htmlspecialchars($_POST['classeEtab']);
  $sessionEtab=htmlspecialchars($_POST['session']);

  $typeclasse=$classex->getTypeofclasseSession($codeEtab,$sessionEtab,$classeEtab);

  echo $typeclasse;

}



 ?>
