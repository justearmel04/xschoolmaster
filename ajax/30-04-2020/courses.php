<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Etablissement.php');
require_once('../class/Student.php');
require_once('../controller/functions.php');

$student= new Student();
$session= new Sessionacade();
$etabs= new Etab();
$classex= new Classe();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
//nous devons publier le cours
//recupération des variables

$content="";

$teatcherid=htmlspecialchars($_POST['teatcherid']);
$classeid=htmlspecialchars($_POST['classe']);
$courseid=htmlspecialchars($_POST['courseid']);
$matiereid=htmlspecialchars($_POST['matiere']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);

$etabs->AddcoursesPublication($courseid,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab);

//nous allons envoyer une notification aux parents et aux enfants de cette classe

$_SESSION['user']['addclasseok']="Le cours a été publié avec succès";


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //supprimer la section
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $sectionid=htmlspecialchars($_POST['sectionid']);

  $etabs->DeletedsectionCourses($sectionid,$courseid);

  $_SESSION['user']['addclasseok']="La section a bien été supprimé avec succès";

}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //supprimer la compétence
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $compid=htmlspecialchars($_POST['compid']);

  $etabs->DeletedcompCourses($compid,$courseid);

  $_SESSION['user']['addclasseok']="La compétence a bien été supprimée avec succès";


}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $homeworkid=htmlspecialchars($_POST['homeworkid']);

  $etabs->DeletedHomewkCourses($homeworkid,$courseid);

  $_SESSION['user']['addclasseok']="Un exercice a bien été supprimé avec succès";

}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //supprimer la section
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $libellesection=htmlspecialchars($_POST['section']);

  $etabs->AddCourseSection($libellesection,$courseid);

  $_SESSION['user']['addclasseok']="Nouvelle section ajouté avec succès";

}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  //supprimer la compétence
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $libellecomp=htmlspecialchars($_POST['libellecomp']);

  $etabs->AddCourseComp($libellecomp,$courseid);

  $_SESSION['user']['addclasseok']="Une nouvelle compétence a bien été ajoutée avec succès";


}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $libellehome=htmlspecialchars($_POST['libellehome']);

  $etabs->AddCourseHomework($libellehome,$courseid);

  $_SESSION['user']['addclasseok']="Un nouvel exercice a bien été ajouté avec succès";

}else if(isset($_POST['etape'])&&($_POST['etape']==8))
{

  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);

  $dateday=date("Y-m-d H:i:s");

  $etabs->StartcoursesDiscussion($courseid,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab,$dateday);

  //nous allons envoyer une notification aux parents et aux enfants de cette classe

  $_SESSION['user']['addclasseok']="La discussion du cours a été démarrée avec succès";

}else if(isset($_POST['etape'])&&($_POST['etape']==9))
{

  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);

  $dateday=date("Y-m-d H:i:s");

  $etabs->StopcoursesDiscussion($courseid,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab,$dateday);

  //nous allons envoyer une notification aux parents et aux enfants de cette classe

$_SESSION['user']['addclasseok']="La discussion du cours a été arretée avec succès";




}else if(isset($_POST['etape'])&&($_POST['etape']==10))
{

  $addby=htmlspecialchars($_POST['addby']);
  $classeid=htmlspecialchars($_POST['classe']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $commentaire=htmlspecialchars($_POST['commentaire']);
  $teatcherid=htmlspecialchars($_POST['teatcherid']);

  $etabs->Addcommentdiscussion($courseid,$addby,$commentaire,$classeid,$codeEtab,$sessionEtab,$matiereid,$teatcherid);

}else if(isset($_POST['etape'])&&($_POST['etape']==11))
{

  $studentid=htmlspecialchars($_POST['studentid']);
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);

  $etabs->AddcommentdiscussionOnlineStudent($courseid,$teatcherid,$studentid,$classeid,$matiereid,$codeEtab,$sessionEtab);


}else if(isset($_POST['etape'])&&($_POST['etape']==12))
{

  $oldonlines=htmlspecialchars($_POST['oldonlines']);
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);

  $content="";


  $datacoursescommentonlines=$etabs->getAllcoursescommentonlinesTea($courseid,$classeid,$codeEtab,$sessionEtab);
  $nbonlines=count($datacoursescommentonlines);

  if($oldonlines==$nbonlines)
  {
    $content=1;
  }else {
    $content=0;
  }

  echo $content."*".$nbonlines;




}else if(isset($_POST['etape'])&&($_POST['etape']==13))
{

  $oldonlines=htmlspecialchars($_POST['oldonlines']);
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);

  $content="";


  $datacoursescommentonlines=$etabs->getAllcoursescommentonlinesTea($courseid,$classeid,$codeEtab,$sessionEtab);
  $nbonlines=count($datacoursescommentonlines);

  if($nbonlines>0 && $nbonlines==1)
  {
    $content.="<div class=\"row\">";
    $content.=" <span class=\"text-purple\" style=\"width:1500px\">Abhay Jani</span>";
    $content.="<div>";
    $content.="<span class=\"clsAvailable\">En ligne</span>";

    $content.="</div>";

    $content.="<div class=\"col-md-4\">";
    $content.="<a href=\"javascript:;\" class=\"single-mail\"> <img src=\"../assets2/img/doc/doc1.jpg\" alt=\"\" width=\"40\" height=\"40\" class=\"pull-right\" style=\"border-radius:10px;margin-left:200px\"></a>";

    $content.="</div>";

    $content.="</div>";

  } else if($nbonlines>0 && $nbonlines>1)
  {
     foreach ($datacoursescommentonlines as  $value):

         $content.="<a href=\"javascript:;\" class=\"single-mail\"> <img src=\"../assets2/img/doc/doc1.jpg\" alt=\"\" width=\"40\" height=\"40\" class=\"pull-right\" style=\"border-radius:10px\"> <span class=\"text-purple\">".utf8_decode(utf8_encode($etabs->getUtilisateurName($value->id_compte)))."</span>";

         $content.="<div>";
         $content.="<span class=\"clsAvailable\">En ligne</span>";
         $content.="</div>";

         $content.="</a>";
     endforeach;
  }


echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==14))
{

  $oldonlines=htmlspecialchars($_POST['oldonlines']);
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);

  $content="";


  $datacoursescomments=$etabs->getAllcoursescommentsTea($courseid,$classeid,$codeEtab,$sessionEtab);
  $nbcomments=count($datacoursescomments);

  if($oldonlines==$nbcomments)
  {
    $content=1;
  }else {
    $content=0;
  }

  echo $content."*".$nbcomments;




}else if(isset($_POST['etape'])&&($_POST['etape']==15))
{

  $oldonlines=htmlspecialchars($_POST['oldonlines']);
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $compteuserid=$_SESSION['user']['IdCompte'];

  $content="";

  $datacoursescomments=$etabs->getAllcoursescommentsTea($courseid,$classeid,$codeEtab,$sessionEtab);
  $nbcomments=count($datacoursescomments);

  if($nbcomments>0)
  {
      foreach ($datacoursescomments as $value):

        if($value->addby_commentc==$compteuserid)
        {
           $content.="<li class=\"in\"><img src=\"../assets2/img/doc/doc1.jpg\" class=\"avatar\" alt=\"\">";
            $content.="<div class=\"message\">";
            $libesexe="";
            $sexe=$etabs->getUtilisateurSexe($value->addby_commentc);
            $dateadded=$value->date_commentc;
            $tabdateadded=explode(" ",$dateadded);

            $dateinsert=$tabdateadded[0];
            $tabdateinsert=explode("-",$dateinsert);
            $yearsinsert=$tabdateinsert[0];
            $monthinsert=$tabdateinsert[1];
            $daysinsert=$tabdateinsert[2];
            $newdate=$daysinsert."-".$monthinsert."-".$yearsinsert;

            if($sexe=="F")
            {
              $libesexe="Mme";
            }else {
              $libesexe="Mr";
            }

           $content.="  <span class=\"arrow\"></span> <a class=\"name\" href=\"#\">".$libesexe." ".tronquer(utf8_decode(utf8_encode($etabs->getUtilisateurName($value->addby_commentc))),20)."</a> <span class=\"datetime\">".$newdate." ".$tabdateadded[1] ."</span>";

           $content.="<span class=\"body\">".utf8_decode(utf8_encode($value->message_commentc))."</span>";

            $content.="</div>";

           $content.="</li>";
        }else {

          $content.="<li class=\"out\"><img src=\"../assets2/img/doc/doc1.jpg\" class=\"avatar\" alt=\"\">";
           $content.="<div class=\"message\">";
           $libesexe="";
           $sexe=$etabs->getUtilisateurSexe($value->addby_commentc);
           $dateadded=$value->date_commentc;
           $tabdateadded=explode(" ",$dateadded);

           $dateinsert=$tabdateadded[0];
           $tabdateinsert=explode("-",$dateinsert);
           $yearsinsert=$tabdateinsert[0];
           $monthinsert=$tabdateinsert[1];
           $daysinsert=$tabdateinsert[2];
           $newdate=$daysinsert."-".$monthinsert."-".$yearsinsert;

           if($sexe=="F")
           {
             $libesexe="Mme";
           }else {
             $libesexe="Mr";
           }

          $content.="  <span class=\"arrow\"></span> <a class=\"name\" href=\"#\">".$libesexe." ".utf8_decode(utf8_encode($etabs->getUtilisateurName($value->addby_commentc)))."</a> <span class=\"datetime\">".$newdate." ".$tabdateadded[1] ."</span>";

          $content.="<span class=\"body\">".utf8_decode(utf8_encode($value->message_commentc))."</span>";

           $content.="</div>";

          $content.="</li>";

        }

      endforeach;
  }

  echo $content;

//
//   $datacoursescommentonlines=$etabs->getAllcoursescommentonlinesTea($courseid,$classeid,$codeEtab,$sessionEtab);
//   $nbonlines=count($datacoursescommentonlines);
//
//   if($nbonlines>0 && $nbonlines==1)
//   {
//     $content.="<div class=\"row\">";
//     $content.=" <span class=\"text-purple\" style=\"width:1500px\">Abhay Jani</span>";
//     $content.="<div>";
//     $content.="<span class=\"clsAvailable\">En ligne</span>";
//
//     $content.="</div>";
//
//     $content.="<div class=\"col-md-4\">";
//     $content.="<a href=\"javascript:;\" class=\"single-mail\"> <img src=\"../assets2/img/doc/doc1.jpg\" alt=\"\" width=\"40\" height=\"40\" class=\"pull-right\" style=\"border-radius:10px;margin-left:200px\"></a>";
//
//     $content.="</div>";
//
//     $content.="</div>";
//
//   } else if($nbonlines>0 && $nbonlines>1)
//   {
//      foreach ($datacoursescommentonlines as  $value):
//
//          $content.="<a href=\"javascript:;\" class=\"single-mail\"> <img src=\"../assets2/img/doc/doc1.jpg\" alt=\"\" width=\"40\" height=\"40\" class=\"pull-right\" style=\"border-radius:10px\"> <span class=\"text-purple\">".utf8_decode(utf8_encode($etabs->getUtilisateurName($value->id_compte)))."</span>";
//
//          $content.="<div>";
//          $content.="<span class=\"clsAvailable\">En ligne</span>";
//          $content.="</div>";
//
//          $content.="</a>";
//      endforeach;
//   }
//
//
// echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==16))
{

  $oldonlines=htmlspecialchars($_POST['oldonlines']);
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $compteuserid=$_SESSION['user']['IdCompte'];

  $content="";

  $datacoursescomments=$etabs->getAllcoursescommentsTea($courseid,$classeid,$codeEtab,$sessionEtab);
  $nbcomments=count($datacoursescomments);

  if($nbcomments>0)
  {
      foreach ($datacoursescomments as $value):

        if($value->addby_commentc==$compteuserid)
        {
           $content.="<li class=\"in\"><img src=\"../assets2/img/doc/doc1.jpg\" class=\"avatar\" alt=\"\">";
            $content.="<div class=\"message\">";
            $libesexe="";
            $sexe=$etabs->getUtilisateurSexe($value->addby_commentc);
            $dateadded=$value->date_commentc;
            $tabdateadded=explode(" ",$dateadded);

            $dateinsert=$tabdateadded[0];
            $tabdateinsert=explode("-",$dateinsert);
            $yearsinsert=$tabdateinsert[0];
            $monthinsert=$tabdateinsert[1];
            $daysinsert=$tabdateinsert[2];
            $newdate=$daysinsert."-".$monthinsert."-".$yearsinsert;

            if($sexe=="F")
            {
              $libesexe="Mme";
            }else {
              $libesexe="Mr";
            }

           $content.="  <span class=\"arrow\"></span> <a class=\"name\" href=\"#\">".$libesexe." ".utf8_decode(utf8_encode($etabs->getUtilisateurName($value->addby_commentc)))."</a> <span class=\"datetime\">".$newdate." ".$tabdateadded[1] ."</span>";

           $content.="<span class=\"body\">".utf8_decode(utf8_encode($value->message_commentc))."</span>";

            $content.="</div>";

           $content.="</li>";
        } else if($value->addby_commentc==$value->teatcherid_commentc)
        {
          $content.="<li class=\"outin\"><img src=\"../assets2/img/doc/doc1.jpg\" class=\"avatar\" alt=\"\">";
           $content.="<div class=\"message\">";
           $libesexe="";
           $sexe=$etabs->getUtilisateurSexe($value->addby_commentc);
           $dateadded=$value->date_commentc;
           $tabdateadded=explode(" ",$dateadded);

           $dateinsert=$tabdateadded[0];
           $tabdateinsert=explode("-",$dateinsert);
           $yearsinsert=$tabdateinsert[0];
           $monthinsert=$tabdateinsert[1];
           $daysinsert=$tabdateinsert[2];
           $newdate=$daysinsert."-".$monthinsert."-".$yearsinsert;

           if($sexe=="F")
           {
             $libesexe="Mme";
           }else {
             $libesexe="Mr";
           }

          $content.="  <span class=\"arrow\"></span> <a class=\"name\" href=\"#\" style=\"color:white\">".$libesexe." ".tronquer(utf8_decode(utf8_encode($etabs->getUtilisateurName($value->addby_commentc))),20)."</a> <span class=\"datetime\" style=\"color:white\">".$newdate." ".$tabdateadded[1] ."</span>";

          $content.="<span class=\"body\">".utf8_decode(utf8_encode($value->message_commentc))."</span>";

           $content.="</div>";

          $content.="</li>";

        }else {

          $content.="<li class=\"out\"><img src=\"../assets2/img/doc/doc1.jpg\" class=\"avatar\" alt=\"\">";
           $content.="<div class=\"message\">";
           $libesexe="";
           $sexe=$etabs->getUtilisateurSexe($value->addby_commentc);
           $dateadded=$value->date_commentc;
           $tabdateadded=explode(" ",$dateadded);

           $dateinsert=$tabdateadded[0];
           $tabdateinsert=explode("-",$dateinsert);
           $yearsinsert=$tabdateinsert[0];
           $monthinsert=$tabdateinsert[1];
           $daysinsert=$tabdateinsert[2];
           $newdate=$daysinsert."-".$monthinsert."-".$yearsinsert;

           if($sexe=="F")
           {
             $libesexe="Mme";
           }else {
             $libesexe="Mr";
           }

          $content.="  <span class=\"arrow\"></span> <a class=\"name\" href=\"#\">".$libesexe." ".utf8_decode(utf8_encode($etabs->getUtilisateurName($value->addby_commentc)))."</a> <span class=\"datetime\">".$newdate." ".$tabdateadded[1] ."</span>";

          $content.="<span class=\"body\">".utf8_decode(utf8_encode($value->message_commentc))."</span>";

           $content.="</div>";

          $content.="</li>";

        }

      endforeach;
  }

  echo $content;

//
//   $datacoursescommentonlines=$etabs->getAllcoursescommentonlinesTea($courseid,$classeid,$codeEtab,$sessionEtab);
//   $nbonlines=count($datacoursescommentonlines);
//
//   if($nbonlines>0 && $nbonlines==1)
//   {
//     $content.="<div class=\"row\">";
//     $content.=" <span class=\"text-purple\" style=\"width:1500px\">Abhay Jani</span>";
//     $content.="<div>";
//     $content.="<span class=\"clsAvailable\">En ligne</span>";
//
//     $content.="</div>";
//
//     $content.="<div class=\"col-md-4\">";
//     $content.="<a href=\"javascript:;\" class=\"single-mail\"> <img src=\"../assets2/img/doc/doc1.jpg\" alt=\"\" width=\"40\" height=\"40\" class=\"pull-right\" style=\"border-radius:10px;margin-left:200px\"></a>";
//
//     $content.="</div>";
//
//     $content.="</div>";
//
//   } else if($nbonlines>0 && $nbonlines>1)
//   {
//      foreach ($datacoursescommentonlines as  $value):
//
//          $content.="<a href=\"javascript:;\" class=\"single-mail\"> <img src=\"../assets2/img/doc/doc1.jpg\" alt=\"\" width=\"40\" height=\"40\" class=\"pull-right\" style=\"border-radius:10px\"> <span class=\"text-purple\">".utf8_decode(utf8_encode($etabs->getUtilisateurName($value->id_compte)))."</span>";
//
//          $content.="<div>";
//          $content.="<span class=\"clsAvailable\">En ligne</span>";
//          $content.="</div>";
//
//          $content.="</a>";
//      endforeach;
//   }
//
//
// echo $content;

}




 ?>
