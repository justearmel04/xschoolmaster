<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');

$etabs=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
//nous allons verifier si le code etablissement existe deja dans la base de données
// $etabs=new Etab();
$content="";

$codeetab=htmlspecialchars($_POST['codeetab']);
$pays=htmlspecialchars($_POST['pays']);

// $check=$etabs->existEtab($codeetab);

$check=$etabs->existEtabCountry($codeetab,$pays);

if($check==0)
{
  $content=0;
}else {
  $content=1;
}

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{

  //recupération du code établissment


  $content="";

  $codeetab=htmlspecialchars($_POST['code']);

  $libelles=$etabs->getEtablissementbyCodeEtab($codeetab);

  foreach ($libelles as $value):
$content .= "<option selected value='". utf8_encode(utf8_decode($value->libelle_etab))."' >" . utf8_encode(utf8_decode($value->libelle_etab)). "</option>";

endforeach;

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{

  $content="";
  $pays=htmlspecialchars($_POST['pays']);
  $datas=$etabs->getAllEtabByCountry($pays);
  $nb=count($datas);

  if($nb>0)
  {
    $content.="<option value=''>Selectionner un Etablissement</option>";
      foreach ($datas as $value):
          $content .= "<option value='". $value->code_etab ."*".$value->libelle_sess."' >" . utf8_encode(utf8_decode($value->libelle_etab)). "</option>";
      endforeach;
  }else if($nb==0)
  {
      $content.="<option value=''>Aucun Etablissement</option>";
  }

echo $content;

}



 ?>
