<?php
session_start();
require_once('../class/Message.php');
require_once('../class/Parent.php');
require_once('../class/Student.php');
require_once('../class/Etablissement.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$messages = new Message();
$etabs=new Etab();

//recuperation des variables

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recuperation des variables
  $content="";
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $studentid=htmlspecialchars($_POST['student']);

  //determiner le nombre de versement de scolarités pour cette année scolaire

  $nbpaiement=$etabs->getscolaritiesfirstpaie($codeEtab,$sessionEtab,$studentid);

  echo $nbpaiement;



}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recuperation des variables
  $content="";
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $studentid=htmlspecialchars($_POST['student']);
  $motif=htmlspecialchars($_POST['motif']);

  //determiner le nombre de versement de scolarités pour cette année scolaire

  $nbpaiement=$etabs->getscolaritiesfirstpaieScolatites($codeEtab,$sessionEtab,$studentid,$motif);

  echo $nbpaiement;



}


 ?>
