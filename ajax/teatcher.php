<?php
session_start();
require_once('../class/Teatcher.php');
require_once('../class/User.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$teatcher= new Teatcher();
$userteatch=new User();
if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  $content="";

  $login=htmlspecialchars(addslashes($_POST['login']));
  $email=htmlspecialchars(addslashes($_POST['email']));
  $codeetab=htmlspecialchars(addslashes($_POST['codeetab']));


  $loginstatus=$userteatch->CheckingLogin($login);

  if($loginstatus>0)
  {
    $content=1;
  }else if($loginstatus==0)
  {
    $content=0;
  }

  // $check=$teatcher->existTeatcher($login,$email);
  //
  // if($check==0)
  // {
  //   $content=0;
  // }else {
  //   // le compte enseignant existe deja nous allons voir si
  //
  //   $idTeatcher=$teatcher->getIdTeatcher($email);
  //
  //   $check1=$teatcher->existTeatcherbySchoolCode($email,$idTeatcher,$codeetab);
  //
  //   if($check1==0)
  //   {
  //     //le client exiset mais n'est pas  pas assigner à un etablissement
  //
  //     $content=2;
  //   }else {
  //     $content=1;
  //   }
  //
  //   //$content=1;
  // }

  echo $content;


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{

  $content="";
  $code=htmlspecialchars(addslashes($_POST['code']));

  $datas=$teatcher->getAllTeatcherbyschoolCodedis($code);


  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>".L::NoTeatchers." </option>";
  }else {
    //var_dump($datas);
    $content.="<option value='' selected>".L::SeleectAnEnseignant."</option>";

    foreach ($datas as $value):
        $content .= "<option value='". $value->id_compte ."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
    endforeach;
  }

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recuperation du matricule du professeurs

  $content="";

  $matricule=htmlspecialchars(addslashes($_POST['matricule']));

  $datas=$teatcher->getTheTeatcherOfThisMatricule($matricule);
  $content.="<option value='' selected>".L::SeleectAnEnseignant."</option>";

  foreach ($datas as $value):
      $content .= "<option  value='". $value->id_compte ."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
  endforeach;

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //recuperation des variables

  $code=htmlspecialchars(addslashes($_POST['code']));
  $content="";
  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));

  //recherche du professeru enseignant cette matiere dans la classe de cet etablissement



  // foreach ($datas as $value):
  //     $content .= "<option selected value='". $value->id_compte ."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
  // endforeach;
  //
  // echo $content;

  $nbligne=$teatcher->getTeatcherOfschooClassesNb($code,$classe,$matiere);
  $content.="<option value='' selected>".L::SeleectAnEnseignant."</option>";

  if($nbligne>0)
  {
$datas=$teatcher->getTeatcherOfschooClasses($code,$classe,$matiere);
foreach ($datas as $value):
    $content .= "<option value='". $value->id_compte ."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
endforeach;
  }else {
    $content.="<option value=''>".L::NoTeatchers." </option>";
  }

echo $content;
}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //recuperation des variables

  $code=htmlspecialchars(addslashes($_POST['code']));
  $content="";
  $classeEtab=htmlspecialchars(addslashes($_POST['classeEtab']));
  $parentid=htmlspecialchars(addslashes($_POST['parentid']));

  $nbligne=$teatcher->getNumberOfTeatcherClasseSchool($classeEtab,$code);

  $datas=$teatcher->getAllTeatchersOfThisClassesEtab($code,$classeEtab);



  if($nbligne>0)
  {
    $content.="<option value='' selected>".L::SeleectAnEnseignant."</option>";
    $datas=$teatcher->getAllTeatchersOfThisClassesEtab($code,$classeEtab);
    foreach ($datas as $value):
        $content .= "<option value='". $value->id_compte ."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
    endforeach;
  }else {
    $content.="<option value=''>".L::NoTeatchers." </option>";
  }



  echo $content;


}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  //recuperation des variables

  $nbchild=htmlspecialchars(addslashes($_POST['nbchild']));
  $sessionEtab=htmlspecialchars(addslashes($_POST['session']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
  $content="";

  $datas=$teatcher->getAllTeatcherSons($sessionEtab,$codeEtab,$teatcherid);

  $nb=count($datas);
 // var_dump($datas);
  if($nb==0)
  {
      $content.="<tr><td style=\"colspan=5\">".L::NoLigne."</td></tr>";
  }else if($nb>0)
  {
    // $content.="<option value='' selected>Selectionner un Enseignant</option>";
    foreach ($datas as $value):
        $content .= "<tr ondbclick=\"deleted(".$value->id_childt.")\"><td>". utf8_encode(utf8_decode($value->nom_childt))."</td><td>".date_format(date_create($value->datenais_childt),"d/m/Y")."</td><td>".utf8_encode(utf8_decode($value->lieunais_childt))."</td><td> <a href=\"#\" class=\"btn btn-warning  btn-md\" style=\"border-radius:3px;\" ><i class=\"fa fa-info-circle\"></i> </a> </td></tr>";
        // $content ."";
        // $content ."";
        // $content ."";
        // $content ."";
        // $content ."";
        // $content.="";
    endforeach;

  }

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{
  //recuperation des variables

  //supprimer l'enfant de cet professeur

  $childid=htmlspecialchars(addslashes($_POST['childid']));
  $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));


  $teatcher->deletedTeatcherson($childid,$teatcherid);

  //nous devons mettre à jour le nombre des enfants du professeur

  $oldnumber=$teatcher->getNumberOfchild($teatcherid);
  $newsonNumber=$oldnumber+1;

  $teatcher->UpdateNumberOfChild($newsonNumber,$teatcherid);

  $_SESSION['user']['addteaok']=L::ChildInfosHasdeletedSuccessfully;

}else if(isset($_POST['etape'])&&($_POST['etape']==8))
{
  $diplomeid=htmlspecialchars(addslashes($_POST['diplomeid']));
  $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));

  $teatcher->deletedTeatcherDiplome($diplomeid,$teatcherid);

  $_SESSION['user']['addteaok']=L::ThediplomeaddSuccessfully;
}else if(isset($_POST['etape'])&&($_POST['etape']==9))
{
  $content="";
  $code=htmlspecialchars($_POST['codeEtab']);
  $classeid=htmlspecialchars($_POST['classeid']);

  // $datas=$teatcher->getAllTeatcherbyschoolCodedis($code);
  $datas=$teatcher->AllEtabsTeatchers($code);

  //nous allons chercher la liste des enseignants qui enseignent deja dans cette classe

  $content1="";
  $tabcontent1="";

  $oldids=$teatcher->getAllIdsofOldteatchers($code,$classeid);
  $nbold=count($oldids);
  if($nbold>0)
  {
    foreach ($oldids as  $values):
      $content1.=$values->id_compte.",";
    endforeach;

    $content1=substr($content1, 0, -1);

    $tabcontent1=explode(",",$content1);
  }


  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>".L::NoTeatchers." </option>";
  }else {
    //var_dump($datas);
    $content.="<option value='' selected>".L::SeleectAnEnseignant."</option>";

    if($nbold>0)
    {
      foreach ($datas as $value):
        if (in_array($value->id_compte, $tabcontent1)) {

        }else {
          $content .= "<option value='". $value->id_compte ."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
        }

      endforeach;
    }else {
      foreach ($datas as $value):
          $content .= "<option value='". $value->id_compte ."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
      endforeach;
    }


  }

  echo $content;
}else if(isset($_POST['etape'])&&($_POST['etape']==10))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $classeid=htmlspecialchars($_POST['classeid']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $teatcherid=htmlspecialchars($_POST['teatcherid']);

  $teatcher->AddEnseignerTeatcher($teatcherid,$classeid,$codeEtab,$sessionEtab);

  $_SESSION['user']['addclasseok']=L::TeatcherAssignatedclasseOk;


}else if(isset($_POST['etape'])&&($_POST['etape']==11))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $classeid=htmlspecialchars($_POST['classeid']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $content="";

  $datas=$teatcher->getAllIdsofOldteatchers($codeEtab,$classeid);
  $nb=count($datas);

  if($nb>0)
  {
    foreach ($datas as $value):
    $content.="<tr>";
    $content.="<td><span>".$value->nom_compte."</span></td>";
    $content.="<td><span >".$value->prenom_compte."</span></td>";
    $content.="<td><span >".$value->tel_compte."</span></td>";
    $content.="</tr>";
  //   $content.="<td><span >".$value->email_compte."</span></td>";
  //   $content.="<td> <a class=\"btn btn-primary btn-xs\" onclick=\"Addtolist(".$value->id_compte.",'".$value->type_compte."')\"> <i class=\"fa fa-plus\"></i> </a> </td>";
  endforeach;

  }
  else if($nb==0){
    $content.="<tr>";
    $content.="<td colspan=\"3\">".L::NoLigne."</td>";
    $content.="</tr>";
  }

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==12))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $classeid=htmlspecialchars($_POST['classe']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $content="";

  $datas=$teatcher->getAllIdsofOldteatchersIn($codeEtab,$sessionEtab,$classeid);
  $nb=count($datas);

  $nbligne=count($datas);

    if($nbligne==0)
  {
      $content.="<option value=''>".L::NoTeatchers." </option>";
  }else {
    $content.="<option value='' selected>".L::SeleectAnEnseignant."</option>";
    foreach ($datas as $value):
        $content .= "<option value='". $value->id_compte ."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
    endforeach;
  }

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==12))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $classeEtab=htmlspecialchars($_POST['classeid']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $content="";

  $nbligne=$teatcher->getNumberOfTeatcherClasseSchool($classeEtab,$codeEtab);

  // $datas=$teatcher->getAllTeatchersOfThisClassesEtab($code,$classeEtab);



  if($nbligne>0)
  {
    $content.="<option value='' selected>".L::SeleectAnEnseignant."</option>";
    $datas=$teatcher->getAllTeatchersOfThisClassesEtab($codeEtab,$classeEtab);
    foreach ($datas as $value):
        $content .= "<option value='". $value->id_compte ."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
    endforeach;
  }else {
    $content.="<option value=''>".L::NoTeatchers." </option>";
  }



  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==13))
{
  $content="";
  $code=htmlspecialchars($_POST['codeEtab']);
  $classeid=htmlspecialchars($_POST['classeid']);

  // $datas=$teatcher->getAllTeatcherbyschoolCodedis($code);
  $datas=$teatcher->AllEtabsTeatchers($code);

  //nous allons chercher la liste des enseignants qui enseignent deja dans cette classe

  $content1="";
  $tabcontent1="";

  $oldids=$teatcher->getAllIdsofOldteatchers($code,$classeid);
  $nbold=count($oldids);
  if($nbold>0)
  {
    foreach ($oldids as  $values):
      $content1.=$values->id_compte.",";
    endforeach;

    $content1=substr($content1, 0, -1);

    $tabcontent1=explode(",",$content1);
  }


  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>".L::NoTeatchers." </option>";
  }else {
    //var_dump($datas);
    $content.="<option value='' selected>".L::SeleectAnEnseignant."</option>";

    foreach ($datas as $value):
        $content .= "<option value='". $value->id_compte ."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
    endforeach;


  }

  echo $content;
}

 ?>
