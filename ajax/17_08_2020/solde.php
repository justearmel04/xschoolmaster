<?php
session_start();
require_once('../class/Message.php');
require_once('../class/Parent.php');
require_once('../class/Student.php');
require_once('../class/Etablissement.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$messages = new Message();
$etabs=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recuperation des variables

  $notifid=htmlspecialchars(addslashes($_POST['notifid']));
  $content="";

  $datas=$etabs->getNotificationInfos($notifid);

  echo $datas;
}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  $smssender=htmlspecialchars(addslashes($_POST['smssender']));
  $emailsender=htmlspecialchars(addslashes($_POST['emailsender']));
  $notifid=htmlspecialchars(addslashes($_POST['notifid']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $destinataires=htmlspecialchars(addslashes($_POST['destinataires']));
  $classes=htmlspecialchars(addslashes($_POST['classes']));
  $sessionEtab=htmlspecialchars(addslashes($_POST['sessionEtab']));

  $tabdatadestinataires=explode("-",$destinataires);
  $nbtabdestinataires=count($tabdatadestinataires);
  $cptedestinatairesval=$nbtabdestinataires-1;

  $tabclasses=explode("-",$classes);
  $nbtabclasses=count($tabclasses);
  $cpteclassesval=$nbtabclasses-1;

  //determiner l'indicatif de ce pays

  $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

  $destimails="";
  $destiphone="";


    for($i=0;$i<$cpteclassesval;$i++)
    {


      for($j=0;$j<$cptedestinatairesval;$j++)
      {
          // echo $tabclasses[$i]."-".$tabdatadestinataires[$j].'<br>';
          if($tabdatadestinataires[$j]=="Parent")
          {
              // echo "Parent de la classe ".$tabclasses[$i]."<br>";
              //rechercher le mail des parents dont un eleve appartient à cette classeEtab

              $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab,$sessionEtab);
               //var_dump($dataParents);
               $ka=1;
               foreach ($dataParents as $parents):

                 $destimails=$destimails.$parents->email_parent."*";
                 $destiphone=$destiphone.$indicatifEtab.$parents->tel_parent."*";


                 $ka++;
               endforeach;

          }else if($tabdatadestinataires[$j]=="Student")
          {
            // echo "Student de la classe ".$tabclasses[$i]."<br>";
            // rechercher le mail des eleves de cette classeEtab
            $dataStudents=$etabs->getEmailsOfStudentOfThisClasses($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab);
            //var_dump($dataStudents);

            $ka=1;
            foreach ($dataStudents as $students):

              $destimails=$destimails.$students->email_eleve."*";
              $destiphone=$destiphone.$indicatifEtab.$students->tel_compte."*";


              $ka++;
            endforeach;

          }else if($tabdatadestinataires[$j]=="Teatcher")
          {
            // echo "Teatcher de la classe ".$tabclasses[$i]."<br>";
            // rechercher le mail des professeurs de cette classe

            $datateatchers=$etabs->getEmailsOfTeatcherOfThisClasses($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab);
            //var_dump($datateatchers);

            $ka=1;
            foreach ($datateatchers as $teatchers):

              $destimails=$destimails.$teatchers->email_compte."*";
              $destiphone=$destiphone.$indicatifEtab.$teatchers->tel_compte."*";


              $ka++;
            endforeach;

          }else if($tabdatadestinataires[$j]=="Admin_locale")
          {
            // echo "Admin de la classe ".$tabclasses[$i]."<br>";
            // rechercher la liste local_admin de cet etablissement
            $datalocaladmins=$etabs->getEmailOfLocaladminOfThisClasses($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab);
          //  var_dump($datalocaladmins);

          $ka=1;
          foreach ($datalocaladmins as $locals):

            $destimails=$destimails.$locals->email_compte."*";
            $destiphone=$destiphone.$indicatifEtab.$locals->tel_compte."*";


            $ka++;
          endforeach;

          }
      }
    }

    echo $destimails."/".$destiphone;

}
 ?>
