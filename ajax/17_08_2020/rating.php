<?php
session_start();
require_once('../class/Student.php');
require_once('../class/Classe.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Teatcher.php');
require_once('../class/Etablissement.php');
require_once('../controller/functions.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$student= new Student();
$classeSchool= new Classe();
$admin= new Localadmin();
$parents=new ParentX();
$teatcher=new Teatcher();
$etabs=new Etab();


if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //nous allons verifier si nous avons au moins une moyenne dans cette classe

  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $classeEtab=htmlspecialchars($_POST['classeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $trimestre=htmlspecialchars($_POST['trimestre']);

  $nb=$etabs->getratingNumberofthisclasse($classeEtab,$codeEtab,$sessionEtab,$trimestre);

  echo $nb;


}


 ?>
