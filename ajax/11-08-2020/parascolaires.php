<?php
session_start();
require_once('../class/Student.php');
require_once('../class/Classe.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Teatcher.php');
require_once('../class/Etablissement.php');
require_once('../class/Sessionsacade.php');
require_once('../controller/functions.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$student= new Student();
$classeSchool= new Classe();
$admin= new Localadmin();
$parents=new ParentX();
$teatcher=new Teatcher();
$etabs=new Etab();
$session= new Sessionacade();


if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //nous allons verifier si cet eleve existe deja dans la base de données

  //recupération des variables

  $content="";
  $content1="";
  $content2="";
  $parascoid=htmlspecialchars(addslashes($_POST['parascoid']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $sessionEtab=htmlspecialchars(addslashes($_POST['sessionEtab']));

  $sessionencours=$session->getSessionEncours($codeEtab);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];

  $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);




  $datas=$etabs->getAllParascolairesActivityForcalendar($codeEtab,$sessionEtab,$parascoid);
  // $datas=$etabs->getAllParascolairesActivityDetails($codeEtab,$sessionEtab,$parascoid);


  foreach ($datas as  $value):
    $denomination=utf8_decode($value->libelle_act);
    $responsablename=utf8_decode($value->respo_act);
    $responsablecontact=utf8_decode($value->contactrespo_act);
    $lieuactivity=utf8_decode($value->lieuact_act);
    $typeactivity=utf8_decode($value->typeact_act);
    $desctiptionactivity=utf8_decode($value->description_act);
    $otheractivity=utf8_decode($value->othertype_act);
    $periodeactivity=utf8_decode($value->typesession_act);
    $notificationid=utf8_decode($value->idnotif_act);

    $data=$etabs->getAllParascolairesActivityDetails($codeEtab,$sessionEtab,$notificationid);
    foreach ($data as $values):
      $precis=utf8_decode($values->precis_msg);
      $eleves=utf8_decode($values->eleves_msg);
      $classes=utf8_decode($values->classes_msg);
    endforeach;

  endforeach;

  foreach ($typesemestre as $value):

    if($value->id_semes==$periodeactivity)
    {
        $content2 .= "<option value='".$value->id_semes."' selected >" . utf8_encode(utf8_decode($value->libelle_semes)). "</option>";
    }else {
      $content2 .= "<option value='".$value->id_semes."'  >" . utf8_encode(utf8_decode($value->libelle_semes)). "</option>";
    }

  endforeach;

  if($typeactivity==1)
  {
  $content1.= "<option value='1-Activités des clubs' selected  >Activités des clubs</option>";
  }else {
    $content1.= "<option value='1-Activités des clubs'  >Activités des clubs</option>";
  }

  if($typeactivity==2)
  {
  $content1.= "<option value='2-Atelier pratique' selected >Atelier pratique</option>";
  }else {
    $content1.= "<option value='2-Atelier pratique' >Atelier pratique</option>";
  }

  if($typeactivity==3)
  {
  $content1.= "<option value='3-Visite d'entreprise' selected >Activités des clubs</option>";
  }else {
    $content1.= "<option value='3-Visite d'entreprise'  >Activités des clubs</option>";
  }

  if($typeactivity==4)
  {
  $content1.= "<option value='4-Activités sportives' selected >Visite d'entreprise</option>";
  }else {
    $content1.= "<option value='4-Activités sportives' >Visite d'entreprise</option>";
  }

  if($typeactivity==5)
  {
  $content1.= "<option value='5-Sortie' selected >Sortie</option>";
  }else {
    $content1.= "<option value='5-Sortie'  >Sortie</option>";
  }

  if($typeactivity==6)
  {
  $content1.= "<option value='6-Autres' selected>Autres</option>";
  }else {
    $content1.= "<option value='6-Autres' >Autres</option>";
  }









$content=$responsablename."*".$responsablecontact."*".$lieuactivity."*".$typeactivity."*".$desctiptionactivity."*".$otheractivity."*".$content1."*".$content2."*".$denomination."*".$precis;

echo $content;


}


 ?>
