<?php
session_start();
require_once('../class/Matiere.php');
require_once('../class/Etablissement.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$matiere= new Matiere();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";


$codefamille=htmlspecialchars($_POST['codefamille']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);



$check=$etab->ExistFamilly($codefamille,$codeEtab);

if($check==0)
{

  $content=0;
}else {
$content=1;

}

echo $content;
}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{

//recuperation des variables

$content="";

$content1="";

$compte=htmlspecialchars($_POST['compte']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);

if($compte=="Parent")
{
  //nous allons rechercher le nom des parents d'élève

 $datas=$etab->getAllparentNameEtab($compte,$codeEtab,$sessionEtab);

 // var_dump($datas);

 $nb=count($datas);

if($nb==0)
{
$content.="<option value=''>".L::Noparent." </option>";
}else {

  // $content.="<option value='' selected>".L::Selectparent."</option>";

    foreach ($datas as $value):
      if(strlen($value->nom_compte)>0)
      {
// $content .= "<option value='".$value->nom_compte."' >" .utf8_encode(utf8_decode($value->nom_compte)). "</option>";

$content .= "<option>" .utf8_encode(utf8_decode($value->nom_compte)). "</option>";

$content1.="'$value->nom_compte'".",";

      }


    endforeach;

}


}else if($compte=="Student")
{
  //nous allons rechercher le nom des élèves inscrits

$datas=$etab->getAllStudenttNameEtab($compte,$codeEtab,$sessionEtab);

$nb=count($datas);

if($nb==0)
{
$content.="<option value=''>".L::Nostudent." </option>";
}else {

 // $content.="<option value='' selected>".L::Selectparent."</option>";

   foreach ($datas as $value):
     if(strlen($value->nom_compte)>0)
     {
// $content .= "<option value='".$value->nom_compte."' >" .utf8_encode(utf8_decode($value->nom_compte)). "</option>";

$content .= "<option>" .utf8_encode(utf8_decode($value->nom_compte)). "</option>";

$content1.="'$value->nom_compte'".",";

     }


   endforeach;

}

}

// $content1=substr($content1, 0, -1);
//
// $concat="[".$content1."]";
// echo $concat;

// $concat="{data:[".$content1."]}";
echo $content;


}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{

  $compte=htmlspecialchars($_POST['compte']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $nom=htmlspecialchars($_POST['nom']);
  $content="";

  if($compte=="Parent")
  {
    //nous allons rechercher la liste des parent avec ce nom

    $datas=$etab->getListparentNameEtab($nom,$codeEtab,$sessionEtab,$compte);
    $nbdep=count($datas);
    if($nbdep==0)
    {
      $content.="<tr>";
      $content.="<td colspan=\"4\">Aucune Ligne</td>";
      $content.="</tr>";
    }else {
      foreach ($datas as $value):
      $content.="<tr ondblclick=\"Addtolist(".$value->id_compte.",'".$value->type_compte."')\">";
      $content.="<td><span>".$value->nom_compte."</span></td>";
      $content.="<td><span >".$value->prenom_compte."</span></td>";
      $content.="<td><span >".$value->tel_compte."</span></td>";
      $content.="<td><span >".$value->email_compte."</span></td>";
      $content.="<td> <a class=\"btn btn-primary btn-xs\" onclick=\"Addtolist(".$value->id_compte.",'".$value->type_compte."')\"> <i class=\"fa fa-plus\"></i> </a> </td>";
    endforeach;
    }


  }else if($compte=="Student"){
    // nous allons rechercher les eleves avec ce nom

    $datas=$etab->getListstudentNameEtab($nom,$codeEtab,$sessionEtab,$compte);

    $nbdep=count($datas);
    if($nbdep==0)
    {
      $content.="<tr>";
      $content.="<td colspan=\"4\">Aucune Ligne</td>";
      $content.="</tr>";
    }else {
      foreach ($datas as $value):
        $content.="<tr ondblclick=\"Addtolist(".$value->id_compte.",'".$value->type_compte."')\">";
        $content.="<td><span>".$value->nom_compte."</span></td>";
        $content.="<td><span >".$value->prenom_compte."</span></td>";
        $content.="<td><span >".$value->tel_compte."</span></td>";
        $content.="<td><span >".$value->email_compte."</span></td>";
        $content.="<td> <a class=\"btn btn-primary btn-xs\" onclick=\"Addtolist(".$value->id_compte.",'".$value->type_compte."')\"> <i class=\"fa fa-plus\"></i> </a> </td>";
    endforeach;
    }
  }

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{

$compte=htmlspecialchars($_POST['compte']);
$type=htmlspecialchars($_POST['type']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$content="";

if($type=="Parent")
{
  $datas=$etab->getparentMember($compte,$type);
  foreach ($datas as $value):
    $content=$value->nom_compte."*".$value->prenom_compte."*".$value->email_compte."*".$value->type_compte;
  endforeach;
}else if($type=="Student")
{
$datas=$etab->getstudentMember($compte,$type,$codeEtab);
foreach ($datas as $value):
$content=$value->nom_compte."*".$value->prenom_compte."*".$value->email_compte."*".$value->type_compte."*".$value->id_compte;
endforeach;

}

echo $content;


}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{

  //recuperation des variables
  $content=0;
  $id=htmlspecialchars($_POST['id']);
  $selectionner=htmlspecialchars($_POST['selectionner']);
  $tabselectionner=explode("@",$selectionner);

  if(in_array($id, $tabselectionner)){
    $content=1;
  }

echo $content;


}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{

//recuperation des variables

$ids=htmlspecialchars($_POST['ids']);
$famillycode=htmlspecialchars($_POST['famillycode']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);

$familleid=$etab->getfamillyId($famillycode,$codeEtab);

$tabids=explode("@",$ids);
$nb=count($tabids);

for($i=1;$i<$nb;$i++)
{
  $typeaccount=$etab->getaccounttype($tabids[$i]);
  // echo $tabids[$i]."/".$typeaccount."</br>";

//nous allons verifier si le compte est deja associé a la famille

 $existe=$etab->AllreadyfamilyMember($tabids[$i],$famillycode,$codeEtab,$typeaccount,$familleid);

 if($existe==0)
 {
   $etab->AddfamillyMembers($familleid,$tabids[$i],$typeaccount);
 }


}

$_SESSION['user']['addctrleok']=L::ListmemberAddsuccessfuly;





}
