<?php
session_start();
require_once('../class/Matiere.php');
require_once('../class/Etablissement.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$matiere= new Matiere();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$matiere=htmlspecialchars(addslashes($_POST['matiere']));
$classe=htmlspecialchars(addslashes($_POST['classe']));
$teatcher=htmlspecialchars(addslashes($_POST['teatcher']));
$codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
$coefficient=htmlspecialchars(addslashes($_POST['coefficient']));
$datectrl=htmlspecialchars(addslashes($_POST['datectrl']));
$libelle=htmlspecialchars(addslashes($_POST['libelle']));

//$check=$matiere->AllreadyExistMatiere();

//$content=$teatcher->getAllTeatchersByschoolCode($codeEtab);

//verifier si la matière existe dejà

//$check=$matiere->AllreadyExistMatiere();

// $check=$etab->ExistMatiereAllready($matiere,$classe,$codeEtab);

// $check=$etab->ExistControleAllready($matiere,$classe,$teatcher,$codeEtab,$coefficient,$datectrl);
$check=$etab->ExistControleAllreadyOne($matiere,$classe,$teatcher,$codeEtab,$coefficient,$datectrl,$libelle);



if($check==0)
{
  //cette matière n'existe pas encore pour cette classe
  $content=0;
}else {
$content=1;

}

echo $content;
}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recuperation des variables

  $content="";

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $code=htmlspecialchars(addslashes($_POST['code']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $statut=2;

  //compter le nombre de controle dont le statut est nulle pour cette classe et dans cette matiere

  $check=$etab->getStatusOfControleClasseMat($classe,$code,$matiere,$statut);


  if($check==0)
  {

    $content.="<option value=''>".L::Nocontroles." </option>";
  }else {
    //var_dump($datas);
    $datas=$etab->getAllControlesOfThisClassesSchoolActive($classe,$code,$matiere,$statut);

    $content.="<option value='' selected>".L::Selectionnecontroles."</option>";

    foreach ($datas as $value):
        $content .= "<option value='".$value->id_ctrl."-".$value->mat_ctrl."-".$value->teatcher_ctrl."' >" .utf8_encode(utf8_decode($value->libelle_ctrl)). "</option>";
    endforeach;
  }

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
    //recuperation des variables

    $classe=htmlspecialchars(addslashes($_POST['classe']));
    $idcompte=htmlspecialchars(addslashes($_POST['idcompte']));
    $matiere=htmlspecialchars(addslashes($_POST['matiere']));
    $semestre=htmlspecialchars(addslashes($_POST['semestre']));
    $notetype=htmlspecialchars(addslashes($_POST['notetype']));
    $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
    $session=htmlspecialchars(addslashes($_POST['session']));

    $concatcontroleid="";
    $content="";

    //nous allons compter le nombre de controle pour le semestre pour cette classe et dont l'eleve à une note

    $nbcontroleNotesStudent=$etab->getNumberOfNotesControleStudentByClassesIdAndSubject($idcompte,$classe,$matiere,$semestre,$notetype,$codeEtab,$session);

    if($nbcontroleNotesStudent>0)
    {
      $datacontroles=$etab->getStudentControle($idcompte,$classe,$matiere,$semestre,$notetype,$codeEtab,$session);

      $content.="<option value='' selected>".L::Selectionnecontroles."</option>";

      foreach ($datacontroles as $value):
          $content .= "<option value='".$value->id_ctrl."-".$value->mat_ctrl."-".$value->teatcher_ctrl."' >" .utf8_encode(utf8_decode($value->libelle_ctrl)). "</option>";
          $concatcontroleid=$concatcontroleid.$value->id_ctrl.",";
      endforeach;
    }

    else if($nbcontroleNotesStudent==0)
    {
      $content.="<option value=''>".L::Nocontroles." </option>";
    }

    echo $nbcontroleNotesStudent."*".$concatcontroleid."*".$content;
}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{

  //recuperation des variables

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $idcompte=htmlspecialchars(addslashes($_POST['idcompte']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  // $semestre=htmlspecialchars(addslashes($_POST['semestre']));
  $notetype=htmlspecialchars(addslashes($_POST['notetype']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $session=htmlspecialchars(addslashes($_POST['session']));

  $concatcontroleid="";
  $content="";

  //nous allons compter le nombre de controle pour le semestre pour cette classe et dont l'eleve à une note

  $nbcontroleNotesStudent=$etab->getNumberOfNotesControleStudentByClassesIdAndSubjectother($idcompte,$classe,$matiere,$notetype,$codeEtab,$session);

  if($nbcontroleNotesStudent>0)
  {
    $datacontroles=$etab->getStudentControleother($idcompte,$classe,$matiere,$notetype,$codeEtab,$session);

    $content.="<option value='' selected>".L::Selectionnecontroles."</option>";

    foreach ($datacontroles as $value):
        $content .= "<option value='".$value->id_ctrl."-".$value->mat_ctrl."-".$value->teatcher_ctrl."' >" .utf8_encode(utf8_decode($value->libelle_ctrl)). "</option>";
        $concatcontroleid=$concatcontroleid.$value->id_ctrl.",";
    endforeach;
  }

  else if($nbcontroleNotesStudent==0)
  {
    $content.="<option value=''>".L::Nocontroles." </option>";
  }

  echo $nbcontroleNotesStudent."*".$concatcontroleid."*".$content;
}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //il est question de recherche les controles

  $content="";

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $code=htmlspecialchars(addslashes($_POST['code']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $statut=0;

  //compter le nombre de controle dont le statut est nulle pour cette classe et dans cette matiere

  $check=$etab->getNbAllControleClasseConsignerNotes($classe,$code,$matiere,$statut);

  // getAllControleClasseConsignerNotes($classe,$code,$matiere,$statut)

  if($check==0)
  {
    $content.="<option value=''>".L::Nocontroles." </option>";
  }else {
    //var_dump($datas);
    $datas=$etab->getAllControleClasseConsignerNotes($classe,$code,$matiere,$statut);
    $content.="<option value='' selected>".L::Selectionnecontroles."</option>";

    foreach ($datas as $value):
      //compter le nombre de notes pour un controle

      $check1=$etab->getNbnotesControles($value->id_ctrl,$value->codeEtab_ctrl,$value->session_ctrl,$classe,$matiere);

      if($check1>0)
      {

      }else {
        $content .= "<option value='".$value->id_ctrl."-".$value->mat_ctrl."-".$value->teatcher_ctrl."' >" .utf8_encode(utf8_decode($value->libelle_ctrl)). "</option>";
      }


    endforeach;
  }

  echo $content;


}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  //il est question de recherche les controles

  $content="";

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $code=htmlspecialchars(addslashes($_POST['code']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $statut=1;

  //compter le nombre de controle dont le statut est nulle pour cette classe et dans cette matiere

  $check=$etab->getNbAllControleClasseConsignerNotes($classe,$code,$matiere,$statut);



  if($check==0)
  {
    $content.="<option value=''>".L::Nocontroles." </option>";
  }else {

    $datas=$etab->getAllControleClasseConsignerNotes($classe,$code,$matiere,$statut);
    $content.="<option value='' selected>".L::Selectionnecontroles."</option>";

    foreach ($datas as $value):
      //compter le nombre de notes pour un controle

      $check1=$etab->getNbnotesControles($value->id_ctrl,$value->codeEtab_ctrl,$value->session_ctrl,$classe,$matiere);

      if($check1>0)
      {
$content .= "<option value='".$value->id_ctrl."-".$value->mat_ctrl."-".$value->teatcher_ctrl."' >" .utf8_encode(utf8_decode($value->libelle_ctrl)). "</option>";
      }else {

      }


    endforeach;
  }

  echo $content;


}



 ?>
