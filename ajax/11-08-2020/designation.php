<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Etablissement.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$classex= new Classe();
$student=new Student();
$etabs=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recuperation des variables
  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $code=htmlspecialchars(addslashes($_POST['code']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $typenote=htmlspecialchars(addslashes($_POST['typenote']));
  $content="";

  if($typenote==1)
  {
    //il est question d'un controle
    //si le statut du controle est a 1 alors nous avons des notes dans le système pour cette classe
    $statut=1;
    $nbstatutcontrole=$etabs->getStatusOfControleClasseMat($classe,$code,$matiere,$statut);
    if($nbstatutcontrole==0)
    {
      $content.="<option value=''>".L::Nocontroles." </option>";
    }else if($nbstatutcontrole>0)
    {
      //nous allons recuperer la liste des controles
        $datas=$etabs->getAllControlesOfThisClassesSchool($classe,$code,$matiere,$statut);
        $content.="<option selected value='' >Selectionner un contrôle</option>";

        foreach ($datas as $value):
            $content .= "<option value='".$value->id_ctrl."-".$value->mat_ctrl."-".$value->teatcher_ctrl."' >" .utf8_encode(utf8_decode($value->libelle_ctrl)). "</option>";
        endforeach;
    }

  }else if($typenote==2)
  {
//il est question d'un examen
//nous allons voir si nous avons au moins une note pour cet examen
  $type=2;
  $nbexamen=$etabs->getNumberOfNotesSchoolClasses($classe,$code,$matiere,$type);
  if($nbexamen==0)
  {
$content.="<option selected value=''>Aucun Examen </option>";
  }else if($nbexamen>0)
  {
  $datas=$etabs->getAllExamenOfSchoolClasses($classe,$code,$matiere,$type);
  $content.="<option selected value='' >Selectionner un Examen</option>";

  foreach ($datas as $value):
      $content .= "<option value='".$value->id_exam."-".$value->id_mat."-".$value->idcompte_enseignant."' >" . utf8_encode(utf8_decode($value->libelle_exam)). "</option>";
  endforeach;
  }

}else if($typenote==3)
{
  //nous allons chercher le nombre de quiz publié et qui ont au moins une note

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $codeEtab=htmlspecialchars(addslashes($_POST['code']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $typenote=htmlspecialchars(addslashes($_POST['typenote']));
  $sessionEtab=htmlspecialchars(addslashes($_POST['sessionEtab']));
  $teatcherid=$_SESSION['user']['IdCompte'];

  $quizsnb=$etabs->getNbTeatcherquizsPublishing($teatcherid,$codeEtab,$sessionEtab,$classe,$matiere);

  if($quizsnb>0)
  {
    $datas=$etabs->getAllTeatcherquizsPublishing($teatcherid,$codeEtab,$sessionEtab,$classe,$matiere);

    foreach ($datas as  $values):
      //nous allons compter les quizs avec au moins une note

      $state=$values->moystate_quiz;

      if($state==0)
      {
        $nbnotes=$etabs->getNbotesquizTeatcher($teatcherid,$codeEtab,$sessionEtab,$classe,$matiere,$values->id_quiz);

        if($nbnotes>0)
        {
          $content .= "<option value='".$values->id_quiz."-".$values->matiere_quiz."-".$values->teatcher_quiz."' >" . utf8_encode(utf8_decode($values->libelle_quiz)). "</option>";
        }

      }else if($state==1)
      {
        $nbnotes=$etabs->getNbotesquizpublishTeatcher($teatcherid,$codeEtab,$sessionEtab,$classe,$matiere,$values->id_quiz);

        if($nbnotes>0)
        {
          $content .= "<option value='".$values->id_quiz."-".$values->matiere_quiz."-".$values->teatcher_quiz."' >" . utf8_encode(utf8_decode($values->libelle_quiz)). "</option>";
        }
      }

    endforeach;
  }else {
    $content.="<option selected value=''>Aucun Quiz </option>";
  }


}



echo $content;
}else if(isset($_POST['etape'])&&($_POST['etape']==0))
{

  //recuperation des variables


  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $code=htmlspecialchars(addslashes($_POST['code']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $tabmat=explode("-",$matiere);
  $matiereid=$tabmat[0];
  $teatcherid=$tabmat[1];
  $typenote=htmlspecialchars(addslashes($_POST['typenote']));
    $content="";

    if($typenote==2)
    {
      // il s'agit ici d'un controle
        $type=2;
        $statut=1;
        $nbstatutcontrole=$etabs->getStatusOfControleClasseMatTeatcher($classe,$code,$matiere,$statut,$teatcherid);

        if($nbstatutcontrole==0)
        {
          $content.="<option value=''>".L::Nocontroles." </option>";
        }else if($nbstatutcontrole>0)
        {

          $datas=$etabs->getAllExamenOfSchoolClassesTeatcher($classe,$code,$matiere,$type,$teatcherid);
          $content.="<option selected value='' >Selectionner un Examen</option>";

          foreach ($datas as $value):
              $content .= "<option value='".$value->id_exam."-".$value->id_mat."-".$value->idcompte_enseignant."' >" . utf8_encode(utf8_decode($value->libelle_exam)). "</option>";
          endforeach;
        }

    }else if($typenote==1)
    {
      // il s'agit ici d'un examen
      $type=1;
      $nbexamen=$etabs->getNumberOfNotesSchoolClassesTeatcher($classe,$code,$matiere,$type,$teatcherid);
      if($nbexamen==0)
      {
    $content.="<option value=''>".L::Nocontroles." </option>";
      }else if($nbexamen>0)
      {
      $datas=$etabs->getAllExamenOfSchoolClassesTeatcher($classe,$code,$matiere,$type,$teatcherid);
      $content.="<option selected value='' >Selectionner un Examen</option>";

      foreach ($datas as $value):
          $content .= "<option value='".$value->id_exam."-".$value->id_mat."-".$value->idcompte_enseignant."' >" . utf8_encode(utf8_decode($value->libelle_exam)). "</option>";
      endforeach;
      }


    }
echo $content;
}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $code=htmlspecialchars(addslashes($_POST['code']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $tabmat=explode("-",$matiere);
  $matiereid=$tabmat[0];
  $teatcherid=$tabmat[1];
  $typenote=htmlspecialchars(addslashes($_POST['typenote']));
  $content="";

  if($typenote==1)
  {
    //il est question d'un controle
    //si le statut du controle est a 1 alors nous avons des notes dans le système pour cette classe
    $statut=1;
    $nbstatutcontrole=$etabs->getStatusOfControleClasseMatTeatcher($classe,$code,$matiereid,$statut,$teatcherid);
    if($nbstatutcontrole==0)
    {
      $content.="<option value=''>".L::Nocontroles." </option>";
    }else if($nbstatutcontrole>0)
    {
      //nous allons recuperer la liste des controles
        $datas=$etabs->getAllControlesOfThisClassesSchoolTeatcher($classe,$code,$matiereid,$statut,$teatcherid);
        $content.="<option selected value='' >Selectionner un contrôle</option>";

        foreach ($datas as $value):
            $content .= "<option value='".$value->id_ctrl."-".$value->mat_ctrl."-".$value->teatcher_ctrl."' >" .utf8_encode(utf8_decode($value->libelle_ctrl)). "</option>";
        endforeach;
    }

  }else if($typenote==2)
  {
    //il est question d'un examen
    //nous allons voir si nous avons au moins une note pour cet examen
      $type=2;
      $nbexamen=$etabs->getNumberOfNotesSchoolClassesTeatcher($classe,$code,$matiereid,$type,$teatcherid);

      if($nbexamen==0)
      {
    $content.="<option value=''>".L::Nocontroles." </option>";
      }else if($nbexamen>0)
      {
      $datas=$etabs->getAllExamenOfSchoolClassesTeatcher($classe,$code,$matiereid,$type,$teatcherid);
      $content.="<option selected value='' >Selectionner un Examen</option>";

      foreach ($datas as $value):
          $content .= "<option value='".$value->id_exam."-".$value->id_mat."-".$value->idcompte_enseignant."' >" . utf8_encode(utf8_decode($value->libelle_exam)). "</option>";
      endforeach;
      }

  }

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  $concatids="";
  $concatmats="";
  $datas=htmlspecialchars(addslashes($_POST['datas']));

  if(strlen($datas)>0)
  {

      $matriculedelete=substr($datas, 1);
      $matriculedelete=str_replace("*",",",$matriculedelete);

      // echo $matriculedelete;

      $tabstudentmat=explode(",",$matriculedelete);
      $nb=count($tabstudentmat);

      // echo $nb;

      //determiner le id des eleves a enlever de la liste

      for($i=0;$i<$nb;$i++)
      {
        $matricule=$tabstudentmat[$i];
        $id=$student->getStudentIdbyMat($matricule);
        // echo $id."<br>";
        $concatids=$concatids.$id.",";
        $concatmats=$concatmats."'".$matricule."',";

      }

      $concatids=substr($concatids,0,-1);
      // $concatmats=trim(substr($concatmats,0,-1));
  }


  echo $concatids;
}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //recuperation des variables

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $session=htmlspecialchars(addslashes($_POST['session']));
  $matriculedelete=htmlspecialchars(addslashes($_POST['matriculedelete']));
  $teatcher=htmlspecialchars(addslashes($_POST['teatcher']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $controle=htmlspecialchars(addslashes($_POST['controle']));

  $tabstudentids=explode(",",$matriculedelete);
  $nbstudentids=count($tabstudentids);
  $content="";

  // echo $nbstudentids;

  //nous allons compter le nombre de modifcation de notes en attente de validation pour ce controles

  $nbcontrlestandby=$student->NBofcontrolestandbyvalidation($controle,$matiere,$teatcher,$classe,$session,$codeEtab);

  // echo $nbcontrlestandby;

  if($nbcontrlestandby==0)
  {
    $content=0;
  }else {
    $content=1;
  }

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //recuperation des variables

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $session=htmlspecialchars(addslashes($_POST['session']));
  $matriculedelete=htmlspecialchars(addslashes($_POST['matriculedelete']));
  $teatcher=htmlspecialchars(addslashes($_POST['teatcher']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $examen=htmlspecialchars(addslashes($_POST['examen']));

  $tabstudentids=explode(",",$matriculedelete);
  $nbstudentids=count($tabstudentids);
  $content="";

  // echo $nbstudentids;

  //nous allons compter le nombre de modifcation de notes en attente de validation pour ce controles

  $nbcontrlestandby=$student->NBofexamenstandbyvalidation($examen,$matiere,$teatcher,$classe,$session,$codeEtab);

  // echo $nbcontrlestandby;

  if($nbcontrlestandby==0)
  {
    $content=0;
  }else {
    $content=1;
  }

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  //recuperation des variables

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $session=htmlspecialchars(addslashes($_POST['session']));
  $matriculedelete=htmlspecialchars(addslashes($_POST['matriculedelete']));
  $teatcher=htmlspecialchars(addslashes($_POST['teatcher']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $controle=htmlspecialchars(addslashes($_POST['controle']));

  $tabstudentids=explode(",",$matriculedelete);
  $nbstudentids=count($tabstudentids);
  $content="";

  // echo $nbstudentids;

  //nous allons compter le nombre de modifcation de notes en attente de validation pour ce controles

  $nbcontrlestandby=$student->NBofcontroleStudentstandbyvalidation($controle,$matiere,$teatcher,$classe,$session,$codeEtab,$matriculedelete);

  // echo $nbcontrlestandby;

  if($nbcontrlestandby==0)
  {
    $content=0;
  }else {
    $content=1;
  }

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{
  //recuperation des variables

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $session=htmlspecialchars(addslashes($_POST['session']));
  $matriculedelete=htmlspecialchars(addslashes($_POST['matriculedelete']));
  $teatcher=htmlspecialchars(addslashes($_POST['teatcher']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $examen=htmlspecialchars(addslashes($_POST['examen']));

  $tabstudentids=explode(",",$matriculedelete);
  $nbstudentids=count($tabstudentids);
  $content="";

  // echo $nbstudentids;

  //nous allons compter le nombre de modifcation de notes en attente de validation pour ce controles

  $nbcontrlestandby=$student->NBofexamenStudentstandbyvalidation($examen,$matiere,$teatcher,$classe,$session,$codeEtab,$matriculedelete);

  // echo $nbcontrlestandby;

  if($nbcontrlestandby==0)
  {
    $content=0;
  }else {
    $content=1;
  }

  echo $content;

}




?>
