<?php
session_start();
require_once('../class/Salle.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Etablissement.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();
$etabs= new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$transportype=htmlspecialchars($_POST['transportype']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);

$check=$etabs->ExistetrajetMode($transportype,$codeEtab,$sessionEtab);

if($check==0)
{
  $content=0;
}else {
  $content=1;
}

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{

  //recuperation des variables
  $content="";
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $trajet=htmlspecialchars($_POST['trajet']);

  //nous allons recuperer les montants du transport

  $transportations=$etabs->gettransportationsTrajet($codeEtab,$sessionEtab,$trajet);
  $nbligne=count($transportations);

  if($nbligne==0)
  {
    $content.="<option value=''>".L::NoTransportFess." </option>";
  }else if($nbligne>0)
  {
    $content.="<option value='' selected>".L::TransportationFeesSelect."</option>";
    $libelletrans="";

    foreach ($transportations as $values):
      if($values->type_trans=="MENSUEL")
      {
        $libelletrans=L::Mensualities;
      }else if($values->type_trans=="ANNUEL")
      {
        $libelletrans=L::Annuialities;
      }
        $content .= "<option value='".$values->frais_trans."-".$values->devises_trans."-".$values->type_trans."' >" .utf8_encode(utf8_decode($libelletrans)). "</option>";
    endforeach;

  }

echo $content;

}



 ?>
