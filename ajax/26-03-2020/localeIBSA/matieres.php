<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Teatcher.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$userId=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$matiere=new Matiere();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement
$schoolsofassign=$etabs->getEtablissementbyCodeEtab($codeEtabAssigner);

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$codesEtab=$schoolsofassign;
$codeEtabLocal=$etabs->getcodeEtabByLocalId($userId);
$classes=$classe->getAllClassesbyschoolCode($codeEtabLocal);
$teatchers=$teatcher->getAllTeatchersBySchoolCode($codeEtabLocal);
$matieres=$matiere->getAllMatiereOfThisSchool($codeEtabLocal);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabLocal);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabLocal);




if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabLocal);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabLocal,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabLocal,$libellesessionencours);
}

$etablissementType=$etabs->DetermineTypeEtab($codeEtabLocal);

$matiereslibelles=$etabs->getAllmatierelibellesEtab($libellesessionencours,$codeEtabLocal);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
        <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
  <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title">Gestion des Matières - Etablissement :</div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#">Matière</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active">Gestion des Matières - Etablissement :</li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['deletesubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['deletesubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'OK',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })


                </script>
                      <?php
                      unset($_SESSION['user']['deletesubjectok']);
                    }

                     ?>


              <?php

                    if(isset($_SESSION['user']['updatesubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['updatesubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'OK',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })


                </script>
                      <?php
                      unset($_SESSION['user']['updatesubjectok']);
                    }

                     ?>
              <?php

                    if(isset($_SESSION['user']['addsubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>


                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['addsubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'OK',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })
                </script>
                      <?php
                      unset($_SESSION['user']['addsubjectok']);
                    }

                     ?>

                     <div class="col-md-12 col-sm-12">
                                   <div class="panel tab-border card-box">
                                       <header class="panel-heading panel-heading-gray custom-tab ">
                                           <ul class="nav nav-tabs">
                                               <li class="nav-item"><a href="#home" data-toggle="tab" class="active"><i class="fa fa-bars"></i> Liste des matières</a>
                                               </li>
                                               <li class="nav-item"><a href="#about" data-toggle="tab"><i class="fa fa-plus-circle"></i> Ajouter Matière</a>
                                               </li>
                                               <li class="nav-item"><a href="#about1" data-toggle="tab"><i class="fa fa-plus-circle"></i> Libellé Matière</a>
                                               </li>

                                           </ul>
                                       </header>
                                       <div class="panel-body">
                                           <div class="tab-content">
                                               <div class="tab-pane active" id="home">
                                                 <div class="row">
                        <div class="col-md-12">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header></header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">

                                    <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                        <thead>
                                            <tr>

                                                <th> Classe </th>
                                                <th> Matière</th>
                                                <th> Professeur </th>

                                                <th> <?php echo L::Actions?> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                            //$matieres

                                            $i=1;
                                              foreach ($matieres as $value):
                                           ?>
                                            <tr class="odd gradeX">

                                                <td> <?php echo $value->libelle_classe;?></td>
                                                <td>
                                                    <?php echo $value->libelle_mat;?>
                                                </td>
                                                <td>
                                                  <?php echo $value->nom_compte." ".$value->prenom_compte;?>
                                                </td>

                                                <td class="valigntop">
                                                  <a href="#"  data-toggle="modal" data-target="#exampleModal<?php echo $value->id_mat?>"class="btn btn-info  btn-md " style="border-radius:3px;">
                                                    <i class="fa fa-pencil"></i>
                                                  </a>
                                                  <!--a href="#"  onclick="modify(<?php //echo $value->id_mat;?>)" class="btn btn-info  btn-md " style="border-radius:3px;">
                                                    <i class="fa fa-pencil"></i>
                                                  </a-->
                                                  <a href="#"  onclick="deleted(<?php echo $value->id_mat;?>)" class="btn btn-danger  btn-md " style="border-radius:3px;">
                                                    <i class="fa fa-trash-o"></i>
                                                  </a>
                                                </td>
                                            </tr>
                                            <div class="modal fade" id="exampleModal<?php echo $value->id_mat;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel<?php echo $value->id_mat;?>" aria-hidden="true">
                                            					    <div class="modal-dialog" role="document">
                                            					        <div class="modal-content">
                                            					            <div class="modal-header">
                                            					                <h4 class="modal-title" id="exampleModalLabel<?php echo $value->id_mat;?>">Modification Matière</h4>
                                            					                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            					                    <span aria-hidden="true">&times;</span>
                                            					                </button>
                                            					            </div>
                                            					            <div class="modal-body">
                                                                    <form  id="FormUpdateSubject<?php echo $value->id_mat;?>" class="form-horizontal" action="../controller/matiere.php" method="post">
                                                                        <div class="form-body">
                                                                          <div class="form-group row">
                                                                                  <label class="control-label col-md-3">Matière
                                                                                      <span class="required"> * </span>
                                                                                  </label>
                                                                                  <div class="col-md-8">
                                                                                      <input type="text" name="matiere<?php echo $value->id_mat;?>" id="matiere<?php echo $value->id_mat;?>" onchange="erasedMat(<?php echo $value->id_mat;?>)" data-required="1" value="<?php echo $value->libelle_mat;?>" placeholder="Entrer la classe" class="form-control input-height" />
                                                                                      <p id="messageMat<?php echo $value->id_mat;?>"></p>
                                                                                     </div>
                                                                           </div>
                                                                           <div class="form-group row">
                                                                                   <label class="control-label col-md-3"><?php echo L::Coefs ?>
                                                                                       <span class="required"> * </span>
                                                                                   </label>
                                                                                   <div class="col-md-8">
                                                                                       <input type="number" min="0.5" name="coef<?php echo $value->id_mat;?>" id="coef<?php echo $value->id_mat;?>" onchange="erasedCoef(<?php echo $value->id_mat;?>)" data-required="1" value="<?php echo $value->coef_mat;?>" placeholder="<?php echo L::EnterCoefSubjectControlLib ?>" class="form-control input-height" />
                                                                                       <p id="messageCoef<?php echo $value->id_mat;?>"></p>
                                                                                      </div>
                                                                            </div>

                                                                          <div class="form-group row">
                                                                                  <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                                                                      <span class="required"> * </span>
                                                                                  </label>
                                                                                  <div class="col-md-8">
                                                                                      <select class="form-control input-height" name="classe<?php echo $value->id_mat;?>" id="classe<?php echo $value->id_mat;?>" onchange="erasedClasse(<?php echo $value->id_mat;?>)"   style="width:100%">
                                                                                          <option value=""><?php echo L::Selectclasses ?></option>
                                                                                          <?php
                                                                                          $i=1;
                                                                                            foreach ($classes as $valueClasse):
                                                                                            ?>
                                                                                            <option <?php if($value->classe_mat==$valueClasse->id_classe){echo "selected";}?> value="<?php echo utf8_encode(utf8_decode($valueClasse->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($valueClasse->libelle_classe)); ?></option>

                                                                                            <?php
                                                                                                                             $i++;
                                                                                                                             endforeach;
                                                                                                                             ?>

                                                                                      </select>
                                                                                      <p id="messageClasse<?php echo $value->id_mat;?>"></p>
                                                                              </div>
                                                                            </div>

                                                                        <div class="form-group row">
                                                                                <label class="control-label col-md-3"><?php echo L::ProfsMenusingle ?>
                                                                                    <span class="required"> * </span>
                                                                                </label>
                                                                                <div class="col-md-8">

                                                                                  <select class="form-control input-height" id="teatcher<?php echo $value->id_mat;?>" name="teatcher<?php echo $value->id_mat;?>" style="width:100%" onchange="erasedTeatcher(<?php echo $value->id_mat;?>)">
                                                                                      <option value=""><?php echo L::TeatcherSelected ?></option>
                                                                                      <?php
                                                                                      $i=1;
                                                                                        foreach ($teatchers as $valueTeather):
                                                                                        ?>
                                                                                        <option <?php if($value->teatcher_mat==$valueTeather->id_compte){echo "selected";}?> value="<?php echo $valueTeather->id_compte?>"><?php echo utf8_encode(utf8_decode($valueTeather->nom_compte." - ".$valueTeather->prenom_compte)) ?></option>

                                                                                        <?php
                                                                                                                         $i++;
                                                                                                                         endforeach;
                                                                                                                         ?>

                                                                                  </select>
                                                                                  <p id="messageTeatcher<?php echo $value->id_mat;?>"></p>
                                                                                  <input type="hidden" name="etape" id="etape" value="2"/>
                                                                                  <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabLocal; ?>"/>
                                                                                  <input type="hidden" name="idmat" id="idmat" value="<?php echo $value->id_mat; ?>"/>
                                                                               </div>
                                                                            </div>




                                                      <div class="form-actions">
                                                                            <div class="row">
                                                                                <div class="offset-md-3 col-md-9">
                                                                                    <button type="submit" onclick="check(<?php echo $value->id_mat;?>)" class="btn btn-info">Modifier</button>
                                                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                                                                                </div>
                                                                              </div>
                                                                           </div>
                                                    </div>
                                                                    </form>
                                            					            </div>

                                            					        </div>
                                            					    </div>
                                            					</div>

                                            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                            <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
                                            <!-- <script src="../assets2/plugins/select2/js/select2.js" ></script>
                                            <script src="../assets2/js/pages/select2/select2-init.js" ></script> -->
                                            <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
                                            <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                            <script>

                                            function soumettre()
                                            {
                                              $("#FormUpdateSubject<?php echo $value->id_mat;?>").submit();
                                            }

                                            function myFunction(idcompte)
                                            {
                                              //var url="detailslocal.php?compte="+idcompte;
                                            document.location.href="detailsadmin.php?compte="+idcompte;
                                            }

                                            function erasedMat(id)
                                            {
                                              document.getElementById("messageMat"+id).innerHTML = "";

                                            }

                                            function erasedCoef(id)
                                            {
                                              document.getElementById("messageCoef"+id).innerHTML = "";
                                            }

                                            function erasedClasse(id)
                                            {
                                              document.getElementById("messageClasse"+id).innerHTML = "";
                                            }

                                            function erasedTeatcher(id)
                                            {
                                              document.getElementById("messageTeatcher"+id).innerHTML = "";
                                            }

                                            function modify(id)
                                            {


                                              Swal.fire({
                                title: '<?php echo L::WarningLib ?>',
                                text: "<?php echo L::DoyouReallyModifyingSubjects ?>",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: '<?php echo L::ModifierBtn ?>',
                                cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                              }).then((result) => {
                                if (result.value) {
                                  document.location.href="updatesubject.php?compte="+id;
                                }else {

                                }
                              })
                                            }

                                            function deleted(id)
                                            {
                                              var classe="<?php echo $value->classe_mat;?>";
                                              var codeEtab="<?php echo $codeEtabAssigner;?>";
                                              Swal.fire({
                                title: '<?php echo L::WarningLib ?>',
                                text: "Voulez vous vraiment supprimer cette matière",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: '<?php echo L::DeleteLib ?>',
                                cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                              }).then((result) => {
                                if (result.value) {
                                  document.location.href="../controller/matiere.php?etape=3&matiere="+id+"&classe="+classe+"&codeEtab="+codeEtab;
                                }else {

                                }
                              })
                                            }

                                            function check(id)
                                            {
                                              //recuperation des variables
                                              var matiere=$("#matiere"+id).val();
                                              var coefficient=$("#coef"+id).val();
                                              var classe=$("#classe"+id).val();
                                              var professeur=$("#teatcher"+id).val();

                                              if(matiere==""||coefficient==""||classe==""||professeur=="")
                                              {
                                                if(matiere=="")
                                                {
                                                   document.getElementById("messageMat"+id).innerHTML = "<font color=\"red\">Merci de renseigner la Matière !</font>";
                                                }

                                                if(coefficient=="")
                                                {
                                                  document.getElementById("messageCoef"+id).innerHTML = "<font color=\"red\">Merci de renseigner le Coefficient de la Matière !</font>";
                                                }

                                                if(classe=="")
                                                {
                                                  document.getElementById("messageClasse"+id).innerHTML = "<font color=\"red\">Merci de selectionner la Classe !</font>";
                                                }

                                                if(professeur=="")
                                                {
                                                  document.getElementById("messageTeatcher"+id).innerHTML = "<font color=\"red\">Merci de selectionner le Professeur !</font>";
                                                }




                                            }else {

                                                  if(coefficient==0)
                                                  {
                                                      document.getElementById("messageCoef"+id).innerHTML = "<font color=\"red\">Le Coefficient de la Matière doit être supérieur ou égale à 1 !</font>";
                                                  }else if(coefficient>=0 && professeur!="" && classe!="" && matiere!="") {
                                                    soumettre();
                                                  }
                                            }

                                            }



















                                            </script>

                                            <?php
                                            $i++;
                                            endforeach;
                                            ?>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                                               </div>
                                               <div class="tab-pane" id="about">
                                                 <div class="row">
                                                     <div class="col-md-12 col-sm-12">
                                                         <div class="card card-box">
                                                             <div class="card-head">
                                                                 <header></header>

                                                             </div>

                                                             <div class="card-body" id="bar-parent">
                                                                 <form  id="FormAddSubject" class="form-horizontal" action="../controller/matiere.php" method="post">
                                                                     <div class="form-body">
                                                                       <div class="form-group row">
                                                                               <label class="control-label col-md-3">Matière
                                                                                   <span class="required"> * </span>
                                                                               </label>
                                                                               <div class="col-md-5">
                                                                                 <select class="form-control input-height" name="matiere" id="matiere"  style="width:100%">
                                                                                     <option value=""><?php echo L::SelectSubjects ?></option>
                                                                                     <?php
                                                                                     $i=1;
                                                                                       foreach ($matiereslibelles as $value):
                                                                                       ?>
                                                                                       <option value="<?php echo utf8_encode(utf8_decode($value->libelle_matlib)); ?>"><?php echo utf8_decode($value->libelle_matlib); ?></option>

                                                                                       <?php
                                                                                                                        $i++;
                                                                                                                        endforeach;
                                                                                                                        ?>

                                                                                 </select>
                                                                                   <!--input type="text" name="matiere" id="matiere" data-required="1" placeholder="Entrer la classe" class="form-control input-height" /-->

                                                                                   <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabLocal; ?>"/>
                                                                                   <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">

                                                                                 </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                                <label class="control-label col-md-3"><?php echo L::Coefs ?>
                                                                                    <span class="required"> * </span>
                                                                                </label>
                                                                                <div class="col-md-5">
                                                                                    <input type="number" min="0.5" name="coef" id="coef" data-required="1" placeholder="<?php echo L::EnterCoefSubjectControlLib ?>" class="form-control input-height" /> </div>
                                                                         </div>
                                                                         <?php
                                                                           if($etablissementType==1||$etablissementType==3)
                                                                           {
                                                                             ?>
                                                                             <div class="form-group row">
                                                                                     <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                                                                         <span class="required"> * </span>
                                                                                     </label>
                                                                                     <div class="col-md-5">
                                                                                         <select class="form-control input-height" name="classe" id="classe"  style="width:100%" onchange="determineteatcher()">
                                                                                             <option value=""><?php echo L::Selectclasses ?></option>
                                                                                             <?php
                                                                                             $i=1;
                                                                                               foreach ($classes as $value):
                                                                                               ?>
                                                                                               <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                                                               <?php
                                                                                                                                $i++;
                                                                                                                                endforeach;
                                                                                                                                ?>

                                                                                         </select>
                                                                                          <input type="hidden" name="etape" id="etape" value="1"/>
                                                                                 </div>
                                                                               </div>

                                                                             <div class="form-group row">
                                                                                   <label class="control-label col-md-3">Enseignant
                                                                                       <span class="required"> * </span>
                                                                                   </label>
                                                                                   <div class="col-md-5">

                                                                                     <select class="form-control input-height" id="teatcher" name="teatcher" style="width:100%">
                                                                                         <option value=""><?php echo L::TeatcherSelected ?></option>
                                                                                     </select>
                                                                                     </div>
                                                                               </div>
                                                                             <?php
                                                                           }else {
                                                                             ?>
                                                                             <div class="form-group row">
                                                                                     <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                                                                         <span class="required"> * </span>
                                                                                     </label>
                                                                                     <div class="col-md-5">
                                                                                         <select class="form-control input-height" multiple="multiple"  name="classe[]" id="classe"  style="width:100%">
                                                                                             <option value=""><?php echo L::Selectclasses ?></option>
                                                                                             <?php
                                                                                             $i=1;
                                                                                               foreach ($classes as $value):
                                                                                               ?>
                                                                                               <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                                                               <?php
                                                                                                                                $i++;
                                                                                                                                endforeach;
                                                                                                                                ?>

                                                                                         </select>
                                                                                          <input type="hidden" name="etape" id="etape" value="4"/>
                                                                                 </div>
                                                                               </div>

                                                                             <div class="form-group row">
                                                                                   <label class="control-label col-md-3"><?php echo L::ProfsMenusingle ?>
                                                                                       <span class="required"> * </span>
                                                                                   </label>
                                                                                   <div class="col-md-5">

                                                                                     <select class="form-control input-height" id="teatcher" name="teatcher" style="width:100%">
                                                                                         <option value=""><?php echo L::TeatcherSelected ?></option>
                                                                                         <?php
                                                                                         $i=1;
                                                                                           foreach ($teatchers as $value):
                                                                                           ?>
                                                                                           <option value="<?php echo $value->id_compte?>"><?php echo utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)) ?></option>

                                                                                           <?php
                                                                                                                            $i++;
                                                                                                                            endforeach;
                                                                                                                            ?>

                                                                                     </select>
                                                                                     </div>
                                                                               </div>
                                                                             <?php
                                                                           }
                                                                          ?>





                                                   <div class="form-actions">
                                                                         <div class="row">
                                                                             <div class="offset-md-3 col-md-9">
                                                                                 <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                                                                 <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                             </div>
                                                                           </div>
                                                                        </div>
                                                 </div>
                                                                 </form>
                                                             </div>
                                                         </div>
                                                     </div>

                                                 </div>
                                                </div>
                                                <div class="tab-pane" id="about1">
                                                  <div class="row">
                                                      <div class="col-md-12 col-sm-12">
                                                          <div class="card card-box">
                                                              <div class="card-head">
                                                                  <header></header>

                                                              </div>

                                                              <div class="card-body" id="bar-parent">
                                                                  <form  id="FormAddmatlib" class="form-horizontal" action="../controller/matiere.php" method="post">
                                                                      <div class="form-body">
                                                                        <div class="form-group row">
                                                                                <label class="control-label col-md-3">Libellé Matière
                                                                                    <span class="required"> * </span>
                                                                                </label>
                                                                                <div class="col-md-5">
                                                                                    <input type="text" name="matierelib" id="matierelib" data-required="1" placeholder="Entrer le libellé de la matière" class="form-control" />

                                                                                    <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabLocal; ?>"/>
                                                                                      <input type="hidden" name="etape" id="etape" value="5"/>
                                                                                    <!--input type="hidden" name="libellesession" id="libellesession" value="<?php //echo $libellesessionencours; ?>"-->

                                                                                  </div>
                                                                         </div>







                                                    <div class="form-actions">
                                                                          <div class="row">
                                                                              <div class="offset-md-3 col-md-9">
                                                                                  <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                                                                  <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                              </div>
                                                                            </div>
                                                                         </div>
                                                  </div>
                                                                  </form>
                                                              </div>
                                                          </div>
                                                      </div>

                                                  </div>
                                                 </div>


                                           </div>
                                       </div>
                                   </div>
                               </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>

   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script>

 function determineteatcher()
 {
   var classe=$("#classe").val();
   var session="<?php echo $libellesessionencours; ?>";
   var codeEtab="<?php echo $codeEtabAssigner; ?>";
   var etape=11;

   $.ajax({
            url: '../ajax/matiere.php',
            type: 'POST',
            async:false,
            data: 'classe='+ classe+'&etape='+etape+'&session='+session+'&codeEtab='+codeEtab,
            dataType: 'text',
            success: function (response, statut) {


              $("#teatcher").html("");
              $("#teatcher").html(response);
            }
          });


 }

 jQuery(document).ready(function() {





$("#teatcher").select2();
$("#matiere").select2();

<?php
if($etablissementType==1||$etablissementType==3)
{
  ?>
$("#classe").select2();
  <?php
}else {
  ?>
  $("#classe").select2({

    tags: true,

  tokenSeparators: [',', ' ']

  });
  <?php
}
 ?>


  $("#FormAddmatlib").validate({

    errorPlacement: function(label, element) {
    label.addClass('mt-2 text-danger');
    label.insertAfter(element);
  },
  highlight: function(element, errorClass) {
    $(element).parent().addClass('has-danger')
    $(element).addClass('form-control-danger')
  },
  success: function (e) {
        $(e).closest('.control-group').removeClass('error').addClass('info');
        $(e).remove();
    },
      rules:{
        matierelib:"required"
      },
      messages:{
        matierelib:"Merci de renseigner le libellé de la matitère"
      },
        submitHandler: function(form) {
          //nous allons verifié si cet libelle de matiere n'existe pas deja au niveau de la base

          var etape=13;

          $.ajax({
            url: '../ajax/matiere.php',
            type: 'POST',
            async:true,
            data: 'matiere=' + $("#matierelib").val()+ '&etape=' + etape+'&codeEtab='+$("#FormAddmatlib #codeEtab").val()+'&session='+$("#FormAddmatlib #libellesession").val(),
            dataType: 'text',
            success: function (content, statut) {

              // alert(content);
              if(content==0)
              {
                //nouveau libelle a ajouter
                form.submit();
              }else if(content>0)
              {
                //cet libelle existe deja  dans le sysyème

                Swal.fire({
                type: 'warning',
                title: '<?php echo L::WarningLib ?>',
                text: "Cet libellé existe deja dans le système",

              })
              }
            }
          });
        }

  });



   $("#FormAddSubject").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

        matiere:"required",
        classe:"required",
        teatcher:"required",
        coef:{
      required: true,
      number: true,
      // range:[0.5,10]
    }


      },
      messages: {
        matiere:"Merci de renseigner la matière",
        classe:"<?php echo L::PleaseSelectclasserequired ?>",
        teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
        coef:{
      required:"Merci de renseigner le coefficient de la matière",
      // number:"Merci de renseigner un nombre",
      min:"Merci de renseigner le coefficient de la matière"
    }

      },
      submitHandler: function(form) {


        var etape=1;

         $.ajax({
           url: '../ajax/matiere.php',
           type: 'POST',
           async:true,
           data: 'matiere=' + $("#matiere").val()+ '&etape=' + etape+'&classe='+$("#classe").val()+'&teatcher='+$("#teatcher").val()+'&codeEtab='+$("#codeEtab").val()+'&coefficient='+$("#coef").val(),
           dataType: 'text',
           success: function (content, statut) {


             if(content==0)
             {
               //cette matière n'existe pas encore pour cette classe

               form.submit();

             }else if(content==1)
             {
               //il est question d'un nouveau professeur pour cette matière
               Swal.fire({
 title: '<?php echo L::WarningLib ?>',
 text: "Cette Matière est dejà dispenser par un Professeur",
 type: 'warning',
 showCancelButton: true,
 confirmButtonColor: '#3085d6',
 cancelButtonColor: '#d33',
 confirmButtonText: 'Modifier le Professeur',
 cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
 if (result.value) {

//nous allons modifier la valeur de etape pour 4
$("#FormAddSubject #etape").val(4);
$("#FormAddSubject").submit();

 }else {

 }
})

             }else if(content==2)
             {
               // il s'agit du meme professeur pour cette matiere

               Swal.fire({
               type: 'warning',
               title: '<?php echo L::WarningLib ?>',
               text: "Cette Matière existe deja dans le système",

             })
             }

           }
         });

             }


           });
      });






 </script>
    <!-- end js include path -->
  </body>

</html>
