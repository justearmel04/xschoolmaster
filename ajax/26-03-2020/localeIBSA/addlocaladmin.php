<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();


$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$matiere=new Matiere();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$examens=$etabs->getAllexamensOfSchool($codeEtabAssigner);


$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
  $matieres=$matiere->getAllControleMatiereOfThisSchool($codeEtabAssigner,$libellesessionencours);
}

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
  <link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
     <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>


	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::AddLocalAdmins?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="#"><?php echo L::LocalAdmins?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::AddLocalAdmins?></li>
                          </ol>
                      </div>
                  </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">
              <?php

                    if(isset($_SESSION['user']['addlocalok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: '<?php echo L::Felicitations ?>',
  text: "<?php echo $_SESSION['user']['addlocalok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::AddNews ?>',
  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="index.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addlocalok']);
                    }

                     ?>
<br/>

                </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-sm-12">
                  <div class="card card-box">
                      <div class="card-head">
                          <header><?php echo L::LocalAdminsInfos?></header>

                      </div>


                      <div class="card-body" id="bar-parent">
                          <form  id="FormAddLocalAd" class="form-horizontal" action="../controller/localadmin.php" method="post" enctype="multipart/form-data">
                              <div class="form-body">
                                <input type="hidden" id="libetab" name="libetab" value="<?php echo $codeEtabAssigner;?>" />
                                 <input type="hidden"  name="comptead" id="comptead" value="ADMIN LOCAL" />
                                 <div class="form-group row">
                                      <label class="control-label col-md-3"><?php echo L::Name?>
                                          <span class="required"> * </span>
                                      </label>
                                      <div class="col-md-5">
                                          <input type="text" name="nomad" id="nomad" data-required="1" placeholder="<?php echo L::EnterName ?>" class="form-control input-height" /> </div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="control-label col-md-3"><?php echo L::PreName?>
                                          <span class="required"> * </span>
                                      </label>
                                      <div class="col-md-5">
                                          <input type="text" name="prenomad" id="prenomad" data-required="1" placeholder="<?php echo L::Enterparentprename ?>" class="form-control input-height" /> </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="control-label col-md-3"><?php echo L::BirthstudentTab?>
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-5">
                                        <input type="text" placeholder="Entrer la date de naissance" name="datenaisad" id="datenaisad" data-mask="99/99/9999" class="form-control input-height">
                                          <span class="help-block"><?php echo L::Datesymbole ?></span>
                                    </div>
                                    </div>
                                  <div class="form-group row">
                                      <label class="control-label col-md-3"><?php echo L::PhonestudentTab?>
                                          <span class="required">*  </span>
                                      </label>
                                      <div class="col-md-5">
                                          <input name="contactad" id="contactad" type="text" placeholder="Entrer le contact " class="form-control input-height" /> </div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="control-label col-md-3"><?php echo L::Fonction?>
                                          <span class="required">*  </span>
                                      </label>
                                      <div class="col-md-5">
                                        <select name="fonctionad" id="fonctionad" placeholder="Entrer la fonction" class="form-control input-height" style="width:100%">
                                                   <option value=""> <?php echo L::SelectFonction?> </option>
                                                   <option value="Proviseur"> Proviseur </option>
                                                   <option value="Censeur"> Censeur </option>
                                                   <option value="Surveillant"> Surveillant Genaral</option>
                                                   <option value="Professeur">Professeur</option>
                                                   <option value="Intendant">Intendant</option>
                                                   <option value="Autre">Autres</option>
                                             </select>

                                         </div>
                                  </div>

                                  <div class="form-group row">
                                      <label class="control-label col-md-3"><?php echo L::EmailstudentTab?>
                                        <span class="required">*  </span>
                                      </label>
                                      <div class="col-md-5">
                                          <div class="input-group">
                                              <span class="input-group-addon">
                                                      <i class="fa fa-envelope"></i>
                                                  </span>
                                              <input type="text" class="form-control input-height" name="emailad" id="emailad" placeholder="Entrer votre Adresse email"> </div>
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="control-label col-md-3"><?php echo L::Logincnx?>
                                          <span class="required">*  </span>
                                      </label>
                                      <div class="col-md-5">
                                          <input name="loginad" id="loginad" type="text" placeholder="Entrer le Login " class="form-control input-height" /> </div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="control-label col-md-3"><?php echo L::Passcnx?>
                                          <span class="required">*  </span>
                                      </label>
                                      <div class="col-md-5">
                                          <input name="passad" id="passad" type="password" placeholder="Entrer le Mot de passe " class="form-control input-height" /> </div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="control-label col-md-3"><?php echo L::ConfirmPasscnx?>
                                          <span class="required">*  </span>
                                      </label>
                                      <div class="col-md-5">
                                          <input name="confirmtad" id="confirmtad" type="password" placeholder="Confirmer le Mot de passe " class="form-control input-height" /> </div>
                                  </div>

                                  <div class="form-group row">
                                      <label class="control-label col-md-3"><?php echo L::Pictures?>
                                        <span class="required">  </span>
                                      </label>
                                      <div class="compose-editor">
                                        <input type="file" id="photoad" name="photoad" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="../photo/user5.jpg" data-allowed-file-extensions="gif png jpg jpeg pjpeg" />
                                        <input type="hidden" name="etape" id="etape" value="1"/>
                                    </div>
                                  </div>

              <div class="form-actions">
                                  <div class="row">
                                      <div class="offset-md-3 col-md-9">
                                          <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                          <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                      </div>
                                    </div>
                                 </div>
              </div>
                          </form>
                      </div>
                  </div>
              </div>

            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
      <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
       <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
  <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
  <script src="../assets2/js/dropify.js"></script>

 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>


   <script>
   // $("#libetab").select2();
   $("#fonctionad").select2();
jQuery(document).ready(function() {

  // $('#FormAddLocalAd').submit(function(e) {
  //
  //    e.preventDefault();
  //
  //    var contact=$("#contactad").val();
  //    $(".error").remove();
  //
  //     var regEx = /^[0-9]{9}$ /;
  //
  //     if (!contact.match(regEx)) {
  //         $('#contactad').after('<span class="error" style="color:red">Merci de renseigner un numéro de téléphone</span>');
  //
  //     }else {
  //       e.sumit();
  //     }
  //
  // });

  $("#FormAddLocalAd").validate({
    errorPlacement: function(label, element) {
    label.addClass('mt-2 text-danger');
    label.insertAfter(element);
  },
  highlight: function(element, errorClass) {
    $(element).parent().addClass('has-danger')
    $(element).addClass('form-control-danger')
  },
  success: function (e) {
        $(e).closest('.control-group').removeClass('error').addClass('info');
        $(e).remove();
    },
    rules:{
      passad: {
          required: true,
          minlength: 6
      },
      confirmtad:{
          required: true,
          minlength: 6,
          equalTo:'#passad'
      },
      fonctionad:"required",

      loginad:"required",
      emailad: {
                 required: true,
                 email: true
             },
      contactad:{
              required: true,
              digits:true,
              minlength: 8,
              maxlength:12,
          },
      datenaisad:"required",
      prenomad:"required",
      nomad:"required",
      libetab:"required"

    },
    messages: {
      confirmtad:{
          required:"<?php echo L::Confirmcheck?>",
          minlength:"<?php echo L::Confirmincheck?>",
          equalTo: "<?php echo L::ConfirmSamecheck?>"
      },
      contactad:{
        required: "<?php echo L::PleaseEnterPhoneNumber?>",
        minlength: "<?php echo L::PhoneNumberCaracteres?>",
        digits: "<?php echo L::PleaseEnterDigitsOnly?>",
        maxlength: "<?php echo L::NumbercaractersMax?>",
      },
      passad: {
          required:"<?php echo L::Passcheck?>",
          minlength:"<?php echo L::Confirmincheck?>"
      },
      loginad:"<?php echo L::Logincheck?>",
      emailad:"<?php echo L::PleaseEnterEmailAdress?>",
      // contactad:"<?php echo L::PleaseEnterPhoneNumber?>",
      datenaisad:"<?php echo L::PleaseEnterPhonestudentTab?>",
      prenomad:"<?php echo L::PleaseEnterPrename?>",
      nomad:"<?php echo L::PleaseEnterName?>",
      fonctionad:"<?php echo L::PleaseEnterFonction?>",
      libetab:"<?php echo L::PleaseSelectSchoolEnseignement?>"
    },
    submitHandler: function(form) {

      var etape=1;
      $.ajax({
        url: '../ajax/localadmin.php',
        type: 'POST',
        async:false,
        data: 'login=' + $("#loginad").val()+ '&email=' + $("#emailad").val() +'&codeetab=' + $("#libetab").val() + '&etape=' + etape,
        dataType: 'text',
        success: function (content, statut) {

          if(content==1)
          {
            Swal.fire({
            type: 'warning',
            title: '<?php echo L::WarningLib ?>',
            text: '<?php echo L::LoginAllredadyExisteInDbb?>',

            })
          }else if(content==0)
          {
            form.submit();
          }
        }

      });

    }



  });
});

   </script>
    <!-- end js include path -->
  </body>

</html>
