<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
$classe = new Classe();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //ajout d'un controle


  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $teatcher=htmlspecialchars(addslashes($_POST['teatcher']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $controle=htmlspecialchars(addslashes($_POST['controle']));
  $coef=htmlspecialchars(addslashes($_POST['coef']));
  $datectrl=htmlspecialchars(addslashes($_POST['datectrl']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
  $typesess=htmlspecialchars(addslashes($_POST['typesess']));

  //insertion dans la table controle

$etab->AddControleClasseSchool($controle,$datectrl,$classe,$matiere,$teatcher,$coef,$codeEtab,$libellesession,$typesess);



}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recuperation des variables

  $idctrl=htmlspecialchars(addslashes($_POST['idctrl']));

  $classe=htmlspecialchars(addslashes($_POST['classe'.$idctrl]));
  $matiere=htmlspecialchars(addslashes($_POST['matiere'.$idctrl]));
  $teatcher=htmlspecialchars(addslashes($_POST['teatcher'.$idctrl]));
  $controle=htmlspecialchars(addslashes($_POST['controle'.$idctrl]));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $coef=htmlspecialchars(addslashes($_POST['coef'.$idctrl]));
  $datectrl=htmlspecialchars(addslashes($_POST['datectrl'.$idctrl]));

  //mise à jour de la table $controle

  $etab->UpdateControleClasseSchool($classe,$matiere,$teatcher,$controle,$codeEtab,$coef,$datectrl,$idctrl);


}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //recuperation des variables

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $teatcher=htmlspecialchars(addslashes($_POST['teatcher']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
  $controle=htmlspecialchars(addslashes($_POST['controle']));
  $coef=htmlspecialchars(addslashes($_POST['coef']));
  $datectrl=htmlspecialchars(addslashes($_POST['datectrl']));

  //ajouter dans la table controle

  $etab->AddControleClasseSchoolPrimary($controle,$datectrl,$classe,$matiere,$teatcher,$coef,$codeEtab,$libellesession);



}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{

  //ajout d'un controle


  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $teatcher=htmlspecialchars(addslashes($_POST['teatcher']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $controle=htmlspecialchars(addslashes($_POST['controle']));
  $coef=htmlspecialchars(addslashes($_POST['coef']));
  $datectrl=htmlspecialchars(addslashes($_POST['datectrl']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
  // $typesess=htmlspecialchars(addslashes($_POST['typesess']));

  //insertion dans la table controle

  $etab->AddControleClasseSchoolPrimary($controle,$datectrl,$classe,$matiere,$teatcher,$coef,$codeEtab,$libellesession);





}

if(isset($_GET['etape'])&&($_GET['etape']==3))
{
  //recupération des variables

  $matiere=htmlspecialchars(addslashes($_GET['matiere']));
  $classe=htmlspecialchars(addslashes($_GET['classe']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
  $controleid=htmlspecialchars(addslashes($_GET['controleid']));

  //nous allons supprimer le controle

  $etab->deletedControleClassesSchool($controleid,$codeEtab,$classe,$matiere);

}


?>
