<?php

session_start();

require_once('../class/Message.php');

require_once('../class/Parent.php');

require_once('../class/Student.php');

require_once('../class/Etablissement.php');

$messages = new Message();

$etabs=new Etab();



//recuperation des variables



if(isset($_POST['etape'])&&($_POST['etape']==1))

{

  //recuperation des variables



  $paiementid=htmlspecialchars(addslashes($_POST['paiementid']));

  $parentid=htmlspecialchars(addslashes($_POST['parentid']));



  //nous allons modifier le statut du paiement qui va passer à 1 et nous allons ajouter la date et l'id du validateur



  $dateday=date("Y-m-d");

  $validateurid=$_SESSION['user']['IdCompte'];

  $statutpaie=1;



  //nous allons recuperer les infos sur le nombre de souscription ainsi que le matricule des eleves



  $paiementdatas=$etabs->getpaiementInfos($paiementid,$parentid);

  $tabdatapaie=explode("*",$paiementdatas);



  $nbstudent=$tabdatapaie[0];

  $subscribes=$tabdatapaie[1];

  $studentids=$tabdatapaie[2];



  $status=1;



  //nous allons faire la mise à jour dans la table paiementab



  $etabs->UpdatePaiementsubscribevalidation($statutpaie,$dateday,$validateurid,$paiementid,$parentid);



  //nous allons ajouter les elements dans la table abonnementhisto pour chaque eleves



  if($nbstudent==1)

  {

      //la souscription concerne un seul eleve

      $studentid=$tabdatapaie[2];

      //nous allons determiner le type d'offre choisi

      $datasubscribes=explode("@",$subscribes);

      $offreid=$datasubscribes[0];

      $matriculestudent=$datasubscribes[1];

      //nous allons determiner le nombre de jour de l'offre et la date de fin

      $joursabonnement=$etabs->getAbonnementdatas($offreid);

      $datedebut=$dateday;

      $datefin=date("Y-m-d", strtotime("+".$joursabonnement." day", strtotime($datedebut)));

      //nous allons determiner le codeEtab ou est inscrit l'enfants



      $codedatas=$etabs->getCodeEtabAndSessionOfStudentsubscribe($studentid);



      $tabcodedatas=explode("*",$codedatas);



      $codeEtab=$tabcodedatas[0];

      $sessionEtab=$tabcodedatas[1];



      //insertion dans la table abonnementhisto



      $etabs->AddHistoAbonnement($dateday,$parentid,$studentid,$datedebut,$datefin,$offreid,$codeEtab,$sessionEtab,$paiementid);



      //insertion dans la table notificationstate



      //nous allons verifier si nous avons deja une ligne dans la table pour la relation parentid- studentIdent



      $nbnotifstate=$etabs->getNbOfnotifstateparent($parentid,$studentid);



      if($nbnotifstate==0)

      {

        // premiere fois nous allons donc ajouter dans la table notificationstate

        $etabs->AddNotificationState($parentid,$studentid,$sessionEtab,$status);



      }else if($nbnotifstate>0)

      {

        //pas la premiere fois , nous allons verifier la session

        $nbnotifsessionstate=$etabs->getNbOfnotifstateparentsession($parentid,$studentid,$sessionEtab);



        if($nbnotifsessionstate==0)

        {

            //mise a jour de la session et su statut

            $etabs->Updatenotificationstateboth($parentid,$studentid,$sessionEtab,$status);



        }else if($nbnotifsessionstate>0)

        {

          //mise a jour du statut



          $etabs->Updatenotificationstatesingle($parentid,$studentid,$sessionEtab,$status);

        }



      }



  }else if($nbstudent>1)

  {

    //la souscription concerne plus d'un eleve



    $datapartstudent=explode(",",$subscribes);

    $dataidstudents=explode(",",$studentids);



    for($i=0;$i<$nbstudent;$i++)

    {

      $studentid=$dataidstudents[$i];



      //nous allons determiner le type d'offre choisi

      $datasubscribes=explode("@",$datapartstudent[$i]);

      $offreid=$datasubscribes[0];

      $matriculestudent=$datasubscribes[1];



      //nous allons determiner le nombre de jour de l'offre et la date de fin

      $joursabonnement=$etabs->getAbonnementdatas($offreid);

      $datedebut=$dateday;

      $datefin=date("Y-m-d", strtotime("+".$joursabonnement." day", strtotime($datedebut)));

      //nous allons determiner le codeEtab ou est inscrit l'enfants



      $codedatas=$etabs->getCodeEtabAndSessionOfStudentsubscribe($studentid);



      $tabcodedatas=explode("*",$codedatas);



      $codeEtab=$tabcodedatas[0];

      $sessionEtab=$tabcodedatas[1];



      //insertion dans la table abonnementhisto



      $etabs->AddHistoAbonnement($dateday,$parentid,$studentid,$datedebut,$datefin,$offreid,$codeEtab,$sessionEtab,$paiementid);



      //insertion dans la table notificationstate



      //nous allons verifier si nous avons deja une ligne dans la table pour la relation parentid- studentIdent



      $nbnotifstate=$etabs->getNbOfnotifstateparent($parentid,$studentid);



      if($nbnotifstate==0)

      {

        // premiere fois nous allons donc ajouter dans la table notificationstate

        $etabs->AddNotificationState($parentid,$studentid,$sessionEtab,$status);



      }else if($nbnotifstate>0)

      {

        //pas la premiere fois , nous allons verifier la session

        $nbnotifsessionstate=$etabs->getNbOfnotifstateparentsession($parentid,$studentid,$sessionEtab);



        if($nbnotifsessionstate==0)

        {

            //mise a jour de la session et su statut

            $etabs->Updatenotificationstateboth($parentid,$studentid,$sessionEtab,$status);



        }else if($nbnotifsessionstate>0)

        {

          //mise a jour du statut



          $etabs->Updatenotificationstatesingle($parentid,$studentid,$sessionEtab,$status);

        }



      }



    }







  }



    $_SESSION['user']['Updateadminok']="Souscription valider avec succès";



}if(isset($_POST['etape'])&&($_POST['etape']==2))

{

  //rejeter la reception des fonds



  //recupertaion des variables



  $paiementid=htmlspecialchars(addslashes($_POST['paiementid']));

  $parentid=htmlspecialchars(addslashes($_POST['parentid']));

  $statutpaie=-1;

  $dateday=date("Y-m-d");

  $validateurid=$_SESSION['user']['IdCompte'];





  //changer le statut à 1 pour les cas rejet



  $etabs->UpdatePaiementsubscribevalidation($statutpaie,$dateday,$validateurid,$paiementid,$parentid);



  $_SESSION['user']['Updateadminok']="Souscription rejeter avec succès";



}else if(isset($_POST['etape'])&&($_POST['etape']==3))

{

  //rejeter la reception des fonds



  //recupertaion des variables



  $paiementid=htmlspecialchars(addslashes($_POST['paiementid']));

  $parentid=htmlspecialchars(addslashes($_POST['parentid']));

  $statutpaie=4;

  $dateday=date("Y-m-d");

  $validateurid=$_SESSION['user']['IdCompte'];





  //changer le statut à 1 pour les cas rejet



  $etabs->UpdatePaiementsubscribevalidation($statutpaie,$dateday,$validateurid,$paiementid,$parentid);



  $_SESSION['user']['Updateadminok']="Souscription rejeter avec succès";



}else if(isset($_POST['etape'])&&($_POST['etape']==4))

{

    //activation



    //recuperation des variables



    $status=1;

    $studentid=htmlspecialchars(addslashes($_POST['studentid']));

    $parentid=htmlspecialchars(addslashes($_POST['parentid']));

    $session=htmlspecialchars(addslashes($_POST['session']));



    $etabs->UpdateStateNotification($status,$parentid,$studentid,$session);



    $_SESSION['user']['Updateadminok']="Notification activer avec succès";





}else if(isset($_POST['etape'])&&($_POST['etape']==5))

{

//desactivation



//recuperation des variables





    $status=0;

    $studentid=htmlspecialchars(addslashes($_POST['studentid']));

    $parentid=htmlspecialchars(addslashes($_POST['parentid']));

    $session=htmlspecialchars(addslashes($_POST['session']));



    $etabs->UpdateStateNotification($status,$parentid,$studentid,$session);



    $_SESSION['user']['Updateadminok']="Notification desactiver avec succès";

}





 ?>

