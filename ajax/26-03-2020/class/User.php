<?php
class User{

public $db;
function __construct() {
  require_once('../class/cnx.php');
  require_once('../class/Etablissement.php');

  /*require_once('../class_html2PDF/html2pdf.class.php');
  require_once('../class_html2PDF/_tcpdf_5.0.002/config/lang/eng.php');
  require_once('../class_html2PDF/_tcpdf_5.0.002/tcpdf.php');
  require_once('../PHPMailer/class.phpmailer.php');
  require_once('../PHPMailer/class.smtp.php');*/
  $db = new mysqlConnector();
  $this->db= $db->dataBase;
    }

    function AddEtabMixte($codeetab,$libetab,$etabphone1,$etabphone2,$emailetab,$addresetab,$fichieretab,$datecrea,$emailuser,$pays,$typeetab,$precisions,$cantine,$transportcar)
    {
      $etabs=new Etab();

      $check=$etabs->existEtabCountry($codeetab,$pays);

      if($check==0)
      {
        //cet etablissment n'existe pas encore dans la base il faut l'ajouter

      $req = $this->db->prepare("INSERT INTO etablissement SET code_etab=?,libelle_etab=?,tel_etab=?,tel1_etab=?,email_etab=?,adresse_etab=?,logo_etab=?,datecrea_etab=?,createby_etab=?,pays_etab=?,type_etab=?,mixte_etab=?,cantine_etab=?,transport_etab=?");
      $req->execute([
      $codeetab,
      $libetab,
      $etabphone1,
      $etabphone2,
      $emailetab,
      $addresetab,
      $fichieretab,
      $datecrea,
      $emailuser,
      $pays,
      $typeetab,
      $precisions,
      $cantine,
      $transportcar
    ]);

      $_SESSION['user']['addetabok']="Nouvel Etablissement ajouté avec succès";

      if($_SESSION['user']['profile'] == "Admin_globale") {

          header("Location:../manager/addschool.php");
        }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/addschool.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

                header("Location:../teatcher/index.php");

            }else if($_SESSION['user']['profile'] == "Student") {

                  header("Location:../student/index.php");

              }else if($_SESSION['user']['profile'] == "Parent") {

                    header("Location:../parent/index.php");

                }

       //header("Location:../manager/addschool.php");
      }else {
      $_SESSION['user']['addetabexist']="Cet Etablissement existe dejà dans le système";
      if($_SESSION['user']['profile'] == "Admin_globale") {

        header("Location:../manager/index.php");
        }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/index.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

                header("Location:../teatcher/index.php");

            }else if($_SESSION['user']['profile'] == "Student") {

                  header("Location:../student/index.php");

              }else if($_SESSION['user']['profile'] == "Parent") {

                    header("Location:../parent/index.php");

                }
      //header("Location:../manager/index.php");
      }




    }

    function CheckingLoginCni($login,$cni)
    {
      $req = $this->db->prepare("SELECT * FROM compte,parent where compte.id_compte=parent.idcompte_parent and ( compte.login_compte=? or parent.cni_parent=?)");
      $req->execute([$login,$cni]);
      $data=$req->fetchAll();
      $nbStudent=count($data);

      return $nbStudent;
    }

    function CheckingLogin($login)
    {
        $req = $this->db->prepare("SELECT * FROM compte where  compte.login_compte=?");
        $req->execute([$login]);
        $data=$req->fetchAll();
        $nbStudent=count($data);

        return $nbStudent;
    }

function UpdateEtabwithFile($codeetab,$libetab,$etabphone1,$etabphone2,$emailetab,$addresetab,$fichieretab)
{
  $req = $this->db->prepare("UPDATE  etablissement SET libelle_etab=?,tel_etab=?,tel1_etab=?,email_etab=?,adresse_etab=?,logo_etab=? where code_etab=?");
  $req->execute([$libetab,$etabphone1,$etabphone2,$emailetab,$addresetab,$fichieretab,$codeetab]);

  $_SESSION['user']['updateetabok']="Etablissement modifier avec succès";

   header("Location:../manager/schools.php");

}

function UpdateEtabwithoutFile($codeetab,$libetab,$etabphone1,$etabphone2,$emailetab,$addresetab)
{
  $req = $this->db->prepare("UPDATE  etablissement SET libelle_etab=?,tel_etab=?,tel1_etab=?,email_etab=?,adresse_etab=? where code_etab=?");
  $req->execute([$libetab,$etabphone1,$etabphone2,$emailetab,$addresetab,$codeetab]);

  $_SESSION['user']['updateetabok']="Etablissement modifier avec succès";

   header("Location:../manager/schools.php");
}

function getNbofStudentforThisschool($localid)
{
  $req = $this->db->prepare("SELECT * FROM eleve,compte where eleve.email_eleve=compte.email_compte and eleve.codeEtab_eleve=?");
  $req->execute([$localid]);
  $data=$req->fetchAll();
  $nbStudent=count($data);

  return $nbStudent;
}

function getNbofStudentforThisschoolSession($codeEtab,$session)
{
  $req = $this->db->prepare("SELECT * FROM eleve,compte,inscription WHERE eleve.idcompte_eleve=compte.id_compte AND inscription.ideleve_inscrip=eleve.idcompte_eleve AND inscription.codeEtab_inscrip=? AND inscription.session_inscrip=?");
  $req->execute([$codeEtab,$session]);
  $data=$req->fetchAll();
  $nbStudent=count($data);

  return $nbStudent;
}

function getNumberStudentAttendanceToday($localid)
{
  $dateday=date("Y-m-d");
  $statut=1;
  $req = $this->db->prepare("SELECT * FROM eleve,compte,presences where compte.email_compte=eleve.email_eleve and eleve.matricule_eleve=presences.matricule_presence and eleve.codeEtab_eleve=? and presences.date_presence=? and presences.statut_presence=?");
  $req->execute([$localid,$dateday,$statut]);
  $data=$req->fetchAll();
  $nbAttendanceday=count($data);

  return $nbAttendanceday;
}

function getNumberofParentforThisschool($localid)
{
    $typecompte="Parent";
    $req = $this->db->prepare("SELECT * FROM eleve,compte,parent,parenter where parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and compte.id_compte=parent.idcompte_parent and compte.type_compte=? and eleve.codeEtab_eleve=?");
    $req->execute([$typecompte,$localid]);
    $data=$req->fetchAll();
    $nbParent=count($data);

    return $nbParent;
}

function getNumberofTeatcherforThisschool($localid)
{
  $req = $this->db->prepare("SELECT * FROM enseignant,compte,enseigner where compte.id_compte=enseigner.id_enseignant and enseignant.idcompte_enseignant=compte.id_compte and enseigner.codeEtab=?");
  $req->execute([$localid]);
  $data=$req->fetchAll();
  $nbTeatcher=count($data);

  return $nbTeatcher;
}

function getStatisById($localid)
{
  //nous allons recuperer le nombre d'etablissment
  $req = $this->db->prepare("SELECT * FROM etablissement where code_etab=?");
  $req->execute([$localid]);
  $data=$req->fetchAll();
  $nbEtab=count($data);

  //nous allons recuperer le nombre admin local

  $typeadl="Admin_locale";
  $req1 = $this->db->prepare("SELECT * FROM compte,assigner where compte.id_compte=assigner.id_adLocal and assigner.codeEtab_assign=? and  type_compte=?");
  $req1->execute([$localid,$typeadl]);
  $data1=$req1->fetchAll();
  $nbAdl=count($data1);

  //nous allons determiner le nombre d'enseignant

  $typeTea="Teatcher";
  $req2 = $this->db->prepare("SELECT * FROM compte ,enseigner where compte.id_compte=enseigner.id_enseignant and enseigner.codeEtab=? and  type_compte=?");
  $req2->execute([$localid,$typeTea]);
  $data2=$req2->fetchAll();
  $nbTea=count($data2);

  $typePar="Parent";
  $req3 = $this->db->prepare("SELECT * FROM compte,parent,eleve,parenter where compte.id_compte=parent.idcompte_parent and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and eleve.codeEtab_eleve=? and  type_compte=?");
  $req3->execute([$localid,$typePar]);
  $data3=$req3->fetchAll();
  $nbPar=count($data3);

  $donnees=$nbEtab."*".$nbAdl."*".$nbTea."*".$nbPar;

  return $donnees;

}

    function getStatis()
    {
      //nous allons recuperer le nombre d'etablissment
      $req = $this->db->prepare("SELECT * FROM etablissement");
      $req->execute();
      $data=$req->fetchAll();
      $nbEtab=count($data);

      //nous allons recuperer le nombre admin local

      $typeadl="Admin_local";
      $req1 = $this->db->prepare("SELECT * FROM compte where type_compte=?");
      $req1->execute([$typeadl]);
      $data1=$req1->fetchAll();
      $nbAdl=count($data1);

      //nous allons determiner le nombre d'enseignant

      $typeTea="Teacher";
      $req2 = $this->db->prepare("SELECT * FROM compte where type_compte=?");
      $req2->execute([$typeTea]);
      $data2=$req2->fetchAll();
      $nbTea=count($data2);

      $typePar="Parent";
      $req3 = $this->db->prepare("SELECT * FROM compte where type_compte=?");
      $req3->execute([$typePar]);
      $data3=$req3->fetchAll();
      $nbPar=count($data3);

      $donnees=$nbEtab."*".$nbAdl."*".$nbTea."*".$nbPar;

      return $donnees;

    }

    function getLoginProfilebyId($compteid)
  {
      $req = $this->db->prepare("SELECT * FROM compte where  id_compte=?");
      $req->execute([$compteid]);
      $data=$req->fetchAll();
      $array=json_encode($data,true);
      $someArray = json_decode($array, true);
      $donnees=$someArray[0]["login_compte"]."*".$someArray[0]["type_compte"];

      return $donnees;

  }

  function getLoginProfile($email)
{
    $req = $this->db->prepare("SELECT * FROM compte where  email_compte=?");
    $req->execute([$email]);
    $data=$req->fetchAll();
    $array=json_encode($data,true);
    $someArray = json_decode($array, true);
    $donnees=$someArray[0]["login_compte"]."*".$someArray[0]["type_compte"];

    return $donnees;

}

function getImageProfile($email)
{
  $req = $this->db->prepare("SELECT * FROM compte where  email_compte=?");
  $req->execute([$email]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);
  $donnees=$someArray[0]["photo_compte"];

  return $donnees;

}

function getImageProfilebyId($compteid)
{
  $req = $this->db->prepare("SELECT * FROM compte where  id_compte=?");
  $req->execute([$compteid]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);
  $donnees=$someArray[0]["photo_compte"];

  return $donnees;

}


    function CheckCompte($login)
    {
      $req = $this->db->prepare("SELECT * FROM compte where  login_compte=?");
      $req->execute([$login]);
      $data=$req->fetchAll();
      $nb=count($data);
      return $nb;
    }

    function CheckComptestatut($login)
    {
      $statut=1;
      $req = $this->db->prepare("SELECT * FROM compte where  login_compte=? and statut_compte=?");
      $req->execute([$login,$statut]);
      $data=$req->fetchAll();
      $nb=count($data);
      return $nb;
    }

    function CheckComptepasswd($login,$pass)
    {

      $req = $this->db->prepare("SELECT * FROM compte where  login_compte=? and pass_compte=?");
      $req->execute([$login,$pass]);
      $data=$req->fetchAll();
      $nb=count($data);
      return $nb;
    }

    function LogMephone($login,$pass)
    {
      $req = $this->db->prepare("SELECT * FROM compte WHERE login_compte = ? AND pass_compte = ?");
      $req->execute([$login,$pass]);
      $data= $req->fetchAll();

      if(count($data)==1){

        return json_encode($data, JSON_FORCE_OBJECT);

      }else {
         return json_encode("Echec", JSON_FORCE_OBJECT);

      }



    }

    function LogMe($login,$pass)
    {

      $req = $this->db->prepare("SELECT * FROM compte WHERE login_compte = ? AND pass_compte = ?");
      $req->execute([$login,$pass]);
      $data= $req->fetchAll();

      if(count($data)==1){

        $_SESSION['user']=array(
    "login"=>$data['0']->login_compte,
    "email"=>$data['0']->email_compte,
    "password"=>$data['0']->pass_compte,
    "nom"=>$data['0']->nom_compte,
    "prenoms"=>$data['0']->prenom_compte,
    "profile"=>$data['0']->type_compte,
    "telephone"=>$data['0']->tel_compte,
    "IdCompte"=>$data['0']->id_compte

            );
      }

      //nous allons chercher le pays dans lequel l'etablissement se trouve dans quel pays
      $etabs=new Etab();
      $codeEtab=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      $paysid=$etabs->getEtabpaysid($codeEtab);
      $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
      $typeetab=$etabs->DetermineTypeEtab($codeEtab);
      $_SESSION['user']['paysid']=$paysid;
      // echo $paysid;

      //nous allons mettre a jour la date de dernière connexion

      if($typeetab==5)
      {
        if($paysid==4)
        {
          if($_SESSION['user']['profile'] == "Admin_globale") {

                header("Location:../manager/index.php");

            }else if($_SESSION['user']['profile'] == "Admin_locale") {



                  header("Location:../localecmr/index.php");

              }else if($_SESSION['user']['profile'] == "Teatcher") {

                    header("Location:../teatcher/index.php");

                }else if($_SESSION['user']['profile'] == "Student") {

                      header("Location:../student/index.php");

                  }else if($_SESSION['user']['profile'] == "Parent") {

                        header("Location:../parent/index.php");

                    }
        }else {
          if($_SESSION['user']['profile'] == "Admin_globale") {

                header("Location:../manager/index.php");

            }else if($_SESSION['user']['profile'] == "Admin_locale") {



                  header("Location:../locale".$libelleEtab."/index.php");

              }else if($_SESSION['user']['profile'] == "Teatcher") {

                    header("Location:../teatcher/index.php");

                }else if($_SESSION['user']['profile'] == "Student") {

                      header("Location:../student/index.php");

                  }else if($_SESSION['user']['profile'] == "Parent") {

                        header("Location:../parent/index.php");

                    }
        }
      }else {

        if($paysid==4)
        {
          if($_SESSION['user']['profile'] == "Admin_globale") {

                header("Location:../manager/index.php");

            }else if($_SESSION['user']['profile'] == "Admin_locale") {



                  header("Location:../localecmr/index.php");

              }else if($_SESSION['user']['profile'] == "Teatcher") {

                    header("Location:../teatcher/index.php");

                }else if($_SESSION['user']['profile'] == "Student") {

                      header("Location:../student/index.php");

                  }else if($_SESSION['user']['profile'] == "Parent") {

                        header("Location:../parent/index.php");

                    }
        }else {
          if($_SESSION['user']['profile'] == "Admin_globale") {

                header("Location:../manager/index.php");

            }else if($_SESSION['user']['profile'] == "Admin_locale") {



                  header("Location:../locale/index.php");

              }else if($_SESSION['user']['profile'] == "Teatcher") {

                    header("Location:../teatcher/index.php");

                }else if($_SESSION['user']['profile'] == "Student") {

                      header("Location:../student/index.php");

                  }else if($_SESSION['user']['profile'] == "Parent") {

                        header("Location:../parent/index.php");

                    }
        }

      }





    }

    function AddEtab($codeetab,$libetab,$etabphone1,$etabphone2,$emailetab,$addresetab,$fichieretab,$datecrea,$emailuser,$pays,$typeetab)
    {
      $etabs=new Etab();

      $check=$etabs->existEtabCountry($codeetab,$pays);

      if($check==0)
      {
        //cet etablissment n'existe pas encore dans la base il faut l'ajouter

      $req = $this->db->prepare("INSERT INTO etablissement SET code_etab=?,libelle_etab=?,tel_etab=?,tel1_etab=?,email_etab=?,adresse_etab=?,logo_etab=?,datecrea_etab=?,createby_etab=?,pays_etab=?,type_etab=?");
      $req->execute([
      $codeetab,
      $libetab,
      $etabphone1,
      $etabphone2,
      $emailetab,
      $addresetab,
      $fichieretab,
      $datecrea,
      $emailuser,
      $pays,
      $typeetab
    ]);

      $_SESSION['user']['addetabok']="Etablissement ajouté avec succès";

      if($_SESSION['user']['profile'] == "Admin_globale") {

          header("Location:../manager/addschool.php");
        }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/addschool.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

                header("Location:../teatcher/index.php");

            }else if($_SESSION['user']['profile'] == "Student") {

                  header("Location:../student/index.php");

              }else if($_SESSION['user']['profile'] == "Parent") {

                    header("Location:../parent/index.php");

                }

       //header("Location:../manager/addschool.php");
      }else {
      $_SESSION['user']['addetabexist']="Cet Etablissement existe dejà dans le système";
      if($_SESSION['user']['profile'] == "Admin_globale") {

        header("Location:../manager/index.php");
        }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/index.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

                header("Location:../teatcher/index.php");

            }else if($_SESSION['user']['profile'] == "Student") {

                  header("Location:../student/index.php");

              }else if($_SESSION['user']['profile'] == "Parent") {

                    header("Location:../parent/index.php");

                }
      //header("Location:../manager/index.php");
      }




    }




}


 ?>
