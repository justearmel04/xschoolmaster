<?php
session_start();
require_once('../class/Salle.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');
require_once('../controller/functions.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();
$matierex= new Matiere();
$etab=new Etab();
$classex= new Classe();
$sallex= new Salle();



if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables
//data: 'etape=' + etape+ '&heurelibid=' +heurelibid+ '&dayshort='+dayshort+ '&horaires=' +horaires+ '&codeEtab='+codeEtab+ '&sessionEtab=' +sessionEtab,
$heurelibid=htmlspecialchars($_POST['heurelibid']);
$dayshort=htmlspecialchars($_POST['dayshort']);
$horaires=htmlspecialchars($_POST['horaires']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);
$classeid=htmlspecialchars($_POST['classeid']);

$tabhoraires=explode("-",$horaires);
$datedeb=trim($tabhoraires[0]).":00";
$datefin=trim($tabhoraires[1]).":00";

$datedeb=trim(str_replace("H", ":",$datedeb));
$datefin=trim(str_replace("H", ":",$datefin));

$content=0;

// echo $datedeb ." ".$datefin;

// echo $heurelibid." ".$dayshort." ".$datedeb." ".$datefin." ".$codeEtab." ".$sessionEtab." ".$classeid;

$datas=$etab->getNbroutinesthisdayAthours($heurelibid,$dayshort,$datedeb,$datefin,$codeEtab,$sessionEtab,$classeid);

$nb=count($datas);

// echo $nb;

if($nb>0)
{
  //est ce un libelle ou un cours

  $content=1;

}

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  $heurelibid=htmlspecialchars($_POST['heurelibid']);
  $dayshort=htmlspecialchars($_POST['dayshort']);
  $horaires=htmlspecialchars($_POST['horaires']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $classeid=htmlspecialchars($_POST['classe']);
  $matiereid=htmlspecialchars($_POST['matiere']);

  $libellematiere=$matierex->getMatiereLibelleByIdMat($matiereid,$codeEtab);

  $tabhoraires=explode("-",$horaires);
  $datedeb=trim($tabhoraires[0]).":00";
  $datefin=trim($tabhoraires[1]).":00";

  $datedeb=trim(str_replace("H", ":",$datedeb));
  $datefin=trim(str_replace("H", ":",$datefin));

  $etab->AddroutineModelNew($classeid,$codeEtab,$datedeb,$datefin,$matiereid,$dayshort,$sessionEtab,$heurelibid,$libellematiere);



}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{

  $heurelibid=htmlspecialchars($_POST['heurelibid']);
  $dayshort=htmlspecialchars($_POST['dayshort']);
  $horaires=htmlspecialchars($_POST['horaires']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $classeid=htmlspecialchars($_POST['classe']);
  $matiereid=0;

  $tabhoraires=explode("-",$horaires);
  $datedeb=trim($tabhoraires[0]).":00";
  $datefin=trim($tabhoraires[1]).":00";

  $datedeb=trim(str_replace("H", ":",$datedeb));
  $datefin=trim(str_replace("H", ":",$datefin));

  $libellematiere=htmlspecialchars($_POST['libelle']);

  $etab->AddroutineModelNew($classeid,$codeEtab,$datedeb,$datefin,$matiereid,$dayshort,$sessionEtab,$heurelibid,$libellematiere);

  $_SESSION['user']['addprogra']=L::RoutinesUpdateSuccessfully;

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  $heurelibid=htmlspecialchars($_POST['heurelibid']);
  $dayshort=htmlspecialchars($_POST['dayshort']);
  $horaires=htmlspecialchars($_POST['horaires']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $classeid=htmlspecialchars($_POST['classe']);
  $routeid=htmlspecialchars($_POST['routeid']);

  $tabhoraires=explode("-",$horaires);
  $datedeb=trim($tabhoraires[0]).":00";
  $datefin=trim($tabhoraires[1]).":00";

  $datedeb=trim(str_replace("H", ":",$datedeb));
  $datefin=trim(str_replace("H", ":",$datefin));

  echo $datedeb." ".$datefin;

  $etab->DeletedRoutinesModel($routeid,$classeid,$heurelibid,$dayshort,$datedeb,$datefin,$codeEtab,$sessionEtab);

  $_SESSION['user']['addprogra']=L::RoutinesUpdateSuccessfully;

}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  $heurelibid=htmlspecialchars($_POST['heurelibid']);
  $dayshort=htmlspecialchars($_POST['dayshort']);
  $horaires=htmlspecialchars($_POST['horaires']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $classeid=htmlspecialchars($_POST['classe']);
  $routeid=htmlspecialchars($_POST['routeid']);

  $content="";

  $data=$etab->getdataRoutinesDetailsInfos($routeid,$heurelibid,$dayshort,$classeid,$codeEtab,$sessionEtab);

  foreach ($data as $value):

    $matiereselected=$value->matiere_route;

    $datas=$etab->getAllsubjectofclassesbyIdclassesAndTeatcherId($classeid,$codeEtab,$sessionEtab);

    $nbligne=count($datas);

    if($nbligne==0)
    {
      $content.="<option selected value=''>".L::NoMatiere." </option>";

    }else {
      //var_dump($datas);
      $content.="<option selected value='' >".L::SelectSubjects."</option>";
}

      foreach ($datas as $values):
        if($matiereselected==$values->id_mat)
        {
            $content .= "<option value='". $values->id_mat."' selected  >" . utf8_encode(utf8_decode($values->libelle_mat)). "</option>";
        }else {
          $content .= "<option value='". $values->id_mat."'  >" . utf8_encode(utf8_decode($values->libelle_mat)). "</option>";
        }

      endforeach;

  endforeach;

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  $heurelibid=htmlspecialchars($_POST['heurelibid']);
  $dayshort=htmlspecialchars($_POST['dayshort']);
  $horaires=htmlspecialchars($_POST['horaires']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $classeid=htmlspecialchars($_POST['classe']);
  $routeid=htmlspecialchars($_POST['routeid']);

  $content="";

  $data=$etab->getdataRoutinesDetailsInfos($routeid,$heurelibid,$dayshort,$classeid,$codeEtab,$sessionEtab);

  foreach ($data as $value):

    $content=$value->libelle_routine;
  endforeach;

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{

//recupération des variables
//data: 'etape=' + etape+ '&heurelibid=' +heurelibid+ '&dayshort='+dayshort+ '&horaires=' +horaires+ '&codeEtab='+codeEtab+ '&sessionEtab=' +sessionEtab,
$heurelibid=htmlspecialchars($_POST['heurelibid']);
$dayshort=htmlspecialchars($_POST['dayshort']);
$horaires=htmlspecialchars($_POST['horaires']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);
$classeid=htmlspecialchars($_POST['classeid']);

$tabhoraires=explode("-",$horaires);
$datedeb=trim($tabhoraires[0]).":00";
$datefin=trim($tabhoraires[1]).":00";

$datedeb=trim(str_replace("H", ":",$datedeb));
$datefin=trim(str_replace("H", ":",$datefin));

$content=0;
$content1="";
$content2="";
$content3="";

// echo $datedeb ." ".$datefin;

// echo $heurelibid." ".$dayshort." ".$datedeb." ".$datefin." ".$codeEtab." ".$sessionEtab." ".$classeid;

$datas=$etab->getNbroutinesthisdayAthours($heurelibid,$dayshort,$datedeb,$datefin,$codeEtab,$sessionEtab,$classeid);

$nb=count($datas);

// echo $nb;

if($nb>0)
{
  //est ce un libelle ou un cours

  $content=1;

}

//nous allons chercher la liste des salles disponible a cette heure

$datasx=$etab->getsallesoccpyAtthisTimes($heurelibid,$dayshort,$codeEtab,$sessionEtab);
$concatids="";
foreach ($datasx as $valuex):
  $concatids=$concatids.$valuex->salle_route."*";
endforeach;

$concatids=substr($concatids, 0, -1);

$tabconcat=explode("*",$concatids);

//la liste de toutes les salles

$dataz=$sallex->getAllSallesbyschoolCode($codeEtab);
$nbligne=count($dataz);
if($nbligne>0)
{
  foreach ($dataz as $valuez):
    // if($valuez->id_salle)
    if (in_array($valuez->id_salle,$tabconcat))
    {

    }else {
        $content2 .= "<option value='". $valuez->id_salle ."' >" . utf8_encode(utf8_decode($valuez->libelle_salle)). "</option>";
    }
  endforeach;
}else {
  // $content2.="<option value=''>".L::NoSalle." </option>";
}

if(strlen($content2)>0)
{
$content3.="<option value='' selected>".L::SelectSalle."</option>";
$content3.=$content2;
}else {
  $content3.="<option value=''>".L::NoSalle." </option>";
}



echo $content."*".$content3;

}else if(isset($_POST['etape'])&&($_POST['etape']==8))
{
  $heurelibid=htmlspecialchars($_POST['heurelibid']);
  $dayshort=htmlspecialchars($_POST['dayshort']);
  $horaires=htmlspecialchars($_POST['horaires']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $classeid=htmlspecialchars($_POST['classe']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $salle=htmlspecialchars($_POST['salle']);

  $tabhoraires=explode("-",$horaires);
  $datedeb=trim($tabhoraires[0]).":00";
  $datefin=trim($tabhoraires[1]).":00";

  $datedeb=trim(str_replace("H", ":",$datedeb));
  $datefin=trim(str_replace("H", ":",$datefin));

  $libellematiere=$matierex->getMatiereLibelleByIdMat($matiereid,$codeEtab);

  $etab->AddroutineModelNewSalle($classeid,$codeEtab,$datedeb,$datefin,$matiereid,$dayshort,$sessionEtab,$heurelibid,$libellematiere,$salle);

  $_SESSION['user']['addprogra']=L::RoutinesUpdateSuccessfully;

}


 ?>
