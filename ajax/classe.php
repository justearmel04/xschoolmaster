<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Etablissement.php');
require_once('../class/Student.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$student= new Student();
$session= new Sessionacade();
$etabs= new Etab();
$classex= new Classe();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$libetab=htmlspecialchars($_POST['libetab']);
$classe=htmlspecialchars($_POST['classe']);
// $section=htmlspecialchars(addslashes($_POST['section']));
$session=htmlspecialchars($_POST['session']);
// $check=$classex->ExisteClasses($libetab,$classe,$session,$section);

$check=$classex->ExisteClasses($libetab,$classe,$session);

  if($check==0)
  {
$content=0;
  }else {
    $content=1;
  }

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  $content="";
  $code=htmlspecialchars($_POST['code']);

  $datas=$classex->getAllClassesbyschoolCode($code);

  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>".L::NoClasse." </option>";
  }else {
    //var_dump($datas);
    $content.="<option value='' selected>".L::Selectclasses."</option>";

    foreach ($datas as $value):
        $content .= "<option value='". $value->id_classe ."' >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";
    endforeach;
  }

  echo $content;


}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recuperation des variables

  $content="";

  $classeId=htmlspecialchars($_POST['classe']);

  $datas=$classex->getAllStudentOfThisClasses($classeId);

  foreach ($datas as $value):
      $content .= "<option  selected value='". $value->id_compte ."' >" . utf8_encode(utf8_decode($value->nom_eleve." ".$value->prenom_eleve)). "</option>";
  endforeach;

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //recuperation des variables
$classeId=htmlspecialchars($_POST['classe']);

$content="";

$content=$classex->getCodeEtabOfClassesByClasseId($classeId);

echo trim($content);

}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{

  $code=htmlspecialchars($_POST['code']);
  $nbsessionOn=$session->getNumberSessionEncoursOn($code);

  if($nbsessionOn>0)
  {
    //recuperer la session en cours
    $sessionencours=$session->getSessionEncours($code);
    $tabsessionencours=explode("*",$sessionencours);
    $libellesessionencours=$tabsessionencours[0];
    $sessionencoursid=$tabsessionencours[1];
    $typesessionencours=$tabsessionencours[2];
  }
  $parentid=htmlspecialchars(addslashes($_POST['parentid']));
   $content="";
   $content.="<option value='' selected>".L::Selectclasses."</option>";

  $classeSchool=$classex->getAllClassesOfParentHadStudent($code,$parentid,$libellesessionencours);

  foreach ($classeSchool as $value):
      $content .= "<option value='". $value->id_classe ."' >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";
  endforeach;

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  //rechercher la liste des eleves du parent et etant inscrit dans cette classe
  //recuperation des variables

  $code=htmlspecialchars($_POST['code']);
  $nbsessionOn=$session->getNumberSessionEncoursOn($code);

  if($nbsessionOn>0)
  {
    //recuperer la session en cours
    $sessionencours=$session->getSessionEncours($code);
    $tabsessionencours=explode("*",$sessionencours);
    $libellesessionencours=$tabsessionencours[0];
    $sessionencoursid=$tabsessionencours[1];
    $typesessionencours=$tabsessionencours[2];
  }
  $parentid=htmlspecialchars($_POST['parentid']);
  $classeEtab=htmlspecialchars($_POST['classeEtab']);
  $content="";

  $students=$classex->getAllStudentOfParentSchool($code,$classeEtab,$parentid,$libellesessionencours);

$content.="<option value=''>".L::SelectAnEleve."</option>";

  foreach ($students as $value):
      $content .= "<option value='". $value->idcompte_eleve ."' >" . utf8_encode(utf8_decode($value->nom_eleve.' '.$value->prenom_eleve)). "</option>";
  endforeach;

  echo $content;



}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{
  //recuperation des variables

  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $section=htmlspecialchars($_POST['section']);
  $session=htmlspecialchars($_POST['session']);

  $content="";

  $dataclasse=$classex->getAllsectionClasses($codeEtab,$section,$session);

  $nb=count($dataclasse);

  if($nb==0)
  {
    $content.="<option value='' selected>".L::NoClasse."</option>";
  }else if($nb>0) {
    $content.="<option value='' selected>".L::Selectclasses."</option>";

    foreach ($dataclasse as $value):
      $content .= "<option value='". $value->id_classe ."' >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";
    endforeach;

  }

echo $content;



}else if(isset($_POST['etape'])&&($_POST['etape']==8))
{
  //recuperation des variables

  $classeSchool=htmlspecialchars($_POST['classe']);
  $session=htmlspecialchars($_POST['session']);
  $codeetab=htmlspecialchars($_POST['codeetab']);
  $a=1;
  $b=2;
  $classeSchool=substr($classeSchool, 0, -1);
  $classeSchool=str_replace("-",",",$classeSchool);
  $tek=$a.",".$b;

  $classeSchools=trim($classeSchool);
  $content="";

  $classeSchoolsdatas=$etabs->getAllSchoolmessageselected($classeSchools,$session,$codeetab);

  $tabclasses=explode(",",$classeSchools);
  $nbtable=count($tabclasses);

    for($i=0;$i<$nbtable;$i++)
    {


      $classeSchoolsdatas=$etabs->getAllSchoolmessageselected($tabclasses[$i],$session,$codeetab);

      foreach ($classeSchoolsdatas as $value):
        $content .= "<option value='". $value->id_classe ."' >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";

      endforeach;

      // foreach ($classeSchoolsdatas as $value):
      //   $content=$content."<option value=".utf8_encode(utf8_decode($value->id_classe)).">".utf8_encode(utf8_decode($value->libelle_classe))."</option>"
      // endforeach

    }
    $content1="<option value='allclasses'>".L::AllClassesMenu."</option>";

    echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==9))
{
  //recuperation des variables

  $classe=htmlspecialchars($_POST['classe']);
  $session=htmlspecialchars($_POST['session']);
  $codeetab=htmlspecialchars($_POST['codeetab']);
  $precis=htmlspecialchars($_POST['precis']);
  $parascolaire=htmlspecialchars($_POST['parascolaire']);
  $message=htmlspecialchars($_POST['message']);



  $content="";

  //determiner la liste des eleves dans les classes Selectionner


  //nous allons voir si la selection est précise ou collective


  if($precis==0)
  {
    $datas=$student->getAllstudentofMulticlassesSession($classe,$session);

    $nbligne=count($datas);

    if($nbligne==0)
    {
      $content.="<option value=''>".L::Nostudent."</option>";
    }else {
      //var_dump($datas);
      $content1="<option value='allstudent'>".L::AllEleves."</option>";

      foreach ($datas as $value):
          $content .= "<option value='".$value->id_compte."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
      endforeach;
    }
  }else if($precis==1)
  {
    //nous allons recuperer les identifiants de(s) eleves

    $datasids=$student->getallidsofstudentprecis($message,$codeetab,$session);
    $datasids=substr($datasids, 0, -1);

    $tabids=explode("-",$datasids);
    $nbids=count($tabids);

    for($i=0;$i<$nbids;$i++)
    {
      $studentid=$tabids[$i];
      $donnees=$student->getEmailParentOfThisStudentByID($studentid,$session);
      $tabdonnees=explode("*",$donnees);

        $content .= "<option value='".$tabdonnees[6]."' >" . utf8_encode(utf8_decode($tabdonnees[1]." - ".$tabdonnees[2])). "</option>";

    }

  }




echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==10))
{
  //recuperation de la variable

  $section=htmlspecialchars($_POST['section']);
  $session=htmlspecialchars($_POST['session']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);

  $content="";

  $datas=$classex->getAllclassesofsection($section,$session,$codeEtab);

  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>".L::NoClasse." </option>";
  }else {
    $content="<option value=''>".L::Selectclasses."</option>";

    foreach ($datas as $value):
        $content .= "<option value='".$value->id_classe."' >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";
    endforeach;
  }

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==11))
{
  //recuperation des variables

$session=htmlspecialchars($_POST['session']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$classe=htmlspecialchars($_POST['classe']);
$content="";

$datas=$classex->determineclassebyId($classe,$codeEtab,$session);

$nbligne=count($datas);

if($nbligne==0)
{
  $content.="<option value=''>".L::NoClasse." </option>";
}else {
  $content="<option value=''>".L::Selectclasses."</option>";

  foreach ($datas as $value):
      $content .= "<option value='".$value->id_classe."' selected >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";
  endforeach;
}

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==12))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);

  $content="";

  $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtab);
  $sessionencours=$session->getSessionEncours($codeEtab);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];

  $datas=$classex->getAllClassesbyschoolCode($codeEtab);

  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>".L::NoClasse." </option>";
  }else {
    //var_dump($datas);


    foreach ($datas as $value):
        $content .= "<option value='". $value->id_classe ."' >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";
    endforeach;
  }

  echo $libellesessionencours."*".$content;

}else if(isset($_POST['etape'])&&($_POST['etape']==13))
{
  //recuperation des variables

  $classe=htmlspecialchars($_POST['classe']);
  $codeEtab=htmlspecialchars($_POST['libetab']);
  $montantscolar=htmlspecialchars($_POST['montantscolar']);
  $session=htmlspecialchars($_POST['session']);

  $check=$classex->ExisteClassescmr($codeEtab,$classe,$session,$montantscolar);

  if($check==0)
  {
$content=0;
  }else {
    $content=1;
  }

echo $content;


}else  if(isset($_POST['etape'])&&($_POST['etape']==14))
{
  $pays=htmlspecialchars($_POST['pays']);
  $concatEtab=htmlspecialchars($_POST['codeEtab']);
  $tabconcat=explode("*",$concatEtab);
  $codeEtab=$tabconcat[0];
  $sessionEtab=$tabconcat[1];
  $content="";

  $datas=$classex->getAllclasseEtabBysession($codeEtab,$sessionEtab);
  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>".L::NoClasse."</option>";
  }else if($nbligne>0)
  {
    $content.="<option value=''>".L::Selectclasses."</option>";
    foreach ($datas as $value):
        $content .= "<option value='". $value->id_classe ."' >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";
    endforeach;
  }

echo $content;

}else  if(isset($_POST['etape'])&&($_POST['etape']==15))
{
  $libelleclasse=htmlspecialchars($_POST['libelleclasse']);
  $idclasse=htmlspecialchars($_POST['idclasse']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);

  $classex->UpdateLibelleclasse($libelleclasse,$idclasse,$codeEtab,$sessionEtab);

  $_SESSION['user']['updateclasseok']=L::ClasseModificationsuccessfully;


}else  if(isset($_POST['etape'])&&($_POST['etape']==16))
{

  $idclasse=htmlspecialchars($_POST['classeid']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $content="";
  $content1="";
  $type="";
  $niveau="";
  $cycle="";

  $datas=$classex->GetClasseInfos($idclasse,$codeEtab,$sessionEtab);

  foreach ($datas as  $value):
    $type=$value->type_classe;
    $niveau=$value->niveau_classe;
    $cycle=$value->cycle_classe;
    $content=$content.$value->inscriptionmont_classe."*".$value->scolarite_classe."*".$value->scolariteaff_classe;
  endforeach;

$content1.="<option value=''>".L::selectLevel."</option>";
if($type=="6e"&&$niveau=="SIXIEME")
{
  $content1.="<option value='1-SIXIEME-6e' selected>".L::Sixieme ."</option>";
}else {
  $content1.="<option value='1-SIXIEME-6e'>".L::Sixieme ."</option>";
}

if($type=="5e"&&$niveau=="CINQUIEME")
{
$content1.="<option value='1-CINQUIEME-5e' selected>".L::Cinqieme ."</option>";
}else {
  $content1.="<option value='1-CINQUIEME-5e'>".L::Cinqieme ."</option>";
}

if($type=="4e"&&$niveau=="QUATRIEME")
{
$content1.="<option value='1-QUATRIEME-4e' selected>". L::Quatrieme ."</option>";
}else {
$content1.="<option value='1-QUATRIEME-4e'>". L::Quatrieme ."</option>";
}

if($type=="3e"&&$niveau=="TROISIEME")
{
$content1.="<option value='1-TROISIEME-3e' selected>". L::Troisieme ."</option>";
}else {
$content1.="<option value='1-TROISIEME-3e'>". L::Troisieme ."</option>";
}


if($type=="2A"&&$niveau=="SECONDE")
{
$content1.="<option value='2-SECONDE-2A' selected>". L::SecondeA ."</option>";
}else {
$content1.="<option value='2-SECONDE-2A'>". L::SecondeA ."</option>";
}

if($type=="2C"&&$niveau=="SECONDE")
{
$content1.="<option value='2-SECONDE-2C' selected>". L::SecondeC ."</option>";
}else {
$content1.="<option value='2-SECONDE-2C'>". L::SecondeC."</option>";
}

if($type=="1A"&&$niveau=="PREMIERE")
{
$content1.="<option value='2-PREMIERE-1A' selected>". L::PremiereA ."</option>";
}else {
$content1.="<option value='2-PREMIERE-1A'>". L::PremiereA ."</option>";
}

if($type=="1D"&&$niveau=='PREMIERE')
{
$content1.="<option value='2-PREMIERE-1D' selected>". L::PremiereD ."</option>";
}else {
$content1.="<option value='2-PREMIERE-1D'>". L::PremiereD ."</option>";
}

if($type=="TA"&&$niveau=='TERMINAL')
{
$content1.="<option value='2-TERMINAL-TA'>". L::TerminalA ."</option>";
}else {
$content1.="<option value='2-TERMINAL-TA'>". L::TerminalA ."</option>";
}

if($type=="TD"&&$niveau=="TERMINAL")
{
$content1.="<option value='2-TERMINAL-TD'>". L::TerminalD ."</option>";
}else {
$content1.="<option value='2-TERMINAL-TD'>". L::TerminalD ."</option>";
}




  // $classex->UpdateLibelleclasse($libelleclasse,$idclasse,$codeEtab,$sessionEtab);
  //
  // $_SESSION['user']['updateclasseok']=L::ClasseModificationsuccessfully;

  echo $content."*".$content1;


}else  if(isset($_POST['etape'])&&($_POST['etape']==17))
{
  $idclasse=htmlspecialchars($_POST['idclasse']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $typeclasse=htmlspecialchars($_POST['typeclasse']);
  $montantinscrip=htmlspecialchars($_POST['montantinscrip']);
  $montantscola=htmlspecialchars($_POST['montantscola']);
  $montantscolaaff=htmlspecialchars($_POST['montantscolaaff']);
  $libelleclasse=htmlspecialchars($_POST['libelleclasse']);

  $tabtypeclasse=explode("-",$typeclasse);
  $cycle=$tabtypeclasse[0];
  $niveau="";
  $niveau="";
  $type="";

  if($cycle==1)
  {
    echo "premier cycle";

    $niveau=$tabtypeclasse[1];
    $type=$tabtypeclasse[2];
    // $classe->Addclassefirstcycle($classes,$codeEtab,$libellesession,$montantinscrip,$montantscola,$montantscolaaff,$cycle,$niveau,$dateday,$type);
    $classex->Updateclassefirstcycle($libelleclasse,$montantinscrip,$montantscola,$montantscolaaff,$cycle,$niveau,$type,$idclasse,$codeEtab,$sessionEtab);

  }else if($cycle==2)
  {
    echo "second cycle";
    $niveau=$tabtypeclasse[1];
    $type=$tabtypeclasse[2];

    $classex->Updateclassefirstcycle($libelleclasse,$montantinscrip,$montantscola,$montantscolaaff,$cycle,$niveau,$type,$idclasse,$codeEtab,$sessionEtab);

      // $classe->AddclasseSecondcycle($classes,$codeEtab,$libellesession,$montantinscrip,$montantscola,$montantscolaaff,$cycle,$niveau,$dateday,$type);

  }

$_SESSION['user']['updateclasseok']=L::ClasseModificationsuccessfully;


}



 ?>
