<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Etablissement.php');
require_once('../class/Student.php');
require_once('../controller/functions.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$student= new Student();
$session= new Sessionacade();
$etabs= new Etab();
$classex= new Classe();

//nous allons recuperer la liste des activités parascolaires

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recuperation des variables

  $content="";
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['session']);
  $data = array();

  $parascos=$etabs->getAllParascolairesActivityOfThisSchool($codeEtab,$sessionEtab);

  foreach($parascos as $row)
{
 $data[] = array(
  'id'   => $row->id_act,
  'title'   =>$row->libelle_act,
  'start'   =>$row->datedeb_act." ".$row->heuredeb_act.":00",
  'end'   =>$row->datefin_act." ".$row->heurefin_act.":00"
 );
}

echo json_encode($data);

}

if(isset($_GET['view'])&&($_GET['view']==1))
{
  $content="";
  $codeEtab=htmlspecialchars($_SESSION['user']['codeEtab']);
  $sessionEtab=htmlspecialchars($_SESSION['user']['session']);
  $data = array();

  $parascos=$etabs->getAllParascolairesActivityOfThisSchool($codeEtab,$sessionEtab);

  foreach($parascos as $row)
{
 $data[] = array(
  'id'   => $row->id_act,
  'title'   =>$row->libelle_act,
  'start'   =>$row->datedeb_act." ".$row->heuredeb_act.":00",
  'end'   =>$row->datefin_act." ".$row->heurefin_act.":00"
 );
}

echo json_encode($data);
}else if(isset($_GET['view'])&&($_GET['view']==2))
{
  $content="";
  $codeEtab=htmlspecialchars($_SESSION['user']['codeEtab']);
  $sessionEtab=htmlspecialchars($_SESSION['user']['session']);
  $data = array();

  $parascos=$etabs->getAllParascolairesActivityOfThisSchool($codeEtab,$sessionEtab);

  foreach($parascos as $row)
{
 $data[] = array(
  'id'   => $row->id_act,
  'title'   =>$row->libelle_act,
  'start'   =>$row->datedeb_act." ".$row->heuredeb_act.":00",
  'end'   =>$row->datefin_act." ".$row->heurefin_act.":00"
 );
}

echo json_encode($data);
}





 ?>
