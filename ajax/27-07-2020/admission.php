<?php
session_start();
require_once('../class/Student.php');
require_once('../class/Classe.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Teatcher.php');
require_once('../class/Etablissement.php');
require_once('../controller/functions.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$student= new Student();
$classeSchool= new Classe();
$admin= new Localadmin();
$parents=new ParentX();
$teatcher=new Teatcher();
$etabs=new Etab();


if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //nous allons verifier si cet eleve existe deja dans la base de données

  //recupération des variables

  $content="";
  $matricule=htmlspecialchars(addslashes($_POST['matricule']));
  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $parent=htmlspecialchars(addslashes($_POST['parent']));

  $years=date('Y');
  $nextyears=date("Y")+1;
  $session=$years."-".$nextyears;

  //verifier si cet elève existe dans la base

  //$check=$student->ExisteStudent($matricule,$classe,$parent,$session);
  $check=$student->ExisteStudent($matricule);

  if($check==1)
  {
    //cet eleve est deja présent dans la base de données
    //nous allons verifier si celui ci est inscrit dans cette classe
    //$content=1;

    $check1=$student->InscriptionAllReady($matricule,$classe,$parent,$session);

    if($check1==1)
    {
      $content=1;
    }else {
      //cet eleve est deja dans la base mais n'est pas encore inscrit pour l'année encours
      $content=2;
    }

  }else {

    $content=0;

  }


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recuperation des variables

  // $idcompte=htmlspecialchars(addslashes($_POST['compte']));
  // $parent=htmlspecialchars(addslashes($_POST['codeEtab']));
  //
  // $lien=$student->generateficheLocalpdf($compte,$codeEtab);
  //
  // echo $lien;
}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recuperation des variables

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $session=htmlspecialchars(addslashes($_POST['session']));

  $content="";


  $datastudent=$student->DetermineAllstudentofthisclassesSessionT($classe,$codeEtab,$session);

  $nbligne=count($datastudent);

  if($nbligne==0)
  {
    $content.="<option value=''>".L::Nostudent."</option>";
  }else {
    //var_dump($datas);
  $content.="<option value=''>".L::SelectAnEleve."</option>";

    foreach ($datastudent as $value):
        $content .= "<option value='".$value->id_compte."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
    endforeach;
  }

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
// recuperation des variables

$codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
$classe=htmlspecialchars(addslashes($_POST['classe']));
$session=htmlspecialchars(addslashes($_POST['session']));
$studentid=htmlspecialchars(addslashes($_POST['student']));
$content="";


//retrouvons le montant à payer

//nous allons compter le nombre de versement effectué pour ce elève au cours de cette année academique

  $nbversement=$student->DetermineNumberOfversement($codeEtab,$classe,$session,$studentid);

  if($nbversement==0)
  {
    //le parent n'a pas encore fait de paiement alors nous allons recuperer le montant de la scolarite pour cette section



    $datavers=$classeSchool->getAllInformationsOfclassesByClasseId($classe,$session);
    $tabdatavers=explode("*",$datavers);

    $etablissementType=$etabs->DetermineTypeEtab($codeEtab);

    if($etablissementType==2)
    {
      //etablissement secondaire

      //nous allons voir si l'eleve est affecté ou non affecté

      $profilestudent=$student->getStudentProfile($studentid);

      if($profilestudent==0)
      {
        //non affecté
        $content=$tabdatavers[4]."-".$tabdatavers[6];
      }else if($profilestudent==1){
        // affecté
        $content=$tabdatavers[5]."-".$tabdatavers[6];
      }



    }else {
      // autre que etablissement secondaire
      $content=$tabdatavers[4]."-".$tabdatavers[6];
    }


  }else if($nbversement>0)
  {
    //nous avons au moins un versement alors nous allons chercher les informations du dernier versement

    $datavers=$student->SelectInformationsOfLastVersement($codeEtab,$classe,$session,$studentid);
    $tabdatavers=explode("*",$datavers);
    $content=$tabdatavers[0]."-".$tabdatavers[1];
  }

echo trim($content);

}if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //recupération des variables

  $montantapayer=htmlspecialchars(addslashes($_POST['montantapayer']));
  $montantpayer=htmlspecialchars(addslashes($_POST['montantpayer']));

  $restepaie=$montantapayer-$montantpayer;

  echo $restepaie;
}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
    //recuperation des variables
    $classe=htmlspecialchars(addslashes($_POST['classe']));
    $session=htmlspecialchars(addslashes($_POST['session']));
    $content="";

    //nous allons determiner la liste des eleves des classes selectionnées

    $datas=$student->getAllstudentofMulticlassesSession($classe,$session);

    $nbligne=count($datas);

    if($nbligne==0)
    {
      $content.="<option value=''>".L::Nostudent."</option>";
    }else {
      //var_dump($datas);
      $content.="<option value=''>".L::SelectAnEleve."</option>";

      foreach ($datas as $value):
          $content .= "<option value='".$value->id_compte."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
      endforeach;
    }

    echo $content;

    // $tabclasse=explode(",",$classe);
    //
    // $nbtable=count($tabclasse);
    //
    // if($nbtable==1)
    // {
    //   echo "egale a 0";
    // }else if($nbtable>1)
    // {
    //   echo "superieur à 1";
    // }



}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{
//recuperation des variables

$codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
$session=htmlspecialchars(addslashes($_POST['session']));
$desti=htmlspecialchars(addslashes($_POST['desti']));
$classe=htmlspecialchars(addslashes($_POST['classe']));
$content="";

if($desti==1)
{
  //Admin local
  //nous allons recher la liste des admin locaux assigner a cet etablissement

  $datas=$admin->getAllAdminLocalByUserId($codeEtab,$_SESSION['user']['IdCompte']);
  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>".L::NoAdmin."</option>";
  }else {
    //var_dump($datas);
    $content.="<option value=''>".L::AdminSelectOne."</option>";

    foreach ($datas as $value):
        $content .= "<option value='".$value->id_compte."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
    endforeach;
  }

  echo $content;



}else if($desti==2)
{
  //Parent

$datas=$parents->getAllParentBySchoolCode($codeEtab);
$nbligne=count($datas);

if($nbligne==0)
{
  $content.="<option value=''>Aucun Parent </option>";
}else {
  //var_dump($datas);
  $content.="<option value=''>Selectionner un parent </option>";

  foreach ($datas as $value):
      $content .= "<option value='".$value->id_compte."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
  endforeach;
}

echo $content;


}else if($desti==3)
{
  //Enseignant


  // $datas=$teatcher->getAllTeatchersBySchoolCode($codeEtab);
  $datas=$teatcher->getTeatcherClassesOfschool($codeEtab,$classe);
  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>Aucun enseignant </option>";
  }else {
    //var_dump($datas);
    $content.="<option value=''>Selectionner un enseignant </option>";

    foreach ($datas as $value):
        $content .= "<option value='".$value->id_compte."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
    endforeach;
  }

  echo $content;

}


}else if(isset($_POST['etape'])&&($_POST['etape']==8))
{
  //recuperation des variables

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $session=htmlspecialchars(addslashes($_POST['session']));
  $desti=htmlspecialchars(addslashes($_POST['desti']));
  $destimail=htmlspecialchars(addslashes($_POST['destimail']));

  $emailuser=$etabs->getCompteEmail($destimail);

  echo $emailuser;


}else if(isset($_POST['etape'])&&($_POST['etape']==9))
{
  //recuperation des variables

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $session=htmlspecialchars(addslashes($_POST['session']));
  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $studentid=htmlspecialchars(addslashes($_POST['studentid']));
  $content="";


  $datas=$student->getAllstudentofthisclassesSessionUnique($classe,$session,$studentid);
  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>Aucun élève </option>";
  }else {
    //var_dump($datas);
    $content.="<option value=''>Selectionner un élève </option>";

    foreach ($datas as $value):
        $content .= "<option value='".$value->id_compte."' selected>" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
    endforeach;
  }

  echo $content;


}else if(isset($_POST['etape'])&&($_POST['etape']==10))
{
  // recuperation des variables

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $session=htmlspecialchars(addslashes($_POST['session']));
  $studentid=htmlspecialchars(addslashes($_POST['student']));
  $content="";


  //retrouvons le montant à payer

  //nous allons compter le nombre de versement effectué pour ce elève au cours de cette année academique

  $nbversement=$student->DetermineNumberOfversement($codeEtab,$classe,$session,$studentid);

  if($nbversement==0)
  {
    //le parent n'a pas encore fait de paiement alors nous allons recuperer le montant de la scolarite pour cette section
    $datavers=$classeSchool->getAllInformationsOfclassesByClasseCmrId($classe,$session);

    $tabdatavers=explode("*",$datavers);
    $content=$tabdatavers[4]."-".$tabdatavers[5];
  }else if($nbversement>0)
  {
    //nous avons au moins un versement alors nous allons chercher les informations du dernier versement

    $datavers=$student->SelectInformationsOfLastVersement($codeEtab,$classe,$session,$studentid);
    $tabdatavers=explode("*",$datavers);
    $content=$tabdatavers[0]."-".$tabdatavers[1];
  }

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==11))
{
  //nous allons verifier si un eleve existe avec ce matricule dans cet Etablissement

//recuperation des variables

$codeEtab=htmlspecialchars($_POST['codeEtab']);
$session=htmlspecialchars($_POST['session']);
$matricule=htmlspecialchars($_POST['matricule']);

$nb=$student->ExisteStudent($matricule);

echo $nb;


}else if(isset($_POST['etape'])&&($_POST['etape']==12))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $session=htmlspecialchars($_POST['session']);
  $matricule=htmlspecialchars($_POST['matricule']);

  $datas=$student->getstudentinfosbyMat($matricule);

  //recuperer le nom et prénoms de l'élève

  $array=json_encode($datas,true);
  $someArray = json_decode($array, true);

  $nomcomplet=$someArray[0]["nom_eleve"]." ".$someArray[0]["prenom_eleve"];

  $texte="Un élève existe dejà avec ce matricule dans le système il s'agit de ".$nomcomplet;

  echo $texte;

}else if(isset($_POST['etape'])&&($_POST['etape']==13))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $session=htmlspecialchars($_POST['session']);
  $matricule=htmlspecialchars($_POST['matricule']);

  $datas=$student->getstudentinfosbyMat($matricule);

  //recuperer le nom et prénoms de l'élève

  $array=json_encode($datas,true);
  $someArray = json_decode($array, true);
  $donnees=$someArray[0]["nom_eleve"]."*".$someArray[0]["prenom_eleve"]."*".date_format(date_create($someArray[0]["datenais_eleve"]),"d/m/Y")."*".$someArray[0]["lieunais_eleve"]."*".$someArray[0]["sexe_eleve"];
  echo $donnees;
}else if(isset($_POST['etape'])&&($_POST['etape']==14))
{
  //nous allons recuperer les informations des parents de cet else

  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $session=htmlspecialchars($_POST['session']);
  $matricule=htmlspecialchars($_POST['matricule']);

  $datas=$student->getstudentinfosbyMat($matricule);

  $array=json_encode($datas,true);
  $someArray = json_decode($array, true);

  $eleveid_compte=$someArray[0]["id_compte"];

  // echo $eleveid_compte;

 $infosparents=$parents->ParentInfostudent($eleveid_compte);
 $content="";

 foreach ($infosparents as $value):
   $content=$content.$value->id_compte."*";
 endforeach;

 $content=substr($content, 0, -1);

 echo $content;

 // $nbparent=count($infosparents);



}else if(isset($_POST['etape'])&&($_POST['etape']==15))
{
  //recupertaion des infos du parents

  $parentstudentid=htmlspecialchars($_POST['parentstudentid']);
  $datas=$parents->getParentInfosbyId($parentstudentid);
  echo $datas;


}else if(isset($_POST['etape'])&&($_POST['etape']==16))
{
  //recupertaion des infos du parents

  $concatnew=htmlspecialchars($_POST['concatnew']);
  $concatold=htmlspecialchars($_POST['concatold']);
  $somme=$concatnew+$concatold;

  echo $somme;


}else if(isset($_POST['etape'])&&($_POST['etape']==17))
{
  $session=htmlspecialchars($_POST['session']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $type="ANTECEDENTS";
  $content="";

  $datas=$student->getAllSchoolAntecedents($codeEtab,$type);

  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>Aucun antécédent médical </option>";
  }else {
    //var_dump($datas);
    $content.="<option value='' selected>Selectionner un antécédent médical</option>";

    foreach ($datas as $value):
        // $content .= "<option value='".$value->id_desease."' >" . utf8_encode(utf8_decode($value->libelle_desease)). "</option>";
$content .= "<option value='".$value->id_desease."' >" . utf8_encode($value->libelle_desease). "</option>";
    endforeach;
  }

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==18))
{
  $session=htmlspecialchars($_POST['session']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $type="INFANTILES";
  $content="";

  $datas=$student->getAllSchoolAntecedents($codeEtab,$type);

  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>Aucune maladies infantiles </option>";
  }else {
    //var_dump($datas);
    $content.="<option value='' selected>Selectionner une maladie infantile</option>";

    foreach ($datas as $value):
        // $content .= "<option value='".$value->id_desease."' >" . utf8_encode(utf8_decode($value->libelle_desease)). "</option>";
$content .= "<option value='".$value->id_desease."' >" . utf8_encode($value->libelle_desease). "</option>";
    endforeach;
  }

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==19))
{
  $session=htmlspecialchars($_POST['session']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $type="ALLERGIES";
  $content="";

  $datas=$student->getAllSchoolAntecedents($codeEtab,$type);

  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>Aucune allergie </option>";
  }else {
    //var_dump($datas);
    $content.="<option value='' selected>Selectionner une allergie</option>";

    foreach ($datas as $value):
        $content .= "<option value='".$value->id_desease."' >" . utf8_encode($value->libelle_desease). "</option>";
        // $content .= "<option value='".$value->id_desease."' >" . utf8_encode(utf8_decode($value->libelle_desease)). "</option>";
    endforeach;
  }

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==20))
{
  //nous devons rechercher le montant des frais d'inscription

  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $classeid=htmlspecialchars($_POST['classeid']);
  $session=htmlspecialchars($_POST['session']);
  $studentid=htmlspecialchars($_POST['student']);
  $motif=htmlspecialchars($_POST['motif']);
  $libellemotif="";
  $content="";

  if($motif==1)
  {
    $libellemotif="INSCRIPTIONS";
  }else if($motif==2)
  {
    $libellemotif="SCOLARITES";
  }else if($motif==3)
  {
    $libellemotif="CANTINES";
  }

  //nous allons compter le nombre de versement effectué pour ce elève au cours de cette année academique pour le motif

  $nbversement=$student->DetermineNumberOfversementByMotif($codeEtab,$classeid,$session,$studentid,$libellemotif);

  if($nbversement==0)
  {
    //le parent n'a pas encore fait de paiement alors nous allons recuperer le montant de l'inscription pour cette classe'

    $datavers=$classeSchool->getAllInformationsOfclassesByClasseId($classeid,$session);
    $tabdatavers=explode("*",$datavers);
    $content=$tabdatavers[7];


  }else {
    // nous avons deja eu des versment pour ce motif alors nous allons chercher le montant correspondant au reste à payer

    $datavers=$student->SelectInformationsOfLastVersementByMotif($codeEtab,$classeid,$session,$studentid,$libellemotif);

    $content=$datavers;

  }

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==21))
{
  //nous devons rechercher le montant des frais d'inscription

  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $classeid=htmlspecialchars($_POST['classeid']);
  $session=htmlspecialchars($_POST['session']);
  $studentid=htmlspecialchars($_POST['student']);
  $motif=htmlspecialchars($_POST['motif']);
  $libellemotif="";
  $content="";

  if($motif==1)
  {
    $libellemotif="INSCRIPTIONS";
  }else if($motif==2)
  {
    $libellemotif="SCOLARITES";
  }else if($motif==3)
  {
    $libellemotif="CANTINES";
  }

  //nous allons compter le nombre de versement effectué pour ce elève au cours de cette année academique pour le motif

  $nbversement=$student->DetermineNumberOfversementByMotif($codeEtab,$classeid,$session,$studentid,$libellemotif);

  if($nbversement==0)
  {
    //le parent n'a pas encore fait de paiement alors nous allons recuperer le montant de la scolarité pour cette classe'

    $datavers=$classeSchool->getAllInformationsOfclassesByClasseId($classeid,$session);
    $tabdatavers=explode("*",$datavers);
    $content=$tabdatavers[4];


  }else {
    // nous avons deja eu des versment pour ce motif alors nous allons chercher le montant correspondant au reste à payer

    $datavers=$student->SelectInformationsOfLastVersementByMotif($codeEtab,$classeid,$session,$studentid,$libellemotif);

    $content=$datavers;

  }

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==22))
{
  //nous devons rechercher le montant des frais de cantine

  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $classeid=htmlspecialchars($_POST['classeid']);
  $session=htmlspecialchars($_POST['session']);
  $studentid=htmlspecialchars($_POST['student']);
  $motif=htmlspecialchars($_POST['motif']);
  $libellemotif="";
  $content="";

  if($motif==1)
  {
    $libellemotif="INSCRIPTIONS";
  }else if($motif==2)
  {
    $libellemotif="SCOLARITES";
  }else if($motif==3)
  {
    $libellemotif="CANTINES";
  }else if($motif==5)
  {
    $libellemotif="TRANSPORTS";
  }


  //
  // if($nbversement==0)
  // {
  //   //aucun versement n'a été fait pour ce motif
  //
  //   //nous allons determiner le choix de cantine du parent
  //
  //   $choice=$student->DetermineCantinechoice($codeEtab,$classeid,$session,$studentid);
  //
  //   //nous allons determiner le montant du choix
  //   $montantchoice=$student->DeterminechoiceAmount($choice,$codeEtab,$classeid,$session,$studentid);
  //
  //
  //
  // }else {
  //   // nous avons au moins un versement pour ce motif
  //   $datavers=$student->SelectInformationsOfLastVersementByMotif($codeEtab,$classeid,$session,$studentid,$libellemotif);
  //
  //   $content=$datavers;
  // }


  //nous allons compter le nombre de versement effectué pour ce elève au cours de cette année academique pour le motif

  $nbversement=$student->DetermineNumberOfversementByMotif($codeEtab,$classeid,$session,$studentid,$libellemotif);

  echo $nbversement;


}else if(isset($_POST['etape'])&&($_POST['etape']==23))
{
  //il n'y as pas encore eu de paiement pour les frais de cantine
  //nous allons recuperer le montant a payer ainsi que le nombre de mois à selectionner

  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $classeid=htmlspecialchars($_POST['classeid']);
  $session=htmlspecialchars($_POST['session']);
  $studentid=htmlspecialchars($_POST['student']);
  $motif=htmlspecialchars($_POST['motif']);
  $libellemotif="";
  $nbchoice="";
  $content="";

  if($motif==1)
  {
    $libellemotif="INSCRIPTIONS";
  }else if($motif==2)
  {
    $libellemotif="SCOLARITES";
  }else if($motif==3)
  {
    $libellemotif="CANTINES";
  }

  //nous allons determiner le choix de cantine du parent

   $choice=$student->DetermineCantinechoice($codeEtab,$classeid,$session,$studentid);

   //nous allons determiner le montant du choix
   $datas=$student->DeterminechoiceAmount($choice,$codeEtab,$classeid,$session,$studentid);
   $tabchoice=explode("*",$datas);

   $montantchoice=$tabchoice[0];
   $libelletypechoice=$tabchoice[1];

   if($libelletypechoice=="MENSUEL")
   {
     $nbchoice=1;
   }else if($libelletypechoice=="TRIMESTRIEL")
   {
     $nbchoice=3;
   }else if($libelletypechoice=="ANNUEL")
   {
     $nbchoice=9;
   }

   $content=$montantchoice."-".$nbchoice;

   echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==24))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $classeid=htmlspecialchars($_POST['classeid']);
  $session=htmlspecialchars($_POST['session']);
  $studentid=htmlspecialchars($_POST['student']);
  $motif=htmlspecialchars($_POST['motif']);
  $libellemotif="";
  $content="";

  if($motif==1)
  {
    $libellemotif="INSCRIPTIONS";
  }else if($motif==2)
  {
    $libellemotif="SCOLARITES";
  }else if($motif==3)
  {
    $libellemotif="CANTINES";
  }

  //rechercher la liste des mois qui ont fait l'objet d'un paiment

  $content="";


  $datas=$student->getAllversementCantineMonth($codeEtab,$classeid,$session,$studentid,$libellemotif);

  $tabrow=array();
  $tabstandard=array('1','2','3','4','5','6','7','8','9','10','11','12');

  $i=0;
  foreach ($datas as  $value):


    $mois=$value->mois_versement;
    $tabrow[$i]=obtenirindiceMois($mois);
    // $tabrow()=obtenirindiceMois($mois);

    $i++;
  endforeach;

// var_dump($tabrow);

$result = array_diff($tabstandard,$tabrow);

$newtab=array_values($result);

// var_dump($newtab);

$nbtab=count($newtab);
//
if($nbtab==0)
{
  $content.="<option value=''>Aucun mois </option>";
}else {

  for($j=0;$j<$nbtab;$j++)
  {
    // echo obtenirMoisbyindice($result[$j]);
  $content .= "<option value='".obtenirMoisbyindice($newtab[$j])."' >" .obtenirMoisbyindice($newtab[$j]). "</option>";
  }
}


$choice=$student->DetermineCantinechoice($codeEtab,$classeid,$session,$studentid);

//nous allons determiner le montant du choix
$datas=$student->DeterminechoiceAmount($choice,$codeEtab,$classeid,$session,$studentid);
$tabchoice=explode("*",$datas);

$montantchoice=$tabchoice[0];
$libelletypechoice=$tabchoice[1];

if($libelletypechoice=="MENSUEL")
{
  $nbchoice=1;
}else if($libelletypechoice=="TRIMESTRIEL")
{
  $nbchoice=3;
}else if($libelletypechoice=="ANNUEL")
{
  $nbchoice=9;
}


echo $content.'-'.$nbchoice.'-'.$montantchoice;

}else if(isset($_POST['etape'])&&($_POST['etape']==25))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $classeid=htmlspecialchars($_POST['classeid']);
  $session=htmlspecialchars($_POST['session']);

  $libellemotif="CANTINES";
  $content="";

  //retrouver la liste des mois pour lesquels nous avons un paiement

  $datas=$student->getAllversementCantineMonthClasse($codeEtab,$classeid,$session,$libellemotif);

  // var_dump($datas);

  $nb=count($datas);

  // echo $nb;
//
  if($nb==0)
  {
  $content.="<option value=''>Aucun mois </option>";
  }else {
    foreach ($datas as $value):
      // echo $value->indicemois_versement;
  $content .= "<option value='".obtenirMoisbyindice($value->indicemois_versement)."' >" .obtenirMoisbyindice($value->indicemois_versement). "</option>";
    endforeach;
  }
//
echo $content;
}else if(isset($_POST['etape'])&&($_POST['etape']==26))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $classeid=htmlspecialchars($_POST['classeid']);
  $session=htmlspecialchars($_POST['session']);
  $studentid=htmlspecialchars($_POST['student']);
  $motif=htmlspecialchars($_POST['motif']);
  $libellemotif="";
  $content="";
  $libellemotif="AES";

  $content="";

  //la liste des activités extra scolaire de cette classe

$datas=$student->getAllActivitiesOfThisclasse($codeEtab,$session);
$newtab=array();
$i=0;
 foreach ($datas as $value):

   $classeSelect=substr($value->classes_act, 0, -1);
   $tabclasseSelect=explode("-",$classeSelect);

if(in_array($classeid,$tabclasseSelect))
{
  $newtab[$i]=$value->id_act;
}

$i++;
 endforeach;


// var_dump($newtab);

$nb=count($newtab);

// echo $nb;

if($nb==0)
{
$content.="<option value=''>Aucune activité </option>";
}else {
  $content.="<option value=''>Selectionner une activité </option>";
    for($i=0;$i<$nb;$i++)
    {
      $content .= "<option value='".$newtab[$i]."' >" .$student->getLibelleActivities($newtab[$i]). "</option>";
    }
}

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==27))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $session=htmlspecialchars($_POST['session']);
  $activities=htmlspecialchars($_POST['activities']);

  $montant=$student->getActivitiesAmount($codeEtab,$session,$activities);

  echo $montant;

}else if(isset($_POST['etape'])&&($_POST['etape']==28))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $classeid=htmlspecialchars($_POST['classeid']);
  $session=htmlspecialchars($_POST['session']);

  $libellemotif="TRANSPORTS";
  $content="";

  //retrouver la liste des mois pour lesquels nous avons un paiement

  $datas=$student->getAllversementTransportsMonthClasse($codeEtab,$classeid,$session,$libellemotif);

  $nb=count($datas);

  // echo $nb;
//
  if($nb==0)
  {
  $content.="<option value=''>Aucun mois </option>";
  }else {
    foreach ($datas as $value):
      // echo $value->indicemois_versement;
  $content .= "<option value='".obtenirMoisbyindice($value->indicemois_versement)."' >" .obtenirMoisbyindice($value->indicemois_versement). "</option>";
    endforeach;
  }
//
echo $content;


}else if(isset($_POST['etape'])&&($_POST['etape']==29))
{

//nous allons chercher le nombre de versement concernant cet eleve et dont il est question de transport

  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $classeid=htmlspecialchars($_POST['classeid']);
  $session=htmlspecialchars($_POST['session']);
  $studentid=htmlspecialchars($_POST['student']);
  $motif=htmlspecialchars($_POST['motif']);
  $transportype=htmlspecialchars($_POST['transportype']);
  $libellemotif="";
  $nbchoice="";
  $content="";

  if($motif==1)
  {
    $libellemotif="INSCRIPTIONS";
  }else if($motif==2)
  {
    $libellemotif="SCOLARITES";
  }else if($motif==3)
  {
    $libellemotif="CANTINES";
  }else if($motif==5)
  {
    $libellemotif="TRANSPORTS";
  }

  //nous allons determiner le choix de cantine du parent


  if($transportype=="MENSUEL")
  {
    $nbchoice=1;
  }else if($transportype=="TRIMESTRIEL")
  {
    $nbchoice=3;
  }else if($transportype=="ANNUEL")
  {
    $nbchoice=9;
  }


$nbversement=$student->DetermineNumberOfversementByMotif($codeEtab,$classeid,$session,$studentid,$libellemotif);

echo $nbchoice."-".$nbversement;

}else if(isset($_POST['etape'])&&($_POST['etape']==30))
{

  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $classeid=htmlspecialchars($_POST['classeid']);
  $session=htmlspecialchars($_POST['session']);
  $studentid=htmlspecialchars($_POST['student']);
  $motif=htmlspecialchars($_POST['motif']);
  $transportype=htmlspecialchars($_POST['transportype']);
  $libellemotif="";
  $nbchoice="";
  $content="";

  if($motif==1)
  {
    $libellemotif="INSCRIPTIONS";
  }else if($motif==2)
  {
    $libellemotif="SCOLARITES";
  }else if($motif==3)
  {
    $libellemotif="CANTINES";
  }else if($motif==5)
  {
    $libellemotif="TRANSPORTS";
  }

  //nous allons determiner le choix de cantine du parent


  if($transportype=="MENSUEL")
  {
    $nbchoice=1;
  }else if($transportype=="TRIMESTRIEL")
  {
    $nbchoice=3;
  }else if($transportype=="ANNUEL")
  {
    $nbchoice=9;
  }


  $datas=$student->getAllversementCantineMonth($codeEtab,$classeid,$session,$studentid,$libellemotif);

  $tabrow=array();
  $tabstandard=array('1','2','3','4','5','6','7','8','9','10','11','12');

  $i=0;
  foreach ($datas as  $value):


    $mois=$value->mois_versement;
    // echo $mois;
    // $tabrow[$i]=obtenirindiceMois($mois);
    $tabrow[$i]=obtenirindiceMoisCaps($mois);

    // $tabrow()=obtenirindiceMois($mois);

    $i++;
  endforeach;

  // var_dump($tabrow);

  // var_dump($datas);

  $result = array_diff($tabstandard,$tabrow);

  $newtab=array_values($result);

  // var_dump($newtab);

  $nbtab=count($newtab);
  //
  if($nbtab==0)
  {
  $content.="<option value=''>Aucun mois </option>";
  }else {

  for($j=0;$j<$nbtab;$j++)
  {

  $content .= "<option value='".obtenirMoisbyindiceCaps($newtab[$j])."' >" .obtenirMoisbyindiceCaps($newtab[$j]). "</option>";
  }
  }

echo $content;


}


 ?>
