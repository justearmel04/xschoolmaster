<?php
session_start();
require_once('../class/Salle.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$sallex= new Salle();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$libetab=htmlspecialchars(addslashes($_POST['libetab']));
$salle=htmlspecialchars(addslashes($_POST['salle']));

  $check=$sallex->ExisteSalles($libetab,$salle);

  if($check==0)
  {
$content=0;
  }else {
    $content=1;
  }

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  $content="";
  $code=htmlspecialchars(addslashes($_POST['code']));

  $datas=$sallex->getAllSallesbyschoolCode($code);

  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>Aucune Salle </option>";
  }else {
    //var_dump($datas);
    $content.="<option value='' selected>Selectionner une salle</option>";

    foreach ($datas as $value):
        $content .= "<option value='". $value->id_salle ."' >" . utf8_encode(utf8_decode($value->libelle_salle)). "</option>";
    endforeach;
  }

  echo $content;


}



 ?>
