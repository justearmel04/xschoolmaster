<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Etablissement.php');
require_once('../class/Student.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$student= new Student();
$session= new Sessionacade();
$etabs= new Etab();
$classex= new Classe();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
//nous devons publier le cours
//recupération des variables

$content="";

$teatcherid=htmlspecialchars($_POST['teatcherid']);
$classeid=htmlspecialchars($_POST['classe']);
$courseid=htmlspecialchars($_POST['courseid']);
$matiereid=htmlspecialchars($_POST['matiere']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);

$etabs->AdddevoirsPublication($courseid,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab);

//nous allons envoyer une notification aux parents et aux enfants de cette classe

$_SESSION['user']['addclasseok']=L::DevoirspublishedOk;


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $homeworkid=htmlspecialchars($_POST['homeworkid']);

  $etabs->DeletedSupportsDevoirs($homeworkid,$courseid);

  $_SESSION['user']['addclasseok']=L::SupportsDevDeletedOk;

}


 ?>
