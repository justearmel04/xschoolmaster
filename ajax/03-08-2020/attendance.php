<?php
session_start();
require_once('../controller/functions.php');
require_once('../class/Student.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$student= new Student();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$classe=htmlspecialchars(addslashes($_POST['classe']));
$datepre=htmlspecialchars(addslashes($_POST['datepre']));

$tabdate=explode("/",$datepre);


//echo retranscrireMois($mois)." ".$annee;


$newdate=dateFormat($datepre);

//echo $newdate;

  $check=$student->presencesExistByClassesAndDate($classe,$newdate);

  if($check>0)
  {
    $content=1;
  }else {
    $content=0;
  }

echo $content;

}



 ?>
