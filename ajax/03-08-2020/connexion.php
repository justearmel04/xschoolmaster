<?php
session_start();
require_once('../class/User.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$users = new User();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recupération des variables

  $login=$_POST['login'];
  $pass=$_POST['pass'];

  $content="";

$check=$users->CheckCompte($login);

if($check>0)
{
  //nous allons voir si le statut est actif

  $check1=$users->CheckComptestatut($login);

  if($check1>0)
  {
      //nous allons verifier que le mot de passe est le bon

      $check2=$users->CheckComptepasswd($login,$pass);

      if($check2>0)
      {
          //les informations sont correctes
          $content=3;
      }else {
        // le mot de passe du compte est incorrect

        $content=2;
      }
  }else {
    // le statut du compte est inactif
    $content=1;
  }
}else {
  // le login n'existe pas

  $content=0;
}

echo $content;
}

 ?>
