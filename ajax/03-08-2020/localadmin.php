<?php
session_start();
require_once('../class/LocalAdmin.php');
require_once('../class/User.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$admin= new Localadmin();
$userlocal=new User();
if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recupération des variables

  $content="";



  $login=htmlspecialchars(addslashes($_POST['login']));
  $email=htmlspecialchars(addslashes($_POST['email']));
  $codeetab=htmlspecialchars(addslashes($_POST['codeetab']));

  //$admin= new Localadmin();

  $loginstatus=$userlocal->CheckingLogin($login);

  if($loginstatus>0)
  {
    $content=1;
  }else if($loginstatus==0)
  {
    $content=0;
  }

  // $check=$admin->existAdminLocal($login,$email);
  // //$check=$admin=checklocalcompte($login,$email);
  //
  // if($check==0)
  // {
  //   $content=0;
  // }else {
  //   // nous allons verifier si le compte existe dans cet etablissment
  //   $idAdlocal=$admin->getIdlocal($email);
  //   $check1=$admin->existAdminbySchoolCode($email,$idAdlocal);
  //
  //   //echo $check1;
  //
  //   if($check1==0)
  //   {
  //     //le client exiset mais n'est pas  pas assigner à un etablissement
  //
  //     $content=2;
  //   }else {
  //     $content=1;
  //   }
  //   //$content=1;
  // }
//var_dump($idAdlocal);
  echo $content;

}if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recuperation des variables
  $content="";
  $code=htmlspecialchars(addslashes($_POST['code']));
  $localId=htmlspecialchars(addslashes($_POST['idcompte']));

  //rechercher la liste des des enseignants assigner a cet etablissement

  $datas=$admin->getAllAdmibyschoolCode($code,$localId);

  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>Aucun Administrateur Local</option>";
  }else {
    //var_dump($datas);
    $content.="<option value='' >Selectionner un Administrateur</option>";

    foreach ($datas as $value):
        $content .= "<option value='". $value->id_compte ."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
    endforeach;
  }


//var_dump($datas);
/*
  foreach ($datas as $value):





    $content .= "<option value='". $value->id_compte ."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";



  endforeach;*/

echo $content;

}if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  $compte=htmlspecialchars(addslashes($_POST['compte']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $lien=$admin->generateficheLocalpdf($compte,$codeEtab);

  echo $lien;
}


 ?>
