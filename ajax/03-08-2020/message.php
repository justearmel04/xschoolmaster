<?php
session_start();
require_once('../class/Message.php');
require_once('../class/Parent.php');
require_once('../class/Student.php');
require_once('../class/Etablissement.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$messages = new Message();
$etabs=new Etab();

//recuperation des variables


if(isset($_POST['email'])&&($_POST['phone'])&&($_POST['name'])&&($_POST['message']))
{
  $name=$_POST['name'];
  $email=$_POST['email'];
  $phone=$_POST['phone'];
  $message=$_POST['message'];

$check=$messages->MessageDemo($name,$email,$phone,$message);

echo $check;



}
if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recuperation des variables

  $notifid=htmlspecialchars(addslashes($_POST['notifid']));
  $content="";

  $datas=$etabs->getNotificationInfos($notifid);

  echo $datas;



}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recuperation des variables smssender+'&emailsender='+emailsender
  $smssender=htmlspecialchars(addslashes($_POST['smssender']));
  $emailsender=htmlspecialchars(addslashes($_POST['emailsender']));
  $notifid=htmlspecialchars(addslashes($_POST['notifid']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $destinataires=htmlspecialchars(addslashes($_POST['destinataires']));
  $classes=htmlspecialchars(addslashes($_POST['classes']));
  $sessionEtab=htmlspecialchars(addslashes($_POST['sessionEtab']));

  $tabdatadestinataires=explode("-",$destinataires);
  $nbtabdestinataires=count($tabdatadestinataires);
  $cptedestinatairesval=$nbtabdestinataires-1;

  $tabclasses=explode("-",$classes);
  $nbtabclasses=count($tabclasses);
  $cpteclassesval=$nbtabclasses-1;

  //determiner l'indicatif de ce pays

  $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

  $destimails="";
  $destiphone="";


    for($i=0;$i<$cpteclassesval;$i++)
    {


      for($j=0;$j<$cptedestinatairesval;$j++)
      {
          // echo $tabclasses[$i]."-".$tabdatadestinataires[$j].'<br>';
          if($tabdatadestinataires[$j]=="Parent")
          {
              // echo "Parent de la classe ".$tabclasses[$i]."<br>";
              //rechercher le mail des parents dont un eleve appartient à cette classeEtab

              $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab,$sessionEtab);
               //var_dump($dataParents);
               $ka=1;
               foreach ($dataParents as $parents):

                 $destimails=$destimails.$parents->email_parent."*";
                 $destiphone=$destiphone.$indicatifEtab.$parents->tel_parent."*";


                 $ka++;
               endforeach;

          }else if($tabdatadestinataires[$j]=="Student")
          {
            // echo "Student de la classe ".$tabclasses[$i]."<br>";
            // rechercher le mail des eleves de cette classeEtab
            $dataStudents=$etabs->getEmailsOfStudentOfThisClasses($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab);
            //var_dump($dataStudents);

            $ka=1;
            foreach ($dataStudents as $students):

              $destimails=$destimails.$students->email_eleve."*";
              $destiphone=$destiphone.$indicatifEtab.$students->tel_compte."*";


              $ka++;
            endforeach;

          }else if($tabdatadestinataires[$j]=="Teatcher")
          {
            // echo "Teatcher de la classe ".$tabclasses[$i]."<br>";
            // rechercher le mail des professeurs de cette classe

            $datateatchers=$etabs->getEmailsOfTeatcherOfThisClasses($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab);
            //var_dump($datateatchers);

            $ka=1;
            foreach ($datateatchers as $teatchers):

              $destimails=$destimails.$teatchers->email_compte."*";
              $destiphone=$destiphone.$indicatifEtab.$teatchers->tel_compte."*";


              $ka++;
            endforeach;

          }else if($tabdatadestinataires[$j]=="Admin_locale")
          {
            // echo "Admin de la classe ".$tabclasses[$i]."<br>";
            // rechercher la liste local_admin de cet etablissement
            $datalocaladmins=$etabs->getEmailOfLocaladminOfThisClasses($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab);
          //  var_dump($datalocaladmins);

          $ka=1;
          foreach ($datalocaladmins as $locals):

            $destimails=$destimails.$locals->email_compte."*";
            $destiphone=$destiphone.$indicatifEtab.$locals->tel_compte."*";


            $ka++;
          endforeach;

          }
      }
    }

    echo $destimails."/".$destiphone;



}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  $notifid=htmlspecialchars(addslashes($_POST['notifid']));
  $content="";

  // $datas=$etabs->getNotificationInfos($notifid);

  $datas=$etabs->getMessagesInfos($notifid);

  echo $datas;
}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  $smssender=htmlspecialchars(addslashes($_POST['smssender']));
  $emailsender=htmlspecialchars(addslashes($_POST['emailsender']));
  $notifid=htmlspecialchars(addslashes($_POST['notifid']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $destinataires=htmlspecialchars(addslashes($_POST['destinataires']));
  $classes=htmlspecialchars(addslashes($_POST['classes']));
  $sessionEtab=htmlspecialchars(addslashes($_POST['sessionEtab']));
  $precis=htmlspecialchars(addslashes($_POST['precis']));
  $eleves=htmlspecialchars(addslashes($_POST['eleves']));

  $tabdatadestinataires=explode("-",$destinataires);
  $nbtabdestinataires=count($tabdatadestinataires);
  $cptedestinatairesval=$nbtabdestinataires-1;

  $tabclasses=explode("-",$classes);
  $nbtabclasses=count($tabclasses);
  $cpteclassesval=$nbtabclasses-1;

  //determiner l'indicatif de ce pays

  $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

  $destimails="";
  $destiphone="";

  if($precis==0)
  {
  //la liste des parents de toutes les classes selectionnées

  for($i=0;$i<$cpteclassesval;$i++)
  {
    for($j=0;$j<$cptedestinatairesval;$j++)
    {

    if($tabdatadestinataires[$j]=="Parent")
    {
        // echo "Parent de la classe ".$tabclasses[$i]."<br>";
        //rechercher le mail des parents dont un eleve appartient à cette classeEtab

        // $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab,$sessionEtab);

        $dataParents=$etabs->getEmailsOfParentOfStudentInThisClassesParenter($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab,$sessionEtab);


         // var_dump($dataParents);
         $ka=1;
         foreach ($dataParents as $parents):

           $destimails=$destimails.$parents->email_parent."*";
           $destiphone=$destiphone.$indicatifEtab.$parents->tel_parent."*";


           $ka++;
         endforeach;

    }
  }
  }



  }else if($precis==1)
  {
      //listes des parents en fonction des eleves selectionnées

      $tabeleves=explode("-",$eleves);
      $nbtabeleves=count($tabeleves);
      $cpteelevesval=$nbtabeleves-1;

      // echo $classes." ".$eleves;

      $classes=substr($classes, 0, -1);
      $eleves=substr($eleves, 0, -1);

      $classes = str_replace("-", ",",$classes);
      $eleves=str_replace("-", ",",$eleves);

      // $dataParents=$etabs->getEmailsAndPhoneOfParentOfStudentclasses($classes,$eleves,$sessionEtab,$codeEtab);
      $dataParents=$etabs->getEmailsAndPhoneOfParentOfStudentclassesParenter($classes,$eleves,$sessionEtab,$codeEtab);


      // var_dump($dataParents);
      $ka=1;
      foreach ($dataParents as $parents):

        $destimails=$destimails.$parents->email_parent."*";
        $destiphone=$destiphone.$indicatifEtab.$parents->tel_parent."*";


        $ka++;
      endforeach;




  }

    echo $destimails."/".$destiphone;

}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  //recuperation des variables

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $session=htmlspecialchars(addslashes($_POST['session']));
  $codeetab=htmlspecialchars(addslashes($_POST['codeetab']));
  $eleves=htmlspecialchars(addslashes($_POST['eleves']));
  $message=htmlspecialchars(addslashes($_POST['message']));
  $addby=htmlspecialchars(addslashes($_POST['addby']));
  $precis=htmlspecialchars(addslashes($_POST['precis']));

  //nous allons chercher à savoir qui a ecrit cette note

  $typecompteadder=$etabs->getTypecompteOfuser($addby);

  //nous allons essayer de voir si il est question d'une activité parascolaire ou note d'observation

    $datamessages=$etabs->getMessagesType($message,$addby);
    $tabmessages=explode("*",$datamessages);
    $parascolaire=$tabmessages[0];
    $scolarite=$tabmessages[1];



  if($typecompteadder=="Teatcher")
  {
    // $lien=$etabs->creationOfObservationNoteTeatcher($classe,$session,$codeetab,$eleves,$message);

    if($parascolaire==0)
    {
      if($precis==0)
      {
$lien=$etabs->creationOfObservationNoteTeatcher($classe,$session,$codeetab,$eleves,$message);
      }else if($precis==1)
      {
$lien=$etabs->creationOfObservationNoteTeatcherPrecis($classe,$session,$codeetab,$eleves,$message);
      }
    }

  }else if($typecompteadder=="Admin_locale")
  {
    if($parascolaire==1)
    {
      // $lien=$etabs->creationOfObservationNote($classe,$session,$codeetab,$eleves,$message);
      if($precis==0)
      {
        $lien=$etabs->creationOfObservationNoteAllselect($classe,$session,$codeetab,$eleves,$message);
      }else if($precis==1)
      {
        $lien=$etabs->creationOfObservationNote($classe,$session,$codeetab,$eleves,$message);

      }


    }else if($parascolaire==0)
    {
      if($precis==0)
      {
        $lien=$etabs->createMessageEtatForAll($classe,$session,$codeetab,$eleves,$message);
      }else if($precis==1)
      {
          $lien=$etabs->createMessageEtatForPrecise($classe,$session,$codeetab,$eleves,$message);
      }
    }

  }

echo $lien;


}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{
  //recuperation des variables

  $notifid=htmlspecialchars(addslashes($_POST['notifid']));
  $paraid=htmlspecialchars(addslashes($_POST['paraid']));
  $scolaire=htmlspecialchars(addslashes($_POST['scolaire']));
  $sessionEtab=htmlspecialchars(addslashes($_POST['sessionEtab']));
  $addby=htmlspecialchars(addslashes($_POST['addby']));

  if($paraid==1)
  {
    //il est question d'une activité parascolaire
    $datas=$etabs->getMessagesInfos($notifid);
  }else if($scolaire==1)
  {
    //il est question du paiement de scolarité

  }else if($paraid==0&&$scolaire==0)
  {
    //il est question d'un message

    $datas=$etabs->getMessagesInfosOnly($notifid);
  }

echo $datas;
}



 ?>
