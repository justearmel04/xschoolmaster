<?php
session_start();
require_once('../class/Student.php');
require_once('../class/Classe.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Teatcher.php');
require_once('../class/Etablissement.php');
require_once('../controller/functions.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$student= new Student();
$classeSchool= new Classe();
$admin= new Localadmin();
$parents=new ParentX();
$teatcher=new Teatcher();
$etabs=new Etab();


if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //nous allons verifier si cet eleve existe deja dans la base de données

  //recupération des variables


  $senderid=htmlspecialchars($_POST['senderid']);
  $receiverid=htmlspecialchars($_POST['receiverid']);
  $codeEtab=$_SESSION['user']['codeEtab'];

  $_SESSION['user']['senderid']=$senderid;
  $_SESSION['user']['receiverid']=$receiverid;

  $codeserach=$senderid."@".$receiverid;
  $codesearch1=$receiverid."@".$senderid;

  $concatids=$etabs->getAllchatmessages($receiverid,$senderid,$codeEtab);

  $concatids=substr($concatids, 0, -1);

  //tous les messages concernant nosdeux utilisateurs

  // $datas=$etabs->getMessagesconversations($concatids);
  $datas=$etabs->getconversationsbycodes($codeserach,$codesearch1);
  $nbmessages=count($datas);

  echo $nbmessages;

  $_SESSION['user']['messageNumber']=$nbmessages;

  //determiner le nombre de message entre les deux utilisateurs




}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  $content="";
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $receiverid=htmlspecialchars($_POST['receiverid']);
  $senderid=htmlspecialchars($_POST['senderid']);
  $messages=htmlspecialchars($_POST['messages']);
  $datemessage=date("Y-m-d");
  $codemessage=$senderid."@".$receiverid;
  //insertion dans la bdd
  $status=0;
  //rechercher le statut du receiver

  $receiverstatut=$etabs->getreceiverstatus($receiverid);

  if($receiverstatut>0)
  {
    $status=1;
  }

  $etabs->AddchatsMessages($codeEtab,$sessionEtab,$receiverid,$senderid,$messages,$datemessage,$codemessage,$status);

  $concatids=$etabs->getAllchatmessages($receiverid,$senderid,$codeEtab);

  $concatids=substr($concatids, 0, -1);

  //tous les messages concernant nosdeux utilisateurs

  $datas=$etabs->getMessagesconversations($concatids);
  $nbmessages=count($datas);

  $compteuserid=$_SESSION['user']['IdCompte'];

  $retour="";

  foreach ($datas as  $value):
    $code=$value->code_chat;
    $tabcode=explode("@",$code);
    if($tabcode[0]==$compteuserid)
    {

      $datax=$etabs->getcompteInfos($compteuserid);
      $lientofreceiver="";
      foreach ($datax as $datasUser):
        $tofreceiver=$datasUser->photo_compte;
        if(strlen($tofreceiver)>0)
        {
          $lientofreceiver="../photo/".$datasUser->email_compte."/".$datasUser->photo_compte;
        }else {
          $lientofreceiver="../photo/user5.jpg";
        }
      endforeach;

      //sender
      $retour.="<li class=\"replies\">";

      $retour.="<div id=\"messages\">";

      $retour.="<img src=".$lientofreceiver."  id=\"pseudo\" alt=\"\" />";
     $retour.="<p>";
     $retour.=$value->messages_chat;
     $retour.="</p>";
      $retour.="</div>";

      $retour.="</li>";

    }else {
      $datax=$etabs->getcompteInfos($senderid);
      $lientofreceiver="";
      foreach ($datax as $datasUser):
        $tofreceiver=$datasUser->photo_compte;
        if(strlen($tofreceiver)>0)
        {
          $lientofreceiver="../photo/".$datasUser->email_compte."/".$datasUser->photo_compte;
        }else {
          $lientofreceiver="../photo/user5.jpg";
        }
      endforeach;

      //sender
      $retour.="<li class=\"sent\">";

      $retour.="<div id=\"messages\">";

      $retour.="<img src=".$lientofreceiver."  id=\"pseudo\" alt=\"\" />";
     $retour.="<p>";
     $retour.=$value->messages_chat;
     $retour.="</p>";
      $retour.="</div>";

      $retour.="</li>";
    }
  endforeach;


echo $retour."*".$nbmessages;

}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recuperation des  variables
  $content=1;

  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $toi=htmlspecialchars($_POST['toi']);
  $moi=htmlspecialchars($_POST['moi']);
  $nbmessages=htmlspecialchars($_POST['nbmessages']);

  $codeserach=$toi."@".$moi;
  $codesearch1=$moi."@".$toi;

  $concatids=$etabs->getAllchatmessages($toi,$moi,$codeEtab);

  $concatids=substr($concatids, 0, -1);

  //tous les messages concernant nosdeux utilisateurs

  // $datas=$etabs->getMessagesconversations($concatids);
  $datas=$etabs->getconversationsbycodes($codeserach,$codesearch1);
  $nbmessagex=count($datas);

  if($nbmessagex==$nbmessages)
  {
    $content=0;
  }

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $toi=htmlspecialchars($_POST['toi']);
  $moi=htmlspecialchars($_POST['moi']);

  $codeserach=$toi."@".$moi;
  $codesearch1=$moi."@".$toi;


  $concatids=$etabs->getAllchatmessages($toi,$moi,$codeEtab);

  $concatids=substr($concatids, 0, -1);

  //tous les messages concernant nosdeux utilisateurs

  // $datas=$etabs->getMessagesconversations($concatids);

  $datas=$etabs->getconversationsbycodes($codeserach,$codesearch1);

  $compteuserid=$_SESSION['user']['IdCompte'];

  $retour="";

  foreach ($datas as  $value):
    $code=$value->code_chat;
    $tabcode=explode("@",$code);
    if($tabcode[0]==$compteuserid)
    {

      $datax=$etabs->getcompteInfos($compteuserid);
      $lientofreceiver="";
      foreach ($datax as $datasUser):
        $tofreceiver=$datasUser->photo_compte;
        if(strlen($tofreceiver)>0)
        {
          $lientofreceiver="../photo/".$datasUser->email_compte."/".$datasUser->photo_compte;
        }else {
          $lientofreceiver="../photo/user5.jpg";
        }
      endforeach;

      //sender
      $retour.="<li class=\"replies\">";

      $retour.="<div id=\"messages\">";

      $retour.="<img src=".$lientofreceiver."  id=\"pseudo\" alt=\"\" />";
     $retour.="<p>";
     $retour.=$value->messages_chat;
     $retour.="</p>";
      $retour.="</div>";

      $retour.="</li>";

    }else {
      $datax=$etabs->getcompteInfos($toi);
      $lientofreceiver="";
      foreach ($datax as $datasUser):
        $tofreceiver=$datasUser->photo_compte;
        if(strlen($tofreceiver)>0)
        {
          $lientofreceiver="../photo/".$datasUser->email_compte."/".$datasUser->photo_compte;
        }else {
          $lientofreceiver="../photo/user5.jpg";
        }
      endforeach;

      //sender
      $retour.="<li class=\"sent\">";

      $retour.="<div id=\"messages\">";

      $retour.="<img src=".$lientofreceiver."  id=\"pseudo\" alt=\"\" />";
     $retour.="<p>";
     $retour.=$value->messages_chat;
     $retour.="</p>";
      $retour.="</div>";

      $retour.="</li>";
    }
  endforeach;


echo $retour;
}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $receiverid=htmlspecialchars($_POST['receiverid']);
  $senderid=htmlspecialchars($_POST['senderid']);
  $messages=htmlspecialchars($_POST['messages']);
  $datemessage=date("Y-m-d");
  $codemessage=$senderid."@".$receiverid;

  $codeserach=$senderid."@".$receiverid;
  $codesearch1=$receiverid."@".$senderid;
  //insertion dans la bdd
  $status=0;

  // $etabs->AddchatsMessages($codeEtab,$sessionEtab,$receiverid,$senderid,$messages,$datemessage,$codemessage,$status);

  $concatids=$etabs->getAllchatmessages($receiverid,$senderid,$codeEtab);

  $concatids=substr($concatids, 0, -1);


  // $datas=$etabs->getMessagesconversations($concatids);
  // $nbmessages=count($datas);

  //rechercher les conversations des deux

  $data=$etabs->getconversationsbycodes($codeserach,$codesearch1);
  $nbmessages=count($data);

  $compteuserid=$_SESSION['user']['IdCompte'];

  $retour="";

  foreach ($data as  $value):
    $code=$value->code_chat;
    $tabcode=explode("@",$code);
    if($tabcode[0]==$compteuserid)
    {

      $datax=$etabs->getcompteInfos($compteuserid);
      $lientofreceiver="";
      foreach ($datax as $datasUser):
        $tofreceiver=$datasUser->photo_compte;
        if(strlen($tofreceiver)>0)
        {
          $lientofreceiver="../photo/".$datasUser->email_compte."/".$datasUser->photo_compte;
        }else {
          $lientofreceiver="../photo/user5.jpg";
        }
      endforeach;

      //sender
      $retour.="<li class=\"replies\">";

      $retour.="<div id=\"messages\">";

      $retour.="<img src=".$lientofreceiver."  id=\"pseudo\" alt=\"\" />";
     $retour.="<p>";
     $retour.=$value->messages_chat;
     $retour.="</p>";
      $retour.="</div>";

      $retour.="</li>";

    }else {
      $datax=$etabs->getcompteInfos($senderid);
      $lientofreceiver="";
      foreach ($datax as $datasUser):
        $tofreceiver=$datasUser->photo_compte;
        if(strlen($tofreceiver)>0)
        {
          $lientofreceiver="../photo/".$datasUser->email_compte."/".$datasUser->photo_compte;
        }else {
          $lientofreceiver="../photo/user5.jpg";
        }
      endforeach;

      //sender
      $retour.="<li class=\"sent\">";

      $retour.="<div id=\"messages\">";

      $retour.="<img src=".$lientofreceiver."  id=\"pseudo\" alt=\"\" />";
     $retour.="<p>";
     $retour.=$value->messages_chat;
     $retour.="</p>";
      $retour.="</div>";

      $retour.="</li>";
    }
  endforeach;


echo $retour."*".$nbmessages;


}

 ?>
