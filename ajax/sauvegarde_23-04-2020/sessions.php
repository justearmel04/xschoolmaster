<?php
session_start();
require_once('../class/Salle.php');
require_once('../class/Sessionsacade.php');
$session= new Sessionacade();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$titre=htmlspecialchars(addslashes($_POST['titre']));
$codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));

$check=$session->ExisteSessionForThisSchool($titre,$codeEtab);

if($check==0)
{
  $content=0;
}else if($check>0)
{
  $content=1;
}

echo $content;

}if(isset($_POST['etape'])&&($_POST['etape']==2))
{
    //recuperation des variables

    $sessionid=htmlspecialchars(addslashes($_POST['sessionid']));
    $sessionlib=htmlspecialchars(addslashes($_POST['sessionlib']));
    $typesession=htmlspecialchars(addslashes($_POST['typesession']));
    $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
    $oldsessiontype=htmlspecialchars(addslashes($_POST['oldsessiontype']));

    $check=$session->UpdateSessionSchool($sessionid,$sessionlib,$typesession,$codeEtab);

    //insertion dans la table semestre

    $content=1;

    if($typesession==2)
    {
      //cas semestre

      for($i=1;$i<=$typesession;$i++)
      {
        $libellesemestre="SEMESTRE ".$i;
        $statut=1;
        //insertion dans la tables semestre
        $session->AddSemestre($libellesemestre,$sessionid,$statut,$codeEtab);
      }

    }else if($typesession==3)
    {
      //cas trimestre

      for($i=1;$i<=$typesession;$i++)
      {
        $libellesemestre="TRIMESTRE ".$i;
        $statut=1;
        //insertion dans la tables semestre
        $session->AddSemestre($libellesemestre,$sessionid,$statut,$codeEtab);
      }

    }

    $_SESSION['user']['Updateadminok']="Session mis à jour avec succès";

    echo $content;
}



 ?>
