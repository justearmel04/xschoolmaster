<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Etablissement.php');
require_once('../class/Student.php');

$student= new Student();
$session= new Sessionacade();
$etabs= new Etab();
$classex= new Classe();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
//nous devons publier le cours
//recupération des variables

$content="";

$teatcherid=htmlspecialchars($_POST['teatcherid']);
$classeid=htmlspecialchars($_POST['classe']);
$courseid=htmlspecialchars($_POST['courseid']);
$matiereid=htmlspecialchars($_POST['matiere']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);

$etabs->AddcoursesPublication($courseid,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab);

//nous allons envoyer une notification aux parents et aux enfants de cette classe

$_SESSION['user']['addclasseok']="Le cours a été publié avec succès";


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //supprimer la section
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $sectionid=htmlspecialchars($_POST['sectionid']);

  $etabs->DeletedsectionCourses($sectionid,$courseid);

  $_SESSION['user']['addclasseok']="La section a bien été supprimé avec succès";

}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //supprimer la compétence
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $compid=htmlspecialchars($_POST['compid']);

  $etabs->DeletedcompCourses($compid,$courseid);

  $_SESSION['user']['addclasseok']="La compétence a bien été supprimée avec succès";


}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $homeworkid=htmlspecialchars($_POST['homeworkid']);

  $etabs->DeletedHomewkCourses($homeworkid,$courseid);

  $_SESSION['user']['addclasseok']="Un exercice a bien été supprimé avec succès";

}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //supprimer la section
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $libellesection=htmlspecialchars($_POST['section']);

  $etabs->AddCourseSection($libellesection,$courseid);

  $_SESSION['user']['addclasseok']="Nouvelle section ajouté avec succès";

}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  //supprimer la compétence
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $libellecomp=htmlspecialchars($_POST['libellecomp']);

  $etabs->AddCourseComp($libellecomp,$courseid);

  $_SESSION['user']['addclasseok']="Une nouvelle compétence a bien été ajoutée avec succès";


}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classeid=htmlspecialchars($_POST['classe']);
  $courseid=htmlspecialchars($_POST['courseid']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $libellehome=htmlspecialchars($_POST['libellehome']);

  $etabs->AddCourseHomework($libellehome,$courseid);

  $_SESSION['user']['addclasseok']="Un nouvel exercice a bien été ajouté avec succès";

}


 ?>
