<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Etablissement.php');
require_once('../class/Student.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$student= new Student();
$session= new Sessionacade();
$etabs= new Etab();
$classex= new Classe();




if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$classeid=htmlspecialchars($_POST['classeid']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
// $section=htmlspecialchars(addslashes($_POST['section']));
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);
// $check=$classex->ExisteClasses($libetab,$classe,$session,$section);

$datas=$classex->getAllclasseEtabBysession($codeEtab,$sessionEtab);

$nbligne=count($datas);

if($nbligne==0)
{
  $content.="<option value=''>".L::NoClasse."</option>";
}else if($nbligne>0)
{
  $content.="<option value=''>".L::Selectclasses."</option>";
  foreach ($datas as $value):
    $content .= "<option value='". $value->id_classe ."' >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";

  endforeach;
}

echo $content;

}elseif (isset($_POST['etape'])&&($_POST['etape']==2)) {


  $studentid = htmlspecialchars($_POST['studentid']);
  $classeactu = htmlspecialchars($_POST['classeactu']);
  $newclasse = htmlspecialchars($_POST['newclasse']);
  $classeid=htmlspecialchars($_POST['classeid']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  // $section=htmlspecialchars(addslashes($_POST['section']));
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $classex->ChangeStudentClasse($studentid,$newclasse,$classeid,$codeEtab,$sessionEtab);

  $_SESSION['user']['updateteaok']=L::ChangeTheclasseSuccessfully;


}elseif (isset($_POST['etape'])&&($_POST['etape']==3)) {


  $student = htmlspecialchars($_POST['studentid']);
  // $classe = htmlspecialchars($_POST['classeactu']);
  $classnew = htmlspecialchars($_POST['classenew']);
  $idclasse=htmlspecialchars($_POST['idclasse']);
  $Etabactu=htmlspecialchars($_POST['codeEtab']);
  $newetab =htmlspecialchars($_POST['newEtab']);
  $sessionEta=htmlspecialchars($_POST['sessionEtab']);
  $classex->ChangeStudentEtab($student,$classnew,$idclasse,$Etabactu,$sessionEta,$newetab);

}



if(isset($_POST['etape'])&&($_POST['etape']==5))
{

//recupération des variables

$content="";
$content1="";

$classeid=htmlspecialchars($_POST['classeid']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
// $section=htmlspecialchars(addslashes($_POST['section']));
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);

$datas=$etabs->getIbsaschools();
// var_dump($datas);
$content.="<option value=''>".L::SelectOneEtabs."</option>";
foreach ($datas as  $value) :

  if($value->code_etab==$codeEtab)
  {

  }else {
    $datax=$etabs->getEtablissementbyCodeEtab($value->code_etab);
    foreach ($datax as $values):
      $content .= "<option value='". $values->code_etab ."' >" . utf8_encode(utf8_decode($values->libelle_etab)). "</option>";
    endforeach;

  }

endforeach;

echo $content;

// $check=$classex->ExisteClasses($libetab,$classe,$session,$section);

// $datas=$classex->getAllclasseEtabBysession($codeEtab,$sessionEtab);
// $nbligne=count($datas);
//
// if($nbligne==0)
// {
//   $content.="<option value=''>".L::NoClasse."</option>";
// }else if($nbligne>0)
// {
//   $content.="<option value=''>".L::Selectclasses."</option>";
//   foreach ($datas as $value):
//     if ($value->id_classe==$classeid) {
//       // code...
//     }else {
//       $content .= "<option value='". $value->id_classe ."' >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";
//     }
//
//   endforeach;
// }
//
// echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  $content="";


  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);

$datas=$classex->getAllclasseEtabBysession($codeEtab,$sessionEtab);
 $nbligne=count($datas);

 if($nbligne==0)
 {
   $content.="<option value=''>".L::NoClasse."</option>";
 }else if($nbligne>0)
 {
   $content.="<option value=''>".L::Selectclasses."</option>";
   foreach ($datas as $value):
     $content .= "<option value='". $value->id_classe ."' >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";

   endforeach;
 }

 echo $content;

}

 ?>
