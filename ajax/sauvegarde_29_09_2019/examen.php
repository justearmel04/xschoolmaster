<?php
session_start();
require_once('../class/Matiere.php');
require_once('../class/Etablissement.php');
require_once('../class/Sessionsacade.php');


$session= new Sessionacade();
$matiere= new Matiere();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$examen=htmlspecialchars(addslashes($_POST['examen']));
$datedeb=htmlspecialchars(addslashes($_POST['datedeb']));
$datefin=htmlspecialchars(addslashes($_POST['datefin']));
$codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));

//$check=$matiere->AllreadyExistMatiere();

//$content=$teatcher->getAllTeatchersByschoolCode($codeEtab);

//verifier si la matière existe dejà

//$check=$matiere->AllreadyExistMatiere();

$check=$etab->AllreadyExistExamens($examen,$datedeb,$datefin,$codeEtab);


if($check==0)
{
  //cette matière n'existe pas encore pour cette classe
  $content=0;
}else {
//cette matière existe deja pour cette classe
$content=1;

}

echo $content;
}if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recupération des variables

$code=htmlspecialchars(addslashes($_POST['code']));
$classe=htmlspecialchars(addslashes($_POST['classe']));

$nbsessionOn=$session->getNumberSessionEncoursOn($code);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($code);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
}
$content="";

$datas=$etab->getAllsubjectofclassesbyIdclasses($classe,$code,$libellesessionencours);

$nbligne=count($datas);

if($nbligne==0)
{
  $content.="<option selected value=''>Aucune Matière </option>";
}else {
  //var_dump($datas);
  $content.="<option selected value='' ><?php echo L::SelectSubjects ?></option>";

  foreach ($datas as $value):
      $content .= "<option value='". $value->id_mat."' >" . utf8_encode(utf8_decode($value->libelle_mat)). "</option>";
  endforeach;
}

echo $content;

}if(isset($_POST['etape'])&&($_POST['etape']==3))
{
    //recupération des variables

    $classe=htmlspecialchars(addslashes($_POST['classe']));
    $matiere=htmlspecialchars(addslashes($_POST['matiere']));
    $day=htmlspecialchars(addslashes($_POST['day']));
    $heuredeb=htmlspecialchars(addslashes($_POST['heuredeb']));
    $heurefin=htmlspecialchars(addslashes($_POST['heurefin']));
    $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
    $content="";

    //verifier si nous avons deja une routine dans cette période

    $check=$etab->verifyRoutineExistInTimePeriode($heuredeb,$heurefin,$day,$classe,$codeEtab);

    if($check>0)
    {
      //nous avons une matière dans la période choisie

      //nous allons verifier si il est question de la meme matière ou d'une autre matière
      $check1=$etab->verifyRoutineExistwithThisSubject($heuredeb,$heurefin,$day,$classe,$codeEtab,$matiere);

      if($check1>0)
      {
        //il est question de la même matiere
        $content=1;
      }else {
        //il est question d'une nouvelle matière or nous avons une matière a cette période
        $content=2;
      }

    }else {
      // nous n'avons pas de matière dans la période choisie
      $content=0;
    }

echo $content;
}if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //recuperation des variables

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $code=htmlspecialchars(addslashes($_POST['code']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $type=2;
  $content='';

  $dataExam=$etab->getAllexamensOfSchool($code);
  $concatidExam="";

  foreach ($dataExam as $value):
      $concatidExam .=$value->id_exam."*";
  endforeach;

  $concatidExam=substr($concatidExam, 0, -1);

  echo $concatidExam;
  $concatidExamwithoutNotes="";
  $tabExamid=explode("*",$concatidExam);
  $nbligne=count($tabExamid);

   echo $nbligne;

  for($i=0;$i<$nbligne;$i++)
  {
    $idExam=$tabExamid[$i];
    //compter le nombre de note pour cette classe,exam et codeEtab
    $check=$etab->getNumberOfNotesSchoolClassesExamen($classe,$code,$matiere,$type,$idExam);
    if($check==0)
    {
      $concatidExamwithoutNotes.=$idExam.",";
    }

  }

//  echo $concatidExamwithoutNotes;

  if($concatidExamwithoutNotes!="")
  {
    $concatidExamwithoutNotes=substr($concatidExamwithoutNotes, 0, -1);
   //echo $concatidExamwithoutNotes;
   $tabNotes=explode(",",$concatidExamwithoutNotes);
   $datas=$etab->getAllExamOfThisValue($concatidExamwithoutNotes,$code);
   // $datas=$etab->getAllexamensOfSchool($code);

   $nbligne=count($datas);

   if($nbligne==0)
   {
   $content.="<option selected value=''>Aucun Examen </option>";
   }else {
   //var_dump($datas);
   $content.="<option selected value=''  >Selectionner une Examen</option>";

   foreach ($datas as $value):
       $content .= "<option value='". $value->id_exam."' >" . utf8_encode(utf8_decode($value->libelle_exam)). "</option>";
   endforeach;
   }

 }else {
    $content.="<option selected value=''>Aucun Examen </option>";
 }


  echo $content;

  //nous allons verifier si nous avons des notes pour cette matiere et cette classe

}if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //recuperation des variables

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $code=htmlspecialchars(addslashes($_POST['code']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $tabmat=explode("-",$matiere);
  $matiereid=$tabmat[0];
  $teatcherid=htmlspecialchars(addslashes($_POST['teatcher']));
  $type=2;
  $content='';

  $dataExam=$etab->getAllexamensOfSchool($code);
  $concatidExam="";

  foreach ($dataExam as $value):
      $concatidExam .=$value->id_exam."*";
  endforeach;

  $concatidExam=substr($concatidExam, 0, -1);

  //echo $concatidExam;

  $concatidExamwithoutNotes="";
  $tabExamid=explode("*",$concatidExam);
  $nbligne=count($tabExamid);

   //echo $nbligne;

   for($i=0;$i<$nbligne;$i++)
   {
     $idExam=$tabExamid[$i];
     //compter le nombre de note pour cette classe,exam et codeEtab
     $check=$etab->getNumberOfNotesSchoolClassesExamenTeatcher($classe,$code,$matiereid,$type,$idExam,$teatcherid);
     if($check==0)
     {
       $concatidExamwithoutNotes.=$idExam.",";
     }

   }

   if($concatidExamwithoutNotes!="")
   {
     $concatidExamwithoutNotes=substr($concatidExamwithoutNotes, 0, -1);
    //echo $concatidExamwithoutNotes;
    $tabNotes=explode(",",$concatidExamwithoutNotes);
    $datas=$etab->getAllExamOfThisValue($concatidExamwithoutNotes,$code);
    // $datas=$etab->getAllexamensOfSchool($code);

    $nbligne=count($datas);

    if($nbligne==0)
    {
    $content.="<option selected value=''>Aucun Examen </option>";
    }else {
    //var_dump($datas);
    $content.="<option selected value=''  >Selectionner une Examen</option>";

    foreach ($datas as $value):
        $content .= "<option value='". $value->id_exam."' >" . utf8_encode(utf8_decode($value->libelle_exam)). "</option>";
    endforeach;
    }

  }else {
     $content.="<option selected value=''>Aucun Examen </option>";
  }

  echo $content;







}



 ?>
