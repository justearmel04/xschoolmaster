<?php
session_start();
require_once('../class/Classe.php');
$classex= new Classe();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$libetab=htmlspecialchars(addslashes($_POST['libetab']));
$classe=htmlspecialchars(addslashes($_POST['classe']));

  $check=$classex->ExisteClasses($libetab,$classe);

  if($check==0)
  {
$content=0;
  }else {
    $content=1;
  }

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  $content="";
  $code=htmlspecialchars(addslashes($_POST['code']));

  $datas=$classex->getAllClassesbyschoolCode($code);

  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>Aucune Classe </option>";
  }else {
    //var_dump($datas);
    $content.="<option value='' selected>Selectionner une classe</option>";

    foreach ($datas as $value):
        $content .= "<option value='". $value->id_classe ."' >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";
    endforeach;
  }

  echo $content;


}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recuperation des variables

  $content="";

  $classeId=htmlspecialchars(addslashes($_POST['classe']));

  $datas=$classex->getAllStudentOfThisClasses($classeId);

  foreach ($datas as $value):
      $content .= "<option  selected value='". $value->id_compte ."' >" . utf8_encode(utf8_decode($value->nom_eleve." ".$value->prenom_eleve)). "</option>";
  endforeach;

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //recuperation des variables
$classeId=htmlspecialchars(addslashes($_POST['classe']));

$content="";

$content=$classex->getCodeEtabOfClassesByClasseId($classeId);

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{

  $code=htmlspecialchars(addslashes($_POST['code']));
  $parentid=htmlspecialchars(addslashes($_POST['parentid']));
   $content="";
   $content.="<option value='' selected>Selectionner une classe</option>";

  $classeSchool=$classex->getAllClassesOfParentHadStudent($code,$parentid);

  foreach ($classeSchool as $value):
      $content .= "<option value='". $value->id_classe ."' >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";
  endforeach;

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  //rechercher la liste des eleves du parent et etant inscrit dans cette classe
  //recuperation des variables

  $code=htmlspecialchars(addslashes($_POST['code']));
  $parentid=htmlspecialchars(addslashes($_POST['parentid']));
  $classeEtab=htmlspecialchars(addslashes($_POST['classeEtab']));
  $content="";

  $students=$classex->getAllStudentOfParentSchool($code,$classeEtab,$parentid);

$content.="<option value=''>".L::SelectAnEleve."</option>";

  foreach ($students as $value):
      $content .= "<option value='". $value->idcompte_eleve ."' >" . utf8_encode(utf8_decode($value->nom_eleve.' '.$value->prenom_eleve)). "</option>";
  endforeach;

  echo $content;



}



 ?>
