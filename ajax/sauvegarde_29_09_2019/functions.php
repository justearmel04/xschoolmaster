<?php

function regiveMois($mois) {
switch($mois) {
    case '1': $mois = '01'; break;
    case '2': $mois = '02'; break;
    case '3': $mois = '03'; break;
    case '4': $mois = '04'; break;
    case '5': $mois = '05'; break;
    case '6': $mois = '06'; break;
    case '7': $mois = '07'; break;
    case '8': $mois = '08'; break;
    case '9': $mois = '09'; break;
    case '10': $mois = '10'; break;
    case '11': $mois = '11'; break;
    case '12': $mois = '12'; break;
    default: $mois =''; break;
  }
  return $mois;
}

function retranscrireMois($mois) {
switch($mois) {
    case '01': $mois = '1'; break;
    case '02': $mois = '2'; break;
    case '03': $mois = '3'; break;
    case '04': $mois = '4'; break;
    case '05': $mois = '5'; break;
    case '06': $mois = '6'; break;
    case '07': $mois = '7'; break;
    case '08': $mois = '8'; break;
    case '09': $mois = '9'; break;
    case '10': $mois = '10'; break;
    case '11': $mois = '11'; break;
    case '12': $mois = '12'; break;
    default: $mois =''; break;
  }
  return $mois;
}
function obtenirLibelleMois($mois) {
switch($mois) {
    case '01': $mois = 'JANVIER'; break;
    case '02': $mois = 'FEVRIER'; break;
    case '03': $mois = 'MARS'; break;
    case '04': $mois = 'AVRIL'; break;
    case '05': $mois = 'MAI'; break;
    case '06': $mois = 'JUIN'; break;
    case '07': $mois = 'JUILLET'; break;
    case '08': $mois = 'AOUT'; break;
    case '09': $mois = 'SEPTEMBRE'; break;
    case '10': $mois = 'OCTOBRE'; break;
    case '11': $mois = 'NOVEMBRE'; break;
    case '12': $mois = 'DECEMBRE'; break;
    default: $mois =''; break;
  }
  return $mois;
}

function dateFormat($date) {

   $date = substr($date, 6, 4) . "-" . substr($date, 3, 2) . "-" . substr($date, 0, 2);

   return $date;

  }

  function AfficherSemaine($day)
  {
    $valeur="";
    if($day=="LUN")
    {
      $valeur="LUNDI";
    }else if($day=="MAR")
    {
      $valeur="MARDI";
    }else if($day=="MER")
    {
      $valeur="MERCREDI";
    }else if($day=="JEU")
    {
      $valeur="JEUDI";
    }else if($day=="VEN")
    {
      $valeur="VENDREDI";
    }else if($day=="SAM")
    {
      $valeur="SAMEDI";
    }else if($day=="DIM")
    {
      $valeur="DIMANCHE";
    }

    return $valeur;
  }

?>
