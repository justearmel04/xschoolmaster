<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Etablissement.php');
require_once('../class/Student.php');

$student= new Student();
$session= new Sessionacade();
$etabs= new Etab();
$classex= new Classe();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
//nous devons publier le cours
//recupération des variables

$content="";

$teatcherid=htmlspecialchars($_POST['teatcherid']);
$classeid=htmlspecialchars($_POST['classe']);
$courseid=htmlspecialchars($_POST['courseid']);
$matiereid=htmlspecialchars($_POST['matiere']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);

$etabs->AddquizPublication($courseid,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab);

//nous allons envoyer une notification aux parents et aux enfants de cette classe

$_SESSION['user']['addclasseok']="Le quiz a été publié avec succès";


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recuperation des variables

  $libelle=htmlspecialchars($_POST['libelle']);
  $propositionid=htmlspecialchars($_POST['propositionid']);
  $questionid=htmlspecialchars($_POST['questionid']);
  $valeurproposition=htmlspecialchars($_POST['valeurproposition']);

  $idfalseproposition=$etabs->ChangePropositionFalseToTrueByQuestionId($questionid);

  $etabs->ChangePropositionTrueToFalse($libelle,$valeurproposition,$propositionid,$questionid);

  $_SESSION['user']['addclasseok']="La réponse à la question a été modifiée en FAUX";


}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  $libelle=htmlspecialchars($_POST['libelle']);
  $propositionid=htmlspecialchars($_POST['propositionid']);
  $questionid=htmlspecialchars($_POST['questionid']);
  $valeurproposition=htmlspecialchars($_POST['valeurproposition']);

  $idfalseproposition=$etabs->ChangePropositionTrueToFalseByQuestionId($questionid);

  $etabs->ChangePropositionTrueToFalse($libelle,$valeurproposition,$propositionid,$questionid);

  $_SESSION['user']['addclasseok']="La réponse à la question a été modifiée en VRAI";

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //il s'agit de décocher la proposition de réponse

  $propositionid=htmlspecialchars($_POST['propositionid']);
  $questionid=htmlspecialchars($_POST['questionid']);
  $valeurproposition=htmlspecialchars($_POST['valeurproposition']);

    $etabs->UpadtepropositionVal($valeurproposition,$propositionid,$questionid);

      $_SESSION['user']['addclasseok']="La proposition de réponse a été cochée avec succès";

}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //il s'agit de cocher la proposition de réponse

  $propositionid=htmlspecialchars($_POST['propositionid']);
  $questionid=htmlspecialchars($_POST['questionid']);
  $valeurproposition=htmlspecialchars($_POST['valeurproposition']);

    $etabs->UpadtepropositionVal($valeurproposition,$propositionid,$questionid);

    $_SESSION['user']['addclasseok']="La proposition de réponse a été décochée avec succès";

}



 ?>
