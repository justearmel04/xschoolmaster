<?php
session_start();
require_once('../class/Matiere.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');
require_once('../controller/functions.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();
$matierex= new Matiere();
$etab=new Etab();
$classex= new Classe();



if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables
//data: 'etape=' + etape+ '&heurelibid=' +heurelibid+ '&dayshort='+dayshort+ '&horaires=' +horaires+ '&codeEtab='+codeEtab+ '&sessionEtab=' +sessionEtab,
$heurelibid=htmlspecialchars($_POST['heurelibid']);
$dayshort=htmlspecialchars($_POST['dayshort']);
$horaires=htmlspecialchars($_POST['horaires']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);
$classeid=htmlspecialchars($_POST['classeid']);

$tabhoraires=explode("-",$horaires);
$datedeb=trim($tabhoraires[0]).":00";
$datefin=trim($tabhoraires[1]).":00";

$datedeb=trim(str_replace("H", ":",$datedeb));
$datefin=trim(str_replace("H", ":",$datefin));

$content=0;

// echo $datedeb ." ".$datefin;

echo $heurelibid." ".$dayshort." ".$datedeb." ".$datefin." ".$codeEtab." ".$sessionEtab." ".$classeid;

$datas=$etab->getNbroutinesthisdayAthours($heurelibid,$dayshort,$datedeb,$datefin,$codeEtab,$sessionEtab,$classeid);

$nb=count($datas);

// echo $nb;

if($nb>0)
{
  //est ce un libelle ou un cours
  foreach ($datas as $value):
    if($value->matiere_route>0)
    {
      //est ce qu'il s'agit de la meme classe

      if($value->classe_route==$classeid)
      {
        $content=0;
      }else {
        $content=1;
      }

// $content=1;
}else {
  $content=2;
}
  endforeach;

}

echo $content;

}


 ?>
