<?php
session_start();
require_once('../class/Student.php');
$student= new Student();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //nous allons verifier si cet eleve existe deja dans la base de données

  //recupération des variables

  $content="";
  $matricule=htmlspecialchars(addslashes($_POST['matricule']));
  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $parent=htmlspecialchars(addslashes($_POST['parent']));

  $years=date('Y');
  $nextyears=date("Y")+1;
  $session=$years."-".$nextyears;

  //verifier si cet elève existe dans la base

  //$check=$student->ExisteStudent($matricule,$classe,$parent,$session);
  $check=$student->ExisteStudent($matricule);

  if($check==1)
  {
    //cet eleve est deja présent dans la base de données
    //nous allons verifier si celui ci est inscrit dans cette classe
    //$content=1;

    $check1=$student->InscriptionAllReady($matricule,$classe,$parent,$session);

    if($check1==1)
    {
      $content=1;
    }else {
      //cet eleve est deja dans la base mais n'est pas encore inscrit pour l'année encours
      $content=2;
    }

  }else {

    $content=0;

  }


}


 ?>
