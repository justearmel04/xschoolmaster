<?php
session_start();

require_once('../class/Etablissement.php');
$routine = new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //ajout de la routine

  //recupération des variables

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $day=htmlspecialchars(addslashes($_POST['day']));
  $heuredeb=htmlspecialchars(addslashes($_POST['heuredeb']));
  $heurefin=htmlspecialchars(addslashes($_POST['heurefin']));
  $Etab=htmlspecialchars(addslashes($_POST['Etab']));

  //ajout de la routine

  $routine->AddRoutine($classe,$Etab,$heuredeb,$heurefin,$matiere,$day);

}if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recupération des variables

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $day=htmlspecialchars(addslashes($_POST['day']));
  $heuredeb=htmlspecialchars(addslashes($_POST['heuredeb']));
  $heurefin=htmlspecialchars(addslashes($_POST['heurefin']));
  $idroutine=htmlspecialchars(addslashes($_POST['idroutine']));
  $Etab=htmlspecialchars(addslashes($_POST['Etab']));

  //modification de la routine

  $routine->UpdateRoutine($classe,$Etab,$heuredeb,$heurefin,$matiere,$day,$idroutine);


}

?>
