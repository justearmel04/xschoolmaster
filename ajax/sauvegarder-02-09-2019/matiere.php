<?php
session_start();
require_once('../class/Matiere.php');
require_once('../class/Etablissement.php');

$matiere= new Matiere();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$matiere=htmlspecialchars(addslashes($_POST['matiere']));
$classe=htmlspecialchars(addslashes($_POST['classe']));
$teatcher=htmlspecialchars(addslashes($_POST['teatcher']));
$codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
$coefficient=htmlspecialchars(addslashes($_POST['coefficient']));

//$check=$matiere->AllreadyExistMatiere();

//$content=$teatcher->getAllTeatchersByschoolCode($codeEtab);

//verifier si la matière existe dejà

//$check=$matiere->AllreadyExistMatiere();

$check=$etab->ExistMatiereAllready($matiere,$classe,$codeEtab);


if($check==0)
{
  //cette matière n'existe pas encore pour cette classe
  $content=0;
}else {
//cette matière existe deja pour cette classe
//nous allons verifier si il est question du meme professeur
$check1=$matiere->ExistMatiereWithSameProf($matiere,$classe,$codeEtab,$teatcher);

if($check1==0)
{
  //il est question d'un nouveau professeur pour cette matière

  $content=1;
}else {
  // il s'agit du meme professeur pour cette matiere
  $content=2;
}

}

echo $content;
}if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recupération des variables

$code=htmlspecialchars(addslashes($_POST['code']));
$classe=htmlspecialchars(addslashes($_POST['classe']));
$content="";

$datas=$etab->getAllsubjectofclassesbyIdclasses($classe,$code);

$nbligne=count($datas);

if($nbligne==0)
{
  $content.="<option selected value=''>Aucune Matière </option>";
}else {
  //var_dump($datas);
  $content.="<option selected value='' ><?php echo L::SelectSubjects ?></option>";

  foreach ($datas as $value):
      $content .= "<option value='". $value->id_mat."' >" . utf8_encode(utf8_decode($value->libelle_mat)). "</option>";
  endforeach;
}

echo $content;

}if(isset($_POST['etape'])&&($_POST['etape']==3))
{
    //recupération des variables

    $classe=htmlspecialchars(addslashes($_POST['classe']));
    $matiere=htmlspecialchars(addslashes($_POST['matiere']));
    $day=htmlspecialchars(addslashes($_POST['day']));
    $heuredeb=htmlspecialchars(addslashes($_POST['heuredeb']));
    $heurefin=htmlspecialchars(addslashes($_POST['heurefin']));
    $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
    $content="";

    //verifier si nous avons deja une routine dans cette période

    $check=$etab->verifyRoutineExistInTimePeriode($heuredeb,$heurefin,$day,$classe,$codeEtab);

    if($check>0)
    {
      //nous avons une matière dans la période choisie

      //nous allons verifier si il est question de la meme matière ou d'une autre matière
      $check1=$etab->verifyRoutineExistwithThisSubject($heuredeb,$heurefin,$day,$classe,$codeEtab,$matiere);

      if($check1>0)
      {
        //il est question de la même matiere
        $content=1;
      }else {
        //il est question d'une nouvelle matière or nous avons une matière a cette période
        $content=2;
      }

    }else {
      // nous n'avons pas de matière dans la période choisie
      $content=0;
    }

echo $content;
}if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //recupération des variables

$code=htmlspecialchars(addslashes($_POST['code']));
$classe=htmlspecialchars(addslashes($_POST['classe']));
$content="";

$datas=$etab->getAllsubjectofclassesbyIdclassesAndTeatcherId($classe,$code);

$nbligne=count($datas);

if($nbligne==0)
{
  $content.="<option selected value=''>Aucune Matière </option>";
}else {
  //var_dump($datas);
  $content.="<option selected value='' ><?php echo L::SelectSubjects ?></option>";

  foreach ($datas as $value):
      $content .= "<option value='". $value->id_mat."-".$value->teatcher_mat."' >" . utf8_encode(utf8_decode($value->libelle_mat)). "</option>";
  endforeach;
}

echo $content;

}if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //recupération des variables

  $code=htmlspecialchars(addslashes($_POST['code']));
  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $content="";

  $datas=$etab->getAllsubjectofclassesbyIdclasses($classe,$code);

  $nbligne=count($datas);

  if($nbligne==0)
  {
  $content.="<option selected value=''>Aucune Matière </option>";
  }else {
  //var_dump($datas);
  $content.="<option selected value=''  ><?php echo L::SelectSubjects ?></option>";

  foreach ($datas as $value):
      $content .= "<option value='". $value->id_mat."-".$value->teatcher_mat."' >". utf8_encode(utf8_decode($value->libelle_mat)). "</option>";
  endforeach;
  }

  echo $content;
}



 ?>
