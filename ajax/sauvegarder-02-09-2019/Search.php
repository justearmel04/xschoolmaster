<?php
session_start();
require_once('../class/Matiere.php');
require_once('../class/Etablissement.php');

$matiere= new Matiere();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
//il est question d"un controle
//recupération des variables

//nous devons rechercher la liste des controles de cette classe

$content="";

$notetype=htmlspecialchars(addslashes($_POST['notetype']));

$classeEtab=htmlspecialchars(addslashes($_POST['classeEtab']));
$codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));

$datas=$etab->getAllControleOfThisClassesOfSchool($notetype,$classeEtab,$codeEtab);

$nbligne=count($datas);

if($nbligne==0)
{
  $content.="<option value=''>".L::Nocontroles." </option>";
}else {
  //var_dump($datas);
  $content.="<option value='' selected>".L::Selectionnecontroles."</option>";

  foreach ($datas as $value):
      $content .= "<option value='".$value->id_ctrl."-".$value->mat_ctrl."-".$value->teatcher_ctrl."' >" .utf8_encode(utf8_decode($value->libelle_ctrl)). "</option>";
  endforeach;
}


/*
$matiere=htmlspecialchars(addslashes($_POST['matiere']));
$classe=htmlspecialchars(addslashes($_POST['classe']));
$teatcher=htmlspecialchars(addslashes($_POST['teatcher']));
$codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
$coefficient=htmlspecialchars(addslashes($_POST['coefficient']));
$datectrl=htmlspecialchars(addslashes($_POST['datectrl']));



$check=$etab->ExistControleAllready($matiere,$classe,$teatcher,$codeEtab,$coefficient,$datectrl);



if($check==0)
{
  //cette matière n'existe pas encore pour cette classe
  $content=0;
}else {
$content=1;

}*/

echo $content;
}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //il est question d"un examen

  $content="";

  $notetype=htmlspecialchars(addslashes($_POST['notetype']));

  $classeEtab=htmlspecialchars(addslashes($_POST['classeEtab']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));

  $datas=$etab->getAllDesignationNotesOfClassesE($notetype,$classeEtab,$codeEtab);

  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>Aucune Désignation </option>";
  }else {
    //var_dump($datas);
    $content.="<option value='' selected>Selectionner une Désignation </option>";

    foreach ($datas as $value):
        $content .= "<option value='".$value->id_exam."' >". utf8_encode(utf8_decode($value->libelle_exam))."</option>";
    endforeach;
  }

  echo $content;
}



 ?>
