<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Etablissement.php');
require_once('../class/Student.php');
require_once('../class/Teatcher.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$student= new Student();
$session= new Sessionacade();
$etabs= new Etab();
$classex= new Classe();
$teatcher= new Teatcher();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
//nous devons publier le cours
//recupération des variables

$content="";

$controleid=htmlspecialchars($_POST['controleid']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);

$datas=$etabs->getAllControlesDetailsOne($codeEtab,$sessionEtab,$controleid);

foreach ($datas as $value):
    $libelle=$value->libelle_ctrl;
    $controledate=date_format(date_create($value->date_ctrl),'d-m-Y');
    $classeid=$value->classe_ctrl;
    $matiereid=$value->mat_ctrl;
    $teatecherid=$value->teatcher_ctrl;
    $coeff=$value->coef_ctrl;
    $periode=$value->typesess_ctrl;
    $libellematiere=utf8_decode($value->libelle_mat);


  endforeach;

  $content=$codeEtab."*".$libelle."*".$controledate."*".$classeid."*".$matiereid."*".$teatecherid."*".$coeff."*".$periode."*".$libellematiere;
  // $content=$codeEtab."*".$libelle;
    echo $content;


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{

  $content="";
  $content1="";
  $content2="";

  $controleid=htmlspecialchars($_POST['controleid']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);

  $datas=$etabs->getAllControlesDetailsOne($codeEtab,$sessionEtab,$controleid);

  foreach ($datas as $value):
      $libelle=$value->libelle_ctrl;
      $controledate=date_format(date_create($value->date_ctrl),'d-m-Y');
      $classeid=$value->classe_ctrl;
      $matiereid=$value->mat_ctrl;
      $teatecherid=$value->teatcher_ctrl;
      $coeff=$value->coef_ctrl;
      $periode=$value->typesess_ctrl;
      $libellematiere=utf8_decode($value->libelle_mat);


    endforeach;


    $datas=$classex->getAllClassesbyschoolCode($codeEtab);

    $nbligne=count($datas);

    if($nbligne==0)
    {
      $content.="<option value=''>Aucune Classe </option>";
    }else {
      //var_dump($datas);
      $content.="<option value=''>Selectionner une classe</option>";

      foreach ($datas as $value):
        if($value->id_classe==$classeid)
        {
          $content .= "<option value='". $value->id_classe ."' selected >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";
        }else {
          $content .= "<option value='". $value->id_classe ."' >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";
        }

      endforeach;
    }


    $data=$etabs->getAllsubjectofclassesbyIdclasses($classeid,$codeEtab,$sessionEtab);

    $nbligne=count($data);

    if($nbligne==0)
    {
      $content1.="<option selected value=''>Aucune Matière </option>";
    }else {
      //var_dump($datas);
      $content1.="<option  value='' ><?php echo L::SelectSubjects ?></option>";

      foreach ($data as $value):

        if($value->id_mat==$matiereid)
        {
        $content1 .= "<option value='". $value->id_mat."' selected>" . utf8_encode(utf8_decode($value->libelle_mat)). "</option>";
        }else {
          $content1 .= "<option value='". $value->id_mat."' >" . utf8_encode(utf8_decode($value->libelle_mat)). "</option>";
        }


      endforeach;
    }


    $datax=$teatcher->getAllTeatcherbyschoolCode($codeEtab);


    $nbligne=count($datax);

    if($nbligne==0)
    {
      $content2.="<option value=''>Aucun Enseignant </option>";
    }else {
      //var_dump($datas);
      $content2.="<option value=''>Selectionner un Enseignant</option>";

      foreach ($datax as $value):
        if($value->id_compte==$teatecherid)
        {
          $content2 .= "<option value='". $value->id_compte ."' selected>" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
        }else {
          $content2 .= "<option value='". $value->id_compte ."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
        }

      endforeach;
    }


echo $content."*".$content1."*".$content2;


}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  $controleid=htmlspecialchars($_POST['controleid']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);

  $datas=$etabs->getAllExamenDetails($codeEtab,$sessionEtab,$controleid);

  foreach ($datas as  $value):
    $libelle=$value->libelle_exam;
    $datedeb=date_format(date_create($value->du_exam),'d-m-Y');
    $datefin=date_format(date_create($value->au_exam),'d-m-Y');
  endforeach;

  echo $libelle."*".$datedeb."*".$datefin;



}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
//modification examens
$typeEvaluation="EXAMENS";
$examid=htmlspecialchars($_POST['examid']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);
$libelleexam=htmlspecialchars($_POST['exammod']);
$datedebmodexa=dateFormat(htmlspecialchars($_POST['datedebmodexa']));
$datefinmodexa=dateFormat(htmlspecialchars($_POST['datefinmodexa']));

$etabs->UpdateEvaluationsExamens($libelleexam,$datedebmodexa,$datefinmodexa,$examid,$codeEtab,$sessionEtab,$typeEvaluation);

$_SESSION['user']['updatesubjectok']=L::EvaluationModOk;


}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
//modification du controle
$typeEvaluation="CONTROLES";
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);
$datecontrole=dateFormat(htmlspecialchars($_POST['datecontrole']));
$coefcontrole=htmlspecialchars($_POST['coefcontrole']);
$libellecontrole=htmlspecialchars($_POST['libellecontrole']);
$controleid=htmlspecialchars($_POST['controleid']);


$etabs->UpdateEvaluationsControles($libellecontrole,$coefcontrole,$datecontrole,$controleid,$codeEtab,$sessionEtab,$typeEvaluation);

$_SESSION['user']['updatesubjectok']=L::EvaluationModOk;





}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  //supprimer un controle

  $typeEvaluation="CONTROLES";
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $controleid=htmlspecialchars($_POST['controleid']);
  $matiereid=htmlspecialchars($_POST['matiereid']);
  $evalid=htmlspecialchars($_POST['evalid']);
  $classeid=htmlspecialchars($_POST['classeid']);

$etabs->DeletedEvaluationsControles($evalid,$typeEvaluation,$controleid,$classeid,$matiereid,$codeEtab,$sessionEtab);


$_SESSION['user']['updatesubjectok']=L::EvaluationDelOk;

}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{
  $typeEvaluation="EXAMENS";
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $evalid=htmlspecialchars($_POST['evalid']);
  $examenid=htmlspecialchars($_POST['examenid']);

  $etabs->DeletedEvaluationsExamens($evalid,$typeEvaluation,$examenid,$codeEtab,$sessionEtab);
  $_SESSION['user']['updatesubjectok']=L::EvaluationDelOk;

}


 ?>
