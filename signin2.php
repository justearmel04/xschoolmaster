<?php 


session_start();

//require_once('class/User.php');

require_once('single/Etablissement.php');

//require_once('class/LocalAdmin.php');
//require_once('class/Parent.php');
//require_once('class/Classe.php');
//require_once('class/Teatcher.php');

//require_once('single/Sessionsacade.php');


$etabs=new Etab();

//$session= new Sessionacade();


$alletabschool=$etabs->getAllEtab();

 ?>


<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>

</head>
<body>

<br><br>
	<div class="col-md-6 col-xs-12 col-md-offset-3">
    <div class="panel panel-default">
        <div class="panel-headin" style="background-color: #009fe3; font-size: 17px;color: white;">  Retrouvez le(s)  enfant(s) dont vous êtes le parent 
	    </div>

<div class="panel-body">


       <form action="index2.php" method="post">


         <div class="row">
				<div class="form-group col-xs-5">
					<label for="matricule"  class="control-label"> Matricule </label>
					<input type="text" class="form-control" id="matricule" name="nom" placeholder="Saisir le matricule de votre enfant ici " />	
				</div> 


				 <div class="form-group col-xs-7">
		                <label class="control-label"> Etablissement </label>
		                        
		                <select class="form-control" name="etablissement" id="etablissement" required="required" onchange="determinatestudent()">    
		                    <option value="">Selectionner un etablissement</option>
                                          <?php
                                            $i=1;
                                            foreach ($alletabschool as $value):
                                            ?>
                                             <option value="<?php echo $value->code_etab; ?>"><?php echo utf8_encode(utf8_decode($value->libelle_etab)); ?>
                                             	
                                             </option>

                                                <?php
                                                     $i++;
                                                     endforeach;
                                                 ?>           
                         </select>


	            </div>


	        </div>


	         <div class="form-group">
	               <label class="control-label"> Nom de l'éleve  </label>
                   <input type="text" class="form-control" name="nom"  id="nom" value="" readonly />
		     </div>


		      <div class="form-group">
                   <label class="control-label"> Prénom de l'éleve  </label>
                   <input type="text" class="form-control" name="prenom" id="prenom" readonly/>
		      </div>

		      <div class="form-group">
                   <label class="control-label"> Classe  </label>
                   <input type="text" class="form-control" name="classe" id="classe" readonly/>
		      </div>


		

		</form> 

</div>

</div>

</div> 

<script src="assets2/plugins/jquery/jquery.min.js" ></script>

<script type="text/javascript">
	
	function determinatestudent(){

   var matricule=$("#matricule").val();
   var codeEtab=$("#etablissement").val();
   var etape=1;
   
  $.ajax({

        url: 'ajax/determination.php',
        type: 'POST',
        async:true,
         data: 'codeEtab='+codeEtab+'&etape='+etape+'&matricule='+matricule,
        dataType: 'text',
        success: function (response, statut) {

         if(response==0)
       {
         //cet eleve n'existe pas ou n'est pas inscrit pour la session encours
       }else if(response!=0) {
         var tabdata=response.split("*");
         //nous pouvons recuperer de facon individuelle les variables nom,prenoms et classe

         var nom=tabdata[0];
         var prenom=tabdata[1];
         var classe=tabdata[2];
		 
		
			
		
		 $("#nom").val(nom);

		 $("#prenom").val(prenom);
		  
		 $("#classe").val(classe);
         
       }

        }
      });
	}

</script>



</body>
</html>