<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Teatcher.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$userId=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$matiere=new Matiere();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$datasinscriptions=$etabs->getCodeEtabOfStudentInscript($compteuserid);
foreach ($datasinscriptions as $datasinscription):
  $codeEtabInscript=$datasinscription->codeEtab_inscrip;
  $sessionEtabInscript=$datasinscription->session_inscrip;
  $classeidInscript=$datasinscription->idclasse_inscrip;
endforeach;
$_SESSION['user']['session']=$sessionEtabInscript;

$_SESSION['user']['codeEtab']=$codeEtabInscript;
//recuperer tous les cntrole de cet etablissement

$codeEtabAssigner=$codeEtabInscript;
$codeEtabLocal=$codeEtabInscript;

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabLocal);

if($etablissementType==1||$etablissementType==3)
{

  if($nbsessionOn>0)
  {
    //recuperer la session en cours
    $sessionencours=$session->getSessionEncours($codeEtabLocal);
    $tabsessionencours=explode("*",$sessionencours);
    $libellesessionencours=$tabsessionencours[0];
    $sessionencoursid=$tabsessionencours[1];
    $typesessionencours=$tabsessionencours[2];
    // $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
    // $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabLocal,$libellesessionencours);
    // $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabLocal,$libellesessionencours);
    $controles=$matiere->getAllControleMatiereOfThisSchool($codeEtabLocal,$libellesessionencours);
  }


}else {
  if($nbsessionOn>0)
  {
    //recuperer la session en cours
    $sessionencours=$session->getSessionEncours($codeEtabLocal);
    $tabsessionencours=explode("*",$sessionencours);
    $libellesessionencours=$tabsessionencours[0];
    $sessionencoursid=$tabsessionencours[1];
    $typesessionencours=$tabsessionencours[2];
    $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
    $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabLocal,$libellesessionencours);
    $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabLocal,$libellesessionencours);
    $controles=$matiere->getAllControleMatiereOfThisSchool($codeEtabLocal,$libellesessionencours);
  }
}





 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
        <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
  <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title"><?php echo L::ControlsManagements ?></div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">

                          <li><a class="parent-item" href="#"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>

                          <li class="active"><?php echo L::ControlsMenu ?></li>
                      </ol>
                  </div>
              </div>

              <?php

                    if(isset($_SESSION['user']['addctrleok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
              <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
              <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['addctrleok']; ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addctrleok']);
                    }

                     ?>




              <?php

                    if(isset($_SESSION['user']['deletesubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['deletesubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'OK',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })


                </script>
                      <?php
                      unset($_SESSION['user']['deletesubjectok']);
                    }

                     ?>


              <?php

                    if(isset($_SESSION['user']['updatesubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['updatesubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'OK',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })


                </script>
                      <?php
                      unset($_SESSION['user']['updatesubjectok']);
                    }

                     ?>
              <?php

                    if(isset($_SESSION['user']['addsubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>


                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['addsubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'OK',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })
                </script>
                      <?php
                      unset($_SESSION['user']['addsubjectok']);
                    }

                     ?>


                     <div class="col-md-12 col-sm-12">
                                   <div class="panel tab-border card-box">
                                       <header class="panel-heading panel-heading-gray custom-tab ">
                                           <ul class="nav nav-tabs">
                                               <li class="nav-item"><a href="#home" data-toggle="tab" class="active"><i class="fa fa-bars"></i> <?php echo L::ControlLists ?></a>
                                               </li>




                                           </ul>
                                       </header>
                                       <div class="panel-body">
                                           <div class="tab-content">
                                               <div class="tab-pane active" id="home">
                                                 <div class="row">
                        <div class="col-md-12">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header></header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">

                                    <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                        <thead>
                                            <tr>

                                                <!-- <th> Classe </th> -->
                                                <th> <?php echo L::MatiereMenusingle ?></th>
                                                <th> <?php echo L::ProfsMenusingle ?> </th>
                                                <th> <?php echo L::ControlsMenu ?> </th>
                                                <th> <?php echo L::LittleDateVersements ?> </th>
                                                  <th> <?php echo L::Coefs ?></th>
                                                <!-- <th> <?php echo L::Actions?> </th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                            //$matieres

                                            $i=1;
                                              foreach ($controles as $value):
                                           ?>
                                            <tr class="odd gradeX">

                                                <!-- <td> <?php //echo $value->libelle_classe;?></td> -->
                                                <td>
                                                    <?php echo $value->libelle_mat;?>
                                                </td>
                                                <td>
                                                  <?php echo $value->nom_compte." ".$value->prenom_compte;?>
                                                </td>
                                                <td>
                                                  <?php echo $value->libelle_ctrl;?>
                                                </td>
                                                <td>
                                                  <?php echo date_format(date_create($value->date_ctrl), "d/m/Y");?>
                                                </td>
                                                <td>
                                                  <?php echo $value->coef_ctrl;?>
                                                </td>


                                                <div class="modal fade" id="exampleModal<?php echo $value->id_mat;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel<?php echo $value->id_mat;?>" aria-hidden="true">
                                                					    <div class="modal-dialog" role="document">
                                                					        <div class="modal-content">
                                                					            <div class="modal-header">
                                                					                <h4 class="modal-title" id="exampleModalLabel<?php echo $value->id_mat;?>"><?php echo L::ModificationControle ?></h4>
                                                					                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                					                    <span aria-hidden="true">&times;</span>
                                                					                </button>
                                                					            </div>
                                                					            <div class="modal-body">
                                                                        <form  id="FormUpdateControle<?php echo $value->id_mat;?>" class="form-horizontal" action="../controller/controle.php" method="post">
                                                                            <div class="form-body">
                                                                              <div class="form-group row">
                                                                                      <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                                                                          <span class="required"> * </span>
                                                                                      </label>
                                                                                      <div class="col-md-5">
                                                                                          <select class="form-control " name="classe<?php echo $value->id_ctrl;?>" id="classe<?php echo $value->id_ctrl;?>"  style="width:171%" onchange="searchmatiereT(<?php echo $value->id_ctrl;?>)">
                                                                                              <option value=""><?php echo L::Selectclasses ?></option>
                                                                                              <?php
                                                                                              $i=1;
                                                                                                foreach ($classes as $valueClasses):
                                                                                                ?>
                                                                                                <option   <?php if($valueClasses->id_classe==$value->classe_ctrl){echo "selected";}?>  value="<?php echo utf8_encode(utf8_decode($valueClasses->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($valueClasses->libelle_classe)); ?></option>

                                                                                                <?php
                                                                                                                                 $i++;
                                                                                                                                 endforeach;
                                                                                                                                 ?>

                                                                                          </select>
                                                                                          <p id="messageClas<?php echo $value->id_ctrl;?>"></p>
                                                                                  </div>
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                        <label class="control-label col-md-3"><?php echo L::MatiereMenusingle ?>
                                                                                            <span class="required"> * </span>
                                                                                        </label>
                                                                                        <div class="col-md-5">
                                                                                            <!--input type="text" name="matiere" id="matiere" data-required="1" placeholder="Entrer la classe" class="form-control input-height" /-->
                                                                                            <select class="form-control " name="matiere<?php echo $value->id_ctrl;?>" id="matiere<?php echo $value->id_ctrl;?>"  style="width:171%" onchange='searchprofesseurT(<?php echo $value->id_ctrl;?>)'>
                                                                                                <option value=""><?php echo L::SelectSubjects ?></option>
                                                                                                <option selected value="<?php echo $value->id_mat;?>"><?php echo $value->libelle_mat;?></option>

                                                                                            </select>
                                                                                            <p id="messageMat<?php echo $value->id_ctrl;?>"></p>
                                                                                          </div>

                                                                                 </div>
                                                                                 <div class="form-group row">
                                                                                         <label class="control-label col-md-3"><?php echo L::ProfsMenusingle ?>
                                                                                             <span class="required"> * </span>
                                                                                         </label>
                                                                                         <div class="col-md-5">

                                                                                           <select class="form-control " id="teatcher<?php echo $value->id_ctrl;?>" name="teatcher<?php echo $value->id_ctrl;?>" style="width:171%" >
                                                                                               <option value=""><?php echo L::TeatcherSelected ?></option>
                                                                                               <?php
                                                                                               $i=1;
                                                                                                 foreach ($teatchers as $valueTea):
                                                                                                 ?>
                                                                                                 <option <?php if($value->id_compte==$valueTea->id_compte){echo "selected";}?>   value="<?php echo $valueTea->id_compte?>"><?php echo utf8_encode(utf8_decode($valueTea->nom_compte." - ".$valueTea->prenom_compte)) ?></option>

                                                                                                 <?php
                                                                                                                                  $i++;
                                                                                                                                  endforeach;
                                                                                                                                  ?>

                                                                                           </select>
                                                                                           <p id="messageProf<?php echo $value->id_ctrl;?>"></p>

                                                                                           <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabLocal; ?>"/>
                                                                                        </div>
                                                                                     </div>
                                                                                     <div class="form-group row">
                                                                                             <label class="control-label col-md-3"><?php echo L::LibelleControle ?>
                                                                                                 <span class="required"> * </span>
                                                                                             </label>
                                                                                             <div class="col-md-5">
                                                                                                 <input type="text" name="controle<?php echo $value->id_ctrl;?>" id="controle<?php echo $value->id_ctrl;?>" value="<?php echo $value->libelle_ctrl;?>" data-required="1" placeholder="Entrer la classe" class="form-control " style="width:171%" />
                                                                                                 <input type="hidden" name="etape" id="etape" value="7"/>
                                                                                                 <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabLocal; ?>"/>
                                                                                                 <input type="hidden" name="idctrl" id="idctrl" value="<?php echo $value->id_ctrl; ?>"/>
                                                                                                 <p id="messageCont<?php echo $value->id_ctrl;?>"></p>
                                                                                               </div>

                                                                                      </div>

                                                                                      <div class="form-group row">
                                                                                              <label class="control-label col-md-3"><?php echo L::Coefs ?>
                                                                                                  <span class="required"> * </span>
                                                                                              </label>
                                                                                              <div class="col-md-5">
                                                                                                  <input type="number" min="1" value="<?php echo $value->coef_ctrl;?>" name="coef<?php echo $value->id_ctrl;?>" id="coef<?php echo $value->id_ctrl;?>" data-required="1" placeholder="<?php echo L::EnterCoefSubjectControlLib ?>" class="form-control " style="width:171%"/>
                                                                                                  <p id="messageCoef<?php echo $value->id_ctrl;?>"></p>
                                                                                                </div>
                                                                                       </div>
                                                                                       <div class="form-group row">
                                                                                         <label class="control-label col-md-3"><?php echo L::DatecontolsTea ?>
                                                                                             <span class="required"> * </span>
                                                                                         </label>
                                                                                             <div class="col-md-5">
                                                                                                 <input type="text"  placeholder="<?php echo L::DatecontolsTea ?>" value="<?php echo date_format(date_create($value->date_ctrl), "d/m/Y");?>" name="datectrl<?php echo $value->id_ctrl;?>" id="datectrl<?php echo $value->id_ctrl;?>" data-mask="99/99/9999" class="form-control " style="width:171%">
                                                                                                   <span class="help-block"><?php echo L::Datesymbole ?></span>
                                                                                                   <p id="messageDate<?php echo $value->id_ctrl;?>"></p>
                                                                                             </div>
                                                                                         </div>




                                                          <div class="form-actions">
                                                                                <div class="row">
                                                                                    <div class="offset-md-3 col-md-9">

                                                                                          <button type="submit" onclick="check(<?php echo $value->id_ctrl;?>)" class="btn btn-info">Modifier</button>
                                                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                                                                                    </div>
                                                                                  </div>
                                                                               </div>
                                                        </div>
                                                                        </form>
                                                					            </div>

                                                					        </div>
                                                					    </div>
                                                					</div>

                                                <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                                <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
                                                <!-- <script src="../assets2/plugins/select2/js/select2.js" ></script>
                                                <script src="../assets2/js/pages/select2/select2-init.js" ></script> -->
                                                <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
                                                <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                                <script>

                                                function soumettre()
                                                {
                                                  $("#FormUpdateControle<?php echo $value->id_mat;?>").submit();
                                                }

                                                function myFunction(idcompte)
                                                {
                                                  //var url="detailslocal.php?compte="+idcompte;
                                                document.location.href="detailsadmin.php?compte="+idcompte;
                                                }

                                                function erasedMat(id)
                                                {
                                                  document.getElementById("messageMat"+id).innerHTML = "";

                                                }

                                                function erasedCoef(id)
                                                {
                                                  document.getElementById("messageCoef"+id).innerHTML = "";
                                                }

                                                function erasedClasse(id)
                                                {
                                                  document.getElementById("messageClasse"+id).innerHTML = "";
                                                }

                                                function erasedTeatcher(id)
                                                {
                                                  document.getElementById("messageTeatcher"+id).innerHTML = "";
                                                }

                                                function modify(id)
                                                {


                                                  Swal.fire({
                                    title: '<?php echo L::WarningLib ?>',
                                    text: "<?php echo L::DoyouReallyModifyingSubjects ?>",
                                    type: 'warning',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: '<?php echo L::ModifierBtn ?>',
                                    cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                  }).then((result) => {
                                    if (result.value) {
                                      document.location.href="updatesubject.php?compte="+id;
                                    }else {

                                    }
                                  })
                                                }

                                                function deleted(id,controleid)
                                                {
                                                  var classe="<?php echo $value->classe_mat;?>";
                                                  var codeEtab="<?php echo $codeEtabAssigner;?>";
                                                  // var controleid="<?php //echo $value->id_ctrl;?>";
                                                  Swal.fire({
                                    title: '<?php echo L::WarningLib ?>',
                                    text: "Voulez vous vraiment supprimer ce contrôle",
                                    type: 'warning',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: '<?php echo L::DeleteLib ?>',
                                    cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                  }).then((result) => {
                                    if (result.value) {
                                      document.location.href="../controller/controle.php?etape=3&matiere="+id+"&classe="+classe+"&codeEtab="+codeEtab+"&controleid="+controleid;
                                    }else {

                                    }
                                  })
                                                }

                                                function check(id)
                                                {
                                                  //recuperation des variables
                                                  var matiere=$("#matiere"+id).val();
                                                  var professeur=$("#teatcher"+id).val();
                                                  var controle=$("#controle"+id).val();
                                                  var coefficient=$("#coef"+id).val();
                                                  var classe=$("#classe"+id).val();
                                                  var codeEtab=$("#codeEtab").val();
                                                  var idctrl=$("#idctrl").val();
                                                  var datectrl=$("#datectrl"+id).val();

                                                  //alert("bonjour");

                                                  event.preventDefault();


                                                  if(matiere==""||coefficient==""||classe==""||professeur==""||datectrl==""||controle=="")
                                                  {
                                                    if(controle=="")
                                                    {
                                                       document.getElementById("messageCont"+id).innerHTML = "<font color=\"red\">Merci de renseigner le libellé contrôle !</font>";
                                                    }

                                                    if(datectrl=="")
                                                    {
                                                       document.getElementById("messageDate"+id).innerHTML = "<font color=\"red\">Merci de renseigner la Date !</font>";
                                                    }

                                                    if(matiere=="")
                                                    {
                                                       document.getElementById("messageMat"+id).innerHTML = "<font color=\"red\">Merci de selectionner la Matière !</font>";
                                                    }

                                                    if(coefficient=="")
                                                    {
                                                      document.getElementById("messageCoef"+id).innerHTML = "<font color=\"red\">Merci de renseigner le Coefficient de la Matière !</font>";
                                                    }

                                                    if(classe=="")
                                                    {
                                                      document.getElementById("messageClas"+id).innerHTML = "<font color=\"red\">Merci de selectionner la Classe !</font>";
                                                    }

                                                    if(professeur=="")
                                                    {
                                                      document.getElementById("messageProf"+id).innerHTML = "<font color=\"red\">Merci de selectionner le Professeur !</font>";
                                                    }




                                                }else {

                                                      if(coefficient==0)
                                                      {
                                                          document.getElementById("messageCoef"+id).innerHTML = "<font color=\"red\">Le Coefficient de la Matière doit être supérieur ou égale à 1 !</font>";
                                                      }else if(coefficient>=0 && professeur!="" && classe!="" && matiere!="") {
                                                        soumettre();
                                                      }
                                                }

                                                }

                                                function searchmatiereT(id)
                                                {
                                                    var codeEtab="<?php echo $codeEtabLocal;?>";
                                                    var classe=$("#classe"+id).val();
                                                    var etape=2;
                                                     $.ajax({

                                                       url: '../ajax/matiere.php',
                                                       type: 'POST',
                                                       async:true,
                                                       data: 'code=' + codeEtab+ '&etape=' + etape+'&classe='+classe,
                                                       dataType: 'text',
                                                       success: function (content, statut) {

                                                         $("#matiere"+id).html("");
                                                         $("#matiere"+id).html(content);

                                                       }
                                                     });
                                                }

                                                function searchprofesseurT(id)
                                                {
                                                  var codeEtab="<?php echo $codeEtabLocal;?>";
                                                  var classe=$("#classe"+id).val();
                                                  var matiere=$("#matiere"+id).val();
                                                  var etape=4;
                                                  $.ajax({

                                                    url: '../ajax/teatcher.php',
                                                    type: 'POST',
                                                    async:true,
                                                    data: 'code=' + codeEtab+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
                                                    dataType: 'text',
                                                    success: function (content, statut) {

                                                      // alert(content);
                                                      $("#teatcher"+id).html("");
                                                      $("#teatcher"+id).html(content);

                                                    }
                                                  });
                                                }



















                                                </script>
                                            </tr>


                                            <?php
                                            $i++;
                                            endforeach;
                                            ?>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                                               </div>

                      <?php
                      if($etablissementType==1||$etablissementType==3)
                      {
                       ?>
                       <div class="tab-pane" id="about1">
                         <?php
                         if($nbsessionOn>0)
                         {
                          ?>
                          <div class="row">
                              <div class="col-md-12 col-sm-12">
                                  <div class="card card-box">
                                      <div class="card-head">
                                          <header></header>

                                      </div>

                                      <div class="card-body" id="bar-parent">
                                          <form  id="FormAddCtrl1" class="form-horizontal" action="../controller/controle.php" method="post">
                                              <div class="form-body">
                                                <div class="form-group row">
                                                        <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-5">
                                                            <select class="form-control input-height" name="classe" id="classe"  style="width:100%" onchange="searchmatiere()">
                                                                <option value=""><?php echo L::Selectclasses ?></option>
                                                                <?php
                                                                $i=1;
                                                                  foreach ($classes as $value):
                                                                  ?>
                                                                  <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                                  <?php
                                                                                                   $i++;
                                                                                                   endforeach;
                                                                                                   ?>

                                                            </select>
                                                    </div>
                                                  </div>
                                                <div class="form-group row">
                                                        <label class="control-label col-md-3"><?php echo L::MatiereMenusingle ?>
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-5">
                                                            <!--input type="text" name="matiere" id="matiere" data-required="1" placeholder="Entrer la classe" class="form-control input-height" /-->
                                                            <select class="form-control input-height" name="matiere" id="matiere"  style="width:100%" onchange='searchprofesseur()'>
                                                                <option value=""><?php echo L::SelectSubjects ?></option>

                                                            </select>
                                                          </div>

                                                 </div>
                                                 <div class="form-group row">
                                                         <label class="control-label col-md-3"><?php echo L::ProfsMenusingle ?>
                                                             <span class="required"> * </span>
                                                         </label>
                                                         <div class="col-md-5">

                                                           <select class="form-control input-height" id="teatcher" name="teatcher" style="width:100%" >
                                                               <option value=""><?php echo L::TeatcherSelected ?></option>
                                                               <?php
                                                               $i=1;
                                                                 foreach ($teatchers as $value):
                                                                 ?>
                                                                 <option value="<?php echo $value->id_compte?>"><?php echo utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)) ?></option>

                                                                 <?php
                                                                                                  $i++;
                                                                                                  endforeach;
                                                                                                  ?>

                                                           </select>
                                                           <input type="hidden" name="etape" id="etape" value="4"/>
                                                           <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabLocal; ?>"/>
                                                             <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                                        </div>
                                                     </div>

                                                 <div class="form-group row">
                                                         <label class="control-label col-md-3"><?php echo L::LibelleControle ?>
                                                             <span class="required"> * </span>
                                                         </label>
                                                         <div class="col-md-5">
                                                             <input type="text" name="controle" id="controle" data-required="1" placeholder="Entrer la classe" class="form-control input-height" />

                                                           </div>

                                                  </div>
                                                 <div class="form-group row">
                                                         <label class="control-label col-md-3"><?php echo L::Coefs ?>
                                                             <span class="required"> * </span>
                                                         </label>
                                                         <div class="col-md-5">
                                                             <input type="number" min="1" name="coef" id="coef" data-required="1" placeholder="<?php echo L::EnterCoefSubjectControlLib ?>" class="form-control input-height" /> </div>
                                                  </div>
                                                  <div class="form-group row">
                                                    <label class="control-label col-md-3"><?php echo L::DatecontolsTea ?>
                                                        <span class="required"> * </span>
                                                    </label>
                                                        <div class="col-md-5">
                                                            <input type="date"  placeholder="<?php echo L::DatecontolsTea ?>" name="datectrl" id="datectrl"  class="form-control input-height">
                                                              <span class="help-block"><?php echo L::Datesymbole ?></span>
                                                        </div>
                                                    </div>







                            <div class="form-actions">
                                                  <div class="row">
                                                      <div class="offset-md-3 col-md-9">

                                                          <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                                          <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                      </div>
                                                    </div>
                                                 </div>
                          </div>
                                          </form>
                                      </div>
                                  </div>
                              </div>

                          </div>


                          <?php
                        }
                          ?>



                        </div>


    <?php
  }else {
    ?>
    <div class="tab-pane" id="about">
      <?php
      if($nbsessionOn>0)
      {
       ?>
       <div class="row">
           <div class="col-md-12 col-sm-12">
               <div class="card card-box">
                   <div class="card-head">
                       <header></header>

                   </div>

                   <div class="card-body" id="bar-parent">
                       <form  id="FormAddCtrl" class="form-horizontal" action="../controller/controle.php" method="post">
                           <div class="form-body">
                             <div class="form-group row">
                                     <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                         <span class="required"> * </span>
                                     </label>
                                     <div class="col-md-5">
                                         <select class="form-control " name="classe" id="classe"  style="width:100%" onchange="searchmatiere()">
                                             <option value=""><?php echo L::Selectclasses ?></option>
                                             <?php
                                             $i=1;
                                               foreach ($classes as $value):
                                               ?>
                                               <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                               <?php
                                                                                $i++;
                                                                                endforeach;
                                                                                ?>

                                         </select>
                                 </div>
                               </div>
                             <div class="form-group row">
                                     <label class="control-label col-md-3"><?php echo L::MatiereMenusingle ?>
                                         <span class="required"> * </span>
                                     </label>
                                     <div class="col-md-5">
                                         <!--input type="text" name="matiere" id="matiere" data-required="1" placeholder="Entrer la classe" class="form-control input-height" /-->
                                         <select class="form-control input-height" name="matiere" id="matiere"  style="width:100%" onchange='searchprofesseur()'>
                                             <option value=""><?php echo L::SelectSubjects ?></option>

                                         </select>
                                       </div>

                              </div>
                              <div class="form-group row">
                                      <label class="control-label col-md-3"><?php echo L::ProfsMenusingle ?>
                                          <span class="required"> * </span>
                                      </label>
                                      <div class="col-md-5">

                                        <select class="form-control " id="teatcher" name="teatcher" style="width:100%" >
                                            <option value=""><?php echo L::TeatcherSelected ?></option>
                                            <?php
                                            $i=1;
                                              foreach ($teatchers as $value):
                                              ?>
                                              <option value="<?php echo $value->id_compte?>"><?php echo utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)) ?></option>

                                              <?php
                                                                               $i++;
                                                                               endforeach;
                                                                               ?>

                                        </select>
                                        <input type="hidden" name="etape" id="etape" value="6"/>
                                        <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabLocal; ?>"/>
                                          <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                     </div>
                                  </div>
                                  <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::Period ?>
                                              <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-5">

                                            <select class="form-control " id="typesess" name="typesess" style="width:100%" >
                                                <option value=""><?php echo L::SelectedPeriod ?></option>
                                                <?php
                                                $i=1;
                                                  foreach ($typesemestre as $value):
                                                  ?>
                                                  <option value="<?php echo $value->id_semes?>"><?php echo utf8_encode(utf8_decode($value->libelle_semes)) ?></option>

                                                  <?php
                                                                                   $i++;
                                                                                   endforeach;
                                                                                   ?>

                                            </select>

                                         </div>
                                      </div>
                              <div class="form-group row">
                                      <label class="control-label col-md-3"><?php echo L::LibelleControle ?>
                                          <span class="required"> * </span>
                                      </label>
                                      <div class="col-md-5">
                                          <input type="text" name="controle" id="controle" data-required="1" placeholder="<?php echo L::EnterControlLib ?>" class="form-control" />

                                        </div>

                               </div>
                              <div class="form-group row">
                                      <label class="control-label col-md-3"><?php echo L::Coefs ?>
                                          <span class="required"> * </span>
                                      </label>
                                      <div class="col-md-5">
                                          <input type="number" min="1" name="coef" id="coef" data-required="1" placeholder="<?php echo L::EnterCoefSubjectControlLib ?>" class="form-control" /> </div>
                               </div>
                               <div class="form-group row">
                                 <label class="control-label col-md-3"><?php echo L::DatecontolsTea ?>
                                     <span class="required"> * </span>
                                 </label>
                                     <div class="col-md-5">
                                         <input type="text"  placeholder="<?php echo L::DatecontolsTea ?>" data-mask="99/99/9999"  name="datectrl" id="datectrl"  class="form-control">
                                           <span class="help-block"><?php echo L::Datesymbole ?></span>
                                     </div>
                                 </div>







         <div class="form-actions">
                               <div class="row">
                                   <div class="offset-md-3 col-md-9">

                                       <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                       <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                   </div>
                                 </div>
                              </div>
       </div>
                       </form>
                   </div>
               </div>
           </div>

       </div>


       <?php
     }
       ?>



     </div>
    <?php
  }
     ?>





                                           </div>
                                       </div>
                                   </div>
                               </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>

   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

 <script>

 function addFrench()
 {
   var etape=1;
   var lang="fr";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function addEnglish()
 {
   var etape=1;
   var lang="en";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function searchmatiere()
 {
     var codeEtab="<?php echo $codeEtabLocal;?>";
     var classe=$("#classe").val();
     var etape=2;
      $.ajax({

        url: '../ajax/matiere.php',
        type: 'POST',
        async:true,
        data: 'code=' + codeEtab+ '&etape=' + etape+'&classe='+classe,
        dataType: 'text',
        success: function (content, statut) {

          $("#matiere").html("");
          $("#matiere").html(content);

        }
      });
 }

 function searchprofesseur()
 {
   var codeEtab="<?php echo $codeEtabLocal;?>";
   var classe=$("#classe").val();
   var matiere=$("#matiere").val();
   var etape=4;
   $.ajax({

     url: '../ajax/teatcher.php',
     type: 'POST',
     async:true,
     data: 'code=' + codeEtab+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
     dataType: 'text',
     success: function (content, statut) {

       // alert(content);
       $("#teatcher").html("");
       $("#teatcher").html(content);

     }
   });
 }

 jQuery(document).ready(function() {




$("#classe").select2();
$("#teatcher").select2();
$("#classeEtab").select2();
$("#matiere").select2();
$("#typesess").select2();


   $("#FormAddCtrl").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

        // matiere:"required",
        // classe:"required",
        // teatcher:"required",
        // coef:"required",
        classe:"required",
        matiere:"required",
        controle:"required",
        coef:"required",
        teatcher:"required",
        datectrl:"required",
        typesess:"required"


      },
      messages: {
        // matiere:"Merci de renseigner la matière",
        // classe:"<?php echo L::PleaseSelectclasserequired ?>",
        // teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
        // coef:"Merci de renseigner le coefficient de la matière"
        classe:"<?php echo L::PleaseSelectclasserequired ?>",
        matiere:"<?php echo L::PleaseselectSubjects ?>",
        controle:"<?php echo L::Controlsrequired ?>",
        coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
        teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
          datectrl:"<?php echo L::PleaseEnterDateControls ?>",
          typesess:"Merci de selectionner le type de session"


      },
      submitHandler: function(form) {


// nous allons verifier un controle similaire n'existe pas
        var etape=1;

         $.ajax({
           url: '../ajax/controle.php',
           type: 'POST',
           async:true,
           data: 'matiere=' + $("#matiere").val()+ '&etape=' + etape+'&classe='+$("#classe").val()+'&teatcher='+$("#teatcher").val()+'&codeEtab='+$("#codeEtab").val()+'&coefficient='+$("#coef").val()+'&datectrl='+$("#datectrl").val(),
           dataType: 'text',
           success: function (content, statut) {


             if(content==0)
             {
               //cette matière n'existe pas encore pour cette classe

               form.submit();

             }else if(content==1)
             {
               //il est question d'un nouveau professeur pour cette matière
               Swal.fire({
               type: 'warning',
               title: '<?php echo L::WarningLib ?>',
               text: '<?php echo L::ControlAllreadyExist ?>',

               })

             }

           }
         });

             }


           });

           $("#FormAddCtrl1").validate({

             errorPlacement: function(label, element) {
             label.addClass('mt-2 text-danger');
             label.insertAfter(element);
           },
           highlight: function(element, errorClass) {
             $(element).parent().addClass('has-danger')
             $(element).addClass('form-control-danger')
           },
           success: function (e) {
                 $(e).closest('.control-group').removeClass('error').addClass('info');
                 $(e).remove();
             },
              rules:{

                // matiere:"required",
                // classe:"required",
                // teatcher:"required",
                // coef:"required",
                classe:"required",
                matiere:"required",
                controle:"required",
                coef:"required",
                teatcher:"required",
                datectrl:"required",
                typesess:"required"


              },
              messages: {
                // matiere:"Merci de renseigner la matière",
                // classe:"<?php echo L::PleaseSelectclasserequired ?>",
                // teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
                // coef:"Merci de renseigner le coefficient de la matière"
                classe:"<?php echo L::PleaseSelectclasserequired ?>",
                matiere:"<?php echo L::PleaseselectSubjects ?>",
                controle:"<?php echo L::Controlsrequired ?>",
                coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
                teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
                  datectrl:"<?php echo L::PleaseEnterDateControls ?>",
                  typesess:"Merci de selectionner le type de session"


              },
              submitHandler: function(form) {


        // nous allons verifier un controle similaire n'existe pas
                var etape=1;

                 $.ajax({
                   url: '../ajax/controle.php',
                   type: 'POST',
                   async:true,
                   data: 'matiere=' + $("#matiere").val()+ '&etape=' + etape+'&classe='+$("#classe").val()+'&teatcher='+$("#teatcher").val()+'&codeEtab='+$("#codeEtab").val()+'&coefficient='+$("#coef").val()+'&datectrl='+$("#datectrl").val(),
                   dataType: 'text',
                   success: function (content, statut) {


                     if(content==0)
                     {
                       //cette matière n'existe pas encore pour cette classe

                       form.submit();

                     }else if(content==1)
                     {
                       //il est question d'un nouveau professeur pour cette matière
                       Swal.fire({
                       type: 'warning',
                       title: '<?php echo L::WarningLib ?>',
                       text: '<?php echo L::ControlAllreadyExist ?>',

                       })

                     }

                   }
                 });

                     }


                   });
      });






 </script>
    <!-- end js include path -->
  </body>

</html>
