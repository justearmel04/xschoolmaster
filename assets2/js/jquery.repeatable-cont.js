(function ($) {

	$.fn.repeatable = function (userSettings) {

		/**
		 * Default settings
		 * @type {Object}
		 */
		var defaults = {
			addTrigger: "add",
			deleteTrigger: "delete",
			max: null,
      min: 1,
			template: null,
			itemContainer: ".field-group",
			beforeAdd: function () {},
			afterAdd: function (item) {},
			beforeDelete: function (item) {},
			afterDelete: function () {}
		};

		var defaults1 = {
			addTrigger: "addcont",
			deleteTrigger: "deletecont",
			max: null,
      min: 1,
			template: null,
			itemContainer: ".field1-group",
			beforeAdd: function () {},
			afterAdd: function (item) {},
			beforeDelete: function (item) {},
			afterDelete: function () {}
		};

		/**
		 * Iterator used to make each added
		 * repeatable element unique
		 * @type {Number}
		 */
		var i = 1;

		/**
		 * DOM element into which repeatable
		 * items will be added
		 * @type {jQuery object}
		 */
		var target = $(this);

		/**
		 * Blend passed user settings with defauly settings
		 * @type {array}
		 */
		var settings = $.extend({}, defaults, userSettings);



		/**
		 * Total templated items found on the page
		 * at load. These may be created by server-side
		 * scripts.
		 * @return null
		 */
		var total = function () {
			return $(target).find(settings.itemContainer).length;
		}();




		/**
		 * Add an element to the target
		 * and call the callback function
		 * @param  object e Event
		 * @return null
		 */
		var addOne = function (e) {
			var nb=$("#nb").val();
			var nouveau= parseInt(nb)+1;

			$("#nb").val(nouveau);

			// alert(nouveau);
			e.preventDefault();
			settings.beforeAdd.call(this);
			var item = createOne();
			settings.afterAdd.call(this, item);
		};



		/**
		 * Delete the parent element
		 * and call the callback function
		 * @param  object e Event
		 * @return null
		 */
		var deleteOne = function (e) {

			var nb=$("#nb").val();
			var nouveau= parseInt(nb)-1;

			$("#nb").val(nouveau);

			// alert(nouveau);
			e.preventDefault();
			if (total === settings.min) return;
			var item = $(this).parents(settings.itemContainer).first();
			settings.beforeDelete.call(this, item);
			item.remove();
			total--;
			maintainAddBtn();
			settings.afterDelete.call(this);
		};



		/**
		 * Add an element to the target
		 * @return null
		 */
		var createOne = function() {
			var item = getUniqueTemplate();
			item.appendTo(target);
			total++;
			maintainAddBtn();
			return item;
		};



		/**
		 * Alter the given template to make
		 * each form field name unique
		 * @return {jQuery object}
		 */
		var getUniqueTemplate = function () {
			var template = $(settings.template).html();
			// template = template.replace(/{\?}/g, "new" + i++); 	// {?} => iterated placeholder
			template = template.replace(/{\?}/g, i++);
			template = template.replace(/\{[^\?\}]*\}/g, ""); 	// {valuePlaceholder} => ""
			return $(template);
		};



		/**
		 * Determines if the add trigger
		 * needs to be disabled
		 * @return null
		 */
		var maintainAddBtn = function () {
			if (!settings.max) {
				return;
			}

			if (total === settings.max) {
				$(settings.addTrigger).attr("disabled", "disabled");
			} else if (total < settings.max) {
				$(settings.addTrigger).removeAttr("disabled");
			}
		};



		/**
		 * Setup the repeater
		 * @return null
		 */
		(function () {
			$(settings.addTrigger).on("click", addOne);
			$("form").on("click", settings.deleteTrigger, deleteOne);

			if (!total) {
				var toCreate = settings.min - total;
				for (var j = 0; j < toCreate; j++) {
					createOne();
				}
			}

		})();




	};


	$.fn.repeatable = function (userSettings1) {

		/**
		 * Default settings
		 * @type {Object}
		 */


		var defaults1 = {
			addTrigger: "addcont",
			deleteTrigger: "deletecont",
			max: null,
      min: 1,
			template: null,
			itemContainer: ".field1-group",
			beforeAdd: function () {},
			afterAdd: function (item) {},
			beforeDelete: function (item) {},
			afterDelete: function () {}
		};

		/**
		 * Iterator used to make each added
		 * repeatable element unique
		 * @type {Number}
		 */
		var i = 1;

		/**
		 * DOM element into which repeatable
		 * items will be added
		 * @type {jQuery object}
		 */
		var target = $(this);

		/**
		 * Blend passed user settings with defauly settings
		 * @type {array}
		 */
		var settings1 = $.extend({}, defaults1, userSettings1);



		/**
		 * Total templated items found on the page
		 * at load. These may be created by server-side
		 * scripts.
		 * @return null
		 */
		var total1 = function () {
			return $(target).find(settings1.itemContainer).length;
		}();




		/**
		 * Add an element to the target
		 * and call the callback function
		 * @param  object e Event
		 * @return null
		 */
		var addCont = function (e) {
			var nb=$("#nbcont").val();
			var nouveau= parseInt(nb)+1;

			$("#nbcont").val(nouveau);

			// alert(nouveau);
			e.preventDefault();
			settings1.beforeAdd.call(this);
			var item = createCont();
			settings1.afterAdd.call(this, item);
		};



		/**
		 * Delete the parent element
		 * and call the callback function
		 * @param  object e Event
		 * @return null
		 */
		var deleteCont = function (e) {

			var nb=$("#nbcont").val();
			var nouveau= parseInt(nb)-1;

			$("#nbcont").val(nouveau);

			// alert(nouveau);
			e.preventDefault();
			if (total1 === settings1.min) return;
			var item = $(this).parents(settings1.itemContainer).first();
			settings1.beforeDelete.call(this, item);
			item.remove();
			total1--;
			maintainAddBtn1();
			settings1.afterDelete.call(this);
		};



		/**
		 * Add an element to the target
		 * @return null
		 */
		var createCont = function() {
			var item = getUniqueTemplate1();
			item.appendTo(target);
			total1++;
			maintainAddBtn1();
			return item;
		};



		/**
		 * Alter the given template to make
		 * each form field name unique
		 * @return {jQuery object}
		 */
		var getUniqueTemplate1 = function () {
			var template1 = $(settings1.template).html();
			// template = template.replace(/{\?}/g, "new" + i++); 	// {?} => iterated placeholder
			template1 = template1.replace(/{\?}/g, i++);
			template1 = template1.replace(/\{[^\?\}]*\}/g, ""); 	// {valuePlaceholder} => ""
			return $(template1);
		};



		/**
		 * Determines if the add trigger
		 * needs to be disabled
		 * @return null
		 */
		var maintainAddBtn1 = function () {
			if (!settings1.max) {
				return;
			}

			if (total1 === settings1.max) {
				$(settings1.addTrigger).attr("disabled", "disabled");
			} else if (total1 < settings1.max) {
				$(settings1.addTrigger).removeAttr("disabled");
			}
		};



		/**
		 * Setup the repeater
		 * @return null
		 */
		(function () {
			$(settings1.addTrigger).on("click", addCont);
			$("form").on("click", settings1.deleteTrigger, deleteCont);

			if (!total1) {
				var toCreate = settings1.min - total1;
				for (var j = 0; j < toCreate; j++) {
					createCont();
				}
			}

		})();




	};


	$.fn.repeatable = function (userSettings2) {

		/**
		 * Default settings
		 * @type {Object}
		 */


		var defaults2 = {
			addTrigger: "addreq",
			deleteTrigger: "deletereq",
			max: null,
      min: 1,
			template: null,
			itemContainer: ".field2-group",
			beforeAdd: function () {},
			afterAdd: function (item) {},
			beforeDelete: function (item) {},
			afterDelete: function () {}
		};

		/**
		 * Iterator used to make each added
		 * repeatable element unique
		 * @type {Number}
		 */
		var i = 1;

		/**
		 * DOM element into which repeatable
		 * items will be added
		 * @type {jQuery object}
		 */
		var target = $(this);

		/**
		 * Blend passed user settings with defauly settings
		 * @type {array}
		 */
		var settings2 = $.extend({}, defaults2, userSettings2);



		/**
		 * Total templated items found on the page
		 * at load. These may be created by server-side
		 * scripts.
		 * @return null
		 */
		var total2 = function () {
			return $(target).find(settings2.itemContainer).length;
		}();




		/**
		 * Add an element to the target
		 * and call the callback function
		 * @param  object e Event
		 * @return null
		 */
		var addReq = function (e) {
			var nb=$("#nbreq").val();
			var nouveau= parseInt(nb)+1;

			$("#nbreq").val(nouveau);

			// alert(nouveau);
			e.preventDefault();
			settings2.beforeAdd.call(this);
			var item = createReq();
			settings2.afterAdd.call(this, item);
		};



		/**
		 * Delete the parent element
		 * and call the callback function
		 * @param  object e Event
		 * @return null
		 */
		var deleteReq = function (e) {

			var nb=$("#nbreq").val();
			var nouveau= parseInt(nb)-1;

			$("#nbreq").val(nouveau);

			// alert(nouveau);
			e.preventDefault();
			if (total2 === settings2.min) return;
			var item = $(this).parents(settings2.itemContainer).first();
			settings2.beforeDelete.call(this, item);
			item.remove();
			total2--;
			maintainAddBtn2();
			settings2.afterDelete.call(this);
		};



		/**
		 * Add an element to the target
		 * @return null
		 */
		var createReq = function() {
			var item = getUniqueTemplate2();
			item.appendTo(target);
			total2++;
			maintainAddBtn2();
			return item;
		};



		/**
		 * Alter the given template to make
		 * each form field name unique
		 * @return {jQuery object}
		 */
		var getUniqueTemplate2 = function () {
			var template2 = $(settings2.template).html();
			// template = template.replace(/{\?}/g, "new" + i++); 	// {?} => iterated placeholder
			template2 = template2.replace(/{\?}/g, i++);
			template2 = template2.replace(/\{[^\?\}]*\}/g, ""); 	// {valuePlaceholder} => ""
			return $(template2);
		};



		/**
		 * Determines if the add trigger
		 * needs to be disabled
		 * @return null
		 */
		var maintainAddBtn2 = function () {
			if (!settings2.max) {
				return;
			}

			if (total2 === settings2.max) {
				$(settings2.addTrigger).attr("disabled", "disabled");
			} else if (total2 < settings2.max) {
				$(settings2.addTrigger).removeAttr("disabled");
			}
		};



		/**
		 * Setup the repeater
		 * @return null
		 */
		(function () {
			$(settings2.addTrigger).on("click", addReq);
			$("form").on("click", settings2.deleteTrigger, deleteReq);

			if (!total2) {
				var toCreate = settings2.min - total2;
				for (var j = 0; j < toCreate; j++) {
					createReq();
				}
			}

		})();




	};


	$.fn.repeatable = function (userSettings3) {

		/**
		 * Default settings
		 * @type {Object}
		 */


		var defaults3 = {
			addTrigger: "addcomp",
			deleteTrigger: "deletecomp",
			max: null,
      min: 1,
			template: null,
			itemContainer: ".field3-group",
			beforeAdd: function () {},
			afterAdd: function (item) {},
			beforeDelete: function (item) {},
			afterDelete: function () {}
		};

		/**
		 * Iterator used to make each added
		 * repeatable element unique
		 * @type {Number}
		 */
		var i = 1;

		/**
		 * DOM element into which repeatable
		 * items will be added
		 * @type {jQuery object}
		 */
		var target = $(this);

		/**
		 * Blend passed user settings with defauly settings
		 * @type {array}
		 */
		var settings3 = $.extend({}, defaults3, userSettings3);



		/**
		 * Total templated items found on the page
		 * at load. These may be created by server-side
		 * scripts.
		 * @return null
		 */
		var total3 = function () {
			return $(target).find(settings3.itemContainer).length;
		}();




		/**
		 * Add an element to the target
		 * and call the callback function
		 * @param  object e Event
		 * @return null
		 */
		var addComp = function (e) {
			var nb=$("#nbcomp").val();
			var nouveau= parseInt(nb)+1;

			$("#nbcomp").val(nouveau);

			// alert(nouveau);
			e.preventDefault();
			settings3.beforeAdd.call(this);
			var item = createComp();
			settings3.afterAdd.call(this, item);
		};



		/**
		 * Delete the parent element
		 * and call the callback function
		 * @param  object e Event
		 * @return null
		 */
		var deleteComp = function (e) {

			var nb=$("#nbcomp").val();
			var nouveau= parseInt(nb)-1;

			$("#nbcomp").val(nouveau);

			// alert(nouveau);
			e.preventDefault();
			if (total3 === settings3.min) return;
			var item = $(this).parents(settings3.itemContainer).first();
			settings3.beforeDelete.call(this, item);
			item.remove();
			total3--;
			maintainAddBtn3();
			settings3.afterDelete.call(this);
		};



		/**
		 * Add an element to the target
		 * @return null
		 */
		var createComp = function() {
			var item = getUniqueTemplate3();
			item.appendTo(target);
			total3++;
			maintainAddBtn3();
			return item;
		};



		/**
		 * Alter the given template to make
		 * each form field name unique
		 * @return {jQuery object}
		 */
		var getUniqueTemplate3 = function () {
			var template3 = $(settings3.template).html();
			// template = template.replace(/{\?}/g, "new" + i++); 	// {?} => iterated placeholder
			template3 = template3.replace(/{\?}/g, i++);
			template3 = template3.replace(/\{[^\?\}]*\}/g, ""); 	// {valuePlaceholder} => ""
			return $(template3);
		};



		/**
		 * Determines if the add trigger
		 * needs to be disabled
		 * @return null
		 */
		var maintainAddBtn3 = function () {
			if (!settings3.max) {
				return;
			}

			if (total3 === settings3.max) {
				$(settings3.addTrigger).attr("disabled", "disabled");
			} else if (total3 < settings3.max) {
				$(settings3.addTrigger).removeAttr("disabled");
			}
		};



		/**
		 * Setup the repeater
		 * @return null
		 */
		(function () {
			$(settings3.addTrigger).on("click", addComp);
			$("form").on("click", settings3.deleteTrigger, deleteComp);

			if (!total3) {
				var toCreate = settings3.min - total3;
				for (var j = 0; j < toCreate; j++) {
					createComp();
				}
			}

		})();




	};


	$.fn.repeatable = function (userSettings4) {

		/**
		 * Default settings
		 * @type {Object}
		 */


		var defaults4 = {
			addTrigger: "adddoc",
			deleteTrigger: "deletedoc",
			max: null,
      min: 1,
			template: null,
			itemContainer: ".field4-group",
			beforeAdd: function () {},
			afterAdd: function (item) {},
			beforeDelete: function (item) {},
			afterDelete: function () {}
		};

		/**
		 * Iterator used to make each added
		 * repeatable element unique
		 * @type {Number}
		 */
		var i = 1;

		/**
		 * DOM element into which repeatable
		 * items will be added
		 * @type {jQuery object}
		 */
		var target = $(this);

		/**
		 * Blend passed user settings with defauly settings
		 * @type {array}
		 */
		var settings4 = $.extend({}, defaults4, userSettings4);



		/**
		 * Total templated items found on the page
		 * at load. These may be created by server-side
		 * scripts.
		 * @return null
		 */
		var total4 = function () {
			return $(target).find(settings4.itemContainer).length;
		}();




		/**
		 * Add an element to the target
		 * and call the callback function
		 * @param  object e Event
		 * @return null
		 */
		var addDoc = function (e) {
			var nb=$("#nbdoc").val();
			var nouveau= parseInt(nb)+1;

			$("#nbdoc").val(nouveau);

			// alert(nouveau);
			e.preventDefault();
			settings4.beforeAdd.call(this);
			var item = createDoc();
			settings4.afterAdd.call(this, item);
		};



		/**
		 * Delete the parent element
		 * and call the callback function
		 * @param  object e Event
		 * @return null
		 */
		var deleteDoc = function (e) {

			var nb=$("#nbdoc").val();
			var nouveau= parseInt(nb)-1;

			$("#nbdoc").val(nouveau);

			// alert(nouveau);
			e.preventDefault();
			if (total4 === settings4.min) return;
			var item = $(this).parents(settings4.itemContainer).first();
			settings4.beforeDelete.call(this, item);
			item.remove();
			total4--;
			maintainAddBtn4();
			settings4.afterDelete.call(this);
		};



		/**
		 * Add an element to the target
		 * @return null
		 */
		var createDoc = function() {
			var item = getUniqueTemplate4();
			item.appendTo(target);
			total4++;
			maintainAddBtn4();
			return item;
		};



		/**
		 * Alter the given template to make
		 * each form field name unique
		 * @return {jQuery object}
		 */
		var getUniqueTemplate4 = function () {
			var template4 = $(settings4.template).html();
			// template = template.replace(/{\?}/g, "new" + i++); 	// {?} => iterated placeholder
			template4 = template4.replace(/{\?}/g, i++);
			template4 = template4.replace(/\{[^\?\}]*\}/g, ""); 	// {valuePlaceholder} => ""
			return $(template4);
		};



		/**
		 * Determines if the add trigger
		 * needs to be disabled
		 * @return null
		 */
		var maintainAddBtn4 = function () {
			if (!settings4.max) {
				return;
			}

			if (total4 === settings4.max) {
				$(settings4.addTrigger).attr("disabled", "disabled");
			} else if (total4 < settings4.max) {
				$(settings4.addTrigger).removeAttr("disabled");
			}
		};



		/**
		 * Setup the repeater
		 * @return null
		 */
		(function () {
			$(settings4.addTrigger).on("click", addDoc);
			$("form").on("click", settings4.deleteTrigger, deleteDoc);

			if (!total4) {
				var toCreate = settings4.min - total4;
				for (var j = 0; j < toCreate; j++) {
					createDoc();
				}
			}

		})();




	};

	$.fn.repeatable = function (userSettings5) {

		/**
		 * Default settings
		 * @type {Object}
		 */


		var defaults5 = {
			addTrigger: "adddocfac",
			deleteTrigger: "deletedocfac",
			max: null,
      min: 1,
			template: null,
			itemContainer: ".field5-group",
			beforeAdd: function () {},
			afterAdd: function (item) {},
			beforeDelete: function (item) {},
			afterDelete: function () {}
		};

		/**
		 * Iterator used to make each added
		 * repeatable element unique
		 * @type {Number}
		 */
		var i = 1;

		/**
		 * DOM element into which repeatable
		 * items will be added
		 * @type {jQuery object}
		 */
		var target = $(this);

		/**
		 * Blend passed user settings with defauly settings
		 * @type {array}
		 */
		var settings5 = $.extend({}, defaults5, userSettings5);



		/**
		 * Total templated items found on the page
		 * at load. These may be created by server-side
		 * scripts.
		 * @return null
		 */
		var total5 = function () {
			return $(target).find(settings5.itemContainer).length;
		}();




		/**
		 * Add an element to the target
		 * and call the callback function
		 * @param  object e Event
		 * @return null
		 */
		var addDocfac= function (e) {
			var nb=$("#nbdocfac").val();
			var nouveau= parseInt(nb)+1;

			$("#nbdocfac").val(nouveau);

			// alert(nouveau);
			e.preventDefault();
			settings5.beforeAdd.call(this);
			var item = createDocfac();
			settings5.afterAdd.call(this, item);
		};



		/**
		 * Delete the parent element
		 * and call the callback function
		 * @param  object e Event
		 * @return null
		 */
		var deleteDocfac = function (e) {

			var nb=$("#nbdocfac").val();
			var nouveau= parseInt(nb)-1;

			$("#nbdocfac").val(nouveau);

			// alert(nouveau);
			e.preventDefault();
			if (total5 === settings5.min) return;
			var item = $(this).parents(settings5.itemContainer).first();
			settings5.beforeDelete.call(this, item);
			item.remove();
			total5--;
			maintainAddBtn5();
			settings5.afterDelete.call(this);
		};



		/**
		 * Add an element to the target
		 * @return null
		 */
		var createDocfac = function() {
			var item = getUniqueTemplate5();
			item.appendTo(target);
			total5++;
			maintainAddBtn5();
			return item;
		};



		/**
		 * Alter the given template to make
		 * each form field name unique
		 * @return {jQuery object}
		 */
		var getUniqueTemplate5 = function () {
			var template5 = $(settings5.template).html();
			// template = template.replace(/{\?}/g, "new" + i++); 	// {?} => iterated placeholder
			template5 = template5.replace(/{\?}/g, i++);
			template5 = template5.replace(/\{[^\?\}]*\}/g, ""); 	// {valuePlaceholder} => ""
			return $(template5);
		};



		/**
		 * Determines if the add trigger
		 * needs to be disabled
		 * @return null
		 */
		var maintainAddBtn5 = function () {
			if (!settings5.max) {
				return;
			}

			if (total5 === settings5.max) {
				$(settings5.addTrigger).attr("disabled", "disabled");
			} else if (total5 < settings5.max) {
				$(settings5.addTrigger).removeAttr("disabled");
			}
		};



		/**
		 * Setup the repeater
		 * @return null
		 */
		(function () {
			$(settings5.addTrigger).on("click", addDocfac);
			$("form").on("click", settings5.deleteTrigger, deleteDocfac);

			if (!total5) {
				var toCreate = settings5.min - total5;
				for (var j = 0; j < toCreate; j++) {
					createDocfac();
				}
			}

		})();




	};

})(jQuery);
