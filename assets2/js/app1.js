let p=null;
function bindEvents(p)
{
  p.on('error',function(err){
    console.log('error',err)
  })

  p.on('signal',function(data){
    document.querySelector("#offer").textContent=JSON.stringify(data)
    // $("#offer").val(JSON.stringify(data))
    // document.getElementById("offer").textContent = textContent=JSON.stringify(data)
  })

  p.on('Stream',function(Stream){

    let mediaStream = new MediaStream();
    let Video=document.querySelector("#receiver-video")
    Video.volume=0;
    try {
      Video.srcObject=stream;
    } catch (error) {
      Video.src = URL.createObjectURL(stream);
    }

    emitterVideo.play();

  })

}

document.querySelector("#start").addEventListener('click',function(e){

  navigator.getUserMedia({
    video:true,
    audio:true
  }, function(stream){
    // let p=new simplePeer({
       p=new simplePeer({
      initiator: true,
      stream: stream,
      trickle:false
    });

    bindEvents(p);
    let mediaStream = new MediaStream();
    let emitterVideo=document.querySelector("#emitter-video")
    emitterVideo.volume=0;
    try {
      emitterVideo.srcObject=stream;
    } catch (error) {
      emitterVideo.src = URL.createObjectURL(stream);
    }

    emitterVideo.play();

  }, function(){

  })

})

document.querySelector("#incoming").addEventListener('submit',function(e){
  e.preventDefault();
  // let p=new simplePeer({

  if(p==null)
  {
    p=new simplePeer({
   initiator: false,
   trickle:false
 });

 bindEvents(p);
  }

p.signal(JSON.parse(e.target.querySelector('textarea').value));


})
