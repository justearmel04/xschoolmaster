<?php

session_start();

require_once('../class/User.php');

require_once('../class/Etablissement.php');

require_once('../class/LocalAdmin.php');

require_once('../class/Parent.php');

require_once('../class/Teatcher.php');

require_once('../class/Classe.php');

require_once('../class/Student.php');

require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();



$student=new Student();

$studentInfos=$student->getAllInformationsOfStudentNew($_GET['compte'],$_GET['sessionEtab']);

foreach ($studentInfos as  $value):
  // $studentparentid=

$matricule_eleve=$value->matricule_eleve;
$email_eleve=$value->email_eleve;
$nom=$value->nom_eleve;
$classe_eleve=$value->id_classe;
$libelleclasse_eleve=utf8_decode(utf8_encode($value->libelle_classe));
$tel=$value->tel_compte;
$datenais=$value->datenais_compte;
$photo=$value->photo_compte;
$prenom_eleve=$value->prenom_compte;


endforeach;








$parents=new ParentX();

// $parentInfos=$parents->getParentInfosbyId($studentparentid);
$parentInfos=$parents->ParentInfostudent($_GET['compte']);

// $tabParent=explode("*",$parentInfos);



$etabs=new Etab();

$codeEtabAssigner=$_GET['codeEtab'];

$code = $codeEtabAssigner;





try{

      $bdd = new PDO('mysql:host=localhost;dbname=xschool', 'root', '');
// $bdd = new PDO('mysql:host=www.proximity-cm.com;dbname=proximi5_xschool', 'proximi5_xschool', 'Psa@123456');



   }

catch(PDoExeption $e)

{

   $masseErreur='Erreur PDO dans'.$e->getMessage();

    die($masseErreur);

}





// informations personelles eleve





// $matricule_eleve = $tabStudent[1];
//
//
//
// $email_eleve=$tabStudent[7];
//
//
//
// $nom=$tabStudent[2];  $prenom_eleve= $tabStudent[3];







$date=date_format(date_create($datenais),"d/m/Y");

$lien="";

if(strlen($photo)>0)
{
$lien="../photo/Students/".$matricule_eleve."/".$photo;
}else {
  $lien="../photo/user9.jpg";
}





// $sexe = $tabStudent['6'];





if($sexe='M') {



  $sexe="Masculin";

} else {

  $sexe="Feminin";

}





// informations des parents





// $nom_parent =$tabParent['0'];  $email_parent=$tabParent['4'];
//
//
//
// $prenom_parent=$tabParent['1'];
//
//
//
// $fonction_parent= $tabParent['3'];
//
//
//
// $tel_parent=$tabParent['2'];





  //recupérer le logo de l'etablissemnt a partir du code etablissement





      $ps4=$bdd->prepare("SELECT  logo_etab FROM etablissement where code_etab=?");

      $parametre=array($code);

      $ps4->execute($parametre);

      $donnees4=$ps4->fetch();







  // recuperer la session en cour





     $encours=1;

     $req = $bdd->prepare("SELECT * from sessions where codeEtab_sess=? and encours_sess=?");

     $parametre=array($code,$encours);

     $req->execute($parametre);

     $data=$req->fetch();





  // recuperer le nom de l'établissement





      $ps=$bdd->prepare("SELECT  libelle_etab FROM etablissement where code_etab=?");

      $parametre=array($code);

      $ps->execute($parametre);

      $donnees1=$ps->fetch();







      $etabs=new Etab();



      require('fpdf/fpdf.php');

      class PDF extends FPDF
      {
      function Footer()
      {
          // Positionnement à 1,5 cm du bas
          $this->SetY(-15);
          // Police Arial italique 8
          $this->SetFont('Arial','I',8);
          // Numéro et nombre de pages
          $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
          // $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
      }

      // Tableau simple
      function BasicTable($header, $data)
      {
          // En-tête
          foreach($header as $col)
              $this->Cell(40,7,$col,1);
          $this->Ln();
          // Données
          foreach($data as $row)
          {
              foreach($row as $col)
                  $this->Cell(40,6,$col,1);
              $this->Ln();
          }
      }
      }




      $pdf = new PDF();
      $pdf->AliasNbPages();
      $pdf->SetMargins(3,0);
      $pdf->SetFont('Times','B',  16);
      $pdf->AddPage();
      $pdf->Ln(20);

      // $pdf->SetFont('Times','B',  16);
      //
      // $pdf->AddPage();

      $pdf->Image("../logo_etab/".$code."/".$donnees4['logo_etab'],8,8,45);

      $pdf->Ln(40);

      $pdf-> SetFont('Times','B',  14);



     $pdf->Cell(176,5,$donnees1['libelle_etab'],0,0,'C');

      $pdf->Ln(15);

      $pdf-> SetFont('Times','B', 12);

      $pdf->SetTextColor(0,0,0);

      $pdf->Cell(176,5,L::ReacapStudentsInfos,0,0,'C');

      $pdf->SetTextColor(0,0,0);

      $pdf->Ln(10);

      $pdf-> SetFont('Times','B',  12);

      $pdf->Cell(176,5,utf8_decode(L::ScolaryyearMenu).':'.' '.$data['libelle_sess'],0,0,'C');



      $pdf->Ln(10);



      $pdf->SetLineWidth(.3);

      $pdf->SetFont('Times','B',12);



      $pdf->Cell(176,5,L::GeneralInfostudentTabCaps,0,0,'C');
      $pdf->Image("$lien",160,57,30);
      $pdf->Ln(30);
      $pdf->SetXY(15, 110);
      $pdf->Cell(100,8,L::Name.':'.'  '.$nom,0,0,'L','0');
      $pdf->SetXY(140, 110);
      $pdf->Cell(100,8,L::MatriculestudentTab.':'.'  '.$matricule_eleve,0,0,'L','0');
      $pdf->SetXY(15, 120);
      $pdf->Cell(100,8,mb_strtolower(L::PreName).':'.'  '.$prenom_eleve,0,0,'L','0');
      $pdf->SetXY(140, 120);
      $pdf->Cell(100,8,utf8_decode(L::PhonestudentTab).':'.'  '.$tel,0,0,'L','0');
      $pdf->SetXY(15, 130);
      $pdf->Cell(100,8,L::EmailstudentTab.':'.'  '.$email_eleve,0,0,'L','0');
      $pdf->SetXY(140, 130);
      $pdf->Cell(100,8,L::ClassestudentTab.':'.'  '.$libelleclasse_eleve,0,0,'L','0');
      $pdf->SetXY(15, 140);
      $pdf->Cell(100,8,L::BirthstudentTab.':'.'  '.$date,0,0,'L','0');



      $pdf->SetXY(140, 140);



      $pdf->Cell(100,8,L::SexestudentTab.':'.'  '.$sexe,0,0,'L','0');





      $pdf->Ln(40);

      $pdf-> SetFont('Times','B',  13);

      $pdf->Cell(176,5,L::GeneralInfosParentTabCapsDu,0,0,'C');

      $pdf->Ln(10);

      $pdf-> SetFont('Times','B',  12);

      $pdf->SetFillColor(230,230,0);
      $pdf->SetLineWidth(.3);
      $pdf->SetFont('Times','B',12);
      $pdf->Cell(80,8,'Nom & prenoms',1,0,'C');
      $pdf->Cell(60,8,'Profession',1,0,'C');
      $pdf->Cell(60,8,'Email',1,0,'C');
      $pdf->Ln();
      foreach ($parentInfos as  $valueparents):


        $pdf->Cell(80,8,$valueparents->nom_compte." ".$valueparents->prenom_compte,1,0,'L');
        $pdf->Cell(60,8,$valueparents->fonction_compte,1,0,'L');
        $pdf->Cell(60,8,$valueparents->email_compte,1,0,'C');
        $pdf->Ln();


        // $pdf->SetXY(15,200);
        // $pdf->Cell(100,8,'Nom : '.$valueparents->nom_compte,0,0,'L','0');
        //
        // $pdf->SetXY(130, 200);
        // $pdf->Cell(100,8,'Telephone :'.$valueparents->tel_compte,0,0,'L','0');
        //
        // $pdf->SetXY(15, 210);
        // $pdf->Cell(100,8,'Prenom :'.$valueparents->prenom_compte,0,0,'L','0');
        //
        // $pdf->SetXY(130, 210);
        // $pdf->Cell(100,8,'Profession :'.$valueparents->fonction_compte,0,0,'L','0');
        //
        // $pdf->SetXY(15, 220);
        // $pdf->Cell(100,8,'Email :'.$valueparents->email_compte,0,0,'L','0');
        // $pdf->Ln(40);

      endforeach;


  $pdf->Output();





?>
