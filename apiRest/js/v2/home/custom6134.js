
$(window).load(function() { // makes sure the whole site is loaded

	"use strict";
//------------------------------------------------------------------------
//						PRELOADER SCRIPT
//------------------------------------------------------------------------   
    //$('#preloader').delay(400).fadeOut('slow'); // will fade out the white DIV that covers the website.
    //$('#preloader .inner').fadeOut(); // will first fade out the loading animation
//------------------------------------------------------------------------
//						WOW ANIMATION SETTINGS
//------------------------------------------------------------------------ 	
	var wow = new WOW({
    	offset:100,        // distance to the element when triggering the animation (default is 0)
    	mobile:false       // trigger animations on mobile devices (default is true)
  	});
	wow.init();
//------------------------------------------------------------------------
//						STELLAR (PARALLAX) SETTINGS
//------------------------------------------------------------------------ 	
if(!(navigator.userAgent.match(/iPhone|iPad|iPod|Android|BlackBerry|IEMobile/i))) {
	$.stellar({
		horizontalScrolling: false,
		positionProperty: 'transform'
	});
}
//------------------------------------------------------------------------
//						NAVBAR SLIDE SCRIPT
//------------------------------------------------------------------------ 		
	$(window).scroll(function () {
        if ($(window).scrollTop() > $("nav").height()) {
            $("nav.navbar-slide").addClass("show-menu");
        } else {
            $("nav.navbar-slide").removeClass("show-menu");
			$(".navbar-slide .navMenuCollapse").collapse({toggle: false});
			$(".navbar-slide .navMenuCollapse").collapse("hide");
			$(".navbar-slide .navbar-toggle").addClass("collapsed");
        }
    });
	
//------------------------------------------------------------------------
//						NAVBAR HIDE ON CLICK (COLLAPSED) SCRIPT
//------------------------------------------------------------------------ 		
    $('.nav a').on('click', function(){ 
        if($('.navbar-toggle').css('display') !='none'){
            $(".navbar-toggle").click()
        }
    });
	
})
$(document).ready(function(){
	"use strict";
//------------------------------------------------------------------------
//						ANCHOR SMOOTHSCROLL SETTINGS
//------------------------------------------------------------------------
	$('a.goto, .navbar .nav a').smoothScroll({speed: 1200});
//------------------------------------------------------------------------
//						FULL HEIGHT SECTION SCRIPT
//------------------------------------------------------------------------
	$("#minimal-intro").css("min-height",$( window ).height());
	$( window ).resize(function() {
		$("#minimal-intro").css("min-height",$( window ).height());
	})
//------------------------------------------------------------------------
//						INTRO SUPERSLIDER SETTINGS
//------------------------------------------------------------------------
	$("#slides").superslides({
		play: 8000, //Milliseconds before progressing to next slide automatically. Use a falsey value to disable.
		animation: "fade", //slide or fade. This matches animations defined by fx engine.
		pagination: false,
		inherit_height_from:".intro-block",
		inherit_width_from:".intro-block"
	});
//------------------------------------------------------------------------
//						SCREENSHOTS SLIDER SETTINGS
//------------------------------------------------------------------------
    var owl = $("#screenshots-slider");
    owl.owlCarousel({
        items : 4, 
        itemsDesktop : [1400,4], 
        itemsDesktopSmall : [1200,3], 
        itemsTablet: [900,2], 
        itemsMobile : [600,1],
		stopOnHover:true
    });
//------------------------------------------------------------------------
//						TESTIMONIALS SLIDER SETTINGS
//------------------------------------------------------------------------
    var owl = $("#testimonials-slider");
    owl.owlCarousel({
        singleItem:true,
		autoPlay:5000,
		stopOnHover:true
    });
//------------------------------------------------------------------------	
//                    MAGNIFIC POPUP(LIGHTBOX) SETTINGS
//------------------------------------------------------------------------  
	          
    $('#screenshots-slider').magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true
        }
    });
//------------------------------------------------------------------------
//					SUBSCRIBE FORM VALIDATION'S SETTINGS
//------------------------------------------------------------------------          
    $('#subscribe_form').validate({
        onfocusout: false,
        onkeyup: false,
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        errorPlacement: function(error, element) {
            error.appendTo( element.closest("form"));
        },
        messages: {
            email: {
                required: "We need your email address to contact you",
                email: "Please, enter a valid email"
            }
        },
					
        highlight: function(element) {
            $(element)
        },                    
					
        success: function(element) {
            element
            .text('').addClass('valid')
        }
    }); 
//------------------------------------------------------------------------------------
//						SUBSCRIBE FORM MAILCHIMP INTEGRATIONS SCRIPT
//------------------------------------------------------------------------------------		
    $('#subscribe_form').submit(function() {
        $('.error').hide();
        $('.error').fadeIn();
        // submit the form
        if($(this).valid()){
            $('#subscribe_submit').button('loading'); 
            var action = $(this).attr('action');
            $.ajax({
                url: action,
                type: 'POST',
                data: {
                    newsletter_email: $('#subscribe_email').val()
                },
                success: function(data) {
                    $('#subscribe_submit').button('reset');
					
					//Use modal popups to display messages
					$('#modalMessage .modal-title').html('<i class="icon icon-envelope-open"></i>' + data);
					$('#modalMessage').modal('show');
					
                },
                error: function() {
                    $('#subscribe_submit').button('reset');
					
					//Use modal popups to display messages
					$('#modalMessage .modal-title').html('<i class="icon icon-ban"></i>Oops!<br>Something went wrong!');
					$('#modalMessage').modal('show');
					
                }
            });
        }
        return false; 
    });
//------------------------------------------------------------------------------------
//						CONTACT FORM VALIDATION'S SETTINGS
//------------------------------------------------------------------------------------		  
    $('#contact_form').validate({
        onfocusout: false,
        onkeyup: false,
        rules: {
        	PageName: "required",
        	PageMessage: "required",
            PageEmail: {
                required: true,
                PageEmail: true
            }
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        },
        messages: {
        	PageName: "What's your name?",
        	PageMessage: "Type your message",
            PageEmail: {
                required: "What's your email?",
                PageEmail: "Please, enter a valid email"
            }
        },
					
        highlight: function(element) {
            $(element)
            .text('').addClass('error')
        },                    
					
        success: function(element) {
            element
            .text('').addClass('valid')
        }
    });   
    $('#Login_Form').validate({
        onfocusout: false,
        onkeyup: false,
        rules: {},
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        },
        messages: {
        	UserEmail: {
                required: "What's your email?",
                UserEmail: "Please, enter a valid email"
            }
        },
					
        highlight: function(element) {
            $(element)
            .text('').addClass('error')
        },                    
					
        success: function(element) {
            element
            .text('').addClass('valid')
        }
    });   
    
    $('#UserRegisterForm,#UserRegisterForm1').validate({
        onfocusout: false,
        onkeyup: false,
        rules: {},
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        },
        messages: {
        	UserName:"PLease enter parent name",
        	UserEmail: {
                required: "What's your email?",
                UserEmail: "Please, enter a valid email"
            }
        },
					
        highlight: function(element) {
            $(element)
            .text('').addClass('error')
        },                    
					
        success: function(element) {
            element
            .text('').addClass('valid')
        }
    });  
//------------------------------------------------------------------------------------
//								CONTACT FORM SCRIPT
//------------------------------------------------------------------------------------	
	
    $('#contact_form').submit(function() {
        // submit the form
        if($(this).valid()){
            $('#contact_submit').button('loading'); 
            var action = $(this).attr('action');
            $.ajax({
                url: action,
                type: 'POST',
                data: $( this ).serialize(),
                success: function(response) {
                    $('#contact_submit').button('reset');                    
                    response = JSON.parse(response);
                    console.log(response);
                    var msg='';				 
    				msg = '<span style="color:#a94442;font-size: 22px;">'+response['msg']+'!</span>';
    				//Use modal popups to display messages
    				if(msg!=''){
    					if(response['success']==1){
    						$('#modalContact').modal('hide');
    						
    						//Use modal popups to display messages
    						$('#modalMessage .modal-title').html('<i class="icon icon-envelope-open"></i>Thanks !<br> We’ll get back to you soon.');
    						$('#modalMessage').modal('show');
    						/*var onclick1 = "$('#errorMsg').slideUp(500).delay(7000)";
    						jQuery('#modalContact').prepend('<div id="errorMsg" class="multimsg" style="position: absolute; top: 0px; display: block;z-index: 10;"><div class="flash flash_success">'+msg+'<a href="javascript:void(0);" onclick='+onclick1+' class="msg_close" alt=""><span>X</span></a></div></div>');
    						$("#errorMsg").fadeIn('1000');
    						setTimeout(function(){ $('#errorMsg').slideUp(700).delay(8000);$('#modalForgot').modal('hide');}, 8000);*/
    						
    					}else{
    						var onclick1 = "$('#errorMsg').slideUp(500).delay(7000)";
    						jQuery('#modalContact').prepend('<div id="errorMsg" class="multimsg" style="position: absolute; top: 0px; display: block;z-index: 10;"><div class="flash flash_failure">'+msg+'<a href="javascript:void(0);" onclick='+onclick1+' class="close" alt=""><span>x</span></a></div></div>');
    						$("#errorMsg").fadeIn('1000');
    						setTimeout(function(){ $('#errorMsg').slideUp(700).delay(8000);}, 8000);
    					}
    				}
					/*$('#modalContact').modal('hide');
					
					//Use modal popups to display messages
					$('#modalMessage .modal-title').html('<i class="icon icon-envelope-open"></i>Well done!<br>Your message has been successfully sent!');
					$('#modalMessage').modal('show');*/
                },
                error: function() {
                    $('#contact_submit').button('reset');
					$('#modalContact').modal('hide');
					
					//Use modal popups to display messages
					$('#modalMessage .modal-title').html('<i class="icon icon-ban"></i>Oops!<br>Something went wrong!');
					$('#modalMessage').modal('show');
                }
            });
        } else {
            $('#contact_submit').button('reset')
        }
        return false; 
    });	    
	 
  //------------------------------------------------------------------------------------
//	LOGIN FORM SCRIPT
//------------------------------------------------------------------------------------	
    	    
    $('#UserRegisterForm1').submit(function() {

        // submit the form
        if(!$(this).valid() || $(this).valid()){
            $('.blue_btn').button('loading'); 
            console.log($( this ).serialize());
            var action = $(this).attr('action');            
            $.ajax({
                url: action,
                type: 'POST',
                data: $( this ).serialize(),
                success: function(response) {
                	response = JSON.parse(response);
                	console.log(response['msg'])
                    $('.blue_btn').button('reset');
					$('#modalContact').modal('hide');
					var msg='';
					if(response['success']==0){						
						msg = '<span style="color:#a94442;font-size: 22px;">'+response['msg']+'!</span>';
						$('#modalMessage .modal-title').html(msg);
						$('#modalMessage').modal('show');
						$('#modalMessage').attr('style','z-index:100000');
						setTimeout(function(){ $('#modalMessage').modal('hide');}, 10000);
					}
					//Use modal popups to display messages
					if(response['success']==1){						
						$("#UserName").val('');
						$("#UserEmail").val('');
						$("#UserPassword").val('');
						$("#UserConfirmPassword").val('');
						$("#UserStudentName").val('');
						$("#modalRegister .close").trigger('click');
						msg = '<span style="color:#005E5D;font-size: 22px;">'+response['msg']+'!</span>';
						$('#modalMessage .modal-title').html(msg);
						$('#modalMessage').modal('show');
						setTimeout(function(){ $('#modalMessage').modal('hide');}, 3000);
					}
                },
                error: function() {
                    $('.blue_btn').button('reset');
					$('#modalContact').modal('hide');
					
					//Use modal popups to display messages
					$('#modalMessage .modal-title').html('<i class="icon icon-ban"></i>Oops!<br>Something went wrong!');
					$('#modalMessage').modal('show');
                }
            });
        } else {
        	$('.blue_btn').button('reset');
        }
        return false; 
    
    });
    $('#Login_Form').submit(function() {
    	
        // submit the form
        if($(this).valid()){
            $('.login_btn').button('loading'); 
            console.log($( this ).serialize());
            var action = $(this).attr('action');
            var remember =0;
            if($('#UserRememberMe').prop('checked')){
            	remember =1;
            } 
            $.ajax({
                url: action,
                type: 'POST',
                data: $( this ).serialize(),
                success: function(response) {
                	response = JSON.parse(response);
                	//console.log(response['msg'])
                    $('.login_btn').button('reset');
					$('#modalContact').modal('hide');
					var msg='';
					if(response['success']==0){
						msg = '<span style="color:#a94442;font-size: 22px;">'+response['msg']+'!</span>';
					}
					//Use modal popups to display messages
					if(response['success']==1){
						window.location.href = "/messages";
					}
					if(msg!=''){
						$('#modalMessage .modal-title').html(msg);
						$('#modalMessage').modal('show');
						$('#modalMessage').attr('style','z-index:100000');
						/*var onclick1 = "$('#errorMsg').slideUp(500).delay(7000)";
						jQuery('#modalLogin').prepend('<div id="errorMsg" class="multimsg" style="position: absolute; top: 0px; display: block;z-index: 10;"><div class="flash flash_failure">'+msg+'<a href="javascript:void(0);" onclick='+onclick1+' class="close" alt=""><span>x</span></a></div></div>');
						$("#errorMsg").fadeIn('1000');*/
						setTimeout(function(){ $('#modalMessage').modal('hide');}, 6000);
					}
                },
                error: function() {
                    $('#contact_submit').button('reset');
					$('#modalContact').modal('hide');
					
					//Use modal popups to display messages
					$('#modalMessage .modal-title').html('<i class="icon icon-ban"></i>Oops!<br>Something went wrong!');
					$('#modalMessage').modal('show');
                }
            });
        } else {
        	$('.login_btn').button('reset');
        }
        return false; 
    });	    
  //------------------------------------------------------------------------------------
//	REGISTER FORM SCRIPT
//------------------------------------------------------------------------------------	
 $('#UserRegisterForm').submit(function() {
    	
        // submit the form
        if($(this).valid()){
            $('.blue_btn').button('loading'); 
            console.log($( this ).serialize());
            var action = $(this).attr('action');
             
            $.ajax({
                url: action,
                type: 'POST',
                data: $( this ).serialize(),
                success: function(response) {
                	response = JSON.parse(response);
                	console.log(response['msg'])
                    $('.blue_btn').button('reset');
					$('#modalContact').modal('hide');
					var msg='';
					if(response['success']==0){						
						msg = '<span style="color:#a94442;font-size: 22px;">'+response['msg']+'!</span>';
						$('#modalMessage .modal-title').html(msg);
						$('#modalMessage').modal('show');
						$('#modalMessage').attr('style','z-index:100000');
						setTimeout(function(){ $('#modalMessage').modal('hide');}, 10000);
					}
					//Use modal popups to display messages
					if(response['success']==1){						
						$("#UserName").val('');
						$("#UserEmail").val('');
						$("#UserPassword").val('');
						$("#UserConfirmPassword").val('');
						$("#UserStudentName").val('');
						$("#modalRegister .close").trigger('click');
						msg = '<span style="color:#005E5D;font-size: 22px;">'+response['msg']+'!</span>';
						$('#modalMessage .modal-title').html(msg);
						$('#modalMessage').modal('show');
						setTimeout(function(){ $('#modalMessage').modal('hide');}, 3000);
					}
                },
                error: function() {
                    $('.blue_btn').button('reset');
					$('#modalContact').modal('hide');
					
					//Use modal popups to display messages
					$('#modalMessage .modal-title').html('<i class="icon icon-ban"></i>Oops!<br>Something went wrong!');
					$('#modalMessage').modal('show');
                }
            });
        } else {
        	$('.blue_btn').button('reset');
        }
        return false; 
    });	
});
$('#Forgot_Form').submit(function() {
    // submit the form
    if($(this).valid()){
        $('.blue_btn').button('loading'); 
        console.log($( this ).serialize());
        var action = $(this).attr('action');
      
        $.ajax({
            url: action,
            type: 'POST',
            data: $( this ).serialize(),
            success: function(response) {
            	response = JSON.parse(response);
            	//console.log(response['msg'])
                $('.blue_btn').button('reset');
				$('#modalContact').modal('hide');
				var msg='';				 
				msg = '<span style="color:#a94442;font-size: 22px;">'+response['msg']+'!</span>';
				
				//Use modal popups to display messages
		
				if(msg!=''){
					if(response['success']==1){
						var onclick1 = "$('#errorMsg').slideUp(500).delay(7000)";
						jQuery('#modalForgot').prepend('<div id="errorMsg" class="multimsg" style="position: absolute; top: 0px; display: block;z-index: 10;"><div class="flash flash_success">'+msg+'<a href="javascript:void(0);" onclick='+onclick1+' class="msg_close" alt=""><span>X</span></a></div></div>');
						$("#errorMsg").fadeIn('1000');
						setTimeout(function(){ $('#errorMsg').slideUp(700).delay(8000);$('#modalForgot').modal('hide');}, 8000);
						
					}else{
						var onclick1 = "$('#errorMsg').slideUp(500).delay(7000)";
						jQuery('#modalForgot').prepend('<div id="errorMsg" class="multimsg" style="position: absolute; top: 0px; display: block;z-index: 10;"><div class="flash flash_failure">'+msg+'<a href="javascript:void(0);" onclick='+onclick1+' class="close" alt=""><span>x</span></a></div></div>');
						$("#errorMsg").fadeIn('1000');
						setTimeout(function(){ $('#errorMsg').slideUp(700).delay(8000);}, 8000);
					}
				}
            },
            error: function() {
                $('#blue_btn').button('reset');
				$('#modalContact').modal('hide');
				
				//Use modal popups to display messages
				$('#modalMessage .modal-title').html('<i class="icon icon-ban"></i>Oops!<br>Something went wrong!');
				$('#modalMessage').modal('show');
            }
        });
    } else {
    	$('.blue_btn').button('reset');
    }
    return false; 
});	    
//------------------------------------------------------------------------------------
//								SCROLL
//------------------------------------------------------------------------------------	
$(function(){
    $('.user_register_form').slimScroll({
        height: '280px',
        size: '5px',
        distance: '8px',
        color: '#CCCCCC'
        
    });
});