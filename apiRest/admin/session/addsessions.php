<?php

// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}


//require_once('../../class1/User.php');

//require_once('../../class1/cnx.php');

//require_once('../../class1/Parent.php');

//require_once('../../class1/Etablissement.php');

require_once('../../class/cnx.php');

require_once('../../class1/functions.php');

$db = new mysqlConnector();

//$users = new User();

//$parents=new ParentX();

//$etabs=new Etab();





if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Recuperation des donnees de la premiere etape : Infos Client

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

  $datedeb=$request->datedeb;
  $datefin=$request->datefin;
  $codeEtab=$request->codeEtab;
  $sessionEtab=$request->sessionEtab;
  $libellesession=$request->libelle;
  $semestre=$request->semestre;
  $statut=0;
  $encours=1;

      $code ="INSERT INTO sessions SET libelle_sess='$libellesession',codeEtab_sess='$codeEtab',statut_sess='$statut',encours_sess='$encours',type_sess='$semestre',datedeb_sess='$datedeb',datefin_sess='$datefin'";
       $req=$db->dataBase->prepare($code);
       $req->execute([]);
       //$datas=$req->fetchAll();
       $idlastsession=$db->dataBase->lastInsertId();
      $tabsemesterid=array();
  
if($semestre==2)
  {
    $suivant=1;
    $down=-1;
    //semestre
    //creation des semestres
    for($i=1;$i<=$semestre;$i++)
    {
      $libellesemestre="SEMESTRE ".$i;
      $statut=0;
      //insertion dans la tables semestre
      //$idsemestreadd=$session->AddSemestreFirst($libellesemestre,$idsession,$statut,$codeEtab);
 
       $code1 ="INSERT INTO semestre SET idsess_semes='$idlastsession',libelle_semes='$libellesemestre',statut_semes='$statut',codeEtab_semes='$codeEtab'";
       $req1=$db->dataBase->prepare($code1);
       $req1->execute([]);
       //$datas=$req1->fetchAll();
       $idsemestreadd=$db->dataBase->lastInsertId();
       $suivant=$idsemestreadd+1;
       $tabsemesterid[$i]=$idsemestreadd;

      if($i==2)
      {
        //$session->UpdateNextSemestreFinal($idsemestreadd,$idsession,$codeEtab,$down);
	
	  $code2="UPDATE semestre SET next_semes='$down' WHERE idsess_semes='$idsession' and codeEtab_semes= '$codeEtab' and id_semes='$idsemestreadd'";
	  $req2= $db->dataBase->prepare($code2);
       	  $req2->execute([]);
         //$datas=$req2->fetchAll();

      }else {
        //$session->UpdateNextSemestre($idsemestreadd,$idsession,$codeEtab,$suivant);
	
	  $code3="UPDATE semestre SET next_semes='$suivant' WHERE idsess_semes='$idsession' and codeEtab_semes= '$codeEtab' and id_semes= '$idsemestreadd'";
	  $req3=$db->dataBase->prepare($code3);
       	  $req3->execute([]);
         //$datas=$req3->fetchAll();
        }
       
      }
    }
    else if($semestre==3)
       {
   	//trimestre
    	 $suivant=1;
   	 $down=-1;
   	 for($i=1;$i<=$semestre;$i++)
   	 {
     	 $libellesemestre="TRIMESTRE ".$i;
     	 $statut=0;
     
     }
      //insertion dans la tables semestre
      //$idsemestreadd=$session->AddSemestreFirst($libellesemestre,$idsession,$statut,$codeEtab);
	
       $code4 ="INSERT INTO semestre SET idsess_semes='$idsession',libelle_semes='$libellesemestre',statut_semes='$statut',codeEtab_semes='$codeEtab'";
       $req4=$db->dataBase->prepare($code4);
       $req4->execute([]);
       //$datas=$req4->fetchAll();

      $suivant=$idsemestreadd+1;
      $tabsemesterid[$i]=$idsemestreadd;

     if($i==3)
      {
        //$session->UpdateNextSemestreFinal($idsemestreadd,$idsession,$codeEtab,$down);

	 $code4="UPDATE semestre SET next_semes='$down' WHERE idsess_semes='$idsession'and codeEtab_semes='$codeEtab' and id_semes='$idsemestreadd'";
	  $req4=$db->dataBase->prepare($code4);
       	  $req4->execute([]);
          //$datas=$req4->fetchAll();

      }else {

        //$session->UpdateNextSemestre($idsemestreadd,$idsession,$codeEtab,$suivant);

	$code5="UPDATE semestre SET next_semes='$suivant' WHERE idsess_semes='$idsession' and codeEtab_semes='$codeEtab' and id_semes='$idsemestreadd'";
	$req5=$db->dataBase->prepare($code5);
       	$req5->execute([]);
        //$datas=$req5->fetchAll();

      	   }

         }	
     

	        $firstsemesterid=$tabsemesterid[1];

       // $session->UpdateCurrentlySemester($firstsemesterid,$encours,$libellesession,$codeEtab,$idsession);

	$code6="UPDATE semestre SET statut_semes='$encours' WHERE id_semes='$libellesession' and codeEtab_semes='$codeEtab' and idsess_semes='$idsession'";	
	$req6=$db->dataBase->prepare($code6);
       	$req6->execute([]);
       // $datas=$req6->fetchAll();


     }

   

?>
