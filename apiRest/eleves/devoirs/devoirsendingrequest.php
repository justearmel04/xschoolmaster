<?php

session_start();

// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}


require_once('../../class/cnx.php');

require_once('../../class1/functions.php');

$db = new mysqlConnector();



//$users = new User();

//$parents=new ParentX();

//$etabs=new Etab();



if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Recuperation des donnees de la premiere etape : Infos Client


$postdata =$_POST['devoir'];
$request = json_decode($postdata);

$studentid=utf8_decode($request->studentid);
$devoirid=utf8_decode($request->devoirid);
$codeEtab=utf8_decode($request->codeEtab);
$sessionEtab=utf8_decode($request->sessionEtab);
$matiereid=utf8_decode($request->matiereid);
$libellematiere=utf8_decode($request->matierelib);
$teatcherid=utf8_decode($request->teatcherid);
$classeid=utf8_decode($request->classeid);
$libelleclasse=utf8_decode($request->classelib);



  $date=date("Y-m-d");
  $tabdate=explode("-",$date);
  $years=$tabdate[0];
  $mois=$tabdate[1];
  $days=$tabdate[2];
  $libellemois=obtenirLibelleMois($mois);

$target_path = "../../../temp/";
 $target_path = $target_path . basename($_FILES['file']['name']);
 if(move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {

   $file_name = @$_FILES['file']['name'];
    $_SESSION["file"] = $file_name;
    $file_size =@$_FILES['file']['size'];
    $file_tmp =@$_FILES['file']['tmp_name'];
    $file_type=@$_FILES['file']['type'];
    @$file_ext=strtolower(end(explode('.',@$_FILES['file']['name'])));
    $fichierTemp = uniqid() . "." . $file_ext;

    $transactionId =  "DEVOIRSTUDENT_".date("Y")."_".$codeEtab."_".$classeid."_".$devoirid."_".$studentid;
    $fichierad=$transactionId.".".$file_ext;

    $dossier="../../../devoirs/Etablissements/";
    $dossier1="../../../devoirs/Etablissements/".$codeEtab."/";
    $dossier2="../../../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere);
    $dossier3="../../../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/";
    $dossier4="../../../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/Rendus/";

    if(!is_dir($dossier)) {
        @mkdir($dossier);
        if(!is_dir($dossier1)) {
            @mkdir($dossier1);

            if(!is_dir($dossier2)) {
                @mkdir($dossier2);

            }

            if(!is_dir($dossier3)) {
                @mkdir($dossier3);

            }

            if(!is_dir($dossier4)) {
                @mkdir($dossier4);

            }

        }
    }else {
      if(!is_dir($dossier1)) {
          @mkdir($dossier1);

          if(!is_dir($dossier2)) {
              @mkdir($dossier2);

          }

          if(!is_dir($dossier3)) {
              @mkdir($dossier3);

          }

          if(!is_dir($dossier4)) {
              @mkdir($dossier4);

          }

      }
    }

      @mkdir($dossier4);

   @rename($target_path ,"../../../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/Rendus/".$fichierad);

   //Suppression du fichier se trouvant dans le dossier temp

    @unlink($target_path);

  $type="DEVOIR";
  $statut=1;



  $code="INSERT INTO rendus SET type_rend=?,idrendu_rend=?,student_rend=?,classe_rend=?,matiere_rend=?,codeEtab_rend=?,sessionEtab_rend=?,fichier_rend=?,teatcher_rend=?,statut_rend=?";
  $req=$db->dataBase->prepare($code);
  $req->execute([$type,$devoirid,$studentid,$classeid,$matiereid,$codeEtab,$sessionEtab,$fichierad,$teatcherid,$statut]);


echo json_encode('success');



}

}


?>

