<?php

// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}



require_once('../../class/cnx.php');

require_once('../../class1/functions.php');

$db = new mysqlConnector();


if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Recuperation des donnees de la premiere etape : Infos Client

//$postdata = file_get_contents("php://input");
$postdata =$_POST['support'];
$request = json_decode($postdata);

$codeEtab=utf8_decode($request->codeEtab);
$sessionEtab=utf8_decode($request->sessionEtab);
$matiereid=utf8_decode($request->matiereid);
$libellematiere=utf8_decode($request->matierelib);
$teatcherid=utf8_decode($request->teatcherid);
$classeid=utf8_decode($request->classeid);
$libelleclasse=utf8_decode($request->classelib);
$datecourse=utf8_decode($request->datecourse);
$libellecourse=utf8_decode($request->libellecourse);
$durationcourse=utf8_decode($request->durationcourse);
$descriptioncourse=utf8_decode($request->descriptioncourse);

$sections=$request->sections;
$competences=$request->competences;
$exercices=$request->exercices;

//on ajoute le cours dans le syst�me

// $code='INSERT INTO courses SET 	libelle_courses='."$libellecourse".',date_courses="$datecourse",mat_courses="$matiereid",teatcher_courses="$teatcherid",classe_courses="$classeid",codeEtab_courses="$codeEtab",sessionEtab_courses="$sessionEtab",descri_courses="$descriptioncourse",duree_courses="$durationcourse"';
// $req=$db->dataBase->prepare($code);
// $req->execute([]);
// $courseid=$db->dataBase->lastInsertId();

$code="INSERT INTO courses SET 	libelle_courses=?,date_courses=?,mat_courses=?,teatcher_courses=?,classe_courses=?,codeEtab_courses=?,sessionEtab_courses=?,descri_courses=?,duree_courses=?";
$req=$db->dataBase->prepare($code);
$req->execute([$libellecourse,$datecourse,$matiereid,$teatcherid,$classeid,$codeEtab,$sessionEtab,$descriptioncourse,$durationcourse]);
$courseid=$db->dataBase->lastInsertId();

//insertion des sections du cours

 $sections=$request->sections;

 $nbsections=count($sections);

  if($nbsections>0)
  {
     foreach ($sections as $value):
        $libelle=utf8_decode($value->libellesection);
	$code1="INSERT INTO coursessection SET libelle_coursesec=?,idcourse_coursesec=?";
        $req1=$db->dataBase->prepare($code1);
        $req1->execute([$libelle,$courseid]);

     endforeach;
  }

//insertion des comp�tences vis�es

  $competences=$request->competences;

  $nbcompetences=count($competences);

  if($nbcompetences>0)
  {
	foreach ($competences as $value):
    $competence=utf8_decode($value->libellecompetence);
    $code2="INSERT INTO coursescomp SET libelle_coursecomp=?,idcourse_coursescomp=?";
    $req2=$db->dataBase->prepare($code2);
    $req2->execute([$competence,$courseid]);

  endforeach;

  }



//nous allons ajouter les exercices


  $exercices=$request->exercices;

  $nbexercices=count($exercices);

  if($nbexercices>0)
  {
	foreach ($exercices as $value):
    $exercice=utf8_decode($value->libelleexercice);
    $code3="INSERT INTO coursesworkh SET libelle_coursewh=?,idcourse_coursesworkh=?";
    $req3=$db->dataBase->prepare($code3);
    $req3->execute([$exercice,$courseid]);

  endforeach;

  }

  $target_path = "../../../temp/";
$target_path = $target_path . basename($_FILES['file']['name']);
if(move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {

$file_name = @$_FILES['file']['name'];

  $_SESSION["file"] = $file_name;

  $file_size =@$_FILES['file']['size'];

  $file_tmp =@$_FILES['file']['tmp_name'];

  $file_type=@$_FILES['file']['type'];

  @$file_ext=strtolower(end(explode('.',@$_FILES['file']['name'])));

  $fichierTemp = uniqid() . "." . $file_ext;

$transactionId =  "COURSES".date("Y") . date("m").strtoupper(substr($libellematiere, 0, 3)).$courseid. "." . $file_ext;

        $dossier="../../../courses/".$libelleclasse."/";

        $dossier1="../../../courses/".$libelleclasse."/".$datecourse."/";

        $dossier2="../../..//courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/";

        if(!is_dir($dossier)) {
              //Le dossier n'existe pas - In procède à ssa création
              @mkdir($dossier);
              @mkdir($dossier1);
              @mkdir($dossier2);

                  }else
                  {

             @mkdir($dossier1);
             @mkdir($dossier2);

                  }

  @rename($target_path , "../../../courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/".$transactionId);



                 //Suppression du fichier se trouvant dans le dossier temp

      //@unlink($target_path);
$typesupport="COURSES";

  $codeinsert="INSERT INTO supports set courseid_support=?,matiereid_support=?,teatcherid_support=?,codeEtab_support=?,sessionEtab_support=?,classeid_support=?,fichier_support=?,type_support=?";
  $req4=$db->dataBase->prepare($codeinsert);
  $req4->execute([$courseid,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$classeid,$transactionId,$typesupport]);

echo json_encode('success');

// echo json_encode($dataretour);



}
else{

echo json_encode('unsuccess upload');
}



}



?>
