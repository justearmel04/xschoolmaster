<?php

// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}



require_once('../../class/cnx.php');

require_once('../../class1/functions.php');

$db = new mysqlConnector();


if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Recuperation des donnees de la premiere etape : Infos Client

$postdata = file_get_contents("php://input");
//$postdata =$_POST['support'];
$request = json_decode($postdata);

$codeEtab=utf8_decode($request->codeEtab);
$sessionEtab=utf8_decode($request->sessionEtab);
$matiereid=utf8_decode($request->matiereid);
$teatcherid=utf8_decode($request->teatcherid);
$classeid=utf8_decode($request->classeid);
$datecourse=utf8_decode($request->datecourse);
$libellecourse=utf8_decode($request->libellecourse);
$durationcourse=utf8_decode($request->durationcourse);
$descriptioncourse=utf8_decode($request->descriptioncourse);
$courseid=utf8_decode($request->courseid);


//echo json_encode('success');
//echo json_encode($codeEtab.'-'.$sessionEtab.'-'.$matiereid.'-'.$teatcherid.'-'.$classeid.'-'.$datecourse.'-'.$libellecourse.'-'.$durationcourse.'-'.$descriptioncourse.'-'.$courseid);

//$code="UPDATE courses SET libelle_courses='$libellecourse',date_courses='$datecourse',duree_courses='$durationcourse',descri_courses='$descriptioncourse' where teatcher_courses='$teatcherid' and classe_courses='$classeid' and mat_courses='$matiereid' and codeEtab_courses='$sessionEtab' and sessionEtab_courses='$sessionEtab' and id_courses='$courseid'";
echo json_encode($code);
//
//$req->execute([]);



$code="UPDATE courses SET libelle_courses='$libellecourse',date_courses='$datecourse',duree_courses='$durationcourse',descri_courses='$descriptioncourse' where teatcher_courses='$teatcherid' and classe_courses='$classeid' and mat_courses='$matiereid' and codeEtab_courses='$codeEtab' and sessionEtab_courses='$sessionEtab' and id_courses='$courseid'";
$req=$db->dataBase->prepare($code);
$req->execute([$libellecourse,$datecourse,$durationcourse,$descriptioncourse,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab,$courseid]);

echo json_encode('success');


}



?>
