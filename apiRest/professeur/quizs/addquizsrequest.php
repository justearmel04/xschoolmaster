<?php

// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}



require_once('../../class/cnx.php');

require_once('../../class1/functions.php');

$db = new mysqlConnector();


if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Recuperation des donnees de la premiere etape : Infos Client

//$postdata =$_POST['devoir'];
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$codeEtab=utf8_decode($request->codeEtab);
$sessionEtab=utf8_decode($request->sessionEtab);
$matiereid=utf8_decode($request->matiereid);
$libellematiere=utf8_decode($request->matierelib);
$teatcherid=utf8_decode($request->teatcherid);
$classeid=utf8_decode($request->classeid);
$libelleclasse=utf8_decode($request->classelib);
$datelimitedevoir=utf8_decode($request->datelimite);
$libelledevoir=utf8_decode($request->libelle);
$verouillerdevoir=utf8_decode($request->verouillerquiz);
$instructionsdevoir=utf8_decode($request->instructionsquiz);
$durationquiz=utf8_decode($request->durationquiz);

$trueorfalse=$request->trueorfalse;
$multiplechoice=$request->multiplechoice;

$nbtrueorfalses=count($trueorfalse);
$nbmultiplechoices=count($multiplechoice);
$statutquiz=0;
$destinataires="ELEVE@PARENT";

$code="INSERT INTO quiz SET libelle_quiz=?,duree_quiz=?,instruction_quiz=?,classe_quiz=?,matiere_quiz=?,teatcher_quiz=?,codeEtab_quiz=?,sessionEtab_quiz=?,verouiller_quiz=?,statut_quiz=?,datelimite_quiz=?";
$req=$db->dataBase->prepare($code);
$req->execute([$libelledevoir,$durationquiz,$instructionsdevoir,$classeid,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$verouillerdevoir,$statutquiz,$datelimitedevoir]);
$quizid=$db->dataBase->lastInsertId();


if($nbtrueorfalses>0)
{
   foreach($trueorfalse as $value):

   $libellequestion=$value->libellequestion;
   $pointquestion=$value->pointquestion;
   $answer=$value->answer;
   $mode=1;

  $code1="INSERT INTO question SET libelle_quest=?,mode_quest=?,point_quest=?,idquiz_quest=?";
  $req=$db->dataBase->prepare($code1);
  $req->execute([$libellequestion,$mode,$pointquestion,$quizid]);
  $questionid=$db->dataBase->lastInsertId();

  if($answer==1)
{
  //cas de vrai
 
   $code2="INSERT INTO propositionrep SET libelle_proprep='VRAI',idquest_proprep=?,valeur_proprep=1";
   $req=$db->dataBase->prepare($code2);
   $req->execute([$questionid]);

   $code3="INSERT INTO propositionrep SET libelle_proprep='FAUX',idquest_proprep=?";
   $req=$db->dataBase->prepare($code3);
   $req->execute([$questionid]);



  

}else {
  // cos de faux
  //$etabs->AddPropositionTrue($questionid);

  $code2="INSERT INTO propositionrep SET libelle_proprep='VRAI',idquest_proprep=?";
  $req=$db->dataBase->prepare($code2);
  $req->execute([$questionid]);

  $code3="INSERT INTO propositionrep SET libelle_proprep='FAUX',idquest_proprep=?,valeur_proprep=1";
  $req=$db->dataBase->prepare($code3);
  $req->execute([$questionid]);


  //$etabs->AddPropositionFalseActive($questionid);
}


   endforeach;

}

if($nbmultiplechoices>0)
{
  foreach($multiplechoice as $value):
  $libellequestion=$value->libellequestion;
  $pointquestion=$value->pointquestion;
  $answer=$value->answer;
  $mode=2;
  $propositions=$value->propositions;
  $nbpropositions=count($propositions);
  
  $code1="INSERT INTO question SET libelle_quest=?,mode_quest=?,point_quest=?,idquiz_quest=?";
  $req=$db->dataBase->prepare($code1);
  $req->execute([$libellequestion,$mode,$pointquestion,$quizid]);
  $questionid=$db->dataBase->lastInsertId();




  if($nbpropositions>0)
  {
    foreach($propositions as $values):

   $libelle_proposition=$values->proposition;

   $code4="INSERT INTO propositionrep SET libelle_proprep=?,idquest_proprep=?";
   $req=$db->dataBase->prepare($code4);
   $req->execute([$libelle_proposition,$questionid]);
   $proposiionid=$db->dataBase->lastInsertId();

   $chk_proposition=$values->check;

   if($chk_proposition==1)
   {
    $code5="UPDATE propositionrep SET valeur_proprep=? where id_proprep=?";
    $req=$db->dataBase->prepare($code5);
    $req->execute([$chk_proposition,$proposiionid]);


   }




    endforeach;
  }




   endforeach;

}

echo json_encode('success');


}



?>
