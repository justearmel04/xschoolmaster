<?php

// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}


//require_once('../../class1/User.php');

//require_once('../../class1/cnx.php');

//require_once('../../class1/Parent.php');

//require_once('../../class1/Etablissement.php');

require_once('../../class/cnx.php');

$db = new mysqlConnector();

//$users = new User();

//$parents=new ParentX();

//$etabs=new Etab();



if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Recuperation des donnees de la premiere etape : Infos Client

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$classe = $request->classeid;
$matiere = $request->matiereid;
$teatcher = $request->teatcherid;
$codeEtab = $request->codeEtab;
$controle = $request->controle;
$coef = $request->coef;
$datectrl = $request->datectrl;
$libellesession=$request->sessionEtab;
$typesess=$request->semester;

//$etabs->AddControleClasseSchool($controle,$datectrl,$classe,$matiere,$teatcher,$coef,$codeEtab,$libellesession,$typesess);

$statut=0;

$code1="SELECT * FROM controle where mat_ctrl='$matiere' and classe_ctrl='$classe' and teatcher_ctrl='$teatcher' and codeEtab_ctrl='$codeEtab' and coef_ctrl='$coef' and date_ctrl='$datectrl' ";
$req1=$db->dataBase->prepare($code1);
$req1->execute([]);
$data=$req1->fetchAll();
$nb=count($data);

if($nb==0)
{
$code="INSERT INTO controle SET libelle_ctrl='$controle',date_ctrl='$datectrl',classe_ctrl='$classe',mat_ctrl='$matiere',teatcher_ctrl='$teatcher',coef_ctrl='$coef',codeEtab_ctrl='$codeEtab',statut_ctrl='$statut',session_ctrl='$libellesession',typesess_ctrl='$typesess'";

$req=$db->dataBase->prepare($code);
$req->execute([]);
$idcontrole=$db->dataBase->lastInsertId();

if($idcontrole>0)
{
echo json_encode('success');

}else
{
echo json_encode('error');

}

}else
{
echo json_encode('existe');
}   


 





}else if ($_SERVER['REQUEST_METHOD'] === 'GET'){


 if(isset($_GET['id'])&& isset($_GET['sessionEtab'])&& isset($_GET['codeEtab'])&& isset($_GET['classeid']))
{
$teacherid=$_GET['id'];
$sessionEtab=$_GET['sessionEtab'];
$codeEtab=$_GET['codeEtab'];
$classeid=$_GET['classeid'];

$data = $etabs->getAllControleMatiereOfThisTeatcherIdClasse($teacherid,$codeEtab,$sessionEtab,$classeid);

echo $data;

}else 
{

if(isset($_GET['id']))

{

$teacherid=$_GET['id'];
  
  //echo $courseid." / ".$classeid." / ".$codeEtab." / ".$sessionEtab." / ".$teacherid;
  
$data = $etabs->getAllControleMatiereOfThisTeatcherId($teacherid);

  //$data = $etabs->getAllCoursesTea($teacherid,$codeEtab,$sessionEtab);
  echo $data;

}
}


  

}



?>

