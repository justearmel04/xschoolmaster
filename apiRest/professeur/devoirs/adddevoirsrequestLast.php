<?php

// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}



require_once('../../class/cnx.php');

require_once('../../class1/functions.php');

$db = new mysqlConnector();


if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Recuperation des donnees de la premiere etape : Infos Client

$postdata =$_POST['devoir'];
$request = json_decode($postdata);

$codeEtab=utf8_decode($request->codeEtab);
$sessionEtab=utf8_decode($request->sessionEtab);
$matiereid=utf8_decode($request->matiereid);
$libellematiere=utf8_decode($request->matierelib);
$teatcherid=utf8_decode($request->teatcherid);
$classeid=utf8_decode($request->classeid);
$libelleclasse=utf8_decode($request->classelib);
$datelimitedevoir=utf8_decode($request->datelimitedevoir);
$libelledevoir=utf8_decode($request->libelledevoir);
$verouillerdevoir=utf8_decode($request->verouillerdevoir);
$instructionsdevoir=utf8_decode($request->instructionsdevoir);

 $statutdevoir=0;
 $destinataires="ELEVE@PARENT";

 $date=date("Y-m-d");

   $tabdate=explode("-",$date);
   $years=$tabdate[0];
   $mois=$tabdate[1];
   $days=$tabdate[2];
   $libellemois=obtenirLibelleMois($mois);


   $code="INSERT INTO devoirs SET libelle_dev=?,datelimite_dev=?,classe_dev=?,matiere_dev=?,teatcher_dev=?,codeEtab_dev=?,sessionEtab_dev=?,instructions_dev=?,verouiller_dev=?,destinataires_dev=?,statut_dev=?";
   $req=$db->dataBase->prepare($code);
   $req->execute([$libelledevoir,$datelimitedevoir,$classeid,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$instructionsdevoir,$verouillerdevoir,$destinataires,$statutdevoir]);
   $devoirid=$db->dataBase->lastInsertId();

      $transactionId =  "DEVOIR_".date("Y")."_".$codeEtab."_".$classeid."_".$devoirid;

     $dossier="../../../devoirs/Etablissements/";
     $dossier1="../../../devoirs/Etablissements/".$codeEtab."/";
     $dossier2="../../../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere);
     $dossier3="../../../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/";

     if(!is_dir($dossier)) {
               @mkdir($dossier);
               if(!is_dir($dossier1)) {
                  @mkdir($dossier1);

                   if(!is_dir($dossier2)) {
                       @mkdir($dossier2);

                   }

                   if(!is_dir($dossier3)) {
                       @mkdir($dossier3);

                  }

               }
           }else {
             if(!is_dir($dossier1)) {
                 @mkdir($dossier1);

                 if(!is_dir($dossier2)) {
                     @mkdir($dossier2);

                 }

                 if(!is_dir($dossier3)) {
                     @mkdir($dossier3);

                 }

             }
           }



   $target_path = "../../../temp/";
 $target_path = $target_path . basename($_FILES['file']['name']);
 if(move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {

 $file_name = @$_FILES['file']['name'];

   $_SESSION["file"] = $file_name;

   $file_size =@$_FILES['file']['size'];

   $file_tmp =@$_FILES['file']['tmp_name'];

   $file_type=@$_FILES['file']['type'];

   @$file_ext=strtolower(end(explode('.',@$_FILES['file']['name'])));

   $fichierTemp = uniqid() . "." . $file_ext;

   $typesupport="DEVOIRS";

   $fichierad=$transactionId.".".$file_ext;

  @rename($target_path ,"../../../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/".$fichierad);

     $code1="INSERT INTO supports set courseid_support=?,matiereid_support=?,teatcherid_support=?,codeEtab_support=?,sessionEtab_support=?,classeid_support=?,fichier_support=?,type_support=?";
     $req1=$db->dataBase->prepare($code1);
     $req1->execute([$devoirid,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$classeid,$fichierad,$typesupport]);
     @unlink($target_path);



 echo json_encode('success');

 }else{

 echo json_encode('unsuccess upload');
 }


}



?>
