<?php

// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}


//require_once('../../class1/User.php');

//require_once('../../class1/cnx.php');

//require_once('../../class1/Parent.php');

//require_once('../../class1/Etablissement.php');

require_once('../../class/cnx.php');

require_once('../../class1/functions.php');

$db = new mysqlConnector();



//$users = new User();

//$parents=new ParentX();

//$etabs=new Etab();





if($_SERVER['REQUEST_METHOD'] === 'POST'){

//Recuperation des donnees de la premiere etape : Infos Client

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$codeEtab=$request->codeEtab;
$sessionEtab=$request->sessionEtab;
$matiereid=$request->matiereid;
$libellematiere=$request->matierelib;
$teatcherid=$request->teatcherid;
$classeid=$request->classeid;
$libelleclasse=$request->classelib;
$descri=$request->description;

$dateday=date("Y-m-d");

//nous allons ajouter le syllabus

$code="INSERT INTO syllabus SET idteatcher_syllab='$teatcherid',idclasse_syllab='$classeid',idmatiere_syllab='$matiereid',session_syllab='$sessionEtab',descri_syllab='$descri',addby_syllab='$teatcherid',datecrea_syllab='$dateday',codeEtab_syllab='$codeEtab'";
$req=$db->dataBase->prepare($code);
$req->execute([]);
//@$syllabusid=$this->db->lastInsertId();

//nous allons faire une requete pour retrouver l'id du syllabus

$code1="SELECT * from syllabus where idteatcher_syllab='$teatcherid' and idclasse_syllab='$classeid' and idmatiere_syllab='$matiereid' and session_syllab='$sessionEtab' and codeEtab_syllab='$codeEtab' and datecrea_syllab='$dateday'";
$req1=$db->dataBase->prepare($code1);
$req1->execute([]);
$datas=$req1->fetchAll();
/*
$array=json_encode($datas,true);
$someArray = json_decode($array, true);
$syllabusid=$someArray[0]["id_syllab"];

foreach($datas as values):
$syllabusid=values->id_syllab;
endforeach;*/

//nous allons ajouter les objectifs

$objectifs=$request->objectifs;
$themes=$request->themes;
$prerequis=$request->prerequis;
$competences=$request->competences;
$docs=$request->docs;
$docfacs=$request->docfacs;
$calendars=$request->calendars;
$evaluations=$request->evaluations;
$regelements=$request->regelements;

$nbobjectifs=count($objectifs);

if($nbobjectifs>0)
{
  foreach($objectifs as valueobj):
$libelleobj=valueobj->objectif;
$codeobj="INSERT INTO syllabobjet SET idsyllab_syllabob='$syllabusid',libelle_syllabob='$libelleobj',date_syllabob='$dateday'";
$reqobj=$db->dataBase->prepare($codeobj);
$reqobj->execute([]);

endforeach;
}

$nbthemes=count($themes);

if($nbthemes>0)
{
  foreach($themes as valuetheme):
$libelletheme=valuetheme->theme;
$codetheme="INSERT INTO syllabtheme SET idsyllab_syllabth='$syllabusid',libelle_syllabth='$libelletheme',date_syllabth='$dateday'";
$reqtheme=$db->dataBase->prepare($codetheme);
$reqtheme->execute([]);

endforeach;
}

$nbprerequis=count($prerequis);

if($nbprerequis>0)
{
  foreach($prerequis as valueprerequis):
$libelleprerequis=valueprerequis->requis;
$coderequis="INSERT INTO syllabrequis SET idsyllab_syllabreq='$syllabusid',libelle_syllabreq='$libelleprerequis',date_syllabreq='$dateday'";
$reqrequis=$db->dataBase->prepare($coderequis);
$reqrequis->execute([]);

endforeach;
}


$nbcompetences=count($competences);

if($nbcompetences>0)
{
  foreach($competences as valuecomp):
$libellecompetences=valuecomp->competence;
$codecomp="INSERT INTO syllabcomp SET idsyllab_syllabcomp='$syllabusid',libelle_syllabcomp='$libellecompetences',date_syllabcomp='$dateday'";
$reqcomp=$db->dataBase->prepare($codecomp);
$reqcomp->execute([]);

endforeach;
}


$nbdocs=count($docs);

if($nbdocs>0)
{
  foreach($docs as valuedocs):
$libelledocs=valuedocs->document;
$codedocs="INSERT INTO syllabdoc SET idsyllab_syllabdoc='$syllabusid',libelle_syllabdoc='$libelledocs',date_syllabdoc='$dateday'";
$reqdocs=$db->dataBase->prepare($codedocs);
$reqdocs->execute([]);

endforeach;
}

$nbdocfacs=count($docfacs);

if($nbdocfacs>0)
{
  foreach($docfacs as valuedocfacs):
$libelledocfacs=valuedocs->documentfac;
$facultatif=1;
$codedocfacs="INSERT INTO syllabdoc SET idsyllab_syllabdoc='$syllabusid',libelle_syllabdoc='$libelledocfacs',facultatif_syllabdoc='$facultatif',date_syllabdoc='$dateday'";
$reqdocfacs=$db->dataBase->prepare($codedocfacs);
$reqdocfacs->execute([]);

endforeach;
}

$nbcalendars=count($calendars);

if($nbcalendars>0)
{
  foreach($calendars as valuecalendars):
$dateCalandar=valuecalendars->date;
$seanceCalandar=valuecalendars->seance;
$contentCalandar=valuecalendars->contenu;
$workCalandar=valuecalendars->préalable;

$codecalendars="INSERT INTO syllabcalendar SET idsyllab_syllabcal='$syllabusid',date_syllabcal='$dateCalandar',seance_syllabcal='$seanceCalandar',contenu_syllabcal='$contentCalandar',prealable_syllabcal='$workCalandar',datecrea_syllabcal='$dateday'";
$reqcalendars=$db->dataBase->prepare($codecalendars);
$reqcalendars->execute([]);

endforeach;
}

$nbevaluations=count($evaluations);

if($nbevaluations>0)
{
  foreach($evaluations as valuecodeevaluations):
$dateEvaluation=valuecodeevaluations->date;
$typeEvaluation=valuecodeevaluations->type;
$competenceEvaluation=valuecodeevaluations->competence;
$ponderationEvaluation=valuecodeevaluations->pondération;

$codeevaluations="INSERT INTO syllabeval SET idsyllab_syllabeval='$syllabusid',date_syllabeval='$dateEvaluation',type_syllabeval='$typeEvaluation',competence_syllabeval='$competenceEvaluation',ponderation_syllabeval='$ponderationEvaluation',datecrea_syllabeval='$dateday'";
$reqevaluations=$db->dataBase->prepare($codeevaluations);
$reqevaluations->execute([]);

endforeach;
}


$nbregelements=count($regelements);

if($nbregelements>0)
{
  foreach($regelements as valueregelements):
$regle=valueregelements->regle;

$coderegelements="INSERT INTO syllabregle SET idsyllab_syllabregle='$syllabusid',libelle_syllabregle='$regle',date_syllabregle='$dateday'";
$reqregelements=$db->dataBase->prepare($coderegelements);
$reqregelements->execute([]);

endforeach;
}





echo json_encode('success');


}


?>

