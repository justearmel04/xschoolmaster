<div class="white-sidebar-color sidebar-container">
 				<div class="sidemenu-container navbar-collapse collapse fixed-menu">
	                <div id="remove-scroll" class="left-sidemenu">
	                    <ul class="sidemenu  page-header-fixed slimscroll-style" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
	                        <li class="sidebar-toggler-wrapper hide">
	                            <div class="sidebar-toggler">
	                                <span></span>
	                            </div>
	                        </li>
	                        <li class="sidebar-user-panel">
	                            <div class="user-panel">
	                                <div class="pull-left image">
	                                    <img src="<?php echo $lienphoto?>" class="img-circle user-img-circle" alt="User Image" />
	                                </div>
	                                <div class="pull-left info">
	                                    <p> <?php echo $tablogin[0];?></p>
	                                    <small>
                                        <?php

                                          if($tablogin[1]=="Admin_globale")
                                          {
                                            echo "Administrateur";
                                          }else if($tablogin[1]=="Admin_locale")
                                          {
                                            echo "Admin Locale";
                                          }else if($tablogin[1]=="Teacher")
                                          {
                                            echo "Enseignant";
                                          }else if($tablogin[1]=="Parent")
                                          {
                                            echo "Parent";
                                          }
                                        ?>
                                      </small>
	                                </div>
	                            </div>
	                        </li>
                          <li class="nav-item active open">
	                            <a href="index.php" class="nav-link nav-toggle">
	                                <i class="material-icons">dashboard</i>
	                                <span class="title">Dashboard</span>
	                                <span class="selected"></span>

	                            </a>

	                        </li>

	                        <li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
	                                <i class="material-icons">dashboard</i>
	                                <span class="title">Etablissements</span>
                                  <span class="arrow open"></span>

	                            </a>
	                            <ul class="sub-menu">

	                                <li class="nav-item" style="margin-left:-50px">
	                                    <a href="addschool.php" class="nav-link ">
	                                        <span class="title">Nouvel établissement</span>
	                                    </a>
	                                </li>

                                  <li class="nav-item  " class="nav-link nav-toggle" style="margin-left:-50px;">
                                      <a href="#" class="nav-link nav-toggle" >Liste établissements</a>
                                    <ul class="sub-menu" style="">
                                      <?php
                                      //le nombre d'etablissement dans le système
                                      $nbetab=$etabs->getNumberOfEtablissement();
                                        if($nbetab>0){
                                      $i=1;
                                        foreach ($alletab as $value):
                                       ?>
                                       <li class="nav-item" style="margin-left:5px">
                                           <a href="schoolInfos.php?compte=<?php echo $value->code_etab;?>" class="nav-link "> <span class="title"><?php echo $value->libelle_etab;?></span>
                                           </a>
                                       </li>

                                      <?php
                                      $i++;
                                      endforeach;
                                    }else {
                                      ?>
                                      <li class="nav-item" style="margin-left:5px">
                                          <a href="#" class="nav-link "> <span class="title">Aucun Etablissement</span>
                                          </a>
                                      </li>
                                      <?php
                                    }
                                       ?>
                                    </ul>
	                                    <!--a href="recapattendance.php" class="nav-link "> <span class="title">Récap des absences</span>
	                                    </a-->
	                                </li>
                                  <li class="nav-item" style="margin-left:-50px">
                                     <a href="importations.php" class="nav-link ">
                                         <span class="title">Importations</span>
                                     </a>
                                 </li>

	                            </ul>
	                        </li>


                          <li class="nav-item">
	                            <a href="javascript:;" class="nav-link nav-toggle">
	                                <i class="material-icons">dashboard</i>
	                                <span class="title">Elèves</span>
                                  <span class="arrow open"></span>

	                            </a>
	                            <ul class="sub-menu" >
                                <?php
                                if($nbetab>0)
                                {
                                  $i=1;
                                  foreach ($alletab as $valueEtab):
                                  ?>
                                  <li class="nav-item" style="margin-left:-50px">
                                      <a href="javascript:;" class="nav-link nav-toggle"> <span class="title"><?php echo $valueEtab->libelle_etab;?></span>
                                      </a>
                                      <ul class="sub-menu">
                                        <?php
                                        $nombreclasseinschool=$classe->getNumberOfClassesOfThisClasses($valueEtab->code_etab);
                                        if($nombreclasseinschool>0)
                                        {
                                          $specificlasses=$classe->getAllClassesOfThisSchool($valueEtab->code_etab);

                                          $j=1;
                                          foreach ($specificlasses as $value):
                                          ?>
                                          <li class="nav-item">
                                            <li class="nav-item" style="margin-left:10px">
                                                <a href="showInfosclasse.php?classe=<?php echo $value->id_classe;?>&codeEtab=<?php echo $valueEtab->code_etab?>" class="nav-link "> <span class="title"><?php echo $value->libelle_classe;?></span>
                                                </a>
                                            </li>
                                          </li>
                                          <?php
                                          $j++;
                                          endforeach;

                                        }else {
                                          ?>
                                          <li class="nav-item" style="margin-left:5px">
                                              <a href="#" class="nav-link "> <span class="title">Aucune Classe</span>
                                              </a>
                                          </li>
                                          <?php
                                        }
                                         ?>
                                      </ul>


                                  </li>
                                  <?php
                                  $i++;
                                  endforeach;



                                }else {
                                  ?>
                                  <li class="nav-item" style="margin-left:-50px">
                                      <a href="#" class="nav-link "> <span class="title">Aucun Etablissement</span>
                                      </a>
                                  </li>
                                  <?php
                                }
                                ?>


	                            </ul>
	                        </li>
                          <li class="nav-item">
	                            <a href="javascript:;" class="nav-link nav-toggle">
	                                <i class="material-icons">dashboard</i>
	                                <span class="title">Absences</span>
                                  <span class="arrow open"></span>

	                            </a>
	                            <ul class="sub-menu" >
                                <?php
                                if($nbetab>0)
                                {

                                $i=1;
                                  foreach ($alletab as $value):
                                 ?>
                                 <!--li class="nav-item" style="margin-left:-50px">
                                     <a href="dailyattendance.php?codeEtab=<?php //echo $value->code_etab;?>" class="nav-link "> <span class="title"><?php //echo $value->libelle_etab;?></span>
                                     </a>
                                 </li-->
                                 <li class="nav-item  " class="nav-link nav-toggle" style="margin-left:-50px;">
                                     <a href="#" class="nav-link nav-toggle" ><?php echo $value->libelle_etab;?></a>
                                   <ul class="sub-menu" style="">
                                     <li class="nav-item">
                                         <a href="dailyattendance.php?codeEtab=<?php echo $value->code_etab;?>" class="nav-link "> <span class="title">Consigner les absences</span>
                                         </a>
                                     </li>
                                     <li class="nav-item  " class="nav-link nav-toggle" style="margin-left:1px;">
                                         <a href="#" class="nav-link nav-toggle" >Recap des absences</a>

                                       <ul class="sub-menu" style="margin-left:-15px;">
                                         <li class="nav-item" ><a href="recapattendanceclasse.php?codeEtab=<?php echo $value->code_etab;?>">Classe</a></li>
                                         <li class="nav-item"><a href="recapattendanceclasseeleves.php?codeEtab=<?php echo $value->code_etab;?>">Eleves</a> </li>
                                         <li class="nav-item"> <a href="recapattendanceclassematiere.php?codeEtab=<?php echo $value->code_etab;?>">Matières</a> </li>
                                       </ul>
                                         <!--a href="recapattendance.php" class="nav-link "> <span class="title">Récap des absences</span>
                                         </a-->
                                     </li>
                                     <li class="nav-item  " class="nav-link nav-toggle">
                                         <a href="#" class="nav-link nav-toggle" >Courbe des absences</a>
                                       <ul class="sub-menu" style="">
                                         <li class="nav-item" ><a href="attendancegraphclasse.php?codeEtab=<?php echo $value->code_etab;?>">Classe</a></li>
                                         <li class="nav-item"><a href="attendancegraph.php?codeEtab=<?php echo $value->code_etab;?>">Eleves</a> </li>

                                       </ul>
   	                                    <!--a href="recapattendance.php" class="nav-link "> <span class="title">Récap des absences</span>
   	                                    </a-->
   	                                </li>
                                   </ul>
                                     <!--a href="recapattendance.php" class="nav-link "> <span class="title">Récap des absences</span>
                                     </a-->
                                 </li>

                                <?php
                                $i++;
                                endforeach;
                                 ?>

                                 <!--li class="nav-item" style="margin-left:-50px">
                                     <a href="recapattendance.php" class="nav-link "> <span class="title">RECAP PRESENCES</span>
                                     </a>
                                 </li-->
                                 <?php
                               }else {
                                 ?>
                                 <li class="nav-item" style="margin-left:-50px">
                                     <a href="#" class="nav-link "> <span class="title">Aucun Etablissement</span>
                                     </a>
                                 </li>
                                 <?php
                               }
                                  ?>
	                            </ul>
	                        </li>
                          <li class="nav-item">
	                            <a href="javascript:;" class="nav-link nav-toggle">
	                                <i class="material-icons">dashboard</i>
	                                <span class="title">Evaluations et notes</span>
                                  <span class="arrow open"></span>

	                            </a>
	                            <ul class="sub-menu" >

                                <?php
                                  if($nbetab>0)
                                  {
                                $i=1;
                                  foreach ($alletab as $value):
                                 ?>
                                 <li class="nav-item  " class="nav-link nav-toggle" style="margin-left:-50px;">
                                     <a href="#" class="nav-link nav-toggle" ><?php echo $value->libelle_etab;?></a>
                                   <ul class="sub-menu" style="">

                                     <li class="nav-item  " class="nav-link nav-toggle">
                                         <a href="#" class="nav-link nav-toggle" >Consigner une note</a>
                                       <ul class="sub-menu" style="">
                                         <li class="nav-item" ><a href="addnotes.php?codeEtab=<?php echo $value->code_etab;?>">Classe</a></li>
                                         <li class="nav-item"><a href="addnoteseleves.php?codeEtab=<?php echo $value->code_etab;?>">Eleves</a> </li>

                                       </ul>
                                         <!--a href="recapattendance.php" class="nav-link "> <span class="title">Récap des absences</span>
                                         </a-->
                                     </li>
                                     <li class="nav-item  ">
                                         <a href="examens.php?codeEtab=<?php echo $value->code_etab;?>" class="nav-link "> <span class="title">Examens</span>
                                         </a>
                                     </li>
                                     <li class="nav-item  ">
                                         <a href="controles.php?codeEtab=<?php echo $value->code_etab;?>" class="nav-link "> <span class="title">Contrôles</span>
                                         </a>
                                     </li>
                                     <li class="nav-item  " class="nav-link nav-toggle">
                                         <a href="#" class="nav-link nav-toggle" >Récap des notes</a>
                                       <ul class="sub-menu" style="">
                                         <li class="nav-item" ><a href="notes.php?codeEtab=<?php echo $value->code_etab;?>">Classe</a></li>
                                         <li class="nav-item"><a href="noteseleves.php?codeEtab=<?php echo $value->code_etab;?>">Eleves</a> </li>

                                       </ul>
   	                                    <!--a href="recapattendance.php" class="nav-link "> <span class="title">Récap des absences</span>
   	                                    </a-->
   	                                </li>
                                    <!--li class="nav-item  ">
                                        <a href="validations.php?codeEtab=<?php //echo $value->code_etab;?>" class="nav-link "> <span class="title">Validation modification</span>
                                        </a>
                                    </li-->



                                   </ul>
                                     <!--a href="recapattendance.php" class="nav-link "> <span class="title">Récap des absences</span>
                                     </a-->
                                 </li>

                                <?php
                                $i++;
                                endforeach;
                              }else {
                                ?>
                                <li class="nav-item" style="margin-left:-50px">
                                    <a href="" class="nav-link "> <span class="title">Aucun Etablissement</span>
                                    </a>
                                </li>
                                <?php
                              }
                                 ?>


	                            </ul>
	                        </li>
                          <!--li class="nav-item">
	                            <a href="javascript:;" class="nav-link nav-toggle">
	                                <i class="material-icons">dashboard</i>
	                                <span class="title">Evaluations et notes</span>
                                  <span class="arrow open"></span>

	                            </a>
	                            <ul class="sub-menu" >

                                <?php
                                  // if($nbetab>0)
                                  {
                                // $i=1;
                                //   foreach ($alletab as $value):
                                 ?>
                                 <li class="nav-item" style="margin-left:-50px">
                                     <a href="javascript:;" class="nav-link nav-toggle"> <span class="title"><?php// echo $value->libelle_etab;?></span>
                                     </a>
                                     <ul class="sub-menu">
                                       <li class="nav-item" style="">
                                           <a href="notes.php?codeEtab=<?php //echo $value->code_etab;?>" class="nav-link "> <span class="title">Toutes les Notes</span>
                                           </a>
                                       </li>
                                       <li class="nav-item" style="">
                                           <a href="addnotes.php?codeEtab=<?php //echo $value->code_etab;?>" class="nav-link "> <span class="title">Ajouter une Note</span>
                                           </a>
                                       </li>
                                     </ul>
                                 </li>

                                <?php
                              //   $i++;
                              //   endforeach;
                              // }else {
                                ?>
                                <li class="nav-item" style="margin-left:-50px">
                                    <a href="" class="nav-link "> <span class="title">Aucun Etablissement</span>
                                    </a>
                                </li>
                                <?php
                              }
                                 ?>


	                            </ul>
	                        </li-->
                          <li class="nav-item">
	                            <a href="javascript:;" class="nav-link nav-toggle">
	                                <i class="material-icons">dashboard</i>
	                                <span class="title">Emploi de temps</span>
                                  <span class="arrow open"></span>

	                            </a>
	                            <ul class="sub-menu" >
                                <?php
                                //$alletab=$etabs->getAllEtab();
                                if($nbetab>0)
                                {
                                $i=1;
                                  foreach ($alletab as $value):
                                 ?>
                                 <li class="nav-item" style="margin-left:-50px">
                                     <a href="javascript:;" class="nav-link nav-toggle"> <span class="title"><?php echo $value->libelle_etab;?></span>
                                     </a>
                                     <ul class="sub-menu">
                                       <?php
                                        $classesSchool=$classe->getAllClassesOfThisSchool($value->code_etab);
                                        $j=1;
                                          foreach ($classesSchool as $valueSku):
                                        ?>
                                        <li class="nav-item" style="">
                                            <a href="routines.php?classe=<?php echo $valueSku->id_classe;?>&codeEtab=<?php echo $valueSku->codeEtab_classe;?>" class="nav-link "> <span class="title"><?php echo $valueSku->libelle_classe;?></span>
                                            </a>
                                        </li>
                                        <?php
                                        $j++;
                                        endforeach;
                                         ?>
                                     </ul>
                                 </li>

                                <?php
                                $i++;
                                endforeach;
                              }else {
                                ?>
                                <li class="nav-item" style="margin-left:-50px">
                                    <a href="" class="nav-link "> <span class="title">Aucun Etablissement</span>
                                    </a>
                                </li>
                                <?php
                              }
                                 ?>


	                            </ul>
	                        </li>
                          <li class="nav-item">
                              <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="material-icons">dashboard</i>
                                  <span class="title">Souscriptions</span><span class="badge badge-success"><?php echo $classe->getNumberOfsatandbypaiement(); ?></span>
                                  <span class="arrow open"></span>
                              </a>
                              <ul class="sub-menu" >

                                 <li class="nav-item" style="margin-left:-50px">
                                     <a href="souscriptions.php" class="nav-link nav-toggle"> <span class="title">Souscriptions</span><span class="badge badge-success"><?php echo $classe->getNumberOfsatandbypaiement(); ?></span>
                                     </a>

                                 </li>

                                 <li class="nav-item" style="margin-left:-50px">
                                     <a href="paiementstory.php" class="nav-link nav-toggle"> <span class="title">Historique Paiement</span>
                                     </a>

                                 </li>
                                 <li class="nav-item" style="margin-left:-50px">
                                     <a href="activations.php" class="nav-link nav-toggle"> <span class="title">Activation / Désactivation</span>
                                     </a>

                                 </li>




                              </ul>
                          </li>

                          <li class="nav-item  ">
	                            <a href="#" class="nav-link nav-toggle"> <i class="material-icons">dashboard</i>
	                                <span class="title">Notifications</span>
                                  <span class="selected"></span>
                                 <span class="arrow open"></span>
	                            </a>
	                            <ul class="sub-menu" style="margin-left:-40px;">

	                                <li class="nav-item  ">
	                                    <a href="addmessages.php" class="nav-link "> <span class="title">Nouvelle notification</span>
	                                    </a>
	                                </li>
                                  <li class="nav-item  ">
                                      <a href="allmessages.php" class="nav-link "> <span class="title">Liste notification</span>
                                      </a>
                                  </li>


	                            </ul>
	                        </li>
                          <!--li class="nav-item">
                            <a href="parascolaires.php" class="nav-link">
                                <i class="material-icons">dashboard</i>
                                <span class="title">PARASCOLAIRES</span>

                            </a>


	                        </li-->

                          <li class="nav-item  ">
	                            <a href="#" class="nav-link nav-toggle"> <i class="material-icons">dashboard</i>
	                                <span class="title">Activités parascolaires</span>
                                  <span class="selected"></span>
                                 <span class="arrow open"></span>
	                            </a>
	                            <ul class="sub-menu" style="margin-left:-40px;">

	                                <li class="nav-item  ">
	                                    <a href="addparascolaires.php" class="nav-link "> <span class="title">Nouvelle activité</span>
	                                    </a>
	                                </li>
                                  <li class="nav-item  ">
                                      <a href="allparascolaires.php" class="nav-link "> <span class="title">Liste des activités</span>
                                      </a>
                                  </li>


	                            </ul>
	                        </li>
                          <li class="nav-item">
	                            <a href="javascript:;" class="nav-link nav-toggle">
	                                <i class="material-icons">dashboard</i>
	                                <span class="title">Utilisateurs</span>
                                  <span class="arrow open"></span>

	                            </a>
	                            <ul class="sub-menu">
                                <li class="nav-item " style="margin-left:-30px">
                                    <a href="#" class="nav-link nav-toggle">
                                        <span class="title">Admin Globale</span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item  ">
                                            <a href="admins.php" class="nav-link "> <span class="title">Tous les Admin globale</span>
                                            </a>
                                        </li>
                                        <li class="nav-item  ">
                                            <a href="addAdmin.php" class="nav-link "> <span class="title">Ajouter Admin globale</span>
                                            </a>
                                        </li>


                                    </ul>
                                </li>
                                <li class="nav-item" style="margin-left:-30px">
                                    <a href="#" class="nav-link nav-toggle">
                                    <span class="title">Admin Locale</span></a>
                                    <ul class="sub-menu">
                                        <li class="nav-item  ">
                                            <a href="localadmins.php" class="nav-link "> <span class="title">Tous les Admin Locale</span>
                                            </a>
                                        </li>
                                        <li class="nav-item  ">
                                            <a href="addlocal.php" class="nav-link "> <span class="title">Ajouter Admin Locale</span>
                                            </a>
                                        </li>



                                    </ul>
                                </li>
                                <li class="nav-item" style="margin-left:-30px">
      	                            <a href="#" class="nav-link nav-toggle">
      	                                <span class="title">Professeurs</span>
                                        <span class="arrow open"></span>
      	                            </a>
      	                            <ul class="sub-menu">
      	                                <li class="nav-item  ">
      	                                    <a href="teatchers.php" class="nav-link "> <span class="title">Tous les Professeurs</span>
      	                                    </a>
      	                                </li>
      	                                <li class="nav-item  ">
      	                                    <a href="addteatcher.php" class="nav-link "> <span class="title">Ajouter Professeur</span>
      	                                    </a>
      	                                </li>


      	                            </ul>
      	                        </li>
      	                        <li class="nav-item" style="margin-left:-30px">
      	                            <a href="#" class="nav-link nav-toggle">
      	                                <span class="title">Parents</span>
      	                            </a>
      	                            <ul class="sub-menu">
      	                                <li class="nav-item  ">
      	                                    <a href="parents.php" class="nav-link "> <span class="title">Tous les parents</span>
      	                                    </a>
      	                                </li>
      	                                <li class="nav-item  ">
      	                                    <a href="addparent.php" class="nav-link "> <span class="title">Ajouter parent</span>
      	                                    </a>
      	                                </li>


      	                            </ul>
      	                        </li>

	                            </ul>
	                        </li>


                </div>
            </div>
