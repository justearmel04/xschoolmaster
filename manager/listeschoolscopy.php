<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Admin.php');
require_once('../class/Classe.php');
$classe=new Classe();

$emailUti=$_SESSION['user']['email'];

$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$admin=new Admin();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$pays=$etabs->getAllCountriesOfSystem();

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
 <head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<title><?php echo L::Titlepage?></title>
<meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
<meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

<!-- google font -->
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
<!-- icons -->
<link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
<!--bootstrap -->
<!--bootstrap -->
<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<!-- Material Design Lite CSS -->
<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
<link href="../assets2/css/material_style.css" rel="stylesheet">
<!-- Theme Styles -->
<link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
<link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
<link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
<link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
<link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <style>
    #radioBtn .notActive{
    color: #3276b1;
    background-color: #fff;
    }

    #radioBtn1 .notActive{
    color: #3276b1;
    background-color: #fff;
    }
    </style>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title">Liste des Etablissements</div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#">Ecoles</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active">Liste des Etablissements</li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addetabok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: 'Félicitations !',
  text: "<?php echo $_SESSION['user']['addetabok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::AddNews ?>',
  cancelButtonText: 'Annuler',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="index.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addetabok']);
                    }

                     ?>

              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <div class="card card-box">
                          <div class="card-head">
                              <header></header>
                              
                          </div>

                          <div class="card-body" id="bar-parent">
                              <form  id="FormAddEtab" class="form-horizontal" action="../controller/school.php" method="post" enctype="multipart/form-data">
                                  <div class="form-body">
                                    <div class="form-group row">
                                            <label class="control-label col-md-3">Pays
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-5">

                                                <select class="form-control" name="pays" id="pays" style="width:100%;" >
                                                    <option value="">Selectionner une Pays</option>
                                                    <?php
                                                    $i=1;
                                                      foreach ($pays as $value):
                                                      ?>
                                                      <option value="<?php echo utf8_encode(utf8_decode($value->id_pays)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_pays)); ?></option>

                                                      <?php
                                                                                       $i++;
                                                                                       endforeach;
                                                                                       ?>

                                                </select>
                                               </div>
                                        </div>
                                        <div class="form-group row">
                                                <label class="control-label col-md-3"><?php echo L::Name?>bre d'établissement
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">

                                                    <input type="text" name="nbschool" id="nbschool" value="">
                                                   </div>
                                            </div>
                                  <div class="form-group row">
                                          <label class="control-label col-md-3">Code Etablissement
                                              <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-5">
                                              <input type="text" name="codeetab" id="codeetab" data-required="1" placeholder="Entrer le Code Etablissement" class="form-control " /> </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3">Libellé Etablissement
                                              <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-5">
                                              <input type="text" name="libetab" id="libetab" data-required="1" placeholder="Entrer le libellé Etablissement" class="form-control " /> </div>
                                      </div>
                                      <div class="form-group row">
                                              <label class="control-label col-md-3">Type Etablissement
                                                  <span class="required"> * </span>
                                              </label>
                                              <div class="col-md-5">

                                                  <select class="form-control " name="typeetab" id="typeetab" style="width:100%;" onchange="checktypeEtab()" >
                                                      <option value="">Selectionner un type etablissement</option>
                                                      <option value="1">Etablissement Primaire</option>
                                                      <option value="2">Etablissement Secondaire</option>
                                                      <option value="3">Etablissement Maternel</option>
                                                      <option value="4">Etablissement Universitaire</option>
                                                      <option value="5">Etablissement Mixte</option>

                                                  </select>
                                                 </div>
                                          </div>
                                          <div class="form-group row" id="precisionRow">
                                                  <label class="control-label col-md-3">Précisions
                                                      <span class="required"> * </span>
                                                  </label>
                                                  <div class="col-md-5">

                                                      <select class="form-control " id="precision" multiple="multiple"  name="precision[]" style="width:100%;" onchange="compter()">
                                                          <option value="">Selectionner les types etablissements</option>
                                                          <option value="1">Etablissement Primaire</option>
                                                          <option value="2">Etablissement Secondaire</option>
                                                          <option value="3">Etablissement Maternel</option>
                                                          <option value="4">Etablissement Universitaire</option>
                                                          </select>
                                                     </div>
                                              </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3">Contact 1 Etablissement

                                          </label>
                                          <div class="col-md-5">
                                              <input name="etabphone1" id="etabphone1" type="text" placeholder="Entrer le contact 1 Etablissement" class="form-control " /> </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3">Contact 2 Etablissement
                                              <span class="required">  </span>
                                          </label>
                                          <div class="col-md-5">
                                              <input name="etabphone2" id="etabphone2" type="text" placeholder="Entrer le contact 2 Etablissement" class="form-control " /> </div>
                                      </div>

                                      <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::EmailstudentTab?>
                                          </label>
                                          <div class="col-md-5">
                                              <div class="input-group">
                                                  <span class="input-group-addon">
                                                          <i class="fa fa-envelope"></i>
                                                      </span>
                                                  <input type="text" class="form-control " name="emailetab" id="emailetab" placeholder="Email Address"> </div>
                                          </div>
                                      </div>

                                     <div class="form-group row">
                                          <label class="control-label col-md-3">Adresse

                                          </label>
                                          <div class="col-md-5">
                                              <textarea name="addresetab"  id="addresetab" placeholder="adresse" class="form-control-textarea" rows="5" ></textarea>
                                          </div>
                                      </div>
                                      <div class="form-group row">
                                        <label class="control-label col-md-3">
                                        <span class="required"> </span>
                                        </label>
                                        <label class="col-sm-7 col-md-7">Existe t-il une Cantine scolaire ?
                                        <span class="required"> </span>
                                        </label>
                                      </div>

                                      <div class="form-group row">

                                <label class="control-label col-md-3">
                                <span class="required"> </span>
                                </label>
                                <div class="col-sm-7 col-md-7">
                                <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                <a class="btn btn-primary btn-sm active" data-toggle="cantine" data-title="0" onclick="cantineN()">NON</a>
                                <a class="btn btn-primary btn-sm notActive" data-toggle="cantine" data-title="1" onclick="cantine()">OUI</a>
                                </div>
                                <input type="hidden" name="cantine" id="cantine" value="0">
                                </div>
                                </div>
                                </div>

                                <div class="form-group row">
                                  <label class="control-label col-md-3">
                                  <span class="required"> </span>
                                  </label>
                                  <label class="col-sm-7 col-md-7">Existe t-il un car de transport scolaire ?
                                  <span class="required"> </span>
                                  </label>
                                </div>

                                <div class="form-group row">

                          <label class="control-label col-md-3">
                          <span class="required"> </span>
                          </label>
                          <div class="col-sm-7 col-md-7">
                          <div class="input-group">
                          <div id="radioBtn1" class="btn-group">
                          <a class="btn btn-primary btn-sm active" data-toggle="transportcar" data-title="0" onclick="transportcarN()">NON</a>
                          <a class="btn btn-primary btn-sm notActive" data-toggle="transportcar" data-title="1" onclick="transportcar()">OUI</a>
                          </div>
                          <input type="hidden" name="transportcar" id="transportcar" value="0">
                          </div>
                          </div>
                          </div>

                                      <div class="form-group row">
                                          <label class="control-label col-md-3">Logo Etablissement

                                          </label>
                                          <div class="compose-editor">
                                            <input type="file" id="logoetab" name="logoetab" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="../assets2/img/bg-01.jpg" data-allowed-file-extensions="gif png jpg jpeg pjpeg" data-errors-position="outside" data-show-errors="true" />
                                            <input type="hidden" name="etape" id="etape" value="1"/>

                                        </div>
                                      </div>

                <div class="form-actions">
                                      <div class="row">
                                          <div class="offset-md-3 col-md-9">
                                              <button type="submit" class="btn btn-info" id="submitbtn">Enregistrer</button>
                                              <button type="button" class="btn btn-danger">Annuler</button>
                                          </div>
                                        </div>
                                     </div>
              </div>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

 <script>

 $('#radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);

    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
})

$('#radioBtn1 a').on('click', function(){
   var sel = $(this).data('title');
   var tog = $(this).data('toggle');
   $('#'+tog).prop('value', sel);

   $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
   $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
})

function checktypeEtab()
{
  var typeetab=$("#typeetab").val();
  if(typeetab==5)
  {
    $("#precisionRow").show();
    $("#submitbtn").attr("disabled",true);
    $('#etape').val(3);
  }else {
$("#precisionRow").hide();
$("#submitbtn").attr("disabled",false);
$('#etape').val(1);
  }
}

 function cantineN()
 {
   $("#cantine").val(0);
 }

 function cantine()
 {
 $("#cantine").val(1);
 }

 function transportcar()
 {
$("#transportcar").val(1);
 }

 function transportcarN()
 {
$("#transportcar").val(0);
 }

 function compter()
 {
   var typeetab=$("#typeetab").val();
   var precision=$("#precision").val();
   if($("#precision").val().length>0)
   {
     $("#submitbtn").attr("disabled",false);
   }else {
     $("#submitbtn").attr("disabled",true);
   }

//    if(typeetab==5)
//    {
// //$("#submitbtn").attr("disabled",true);
// document.getElementById("submitbtn").disabled = true;
//    }else {
//  // $("#submitbtn").attr("disabled",false);
//  document.getElementById("submitbtn").disabled = false;
//    }
 }

 $("#pays").select2();
 $("#typeetab").select2();
 $("#precision").select2({
   tags: true,
 tokenSeparators: [',', ' ']
 });


 jQuery(document).ready(function() {

   $("#precisionRow").hide();

   $("#FormAddEtab").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

        codeetab:"required",
        libetab:"required",
        precision:"required",
        // addresetab:"required",
        logoetab:"required",
        pays:"required",
        typeetab:"required"

      },
      messages: {
        codeetab:"Merci de renseigner le code établissement",
        libetab:"Merci de renseigner le libellé établissement",
        etabphone1:"Merci de renseigner le contact 1",
        precision:"Merci de selectionner les différents type d'etablissement",
        addresetab:"Merci de renseigner l'adresse de l'établissement",
        logoetab:"Merci de choisir un fichier",
        pays:"Merci de selectionner le pays",
        typeetab:"Merci de selectionner le type d'etablissement"
       },

       submitHandler: function(form) {

         var etape=1;
         $.ajax({

           url: '../ajax/school.php',
           type: 'POST',
           async:false,
           data: 'codeetab=' + $("#codeetab").val()+ '&etape=' + etape+ '&pays=' + $("#pays").val(),
           dataType: 'text',
           success: function (content, statut) {

             if(content==1)
             {
              //showSwal('basic');
              Swal.fire({
  type: 'warning',
  title: 'Attention',
  text: 'Un Etablissemnt existe dejà avec ce code',

})

             }else if(content==0) {
               form.submit();
             }

           }
         });

       }


   });


 });
 </script>
    <!-- end js include path -->
  </body>

</html>
