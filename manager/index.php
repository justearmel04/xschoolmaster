<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Admin.php');
require_once('../class/Classe.php');

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$admin=new Admin();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Dashboard</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Dashboard</li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


          <div class="row">
            <div class="col-md-8 col-sm-12">
                 <div class="card-box">
                     <div class="card-head">
                         <header>Calendar</header>
                     </div>
                     <div class="card-body ">
                      <div class="panel-body">
                                <div id="calendar" class="has-toolbar"> </div>
                            </div>
                     </div>
                 </div>
             </div>
             <div class="col-md-4 col-sm-12">

               <div class="state-overview">
     						<div class="row">
                   <div class="col-lg-12 col-sm-12">
                 <div class="overview-panel blue-bgcolor">
                   <div class="symbol">
                     <i class="material-icons f-left">account_balance</i>
                   </div>
                   <div class="value white">
                     <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php echo $tabstat[0];?>"><?php echo $admin->getAllEtabOfThisSystem();?></p>
                     <p><?php echo strtoupper("Ecoles") ?></p>
                   </div>
                 </div>
               </div>
     					        <!-- /.col -->
                      <div class="col-lg-12 col-sm-12">
     								<div class="overview-panel orange">
     									<div class="symbol">
     										<i class="material-icons f-left">group</i>
     									</div>
     									<div class="value white">
     										<p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php echo $tabstat[1];?>"><?php echo $admin->getAllAdminLocalOfThisSystem();?></p>
     										<p><?php echo strtoupper("Admin locale") ?></p>
     									</div>
     								</div>
     							</div>
     					        <!-- /.col -->
                       <div class="col-lg-12 col-sm-12">
     								<div class="overview-panel deepPink-bgcolor">
     									<div class="symbol">
     										<i class="material-icons f-left">school</i>
     									</div>
     									<div class="value white">
     										<p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php echo $tabstat[2];?>"><?php echo $admin->getAllTeatcherOfThisSystem();?></p>
     										<p><?php echo strtoupper("Enseignants") ?></p>
     									</div>
     								</div>
     							</div>
                  <div class="col-lg-12 col-sm-12">
                <div class="overview-panel blue-bgcolor">
                  <div class="symbol">
                    <i class="material-icons f-left">account_balance</i>
                  </div>
                  <div class="value white">
                    <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php echo $tabstat[0];?>"><?php echo $admin->getAllStudentOfThisSystem();?></p>
                    <p><?php echo strtoupper("Eleves") ?></p>
                  </div>
                </div>
              </div>
     					        <!-- /.col -->
                       <div class="col-lg-12 col-sm-12">
     								<div class="overview-panel purple">
     									<div class="symbol">
     											<i class="material-icons f-left">person</i>
     									</div>
     									<div class="value white">
     										<p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php echo $tabstat[3];?>"><?php echo $admin->getAllParentOfThisSystem();?></p>
     									<p><?php echo strtoupper("Parents") ?></p>
     									</div>
     								</div>
     							</div>
                  <!--div class="col-lg-12 col-sm-12">
               <div class="overview-panel purple">
                 <div class="symbol">
                     <i class="material-icons f-left">person</i>
                 </div>
                 <div class="value white">
                   <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php //echo $tabstat[3];?>"><?php //echo $admin->getAllpresencesOfThisDay();?></p>
                 <p><?php //echo strtoupper("Presences du jour") ?></p>
                 </div>
               </div>
             </div-->
     					        <!-- /.col -->
     					      </div>
     						</div>


             </div>
                    </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   $(document).ready(function() {



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
