<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$codesEtab=$etabs->getAllcodesEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$etablissemementNb=$etabs->getNumberOfEtablissement();

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Etablissements</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="#">Ecoles</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Tous les Etablissements</li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['updateetabok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: 'Félicitations',
            text: '<?php echo $_SESSION['user']['updateetabok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['updateetabok']);
                }

                 ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

                 <?php
                 if($etablissemementNb==0)
                 {
                   ?>
                   <div class="alert alert-danger alert-dismissible fade show" role="alert">
                     Aucun Etablissement existe dans le système. Merci de proceder à la création d'un établissement
                  <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                    </a>
                  </div>
                   <?php
                 }else {
                   ?>
                   <div class="row">
                     <div class="col-md-12 col-sm-12">
                                               <div class="card card-box">
                                                   <div class="card-head">
                                                       <header>Rechercher</header>

                                                   </div>
                                                   <div class="card-body " id="bar-parent">
                                                       <form method="post" id="FormSearch">
                                                           <div class="row">
                                                             <div class="col-md-6 col-sm-6">
                   	                                        <!-- text input -->
                   	                                        <div class="form-group">
                   	                                            <label>Code Etablissemnt</label>
                   	                                            <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                                                                 <select class="form-control input-height" id="codeetab" name="codeetab" onchange="searchlibetab()">
                                                                     <option>Selectionner un code etablissement</option>
                                                                     <?php
                                                                     $i=1;
                                                                       foreach ($codesEtab as $value):
                                                                       ?>
                                                                       <option value="<?php echo $value->code_etab?>"><?php echo $value->code_etab?></option>

                                                                       <?php
                                                                                                        $i++;
                                                                                                        endforeach;
                                                                                                        ?>

                                                                 </select>
                                                             </div>
                   	                                        <div class="form-group">
                   	                                            <label>Période Du </label>
                   	                                            <input type="date" id="datedu" name="datedu" class="form-control" placeholder="Enter ..." >
                   	                                        </div>

                   	                                    </div>
                                                         <div class="col-md-6 col-sm-6">
                                                         <!-- text input -->
                                                         <div class="form-group">
                                                             <label>Nom Etablissement</label>
                                                             <select class="form-control input-height" id="libetab" name="libetab" style="width:100%">
                                                                 <option>Selectionner un code etablissement</option>
                                                                 <?php
                                                                 $i=1;
                                                                   foreach ($codesEtab as $value):
                                                                   ?>
                                                                   <option value="<?php echo utf8_encode(utf8_decode($value->libelle_etab)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_etab)); ?></option>

                                                                   <?php
                                                                                                    $i++;
                                                                                                    endforeach;
                                                                                                    ?>

                                                             </select>
                                                         </div>
                                                         <div class="form-group">
                                                             <label>Au</label>
                                                             <input type="date" id="dateau" name="dateau" class="form-control" placeholder="Enter ..." >
                                                         </div>

                                                         <input type="hidden" name="search" id="search"/>

                                                     </div>
                                                           </div>

                                                           <button type="submit" class="btn btn-success">Rechercher</button>
                                                       </form>
                                                   </div>
                                               </div>
                                           </div>
                   </div>
                             <div class="row">
                                           <div class="col-md-12">
                                               <div class="card  card-box">
                                                   <div class="card-head">
                                                       <header>LISTE DES ETABLISSEMENTS</header>
                                                       <div class="tools">
                                                           <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                   	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                   	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                                       </div>
                                                   </div>
                                                   <div class="card-body ">

                                                       <div class="pull-right">
                                                          <a class="btn btn-primary " style="border-radius:5px;" href="addschool.php"><i class="fa fa-plus"></i> Nouveau etablissement</a>
                                                           <a class="btn btn-warning " style="border-radius:5px;" href="#"><i class="fa fa-print"></i> Liste Etablissement</a>

                                                       </div>
                                                       <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                                           <thead>
                                                               <tr>

                                                                   <th>  </th>
                                                                   <th> Code Etab </th>
                                                                   <th> Libellé </th>
                                                                   <th> Contact(s) </th>
                                                                   <th> Email </th>
                                                                   <th> Date création </th>
                                                                   <th> <?php echo L::Actions?> </th>
                                                               </tr>
                                                           </thead>
                                                           <tbody>
                                                             <?php

                                                                 $i=1;
                                                                 if(isset($_POST['search']))
                                                                 {
                                                                     if(strlen($_POST['datedu'])>0)
                                                                     {
                                                                       //recherche de l'etabissement en fonction  de la période

                                                                       $alletab=$etabs->getAllAdminLocalBysearchperiode($_POST['datedu'],$_POST['dateau']);
                                                                     }else {
                                                                       // recherche de l'etablissement en fonction du code

                                                                       $alletab=$etabs->getAllAdminLocalBysearchCode($_POST['codeetab']);
                                                                     }
                                                                 }

                                                                   foreach ($alletab as $value):
                                                                   ?>
                                                               <tr class="odd gradeX">

                                                                   <td> <span class="photo">
                                                                     <?php
                                                                        $image="../logo_etab/".$value->code_etab."/".$value->logo_etab;
                                                                     ?>
                                                                   	<img src="<?php echo $image?>" class="img-circle" style="height:70px;width:70%" alt="">
                                                                    </span> </td>
                                                                   <td>
                                                                       <?php echo $value->code_etab?>
                                                                   </td>
                                                                   <td>
                                                                       <span class="label label-sm label-default"><?php echo $value->libelle_etab?>  </span>
                                                                   </td>
                                                                   <td>
                                                                     <?php
                                                                     $contact1=$value->tel1_etab;
                                                                     if(strlen($contact1)>0)
                                                                     {
                                                                       echo $value->tel_etab." / ".$value->tel1_etab;
                                                                     }else {
                                                                       echo $value->tel_etab;
                                                                     }
                                                                      ?>

                                                                   </td>
                                                                   <td> <?php echo $value->email_etab?></td>
                                                                     <td> <?php echo date_format(date_create($value->datecrea_etab),"d/m/Y");?></td>
                                                                   <td class="">
                                                                       <div class="btn-group">
                                                                         <a href="detailsEtab.php?etab=<?php echo $value->code_etab?>" class="btn btn-primary" title="Détails" ><i class="fa fa-exclamation-circle"></i></a>
                                                                         <a href="updateEtab.php?etab=<?php echo $value->code_etab?>" class="btn btn-warning" title="Modifier"><i class="fa fa-pencil"></i></a>
                                                                         <a href="#" class="btn btn-danger" title="Supprimer"><i class="fa fa-trash-o"></i></a>

                                                                       </div>
                                                                   </td>
                                                               </tr>
                                                               <?php
                                                                                          $i++;
                                                                                          endforeach;
                                                                                          ?>



                                                           </tbody>
                                                       </table>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                   <?php
                 }
                  ?>




                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function searchlibetab()
   {

     var etape=2;

     $.ajax({
              url: '../ajax/school.php',
              type: 'POST',
              async:false,
              data: 'code='+ $("#codeetab").val()+'&etape='+etape,
              dataType: 'text',
              success: function (content, statut) {

                $("#libetab").html("");
                $("#libetab").html(content);

              }

            });

   }

   $(document).ready(function() {

     $('#example5').DataTable( {
         "scrollX": true

     } );

     $("#codeetab").select2();

     $("#libetab").select2();

   $("#FormSearch").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
     rules:{
       dateau:{

         'required': {
                depends: function (element) {
                    return ($('#datedu').val() !='');

                }
            }
       }
     },
     messages:{
       datedu:"Merci de renseigner la date de fin",
       dateau:"Merci de renseigner la date de debut"
     }

   });


   });

   </script>
    <!-- end js include path -->
  </body>

</html>
