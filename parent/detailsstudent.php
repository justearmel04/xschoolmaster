<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$emailUti=$_SESSION['user']['email'];
$session= new Sessionacade();
$user=new User();
$etabs=new Etab();
$mat=new Matiere();
$localadmins= new Localadmin();
$parents=new ParentX();
$classe=new Classe();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);
// $classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);
// $nbclasse=$classe->getNumberClassesOfTeatcherId($_SESSION['user']['IdCompte']);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}
$codeEtabAssigner=$_GET['codeEtab'];

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)

{

  //recuperer la session en cours

  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
  $studentInfos=$student->getAllInformationsOfStudentOne($_GET['compte'],$libellesessionencours);
  $datascolarity=$etabs->DetermineScolarityStateOfStudent($codeEtabAssigner,$libellesessionencours,$_GET['compte']);

}

$tabStudent=array();

foreach ($studentInfos as $personnal):
  $tabStudent[0]= $personnal->id_compte;
  $tabStudent[1]=$personnal->matricule_eleve;
  $tabStudent[2]= $personnal->nom_eleve;
  $tabStudent[3]=$personnal->prenom_eleve;
  $tabStudent[4]= $personnal->datenais_eleve;
  $tabStudent[5]=$personnal->lieunais_eleve;
  $tabStudent[6]= $personnal->sexe_eleve;
  $tabStudent[7]=$personnal->email_eleve;
  $tabStudent[8]=$personnal->email_eleve;
  $tabStudent[9]= $personnal->libelle_classe;
  $tabStudent[10]=$personnal->codeEtab_classe;
  $tabStudent[11]= $personnal->photo_compte;
  $tabStudent[12]=$personnal->tel_compte;
  $tabStudent[13]= $personnal->login_compte;
  $tabStudent[14]=$personnal->codeEtab_inscrip;
  $tabStudent[15]= $personnal->id_classe;
  $tabStudent[16]=$personnal->allergie_eleve;
  $tabStudent[17]=$personnal->condphy_eleve;
endforeach;



// $parentInfos=$parents->getParentInfosbyId($studentparentid);

// $tabParent=explode("*",$parentInfos);

// $infosparents=$parents->ParentInfostudent($_GET['compte']);

$infosparents=$parents->ParentInfostudent($_GET['compte']);

$allabsencesLast=$etabs->getListeAttendanceLast($tabStudent[1],$tabStudent[15],$codeEtabAssigner,$libellesessionencours);

//nous allons compter le nombre d'antecedents medicaux

$medicalesAnte=$etabs->getAllMedicalesAnteOfChild($_GET['compte']);

$nbmedicalesAnte=count($medicalesAnte);

$datasallergies=$etabs->getAllMedicalesAllergiesOfChild($_GET['compte']);

$nballergies=count($datasallergies);

$datasinfantmaladies=$etabs->getAllMedicalesInfantDiseaseOfChild($_GET['compte']);

$nbinfantiles=count($datasinfantmaladies);

$datasmedicals=$etabs->getFormMedicalInfosChild($_GET['compte']);
$nbmedicales=count($datasmedicals);

$arrayM=json_encode($datasmedicals,true);
$someArrayM = json_decode($arrayM, true);


$nbnotificationstandby=$parents->getParentnotificationstandbyNb($compteuserid);

//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($compteuserid);




 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
 <?php
 include("style.php")
 ?>

 <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
</head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
             <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>

 			 <!-- end sidebar menu -->

 			<!-- start page content -->

             <div class="page-content-wrapper">

                 <div class="page-content">

                     <div class="page-bar">

                         <div class="page-title-breadcrumb">

                             <div class=" pull-left">

                                 <div class="page-title">Fiche Elève</div>

                             </div>

                             <ol class="breadcrumb page-breadcrumb pull-right">

                                 <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>

                                 </li>

                                 <li class="">Elèves</li>&nbsp;<i class="fa fa-angle-right"></i>

                                 <li class="active">Fiche Elève</li>

                             </ol>

                         </div>

                     </div>

 					<!-- start widget -->

 					<div class="state-overview">

 						<div class="row">



 					        <!-- /.col -->



 					        <!-- /.col -->



 					        <!-- /.col -->



 					        <!-- /.col -->

 					      </div>

 						</div>

 					<!-- end widget -->

           <?php



                 if(isset($_SESSION['user']['addetabexist']))

                 {



                   ?>

                   <div class="alert alert-danger alert-dismissible fade show" role="alert">

                 <?php

                 echo $_SESSION['user']['addetabexist'];

                 ?>

                 <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                 <span aria-hidden="true">&times;</span>

                    </a>

                 </div>







                   <?php

                   unset($_SESSION['user']['addetabexist']);

                 }



                  ?>





                  <div class="row">



                    <div class="col-sm-4">

                      <div class="card ">

                                            <div class="card-body no-padding height-9">

                                                <div class="row">

                                                    <div class="profile-userpic">

                                                      <?php

                                                          if($tabStudent[11]!="")

                                                          {

                                                            $lien="../photo/Students/".$tabStudent[1]."/".$tabStudent[11];

                                                          }else {

                                                            $lien="../photo/user5.jpg";

                                                          }

                                                       ?>

                                                        <img src="<?php echo $lien;?>" class="img-responsive" alt=""> </div>

                                                </div>

                                                <div class="profile-usertitle">

                                                    <div class="profile-usertitle-name"><?php echo $tabStudent[2]." ".$tabStudent[3];?></div>

                                                    <div class="profile-usertitle-job"> <?php echo $tabStudent[1];?></div>

                                                </div>

                                                <ul class="list-group list-group-unbordered">

                                                    <li class="list-group-item">

                                                        <b>Moyenne Trimestre</b> <a class="pull-right"></a>

                                                    </li>

                                                    <li class="list-group-item">

                                                        <b>Conduite Trimestre</b> <a class="pull-right"></a>

                                                    </li>



                                                </ul>

                                                <!-- END SIDEBAR USER TITLE -->

                                                <!-- SIDEBAR BUTTONS -->

                                                <div class="profile-userbuttons">

        			<a  target="_blank" class="btn btn-circle btn-warning btn-sm" href="listePersonnelEleve.php?compte=<?php echo $_GET['compte']?>&sessionEtab=<?php echo $libellesessionencours; ?>&codeEtab=<?php echo $codeEtabAssigner ?>"> <i class="fa fa-print"></i>Imprimer la  fiche Eleve
                                </a>


        					<!--button type="button" class="btn btn-circle red btn-sm">Message</button-->

                                                </div>

                                                <!-- END SIDEBAR BUTTONS -->

                                            </div>

                                        </div>

                    </div>

                    <div class="col-sm-8">

                        <div class="profile-content">

                          <div class="row">
                            <div class="profile-tab-box">
                              <div class="p-l-20">
                                <ul class="nav ">
                                  <li class="nav-item tab-all"><a
                                    class="nav-link active show" href="#tab1" data-toggle="tab"><?php echo L::GeneralInfoTab ?></a></li>
                                  <li class="nav-item tab-all p-l-20"><a class="nav-link"
                                    href="#tab2" data-toggle="tab">Paiements</a></li>
                                  <li class="nav-item tab-all p-l-20"><a class="nav-link"
                                    href="#tab3" data-toggle="tab">Notes</a></li>
                                    <li class="nav-item tab-all p-l-20"><a class="nav-link"
                                      href="#tab4" data-toggle="tab">Absences</a></li>
                                      <!-- <li class="nav-item tab-all p-l-20"><a class="nav-link"
                                        href="#tab5" data-toggle="tab"><?php echo L::Parameters ?></a></li> -->

                                </ul>
                              </div>
                            </div>
                            <div class="white-box">

                              <div class="tab-content">
                                <div class="tab-pane active fontawesome-demo" id="tab1">
                                  <div id="biography" >

                                    <div class="row">
                                      <div class="col-md-12">
                                      <span class="label label-md label-info" style="text-align:center">Informations Eleve</span>
                                      </div>

                                    </div>
                                    </br>
                                    <div class="table-responsive">

                                      <table class="table table-striped custom-table table-hover" >

                                          <thead>



                                          </thead>

                                          <tbody>

                                            <tr>

                                                <td><a href="#">Matricule :</a>

                                                </td>

                                                <td ><?php echo $tabStudent[1];?></td>



                                            </tr>

                                              <tr>

                                                  <td><a href="#"><?php echo L::NamestudentTab ?> :</a>

                                                  </td>

                                                  <td ><?php echo $tabStudent[2]." ".$tabStudent[3];?></td>



                                              </tr>



                                              <tr>

                                                  <td><a href="#"> Classe : </a>

                                                  </td>

                                                  <td ><?php echo $tabStudent[9];?></td>



                                              </tr>
                                              <?php
                                              if(strlen($tabStudent[7])>0)
                                              {
                                               ?>
                                              <tr>

                                                  <td><a href="#"> <?php echo L::EmailstudentTab ?>: </a>

                                                  </td>

                                                  <td ><?php echo $tabStudent[7];?></td>



                                              </tr>
                                              <?php
                                            }
                                               ?>

                                               <?php
                                               if(strlen($tabStudent[12])>0)
                                               {
                                                ?>
                                              <tr>

                                                  <td><a href="#"><?php echo L::PhonestudentTab ?> :</a>

                                                  </td>

                                                  <td ><?php echo $tabStudent[12];?></td>



                                              </tr>
                                              <?php
                                            }
                                               ?>
                                              <tr>

                                                  <td><a href="#"> <?php echo L::SexestudentTab ?>: </a>

                                                  </td>

                                                  <td ><?php

                                                  $sexe=$tabStudent[6];

                                                  if($sexe=="M")

                                                  {

                                                    echo "Masculin";

                                                  }else {

                                                    echo "Féminin";

                                                  }

                                                  ?></td>



                                              </tr>

                                              <tr>

                                                <td><a href="#">Date de naissance :</a>

                                                  </td>

                                                  <td ><?php echo date_format(date_create($tabStudent[4]),"d/m/Y");?></td>



                                              </tr>



                                          </tbody>

                                      </table>

                                    </div></br>
                                    <div class="row">
                                      <div class="col-md-12">
                                      <span class="label label-md label-info" style="text-align:center">Informations Parents</span>
                                      </div>
                                    </div></br>
                                      <div class="table-responsive">
                                        <table class="table table-striped custom-table table-hover">

                                            <thead>
                                              <th>Nom & Prénoms </th>
                                              <th>Email </th>
                                              <th>Contact </th>
                                              <th>Profession</th>


                                            </thead>

                                            <tbody>
                                              <?php
                                              foreach ($infosparents as $value):
                                               ?>

                                                <tr ondblclick="redirectparent(<?php echo $value->id_compte  ?>)">

                                                    <td><?php echo $value->nom_compte." ".$value->prenom_compte; ?></td>
                                                    <td><?php echo $value->email_compte; ?></td>
                                                    <td ><?php echo $value->tel_compte; ?></td>
                                                    <td ><?php echo $value->fonction_compte; ?></td>

                                                </tr>

                                              <?php
                                            endforeach;
                                               ?>
                                            </tbody>

                                        </table>
                                      </div></br>
                                      <?php
                                      if($nbmedicales>0)
                                      {
                                        ?>
                                        <div class="row">
                                          <div class="col-md-12">
                                          <span class="label label-md label-info" style="text-align:center">Informations Médicales</span>
                                          </div>
                                        </div>
                                        </br>
                                          <div class="table-responsive">

                                            <table class="table table-striped custom-table table-hover" >

                                                <thead>



                                                </thead>

                                                <tbody>

                                                  <tr>

                                                      <td><a href="#">Groupe sanguin :</a>

                                                      </td>

                                                      <td ><?php echo @$someArrayM[0]["blood_medical"];?></td>



                                                  </tr>

                                                    <tr>

                                                        <td><a href="#">Clinique/Hopital :</a>

                                                        </td>

                                                        <td ><?php echo @$someArrayM[0]["hopital_medical"];?></td>



                                                    </tr>



                                                    <tr>

                                                        <td><a href="#"> Numéro matricule Enfant / clinique : </a>

                                                        </td>

                                                        <td ><?php echo @$someArrayM[0]["pinchildhopital_medical"];?></td>



                                                    </tr>
                                                    <?php
                                                    if(strlen(@$someArrayM[0]["doctor_medical"])>0)
                                                    {
                                                     ?>
                                                    <tr>

                                                        <td><a href="#"> Medecin traitant : </a>

                                                        </td>

                                                        <td ><?php echo @$someArrayM[0]["doctor_medical"];?></td>



                                                    </tr>
                                                    <?php
                                                  }
                                                     ?>

                                                     <?php
                                                     if(strlen(@$someArrayM[0]["phonedoctor_medical"])>0)
                                                     {
                                                      ?>
                                                    <tr>

                                                        <td><a href="#"><?php echo L::PhonestudentTab ?> :</a>

                                                        </td>

                                                        <td ><?php echo @$someArrayM[0]["phonedoctor_medical"];?></td>



                                                    </tr>
                                                    <?php
                                                  }
                                                     ?>

                                                     <tr>

                                                         <td><a href="#">En cas de maladie :</a>

                                                         </td>

                                                         <td ><?php
                                                         if(@$someArrayM[0]["localisationsick_medical"]==1)
                                                         {
                                                           ?>
          <span class="label label-md label-danger" style="text-align:center">Réconduire l’enfant à son domicile</span>
                                                           <?php
                                                         }else {
                                                           ?>
          <span class="label label-md label-danger" style="text-align:center">Conduire l’enfant à l’adresse suivante</span>
                                                           <?php
                                                         }

                                                         ?></td>



                                                     </tr>
                                                     <tr>

                                                         <td colspan="2"><a href="#">Adresse géographique :</a>

                                                         </td>

                                                     </tr>
                                                     <tr>

                                                         <td colspan="2">
                                                          <span class="label label-md label-success" style="text-align:center"><?php echo utf8_decode(@$someArrayM[0]["adresgeolocalsick_medical"]);  ?></span>

                                                         </td>

                                                     </tr>





                                                </tbody>

                                            </table>

                                          </div></br>
                                          <div class="table-responsive">
                                            <ol>
                                            <?php
                                            if(@$nbmedicalesAnte>0)
                                            {
                                             ?>

                                              <li>ANTECEDENTS MEDICAUX</li>


                                            <table class="table table-striped custom-table table-hover">

                                                <thead>
                                                  <th>ANTECEDENTS MEDICAUX </th>
                                                  <th>Enfant </th>
                                                  <th>Père </th>
                                                  <th>Mère</th>


                                                </thead>

                                                <tbody>
                                                  <?php
                                                  foreach (@$medicalesAnte as $value):
                                                   ?>

                                                    <tr>

                                                        <td><span class="label label-sm label-danger" style="text-align:center"><?php echo utf8_encode(@$value->libelle_desease); ?></span></td>
                                                        <td><span class="label label-sm label-danger" style="text-align:center"><?php if(@$value->childstate_ante==1){echo "OUI";}else{echo "NON";} ?></span></td>
                                                        <td ><span class="label label-sm label-danger" style="text-align:center"><?php if(@$value->fatherstate_ante==1){echo "OUI";}else{echo "NON";} ?></span></td>
                                                        <td ><span class="label label-sm label-danger" style="text-align:center"><?php if(@$value->motherstate_ante==1){echo "OUI";}else{echo "NON";} ?></td>






                                                    </tr>

                                                  <?php
                                                endforeach;
                                                   ?>
                                                </tbody>

                                            </table>

                                            <?php
                                             }
                                             ?>
                                             <?php
                                             if(@$nballergies>0)
                                             {
                                               $array=json_encode(@$datasallergies,true);
                                               $someArray = json_decode(@$array, true);
                                               $donnees=@$someArray[0]["allergies_medical"];
                                               $donnees=substr($donnees, 0, -1);

                                               @$tabdonnees=explode(",",$donnees);
                                               @$nbtabdonnees=count($tabdonnees);
                                              ?>

                                               <li>ALLERGIES/ALLERGIES</li>

                                               <table class="table table-striped custom-table table-hover">



                                                   <tbody>
                                                    <?php
                                                    for($i=0;$i<@$nbtabdonnees;$i++)
                                                    {


                                                     ?>

                                                       <tr>

                                                           <td><span class="label label-sm label-danger" style="text-align:center"><?php echo $etabs->getLibelleOfDiseaseById($tabdonnees[$i]); ?></span></td>


                                                       </tr>
                                                       <?php
                                                     }
                                                        ?>

                                                   </tbody>

                                               </table>
                                               <?php
                                             }
                                                ?>
                                                <?php
                                                if(@$nbinfantiles>0)
                                                {
                                                  @$array=json_encode(@$datasinfantmaladies,true);
                                                  @$someArray = json_decode(@$array, true);
                                                  @$donnees=@$someArray[0]["deseases_medical"];
                                                  @$donnees=substr(@$donnees, 0, -1);

                                                  @$tabdonnees=explode(",",@$donnees);
                                                  @$nbtabdonnees=count(@$tabdonnees);
                                                 ?>
                                                <li>MALADIES INFANTILES/CHILDHOOD DISEASES</li>
                                                <table class="table table-striped custom-table table-hover">



                                                    <tbody>
                                                     <?php
                                                     for($i=0;$i<@$nbtabdonnees;$i++)
                                                     {


                                                      ?>

                                                        <tr>

                                                            <td><span class="label label-sm label-danger" style="text-align:center"><?php echo $etabs->getLibelleOfDiseaseById(@$tabdonnees[$i]); ?></span></td>


                                                        </tr>
                                                        <?php
                                                      }
                                                         ?>

                                                    </tbody>

                                                </table>
                                                <?php
                                                }
                                                 ?>
                                                  </ol>


                                          </div></br>
                                          <div class="row">
                                            <div class="col-md-12">
                                            <span class="label label-md label-info" style="text-align:center">Personne à prévenir</span>
                                            </div>
                                          </div></br>
                                          <div class="table-responsive">

                                            <table class="table table-striped custom-table table-hover" >

                                                <thead>



                                                </thead>

                                                <tbody>
                                                  <?php
                                                  if(strlen(@$someArrayM[0]["guardian_medical"])>0)
                                                  {
                                                   ?>
                                                  <tr>

                                                      <td><a href="#"><?php echo L::NamestudentTab ?> :</a>

                                                      </td>

                                                      <td ><?php echo utf8_decode(@$someArrayM[0]["guardian_medical"]);?></td>



                                                  </tr>
                                                  <?php
                                                }
                                                   ?>

                                                  <?php
                                                  if(strlen(@$someArrayM[0]["telBguardian_medical"])>0)
                                                  {
                                                   ?>

                                                    <tr>

                                                        <td><a href="#">Tel / Bureau:</a>

                                                        </td>

                                                        <td ><?php echo @$someArrayM[0]["telBguardian_medical"];?></td>



                                                    </tr>

                                                    <?php
                                                  }
                                                     ?>

                                                    <?php
                                                    if(strlen(@$someArrayM[0]["telMobguardian_medical"])>0)
                                                    {
                                                     ?>

                                                    <tr>

                                                        <td><a href="#"> Cell : </a>

                                                        </td>

                                                        <td ><?php echo @$someArrayM[0]["telMobguardian_medical"];?></td>



                                                    </tr>
                                                    <?php
                                                  }
                                                     ?>
                                                    <?php
                                                    if(strlen(@$someArrayM[0]["domicileguardian_medical"])>0)
                                                    {
                                                     ?>
                                                    <tr>

                                                        <td><a href="#"> Domicile : </a>

                                                        </td>

                                                        <td ><?php echo @$someArrayM[0]["domicileguardian_medical"];?></td>



                                                    </tr>
                                                    <?php
                                                  }
                                                     ?>

                                                      </tbody>

                                            </table>

                                          </div></br>

                                        <?php
                                      }else {
                                        ?>

                                            <!-- <a href="#" class="btn btn-success btn-md"><i class="fa fa-plus"></i> informations médicales</a> -->

                                        <?php
                                      }
                                       ?>



                                  </div>

                                </div>

                                <div class="tab-pane" id="tab2">
                                  <div class="container-fluid">

                                    <div class="row">
                                        <div class="full-width p-l-20">
                                            <div class="panel">



                                              <div class="row">
                                                <div class="col-md-12">
                                                <span class="label label-md label-info" style="text-align:center">LISTE DES VERSEMENTS SCOLARITES</span>
                                                </div>

                                              </div></br></br>
                                              <?php
                                              //liste des versement concernant la scolarité

                                              $datasScolarites=$etabs->getAllScolaritesVersmentChild($codeEtabAssigner,$libellesessionencours,$_GET['compte'],$tabStudent[15]);
                                              // var_dump($datasInscriptions);
                                              $nbdatascolarites=count($datasScolarites);

                                              if($nbdatascolarites==0)
                                              {
                                                ?>
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">

                                             Aucun Paiement des frais de scolarités à ce jour

                                              <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                                              <span aria-hidden="true">&times;</span>

                                                 </a>

                                              </div>
                                                <?php
                                              }else {
                                                ?>
                                                <table id="example1" style="width:100%;">

                                                    <thead>

                                                        <tr>

                                                            <th>Versements </th>

                                                            <th>Date</th>

                                                            <th>Montant Versé</th>

                                                            <th>Reste a payer</th>

                                                          </tr>


                                                    </thead>

                                                    <tbody>

                                                      <?php

                                                      $j=1;

                                                      foreach ($datasScolarites as $value):

                                                       ?>

                                                        <tr>

                                                            <td><span class="label label-sm label-info"><?php echo $value->code_versement; ?></span></td>

                                                            <td><?php echo date_format(date_create($value->date_versement),"d-m-Y")?></td>

                                                            <td><?php echo $value->montant_versement. " ".$value->devise_versement;?></td>

                                                            <td><?php

                                                            $resteapayer=$value->solde_versement;

                                                            if($resteapayer==0)

                                                            {

                                                              ?>

                                                              <span class="label label-sm label-success">Soldé</span>

                                                              <?php

                                                            }else if($resteapayer>0)

                                                            {

                                                              ?>

                                                               <span class="label label-sm label-danger"><?php echo $value->solde_versement. " ".$value->devise_versement;?></span>

                                                              <?php

                                                            }

                                                            ?></td>



                                                        </tr>

                                                      <?php

                                                      $j++;

                                                      endforeach;

                                                       ?>



                                                    </tbody>

                                                </table>
                                                <br>
                                                <?php
                                              }
                                               ?>
                                               <?php
                                               //nous allons verifier si cet etablissement dispose de cantine
                                               $stateschoolcantines=$etabs->getCantineOption($codeEtabAssigner);
                                               if($stateschoolcantines==1)
                                               {
                                                 ?>
                                               </br></br>  <div class="row">
                                                   <div class="col-md-12">
                                                   <span class="label label-md label-info" style="text-align:center">LISTE DES VERSEMENTS CANTINES</span>
                                                   </div>

                                                 </div></br></br>

                                                 <?php
                                                 //liste des versement concernant la cantine

                                                 $datasCantines=$etabs->getAllCantinesVersmentChild($codeEtabAssigner,$libellesessionencours,$_GET['compte'],$tabStudent[15]);
                                                 // var_dump($datasInscriptions);
                                                 $nbdatascantines=count($datasCantines);

                                                 if($nbdatascantines==0)
                                                 {
                                                   ?>
                                                   <div class="alert alert-danger alert-dismissible fade show" role="alert">

                                                Aucun Paiement des frais de cantine à ce jour

                                                 <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                                                 <span aria-hidden="true">&times;</span>

                                                    </a>

                                                 </div>
                                                   <?php
                                                 }else {
                                                   ?>
                                                   <table id="example1" style="width:100%;">

                                                       <thead>

                                                           <tr>

                                                               <th>Versements </th>

                                                               <th>Date</th>

                                                               <th>Montant Versé</th>

                                                               <!-- <th>Reste a payer</th> -->



                                                       </thead>

                                                       <tbody>

                                                         <?php

                                                         $j=1;

                                                         foreach ($datasCantines as $value):

                                                          ?>

                                                           <tr>

                                                               <td><span class="label label-sm label-info"><?php echo $value->code_versement; ?></span></td>

                                                               <td><?php echo date_format(date_create($value->date_versement),"d-m-Y")?></td>

                                                               <td><?php echo $value->montant_versement. " ".$value->devise_versement;?></td>





                                                           </tr>

                                                         <?php

                                                         $j++;

                                                         endforeach;

                                                          ?>



                                                       </tbody>

                                                   </table>
                                                   <br>
                                                   <?php
                                                 }
                                                  ?>
                                                 <?php
                                               }
                                                ?>

                                              <?php
                                              //compter le nombre AES DE CET ETABLISSEMENT
                                              $dataschoolAES=$etabs->getAllParascolairesActivityOfThisSchool($codeEtabAssigner,$libellesessionencours);
                                              $nbschoolAES=count($dataschoolAES);

                                              if($nbschoolAES>0)
                                              {
                                                ?>
                                              </br></br> <div class="row">
                                                 <div class="col-md-12">
                                                 <span class="label label-md label-info" style="text-align:center">LISTE DES VERSEMENTS AES</span>
                                                 </div>

                                               </div></br></br>
                                               <?php
                                               //liste des versement concernant les frais AES

                                               $datasAes=$etabs->getAllAesVersmentChild($codeEtabAssigner,$libellesessionencours,$_GET['compte'],$tabStudent[15]);
                                               // var_dump($datasInscriptions);
                                               $nbdatasAes=count($datasAes);

                                               if($nbdatasAes==0)
                                               {
                                                 ?>
                                                 <div class="alert alert-danger alert-dismissible fade show" role="alert">

                                              Aucun Paiement des frais AES à ce jour

                                               <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                                               <span aria-hidden="true">&times;</span>

                                                  </a>

                                               </div>
                                                 <?php
                                               }else {
                                                 ?>
                                                 <table id="example1" style="width:100%;">

                                                     <thead>

                                                         <tr>

                                                             <th>Versements </th>

                                                             <th>Date</th>

                                                             <th>Montant Versé</th>

                                                             <!-- <th>Reste a payer</th> -->



                                                     </thead>

                                                     <tbody>

                                                       <?php

                                                       $j=1;

                                                       foreach ($datasCantines as $value):

                                                        ?>

                                                         <tr>

                                                             <td><span class="label label-sm label-info"><?php echo $value->code_versement; ?></span></td>

                                                             <td><?php echo date_format(date_create($value->date_versement),"d-m-Y")?></td>

                                                             <td><?php echo $value->montant_versement. " ".$value->devise_versement;?></td>





                                                         </tr>

                                                       <?php

                                                       $j++;

                                                       endforeach;

                                                        ?>



                                                     </tbody>

                                                 </table>
                                                 <br>
                                                 <?php
                                               }

                                                ?>
                                                <?php
                                              }

                                               ?>



                                            </div>
                                        </div>

                                  </div>

                                  </div>

                                </div>

                                <div class="tab-pane" id="tab3">

                                  <div class="container-fluid">

                                    <div class="row">
                                        <div class="full-width p-l-20">
                                            <div class="panel">

                                              <br><br>
                                              <div class="row">
                                                <div class="col-md-12">
                                                <span class="label label-md label-info" style="text-align:center">LISTE DES NOTES D'EXAMENS</span>
                                                </div>

                                              </div></br></br>
                                              <?php
                                              //compter le nombre de versement concernant l'inscription de cet eleve
                                              $numberOfExamen=$student->getNumberOfExamNoteOfStudentThisSessions($codeEtabAssigner,$libellesessionencours,$_GET['compte']);

                                              if($numberOfExamen==0)
                                              {
                                                ?>
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">

                                             Aucune Note d'examen à ce jour

                                              <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                                              <span aria-hidden="true">&times;</span>

                                                 </a>

                                              </div>
                                                <?php
                                              }else {
                                                ?>
                                                <table id="example1" style="width:100%;">

                                                    <thead>

                                                        <tr>

                                                            <th>Examens </th>

                                                            <th>Note</th>

                                                            <th>Observation</th>



                                                    </thead>

                                                    <tbody>

                                                      <?php

                                                      $j=1;
                                                      $examenalls=$student->getExameNotesOfStudentThisSession($codeEtabAssigner,$libellesessionencours,$_GET['compte']);
                                                      foreach ($examenalls as $value):

                                                       ?>

                                                        <tr>

                                                            <td><span class="label label-sm label-success"> <?php echo $value->libelle_exam ?></span></td>

                                                            <td><?php echo $value->valeur_notes; ?></td>

                                                            <td><?php echo $value->obser_notes; ?></td>



                                                        </tr>

                                                      <?php

                                                      $j++;

                                                      endforeach;

                                                       ?>



                                                    </tbody>

                                                </table>
                                                <?php
                                              }
                                               ?>
                                             </br></br> <div class="row">
                                                <div class="col-md-12">
                                                <span class="label label-md label-info" style="text-align:center">LISTE DES NOTES DE CONTROLES</span>
                                                </div>

                                              </div></br></br>
                                              <?php
                                              //liste des versement concernant la scolarité

                                            $numberOfControle=$student->getNumberOfControleNoteOfStudentThisSessions($codeEtabAssigner,$libellesessionencours,$_GET['compte']);

                                              if($numberOfControle==0)
                                              {
                                                ?>
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">

                                             Aucune Note de contrôle à ce jour

                                              <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                                              <span aria-hidden="true">&times;</span>

                                                 </a>

                                              </div>
                                                <?php
                                              }else {
                                                ?>
                                                <table id="example1" style="width:100%;">

                                                <thead>

                                                 <tr>
                                                     <th>Matière</th>
                                                     <th>Controle </th>
                                                     <th>Note</th>
                                                     <th>Observation</th>
                                                   </tr>




                                                </thead>

                                                <tbody>

                                                <?php

                                                $j=1;
                                                $controlealls=$student->getControleNotesOfStudentLimitedThisSessions($codeEtabAssigner,$libellesessionencours,$_GET['compte']);
                                                foreach ($controlealls as $value):

                                                ?>

                                                 <tr>
                                                    <td><span class="label label-sm label-success"> <?php echo $mat->getMatiereLibelleByIdMat($value->mat_ctrl,$value->codeEtab_ctrl); ?></span></td>
                                                     <td><span class="label label-sm label-success"> <?php echo $value->libelle_ctrl; ?></span></td>

                                                     <td><?php echo $value->valeur_notes; ?></td>

                                                     <td><?php echo $value->obser_notes; ?></td>



                                                 </tr>

                                                <?php

                                                $j++;

                                                endforeach;

                                                ?>



                                                </tbody>

                                                </table>
                                                <?php
                                              }
                                               ?>
                                            </br></br>



                                            </div>
                                        </div>

                                  </div>

                                  </div>

                                </div>
                                <div class="tab-pane  fontawesome-demo" id="tab4">

                                  <div id="biography" >

                                    <div class="row">

                                      <div class="row">
                                        <div class="col-md-12">
                                        <span class="label label-md label-info" style="text-align:center"><?php echo L::HistoriqAttendance ?></span>
                                        </div>
                                      </div>

                                    </div></br>
                                    <?php
                                    $nbabseces=count($allabsencesLast);
                                    if($nbabseces==0)
                                    {
                                      ?>

                                      <?php

                                    }else {
                                      ?>
                                      <!-- <div class="table-responsive"> -->

                                          <table class="table table-striped custom-table table-hover">

                                              <thead>
                                                <th>Date:</th>
                                                <th>Matière :</th>
                                                <!-- <th>Coefficient:</th> -->
                                                <th>Heure début :</th>
                                                <th>Heure fin:</th>



                                              </thead>

                                              <tbody>
                                                <?php
                                                foreach ($allabsencesLast as $value):
                                                 ?>

                                                  <tr>

                                                      <td><?php echo $value->date_presence; ?></td>
                                                      <td><?php echo $etabs->getMatiereLibelleByIdMat($value->matiere_presence,$value->codeEtab_presence); ?></td>
                                                      <!-- <td ><?php //echo $value->tel_compte; ?></td> -->
                                                      <td ><?php echo $value->heuredeb_heure; ?></td>
                                                      <td ><?php echo $value->heurefin_heure; ?></td>






                                                  </tr>

                                                <?php
                                              endforeach;
                                                 ?>
                                              </tbody>

                                          </table>
                                          <?php
                                        }
                                         ?>



                                  </div>

                                </div>

                                <div class="tab-pane  fontawesome-demo" id="tab5">
                                  <div id="biography" >

                                    <div class="row">

                                      <div class="row">
                                        <div class="col-md-12">
                                        <span class="label label-md label-info" style="text-align:center">Information du connexion</span>
                                        </div>
                                      </div>

                                    </div></br>
                                    <form class="form-control" action="../controller/compte.php" method="post" id="Formcnx">
                                      <div class="form-body">

                                        <div class="form-group row">
                                             <label class="control-label col-md-3"><?php echo L::Logincnx?>
                                                 <span class="required">*  </span>
                                             </label>
                                             <div class="col-md-6">
                                                 <input name="loginTea" id="loginTea" type="text" placeholder="<?php if(strlen($tabStudent[13])>0) { echo $tabStudent[13]; }else { echo "Entrer le Login";}?>"  value="<?php echo $tabStudent[13];?> " class="form-control" />

                                                   </div>
                                         </div>
                                         <div class="form-group row">
                                             <label class="control-label col-md-3"><?php echo L::Passcnx?>
                                                 <span class="required">*  </span>
                                             </label>
                                             <div class="col-md-6">
                                                 <input name="passTea" id="passTea" type="password" placeholder="Entrer le Mot de passe " class="form-control " /> </div>
                                         </div>
                                         <div class="form-group row">
                                             <label class="control-label col-md-3"><?php echo L::ConfirmPasscnx?>
                                                 <span class="required">*  </span>
                                             </label>
                                             <div class="col-md-6">
                                                 <input name="confirmTea" id="confirmTea" type="password" placeholder="Confirmer le Mot de passe " class="form-control " /> </div>
                                                 <input type="hidden" name="etape" id="etape" value="1"/>
                                                  <input type="hidden" name="idcompte" id="idcompte" value="<?php echo $_GET['compte'];?>"/>
                                                  <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner;?>"/>

                                         </div>
                                         <div class="form-actions">
                                                               <div class="row">
                                                                   <div class="offset-md-3 col-md-9">
                                                                       <button class="btn btn-success" type="submit">Modifier</button>
                                                                       <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                   </div>
                                                                 </div>
                                                              </div>

                                      </div>

                                    </form>


                                  </div>
                                </div>



                              </div>

                            </div>

                          </div>

                      </div>

                    </div>


                    							</div>





                      <!-- start new patient list -->



                     <!-- end new patient list -->



                 </div>

             </div>

             <!-- end page content -->

             <!-- start chat sidebar -->



             <!-- end chat sidebar -->

         </div>

         <!-- end page container -->

         <!-- start footer -->

         <div class="page-footer">

             <div class="page-footer-inner"> 2019 &copy;

             <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>

             </div>

             <div class="scroll-to-top">

                 <i class="material-icons">eject</i>

             </div>

         </div>

         <!-- end footer -->

     </div>

     <!-- start js include path -->

     <script src="../assets2/plugins/jquery/jquery.min.js" ></script>

  	<script src="../assets2/plugins/popper/popper.min.js" ></script>

      <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>

  	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

      <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>

      <!-- bootstrap -->

      <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>

      <!-- calendar -->

      <script src="../assets2/plugins/moment/moment.min.js" ></script>

      <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>

      <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>

      <!-- Common js-->

  	<script src="../assets2/js/app.js" ></script>

      <script src="../assets2/js/layout.js" ></script>

  	<script src="../assets2/js/theme-color.js" ></script>

  	<!-- Material -->

  	<script src="../assets2/plugins/material/material.min.js"></script>









     <!-- morris chart -->

     <!--script src="../assets2/plugins/morris/morris.min.js" ></script>

     <script src="../assets2/plugins/morris/raphael-min.js" ></script>

     <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->



    <script>


    function addFrench()
    {
      var etape=1;
      var lang="fr";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

    function addEnglish()
    {
      var etape=1;
      var lang="en";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }


    function generatefichepdf(idcompte)

    {

      var codeEtab="<?php echo $codeEtabAssigner ?>";

       var etape=2;

        $.ajax({

          url: '../ajax/admission.php',

          type: 'POST',

          async:false,

          data: 'compte=' +idcompte+ '&etape=' + etape+'&codeEtab='+ codeEtab,

          dataType: 'text',

          success: function (content, statut) {



           window.open(content, '_blank');



          }

        });

    }



    $(document).ready(function() {







    });



    </script>

     <!-- end js include path -->

   </body>

</html>
