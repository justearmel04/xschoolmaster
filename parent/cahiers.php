<?php

session_start();

require_once('../class/User.php');

require_once('../class/Etablissement.php');

require_once('../class/LocalAdmin.php');

require_once('../class/Parent.php');

require_once('../class/Classe.php');

require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();





$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];

$user=new User();

$etabs=new Etab();

$localadmins= new Localadmin();

$parents=new ParentX();

$classe=new Classe();

$compteuserid=$_SESSION['user']['IdCompte'];

$imageprofile=$user->getImageProfilebyId($compteuserid);

$logindata=$user->getLoginProfilebyId($compteuserid);

$tablogin=explode("*",$logindata);

$datastat=$user->getStatis();

$tabstat=explode("*",$datastat);

// $classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);

// $nbclasse=$classe->getNumberClassesOfTeatcherId($_SESSION['user']['IdCompte']);



if(strlen($imageprofile)>0)

{

  $lienphoto="../photo/".$emailUti."/".$imageprofile;

}else {

  $lienphoto="../photo/user5.jpg";

}



if(isset($_POST['codeEtab']))

{

  $codeEtab=$_POST['codeEtab'];

}

if(isset($_GET['codeEtab']))

{

$codeEtab=$_GET['codeEtab'];

}



if(isset($_POST['classe']))

{

  $classeEtab=$_POST['classe'];

}

if(isset($_GET['classe']))

{

$classeEtab=$_GET['classe'];

}



$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtab);



if($nbsessionOn>0)

{

  //recuperer la session en cours

  $sessionencours=$session->getSessionEncours($codeEtab);

  $tabsessionencours=explode("*",$sessionencours);

  $libellesessionencours=$tabsessionencours[0];

  $sessionencoursid=$tabsessionencours[1];

  $typesessionencours=$tabsessionencours[2];

}



// $alletab=$etabs->getAllEtab();

// $locals=$localadmins->getAllAdminLocal();

// $allparents=$parents->getAllParent();



//retrouver les classes ou le parent à au moins un enfant



$classesOfStudent=$parents->getAllclassesOfStudentParentNew($_SESSION['user']['IdCompte']);







$etabofStudent=$parents->getAlletabOfStudentParent($_SESSION['user']['IdCompte']);



$classesStudents=$etabs->getAllsubjectofclassesbyIdclasses($classeEtab,$codeEtab,$libellesessionencours);



// var_dump($etabofStudent);



$teatcherprogrammes=$etabs->getAllprogrammesOfTeatcherSessionClasses($_GET['codeEtab'],$libellesessionencours);



$fiches=$etabs->getAllFichesClassesOfTeatchers($_GET['codeEtab'],$libellesessionencours);




$nbnotificationstandby=$parents->getParentnotificationstandbyNb($compteuserid);

//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($compteuserid);


 ?>
<br>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
   <?php
 include("style.php")
?>
</head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
    include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
			 <!-- end sidebar menu -->

			<!-- start page content -->

            <div class="page-content-wrapper">

                <div class="page-content">

                  <div class="page-bar">

                      <div class="page-title-breadcrumb">

                          <div class=" pull-left">

                              <div class="page-title">Cahier de textes</div>

                          </div>

                          <ol class="breadcrumb page-breadcrumb pull-right">

                              <li>&nbsp;<a class="parent-item" href="#">Home</a>&nbsp;<i class="fa fa-angle-right"></i>

                              </li>

                              <li>&nbsp;<a class="parent-item" href="#">Enseignement</a>&nbsp;<i class="fa fa-angle-right"></i>

                              </li>

                              <li class="active">Cahier de texte</li>

                          </ol>

                      </div>

                  </div>

					<!-- start widget -->

					<div class="state-overview">

						<div class="row">



					        <!-- /.col -->



					        <!-- /.col -->



					        <!-- /.col -->



					        <!-- /.col -->

					      </div>

						</div>

					<!-- end widget -->

          <?php



                if(isset($_SESSION['user']['addetabexist']))

                {



                  ?>

                  <div class="alert alert-danger alert-dismissible fade show" role="alert">

                <?php

                echo $_SESSION['user']['addetabexist'];

                ?>

                <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                <span aria-hidden="true">&times;</span>

                   </a>

                </div>







                  <?php

                  unset($_SESSION['user']['addetabexist']);

                }



                 ?>





                 <div class="row">

                   <div class="col-md-12 col-sm-12">

                                             <div class="card card-box">

                                                 <div class="card-head">

                                                     <header><?php echo L::Seacher ?></header>



                                                 </div>

                                                 <div class="card-body " id="bar-parent">

                                                   <form method="post" id="FormTeatcherSearch" name="academiqueclasses.php">

                                                       <div class="row">

                                                         <div class="col-md-6 col-sm-6">

                                                         <!-- text input -->

                                                         <div class="form-group">

                                                             <label>Etablissement</label>

                                                             <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->

                                                             <select class="form-control " id="codeEtab" name="codeEtab" onchange="searchclasseandteatcher()" style="width:100%" readonly>



                                                                 <?php

                                                                 $i=1;

                                                                   foreach ($etabofStudent as $value):

                                                                   ?>

                                                                   <option <?php if($codeEtab==$value->code_etab){ echo "selected";} ?> value="<?php echo $value->code_etab?>"><?php echo $value->libelle_etab?></option>



                                                                   <?php

                                                                                                    $i++;

                                                                                                    endforeach;

                                                                                                    ?>



                                                             </select>

                                                         </div>

                                                         <div class="form-group">

                                                             <label><?php echo L::MatiereMenusingle ?></label>

                                                             <select class="form-control input-height" id="matiere" name="matiere" style="width:100%">

                                                                 <option value=""><?php echo L::SelectSubjects ?></option>

                                                                 <?php

                                                                 foreach ($classesStudents as $value):

                                                                  ?>

                                                                  <option value="<?php echo $value->id_mat; ?>"><?php echo utf8_encode(utf8_decode($value->libelle_mat)) ?></option>

                                                                  <?php

                                                                endforeach;

                                                                   ?>

                                                             </select>

                                                         </div>



                                                     </div>

                                                     <div class="col-md-6 col-sm-6">

                                                     <!-- text input -->

                                                     <div class="form-group">

                                                         <label>Classe</label>

                                                         <select class="form-control " id="classe" name="classe"  style="width:100%">

                                                             <option value="<?php echo $classeEtab ?>" selected><?php echo $classe->getInfosofclassesbyId($classeEtab,$libellesessionencours) ?></option>

                                                         </select>

                                                         <input type="hidden" name="search" id="search"/>

                                                     </div>





                                                 </div>

                                                       </div>



                                                       <button type="submit" class="btn btn-danger"><?php echo L::Seacher ?></button>

                                                   </form>

                                                 </div>

                                             </div>

                                         </div>

                 </div>



                 <div class="row" style="">



                   <?php

                   if(isset($_POST['search']))

                   {

                     if(isset($_POST['classe'])&&isset($_POST['codeEtab']))

                     {

                       if(isset($_POST['matiere'])&& strlen($_POST['matiere'])>0)

                       {

                          //echo "Avec teatcher";

                          $programmes=$parents->getTheSpecificMatiereForSchool($_POST['codeEtab'],$_POST['classe'],$_POST['matiere']);

                       }else {

                         //echo "Sans teatcher";

                         $programmes=$parents->getAllMatiereOfThisClasseSchool($_POST['codeEtab'],$_POST['classe']);

                       }



                     }

                   }else {

                      $programmes=$parents->getAllMatiereOfThisClasseSchool($codeEtab,$classeEtab);

                   }



?>

<div class="col-md-12">

    <div class="card  card-box">

        <div class="card-head">

            <header></header>

            <div class="tools">

                <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>

              <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>

              <a class="t-close btn-color fa fa-times" href="javascript:;"></a>

            </div>

        </div>

        <div class="card-body ">



          <table id="example1" class="table table-striped table-bordered table-hover table-checkable order-column full-width " style="width:100%;">

              <thead>

                  <tr>





                      <th>Matière</th>

                      <th>Classes</th>

                      <th>Sections</th>

                      <th>Taches</th>



                  </tr>

              </thead>

              <tbody>

                <?php foreach ($fiches as $value): ?>

                  <tr ondblclick="detailsfiches(<?php echo $value->id_cahier;?>,<?php echo $value->teatcher_cahier; ?>,'<?php echo $value->codeEtab_cahier;  ?>')">

                    <td><?php echo $value->libelle_mat ?></td>

                    <td><?php echo $value->libelle_classe ?></td>

                    <td>

                       <ul>

                      <?php

                      //determiner la liste des sections

                       $sectionfiches=$etabs->getAllsectionsOfThisCahier($value->id_cahier,$value->session_cahier,$value->codeEtab_cahier,$value->matiere_cahier,$value->teatcher_cahier);



                       foreach ($sectionfiches as $valuesection):

                         ?>



                         <li><?php echo $valuesection->libelle_secahier; ?></li>



                         <?php

                       endforeach;



                       ?>

                        </ul>

                    </td>

                    <td>

                      <ul>

                        <?php

                        $tachesfiches=$etabs->getAlltachesOfThisCahier($value->id_cahier,$value->session_cahier,$value->codeEtab_cahier,$value->matiere_cahier,$value->teatcher_cahier);



                        foreach ($tachesfiches as $valuetaches):

                          ?>

                          <li><?php echo $valuetaches->libelle_tacheca; ?></li>

                          <?php

                        endforeach;



                         ?>

                      </ul>

                  </tr>



                  <script type="text/javascript">



                  function detailsfiches(fiche,teatcher,codeEtab)

                  {

                    var idcompte="<?php echo $_GET['idcompte'] ?>";

                    var classe="<?php echo $_GET['classe']; ?>";

                    document.location.href="detailcahier.php?fiche="+fiche+"&teatcher="+teatcher+"&codeEtab="+codeEtab+"&idcompte="+idcompte+"&classe="+classe;

                  }



                  </script>



                <?php endforeach; ?>



              </tbody>

          </table>

        </div>

    </div>

</div>









                </div>





                     <!-- start new patient list -->



                    <!-- end new patient list -->



                </div>

            </div>

            <!-- end page content -->

            <!-- start chat sidebar -->



            <!-- end chat sidebar -->

        </div>

        <!-- end page container -->

        <!-- start footer -->

        <div class="page-footer">

            <div class="page-footer-inner"> 2019 &copy;

            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>

            </div>

            <div class="scroll-to-top">

                <i class="material-icons">eject</i>

            </div>

        </div>

        <!-- end footer -->

    </div>

    <!-- start js include path -->

    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>

	<script src="../assets2/plugins/popper/popper.min.js" ></script>

    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>

	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- bootstrap -->

    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>

    <!-- counterup -->

    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>

    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>

    <!-- Common js-->

	<script src="../assets2/js/app.js" ></script>

    <script src="../assets2/js/layout.js" ></script>

    <script src="../assets2/js/theme-color.js" ></script>

    <!-- material -->

    <!-- data tables -->

   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>

 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>

   <script src="../assets2/js/pages/table/table_data.js" ></script>

    <script src="../assets2/plugins/material/material.min.js"></script>

    <script src="../assets2/plugins/select2/js/select2.js" ></script>

    <script src="../assets2/js/pages/select2/select2-init.js" ></script>

    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>

    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>



    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>











    <!-- morris chart -->

    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>

    <script src="../assets2/plugins/morris/raphael-min.js" ></script>

    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->



    <script>

    function addFrench()
    {
      var etape=1;
      var lang="fr";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

    function addEnglish()
    {
      var etape=1;
      var lang="en";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

    $("#codeEtab").select2();

    $("#classe").select2();

    $("#matiere").select2();



    function searchclasseandteatcher()

    {

      var etape=5;

      var parentid="<?php echo $_SESSION['user']['IdCompte'];?>";

      $.ajax({

        url: '../ajax/classe.php',

        type: 'POST',

        async:false,

        data: 'code='+ $("#codeEtab").val()+'&etape='+etape+'&parentid='+parentid,

        dataType: 'text',

        success: function (content, statut) {



          $("#classe").html("");

          $("#classe").html(content);



        }

      });

    }



    function searchteatcher()

    {

      var etape=5;

      var parentid="<?php echo $_SESSION['user']['IdCompte'];?>";

      var classe=$("#classe").val();





      $.ajax({

        url: '../ajax/teatcher.php',

        type: 'POST',

        async:false,

        data: 'code='+ $("#codeEtab").val()+'&etape='+etape+'&parentid='+parentid+'&classe='+classe,

        dataType: 'text',

        success: function (content, statut) {





          $("#teatcher").html("");

          $("#teatcher").html(content);



        }

      });







    }



    function searchmatiere()

    {

      var etape=5;

      var parentid="<?php echo $_SESSION['user']['IdCompte'];?>";

      var classe=$("#classe").val();

      var codeEtab=$("#codeEtab").val();



      $.ajax({

        url: '../ajax/matiere.php',

        type: 'POST',

        async:false,

        data: 'code='+ $("#codeEtab").val()+'&etape='+etape+'&parentid='+parentid+'&classe='+classe,

        dataType: 'text',

        success: function (content, statut) {



          // alert(content);

          $("#matiere").html("");

          $("#matiere").html(content);



        }

      });

    }



    $(document).ready(function() {



 $("#FormTeatcherSearch").validate({

   errorPlacement: function(label, element) {

   label.addClass('mt-2 text-danger');

   label.insertAfter(element);

 },

 highlight: function(element, errorClass) {

   $(element).parent().addClass('has-danger')

   $(element).addClass('form-control-danger')

 },

 success: function (e) {

       $(e).closest('.control-group').removeClass('error').addClass('info');

       $(e).remove();

   },

   rules:{

     codeEtab:"required",

     classe:"required"

   },

   messages: {

     codeEtab:"Merci de selectioner un Etablissement",

     classe:"Merci de selectionner une classe"

   }

 });



    });



    </script>

    <!-- end js include path -->

  </body>



</html>
