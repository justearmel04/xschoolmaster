<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}


$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$emailUti=$_SESSION['user']['email'];



$user=new User();

$etabs=new Etab();

$localadmins= new Localadmin();

$parents=new ParentX();

$classe=new Classe();

$compteuserid=$_SESSION['user']['IdCompte'];

$imageprofile=$user->getImageProfilebyId($compteuserid);

$logindata=$user->getLoginProfilebyId($compteuserid);

$tablogin=explode("*",$logindata);

$datastat=$user->getStatis();

$tabstat=explode("*",$datastat);

// $classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);

// $nbclasse=$classe->getNumberClassesOfTeatcherId($_SESSION['user']['IdCompte']);



if(strlen($imageprofile)>0)

{

  $lienphoto="../photo/".$emailUti."/".$imageprofile;

}else {

  $lienphoto="../photo/user5.jpg";

}


$parentlyStudent=$parents->getDifferentStudentByParentId($_SESSION['user']['IdCompte']);

$concatstudentids="";
foreach ($parentlyStudent as $value):
  $concatstudentids=$concatstudentids.$value->id_compte.",";
endforeach;

$concatstudentids=substr($concatstudentids, 0, -1);

$montantAttente=$parents->getAllversementOfParentAttente($concatstudentids,$compteuserid);
$montantpaiements=$parents->getAllversementOfParent($concatstudentids,$compteuserid);
$versementHisto=$parents->getHistoriqueversement($concatstudentids,$compteuserid);


$courseid=$_GET['course'];


$datasinscriptions=$etabs->getCodeEtabOfStudentInscript($_GET['studentid']);
foreach ($datasinscriptions as $datasinscription):
  $codeEtabInscript=$datasinscription->codeEtab_inscrip;
  $sessionEtabInscript=$datasinscription->session_inscrip;
  $classeidInscript=$datasinscription->idclasse_inscrip;
endforeach;

$classeid=$classeidInscript;
$codeEtabsession=$codeEtabInscript;
$libellesessionencours=$sessionEtabInscript;


// $courses=$etabs->getAllTeatchercourses($compteuserid,$codeEtabsession,$libellesessionencours);
// $coursesdetails=$etabs->getAllcoursesdetails($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);

$coursesdetails=$etabs->getAllcoursesdetailsofClasses($courseid,$classeid,$codeEtabsession,$libellesessionencours);

$datacoursescomments=$etabs->getAllcoursescommentsTea($courseid,$classeid,$codeEtabsession,$libellesessionencours);
$nbonlinethisStudent=$etabs->getNBcoursescommentsStudent($courseid,$classeid,$codeEtabsession,$libellesessionencours,$_GET['studentid']);
$nbcomments=count($datacoursescomments);

$datacoursescommentonlines=$etabs->getAllcoursescommentonlinesTea($courseid,$classeid,$codeEtabsession,$libellesessionencours);
$nbonlines=count($datacoursescommentonlines);

// echo $compteuserid;

foreach ($coursesdetails as  $datacourses):
  $descricourses=$datacourses->descri_courses;
  $durationcourses=$datacourses->duree_courses;
  $teatchercourses=$datacourses->nom_compte;
  $datercourses=$datacourses->date_courses;
  $namecourses=$datacourses->libelle_courses;
  $classecourses=$datacourses->libelle_classe;
  $classeidcourses=$datacourses->id_classe;
  $matiereidcourses=$datacourses->id_mat;
  $libellematcourses=$datacourses->libelle_mat;
  $statutcourses=$datacourses->statut_courses;
  $filescourses=$datacourses->support_courses;
  $discussion_courses=$datacourses->discussion_courses;
  $teatcherid_courses=$datacourses->teatcher_courses;

endforeach;



// Nous allons recuperer les sections du cours

$coursesSections=$etabs->getAllcoursesSectionsofClasses($courseid,$classeid,$codeEtabsession,$libellesessionencours);

$coursescompetences=$etabs->getAllcoursesCompsofClasses($courseid,$classeid,$codeEtabsession,$libellesessionencours);

$courseshomeworks=$etabs->getAllcoursesHomeworkssofClasses($courseid,$classeid,$codeEtabsession,$libellesessionencours);

$coursesupports=$etabs->getAllsuppourtsStudent($courseid,$classeid,$codeEtabsession,$libellesessionencours);



 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
 <?php
 include("style.php")
 ?>

 <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
</head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::detailscourse ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="#"><?php echo L::CourseMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::detailscourse ?></li>
                          </ol>
                      </div>
                  </div>



                  <?php

                        if(isset($_SESSION['user']['addclasseok']))
                        {

                          ?>
                          <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?php
                        //echo $_SESSION['user']['addetabok'];
                        ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                           </a>
                        </div-->
      <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
      <script src="../assets/js/sweetalert2.min.js"></script>

                    <script>
                    Swal.fire({
                    type: 'success',
                    title: 'Félicitations',
                    text: '<?php echo $_SESSION['user']['addclasseok'] ?>',

                    })
                    </script>
                          <?php
                          unset($_SESSION['user']['addclasseok']);
                        }

                         ?>




                          <div class="row">
                <div class="col-md-12">
                  <!-- BEGIN PROFILE SIDEBAR -->
                  <div class="profile-sidebar">
                    <div class="card card-topline-aqua">
                      <div class="card-body no-padding height-9">
                        <div class="row">
                          <div class="course-picture">
                            <img src="../photo/course2.jpg" class="img-responsive"
                              alt=""> </div>
                        </div>
                        <div class="profile-usertitle">
                          <div class="profile-usertitle-name"> <?php echo utf8_decode(utf8_encode($namecourses));?> </div>
                        </div>
                        <!-- END SIDEBAR USER TITLE -->
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-head card-topline-aqua">
                        <header><?php echo L::aproposcourse ?></header>
                      </div>
                      <div class="card-body no-padding height-9">
                        <div class="profile-desc">
                          <?php //echo utf8_decode(utf8_encode($descricourses)); ?>
                        </div>
                        <ul class="list-group list-group-unbordered">
                          <li class="list-group-item">
                            <b><i class=""></i><?php echo L::MatiereMenusingle ?></b>
                            <div class="profile-desc-item pull-right"><?php echo utf8_decode(utf8_encode($libellematcourses)); ?>  </div>
                          </li>
                          <li class="list-group-item">
                            <b><?php echo L::ClassestudentTab ?> </b>
                            <div class="profile-desc-item pull-right"><?php echo utf8_decode(utf8_encode($classecourses)); ?></div>
                          </li>
                          <li class="list-group-item">
                            <b><?php echo L::LittleDateVersements ?></b>
                            <div class="profile-desc-item pull-right">
                              <?php
                              $tabdate=explode("-",$datercourses);

                              echo $tabdate[2]." ".obtenirLibelleMois($tabdate[1])." ".$tabdate[0];
                              ?>
                            </div>
                          </li>

                          <li class="list-group-item">
                            <b><?php echo L::Duration ?> </b>
                            <div class="profile-desc-item pull-right"><?php echo utf8_decode(utf8_encode($durationcourses)); ?></div>
                          </li>


                          <li class="list-group-item">
                            <b><?php echo L::ProfsMenusingle ?></b>
                            <div class="profile-desc-item pull-right"><?php echo utf8_decode(utf8_encode($teatchercourses)); ?>  </div>
                          </li>

                        </ul>


                      </div>
                      <div class="row list-separated profile-stat">
                        <div class="col-md-6 col-sm-4 col-6">
                          <?php
                          if($statutcourses==1 &&$discussion_courses==0)
                          {
                            ?>
                              <a href="#" class="btn btn-success btn-md" style="margin-left:70px;" > <i class="fa fa-send"></i><?php echo L::Publishcourse ?></a>
                            <?php
                          }else {
                            if($nbonlinethisStudent==0)
                            {
                              if($discussion_courses==1)
                              {
                                ?>
                          <!-- <a href="#" class="btn btn-success btn-md" style="margin-left:70px;" onclick="commentonline(<?php //echo $courseid;?>,<?php //echo $classeid;  ?>,<?php// echo $compteuserid; ?>,'<?php //echo $codeEtabsession; ?>','<?php //echo $libellesessionencours; ?>',<?php //echo $matiereidcourses; ?>,<?php //echo $teatcherid_courses; ?> )"> <i class="fa fa-check-circle "></i> <?php echo L::Suivrecoursediscuss ?> </a> -->
                                <?php
                              }else if($discussion_courses==2) {
                                ?>
                          <!-- <a href="#" class="btn btn-danger btn-md" style="margin-left:70px;" > <i class="fa fa-check-circle "></i> <?php //cho L::Stopcoursediscuss ?> </a> -->
                                <?php
                              }

                            }else if($nbonlinethisStudent>0)
                            {
                              if($discussion_courses==1)
                              {
                                ?>
                              <!-- <a href="#" class="btn btn-success btn-md" style="margin-left:70px;"  > <i class="fa fa-check-circle "></i><?php //echo L::coursediscussonline ?> </a> -->
                                <?php
                              }else if($discussion_courses==2) {
                                ?>
                              <!-- <a href="#" class="btn btn-danger btn-md" style="margin-left:70px;" > <i class="fa fa-check-circle "></i> <?php//echo L::Stopcoursediscuss ?> </a> -->
                                <?php
                              }
                            }


                          }
                           ?>
                        </div>

                      </div>
                    </div>
                  </div>
                  <!-- END BEGIN PROFILE SIDEBAR -->
                  <!-- BEGIN PROFILE CONTENT -->
                  <div class="profile-content">
                    <div class="row">

                      <div class="col-md-12">
                        <div class="card card-topline-aqua">
                          <div class="card-head" style="text-align:center;">
                              <header><span ><?php echo L::CourseSupport ?> ( <?php echo count($coursesupports); ?> )</span> </header>

                          </div>
                          <div class="card-body no-padding height-9">
                            <div class="" >
                              <?php
                              $i=1;
                              foreach ($coursesupports as $value):
                                if(strlen($value->fichier_support)>0)
                                {
                                  $lien="../courses/".$classecourses."/".$datercourses."/".$libellematcourses."/".$value->fichier_support;
                                  ?>
                                    <a href="<?php echo $lien;  ?>" target="_blank" class="btn btn-warning btn-md" ><i class="fa fa-download"></i> <?php echo L::LibelleSupport ?> <?php echo $i; ?></a>
                                  <?php
                                }
                                $i++;
                              endforeach;
                               ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="card">
                          <div class="card-topline-aqua">
                            <header></header>
                          </div>
                          <div class="white-box">
                            <!-- Nav tabs -->
                            <!-- Tab panes -->
                            <div class="tab-content">
                              <div class="tab-pane active fontawesome-demo">
                                <div id="biography" class="col-md-12">

                                  <h4 class="font-bold"><?php echo L::CourseTheme ?></h4>
                                  <hr>
                                  <ul>
                                    <?php
                                    foreach ($coursesSections as  $value):
                                      ?>
                                      <li><?php echo utf8_decode(utf8_encode($value->libelle_coursesec)); ?></li>
                                      <?php
                                    endforeach;
                                     ?>

                                  </ul>
                                  <br>
                                  <h4 class="font-bold"><?php echo L::CourseAble ?>: </h4>
                                  <hr>
                                  <ul>
                                    <?php
                                    foreach ($coursescompetences as  $value):
                                      ?>
                                      <li><?php echo utf8_decode(utf8_encode($value->libelle_coursecomp)); ?></li>

                                      <?php
                                    endforeach;
                                     ?>

                                  </ul>
                                  <br>
                                  <h4 class="font-bold"><?php echo L::CourseHomeworksOrExercice ?> </h4>
                                  <hr>
                                  <ul>
                                    <?php
                                    foreach ($courseshomeworks as $value):
                                      ?>
                                      <li><?php echo utf8_decode(utf8_encode($value->libelle_coursewh)); ?></li>

                                      <?php
                                    endforeach;
                                     ?>

                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- END PROFILE CONTENT -->
                </div>
              </div>
              <?php
              if($discussion_courses==0||$discussion_courses==2)
              {

              }else {
                ?>
                <div class="row"
                style="<?php
                if($nbonlinethisStudent==0)
                {
                  echo "display:none";
                }else {
                  echo "display:inline";
                }
                 ?>">



                        <div class="col-md-12">

                          <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-8">
                              <div class="card  card-box">
                                    <div class="card-head">
                                        <header><?php echo L::Comments ?></header>
                                        <input type="hidden" name="nbcomments" id="nbcomments" value="<?php echo $nbcomments  ?>">
                                        <input type="hidden" name="nbonlines" id="nbonlines"  value="<?php echo $nbonlines; ?>">
                                        <?php

                                         ?>
                                        <div class="tools">

                                        </div>
                                    </div>
                                    <div class="card-body no-padding height-10">
                                        <div class="row">
                                            <ul class="chat nice-chat small-slimscroll-style" id="rowcomment">
                                              <?php
                                              if($nbcomments>0)
                                              {

                                                foreach ($datacoursescomments as $value):

                                                  if($value->addby_commentc==$compteuserid)
                                                  {
                                                    $studentInfos=$student->getAllInformationsOfStudentOne($compteuserid,$libellesessionencours);
                                                    foreach ($studentInfos as $personnal):
                                                      $matriculestudents=$personnal->matricule_eleve;
                                                      $photostudents=$personnal->photo_compte;
                                                    endforeach;
                                                    $lienphoto="";

                                                    if(strlen($photostudents)>0)
                                                    {
                                                      $lienphoto="../photo/Students/".$matriculestudents."/".$photostudents;
                                                    }else {
                                                      $lienphoto="../photo/user5.jpg";
                                                    }
                                                    ?>
                                                    <li class="in"><img src="<?php echo $lienphoto; ?>" class="avatar" alt="">
                                                        <div class="message">
                                                          <?php
                                                          $libesexe="";
                                                          $sexe=$etabs->getUtilisateurSexe($value->addby_commentc);
                                                          $dateadded=$value->date_commentc;
                                                          $tabdateadded=explode(" ",$dateadded);

                                                          $dateinsert=$tabdateadded[0];
                                                          $tabdateinsert=explode("-",$dateinsert);
                                                          $yearsinsert=$tabdateinsert[0];
                                                          $monthinsert=$tabdateinsert[1];
                                                          $daysinsert=$tabdateinsert[2];
                                                          $newdate=$daysinsert."-".$monthinsert."-".$yearsinsert;

                                                          if($sexe=="F")
                                                          {
                                                            $libesexe="Mme";
                                                          }else {
                                                            $libesexe="Mr";
                                                          }

                                                           ?>
                                                            <span class="arrow"></span> <a class="name" href="#"><?php echo $libesexe." ".utf8_decode(utf8_encode($etabs->getUtilisateurName($value->addby_commentc))); ?></a> <span class="datetime"><?php echo $newdate." ".$tabdateadded[1]; ?></span>
                                                            <span class="body"><?php echo utf8_decode(utf8_encode($value->message_commentc)); ?>  </span>
                                                        </div>
                                                    </li>
                                                    <?php
                                                  }else if($value->addby_commentc==$value->teatcherid_commentc){
                                                    $datateatchers=$teatcher->getTeatcherInforamtionsById($value->teatcherid_commentc);
                                                    foreach ($datateatchers as $valueTea):
                                                      $emailteatchers=$valueTea->email_compte;
                                                      $phototeatchers=$valueTea->photo_compte;
                                                    endforeach;
                                                    $lienphoto="";
                                                    if(strlen($phototeatchers)>0)
                                                    {
                                                      $lienphoto="../photo/".$emailteatchers."/".$phototeatchers;
                                                    }else {
                                                      $lienphoto="../photo/user5.jpg";
                                                    }
                                                    ?>
                                                    <li class="outin"><img src="<?php echo $lienphoto ?>" class="avatar" alt="">
                                                      <div class="message">
                                                        <?php
                                                        $libesexe="";
                                                        $sexe=$etabs->getUtilisateurSexe($value->addby_commentc);
                                                        $dateadded=$value->date_commentc;
                                                        $tabdateadded=explode(" ",$dateadded);

                                                        $dateinsert=$tabdateadded[0];
                                                        $tabdateinsert=explode("-",$dateinsert);
                                                        $yearsinsert=$tabdateinsert[0];
                                                        $monthinsert=$tabdateinsert[1];
                                                        $daysinsert=$tabdateinsert[2];
                                                        $newdate=$daysinsert."-".$monthinsert."-".$yearsinsert;

                                                        if($sexe=="F")
                                                        {
                                                          $libesexe="Mme";
                                                        }else {
                                                          $libesexe="Mr";
                                                        }

                                                         ?>
                                                          <span class="arrow"></span> <a class="name" href="#" style="color:white"><?php echo $libesexe." ".tronquer(utf8_decode(utf8_encode($etabs->getUtilisateurName($value->addby_commentc))),20); ?></a> <span class="datetime" style="color:white"><?php echo $newdate." ".$tabdateadded[1]; ?></span>
                                                          <span class="body"><?php echo utf8_decode(utf8_encode($value->message_commentc)); ?>  </span>
                                                      </div>
                                                    </li>
                                                    <?php
                                                  }else {
                                                    $studentInfos=$student->getAllInformationsOfStudentOne($value->addby_commentc,$libellesessionencours);
                                                    foreach ($studentInfos as $personnal):
                                                      $matriculestudents=$personnal->matricule_eleve;
                                                      $photostudents=$personnal->photo_compte;
                                                    endforeach;
                                                    $lienphoto="";

                                                    if(strlen($photostudents)>0)
                                                    {
                                                      $lienphoto="../photo/Students/".$matriculestudents."/".$photostudents;
                                                    }else {
                                                      $lienphoto="../photo/user5.jpg";
                                                    }
                                                    ?>
                                                    <li class="out"><img src="<?php echo $lienphoto; ?>" class="avatar" alt="">
                                                      <div class="message">
                                                        <?php
                                                        $libesexe="";
                                                        $sexe=$etabs->getUtilisateurSexe($value->addby_commentc);
                                                        $dateadded=$value->date_commentc;
                                                        $tabdateadded=explode(" ",$dateadded);

                                                        $dateinsert=$tabdateadded[0];
                                                        $tabdateinsert=explode("-",$dateinsert);
                                                        $yearsinsert=$tabdateinsert[0];
                                                        $monthinsert=$tabdateinsert[1];
                                                        $daysinsert=$tabdateinsert[2];
                                                        $newdate=$daysinsert."-".$monthinsert."-".$yearsinsert;

                                                        if($sexe=="F")
                                                        {
                                                          $libesexe="Mme";
                                                        }else {
                                                          $libesexe="Mr";
                                                        }

                                                         ?>
                                                          <span class="arrow"></span> <a class="name" href="#"><?php echo $libesexe." ".utf8_decode(utf8_encode($etabs->getUtilisateurName($value->addby_commentc))); ?></a> <span class="datetime"><?php echo $newdate." ".$tabdateadded[1]; ?></span>
                                                          <span class="body"><?php echo utf8_decode(utf8_encode($value->message_commentc)); ?>  </span>
                                                      </div>
                                                    </li>
                                                    <?php
                                                  }

                                                endforeach;
                                                ?>

                                                <?php
                                              }else {
                                                ?>

                                                <?php
                                              }
                                               ?>

                                            </ul>
                                            <div class="box-footer chat-box-submit">
                                <form id="commentForm">
                                  <div class="input-group">
                                    <div class="col-md-12">
                                    <div class="form-group">

                                      <div class="row">
                                        <div class="col-md-12">
                                            <input type="text" name="message"  id="message"placeholder="<?php echo L::CommentsEnter ?>" class="form-control input-height">
                                        </div><br><br>
                                        <div class="col-md-12">
                                          <span class="input-group-btn pull-right">
                                          <button type="submit" class="btn btn-success btn-md" style="height:40px"><?php echo L::Sendbutton ?> <i class="fa fa-arrow-right"></i></button>
                                          </span>
                                        </div>
                                      </div>




                                         </div>
                                     </div>

                                     </div>
                                </form>
                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                              <div class="card  card-box" style="height:460px;">
                                 <div class="card-head">
                                     <header><?php echo L::Online ?></header>
                                     <div class="tools">

                                     </div>
                                 </div>
                                 <div class="card-body no-padding height-10">
                                     <div class="row">
                                         <div class="noti-information notification-menu">
                                           <div class="notification-list mail-list not-list small-slimscroll-style" id="rowconnexion">

                                             <?php
                                             if($nbonlines>0 && $nbonlines==1)
                                             {
                                               foreach ($datacoursescommentonlines as  $value):
                                                 $studentInfos=$student->getAllInformationsOfStudentOne($value->id_compte,$libellesessionencours);
                                                 foreach ($studentInfos as $personnal):
                                                   $matriculestudents=$personnal->matricule_eleve;
                                                   $photostudents=$personnal->photo_compte;
                                                 endforeach;
                                                 $lienphoto="";

                                                 if(strlen($photostudents)>0)
                                                 {
                                                   $lienphoto="../photo/Students/".$matriculestudents."/".$photostudents;
                                                 }else {
                                                   $lienphoto="../photo/user5.jpg";
                                                 }
                                               ?>
                                               <div class="row">
                                                 <div class="col-md-8">
                                                   <span class="text-purple" style="width:1500px"><?php echo utf8_decode(utf8_encode($etabs->getUtilisateurName($value->id_compte))); ?></span>
                                                   <div>
                                                    <span class="clsAvailable"><?php echo L::Online ?></span>
                                                  </div>
                                                 </div>
                                                 <div class="col-md-4">
                                                   <a href="javascript:;" class="single-mail"> <img src="<?php echo $lienphoto; ?>" alt="" width="40" height="40" class="pull-right" style="border-radius:10px;margin-left:200px">

                                                   </a>
                                                 </div>

                                               </div>
                                               <?php
                                             endforeach;
                                                ?>

                                               <?php
                                             }else if($nbonlines>0 && $nbonlines>1)
                                             {
                                               foreach ($datacoursescommentonlines as  $value):
                                                 $studentInfos=$student->getAllInformationsOfStudentOne($value->id_compte,$libellesessionencours);
                                                 foreach ($studentInfos as $personnal):
                                                   $matriculestudents=$personnal->matricule_eleve;
                                                   $photostudents=$personnal->photo_compte;
                                                 endforeach;
                                                 $lienphoto="";

                                                 if(strlen($photostudents)>0)
                                                 {
                                                   $lienphoto="../photo/Students/".$matriculestudents."/".$photostudents;
                                                 }else {
                                                   $lienphoto="../photo/user5.jpg";
                                                 }

                                                 ?>
                                                 <a href="javascript:;" class="single-mail"> <img src="<?php echo $lienphoto; ?>" alt="" width="40" height="40" class="pull-right" style="border-radius:10px"> <span class="text-purple"><?php echo utf8_decode(utf8_encode($etabs->getUtilisateurName($value->id_compte))); ?></span>
                                                   <div>
                                                               <span class="clsAvailable"><?php echo L::Online ?></span>
                                                           </div>
                                                 </a>
                                                 <?php
                                               endforeach;
                                             }
                                              ?>





                                           </div>

                                         </div>
                                     </div>
                                 </div>
                             </div>
                </div>

                          </div>

                        </div>
                </div>
                <?php
              }
               ?>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>

 	<script src="../assets2/plugins/popper/popper.min.js" ></script>

     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>

 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>

     <!-- bootstrap -->

     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>

     <!-- calendar -->
     <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
    <script src="../assets2/js/pages/table/table_data.js" ></script>
     <script src="../assets2/plugins/moment/moment.min.js" ></script>

     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>

     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>

     <!-- Common js-->

 	<script src="../assets2/js/app.js" ></script>

     <script src="../assets2/js/layout.js" ></script>

 	<script src="../assets2/js/theme-color.js" ></script>

 	<!-- Material -->

 	<script src="../assets2/plugins/material/material.min.js"></script>









    <!-- morris chart -->

    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>

    <script src="../assets2/plugins/morris/raphael-min.js" ></script>

    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->



   <script>

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   $(document).ready(function() {







   });



   </script>

    <!-- end js include path -->

  </body>



</html>
