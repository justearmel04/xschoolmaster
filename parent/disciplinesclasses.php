<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');
require_once('../controller/functions.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$matiereschool=new Matiere();
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$_GET['codeEtab'];
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
  $datadisciplines=$matiereschool->getAllSubjectOfClasses($_GET['classe'],$codeEtabAssigner,$libellesessionencours);
}



$nbnotificationstandby=$parents->getParentnotificationstandbyNb($compteuserid);

//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($compteuserid);



//var_dump($datadisciplines);
 ?>
 <br>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <?php
 include("style.php")
?>

  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />

 </head>

 <!-- END HEAD -->
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
    include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Disciplines classe</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Disciplines clase</li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
          <div class="row ">


                <!-- /.col -->

                <!-- /.col -->

                <!-- /.col -->

                <!-- /.col -->
              </div>
					<div class="state-overview">

						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


          <div class="row">

            <div class="col-md-12 col-sm-12">
                                       <div class="panel tab-border card-box">
                                           <header class="panel-heading panel-heading-gray custom-tab ">
                                               <ul class="nav nav-tabs">
                                                   <li class="nav-item"><a href="#home" data-toggle="tab" class="active"> <i class="fa fa-list"></i> Liste Disciplines</a>
                                                   </li>
                                                   <li class="nav-item"><a href="#about" data-toggle="tab">Courbe Disciplines</a>
                                                   </li>

                                               </ul>
                                           </header>
                                           <div class="panel-body">
                                               <div class="tab-content">
                                                   <div class="tab-pane active" id="home">
                                                     <div class="row">
                                                       <div class="col-sm-12">
                                   <div class="card  card-box">
                                       <div class="card-head">
                                           <header></header>
                                           <div class="tools">
                                               <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                         <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                         <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                           </div>
                                       </div>
                                       <div class="card-body ">
                                         <?php

                                          ?>
                                           <div class="table-scrollable">
                                               <table class="table table-striped table-hover">
                                                   <thead>
                                                       <tr>
                                                           <th>Matière</th>
                                                           <th> Coefficient </th>
                                                           <th> Professeur </th>
                                                           <th> <?php echo L::Actions?> </th>

                                                       </tr>
                                                   </thead>
                                                   <tbody>
                                                     <?php
                                                      $tableauLibellematiere=array();
                                                      $tableauCouleurMatiere=array();
                                                      $tableauvaleurMatiere=array();
                                                     $j=1;
                                                     foreach ($datadisciplines as $value):

                                                      ?>
                                                       <tr>
                                                           <td><span class="label label-sm label-success"> <?php echo $value->libelle_mat; ?></span></td>
                                                           <td><?php echo $value->coef_mat; ?></td>
                                                           <td><?php echo $value->nom_compte ." ".$value->prenom_compte; ?></td>
                                                           <td>
                                                             <?php
                                                             $nbsessionOn=$session->getNumberSessionEncoursOn($_GET['codeEtab']);

                                                             if($nbsessionOn>0)
                                                             {
                                                               //recuperer la session en cours
                                                               $sessionencours=$session->getSessionEncours($_GET['codeEtab']);
                                                               $tabsessionencours=explode("*",$sessionencours);
                                                               $libellesessionencours=$tabsessionencours[0];
                                                               $sessionencoursid=$tabsessionencours[1];
                                                               $typesessionencours=$tabsessionencours[2];
                                                             }
                                                             $etablissementType=$etabs->DetermineTypeEtab($_GET['codeEtab']);
                                                             $paysetablissement=$etabs->getpaysidofschool($_GET['codeEtab']);
                                                             if($etablissementType==1||$etablissementType==3)
                                                             {
                                                               if($etablissementType==1)
                                                               {
                                                                 //etablissement primaire
                                                                 if($paysetablissement==4)
                                                                 {
                                                                   //etablissement primaire au cameroun
                                                                   ?>
                                                                   <a href="performancevolution.php?classe=<?php echo $value->id_classe;?>&codeEtab=<?php echo $codeEtabAssigner;?>&matiere=<?php echo$value->id_mat ?>&studetid=<?php echo $_GET['studentid'] ?>" class="btn btn-warning btn-md" title="Evolution des notes"> <i class="fa fa-list"></i>  </a>
                                                                   <?php
                                                                 }else {
                                                                   //etablissement primaire dans un autre pays que le cameroun
                                                                   ?>
                                                                   <a href="performancevolutionother.php?classe=<?php echo $value->id_classe;?>&codeEtab=<?php echo $codeEtabAssigner;?>&matiere=<?php echo$value->id_mat ?>&studetid=<?php echo $_GET['studentid'] ?>" class="btn btn-warning btn-md" title="Evolution des notes"> <i class="fa fa-list"></i>  </a>
                                                                   <?php
                                                                 }
                                                               }else if($etablissementType==3)
                                                               {
                                                                 //etablissement maternelle
                                                                 ?>
                                                                <a href="performancevolutionother.php?classe=<?php echo $value->id_classe;?>&codeEtab=<?php echo $codeEtabAssigner;?>&matiere=<?php echo$value->id_mat ?>&studetid=<?php echo $_GET['studentid'] ?>" class="btn btn-warning btn-md" title="Evolution des notes"> <i class="fa fa-list"></i>  </a>
                                                                 <?php
                                                               }
                                                             }else {
                                                               ?>
                                                               <a href="performancevolution.php?classe=<?php echo $value->id_classe;?>&codeEtab=<?php echo $codeEtabAssigner;?>&matiere=<?php echo$value->id_mat ?>&studetid=<?php echo $_GET['studentid'] ?>" class="btn btn-warning btn-md" title="Evolution des notes"> <i class="fa fa-list"></i>  </a>
                                                               <?php
                                                             }
                                                              ?>
                                                           </td>
                                                           <?php
                                                           $tableauLibellematiere[$j]=$value->libelle_mat;
                                                           $tableauCouleurMatiere[$j]=getRandomColorHex();
                                                           $tableauvaleurMatiere[$j]=1;
                                                            ?>
                                                       </tr>
                                                     <?php
                                                     $j++;
                                                     endforeach;
                                                      ?>

                                                   </tbody>
                                               </table>
                                             </div>
                                       </div>
                                   </div>
                               </div>
                              </div>
                           </div>
                           <div class="tab-pane" id="about">
                       <div class="row">
                       <div class="col-md-12">
                           <div class="card card-box">
                               <div class="card-head">
                                   <header></header>
                                   <div class="tools">
                                       <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                     <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>

                                   </div>
                               </div>
                               <div class="card-body " id="chartjs_pie_parent">
                                   <div class="row">
                                        <canvas id="chartjs_discipline" height="120"></canvas>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
                </div>
            </div>
           </div>
        </div>
    </div>
 </div>
   <!-- start new patient list -->
    <!-- end new patient list -->
 </div>
 </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!--Chart JS-->
     <script src="../assets2/plugins/chart-js/Chart.bundle.js" ></script>
     <script src="../assets2/plugins/chart-js/utils.js" ></script>
     <script src="../assets2/js/pages/chart/chartjs/chartjs-data.js" ></script>
     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   $(document).ready(function() {
   	var randomScalingFactor = function() {
           return Math.round(Math.random() * 100);
       };

       var config = {
           type: 'pie',
       data: {
           datasets: [{
               data: [
                 <?php
                                  for($i=1;$i<$j;$i++)
                                  {
                                    echo '"'.@$tableauvaleurMatiere[$i].'",';
                                  }
                  ?>
               ],
               backgroundColor: [
                 <?php
                                  for($i=1;$i<$j;$i++)
                                  {
                                    echo '"'.@$tableauCouleurMatiere[$i].'",';
                                  }
                  ?>
               ],
               label: 'Dataset 1'
           }],
           labels: [
             <?php
                              for($j=1;$j<$i;$j++)
                              {
                                echo '"'.@$tableauLibellematiere[$j].'",';
                              }
              ?>
           ]
       },
       options: {
           responsive: true
       }
   };

       var ctx = document.getElementById("chartjs_discipline").getContext("2d");
       window.myPie = new Chart(ctx, config);
   });

   </script>
    <!-- end js include path -->
  </body>

</html>
