<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];

$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$classe=new Classe();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);
// $classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);
// $nbclasse=$classe->getNumberClassesOfTeatcherId($_SESSION['user']['IdCompte']);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$parentlyStudent=$parents->getDifferentStudentByParentId($_SESSION['user']['IdCompte']);


// $alletab=$etabs->getAllEtab();
// $locals=$localadmins->getAllAdminLocal();
// $allparents=$parents->getAllParent();

 $nbnotificationstandby=$parents->getParentnotificationstandbyNb($compteuserid);

//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($compteuserid);


 ?>
<br>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
   <?php
 include("style.php")
?>
</head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
    include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Liste Enfants</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Liste Enfants</li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


          <div class="row">

            <div class="col-md-12">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header></header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>

                                    </div>
                                </div>
                                <div class="card-body ">

                                    <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                        <thead>
                                            <tr>
                                                <th> Photo </th>
                                                <th> Matricule </th>
                                                <th> Nom & Prénoms </th>
                                                <th> Etablissement </th>
                                                <th> Classe </th>
                                                <th> <?php echo L::Actions?> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                          foreach ($parentlyStudent as $value):
                                           ?>
                                            <tr class="odd gradeX">
                                              <td class="patient-img">
                                                <?php

                                                if(strlen($value->photo_compte)>0)
                                                {
                                                  $lien="../photo/Students/".$value->matricule_eleve."/".$value->photo_compte;
                                                }else {
                                                  $lien="../photo/user5.jpg";
                                                }
                                                 ?>
                                                  <img src="<?php echo $lien?>" alt="">
                                              </td>
                                                <td> <?php echo $value->matricule_eleve; ?></td>
                                                <td> <?php echo $value->nom_eleve." ".$value->prenom_eleve; ?> </td>
                                                <td>
                                                    <?php
                                                  $dataSchool=$parents->getStudentCurrentlyinscription($_SESSION['user']['IdCompte'],$value->idcompte_eleve);

                                                  $array=json_encode($dataSchool,true);
                                                  $someArray = json_decode($array, true);
                                                  $donnees=$someArray[0]["codeEtab_inscrip"]."*".$someArray[0]["id_classe"]."*".$someArray[0]["libelle_etab"]."*".$someArray[0]["libelle_classe"];

                                                  $escampe=explode("*",$donnees);

                                                  $codeEtab=$escampe[0];
                                                  $idclasse=$escampe[1];
                                                  $libelleEtab=$escampe[2];
                                                  $libelleclasse=$escampe[3];

                                                  echo $libelleEtab;

                                                     ?>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success"> <?php echo $libelleclasse; ?></span>
                                                </td>
                                                <td>
                                                  <?php
                                                  $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtab);

                                                  if($nbsessionOn>0)
                                                  {
                                                    //recuperer la session en cours
                                                    $sessionencours=$session->getSessionEncours($codeEtab);
                                                    $tabsessionencours=explode("*",$sessionencours);
                                                    $libellesessionencours=$tabsessionencours[0];
                                                    $sessionencoursid=$tabsessionencours[1];
                                                    $typesessionencours=$tabsessionencours[2];
                                                  }
                                                  $etablissementType=$etabs->DetermineTypeEtab($codeEtab);
                                                  $paysetablissement=$etabs->getpaysidofschool($codeEtab);
                                                   ?>
                                                    <a href="listquizs.php?idcompte=<?php echo $value->idcompte_eleve?>&classe=<?php echo $idclasse;?>&codeEtab=<?php echo $codeEtab; ?>" class="btn btn-warning btn-xs" title="Recap des notes"> <i class="fa fa-eye" ></i> </a>
                                                </td>
                                                </tr>
                                                <?php
                                          endforeach;
                                             ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                       </div>
                      <!-- start new patient list -->
                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- calendar -->
     <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
   <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
    <script src="../assets2/js/pages/table/table_data.js" ></script>
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>

    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }
   $(document).ready(function() {



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
