<?php

session_start();

require_once('../class/User.php');

require_once('../class/Etablissement.php');

require_once('../class/LocalAdmin.php');

require_once('../class/Parent.php');

require_once('../class/Classe.php');

require_once('../class/Student.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();



$emailUti=$_SESSION['user']['email'];

$student=new Student();

$user=new User();

$etabs=new Etab();

$localadmins= new Localadmin();

$parents=new ParentX();

$classe=new Classe();

$compteuserid=$_SESSION['user']['IdCompte'];

$imageprofile=$user->getImageProfilebyId($compteuserid);

$logindata=$user->getLoginProfilebyId($compteuserid);

$tablogin=explode("*",$logindata);

$datastat=$user->getStatis();

$tabstat=explode("*",$datastat);

// $classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);

// $nbclasse=$classe->getNumberClassesOfTeatcherId($_SESSION['user']['IdCompte']);



if(strlen($imageprofile)>0)

{

  $lienphoto="../photo/".$emailUti."/".$imageprofile;

}else {

  $lienphoto="../photo/user5.jpg";

}



$parentlyStudent=$parents->getDifferentStudentByParentId($_SESSION['user']['IdCompte']);



$dataSchool=$parents->getStudentCurrentlyinscription($_SESSION['user']['IdCompte'],$_GET['idcompte']);

$array=json_encode($dataSchool,true);

$someArray = json_decode($array, true);

$donnees=$someArray[0]["codeEtab_inscrip"]."*".$someArray[0]["id_classe"]."*".$someArray[0]["libelle_etab"]."*".$someArray[0]["libelle_classe"]."*".$someArray[0]["pays_etab"]."*".$someArray[0]["devises_pays"];



$escampe=explode("*",$donnees);



$codeEtab=$escampe[0];

$idclasse=$escampe[1];

$libelleEtab=$escampe[2];

$libelleclasse=$escampe[3];

$idpays=$escampe[4];

$devisespays=$escampe[5];

// $alletab=$etabs->getAllEtab();

// $locals=$localadmins->getAllAdminLocal();

// $allparents=$parents->getAllParent();



$datastudents=$student->getelevesinfosbyidcompte($_GET['idcompte']);



$arraystudent=json_encode($datastudents,true);

$studentArray = json_decode($arraystudent, true);





 ?>

<!DOCTYPE html>

<html lang="en">

<!-- BEGIN HEAD -->



<head>

    <meta charset="utf-8" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta content="width=device-width, initial-scale=1" name="viewport" />

    <title><?php echo L::Titlepage?></title>

    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">

    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">



    <!-- google font -->

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />

	<!-- icons -->

    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />

	<!--bootstrap -->

  <!--bootstrap -->

	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

	<!-- Material Design Lite CSS -->

	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >

	<link href="../assets2/css/material_style.css" rel="stylesheet">

	<!-- Theme Styles -->

    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />

    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />

	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />

	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />

	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>

    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

 </head>

 <!-- END HEAD -->

<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">

    <div class="page-wrapper">

        <!-- start header -->

		<?php

include("header.php");

    ?>

        <!-- end header -->

        <!-- start page container -->

        <div class="page-container">

 			<!-- start sidebar menu -->

 			<?php

				include("menu.php");

			?>

			 <!-- end sidebar menu -->

			<!-- start page content -->

            <div class="page-content-wrapper">

                <div class="page-content">

                    <div class="page-bar">

                        <div class="page-title-breadcrumb">

                            <div class=" pull-left">

                                <div class="page-title">Ajouter une souscription</div>

                            </div>

                            <ol class="breadcrumb page-breadcrumb pull-right">

                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>

                                </li>

                                <li><a class="parent-item" href="#">Ajouter une souscription</a>

                                </li>



                            </ol>

                        </div>

                    </div>

					<!-- start widget -->

					<div class="state-overview">

						<div class="row">



					        <!-- /.col -->



					        <!-- /.col -->



					        <!-- /.col -->



					        <!-- /.col -->

					      </div>

						</div>

					<!-- end widget -->

          <?php



                if(isset($_SESSION['user']['addetabexist']))

                {



                  ?>

                  <div class="alert alert-danger alert-dismissible fade show" role="alert">

                <?php

                echo $_SESSION['user']['addetabexist'];

                ?>

                <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                <span aria-hidden="true">&times;</span>

                   </a>

                </div>







                  <?php

                  unset($_SESSION['user']['addetabexist']);

                }



                 ?>





          <div class="row">



            <div class="col-sm-12 col-md-12">



              <div class="card-box">

                  <div class="card-body ">

                    <form class="" action="../controller/subscription.php" method="post" id="subscribeform">

                      <div class=" col-md-12 panel-headin" style="background-color:#009fe3; font-size:20px;color: white;text-align:center;"> Informations de l'élève </div>

                    <br>

                    <div class="row">

                      <div class="col-md-6">

                        <div class="form-group">

                        <label for=""><b>Matricule<span class="required"> * </span>: </b></label>



                      <input type="text" name="matstudent" id="matstudent" data-required="1" value="<?php echo $studentArray[0]["matricule_eleve"] ?>" class="form-control input-height " readonly />

                        </div>

                      </div>

                      <div class="col-md-6">





                       </div>

                                        <div class="col-md-6">

                    <div class="form-group">

                    <label for=""><b><?php echo L::Name?><span class="required"> * </span>: </b></label>



                  <input type="text" name="nomTea" id="nomTea" data-required="1" value="<?php echo $studentArray[0]["nom_compte"] ?>" class="form-control input-height " readonly/>

                    </div>



                    </div>

                    <div class="col-md-6">

                         <div class="form-group">

                           <label for=""><b>Prénoms <span class="required"> * </span>: </b></label>

                            <input type="text" name="prenomTea" id="prenomTea" data-required="1" value="<?php echo $studentArray[0]["prenom_compte"] ?>" class="form-control input-height" readonly />
			   <input type="hidden" name="etape" id="etape" value="2">
                          </div>





                     </div>



                     <div class="col-md-6">

 <div class="form-group">

 <label for=""><b>Etablissement<span class="required"> * </span>: </b></label>

<input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtab; ?>">

<input type="text" name="studentetab" id="studentetab" data-required="1" value="<?php echo $libelleEtab; ?>" class="form-control input-height " readonly />

 </div>



 </div>

 <div class="col-md-6">

      <div class="form-group">

        <label for=""><b>Classe <span class="required"> * </span>: </b></label>

         <input type="text" name="studentclasse" id="studentclasse" data-required="1" value="<?php echo $libelleclasse; ?>" class="form-control input-height" readonly />

         <input type="hidden" name="idclasse" id="idclasse" value="<?php echo $idclasse; ?>">

       </div>





  </div>

    </div>

    <div class=" col-md-12 panel-headin" style="background-color:#009fe3; font-size:20px;color: white;text-align:center;"> Choix de l'abonnement</div>

  <br>

  <div class="row">

    <div class="col-lg-6">

      <select class="form-control " id="choicesubscribe" name="choicesubscribe" style="width:100%;text-align:center;" onchange="determineAb()" >



        <option value="">Selectionner un Abonnement</option>

        <option value="1-1000">Abonnement mensuel</option>

        <option value="2-3000">Abonnement trimestriel</option>

        <option value="3-6000">Abonnement semestriel</option>

        <option value="4-12000">Abonnement annuel</option>

      </select>



    </div>

    <div class="col-lg-6">

    <input type="text"   name="montantab" id="montantab" data-required="1" placeholder="Montant Abonnement" class="form-control"  readonly/>

    <input type="hidden" name="devisecontry" id="devisecontry" value="<?php echo $devisespays; ?>">
<input type="hidden" name="studentidcompte" id="studentidcompte"  value="<?php echo $_GET['idcompte'] ?>">
    <input type="hidden" name="parentidcompte" id="parentidcompte"  value="<?php echo $_GET['parent'] ?>">

    </div>

  </div>

  <br>



  <div class=" col-md-12 panel-headin" style="background-color:#009fe3; font-size:20px;color: white;text-align:center;"> Paiement</div>

<br>

  <br>

  <div class="form-group row">

      <label class="control-label col-md-4">Téléphone

          <span class="required"> * </span>

      </label>

      <div class="col-md-6">

          <input type="text" name="paymentno" id="paymentno" data-required="1" placeholder="entrer le numéro ayant servit au paiement" class="form-control input-height" />

        </div>

  </div>

  <div class="form-group row">

      <label class="control-label col-md-4">Transaction Id

          <span class="required"> * </span>

      </label>

      <div class="col-md-6">

          <input type="text" name="transacId" id="transacId" data-required="1" placeholder="entrer le numéro de transaction Id" class="form-control input-height" /> </div>

  </div>

  <div class="form-group row">

      <label class="control-label col-md-4">Opérateur Mobile

          <span class="required"> * </span>

      </label>

      <div class="col-md-6">

          <select class="form-control input-height" name="selectmobileop" id="selectmobileop">

              <option value="">Selectionnez un opérateur</option>

              <option value="Category 1">Dr. Rajesh</option>

              <option value="Category 2">Dr. Sarah Smith</option>

              <option value="Category 3">Dr. John Deo</option>

              <option value="Category 3">Dr. Jay Soni</option>

              <option value="Category 3">Dr. Jacob Ryan</option>

              <option value="Category 3">Dr. Megha Trivedi</option>

          </select>

      </div>

  </div>

  <br>



  <div class="row">

    <div class="col-md-6">





<button type="submit" id="btnAccepter"  onclick="" class="btn btn-md btn-primary" >Valider</button>

<button type="button"  id="btnAnnuler"  class="btn btn-md btn-danger" >Annuler</button>





</div>

  </div>





                    </form>

                  </div>



              </div>



            </div>



          </div>





                     <!-- start new patient list -->



                    <!-- end new patient list -->



                </div>

            </div>

            <!-- end page content -->

            <!-- start chat sidebar -->



            <!-- end chat sidebar -->

        </div>

        <!-- end page container -->

        <!-- start footer -->

        <div class="page-footer">

            <div class="page-footer-inner"> 2019 &copy;

            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>

            </div>

            <div class="scroll-to-top">

                <i class="material-icons">eject</i>

            </div>

        </div>

        <!-- end footer -->

    </div>

    <!-- start js include path -->

    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>

  <script src="../assets2/plugins/popper/popper.min.js" ></script>

   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>

   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>

   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>

   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>

   <!-- bootstrap -->

   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>

   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>

   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>

   <!-- Common js-->

  <script src="../assets2/js/app.js" ></script>

   <script src="../assets2/js/pages/validation/form-validation.js" ></script>

   <script src="../assets2/js/layout.js" ></script>

  <script src="../assets2/js/theme-color.js" ></script>

  <script src="../assets2/dropify/dist/js/dropify.min.js"></script>

  <script src="../assets2/js/dropify.js"></script>

  <!-- Material -->

  <script src="../assets2/plugins/material/material.min.js"></script>

  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>

  <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

  <script>

  function addFrench()
  {
    var etape=1;
    var lang="fr";
    $.ajax({
      url: '../ajax/langue.php',
      type: 'POST',
      async:false,
      data: 'etape=' + etape+ '&lang=' +lang,
      dataType: 'text',
      success: function (content, statut) {

  window.location.reload();

      }
    });
  }

  function addEnglish()
  {
    var etape=1;
    var lang="en";
    $.ajax({
      url: '../ajax/langue.php',
      type: 'POST',
      async:false,
      data: 'etape=' + etape+ '&lang=' +lang,
      dataType: 'text',
      success: function (content, statut) {

  window.location.reload();

      }
    });
  }

   function determineAb()

   {

     var choicesubscribe=$("#choicesubscribe").val();

     //alert(choicesubscribe);

     var datachoice=choicesubscribe.split("-");

     var montantchoice=datachoice[1];

     var pays="<?php echo $idpays; ?>";

     var etape=2;



     $.ajax({

       url: '../ajax/determination.php',

       type: 'POST',

       async:true,

        data: 'pays='+pays+'&etape='+etape,

       dataType: 'text',

       success: function (response, statut) {



         $("#montantab").val(montantchoice+" "+response);

         $("#devisecontry").val(response);



       }

     });









     //$("#montantab").val(montantchoice);

   }

   $(document).ready(function() {



     var etape1=6;

     var pays="<?php echo $idpays; ?>";



     $.ajax({

       url: '../ajax/determination.php',

       type: 'POST',

       async:true,

        data: 'pays='+pays+'&etape='+etape1,

       dataType: 'text',

       success: function (response, statut) {



         $("#mobileInfo").html("");

         $("#mobileInfo").html(response);





         var etape2=7;



         $.ajax({

           url: '../ajax/determination.php',

           type: 'POST',

           async:true,

            data: 'pays='+pays+'&etape='+etape2,

           dataType: 'text',

           success: function (response, statut) {



             $("#selectmobileop").html("");

             $("#selectmobileop").html(response);



             var etape3=8;



             $.ajax({

               url: '../ajax/determination.php',

               type: 'POST',

               async:true,

                data: 'pays='+pays+'&etape='+etape3,

               dataType: 'text',

               success: function (response, statut) {



                 $("#choicesubscribe").html("");

                 $("#choicesubscribe").html(response);



               }



             });



           }

         });





       }

     });



     $("#subscribeform").validate({

       errorPlacement: function(label, element) {

       label.addClass('mt-2 text-danger');

       label.insertAfter(element);

     },

     highlight: function(element, errorClass) {

       $(element).parent().addClass('has-danger')

       $(element).addClass('form-control-danger')

     },

     success: function (e) {

           $(e).closest('.control-group').removeClass('error').addClass('info');

           $(e).remove();

       },

       rules:{

         choicesubscribe:"required",

         paymentno:"required",

         transacId:"required",

         selectmobileop:"required",

       },

       messages: {

         choicesubscribe:"Merci de selectionner une offre d'abonnement",

         paymentno:"Merci de renseigner le numéro ayant servit à la transaction",

         transacId:"Merci de renseigner le numéro de transaction",

         selectmobileop:"Merci de selectionner l'opérateur mobile",

       }

     });



   });



   </script>

    <!-- end js include path -->

  </body>



</html>
