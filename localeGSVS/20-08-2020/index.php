<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

// echo $_SESSION['user']['fonctionuser'];
if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

  if($_SESSION['user']['groupe']==1)
  {
    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
    {
      // echo "bonsoir";
      $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);

      //nous allons chercher la liste des etablissements du groupe

      $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
        }

        if(strlen($_SESSION['user']['codeEtab'])>0)
        {

        }else {
          $_SESSION['user']['codeEtab']=$codeEtabAssigner;
        }

        $_SESSION['user']['session']=$libellesessionencours;





         // var_dump($allcodeEtabs);

        $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

        $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


    }else if($_SESSION['user']['fonctionuser']=="Coordonnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
    {
      // echo "bonjour";
      $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);
      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
          $_SESSION['user']['session']=$libellesessionencours;

          if(strlen($_SESSION['user']['codeEtab'])>0)
          {

          }else {
            $_SESSION['user']['codeEtab']=$codeEtabAssigner;
          }

          $notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

          // $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
          $classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);
          $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

          // var_dump($classes);
        }

    }
  }

$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);

  // var_dump($allcodeEtabs);

// echo $libellesessionencours;

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <link href="../assets2/css/style1.css" rel="stylesheet" type="text/css" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::dashb ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::dashb ?></li>
                            </ol>
                        </div>

                    </div>




					<!-- start widget -->
          <?php
          if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
          {
           ?>
          <div class="row ">
            <!-- <div class="col-md-12 ">
          <img src="../assets/img/logo/eduksvg.svg" alt="" style="width:20%" class="pull-right">
            </div> -->

            <div class="col-md-12 ">
              <div class="alert blue-bgcolor alert-dismissible fade show" role="alert">
              <?php echo L::Bienvenues ?> <?php echo $etabs->getEtabLibellebyCodeEtab($_SESSION['user']['codeEtab']); ?>
              <a href="#" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
               </a>
              </div>
            </div>

          </div>
          <?php
        }else {
          ?>
          <div class="row ">
            <div class="col-md-12 ">
              <div class="alert blue-bgcolor alert-dismissible fade show" role="alert">
              <?php echo L::greeting ?> <?php echo $etabs->getEtabNamebyCodeEtab($_SESSION['user']['codeEtab']); ?>
              <a href="#" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
               </a>
              </div>
            </div>

          </div>
          <?php
        }
           ?>



					<div class="state-overview">

						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


          <div class="row">


            <?php

            if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
            {

              ?>
              <div class="col-md-12 col-sm-12">

                <div class="state-overview">

                  <div class="row">

                         <!-- /.col -->
                        <div class="col-lg-4 col-sm-4">
                       <div class="overview-panel orange">
                         <div class="symbol">
                           <i class="material-icons f-left">group</i>
                         </div>
                         <div class="value white">
                           <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php if(strlen($libellesessionencours)>0){echo $user->getNbofStudentforThisAllschoolSession($compteuserid,$libellesessionencours);}else{echo "0";} ?>"><?php echo $user->getNbofStudentforThisAllschoolSession($compteuserid,$libellesessionencours);?></p>
                           <p> <a href="studentalls.php" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::EleveMenu) ?></a> </p>
                         </div>
                       </div>
                     </div>
                         <!-- /.col -->
                         <div class="col-lg-4 col-sm-4">
                       <div class="overview-panel deepPink-bgcolor">
                         <div class="symbol">
                           <i class="material-icons f-left">school</i>
                         </div>
                         <div class="value white">
                           <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php echo $tabstat[2];?>"><?php echo $user->getNumberofTeatcherforThisAllschool($compteuserid);?></p>
                           <p> <a href="teatchers.php" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::TeatcherCapsingle) ?></a> </p>
                         </div>
                       </div>
                     </div>
                         <!-- /.col -->
                         <div class="col-lg-4 col-sm-4">
                       <div class="overview-panel blue-bgcolor">
                         <div class="symbol">
                             <i class="material-icons f-left">person</i>
                         </div>
                         <div class="value white">
                           <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php echo $tabstat[3];?>"><?php echo $user->getNumberofParentforThisAllschool($compteuserid)?></p>
                         <!-- <p><?php //echo strtoupper(L::ParentsMenu) ?></p> -->
                         <p> <a href="parents.php" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::ParentsMenu) ?></a></p>
                         </div>
                       </div>
                     </div>

                     <div class="col-lg-4 col-sm-4">
               <div class="overview-panel purple">
                 <div class="symbol">
                   <i class="fa fa-users usr-clr"></i>
                 </div>
                 <div class="value white">
                   <p class="sbold addr-font-h1" data-counter="counterup" data-value="23"><?php echo $etabs->getAllfamillynumberEtabs($compteuserid) ?></p>
                   <p><a href="families.php" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::FamillesCaps);  ?></a> </p>
                 </div>
               </div>
             </div>

             <div class="col-lg-4 col-sm-4">
       <div class="overview-panel green">
         <div class="symbol">
           <i class="fa fa-users usr-clr"></i>
         </div>
         <div class="value white">
           <p class="sbold addr-font-h1" data-counter="counterup" data-value="23"><?php echo $etabs->getAllClassesnumberEtabs($compteuserid) ?></p>
           <p><a href="users.php" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::UsersMenu);  ?></a> </p>
         </div>
       </div>
     </div>
     <div class="col-lg-4 col-sm-4">
<div class="overview-panel yellow">
 <div class="symbol">
   <i class="fa fa-institution usr-clr"></i>
 </div>
 <div class="value white">
   <p class="sbold addr-font-h1" data-counter="counterup" data-value="23"><?php echo $etabs->getNumberAllclassesEtab($compteuserid,$libellesessionencours); ?></p>
   <p><a href="#" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::ClassesMenu);  ?></a> </p>
 </div>
</div>
</div>

                       </div>

                </div>

              </div>
              <?php

            }else {

              ?>
              <div class="col-md-12 col-sm-12">

                <div class="state-overview">

                  <div class="row">

                         <!-- /.col -->
                        <div class="col-lg-4 col-sm-4">
                       <div class="overview-panel orange">
                         <div class="symbol">
                           <i class="material-icons f-left">group</i>
                         </div>
                         <div class="value white">
                           <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php if(strlen($libellesessionencours)>0){echo $user->getNbofStudentforThisschoolSession($_SESSION['user']['codeEtab'],$libellesessionencours);}else{echo "0";} ?>"><?php echo $user->getNbofStudentforThisschoolSession($_SESSION['user']['codeEtab'],$libellesessionencours);?></p>
                           <p><a href="studentalls.php" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::EleveMenu) ?></a> </p>
                         </div>
                       </div>
                     </div>
                         <!-- /.col -->
                         <div class="col-lg-4 col-sm-4">
                       <div class="overview-panel deepPink-bgcolor">
                         <div class="symbol">
                           <i class="material-icons f-left">school</i>
                         </div>
                         <div class="value white">
                           <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php echo $tabstat[2];?>"><?php echo $user->getNumberofTeatcherforThisschool($_SESSION['user']['codeEtab']);?></p>
                           <p> <a href="teatchers.php" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::TeatcherCapsingle) ?></a> </p>
                         </div>
                       </div>
                     </div>
                         <!-- /.col -->
                         <div class="col-lg-4 col-sm-4">
                       <div class="overview-panel blue-bgcolor">
                         <div class="symbol">
                             <i class="material-icons f-left">person</i>
                         </div>
                         <div class="value white">
                           <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php echo $tabstat[3];?>"><?php echo $user->getNumberofParentforThisschool($_SESSION['user']['codeEtab'])?></p>
                         <!-- <p><?php //echo strtoupper(L::ParentsMenu) ?></p> -->
                         <p> <a href="parents.php" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::ParentsMenu) ?></a></p>
                         </div>
                       </div>
                     </div>

                     <div class="col-lg-4 col-sm-4">
               <div class="overview-panel purple">
                 <div class="symbol">
                   <i class="fa fa-users usr-clr"></i>
                 </div>
                 <div class="value white">
                   <p class="sbold addr-font-h1" data-counter="counterup" data-value="23"><?php echo $etabs->getAllfamillynumber($_SESSION['user']['codeEtab']) ?></p>
                   <p><a href="families.php" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::FamillesCaps);  ?></a> </p>
                 </div>
               </div>
             </div>

                       </div>
                       <div class="row">
                             <div class="col-md-4">
                                 <div class="card card-box">
                                     <div class="card-head">
                                         <header><?php echo L::EleveCaps ?></header>
                                         <!--div class="tools">
                                             <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                           <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                           <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                         </div-->
                                     </div>
                                     <div class="card-body " id="chartjs_doughnut_parent">
                                         <div class="row">
                                             <canvas id="chartjs_doughnut" height="320"></canvas>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                             <div class="col-md-8" style="display:none">
                               <div class="card  card-box">
                                   <div class="card-head">
                                       <header><?php echo L::Gains ?></header>
                                       <div class="tools">
                                           <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                         <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                         <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                       </div>
                                   </div>
                                   <div class="card-body " id="chartjs_line_parent">
                                       <div class="row">
                                           <canvas id="chartjs_line" ></canvas>
                                       </div>
                                   </div>
                               </div>
                           </div>



                           <?php
                           if($_SESSION['user']['fonctionuser']=="Coordonnateur")
                           {
                             ?>
                             <div class="col-lg-8 col-sm-12">
                         <div class="card card-shadow mb-4">
                         <div class="card-header">
                         <div class="card-title">
                           Historiques Messages
                           <ul class="nav nav-pills nav-pill-custom nav-pills-sm float-right " id="pills-tab" role="tablist">
                             <li class="nav-item">
                               <a class="nav-link active" id="pills-today-tab" data-toggle="pill" href="#pills-today" role="tab" aria-controls="pills-today"
                                   aria-selected="true" style="display:none">Aujourd'hui</a>
                             </li>
                             <li class="nav-item">
                               <a class="nav-link" id="pills-week-tab" data-toggle="pill" href="#pills-week" role="tab" aria-controls="pills-week" aria-selected="false" style="display:none">Semaine</a>
                             </li>
                             <li class="nav-item">
                               <a class="nav-link" id="pills-month-tab" data-toggle="pill" href="#pills-month" role="tab" aria-controls="pills-month" aria-selected="false" style="display:none">Mois</a>
                             </li>
                           </ul>

                         </div>
                         </div>
                         <div class="card-body">
                         <div class="tab-content" id="pills-tabContent">
                           <div class="tab-pane fade show active" id="pills-today" role="tabpanel" aria-labelledby="pills-today-tab">
                             <ul class="list-unstyled task-d-list">
                               <?php
                               foreach ($notifications as $value):
                                 ?>
                                 <li>
                                   <div class="row">

                                     <div class="col-10">
                                        <span class="custom-control-description default-color">
                                           <a href="#"><?php

                                           if($value->parascolaire_msg==1)
                                           {
                                             // echo "parascolaire";
                                             echo $etabs->getparacolaireDesignation($value->id_msg);
                                           }else if($value->scola_msg==1)
                                           {
                                             // echo "scolarite";
                                           }else if(($value->parascolaire_msg==0)&&($value->scola_msg==0))
                                           {
                                             // echo "note observation";
                                             if($value->objet_msg==8)
                                             {
                                               echo $value->other_msg;
                                             }else {
                                               echo $value->libelle_msg;
                                             }
                                           }
                                            ?></a>
                                           <span class="badge badge-pill badge-success pull-right"><?php echo date_format(date_create($value->date_msg),"d/m/Y");?></span>
                                         </span>
                                       </label>
                                     </div>
                                     <div class="col">
                                       <div class="btn-group float-right task-list-action">
                                         <a href="detailsmessages.php?msg=<?php echo $value->id_msg ?>" class="btn btn-primary btn-xs" title=""><i class="fa fa-info-circle"></i> </a>
                                         <div class="dropdown " style="display:none">
                                           <a href="#" class="btn btn-primary default-color dropdown-hover p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                             <i class="fa fa-info-circle"></i>
                                           </a>
                                           <div class="dropdown-menu dropdown-menu-right ">
                                             <a class="dropdown-item" href="#">
                                               <i class="icon-user text-info pr-2"></i>reçus</a>
                                             <a class="dropdown-item" href="#">
                                               <i class="icon-close text-danger pr-2"></i>Lus</a>
                                             <!-- <a class="dropdown-item" href="#">
                                               <i class="icon-note text-warning pr-2"></i> Done</a> -->
                                           </div>
                                         </div>
                                       </div>
                                     </div>
                                   </div>
                                 </li>
                                 <?php
                               endforeach;
                                ?>


                               </ul>
                           </div>
                           <div class="tab-pane fade" id="pills-week" role="tabpanel" aria-labelledby="pills-week-tab">
                             <ul class="list-unstyled task-d-list">
                               <li class="list-mark list-primary">
                                 <div class="row">
                                   <div class="col-10">



                                       <span class="custom-control-description default-color">
                                         <a href="#">Ipsum has been industry's standard dummy</a>
                                         <span class="badge badge-pill badge-primary">Sz Tasi</span>
                                       </span>
                                     </label>
                                   </div>
                                   <div class="col">
                                     <div class="btn-group float-right task-list-action">
                                       <div class="dropdown ">
                                         <a href="#" class="btn btn-transparent default-color dropdown-hover p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                           <i class=" icon-options"></i>
                                         </a>
                                         <div class="dropdown-menu dropdown-menu-right ">
                                           <a class="dropdown-item" href="#">
                                             <i class="icon-user text-info pr-2"></i> Assign</a>
                                           <a class="dropdown-item" href="#">
                                             <i class="icon-close text-danger pr-2"></i> Remove</a>
                                           <a class="dropdown-item" href="#">
                                             <i class="icon-note text-warning pr-2"></i> Done</a>
                                         </div>
                                       </div>
                                     </div>
                                   </div>
                                 </div>
                               </li>
                               <li class="list-mark list-info">
                                 <div class="row">
                                   <div class="col-10">



                                       <span class="custom-control-description default-color">
                                         <a href="#">Contrary Ipsum is not simply random text</a>
                                         <span class="badge badge-pill badge-secondary">MH Geek</span>
                                       </span>
                                     </label>
                                   </div>
                                   <div class="col">
                                     <div class="btn-group float-right task-list-action">
                                       <div class="dropdown ">
                                         <a href="#" class="btn btn-transparent default-color dropdown-hover p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                           <i class=" icon-options"></i>
                                         </a>
                                         <div class="dropdown-menu dropdown-menu-right ">
                                           <a class="dropdown-item" href="#">
                                             <i class="icon-user text-info pr-2"></i> Assign</a>
                                           <a class="dropdown-item" href="#">
                                             <i class="icon-close text-danger pr-2"></i> Remove</a>
                                           <a class="dropdown-item" href="#">
                                             <i class="icon-note text-warning pr-2"></i> Done</a>
                                         </div>
                                       </div>
                                     </div>
                                   </div>
                                 </div>
                               </li>

                             </ul>
                           </div>
                           <div class="tab-pane fade" id="pills-month" role="tabpanel" aria-labelledby="pills-month-tab">
                             <ul class="list-unstyled task-d-list">
                               <li>
                                 <div class="row">
                                   <div class="col-10">



                                       <span class="custom-control-description default-color">
                                         <a href="#">Ipsum has been industry's standard dummy</a>
                                         <span class="badge badge-pill badge-primary">Sz Tasi</span>
                                       </span>
                                     </label>
                                   </div>
                                   <div class="col">
                                     <div class="btn-group float-right task-list-action">
                                       <div class="dropdown ">
                                         <a href="#" class="btn btn-transparent default-color dropdown-hover p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                           <i class=" icon-options"></i>
                                         </a>
                                         <div class="dropdown-menu dropdown-menu-right ">
                                           <a class="dropdown-item" href="#">
                                             <i class="icon-user text-info pr-2"></i> Assign</a>
                                           <a class="dropdown-item" href="#">
                                             <i class="icon-close text-danger pr-2"></i> Remove</a>
                                           <a class="dropdown-item" href="#">
                                             <i class="icon-note text-warning pr-2"></i> Done</a>
                                         </div>
                                       </div>
                                     </div>
                                   </div>
                                 </div>
                               </li>
                               <li>
                                 <div class="row">
                                   <div class="col-10">



                                       <span class="custom-control-description default-color">
                                         <a href="#"> All the Lorem Ipsum generators</a>
                                         <span class="badge badge-pill badge-danger">Jane Doe</span>
                                       </span>
                                     </label>
                                   </div>
                                   <div class="col">
                                     <div class="btn-group float-right task-list-action">
                                       <div class="dropdown ">
                                         <a href="#" class="btn btn-transparent default-color dropdown-hover p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                           <i class=" icon-options"></i>
                                         </a>
                                         <div class="dropdown-menu dropdown-menu-right ">
                                           <a class="dropdown-item" href="#">
                                             <i class="icon-user text-info pr-2"></i> Assign</a>
                                           <a class="dropdown-item" href="#">
                                             <i class="icon-close text-danger pr-2"></i> Remove</a>
                                           <a class="dropdown-item" href="#">
                                             <i class="icon-note text-warning pr-2"></i> Done</a>
                                         </div>
                                       </div>
                                     </div>
                                   </div>
                                 </div>
                               </li>

                             </ul>
                           </div>
                         </div>
                         </div>
                         </div>
                         </div>
                             <?php
                           }else if($_SESSION['user']['fonctionuser']=="Comptable")
                           {
                             //nous allons compter le nombre de versement

                             // echo $_SESSION['user']['codeEtab']."/".$libellesessionencours;

                             $transactionNb=$etabs->getNumberOfTransactions($_SESSION['user']['codeEtab'],$libellesessionencours);

                             // echo $transactionNb;

                             if($transactionNb>0)
                             {
                               $lasttransactions=$etabs->getAllLastTransactions($_SESSION['user']['codeEtab'],$libellesessionencours);
                               ?>
                               <div class="col-md-8 col-sm-12 col-12">
                          	<div class="card  card-box">
                                  <div class="card-head">
                                      <header><?php echo L::transactionstories ?></header>
                                      <div class="tools">
                                          <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
  	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
  	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                      </div>
                                  </div>
                                  <div class="card-body no-padding height-9">
                                      <div class="row">
                                          <div class="noti-information notification-menu">
                                              <div class="notification-list mail-list not-list small-slimscroll-style">
                                                <?php

                                                foreach ($lasttransactions as $value):
                                                  ?>
                                                  <a href="javascript:;" class="single-mail"> <span class="icon bg-b-green box-shadow-1"> <i class="fa fa-money"></i>
  												</span> <span class="text-purple"><?php echo $value->code_versement ?></span> <span class="label label-info" style="margin-left:20px;"> <?php echo $value->motif_versement ?></span>  <span style="margin-left:20px;" ><?php echo $student->getNameInfos($value->ideleve_versement) ?></span> <span class="label label-warning pull-right"> <?php echo $value->montant_versement ?> </span>
                                                      <span class="notificationtime">
                                                          <small><?php echo date_format(date_create($value->date_versement),"d/m/Y") ?></small>
                                                      </span>
                                                  </a>
                                                  <?php
                                                endforeach;

                                                 ?>


                                              </div>
                                              <?php
                                              // if($transactionNb>10)
                                              // {
                                                ?>
                                                <div class="full-width text-center p-t-10" >
                                                  <a href="transactions.php" class="btn green btn-outline btn-circle margin-0"><?php echo L::ViewAll ?></a>
                                                  <!-- <button type="button" class="btn purple btn-outline btn-circle margin-0"><?php echo L::ViewAll ?></button> -->
                                                </div>
                                                <?php
                                              // }
                                               ?>

                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                               <?php
                             }

                             ?>

                             <?php
                           }
                            ?>


                         </div>
                </div>

              </div>
              <?php

            }


             ?>




                    </div>




                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <!-- start chat sidebar -->

                        <div class="chat-sidebar-container" data-close-on-body-click="false">
                        <div class="chat-sidebar">
                          <ul class="nav nav-tabs">
                            <li class="nav-item">
                              <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                                  class="material-icons">chat</i>Chat
                                <span class="badge badge-danger">4</span>
                              </a>
                            </li>
                          </ul>
                          <div class="tab-content">
                            <!-- Start User Chat -->
                            <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                              id="quick_sidebar_tab_1"> -->
                              <div class="chat-sidebar-chat "
                                >
                              <div class="chat-sidebar-list">
                                <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                                  data-wrapper-class="chat-sidebar-list">
                                  <div class="chat-header">
                                    <h5 class="list-heading">Online</h5>
                                  </div>
                                  <ul class="media-list list-items">
                                    <?php

                                    foreach ($onlineUsers as  $valueUsers):
                                      $tofuser=$valueUsers->photo_compte;
                                      if(strlen($tofuser)>0)
                                      {
                                        $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                                      }else {
                                        $lientofuser="../photo/user5.jpg";
                                      }
                                      ?>
                                      <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                          width="35" height="35" alt="...">
                                        <i class="online dot red"></i>
                                        <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                          <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                          <!-- <div class="media-heading-sub"><?php //echo $valueUsers->fonction_compte ?></div> -->
                                        </div>
                                      </li>
                                      <?php
                                    endforeach;
                                     ?>

                                  </ul>
                                  <div class="chat-header">
                                    <h5 class="list-heading">Offline</h5>
                                  </div>
                                  <ul class="media-list list-items">
                                    <?php

                                    foreach ($offlineUsers as  $valueUsers):
                                      $tofuser=$valueUsers->photo_compte;
                                      if(strlen($tofuser)>0)
                                      {
                                        $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                                      }else {
                                        $lientofuser="../photo/user5.jpg";
                                      }
                                      ?>
                                      <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                          width="35" height="35" alt="...">
                                        <i class="offline dot"></i>
                                        <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                          <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                          <!-- <div class="media-heading-sub"><?php //echo $valueUsers->fonction_compte ?></div> -->
                                        </div>
                                      </li>
                                      <?php
                                    endforeach;
                                     ?>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <!-- End User Chat -->
                          </div>
                        </div>
                      </div>
                        <!-- end chat sidebar -->
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
  <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- morris chart -->

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>

     <script src="../assets2/plugins/chart-js/Chart.bundle.js" ></script>
     <script src="../assets2/plugins/chart-js/utils.js" ></script>
     <!-- <script src="../assets2/js/pages/chart/chartjs/chartjs-data.js" ></script> -->
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function addmessages(senderid,receiverid)
   {

     var etape=1;
     $.ajax({
       url: '../ajax/chat.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&senderid=' +senderid+'&receiverid='+receiverid,
       dataType: 'text',
       success: function (content, statut) {

       document.location.href="chats.php";

       }
     });
   }

   function SetcodeEtab(codeEtab)
   {
     var etape=3;
     $.ajax({
       url: '../ajax/sessions.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function loader()
   {
     var etape=1;
     var codeEtab="<?php echo $_SESSION['user']['codeEtab'];?>";
     var session="<?php echo $libellesessionencours ; ?>";

     $.ajax({
       url: '../ajax/load.php',
       type: 'POST',
       async:false,
       data: 'codeEtab=' +codeEtab+ '&etape='+etape+'&session='+session,
       dataType: 'text',
       success: function (content, statut) {

         var data=content;

         // callback(data);

       }
     });

   }

   $(document).ready(function() {

     // alert(session);

     // var MONTHS = ['<?php echo L::JanvLib?>','<?php echo L::FevLib ?>','<?php echo L::MarsLib ?>','<?php echo L::AvriLib ?>','<?php echo L::MaiLib ?>','<?php echo L::JuneLib ?>','<?php echo L::JulLib ?>','<?php echo L::AoutLib ?>','<?php echo L::SeptLib?>','<?php echo L::OctobLib?>','<?php echo L::NovbLib?>','<?php echo L::DecemLib?>'];
     //    var config = {
     //        type: 'line',
     //        data: {
     //            labels: ["January", "February", "March", "April", "May", "June", "July"],
     //            datasets: [{
     //                label: "Paiements Attendus",
     //                backgroundColor: window.chartColors.red,
     //                borderColor: window.chartColors.red,
     //                data: [
     //                    randomScalingFactor(),
     //                    randomScalingFactor(),
     //                    randomScalingFactor(),
     //                    randomScalingFactor(),
     //                    randomScalingFactor(),
     //                    randomScalingFactor(),
     //                    randomScalingFactor()
     //                ],
     //                fill: false,
     //            }, {
     //                label: "Paiements reçus",
     //                fill: false,
     //                backgroundColor: window.chartColors.blue,
     //                borderColor: window.chartColors.blue,
     //                data: [
     //                    randomScalingFactor(),
     //                    randomScalingFactor(),
     //                    randomScalingFactor(),
     //                    randomScalingFactor(),
     //                    randomScalingFactor(),
     //                    randomScalingFactor(),
     //                    randomScalingFactor()
     //                ],
     //            }]
     //        },
     //        options: {
     //            responsive: true,
     //            title:{
     //                display:true,
     //                text:''
     //            },
     //            tooltips: {
     //                mode: 'index',
     //                intersect: false,
     //            },
     //            hover: {
     //                mode: 'nearest',
     //                intersect: true
     //            },
     //            scales: {
     //                xAxes: [{
     //                    display: true,
     //                    scaleLabel: {
     //                        display: true,
     //                        labelString: 'Mois'
     //                    }
     //                }],
     //                yAxes: [{
     //                    display: true,
     //                    scaleLabel: {
     //                        display: true,
     //                        labelString: ''
     //                    }
     //                }]
     //            }
     //        }
     //    };
     //    var ctx = document.getElementById("chartjs_line").getContext("2d");
     //    window.myLine = new Chart(ctx, config);
     //
     // // var randomScalingFactor = function() {
     // //       return Math.round(Math.random() * 100);
     // //   };
     //
     //   var config = {
     //       type: 'doughnut',
     //       data: {
     //           datasets: [{
     //               data: [
     //                   <?php echo $etabs->getnumberOfwomenchild($_SESSION['user']['codeEtab'],$libellesessionencours); ?>,
     //                   // randomScalingFactor(),
     //                   // randomScalingFactor(),
     //                   // randomScalingFactor(),
     //                   <?php echo $etabs->getnumberOfmenchild($_SESSION['user']['codeEtab'],$libellesessionencours); ?>,
     //               ],
     //               backgroundColor: [
     //                   window.chartColors.red,
     //                   // window.chartColors.orange,
     //                   // window.chartColors.yellow,
     //                   // window.chartColors.green,
     //                   window.chartColors.blue,
     //               ],
     //               label: 'Dataset 1'
     //           }],
     //           labels: [
     //               "<?php echo L::Filles ?>",
     //               // "Orange",
     //               // "Yellow",
     //               // "Green",
     //               "<?php echo L::Garcons ?>"
     //           ]
     //       },
     //       options: {
     //           responsive: true,
     //           legend: {
     //               position: 'top',
     //           },
     //           title: {
     //               display: true,
     //               text: ''
     //           },
     //           animation: {
     //               animateScale: true,
     //               animateRotate: true
     //           }
     //       }
     //   };
     //
     //       var ctx = document.getElementById("chartjs_doughnut").getContext("2d");
     //       window.myDoughnut = new Chart(ctx, config);


   });


   </script>
    <!-- end js include path -->
  </body>

</html>
