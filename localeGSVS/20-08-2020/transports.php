<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Student.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$student=new Student();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

  if($_SESSION['user']['groupe']==1)
  {
    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
    {
      $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);

      //nous allons chercher la liste des etablissements du groupe

      $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
        }

        if(strlen($_SESSION['user']['codeEtab'])>0)
        {

        }else {
          $_SESSION['user']['codeEtab']=$codeEtabAssigner;
        }

        $_SESSION['user']['session']=$libellesessionencours;





         // var_dump($allcodeEtabs);

        $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

        $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


    }else if($_SESSION['user']['fonctionuser']=="Coordonnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
    {
      $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);
      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
          $_SESSION['user']['session']=$libellesessionencours;

          if(strlen($_SESSION['user']['codeEtab'])>0)
          {

          }else {
            $_SESSION['user']['codeEtab']=$codeEtabAssigner;
          }

          $notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

          // $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
          $classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);
          $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);
        }

    }
  }

  // var_dump($allcodeEtabs);

// echo $libellesessionencours;

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
        <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
  <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title"><?php echo L::TransportationsManagements ?></div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">

                          <li><a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active"><?php echo L::TP ?></li>

                      </ol>
                  </div>
              </div>

              <?php

                    if(isset($_SESSION['user']['addctrleok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
              <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
              <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
                type: 'success',
                title: '<?php echo L::Felicitations ?>',
                text: 'Contrôle ajouté avec succès',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addctrleok']);
                    }

                     ?>




              <?php

                    if(isset($_SESSION['user']['deletesubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['deletesubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: '<?php echo L::Okay ?>',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })


                </script>
                      <?php
                      unset($_SESSION['user']['deletesubjectok']);
                    }

                     ?>


              <?php

                    if(isset($_SESSION['user']['updatesubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['updatesubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: '<?php echo L::Okay ?>',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })


                </script>
                      <?php
                      unset($_SESSION['user']['updatesubjectok']);
                    }

                     ?>
              <?php

                    if(isset($_SESSION['user']['addsubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>


                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['addsubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: '<?php echo L::Okay ?>',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })
                </script>
                      <?php
                      unset($_SESSION['user']['addsubjectok']);
                    }

                     ?>


                     <div class="col-md-12 col-sm-12">
                                   <div class="panel tab-border card-box">
                                       <header class="panel-heading panel-heading-gray custom-tab ">
                                           <ul class="nav nav-tabs">
                                               <!-- <li class="nav-item "><a href="#home" data-toggle="tab" class="<?php //if(!isset($_POST['search'])){echo "active";} ?>" ><i class="fa fa-bars"></i> Liste des paiements cantines <?php //if(!isset($_POST['search'])){echo "active";} ?></a>
                                               </li> -->
                                               <li class="nav-item"><a href="#about" data-toggle="tab" class="active"><i class="fa fa-bars"></i> <?php echo L::ClasseByLists ?> </a>
                                               </li>



                                           </ul>
                                       </header>
                                       <div class="panel-body">
                                           <div class="tab-content">
                                               <div class="tab-pane <?php //if(!isset($_POST['search'])){echo "active";} ?>" id="home">
                                                 <div class="row">
                        <div class="col-md-12">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header></header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">
                                  <div class="pull-right">

                                        <a class="btn btn-warning" onclick="" href="#"><i class="fa fa-print"></i> <?php echo L::ListsofPayements ?></a>

                                  </div><br>

                                    <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                        <thead>
                                            <tr>

                                                <th style="width:200px;"> <?php echo L::CodeVersements ?></th>
                                                <th style="text-align:center;"> <?php echo L::MonthPaiements ?></th>
                                                <th style="text-align:center;"> <?php echo L::ClassesMenu  ?></th>
                                                <th style="text-align:center;"> <?php echo L::NamestudentTab ?> </th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                            //$matieres

                                            $i=1;
                                              foreach ($paiements as $value):
                                           ?>
                                            <tr class="odd gradeX">


                                                <td>
                                                    <?php echo $value->code_versement;?>
                                                </td>
                                                <td style="text-align:center">
                                                  <?php echo $value->mois_versement;?>
                                                </td>

                                                <td style="text-align:center">
                                                  <?php echo $classe->getInfosofclassesbyId($value->classe_versement,$value->session_versement);?>
                                                </td>
                                                <td style="text-align:center">
                                                  <?php echo $value->nom_compte." ".$value->prenom_compte;?>

                                                </td>



                                            </tr>


                                            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                            <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
                                            <!-- <script src="../assets2/plugins/select2/js/select2.js" ></script>
                                            <script src="../assets2/js/pages/select2/select2-init.js" ></script> -->
                                            <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
                                            <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                            <script>



                                            function myFunction(idcompte)
                                            {
                                              //var url="detailslocal.php?compte="+idcompte;
                                            document.location.href="detailsadmin.php?compte="+idcompte;
                                            }

                                            function erasedMat(id)
                                            {
                                              document.getElementById("messageMat"+id).innerHTML = "";

                                            }

                                            function erasedCoef(id)
                                            {
                                              document.getElementById("messageCoef"+id).innerHTML = "";
                                            }

                                            function erasedClasse(id)
                                            {
                                              document.getElementById("messageClasse"+id).innerHTML = "";
                                            }

                                            function erasedTeatcher(id)
                                            {
                                              document.getElementById("messageTeatcher"+id).innerHTML = "";
                                            }

                                            function modify(id)
                                            {


                                              Swal.fire({
                                title: '<?php echo L::WarningLib ?>',
                                text: "<?php echo L::DoyouReallyModifyingSubjects ?>",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: '<?php echo L::ModifierBtn ?>',
                                cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                              }).then((result) => {
                                if (result.value) {
                                  document.location.href="updatesubject.php?compte="+id;
                                }else {

                                }
                              })
                                            }



                                            function check(id)
                                            {
                                              //recuperation des variables
                                              var matiere=$("#matiere"+id).val();
                                              var professeur=$("#teatcher"+id).val();
                                              var controle=$("#controle"+id).val();
                                              var coefficient=$("#coef"+id).val();
                                              var classe=$("#classe"+id).val();
                                              var codeEtab=$("#codeEtab").val();
                                              var idctrl=$("#idctrl").val();
                                              var datectrl=$("#datectrl"+id).val();

                                              //alert("bonjour");

                                              event.preventDefault();


                                              if(matiere==""||coefficient==""||classe==""||professeur==""||datectrl==""||controle=="")
                                              {
                                                if(controle=="")
                                                {
                                                   document.getElementById("messageCont"+id).innerHTML = "<font color=\"red\"><?php echo L::Controlsrequired ?></font>";
                                                }

                                                if(datectrl=="")
                                                {
                                                   document.getElementById("messageDate"+id).innerHTML = "<font color=\"red\"><?php echo L::Daterenseigner ?></font>";
                                                }

                                                if(matiere=="")
                                                {
                                                   document.getElementById("messageMat"+id).innerHTML = "<font color=\"red\"><?php echo L::PleaseselectSubjects ?></font>";
                                                }

                                                if(coefficient=="")
                                                {
                                                  document.getElementById("messageCoef"+id).innerHTML = "<font color=\"red\"><?php echo L::SubjectcoefSelectedrequired ?></font>";
                                                }

                                                if(classe=="")
                                                {
                                                  document.getElementById("messageClas"+id).innerHTML = "<font color=\"red\"><?php echo L::PleaseSelectclasserequired ?></font>";
                                                }

                                                if(professeur=="")
                                                {
                                                  document.getElementById("messageProf"+id).innerHTML = "<font color=\"red\"><?php echo L::PleaseselectTeat ?> !</font>";
                                                }




                                            }else {

                                                  if(coefficient==0)
                                                  {
                                                      document.getElementById("messageCoef"+id).innerHTML = "<font color=\"red\"><?php echo L::Coefminrequired ?>!</font>";
                                                  }else if(coefficient>=0 && professeur!="" && classe!="" && matiere!="") {
                                                    soumettre();
                                                  }
                                            }

                                            }

                                            function searchmatiereT(id)
                                            {
                                                var codeEtab="<?php echo $_SESSION['user']['codeEtab'];?>";
                                                var classe=$("#classe"+id).val();
                                                var etape=2;
                                                 $.ajax({

                                                   url: '../ajax/matiere.php',
                                                   type: 'POST',
                                                   async:true,
                                                   data: 'code=' + codeEtab+ '&etape=' + etape+'&classe='+classe,
                                                   dataType: 'text',
                                                   success: function (content, statut) {

                                                     $("#matiere"+id).html("");
                                                     $("#matiere"+id).html(content);

                                                   }
                                                 });
                                            }

                                            function searchprofesseurT(id)
                                            {
                                              var codeEtab="<?php echo $_SESSION['user']['codeEtab'];?>";
                                              var classe=$("#classe"+id).val();
                                              var matiere=$("#matiere"+id).val();
                                              var etape=4;
                                              $.ajax({

                                                url: '../ajax/teatcher.php',
                                                type: 'POST',
                                                async:true,
                                                data: 'code=' + codeEtab+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
                                                dataType: 'text',
                                                success: function (content, statut) {

                                                  // alert(content);
                                                  $("#teatcher"+id).html("");
                                                  $("#teatcher"+id).html(content);

                                                }
                                              });
                                            }



















                                            </script>

                                            <?php
                                            $i++;
                                            endforeach;
                                            ?>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                                               </div>


    <div class="tab-pane active" id="about">

       <div class="row">
           <div class="col-md-12 col-sm-12">
               <div class="card card-box">
                   <div class="card-head">
                       <header></header>

                   </div>

                   <div class="card-body">
                     <form method="post" id="FormSearch">
                         <div class="row">
                           <div class="col-md-6 col-sm-6">
                           <!-- text input -->
                             <input type="hidden" id="codeEtab" name="codeEtab" class="form-control" value="<?php echo $_SESSION['user']['codeEtab']; ?>">
                              <input type="hidden" id="sessionEtab" name="sessionEtab" class="form-control" value="<?php echo $libellesessionencours; ?>">
                           <div class="form-group">
                               <label><?php echo L::ClasseMenu ?></label>
                               <select class="form-control input-height" id="classeEtab" name="classeEtab" style="width:100%" onchange="transportmois()">
                                   <option value=""><?php echo L::Selectclasses ?></option>
                                   <?php
                                   $i=1;
                                     foreach ($classes as $value):
                                     ?>
                                     <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                     <?php
                                                                      $i++;
                                                                      endforeach;
                                                                      ?>

                               </select>
                               <input type="hidden" name="search" id="search" />
                           </div>

                       </div>
                       <div class="col-md-6 col-sm-6">

                         <div class="form-group">
                             <label><?php echo mb_strtolower(L::MonthLib) ?></label>
                             <select class="form-control input-height" id="mois" name="mois" style="width:100%">
                                 <option value=""><?php echo L::SelectAnMonth ?></option>
                                 <option value="JANVIER"><?php echo mb_strtoupper(L::JanvLib);  ?></option>
                                 <option value="FEVRIER"><?php echo mb_strtoupper(L::FevLib);  ?></option>
                                 <option value="MARS"><?php echo mb_strtoupper(L::MarsLib);  ?></option>
                                 <option value="AVRIL"><?php echo mb_strtoupper(L::AvriLib);  ?></option>
                                 <option value="MAI"><?php echo mb_strtoupper(L::MaiLib);  ?></option>
                                 <option value="JUIN"><?php echo mb_strtoupper(L::JuneLib);  ?></option>
                                 <option value="JUILLET"><?php echo mb_strtoupper(L::JulLib);  ?></option>
                                 <option value="AOUT"><?php echo mb_strtoupper(L::AoutLib);  ?></option>
                                 <option value="SEPTEMBRE"><?php echo mb_strtoupper(L::SeptLib);  ?></option>
                                 <option value="OCTOBRE"><?php echo mb_strtoupper(L::OctobLib);  ?></option>
                                 <option value="NOVEMBRE"><?php echo mb_strtoupper(L::NovbLib);  ?></option>
                                 <option value="DECEMBRE"><?php echo mb_strtoupper(L::DecemLib);  ?></option>

                             </select>

                         </div>
                       </div>

                         </div>

                         <button type="submit" class="btn btn-danger"><?php echo L::Seacher ?></button>
                     </form>




                   </div>
               </div>
           </div>

       </div>


         <?php
         if(isset($_POST['codeEtab'])&&isset($_POST['sessionEtab'])&&isset($_POST['classeEtab'])&&isset($_POST['mois'])&&isset($_POST['search']))
         {
           $codeEtab=$_POST['codeEtab'];
           $sessionEtab=$_POST['sessionEtab'];
           $classeEtab=$_POST['classeEtab'];
           $mois=$_POST['mois'];

           $listeclassecantine=$student->getAllCantinesStudentclasseByMonth($_SESSION['user']['codeEtab'],$sessionEtab,$classeEtab,$mois);

           // var_dump($listeclassecantine);

           ?>
           <div class="row">
<div class="col-md-12">
<div class="card  card-box">
<div class="card-head">
<header></header>
<div class="tools">
  <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
<a class="t-close btn-color fa fa-times" href="javascript:;"></a>
</div>
</div>
<div class="card-body ">
  <div class="pull-right">

        <a class="btn btn-warning" target="_blank" href="listetransportclasse.php?classe=<?php echo $classeEtab; ?>&codeEtab=<?php echo $_SESSION['user']['codeEtab']; ?>&session=<?php echo $sessionEtab; ?>&mois=<?php echo $mois ?>" href="#"><i class="fa fa-print"></i> <?php echo L::ClassesTransportList ?></a>

  </div><br>

    <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example5">
        <thead>
            <tr>

                <th style="width:200px;"><?php echo L::MatriculestudentTab ?></th>
                <th style="text-align:center;"><?php echo L::Name ?></th>
                <th style="text-align:center;"> <?php echo L::PreName ?></th>


            </tr>
        </thead>
        <tbody>
          <?php
          foreach ($listeclassecantine as  $value):
           ?>
         <tr>
           <td><span class="label label-sm label-info" style=""><?php echo $value->matricule_eleve; ?></span></td>
            <td style="text-align:center"><?php echo $value->nom_compte; ?></td>
             <td style="text-align:center"><?php echo $value->prenom_compte; ?></td>
         </tr>
         <?php
       endforeach;
          ?>

        </tbody>
    </table>
</div>
</div>
</div>
</div>

           <?php
         }
          ?>







     </div>






                                           </div>
                                       </div>
                                   </div>
                               </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>

   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

 <script>
 function SetcodeEtab(codeEtab)
 {
   var etape=3;
   $.ajax({
     url: '../ajax/sessions.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }
 function addFrench()
 {
   var etape=1;
   var lang="fr";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function addEnglish()
 {
   var etape=1;
   var lang="en";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function ImprimeListecantine(codeEtab,sessionEtab,classeEtab,mois)
 {
   alert(codeEtab+' '+sessionEtab+' '+classeEtab+' '+mois);
 }

 function cantinemois()
 {
   var codeEtab="<?php echo $_SESSION['user']['codeEtab'];?>";
   var session="<?php echo $libellesessionencours;?>";
   var classeid=$("#classeEtab").val();
   var etape=25;
  $.ajax({
    url: '../ajax/admission.php',
    type: 'POST',
    async:true,
    data: 'codeEtab=' +codeEtab+ '&etape=' +etape+'&classeid='+classeid+'&session='+session,
    dataType: 'text',
    success: function (content, statut) {
       var cut = $.trim(content);
      $("#mois").html("");
      $("#mois").html(cut);

    }
  });

 }

 function transportmois()
 {
   var codeEtab="<?php echo $_SESSION['user']['codeEtab'];?>";
   var session="<?php echo $libellesessionencours;?>";
   var classeid=$("#classeEtab").val();
   var etape=28;
  $.ajax({
    url: '../ajax/admission.php',
    type: 'POST',
    async:true,
    data: 'codeEtab=' +codeEtab+ '&etape=' +etape+'&classeid='+classeid+'&session='+session,
    dataType: 'text',
    success: function (content, statut) {
       var cut = $.trim(content);
      $("#mois").html("");
      $("#mois").html(cut);

    }
  });

 }

 function searchmatiere()
 {
     var codeEtab="<?php echo $_SESSION['user']['codeEtab'];?>";
     var classe=$("#classe").val();
     var etape=2;
      $.ajax({

        url: '../ajax/matiere.php',
        type: 'POST',
        async:true,
        data: 'code=' + codeEtab+ '&etape=' + etape+'&classe='+classe,
        dataType: 'text',
        success: function (content, statut) {

          $("#matiere").html("");
          $("#matiere").html(content);

        }
      });
 }

 function searchprofesseur()
 {
   var codeEtab="<?php echo $_SESSION['user']['codeEtab'];?>";
   var classe=$("#classe").val();
   var matiere=$("#matiere").val();
   var etape=4;
   $.ajax({

     url: '../ajax/teatcher.php',
     type: 'POST',
     async:true,
     data: 'code=' + codeEtab+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
     dataType: 'text',
     success: function (content, statut) {

       // alert(content);
       $("#teatcher").html("");
       $("#teatcher").html(content);

     }
   });
 }

 jQuery(document).ready(function() {




$("#classe").select2();
$("#teatcher").select2();
$("#classeEtab").select2();
$("#matiere").select2();
$("#typesess").select2();
$("#mois").select2();



   $("#FormAddCtrl").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

        // matiere:"required",
        // classe:"required",
        // teatcher:"required",
        // coef:"required",
        classe:"required",
        matiere:"required",
        controle:"required",
        coef:"required",
        teatcher:"required",
        datectrl:"required",
        typesess:"required"


      },
      messages: {
        // matiere:"Merci de renseigner la matière",
        // classe:"<?php echo L::PleaseSelectclasserequired ?>",
        // teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
        // coef:"Merci de renseigner le coefficient de la matière"
        classe:"<?php echo L::PleaseSelectclasserequired ?>",
        matiere:"<?php echo L::PleaseselectSubjects ?>",
        controle:"<?php echo L::Controlsrequired ?>",
        coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
        teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
          datectrl:"<?php echo L::PleaseEnterDateControls ?>",
          typesess:"<?php echo L::PeriodRequired ?>"


      },
      submitHandler: function(form) {


// nous allons verifier un controle similaire n'existe pas
        var etape=1;

         $.ajax({
           url: '../ajax/controle.php',
           type: 'POST',
           async:true,
           data: 'matiere=' + $("#matiere").val()+ '&etape=' + etape+'&classe='+$("#classe").val()+'&teatcher='+$("#teatcher").val()+'&codeEtab='+$("#codeEtab").val()+'&coefficient='+$("#coef").val()+'&datectrl='+$("#datectrl").val(),
           dataType: 'text',
           success: function (content, statut) {


             if(content==0)
             {
               //cette matière n'existe pas encore pour cette classe

               form.submit();

             }else if(content==1)
             {
               //il est question d'un nouveau professeur pour cette matière
               Swal.fire({
               type: 'warning',
               title: '<?php echo L::WarningLib ?>',
               text: '<?php echo L::ControlAllreadyExist ?>',

               })

             }

           }
         });

             }


           });

           $("#FormAddCtrl1").validate({

             errorPlacement: function(label, element) {
             label.addClass('mt-2 text-danger');
             label.insertAfter(element);
           },
           highlight: function(element, errorClass) {
             $(element).parent().addClass('has-danger')
             $(element).addClass('form-control-danger')
           },
           success: function (e) {
                 $(e).closest('.control-group').removeClass('error').addClass('info');
                 $(e).remove();
             },
              rules:{

                // matiere:"required",
                // classe:"required",
                // teatcher:"required",
                // coef:"required",
                classe:"required",
                matiere:"required",
                controle:"required",
                coef:"required",
                teatcher:"required",
                datectrl:"required",
                typesess:"required"


              },
              messages: {
                // matiere:"Merci de renseigner la matière",
                // classe:"<?php echo L::PleaseSelectclasserequired ?>",
                // teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
                // coef:"Merci de renseigner le coefficient de la matière"
                classe:"<?php echo L::PleaseSelectclasserequired ?>",
                matiere:"<?php echo L::PleaseselectSubjects ?>",
                controle:"<?php echo L::Controlsrequired ?>",
                coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
                teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
                  datectrl:"<?php echo L::PleaseEnterDateControls ?>",
                  typesess:"<?php echo L::PeriodRequired ?>"


              },
              submitHandler: function(form) {


        // nous allons verifier un controle similaire n'existe pas
                var etape=1;

                 $.ajax({
                   url: '../ajax/controle.php',
                   type: 'POST',
                   async:true,
                   data: 'matiere=' + $("#matiere").val()+ '&etape=' + etape+'&classe='+$("#classe").val()+'&teatcher='+$("#teatcher").val()+'&codeEtab='+$("#codeEtab").val()+'&coefficient='+$("#coef").val()+'&datectrl='+$("#datectrl").val(),
                   dataType: 'text',
                   success: function (content, statut) {


                     if(content==0)
                     {
                       //cette matière n'existe pas encore pour cette classe

                       form.submit();

                     }else if(content==1)
                     {
                       //il est question d'un nouveau professeur pour cette matière
                       Swal.fire({
                       type: 'warning',
                       title: '<?php echo L::WarningLib ?>',
                       text: '<?php echo L::ControlAllreadyExist ?>',

                       })

                     }

                   }
                 });

                     }


                   });
      });






 </script>
    <!-- end js include path -->
  </body>

</html>
