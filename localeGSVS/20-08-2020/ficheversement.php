<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Teatcher.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../controller/functions.php');
require_once('../class/ChiffresEnLettres.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$codeEtab=$_GET['codeEtab'] ;
$idClasse=$_GET['classeEtab'];
$session=$_GET['sessionEtab'];

$etabs=new Etab();
$student=new Student();
$classe=new Classe();
$lettre=new ChiffreEnLettre();

$libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
$libelleclasse=$classe->getInfosofclassesbyId($idClasse,$session);
$logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
$etablissementType=$etabs->DetermineTypeEtab($codeEtab);


$students=$student->getAllStudentOfClassesId($idClasse,$session);

//nous allons faire nos différents calculs

//le nombre d'eleve de cette classe


$nbclassestudents=$classe->getAllStudentOfThisClassesNb($idClasse,$session);

//le nombre de fille affecté

$nbaffectefilles=$classe->getAllStudentFilleAffectOfThisClassesNb($idClasse,$session);

//nombre de fille non Affecté

$nbnonaffectefilles=$classe->getAllStudentFilleNAffectOfThisClassesNb($idClasse,$session);

//nombre de mec affectés

$nbaffectemecs=$classe->getAllStudentMecAffectOfThisClassesNb($idClasse,$session);

//nombre de mec non affectés

$nbnonaffectemecs=$classe->getAllStudentMecNAffectOfThisClassesNb($idClasse,$session);

//nombre de fille redoublante

$nbredoubfilles=$classe->getAllStudentFilleRedtOfThisClassesNb($idClasse,$session);

//nombre non redoublante fille

$nbnredoubfilles=$classe->getAllStudentFilleNRedtOfThisClassesNb($idClasse,$session);

//nombre de mec redoublant

$nbredoubmecs=$classe->getAllStudentMecRedtOfThisClassesNb($idClasse,$session);

//nombre non redoublant mec

$nbnredoubmecs=$classe->getAllStudentMecNRedtOfThisClassesNb($idClasse,$session);

$montantpaiebaseclasses=$student->getmontantpaiebaseclasse($idClasse,$session);

foreach ($montantpaiebaseclasses as  $amountvalue) :

$montantinscriptionsclasses=$amountvalue->inscriptionmont_classe;
// $montantreinscriptionsclasses=$amountvalue->
$montantscolaritesclasses=$amountvalue->scolarite_classe;

endforeach;


 $versementStudent=$student->getStudentversementInfosNew($codeEtab,$session,$_GET['compte'],$_GET['versementid']);

 foreach ($versementStudent as  $valueVersement):
   $deposant=$valueVersement->deposant_versement;
   $motif=$valueVersement->motif_versement;
   $mode=$valueVersement->mode_versement;
   $montant=$valueVersement->montant_versement;
   $solde=$valueVersement->solde_versement;
   $code=$valueVersement->code_versement;
   $dateversement=$valueVersement->date_versement;
   $beneficiaire=$valueVersement->nom_eleve." ".$valueVersement->prenom_eleve;
   $beneficiairematri=$valueVersement->matricule_eleve;
   $beneficiaireclasse=$valueVersement->libelle_classe;
   $scolaritesmontant=$valueVersement->scolarite_classe;
   $scolaritesnet=$montant+$solde;
   $motifid=$valueVersement->motifid_versement;
   $libelleactivities="";

   if($motif=="AES")
   {
     $datasactivities=$etabs->getActivitiesDetails($motifid,$codeEtab,$session);
     foreach ($datasactivities as $valueactivities):
       $libelleactivities=$valueactivities->libelle_act;
     endforeach;
   }

 endforeach;



 $libellemode="";

 if($mode==1)
 {
  $libellemode="CHEQUES";
 }else if($mode==2)
 {
    $libellemode="ESPECES";

 }else if($mode==3)
 {
   $libellemode="TPE";
 }else if($mode==4)
 {
   $libellemode="Virement Bancaire";
 }


require('fpdf/fpdf.php');

class PDF extends FPDF
{



function Footer()
{
    // Positionnement à 1,5 cm du bas
    $this->SetY(-15);
    // Police Arial italique 8
    $this->SetFont('Arial','I',8);
    // Numéro et nombre de pages
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    // $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
}

// Tableau simple
function BasicTable($header, $data)
{
    // En-tête
    foreach($header as $col)
        $this->Cell(40,7,$col,1);
    $this->Ln();
    // Données
    foreach($data as $row)
    {
        foreach($row as $col)
            $this->Cell(40,6,$col,1);
        $this->Ln();
    }
}
}

$pdf = new PDF('L','mm',array(150,200));
$pdf->AliasNbPages();
$pdf->SetMargins(3,0);
$pdf->SetFont('Times','B',  16);
$pdf->AddPage();
$pdf->Ln(17);
$pdf -> SetX(2);
$pdf->Image("../logo_etab/".$codeEtab."/".$logoEtab,2,1,35,35);
$pdf -> SetX(5);
$pdf-> SetFont('Times','B',14);
$pdf->Cell(125,-24,"INTERNATIONAL BILINGUAL",0,0,'C');
$pdf-> SetFont('Times','B',12);
$pdf->Cell(75,-24,"ANNEE SCOLAIRE : ".$session,0,0,'C');
$pdf->Ln(6);
$pdf-> SetFont('Times','B',14);
$pdf->Cell(125,-24,"SCHOOLS OF AFRICA",0,0,'C');
$pdf-> SetFont('Times','B',12);
$pdf->Cell(75,-24,"DATE : ".date_format(date_create($dateversement),"d/m/Y"),0,0,'C');
$pdf->Ln(6);
$pdf-> SetFont('Times','B',14);
$pdf->Cell(125,-24,"RIVIERA",0,0,'C');
$pdf->SetFont('Times','B',  15);
$pdf->Cell(75,-24,"B.P.F :".$montant." #",0,0,'C');
$pdf->Ln(3);
$pdf -> SetX(5);
$pdf-> SetFont('Times','B',  14);
$pdf->Cell(176,5,"Matricule : ".$beneficiairematri,0,0,'C');
$pdf->Ln(6);
$pdf->Cell(165,5,utf8_decode("N° Famille :"),0,0,'C');
$pdf->Ln(10);
$pdf -> SetX(5);
$pdf-> SetFont('Times','B', 16);
if($motif=="SCOLARITES")
{
  $pdf -> SetX(38);
  $pdf->Cell(80,5,utf8_decode("REÇU - SCOLARITE"),0,0,'C');
  $pdf->SetTextColor(255,0,0);
  $tabcode=explode("VER",$code);
  $versementcode=utf8_decode("N° ".$tabcode[1]);
  $pdf->Cell(25,5,$versementcode,0,0,'C');
  $pdf->Ln(15);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("3");
  $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(40,5,utf8_decode('Nom & Prénoms '),0,0,'C');
  $pdf-> SetFont('Times','B', 12);
  $tabbeneficiaire=explode(" ",$beneficiaire);
  $nbtabbenef=count($tabbeneficiaire);

  if(strlen($beneficiaire)<=30)
   {
     $pdf-> SetFont('Times','I', 14);
     $pdf->Cell(93,5,utf8_decode($beneficiaire),0,0,'L');
   }else {
     $pdf-> SetFont('Times','I', 14);
     $case=$tabbeneficiaire[0]." ".$tabbeneficiaire[1];
     if(strlen($case)>30)
     {
       $case = substr($beneficiaire, 0, 30);
       $pdf->Cell(93,5,utf8_decode($case),0,0,'L');
     }else {
       $pdf->Cell(93,5,utf8_decode($case),0,0,'L');
     }
   }
   $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(13,5,utf8_decode('Classe '),0,0,'C');
  if(strlen($beneficiaireclasse)>15)
  {
    $tabclassebenef=explode('/',$beneficiaireclasse);
    $nbclassebenef=count($tabclassebenef);
    if($nbclassebenef>0)
    {
      $pdf-> SetFont('Times','I', 14);
      $pdf->Cell(47,5,utf8_decode($tabclassebenef[0]),0,0,'L');
    }else {
      $caseclasse= substr($beneficiaireclasse, 0, 15);
      $pdf-> SetFont('Times','I', 14);
      $pdf->Cell(47,5,utf8_decode($caseclasse),0,0,'L');
    }

  }else {
    $pdf-> SetFont('Times','I', 14);
    $pdf->Cell(47,5,utf8_decode($beneficiaireclasse),0,0,'L');
  }
$pdf->Ln(10);
$pdf->SetTextColor(10,5,8);
$pdf -> SetX("3");
$pdf-> SetFont('Times','B', 14);
$pdf->Cell(40,5,utf8_decode('Montant scolarité '),0,0,'C');
$pdf-> SetFont('Times','I', 14);
$pdf->Cell(110,5,utf8_decode(ucfirst($lettre->Conversion($montantscolaritesclasses)).' Francs CFA'),0,0,'L');
$pdf-> SetFont('Times','B', 14);
$pdf->Cell(40,5,utf8_decode('Réduction '),0,0,'L');
$pdf->Ln(10);
$pdf->SetTextColor(10,5,8);
$pdf -> SetX("3");
$pdf-> SetFont('Times','B', 14);
$pdf->Cell(48,5,utf8_decode('Scolarité nette à payer '),0,0,'L');
$pdf-> SetFont('Times','I', 14);
$montantnette=$solde+$montant;
$pdf->Cell(145,5,utf8_decode(ucfirst($lettre->Conversion($solde)).' Francs CFA'),0,0,'L');
$pdf->Ln(10);
$pdf->SetTextColor(10,5,8);
$pdf -> SetX("3");
$pdf-> SetFont('Times','B', 14);
$pdf->Cell(24,5,utf8_decode('Versement '),0,0,'L');
$pdf-> SetFont('Times','I', 14);
$pdf->Cell(90,5,utf8_decode(ucfirst($lettre->Conversion($montant)).' Francs CFA'),0,0,'L');
$pdf-> SetFont('Times','B', 14);
$pdf->Cell(42,5,utf8_decode('Mode de règlement '),0,0,'L');
$pdf-> SetFont('Times','I', 12);
$pdf->Cell(38,5,utf8_decode($libellemode),0,0,'L');

$pdf->Ln(10);



}else if($motif=="TRANSPORTS")
{
  $pdf -> SetX(38);
  $pdf->Cell(80,5,utf8_decode("REÇU - TRANSPORT"),0,0,'C');
  $pdf->SetTextColor(255,0,0);
  $tabcode=explode("VER",$code);
  $versementcode=utf8_decode("N° ".$tabcode[1]);
  $pdf->Cell(25,5,$versementcode,0,0,'C');
  $pdf->Ln(15);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("3");
  $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(40,5,utf8_decode('Nom & Prénoms '),0,0,'C');
  $pdf-> SetFont('Times','B', 12);
  $tabbeneficiaire=explode(" ",$beneficiaire);
  $nbtabbenef=count($tabbeneficiaire);

  if(strlen($beneficiaire)<=30)
   {
     $pdf-> SetFont('Times','I', 14);
     $pdf->Cell(93,5,utf8_decode($beneficiaire),0,0,'L');
   }else {
     $pdf-> SetFont('Times','I', 14);
     $case=$tabbeneficiaire[0]." ".$tabbeneficiaire[1];
     if(strlen($case)>30)
     {
       $case = substr($beneficiaire, 0, 30);
       $pdf->Cell(93,5,utf8_decode($case),0,0,'L');
     }else {
       $pdf->Cell(93,5,utf8_decode($case),0,0,'L');
     }
   }
   $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(13,5,utf8_decode('Classe '),0,0,'C');
  if(strlen($beneficiaireclasse)>15)
  {
    $tabclassebenef=explode('/',$beneficiaireclasse);
    $nbclassebenef=count($tabclassebenef);
    if($nbclassebenef>0)
    {
      $pdf-> SetFont('Times','I', 14);
      $pdf->Cell(47,5,utf8_decode($tabclassebenef[0]),0,0,'L');
    }else {
      $caseclasse= substr($beneficiaireclasse, 0, 15);
      $pdf-> SetFont('Times','I', 14);
      $pdf->Cell(47,5,utf8_decode($caseclasse),0,0,'L');
    }

  }else {
    $pdf-> SetFont('Times','I', 14);
    $pdf->Cell(47,5,utf8_decode($beneficiaireclasse),0,0,'L');
  }
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("3");
  $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(40,5,utf8_decode('Montant scolarité '),0,0,'C');
  $pdf-> SetFont('Times','I', 14);
  $pdf->Cell(110,5,utf8_decode(ucfirst($lettre->Conversion($montant)).' Francs CFA'),0,0,'L');
  $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(40,5,utf8_decode('Réduction '),0,0,'L');
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("3");
  $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(48,5,utf8_decode('Scolarité nette à payer '),0,0,'L');
  $pdf-> SetFont('Times','I', 14);
  $montantnette=$solde+$montant;
  $pdf->Cell(145,5,utf8_decode(''),0,0,'L');
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("3");
  $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(24,5,utf8_decode('Versement '),0,0,'L');
  $pdf-> SetFont('Times','I', 14);
  $pdf->Cell(90,5,utf8_decode(ucfirst($lettre->Conversion($montant)).' Francs CFA'),0,0,'L');
  $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(42,5,utf8_decode('Mode de règlement '),0,0,'L');
  $pdf-> SetFont('Times','I', 12);
  $pdf->Cell(38,5,utf8_decode($libellemode),0,0,'L');

  $pdf->Ln(10);
}else if($motif=="INSCRIPTIONS")
{
  $pdf -> SetX(38);
  $pdf->Cell(80,5,utf8_decode("REÇU - INSCRIPTION"),0,0,'C');
  $pdf->SetTextColor(255,0,0);
  $tabcode=explode("VER",$code);
  $versementcode=utf8_decode("N° ".$tabcode[1]);
  $pdf->Cell(25,5,$versementcode,0,0,'C');
  $pdf->Ln(15);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("3");
  $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(40,5,utf8_decode('Nom & Prénoms '),0,0,'C');
  $pdf-> SetFont('Times','B', 12);
  $tabbeneficiaire=explode(" ",$beneficiaire);
  $nbtabbenef=count($tabbeneficiaire);

  if(strlen($beneficiaire)<=30)
   {
     $pdf-> SetFont('Times','I', 14);
     $pdf->Cell(93,5,utf8_decode($beneficiaire),0,0,'L');
   }else {
     $pdf-> SetFont('Times','I', 14);
     $case=$tabbeneficiaire[0]." ".$tabbeneficiaire[1];
     if(strlen($case)>30)
     {
       $case = substr($beneficiaire, 0, 30);
       $pdf->Cell(93,5,utf8_decode($case),0,0,'L');
     }else {
       $pdf->Cell(93,5,utf8_decode($case),0,0,'L');
     }
   }
   $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(13,5,utf8_decode('Classe '),0,0,'C');
  if(strlen($beneficiaireclasse)>15)
  {
    $tabclassebenef=explode('/',$beneficiaireclasse);
    $nbclassebenef=count($tabclassebenef);
    if($nbclassebenef>0)
    {
      $pdf-> SetFont('Times','I', 14);
      $pdf->Cell(47,5,utf8_decode($tabclassebenef[0]),0,0,'L');
    }else {
      $caseclasse= substr($beneficiaireclasse, 0, 15);
      $pdf-> SetFont('Times','I', 14);
      $pdf->Cell(47,5,utf8_decode($caseclasse),0,0,'L');
    }

  }else {
    $pdf-> SetFont('Times','I', 14);
    $pdf->Cell(47,5,utf8_decode($beneficiaireclasse),0,0,'L');
  }
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("3");
  $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(40,5,utf8_decode('Montant scolarité '),0,0,'C');
  $pdf-> SetFont('Times','I', 14);
  $pdf->Cell(110,5,utf8_decode(ucfirst($lettre->Conversion($montantinscriptionsclasses)).' Francs CFA'),0,0,'L');
  $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(40,5,utf8_decode('Réduction '),0,0,'L');
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("3");
  $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(48,5,utf8_decode('Scolarité nette à payer '),0,0,'L');
  $pdf-> SetFont('Times','I', 14);
  $montantnette=$solde+$montant;
  $pdf->Cell(145,5,utf8_decode(ucfirst($lettre->Conversion($solde)).' Francs CFA'),0,0,'L');
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("3");
  $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(24,5,utf8_decode('Versement '),0,0,'L');
  $pdf-> SetFont('Times','I', 14);
  $pdf->Cell(90,5,utf8_decode(ucfirst($lettre->Conversion($montant)).' Francs CFA'),0,0,'L');
  $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(42,5,utf8_decode('Mode de règlement '),0,0,'L');
  $pdf-> SetFont('Times','I', 12);
  $pdf->Cell(38,5,utf8_decode($libellemode),0,0,'L');

  $pdf->Ln(10);


}else if($motif=="AES")
{
  $pdf -> SetX(38);
  $pdf->Cell(80,5,utf8_decode("EXTRA"),0,0,'C');
  $pdf->SetTextColor(255,0,0);
  $tabcode=explode("VER",$code);
  $versementcode=utf8_decode("N° ".$tabcode[1]);
  $pdf->Cell(25,5,$versementcode,0,0,'C');
  $pdf->Ln(15);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("3");
  $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(15,5,utf8_decode('De M. '),0,0,'L');
  $pdf-> SetFont('Times','B', 12);
  $tabbeneficiaire=explode(" ",$beneficiaire);
  $nbtabbenef=count($tabbeneficiaire);

  if(strlen($beneficiaire)<=60)
   {
     $pdf-> SetFont('Times','I', 14);
     $pdf->Cell(150,5,utf8_decode($beneficiaire),0,0,'L');
   }else {
     $pdf-> SetFont('Times','I', 14);
     $case=$tabbeneficiaire[0]." ".$tabbeneficiaire[1]." ".$tabbeneficiaire[2];
     if(strlen($case)>30)
     {
       $case = substr($beneficiaire, 0, 30);
       $pdf->Cell(150,5,utf8_decode($case),0,0,'L');
     }else {
       $pdf->Cell(150,5,utf8_decode($case),0,0,'L');
     }
   }

  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("3");
  $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(30,5,utf8_decode('La somme de '),0,0,'L');
  $pdf-> SetFont('Times','I', 14);
  $pdf->Cell(150,5,utf8_decode(ucfirst($lettre->Conversion($montant)).' Francs CFA'),0,0,'L');
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("3");
  $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(12,5,utf8_decode('Pour '),0,0,'L');
  $pdf-> SetFont('Times','I', 14);

  $pdf->Cell(145,5,utf8_decode($libelleactivities),0,0,'L');
  $pdf->Ln(10);
  $pdf->SetTextColor(10,5,8);
  $pdf -> SetX("3");
  $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(24,5,utf8_decode('Avance '),0,0,'L');
  $pdf-> SetFont('Times','I', 14);
  $pdf->Cell(90,5,utf8_decode(ucfirst($lettre->Conversion($montant)).' Francs CFA'),0,0,'L');
  $pdf-> SetFont('Times','B', 14);
  $pdf->Cell(42,5,utf8_decode('Reste '),0,0,'L');
  $pdf-> SetFont('Times','I', 12);
  $pdf->Cell(38,5,"",0,0,'L');

  $pdf->Ln(10);
}
$pdf-> SetFont('Times','BU', 14);
$pdf->Ln(8);
$pdf->Cell(80,8,'Remettant',0,0,'C');
$pdf->Cell(110,8,'Visa Caisse  ',0,0,'C');















$pdf->Output();



 ?>
