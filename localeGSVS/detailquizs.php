<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/Tache.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

// echo $_SESSION['user']['fonctionuser'];
if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$tachex = new Tache();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

  if($_SESSION['user']['groupe']==1)
  {
    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
    {
      // echo "bonsoir";
      $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);

      //nous allons chercher la liste des etablissements du groupe

      $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
        }

        if(strlen($_SESSION['user']['codeEtab'])>0)
        {

        }else {
          $_SESSION['user']['codeEtab']=$codeEtabAssigner;
        }

        $_SESSION['user']['session']=$libellesessionencours;





         // var_dump($allcodeEtabs);

        $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

        $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


    }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
    {
      // echo "bonjour";
      $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);
      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
          $_SESSION['user']['session']=$libellesessionencours;

          if(strlen($_SESSION['user']['codeEtab'])>0)
          {

          }else {
            $_SESSION['user']['codeEtab']=$codeEtabAssigner;
          }

          $notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

          // $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
          $classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);
          $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

          // var_dump($classes);
        }

    }
  }

$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);

$lasttachesassignated=$tachex->gettacherassignerTouserlast($_SESSION['user']['IdCompte']);

$lasttachesassignatedby=$tachex->gettacherassignerTouserlastby($_SESSION['user']['IdCompte']);

$nbnotificationstandby=$parents->getParentnotificationstandbyNb($compteuserid);

//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($compteuserid);

$courseid=$_GET['course'];
$classeid=$_GET['classeid'];

$coursesdetails=$etabs->getAllquizsdetailsEtab($courseid,$classeid,$codeEtabAssigner,$libellesessionencours);

foreach ($coursesdetails as  $datacourses):
  $descricourses=$datacourses->instruction_quiz;
  $durationcourses=$datacourses->duree_quiz;
  $teatchercourses=$datacourses->nom_compte;
  $datercourses=$datacourses->datelimite_quiz;
  $namecourses=$datacourses->libelle_quiz;
  $classecourses=$datacourses->libelle_classe;
  $classeidcourses=$datacourses->id_classe;
  $matiereidcourses=$datacourses->id_mat;
  $libellematcourses=$datacourses->libelle_mat;
  $statutcourses=$datacourses->statut_quiz;
     /* ici je vais verifier si l'LocalAdmin a valider le Quiz pour permettre au prof de publier*/
  $confirmquiz=$datacourses->confirm_quiz;
  $filescourses="";

endforeach;



// Nous allons recuperer les questions du quiz

  $questions=$etabs->getAllquizQuestionEtab($courseid,$classeid,$codeEtabAssigner,$libellesessionencours);
  // var_dump($lasttachesassignated);

// echo $libellesessionencours."/".$_SESSION['user']['codeEtab'];

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <link href="../assets2/css/style1.css" rel="stylesheet" type="text/css" />
    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
              <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">Détails du quiz</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                            </li>
                            <li><a class="parent-item" href="#">Quizs</a>&nbsp;<i class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">Détails du quiz</li>
                        </ol>
                    </div>
                </div>

        <!-- start widget -->
        <?php

              if(isset($_SESSION['user']['addclasseok']))
              {

                ?>
                <!--div class="alert alert-success alert-dismissible fade show" role="alert">
              <?php
              //echo $_SESSION['user']['addetabok'];
              ?>
              <a href="#" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
                 </a>
              </div-->
<link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
<script src="../assets/js/sweetalert2.min.js"></script>

          <script>
          Swal.fire({
          type: 'success',
          title: 'Félicitations',
          text: '<?php echo $_SESSION['user']['addclasseok'] ?>',

          })
          </script>
                <?php
                unset($_SESSION['user']['addclasseok']);
              }

               ?>

               <?php
               if($nbsessionOn==0)
               {
                 ?>
                 <div class="alert alert-danger alert-dismissible fade show" role="alert">

                 Vous devez definir la Sessionn scolaire

                 <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
                  </a>
                 </div>
                 <?php
               }
                ?>



                <div class="row">
      <div class="col-md-12">
        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar">
          <div class="card card-topline-aqua">
            <div class="card-body no-padding height-9">
              <div class="row">
                <div class="course-picture">
                  <img src="../photo/course2.jpg" class="img-responsive"
                    alt=""> </div>
              </div>
              <div class="profile-usertitle">
                <div class="profile-usertitle-name"> <?php echo utf8_decode(utf8_encode($namecourses));?> </div>
              </div>
              <!-- END SIDEBAR USER TITLE -->
            </div>
          </div>
          <div class="card">
            <div class="card-head card-topline-aqua" style="text-align:center">
              <header>Instructions du quiz</header>
            </div>
            <div class="card-body no-padding height-9">
              <div class="profile-desc">
                <?php echo utf8_decode(utf8_encode($descricourses)); ?>
              </div>
              <ul class="list-group list-group-unbordered">
                <!-- <li class="list-group-item">
                  <b>Durée </b>
                  <div class="profile-desc-item pull-right"><?php //echo utf8_decode(utf8_encode($durationcourses)); ?></div>
                </li> -->
                <li class="list-group-item">
                  <b><i class="fa fa-university"></i> <?php echo L::ClasseMenu  ?> </b>
                  <div class="profile-desc-item pull-right"><?php echo utf8_decode(utf8_encode($classecourses)); ?></div>
                </li>
                <li class="list-group-item">
                  <b><i class="fa fa-user-circle-o"></i> <?php echo L::ProfsMenusingle  ?></b>
                  <div class="profile-desc-item pull-right"><?php echo utf8_decode(utf8_encode($teatchercourses)); ?>  </div>
                </li>
                <li class="list-group-item">
                  <b><i class="fa fa-calendar"></i> Date limite</b>
                  <div class="profile-desc-item pull-right">
                    <?php
                    $tabdate=explode("-",$datercourses);

                    echo $tabdate[2]." ".obtenirLibelleMois($tabdate[1])." ".$tabdate[0];
                    ?>
                  </div>
                </li>
                <li class="list-group-item">
                  <b><i class="fa fa-clock-o"></i> Durée </b>
                  <div class="profile-desc-item pull-right"><?php echo returnHours($durationcourses) ?></div>
                </li>
              </ul>
              <div class="row list-separated profile-stat">

                <div class="col-md-4 col-sm-4 col-6">
                  <div class="uppercase profile-stat-title"> <?php echo  $classe->DetermineNumberOfStudentInThisClasse($matiereidcourses,$codeEtabAssigner,$libellesessionencours); ?> </div>
                  <div class="uppercase profile-stat-text"> <a href="#" title="Nombre d'étudiant de la classe"><i class="fa fa-group fa-2x"></i></a> </div>
                </div>
                <div class="col-md-4 col-sm-4 col-6">
                  <div class="uppercase profile-stat-title"> 0 </div>
                  <div class="uppercase profile-stat-text">  <a href="#" title="quiz terminé"><i class="fa fa-upload fa-2x"></i></a> </div>
                </div>
                <div class="col-md-4 col-sm-4 col-6">
                  <div class="uppercase profile-stat-title"> 0 </div>
                  <div class="uppercase profile-stat-text">  <a href="#" title="Commentaires du quiz"><i class="fa fa-comments fa-2x"></i></a> </div>
                </div>

              </div>
              <?php
              // echo $statutcourses;
              if($statutcourses==0)
              {
               ?>
               <?php
               if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="Marketing"||$_SESSION['user']['fonctionuser']=="CC"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG")
               {
                ?>
              <div class="col-md-12" style="text-align:center">
                <a href="updatequizs.php?courseid=<?php echo $courseid ?>&classeid=<?php echo $classeid; ?>" class="btn btn-md btn-primary" style="border-radius:5px;"> <i class="fa fa-pencil"></i> Modifier le Quiz </a>
              </div>
              <?php
            }
               ?>
              <?php
            }
               ?>

            </div>
          </div>
        </div>
        <!-- END BEGIN PROFILE SIDEBAR -->
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
          <div class="row">
            <div class="col-md-12">
              <div class="card card-topline-aqua">
                <div class="card-head" style="text-align:center">
                    <header>Publication du quiz</header>

                </div>
                <div class="card-body no-padding height-9" style="text-align:center">
                   <?php
                        if($confirmquiz==0)
                        {
                          ?>
                            <a  class="btn btn-circle btn-danger btn-sm" href="#"  style="" onclick="ConfirmQuiz(<?php echo $courseid;?>,<?php echo $classeid;  ?>,<?php echo $compteuserid; ?>,'<?php echo $codeEtabAssigner; ?>','<?php echo $libellesessionencours; ?>',<?php echo $matiereidcourses; ?> )"> <i class="fa fa-send"></i> Confirmer le Quiz</a>
                            <!-- <p class="btn btn-danger">Confirmer le devoir</p> -->
                          <?php
                        }
                        elseif ($statutcourses==0) {

                          if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="CC"||$_SESSION['user']['fonctionuser']=="CD")
                          {
                          ?>
                          <a href="#" class="btn btn-success btn-md" style="" onclick="publicationdevoir(<?php echo $courseid;?>,<?php echo $classeid;  ?>,<?php echo $compteuserid; ?>,'<?php echo $codeEtabAssigner; ?>','<?php echo $libellesessionencours; ?>',<?php echo $matiereidcourses; ?> )"> <i class="fa fa-send"></i> <?php echo L::DevoirsPublishBtn ?></a>
                          <?php
                        }
                        }else {

                          ?>
                          <a href="#" class="btn btn-success btn-md" style="margin-left:-110px" > <i class="fa fa-check-circle "></i> quiz publiéS</a>

                          <?php
                        }
                         ?>


                  <!--   <?php
                    if($statutcourses==0)
                    {
                      ?> -->
                        <!-- <a href="#" class="btn btn-success btn-md" onclick="publicationquiz(<?php echo $courseid;?>,<?php echo $classeid;  ?>,<?php echo $compteuserid; ?>,'<?php echo $codeEtabAssigner; ?>','<?php echo $libellesessionencours; ?>',<?php echo $matiereidcourses; ?> )"> <i class="fa fa-send"></i> Publier le quiz</a> -->
                  <!--     <?php
                    }else {
                      ?>
                        <a href="#" class="btn btn-success btn-md" > <i class="fa fa-check-circle "></i>quiz publié</a>
                      <?php
                    }
                     ?>
 -->




                  <!-- END SIDEBAR USER TITLE -->
                </div>
              </div>
            </div>
            <!-- <div class="col-md-6">
              <div class="card card-topline-aqua">
                <div class="card-head" style="text-align:center">
                    <header><span style="text-align:center;">Support du devoir</span> </header>

                </div>
                <div class="card-body no-padding height-9" style="text-align:center">
                <!-- <a href="#" class="btn btn-primary btn-md"><i class="fa fa-pencil"></i> Modiffier</a> -->
                <?php
                //$lien="../courses/".$classecourses."/".$datercourses."/".$libellematcourses."/".$filescourses;
                 ?>
                <!-- <a href="<?php //echo $lien;  ?>" target="_blank" class="btn btn-warning btn-md" ><i class="fa fa-download"></i>Telecharger</a> -->
                  <!-- END SIDEBAR USER TITLE -->
                <!-- </div> -->
              <!-- </div> -->
            <!-- </div> -->
          </div>
          <div class="row">
            <div class="col-md-12 col-sm-12">
        <div class="card card-box">
          <div class="card-head">
            <header><span class="label label-lg label-warning"  style="text-align:center;"> <i class="fa fa-info-circle"> </i> Questions & propositions de réponse</span></header>
            <div class="tools">
              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
              <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
              <!-- <a class="t-close btn-color fa fa-times" href="javascript:;"></a> -->
            </div>
          </div>
          <div class="card-body" id="line-parent">
            <div class="panel-group accordion" id="accordion3">

              <?php
              $i=1;
                foreach ($questions as $value):

               ?>
               <?php
                if($i==1)
                {
                  ?>
                  <div class="panel panel-default">
                    <div class="panel-heading panel-heading-gray">
                      <h4 class="panel-title">
                        <a class="accordion-toggle accordion-toggle-styled collapsed"
                          data-toggle="collapse" data-parent="#accordion3"
                          href="#collapse_3_<?php echo $i; ?>"><?php echo $value->libelle_quest ?> </a>
                      </h4>
                    </div>
                    <div id="collapse_3_<?php echo $i; ?>" class="panel-collapse in ">
                      <div class="panel-body">

                        <?php
                        $modereponse=$value->mode_quest;

                        //determiner la liste des propositions

                        $propositions=$etabs->getPropositionsOfQuestions($value->id_quest);

                        // var_dump($propositions);

                         ?>

                         <?php
                          if($modereponse==1)
                          {
                            ?>
                            <a class="btn green-bgcolor" href="#"	> VRAI / FAUX  </a> <div class="pull-right"><?php echo $value->point_quest; ?> POINT(S)</div>
                            <?php
                          }else {
                            ?>
                            <a class="btn red" href="#"	> CHOIX MULTIPLE </a> <div class="pull-right"><?php echo $value->point_quest; ?> POINT(S)</div>

                            <?php
                          }
                          ?>

                          <hr>
                          <?php
                          if($modereponse==1)
                          {
                            $solutionquestion=$etabs->getPropositionsOfQuestionsolution($value->id_quest);
                            foreach ($solutionquestion as  $solution):
                              ?>
                                <span class="label label-lg label-warning"  style="text-align:center;"> <i class="fa fa-check-circle"> </i> SOLUTION</span> &nbsp;&nbsp;<b style="text-align:center;">  <?php echo $solution->libelle_proprep ?></b>
                              <?php
                            endforeach;
                            ?>

                            <hr>
                            <?php

                          }else {
                            // code...
                          }
                           ?>



                      </div>
                    </div>
                  </div>
                  <?php
                }else {
                  ?>
                  <div class="panel panel-default">
                <div class="panel-heading panel-heading-gray">
                  <h4 class="panel-title">
                    <a class="accordion-toggle accordion-toggle-styled collapsed"
                      data-toggle="collapse" data-parent="#accordion3"
                      href="#collapse_3_<?php echo $i; ?>"> <?php echo $value->libelle_quest ?> </a>
                  </h4>
                </div>
                <div id="collapse_3_<?php echo $i; ?>" class="panel-collapse collapse">
                  <div class="panel-body">

                    <?php
                    $modereponse=$value->mode_quest;
                     ?>

                     <?php
                      if($modereponse==1)
                      {
                        ?>
                        <a class="btn green-bgcolor" href="#"	> VRAI / FAUX  </a> <div class="pull-right"><?php echo $value->point_quest; ?> POINT(S)</div>
                        <?php
                      }else {
                        ?>
                        <a class="btn red" href="#"	> CHOIX MULTIPLE </a> <div class="pull-right"><?php echo $value->point_quest; ?> POINT(S)</div>

                        <?php
                      }
                      ?>
                      <hr>
                      <?php
                      if($modereponse==1)
                      {
                        $solutionquestion=$etabs->getPropositionsOfQuestionsolution($value->id_quest);
                        foreach ($solutionquestion as  $solution):
                          ?>
                            <span class="label label-lg label-warning"  style="text-align:center;"> <i class="fa fa-check-circle"> </i> SOLUTION</span> &nbsp;&nbsp;<b style="text-align:center;">  <?php echo $solution->libelle_proprep ?></b>
                          <?php
                        endforeach;
                        ?>

                        <hr>
                        <?php

                      }else {
                        ?>
                          <span class="label label-lg label-warning"  style="text-align:center;"> <i class="fa fa-check-circle"> </i> SOLUTION</span>
                          <hr>

                        <?php
                        $solutionquestion=$etabs->getPropositionsOfQuestionsolution($value->id_quest);
                        foreach ($solutionquestion as  $solution):
                          ?>
                          <b><?php echo $solution->libelle_proprep  ?></b>&nbsp;&nbsp; <i class="fa fa-check" style="color:green;"></i>
                          <?php
                        endforeach;
                      }
                       ?>



                  </div>
                </div>
              </div>
                  <?php
                }
                ?>


              <?php
              $i++;
            endforeach;
               ?>
            </div>
          </div>
        </div>
      </div>
            <?php
            if($statutcourses!=0)
            {
              ?>
              <div class="col-md-12">
                <div class="card">
                  <div class="card-topline-aqua">
                    <header></header>
                  </div>
                  <div class="white-box">
                    <!-- Nav tabs -->
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane active fontawesome-demo">
                        <div id="biography" class="col-md-12">
<span class="label label-lg label-warning"  style="text-align:center;"> <i class="fa fa-info-circle"> </i> Liste de classe</span>

                          <hr>
                        <div class="table-scrollable">
                          <div class="pull-right">

                            <button type="button"   class="btn btn-success  btn-xs"> <i class="fa fa-check"></i> Valider les notes</button>
                          </div>
                          <br><br>
                          <table class="table table-hover table-checkable order-column full-width" >
                            <thead>
                              <tr>
                                <th style="text-align:center">Nom & prénoms</th>
                                <th style="text-align:center">Statut</th>
                                <th style="text-align:center">Note</th>
                                <th style="text-align:center">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              $nbsoumisquiz=$etabs->getNbquizsoumis($courseid,$classeid,$codeEtabAssigner,$libellesessionencours);
                              if($nbsoumisquiz==0)
                              {
                                ?>
                                <tr>
                                  <td colspan="4">Aucun quiz soumis</td>
                                </tr>
                                <?php
                              }else {

                                $students=$student->getAllStudentOfClassesId($_GET['classeid'],$libellesessionencours);
                                foreach ($students as  $value):
                                  $rendus=$etabs->soumisquizstudent($value->id_compte,$courseid,$classeid,$codeEtabAssigner,$libellesessionencours);
                                ?>
                                <tr>
                                  <td style=""> <span style="font-size:10px;"><?php   echo $value->nom_eleve." ".$value->prenom_eleve; ?></span> </td>
                                  <td style="text-align:center">
                                    <?php
                                    if(count($rendus)>0)
                                    {
                                      ?>
                                      <span class="label label-sm label-success">Quiz soumis</span>
                                      <?php
                                    }else {
                                      ?>
                                      <span class="label label-sm label-danger">Quiz non soumis</span>
                                      <?php
                                    }
                                     ?>
                                  </td>
                                  <td style="text-align:center">
                                    <?php
                                    if(count($rendus)>0)
                                    {
                                      ?>
                                      <span class="label label-sm label-success"><?php echo $etabs->getcompteNotequiz($courseid,$classeid,$codeEtabAssigner,$libellesessionencours,$value->id_compte) ?></span>
                                      <?php
                                    }else {
                                      ?>
                                      <!-- <span class="label label-sm label-danger">Quiz non soumis</span> -->
                                      <?php
                                    }
                                     ?>
                                  </td>
                                  <td></td>
                                </tr>
                                <?php
                                endforeach;
                              }
                               ?>
                            </tbody>
                          </table>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php
            }
             ?>
          </div>
        </div>
        <!-- END PROFILE CONTENT -->
      </div>
    </div>
        <!-- end widget -->
        <!-- chart start -->



              </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <!-- start chat sidebar -->

                        <div class="chat-sidebar-container" data-close-on-body-click="false">
                        <div class="chat-sidebar">
                          <ul class="nav nav-tabs">
                            <li class="nav-item">
                              <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                                  class="material-icons">
                                  chat</i>Chat
                                <!-- <span class="badge badge-danger">4</span> -->
                              </a>
                            </li>
                          </ul>
                          <div class="tab-content">
                            <!-- Start User Chat -->
                            <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                              id="quick_sidebar_tab_1"> -->
                              <div class="chat-sidebar-chat "
                                >
                              <div class="chat-sidebar-list">
                                <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                                  data-wrapper-class="chat-sidebar-list">
                                  <div class="chat-header">
                                    <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                                  </div>
                                  <ul class="media-list list-items">
                                    <?php

                                    foreach ($onlineUsers as  $valueUsers):
                                      $tofuser=$valueUsers->photo_compte;
                                      if(strlen($tofuser)>0)
                                      {
                                        $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                                      }else {
                                        $lientofuser="../photo/user5.jpg";
                                      }
                                      if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                                      {
                                        ?>
                                        <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                            width="35" height="35" alt="...">
                                          <i class="online dot red"></i>
                                          <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                            <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                            <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                                          </div>
                                        </li>
                                        <?php
                                      }
                                      ?>

                                      <?php
                                    endforeach;
                                     ?>

                                  </ul>
                                  <div class="chat-header">
                                    <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                                  </div>
                                  <ul class="media-list list-items">
                                    <?php

                                    foreach ($offlineUsers as  $valueUsers):
                                      $tofuser=$valueUsers->photo_compte;
                                      if(strlen($tofuser)>0)
                                      {
                                        $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                                      }else {
                                        $lientofuser="../photo/user5.jpg";
                                      }
                                      ?>
                                      <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                          width="35" height="35" alt="...">
                                        <i class="offline dot"></i>
                                        <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                          <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                          <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                                        </div>
                                      </li>
                                      <?php
                                    endforeach;
                                     ?>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <!-- End User Chat -->
                          </div>
                        </div>
                      </div>
                        <!-- end chat sidebar -->
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
  <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- morris chart -->

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>

     <script src="../assets2/plugins/chart-js/Chart.bundle.js" ></script>
     <script src="../assets2/plugins/chart-js/utils.js" ></script>
     <!-- <script src="../assets2/js/pages/chart/chartjs/chartjs-data.js" ></script> -->
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>

  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>



   function terminer(idcompte,idtache)
   {
     // alert(idcompte);
     var etape=1;

     Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::Haveyoufinishtasks ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::True ?>',
cancelButtonText: '<?php echo L::False ?>',
}).then((result) => {
if (result.value) {

  $.ajax({
    url: '../ajax/taches.php',
    type: 'POST',
    async:false,
    data: 'etape=' + etape+ '&idcompte=' +idcompte+'&idtache='+idtache,
    dataType: 'text',
    success: function (content, statut) {

    // location.reload();

    }
  });

}else {

}
})
   }

   function addmessages(senderid,receiverid)
   {

     var etape=1;
     $.ajax({
       url: '../ajax/chat.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&senderid=' +senderid+'&receiverid='+receiverid,
       dataType: 'text',
       success: function (content, statut) {

       document.location.href="chats.php";

       }
     });
   }

   function SetcodeEtab(codeEtab)
   {
     var etape=3;
     $.ajax({
       url: '../ajax/sessions.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   $("#fichier3").dropify({
     messages: {
         "default": "Merci de selectionner le support",
         "replace": "Modifier le support",
         "remove" : "Supprimer le support",
         "error"  : "Erreur"
     }
   });

  $("#matclasse").select2();
  $("#classeEtab").select2();

  function searchcodeEtab(id)
  {
  var classe=$("#classeEtab").val();
  var teatcherId=id;
  var etape=7;
  var matiere=$("#matclasse").val();

  $.ajax({

       url: '../ajax/matiere.php',
       type: 'POST',
       async:true,
       data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
       dataType: 'text',
       success: function (content, statut) {

          $("#FormAddAcademique #codeEtab").val(content);

       }
     });

  }


  /*Confirmation du devoir par l'administration de GSVS*/

  function ConfirmQuiz(courseid,classeid,teatcherid,codeEtab,sessionEtab,matiereid)
     {
       Swal.fire({
     title: '<?php echo L::WarningLib ?>',
     text: "<?php echo L::confirmeQuiz ?>",
     type: 'warning',
     showCancelButton: true,
     confirmButtonColor: '#3085d6',
     cancelButtonColor: '#d33',
     confirmButtonText: '<?php echo L::Published ?>',
     cancelButtonText: '<?php echo L::AnnulerBtn ?>',
     }).then((result) => {
     if (result.value) {

       var etape=4;

       $.ajax({

            url: '../ajax/devoirs.php',
            type: 'POST',
            async:true,
            data: 'teatcherid=' + teatcherid+ '&etape=' + etape+'&classe='+classeid+'&matiere='+matiereid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&courseid='+courseid,
            dataType: 'text',
            success: function (content, statut) {

               $("#FormAddAcademique #codeEtab").val(content);

               location.reload();

            }
          });

     }else {

     }
     })
     }

function searchmatiere(id)
  {
    var classe=$("#classeEtab").val();
    var teatcherId=id;
    var etape=6;

  $.ajax({

       url: '../ajax/matiere.php',
       type: 'POST',
       async:true,
       data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe,
       dataType: 'text',
       success: function (content, statut) {


         $("#matclasse").html("");
         $("#matclasse").html(content);

       }
     });
  }

  function deletedTache(id)
  {
  var concattache=$("#concattache").val();

  $("#concattache").val($("#concattache").val().replace(id+"@", ""));

   $('#rowTache'+id+'').remove();

   recalcultachenb();
  }

  function deletedComp(id)
  {
  var concatcomp=$("#concatcomp").val();

  $("#concatcomp").val($("#concatcomp").val().replace(id+"@", ""));

   $('#rowComp'+id+'').remove();

   // recalculsectionnb();

   var concatcomp=$("#concatcomp").val();

   var tab=concatcomp.split("@");

   var nbtab=tab.length;

   var nbtabnew=parseInt(nbtab)-1;

   $("#concatnbcomp").val(nbtabnew);

  }

  function deletedTaches(id)
  {
  var concattaches=$("#concattaches").val();

  $("#concattaches").val($("#concattaches").val().replace(id+"@", ""));

   $('#rowTaches'+id+'').remove();

   // recalculsectionnb();

   var concattaches=$("#concattaches").val();

   var tab=concattaches.split("@");

   var nbtab=tab.length;

   var nbtabnew=parseInt(nbtab)-1;

   $("#concatnbtaches").val(nbtabnew);

  }


  function deletedSection(id)
  {
  var concatsection=$("#concatsection").val();

  $("#concatsection").val($("#concatsection").val().replace(id+"@", ""));

   $('#rowSection'+id+'').remove();

   // recalculsectionnb();

   var concatsection=$("#concatsection").val();

   var tab=concatsection.split("@");

   var nbtab=tab.length;

   var nbtabnew=parseInt(nbtab)-1;

   $("#concatnbsection").val(nbtabnew);

  }

  function recalcultachenb()
  {
  var concattache=$("#concattache").val();

  var tab=concattache.split("@");

  var nbtab=tab.length;

  var nbtabnew=parseInt(nbtab)-1;

  $("#concatnbtache").val(nbtabnew);
  }

  function recalculsectionnb()
  {

  }


  function AddtachesRow()
  {
  var nb=$("#nbtaches").val();
  var nouveau= parseInt(nb)+1;
  $("#nbtaches").val(nouveau);

    var concattaches=$("#concattaches").val();
    $("#concattaches").val(concattaches+nouveau+"@");

    var concattaches=$("#concattaches").val();

    var tab=concattaches.split("@");

    var nbtab=tab.length;

    var nbtabnew=parseInt(nbtab)-1;

    $("#concatnbtaches").val(nbtabnew);

    // recalculsectionnb();

    if(nbtabnew==1)
    {
      $('#dynamic_field2').append('<tr id="rowTaches'+nouveau+'"><td><input type="text" name="taches_'+nouveau+'" id="taches_'+nouveau+'" placeholder="<?php echo L::EnterNewExercice ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteTaches'+nouveau+'" id="deleteTaches'+nouveau+'"  onclick="deletedTaches('+nouveau+')"  class="btn btn-danger btn_remove" disabled="disabled">X</button></td></tr>');
    }else {
      $('#dynamic_field2').append('<tr id="rowTaches'+nouveau+'"><td><input type="text" name="taches_'+nouveau+'" id="taches_'+nouveau+'" placeholder="<?php echo L::EnterNewExercice ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteTaches'+nouveau+'" id="deleteTaches'+nouveau+'"  onclick="deletedTaches('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');
    }



    for(var i=0;i<nbtabnew;i++)
    {
      var indice=tab[i];
      // alert(indice);
      $("#taches_"+indice).rules( "add", {
          required: true,
          messages: {
          required: "<?php echo L::AddNewexerciceplease ?>"
  }
        });
    }


  }

  function AddcompRow()
  {
  var nb=$("#nbcomp").val();
  var nouveau= parseInt(nb)+1;
  $("#nbcomp").val(nouveau);

    var concatcomp=$("#concatcomp").val();
    $("#concatcomp").val(concatcomp+nouveau+"@");

    var concatcomp=$("#concatcomp").val();

    var tab=concatcomp.split("@");

    var nbtab=tab.length;

    var nbtabnew=parseInt(nbtab)-1;

    $("#concatnbcomp").val(nbtabnew);

    // recalculsectionnb();

    if(nbtabnew==1)
    {
      $('#dynamic_field1').append('<tr id="rowComp'+nouveau+'"><td><input type="text" name="comp_'+nouveau+'" id="comp_'+nouveau+'" placeholder="<?php echo L::AddCompViseeRequired ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteComp'+nouveau+'" id="deleteComp'+nouveau+'"  onclick="deletedComp('+nouveau+')"  class="btn btn-danger btn_remove" disabled="disabled">X</button></td></tr>');
    }else {
      $('#dynamic_field1').append('<tr id="rowComp'+nouveau+'"><td><input type="text" name="comp_'+nouveau+'" id="comp_'+nouveau+'" placeholder="<?php echo L::AddCompViseeRequired ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteComp'+nouveau+'" id="deleteComp'+nouveau+'"  onclick="deletedComp('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');
    }



    for(var i=0;i<nbtabnew;i++)
    {
      var indice=tab[i];
      // alert(indice);
      $("#comp_"+indice).rules( "add", {
          required: true,
          messages: {
          required: "<?php echo L::AddCompetenceViseeplease ?>"
  }
        });
    }


  }

  function AddsectionRow()
  {
  var nb=$("#nb").val();
  var nouveau= parseInt(nb)+1;
  $("#nb").val(nouveau);

    var concatsection=$("#concatsection").val();
    $("#concatsection").val(concatsection+nouveau+"@");

    var concatsection=$("#concatsection").val();

    var tab=concatsection.split("@");

    var nbtab=tab.length;

    var nbtabnew=parseInt(nbtab)-1;

    $("#concatnbsection").val(nbtabnew);

    // recalculsectionnb();

    if(nbtabnew==1)
    {
      $('#dynamic_field').append('<tr id="rowSection'+nouveau+'"><td><input type="text" name="section_'+nouveau+'" id="section_'+nouveau+'" placeholder="<?php echo L::EnterSection ?>" class="form-control objectif_list" /></td><td><button type="button" id="deleteSection'+nouveau+'" id="deleteSection'+nouveau+'"  onclick="deletedSection('+nouveau+')"  class="btn btn-danger btn_remove" disabled="disabled">X</button></td></tr>');
    }else {
      $('#dynamic_field').append('<tr id="rowSection'+nouveau+'"><td><input type="text" name="section_'+nouveau+'" id="section_'+nouveau+'" placeholder="<?php echo L::EnterSection ?>" class="form-control objectif_list" /></td><td><button type="button" id="deleteSection'+nouveau+'" id="deleteSection'+nouveau+'"  onclick="deletedSection('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');
    }



    for(var i=0;i<nbtabnew;i++)
    {
      var indice=tab[i];
      // alert(indice);
      $("#section_"+indice).rules( "add", {
          required: true,
          messages: {
          required: "<?php echo L::AddSectionplease ?>"
  }
        });
    }


  }

  function AddtacheRow()
  {
  var nb=$("#nbtache").val();
  var nouveau= parseInt(nb)+1;
  $("#nbtache").val(nouveau);

    var concattache=$("#concattache").val();
    $("#concattache").val(concattache+nouveau+"@");

    recalcultachenb();

    $('#dynamic_field1').append('<tr id="rowTache'+nouveau+'"><td><input type="text" name="tache_'+nouveau+'" id="tache_'+nouveau+'" placeholder="Entrer une tache" class="form-control objectif_list" /></td><td><button type="button" id="deleteTache'+nouveau+'" id="deleteTache'+nouveau+'"  onclick="deletedTache('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');

    for(var i=1;i<=nouveau;i++)
    {
      $("#tache_"+i).rules( "add", {
          required: true,
          messages: {
          required: "<?php echo L::RequiredChamp ?>"
  }
        });
    }


  }

   $(document).ready(function() {

     $("#FormAddAcademique").validate({

       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
      },
      success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },
       rules:{

         programme:"required",
         descri:"required",
         classeEtab:"required",
         matclasse:"required",
         fichier:"required",
         durationcourse:"required",
         detailscourse:"required",
         datecourse:"required"

       },
     messages: {
       programme:"Merci de renseigner le libellé du programme",
       descri:"Merci de renseigner le détails de la fiche ",
       classeEtab:"<?php echo L::PleaseSelectclasseOnerequired ?>",
       matclasse:"Merci de <?php echo L::SelectSubjects ?>",
       fichier:"Merci de selectionner le fichier du programme",
       durationcourse:"<?php echo L::DurationcourseRequired ?>",
       detailscourse:"<?php echo L::DetailscourseRequired ?>",
       datecourse:"<?php echo L::DatecourseRequired ?>"
     },
     submitHandler: function(form) {

     form.submit();



     }

     });

     AddsectionRow();
      AddcompRow();

     $('#add').click(function(){

       //creation d'une ligne de section

       AddsectionRow();

     });

     $('#addcomp').click(function(){

       //creation d'une ligne de section

       AddcompRow();

     });

     $('#addtache').click(function(){

       //creation d'une ligne de section

       AddtachesRow();

     });






   });

   </script>
    <!-- end js include path -->
  </body>

</html>
