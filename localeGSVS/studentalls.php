<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Student.php');
require_once('../controller/functions.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();
$student= new Student();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

if($_SESSION['user']['groupe']==1)
{
  if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
  {
    $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
    $datastat=$user->getStatisById($codeEtabAssigner);
    $tabstat=explode("*",$datastat);

    //nous allons chercher la liste des etablissements du groupe

    $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

    // var_dump($allcodeEtabs);

    $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
    $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
    $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
    $libellesessionencours="";

      if($etablissementType==1||$etablissementType==3)
      {
        if($nbsessionOn>0){
          //recuperer la session en cours
          $sessionencours=$session->getSessionEncours($codeEtabAssigner);
          $tabsessionencours=explode("*",$sessionencours);
          $libellesessionencours=$tabsessionencours[0];
          $sessionencoursid=$tabsessionencours[1];
          $typesessionencours=$tabsessionencours[2];

        }

      }else {
        if($nbsessionOn>0)
        {
          //recuperer la session en cours
          $sessionencours=$session->getSessionEncours($codeEtabAssigner);
          $tabsessionencours=explode("*",$sessionencours);
          $libellesessionencours=$tabsessionencours[0];
          $sessionencoursid=$tabsessionencours[1];
          $typesessionencours=$tabsessionencours[2];
          $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
          $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
          $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
        }
      }



      $allparents=$parents->getAllParentBySchoolCodeEtabs($compteuserid);

      $elevesinscrits=$student->ListeinscritsthisyearEtabs($compteuserid,$libellesessionencours);

      $elevesinscritsG=$student->ListeinscritsthisyearEtabsG($compteuserid,$libellesessionencours);
      $elevesinscritsF=$student->ListeinscritsthisyearEtabsF($compteuserid,$libellesessionencours);

      $nbfilles=count($elevesinscritsF);
      $nbmales=count($elevesinscritsG);

      $elevesinscritsclasses=$student->ListeinscritsthisyearEtabclasses($compteuserid,$libellesessionencours);

      $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);

       // var_dump($elevesinscritsG);

       $_SESSION['user']['session']=$libellesessionencours;

       $_SESSION['user']['codeEtab']=$codeEtabAssigner;


  }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
  {
    // echo 'Bonsoir';
    $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
    $datastat=$user->getStatisById($codeEtabAssigner);
    $tabstat=explode("*",$datastat);
    $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
    $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
    $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
    $libellesessionencours="";

      if($etablissementType==1||$etablissementType==3)
      {
        if($nbsessionOn>0){
          //recuperer la session en cours
          $sessionencours=$session->getSessionEncours($codeEtabAssigner);
          $tabsessionencours=explode("*",$sessionencours);
          $libellesessionencours=$tabsessionencours[0];
          $sessionencoursid=$tabsessionencours[1];
          $typesessionencours=$tabsessionencours[2];

        }

      }else {
        if($nbsessionOn>0)
        {
          //recuperer la session en cours
          $sessionencours=$session->getSessionEncours($codeEtabAssigner);
          $tabsessionencours=explode("*",$sessionencours);
          $libellesessionencours=$tabsessionencours[0];
          $sessionencoursid=$tabsessionencours[1];
          $typesessionencours=$tabsessionencours[2];
          $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
          $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
          $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
        }


        $allparents=$parents->getAllParentBySchoolCode($codeEtabAssigner);

        $elevesinscrits=$student->Listeinscritsthisyear($codeEtabAssigner,$libellesessionencours);
        $elevesinscritsG=$student->ListeinscritsthisyearG($codeEtabAssigner,$libellesessionencours);
        $elevesinscritsF=$student->ListeinscritsthisyearF($codeEtabAssigner,$libellesessionencours);

        $nbfilles=count($elevesinscritsF);
        $nbmales=count($elevesinscritsG);

        $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);

        $_SESSION['user']['session']=$libellesessionencours;

        $_SESSION['user']['codeEtab']=$codeEtabAssigner;


      }

  }
}

$nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
// echo var_dump($elevesinscrits);
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::Listedeseleves ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="studentalls.php"><?php echo L::studMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::Listedeseleves ?></li>
                          </ol>
                      </div>
                  </div>
					<!-- start widget -->

					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['updateparentok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: '<?php echo L::Felicitations ?>',
            text: '<?php echo $_SESSION['user']['updateparentok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['updateparentok']);
                }

                 ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

                  <div class="row">

                 <?php

                 if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
                 {
                   ?>
                   <div class="col-md-12 col-sm-12">

                     <div class="state-overview">

                      <div class="row">

                        <div class="col-lg-6 col-sm-6">
                          <div class="card  card-box">
                                                <div class="card-head">
                                                    <header><?php echo L::CAByEtabs ?></header>
                                                    <div class="tools">
                                           <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                       </div>

                                                </div>
                                                <div class="card-body ">
                                                    <div class="table-scrollable">
                                                        <table class="table" >
                                                            <thead>
                                                                <tr>
                                                                    <th style="color:#3a405b">#</th>
                                                                    <th style="color:#3a405b"><?php echo L::Etabses ?></th>
                                                                    <th style="color:#3a405b"><?php echo L::CA ?></th>

                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                              <?php
                                                              if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
                                                              {
                                                                $i=1;
                                                                $montantCA=0;
                                                                foreach ($allcodeEtabs as $value):
                                                                  ?>
                                                                  <tr>
                                                                      <td style="color:#3a405b"><?php echo $i ?></td>
                                                                      <td style="color:#3a405b"><?php echo $etabs->getEtabNamebyCodeEtab($value->codeEtab_assign) ?></td>
                                                                      <td style="color:#3a405b">
                                                                        <?php
                                                                        $montant=$etabs->getturnoverschool($value->codeEtab_assign,$libellesessionencours);
                                                                      echo prixMill($etabs->getturnoverschool($value->codeEtab_assign,$libellesessionencours));

                                                                      ?>
                                                                    </td>

                                                                  </tr>
                                                                  <?php
                                                                  $montantCA=$montantCA+$montant;
                                                                  $i++;
                                                                endforeach;

                                                                ?>

                                                                <?php
                                                              }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
                                                              {
                                                                ?>

                                                                <?php
                                                              }
                                                               ?>



                                                            </tbody>
                                                            <tfoot>
                                                              <tr>
                                                                <td colspan="2" style="color:#3a405b;" class="left"><?php echo L::TotalCas ?></td>
                                                                <!-- <td></td> -->
                                                                <td style="color:#3a405b;" class="left"><?php echo number_format($montantCA,0, ',', ' '); ?></td>
                                                              </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                        </div>

                     <div class="col-lg-6 col-sm-6">
                       <div class="card  card-box">
                                             <div class="card-head">
                                                 <header><?php echo L::NbstudentbyEtabs ?></header>
                                                 <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
										<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
										<a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>

                                             </div>
                                             <div class="card-body ">
                                                 <div class="table-scrollable">
                                                     <table class="table" >
                                                         <thead>
                                                             <tr>
                                                                 <th style="color:#3a405b">#</th>
                                                                 <th style="color:#3a405b"><?php echo L::Etabses ?></th>
                                                                 <th style="color:#3a405b"><?php echo L::Nbstudents ?></th>

                                                             </tr>
                                                         </thead>
                                                         <tbody>
                                                           <?php
                                                           if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
                                                           {
                                                             $i=1;
                                                             $montantCA=0;
                                                             foreach ($allcodeEtabs as $value):
                                                               ?>
                                                               <tr>
                                                                   <td style="color:#3a405b"><?php echo $i ?></td>
                                                                   <td style="color:#3a405b"><?php echo $etabs->getEtabNamebyCodeEtab($value->codeEtab_assign) ?></td>
                                                                   <td style="color:#3a405b;text-align:center">
                                                                     <?php
                                                                     $montant=$etabs->getstudentinscritsEtabs($value->codeEtab_assign,$libellesessionencours);
                                                                   echo $etabs->getstudentinscritsEtabs($value->codeEtab_assign,$libellesessionencours);

                                                                   ?>
                                                                 </td>

                                                               </tr>
                                                               <?php
                                                               $montantCA=$montantCA+$montant;
                                                               $i++;
                                                             endforeach;

                                                             ?>

                                                             <?php
                                                           }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
                                                           {
                                                             ?>

                                                             <?php
                                                           }
                                                            ?>



                                                         </tbody>
                                                         <tfoot>
                                                           <tr>
                                                             <td colspan="2" style="color:#3a405b;" class="left"><?php echo L::Total ?></td>
                                                             <!-- <td></td> -->
                                                             <td style="color:#3a405b;text-align:center" ><?php echo number_format($montantCA,0, ',', ' '); ?></td>
                                                           </tr>
                                                         </tfoot>
                                                     </table>
                                                 </div>
                                             </div>
                                         </div>
                 </div>
                 <!-- <div class="col-lg-6 col-sm-6">
                   <div class="info-box green">
             <span class="info-box-icon push-bottom"><i class="material-icons">attach_money</i></span>
             <div class="info-box-content">
               <span class="info-box-text"><?php //echo L::RealisedCA ?></span>
               <span class="info-box-number"><?php //echo $montantCA; ?></span>

             </div>

           </div>
              </div> -->

                        <div class="col-lg-6 col-sm-6">
                       <div class="overview-panel orange">
                         <div class="symbol">
                           <i class="material-icons f-left">group</i>
                         </div>
                         <div class="value white">
                           <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php echo $nbfilles;?>"><?php echo $nbfilles;?></p>
                           <p> <a href="#" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::Filles) ?></a> </p>
                         </div>
                       </div>
                     </div>
                         <!-- /.col -->
                         <div class="col-lg-6 col-sm-6">
                       <div class="overview-panel blue">
                         <div class="symbol">
                           <i class="material-icons f-left">school</i>
                         </div>
                         <div class="value white">
                           <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php echo $nbmales;?>"><?php echo $nbmales;?></p>
                           <p> <a href="#" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::GarconsCaps) ?></a> </p>
                         </div>
                       </div>
                     </div>


                       </div>

                     </div>

                   </div>

                   <?php
                 }else {
                   ?>
                   <div class="col-md-12 col-sm-12">

                     <div class="state-overview">

                      <div class="row">



                     <div class="col-lg-6 col-sm-6">
                       <div class="card  card-box">
                                             <div class="card-head">
                                                 <header><?php echo L::CAByEtabs ?></header>
                                                 <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                   <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                   <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>

                                             </div>
                                             <div class="card-body ">
                                                 <div class="table-scrollable">
                                                     <table class="table">
                                                         <thead>
                                                             <tr>
                                                                 <th style="color:#3a405b">#</th>
                                                                 <th style="color:#3a405b"><?php echo L::Etabses ?></th>
                                                                 <th style="color:#3a405b"><?php echo L::CA ?></th>

                                                             </tr>
                                                         </thead>
                                                         <tbody>
                                                           <?php
                                                           if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
                                                           {
                                                             $i=1;
                                                             $montantCA=0;
                                                             foreach ($allcodeEtabs as $value):
                                                               ?>
                                                               <tr>
                                                                   <td style="color:#3a405b"><?php echo $i ?></td>
                                                                   <td style="color:#3a405b"><?php echo $etabs->getEtabNamebyCodeEtab($value->codeEtab_assign) ?></td>
                                                                   <td style="color:#3a405b">
                                                                     <?php
                                                                     $montant=$etabs->getturnoverschool($value->codeEtab_assign,$libellesessionencours);
                                                                   echo prixMill($etabs->getturnoverschool($value->codeEtab_assign,$libellesessionencours));

                                                                   ?>
                                                                 </td>

                                                               </tr>
                                                               <?php
                                                               $montantCA=$montantCA+$montant;
                                                               $i++;
                                                             endforeach;

                                                             ?>

                                                             <?php
                                                           }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
                                                           {
                                                             $i=1;
                                                             $montantCA=0;
                                                             // foreach ($allcodeEtabs as $value):
                                                             ?>
                                                             <tr>
                                                                 <td style="color:#3a405b"><?php echo $i ?></td>
                                                                 <td style="color:#3a405b"><?php echo $etabs->getEtabNamebyCodeEtab($_SESSION['user']['codeEtab']) ?></td>
                                                                 <td style="color:#3a405b">
                                                                   <?php
                                                                   $montant=$etabs->getturnoverschool($_SESSION['user']['codeEtab'],$libellesessionencours);
                                                                 echo prixMill($etabs->getturnoverschool($_SESSION['user']['codeEtab'],$libellesessionencours));

                                                                 ?>
                                                               </td>

                                                             </tr>
                                                             <?php
                                                             $montantCA=$montantCA+$montant;
                                                           //   $i++;
                                                           // endforeach;
                                                           }
                                                            ?>



                                                         </tbody>
                                                     </table>
                                                 </div>
                                             </div>
                                         </div>
                   </div>
                   <div class="col-lg-6 col-sm-6">
                   <div class="info-box green">
                   <span class="info-box-icon push-bottom"><i class="material-icons">attach_money</i></span>
                   <div class="info-box-content">
                   <span class="info-box-text"><?php echo L::RealisedCA ?></span>
                   <span class="info-box-number"><?php echo $montantCA; ?></span>
                   <!-- <div class="progress">
                   <div class="progress-bar" style="width: 45%"></div>
                   </div> -->
                   <!-- <span class="progress-description">
                   <!-- 45% Increase in 28 Days -->
                   <!-- </span>  -->
                   </div>
                   <!-- /.info-box-content -->
                   </div>
                   </div>

                        <div class="col-lg-6 col-sm-6">
                       <div class="overview-panel orange">
                         <div class="symbol">
                           <i class="material-icons f-left">group</i>
                         </div>
                         <div class="value white">
                           <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php echo $nbfilles;?>"><?php echo $nbfilles;?></p>
                           <p> <a href="#" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::Filles) ?></a> </p>
                         </div>
                       </div>
                     </div>
                         <!-- /.col -->
                         <div class="col-lg-6 col-sm-6">
                       <div class="overview-panel blue">
                         <div class="symbol">
                           <i class="material-icons f-left">school</i>
                         </div>
                         <div class="value white">
                           <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php echo $nbmales;?>"><?php echo $nbmales;?></p>
                           <p> <a href="#" style="color:#f8f9fa;text-decoration:none"><?php echo strtoupper(L::GarconsCaps) ?></a> </p>
                         </div>
                       </div>
                     </div>


                       </div>

                     </div>

                   </div>
                   <?php
                 }

                  ?>



                 </div>
                 <div class="row">
                                         <div class="col-md-12">
                                             <div class="tabbable-line">
                                                <ul class="nav nav-pills nav-pills-rose" style="display:none">
                 									<li class="nav-item tab-all"><a class="nav-link active show"
                 										href="#tab1" data-toggle="tab"><?php echo L::List ?></a></li>
                 									<li class="nav-item tab-all"><a class="nav-link" href="#tab2"
                 										data-toggle="tab"><?php echo L::Grille ?></a></li>
                 								</ul>
                                                 <div class="tab-content">
                                                     <div class="tab-pane active fontawesome-demo" id="tab1">
                                                         <div class="row">
                 					                        <div class="col-md-12">
                 					                            <div class="card  card-box">
                 					                                <div class="card-head">
                 					                                    <header></header>
                 					                                    <div class="tools">
                 					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                 						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                 						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                 					                                    </div>
                 					                                </div>
                 					                                <div class="card-body ">

                 					                                  <div class="table-scrollable">
                                                              <?php
                                                              if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
                                                              {
                                                                ?>
                                                                <table class="table table-hover table-checkable order-column full-width" id="example45">
                                                                  <thead>
                                                                    <tr>
                                                                      <th style="width:30%"> <?php echo L::Etabses ?> </th>
                                                                      <th style="width:30%"> <?php echo L::ClassesMenu ?></th>
                                                                      <th style="width:35%"> <?php echo L::NamestudentTab ?> </th>

                                                                      <th class="noExport" style="width:30%"> <?php echo L::Actions ?> </th>
                                                                    </tr>
                                                                  </thead>
                                                                  <tbody>
                                                                    <?php
                                                                    foreach ($elevesinscrits as $value):
                                                                      ?>
                                                                    <tr>
                                                                      <td class="left"><?php echo $etabs->getEtabNamebyCodeEtab($value->codeEtab_classe) ?></td>
                                                                      <td><?php echo utf8_encode(utf8_decode($value->libelle_classe))?></td>
                                                                      <td><?php echo utf8_encode(utf8_decode($value->nom_compte." ".$value->prenom_compte))?></td>
                                                                      <td>
                                                                        <a href="#" class="btn btn-warning btn-xs" title=""> <i class="fa fa-history"></i> </a>
                                                                      </td>

                                                                    </tr>
                                                                      <?php
                                                                    endforeach;
                                                                     ?>


                                                                  </tbody>
                   					                                    </table>
                                                                <?php
                                                              }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
                                                              {
                                                                ?>
                                                                <table class="table table-hover table-checkable order-column full-width" id="example45">
                                                                  <thead>
                                                                    <tr>
                                                                      <th style="width:30%"> <?php echo L::Etabses ?> </th>
                                                                      <th style="width:35%"> <?php echo L::NamestudentTab ?> </th>
                                                                      <th style="width:30%"> <?php echo L::ClassesMenu ?></th>
                                                                      <th class="noExport" style="width:30%"> <?php echo L::Actions ?> </th>
                                                                    </tr>
                                                                  </thead>
                                                                  <tbody>
                                                                    <?php
                                                                    foreach ($elevesinscrits as $value):
                                                                      ?>
                                                                    <tr>
                                                                      <td class="left"><?php echo $etabs->getEtabNamebyCodeEtab($value->codeEtab_classe) ?></td>
                                                                      <td><?php echo utf8_encode(utf8_decode($value->libelle_classe))?></td>
                                                                      <td><?php echo utf8_encode(utf8_decode($value->nom_compte." ".$value->prenom_compte))?></td>
                                                                      <td>
                                                                        <a href="#" class="btn btn-warning btn-xs" title=""> <i class="fa fa-history"></i> </a>
                                                                      </td>

                                                                    </tr>
                                                                      <?php
                                                                    endforeach;
                                                                     ?>


                                                                  </tbody>
                   					                                    </table>
                                                                <?php
                                                              }
                                                               ?>

                 					                                    </div>
                 					                                </div>
                 					                            </div>
                 					                        </div>
                 					                    </div>
                                                     </div>
                                                     <div class="tab-pane" id="tab2">



                                     					<div class="row">
                                                 <?php
                                                 $i=1;
                                                   foreach ($allparents as $value):
                                                   ?>
                 					                        <div class="col-md-4" ondblclick="myFunction(<?php echo $value->id_compte?>)">
                 				                                <div class="card card-box">
                 				                                    <div class="card-body no-padding ">
                 				                                    	<div class="doctor-profile">
                                                                 <?php
                                                                   $lienImg="";
                                                                   if(strlen($value->photo_compte)>0)
                                                                   {
                                                                     $lienImg="../photo/".$value->email_compte."/".$value->photo_compte;
                                                                   }else {
                                                                     $lienImg="../photo/user5.jpg";
                                                                   }
                                                                  ?>
                                                                        <img src="<?php echo $lienImg?>" class="doctor-pic" alt="" style="height:100px; width:90px;">
                 					                                        <div class="profile-usertitle">
                 					                                             <div class="doctor-name"><?php echo $value->nom_compte." ".$value->prenom_compte;?> </div>
                 					                                             <div class="name-center"> <?php echo $value->fonction_compte;?></div>
                 					                                        </div>

                                                                   <p><?php echo $value->email_compte;?></p>
                                                                         <div><p><i class="fa fa-phone"></i><a href="">  <?php echo $value->tel_parent; ?></a></p> </div>
                                                                   <div class="profile-userbuttons">
                                                                        <a href="#" class="btn btn-circle btn-info btn-sm" onclick="modify(<?php echo $value->id_compte ?>)"><i class="fa fa-pencil"></i></a>
                                                                        <a href="#" class="btn btn-circle btn-danger btn-sm" onclick="deleted(<?php echo $value->id_compte?>,'<?php echo $codeEtabAssigner ?>')"><i class="fa fa-trash"></i></a>
                                                                   </div>
                 				                                        </div>
                 				                                    </div>
                 				                                </div>
                 					                        </div>
                                                   <?php
                                                                                    $i++;
                                                                                    endforeach;
                                                                                    ?>


                                     					</div>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
  <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
  <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
  <script src="../cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" ></script>
  	<script src="../cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
  	<script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" ></script>
  	<script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" ></script>
  	<script src="../cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   function SetcodeEtab(codeEtab)
   {
     var etape=3;
     $.ajax({
       url: '../ajax/sessions.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function searchstudentname()
   {
     var codeEtab=$("#Etabs").val();

     if(codeEtab=="ALL")
     {
       var etape=2;
       var sessionEtab="<?php echo $libellesessionencours; ?>";
       var userid="<?php echo $compteuserid ?>";

       $.ajax({
         url: '../ajax/student.php',
         type: 'POST',
         async:false,
         data: 'userid=' +userid+ '&sessionEtab=' +sessionEtab+'&etape=' + etape,
         dataType: 'text',
         success: function (content, statut) {

           $("#studentid").html("");
           $("#studentid").html(content);

         }
       });
     }else {
       //nous allons chercher la liste des élèves inscrits dans cet etablissement cette année scolaire

       var etape=1;
       var sessionEtab="<?php echo $libellesessionencours; ?>";

       $.ajax({
         url: '../ajax/student.php',
         type: 'POST',
         async:false,
         data: 'codeEtab=' +codeEtab+ '&sessionEtab=' +sessionEtab+'&etape=' + etape,
         dataType: 'text',
         success: function (content, statut) {

           $("#studentid").html("");
           $("#studentid").html(content);

         }
       });

     }

   }

   function searchparentnames()
   {
     var codeEtab=$("#Etabs").val();

     if(codeEtab=="ALL")
     {
       var etape=7;
       var userid="<?php echo $compteuserid ?>";
       $.ajax({
                url: '../ajax/parent.php',
                type: 'POST',
                async:false,
                data: 'userid='+userid+'&etape='+etape+'&idcompte='+$("#studentid").val(),
                dataType: 'text',
                success: function (content, statut) {

                  $("#parentname").html("");
                  $("#parentname").html(content);



                }

              });

     }else {

       var etape=4;
       $.ajax({
                url: '../ajax/parent.php',
                type: 'POST',
                async:false,
                data: 'code='+codeEtab+'&etape='+etape+'&idcompte='+$("#studentid").val(),
                dataType: 'text',
                success: function (content, statut) {

                  $("#parentname").html("");
                  $("#parentname").html(content);



                }

              });
     }

   }


   function searchparentname()
   {

     var etape=4;
     var codeEtab="<?php echo $_SESSION['user']['codeEtab']; ?>";

     $.ajax({
              url: '../ajax/parent.php',
              type: 'POST',
              async:false,
              data: 'code='+codeEtab+'&etape='+etape+'&idcompte='+$("#studentid").val(),
              dataType: 'text',
              success: function (content, statut) {

                $("#parentname").html("");
                $("#parentname").html(content);

                //nous allons chercher la liste des admins local de cet etablissement

              /*  $.ajax({
                         url: '../ajax/localadmin.php',
                         type: 'POST',
                         async:false,
                         data: 'code='+ $("#codeetab").val()+'&etape='+etape,
                         dataType: 'text',
                         success: function (response, statut) {


                           $("#adminlo").html("");
                           $("#adminlo").html(response);
                         }
                       });*/

              }

            });

   }
   $(document).ready(function() {

     // $('#example5').DataTable( {
     //     "scrollX": true
     //
     // } );

     $('#example45').DataTable( {

       "scrollX": true,
       "language": {
           "lengthMenu": "_MENU_  ",
           "zeroRecords": "Aucune correspondance",
           "info": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
           "infoEmpty": "Aucun enregistrement disponible",
           "infoFiltered": "(filtered from _MAX_ total records)",
           "sEmptyTable":"Aucune donnée disponible dans le tableau",
            "sSearch":"Rechercher :",
            "oPaginate": {
       "sFirst":    "Premier",
       "sLast":     "Dernier",
       "sNext":     "Suivant",
       "sPrevious": "Précédent"
     }
     },

         dom: 'Bfrtip',
         buttons: [
             // 'copyHtml5',

             // 'excelHtml5',
             {
               extend: 'excelHtml5',
               title: 'Data export',
               exportOptions: {
                             columns: "thead th:not(.noExport)"
                         }
             }

             // 'csvHtml5',
             // 'pdfHtml5'
         ]
     } );

     $("#codeetab").select2();

     $("#Etabs").select2();


     $("#libetab").select2();

     $("#parentname").select2();

     $("#studentid").select2();

     $("#FormSearch1").validate({

            errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
          },
          highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger')
            $(element).addClass('form-control-danger')
          },
          success: function (e) {
                $(e).closest('.control-group').removeClass('error').addClass('info');
                $(e).remove();
            },
              rules:{
                studentid:"required"
              },
              messages:{
                studentid:"<?php echo L::PleaseSelectOneStudent ?>"
              }
     });

     $("#FormSearch").validate({

            errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
          },
          highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger')
            $(element).addClass('form-control-danger')
          },
          success: function (e) {
                $(e).closest('.control-group').removeClass('error').addClass('info');
                $(e).remove();
            },
              rules:{
                studentid:"required"
              },
              messages:{
                studentid:"<?php echo L::PleaseSelectOneStudent ?>"
              }
     });




   });

   </script>
    <!-- end js include path -->
  </body>

</html>
