<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Matiere.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$subject=new Matiere();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

if(isset($_GET['classe']))
{
$classeEtab=$_GET['classe'];
}

if(isset($_POST['classe']))
{
  $classeEtab=$_POST['classe'];
}

if(isset($_GET['studetid']))
{
  $idstudent=$_GET['studetid'];
}

if(isset($_POST['idcompte']))
{
  $idstudent=$_POST['idcompte'];
}



if(isset($_GET['matiere']))
{
  $matclasse=$_GET['matiere'];
}

if(isset($_POST['matclasse']))
{
  $matclasse=$_POST['matclasse'];
}


  if($_SESSION['user']['groupe']==1)
  {
    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
    {
      $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);

      //nous allons chercher la liste des etablissements du groupe

      $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
        }

        if(strlen($_SESSION['user']['codeEtab'])>0)
        {
          if(isset($_GET['codeEtab']))
          {
            $codeEtab=$_GET['codeEtab'];
              $_SESSION['user']['codeEtab']=$codeEtab;
          }

          if(isset($_POST['codeEtab']))
          {
            $codeEtab=$_POST['codeEtab'];
              $_SESSION['user']['codeEtab']=$codeEtab;
          }
        }else {
          $_SESSION['user']['codeEtab']=$codeEtabAssigner;
        }

        $_SESSION['user']['session']=$libellesessionencours;





         // var_dump($allcodeEtabs);

        $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

        $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


    }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
    {
      $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);
      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
          $_SESSION['user']['session']=$libellesessionencours;

          if(strlen($_SESSION['user']['codeEtab'])>0)
          {
            if(isset($_GET['codeEtab']))
            {
              $codeEtab=$_GET['codeEtab'];
                $_SESSION['user']['codeEtab']=$codeEtab;
            }

            if(isset($_POST['codeEtab']))
            {
              $codeEtab=$_POST['codeEtab'];
              $_SESSION['user']['codeEtab']=$codeEtab;
            }
          }else {
            $_SESSION['user']['codeEtab']=$codeEtabAssigner;
          }

          $notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

          // $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
          $classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);
          $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);
        }

    }
  }

  $schoolmatieres=$subject->getAllSubjectOfClasses($classeEtab,$_SESSION['user']['codeEtab'],$libellesessionencours);

  // var_dump($allcodeEtabs);
  $nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
// echo $libellesessionencours;
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::EvolutiondesNotes ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i><a href="#">Eleves</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::EvolutiondesNotes ?></li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


                 <div class="row">



                   <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="card card-topline-green">
                          <div class="card-head">
                              <header></header>
                              <div class="tools">
                                  <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                   <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                   <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                              </div>
                          </div>
                          <div class="card-body ">
                            <form method="post" id="recapnotes" action="performancevolution.php" >
                                <div class="row">

                              <div class="col-md-6 col-sm-6">
                              <!-- text input -->
                              <div class="form-group" style="margin-top:8px;">
                                  <label><?php echo L::MatiereMenusingle ?></label>
                                  <input type="hidden" id="matclasse" name="matclasse" class="form-control" value="<?php echo $matclasse; ?>">
                                  <input type="text" id="libellematiereEtab" class="form-control" name="libellematiereEtab" value="<?php echo $subject->getMatiereLibelleByIdMat($matclasse,$_SESSION['user']['codeEtab']); ?>">

                                  <input type="hidden" id="notetype" name="notetype" class="form-control" value="1">
                              </div>


                          </div>


                          <div class="col-md-6 col-sm-6">
                          <!-- text input -->
                          <div class="form-group" style="margin-top:8px;">
                              <label><?php echo L::Period ?></label>
                              <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                              <select class="form-control input-height" id="libsemes" name="libsemes" style="width:100%;" onchange="searchDesignation()">
                                  <option value="" selected ><?php echo L::SelectedPeriod ?></option>
                                  <?php
                                    $i=1;
                                    foreach ($typesemestre as $value):
                                    ?>
                                    <option value="<?php echo $value->id_semes?>"><?php echo utf8_encode(utf8_decode($value->libelle_semes)) ?></option>

                                    <?php
                                         $i++;
                                         endforeach;
                                     ?>

                              </select>
                          </div>


                      </div>

                      <div class="col-md-6 col-sm-6">
                      <!-- text input -->
                      <div class="form-group" style="margin-top:8px;">
                          <label><?php echo L::Evaluation ?></label>
                          <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                          <select class="form-control input-height" id="libctrl" name="libctrl" style="width:100%;">
                              <option value="" selected ><?php echo L::EvalSelect ?></option>

                          </select>
                      </div>


                  </div>

                              <div class="col-md-3 col-sm-3">
                              <!-- text input -->
                              <!--div class="form-group">
                                  <label style="margin-top:3px;">Date</label>
                                  <input type="text" id="datepre" name ="datepre" class="floating-label mdl-textfield__input"  placeholder="Date présence">
                                  <input type="hidden" name="search" id="search" />
                              </div-->
                              <input type="hidden" name="listdesignation" id="listdesignation" value="">
                              <input type="hidden" name="nbdesignation" id="nbdesignation" value="">
                              <input type="hidden" name="classe" id="classe" value="<?php echo $classeEtab ?>">
                              <input type="hidden" name="idcompte" id="idcompte"  value="<?php echo $idstudent ?>">
                              <input type="hidden" name="search" id="search" />
                              <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $_SESSION['user']['codeEtab']; ?>" />
                              <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                              <button type="submit" class="btn btn-primary btn-md" style="width:200px;height:35px;margin-top:40px;text-align:center;"><?php echo L::DescribNotes ?></button>


                          </div>


                                </div>


                            </form>
                          </div>
                      </div>
                               </div>

                       </div>

                       <div class="row">

                         <?php
                         if(isset($_POST['search']))
                         {
                           //nous devons faire une recherche

                            if(isset($_POST['notetype'])&& $_POST['notetype']==1)
                            {
                              //controle
                              //echo "controle";
                              //recuperation des variables

                              if(isset($_POST['libctrl']) && strlen($_POST['libctrl'])>0)
                              {
                                //note d'un controle précis pour un semestre precis

                                //recuperation des variables completes

                                $matclasse=htmlspecialchars(addslashes($_POST['matclasse']));
                                $notetype=htmlspecialchars(addslashes($_POST['notetype']));
                                $libsemes=htmlspecialchars(addslashes($_POST['libsemes']));
                                $libctrl=htmlspecialchars(addslashes($_POST['libctrl']));
                                $datalibctrl=explode("-",$libctrl);
                                $controleid=$datalibctrl[0];
                                $teatcherid=$datalibctrl[2];
                                $listdesignation=htmlspecialchars(addslashes($_POST['listdesignation']));
                                $nbdesignation=htmlspecialchars(addslashes($_POST['nbdesignation']));
                                $classe=htmlspecialchars(addslashes($_POST['classe']));
                                $idcompte=htmlspecialchars(addslashes($_POST['idcompte']));

                                $codeEtab=htmlspecialchars(addslashes($_SESSION['user']['codeEtab']));
                                $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

                                $datanotesStudent=$etabs->getPreciseNotesControleStudent($idcompte,$controleid,$notetype,$teatcherid,$matclasse,$classe,$_SESSION['user']['codeEtab'],$libellesession,$libsemes);
                              }else {
                                //les notes de tous les controles d'un semestre

                                //recuperation des variables
                                $matclasse=htmlspecialchars(addslashes($_POST['matclasse']));
                                $notetype=htmlspecialchars(addslashes($_POST['notetype']));
                                $libsemes=htmlspecialchars(addslashes($_POST['libsemes']));
                                $listdesignation=htmlspecialchars(addslashes($_POST['listdesignation']));
                                $listdesignation=substr($listdesignation, 0, -1);
                                $nbdesignation=htmlspecialchars(addslashes($_POST['nbdesignation']));
                                $classe=htmlspecialchars(addslashes($_POST['classe']));
                                $idcompte=htmlspecialchars(addslashes($_POST['idcompte']));
                                $codeEtab=htmlspecialchars(addslashes($_SESSION['user']['codeEtab']));
                                $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

                                //$datanotesStudent=$etabs->getNoteStudentAllByTypeAndSession($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession,$libsemes);
                                $datanotesStudent=$etabs->getControleNotesWithoutLibctrl($idcompte,$listdesignation,$notetype,$matclasse,$classe,$_SESSION['user']['codeEtab'],$libellesession,$libsemes);
                              }

                            }else if(isset($_POST['notetype'])&& $_POST['notetype']==2)
                            {
                              //examen
                              if(isset($_POST['libctrl']) && strlen($_POST['libctrl'])>0)
                              {
                                //note d'un examen précis pour un semestre precis

                                //recuperation des variables completes

                                $matclasse=htmlspecialchars(addslashes($_POST['matclasse']));
                                $notetype=htmlspecialchars(addslashes($_POST['notetype']));
                                $libsemes=htmlspecialchars(addslashes($_POST['libsemes']));
                                $libctrl=htmlspecialchars(addslashes($_POST['libctrl']));
                                $datalibctrl=explode("-",$libctrl);
                                $examenid=$datalibctrl[0];
                                $teatcherid=$datalibctrl[2];
                                $listdesignation=htmlspecialchars(addslashes($_POST['listdesignation']));
                                $nbdesignation=htmlspecialchars(addslashes($_POST['nbdesignation']));
                                $classe=htmlspecialchars(addslashes($_POST['classe']));
                                $idcompte=htmlspecialchars(addslashes($_POST['idcompte']));

                                $codeEtab=htmlspecialchars(addslashes($_SESSION['user']['codeEtab']));
                                $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

                                $datanotesStudent=$etabs->getPreciseNotesExamenStudent($idcompte,$examenid,$notetype,$teatcherid,$matclasse,$classe,$_SESSION['user']['codeEtab'],$libellesession,$libsemes);
                              }else {
                                //les notes de tous les examens d'un semestre

                                //recuperation des variables
                                $matclasse=htmlspecialchars(addslashes($_POST['matclasse']));
                                $notetype=htmlspecialchars(addslashes($_POST['notetype']));
                                $libsemes=htmlspecialchars(addslashes($_POST['libsemes']));
                                $listdesignation=htmlspecialchars(addslashes($_POST['listdesignation']));
                                $listdesignation=substr($listdesignation, 0, -1);
                                $nbdesignation=htmlspecialchars(addslashes($_POST['nbdesignation']));
                                $classe=htmlspecialchars(addslashes($_POST['classe']));
                                $idcompte=htmlspecialchars(addslashes($_POST['idcompte']));
                                $codeEtab=htmlspecialchars(addslashes($_SESSION['user']['codeEtab']));
                                $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

                                //$datanotesStudent=$etabs->getNoteStudentAllByTypeAndSessionExam($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession,$libsemes);
                                  $datanotesStudent=$etabs->getExamenNotesWithoutLibctrl($idcompte,$listdesignation,$notetype,$matclasse,$classe,$_SESSION['user']['codeEtab'],$libellesession,$libsemes);
                              }
                            }

                            // var_dump($datanotesStudent);
                         }
                          ?>

                       </div>
                       <div class="row">

                         <?php

                         if(isset($_POST['search']))
                         {
                          $nbligne=count($datanotesStudent);

                          if($nbligne==0)
                          {
                            ?>
                            <div class="col-md-12 col-sm-12">
                              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <?php echo L::NoNotes ?>
                              <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                               </a>
                              </div>
                            </div>

                            <?php

                          }else if($nbligne>0)
                          {
                            ?>
                            <div class="col-md-12 col-sm-12">
                           <div class="panel tab-border card-box">
                               <header class="panel-heading panel-heading-gray custom-tab ">
                                   <ul class="nav nav-tabs">
                                       <li class="nav-item"><a href="#home" data-toggle="tab" class="active"><i class="fa fa-list"></i> <?php echo L::ListNotesLib ?></a>
                                       </li>
                                       <li class="nav-item"><a href="#about" data-toggle="tab"><i class="fa fa-bar-chart-o"></i> <?php echo L::Evolution ?></a>
                                       </li>
                                       <!--li class="nav-item"><a href="#profile" data-toggle="tab">Profile</a>
                                       </li>
                                       <li class="nav-item"><a href="#contact" data-toggle="tab">Contact</a>
                                       </li-->
                                   </ul>
                               </header>
                               <div class="panel-body">
                                   <div class="tab-content">
                                       <div class="tab-pane active" id="home">
                                         <div class="col-md-12">
                           <div class="card card-box">
                               <div class="card-head">
                                   <header></header>
                                   <div class="tools">
                                       <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                     <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                     <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                   </div>
                               </div>
                               <div class="card-body ">
                                   <table id="saveStage" class="display" style="width:100%;">
                                       <thead>
                                           <tr>
                                               <th>
                                                 <?php
                                                  if(isset($_POST['notetype'])&& $_POST['notetype']==1)
                                                  {
                                                    echo "Controles";
                                                  }else if(isset($_POST['notetype'])&& $_POST['notetype']==2)
                                                  {
                                                    echo "Examens";
                                                  }
                                                  ?>
                                               </th>

                                               <th><?php echo L::NotesMax ?></th>
                                               <th><?php echo L::NoteObservationplural ?></th>

                                           </tr>
                                       </thead>
                                       <tbody>
                                         <?php
                                         $tableauNotesLibelle=array();
                                         $tableauNotes=array();
                                         $concatlibnotes="";
                                         $concatnotes="";
                                         $i=1;
                                         foreach ($datanotesStudent as $value):
                                          ?>
                                           <tr>
                                               <td><?php echo $value->libelle_ctrl; ?></td>
                                               <td><?php echo $value->valeur_notes; ?></td>
                                               <td><?php echo $value->obser_notes; ?></td>

                                               <!--input type="hidden" name="notes<?php //echo $i;?>" name="notes<?php //echo $i;?>" value="<?php //echo $value->valeur_notes; ?>"-->
                                           </tr>
                                           <?php
                                           if(isset($_POST['notetype'])&& $_POST['notetype']==1)
                                           {
                                              $concatlibnotes=$concatlibnotes.'"'.$value->libelle_ctrl.'",';
                                              $tableauNotesLibelle[$i]=$value->libelle_ctrl;

                                              $concatnotes=$concatnotes.$value->valeur_notes.",";
                                              $tableauNotes[$i]=$value->valeur_notes;

                                              ?>
                                              <input type="hidden" name="libelle<?php echo $i  ?>" value="<?php echo $value->libelle_ctrl; ?>">
                                              <?php

                                           }else if(isset($_POST['notetype'])&& $_POST['notetype']==2)
                                           {
                                              $concatlibnotes=$concatlibnotes.$value->libelle_exam.",";
                                              $tableauNotesLibelle[$i]=$value->libelle_exam;
                                              $concatnotes=$concatnotes.$value->valeur_notes.",";
                                              $tableauNotes[$i]=$value->valeur_notes;

                                              ?>
                                              <input type="hidden" name="libelle<?php echo $i  ?>" value="<?php echo $value->libelle_exam; ?>">
                                              <?php
                                           }


                                           $i++;
                                         endforeach;


                                          $concatlibnotes=substr($concatlibnotes, 0, -1);
                                          $concatnotes=substr($concatnotes, 0, -1);

                                          //var_dump($tableauNotesLibelle);
                                            ?>
                                         </tbody>
                                   </table>
                                   <input type="hidden" name="labelreport" id="labelreport" value='<?php echo "[".$concatlibnotes."]"; ?>'>
                                   <input type="hidden" name="notesreport" id="notesreport" value="<?php echo $concatnotes; ?>">
                                   <input type="hidden" name="totalnotes" id="totalnotes" value="<?php echo $i; ?>">

                               </div>
                           </div>
                       </div>
                                       </div>
                                       <div class="tab-pane" id="about">
                                         <div class="col-md-12">
                           <div class="card  card-box">
                               <div class="card-head">
                                   <header></header>
                                   <div class="tools">
                                       <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                     <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                     <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                   </div>
                               </div>
                               <div class="card-body " id="chartjs_bar_parent">
                                    <div class="row">
                                        <canvas id="notes_bar" ></canvas>
                                    </div>
                                </div>
                           </div>
                       </div>
                                       </div>

                                   </div>
                               </div>
                           </div>
                       </div>
                            <?php
                          }
                        }
                          ?>

                       </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 	<script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
    <script src="../assets2/js/pages/table/table_data.js" ></script>
    <!-- Common js-->
    <!--Chart JS-->
    <script src="../assets2/plugins/chart-js/Chart.bundle.js" ></script>
    <script src="../assets2/plugins/chart-js/utils.js" ></script>
    <script src="../assets2/js/pages/chart/chartjs/chartjs-data.js" ></script>
     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   function SetcodeEtab(codeEtab)
   {
     var etape=3;
     $.ajax({
       url: '../ajax/sessions.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

    // $("#matclasse").select2();
    // $("#notetype").select2();
    $("#libctrl").select2();
    $("#libsemes").select2();

    function refreshing()
    {

    }

    function searchDesignation()
    {
      var matiere=$("#matclasse").val();
      var notetype=$("#notetype").val();
      var semestre=$("#libsemes").val();
      var classe="<?php echo $classeEtab; ?>";
      var idcompte="<?php echo $idstudent; ?>";
      var codeEtab="<?php echo $_SESSION['user']['codeEtab'] ?>";
      var session="<?php echo $libellesessionencours; ?>";

      if(notetype==1)
      {
      //controle
      var etape=3;

      $.ajax({

           url: '../ajax/controle.php',
           type: 'POST',
           async:true,
           data: 'idcompte=' + idcompte+ '&etape=' + etape+'&classe='+classe+'&semestre='+semestre+'&notetype='+notetype+'&matiere='+matiere+'&codeEtab='+codeEtab+'&session='+session,
           dataType: 'text',
           success: function (content, statut) {

             var tabcontent=content.split("*");

             var nbcontrole=tabcontent[0];

             if(nbcontrole>0)
             {
               var contener=tabcontent[2];
               var listcontroleid=tabcontent[1];

               $("#libctrl").html("");

               $("#libctrl").html(contener);

               $("#nbdesignation").val(nbcontrole);

               $("#listdesignation").val(listcontroleid);


             }else if(nbcontrole==0)
             {
               var contener=tabcontent[2];
               var listcontroleid=tabcontent[1];


               $("#libctrl").html("");

               $("#libctrl").html(contener);

               $("#nbdesignation").val(nbcontrole);

               $("#listdesignation").val(listcontroleid);


               //

             }


           }
         });



      }else if(notetype==2)
      {
        //examen

        var etape=6;

        $.ajax({

             url: '../ajax/examen.php',
             type: 'POST',
             async:true,
             data: 'idcompte=' + idcompte+ '&etape=' + etape+'&classe='+classe+'&semestre='+semestre+'&notetype='+notetype+'&matiere='+matiere+'&codeEtab='+codeEtab+'&session='+session,
             dataType: 'text',
             success: function (content, statut) {

               var tabcontent=content.split("*");

               var nbcontrole=tabcontent[0];

               if(nbcontrole>0)
               {
                 var contener=tabcontent[2];
                 var listcontroleid=tabcontent[1];

                 $("#libctrl").html("");

                 $("#libctrl").html(contener);

                 $("#nbdesignation").val(nbcontrole);

                 $("#listdesignation").val(listcontroleid);


               }else if(nbcontrole==0)
               {
                 var contener=tabcontent[2];
                 var listcontroleid=tabcontent[1];


                 $("#libctrl").html("");

                 $("#libctrl").html(contener);

                 $("#nbdesignation").val(nbcontrole);

                 $("#listdesignation").val(listcontroleid);


                 //

               }


             }
           });
      }

    }

   $(document).ready(function() {

     $("#recapnotes").validate({
       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
      },
      success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },
       rules:{
          matclasse:"required",
          notetype:"required",
          libsemes:"required"
       },
       messages: {
         matclasse:"<?php echo L::PleaseselectSubjects ?>",
         notetype:"<?php echo L::PleaseSelectnotetyperequired ?>",
         libsemes:"<?php echo L::PleaseSelectPeriodes ?>"
       },
       submitHandler: function(form) {
         form.submit();
       }

     });

     var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
       var color = Chart.helpers.color;

       var detailsLabelsName='<?php echo "[".@$concatlibnotes."]";  ?>';
       var barChartData = {
           labels:[ <?php
                            for($j=1;$j<$i;$j++)
                            {
                              echo '"'.@$tableauNotesLibelle[$j].'",';
                            }
            ?>],
           datasets: [{
             label: '<?php echo L::NotesObtenues ?>',
             backgroundColor: color(window.chartColors.green).alpha(0.5).rgbString(),
             borderColor: window.chartColors.green,
             borderWidth: 1,
               data: [
                    <?php

                    for($j=1;$j<$i;$j++)
                    {
                      echo '"'.@$tableauNotes[$j].'",';
                    }
                     ?>
               ]
           }]

       };
           var ctx = document.getElementById("notes_bar").getContext("2d");
           window.myBar = new Chart(ctx, {
               type: 'bar',
               data: barChartData,
               options: {
                   responsive: true,
                   legend: {
                       position: 'top',
                   },
                   title: {
                       display: true,
                       text: '<?php echo L::EvolutiondesNotes ?>'
                   },
                   scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 2,
                        max: 20

                    }
                }]
            }
               }
           });

   });

   </script>
    <!-- end js include path -->
  </body>

</html>
