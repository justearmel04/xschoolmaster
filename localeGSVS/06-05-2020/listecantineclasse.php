<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Teatcher.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');
require_once('../controller/functions.php');


$codeEtab=$_GET['codeEtab'] ;
$idClasse=$_GET['classe'];
$session=$_GET['session'];
$mois=$_GET['mois'];

//
$etabs=new Etab();
$student=new Student();
$classe=new Classe();
$parentX=new ParentX();
$matieres=new Matiere();
$dataday=$etabs->getAllweeks();

$libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
$libelleclasse=$classe->getInfosofclassesbyId($idClasse,$session);
$logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
$etablissementType=$etabs->DetermineTypeEtab($codeEtab);


$students=$student->getAllStudentOfClassesId($idClasse,$session);

//nous allons faire nos différents calculs

//le nombre d'eleve de cette classe

$nbclassestudents=$classe->getAllStudentOfThisClassesNb($idClasse,$session);

$libellemotif="CANTINES";

$nbadmis=$student->getNumberStudentversementcantineByMonth($codeEtab,$idClasse,$session,$libellemotif,$mois);

$nbnonadmis=$nbclassestudents-$nbadmis;

//le nombre de fille affecté

$nbaffectefilles=$classe->getAllStudentFilleAffectOfThisClassesNb($idClasse,$session);

//nombre de fille non Affecté

$nbnonaffectefilles=$classe->getAllStudentFilleNAffectOfThisClassesNb($idClasse,$session);

//nombre de mec affectés

$nbaffectemecs=$classe->getAllStudentMecAffectOfThisClassesNb($idClasse,$session);

//nombre de mec non affectés

$nbnonaffectemecs=$classe->getAllStudentMecNAffectOfThisClassesNb($idClasse,$session);

//nombre de fille redoublante

$nbredoubfilles=$classe->getAllStudentFilleRedtOfThisClassesNb($idClasse,$session);

//nombre non redoublante fille

$nbnredoubfilles=$classe->getAllStudentFilleNRedtOfThisClassesNb($idClasse,$session);

//nombre de mec redoublant

$nbredoubmecs=$classe->getAllStudentMecRedtOfThisClassesNb($idClasse,$session);

//nombre non redoublant mec

$nbnredoubmecs=$classe->getAllStudentMecNRedtOfThisClassesNb($idClasse,$session);

$allLibellesHours=$etabs->getHoursAllLibs($codeEtab,$session);

$allteatchers=$parentX->getAllTeatcherOfThisClasseSchool($codeEtab,$idClasse);

$listeclassecantine=$student->getAllCantinesStudentclasseByMonth($codeEtab,$session,$idClasse,$mois);


require('fpdf/fpdf.php');

class PDF extends FPDF
{
function Footer()
{
    // Positionnement à 1,5 cm du bas
    $this->SetY(-15);
    // Police Arial italique 8
    $this->SetFont('Arial','I',8);
    // Numéro et nombre de pages
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    // $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
}

// Tableau simple
function BasicTable($header, $data)
{
    // En-tête
    foreach($header as $col)
        $this->Cell(40,7,$col,1);
    $this->Ln();
    // Données
    foreach($data as $row)
    {
        foreach($row as $col)
            $this->Cell(40,6,$col,1);
        $this->Ln();
    }
}
}

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->SetMargins(3,0);
$pdf->SetFont('Times','B',  16);
$pdf->AddPage("P");
$pdf->Ln(10);
$pdf->Image("../logo_etab/".$codeEtab."/".$logoEtab,5,5,25,25);
$pdf->Ln(18);
$pdf -> SetX(20);
$pdf-> SetFont('Times','B',  15);
// $pdf->Cell(176,5,$libelleEtab,0,0,'C');
$pdf->Ln(8);
$pdf -> SetX(20);
$pdf-> SetFont('Times','',  13);
$pdf->Cell(176,5,'LISTE DES ELEVES ADMIS A LA CANTINE MOIS DE '. $mois,0,0,'C');
$pdf->Ln(12);
$pdf -> SetX(20);
$pdf->Cell(176,5,$libelleclasse,0,0,'C');
$pdf->Ln(8);
// $pdf -> SetX(60);
// $pdf-> SetFont('Times','',  11);
// $pdf->Cell(176,5,'Annees scolaire : 2019-2020',0,0,'C');
$pdf->Ln(10);
// $pdf -> SetX(20);
// $pdf-> SetFont('Times','',  11);
// $pdf->Cell(176,5,$libelleclasse,0,0,'C');
// $pdf->Ln(20);

$pdf->SetFillColor(230,230,0);
$pdf->SetLineWidth(.3);
$pdf->SetFont('Times','B',12);
$pdf->Cell(10,8,'N',1,0,'C');
$pdf->Cell(50,8,'Matricule',1,0,'C');
$pdf->Cell(140,8,'Nom & prenoms',1,0,'C');
$pdf->Ln();
$pdf->SetFont('Times','B',12);
$pdf->SetFillColor(96,96,96);
$tabstudentAdmis=array();
$i=1;
$j=0;
foreach ($listeclassecantine as $value):
$pdf->Cell(10,8,$i,1,0,'C');
$pdf->Cell(50,8,$value->matricule_eleve,1,0,'C');
$pdf->Cell(140,8,$value->nom_eleve.' '.$value->prenom_eleve,1,0,'C');

$tabstudentAdmis[$j]=$value->idcompte_eleve;
$pdf->Ln();
$i++;
$j++;
endforeach;

$pdf->Ln(15);
$pdf -> SetX(10);
$pdf->SetFillColor(230,230,0);
$pdf->SetLineWidth(.3);
$pdf->SetFont('Times','B',11);

$pdf->Cell(50,8,'Nbre total classe',1,0,'C');
$pdf->Cell(30,8,$nbclassestudents,1,0,'C');
$pdf->Ln();
$pdf -> SetX(10);
$pdf->Cell(50,8,'Nbre admis cantine',1,0,'C');
$pdf->Cell(30,8,$nbadmis,1,0,'C');
$pdf->Ln();
$pdf -> SetX(10);
$pdf->Cell(50,8,'Nbre non admis cantine',1,0,'C');
$pdf->Cell(30,8,$nbnonadmis,1,0,'C');
$pdf->Ln();

if($nbnonadmis>0)
{

  $pdf->AddPage("P");
  $pdf->Ln(10);
  $pdf->Image("../logo_etab/".$codeEtab."/".$logoEtab,5,5,25,25);
  $pdf->Ln(18);
  $pdf -> SetX(20);
  $pdf-> SetFont('Times','B',  15);
  // $pdf->Cell(176,5,$libelleEtab,0,0,'C');
  $pdf->Ln(8);
  $pdf -> SetX(20);
  $pdf-> SetFont('Times','',  13);
  $pdf->Cell(176,5,'LISTE DES ELEVES NON ADMIS A LA CANTINE MOIS DE '. $mois,0,0,'C');
  $pdf->Ln(12);
  $pdf -> SetX(20);
  $pdf->Cell(176,5,$libelleclasse,0,0,'C');
  $pdf->Ln(8);
  // $pdf -> SetX(60);
  // $pdf-> SetFont('Times','',  11);
  // $pdf->Cell(176,5,'Annees scolaire : 2019-2020',0,0,'C');
  $pdf->Ln(10);
  // $pdf -> SetX(20);
  // $pdf-> SetFont('Times','',  11);
  // $pdf->Cell(176,5,$libelleclasse,0,0,'C');
  // $pdf->Ln(20);

  $pdf->SetFillColor(230,230,0);
  $pdf->SetLineWidth(.3);
  $pdf->SetFont('Times','B',12);
  $pdf->Cell(10,8,'N',1,0,'C');
  $pdf->Cell(50,8,'Matricule',1,0,'C');
  $pdf->Cell(140,8,'Nom & prenoms',1,0,'C');
  $pdf->Ln();
  $pdf->SetFont('Times','B',12);
  $pdf->SetFillColor(96,96,96);

  $i=1;
  foreach ($students as $value):

    if (in_array($value->idcompte_eleve,$tabstudentAdmis))
    {

    }else {
      $pdf->Cell(10,8,$i,1,0,'C');
      $pdf->Cell(50,8,$value->matricule_eleve,1,0,'C');
      $pdf->Cell(140,8,$value->nom_eleve.' '.$value->prenom_eleve,1,0,'C');
    }



  $i++;
  endforeach;

  $pdf->Ln(15);
  $pdf -> SetX(10);
  $pdf->SetFillColor(230,230,0);
  $pdf->SetLineWidth(.3);
  $pdf->SetFont('Times','B',11);

  $pdf->Cell(50,8,'Nbre total classe',1,0,'C');
  $pdf->Cell(30,8,$nbclassestudents,1,0,'C');
  $pdf->Ln();
  $pdf -> SetX(10);
  $pdf->Cell(50,8,'Nbre admis cantine',1,0,'C');
  $pdf->Cell(30,8,$nbadmis,1,0,'C');
  $pdf->Ln();
  $pdf -> SetX(10);
  $pdf->Cell(50,8,'Nbre non admis cantine',1,0,'C');
  $pdf->Cell(30,8,$nbnonadmis,1,0,'C');
  $pdf->Ln();
}



$pdf->Output();



 ?>
