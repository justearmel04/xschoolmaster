<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllClassesbyschoolCode($codeEtabAssigner);
$notifications=$etabs->getAllNotificationbySchoolCode($codeEtabAssigner);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
  <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
<!--bootstrap -->
<!--bootstrap -->
<link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<!-- Material Design Lite CSS -->
<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
<link href="../assets2/css/material_style.css" rel="stylesheet">
<!-- Theme Styles -->
  <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
<link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
<link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Trimestres / Semestres</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Trimestres / Semestres</li>
                            </ol>
                        </div>
                    </div>
                    <?php

                          if(isset($_SESSION['user']['Updateadminok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: 'Félicitations',
                      text: '<?php echo $_SESSION['user']['Updateadminok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['Updateadminok']);
                          }

                           ?>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


          <div class="row">

            <div class="col-md-12 col-sm-12">
                           <div class="panel tab-border card-box">
                               <header class="panel-heading panel-heading-gray custom-tab ">
                                   <ul class="nav nav-tabs">
                                       <li class="nav-item"><a href="#home" data-toggle="tab" class="active">Liste Trimestres / Semestres</a>
                                       </li>
                                       <li class="nav-item"><a href="#about" data-toggle="tab">Ajouter Trimestre / Semestre</a>
                                       </li>

                                   </ul>
                               </header>
                               <div class="panel-body">
                                   <div class="tab-content">
                                       <div class="tab-pane active" id="home">
                                         <div class="col-md-12">
                           <div class="card  card-box">

                               <div class="card-body ">

                                   <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                       <thead>
                                           <tr>

                                               <th> Id </th>
                                               <th> Libellé Notification </th>
                                               <th> Classe(s)</th>
                                               <th> Destinataire(s)</th>
                                               <th> Type envoi</th>
                                               <th> Statut</th>
                                               <th> <?php echo L::Actions?> </th>
                                           </tr>
                                       </thead>
                                       <tbody>
                                         <?php
                                         $i=1;
                                         foreach ($notifications as $value):
                                          ?>
                                           <tr class="odd gradeX">

                                               <td> <?php echo $i ?></td>
                                               <td>
                                                   <a href="#"> <?php echo $value->libelle_notif; ?> </a>
                                               </td>
                                               <td>
                                                 <?php
                                                  $dataclasse=$value->classes_notif;
                                                  $tabdataclasse=explode("-",$dataclasse);
                                                  $nbtabdataclasse=count($tabdataclasse);
                                                  $limitdataclasse=$nbtabdataclasse-1;
                                                  // echo $limitdataclasse;

                                                  for($z=0;$z<$limitdataclasse;$z++)
                                                  {
                                                    ?>

                                                      <span class="label label-sm label-success" style=""> <?php echo $classe->getInfosofclassesbyId($tabdataclasse[$z],$libellesessionencours); ?> </span>


                                                   <?php

                                                  }
                                                  ?>

                                               </td>
                                               <td>
                                                 <?php
                                                 $datadestinataire=$value->destinataires_notif;
                                                 $tabdatadestinataires=explode("-",$datadestinataire);
                                                 $nbtabdatadestinataires=count($tabdatadestinataires);
                                                 $limitdatadestinataires=$nbtabdatadestinataires-1;

                                                 for($k=0;$k<$limitdatadestinataires;$k++)
                                                 {
                                                   ?>
                                                   <span class="label label-sm label-warning"> <?php echo $tabdatadestinataires[$k]; ?> </span>
                                                  <?php

                                                 }
                                                 ?>
                                              </td>
                                              <td>
                                                <?php
                                                  $envoitype="";
                                                  if($value->sms_notif==0)
                                                  {
                                                    if($value->email_notif==0)
                                                    {

                                                    }else if($value->email_notif==1)
                                                    {
                                                      echo "EMAIL";
                                                    }
                                                  }else if($value->sms_notif==1){
                                                    if($value->email_notif==0)
                                                    {
                                                      echo "SMS";
                                                    }else if($value->email_notif==1)
                                                    {
                                                      echo "SMS / EMAIL";
                                                    }
                                                  }
                                                 ?>
                                              </td>
                                                <td>
                                                  <?php
                                                    $statutnotif=$value->statut_notif;

                                                    if($statutnotif==0)
                                                    {
                                                      ?>
                      <button type="button" class="btn btn-circle btn-danger btn-xs m-b-10">En attente</button>
                                                      <?php

                                                    }else if($statutnotif==1)
                                                    {
                                                      ?>
                    <button type="button" class="btn btn-circle btn-success btn-xs m-b-10">Envoyé</button>
                                                      <?php
                                                    }else if($statutnotif==2)
                                                    {
                                                      ?>

                                                      <?php
                                                    }
                                                 ?>
                                                </td>
                                                 <td>
                                                   <?php
                                                     $statutnotif=$value->statut_notif;

                                                     if($statutnotif==0)
                                                     {
                                                       ?>
                                                       <a href="#"  title="Envoyer" onclick="send(<?php echo $value->id_notif; ?>)" class="btn btn-info btn-circle ">
                                                         <i class="fa fa-send"></i>
                                                       </a>
                                                       <a class="btn btn-danger btn-circle  " onclick="deleted(<?php echo $value->id_notif; ?>)"  title="Supprimer">
                                                         <i class="fa fa-trash-o "></i>
                                                       </a>
                                                        <?php

                                                     }else if($statutnotif==1)
                                                     {
                                                       ?>
                                                       <a class="btn btn-warning btn-circle  " title="archiver" onclick="archiver(<?php echo $value->id_notif; ?>)">
                                                         <i class="fa fa-repeat "></i>
                                                       </a>
                                                       <?php
                                                     }else if($statutnotif==2)
                                                     {
                                                       ?>

                                                       <?php
                                                     }
                                                  ?>
                                                 </td>

                                           </tr>
                                           <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                           <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                           <script type="text/javascript">

                                           function archiver(id)
                                           {
                                             Swal.fire({
                               title: '<?php echo L::WarningLib ?>',
                               text: "Voulez vous vraiment archiver ce message",
                               type: 'warning',
                               showCancelButton: true,
                               confirmButtonColor: '#2CA8FF',
                               cancelButtonColor: '#d33',
                               confirmButtonText: '<?php echo L::Archived ?>',
                               cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                             }).then((result) => {
                               if (result.value) {
                                 document.location.href="../controller/messages.php?etape=5&notifid="+id;

                               }else {

                               }
                             })
                                           }

                                           function deleted(id)
                                           {
                                             // var notificationid="<?php //echo $value->id_notif; ?>";
                                             // var destinataires="<?php //echo $value->destinataires_notif; ?>";
                                             // var classes="<?php //echo  $value->classes_notif;?>";
                                             var etape=4;
                                             // var codeEtab="<?php  //echo $value->codeEtab_notif;?>";

                                             Swal.fire({
                               title: '<?php echo L::WarningLib ?>',
                               text: "Voulez vous vraiment supprimer ce message",
                               type: 'warning',
                               showCancelButton: true,
                               confirmButtonColor: '#2CA8FF',
                               cancelButtonColor: '#d33',
                               confirmButtonText: 'Supprmer',
                               cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                             }).then((result) => {
                               if (result.value) {
                                 document.location.href="../controller/messages.php?etape=4&notifid="+id;

                               }else {

                               }
                             })
                                           }

                                           function send(id)
                                           {

                                             //nous allons recuperer les elements essentiels pour l'envoi de mail ou sms
                                             var etape1=1;
                                             $.ajax({
                                               url: '../ajax/message.php',
                                               type: 'POST',
                                               async:false,
                                               data: 'notifid='+id+'&etape='+etape1,
                                               dataType: 'text',
                                                 success: function (content, statut) {


                                                   var tabcontent=content.split("*");
                                                   var destinataires=tabcontent[0];
                                                   var classes=tabcontent[1];
                                                   var codeEtab=tabcontent[2];
                                                   var smssender=tabcontent[3];
                                                   var emailsender=tabcontent[4];
                                                   var joinfile=tabcontent[5];
                                                   var file=tabcontent[6];
                                                   var etape2=3;

                                                   Swal.fire({
                                     title: '<?php echo L::WarningLib ?>',
                                     text: "<?php echo L::DoyouReallySendingThisMessages ?>",
                                     type: 'warning',
                                     showCancelButton: true,
                                     confirmButtonColor: '#2CA8FF',
                                     cancelButtonColor: '#d33',
                                     confirmButtonText: '<?php echo L::Sendbutton ?>',
                                     cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                   }).then((result) => {
                                     if (result.value) {
                                       // document.location.href="../controller/messages/etape=3&notifid="+notificationid+"&destinataires="+destinataires+"&classes="+classes;
                                       $.ajax({
                                         url: '../ajax/message.php',
                                         type: 'POST',
                                         async:false,
                                         data: 'notifid='+id+'&etape='+etape2+'&codeEtab='+codeEtab+'&destinataires='+destinataires+'&classes='+classes+'&smssender='+smssender+'&emailsender='+emailsender,
                                         dataType: 'text',
                                           success: function (content, statut) {

                                             var tab=content.split("/");
                                             var destimails=tab[0];
                                             var destiphones=tab[1];

                            document.location.href="../controller/messages.php?etape=3&notifid="+id+"&destimails="+destimails+"&destiphones="+destiphones+"&codeEtab="+codeEtab+"&smssender="+smssender+"&emailsender="+emailsender+"&joinfile="+joinfile+"&file="+file;


                                           }
                                       });
                                     }else {

                                     }
                                   })

                                                 }
                                             });







                                           }

                                           </script>

                                           <?php
                                           $i++;
                                           endforeach
                                            ?>



                                       </tbody>
                                   </table>
                               </div>
                           </div>
                       </div>
                                       </div>
                                       <div class="tab-pane" id="about">
                                         <div class="offset-md-1 col-md-10 col-sm-10">
                          <div class="card card-box">

                              <div class="card-body " id="bar-parent">
                                  <form method="post" action="../controller/messages.php" id="FormAddmessage" enctype="multipart/form-data" >
                                      <div class="form-group">
                                          <label for="titre">Titre :</label>
                                          <input type="text" class="form-control col-md-8" id="titre" name="titre" placeholder="Renseigner le titre du message">
                                      </div>
                                      <div class="form-group">
                                          <label for="message">Message</label>
                                          <textarea class="form-control col-md-8" name="message" id="message" rows="8" cols="80" placeholder="Renseigner le Message"></textarea>

                                      </div>
                                      <div class="form-group">
                                          <label for="simpleFormPassword">Fichier joint</label>
                                          <input type="file" id="joinfile" name="joinfile" class="joinfile"  data-show-loader="true" data-max-file-size="1mb" data-default-file="../assets2/img/user/user5.jpg" data-allowed-file-extensions="gif png jpg jpeg pjpeg pdf docx xlsx xls doc" />

                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-4">Classe(s) :
                                              <span class="required">  </span>
                                          </label>
                                          <div class="col-md-8">
                                            <select class="form-control input-height" multiple="multiple" id="classeEtab" name="classeEtab[]" style="width:100%">
                                                <option value="">Selectionner le(s) classe(s)</option>
                                                <?php
                                                $i=1;
                                                  foreach ($classes as $value):
                                                  ?>
                                                  <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                  <?php
                                                                                   $i++;
                                                                                   endforeach;
                                                                                   ?>

                                            </select>
                                            </div>
                                      </div>

                                      <div class="form-group row">
                                          <label class="control-label col-md-4">Destinataire(s) :
                                              <span class="required">  </span>
                                          </label>
                                          <div class="col-md-8">
                                            <select class="form-control input-height" multiple="multiple" id="destinataires" name="destinataires[]" style="width:100%">
                                                <option value="">Selectionner le(s) destinataire(s)</option>
                                                <option value="Parent">PARENTS</option>
                                                <option value="Student">ELEVES</option>
                                                <option value="Teatcher">PROFESSEURS</option>
                                                <option value="Admin_locale">ADMINISTRATION</option>

                                            </select>
                                            <input type="hidden" name="etape" id="etape"  value="1">
                                            <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner; ?>">
                                            </div>
                                      </div>
                                      <br>


                                      <div class="form-group">
                                        <div class="custom-control custom-checkbox custom-control-inline">
    <input type="checkbox" class="custom-control-input" id="smssender" name="smssender" onclick='checksms()'>
    <input type="hidden" name="smsvalue" id="smsvalue"  value="0">
    <label class="custom-control-label" for="smssender">Notification SMS</label>

                                        </div>
                                        <div class="custom-control custom-checkbox custom-control-inline">
    <input type="checkbox" class="custom-control-input" id="emailsender" name="emailsender" onclick='checkmail()'>
    <input type="hidden" name="emailvalue" id="emailvalue"  value="0">
    <label class="custom-control-label" for="emailsender">Notification Email</label>

                                        </div>


                                      </div>
                                      <button type="submit" class="btn btn-primary">Enregistrer</button>
                                  </form>
                              </div>
                          </div>
                      </div>
                                       </div>

                                   </div>
                               </div>
                           </div>
                       </div>

          </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
  <script src="../assets2/js/dropify.js"></script>
  <script src="../assets2/plugins/select2/js/select2.js" ></script>
  <script src="../assets2/js/pages/select2/select2-init.js" ></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
  <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function checksms()
   {


     if($('#smssender').prop('checked') == true){
 			$("#smsvalue").val(1);
 		}
 		else {

 			$("#smsvalue").val(0);
 		}

   }

   function checkmail()
   {


     if($('#emailsender').prop('checked') == true){
      $("#emailvalue").val(1);
    }
    else {

      $("#emailvalue").val(0);
    }



   }



   $("#classeEtab").select2({
     tags: true,
  tokenSeparators: [',', ' ']
   });
   $("#destinataires").select2({
     tags: true,
  tokenSeparators: [',', ' ']
   });
   $('.joinfile').dropify({
       messages: {
           'default': 'Selectionner un fichier joint',
           'replace': 'Remplacer le fichier joint',
           'remove':  'Retirer',
           'error':   'Ooops, Une erreur est survenue.'
       }
   });
   $(document).ready(function() {
$("#FormAddmessage").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
},
highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
},
success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{
    titre:"required",
    message:"required",
    classeEtab:"required",
    destinataires:"required"
  },
  messages: {
    titre:"Merci de renseigner le titre du message",
    message:"Merci de renseigner le message",
    classeEtab:"Merci de selectionner au moins une classe",
    destinataires:"Merci de selectionner au moins un destinataire"
  },
  submitHandler: function(form) {
form.submit();
  }


});


   });

   </script>
    <!-- end js include path -->
  </body>

</html>
