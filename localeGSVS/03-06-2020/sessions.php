<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$session= new Sessionacade();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
//nombre de session on
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
//nous allons recuperer le type d'etablissement

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

$sessions="";

if($etablissementType==1||$etablissementType==3)
{

  if($nbsessionOn>0)
  {
    //recuperer la session en cours
    $sessionencours=$session->getSessionEncours($codeEtabAssigner);
    $tabsessionencours=explode("*",$sessionencours);
    $libellesessionencours=$tabsessionencours[0];
    $sessionencoursid=$tabsessionencours[1];
    $typesessionencours=$tabsessionencours[2];
    // $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
    // $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);

    $notifications=$etabs->getAllNotificationbySchoolCode($codeEtabAssigner,$libellesessionencours);
    $sessions=$session->getAllSessionsOfThisSchoolCode($codeEtabAssigner);

    // $semesters=$session->getAllsemesterOfthisSession($sessionencoursid,$codeEtabAssigner);
  }

}else {
  if($nbsessionOn>0)
  {
    //recuperer la session en cours
    $sessionencours=$session->getSessionEncours($codeEtabAssigner);
    $tabsessionencours=explode("*",$sessionencours);
    $libellesessionencours=$tabsessionencours[0];
    $sessionencoursid=$tabsessionencours[1];
    $typesessionencours=$tabsessionencours[2];
      $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);

    $notifications=$etabs->getAllNotificationbySchoolCode($codeEtabAssigner,$libellesessionencours);
    $sessions=$session->getAllSessionsOfThisSchoolCode($codeEtabAssigner);

    $semesters=$session->getAllsemesterOfthisSession($sessionencoursid,$codeEtabAssigner);
  }
}






$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllClassesbyschoolCode($codeEtabAssigner);





 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
  <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
<!--bootstrap -->
<!--bootstrap -->
<link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<!-- Material Design Lite CSS -->
<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
<link href="../assets2/css/material_style.css" rel="stylesheet">
<!-- Theme Styles -->
  <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
<link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
<link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
<link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <style>
  #radioBtn .notActive{
  color: #3276b1;
  background-color: #fff;
  }

  </style>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::ScolaryyearMenu ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::ScolaryyearMenu ?></li>
                            </ol>
                        </div>
                    </div>
                    <?php

                          if(isset($_SESSION['user']['Updateadminok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: '<?php echo L::Felicitations  ?>',
                      text: '<?php echo $_SESSION['user']['Updateadminok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['Updateadminok']);
                          }

                           ?>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


          <?php

  ?>
  <div class="row">

    <div class="col-md-12 col-sm-12">
                   <div class="panel tab-border card-box">
                       <header class="panel-heading panel-heading-gray custom-tab ">
                           <ul class="nav nav-tabs">
                               <li class="nav-item"><a href="#home" data-toggle="tab" class="active"><?php echo L::ScolaryyearMenu ?> </a>
                               </li>
                               <li class="nav-item"><a href="#type" data-toggle="tab"><?php echo L::Periods ?></a>
                               </li>
                               <?php
                                if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Superviseur")
                                {

                                }else {
                                  ?>
                                  <li class="nav-item"><a href="#about" data-toggle="tab"><?php echo L::AddScolaryears ?></a>
                                  </li>
                                  <?php
                                }
                                ?>


                           </ul>
                       </header>
                       <div class="panel-body">
                           <div class="tab-content">
                               <div class="tab-pane active" id="home">
                                 <div class="col-md-12">
                   <div class="card  card-box">

                       <div class="card-body ">

                           <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                               <thead>
                                   <tr>

                                       <th> <?php echo L::Id ?> </th>
                                       <th> <?php echo L::ScolaryyearMenu ?></th>
                                       <th> <?php echo L::Periods ?></th>
                                       <th> <?php echo L::Etat ?></th>
                                       <th> <?php echo L::Actions ?> </th>
                                   </tr>
                               </thead>
                               <?php
                               if($nbsessionOn>0)
                               {
                                ?>
                               <tbody>
                                 <?php
                                 $i=1;
                                 foreach ($sessions as $value):
                                  ?>
                                   <tr class="odd gradeX">

                                       <td> <?php echo $i ?></td>
                                       <td>
                                           <a href="#"> <?php echo $value->libelle_sess; ?> </a>
                                       </td>
                                       <td>
                                           <?php
                                            if($value->type_sess==2)
                                            {
                                              ?>
                                <span class="label label-sm label-primary"> <?php echo L::Semestre ?> </span>
                                              <?php
                                            }else if($value->type_sess==3)
                                            {
                                              ?>
                                <span class="label label-sm label-primary"> <?php echo L::Trimestre ?> </span>
                                              <?php
                                            }
                                            ?>
                                       </td>
                                       <td>
                                         <?php
                                         if($value->encours_sess==0)
                                         {
                                           ?>
                                  <span class="label label-sm label-danger"> <?php echo L::Closure ?> </span>
                                           <?php
                                         }else if($value->encours_sess==1)
                                         {
                                           ?>
                                  <span class="label label-sm label-success"> <?php echo L::Actif ?> </span>
                                           <?php
                                         }
                                          ?>
                                       </td>
                                       <td>
                                         <?php
                                         if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Superviseur")
                                         {

                                         }else {
                                            $check=$session->getNumberOfSemestreDown($value->id_sess,$value->type_sess,$value->codeEtab_sess);
                                            if($check>0)
                                            {
                                              ?>

                                              <a href="#"  class="btn btn-danger btn-xs" title="Clôturer l'année scolaire">
                                                <i class="fa fa-times-rectangle"></i>
                                              </a>

                                              <?php
                                            }else
                                            {
                                              //verifier si nous avons au moins un semestre ou trimestre terminé
                                              $check1=$session->getNbOfSemestreDown($value->id_sess,$value->type_sess,$value->codeEtab_sess);



                                              if($check1==0)
                                              {
                                                if($value->encours_sess>0)
                                                {
                                                  if($value->statut_sess==0)
                                                  {
                                                    //ajouter un trimestre ou semestre
                                                    ?>
                                                    <a href="#" onclick="modify(<?php echo $value->id_sess?>)"  class="btn btn-warning btn-xs" title="<?php echo L::ModifierBtn ?>">
                                                      <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <a href="#" data-toggle="modal" style="display:none" data-target="#mediumModel<?php echo $value->id_sess?>" id="btn<?php echo $value->id_sess?>"  class="btn btn-warning btn-xs" title="<?php echo L::ModifierBtn ?>">
                                                      <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <?php

                                                  }
                                                }else {

                                                }

                                              }else {
                                                // code...
                                              }

                                            }
                                         }




                                          ?>
                                       </td>

      <div class="modal fade" id="mediumModel<?php echo $value->id_sess?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title" id="exampleModalLabel"><?php echo L::ModScolaryear ?></h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                <form>
                               <div class="form-group">
                                   <label for="simpleFormEmail"><?php echo L::ScolaryyearMenu ?></label>
                                   <input type="text" onclick="erasedSession(<?php echo $value->id_sess;?>)" class="form-control" id="sessionlib<?php echo $value->id_sess;?>" name="sessionlib<?php echo $value->id_sess;?>" value="<?php echo $value->libelle_sess ?>" placeholder="Merci de renseigner l'année scolaire">
                                   <p id="messageSessionLib<?php echo $value->id_sess;?>"></p>
                               </div>
                               <div class="form-group">
                                   <label for="simpleFormPassword"><?php echo L::Period ?></label>
                                   <select class="form-control" name="typesession<?php echo $value->id_sess;?>" id="typesession<?php echo $value->id_sess;?>">
                                     <option value=""><?php echo L::SelectedPeriod ?></option>
                                     <option <?php if($value->type_sess==2){echo "selected";} ?> value="2"><?php echo L::Semestre ?></option>
                                     <option <?php if($value->type_sess==3){echo "selected";} ?> value="3"><?php echo L::Trimestre ?></option>

                                   </select>
                                   <input type="hidden" name="etape" id="etape" value="2">
                                   <input type="hidden" name="oldsessiontype<?php echo $value->id_sess;?>" id="oldsessiontype<?php echo $value->id_sess;?>" value="<?php echo $value->type_sess; ?>">
                                   <input type="hidden" name="codeEtab<?php echo $value->id_sess;?>" id="codeEtab<?php echo $value->id_sess;?>" value="<?php echo $value->codeEtab_sess;?>">



                               </div>

                               <a href="#" onclick="checkSession(<?php echo $value->id_sess;?>)" class="btn btn-primary"><?php echo L::ModifierBtn ?></a>
                               <a href="#" class="btn btn-danger btn-md" data-dismiss="modal"> <?php echo L::AnnulerBtn ?></a>
                           </form>
              </div>

          </div>
      </div>
  </div>

                                   </tr>
                                   <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                   <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                   <script type="text/javascript">

                                   function erasedSession(id)
                                   {
     document.getElementById("messageSessionLib"+id).innerHTML = "";

                                   }

                                   function checkSession(id)
                                   {
                                     var sessionlib=$("#sessionlib"+id).val();
                                     var typesession=$("#typesession"+id).val();
                                     var codeEtab=$("#codeEtab"+id).val();
                                     var oldsessiontype=$("#oldsessiontype"+id).val();

                                     if(sessionlib==""||typesession=="")
                                     {
                                        if(sessionlib=="")
                                        {
            document.getElementById("messageSessionLib"+id).innerHTML = "<font color=\"red\"><?php echo L::PleaseEnterScolaryear ?></font>";

                                        }
                                     }else {

                                      //comparer les types de session

                                      var etape=2;
                                      $.ajax({
                                      url: '../ajax/sessions.php',
                                      type: 'POST',
                                      async:true,
                                       data:'etape=' + etape+'&sessionid='+id+'&sessionlib='+sessionlib+'&typesession='+typesession+'&codeEtab='+codeEtab+'&oldsessiontype='+oldsessiontype,
                                       dataType: 'text',
                                       success: function (content, statut) {

                                         //event.preventDefault();

                                         $("#mediumModel"+id).css("display", "none")

                                         Swal.fire({
                                         type: 'success',
                                         title: '<?php echo L::Felicitations ?>',
                                         text: "<?php echo L::ScolaryearModifyOkay ?>",

                                       });

                                         location.reload();
                                     }

                                   });


                                 }
                               }

                                   function modify(id)
                                   {
                                     Swal.fire({
                       title: '<?php echo L::WarningLib ?>',
                       text: "<?php echo L::DoyouReallyModifyingScolaryear ?>",
                       type: 'warning',
                       showCancelButton: true,
                       confirmButtonColor: '#2CA8FF',
                       cancelButtonColor: '#d33',
                       confirmButtonText: '<?php echo L::ModifierBtn ?>',
                       cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                     }).then((result) => {
                       if (result.value) {
                         // document.location.href="../controller/messages.php?etape=5&notifid="+id;
                         $('#btn<?php echo $value->id_sess?>').click();


                       }else {

                       }
                     })
                                   }

                                   function archiver(id)
                                   {
                                     Swal.fire({
                       title: '<?php echo L::WarningLib ?>',
                       text: "<?php echo L::DoyouReallyArchivedMessages ?>",
                       type: 'warning',
                       showCancelButton: true,
                       confirmButtonColor: '#2CA8FF',
                       cancelButtonColor: '#d33',
                       confirmButtonText: '<?php echo L::Archived ?>',
                       cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                     }).then((result) => {
                       if (result.value) {
                         document.location.href="../controller/messages.php?etape=5&notifid="+id;

                       }else {

                       }
                     })
                                   }

                                   function deleted(id)
                                   {
                                     // var notificationid="<?php //echo $value->id_notif; ?>";
                                     // var destinataires="<?php //echo $value->destinataires_notif; ?>";
                                     // var classes="<?php //echo  $value->classes_notif;?>";
                                     var etape=4;
                                     // var codeEtab="<?php  //echo $value->codeEtab_notif;?>";

                                     Swal.fire({
                       title: '<?php echo L::WarningLib ?>',
                       text: "<?php echo L::DoyouReallyDeletedMessages ?>",
                       type: 'warning',
                       showCancelButton: true,
                       confirmButtonColor: '#2CA8FF',
                       cancelButtonColor: '#d33',
                       confirmButtonText: '<?php echo L::DeleteLib ?>',
                       cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                     }).then((result) => {
                       if (result.value) {
                         document.location.href="../controller/messages.php?etape=4&notifid="+id;

                       }else {

                       }
                     })
                                   }

                                   function send(id)
                                   {

                                     //nous allons recuperer les elements essentiels pour l'envoi de mail ou sms
                                     var etape1=1;
                                     $.ajax({
                                       url: '../ajax/message.php',
                                       type: 'POST',
                                       async:false,
                                       data: 'notifid='+id+'&etape='+etape1,
                                       dataType: 'text',
                                         success: function (content, statut) {


                                           var tabcontent=content.split("*");
                                           var destinataires=tabcontent[0];
                                           var classes=tabcontent[1];
                                           var codeEtab=tabcontent[2];
                                           var smssender=tabcontent[3];
                                           var emailsender=tabcontent[4];
                                           var joinfile=tabcontent[5];
                                           var file=tabcontent[6];
                                           var etape2=3;

                                           Swal.fire({
                             title: '<?php echo L::WarningLib ?>',
                             text: "<?php echo L::DoyouReallySendingThisMessages ?>",
                             type: 'warning',
                             showCancelButton: true,
                             confirmButtonColor: '#2CA8FF',
                             cancelButtonColor: '#d33',
                             confirmButtonText: '<?php echo L::Sendbutton ?>',
                             cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                           }).then((result) => {
                             if (result.value) {
                               // document.location.href="../controller/messages/etape=3&notifid="+notificationid+"&destinataires="+destinataires+"&classes="+classes;
                               $.ajax({
                                 url: '../ajax/message.php',
                                 type: 'POST',
                                 async:false,
                                 data: 'notifid='+id+'&etape='+etape2+'&codeEtab='+codeEtab+'&destinataires='+destinataires+'&classes='+classes+'&smssender='+smssender+'&emailsender='+emailsender,
                                 dataType: 'text',
                                   success: function (content, statut) {

                                     var tab=content.split("/");
                                     var destimails=tab[0];
                                     var destiphones=tab[1];

                    document.location.href="../controller/messages.php?etape=3&notifid="+id+"&destimails="+destimails+"&destiphones="+destiphones+"&codeEtab="+codeEtab+"&smssender="+smssender+"&emailsender="+emailsender+"&joinfile="+joinfile+"&file="+file;


                                   }
                               });
                             }else {

                             }
                           })

                                         }
                                     });







                                   }

                                   </script>

                                   <?php
                                   $i++;
                                   endforeach
                                    ?>



                               </tbody>

                               <?php
                             }
                                ?>
                           </table>
                       </div>
                   </div>
               </div>
                               </div>
                               <div class="tab-pane" id="about">
                                 <div class="offset-md-1 col-md-10 col-sm-10">
                  <div class="card card-box">

                      <div class="card-body " id="bar-parent">
                          <form method="post" action="../controller/sessions.php" id="FormAddmessage" enctype="multipart/form-data" >

                              <div class="form-group">
                                  <label for="titre"><?php echo L::ScolaryyearMenu ?> :</label>
                                  <input type="text" class="form-control col-md-8" id="titre" name="titre" placeholder="<?php echo L::EnterScolaryyearMenu ?>">
                                  <input type="hidden" name="etape" id="etape"  value="6">
                                  <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner; ?>">
                              </div>

                              <!--div class="form-group">
                                  <label for="semestre">Semestre / Trimestre :</label>
                                  <select class="form-control input-height" id="semestre" name="semestre" style="width:50%">

                                      <option value="2">Semestre</option>
                                      <option value="3" selected>Trimestre</option>
                                  </select>
                              </div-->

                              <div class="form-group">
                                  <label for="titre"><?php echo L::DatedebLib ?>:</label>
                                  <input type="text" placeholder="<?php echo L::DatedebLib ?>" name="datedeb" id="datedeb"  class="form-control col-md-8">

                              </div>

                              <div class="form-group">
                                  <label for="titre"><?php echo L::DatefinLib ?>:</label>
                                  <input type="text" placeholder="<?php echo L::DatefinLib ?>" name="datefin" id="datefin"  class="form-control col-md-8">

                              </div>

                              <div class="form-group row">


                        <div class="col-sm-7 col-md-7">
                        <div class="input-group">
                        <div id="radioBtn" class="btn-group">
                        <a class="btn btn-primary btn-sm active" data-toggle="semestre" data-title="3" onclick="periodeT()"><?php echo L::Trimestrecaps ?></a>
                        <a class="btn btn-primary btn-sm notActive" data-toggle="semestre" data-title="2" onclick="periodeS()"><?php echo L::Semestrecaps ?></a>
                        </div>
                        <input type="hidden" name="semestre" id="semestre" value="3">
                        </div>
                        </div>
                        </div>



  <!-- Material inline 2 -->




                              <br>
                              <?php
                              // $libellesessionencours=$tabsessionencours[0];
                              // $sessionencoursid=$tabsessionencours[1];
                              // $typesessionencours=$tabsessionencours[2];
                              //verifier si nous avons un semestre ou un trimestre dont le statut est à 2

                              $checkstat=$session->getNumberSessionEncoursOn($codeEtabAssigner);

                              if($checkstat==0)
                              {
                              ?>
                              <button type="submit" class="btn btn-success"><?php echo L::Saving ?></button>
                              <?php
                            }else {
                              ?>
                              <button type="submit" class="btn btn-primary" disabled><?php echo L::Saving ?></button>
                              <?php
                            }


                               ?>

                          </form>
                      </div>
                  </div>
              </div>
                               </div>
                               <div class="tab-pane" id="type">
                                 <div class="offset-md-1 col-md-10 col-sm-10">
                  <!--div class="card card-box"-->

                      <div class="card-body " id="bar-parent">
                       <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                         <thead>
                             <tr>
                               <th><?php echo L::Periods ?></th>
                                 <th> <?php echo L::Etat ?></th>
                                 <th> <?php echo L::Actions ?></th>
                             </tr>
                         </thead>
                         <?php
                         if($nbsessionOn>0)
                         {
                          ?>
                         <tbody>
                           <?php
                           $i=1;
                           foreach ($semesters as $value):
                            ?>
                           <tr>

                             <td><?php echo $value->libelle_semes ?></td>
                             <td>
                               <?php
                               $statut=$value->statut_semes;
                               if($statut==1)
                               {
                                 ?>
                 <span class="label label-sm label-success"> <?php echo L::Actif ?> </span>
                                 <?php
                               }else if($statut==2)
                               {
                                 ?>
                <span class="label label-sm label-danger"> <?php echo L::Terminato ?> </span>
                                 <?php
                               }
                                ?>
                             </td>
                             <td>
                               <?php

                               if($value->statut_semes!=2)
                               {
                                ?>
                                <?php
                                if($statut==1)
                                {
                                 ?>
                                 <?php
                                 if($_SESSION['user']['fonctionuser']=="Directeur")
                                 {

                                 }else {
                                   ?>
                                   <a href="#"  onclick="semesterclose(<?php echo $value->id_semes;?>,<?php echo $value->idsess_semes;?>,<?php echo $value->next_semes ?>)" class="btn btn-danger btn-xs" title="<?php echo L::Closure ?>">
                                     <i class="fa fa-times-rectangle"></i>
                                   </a>
                                   <?php
                                 }
                                  ?>

                                 <?php
                               }
                                  ?>


                               <?php
                               }
                                ?>
                             </td>
                           </tr>
                           <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                           <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                           <script type="text/javascript">
                           function semesterclose(id,sessionid,suivant)
                           {

                             Swal.fire({
               title: '<?php echo L::WarningLib ?>',
               text: "<?php echo L::DoyouReallyCloturePeriode ?> ",
               type: 'warning',
               showCancelButton: true,
               confirmButtonColor: '#2CA8FF',
               cancelButtonColor: '#d33',
               confirmButtonText: '<?php echo L::Closure ?>',
               cancelButtonText: '<?php echo L::AnnulerBtn ?>',
             }).then((result) => {
               if (result.value) {
                 // document.location.href="../controller/messages.php?etape=4&notifid="+id;
                 // document.location.href="../controller/sessions.php?etape=2&typesess="+id+"&sessionid="+sessionid+"&suivantid="+suivant;

                 Swal.fire({
   title: '<?php echo L::WarningLib ?>',
   text: "<?php echo L::PeriodeClotureAdvanced ?>",
   type: 'warning',
   showCancelButton: true,
   confirmButtonColor: '#2CA8FF',
   cancelButtonColor: '#d33',
   confirmButtonText: '<?php echo L::Closure ?>',
   cancelButtonText: '<?php echo L::AnnulerBtn ?>',
 }).then((result) => {
   if (result.value) {
     // document.location.href="../controller/messages.php?etape=4&notifid="+id;

     document.location.href="../controller/sessions.php?etape=2&typesess="+id+"&sessionid="+sessionid+"&suivantid="+suivant;

   }else {

   }
 })

               }else {

               }
             })
                           }
                           </script>

                           <?php
                           $i++;
                           endforeach
                            ?>

                         </tbody>
                         <?php
                       }
                          ?>

                         </table>

                      </div>
                  <!--/div-->
              </div>
                               </div>

                           </div>
                       </div>
                   </div>
               </div>

  </div>



                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
  <script src="../assets2/js/dropify.js"></script>
  <script src="../assets2/plugins/select2/js/select2.js" ></script>
  <script src="../assets2/js/pages/select2/select2-init.js" ></script>
  <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
  <script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
  <script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
  <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   $('#radioBtn a').on('click', function(){
      var sel = $(this).data('title');
      var tog = $(this).data('toggle');
      $('#'+tog).prop('value', sel);

      $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
      $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
  })

   var date = new Date();

  var newDate = new Date(date.setTime( date.getTime() + (-6120 * 86400000)));

   $('#datedeb').bootstrapMaterialDatePicker
   ({
   date: true,
   shortTime: false,
   time: false,
   format: 'DD-MM-YYYY',
   lang: 'fr',
   cancelText: '<?php echo L::AnnulerBtn ?>',
   okText: '<?php echo L::Okay ?>',
   clearText: '<?php echo L::Eraser ?>',
   nowText: '<?php echo L::Now ?>'

   });

   $('#datefin').bootstrapMaterialDatePicker
   ({
   date: true,
   shortTime: false,
   time: false,
   format: 'DD-MM-YYYY',
   lang: 'fr',
   cancelText: '<?php echo L::AnnulerBtn ?>',
   okText: '<?php echo L::Okay ?>',
   clearText: '<?php echo L::Eraser ?>',
   nowText: '<?php echo L::Now ?>'

   });

      // $("#semestre").select2();

      function sessioncloturate(sessionid)
      {
        var etape=4;
        var codeEtab="<?php echo $codeEtabAssigner ?>";
        Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::DoyouClosureScolaryear ?> ",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#2CA8FF',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::Closure ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
// document.location.href="../controller/messages.php?etape=4&notifid="+id;
// document.location.href="../controller/sessions.php?etape=2&typesess="+id+"&sessionid="+sessionid+"&suivantid="+suivant;



Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::PeriodeClotureAdvanced ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#2CA8FF',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::Closure ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
// document.location.href="../controller/messages.php?etape=4&notifid="+id;

$.ajax({
  url: '../controller/sessions.php',
  type: 'POST',
  async:false,
  data: 'sessionid='+sessionid+'&etape='+etape+'&codeEtab='+codeEtab,
  dataType: 'text',
    success: function (content, statut) {

 location.reload();

      }
});


}else {

}
})

}else {

}
})
      }

   function checksms()
   {


     if($('#smssender').prop('checked') == true){
 			$("#smsvalue").val(1);
 		}
 		else {

 			$("#smsvalue").val(0);
 		}

   }

   function checkmail()
   {


     if($('#emailsender').prop('checked') == true){
      $("#emailvalue").val(1);
    }
    else {

      $("#emailvalue").val(0);
    }



   }



   $("#classeEtab").select2({
     tags: true,
  tokenSeparators: [',', ' ']
   });
   $("#destinataires").select2({
     tags: true,
  tokenSeparators: [',', ' ']
   });
   $('.joinfile').dropify({
       messages: {
           'default': 'Selectionner un fichier joint',
           'replace': 'Remplacer le fichier joint',
           'remove':  'Retirer',
           'error':   'Ooops, Une erreur est survenue.'
       }
   });
   $(document).ready(function() {
$("#FormAddmessage").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
},
highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
},
success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{
    titre:"required",
    message:"required",
    classeEtab:"required",
    destinataires:"required",
    semestre:"required",
    datefin:"required",
    datedeb:"required",
  },
  messages: {
    titre:"<?php echo L::PleaseEnterScolaryears ?>",
    message:"<?php echo L::PleaseEnterActivityMesaage ?>",
    classeEtab:"<?php echo L::PleaseSelectaumoinsClasse ?>",
    destinataires:"<?php echo L::PleaseSelectaumoinsDestinataires ?>",
    semestre:"<?php echo L::PleaseEnterValues ?>",
    datefin:"<?php echo L::PleaseEnterParascoActivityDateStart ?>",
    datedeb:"<?php echo L::PleaseEnterParascoActivityDateEnd ?>",
  },
  submitHandler: function(form) {

//nous allins verifier si cette session existe deja

var etape=1;
var codeEtab="<?php echo $codeEtabAssigner; ?>";
var titre=$("#titre").val();

$.ajax({
         url: '../ajax/sessions.php',
         type: 'POST',
         async:false,
         data: 'codeEtab='+codeEtab+'&etape='+etape+'&titre='+titre,
         dataType: 'text',
         success: function (content, statut) {


          if(content==0)
          {
            form.submit();
          }else if(content==1)
          {
            Swal.fire({
            type: 'warning',
            title: '<?php echo L::WarningLib ?>',
            text: '<?php echo L::ScolaryearAllReadyExiste ?>',

            })
          }
         }
       });

//
  }


});


   });

   </script>
    <!-- end js include path -->
  </body>

</html>
