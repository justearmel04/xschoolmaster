<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Teatcher.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Classe.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];

$user=new User();
$etabs=new Etab();
$admin= new Localadmin();
$classe=new Classe();
$localadmins= new Localadmin();
$parents=new ParentX();
$teatcher=new Teatcher();

$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

$admins=$admin->getAllAdminLocal();
$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);



if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$codesEtab=$etabs->getAllcodesEtab();
$codesEtab=$etabs->getAllcodesEtabBycodeEtab($codeEtabAssigner);
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$teatchers=$teatcher->getAllTeatchers();

$teatchers=$teatcher->getAllTeatchersBySchoolCode($codeEtabAssigner);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);

}
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);

$syllabusInfos=$etabs->getSyllabusInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

$array=json_encode($syllabusInfos,true);
$someArray = json_decode($array, true);


 ?>
 <html lang="en">
 <!-- BEGIN HEAD -->

 <head>
     <meta charset="utf-8" />
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta content="width=device-width, initial-scale=1" name="viewport" />
     <title><?php echo L::Titlesite ?></title>
     <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
     <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

     <!-- google font -->
     <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
 	<!-- icons -->
     <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
 	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
 	<!--bootstrap -->
    <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
 	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <!-- data tables -->
    <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
     <!-- Material Design Lite CSS -->
 	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
 	<link href="../assets2/css/material_style.css" rel="stylesheet">
   <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
 	<!-- morris chart -->
     <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
     <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
     <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

 	<!-- Theme Styles -->
     <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
     <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
     <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
     <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
     <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

 	<!-- favicon -->
     <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
  </head>
  <!-- END HEAD -->
 <body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white">
     <div class="page-wrapper">
         <!-- start header -->
 		<?php
 include("header.php");
     ?>
         <!-- end header -->
         <!-- start page container -->
         <div class="page-container">
  			<!-- start sidebar menu -->
  			<?php
 				include("menu.php");
 			?>
 			 <!-- end sidebar menu -->
 			<!-- start page content -->
             <div class="page-content-wrapper">
                 <div class="page-content">
                     <div class="page-bar">
                         <div class="page-title-breadcrumb">
                             <div class=" pull-left">
                                 <div class="page-title"><?php echo L::SyllabrMenu  ?></div>
                             </div>
                             <ol class="breadcrumb page-breadcrumb pull-right">
                                 <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">php</a>&nbsp;<i class="fa fa-angle-right"></i>
                                 </li>
                                 <li class="active"><?php echo L::SyllabrMenu  ?></li>
                             </ol>
                         </div>
                     </div>
 					<!-- start widget -->
 					<div class="state-overview">
 						<div class="row">

 					        <!-- /.col -->

 					        <!-- /.col -->

 					        <!-- /.col -->

 					        <!-- /.col -->
 					      </div>
 						</div>
 					<!-- end widget -->
           <?php

                 if(isset($_SESSION['user']['addetabexist']))
                 {

                   ?>
                   <div class="alert alert-danger alert-dismissible fade show" role="alert">
                 <?php
                 echo $_SESSION['user']['addetabexist'];
                 ?>
                 <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
                    </a>
                 </div>



                   <?php
                   unset($_SESSION['user']['addetabexist']);
                 }

                  ?>


                  <div class="row">
                      <div class="col-md-12 col-sm-12">
                          <div class="card card-box">
                              <div class="card-head">
                                  <header></header>

                              </div>

                              <div class="card-body" id="bar-parent">
                                <form class="form-horizontal" method="post" action="../controller/academiques.php" id="syllabForm">
                                  <div class="row">
                  <div class="col-md-12">

            <div style="background-color:#007bff;">

            <center><legend style="color:white;"><?php echo L::SyllabusCaps ?></legend></center>
          </div>


            </div>
          </div> <br>
                                       <div class="form-group row">
                                           <label for="horizontalFormEmail" class="col-sm-5 control-label" style="margin-left:-176px;"><b><?php echo L::ScolaryyearMenu ?></b> </label>
                                           <div class="col-sm-6">
                                               <input type="session" class="form-control" id="horizontalFormEmail" placeholder="" style="width:200px;" value="<?php echo $libellesessionencours; ?>" readonly>
                                           </div>
                                       </div>
                                       <div class="form-group row">
                                           <label for="horizontalFormPassword" class="col-sm-5 control-label" style="margin-left:-176px;"><b><?php echo L::NomTeacher ?></b> </label>
                                           <div class="col-sm-5">
                                               <input type="text" class="form-control " id="horizontalFormPassword" placeholder=""  value="<?php echo $teatcher->getNameofTeatcherById($_GET['teatcher']); ?>" readonly>
                                           </div>
                                       </div>
                                       <div class="form-group row">
                                           <label for="horizontalFormPassword" class="col-sm-3 control-label"><b><?php echo L::ClasseMenu ?><span class="required"> * </span></b> </label>
                                           <div class="col-sm-5">
                                               <input type="text" class="form-control" id="horizontalFormPassword" value="<?php echo $someArray[0]["libelle_classe"]; ?>" readonly>

                                               <input type="hidden" name="codeEtab" id="codeEtab" value="">
                                           </div>
                                       </div>
                                       <div class="form-group row">
                                           <label for="horizontalFormPassword" class="col-sm-3 control-label"><b><?php echo L::MatiereMenusingle ?><span class="required"> * </span>: </b></label>
                                           <div class="col-sm-5">
                                               <input type="text" class="form-control" id="horizontalFormPassword" placeholder="" value="<?php echo $someArray[0]["libelle_mat"]; ?>" readonly>

                                           </div>
                                       </div>
                                       <div class="row">
                       <div class="col-md-12">

                 <div style="background-color:#007bff;">

                 <center><legend style="color:white;"><?php echo L::GeneralInfoTabsingle ?></legend></center>
               </div>


                 </div>
               </div> <br>

               <div class="row">
                                   <div class="col-md-12">
               <div class="form-group">
               <label for=""><b><?php echo L::Descriptcourses ?><span class="required"> * </span>: </b></label>
                  <textarea class="form-control descri" rows="8" name="descri" id="descri" placeholder="<?php echo L::Adddescriptcourses ?>" readonly><?php echo $someArray[0]["descri_syllab"]; ?></textarea>

               </div>

               </div>
              </div>

              <div class="row">
                                  <div class="col-md-12">
              <div class="form-group">
              <label for=""><b><?php echo L::Objectifscourses ?> <span class="required"> * </span>: </b></label>

              </div>
 <input type="hidden" id="nb" name="nb" value="0">
 <input type="hidden" name="concatobjectif" id="concatobjectif" value="">
 <input type="hidden" name="concatnbobjectif" id="concatnbobjectif" value="">
              </div>
             </div>

<?php
  $objectifs=$etabs->getSyllabusObjectifsInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);
  // $arrayObj=json_encode($syllabusInfos,true);
  // $ObjetifArray = json_decode($arrayObj, true);
 ?>
   <table class="table" id="dynamic_field" border=0>
     <?php foreach ($objectifs as $value): ?>
<tr>
  <td><?php echo $value->libelle_syllabob ?></td>

</tr>
     <?php endforeach; ?>
                                </table>
 <br>
 <div class="row">
                     <div class="col-md-12">
 <div class="form-group">
 <label for=""><b><?php echo L::Contenu ?><span class="required"> * </span>: </b></label>

 </div>
 <input type="hidden" id="nbcontenu" name="nbcontenu" value="0">
 <input type="hidden" name="concatcontenu" id="concatcontenu" value="">
 <input type="hidden" name="concatnbcontenu" id="concatnbcontenu" value="">
 </div>
 </div>
 <?php
   $themes=$etabs->getSyllabusThemesInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);
   // $arrayObj=json_encode($syllabusInfos,true);
   // $ObjetifArray = json_decode($arrayObj, true);
  ?>
 <table class="table" id="dynamic_field1" border=0>
   <?php foreach ($themes as $value): ?>
  <tr>
  <td><?php echo $value->libelle_syllabth ?></td>

  </tr>
   <?php endforeach; ?>
                              </table>
                              <br>
                              <div class="row">
                                                  <div class="col-md-12">
                              <div class="form-group">
                              <label for=""><b><?php echo L::RequiredPrereq ?>  <span class="required"> * </span>: </b></label>

                              </div>
                              <input type="hidden" id="nbrequis" name="nbrequis" value="0">
                              <input type="hidden" name="concatrequis" id="concatrequis" value="">
                              <input type="hidden" name="concatnbrequis" id="concatnbrequis" value="">
                              </div>
                              </div>
                              <?php
                                $Requis=$etabs->getSyllabusRequisInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);
                                // $arrayObj=json_encode($syllabusInfos,true);
                                // $ObjetifArray = json_decode($arrayObj, true);
                               ?>
                              <table class="table" id="dynamic_field2" border=0>
                                <?php foreach ($Requis as $value): ?>
                               <tr>
                               <td><?php echo $value->libelle_syllabreq ?></td>

                               </tr>
                                <?php endforeach; ?>
                                                           </table>
                                                           <br>
                                                           <div class="row">
                                                                               <div class="col-md-12">
                                                           <div class="form-group">
                                                           <label for=""><b><?php echo L::CompetenceViseeNew ?>  <span class="required"> * </span>: </b></label>

                                                           </div>
                                                           <input type="hidden" id="nbcomp" name="nbcomp" value="0">
                                                           <input type="hidden" name="concatcomp" id="concatcomp" value="">
                                                           <input type="hidden" name="concatnbcomp" id="concatnbcomp" value="">
                                                           </div>
                                                           </div>
                                                           <?php
                                                             $Comp=$etabs->getSyllabusCompInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);
                                                             // $arrayObj=json_encode($syllabusInfos,true);
                                                             // $ObjetifArray = json_decode($arrayObj, true);
                                                            ?>

                                                           <table class="table" id="dynamic_field3" border=0>
                                                             <?php foreach ($Requis as $value): ?>
                                                            <tr>
                                                            <td><?php echo $value->libelle_syllabreq ?></td>

                                                            </tr>
                                                             <?php endforeach; ?>
                                                                                        </table>
                                                                                        <br>

                                                                                        <div class="row">
                                                                                        <div class="col-md-12">

                                                                                        <div style="background-color:#007bff;">

                                                                                        <center><legend style="color:white;"><?php echo L::CourseDocs ?></legend></center>
                                                                                      </div>


                                                                                        </div>
                                                                                        </div> <br>

                                                                                        <div class="row">
                                                                                                            <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                        <label for=""><b><?php echo L::CourseDocsRequired ?>   <span class="required"> * </span>: </b></label>

                                                                                        </div>
                                                                                        <input type="hidden" id="nbdoc" name="nbdoc" value="0">
                                                                                        <input type="hidden" name="concatdoc" id="concatdoc" value="">
                                                                                        <input type="hidden" name="concatnbdoc" id="concatnbdoc" value="">
                                                                                        </div>
                                                                                        </div>
                                                                                        <?php
                                                                                          $Docs=$etabs->getSyllabusDocInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);
                                                                                          // $arrayObj=json_encode($syllabusInfos,true);
                                                                                          // $ObjetifArray = json_decode($arrayObj, true);
                                                                                         ?>
                                                                                        <table class="table" id="dynamic_field4" border=0>
                                                                                          <?php foreach ($Docs as $value): ?>
                                                                                         <tr>
                                                                                         <td><?php echo $value->libelle_syllabdoc ?></td>

                                                                                         </tr>
                                                                                          <?php endforeach; ?>
                                                                                                                     </table>
                                                                                                                     <br>
                                                                                                                     <div class="row">
                                                                                                                                         <div class="col-md-12">
                                                                                                                     <div class="form-group">
                                                                                                                     <label for=""><b><?php echo L::CourseDocsFac ?>   <span class="required"> * </span>: </b></label>

                                                                                                                     </div>
                                                                                                                     <input type="hidden" id="nbdocfac" name="nbdocfac" value="0">
                                                                                                                     <input type="hidden" name="concatdocfac" id="concatdocfac" value="">
                                                                                                                     <input type="hidden" name="concatnbdocfac" id="concatnbdocfac" value="">
                                                                                                                     </div>
                                                                                                                     </div>
                                                                                                                     <?php
                                                                                                                       $Docfacs=$etabs->getSyllabusDocfacInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);
                                                                                                                       // $arrayObj=json_encode($syllabusInfos,true);
                                                                                                                       // $ObjetifArray = json_decode($arrayObj, true);
                                                                                                                      ?>
                                                                                                                     <table class="table" id="dynamic_field5" border=0>
                                                                                                                       <?php foreach ($Docfacs as $value): ?>
                                                                                                                      <tr>
                                                                                                                      <td><?php echo $value->libelle_syllabdoc ?></td>

                                                                                                                      </tr>
                                                                                                                       <?php endforeach; ?>
                                                                                                                                                  </table>
                                                                                                                                                  <br>
                                                                                                                                                  <div class="row">
                                                                                                                                                  <div class="col-md-12">

                                                                                                                                                  <div style="background-color:#007bff;">

                                                                                                                                                  <center><legend style="color:white;"><?php echo L::CourseCalendar ?></legend></center>
                                                                                                                                                </div>


                                                                                                                                                  </div>
                                                                                                                                                  </div> <br>



                                                                                                                                                   <!--a  id="addbutton" class="btn btn btn-success btn-sm " data-toggle="modal" data-target="#largeModel" data-animation="zoomInUp">
                                                                                                                                                    <span class="fa fa-plus-circle"></span>
                                                                                                                                                    AJOUTER CALENDRIER</a-->


                                                                                                                                                   <div class="modal fade" id="largeModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 					    <div class="modal-dialog modal-lg" role="document">
 					        <div class="modal-content">
 					            <div class="modal-header">
 					                <h4 class="modal-title" id="exampleModalLabel"><?php echo L::CourseCalendar ?></h4>
                           <button type="button" id="bntnclose" class="close" data-dismiss="modal" aria-label="Close">
 					                    <span aria-hidden="true">&times;</span>
 					                </button>

 					            </div>
 					            <div class="modal-body">
 					                <div id="divrecherchestation">

                             <div class="form-group row">
                                     <label class="control-label col-md-3"><?php echo L::DateSeances ?>
                                         <span class="required"> * </span>
                                     </label>
                                     <div class="col-md-5">
                                         <input type="date" name="dateseance" id="dateseance" data-required="1" placeholder="<?php echo L::DateSeances ?>" class="form-control input-height" />
                                         <p id="messagedateseance" style=""></p>
                                       </div>

                                 </div>
                                 <div class="form-group row">
                                     <label class="control-label col-md-3"><?php echo L::Seances ?>
                                         <span class="required"> * </span>
                                     </label>
                                     <div class="col-md-5">
                                         <input type="text" name="seance" id="seance" data-required="1" placeholder="<?php echo L::Seances ?>" class="form-control input-height" />
                                         <p id="messageseance"></p>
                                       </div>

                                 </div>
                                 <div class="row">
                                                     <div class="col-md-12">
                                 <div class="form-group">
                                 <label for=""><?php echo L::contenuPedago ?><span class="required"> * </span> </label>
                                    <textarea class="form-control" rows="8" name="contepeda" id="contepeda" placeholder="<?php echo L::Adddescriptcourses ?>"></textarea>
                                      <p id="messagecontepeda"></p>
                                 </div>

                                 </div>
                                </div>
                                <div class="row">
                                                    <div class="col-md-12">
                                <div class="form-group">
                                <label for=""><?php echo L::Prealableworks ?><span class="required"> * </span> </label>
                                   <textarea class="form-control" rows="8" name="travailpre" id="travailpre" placeholder=""></textarea>
                                   <p id="messagetravailpre"></p>
                                </div>

                                </div>
                               </div>

                               <button type="button" id="btnAccepter"  onclick="valideCalendar()" class="btn btn-md btn-primary" ><?php echo L::Validate ?></button>
                               <button type="button"  id="btnAnnuler"  class="btn btn-md btn-danger" ><?php echo L::AnnulerBtn ?></button>


                           </div>
 					            </div>
 					            <div class="modal-footer">
 					                <button type="button" class="btn btn-secondary" id="close" data-dismiss="modal"><?php echo L::Closebtn ?></button>
 					                <button type="button" class="btn btn-primary"><?php echo L::Saving ?></button>
 					            </div>
 					        </div>
 					    </div>
 					</div>

           <div class="col-md-12">
                                         					                            <div class="card  card-box">
                                         					                                <div class="card-head">
                                         					                                    <header></header>
                                         					                                    <div class="tools">
                                         					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                         						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                         						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                         					                                    </div>
                                         					                                </div>
                                         					                                <div class="card-body ">

                                         					                                  <div class="table-scrollable">
                                                                                       <input type="hidden" name="nbcalandar" id="nbcalandar"  value="0">
                                                                                       <input type="hidden" name="concatcalendar" id="concatcalendar" value="">
                                                                                       <input type="hidden" name="concatnbcalandar" id="concatnbcalandar" value="">
                                                                                       <input type="hidden" name="dateseancetab" id="dateseancetab" value="">
                                                                                       <input type="hidden" name="seancetab" id="seancetab" value="">
                                                                                       <input type="hidden" name="pedagotab" id="pedagotab" value="">
                                                                                       <input type="hidden" name="prealabtab" id="prealabtab" value="">
                                                                                       <?php
                                                                                         $Calendars=$etabs->getSyllabusCalendarInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);
                                                                                         // $arrayObj=json_encode($syllabusInfos,true);
                                                                                         // $ObjetifArray = json_decode($arrayObj, true);
                                                                                        ?>
                                                                                       <table  class="table table-hover table-checkable order-column full-width" id="example4">
                                         					                                        <thead>
                                         					                                            <tr>

                                                                                                   <th style="text-align:center;"><?php echo L::LittleDateVersements ?></th>
                                         					                                                <th style="text-align:center;"><?php echo L::SeanceNb ?> </th>
                                                                                                   <th style="text-align:center;"><?php echo L::contenuPedago ?> </th>
                                                                                                   <th style="text-align:center;"><?php echo L::Prealableworks ?> </th>
                                                                                                   <!--th> Actions </th-->
                                         					                                            </tr>
                                         					                                        </thead>
                                         					                                        <tbody id="tabStationCalendar">
                                                                                            <?php foreach ($Calendars as $value): ?>
                                                                                           <tr>
                                                                                           <td style="text-align:center;"><?php echo $value->date_syllabcal?></td>
                                                                                           <td style="text-align:center;"><?php echo $value->seance_syllabcal ?></td>
                                                                                           <td style="text-align:center;"><?php echo $value->contenu_syllabcal?></td>
                                                                                           <td style="text-align:center;"><?php echo $value->prealable_syllabcal ?></td>

                                                                                           </tr>
                                                                                            <?php endforeach; ?>
                                         															                    </tbody>

                                         					                                    </table>
                                         					                                    </div>
                                         					                                </div>
                                         					                            </div>
                                         					                        </div>

                                                                           <br>
                                                                           <div class="row">
                                                                           <div class="col-md-12">

                                                                           <div style="background-color:#007bff;">

                                                                           <center><legend style="color:white;"><?php echo L::EvaluationMode ?> </legend></center>
                                                                         </div>


                                                                           </div>
                                                                           </div> <br>


                                                                            <input type="hidden" name="nbevaluation" id="nbevaluation" value="0">
                                                                            <input type="hidden" name="concatevaluation" id="concatevaluation" value="">
                                                                            <input type="hidden" name="concatnbevaluation" id="concatnbevaluation" value="">
                                                                            <?php
                                                                              $Calendars=$etabs->getSyllabusModeEvalInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);
                                                                              // $arrayObj=json_encode($syllabusInfos,true);
                                                                              // $ObjetifArray = json_decode($arrayObj, true);
                                                                             ?>
                                                                            <table class="table table-hover table-checkable order-column full-width" id="example5">
                                                                                <thead>
                                                                                    <tr>

                                                                                        <th style="text-align:center;width:200px"><?php echo L::LittleDateVersements ?></th>
                                                                                        <th style="text-align:center;"><?php echo L::EvaluationType ?> </th>
                                                                                        <th style="text-align:center;"><?php echo L::CompetenceViseeNew ?> </th>
                                                                                        <th style="text-align:center;"><?php echo L::Ponderation ?></th>
                                                                                        <!--th style="text-align:center;">Actions</th-->

                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody id="tabStationEvaluation">
                                                                                  <?php foreach ($Calendars as $value): ?>
                                                                                 <tr>
                                                                                 <td style="text-align:center;"><?php echo $value->date_syllabeval?></td>
                                                                                 <td style="text-align:center;"><?php echo $value->type_syllabeval ?></td>
                                                                                 <td style="text-align:center;"><?php echo $value->competence_syllabeval?></td>
                                                                                 <td style="text-align:center;"><?php echo $value->ponderation_syllabeval ?></td>

                                                                                 </tr>
                                                                                  <?php endforeach; ?>
                                                                                </tbody>

                                                                            </table>
                                                                            <br><br>

                                                                            <div class="row">
                                                                            <div class="col-md-12">

                                                                            <div style="background-color:#007bff;">

                                                                            <center><legend style="color:white;"><?php echo L::CoursesRules ?> </legend></center>
                                                                          </div>


                                                                            </div>
                                                                            </div> <br>

                                                                            <div class="row">
                                                                                                <div class="col-md-12">
                                                                            <div class="form-group">
                                                                            <label for=""><b><?php echo L::Rules ?>  <span class="required"> * </span>: </b></label>

                                                                            </div>
                                                                            <input type="hidden" id="nbregl" name="nbregl" value="0">
                                                                            <input type="hidden" name="concatregl" id="concatregl"  value="">
                                                                            <input type="hidden" name="concatnbregl" id="concatnbregl" value="">
                                                                            </div>
                                                                            </div>
                                                                            <?php
                                                                              $Regles=$etabs->getSyllabusRegleInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);
                                                                              // $arrayObj=json_encode($syllabusInfos,true);
                                                                              // $ObjetifArray = json_decode($arrayObj, true);
                                                                             ?>

                                                                            <table class="table" id="dynamic_field6" border=0>
                                                                              <?php foreach ($Regles as $value): ?>
                                                                             <tr>
                                                                             <td><?php echo $value->libelle_syllabregle ?></td>

                                                                             </tr>
                                                                              <?php endforeach; ?>
                                                                                                         </table>
                                                                                                         <br>


                                         					                    </div>




                                       <div class="form-group">
                                           <div class="offset-md-3 col-md-9">
                                               <button type="button" class="btn btn-warning"> <i class="fa fa-print"></i> <?php echo L::Printer ?></button>
                                               <button type="button"  onclick="RetourForm(<?php echo $_GET['teatcher']; ?>,'<?php echo $_GET['codeEtab']; ?>')" class="btn btn-danger"><?php echo L::TurnBack ?></button>
                                           </div>
                                       </div>
                                   </form>
                              </div>
                          </div>
                      </div>
                  </div>


                      <!-- start new patient list -->

                     <!-- end new patient list -->

                 </div>
             </div>
             <!-- end page content -->
             <!-- start chat sidebar -->

             <!-- end chat sidebar -->
         </div>
         <!-- end page container -->
         <!-- start footer -->
         <div class="page-footer">
             <div class="page-footer-inner"> 2019 &copy;
             <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
             </div>
             <div class="scroll-to-top">
                 <i class="material-icons">eject</i>
             </div>
         </div>
         <!-- end footer -->
     </div>
     <!-- start js include path -->
     <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  	<script src="../assets2/plugins/popper/popper.min.js" ></script>
      <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
  	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
      <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
      <!-- bootstrap -->
      <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
      <!-- calendar -->
      <script src="../assets2/plugins/moment/moment.min.js" ></script>
      <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
      <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
      <script src="../assets2/plugins/select2/js/select2.js" ></script>
      <script src="../assets2/js/pages/select2/select2-init.js" ></script>
      <!-- Common js-->
      <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
    	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
    	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
  	<script src="../assets2/js/app.js" ></script>
      <script src="../assets2/js/layout.js" ></script>
      <script src="../assets2/js/jquery-dateformat.js" ></script>
  	<script src="../assets2/js/theme-color.js" ></script>
  	<!-- Material -->
  	<script src="../assets2/plugins/material/material.min.js"></script>
   <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>







     <!-- morris chart -->
     <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
     <script src="../assets2/plugins/morris/raphael-min.js" ></script>
     <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->


     <script type="text/javascript">

     function addFrench()
     {
       var etape=1;
       var lang="fr";
       $.ajax({
         url: '../ajax/langue.php',
         type: 'POST',
         async:false,
         data: 'etape=' + etape+ '&lang=' +lang,
         dataType: 'text',
         success: function (content, statut) {

     window.location.reload();

         }
       });
     }

     function addEnglish()
     {
       var etape=1;
       var lang="en";
       $.ajax({
         url: '../ajax/langue.php',
         type: 'POST',
         async:false,
         data: 'etape=' + etape+ '&lang=' +lang,
         dataType: 'text',
         success: function (content, statut) {

     window.location.reload();

         }
       });
     }

     $("#classe").select2();
     $("#matiere").select2();


     function RetourForm(teatcher,codeEtab)
     {
        var lien="programmelistes.php?teatcher="+teatcher+"&codeEtab="+codeEtab;


        //window.location.lien;

        document.location.href="programmelistes.php?teatcher="+teatcher+"&codeEtab="+codeEtab
     }


     function searchmatiere(id)
     {

         var classe=$("#classe").val();
         var teatcherId=id;
         var etape=6;


       $.ajax({

            url: '../ajax/matiere.php',
            type: 'POST',
            async:true,
            data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe,
            dataType: 'text',
            success: function (content, statut) {


              $("#matiere").html("");
              $("#matiere").html(content);

            }
          });
     }

     function searchcodeEtab(id)
     {
       var classe=$("#classe").val();
       var teatcherId=id;
       var etape=7;
       var matiere=$("#matiere").val();

       $.ajax({

            url: '../ajax/matiere.php',
            type: 'POST',
            async:true,
            data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
            dataType: 'text',
            success: function (content, statut) {


               $("#codeEtab").val(content);

            }
          });

     }

     function recalculobjectifnb()
     {
       var concatobjectif=$("#concatobjectif").val();

       var tab=concatobjectif.split("@");

       var nbtab=tab.length;

       var nbtabnew=parseInt(nbtab)-1;

       $("#concatnbobjectif").val(nbtabnew);


     }

     function AddobjectifRow()
     {
       var nb=$("#nb").val();
       var nouveau= parseInt(nb)+1;
       $("#nb").val(nouveau);

       var concatobjectif=$("#concatobjectif").val();

       $("#concatobjectif").val(concatobjectif+nouveau+"@");

       recalculobjectifnb();



       $('#dynamic_field').append('<tr id="rowObj'+nouveau+'"><td><input type="text" name="objectif_'+nouveau+'" id="objectif_'+nouveau+'" placeholder="<?php echo L::AddObjectifRequired ?>" class="form-control objectif_list" /></td><td><button type="button" id="deleteObj'+nouveau+'" id="deleteObj'+nouveau+'"  onclick="deletedObj('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');

       for(var i=1;i<=nouveau;i++)
       {
         $("#objectif_"+i).rules( "add", {
             required: true,
             messages: {
             required: "<?php echo L::RequiredChamp ?>"
   }
           });
       }
     }

     function recalculthemenb()
     {
       var concatcontenu=$("#concatcontenu").val();

       var tab=concatcontenu.split("@");

       var nbtab=tab.length;

       var nbtabnew=parseInt(nbtab)-1;

       $("#concatnbcontenu").val(nbtabnew);
     }

     function deletedTheme(id)
     {
       var concatcontenu=$("#concatcontenu").val();

       $("#concatcontenu").val($("#concatcontenu").val().replace(id+"@", ""));

        $('#rowThem'+id+'').remove();

        recalculthemenb();
     }

     function AddthemeRow()
     {
       var nb=$("#nbcontenu").val();
       var nouveau= parseInt(nb)+1;
       $("#nbcontenu").val(nouveau);

       var concatcontenu=$("#concatcontenu").val();

       $("#concatcontenu").val(concatcontenu+nouveau+"@");

       recalculthemenb();


       $('#dynamic_field1').append('<tr id="rowThem'+nouveau+'"><td><input type="text" name="theme_'+nouveau+'" id="theme_'+nouveau+'" placeholder="<?php echo L::AddTopicRequired ?>" class="form-control theme_list" /></td><td><button type="button" name="remove" id="'+nouveau+'" onclick="deletedTheme('+nouveau+')" class="btn btn-danger btn_remove">X</button></td></tr>');

       for(var i=1;i<=nouveau;i++)
       {
         $("#theme_"+i).rules( "add", {
             required: true,
             messages: {
             required: "<?php echo L::RequiredChamp ?>"
   }
           });
       }
     }

     function recalculrequisnb()
     {
       var concatrequis=$("#concatrequis").val();

       var tab=concatrequis.split("@");

       var nbtab=tab.length;

       var nbtabnew=parseInt(nbtab)-1;

       $("#concatnbrequis").val(nbtabnew);
     }

     function deletedRequis(id)
     {
       var concatrequis=$("#concatrequis").val();

       $("#concatrequis").val($("#concatrequis").val().replace(id+"@", ""));

        $('#rowRequis'+id+'').remove();

        recalculrequisnb();
     }


     function AddrequisRow()
     {
       var nb=$("#nbrequis").val();
       var nouveau= parseInt(nb)+1;
       $("#nbrequis").val(nouveau);

       var concatrequis=$("#concatrequis").val();

       $("#concatrequis").val(concatrequis+nouveau+"@");

       recalculrequisnb();



       $('#dynamic_field2').append('<tr id="rowRequis'+nouveau+'"><td><input type="text" name="requis_'+nouveau+'" id="requis_'+nouveau+'" placeholder="<?php echo L::AddprereqRequired ?>" class="form-control requis_list" /></td><td><button type="button" name="remove" id="'+nouveau+'" onclick="deletedRequis('+nouveau+')" class="btn btn-danger btn_remove">X</button></td></tr>');

       for(var i=1;i<=nouveau;i++)
       {
         $("#requis_"+i).rules( "add", {
             required: true,
             messages: {
             required: "<?php echo L::RequiredChamp ?>"
   }
           });
       }
     }

     function recalculcompnb()
     {
       var concatcomp=$("#concatcomp").val();

       var tab=concatcomp.split("@");

       var nbtab=tab.length;

       var nbtabnew=parseInt(nbtab)-1;

       $("#concatnbcomp").val(nbtabnew);
     }

     function deletedComp(id)
     {
       var concatcomp=$("#concatcomp").val();

       $("#concatcomp").val($("#concatcomp").val().replace(id+"@", ""));

        $('#rowComp'+id+'').remove();

        recalculcompnb();
     }

     function AddcompRow()
     {
       var nb=$("#nbcomp").val();
       var nouveau= parseInt(nb)+1;
       $("#nbcomp").val(nouveau);

       var concatcomp=$("#concatcomp").val();

       $("#concatcomp").val(concatcomp+nouveau+"@");

       recalculcompnb();



       $('#dynamic_field3').append('<tr id="rowComp'+nouveau+'"><td><input type="text" name="comp_'+nouveau+'" id="comp_'+nouveau+'" placeholder="<?php echo L::AddCompViseeRequired ?>" class="form-control comp_list" /></td><td><button type="button" name="remove" id="'+nouveau+'" onclick="deletedComp('+nouveau+')" class="btn btn-danger btn_remove">X</button></td></tr>');

       for(var i=1;i<=nouveau;i++)
       {
         $("#comp_"+i).rules( "add", {
             required: true,
             messages: {
             required: "<?php echo L::RequiredChamp ?>"
   }
           });
       }
     }

     // function deletedDoc

     function recalculdocnb()
     {
       var concatdoc=$("#concatdoc").val();

       var tab=concatdoc.split("@");

       var nbtab=tab.length;

       var nbtabnew=parseInt(nbtab)-1;

       $("#concatnbdoc").val(nbtabnew);
     }

     function deletedDoc(id)
     {
       var concatdoc=$("#concatdoc").val();

       $("#concatdoc").val($("#concatdoc").val().replace(id+"@", ""));

        $('#rowDoc'+id+'').remove();

        recalculdocnb();
     }

     function AddDocumentRow()
     {
       var nb=$("#nbdoc").val();
       var nouveau= parseInt(nb)+1;
       $("#nbdoc").val(nouveau);


       var concatdoc=$("#concatdoc").val();

       $("#concatdoc").val(concatdoc+nouveau+"@");

       recalculdocnb();


       $('#dynamic_field4').append('<tr id="rowDoc'+nouveau+'"><td><input type="text" name="doc_'+nouveau+'" id="doc_'+nouveau+'" placeholder="<?php echo L::AddDocumentRequired ?>" class="form-control doc_list" /></td><td><button type="button" name="remove" id="'+nouveau+'" onclick="deletedDoc('+nouveau+')" class="btn btn-danger btn_remove">X</button></td></tr>');

       for(var i=1;i<=nouveau;i++)
       {
         $("#doc_"+i).rules( "add", {
             required: true,
             messages: {
             required: "<?php echo L::RequiredChamp ?>"
   }
           });
       }

     }

     function retirerCalendar(id)
     {
       var concatcalendar=$("#concatcalendar").val();

       $("#concatcalendar").val($("#concatcalendar").val().replace(id+"@", ""));

        $('#ligneCalendar'+id+'').remove();

       recalculcalendarnb();
     }

     function deletedfacdoc(id)
     {
       var concatdocfac=$("#concatdocfac").val();

       $("#concatdocfac").val($("#concatdocfac").val().replace(id+"@", ""));

        $('#rowDocfac'+id+'').remove();

        recalculdocfacnb();
     }

     function deletedRegle(id)
     {
       var concatregl=$("#concatregl").val();

       $("#concatregl").val($("#concatregl").val().replace(id+"@", ""));

        $('#rowRegl'+id+'').remove();

        recalculreglenb();
     }

     function recalculcalendarnb()
     {
       var concatcalendar=$("#concatcalendar").val();

       var tab=concatcalendar.split("@");

       var nbtab=tab.length;

       var nbtabnew=parseInt(nbtab)-1;

       $("#concatnbcalandar").val(nbtabnew);
     }

     function recalculdocfacnb()
     {
       var concatdocfac=$("#concatdocfac").val();

       var tab=concatdocfac.split("@");

       var nbtab=tab.length;

       var nbtabnew=parseInt(nbtab)-1;

       $("#concatnbdocfac").val(nbtabnew);
     }

     function recalculevalnb()
     {
       var concatevaluation=$("#concatevaluation").val();

       var tab=concatevaluation.split("@");

       var nbtab=tab.length;

       var nbtabnew=parseInt(nbtab)-1;

       $("#concatnbevaluation").val(nbtabnew);
     }

     function recalculreglenb()
     {
       var concatregl=$("#concatregl").val();

       var tab=concatregl.split("@");

       var nbtab=tab.length;

       var nbtabnew=parseInt(nbtab)-1;

       $("#concatnbregl").val(nbtabnew);
     }

     function AddfacDocumentRow()
     {
       var nb=$("#nbdocfac").val();
       var nouveau= parseInt(nb)+1;
       $("#nbdocfac").val(nouveau);

       var concatdocfac=$("#concatdocfac").val();

       $("#concatdocfac").val(concatdocfac+nouveau+"@");

       recalculdocfacnb();


       $('#dynamic_field5').append('<tr id="rowDocfac'+nouveau+'"><td><input type="text" name="docfac_'+nouveau+'" id="docfac_'+nouveau+'" placeholder="<?php echo L::AddDocumentRequired ?>" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+nouveau+'"  onclick="deletedfacdoc('+nouveau+')" class="btn btn-danger btn_remove">X</button></td></tr>');

       for(var i=1;i<=nouveau;i++)
       {
         $("#docfac_"+i).rules( "add", {
             required: true,
             messages: {
             required: "<?php echo L::RequiredChamp ?>"
   }
           });
       }

     }

     function AddEvaluationRow()
     {
       var nb=$("#nbevaluation").val();
       var nouveau= parseInt(nb)+1;

          var ligne = "<tr id=\"ligneEval"+nouveau+"\" ondblclick=\"retirerEval("+nouveau+")\">";
          ligne = ligne + $("#ligne" +nouveau).html();
           ligne = ligne + "<td align=\"center\"><input type=\"text\" id=\"dateEvaluation_"+nouveau+"\" name=\"dateEvaluation_"+nouveau+"\" class=\"dateEval\"></td>";

          // ligne = ligne + "<td align=\"center\"><input type=\"text\" name=\"dateEvaluation"+nouveau+"\"></td>";
          ligne = ligne + "<td align=\"center\"><input type=\"text\" id=\"typeEvaluation_"+nouveau+"\" name=\"typeEvaluation_"+nouveau+"\" class=\"typeEval\"></td>";
          ligne = ligne + "<td align=\"center\"><input type=\"text\" id=\"competenceEvaluation_"+nouveau+"\" name=\"competenceEvaluation_"+nouveau+"\" class=\"compEval\"></td>";
          ligne = ligne + "<td align=\"center\"><input type=\"number\" min=\"1\" max=\"100\" id=\"ponderationEvaluation_"+nouveau+"\" name=\"ponderationEvaluation_"+nouveau+"\" class=\"pondEval\"></td>";
     // ligne = ligne + "<td align=\"center\"> <button type=\"button\" name=\"btn_remove"+nouveau+"\" id=\"btn_remove"+nouveau+"\" class=\"btn btn-danger btn_remove\" >X</button></td>";
          ligne = ligne + "</tr>";

          $('#tabStationEvaluation').append(ligne);

          $("#nbevaluation").val(nouveau);

          var concatevaluation=$("#concatevaluation").val();

          $("#concatevaluation").val(concatevaluation+nouveau+"@");

          recalculevalnb();


          for(var i=1;i<=nouveau;i++)
          {
            $("#dateEvaluation_"+i).rules( "add", {
                required: true,
                messages: {
                required: "<?php echo L::RequiredChamp ?>"
      }
              });


              $("#dateEvaluation_"+i).bootstrapMaterialDatePicker
              ({
                date:true,
                shortTime: false,
                format: 'DD-MM-YYYY',
                lang: 'fr',
               cancelText: '<?php echo L::AnnulerBtn?>',
               okText: '<?php echo L::Okay ?>',
               clearText: '<?php echo L::Eraser ?>',
               nowText: '<?php echo L::Now ?>'

              });

              $("#typeEvaluation_"+i).rules( "add", {
                  required: true,
                  messages: {
                  required: "<?php echo L::RequiredChamp ?>"
        }
                });

                $("#competenceEvaluation_"+i).rules( "add", {
                    required: true,
                    messages: {
                    required: "<?php echo L::RequiredChamp ?>"
          }
                  });

                  $("#ponderationEvaluation_"+i).rules( "add", {
                      required: true,
                      messages: {
                      required: "<?php echo L::RequiredChamp ?>"
            }
                    });
          }

     }

     function AddCalendarRow()
     {
       var nb=$("#nbcalandar").val();
       var nouveau= parseInt(nb)+1;

          var ligne = "<tr id=\"ligneCalendar"+nouveau+"\" ondblclick=\"retirerCalendar("+nouveau+")\">";
          ligne = ligne + $("#ligne" +nouveau).html();
           ligne = ligne + "<td align=\"center\" ><input type=\"\" id=\"dateCalandar_"+nouveau+"\" name=\"dateCalandar_"+nouveau+"\"  ></td>";
           //
          // ligne = ligne + "<td align=\"center\"><input type=\"text\" name=\"dateEvaluation"+nouveau+"\"></td>";
           // ligne = ligne + "<td align=\"center\"><input type=\"text\" id=\"contentCalandar_"+nouveau+"\" name=\"contentCalandar_"+nouveau+"\" ></td>";
          // ligne = ligne + "<td align=\"center\"><input type=\"text\" id=\"workCalandar_"+nouveau+"\" name=\"workCalandar_"+nouveau+"\" ></td>";
          ligne = ligne + "<td align=\"center\"><input type=\"text\" id=\"seanceCalandar_"+nouveau+"\" name=\"seanceCalandar_"+nouveau+"\" ></td>";
          ligne = ligne + "<td align=\"center\"><textarea id=\"contentCalandar_"+nouveau+"\" name=\"contentCalandar_"+nouveau+"\" rows=\"5\" cols=\"25\"></textarea></td>";
          ligne = ligne + "<td align=\"center\"><textarea id=\"workCalandar_"+nouveau+"\" name=\"workCalandar_"+nouveau+"\" rows=\"5\" cols=\"25\"></textarea></td>";
          // ligne = ligne + "<td align=\"center\"> <button type=\"button\" name=\"btn_remove"+nouveau+"\" id=\"btn_remove"+nouveau+"\" class=\"btn btn-danger btn_remove\" onclick=\"retirerCalendar("+nouveau+")\">X</button></td>";
          // ligne = ligne + "</tr>";

          $('#tabStationCalendar').append(ligne);

          $("#nbcalandar").val(nouveau);

          var concatcalendar=$("#concatcalendar").val();

          $("#concatcalendar").val(concatcalendar+nouveau+"@");

          recalculcalendarnb();



          for(var i=1;i<=nouveau;i++)
          {
            $("#dateCalandar_"+i).rules( "add", {
                required: true,
                messages: {
                required: "<?php echo L::RequiredChamp ?>"
      }
              });

              $("#dateCalandar_"+i).bootstrapMaterialDatePicker
              ({
                date:true,
                shortTime: false,
                format: 'DD-MM-YYYY',
                lang: 'fr',
                cancelText: '<?php echo L::AnnulerBtn?>',
                okText: '<?php echo L::Okay ?>',
                clearText: '<?php echo L::Eraser ?>',
                nowText: '<?php echo L::Now ?>'

              });

              $("#seanceCalandar_"+i).rules( "add", {
                  required: true,
                  messages: {
                  required: "<?php echo L::RequiredChamp ?>"
        }
                });

                $("#contentCalandar_"+i).rules( "add", {
                    required: true,
                    messages: {
                    required: "<?php echo L::RequiredChamp ?>"
          }
                  });

                  $("#workCalandar_"+i).rules( "add", {
                      required: true,
                      messages: {
                      required: "<?php echo L::RequiredChamp ?>"
            }
                    });
          }

     }

     function AddRegleRow()
     {
       var nb=$("#nbregl").val();
       var nouveau= parseInt(nb)+1;
       $("#nbregl").val(nouveau);

       $("#nbcalandar").val(nouveau);

       var concatregl=$("#concatregl").val();

       $("#concatregl").val(concatregl+nouveau+"@");

       recalculreglenb();

       $('#dynamic_field6').append('<tr id="rowRegl'+nouveau+'"><td><input type="text" name="regle_'+nouveau+'" id="regle_'+nouveau+'" placeholder="<?php echo L::AddRulesRequired ?>" class="form-control regle_list" /></td><td><button type="button" name="remove" id="'+nouveau+'" onclick="deletedRegle('+nouveau+')" class="btn btn-danger btn_remove">X</button></td></tr>');

       for(var i=1;i<=nouveau;i++)
       {
         $("#regle_"+i).rules( "add", {
             required: true,
             messages: {
             required: "<?php echo L::RequiredChamp ?>"
   }
           });
       }

     }

     function retirerEval(id)
     {
       var concatevaluation=$("#concatevaluation").val();

       $("#concatevaluation").val($("#concatevaluation").val().replace(id+"@", ""));

        $('#ligneEval'+id+'').remove();

        recalculevalnb();
     }



 function valideCalendar()
 {


     var dateseance=$("#dateseance").val();

     if(dateseance=="")
      {
        document.getElementById("messagedateseance").innerHTML = "<font color=\"red\"><?php echo L::SeanceDateRequired ?></font>";

      }

      var seance=$("#seance").val();

      if(seance=="")
       {
         document.getElementById("messageseance").innerHTML = "<font color=\"red\"><?php echo L::SeanceRequired ?></font>";

       }

       var contepeda=$("#contepeda").val();

       if(contepeda=="")
        {
          document.getElementById("messagecontepeda").innerHTML = "<font color=\"red\"><?php echo L::PedagocontenuRequired ?></font>";

        }

        var travailpre=$("#travailpre").val();

        if(travailpre=="")
         {
           document.getElementById("messagetravailpre").innerHTML = "<font color=\"red\"><?php echo L::PreviousworksRequired ?></font>";

         }


         if(travailpre!="" && contepeda!="" && seance!="" && dateseance!="")
         {
           var nbcalandar=$("#nbcalandar").val();
           var nouveaunbcalandar=parseInt(nbcalandar)+1;
           var prealabtab=$("#prealabtab").val();
           var pedagotab=$("#pedagotab").val();
           var seancetab=$("#seancetab").val();
           var dateseancetab=$("#dateseancetab").val();

           var concattravailpre=nouveaunbcalandar+"@"+travailpre;
           var concatcontepeda=nouveaunbcalandar+"@"+contepeda;
           var concatseance=nouveaunbcalandar+"@"+seance;
           var concatdateseance=nouveaunbcalandar+"@"+dateseance;

 // date.format("YYYY-MM-DD")
            var ligne = "<tr id=\"ligneselect"+nouveaunbcalandar+"\">";
             ligne = ligne + $("#ligne" +nouveaunbcalandar).html();
             ligne = ligne + "<td align=\"center\">"+ dateseance+"</td>"
             ligne = ligne + "<td align=\"center\">"+ seance+"</td>"
             ligne = ligne + "<td align=\"center\">"+ contepeda+"</td>"
             ligne = ligne + "<td align=\"center\">"+ travailpre+"</td>"
             ligne = ligne + "<td class=\"visible-lg\"><a onclick=\"retirerStation("+nouveaunbcalandar+",'"+dateseance+"','"+seance+"','"+contepeda+"','"+travailpre+"')\" style=\"color:red;font-weight:normal; cursor:pointer\"><span class=\"fa fa-minus-circle\"></span>&nbsp;Retirer</a> </td>";
             ligne = ligne + "</tr>"

             $("#prealabtab").val($("#prealabtab").val()+concattravailpre+",");
             $("#pedagotab").val($("#pedagotab").val()+concatcontepeda+",");

             $("#seancetab").val($("#seancetab").val()+concatseance+",");
             $("#dateseancetab").val($("#dateseancetab").val()+concatdateseance+",");

             $("#dateseance").val("");
             $("#seance").val("");
             $("#contepeda").val("");
             $("#travailpre").val("");


              $("#aucuneLinge").slideUp();

             $("#tabStationSelectBody").append(ligne);

             $("#divafficherinfosstation").css("display", "none");
             $("#largeModel .close").click();




         }


 }

 function retirerStation(nouveaunbcalandar,dateseance,seance,contepeda,travailpre)
 {
   var nbcalandar=$("#nbcalandar").val();
   var nouveaunbcalandararetirer=parseInt(nbcalandar)-1;

   var concattravailpre=nouveaunbcalandar+"@"+travailpre;
   var concatcontepeda=nouveaunbcalandar+"@"+contepeda;
   var concatseance=nouveaunbcalandar+"@"+seance;
   var concatdateseance=nouveaunbcalandar+"@"+dateseance;

   var lignearetirer=$("#ligneselect"+nouveaunbcalandar).val();

   $("#prealabtab").val($("#prealabtab").val().replace(concattravailpre+",", ""));

   // $("#prealabtab").val($("#prealabtab").val().replace(concattravailpre+",", ""));
   // $("#pedagotab").val($("#pedagotab").val().replace(concatcontepeda+",", ""));
   //
   // $("#seancetab").val($("#seancetab").replace(concatseance+",", ""));
   // $("#dateseancetab").val($("#dateseancetab").val().replace(concatdateseance+",", ""));

   $("#ligneselect"+nouveaunbcalandar).remove();

   if(nouveaunbcalandararetirer==0)
   {
     var ligne = "<tr>";
     ligne = ligne + "<td colspan=5><?php echo L::Nodatass ?></td>";
     ligne = ligne + "</tr>";

       $("#tabStationSelectBody").append(ligne);
   }

 }

 function deletedObj(id)
 {
   var concatobjectif=$("#concatobjectif").val();

   $("#concatobjectif").val($("#concatobjectif").val().replace(id+"@", ""));

    $('#rowObj'+id+'').remove();

    recalculobjectifnb();

 }


    $(document).ready(function() {

      var i=1;

      // AddobjectifRow();
      // AddthemeRow();
      // AddrequisRow();
      // AddcompRow();
      // AddDocumentRow();
      // AddEvaluationRow();
      // AddRegleRow();
      // AddCalendarRow();

        $('#add').click(function(){

          AddobjectifRow();

        });
        // $(document).on('click', '.btn_remove', function(){
        //      var button_id = $(this).attr("id");
        //      $('#row'+button_id+'').remove();
        //      var nb=$("#nb").val();
        //    	var nouveau= parseInt(nb)-1;
        //      $("#nb").val(nouveau);
        // });

        //Contenu

        $('#addtheme').click(function(){
          AddthemeRow();


        });
        // $(document).on('click', '.btn_remove', function(){
        //      var button_id = $(this).attr("id");
        //      $('#row'+button_id+'').remove();
        //      var nb=$("#nbcontenu").val();
        //    	var nouveau= parseInt(nb)-1;
        //      $("#nbcontenu").val(nouveau);
        // });

        //prerequis

        $('#addrequis').click(function(){
             AddrequisRow();

        });
        // $(document).on('click', '.btn_remove', function(){
        //      var button_id = $(this).attr("id");
        //      $('#row'+button_id+'').remove();
        //      var nb=$("#nbrequis").val();
        //    	var nouveau= parseInt(nb)-1;
        //      $("#nbrequis").val(nouveau);
        // });

        //competences visées

        $('#addcomp').click(function(){
             AddcompRow();

        });
        // $(document).on('click', '.btn_remove', function(){
        //      var button_id = $(this).attr("id");
        //      $('#row'+button_id+'').remove();
        //      var nb=$("#nbcomp").val();
        //     var nouveau= parseInt(nb)-1;
        //      $("#nbcomp").val(nouveau);
        // });
 // documents obligatoires




 $('#adddoc').click(function(){
   AddDocumentRow();

 });
 // $(document).on('click', '.btn_remove', function(){
 //      var button_id = $(this).attr("id");
 //      $('#row'+button_id+'').remove();
 //      var nb=$("#nbdoc").val();
 //     var nouveau= parseInt(nb)-1;
 //      $("#nbdoc").val(nouveau);
 // });

 //document facultatif

 $('#adddocfac').click(function(){
   AddfacDocumentRow();

 });
 // $(document).on('click', '.btn_remove', function(){
 //      var button_id = $(this).attr("id");
 //      $('#row'+button_id+'').remove();
 //      var nb=$("#nbdocfac").val();
 //     var nouveau= parseInt(nb)-1;
 //      $("#nbdocfac").val(nouveau);
 // });

 $('#addregle').click(function(){

 AddRegleRow();
 });
 $(document).on('click', '.btn_remove', function(){
      var button_id = $(this).attr("id");
      $('#row'+button_id+'').remove();
      var nb=$("#nbregl").val();
     var nouveau= parseInt(nb)-1;
      $("#nbregl").val(nouveau);
 });



 $('#addbuttonEval').click(function(){

 AddEvaluationRow();
 });

 $('#addcalendarbtn').click(function(){
   AddCalendarRow();

 });



    });

    $("#syllabForm").validate({

      errorPlacement: function(label, element) {
      label.addClass('mt-2 text-danger');
      label.insertAfter(element);
    },
    highlight: function(element, errorClass) {
      $(element).parent().addClass('has-danger')
      $(element).addClass('form-control-danger')
    },
     rules:{
       descri:"required",
       classe:"required",
       matiere:"required"
     },
     messages:{
       descri:"<?php echo L::RequiredChamp ?>",
         classe:"<?php echo L::RequiredChamp ?>",
         matiere:"<?php echo L::RequiredChamp ?>"
     }

    });

    // $('.objectif_list').each(function() {
    //       $(this).rules("add",
    //           {
    //               required: true,
    //               messages: {
    //                   required: "<?php echo L::RequiredChamp ?>",
    //               }
    //           });
    //
    //
    //   });

 $("#syllabForm").on('submit', function(event) {

   $('.typeEval').each(function() {
         $(this).rules("add",
             {
                 required: true,
                 messages: {
                     required: "<?php echo L::RequiredChamp ?>",
                 }
             });
     });
     //
     $('.pondEval').each(function() {
           $(this).rules("add",
               {
                   required: true,
                   messages: {
                       required: "<?php echo L::RequiredChamp ?>",
                   }
               });
       });

       $('.compEval').each(function() {
             $(this).rules("add",
                 {
                     required: true,
                     messages: {
                         required: "<?php echo L::RequiredChamp ?>",
                     }
                 });
         });

         $('.dateEval').each(function() {
               $(this).rules("add",
                   {
                       required: true,
                       messages: {
                           required: "<?php echo L::RequiredChamp ?>",
                       }
                   });
           });

           //Objectifs



     //
     //         $('.theme_list').each(function() {
     //               $(this).rules("add",
     //                   {
     //                       required: true,
     //                       messages: {
     //                           required: "<?php echo L::RequiredChamp ?>",
     //                       }
     //                   });
     //           });
     //
     //           $('.requis_list').each(function() {
     //                 $(this).rules("add",
     //                     {
     //                         required: true,
     //                         messages: {
     //                             required: "<?php echo L::RequiredChamp ?>",
     //                         }
     //                     });
     //             });
     //
     //             $('.comp_list').each(function() {
     //                   $(this).rules("add",
     //                       {
     //                           required: true,
     //                           messages: {
     //                               required: "<?php echo L::RequiredChamp ?>",
     //                           }
     //                       });
     //
     //                       $(this).css( "backgroundColor", "yellow" );
     //               });
     //
     //               $('.doc_list').each(function() {
     //                     $(this).rules("add",
     //                         {
     //                             required: true,
     //                             messages: {
     //                                 required: "<?php echo L::RequiredChamp ?>",
     //                             }
     //                         });
     //                 });
     //
     //                 $('.regle_list').each(function() {
     //                       $(this).rules("add",
     //                           {
     //                               required: true,
     //                               messages: {
     //                                   required: "<?php echo L::RequiredChamp ?>",
     //                               }
     //                           });
     //                   });
     //
     //                   $('.objectifdep').each(function() {
     //                         $(this).rules("add",
     //                             {
     //                                 required: true,
     //                                 messages: {
     //                                     required: "<?php echo L::RequiredChamp ?>",
     //                                 }
     //                             });
     //                     });
     //
     //
     //
     //
     //


 });












    </script>
     <!-- end js include path -->
   </body>

 </html>
