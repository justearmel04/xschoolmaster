<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Teatcher.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$userId=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$matiere=new Matiere();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement
$schoolsofassign=$etabs->getEtablissementbyCodeEtab($codeEtabAssigner);

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$codesEtab=$schoolsofassign;
$codeEtabLocal=$etabs->getcodeEtabByLocalId($userId);
$classes=$classe->getAllClassesbyschoolCode($codeEtabLocal);
$teatchers=$teatcher->getAllTeatchersBySchoolCode($codeEtabLocal);
$matieres=$matiere->getAllMatiereOfThisSchool($codeEtabLocal);

$examens=$etabs->getAllExamensOfThisSchool($codeEtabLocal);



$etablissementType=$etabs->DetermineTypeEtab($codeEtabLocal);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabLocal);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabLocal);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];

  $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);

  $parascos=$etabs->getAllParascolairesOfThisSchool($codeEtabLocal,$libellesessionencours);
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabLocal,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabLocal,$libellesessionencours);
}



// session_start();
// require_once('../class/User.php');
// require_once('../class/Parent.php');
// require_once('../class/Classe.php');
//
// require_once('../class/Teatcher.php');
// require_once('../class/Matiere.php');
// $etab=new Etab();
// $parent=new ParentX();
// $user=new User();
// $classe=new Classe();

// $emailUti=$_SESSION['user']['email'];
//
// $imageprofile=$user->getImageProfilebyId($_SESSION['user']['IdCompte'])
// $logindata=$user->getLoginProfile($emailUti);
// $tablogin=explode("*",$logindata);
// $datastat=$user->getStatis();
// $tabstat=explode("*",$datastat);
// $classeId=$_GET['classe'];
//
//
// if(strlen($imageprofile)>0)
// {
//   $lienphoto="../photo/".$emailUti."/".$imageprofile;
// }else {
//   $lienphoto="../photo/user5.jpg";
// }
//
// $parents=$parent->getAllParent();
//

 // var_dump($parascos);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
    <!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!--bootstrap -->
    <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

    <!-- data tables -->
    <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>

    <!-- Material Design Lite CSS -->
    <link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
    <link href="../assets2/css/material_style.css" rel="stylesheet">

    <!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- wizard -->
    	<link rel="stylesheet" href="../assets2/css/pages/steps.css">

    <!-- Theme Styles -->
      <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
      <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />

      <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <!-- Theme Styles -->

    <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />





 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::CreateNotifications ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="#"><?php echo L::NotificationMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::CreateNotifications ?></li>
                          </ol>
                      </div>
                  </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">
              <?php

                    if(isset($_SESSION['user']['addprogra']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
              <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
              <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: '<?php echo L::Felicitations ?>',
                text: '<?php echo $_SESSION['user']['addprogra'] ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addprogra']);
                    }

                     ?>
              <?php

                    if(isset($_SESSION['user']['addlocalok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: '<?php echo L::Felicitations ?>',
  text: "<?php echo $_SESSION['user']['addlocalok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::AddNews ?>',
  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="index.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addlocalok']);
                    }

                     ?>
<br/>

                </div>
            </div>


            <!-- corps -->
            <div class="row">
              <div class="col-sm-12">
                     <div class="card-box">
                         <div class="card-head">
                             <header></header>
                         </div>
                         <div class="card-body ">
                           <form class="" action="../controller/messaging.php" method="post" id="FormAddExam">


                               <h3><?php echo L::Stepone ?></h3>
                               <fieldset>
                                 <div class="">
                                   <span><b><?php echo L::ParentsNotes ?></b></span>
                                 </div>
                                 <div class="form-group row">
                                    <label class="control-label col-md-3"> <?php echo L::LittleDateVersements ?>
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-5">
                          <input type="text" class="form-control " id="dateobjet" id="dateobjet" name="titre" placeholder="date" style="width:100%">
                          <input type="hidden" name="etape" id="etape"  value="1">
                          <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner; ?>">
                          <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">

                                    </div>
                             </div>

                                 <div class="form-group row">
                                    <label class="control-label col-md-3"> <?php echo L::ObjetNotifications ?>
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-5">
                                        <select class="form-control input-height" id="objet" name="objet" style="width:100%" onchange="checkobjet()" >
                                           <option value=""> <?php echo L::SelectObjetNotifications ?> </option>
                                           <option value="1-Reminder"><?php echo L::ReminderNotifications ?> </option>
                                           <option value="2-Conference"><?php echo L::ConferenceNotifications ?> </option>
                                           <option value="3-Devoir non rendu"> <?php echo L::DevoirNogiveNotifications ?> </option>
                                           <option value="4-Absence"> <?php echo L::AbsenceNotifications ?> </option>
                                           <option value="5-Résultat d’évaluation"> <?php echo L::EvaluationResultNotifications ?> </option>
                                           <option value="6-Discipline"> <?php echo L::DisciplneNotifications ?> </option>
                                           <option value="7-Participation en classe"> <?php echo L::ParticipationNotifications ?> </option>
                                           <option value="8-Autre"> <?php echo L::OthersNotifications ?>  </option>

                                       </select>
                                      </div>
                             </div>
                             <div class="form-group row">
                                 <label class="control-label col-md-3">
                                     <span class="required"></span>
                                 </label>
                                 <div class="col-md-6">
                                   <table class="table" id="dynamic_otherobjet" border=0>

                                     </table>

                                  </div>
                             </div>

                         <div class="form-group row">

                                       <label class="control-label col-md-3"> <?php echo mb_strtolower(L::Comments) ?>
                                           <span class="required"> * </span>
                                       </label>
                                       <div class="col-md-5">
                                         <textarea class="form-control" name="commentaire" id="commentaire" rows="7" cols="60" maxlength="160" placeholder=""></textarea>

                                       </div>

                                   </div>






                               </fieldset>
                                  <h3><?php echo L::Steptwo ?></h3>
                                  <fieldset>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3"><?php echo L::ClasseconcerneNotifications ?>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-5">
                                          <select class="form-control input-height" multiple="multiple" id="classeEtab" name="classeEtab[]" style="width:100%" onchange="erased()">

                                              <?php
                                              $i=1;
                                                foreach ($classes as $value):
                                                ?>
                                                <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                <?php
                                                                                 $i++;
                                                                                 endforeach;
                                                                                 ?>

                                          </select>
                                          <p id="messageselectclasse"></p>
                                          </div>
                                    </div>
                                    <div class="form-group row">
                                       <label class="control-label col-md-3"><?php echo L::DestinatairesNotifications ?>
                                           <span class="required"> * </span>
                                       </label>
                                       <div class="col-md-5">
                                           <select class="form-control input-height" id="destinataires" name="destinataires" style="width:100%" onchange="determinedestinataires()" >
                                              <option value="" > <?php echo L::SelectDestinatairesNotifications ?> </option>
                                              <option value="1" selected ><?php echo L::Alls ?></option>
                                              <option value="2"><?php echo L::PrecisNotifications ?></option>

                                          </select>
                                          <p class="required col-md-12"><?php echo L::TousInfosPreNotifications ?> <br> <?php echo L::TousInfosPostNotifications ?></p>

                                          <input type="hidden" name="nbkd" id="nbkd" value="1">
                                          <input type="hidden" name="concatnbkd" id="concatnbkd" value="1@">
                                          <input type="hidden" name="concatNbrekd" id="concatNbrekd" value="0">
                                         </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-md-3">
                                        <span class="required"></span>
                                    </label>
                                    <div class="col-md-6">
                                      <table class="table" id="dynamic_desti" border=0>
                                        <tr id="precisionRow">
                                          <td><select class="form-control input-height" multiple="multiple" id="eleves" name="eleves[]" style="width:100%" >
                                             <option value="0" selected><?php echo L::Alls ?></option>

                                         </select></td>
                                        </tr>
                                        </table>

                                     </div>

                                </div>

                                <div class="form-group row">
                                       <div class="col-md-3">

                                       </div>

                                         <div class="custom-control custom-checkbox custom-control-inline " >
                           <input type="checkbox" class="custom-control-input" id="smssender" name="smssender" onclick='checksms()'>
                           <input type="hidden" name="smsvalue" id="smsvalue"  value="0">
                           <label class="custom-control-label" for="smssender"><?php echo L::SmsNotifications ?></label>

                                         </div>


                                         <div class="custom-control custom-checkbox custom-control-inline">
                           <input type="checkbox" class="custom-control-input" id="emailsender" name="emailsender" onclick='checkmail()'>
                           <input type="hidden" name="emailvalue" id="emailvalue"  value="0">
                           <label class="custom-control-label" for="emailsender"><?php echo L::EmailNotifications ?></label>

                                         </div>





                                     </div>
                                     <div class="row">
                                       <div class="col-md-3">

                                       </div>
                                       <div class="col-md-6">
                                         <p id="messagesnotificationchoice"></p>
                                       </div>


                                     </div>
                                     <div class="form-group row">
                                        <label class="control-label col-md-3"> <?php echo L::ModesavingNotifications ?>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-5">
                                            <select class="form-control input-height" id="sauvegarde" name="sauvegarde" style="width:100%" onchange="changeEtape()" >
                                               <option value="" > <?php echo L::SelectModesavingNotifications ?> </option>
                                               <option value="1" selected ><?php echo L::SaveAndsendAfterNotifications ?></option>
                                               <option value="2"><?php echo L::SaveAndsendNotifications ?></option>

                                           </select>


                                          </div>
                                 </div>






                           </form>

                         </div>
                     </div>
                 </div>
            </div>
            <!-- corps -->

            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
    <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
	<script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js"></script>
    <!-- bootstrap -->
<script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
<script src="../assets2/plugins/select2/js/select2.js" ></script>
<script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <!-- wizard -->
  <script src="../assets2/plugins/steps/jquery.steps.js" ></script>
 <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
 <script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
 <script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
     <!-- calendar -->
  <script src="../assets2/plugins/moment/moment.min.js" ></script>
  <script src="../assets2/js/app.js" ></script>
  <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/jquery-dateformat.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>

<script type="text/javascript">

function addFrench()
{
  var etape=1;
  var lang="fr";
  $.ajax({
    url: '../ajax/langue.php',
    type: 'POST',
    async:false,
    data: 'etape=' + etape+ '&lang=' +lang,
    dataType: 'text',
    success: function (content, statut) {

window.location.reload();

    }
  });
}

function addEnglish()
{
  var etape=1;
  var lang="en";
  $.ajax({
    url: '../ajax/langue.php',
    type: 'POST',
    async:false,
    data: 'etape=' + etape+ '&lang=' +lang,
    dataType: 'text',
    success: function (content, statut) {

window.location.reload();

    }
  });
}

function changeEtape()
{
  var sauvegarde=$("#sauvegarde").val();

  $("#etape").val(sauvegarde);
}

function createOther()
{
    var ligne="<tr id=\"ligneotherobjet\">";
    ligne=ligne+"<td> ";
    ligne=ligne+"<input  placeholder=\"<?php echo L::PrecisObjetNotifications ?>\" name=\"otherobjet\" id=\"otherobjet\"  value=\"\" class=\"form-control\" style=\"width:83%\">";
    ligne=ligne+"</td>";
    ligne=ligne+"</tr>";

    $("#dynamic_otherobjet").append(ligne);

    $("#otherobjet").rules( "add", {
        required: true,
        messages: {
        required: "<?php echo L::RequiredChamp ?>"
  }
      });

}

function deleteOther()
{
  $("#ligneotherobjet").remove();
}

function checkobjet()
{
  var objet=$("#objet").val();
  var tabobjet=objet.split("-");

  var objetid=tabobjet[0];

  if(objetid==8)
  {
      createOther();
  }else {
    deleteOther();
  }

}

function deletedestinatairesRow()
{
// $("#precisionRow").remove();

  var content="<option value=\"\" selected><?php echo L::Alls ?></option>";
  $("#eleves").html("");
  $("#eleves").html(content);

}

function createdestinatairesRow()
{
  // var ligne="<div class=\"form-group row\" id=\"kendiv\">";
  // ligne=ligne+"<label class=\"control-label col-md-3\">Elèves<span class=\"required\"> * </span></label>";
  // ligne=ligne+"<div class=\"col-md-5\">";
  // ligne=ligne+" <input type=\"text\"  placeholder=\"Entrer le cocntact du responsable\" name=\"contactrespo\" id=\"contactrespo\"  value=\"\" class=\"form-control input-height\">";
  // ligne=ligne+"<div>";
  // ligne=ligne+"<div>";

  //nous allons chercher la liste des eleves de ces classes

  var classeselected=$("#classeEtab").val();
  var session=$("#libellesession").val();
  var etape=6;
  $.ajax({
    url: '../ajax/admission.php',
    type: 'POST',
    async:true,
    data: 'classe=' +classeselected+ '&etape=' + etape+'&session='+session,
    dataType: 'text',
    success: function (content, statut) {

      // var ligne="<tr id=\"precisionRow\">";
      // ligne=ligne+"<td>";
      // ligne=ligne+"<select class=\"form-control input-height\" id=\"eleves\" name=\"eleves[]\" style=\"width:100%\" >";
      // ligne=ligne+content;
      // ligne=ligne+"</select>";
      // // ligne=ligne+" <input type=\"text\"  placeholder=\"Entrer le cocntact du responsable\" name=\"contactrespo\" id=\"contactrespo\"  value=\"\" class=\"form-control input-height\">";
      // ligne=ligne+"</td>";
      // ligne=ligne+"</tr>";

      // $('#dynamic_field').append('<tr id="rowSection'+nouveau+'"><td><input type="text" name="section_'+nouveau+'" id="section_'+nouveau+'" placeholder="<?php echo L::EnterSection ?>" class="form-control objectif_list" /></td><td><button type="button" id="deleteSection'+nouveau+'" id="deleteSection'+nouveau+'"  onclick="deletedSection('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');

      $("#eleves").html("");
      $("#eleves").html(content);

      // $("#dynamic_desti").append(ligne);

    }
  });


}

function determinedestinataires()
{
  var destinataires=$("#destinataires").val();
  //nous allons recuperer la valeur des classes selectionnées

  var classeselected=$("#classeEtab").val();

  if(classeselected=="")
  {
    Swal.fire({
    type: 'warning',
    title: '<?php echo L::WarningLib ?>',
    text: "<?php echo L::SelectclasseconcernerNotifications ?>",

  })
  }else {

    if(destinataires==1)
    {
      //alert('tous');

      //nous allons recuperer la valeur stocker pour voir si elle correspond a tous ou precis

        var concatnbkd=$("#concatnbkd").val();
        var newconcatnbkd=1+"@";
        var donnne=0;
        $("#concatnbkd").val(newconcatnbkd);
        $("#concatNbrekd").val(donnne);
        deletedestinatairesRow();


    }else if(destinataires==2)
    {
        //nous allons recuperer la valeur stocker pour voir si elle correspond a tous ou precis

        var concatnbkd=$("#concatnbkd").val();
        var newconcatnbkd=2+"@";
        var donnne=1;
        $("#concatnbkd").val(newconcatnbkd);
        $("#concatNbrekd").val(donnne);
        createdestinatairesRow();

    }
  }



}


 function checksms()
 {


   if($('#smssender').prop('checked') == true){
    $("#smsvalue").val(1);

  document.getElementById("messagesnotificationchoice").innerHTML = "";
  }
  else {
    $("#smsvalue").val(0);
    if($("#emailvalue").val()==0)
    {
      document.getElementById("messagesnotificationchoice").innerHTML = "<font color=\"red\"><?php echo L::PleaseChooseNotificationSendingType ?></font>";
    }


  }

 }

 function checkmail()
 {


   if($('#emailsender').prop('checked') == true){
    $("#emailvalue").val(1);
    document.getElementById("messagesnotificationchoice").innerHTML = "";
  }
  else {

    $("#emailvalue").val(0);

    if($("#smsvalue").val()==0)
    {
      document.getElementById("messagesnotificationchoice").innerHTML = "<font color=\"red\"><?php echo L::PleaseChooseNotificationSendingType ?></font>";
    }
  }



 }

 function erased1()
 {

   document.getElementById("messageselectdesti").innerHTML = "";
 }

 function erased()
 {
   document.getElementById("messageselectclasse").innerHTML = "";
 }

 function check()
 {
   var classe=$("#classeEtab").val();
   var desti=$("#destinataires").val();
   var smsvalue=$("#smsvalue").val();
   var emailvalue=$("#emailvalue").val();

   if(classe!="" && desti!="")
   {
     //$("#FormAddExam").submit();

     if(smsvalue==0 && emailvalue==0)
     {
       document.getElementById("messagesnotificationchoice").innerHTML = "<font color=\"red\"><?php echo L::PleaseChooseNotificationSendingType ?></font>";
     }else {
       $("#FormAddExam").submit();
     }

   }else if(classe=="" || desti=="") {

     if(classe=="" )
     {
       document.getElementById("messageselectclasse").innerHTML = "<font color=\"red\"><?php echo L::PleaseSelectaumoinsClasse ?></font>";

     }

     if(desti=="")
     {
       document.getElementById("messageselectdesti").innerHTML = "<font color=\"red\"><?php echo L::PleaseSelectaumoinsDestinataires ?></font>";

     }

   }
 }

function affectergrat()
{
  $("#paiecheck").val(0);
  $("#gratuitcheck").val(1);
  document.querySelector("#montantdiv").style.display="none";


}

function affichemontant()
{
  document.querySelector("#montantdiv").style.display="block";
  $("#paiecheck").val(1);
  $("#gratuitcheck").val(0);

  $("#montantAct").rules( "add", {
      required: true,
      messages: {
      required: "<?php echo L::RequiredChamp ?>"
}
    });



}

// $('.joinfile').dropify({
//     messages: {
//         'default': 'Selectionner un fichier joint',
//         'replace': 'Remplacer le fichier joint',
//         'remove':  'Retirer',
//         'error':   'Ooops, Une erreur est survenue.'
//     }
// });




function supprimer()
{
  var classeEtab=$("#classeEtab").val();

  alert(classeEtab);
}

function recalculnbkdnb()
{

}

function create()
{
  var classeEtab=$("#classeEtab").val();
  var nb=$("#nbkd").val();
  var nouveau= parseInt(nb)+1;
  $("#nbkd").val(nouveau);
  var concatnbkd=$("#concatnbkd").val();
  $("#concatnbkd").val(concatnbkd+nouveau+"@");

  recalculnbkdnb();

}

function deletedInteret(id)
{
  var concatinteret=$("#concatinteret").val();

  $("#concatinteret").val($("#concatinteret").val().replace(id+"@", ""));

   $('#rowInteret'+id+'').remove();

   recalculinteretnb();
}

function recalculinteretnb()
{
  var concatinteret=$("#concatinteret").val();

  var tab=concatinteret.split("@");

  var nbtab=tab.length;

  var nbtabnew=parseInt(nbtab)-1;

  $("#concatnbinteret").val(nbtabnew);
}


  function AddinteretRow()
  {
    var nb=$("#nb").val();
    var nouveau= parseInt(nb)+1;
    $("#nb").val(nouveau);

      var concatinteret=$("#concatinteret").val();
      $("#concatinteret").val(concatinteret+nouveau+"@");

      recalculinteretnb();

      $('#dynamic_field').append('<tr id="rowInteret'+nouveau+'"><td><input type="text" name="interet_'+nouveau+'" id="interet_'+nouveau+'" placeholder="Entrer un interet" class="form-control objectif_list" /></td><td><button type="button" id="deleteInteret'+nouveau+'" id="deleteInteret'+nouveau+'"  onclick="deletedInteret('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');

      for(var i=1;i<=nouveau;i++)
      {
        $("#interet_"+i).rules( "add", {
            required: true,
            messages: {
            required: "<?php echo L::RequiredChamp ?>"
  }
          });
      }


  }






  $(document).ready(function() {
    "use strict";
    var wizard = $("#wizard_test").steps();
    var i=1;

    var form = $("#FormAddExam").show();

    form.validate({
      errorPlacement: function(label, element) {
      label.addClass('mt-2 text-danger');
      label.insertAfter(element);
    },
    highlight: function(element, errorClass) {
      $(element).parent().addClass('has-danger')
      $(element).addClass('form-control-danger')
    },
    success: function (e) {
          $(e).closest('.control-group').removeClass('error').addClass('info');
          $(e).remove();
      },

      rules:{
        classeEtab:"required",
        matiere:"required",
        classe:"required",
        teatcher:"required",
        coef:"required",
        examen:"required",
        datedeb:"required",
        datefin:"required",
        typesess:"required",
        montantAct:"required",
        libelactivity:"required",
        message:"required",
        denomination:"required",
        typeactivite:"required",
        locationactivite:"required",
        descripactivite:"required",
        respoactivite:"required",
        contactrespo:"required",
        destinataires:"required",
        eleves:"required",
        dateobjet:"required",
        objet:"required",
        commentaire:"required"





      },
      messages: {
        classeEtab:"<?php echo L::PleaseSelectaumoinsClasse ?>",
        matiere:"<?php echo L::PleaseEnterMatiere ?>",
        classe:"<?php echo L::PleaseSelectclasserequired ?>",
        teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
        coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
        examen:"<?php echo L::PleaseEnterExamLib ?>",
        datedeb:"<?php echo L::PleaseEnterParascoActivityDateStart ?>",
        datefin:"<?php echo L::PleaseEnterParascoActivityDateEnd ?>",
        typesess:"<?php echo L::PeriodRequired ?>",
        montantAct:"<?php echo L::PleaseEnterParascoActivityAmount ?>",
        libelactivity:"<?php echo L::PleaseEnterParascoActivityName ?>",
        message:"<?php echo L::PleaseEnterActivityMesaage ?>",
        denomination:"<?php echo L::PleaseEnterActivityName ?>",
        typeactivite:"<?php echo L::PleaseEnterActivityType ?>",
        locationactivite:"<?php echo L::PleaseEnterActivityLocation ?>",
        descripactivite:"<?php echo L::PleaseEnterActivityDescription ?>",
        respoactivite:"<?php echo L::PleaseEnterRespoNameMessage ?>",
        contactrespo:"<?php echo L::PleaseEnterContactRespoMessage ?>",
        destinataires:"<?php echo L::PleaseSelectaumoinsDestinatairesMessage ?>",
        eleves:"<?php echo L::PleaseSelectStudents ?>",
        dateobjet:"<?php echo L::ControlsDaterequired ?>",
        objet:"<?php echo L::PleaseSelectObjetMessage ?>",
        commentaire:"<?php echo L::PleaseEnterCommentMessage ?>"


      }
    });

    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "slideLeft",
        onStepChanging: function (event, currentIndex, newIndex)
         {
                     // Allways allow previous action even if the current form is not valid!
              if (currentIndex > newIndex)
              {
                  return true;
              }

              // Forbid next action on "Warning" step if the user is to young
              if (newIndex === 2 && Number($("#nbselect").val()) < 0)
              {
                  return false;
              }
              // Needed in some cases if the user went back (clean up)
              if (currentIndex < newIndex)
              {
                  // To remove error styles
                  form.find(".body:eq(" + newIndex + ") label.error").remove();
                  form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
              }
              form.validate().settings.ignore = ":disabled,:hidden";
              return form.valid();


         },
   onStepChanged: function (event, currentIndex, priorIndex)
   {
       // Used to skip the "Warning" step if the user is old enough.
       if (currentIndex === 2 && Number($("#nbselect").val()) >0)
       {
           form.steps("next");
       }

   },
   onFinishing: function (event, currentIndex)
   {
       form.validate().settings.ignore = ":disabled";
       return form.valid();
   },
   onFinished: function (event, currentIndex)
   {
     check();
     // form.submit();

   }


    });



    $("#classe").select2();
    $("#teatcher").select2();
  $("#destinataires").select2();
  $("#sauvegarde").select2();

    $("#classeEtab").select2({
      tags: true,
    tokenSeparators: [',', ' ']
    });

    $("#eleves").select2({
      tags: true,
    tokenSeparators: [',', ' ']
    });

    $("#objet").select2();
    $("#typesess").select2();
    $("#typeactivite").select2();

       $('#heuredeb').bootstrapMaterialDatePicker
     ({
       date: true,
       shortTime: false,
       format: 'YYYY-MM-DD',
       lang: 'fr',
      cancelText: '<?php echo L::AnnulerBtn ?>',
      okText: '<?php echo L::Okay ?>',
      clearText: '<?php echo L::Eraser ?>',
      nowText: '<?php echo L::Now ?>'

 });

 var d = new Date();
 var month = d.getMonth();
 var day = d.getDate();
 var year = d.getFullYear();

 var output = d.getFullYear() + '-' +
(month<10 ? '0' : '') + month + '-' +
(day<10 ? '0' : '') + day;

 $('#dateobjet').bootstrapMaterialDatePicker
({
 date: true,
 shortTime: false,
 time: false,
 format: 'YYYY-MM-DD',
 lang: 'fr',
cancelText: '<?php echo L::AnnulerBtn ?>',
okText: '<?php echo L::Okay ?>',
clearText: '<?php echo L::Eraser ?>',
nowText: '<?php echo L::Now ?>',
switchOnClick : true

});

$('#dateobjet').val(output);




 $('#heurefin').bootstrapMaterialDatePicker
   ({
     date: true,
     shortTime: false,
     format: 'YYYY-MM-DD HH:mm',
     lang: 'fr',
    cancelText: '<?php echo L::AnnulerBtn ?>',
    okText: '<?php echo L::Okay ?>',
    clearText: '<?php echo L::Eraser ?>',
    nowText: '<?php echo L::Now ?>'

});

    $('#add').click(function(){

           //creation d'une ligne de section

           AddinteretRow();

         });

  });

</script>
    <!-- end js include path -->
  </body>

</html>
