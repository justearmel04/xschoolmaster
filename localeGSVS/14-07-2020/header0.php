<header class="page-topbar" id="header">
  <div class="navbar navbar-fixed ">
<nav
  class="navbar-main navbar-color nav-collapsible sideNav-lock  navbar-dark gradient-45deg-light-blue-cyan ">
  <div class="nav-wrapper">
    <ul class="left">
      <li>
        <h1 class="logo-wrapper">
          <a class="brand-logo darken-1" href="index.php">
            <img src="../images/logo/materialize-logo.png" alt="materialize logo">
            <!-- <img src="../images/logo/logo.png" alt="materialize logo"> -->
            <span class="logo-text hide-on-med-and-down">
                              Reporting
                            </span>
          </a>
        </h1>
      </li>
    </ul>
    <!-- <div class="header-search-wrapper hide-on-med-and-down">
      <i class="material-icons">search</i>
      <input class="header-search-input z-depth-2" type="text" name="Search" placeholder="Explore Materialize"
        data-search="template-list">
      <ul class="search-list collection display-none"></ul>
    </div> -->
    <ul class="navbar-list right">

      <li class="hide-on-med-and-down">
        <a class="waves-effect waves-block waves-light toggle-fullscreen" href="javascript:void(0);">
          <i class="material-icons">settings_overscan</i>
        </a>
      </li>
      <li class="hide-on-large-only">
        <a class="waves-effect waves-block waves-light search-button" href="javascript:void(0);">
          <i class="material-icons">search </i>
        </a>
      </li>

      <li>
        <a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);"
          data-target="profile-dropdown">
          <span class="avatar-status avatar-online">
            <img src="../images/avatar/avatar-7.png" alt="avatar">
            <i></i>
          </span>
        </a>
      </li>

    </ul>

    <!-- profile-dropdown-->
    <ul class="dropdown-content" id="profile-dropdown">
      <li>
        <a class="grey-text text-darken-1" href="#">
          <i class="material-icons">person_outline</i>
          Profile
        </a>
      </li>

      <li class="divider"></li>

      <li>
        <a class="grey-text text-darken-1" href="../">
          <i class="material-icons">keyboard_tab</i>
          Logout
        </a>
      </li>
    </ul>
  </div>

</nav>
<!-- BEGIN: Horizontal nav start-->
<nav class="white hide-on-med-and-down" id="horizontal-nav">
  <div class="nav-wrapper">
    <ul class="left hide-on-med-and-down" id="ul-horizontal-nav" data-menu="menu-navigation">

                              <li>
        <a class="dropdown" href="index.php" >
          <i class="material-icons">dashboard</i>
          <span>
            <span class="dropdown-title" data-i18n="Dashboard">Dashboard</span>

                        </span>
        </a>

                </li>
                      <li>
        <a class="dropdown" href="etablissements.php" >
          <i class="material-icons">dvr</i>
          <span>
            <span class="dropdown-title" data-i18n="Templates">Etablissements</span>

                        </span>
        </a>
                                    </li>

                                    <li>
                                    <a class="dropdown" href="visites.php" >
                                    <i class="material-icons">chrome_reader_mode</i>
                                    <span>
                                    <span class="dropdown-title" data-i18n="Templates">Visites</span>

                                      </span>
                                    </a>
                                                  </li>
                                                  <li>
                                                  <a class="dropdown" href="rdvs.php" >
                                                  <i class="material-icons">mail_outline</i>
                                                  <span>
                                                  <span class="dropdown-title" data-i18n="Templates">Rendez-vous</span>

                                                    </span>
                                                  </a>
                                                                </li>



                    </ul>
  </div>
  <!-- END: Horizontal nav start-->
</nav>
</div>


</header>
