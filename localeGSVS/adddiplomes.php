<?php
session_start();
require_once('../class/User.php');
require_once('../class/Diplome.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();
$etabs=new Etab();
$classe=new Classe();
$user=new User();
$diplome=new Diplome();
$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}
$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}



$diplomes=$diplome->getDiplomebyTeatcherId($_GET['compte']);
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title"><?php echo L::TeatcherDegree ?></div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#"><?php echo L::ProfsMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active"><?php echo L::AddDegree ?></li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['adddiplomeok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: '<?php echo L::Felicitations ?>',
  text: "<?php echo $_SESSION['user']['adddiplomeok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::AddNews ?> ',
  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="detailsteatcher.php?compte="+<?php echo $_GET['compte']?>;
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['adddiplomeok']);
                    }

                     ?>

              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <div class="card card-box">
                          <div class="card-head">
                              <header></header>
                               <!--button id = "panel-button"
                             class = "mdl-button mdl-js-button mdl-button--icon pull-right"
                             data-upgraded = ",MaterialButton">
                             <i class = "material-icons">more_vert</i>
                          </button>
                          <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                             data-mdl-for = "panel-button">
                             <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                             <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                             <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                          </ul-->
                          </div>

                          <div class="card-body" id="bar-parent">

                              <div class="row">
              <div class="col-md-12">

          <fieldset style="background-color:#007bff;">

          <center><legend style="color:white;"><?php echo L::Degreelist ?></legend></center>
          </fieldset>


          </div>
          </div><br/>
          <a href="#" class="mdl-button  mdl-button--raised mdl-js-ripple-effect  btn-warning"
											data-toggle="modal" data-target="#exampleModal">

      <span class="fa fa-plus"><?php echo L::Degree ?></span>

  </a></br></br>
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					    <div class="modal-dialog modal-lg" role="document">
					        <div class="modal-content">
					            <div class="modal-header">
					                <h4 class="modal-title" id="exampleModalLabel"><?php echo L::AddDegree ?></h4>
					                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					                    <span aria-hidden="true">&times;</span>
					                </button>
					            </div>
					            <div class="modal-body">
                        <form action="../controller/diplomes.php" id="FormDiplAdd" method="post" enctype="multipart/form-data">
                        <div class="form-group row">
                                <label class="control-label col-md-3"><?php echo L::DegreeDesignation ?>
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" name="libellediplo" id="libellediplo" data-required="1" placeholder="<?php echo L::EnterDegreeDesignation ?>'" class="form-control input-height" /> </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-3"><?php echo L::Ecole ?>
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" name="libecole" id="libecole" data-required="1" placeholder="<?php echo L::EnterEcole ?>" class="form-control input-height" /> </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-3"><?php echo L::Speciality ?>
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" name="speciale" id="speciale" data-required="1" placeholder="<?php echo L::EnterSpeciality ?>" class="form-control input-height" /> </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-3"><?php echo L::Level ?>
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" name="nivodiplo" id="nivodiplo" data-required="1" placeholder="<?php echo L::EnterLevel ?>" class="form-control input-height" />
                                    <input type="hidden" name="etape" id="etape" value="1"/>
                                    <input type="hidden" name="idcompe" id="idcompte" value="<?php echo $_GET['compte']?>"/>
                                  </div>

                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-3"><?php echo L::DateObtain ?>
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                  <input type="date" placeholder="<?php echo L::EnterDateObtain ?>" name="dateo" id="dateo" data-mask="99/99/9999" class="form-control input-height">
                                    <span class="help-block"><?php echo L::Datesymbole ?></span></div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="control-label col-md-3"><?php echo L::File ?>
                                          <span class="required"> * </span>
                                      </label>
                                      <div class="col-md-8">
                                      <input type="file" id="fichier" name="fichier" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb"  data-allowed-file-extensions="pdf doc docx" />
                                    </div>
                                        </div>
                                        <div class="form-actions">
                                                              <div class="row">
                                                                  <div class="offset-md-3 col-md-9">
                                                                      <button type="submit" class="btn btn-info"><?php echo L::AddMenu ?></button>
                                                                      <button type="button" class="btn btn-danger"><?php echo L::Closebtn ?></button>
                                                                  </div>
                                                                </div>
                                                             </div>
                          </form>
					            </div>

					        </div>
					    </div>
					</div>
  <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
      <thead>
          <tr>


              <th> <?php echo L::DegreeDesignation ?> </th>
              <th> <?php echo L::Ecole ?> </th>
              <th> <?php echo L::Speciality ?> </th>
              <th> <?php echo L::Level ?></th>
              <th> <?php echo L::DateObtain ?> </th>
              <th> <?php echo L::File ?> </th>
              <th> <?php echo L::Actions ?> </th>
          </tr>
      </thead>
      <tbody>
        <?php
        $i=1;
          foreach ($diplomes as $value):
          ?>
          <tr class="odd gradeX">

            <td>
              <?php echo $value->libelle_diplo;?>
            </td>
              <td>
<?php echo $value->ecole_diplo;?>
              </td>
              <td>
                  <span class="label label-sm label-default"><?php echo $value->specialite_diplo;?>  </span>
              </td>
              <td>
<?php echo $value->niveau_diplo;?>
              </td>
              <td>
                <?php echo date_format(date_create($value->dateobt_diplo),"d/m/Y");?>
              </td>
                <td>
                  <?php
                  if(strlen($value->fichier_diplo)>0)
                  {
                    $emailTea=$diplome->getTeatcherMail($_GET['compte']);
                    $lien="../diplomes/professeur/".$emailTea."/".$value->fichier_diplo;
                  ?>
                <center>  <a href="<?php echo $lien;?>" target="_blank" class="btn btn-success" title="<?php echo L::SeeFile?>"><span class="fa fa-eye"></span></a></center>
                  <?php
                  }else {
                    ?>
                <center>  <a href="#" target="_blank" class="btn btn-danger" title="<?php echo L::SeeFile ?>"><span class="fa fa-times"></span></a></center>
                    <?php
                  }
                   ?>
                </td>
              <td class="">
                  <div class="btn-group">
                    <a href="" class="btn btn-primary" title="<?php echo mb_strtolower(L::Details); ?>" ><i class="fa fa-exclamation-circle"></i></a>
                    <a href="#" onclick="modify('<?php echo $_GET['compte']?>')"class="btn btn-warning" title="Modifier"><i class="fa fa-pencil"></i></a>
                    <a href="#" onclick="" class="btn btn-danger" title="<?php echo L::DeleteLib ?>"><i class="fa fa-trash-o"></i></a>

                  </div>
              </td>
          </tr>
          <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
          <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
          <script>

          function myFunction(idcompte)
          {
            //var url="detailslocal.php?compte="+idcompte;
          document.location.href="detailsdiplome.php?compte="+idcompte;
          }

          function modify(id)
          {


            Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::DoyouReallyUpdateDegree ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::ModifierBtn ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
document.location.href="updateteatcher.php?compte="+id;
}else {

}
})
          }

          function deleted(id)
          {

            Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::DoyouReallyDeleteDegree ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::DeleteLib ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
document.location.href="../controller/teatcher.php?etape=3&compte="+id;
}else {

}
})
          }

          </script>
          <?php
                                           $i++;
                                           endforeach;
                                           ?>


      </tbody>
  </table>

                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
          </div>

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <!-- Material -->
 <!--script src="../assets2/plugins/material/material.min.js"></script-->
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>


 <script>
 function SetcodeEtab(codeEtab)
 {
   var etape=3;
   $.ajax({
     url: '../ajax/sessions.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }
 function addFrench()
 {
   var etape=1;
   var lang="fr";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function addEnglish()
 {
   var etape=1;
   var lang="en";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 jQuery(document).ready(function() {

   $("#FormDiplAdd").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{


        libellediplo:"required",
        libecole:"required",
        speciale:"required",
        nivodiplo:"required",
        dateo:"required",
        fichier:"required"

      },
      messages: {

        libellediplo:"<?php echo L::PleaseEnterDegreeLib ?>",
        libecole:"<?php echo L::PleaseEnterEcoleLib ?> ",
        speciale:"<?php echo L::PleaseEnterSpeciality ?>",
        nivodiplo:"<?php echo L::PleaseEnterLevelDegree ?>",
        dateo:"<?php echo L::PleaseEnterDateObtainDegree ?>",
        fichier:"<?php echo L::PleasechooseFileDegree ?>"
       },

       submitHandler: function(form) {

        form.submit();


       }


   });


 });
 </script>
    <!-- end js include path -->
  </body>

</html>
