<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

  if($_SESSION['user']['groupe']==1)
  {
    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
    {
      $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);

      //nous allons chercher la liste des etablissements du groupe

      $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
        }

        if(strlen($_SESSION['user']['codeEtab'])>0)
        {

        }else {
          $_SESSION['user']['codeEtab']=$codeEtabAssigner;
        }

        $_SESSION['user']['session']=$libellesessionencours;





         // var_dump($allcodeEtabs);

        $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

        $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


    }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
    {
      $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);
      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
          $_SESSION['user']['session']=$libellesessionencours;

          if(strlen($_SESSION['user']['codeEtab'])>0)
          {

          }else {
            $_SESSION['user']['codeEtab']=$codeEtabAssigner;
          }

          $notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

          // $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
          $classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);
          $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);
        }

    }
  }

$notificationses=$etabs->getAllMessages($codeEtabAssigner,$libellesessionencours);
  // var_dump($allcodeEtabs);
  $nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
// echo $libellesessionencours;
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
      <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::NotificationsLists ?>  </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li>&nbsp;<a class="parent-item" href="#"><?php echo L::NotificationMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>

                                <li class="active"><?php echo L::NotificationsLists ?></li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['updateteaok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: '<?php echo L::Felicitations ?>',
            text: '<?php echo $_SESSION['user']['updateteaok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['updateteaok']);
                }

                 ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

<div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line">
                               <!--ul class="nav nav-pills nav-pills-rose">
									<li class="nav-item tab-all"><a class="nav-link active show"
										href="#tab1" data-toggle="tab">Liste</a></li>
									<li class="nav-item tab-all"><a class="nav-link" href="#tab2"
										data-toggle="tab">Grille</a></li>
								</ul-->
                                <div class="tab-content">
                                    <div class="tab-pane active fontawesome-demo" id="tab1">
                                        <div class="row">
					                        <div class="col-md-12">
					                            <div class="card  card-box">
					                                <div class="card-head">
					                                    <header></header>
					                                    <div class="tools">
					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
					                                    </div>
					                                </div>

					                                <div class="card-body ">



					                                  <div class="table-scrollable">
                                              <?php

                                               ?>
					                                    <table class="table table-hover table-checkable order-column full-width" id="example4">
					                                        <thead>
					                                            <tr>

					                                                <!-- <th> Id</th> -->
					                                                <th style="width:600px"><?php echo L::NotificationsLibs ?> </th>
					                                                <!-- <th> <?php echo L::ClassesMenu ?> </th> -->
                                                           <!-- <th><?php echo L::Destinataires ?></th> -->
					                                                 <!-- <th> <?php echo L::SendsType ?> </th> -->
                                                           <th style="width:200px;text-align:center"> <?php echo L::Etat ?> </th>
                                                           <?php
                                                             if($_SESSION['user']['fonctionuser']=="Directeur")
                                                             {
                                                               ?>
                                                               <th style="width:100px;text-align:center"> <?php echo L::Actions ?> </th>
                                                               <?php
                                                             }else {
                                                               ?>
                                                               <th> <?php echo L::Actions ?> </th>
                                                               <?php
                                                             }
                                                            ?>

					                                            </tr>
					                                        </thead>
                                                  <?php
                                                  if($nbsessionOn>0)
                                                  {
                                                   ?>
                                                  <tbody>
                                                    <?php
                                                    $i=1;
                                                    foreach ($notificationses as $value):
                                                     ?>
                                                      <tr class="odd gradeX">
                                                      <!-- <td><?php //echo $i ?></td> -->
                                                      <td style="width:300px">
                                                        <?php

                                                        if($value->parascolaire_msg==1)
                                                        {
                                                          // echo "parascolaire";
                                                          echo $etabs->getparacolaireDesignation($value->id_msg);
                                                        }else if($value->scola_msg==1)
                                                        {
                                                          // echo "scolarite";
                                                        }else if(($value->parascolaire_msg==0)&&($value->scola_msg==0))
                                                        {
                                                          // echo "note observation";
                                                          if($value->objet_msg==8)
                                                          {
                                                            echo $value->other_msg;
                                                          }else {
                                                            echo $value->libelle_msg;
                                                          }
                                                        }
                                                         ?>
                                                      </td>
                                                      <!-- <td><?php
                                                       $dataclasse=$value->classes_msg;
                                                       $tabdataclasse=explode("-",$dataclasse);
                                                       $nbtabdataclasse=count($tabdataclasse);
                                                       $limitdataclasse=$nbtabdataclasse-1;
                                                       // echo $limitdataclasse;

                                                       for($z=0;$z<$limitdataclasse;$z++)
                                                       {
                                                         ?>

                                                           <span class="label label-sm label-success" style=""> <?php echo $classe->getInfosofclassesbyId($tabdataclasse[$z],$libellesessionencours); ?> </span>


                                                        <?php

                                                       }
                                                       ?>
                                                      </td> -->
                                                      <!-- <td>
                                                        <?php
                                                        $datadestinataire=$value->destinataires_msg;
                                                        $tabdatadestinataires=explode("-",$datadestinataire);
                                                        $nbtabdatadestinataires=count($tabdatadestinataires);
                                                        $limitdatadestinataires=$nbtabdatadestinataires-1;

                                                        for($k=0;$k<$limitdatadestinataires;$k++)
                                                        {
                                                          ?>
                                                          <span class="label label-sm label-warning"> <?php echo $tabdatadestinataires[$k]; ?> </span>
                                                         <?php

                                                        }
                                                        ?>
                                                      </td> -->
                                                      <!-- <td>
                                                        <?php
                                                          $envoitype="";
                                                          if($value->sms_msg==0)
                                                          {
                                                            if($value->email_msg==0)
                                                            {

                                                            }else if($value->email_msg==1)
                                                            {
                                                              echo L::EmailstudentTab;
                                                            }
                                                          }else if($value->sms_msg==1){
                                                            if($value->email_msg==0)
                                                            {
                                                              echo L::SmsLib;
                                                            }else if($value->email_msg==1)
                                                            {
                                                              echo L::EmailOrSmsLib;
                                                            }
                                                          }
                                                         ?>
                                                      </td> -->
                                                      <td style="text-align:center">
                                                        <?php
                                                          $statutnotif=$value->statut_msg;

                                                          if($statutnotif==0)
                                                          {
                                                            ?>
                            <button type="button" class="btn btn-circle btn-danger btn-xs m-b-10"><?php echo L::StandBy ?></button>
                                                            <?php

                                                          }else if($statutnotif==1)
                                                          {
                                                            ?>
                          <button type="button" class="btn btn-circle btn-success btn-xs m-b-10"><?php echo L::Sending ?></button>
                                                            <?php
                                                          }else if($statutnotif==2)
                                                          {
                                                            ?>

                                                            <?php
                                                          }
                                                       ?>
                                                      </td>
                                                      <?php
                                                      if($_SESSION['user']['fonctionuser']==" ")
                                                      {
                                                        ?>
                                                        <td style="text-align:center">
                                                          <?php
                                                            $statutnotif=$value->statut_msg;
                                                            $parascolairestatus=$value->parascolaire_msg;
                                                            $scolastatus=$value->scola_msg;

                                                            if($statutnotif==0)
                                                            {
                                                              ?>
                                                              <?php
                                                              if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="CD")
                                                              {
                                                                ?>
                                                                <a href="#"  title="<?php echo L::Sending ?>" onclick="send(<?php echo $value->id_msg; ?>,<?php echo $value->parascolaire_msg; ?>,<?php echo $value->scola_msg; ?>,'<?php echo $value->session_msg ?>',<?php echo $value->addby_msg?>)" class="btn btn-info btn-circle ">
                                                                  <i class="fa fa-send"></i>
                                                                </a>
                                                                <?php
                                                                if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="CD")
                                                                {
                                                                  ?>
                                                                  <a class="btn btn-danger btn-circle  " onclick="deleted(<?php echo $value->id_msg; ?>,<?php echo $value->parascolaire_msg; ?>,<?php echo $value->scola_msg; ?>)"  title="<?php echo L::DeleteLib ?>">
                                                                    <i class="fa fa-trash-o "></i>
                                                                  </a>
                                                                  <?php
                                                                }
                                                                 ?>

                                                                <?php
                                                              }
                                                               ?>

                                                              <?php
                                                              if(($parascolairestatus==1)||(($parascolairestatus==0)&&($scolastatus==0)))
                                                              {
                                                               ?>
                                                              <a href="detailsmessages.php?msg=<?php echo $value->id_msg; ?>" class="btn btn-warning btn-circle  " > <i class="fa fa-print"></i> </a>
                                                              <?php
                                                            }
                                                               ?>
                                                               <?php

                                                            }else if($statutnotif==1)
                                                            {
                                                              ?>
                                                              <?php
                                                              if(($parascolairestatus==1)||(($parascolairestatus==0)&&($scolastatus==0)))
                                                              {
                                                               ?>
                                                              <a href="detailsmessages.php?msg=<?php echo $value->id_msg; ?>" class="btn btn-info btn-circle  " > <i class="fa fa-info-circle"></i> </a>
                                                              <?php
                                                            }
                                                               ?>



                                                              <?php
                                                            }else if($statutnotif==2)
                                                            {
                                                              ?>

                                                              <?php
                                                            }
                                                         ?>
                                                        </td>
                                                        <?php
                                                      }else {
                                                        ?>
                                                        <td>
                                                          <?php

                                                          $statutnotif=$value->statut_msg;
                                                          $parascolairestatus=$value->parascolaire_msg;
                                                          $scolastatus=$value->scola_msg;
                                                           ?>
                                                          <div class="btn-group">
                                        <button data-toggle="dropdown" class="btn btn-default btn-circle dropdown-toggle m-r-20" type="button">
                                            <?php echo L::Actions ?><span class="caret"></span>
                                        </button>
                                        <ul role="menu" class="dropdown-menu">
                                          <?php
                                          if($statutnotif==0)
                                          {
                                            ?>
                                            <?php

                                            if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="CD")
                                            {
                                              ?>
                                              <li><a href="#" title="<?php echo L::Sending ?>" onclick="send(<?php echo $value->id_msg; ?>,<?php echo $value->parascolaire_msg; ?>,<?php echo $value->scola_msg; ?>,'<?php echo $value->session_msg ?>',<?php echo $value->addby_msg?>)"><i class="fa fa-send"></i><?php echo L::Sending ?></a>
                                              </li>

                                              <?php
                                              if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="CD")
                                              {
                                                ?>
                                                <li><a href="#" onclick="deleted(<?php echo $value->id_msg; ?>,<?php echo $value->parascolaire_msg; ?>,<?php echo $value->scola_msg; ?>)"  title="<?php echo L::DeleteLib ?>"><i class="fa fa-trash-o "></i><?php echo L::DeleteLib ?></a>
                                                </li>
                                                <?php
                                              }
                                               ?>


                                              <?php
                                            }

                                             ?>

                                            <?php
                                            // if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Intendancer"||$_SESSION['user']['fonctionuser']=="DAF"||$_SESSION['user']['fonctionuser']=="BDM"||$_SESSION['user']['fonctionuser']=="CC"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG")
                                            // {
                                             ?>
                                             <?php
                                             if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="CD")
                                             {
                                               ?>

                                               <?php
                                             }
                                              ?>

                                            <?php
                                          // }
                                             ?>

                                            <?php
                                            if(($parascolairestatus==1)||(($parascolairestatus==0)&&($scolastatus==0)))
                                            {
                                              ?>
                                              <li><a  href="detailsmessages.php?msg=<?php echo $value->id_msg; ?>"><i class="fa fa-info-circle"></i><?php echo L::Detailsingle ?></a>
                                              </li>
                                              <?php
                                            }else {
                                              // code...
                                            }
                                             ?>
                                             <?php
                                             if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="CD")
                                           {
                                             
                                              ?>
                                             <li><a href="updatenotifications.php?msg=<?php echo $value->id_msg; ?>"> <i class="fa fa-pencil"></i><?php echo L::ModifierBtn ?></a>
                                             </li>
                                             <?php
                                           }
                                              ?>

                                            <?php
                                          }else if($statutnotif==1)
                                          {
                                            if(($parascolairestatus==1)||(($parascolairestatus==0)&&($scolastatus==0)))
                                            {
                                            ?>

                                            <li><a  href="detailsmessages.php?msg=<?php echo $value->id_msg; ?>"><i class="fa fa-info-circle"></i><?php echo L::Detailsingle ?></a>
                                            </li>
                                            <?php
                                            }
                                            ?>
                                            <li><a href="#" title="<?php echo L::Archived ?>" onclick="archiver(<?php echo $value->id_msg; ?>,<?php echo $value->parascolaire_msg; ?>,<?php echo $value->scola_msg; ?>)"><i class="fa fa-repeat "></i><?php echo L::Archived ?></a>
                                            </li>
                                            <?php
                                          }
                                           ?>







                                        </ul>
                                    </div>
                                                          <?php
/*
                                                            $statutnotif=$value->statut_msg;
                                                            $parascolairestatus=$value->parascolaire_msg;
                                                            $scolastatus=$value->scola_msg;

                                                            if($statutnotif==0)
                                                            {
                                                              ?>
                                                              <a href="#"  title="<?php echo L::Sending ?>" onclick="send(<?php echo $value->id_msg; ?>,<?php echo $value->parascolaire_msg; ?>,<?php echo $value->scola_msg; ?>,'<?php echo $value->session_msg ?>',<?php echo $value->addby_msg?>)" class="btn btn-info btn-circle ">
                                                                <i class="fa fa-send"></i>
                                                              </a>
                                                              <a class="btn btn-danger btn-circle  " onclick="deleted(<?php echo $value->id_msg; ?>,<?php echo $value->parascolaire_msg; ?>,<?php echo $value->scola_msg; ?>)"  title="<?php echo L::DeleteLib ?>">
                                                                <i class="fa fa-trash-o "></i>
                                                              </a>
                                                              <?php
                                                              if(($parascolairestatus==1)||(($parascolairestatus==0)&&($scolastatus==0)))
                                                              {
                                                               ?>
                                                              <a href="detailsmessages.php?msg=<?php echo $value->id_msg; ?>" class="btn btn-warning btn-circle  " > <i class="fa fa-print"></i> </a>
                                                              <?php
                                                            }
                                                               ?>
                                                               <?php

                                                            }else if($statutnotif==1)
                                                            {
                                                              ?>
                                                              <?php
                                                              if(($parascolairestatus==1)||(($parascolairestatus==0)&&($scolastatus==0)))
                                                              {
                                                               ?>
                                                              <a href="detailsmessages.php?msg=<?php echo $value->id_msg; ?>" class="btn btn-info btn-circle  " > <i class="fa fa-print"></i> </a>
                                                              <?php
                                                            }
                                                               ?>

                                                              <a class="btn btn-warning btn-circle  " title="<?php echo L::Archived ?>" onclick="archiver(<?php echo $value->id_msg; ?>,<?php echo $value->parascolaire_msg; ?>,<?php echo $value->scola_msg; ?>)">
                                                                <i class="fa fa-repeat "></i>
                                                              </a>

                                                              <?php
                                                            }else if($statutnotif==2)
                                                            {
                                                              ?>

                                                              <?php
                                                            }*/
                                                         ?>
                                                        </td>
                                                        <?php
                                                      }
                                                       ?>

                                                      </tr>
                                                      <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                                      <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                                      <script type="text/javascript">

                                                      function etatmessage(id_msg,parascolaire_msg,scola_msg,precis_msg,addby_msg)
                                                      {
                                                        document.location.href="etatmessages.php?message="+id_msg+"&parascolaire="+parascolaire_msg+"&precis="+precis_msg+"&addby="+addby_msg;
                                                      }

                                                      function archiver(id,paraid,scolarid)
                                                      {
                                                        var session="<?php echo $libellesessionencours; ?>";
                                                        var codeEtab="<?php echo $_SESSION['user']['codeEtab']; ?>";
                                                        Swal.fire({
                                          title: '<?php echo L::WarningLib ?>',
                                          text: "<?php echo L::DoyouReallyArchivedMessages ?>",
                                          type: 'warning',
                                          showCancelButton: true,
                                          confirmButtonColor: '#2CA8FF',
                                          cancelButtonColor: '#d33',
                                          confirmButtonText: '<?php echo L::Archived ?>',
                                          cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                        }).then((result) => {
                                          if (result.value) {
                                            document.location.href="../controller/messaging.php?etape=5&notifid="+id+'&paraid='+paraid+'&scolarid='+scolarid+'&session='+session+'&codeEtab='+codeEtab;

                                          }else {

                                          }
                                        })
                                                      }

                                                      function deleted(id,paraid,scolaid)
                                                      {
                                                        // var notificationid="<?php //echo $value->id_notif; ?>";
                                                        // var destinataires="<?php //echo $value->destinataires_notif; ?>";
                                                        // var classes="<?php //echo  $value->classes_notif;?>";
                                                        var etape=4;
                                                        // var codeEtab="<?php  //echo $value->codeEtab_notif;?>";

                                                        Swal.fire({
                                          title: '<?php echo L::WarningLib ?>',
                                          text: "<?php echo L::DoyouReallyDeletedMessages ?>",
                                          type: 'warning',
                                          showCancelButton: true,
                                          confirmButtonColor: '#2CA8FF',
                                          cancelButtonColor: '#d33',
                                          confirmButtonText: '<?php echo L::DeleteLib ?>',
                                          cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                        }).then((result) => {
                                          if (result.value) {
                                            document.location.href="../controller/messaging.php?etape=7&notifid="+id+"&paraid="+paraid+'&scolaid='+scolaid;

                                          }else {

                                          }
                                        })
                                                      }


                                                      function send(id,paraid,scolaire,sessionEtab,addby)
                                                      {
                                                        // var sessionEtab="<?php //echo $libellesessionencours  ?>";
                                                        //nous allons recuperer les elements essentiels pour l'envoi de mail ou sms
                                                        // var etape1=4;
                                                        var etape1=7;
                                                        $.ajax({
                                                          url: '../ajax/message.php',
                                                          type: 'POST',
                                                          async:false,
                                                            data: 'notifid='+id+'&etape='+etape1+'&paraid='+paraid+'&scolaire='+scolaire+'&sessionEtab='+sessionEtab+'&addby='+addby,
                                                          dataType: 'text',
                                                            success: function (content, statut) {


                                                              var tabcontent=content.split("*");
                                                              var destinataires=tabcontent[0];
                                                              var classes=tabcontent[1];
                                                              var codeEtab=tabcontent[2];
                                                              var smssender=tabcontent[3];
                                                              var emailsender=tabcontent[4];
                                                              var precis=tabcontent[5];
                                                              var eleves=tabcontent[6];
                                                              var scolarities=tabcontent[9];
                                                              var etape2=5;

                                                              var sessionEtab="<?php echo $libellesessionencours; ?>";

                                                              Swal.fire({
                                                title: '<?php echo L::WarningLib ?>',
                                                text: "<?php echo L::DoyouReallySendingNotificatons ?>",
                                                type: 'warning',
                                                showCancelButton: true,
                                                confirmButtonColor: '#2CA8FF',
                                                cancelButtonColor: '#d33',
                                                confirmButtonText: '<?php echo L::Sending ?>',
                                                cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                              }).then((result) => {
                                                if (result.value) {
                                                  // document.location.href="../controller/messages/etape=3&notifid="+notificationid+"&destinataires="+destinataires+"&classes="+classes;
                                                  $.ajax({
                                                    url: '../ajax/message.php',
                                                    type: 'POST',
                                                    async:false,
                                                    data: 'notifid='+id+'&etape='+etape2+'&codeEtab='+codeEtab+'&destinataires='+destinataires+'&classes='+classes+'&smssender='+smssender+'&emailsender='+emailsender+"&sessionEtab="+sessionEtab+'&precis='+precis+'&eleves='+eleves,
                                                    dataType: 'text',
                                                      success: function (content, statut) {

                                                        var tab=content.split("/");
                                                        var destimails=tab[0];
                                                        var destiphones=tab[1];

                                              document.location.href="../controller/messaging.php?etape=8&notifid="+id+"&destimails="+destimails+"&destiphones="+destiphones+"&codeEtab="+codeEtab+"&smssender="+smssender+"&emailsender="+emailsender+"&precis="+precis+"&eleves="+eleves+"&paraid="+paraid+"&scolarities="+scolarities+'&sessionEtab='+sessionEtab;


                                                      }
                                                  });
                                                }else {

                                                }
                                              })

                                                            }
                                                        });







                                                      }

                                                      </script>

                                                      <?php
                                                      $i++;
                                                      endforeach
                                                       ?>



                                                  </tbody>
                                                  <?php
                                                }
                                                  ?>
					                                    </table>
					                                    </div>
					                                </div>
					                            </div>
					                        </div>
					                    </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>



                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   function SetcodeEtab(codeEtab)
   {
     var etape=3;
     $.ajax({
       url: '../ajax/sessions.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   // function classeliste()
   // {
   //   //listeEnseignant.php?$codeEtabAssigner=<?php //echo $codeEtabAssigner?>
   //
   //   var url="listeClasse.php?$idClasse=<?php //echo $idClasse?>& $codeEtabAssigner=<?php //echo $codeEtabAssigner?>";
   //
   //   window.open(url, '_blank');
   // }

   function searchlibetab()
   {

     var etape=2;

     $.ajax({
              url: '../ajax/school.php',
              type: 'POST',
              async:false,
              data: 'code='+ $("#codeetab").val()+'&etape='+etape,
              dataType: 'text',
              success: function (content, statut) {

                $("#libetab").html("");
                $("#libetab").html(content);

                //nous allons chercher la liste des admins local de cet etablissement

                $.ajax({
                         url: '../ajax/classe.php',
                         type: 'POST',
                         async:false,
                         data: 'code='+ $("#codeetab").val()+'&etape='+etape,
                         dataType: 'text',
                         success: function (response, statut) {


                           $("#classex").html("");
                           $("#classex").html(response);
                         }
                       });

              }

            });

   }
   $(document).ready(function() {

     $('#example5').DataTable( {
         "scrollX": true

     } );

     $("#codeetab").select2();

     $("#libetab").select2();

   });

   </script>
    <!-- end js include path -->
  </body>

</html>
