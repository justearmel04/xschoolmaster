<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Teatcher.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../controller/functions.php');
require_once('../class/ChiffresEnLettres.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$codeEtab=$_GET['codeEtab'] ;
$idClasse=$_GET['classeEtab'];
$session=$_GET['sessionEtab'];

$etabs=new Etab();
$student=new Student();
$classe=new Classe();

$libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
$libelleclasse=$classe->getInfosofclassesbyId($idClasse,$session);
$logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
$etablissementType=$etabs->DetermineTypeEtab($codeEtab);


$students=$student->getAllStudentOfClassesId($idClasse,$session);

//nous allons faire nos différents calculs

//le nombre d'eleve de cette classe

$nbclassestudents=$classe->getAllStudentOfThisClassesNb($idClasse,$session);

//le nombre de fille affecté

$nbaffectefilles=$classe->getAllStudentFilleAffectOfThisClassesNb($idClasse,$session);

//nombre de fille non Affecté

$nbnonaffectefilles=$classe->getAllStudentFilleNAffectOfThisClassesNb($idClasse,$session);

//nombre de mec affectés

$nbaffectemecs=$classe->getAllStudentMecAffectOfThisClassesNb($idClasse,$session);

//nombre de mec non affectés

$nbnonaffectemecs=$classe->getAllStudentMecNAffectOfThisClassesNb($idClasse,$session);

//nombre de fille redoublante

$nbredoubfilles=$classe->getAllStudentFilleRedtOfThisClassesNb($idClasse,$session);

//nombre non redoublante fille

$nbnredoubfilles=$classe->getAllStudentFilleNRedtOfThisClassesNb($idClasse,$session);

//nombre de mec redoublant

$nbredoubmecs=$classe->getAllStudentMecRedtOfThisClassesNb($idClasse,$session);

//nombre non redoublant mec

$nbnredoubmecs=$classe->getAllStudentMecNRedtOfThisClassesNb($idClasse,$session);


 $versementStudent=$student->getStudentversementInfosNew($codeEtab,$session,$_GET['compte'],$_GET['versementid']);

 foreach ($versementStudent as  $valueVersement):
   $deposant=$valueVersement->deposant_versement;
   $motif=$valueVersement->motif_versement;
   $mode=$valueVersement->mode_versement;
   $montant=$valueVersement->montant_versement;
   $solde=$valueVersement->solde_versement;
   $code=$valueVersement->code_versement;
   $dateversement=$valueVersement->date_versement;
   $beneficiaire=$valueVersement->nom_eleve." ".$valueVersement->prenom_eleve;
   $beneficiairematri=$valueVersement->matricule_eleve;
   $beneficiaireclasse=$valueVersement->libelle_classe;
 endforeach;


require('fpdf/fpdf.php');

class PDF extends FPDF
{



function Footer()
{
    // Positionnement à 1,5 cm du bas
    $this->SetY(-15);
    // Police Arial italique 8
    $this->SetFont('Arial','I',8);
    // Numéro et nombre de pages
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    // $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
}

// Tableau simple
function BasicTable($header, $data)
{
    // En-tête
    foreach($header as $col)
        $this->Cell(40,7,$col,1);
    $this->Ln();
    // Données
    foreach($data as $row)
    {
        foreach($row as $col)
            $this->Cell(40,6,$col,1);
        $this->Ln();
    }
}
}

$pdf = new PDF('L','mm',array(150,200));
$pdf->AliasNbPages();
$pdf->SetMargins(3,0);
$pdf->SetFont('Times','B',  16);
$pdf->AddPage();
$pdf->Ln(17);
$pdf -> SetX(2);
$pdf->Image("../logo_etab/".$codeEtab."/".$logoEtab,2,1,35,35);
$pdf -> SetX(5);
$pdf-> SetFont('Times','B',12);
$pdf->Cell(108,-24,"INTERNATIONAL BILINGUAL",0,0,'C');
$pdf->Cell(80,-24,"ANNEE SCOLAIRE : ".$session,0,0,'C');
$pdf->Ln(6);
$pdf->Cell(108,-24,"SCHOOLS OF AFRICA",0,0,'C');
$pdf->Cell(80,-24,"DATE : ".date_format(date_create($dateversement),"d/m/Y"),0,0,'C');
$pdf->Ln(6);
$pdf->Cell(108,-24,"RIVIERA",0,0,'C');
$pdf->SetFont('Times','B',  15);
$pdf->Cell(80,-24,"B.P.F :".$montant." #",0,0,'C');
$pdf->Ln(3);
$pdf -> SetX(5);
$pdf-> SetFont('Times','B',  14);
$pdf->Cell(176,5,"Matricule : ".$beneficiairematri,0,0,'C');
$pdf->Ln(6);
$pdf->Cell(182,5,utf8_decode("N° Famille :"),0,0,'C');
$pdf->Ln(10);
$pdf -> SetX(5);
$pdf-> SetFont('Times','B', 14);
$pdf->Cell(100,5,"RECU - SCOLARITE",0,0,'C');
$pdf-> SetTextColor(255,0,0);
$pdf->Cell(5,5,$code,0,0,'C');
// $pdf -> SetX(30);
// $pdf-> SetFont('Times','B',12);
// $pdf-> SetTextColor(255,0,0);
// $pdf->Cell(200,5,$code,0,0,'C');

$pdf->Ln(15);

$pdf-> SetTextColor(75,54,33);
$pdf->SetFillColor(230,230,0);
$pdf->SetLineWidth(.3);
$pdf->SetFont('Times','B',16);
// $pdf->Cell(45,8,utf8_decode('Nom & Prénoms :'),0,0,'C');
$pdf -> SetX(2);
$pdf->MultiCell(200,8,utf8_decode('Nom & Prénoms :').strtoupper($beneficiaire)." ".'Classe  '.$beneficiaireclasse." ".utf8_decode('Montant scolarité ').$beneficiaireclasse ,0,'L');
$pdf->SetFont('Times','B',15);
$pdf->Cell(180,8,strtoupper($beneficiaire) ,0,0,'L');
$pdf->Ln(8);
$pdf->Cell(80,8,'Classe  '.$beneficiaireclasse,0,0,'L');
$pdf->Cell(1,8,utf8_decode('Montant scolarité ').$beneficiaireclasse,0,0,'L');














$pdf->Output();



 ?>
