<div class="white-sidebar-color sidebar-container">
                <div class="sidemenu-container navbar-collapse collapse fixed-menu notScrollable">
                   <div id="remove-scroll">
                   <ul class="sidemenu  page-header-fixed sidemenu-hover-submenu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                        <li class="sidebar-toggler-wrapper hide">
                            <div class="sidebar-toggler">
                                <span></span>
                            </div>
                        </li>
                        <li class="sidebar-user-panel">

                            <div class="user-panel">

                                <!-- <div class="pull-left image"> -->
                                <div class="" style="text-align:center" >

                                  <?php
                                

                                  // $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);

                                  $imageEtab=$etabs->getEtabLogobyCodeEtab($_SESSION['user']['codeEtab']);

                                  $lienlogoetab="../logo_etab/".$_SESSION['user']['codeEtab']."/".$imageEtab;

                                   ?>

                                    <img src="<?php echo $lienlogoetab?>"  style="" class="" alt="User Image" />

                                    <!-- <img src="../assets/img/ibsaeduk.png"  style="" class="" alt="User Image" /> -->



                                </div>

                                <!-- <div class="pull-left info">

                                    <p> <?php //echo $tablogin[0];?></p><br>

                                    <small>



                                    </small>

                                </div> -->

                                <?php //echo $etabs->getEtabLibellebyCodeEtab($codeEtabAssigner); ?>

                                <?php //echo $tablogin[0];?>

                            </div>

                        </li>



                        <?php

                        if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant")
                        {
                          $menuschoolselected="";
                          $tabotherschool=array();
                          foreach ($allcodeEtabs as  $valueEtabs):
                            // if($_SESSION['user']['codeEtab']==$valueEtabs->codeEtab_assign)
                            // {
                            //   $menuschoolselected=$valueEtabs->codeEtab_assign;
                            // }else {
                            //   $tabotherschool[]=$valueEtabs->codeEtab_assign;
                            // }
                            $tabotherschool[]=$valueEtabs->codeEtab_assign;
                          endforeach;

                          if($_SESSION['user']['groupeselect']=="ALL")
                          {



                          ?>
                          <li class="nav-item selected open">
                              <a href="#" class="nav-link nav-toggle" style="text-align:center" onclick="SetcodeEtabAll()">
                              <span class="title" style="text-align:center">GROUPE IBSA <?php echo $_SESSION['user']['codeEtab'] ?></span>
                              <span class="arrow"></span>
                            </a>
                              <ul class="sub-menu" style="margin-left:225px">
                                <?php
                                for($i=0;$i<count($tabotherschool);$i++)
                                {
                                  ?>
                                  <li class="nav-item">
                                  <a href="#" class="nav-link "> <span class="title" onclick="SetcodeEtab('<?php echo $tabotherschool[$i] ?>')"><?php echo $etabs->getEtabNamebyCodeEtab($tabotherschool[$i]) ?></span></a>
                                   </li>
                                  <?php
                                }

                                 ?>



                              </ul>
                          </li>
                          <?php
}
                           ?>

                          <li class="nav-item ">

                              <a href="index.php" class="nav-link nav-toggle">

                                  <i class="material-icons">home</i>

                                  <span class="title"><?php echo L::Homestartindex ?></span>

                                  <span class="selected"></span>
                                </a>
                              </li>

                          <?php

                        }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
                        {
                          ?>
                          <li class="nav-item active open">
                              <a href="index.php" class="nav-link nav-toggle">
                                  <i class="material-icons">dashboard</i>
                                  <span class="title">Dashboard</span>
                                  <span class="selected"></span>


                              </a>

                          </li>
                          <?php
                        }



                        //
                         ?>


                        <li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
                                <i class="material-icons">group</i>
                                <span class="title"><?php echo L::studMenu ?></span>
                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu" style="margin-left:225px">
                                <?php
                                if($_SESSION['user']['fonctionuser']=="Coordinnateur"|| $_SESSION['user']['fonctionuser']=="Administrateur")
                                {
                                  ?>
                                  <li class="nav-item">

                                      <a href="admission.php" class="nav-link">

                                      </i> <?php echo L::CreateFicheMenu ?></a>

                                  </li>
                                  <?php
                                }
                                 ?>
	                                <li class="nav-item  ">
                                    <a href="listeall.php" class="nav-link">

                                    </i><?php echo L::FichestudMenu  ?>
                                  </a>
	                                </li>
                                  <?php
                                    if($_SESSION['user']['fonctionuser']=="Coordinnateur"|| $_SESSION['user']['fonctionuser']=="Administrateur"|| $_SESSION['user']['fonctionuser']=="Superviseur"||$_SESSION['user']['fonctionuser']=="Directeur")
                                    {
                                      ?>
                                      <!-- <li class="nav-item">

                                            <a href="listeallperformance.php" class="nav-link">

                                            </i> <?php //echo L::PerformeMenu ?></a>

                                        </li> -->

                                      <?php
                                    }
                                   ?>

                                   <?php
                                     if($_SESSION['user']['fonctionuser']=="Administrateur")
                                     {
                                       ?>

                                       <li class="nav-item">
                                             <a href="importations.php" class="nav-link">
                                             </i><?php echo L::ImportMenu ?></a>
                                         </li>
                                       <?php
                                     }
                                    ?>

	                            </ul>
	                        </li>
	                        <li class="nav-item  ">
	                            <a href="#" class="nav-link nav-toggle"><i class="material-icons">directions_bus</i>
	                            <span class="title"><?php echo L::TP ?></span>
                              <span class="arrow"></span>
                            </a>
	                            <ul class="sub-menu" style="margin-left:225px">
                                <?php
                                if($_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Administrateur")
                                {
                                  ?>
                                  <li class="nav-item  ">
                                  <a href="transportations.php" class="nav-link "> <span class="title"><?php echo L::Parameters ?></span></a>
                                   </li>
                                   <li class="nav-item  ">
                                  <?php
                                }
                                 ?>

                                 <a href="transports.php" class="nav-link "> <span class="title"><?php echo L::TPListes ?></span></a>
                                  </li>

	                            </ul>
	                        </li>
                          <?php
                          if($_SESSION['user']['fonctionuser']=="Comptable")
                          {

                          }else {
                            ?>

                            <li class="nav-item  ">
  	                            <a href="#" class="nav-link nav-toggle"> <i class="material-icons">book</i>
  	                                <span class="title"><?php echo L::EnsigneMenu ?></span>
                                     <span class="arrow"></span>
  	                            </a>
  	                            <ul class="sub-menu" style="margin-left:225px">
                                  <?php
                                  if($_SESSION['user']['fonctionuser']=="Coordinnateur"|| $_SESSION['user']['fonctionuser']=="Administrateur"|| $_SESSION['user']['fonctionuser']=="Superviseur"||$_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG")
                                  {
                                    ?>
                                    <li class="nav-item  ">

                                        <a href="sessions.php" class="nav-link "> <span class="title"><?php echo L::ScolaryyearMenu ?></span>

                                        </a>

                                    </li>
                                    <?php
                                  }
                                   ?>
  	                                <li class="nav-item  ">
                                      <a href="programmes.php" class="nav-link "> <span class="title"><?php echo L::syllabusplanning ?></span>

                                      </a>
  	                                </li>
  	                                <li class="nav-item  ">
                                      <a href="fiches.php" class="nav-link "> <span class="title"><?php echo L::CahierMenu ?></span>

                                      </a>
  	                                </li>
                                    <?php
      if($_SESSION['user']['fonctionuser']=="Coordinnateur"|| $_SESSION['user']['fonctionuser']=="Administrateur"|| $_SESSION['user']['fonctionuser']=="Superviseur"|| $_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG")
                                        {
                                          ?>

                                          <li class="nav-item">
                  	                            <a href="javascript:;" class="nav-link nav-toggle">

                  	                                <span class="title"><?php echo L::RoutinesMenu ?></span>
                  	                                <span class="arrow "></span>
                  	                            </a>
                                                <ul class="sub-menu">

                                                  <?php
                                                  if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Superviseur")
                                                  {

                                                  }else if($_SESSION['user']['fonctionuser']=="Administrateur"|| $_SESSION['user']['fonctionuser']=="Superviseur"|| $_SESSION['user']['fonctionuser']=="Coordinnateur"){
                                                    ?>
                                                    <li class="nav-item">

                                                        <a href="routinesadd.php" class="nav-link">

                                                        </i><?php echo L::AddMenu ?></a>

                                                    </li>
                                                    <?php
                                                  }
                                                   ?>





                  <?php

                  $i=1;

                  foreach ($classes as $value):

                   ?>

                   <li class="nav-item">

                       <a href="routines.php?classe=<?php echo $value->id_classe?>" class="nav-link">

                       <?php echo $value->libelle_classe;?></a>

                   </li>

                   <?php

                   $i++;

                     endforeach;

                    ?>



                   	                            </ul>
                  	                        </li>



                                            <li class="nav-item  ">
                                              <a href="hours.php" class="nav-link "> <span class="title"><?php echo L::HoursMenu ?></span>
                                              </a>
          	                                </li>

                                          <?php
                                        }
                                     ?>


  	                            </ul>
  	                        </li>

                            <?php
                          }
                           ?>

                           <?php
                           if($_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Accountant")
                           {

                           }else {
                             ?>

                             <li class="nav-item  ">
   	                            <a href="#" class="nav-link nav-toggle"> <i class="material-icons">assignment_turned_in</i>
   	                                <span class="title"><?php echo L::AbsMenu ?></span> <span class="arrow"></span>
   	                            </a>
   	                            <ul class="sub-menu" style="margin-left:225px">
                                  <?php
                                  if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="Superviseur")
                                  {
                                    ?>
                                    <li class="nav-item  ">
                                      <a href="dailyattendance.php" class="nav-link "> <span class="title"><?php echo L::Consignerlesabsences ?></span>
                                      </a>
                                     </li>
                                    <?php
                                  }
                                     ?>
                                <?php
  if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="Superviseur"||$_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Coordinnateur")
                                {
                                 ?>

                                     <li class="nav-item  " class="nav-link nav-toggle">

                                         <a href="#" class="nav-link nav-toggle" ><?php echo L::RecapsMenu ?></a>

                                       <ul class="sub-menu" style="">

                                         <li class="nav-item" ><a href="recapattendanceclasse.php"><?php echo L::ClasseMenu ?></a></li>

                                         <li class="nav-item"><a href="recapattendanceclasseeleves.php"><?php echo L::EleveMenu ?></a> </li>

                                         <li class="nav-item"> <a href="recapattendanceclassematiere.php"><?php echo L::MatiereMenu ?></a> </li>

                                       </ul>



                                     </li>

                                     <li class="nav-item  " class="nav-link nav-toggle">

                                         <a href="#" class="nav-link nav-toggle" ><?php echo L::AttendanceGraphes ?></a>

                                       <ul class="sub-menu" style="margin-left:225px">

                                         <li class="nav-item" ><a href="attendancegraphclasse.php"><?php echo L::ClasseMenu ?></a></li>

                                         <li class="nav-item"><a href="attendancegraph.php"><?php echo L::EleveMenu ?></a> </li>



                                       </ul>



                                     </li>

                                     <?php
                                   }
                                      ?>

   	                            </ul>
   	                        </li>

                            <li class="nav-item  ">
  	                            <a href="#" class="nav-link nav-toggle"> <i class="material-icons">assignment</i>
  	                                <span class="title"><?php echo L::EvalnotesMenu ?></span> <span class="arrow"></span>
  	                            </a>
  	                            <ul class="sub-menu" style="margin-left:225px">
                                  <?php
                                    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="Superviseur"||$_SESSION['user']['fonctionuser']=="Coordinnateur")
                                     {
                                      ?>
                                      <li class="nav-item  " class="nav-link nav-toggle">

                                          <a href="#" class="nav-link nav-toggle" ><?php echo L::AddnotesMenu ?></a>

                                        <ul class="sub-menu" style="">

                                          <li class="nav-item" ><a href="addnotes.php"><?php echo L::ClasseMenu ?></a></li>

                                          <li class="nav-item"><a href="addnoteseleves.php"><?php echo L::EleveMenu ?></a> </li>



                                        </ul>



                                      </li>
                                      <?php
                                    }
                                   ?>

  	                                <li class="nav-item  ">
                                      <a href="controles.php" class="nav-link "> <span class="title"><?php echo L::ControlsMenu ?></span>
                                      </a>
  	                                </li>

                                    <?php
                                      if($_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Accountant")
                                      {

                                      }else {
                                        ?>

                                    <li class="nav-item  " class="nav-link nav-toggle">

                                        <a href="#" class="nav-link nav-toggle" ><?php echo L::RecapNotesMenu ?></a>

                                      <ul class="sub-menu" style="">

                                        <li class="nav-item" ><a href="notes.php"><?php echo L::ClasseMenu ?></a></li>

                                        <li class="nav-item"><a href="noteseleves.php"><?php echo L::EleveMenu ?></a> </li>



                                      </ul>



                                    </li>
                                    <?php
                                  }
                                     ?>
                                    <?php
                                      if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Superviseur")
                                      {

                                      }else if($_SESSION['user']['fonctionuser']=="Administration"||$_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Superviseur"){
                                        ?>
                                        <li class="nav-item  ">

                                            <a href="validations.php" class="nav-link "> <span class="title"><?php echo L::ValidModesMenu ?></span>

                                            </a>

                                        </li>
                                        <?php
                                      }
                                     ?>


  	                            </ul>
  	                        </li>

                             <?php
                           }
                              ?>



	                        <li class="nav-item  ">
	                            <a href="#" class="nav-link nav-toggle"> <i class="material-icons">event_note</i>
	                                <span class="title"><?php echo L::ExtraActivities ?></span> <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu" style="margin-left:225px">
                                <?php
                                  if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG")
                                  {

                                  }else {
                                    ?>
                                    <li class="nav-item  ">

                                       <a href="parascolairesadd.php" class="nav-link "> <span class="title"><?php echo L::NewparaActMenu ?></span>

                                       </a>

                                   </li>
                                    <?php
                                  }
                                 ?>
	                                <li class="nav-item  ">
                                    <a href="allparascolaires.php" class="nav-link "> <span class="title"><?php echo L::ListeparaActMenu ?></span>
                                    </a>
	                                </li>

	                            </ul>
	                        </li>
	                        <li class="nav-item  ">
	                            <a href="#" class="nav-link nav-toggle"> <i class="material-icons">notifications</i>
	                                <span class="title"><?php echo L::NotificationMenu ?></span> <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu" style="margin-left:225px">
                                <?php
                                    if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG")
                                  {
                                    ?>
                                    <li class="nav-item  ">

                                       <a href="addnotifications.php" class="nav-link "> <span class="title"><?php echo L::NewsNotificationMenu ?></span>

                                       </a>

                                   </li>
                                    <?php
                                  }else {
                                    ?>
                                    <li class="nav-item  ">

                                       <a href="addnotifications.php" class="nav-link "> <span class="title"><?php echo L::NewsNotificationMenu ?></span>

                                       </a>

                                   </li>
                                    <?php
                                  }
                                 ?>
	                                <li class="nav-item  ">
                                    <a href="allmessages.php" class="nav-link "> <span class="title"><?php echo L::ListeNotificationMenu ?></span>
                                    </a>
	                                </li>

	                            </ul>
	                        </li>
                          <?php
if($_SESSION['user']['fonctionuser']=="Coordinnateur"|| $_SESSION['user']['fonctionuser']=="Administrateur"|| $_SESSION['user']['fonctionuser']=="Superviseur"||$_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG")
                          {
                            ?>
                            <li class="nav-item  ">
                                <a href="#" class="nav-link nav-toggle">
                                    <i class="material-icons">dvr</i>
                                    <span class="title"><?php echo L::AdminMenu ?></span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu" style="margin-left:225px">
                                    <!-- <li class="nav-item  ">
                                      <a href="salles.php" class="nav-link "> <span class="title"><?php echo L::SallesMenu ?></span>
                                      </a>
                                    </li> -->
                                    <li class="nav-item  ">
                                      <a href="classes.php" class="nav-link "> <span class="title"><?php echo L::ClassesMenu ?></span>
                                      </a>
                                    </li>
                                    <li class="nav-item  ">
                                      <a href="matieres.php" class="nav-link "> <span class="title"><?php echo L::MatiereMenu ?></span>
                                      </a>
                                    </li>

                                    <li class="nav-item  " class="nav-link nav-toggle">

                                        <a href="#" class="nav-link nav-toggle" >



                                           <span class="title"><?php echo L::UsersMenu ?></span>

                                           <span class="selected"></span>

                                          <span class="arrow open"></span>

                                         </a>

                                      <ul class="sub-menu" style="">

                                        <li class="nav-item  ">

      	                                    <a href="localadmins.php" class="nav-link "> <span class="title"><?php echo L::AdminsMenu ?></span>

      	                                    </a>

      	                                </li>

      	                                <li class="nav-item  ">

      	                                    <a href="teatchers.php" class="nav-link "> <span class="title"><?php echo L::ProfsMenu ?></span>

      	                                    </a>

      	                                </li>

                                        <li class="nav-item  ">

      	                                    <a href="parents.php" class="nav-link "> <span class="title"><?php echo L::ParentsMenu ?></span>

      	                                    </a>

      	                                </li>



                                      </ul>



  	                                </li>

                                </ul>
                            </li>
                            <?php
                          }
                             ?>


                    </ul>
                   </div>
                </div>
            </div>
