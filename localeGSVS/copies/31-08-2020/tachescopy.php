<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');
require_once('../class/Tache.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$salle=new Tache();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

  if($_SESSION['user']['groupe']==1)
  {
    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
    {
      $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);

      //nous allons chercher la liste des etablissements du groupe

      $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
        }

        if(strlen($_SESSION['user']['codeEtab'])>0)
        {

        }else {
          $_SESSION['user']['codeEtab']=$codeEtabAssigner;
        }

        $_SESSION['user']['session']=$libellesessionencours;





         // var_dump($allcodeEtabs);

        $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

        $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


    }else if($_SESSION['user']['fonctionuser']=="Coordonnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
    {
      $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);
      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
          $_SESSION['user']['session']=$libellesessionencours;

          if(strlen($_SESSION['user']['codeEtab'])>0)
          {

          }else {
            $_SESSION['user']['codeEtab']=$codeEtabAssigner;
          }

          $notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

          // $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
          $classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);
          $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);
        }

    }
  }

$salles=$salle->getAlltachesOfThisSchool($_SESSION['user']['IdCompte']);
$nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
  // var_dump($allcodeEtabs);
  $onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
  $offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
// echo $libellesessionencours;
$utilisateurtoassign=$localadmins->getAllUtilisateursEtabs($_SESSION['user']['codeEtab'],$_SESSION['user']['IdCompte']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::tachesLib ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="#"><?php echo L::SallesMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::AllSallesMenu ?></li>
                            </ol>
                        </div>
                    </div>
                    <?php

                          if(isset($_SESSION['user']['addsalleok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: '<?php echo L::Felicitations ?>',
                      text: '<?php echo $_SESSION['user']['addsalleok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['addsalleok']);
                          }

                           ?>
					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['updateteaok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: '<?php echo L::Felicitations ?>',
            text: '<?php echo $_SESSION['user']['updateteaok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['updateteaok']);
                }

                 ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>
                 <?php
                 /*
                  ?>
<div class="row">
  <div class="col-md-12 col-sm-12">
                            <div class="card card-box">
                                <div class="card-head">
                                    <header><?php echo L::Seacher ?></header>

                                </div>
                                <div class="card-body " id="bar-parent">
                                  <form method="post" id="FormSearch">
                                      <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Code Etablissemnt</label>
                                            <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                                            <select class="form-control " id="codeetab" name="codeetab" onchange="searchlibetab()" style="width:100%">
                                                <option value="">Selectionner un code etablissement</option>
                                                <?php
                                                $i=1;
                                                  foreach ($schoolsofassign as $value):
                                                  ?>
                                                  <option value="<?php echo $value->code_etab?>"><?php echo $value->code_etab?></option>

                                                  <?php
                                                                                   $i++;
                                                                                   endforeach;
                                                                                   ?>

                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Salle</label>
                                            <select class="form-control input-height" id="sallex" name="sallex" style="width:100%">
                                                <option value="">Selectionner une salle</option>
                                                <?php
                                                $i=1;
                                                  foreach ($salles as $value):
                                                  ?>
                                                  <option value="<?php echo $value->id_salle?>"><?php echo utf8_encode(utf8_decode($value->libelle_salle)) ?></option>

                                                  <?php
                                                                                   $i++;
                                                                                   endforeach;
                                                                                   ?>

                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Nom Etablissement</label>
                                        <select class="form-control " id="libetab" name="libetab" style="width:100%">
                                            <option value="">Selectionner un code etablissement</option>
                                            <?php
                                            $i=1;
                                              foreach ($schoolsofassign as $value):
                                              ?>
                                              <option value="<?php echo utf8_encode(utf8_decode($value->libelle_etab)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_etab)); ?></option>

                                              <?php
                                                                               $i++;
                                                                               endforeach;
                                                                               ?>

                                        </select>
                                        <input type="hidden" name="search" id="search"/>
                                    </div>


                                </div>
                                      </div>

                                      <button type="submit" class="btn btn-danger"><?php echo L::Seacher ?></button>
                                  </form>
                                </div>
                            </div>
                        </div>
</div>body > div.sweet-alert.show-input.showSweetAlert.visible
<?php
*/
 ?>

                        <div class="col-md-12">
                            <div class="tabbable-line">

                                <div class="tab-content">
                                    <div class="tab-pane active fontawesome-demo" id="tab1">
                                        <div class="row">
					                        <div class="col-md-12">
					                            <div class="card  card-box">
					                                <div class="card-head">
					                                    <header></header>
					                                    <div class="tools">
					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
					                                    </div>
					                                </div>
					                                <div class="card-body ">
                                          <div class="pull-right">
                                            <?php
                                            if($_SESSION['user']['fonctionuser']=="Directeur"||$_SESSION['user']['fonctionuser']=="Superviseur")
                                            {

                                            }else {
                                              ?>
                                                <a class="btn btn-primary " href='addtache.php'><i class="fa fa-plus"></i> <?php echo L::NewtachesLib ?></a>
                                              <?php
                                            }
                                             ?>

                                                <a class="btn btn-warning " href='#'><i class="fa fa-print"></i> <?php echo L::listetachesLib ?></a>

                                          </div>
					                                  <div class="table-scrollable">
					                                    <table class="table table-hover table-checkable order-column full-width" id="example4">
					                                        <thead>
					                                            <tr>



                                                          <th style="width:200px"><?php echo L::tachesLib ?></th>
                                                          <th><?php echo L::DeadlinetachesLib ?></th>
                                                          <th><?php echo L::Etat ?></th>
                                                           <th><?php echo L::assigneto ?></th>
                                                          <th style="width:100px"> <?php echo L::Actions ?> </th>


					                                            </tr>
					                                        </thead>
					                                        <tbody>
                                                    <?php
                                                    $i=1;


//var_dump($teatchers);
                                                      foreach ($salles as $value):
                                                      ?>
																<tr class="odd gradeX">




																	<td><?php echo $value->lib_tache;?></td>
                                  <td><?php echo date_format(date_create($value->deadline),"d-m-Y"); ?></td>
                                  <td><?php
                                  // $nboccupy=$salle->getOccupyby($value->id_salle);
                                  $statut=$value->status;
                                  if($statut==0)
                                  {
                                    ?>
                                <span class="label label-xs label-danger"><i><?php echo L::Tachestattenonassigner  ?></i></span>
                                    <?php
                                  }else if($statut==1)
                                  {

                                    ?>
                                    <span class="label label-xs label-success"><i><?php echo L::Encours ?></i></span>
                                    <?php
                                  }else if($statut==2)
                                  {
                                    ?>
                                    <span class="label label-xs label-warning"><i><?php echo L::Terminator?></i></span>
                                    <?php
                                  }


                                  ?></td>
                                  <td>
                                    <?php
                                    if($value->status>0)
                                    {
                                      ?>
                                      <span><?php echo $etabs->getUtilisateurName($value->assignated) ?></span>
                                      <?php
                                    }else {
                                      // code...
                                    }
                                     ?>
                                  </td>

                                     <td class="center">

                                       <?php
                                       if($statut==0)
                                       {
                                         ?>

                                         <a href="#"  onclick="modify(<?php echo $value->id_tache;?>)" class="btn btn-warning btn-xs">
                                             <i class="fa fa-pencil"></i>
                                         </a>

                                         <!-- <a href="assignate.php?compte=<?php echo $value->id_tache ?>"   class="btn btn-success btn-xs">
                                           <i class="fa fa-user"></i>
                                         </a> -->
                                         <a href="#" data-toggle="modal" data-target="#largeModel1" onclick="addtachesid(<?php echo $value->id_tache ?>)"  class="btn btn-success btn-xs">
                                           <i class="fa fa-user"></i>
                                         </a>


                                         <a class="btn btn-danger btn-xs" onclick="deleted(<?php echo $value->id_tache;?>)">
                                           <i class="fa fa-trash-o "></i>
                                         </a>
                                         <?php
                                       }else if($statut==1)
                                       {
                                         ?>

                                         <?php
                                       }else if($statut==2)
                                       {
                                         ?>

                                         <?php
                                       }
                                        ?>


                                      <?php
                                      // echo $statut;
                                      if($statut==0)
                                      {
                                        ?>

                                        <?php
                                      }
                                       ?>





   																	</td>



																</tr>
                                <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                <script>



                                function myFunction(idcompte)
                                {
                                  //var url="detailslocal.php?compte="+idcompte;
                                document.location.href="detailssalles.php?compte="+idcompte;
                                }

                                function modify(id)
                                {


                                  Swal.fire({
                    title: '<?php echo L::WarningLib ?>',
                    text: "<?php echo L::DoyouReallyModifyingThisSalles ?>",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<?php echo L::ModifierBtn ?>',
                    cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                  }).then((result) => {
                    if (result.value) {
                      document.location.href="updatetache.php?compte="+id;
                    }else {

                    }
                  })
                                }

                                function assignate(id)
                                {



                                    Swal.fire({
                                        title: '<?php echo L::WarningLib ?>',
                                                      text: "<?php echo L::DoyouReallyModifyingThisSalles ?>",
                                                      type: 'warning',
                                                      showCancelButton: true,
                                                      confirmButtonColor: '#3085d6',
                                                      cancelButtonColor: '#d33',
                                                      confirmButtonText: '<?php echo L::ModifierBtn ?>',
                                                      cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                                    }).then((result) => {
                                                      if (result.value) {
                                                        document.location.href="assignate.php?compte="+id;
                                                      }else {

                                                      }
                                                    })
                                }

                                function deleted(id)
                                {
                                  var codeEtab="<?php echo $value->codeEtab_tch ?>";
                                  Swal.fire({
                    title: '<?php echo L::WarningLib ?>',
                    text: "<?php echo L::DoyouReallyDeletingThisSalles ?>",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<?php echo L::DeleteLib ?>',
                    cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                  }).then((result) => {
                    if (result.value) {
                      document.location.href="../controller/tache.php?etape=3&compte="+id+"&codeEtab="+codeEtab;
                    }else {

                    }
                  })
                                }

                                </script>


                                <?php
                                                                 $i++;
                                                                 endforeach;
                                                                 ?>

															</tbody>
					                                    </table>
					                                    </div>
					                                </div>
					                            </div>
					                        </div>
					                    </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>



                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>

            <div class="modal fade" id="largeModel1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel"><?php echo L::Assignerunetache ?></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                      <div id="divrecherchestation">

              <form class=""  id="FormAddSalle" class="form-horizontal" action="../controller/tache.php" method="post">

                <div class="form-group row">
                    <label class="control-label col-md-3"><?php echo L::responsables ?>
                        <span class="required">  </span>
                    </label>
                    <div class="col-md-9">
                      <select class="form-control" name="assign" id="assign">
                        <option value=""></option>
                        <?php
                        foreach ($utilisateurtoassign as  $value):
                          ?>
                          <option value="<?php echo $value->id_compte ?>"><?php echo $value->nom_compte." ".$value->prenom_compte ?></option>
                          <?php
                        endforeach;
                         ?>
                      </select>
                      <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner; ?>">
                      <input type="hidden" name="session" id="sessionEtab" value="<?php echo $libellesessionencours; ?>">
                      <input type="text" name="idtache" id="idtache" value="">
                      <input type="hidden" name="etape" id="etape" value="3">
                      <!-- <input type="date" class="form-control input-height" placeholder="" name="date" id="desc" value="" > -->
                    </div>
                </div>
                <div class="row">
                    <div class="offset-md-3 col-md-9">
                        <button type="submit" class="btn btn-info" ><?php echo L::Saving ?></button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo L::AnnulerBtn ?></button>
                    </div>
                  </div>
<!-- <input type="hidden" name="stationselect" id="stationselect" value="">
<input type="hidden" name="famillycode" id="famillycode" value=""> -->

              </form>

                      </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" style="display:none" class="btn btn-secondary" data-dismiss="modal"><?php echo L::Closebtn  ?></button>
                        <button type="button" style="display:none" class="btn btn-primary" onclick="validerMember()" id="btnsaving"><?php echo L::Saving ?></button>
                    </div>
                </div>
            </div>
        </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function addtachesid(idtache)
   {
     alert(idtache);
    // $("#idtache").val(idtache);
   }


   function SetcodeEtab(codeEtab)
   {
     var etape=3;
     $.ajax({
       url: '../ajax/sessions.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function searchlibetab()
   {

     var etape=2;

     $.ajax({
              url: '../ajax/school.php',
              type: 'POST',
              async:false,
              data: 'code='+ $("#codeetab").val()+'&etape='+etape,
              dataType: 'text',
              success: function (content, statut) {

                $("#libetab").html("");
                $("#libetab").html(content);

                //nous allons chercher la liste des admins local de cet etablissement

                $.ajax({
                         url: '../ajax/salle.php',
                         type: 'POST',
                         async:false,
                         data: 'code='+ $("#codeetab").val()+'&etape='+etape,
                         dataType: 'text',
                         success: function (response, statut) {


                           $("#sallex").html("");
                           $("#sallex").html(response);
                         }
                       });

              }

            });

   }
   $(document).ready(function() {



     // $("#assign").select2();
     //
     // $("#FormAddSalle").validate({
     //
     //   errorPlacement: function(label, element) {
     //   label.addClass('mt-2 text-danger');
     //   label.insertAfter(element);
     // },
     // highlight: function(element, errorClass) {
     //   $(element).parent().addClass('has-danger')
     //   $(element).addClass('form-control-danger')
     // },
     // success: function (e) {
     //       $(e).closest('.control-group').removeClass('error').addClass('info');
     //       $(e).remove();
     //   },
     //    rules:{
     //      passad: {
     //          required: true,
     //          minlength: 6
     //      },
     //      confirmtad:{
     //          required: true,
     //          minlength: 6,
     //          equalTo:'#passad'
     //      },
     //      fonctionad:"required",
     //
     //      loginad:"required",
     //      emailad: {
     //                 required: true,
     //                 email: true
     //             },
     //      contactad:"required",
     //      datenaisad:"required",
     //      prenomad:"required",
     //      classe:"required",
     //      libetab:"required",
     //      libetab:"required",
     //      sallex:"required",
     //      capacitex:"required",
     //      statutsalle:"required"
     //
     //
     //    },
     //    messages: {
     //      confirmtad:{
     //          required:"<?php echo L::Confirmcheck ?>",
     //          minlength:"<?php echo L::Confirminch ?>",
     //          equalTo: "<?php echo L::ConfirmSamecheck ?>"
     //      },
     //      passad: {
     //          required:"<?php echo L::Passcheck ?>",
     //          minlength:"<?php echo L::Passmincheck ?>"
     //      },
     //      loginad:"<?php echo L::Logincheck ?>",
     //      emailad:"<?php echo L::PleaseEnterEmailAdress ?>",
     //      contactad:"<?php echo L::PleaseEnterPhoneNumber ?>",
     //      datenaisad:"<?php echo L::PleaseEnterPhonestudentTab ?>",
     //      prenomad:"<?php echo L::PleaseEnterPrename ?>",
     //      nomad:"<?php echo L::PleaseEnterName ?> ",
     //      fonctionad:"<?php echo L::PleaseEnterFonction ?>",
     //      classe:"<?php echo L::PleaseRenseigneClasse ?>",
     //      libetab:"<?php echo L::PleaseEtabselect ?>",
     //      sallex:"<?php echo L::PleaseEnterSallLib ?>",
     //      capacitex:"<?php echo L::PleaseEnterSallCapacity ?>",
     //      statutsalle:"<?php echo L::PleaseEntertachestate ?>"
     //    },
     //    submitHandler: function(form) {
     //      //verifier si ce compte n'existe pas encore dans la base de données
     //         var etape=1;
     //         $.ajax({
     //           url: '../ajax/salle.php',
     //           type: 'POST',
     //           async:false,
     //           data: 'salle=' + $("#sallex").val()+ '&libetab=' + $("libetab").val() + '&etape=' + etape,
     //           dataType: 'text',
     //           success: function (content, statut) {
     //
     //             if(content==0)
     //             {
     //               //le compte n'existe pas dans la base on peut l'ajouter
     //
     //               form.submit();
     //             }else {
     //               //le compte existe dejà dans la base de données
     //               Swal.fire({
     //               type: 'warning',
     //               title: '<?php echo L::WarningLib ?>',
     //               text: '<?php echo L::ClasseAllreadyexiste ?>',
     //
     //               })
     //
     //             }
     //
     //           }
     //
     //
     //         });
     //    }
     //
     //
     // });

     $('#example5').DataTable( {
         "scrollX": true

     } );

     $("#codeetab").select2();

     $("#libetab").select2();
     $("#sallex").select2();

   });

   </script>
    <!-- end js include path -->
  </body>

</html>
