<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../controller/functions.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();
$userId=$_SESSION['user']['IdCompte'];
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);

$codeEtabsession=$etabs->getcodeEtabByLocalId($userId);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabsession);


if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabsession,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabsession,$libellesessionencours);
}


$nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <!--link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" /-->

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::EmploisduTemps ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::EmploisduTemps ?></li>
                            </ol>
                        </div>
                    </div>

                    <?php

                          if(isset($_SESSION['user']['addprogra']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                    <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: '<?php echo L::Felicitations ?>',
                      text: '<?php echo $_SESSION['user']['addprogra'] ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['addprogra']);
                          }

                           ?>
					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['updateroutineok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
        title: '<?php echo L::Felicitations ?>',
        text: "<?php echo $_SESSION['user']['updateroutineok']; ?>",
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '<?php echo L::AddroutinesNew ?>',
        cancelButtonText: '<?php echo L::AnnulerBtn ?>',
        }).then((result) => {
        if (result.value) {
          document.location.href="routinesadd.php";
        }else {

        }
        })
            </script>
                  <?php
                  unset($_SESSION['user']['updateroutineok']);
                }

                 ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>
                 <div class="pull-right">

                   <a class="btn btn-primary " href="routinesadd.php"><i class="fa fa-plus-circle"> <?php echo L::CoursesAdding ?></i></a>
                    <a class="btn btn-warning " href="scheduletabclasse.php?classe=<?php echo $_GET['classe']; ?>&codeEtab=<?php echo $codeEtabsession; ?>&session=<?php echo $libellesessionencours; ?>" ><i class="fa fa-print"> Emploi du temps</i></a>
                   <!-- <a class="btn btn-warning " href="#" onclick="generetedroutines(<?php //echo $_GET['classe']; ?>,'<?php //echo $codeEtabsession; ?>','<?php //echo $libellesessionencours; ?>')"><i class="fa fa-print"> Emploi du temps</i></a> -->
                   <a class="btn btn-success " href="#" data-toggle="modal" data-target="#smallModel" ><i class="fa fa-send"> <?php echo L::Sendbutton ?></i></a>

                   <div class="modal fade" id="smallModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					    <div class="modal-dialog modal-md" role="document">
					        <div class="modal-content">
					            <div class="modal-header">
					                <h4 class="modal-title" id="exampleModalLabel" style="color: Dodgerblue;"> <i class="fa fa-send "></i> <?php echo L::ByEmailling ?> </h4>
					                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					                    <span aria-hidden="true">&times;</span>
					                </button>
					            </div>
					            <div class="modal-body">

                          <div class="form-group row">
                              <label class="control-label col-md-4"><?php echo L::Destinatairesingle ?>
                                  <span class="required">*  </span>
                              </label>
                              <div class="col-md-8">
                                <select class="form-control" id="desti" name="desti" style="width:100%" onchange="searchmail()">
                                    <option value=""><?php echo L::SelectDestinatairesingle ?></option>
                                    <option value="1"><?php echo L::AdminsMenu ?></option>
                                    <option value="2"><?php echo L::ParentsingleMenu ?></option>
                                    <option value="3"><?php echo L::TeatcherCapsingles ?></option>


                                </select>
                                </div>
                          </div>
                          <div class="form-group row">
                              <label class="control-label col-md-4"><?php echo L::NamestudentTab ?>
                                  <span class="required">*  </span>
                              </label>
                              <div class="col-md-8">
                                <select class="form-control" id="destimail" name="destimail" style="width:100%" onchange="selectmail()">
                                    <option value=""><?php echo L::SelectDestinatairesingle ?></option>
                                </select>
                                </div>
                          </div>
                          <div class="form-group row">
                              <label class="control-label col-md-4"><?php echo L::EmailstudentTab ?>
                                  <span class="required">*  </span>
                              </label>
                              <div class="col-md-8">
                                <input type="text" class="form-control" id="mail" name="mail"  style="width:100%" readonly>

                                </div>
                          </div>

					            </div>
					            <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="sendbutton" disabled onclick="send()"><?php echo L::Sendbutton ?></button>
					                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo L::Closebtn ?></button>

					            </div>
					        </div>
					    </div>
					</div>
                 </div>

<br/><br/>
          <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card card-topline-green">
                                                    <div class="card-head">
                                                        <header><?php echo L::RoutinesByClasses ?></header>
                                                        <div class="tools">
                                                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
            												<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
            												<a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                                        </div>
                                                    </div>
                                                    <div class="card-body ">
                                        <div class="table-responsive">
                                            <table class="table table-striped custom-table table-hover">
                                                <thead>

                                                </thead>
                                                <tbody>
                                                  <?php
                                                  $dataday=$etabs->getAllweeks();
                                                  //var_dump($dataday);

                                                  $i=1;
                                                    foreach ($dataday as $value):

                                                    ?>


                                                      <tr>
                                                          <td>
                                                            <?php
                                                            if($_SESSION['user']['lang']=="fr")
                                                            {
                                                              echo $value->libelle_days;
                                                            }else {
                                                              echo $value->libelleen_days;
                                                            }

                                                            ?>
                                                          </td>
                                                          <td >
                                                            <?php
                                                            $nbroute=$etabs->getNumberofRoutinebyIdroute($value->id_days,$value->short_days,$_GET['classe']);
                                                            if($nbroute>0)
                                                            {
                              //recherche les routines et les affichées
                              $routines=$etabs->getspecificRoutine($value->id_days,$value->short_days,$_GET['classe']);

                              $j=1;
                                foreach ($routines as $valueRoutine):
                                                             ?>
                                  <!-- button-group -->
                                  <div class="btn-group" role="group" aria-label="Button group with nested dropdown">


  <div class="btn-group" role="group">
    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <?php echo $valueRoutine->libelle_mat."(".$valueRoutine->debut_route." - ".$valueRoutine->fin_route.")";?>
    </button>
    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
      <a class="dropdown-item" href="#" onclick="modify(<?php echo $valueRoutine->id_route ?>)"><i class="fa fa-pencil"></i> <?php echo L::ModifierBtn ?></a>
      <a class="dropdown-item" href="#" onclick="deleted(<?php echo $valueRoutine->id_route ?>,<?php echo $_GET['classe'] ?>)"><i class="fa fa-trash-o"></i> <?php echo L::DeleteLib ?></a>

    </div>
  </div>
</div>

<link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
<script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
<script>

function myFunction(idcompte)
{
  //var url="detailslocal.php?compte="+idcompte;
document.location.href="detailsadmin.php?compte="+idcompte;
}

function modify(id)
{


  Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::DoyouReallyModifyingThisRoutines ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::ModifierBtn ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
document.location.href="updateroutine.php?compte="+id;
}else {

}
})
}

function deleted(id,classeid)
{

  Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::DoyouReallyDeletingThisRoutines ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::DeleteLib ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
document.location.href="../controller/routine.php?etape=3&compte="+id+"&classe="+classeid;
}else {

}
})
}

</script>



<?php

$j++;
endforeach;
}
 ?>


                                  <!-- button-group -->


                                                          </td>

                                                      </tr>


                                                      <?php
                                                                                       $i++;
                                                                                       endforeach;
                                                                                       ?>


                                                </tbody>
                                            </table>
                                            </div>

                                        </div>
                                                </div>
          </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- calendar -->
     <!--script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script-->
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   function SetcodeEtab(codeEtab)
   {
     var etape=3;
     $.ajax({
       url: '../ajax/sessions.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function send()
   {
     //recuperaion des variables

     var desti=$("#desti").val();
     var destimail=$("#destimail").val();
     var mail=$("#mail").val();
     var codeEtab="<?php echo $codeEtabsession; ?>";
     var session="<?php echo $libellesessionencours; ?>";
     var classe="<?php echo $_GET['classe']; ?>";

     //nous allons generer le pdf de

   }

   function selectmail()
   {
      var desti=$("#desti").val();
      var destimail=$("#destimail").val();
      var etape=8;
      var codeEtab="<?php echo $codeEtabsession; ?>";
      var session="<?php echo $libellesessionencours; ?>";

      $.ajax({
        url: '../ajax/admission.php',
        type: 'POST',
        async:false,
        data: 'codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&desti='+desti+'&destimail='+destimail,
        dataType: 'text',
        success: function (response, statut) {

          $("#mail").val("");
          $("#mail").val(response);

          //sendbutton

          if(response.length>0)
          {
            $("#sendbutton").removeAttr("disabled");
          }else {
          $("#sendbutton").prop("disabled", true);
          }

        }
      });
   }

   function searchmail()
   {
     var desti=$("#desti").val();
     var etape=7;
     var codeEtab="<?php echo $codeEtabsession; ?>";
     var session="<?php echo $libellesessionencours; ?>";
     var classe="<?php echo $_GET['classe']; ?>";

     if(desti!=4)
     {
       $.ajax({
         url: '../ajax/admission.php',
         type: 'POST',
         async:false,
         data: 'codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&desti='+desti+'&classe='+classe,
         dataType: 'text',
         success: function (response, statut) {

           $("#destimail").html("");
           $("#destimail").html(response);

           $("#sendbutton").prop("disabled", true);
           $("#mail").val("");

         }
       });
     }else {
       alert("egale à 4");
     }

   }

   function generetedroutines(classeid,codeEtab,session)
   {
     var etape=3;
     $.ajax({
       url: '../ajax/etat.php',
       type: 'POST',
       async:false,
       data: 'codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&classe='+classeid,
       dataType: 'text',
       success: function (response, statut) {

           window.open(response, '_blank');

       }
     });
   }
   $(document).ready(function() {



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
