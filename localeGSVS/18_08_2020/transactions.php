<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$student=new Student();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

  if($_SESSION['user']['groupe']==1)
  {
    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
    {
      $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);

      //nous allons chercher la liste des etablissements du groupe

      $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
        }

        if(strlen($_SESSION['user']['codeEtab'])>0)
        {

        }else {
          $_SESSION['user']['codeEtab']=$codeEtabAssigner;
        }

        $_SESSION['user']['session']=$libellesessionencours;

         // var_dump($allcodeEtabs);

         $allstudentschools=$student->getAllStudentOfThisSchool($codeEtabAssigner);

        $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

        $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);

        // $classesEtab=$classe->

    }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
    {
      $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);
      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
          $_SESSION['user']['session']=$libellesessionencours;

          if(strlen($_SESSION['user']['codeEtab'])>0)
          {

          }else {
            $_SESSION['user']['codeEtab']=$codeEtabAssigner;
          }

          $notifications=$etabs->getAllMessagesending($_SESSION['user']['codeEtab'],$libellesessionencours);

          // $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);

            $classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);

          $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);
          $allstudentschools=$student->getAllStudentOfThisSchool($_SESSION['user']['codeEtab']);
        }

    }
  }

  $montantsscolaritiesvers=$etabs->getscolaritiesmontantverser($_SESSION['user']['codeEtab'],$libellesessionencours);
  $montantinscriptionsvers=$etabs->getinscriptionsmontantverser($_SESSION['user']['codeEtab'],$libellesessionencours);
  $montantreinscriptionsvers=$etabs->getreinscriptionsmontantverser($_SESSION['user']['codeEtab'],$libellesessionencours);
  $montantaesvers=$etabs->getaessmontantverser($_SESSION['user']['codeEtab'],$libellesessionencours);

  if(isset($_POST['search']))
  {

//recuperation des variables post

$datedeb=htmlspecialchars($_POST['datedeb']);
$datedeb=date_format(date_create($datedeb),"Y-m-d");
$codeetab=htmlspecialchars($_POST['codeetab']);
$_SESSION['user']['codeEtab']=$codeetab;
$datefin=htmlspecialchars($_POST['datefin']);
$datefin=date_format(date_create($datefin),"Y-m-d");
$motif=htmlspecialchars($_POST['motif']);

if($motif=="ALL")
{
$alltransactions=$etabs->getAllLastTransactionsNotLimitedPeriodes($datedeb,$datefin,$_SESSION['user']['codeEtab'],$libellesessionencours);
} else
{
$alltransactions=$etabs->getAllLastTransactionsNotLimitedPeriodesByMotif($motif,$datedeb,$datefin,$_SESSION['user']['codeEtab'],$libellesessionencours);
}

  }else {
    // echo "Bonsoir";

  $alltransactions=$etabs->getAllLastTransactionsNotLimited($_SESSION['user']['codeEtab'],$libellesessionencours);





  }

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::transactionstories ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="#"><?php echo L::Transactions ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::transactionstories ?></li>
                          </ol>
                      </div>
                  </div>

                    <?php

                          if(isset($_SESSION['user']['addclasseok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: '<?php echo L::Felicitations ?>',
                      text: '<?php echo $_SESSION['user']['addclasseok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['addclasseok']);
                          }

                           ?>

                    <?php

                          if(isset($_SESSION['user']['updateclasseok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: '<?php echo L::Felicitations ?>',
                      text: '<?php echo $_SESSION['user']['updateclasseok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['updateclasseok']);
                          }

                           ?>



                    <?php

                          if(isset($_SESSION['user']['delclasseok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: '<?php echo L::Felicitations ?>',
                      text: '<?php echo $_SESSION['user']['delclasseok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['delclasseok']);
                          }

                           ?>
					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['updateteaok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: '<?php echo L::Felicitations ?>',
            text: '<?php echo $_SESSION['user']['updateteaok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['updateteaok']);
                }

                 ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

                 <div class="state-overview">
                <div class="row">
                <div class="col-lg-3 col-sm-6">
                <div class="overview-panel purple">
                  <div class="symbol">
                    <i class="fa fa-money"></i>
                  </div>
                  <div class="value white">
                    <p class="sbold addr-font-h1" data-counter="counterup" data-value="
                    <?php
                    if($montantsscolaritiesvers=="")
                    {
                      echo "0";
                    }else {
                      echo $montantsscolaritiesvers;
                    }
                     ?>
                    ">0</p>

                    <p><?php echo L::ScolaritiesScream ?></p>
                  </div>
                </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                <div class="overview-panel deepPink-bgcolor">
                  <div class="symbol">
                  <i class="fa fa-money"></i>
                  </div>
                  <div class="value white">
                    <p class="sbold addr-font-h1" data-counter="counterup" data-value="
                    <?php
                    if($montantinscriptionsvers=="")
                    {
                      echo "0";
                    }else {
                      echo $montantinscriptionsvers;
                    }
                     ?>
                    ">0</p>

                    <p><?php echo L::InscriptionScream ?></p>
                  </div>
                </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                <div class="overview-panel orange">
                  <div class="symbol">
                  <i class="fa fa-money"></i>
                  </div>
                  <div class="value white">
                    <p class="sbold addr-font-h1" data-counter="counterup" data-value="
                    <?php
                    if($montantreinscriptionsvers=="")
                    {
                      echo "0";
                    }else {
                      echo $montantreinscriptionsvers;
                    }
                     ?>
                    ">0</p>

                    <p><?php echo L::ReInscriptionScream ?></p>
                  </div>
                </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                <div class="overview-panel blue-bgcolor">
                  <div class="symbol">
                    <i class="fa fa-money"></i>
                  </div>
                  <div class="value white">
                    <p class="sbold addr-font-h1" data-counter="counterup" data-value="
                    <?php
                    if($montantaesvers=="")
                    {
                      echo "0";
                    }else {
                      echo $montantaesvers;
                    }
                     ?>
                    ">0</p>

                    <p><?php echo L::AesScream ?></p>
                  </div>
                </div>
                </div>
                </div>
                </div>
                <div class="row" style="">
                  <div class="col-md-12 col-sm-12">
                                            <div class="card card-box">
                                                <div class="card-head">
                                                    <header><?php echo L::Seacher ?></header>

                                                </div>
                                                <div class="card-body " id="bar-parent">
                                                  <form method="post" id="FormSearch">
                                                      <div class="row">

                                                        <div class="col-md-6 col-sm-6">
                                                        <!-- text input -->

                                                        <div class="form-group">
                                                            <label><?php echo L::DatedebLib ?></label>
                                                              <input type="text" class="form-control col-md-12" name="datedeb" id="datedeb" value="<?php
                                                              if(isset($_POST['datedeb']))
                                                              {
                                                                echo $_POST['datedeb'];
                                                              }else {
                                                                
                                                              }

                                                                ?>" style="width:100%" placeholder="<?php
                                                              if(isset($_POST['datedeb']))
                                                              {
                                                                echo $_POST['datedeb'];
                                                              }else {
                                                                echo L::EntersDatedebLib;
                                                              }

                                                                ?>">
                                                            <input type="hidden" name="search" id="search"/>
                                                            <input type="hidden" id="codeetab" name="codeetab" value="<?php echo $_SESSION['user']['codeEtab']; ?>">

                                                        </div>

                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                    <!-- text input -->

                                                    <div class="form-group">
                                                        <label><?php echo L::DatefinLib ?></label>
                                                          <input type="text" class="form-control col-md-12" name="datefin" id="datefin" value="<?php

                                                          if(isset($_POST['datefin']))
                                                          {
                                                            echo $_POST['datefin'];
                                                          }



                                                           ?>" style="width:100%" placeholder="<?php

                                                          if(isset($_POST['datefin']))
                                                          {
                                                            echo $_POST['datefin'];
                                                          }else {
                                                            echo L::EntersDatefinLib;
                                                          }



                                                           ?>">


                                                    </div>

                                                </div>
                                                    <div class="col-md-6 col-sm-6">
                                                    <!-- text input -->
                                                    <div class="form-group">
                                                        <label><?php echo L::MotifVersementss ?></label>
                                                        <select class="form-control " id="motif" name="motif" style="width:100%">
                                                            <option value="" <?php if(!isset($_POST['motif'])){echo "selected";} ?>  ><?php echo L::SelecaMotif ?></option>
                                                            <option value="SCOLARITES" <?php if(isset($_POST['motif'])&&($_POST['motif']=="SCOLARITES")){echo "selected";} ?> ><?php echo L::ScolaritiesScream ?></option>
                                                            <option value="INSCRIPTIONS" <?php if(isset($_POST['motif'])&&($_POST['motif']=="INSCRIPTIONS")){echo "selected";} ?>><?php echo L::InscriptionScream ?></option>
                                                            <option value="REINSCRIPTIONS" <?php if(isset($_POST['motif'])&&($_POST['motif']=="REINSCRIPTIONS")){echo "selected";} ?>><?php echo L::ReInscriptionScream ?></option>
                                                            <option value="AES" <?php if(isset($_POST['motif'])&&($_POST['motif']=="AES")){echo "selected";} ?>><?php echo L::AesScream ?></option>
                                                            <option value="ALL" <?php if(isset($_POST['motif'])&&($_POST['motif']=="ALL")){echo "selected";} ?>><?php echo L::Alls ?></option>


                                                        </select>
                                                          </div>


                                                </div>
                                                      </div>

                                                      <button type="submit" class="btn btn-danger"><i class="fa fa-search"></i> <?php echo L::Seacher ?></button>
                                                  </form>
                                                </div>
                                            </div>
                                        </div>
                </div>

<div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line">
                               <!--ul class="nav nav-pills nav-pills-rose">
									<li class="nav-item tab-all"><a class="nav-link active show"
										href="#tab1" data-toggle="tab">Liste</a></li>
									<li class="nav-item tab-all"><a class="nav-link" href="#tab2"
										data-toggle="tab">Grille</a></li>
								</ul-->
                                <div class="tab-content">
                                    <div class="tab-pane active fontawesome-demo" id="tab1">
                                        <div class="row">
					                        <div class="col-md-12">
					                            <div class="card  card-box">
					                                <div class="card-head">
					                                    <header></header>
					                                    <div class="tools">
					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
					                                    </div>
					                                </div>
					                                <div class="card-body ">

					                                  <div class="table-scrollable">
					                                    <table class="table table-hover table-checkable order-column full-width" id="example45">
					                                        <thead>
                                                    <tr>

                                                        <th class="left" style="width:200px">  <?php echo L::Versements ?></th>
                                                        <th style="text-align:center;width:180px;"> <?php echo L::MotifVersementss ?> </th>
                                                        <th class=""> <?php echo L::MontantSection ?> </th>
                                                        <th style="width:250px;text-align:center"> <?php echo L::ClassesMenu ?> </th>
                                                        <th  style="text-align:center;width:200px;"><?php echo L::BeneficiaireVersementss ?></th>


                                                    </tr>
					                                        </thead>
					                                        <tbody>
                                                    <?php
                                                    $i=1;

//var_dump($teatchers);
                                                foreach ($alltransactions as $value):
                                                        //nombre de fille redoublante

                                                        $dataclasses=$classe->getClassesByschoolCodewithId($_SESSION['user']['codeEtab'],$value->classe_versement);
                                                        $libelleclasse="";
                                                        foreach ($dataclasses as $values):

                                                            $libelleclasse=$values->libelle_classe;
                                                        endforeach;

                                                      ?>
																<tr>
                                  <td><span class=""><?php echo $value->code_versement; ?></span></td>
                                  <td style="text-align:center"> <span class="label"><?php echo $value->motif_versement ?></span> </td>
                                  <td><?php echo prixMill($value->montant_versement) ?></td>
                                  <td style="text-align:center"><?php echo $libelleclasse ?></td>
                                  <td><?php echo $student->getNameInfos($value->ideleve_versement) ?></td>

                                </tr>


                                <?php
                                                                 $i++;
                                                                 endforeach;
                                                                 ?>

															</tbody>
                              <!--tfoot>
                                <tr>
                                  <td>MONTANT TOTAL SCOLARITES</td>
                                  <td ></td>
                                  <td style="text-align:center"><?php// echo $sommemontantscolarites; ?></td>
                                  <td ></td>
                                </tr>
                                <tr>
                                  <td>MONTANT TOTAL INSCRIPTION</td>
                                  <td ></td>
                                  <td style="text-align:center"><?php //echo $sommemontantinscip; ?></td>
                                  <td></td>
                                </tr>
                              </tfoot-->
					                                    </table>
					                                    </div>
					                                </div>
					                            </div>
					                        </div>
					                    </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>



                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
 <script src="../cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" ></script>
 	<script src="../cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
 	<script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" ></script>
 	<script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" ></script>
 	<script src="../cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script src="../assets/js/formatter/jquery.formatter.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   function SetcodeEtab(codeEtab)
   {
     var etape=3;
     $.ajax({
       url: '../ajax/sessions.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }


   $("#section").select2();
   $("#libetab").select2();
   $("#classex").select2();

   function searchclassesection()
   {
     var etape=10;
     var codeEtab="<?php echo $_SESSION['user']['codeEtab'] ?>";
     var session="<?php echo $libellesessionencours ?>";
      $.ajax({
        url: '../ajax/classe.php',
        type: 'POST',
        async:false,
        data: 'section='+ $("#section").val()+'&etape='+etape+'&session='+session+'&codeEtab='+codeEtab,
        dataType: 'text',
        success: function (content, statut) {

          $("#classex").html("");
          $("#classex").html(content);

        }
      });
   }

   function searchlibetab()
   {

     var etape=2;

     $.ajax({
              url: '../ajax/school.php',
              type: 'POST',
              async:false,
              data: 'code='+ $("#codeetab").val()+'&etape='+etape,
              dataType: 'text',
              success: function (content, statut) {

                $("#libetab").html("");
                $("#libetab").html(content);

                //nous allons chercher la liste des admins local de cet etablissement

                $.ajax({
                         url: '../ajax/classe.php',
                         type: 'POST',
                         async:false,
                         data: 'code='+ $("#codeetab").val()+'&etape='+etape,
                         dataType: 'text',
                         success: function (response, statut) {


                           $("#classex").html("");
                           $("#classex").html(response);
                         }
                       });

              }

            });

   }
   $(document).ready(function() {

     $('#example5').DataTable( {
         "scrollX": true

     } );
     $("#motif").select2();

     $("#datefin").formatter({pattern:"{{99}}-{{99}}-{{9999}}"});
     $("#datedeb").formatter({pattern:"{{99}}-{{99}}-{{9999}}"});

     $('#example45').DataTable( {

       "scrollX": true,
       "language": {
           "lengthMenu": "_MENU_  ",
           "zeroRecords": "Aucune correspondance",
           "info": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
           "infoEmpty": "Aucun enregistrement disponible",
           "infoFiltered": "(filtered from _MAX_ total records)",
           "sEmptyTable":"Aucune donnée disponible dans le tableau",
            "sSearch":"Rechercher :",
            "oPaginate": {
       "sFirst":    "Premier",
       "sLast":     "Dernier",
       "sNext":     "Suivant",
       "sPrevious": "Précédent"
     }
     },

         dom: 'Bfrtip',
         buttons: [
             // 'copyHtml5',

             // 'excelHtml5',
             {
               extend: 'excelHtml5',
               title: 'Data export'
             }
             // 'csvHtml5',
             // 'pdfHtml5'
         ]
     } );


     $("#FormSearch").validate({
       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
      },
      success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },
       rules:{



         datedeb:"required",
         datefin:"required",
         motif:"required"



       },
       messages: {


         datedeb:"<?php echo L::PleaseEnterDatedebLib ?>",
         datefin:"<?php echo L::PleaseEnterDatefinLib ?>",
         motif:"<?php echo L::PLeaseSelecaMotif ?>"

       },
       submitHandler: function(form) {
         form.submit();
       }
     });



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
