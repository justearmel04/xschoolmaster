<?php
session_start();
require_once('../class/User.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$etabs=new Etab();
$etab=new Etab();
$parent=new ParentX();
$user=new User();
$classe=new Classe();
$emailUti=$_SESSION['user']['email'];
$userId=$_SESSION['user']['IdCompte'];
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$codeEtabLocal=$etab->getcodeEtabByLocalId($userId);
$classes=$classe->getAllClassesbyschoolCode($codeEtabLocal);
$parents=$parent->getAllParentSchool($codeEtabLocal);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabLocal);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabLocal);


if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabLocal);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabLocal,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabLocal,$libellesessionencours);
}


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <style>
    #radioBtn .notActive{
  color: #3276b1;
  background-color: #fff;
}
#radioBtn1 .notActive{
color: #3276b1;
background-color: #fff;
}
#radioBtn2 .notActive{
color: #3276b1;
background-color: #fff;
}
    </style>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title">Créer une fiche</div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#">Elèves</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active">Créer une fiche</li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addStudok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: '<?php echo L::Felicitations ?>',
  text: "<?php echo $_SESSION['user']['addStudok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::AddNews ?>',
  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="index.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addStudok']);
                    }

                     ?>

              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="card card-topline-green">
                       <div class="card-head">
                           <header>Informations</header>
                           <div class="tools">
                               <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
       <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
       <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                           </div>
                       </div>
                       <div class="card-body ">
                         <p>
                           <?php
                           if($nbsessionOn>0)
                           {
                             ?>
                             L'admission de nouveaux étudiants crée automatiquement une inscription à la classe sélectionnée dans la session en cours.
                             Veuillez vérifier et revérifier les informations que vous avez insérées car une fois que vous admettez un nouvel étudiant, vous ne pourrez pas modifier sa classe, son rôle, sa section sans passer à la session suivante.
                             <?php
                           }else if($nbsessionOn==0)
                           {
                             ?>
                             Afin de pouvoir faire l'admission de nouveaux élèves nous vous invitons à mentionner l'année scolaire dans l'onglet Enseignement
                             <?php
                           }
                            ?>

                         </p>
                       </div>
                   </div>
                            </div>
                  <div class="col-md-12 col-sm-12">
                      <div class="card card-box">
                          <div class="card-head">
                              <header>Informations Elève</header>
                               <!--button id = "panel-button"
                             class = "mdl-button mdl-js-button mdl-button--icon pull-right"
                             data-upgraded = ",MaterialButton">
                             <i class = "material-icons">more_vert</i>
                          </button>
                          <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                             data-mdl-for = "panel-button">
                             <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                             <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                             <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                          </ul-->
                          </div>

                          <div class="card-body" id="bar-parent">
                              <!-- <form  id="FormAddStudent" class="form-horizontal" action="../controller/admission.php" method="post" enctype="multipart/form-data"> -->
                              <form  id="FormAddStudent" class="form-horizontal" action="../controller/preinscription.php" method="post" enctype="multipart/form-data">

                                  <div class="form-body">
                                    <?php
                                    if($etablissementType==1||$etablissementType==3)
                                    {
                                      ?>
                                      <input type="hidden" name="statuseleve" id="statuseleve" value="0">
                                      <?php
                                    }else if($etablissementType==5)
                                    {

                                    }else {
                                      ?>

                                      <div class="form-group row">
                                          <label class="control-label col-md-3">Statut
                                              <span class="required">*  </span>
                                          </label>
                                          <div class="col-md-8">
                                            <select class="form-control input-height" id="statuseleve" name="statuseleve" style="width:100%">
                                                <option value="">Selectionner un statut</option>
                                                <option value="1">Affecté</option>
                                                <option value="2">Non affecté</option>

                                            </select>
                                            </div>
                                      </div>
                                      <?php
                                    }
                                     ?>

                                     <div class="form-group row" id="RowbtnInscrip">
                                     <!--label for="happy" class="col-sm-4 col-md-4 control-label text-right">Are you happy ?</label-->
                                     <label class="control-label col-md-3">
                                     <span class="required">  </span>
                                     </label>
                                     <div class="col-sm-7 col-md-7">
                                     <div class="input-group">
                                     <div id="radioBtn2" class="btn-group">
                                     <a class="btn btn-primary btn-sm active" data-toggle="inscript" data-title="0" onclick="inscription()">INSCRIPTION</a>
                                     <a class="btn btn-primary btn-sm notActive" data-toggle="inscript" data-title="1" onclick="reinscription()">REINSCRIPTION</a>
                                     </div>
                                     <input type="hidden" name="inscript" id="inscript" value="0">
                                     </div>
                                     </div>
                                     </div>

                                     <div class="form-group row" id="RowbtnDoublant">
                               <!--label for="happy" class="col-sm-4 col-md-4 control-label text-right">Are you happy ?</label-->
                               <label class="control-label col-md-3">
                               <span class="required">  </span>
                               </label>
                               <div class="col-sm-7 col-md-7">
                               <div class="input-group">
                               <div id="radioBtn" class="btn-group">
                               <a class="btn btn-primary btn-sm active" data-toggle="doublant" data-title="Y" onclick="doublantN()">NRED</a>
                               <a class="btn btn-primary btn-sm notActive" data-toggle="doublant" data-title="N" onclick="doublant()">RED</a>
                               </div>
                               <input type="hidden" name="doublant" id="doublant" value="0">
                               </div>
                               </div>
                               </div>
                                     <div class="form-group row" id="RowbtnMatricule">
    		<!--label for="happy" class="col-sm-4 col-md-4 control-label text-right">Are you happy ?</label-->
        <label class="control-label col-md-3">
            <span class="required">  </span>
        </label>
      	<div class="col-sm-7 col-md-7">
    			<div class="input-group">
    				<div id="radioBtn1" class="btn-group">
    					<a class="btn btn-primary btn-sm active" data-toggle="happy" data-title="Y" onclick="manuel()">Matricule manuel</a>
    					<a class="btn btn-primary btn-sm notActive" data-toggle="happy" data-title="N" onclick="automatique()">Matricule automatique</a>
    				</div>
    				<input type="hidden" name="matriculestate" id="matriculestate" value="1">
    			</div>
    		</div>
    	</div>


                                    <div class="form-group row" id="matriculerow">
                                            <label class="control-label col-md-3">Matricule
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8">
                                                <input type="text" name="matri" id="matri" data-required="1" placeholder="Entrer le Matricule" class="form-control input-height" />

                                              </div>
                                        </div>
                                  <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::Name?>
                                              <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-8">
                                              <input type="text" name="nomad" id="nomad" data-required="1" placeholder="<?php echo L::EnterName ?>" class="form-control input-height" /> </div>
                                              <?php
                                              if($nbsessionOn>0)
                                              {
                                                ?>
                                                <input type="hidden" name="sessionscolaire" id="sessionscolaire" value="<?php echo $libellesessionencours; ?>">
                                                <?php
                                              }
                                               ?>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3"><?php echo L::PreName?>
                                              <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-8">
                                              <input type="text" name="prenomad" id="prenomad" data-required="1" placeholder="Entrer le prénom" class="form-control input-height" /> </div>
                                              <input type="hidden" name="etape" id="etape" value="1"/>
                                              <input type="hidden" name="newStudent" id="newStudent" value="1"/>
                                              <input type="hidden" name="codeEtab" id="codeEtab"  value="<?php echo $codeEtabLocal;?>"/>
                                              <input type="hidden" name="nbparent" id="nbparent"  value="0"/>
                                              <input type="hidden" name="nboldparent" id="nboldparent"  value="0"/>
                                              <input type="hidden" name="concatparents" id="concatparents"  value=""/>
                                              <input type="hidden" name="concatoldparents" id="concatoldparents"  value=""/>

                                      </div>


                                      <div class="form-group row">
                                        <label class="control-label col-md-3"><?php echo L::BirthstudentTab?>
                                            <span class="required"> * </span>
                                        </label>
                                            <div class="col-md-8">
                                                <input type="text" placeholder="Entrer la date de naissance" name="datenaisad" id="datenaisad" data-mask="99/99/9999" class="form-control input-height">
                                                  <span class="help-block"><?php echo L::Datesymbole ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                                <label class="control-label col-md-3">Lieu Naissance
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type="text" name="lieunais" id="lieunais" data-required="1" placeholder="Entrer le Lieu de Naissance" class="form-control input-height" /> </div>
                                            </div>

                                        <div class="form-group row">
                                            <label class="control-label col-md-3">Genre
                                                <span class="required">*  </span>
                                            </label>
                                            <div class="col-md-8">
                                              <select class="form-control input-height" id="sexe" name="sexe" style="width:100%">
                                                  <option value="">Selectionner un Genre</option>
                                                  <option value="M">Masculin</option>
                                                  <option value="F">Feminin</option>

                                              </select>
                                              </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                                <span class="required">*  </span>
                                            </label>
                                            <div class="col-md-8">
                                              <select class="form-control input-height" id="classeEtab" name="classeEtab" style="width:100%">
                                                  <option value=""><?php echo L::Selectclasses ?></option>
                                                  <?php
                                                  $i=1;
                                                    foreach ($classes as $value):
                                                    ?>
                                                    <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                    <?php
                                                                                     $i++;
                                                                                     endforeach;
                                                                                     ?>

                                              </select>
                                              </div>
                                        </div>
                                        <!-- <div class="form-group row">
                                            <label class="control-label col-md-3">Parents
                                                <span class="required">*  </span>
                                            </label>
                                            <div class="col-md-8">
                                              <select class="form-control input-height" multiple="multiple" id="parenta" name="parenta[]" style="width:100%">
                                                  <option value="">Selectionner un parent</option>
                                                  <?php
                                                  //$i=1;
                                                    //foreach ($parents as $value):
                                                    ?>
                                                    <option value="<?php //echo utf8_encode(utf8_decode($value->id_compte)); ?>"><?php //echo utf8_encode(utf8_decode($value->nom_parent." - ".$value->prenom_parent)); ?></option>

                                                    <?php
                                                                                    // $i++;
                                                                                     //endforeach;
                                                                                     ?>

                                              </select>
                                              </div>
                                        </div> -->
                                        <div id="dynamic_field">

                                        </div>
                                        <div class="form-group row">
                                          <div class="offset-md-3 col-md-9">

                                              <button type="button" class="btn btn-primary" id="parentNew"> <i class="fa fa-plus"></i> <?php echo L::NewParent ?> </button>

                                              <button type="button" class="btn btn-primary" id="parentOld" data-toggle="modal" data-target="#exampleModal"> <i class="fa fa-plus"></i> Ancien parent </button>
                                              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="exampleModalLabel" aria-hidden="true">
					    <div class="modal-dialog" role="document">
					        <div class="modal-content">
					            <div class="modal-header">
					                <h4 class="modal-title" id="exampleModalLabel">Ajouter un ancien parent</h4>
					                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					                    <span aria-hidden="true">&times;</span>
					                </button>
					            </div>
					            <div class="modal-body">
                        <form id="#" class="">
                               <div class="row">

                                 <div class="col-md-12">
                                   <div class="form-group">
                            <label for="benef">Parent<span class="required">*</span> :</label>
                            <select class="form-control input-height"  id="parentaold" name="parentaold" style="width:100%" onchange="selectparentsinfos()">
                                <option value="">Selectionner un parent</option>
                                <?php
                                $i=1;
                                  foreach ($parents as $value):
                                  ?>
                                  <option value="<?php echo utf8_encode(utf8_decode($value->id_compte)); ?>"><?php echo utf8_encode(utf8_decode($value->nom_parent." - ".$value->prenom_parent)); ?></option>

                                  <?php
                                                                   $i++;
                                                                   endforeach;
                                                                   ?>

                            </select>

                                    </div>


                                    </div>

                                    <div class="col-md-12">
                                     <div class="form-group">
                              <label for="oldparentname">Nom<span class="required">*</span> :</label>
                       <input type="text" name="oldparentname" class="form-control" id="oldparentname" value="" size="32" maxlength="225" />

                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                       <div class="form-group">
                                <label for="oldparentprename">Prénoms<span class="required">*</span> :</label>
                         <input type="text" name="oldparentprename" class="form-control" id="oldparentprename" value="" size="32" maxlength="225" />

                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                         <div class="form-group">
                                  <label for="oldparentprename">Genre<span class="required">*</span> :</label>
                           <input type="text" name="oldparentsexe" class="form-control" id="oldparentsexe" value="" size="32" maxlength="225" />

                                            </div>
                                          </div>
                                        <div class="col-md-12">
                                         <div class="form-group">
                                  <label for="oldparentphone">Téléphone<span class="required">*</span> :</label>
                           <input type="text" name="oldparentphone" class="form-control" id="oldparentphone" value="" size="32" maxlength="225" />

                                            </div>
                                          </div>
              <!-- <button type="submit" name="KT_Insert1" id="KT_Insert1" class="btn btn-primary pull-right" style="">Valider <i class="icon-check position-right"></i></button> -->


                               </div>

                             </form>
					            </div>
					            <div class="modal-footer">
					                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo L::Closebtn  ?></button>
					                <button type="button" class="btn btn-primary" id="addoldbtn" onclick="oldbtnaction()" disabled><i class="fa fa-plus"></i> Ajouter</button>
					            </div>
					        </div>
					    </div>
					</div>

                                          </div>
                                        </div>
                                        <div class="form-group row">
                                                <label class="control-label col-md-3">Allergie

                                                </label>
                                                <div class="col-md-8">
                                                    <input type="text" name="allergie" id="allergie" data-required="1" placeholder="Entrer une allergie" class="form-control input-height" /> </div>
                                            </div>
                                            <div class="form-group row">
                                                    <label class="control-label col-md-3">Condition physique

                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="condphy" id="condphy" data-required="1" placeholder="Entrer la condition physique" class="form-control input-height" /> </div>
                                                </div>
                                        <div class="form-group row">
                                            <label class="control-label col-md-3"><?php echo L::Pictures?>

                                            </label>
                                            <div class="compose-editor">
                                              <input type="file" id="photoad" name="photoad" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="../photo/user5.jpg" data-allowed-file-extensions="gif png jpg jpeg pjpeg" />
                                            </div>
                                        </div>



                <div class="form-actions">
                                      <div class="row">
                                          <div class="offset-md-3 col-md-9">
                                            <?php
                                            if($nbsessionOn>0)
                                            {
                                              ?>
                                              <button type="submit" id="submit1" class="btn btn-info" >Enregistrer</button>
                                              <?php
                                            }else if($nbsessionOn==0)
                                            {
                                              ?>
                                              <button type="submit" class="btn btn-info" disabled><?php echo L::Saving?></button>
                                              <?php
                                            }
                                             ?>

                                              <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                          </div>
                                        </div>
                                     </div>
              </div>
                              </form>
                          </div>
                      </div>
                  </div>

              </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script>


 $('#exampleModal').on('hidden.bs.modal', function (e) {
     // window.location.reload(true);
     $("#parentaold").val("");
     $("#oldparentname").val("");
     $("#oldparentprename").val("");
     $("#oldparentphone").val("");

      // $('#parentaold option[value=" "]').prop('selected', true);
      $('#parentaold>option[value=""]').attr('selected', true);

     $("#addoldbtn").prop("disabled",true);
 });

 function oldbtnaction()
 {
   var parentid= $("#parentaold").val();
   var nom=$("#oldparentname").val();
   var prenom=$("#oldparentprename").val();
   var phone=$("#oldparentphone").val();
   var sexe=$("#oldparentsexe").val();

   var concatoldparents=$("#concatoldparents").val();

   var nb=$("#nboldparent").val();
   var nouveau= parseInt(nb)+1;
   // var concatparents=$("#concatparents").val();
   // $("#concatparents").val(concatparents+nouveau+"@");
   $("#concatoldparents").val(concatoldparents+parentid+"@");

   //recalcule nbconcatparent
   recalculoldparentnb();

   var ligne="<div class=\"form-group row\" id=\"ligneEnteteold"+parentid+"\">";
   ligne=ligne+"<label class=\"control-label col-md-8\" style=\"color:#e40d22\">INFORMATIONS ANCIEN PARENT "+nouveau+" <span class=\"required\">  </span></label>";
   ligne=ligne+"</div>";
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowNameold"+parentid+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Nom <span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-8\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"nomparentold"+parentid+"\"  id=\"nomparentold"+parentid+"\" value="+nom+" readonly >";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //prenoms
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPrenameold"+parentid+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Prénoms <span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-8\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"prenomparentold"+parentid+"\"  id=\"prenomparentold"+parentid+"\" value="+prenom+" readonly>";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //sexe
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowSexeold"+parentid+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Genre <span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-8\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"sexeparentold"+parentid+"\"  id=\"sexeparentold"+parentid+"\" value="+sexe+" readonly>";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";

   //telephone
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPhoneold"+parentid+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Téléphone <span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-8\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"phoneparentold"+parentid+"\"  id=\"phoneparentold"+parentid+"\" value="+phone+" readonly>";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //bouton de suppression
   ligne=ligne+"<div class=\"pull-right\" id=\"ligneRowold"+parentid+"\">";
   ligne=ligne+"<button type=\"button\" id=\"delete"+parentid+"\" name=\"delete"+parentid+"\"  onclick=\"deletedoldrow("+parentid+")\" class=\"btn btn-danger pull-right\"><i class=\"fa fa-trash\"></i></button>";
   ligne=ligne+"</div></br></br>";

   $('#dynamic_field').append(ligne);

   // $('#exampleModal').hide();
   $('#exampleModal').modal('hide');

 }

 function selectparentsinfos()
 {
   var etape=4;
   $.ajax({
     url: '../ajax/login.php',
     type: 'POST',
     async:false,
     data: 'parentid=' + $("#parentaold").val()+ '&etape=' + etape,
     dataType: 'text',
     success: function (response, statut) {

       var nomparent=response.split("*")[0];
       var prenomparent=response.split("*")[1];
       var phoneparent=response.split("*")[2];
       var sexeparent=response.split("*")[6];

       $("#oldparentname").val(nomparent);
       $("#oldparentprename").val(prenomparent);
       $("#oldparentphone").val(phoneparent);
       $("#oldparentsexe").val(sexeparent);

       $("#addoldbtn").prop("disabled",false);


     }
   });
 }
function activatebtn()
{
  $("#submit1").prop("disabled",false);

}

function desactivatebtn()
{
  $("#submit1").prop("disabled",true);
}

function deletedrow(id)
{
  var concatparents=$("#concatparents").val();
  $("#concatparents").val($("#concatparents").val().replace(id+"@", ""));

  $("#ligneEntete"+id).remove();
  $("#ligneRowName"+id).remove();
  $("#ligneRowPrename"+id).remove();
  $("#ligneRowPhone"+id).remove();
  $("#ligneRowSexe"+id).remove();
  $("#ligneRow"+id).remove();

  recalculparentnb();

}

function deletedoldrow(parentid)
{
  var concatparents=$("#concatparents").val();
  var concatoldparents=$("#concatoldparents").val();
  // $("#concatparents").val($("#concatparents").val().replace(id+"@", ""));
  $("#concatoldparents").val($("#concatoldparents").val().replace(parentid+"@", ""));

  $("#ligneEnteteold"+parentid).remove();
  $("#ligneRowNameold"+parentid).remove();
  $("#ligneRowPrenameold"+parentid).remove();
  $("#ligneRowPhoneold"+parentid).remove();
  $("#ligneRowSexeold"+parentid).remove();
  $("#ligneRowold"+parentid).remove();

  recalculparentnb();
}

 function recalculparentnb()
 {
   //calcul du nombre de nouveau parent
   var concatparents=$("#concatparents").val();
   var tab=concatparents.split("@");
   var nbtab=tab.length;
   var nbtabnew=parseInt(nbtab)-1;
   $("#nbparent").val(nbtabnew);
   //calcul du nombre ancien parent
   var concatoldparents=$("#concatoldparents").val();
   var tabold=concatoldparents.split("@");
   var nbtabold=tabold.length;
   var nbtaboldnew=parseInt(nbtabold)-1;
   $("#nboldparent").val(nbtaboldnew);

   if((nbtabnew==0)&&(nbtaboldnew==0))
   {
     //actualiser la page

      // location.reload();
       desactivatebtn();
   }else if((nbtabnew>0)||(nbtaboldnew>0)){
     activatebtn();
   }else if((nbtabnew<0)||(nbtaboldnew<0))
   {
     desactivatebtn();
   }
 }

 function recalculoldparentnb()
 {
   //calcul du nombre de nouveau parent
   var concatparents=$("#concatparents").val();
   var tab=concatparents.split("@");
   var nbtab=tab.length;
   var nbtabnew=parseInt(nbtab)-1;
   $("#nbparent").val(nbtabnew);
   //calcul du nombre ancien parent
   var concatoldparents=$("#concatoldparents").val();
   var tabold=concatoldparents.split("@");
   var nbtabold=tabold.length;
   var nbtaboldnew=parseInt(nbtabold)-1;
   $("#nboldparent").val(nbtaboldnew);

   if((nbtabnew==0)&&(nbtaboldnew==0))
   {
     //actualiser la page

      // location.reload();
      desactivatebtn();
   }else if((nbtabnew>0)||(nbtaboldnew>0)){
     activatebtn();
   }else if((nbtabnew<0)||(nbtaboldnew<0))
   {
     desactivatebtn();
   }
 }

 function telchecked(id)
 {
   var telephone=$("#phoneparent"+id).val();
   var etape=2;
   $.ajax({
     url: '../ajax/login.php',
     type: 'POST',
     async:true,
      data: 'telephone='+telephone+'&etape='+etape,
     dataType: 'text',
     success: function (response, statut) {

       if(response==0)
       {

       }else if(response>0)
       {
         Swal.fire({
 		            type: 'warning',
 		            title: '<?php echo L::WarningLib ?>',
 		            text: "Nous avons dejà un parent avec ce numéro de téléphone dans le système",

 		            })

                deletedrow(id);

                var etape=5;
                $.ajax({
                  url: '../ajax/login.php',
                  type: 'POST',
                  async:true,
                   data: 'telephone='+telephone+'&etape='+etape,
                  dataType: 'text',
                  success: function (response, statut) {
                    var parentid=response.split("*")[11];
                    var nom=response.split("*")[0];
                    var prenom=response.split("*")[1];
                    var phone=response.split("*")[2];
                    var sexe=response.split("*")[6];



                    var concatoldparents=$("#concatoldparents").val();

                    var nb=$("#nboldparent").val();
                    var nouveau= parseInt(nb)+1;
                    // var concatparents=$("#concatparents").val();
                    // $("#concatparents").val(concatparents+nouveau+"@");
                    $("#concatoldparents").val(concatoldparents+parentid+"@");

                    //recalcule nbconcatparent
                    recalculoldparentnb();

                    var ligne="<div class=\"form-group row\" id=\"ligneEnteteold"+parentid+"\">";
                    ligne=ligne+"<label class=\"control-label col-md-8\" style=\"color:#e40d22\">INFORMATIONS ANCIEN PARENT "+nouveau+" <span class=\"required\">  </span></label>";
                    ligne=ligne+"</div>";
                    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowNameold"+parentid+"\">";
                    ligne=ligne+"<label class=\"control-label col-md-3\">Nom <span class=\"required\"> * </span></label>";
                    ligne=ligne+"<div class=\"col-md-8\">";
                    ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"nomparentold"+parentid+"\"  id=\"nomparentold"+parentid+"\" value="+nom+" readonly >";
                    ligne=ligne+"</div>";
                    ligne=ligne+"</div>";
                    //prenoms
                    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPrenameold"+parentid+"\">";
                    ligne=ligne+"<label class=\"control-label col-md-3\">Prénoms <span class=\"required\"> * </span></label>";
                    ligne=ligne+"<div class=\"col-md-8\">";
                    ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"prenomparentold"+parentid+"\"  id=\"prenomparentold"+parentid+"\" value="+prenom+" readonly>";
                    ligne=ligne+"</div>";
                    ligne=ligne+"</div>";
                    //sexe
                    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowSexeold"+parentid+"\">";
                    ligne=ligne+"<label class=\"control-label col-md-3\">Genre <span class=\"required\"> * </span></label>";
                    ligne=ligne+"<div class=\"col-md-8\">";
                    ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"sexeparentold"+parentid+"\"  id=\"sexeparentold"+parentid+"\" value="+sexe+" readonly>";
                    ligne=ligne+"</div>";
                    ligne=ligne+"</div>";

                    //telephone
                    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPhoneold"+parentid+"\">";
                    ligne=ligne+"<label class=\"control-label col-md-3\">Téléphone <span class=\"required\"> * </span></label>";
                    ligne=ligne+"<div class=\"col-md-8\">";
                    ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"phoneparentold"+parentid+"\"  id=\"phoneparentold"+parentid+"\" value="+phone+" readonly>";
                    ligne=ligne+"</div>";
                    ligne=ligne+"</div>";
                    //bouton de suppression
                    ligne=ligne+"<div class=\"pull-right\" id=\"ligneRowold"+parentid+"\">";
                    ligne=ligne+"<button type=\"button\" id=\"delete"+parentid+"\" name=\"delete"+parentid+"\"  onclick=\"deletedoldrow("+parentid+")\" class=\"btn btn-danger pull-right\"><i class=\"fa fa-trash\"></i></button>";
                    ligne=ligne+"</div></br></br>";

                    $('#dynamic_field').append(ligne);

                  }
                });

       }

     }});
 }

 function AddparentsRow()
 {
   var nb=$("#nbparent").val();
   var nouveau= parseInt(nb)+1;
   var concatparents=$("#concatparents").val();
   $("#concatparents").val(concatparents+nouveau+"@");

   //recalcule nbconcatparent
   recalculparentnb();

   var ligne="<div class=\"form-group row\" id=\"ligneEntete"+nouveau+"\">";
   ligne=ligne+"<label class=\"control-label col-md-8\" style=\"color:#e40d22\">INFORMATIONS PARENT "+nouveau+" <span class=\"required\">  </span></label>";
   ligne=ligne+"</div>";
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowName"+nouveau+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Nom <span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-8\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"nomparent"+nouveau+"\"  id=\"nomparent"+nouveau+"\" >";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //prenoms
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPrename"+nouveau+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Prénoms <span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-8\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"prenomparent"+nouveau+"\"  id=\"prenomparent"+nouveau+"\" >";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //sexe
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowSexe"+nouveau+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Genre<span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-8\">";
   ligne=ligne+"<select class=\"form-control \" id=\"sexeparent"+nouveau+"\" name=\"sexeparent"+nouveau+"\" style=\"width:100%\">";
   ligne=ligne+"<option value=\"\">Selectionner un Genre</option>";
   ligne=ligne+"<option value=\"M\">Masculin</option>";
   ligne=ligne+"<option value=\"F\">Feminin</option>";
   ligne=ligne+"</select>";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //telephone
   ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPhone"+nouveau+"\">";
   ligne=ligne+"<label class=\"control-label col-md-3\">Téléphone <span class=\"required\"> * </span></label>";
   ligne=ligne+"<div class=\"col-md-8\">";
   ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"phoneparent"+nouveau+"\"  id=\"phoneparent"+nouveau+"\"  onchange=\"telchecked("+nouveau+")\" >";
   ligne=ligne+"</div>";
   ligne=ligne+"</div>";
   //bouton de suppression
   ligne=ligne+"<div class=\"pull-right\" id=\"ligneRow"+nouveau+"\">";
   ligne=ligne+"<button type=\"button\" id=\"delete"+nouveau+"\" name=\"delete"+nouveau+"\"  onclick=\"deletedrow("+nouveau+")\" class=\"btn btn-danger pull-right\"><i class=\"fa fa-trash\"></i></button>";
   ligne=ligne+"</div></br></br>";

   $("#sexeparent"+nouveau).select2();

   $('#dynamic_field').append(ligne);

   $('#nomparent'+nouveau).rules( "add", {
       required: true,
       messages: {
       required: "Merci de renseigner le nom du parent"
}
     });

     $('#sexeparent'+nouveau).rules( "add", {
         required: true,
         messages: {
         required: "Merci de renseigner le genre du parent"
  }
       });

     $('#prenomparent'+nouveau).rules( "add", {
         required: true,
         messages: {
         required: "Merci de renseigner le prénom du parent"
  }
       });

       $('#phoneparent'+nouveau).rules( "add", {
           required: true,
           digits:true,
           messages: {
           required: "Merci de renseigner le téléphone du parent",
           digits:"<?php echo L::DigitsOnly?>"
    }
         });

 }


function doublantN()
{
  $("#doublant").val(0);
}

function doublant()
{
$("#doublant").val(1);
}

 function manuel()
 {
   $("#matriculestate").val(1);
   $("#matri").val("");
   $("#matri").attr("disabled",false);
   $("#etape").val(1);
   $("#matriculerow").show();


 }

 function automatique()
 {

   $("#matriculestate").val(2);
   $("#matri").val("");
   $("#matri").attr("disabled",true);
   $("#etape").val(2);
   $("#matriculerow").hide();
 }

 $('#radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);

    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
})

$('#radioBtn1 a').on('click', function(){
   var sel = $(this).data('title');
   var tog = $(this).data('toggle');
   $('#'+tog).prop('value', sel);

   $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
   $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
})

$('#radioBtn2 a').on('click', function(){
   var sel = $(this).data('title');
   var tog = $(this).data('toggle');
   $('#'+tog).prop('value', sel);

   $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
   $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
})
 jQuery(document).ready(function() {



$("#parenta").select2();
$("#parentaold").select2();
$("#sexe").select2();
$("#classeEtab").select2();

desactivatebtn();

<?php
if($etablissementType==1||$etablissementType==3)
{

}else
{
  ?>
$("#statuseleve").select2();
  <?php
}
 ?>

 $('#parentNew').click(function(){
       AddparentsRow();

     });


   $("#FormAddStudent").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{
        lieunais:"required",
        matri:"required",
        parenta:"required",
        classeEtab:"required",
        sexe:"required",
        passad: {
            required: true,
            minlength: 6
        },
        confirmtad:{
            required: true,
            minlength: 6,
            equalTo:'#passad'
        },
        fonctionad:"required",
        statuseleve:"required",

        loginad:"required",
        emailad: {
                   required: true,
                   email: true
               },
        contactad:"required",
        datenaisad:"required",
        prenomad:"required",
        nomad:"required"


      },
      messages: {
        lieunais:"Merci de renseigner le Lieu de naissance",
        matri:"Merci de renseigner le Matricule",
        parenta:"Merci de selectionner le parent",
        classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
        sexe:"Merci de selectionner le Genre",
        statuseleve:"Merci de selectionner le statut de l'élève",
        confirmtad:{
            required:"<?php echo L::Confirmcheck?>",
            minlength:"<?php echo L::Confirmincheck?>",
            equalTo: "<?php echo L::ConfirmSamecheck?>"
        },
        passad: {
            required:"<?php echo L::Passcheck?>",
            minlength:"<?php echo L::Confirmincheck?>"
        },
        loginad:"<?php echo L::Logincheck?>",
        emailad:"<?php echo L::PleaseEnterEmailAdress?>",
        contactad:"<?php echo L::PleaseEnterPhoneNumber?>",
        datenaisad:"<?php echo L::PleaseEnterPhonestudentTab?>",
        prenomad:"<?php echo L::PleaseEnterPrename?>",
        nomad:"<?php echo L::PleaseEnterName?>",
        fonctionad:"Merci de renseigner la fonction"
      },
      submitHandler: function(form) {
        //verifier si ce compte n'existe pas encore dans la base de données
           var etape=1;
           $.ajax({
             url: '../ajax/admission.php',
             type: 'POST',
             async:false,
             data: 'matricule=' + $("#matri").val()+ '&etape=' + etape+'&classe='+$("#classeEtab").val()+'&parent='+$("#parenta").val(),
             dataType: 'text',
             success: function (content, statut) {

               if(content==0)
               {
                 //le compte n'existe pas dans la base on peut l'ajouter

                 form.submit();
               }else if(content==1){
                 //le compte existe dejà dans la base de données
                 Swal.fire({
                 type: 'warning',
                 title: '<?php echo L::WarningLib ?>',
                 text: "Cet Elève existe dejà dans la base de données des elèves incrits pour l'année encours",

               })

             }else if(content==2)
             {
               $("#newStudent").val(0);
               form.submit();
             }

             }


           });
      }


   });


 });
 </script>
    <!-- end js include path -->
  </body>

</html>
