<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
$agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$libellesessionencours="";

  if($etablissementType==1||$etablissementType==3)
  {
    if($nbsessionOn>0){
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];
      // $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
      // $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      // $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
    }

  }else {
    if($nbsessionOn>0)
    {
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];
      $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
      $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
    }
  }

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);

$_SESSION['user']['session']=$libellesessionencours;

$_SESSION['user']['codeEtab']=$codeEtabAssigner;

$notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

// $notifications=$etabs->getAllMessagesLast($codeEtabAssigner,$libellesessionencours);




 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <style media="screen">
    /* .f30 {
font-size: 30px;
}
.text-primary {
color: #a768f3 !important;
}
.app-header .d-user span, .f12 {
font-size: 12px;
}

.wb-icon-box {
display: inline-block;
width: 50px;
height: 50px;
text-align: center;
line-height: 60px;
}
.f30 {
font-size: 30px;
}
.task-d-list {
margin-bottom: 0;
}
.custom-checkbox .custom-control-indicator {
border-radius: .25rem;
}
.custom-control-indicator {
position: absolute;
top: .25rem;
left: 0;
display: block;
width: 1rem;
height: 1rem;
pointer-events: none;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
background-color: #ddd;
background-repeat: no-repeat;
background-position: center center;
background-size: 50% 50%;
}
.task-d-list li:hover .task-list-action {
opacity: 1;
}
.task-d-list li .task-list-action {
opacity: 0;
}
.task-d-list li {
margin-bottom: 14px;
}
.quick-links-grid {
display: inline-block;
width: 100%;
}
.quick-links-grid .ql-grid-item {
display: inherit;
width: 49.3%;
padding: 20px 5px;
text-align: center;
vertical-align: middle;
text-decoration: none;
color: #45567c;
}
.quick-links-grid .ql-grid-item i {
-webkit-transition: all .3s;
transition: all .3s;
text-align: center;
color: #d2d6eb;
font-size: 32px;
}
.quick-links-grid .ql-grid-item .ql-grid-title {
display: block;
margin: 10px 0 0;
text-align: center;
font-size: 12px;
font-weight: 400;
line-height: 1;
}
.f60 {
font-size: 60px;
}
.table-vertical-middle tr td {
vertical-align: middle;
}
.table td, .table th {
padding: 0.6 rem 1rem;
}
.border, .table td, .table th {
border-color: #e5e9ec !important;
}
.table tfoot th, .table thead th {
vertical-align: bottom;
border-bottom: none;
}
.table thead th {
border-top: none;
}
.card .card-header .card-title {
	margin-bottom: 0;
	color: #53505f;
	font-size: 18px;
	font-weight: 500;
}
.lobicard-custom-icon > .card-header .dropdown, .card-shadow .card-header .dropdown, .lobicard-custom-control .card-header .dropdown {
	display: inline-block;
	float: right;
	position: relative;
}
.lobicard-custom-icon > .card-header .dropdown .tools, .card-shadow .card-header .dropdown .tools, .lobicard-custom-control .card-header .dropdown .tools {
	margin: 0px;
}
.card-shadow {
	border: none;
	box-shadow: 0 1px 10px 1px rgba(115,108,203,.1);
}
.card .card-header {
	padding: 1rem;
	border-bottom: 1px solid #e5e9ec;
	background: #fff;
}
.nav-pills-sm .nav-link {
	padding: .4em 1em;
	font-size: 12px;
}
.nav-pill-custom .nav-link, .nav-pills-sm .nav-link {
	border-radius: 30px !important;
}
.lobicard-custom-control .card-header .dropdown .tools {
	margin: 0px;
}
.lobicard-custom-control .card-header .dropdown .tools a {
	color: #fff;
} */
    </style>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::MessagesDetails ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li>&nbsp;<a class="parent-item" href="allmessages.php"><?php echo L::NotificationMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::MessagesDetails ?></li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
          <div class="row ">
            

                <!-- /.col -->

                <!-- /.col -->

                <!-- /.col -->

                <!-- /.col -->
              </div>
					<div class="state-overview">

						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


          <div class="row">

             <div class="col-md-12 col-sm-12">




             </div>

                    </div>




                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
  <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- morris chart -->

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>

     <script src="../assets2/plugins/chart-js/Chart.bundle.js" ></script>
     <script src="../assets2/plugins/chart-js/utils.js" ></script>
     <!-- <script src="../assets2/js/pages/chart/chartjs/chartjs-data.js" ></script> -->
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function loader()
   {
     var etape=1;
     var codeEtab="<?php echo $codeEtabAssigner;?>";
     var session="<?php echo $libellesessionencours ; ?>";

     $.ajax({
       url: '../ajax/load.php',
       type: 'POST',
       async:false,
       data: 'codeEtab=' +codeEtab+ '&etape='+etape+'&session='+session,
       dataType: 'text',
       success: function (content, statut) {

         var data=content;

         // callback(data);

       }
     });

   }

   $(document).ready(function() {

     // alert(session);

     var MONTHS = ['<?php echo L::JanvLib?>','<?php echo L::FevLib ?>','<?php echo L::MarsLib ?>','<?php echo L::AvriLib ?>','<?php echo L::MaiLib ?>','<?php echo L::JuneLib ?>','<?php echo L::JulLib ?>','<?php echo L::AoutLib ?>','<?php echo L::SeptLib?>','<?php echo L::OctobLib?>','<?php echo L::NovbLib?>','<?php echo L::DecemLib?>'];
        var config = {
            type: 'line',
            data: {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [{
                    label: "Paiements Attendus",
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor()
                    ],
                    fill: false,
                }, {
                    label: "Paiements reçus",
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor()
                    ],
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Mois'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: ''
                        }
                    }]
                }
            }
        };
        var ctx = document.getElementById("chartjs_line").getContext("2d");
        window.myLine = new Chart(ctx, config);

     // var randomScalingFactor = function() {
     //       return Math.round(Math.random() * 100);
     //   };

       var config = {
           type: 'doughnut',
           data: {
               datasets: [{
                   data: [
                       <?php echo $etabs->getnumberOfwomenchild($codeEtabAssigner,$libellesessionencours); ?>,
                       // randomScalingFactor(),
                       // randomScalingFactor(),
                       // randomScalingFactor(),
                       <?php echo $etabs->getnumberOfmenchild($codeEtabAssigner,$libellesessionencours); ?>,
                   ],
                   backgroundColor: [
                       window.chartColors.red,
                       // window.chartColors.orange,
                       // window.chartColors.yellow,
                       // window.chartColors.green,
                       window.chartColors.blue,
                   ],
                   label: 'Dataset 1'
               }],
               labels: [
                   "<?php echo L::Filles ?>",
                   // "Orange",
                   // "Yellow",
                   // "Green",
                   "<?php echo L::Garcons ?>"
               ]
           },
           options: {
               responsive: true,
               legend: {
                   position: 'top',
               },
               title: {
                   display: true,
                   text: ''
               },
               animation: {
                   animateScale: true,
                   animateRotate: true
               }
           }
       };

           var ctx = document.getElementById("chartjs_doughnut").getContext("2d");
           window.myDoughnut = new Chart(ctx, config);


     var calendar = $('#calendar1').fullCalendar({
       monthNames: ['<?php echo L::JanvLib?>','<?php echo L::FevLib ?>','<?php echo L::MarsLib ?>','<?php echo L::AvriLib ?>','<?php echo L::MaiLib ?>','<?php echo L::JuneLib ?>','<?php echo L::JulLib ?>','<?php echo L::AoutLib ?>','<?php echo L::SeptLib?>','<?php echo L::OctobLib?>','<?php echo L::NovbLib?>','<?php echo L::DecemLib?>'],
       monthNamesShort: ['<?php echo L::JanvLibshort ?>','<?php echo L::FevLibshort ?>','<?php echo L::MarsLibshort ?>','<?php echo L::AvriLibshort ?>','<?php echo L::MaiLibshort ?>','<?php echo L::Juinbshort ?>','<?php echo L::JulLibshort ?>','<?php echo L::AoutLibshort ?>','<?php echo L::SeptLibshort?>','<?php echo L::OctobLibshort?>','<?php echo L::NovbLibshort?>','<?php echo L::DecemLibshort?>'],
       dayNamesShort: ['<?php echo L::Dayslibshort7 ?>', '<?php echo L::Dayslibshort1 ?>', '<?php echo L::Dayslibshort2 ?>', '<?php echo L::Dayslibshort3 ?>', '<?php echo L::Dayslibshort4 ?>', '<?php echo L::Dayslibshort5 ?>', '<?php echo L::Dayslibshort6 ?>'],
       dayNames: ['<?php echo L::Dayslibs7 ?>', '<?php echo L::Dayslibs1 ?>', '<?php echo L::Dayslibs2 ?>', '<?php echo L::Dayslibs3 ?>', '<?php echo L::Dayslibs4 ?>', '<?php echo L::Dayslibs5 ?>', '<?php echo L::Dayslibs6 ?>'],
       timeFormat:'H(:mm)',
       timeZone: 'UTC',
       allDayText:'<?php echo L::Daysall ?>',
       // minTime: "08:00",
       // maxTime: "20:00",
       buttonText:{
  today:    '<?php echo L::TodayLib ?>',
  month:    '<?php echo L::MonthLib ?>',
  week:     '<?php echo L::SemaineLib ?>',
  day:      '<?php echo L::jourLib ?>',
  list:     'list'
},
    locale: 'fr',
    editable:true,
    header:{
     left:'prev,next today',
     center:'title',
     right:'month,agendaWeek,agendaDay'
    },
    events:'../ajax/load.php?view=1',
   // events:loader(),
    // events: 'load.php?codeEtab='+codeEtab+'&session='+session+'&etape='+etape,
    selectable:false,
    selectHelper:false,
    select: function(start, end, allDay)
    {
     var title = prompt("Enter Event Title");
     if(title)
     {
      var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
      var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
      $.ajax({
       url:"insert.php",
       type:"POST",
       data:{title:title, start:start, end:end},
       success:function()
       {
        calendar.fullCalendar('refetchEvents');
        alert("Added Successfully");
       }
      })
     }
    },
    editable:false,
    eventResize:function(event)
    {
     var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
     var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
     var title = event.title;
     var id = event.id;
     $.ajax({
      url:"update.php",
      type:"POST",
      data:{title:title, start:start, end:end, id:id},
      success:function(){
       calendar.fullCalendar('refetchEvents');
       alert('Event Update');
      }
     })
    },

    eventDrop:function(event)
    {
     var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
     var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
     var title = event.title;
     var id = event.id;
     $.ajax({
      url:"update.php",
      type:"POST",
      data:{title:title, start:start, end:end, id:id},
      success:function()
      {
       calendar.fullCalendar('refetchEvents');
       alert("Event Updated");
      }
     });
   },
   eventClick:function(event)
   {
     var id = event.id;
     var etape=7;
     var codeEtab="<?php echo $codeEtabAssigner;?>";
     var session="<?php echo $libellesessionencours ; ?>";
     // alert(id);
     $.ajax({
       url: '../ajax/login.php',
       type: 'POST',
       async:false,
       data: 'codeEtab=' +codeEtab+ '&etape='+etape+'&session='+session+'&parascolaire='+id,
       dataType: 'text',
       success: function (content, statut) {

         // var data=content;
         var cut = $.trim(content);
         $tabcontent=cut.split("*");

         $("#oldparentname").val($tabcontent[0]);
         $("#oldparentprename").val($tabcontent[1]+" "+$tabcontent[2]);
         $("#oldparentsexe").val($tabcontent[3]+" "+$tabcontent[4]);
         $("#oldparentphone").val($tabcontent[5]);
         $("#descri").html($tabcontent[10]);

         var gratuit=$tabcontent[7];
         var montant=$tabcontent[8];

         if(gratuit==0)
         {
           $("#rowgratuit").show();
           $("#rowpayant").hide();

         }else {
           $("#rowpayant").show();
           $("#rowgratuit").hide();
           $("#montpayant").html(montant+" FCFA");
         }

         // alert($tabcontent[11]);

         // callback(data);

          $("#parentOld").click();

       }
     });

   },

    // ,
    //
    // eventClick:function(event)
    // {
    //  if(confirm("Are you sure you want to remove it?"))
    //  {
    //   var id = event.id;
    //   $.ajax({
    //    url:"delete.php",
    //    type:"POST",
    //    data:{id:id},
    //    success:function()
    //    {
    //     calendar.fullCalendar('refetchEvents');
    //     alert("Event Removed");
    //    }
    //   })
    //  }
    // },

   });

   });

   </script>
    <!-- end js include path -->
  </body>

</html>
