<?php
session_start();
require_once('../class/User.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$etabs=new Etab();
$etab=new Etab();
$parent=new ParentX();
$user=new User();
$classe=new Classe();
$student=new Student();
$emailUti=$_SESSION['user']['email'];
$userId=$_SESSION['user']['IdCompte'];
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$codeEtabLocal=$etab->getcodeEtabByLocalId($userId);
$classes=$classe->getAllClassesbyschoolCode($codeEtabLocal);
$parents=$parent->getAllParentSchool($codeEtabLocal);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabLocal);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabLocal);
$allstudentschools=$student->getAllStudentOfThisSchool($codeEtabLocal);


if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabLocal);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabLocal,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabLocal,$libellesessionencours);
  $classes=$classe->getAllClassesbyschoolCodeAndsession($codeEtabLocal,$libellesessionencours);
}


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
  <!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
  <!--bootstrap -->
  <!--bootstrap -->
  <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
  <!-- Material Design Lite CSS -->
  <link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
  <link href="../assets2/css/material_style.css" rel="stylesheet">
  <!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <link rel="stylesheet" href="../assets2/css/pages/steps.css">
    <style>
    #radioBtn .notActive{
  color: #3276b1;
  background-color: #fff;
}
#radioBtn1 .notActive{
color: #3276b1;
background-color: #fff;
}
#radioBtn2 .notActive{
color: #3276b1;
background-color: #fff;
}

#radioBtn3 .notActive{
color: #3276b1;
background-color: #fff;
}
    </style>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title">Créer une fiche</div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#">Elèves</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active">Créer une fiche</li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addStudok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: '<?php echo L::Felicitations ?>',
  text: "<?php echo $_SESSION['user']['addStudok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::AddNews ?>',
  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="index.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addStudok']);
                    }

                     ?>

              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="card card-topline-green">
                       <div class="card-head">
                           <header>Informations</header>
                           <div class="tools">
                               <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
       <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
       <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                           </div>
                       </div>
                       <div class="card-body ">
                         <p>
                           <?php
                           if($nbsessionOn>0)
                           {
                             ?>
                             L'admission de nouveaux étudiants crée automatiquement une inscription à la classe sélectionnée dans la session en cours.
                             Veuillez vérifier et revérifier les informations que vous avez insérées car une fois que vous admettez un nouvel étudiant, vous ne pourrez pas modifier sa classe, son rôle, sa section sans passer à la session suivante.
                             <?php
                           }else if($nbsessionOn==0)
                           {
                             ?>
                             Afin de pouvoir faire l'admission de nouveaux élèves nous vous invitons à mentionner l'année scolaire dans l'onglet Enseignement
                             <?php
                           }
                            ?>

                         </p>
                       </div>
                   </div>
                            </div>


              </div>
              <div class="row">
                    	<div class="col-sm-12">
                             <div class="card-box">
                                 <div class="card-head">
                                     <header></header>
                                 </div>
                                 <div class="card-body ">
                                   <form class="" action="../controller/preinscription.php" method="post" id="FormAddStudent" enctype="multipart/form-data">

                                   <!-- <form class="" action="../controller/preinscription.php" method="post" id="FormAddStudent"> -->



                                       <fieldset>
                                         <legend style="background-color:#d8dbe0;height:50px;text-align:center"><span style='font-family: "Trebuchet MS", Verdana, sans-serif;font-style: italic;font-variant-ligatures: no-common-ligatures;font-size:medium;'>INSCRIPTION / REINSCRIPTION</span></legend>
                                        <br>  <div class="form-group row" id="RowbtnInscrip">
                                         <!--label for="happy" class="col-sm-4 col-md-4 control-label text-right">Are you happy ?</label-->
                                         <!-- <label class="control-label col-md-3">
                                         <span class="required">  </span>
                                         </label> -->
                                         <div class="col-sm-7 col-md-7">
                                         <div class="input-group">
                                         <div id="radioBtn2" class="btn-group">
                                         <a class="btn btn-primary btn-sm active" id="btn21" data-toggle="inscript" data-title="1" onclick="inscription()">INSCRIPTION</a>
                                         <a class="btn btn-primary btn-sm notActive" id="btn22" data-toggle="inscript" data-title="2" onclick="reinscription()">REINSCRIPTION</a>
                                         </div>
                                         <input type="hidden" name="inscript" id="inscript" value="1">
                                         </div>
                                         </div>
                                         </div>

                                         <div class="form-group row" id="RowbtnDoublant">
                                   <!--label for="happy" class="col-sm-4 col-md-4 control-label text-right">Are you happy ?</label-->
                                   <!-- <label class="control-label col-md-3">
                                   <span class="required">  </span>
                                   </label> -->
                                   <div class="col-sm-7 col-md-7">
                                   <div class="input-group">
                                   <div id="radioBtn" class="btn-group">
                                   <a class="btn btn-primary btn-sm active" id="btn1" data-toggle="doublant" data-title="0" onclick="doublantN()">NRED</a>
                                   <a class="btn btn-primary btn-sm notActive" id="btn2" data-toggle="doublant" data-title="1" onclick="doublant()">RED</a>
                                   </div>
                                   <input type="hidden" name="doublant" id="doublant" value="0">
                                   </div>
                                   </div>
                                   </div>
                                         <div class="form-group row" id="RowbtnMatricule">
        		<!--label for="happy" class="col-sm-4 col-md-4 control-label text-right">Are you happy ?</label-->
            <!-- <label class="control-label col-md-3">
                <span class="required">  </span>
            </label> -->
          	<div class="col-sm-7 col-md-7">
        			<div class="input-group">
        				<div id="radioBtn1" class="btn-group">
        					<a class="btn btn-primary btn-sm active" id="btn11" data-toggle="matriculestate" data-title="1" onclick="manuel()">Matricule manuel</a>
        					<a class="btn btn-primary btn-sm notActive" id="btn12" data-toggle="matriculestate" data-title="2" onclick="automatique()">Matricule automatique</a>
        				</div>
        				<input type="hidden" name="matriculestate" id="matriculestate" value="1">
        			</div>
        		</div>
        	</div>
          <div class="form-group row " id="matriculerowInscript">
                  <label class="control-label col-md-3">Matricule
                      <span class="required"> * </span>
                  </label>
                  <div class="col-md-6">
                      <input type="text" name="matri" id="matri" data-required="1" placeholder="Entrer le Matricule" class="form-control" onchange="checkmatri()"/>

                    </div>
              </div>
              <div class="form-group row " id="matriculerowReinscript">
                      <label class="control-label col-md-3">Matricule
                          <span class="required"> * </span>
                      </label>
                      <div class="col-md-6">
                        <select class="form-control" name="matriselect" id="matriselect" style="width:100%" onchange="checkmatri()">
                          <option value="">Selectionner le matricule d'un élève</option>
                          <?php
                            foreach ($allstudentschools as $value):
                              ?>
                                <option value="<?php echo utf8_encode(utf8_decode($value->matricule_eleve)); ?>"><?php echo utf8_encode(utf8_decode($value->matricule_eleve)); ?></option>
                              <?php
                            endforeach;
                           ?>

                        </select>


                        </div>
                  </div>
        <div class="form-group row">
                <label class="control-label col-md-3"><?php echo L::Name?>
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input type="text" name="nomad" id="nomad" data-required="1" placeholder="<?php echo L::EnterName ?>" class="form-control" /> </div>
                    <input type="hidden" name="sessionscolaire" id="sessionscolaire" value="<?php echo $libellesessionencours; ?>">
            </div>

            <div class="form-group row">
                <label class="control-label col-md-3"><?php echo L::PreName?>
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input type="text" name="prenomad" id="prenomad" data-required="1" placeholder="Entrer le prénom" class="form-control" /> </div>
                    <!-- <input type="hidden" name="etape" id="etape" value="3"/> -->
                    <input type="hidden" name="etape" id="etape" value="4"/>
                    <input type="hidden" name="newStudent" id="newStudent" value="1"/>
                    <input type="hidden" name="codeEtab" id="codeEtab"  value="<?php echo $codeEtabLocal;?>"/>
                    <input type="hidden" name="nbtotalparent" id="nbtotalparent"  value="0"/>
                    <input type="hidden" name="nbparent" id="nbparent"  value="0"/>
                    <input type="hidden" name="nboldparent" id="nboldparent"  value="0"/>
                    <input type="hidden" name="concatparents" id="concatparents"  value=""/>
                    <input type="hidden" name="concatoldparents" id="concatoldparents"  value=""/>

            </div>

            <div class="form-group row">
              <label class="control-label col-md-3"><?php echo L::BirthstudentTab?>
                  <span class="required"> * </span>
              </label>
                  <div class="col-md-6">
                      <input type="text" placeholder="Entrer la date de naissance" name="datenaisad" id="datenaisad" data-mask="99/99/9999" class="form-control">
                        <span class="help-block"><?php echo L::Datesymbole ?></span>
                  </div>
              </div>
              <div class="form-group row">
                      <label class="control-label col-md-3">Lieu Naissance
                          <span class="required"> * </span>
                      </label>
                      <div class="col-md-6">
                          <input type="text" name="lieunais" id="lieunais" data-required="1" placeholder="Entrer le Lieu de Naissance" class="form-control" /> </div>
                  </div>

              <div class="form-group row">
                  <label class="control-label col-md-3">Genre
                      <span class="required">*  </span>
                  </label>
                  <div class="col-md-6">
                    <select class="form-control input-height" id="sexe" name="sexe" style="width:100%">
                        <option value="">Selectionner un Genre</option>
                        <option value="M">Masculin</option>
                        <option value="F">Feminin</option>

                    </select>
                    <input type="hidden" name="etabname" id="etabname" value="<?php echo $etabs->getEtabLibellebyCodeEtab($codeEtabLocal); ?>">
                    </div>
              </div>
              <div class="form-group row">
                  <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                      <span class="required">*  </span>
                  </label>
                  <div class="col-md-6">
                    <select class="form-control input-height" id="classeEtab" name="classeEtab" style="width:100%" onchange="cantineoption()">
                        <option value=""><?php echo L::Selectclasses ?></option>
                        <?php
                        $i=1;
                          foreach ($classes as $value):
                          ?>
                          <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                          <?php
                                                           $i++;
                                                           endforeach;
                                                           ?>

                    </select>
                    </div>
              </div>
              <div class="form-group row" id="lastschoolRow">
                      <label class="control-label col-md-3">Dernier établissement fréquenté
                          <span class="required">  </span>
                      </label>
                      <div class="col-md-6">
                            <input type="text" class="form-control" name="lastschool" id="lastschool" value="" placeholder="Entrer le Dernier établissement fréquenté">

                        </div>
                  </div>
              <div class="form-group row">
                  <label class="control-label col-md-3">Mode de paiement (Cantine)
                      <span class="required" id="spancantine"> * </span>
                  </label>
                  <div class="col-md-6">
                    <select class="form-control input-height" id="cantineEtab" name="cantineEtab" style="width:100%">


                    </select>
                    </div>
              </div>
              <div id="dynamic_field">

              </div>
              <div class="form-group row">

                <div class="offset-md-3 col-md-9">
                  <button type="button" class="btn btn-primary" id="parentNew"> <i class="fa fa-plus"></i> <?php echo L::NewParent ?> </button>

                  <button type="button" class="btn btn-primary" id="parentOld" data-toggle="modal" data-target="#exampleModal"> <i class="fa fa-plus"></i> Ancien parent </button>



                </div>

              </div>

              <div class="form-group row">
                  <label class="control-label col-md-3"><?php echo L::Pictures?>

                  </label>
                  <div class="compose-editor">
                    <input type="file" id="photoad" name="photoad" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="../photo/user5.jpg" data-allowed-file-extensions="gif png jpg jpeg pjpeg" />
                  </div>
              </div>






                                       </fieldset>

                                          <fieldset>
                                            <legend style="background-color:#d8dbe0;height:50px;text-align:center"><span style='font-family: "Trebuchet MS", Verdana, sans-serif;font-style: italic;font-variant-ligatures: no-common-ligatures;font-size:medium;'>FORMULAIRE MEDICAL</span></legend>
                                            <br>
                                            <div class="form-group row">
                                                    <label class="control-label col-md-3">Groupe sanguin
                                                        <span class="required">  </span>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="blood" id="blood" data-required="1" placeholder="Entrer le groupe sanguin" class="form-control" />

                                                      </div>
                                                </div>
                                                <div class="form-group row">
                                                        <label class="control-label col-md-3">Clinique / Hopital
                                                            <span class="required">  </span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <input type="text" name="hopital" id="hopital" data-required="1" placeholder="Entrer le nom de l'hopital / clinique" class="form-control" />

                                                          </div>
                                                    </div>
                                                <div class="form-group row">
                                                        <label class="control-label col-md-3">Matricule clinique enfant
                                                            <span class="required">  </span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <input type="text" name="pinchildhopial" id="pinchildhopial" data-required="1" placeholder="Entrer le matricule de l'enfant dans la clinique" class="form-control" />

                                                          </div>
                                                    </div>

                                                    <div class="form-group row">
                                                            <label class="control-label col-md-3">Medecin traitant
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="doctor" id="doctor" data-required="1" placeholder="Entrer le nom du medecin traitant" class="form-control" />

                                                              </div>
                                                        </div>
                                                        <div class="form-group row">
                                                                <label class="control-label col-md-3">Tel Medecin
                                                                    <span class="required">  </span>
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" name="phonedoctor" id="phonedoctor" data-required="1" placeholder="Entrer le téléphone du medecin traitant" class="form-control" />

                                                                  </div>
                                                            </div>
                                                            <div class="form-group row">

                                                              <div class="offset-md-3 col-md-9">
                                                                <button type="button" class="btn btn-primary" id="AllergiesNew" onclick="addAllergies()"> <i class="fa fa-plus"></i> allergie</button>
                                                                <input type="hidden" name="nballergies" id="nballergies" value="0">
                                                                <input type="hidden" name="concatnballergies" id="concatnballergies" value="">
                                                                <input type="hidden" name="nbconcatnballergies" id="nbconcatnballergies" value="0">
                                                              </div>



                                                              </div>
                                                              <div class="form-group row">
                                                                  <label class="control-label col-md-3">
                                                                      <span class="required">  </span>
                                                                  </label>
                                                                  <div class="col-md-6">
                                                                  <table class="table" id="fields_allergies" border=0></table>
                                                                    </div>
                                                              </div>
                                                            <!--div class="form-group row">
                                                                <label class="control-label col-md-3">Allergies
                                                                    <span class="required">  </span>
                                                                </label>
                                                                <div class="col-md-6">
                                                                  <select class="form-control input-height" id="allergiestudents" name="allergiestudents[]" multiple="multiple" style="width:100%">
                                                                    <option value="1">Aspirine / Aspirin</option>
                                                                    <option value="2">Sérum antitétanique / Anti-tetanus Serum</option>
                                                                    <option value="3">Nivaquine / Chloroquine</option>
                                                                    <option value="4">Pénicilline / Penicillin</option>
                                                                    <option value="5">Iode / Iodine</option>
                                                                    <option value="6">Autres</option>

                                                                  </select>
                                                                  </div>
                                                            </div-->
                                                            <div class="form-group row" style="margin-top:-35px;">

                                                              <div class="offset-md-3 col-md-9">
                                                                <button type="button" class="btn btn-primary" id="InfantilesDNew" onclick="addInfantiles()"> <i class="fa fa-plus"></i> Maladies infantiles</button>
                                                                <input type="hidden" name="nbinfantilesD" id="nbinfantilesD" value="0">
                                                                <input type="hidden" name="concatnbinfantilesD" id="concatnbinfantilesD" value="">
                                                                <input type="hidden" name="nbconcatnbinfantilesD" id="nbconcatnbinfantilesD" value="0">
                                                              </div>


                                                              </div>

                                                              <div class="form-group row">
                                                                  <label class="control-label col-md-3">
                                                                      <span class="required">  </span>
                                                                  </label>
                                                                  <div class="col-md-6">
                                                                  <table class="table" id="fields_infantiles" border=0></table>
                                                                    </div>
                                                              </div>
                                                            <!--div class="form-group row">
                                                                <label class="control-label col-md-3">Maladies infantiles
                                                                    <span class="required">  </span>
                                                                </label>
                                                                <div class="col-md-6">
                                                                  <select class="form-control input-height" id="deseasesinfant" name="deseasesinfant[]" multiple="multiple" style="width:100%">
                                                                    <option value="1">Rougeole/Measles</option>
                                                                    <option value="2">Oreillons/ Mumps</option>
                                                                    <option value="3">Varicelle/ Chicken Pox</option>
                                                                    <option value="4">Scarlatine/ Scarlet</option>
                                                                  </select>
                                                                  </div>
                                                            </div-->

                                                            <div class="form-group row" style="margin-top:-35px;">

                                                              <div class="offset-md-3 col-md-9" >
                                                                <button type="button" class="btn btn-primary" id="MedicalNew"> <i class="fa fa-plus"></i> antécédent medical</button>
                                                                <input type="hidden" name="nbantecedent" id="nbantecedent" value="0">
                                                                <input type="hidden" name="concatantecedent" id="concatantecedent" value="">
                                                                <input type="hidden" name="nbconcatantecedent" id="nbconcatantecedent" value="0">
                                                              </div>


                                                              </div>


                                                            <div class="form-group row">

                                                                    <div class="col-md-12">
                                                                      <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                                                        <thead>
                                                                          <th style="width:350px;">ANTECEDENTS MEDICAUX</th>
                                                                          <th style="width:80px;text-align:center">Enfant / Child</th>
                                                                          <th style="width:80px;text-align:center">Père / Father</th>
                                                                          <th style="width:80px;text-align:center">Mère/ Mother</th>
                                                                        </thead>
                                                                        <tbody id="tablebody">
                                                                          <tr id="lignevide">
                                                                            <td colspan="4" style="text-align:center">Aucune information</td>
                                                                          </tr>
                                                                        </tbody>

                                                                      </table>

                                                                      </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                        <label class="control-label col-md-4">Photocopie des pages du carnet de santé (vaccination)
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-6">
                                                                              <input type="file" id="carnet" name="carnet" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="../photo/images_files2.jpg" data-allowed-file-extensions="gif png jpg jpeg pjpeg pdf" />

                                                                          </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                      <!-- <label class="control-label col-md-3">
                                                                      <span class="required"> </span>
                                                                      </label> -->
                                                                      <label class="col-sm-7 col-md-7">Où votre enfant devra être reconduit en cas de maladie ?
                                                                      <span class="required"> </span>
                                                                      </label>
                                                                    </div>

                                                                    <div class="form-group row">

                                                              <!-- <label class="control-label col-md-3">
                                                              <span class="required"> </span>
                                                              </label> -->
                                                              <div class="col-sm-7 col-md-7">
                                                              <div class="input-group">
                                                              <div id="radioBtn3" class="btn-group">
                                                              <a class="btn btn-primary btn-sm active" id="btn31" data-toggle="localisation" data-title="1" onclick="Home()">RECONDUIRE A LA MAISON</a>
                                                              <a class="btn btn-primary btn-sm notActive" id="btn32" data-toggle="localisation" data-title="0" onclick="HomeN()">RECONDUIRE A UNE ADRESSE</a>
                                                              </div>
                                                              <input type="hidden" name="localisation" id="localisation" value="1">
                                                              </div>
                                                              </div>
                                                              </div>

                                                              <div class="form-group row">
                                                                <!-- <label class="control-label col-md-3">
                                                                <span class="required"> </span>
                                                                </label> -->
                                                                <label class="col-sm-7 col-md-7">Adresse géographique détaillée (adresse, quartier, commune)
                                                                <span class="required"> </span>
                                                                </label>
                                                              </div>
                                                              <div class="form-group row">
                                                                  <!-- <label class="control-label col-md-3">Maladies infantiles
                                                                      <span class="required">  </span>
                                                                  </label> -->
                                                                  <div class="offset-md-3 col-md-7">
                                                                    <textarea name="adresgeodetails" id="adresgeodetails" rows="8" cols="80" class="form-control"></textarea>
                                                                    </div>
                                                              </div>
                                                              <div class="form-group row">
                                                                      <label class="control-label col-md-3">Personne à prévenir
                                                                          <span class="required"> * </span>
                                                                      </label>
                                                                      <div class="col-md-6">
                                                                            <input type="text" class="form-control" name="guardian" id="guardian" value="" placeholder="Entrer le nom de la personne à prévenir">

                                                                        </div>
                                                                  </div>
                                                                  <div class="form-group row">
                                                                          <label class="control-label col-md-3">Tel Mobile
                                                                              <span class="required"> * </span>
                                                                          </label>
                                                                          <div class="col-md-6">
                                                                                <input type="text" class="form-control" name="telMobguardian" id="telmobguardian" value="" placeholder="Entrer le télephone mobile">

                                                                            </div>
                                                                      </div>
                                                                  <div class="form-group row">
                                                                          <label class="control-label col-md-3">Tel Bureau
                                                                              <span class="required">  </span>
                                                                          </label>
                                                                          <div class="col-md-6">
                                                                                <input type="text" class="form-control" name="telBguardian" id="telBguardian" value="" placeholder="Entrer le téléphone du bureau">

                                                                            </div>
                                                                      </div>

                                                                          <div class="form-group row">
                                                                                  <label class="control-label col-md-3">Tel Domicile
                                                                                      <span class="required">  </span>
                                                                                  </label>
                                                                                  <div class="col-md-6">
                                                                                        <input type="text" class="form-control" name="domicileguardian" id="domicileguardian" value="" placeholder="Entrer le télephone du domicile">

                                                                                    </div>
                                                                              </div>

                                          </fieldset>


                                          <fieldset>
                                            <legend style="background-color:#d8dbe0;height:50px;text-align:center"><span style='font-family: "Trebuchet MS", Verdana, sans-serif;font-style: italic;font-variant-ligatures: no-common-ligatures;font-size:medium;'>CONTRAT PARENTAL</span></legend>
                                            <br>
                                            <div class="form-group row">
                                              <!-- <label class="control-label col-md-3">
                                              <span class="required"> </span>
                                              </label> -->
                                              <label class="col-sm-7 col-md-7">Contrat Parental signé
                                              <span class="required"> </span>
                                              </label>
                                            </div>
                                            <div class="form-group row">
                                                    <!-- <label class="control-label col-md-4">Photocopie des pages du carnet de santé (vaccination)
                                                        <span class="required"> * </span>
                                                    </label> -->
                                                    <div class="offset-md-3 col-md-6">
                                                          <input type="file" id="contrat" name="contrat" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="../photo/images_files3.jpg" data-allowed-file-extensions="gif png jpg jpeg pjpeg pdf" />

                                                      </div>
                                                </div>
                                          </fieldset>

                                          <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <button type="submit" id="submitBtn" class="btn btn-info">Enregistrer</button>
                                                                        <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                    </div>
                                                                  </div>
                                                               </div>

                                   </form>

                                   <div class="modal fade" id="exampleModal"  tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                     <div class="modal-dialog modal-lg" role="document">

                                       <div class="modal-content">

                                         <div class="modal-header">
                                            <h4 class="modal-title" id="exampleModalLabel">Sélectionner un parent</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">

                                          <div id="divrecherchestation">

                                            <p>Critère de recherche</p>

                                            <form class = "" id = "FormSearch">
                                              <div class="row">
                                                                  <div class="col-md-6">
                                              <div class="form-group">
                                              <label for=""><b>Nom parent<span class="required">  </span> :</b></label>

                                            <input type="text" name="nomserach" id="nomserach" data-required="1" placeholder="<?php echo L::ParentName; ?>" class="form-control" />
                                              </div>

                                              </div>
                                              <div class="col-md-6">
                                                   <div class="form-group">
                                                     <label for=""><b>Telephone  parent <span class="required">  </span>: </b></label>
                                                      <input type="text" name="telserach" id="telserach" data-required="1" placeholder="Télephone parent" class="form-control" />
                                                    </div>


                                               </div>
                                               <div class="col-md-6">
                                                   <button type="submit" class="btn btn-danger" id="searchbtn"><i class="fa fa-search"></i> Rechercher</button>
                                                   <img src="../photo/ajax-loader.gif" id="imgLoader" style="display:none;" />

                                                </div>


                                                            </div>
                                            </form>
                                            <br>
                                            <table class="table table-striped table-bordered table-hover">
									    <thead style="background-color:#28a745; color:white; font-weight: bold;">
                      <tr>
										  <th style="">Nom</th>
										  <th class="visible-lg">Prénoms </th>
										  <th> Télephone mobile </th>
										  <th style="">Email </th>
									  </tr>
									   </thead>
									   <tbody id="tabStationBody">

										<tr>

										  <td colspan="4">Aucune Ligne</td>

										</tr>

                                      </tbody>
                                       </table>

                                          </div>

                                          <div class="row" id="RowInfosOld">
                                            <input type="hidden" id="parentaold" name="parentaold"value="">
                                            <div class="col-md-12">
                                              <div class="form-group" style="display:none">
                                       <label for="benef">Parent<span class="required">*</span> :</label>
                                       <!--select class="form-control input-height"  id="parentaold" name="parentaold" style="width:100%" onchange="selectparentsinfos()">
                                           <option value="">Selectionner un parent</option>
                                           <?php
                                           $i=1;
                                             //foreach ($parents as $value):
                                             ?>
                                             <option value="<?php //echo utf8_encode(utf8_decode($value->id_compte)); ?>"><?php //echo utf8_encode(utf8_decode($value->nom_parent." - ".$value->prenom_parent)); ?></option>

                                             <?php
                                                                              $i++;
                                                                              //endforeach;
                                                                              ?>

                                       </select-->

                                               </div>


                                               </div>

                                               <div class="col-md-12">
                                                <div class="form-group">
                                         <label for="oldparentname">Nom<span class="required">*</span> :</label>
                                  <input type="text" name="oldparentname" class="form-control" id="oldparentname" value="" size="32" maxlength="225" />

                                                   </div>
                                                 </div>
                                                 <div class="col-md-12">
                                                  <div class="form-group">
                                           <label for="oldparentprename">Prénoms<span class="required">*</span> :</label>
                                    <input type="text" name="oldparentprename" class="form-control" id="oldparentprename" value="" size="32" maxlength="225" />

                                                     </div>
                                                   </div>
                                                   <div class="col-md-12">
                                                    <div class="form-group">
                                             <label for="oldparentprename">Genre<span class="required">*</span> :</label>
                                      <input type="text" name="oldparentsexe" class="form-control" id="oldparentsexe" value="" size="32" maxlength="225" />

                                                       </div>
                                                     </div>
                                                   <div class="col-md-12">
                                                    <div class="form-group">
                                             <label for="oldparentphone">Téléphone<span class="required">*</span> :</label>
                                      <input type="text" name="oldparentphone" class="form-control" id="oldparentphone" value="" size="32" maxlength="225" />

                                                       </div>
                                                     </div>
                         <!-- <button type="submit" name="KT_Insert1" id="KT_Insert1" class="btn btn-primary pull-right" style="">Valider <i class="icon-check position-right"></i></button> -->


                                          </div>

                                        </div>
                                        <div class="modal-footer">
                  					                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo L::Closebtn  ?></button>
                  					                <button type="button" class="btn btn-primary" id="addoldbtn" onclick="oldbtnaction()" disabled><i class="fa fa-plus"></i> Ajouter</button>
                  					            </div>

                                       </div>

                                     </div>

                                   </div>
                                 </div>
                             </div>
                         </div>
                    </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <!-- <script src="../assets2/plugins/steps/jquery.steps.js" ></script> -->


  <script>


  // $(document).ready(function() {
    jQuery(document).ready(function() {

    "use strict";
    // var wizard = $("#wizard_test").steps();
    var i=1;
    // determinedestinataires();

    $('#exampleModal').on('shown.bs.modal', function () {
      // console.log("bonsoir");
      //nous allons initialiser le tableau
       $("#tabStationBody").html("");
       $("#nomserach").val("");
       $("#telserach").val("");
       $("#RowInfosOld").hide();

       var content="<tr><td colspan=\"4\">Aucune Ligne</td><tr>";
       $("#tabStationBody").html(content);

    });

$("#submitBtn").attr("disabled",true);
$("#RowInfosOld").hide();



    desactivatebtn();
    getInfantiles();
    getAllergies();


    // var form = $("#FormAddStudent").show();
    var form = $("#FormAddStudent");
    var form1=$("#FormSearch");

    form1.validate({
      errorPlacement: function(label, element) {
      label.addClass('mt-2 text-danger');
      label.insertAfter(element);
    },
    highlight: function(element, errorClass) {
      $(element).parent().addClass('has-danger')
      $(element).addClass('form-control-danger')
    },
    success: function (e) {
          $(e).closest('.control-group').removeClass('error').addClass('info');
          $(e).remove();
      },
  rules:{
    nomserach: {
     'required': {
        depends: function (element) {
            return ($("#telserach").val()=="");

        }
    }
  },
  telserach: {
    digits:true,
   'required': {
      depends: function (element) {
          return ($("#nomserach").val()=="");

      }
  }
}

  },
  messages: {
nomserach:"Merci de renseigner au moins le nom du parent",
telserach:{
  required:"Merci de renseigner au moins le numéro de telephone",
  digits:"<?php echo L::DigitsOnly?>"
}


},
submitHandler: function(form) {

  //nous allons rechercher le parent
   $("#submitBtn").attr("disabled",true);
  var nomserach=$("#nomserach").val();
  var telserach=$("#telserach").val();
  var codeEtab="<?php echo $codeEtabLocal; ?>";
  var concatoldparents=$("#concatoldparents").val();
  var etape=6;
     $.ajax({
       url: '../ajax/parent.php',
       type: 'POST',
       async:false,
       data: 'nom=' + nomserach+ '&etape=' + etape+'&telephone='+telserach+'&codeEtab='+codeEtab+'&concatoldparents='+concatoldparents,
       dataType: 'text',
       beforeSend: function (xhr) {

            // $("#imgLoader").css("display", "inline");
            $("#imgLoader").show();
        //     setTimeout(function () {
        //     $("#imgLoader").show();
        // }, 100);

          },
       success: function (content, statut) {
       // $("#nbtotalparent").val(0);
       $("#RowInfosOld").hide();
       $("#imgLoader").hide(2000);
       $("#tabStationBody").html("");
       $("#tabStationBody").html(content);

       }
     });

}

    });

    form.validate({
      errorPlacement: function(label, element) {
      label.addClass('mt-2 text-danger');
      label.insertAfter(element);
    },
    highlight: function(element, errorClass) {
      $(element).parent().addClass('has-danger')
      $(element).addClass('form-control-danger')
    },
    success: function (e) {
          $(e).closest('.control-group').removeClass('error').addClass('info');
          $(e).remove();
      },

      rules:{
        // lastschool:"required",
        guardian:"required",
        // telBguardian:"required",
        telMobguardian:"required",
        // domicileguardian:"required",
        adresgeodetails:"required",
        blood:"required",
        cantineEtab:"required",
        matri:"required",
        matriselect:"required",
        matri: {
				 'required': {
 						depends: function (element) {
 								return ($("#inscript").val()==1);

 						}
 				}
			},
        matriselect: {
				 'required': {
 						depends: function (element) {
 								return ($("#inscript").val()==2);

 						}
 				}
			},
        nomad:"required",
        prenomad:"required",
        datenaisad:"required",
        lieunais:"required",
        sexe:"required",
        classeEtab:"required"






      },
      messages: {
        // lastschool:"Merci de renseigner la derni",
        guardian:"Merci de renseigner le Nom de la personne à prévenir",
        telBguardian:"Merci de renseigner le Tel du bureau",
        telMobguardian:"Merci de renseigner le Tel Mobile",
        domicileguardian:"Merci de renseigner le Tel du domicile",
        adresgeodetails:"Merci de renseigner l'adresse géographique détaillée",
        blood:"Merci de renseigner le groupe sanguin",
        cantineEtab:"Merci de selectionner un mode de paiement cantine",
        matri:"Merci de renseigner le matricule de l'élève",
        matriselect:"Merci de selectionner le matricule de l'élève",
        nomad:"Merci de renseigner le nom de l'élève",
        prenomad:"Merci de renseigner le prénom de l'élève",
        datenaisad:"Merci de renseigner la date de naissance de l'élève",
        lieunais:"Merci de renseigner le lieu de naissance de l'élève",
        sexe:"Merci de selectionner le genre de l'élève",
        classeEtab:"Merci de selectionner au moins une classe",
        matiere:"Merci de renseigner la matière"



      }
    });
// form.steps({
//   headerTag: "h3",
//   bodyTag: "fieldset",
//   transitionEffect: "slideLeft",
//   onStepChanging: function (event, currentIndex, newIndex)
//   {
//       // Allways allow previous action even if the current form is not valid!
//       if (currentIndex > newIndex)
//       {
//           return true;
//       }
//
//       if ($("#nbtotalparent").val()==0)
//       {
//         // Swal.fire({
//         //         type: 'warning',
//         //         title: '<?php echo L::WarningLib ?>',
//         //         text: "Vous devez ajouter au moins un parent à cet élève",
//         //
//         //         })
//         //   return false;
//       }
//
//       // Forbid next action on "Warning" step if the user is to young
//       // if (newIndex === 3 && Number($("#age-2").val()) < 18)
//       // {
//       //     return false;
//       // }
//       // Needed in some cases if the user went back (clean up)
//       if (currentIndex < newIndex)
//       {
//           // To remove error styles
//           form.find(".body:eq(" + newIndex + ") label.error").remove();
//           form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
//       }
//       form.validate().settings.ignore = ":disabled,:hidden";
//       return form.valid();
//   },
//   onStepChanged: function (event, currentIndex, priorIndex)
//   {
//       // Used to skip the "Warning" step if the user is old enough.
//       //nbparent
//
//       if (currentIndex === 1 && $("#nbparent").val()==0)
//       {
//         Swal.fire({
//                 type: 'warning',
//                 title: '<?php echo L::WarningLib ?>',
//                 text: "Vous devez ajouter au moins un parent à cet élève",
//
//                 })
//           return false;
//       }
//
//       // if (currentIndex === 2 && Number($("#age-2").val()) >= 18)
//       // {
//       //     form.steps("next");
//       // }
//       // // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
//       // if (currentIndex === 2 && priorIndex === 3)
//       // {
//       //     form.steps("previous");
//       // }
//   },
//   onFinishing: function (event, currentIndex)
//   {
//       form.validate().settings.ignore = ":disabled";
//       return form.valid();
//   },
//   onFinished: function (event, currentIndex)
//   {
//       //alert("Submitted!");
//       form.submit();
//   }
// });


    //  form.steps({
    //      headerTag: "h3",
    //      bodyTag: "fieldset",
    //      transitionEffect: "slideLeft",
    //      onStepChanging: function (event, currentIndex, newIndex)
    //       {
    //                   // Allways allow previous action even if the current form is not valid!
    //                   // var classeselected=$("#classeEtab").val();
    //                   // // alert(classeselected);
    //                   //
    //                   // if(classeselected=="")
    //                   // {
    //                   //   $("#classeEtab").rules( "add", {
    //                   //       required: true,
    //                   //       messages: {
    //                   //       required: "Merci de selectionner au moins une classe"
    //                   // }
    //                   //     });
    //                   //   // return false;
    //                   // }
    //            if (currentIndex > newIndex)
    //            {
    //                return true;
    //            }
    //
    //            // Forbid next action on "Warning" step if the user is to young
    //            // if (newIndex === 2 && Number($("#nbselect").val()) < 0)
    //            // {
    //            //     return false;
    //            // }
    //            // Needed in some cases if the user went back (clean up)
    //            if (currentIndex < newIndex)
    //            {
    //                // To remove error styles
    //                form.find(".body:eq(" + newIndex + ") label.error").remove();
    //                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
    //            }
    //            form.validate().settings.ignore = ":disabled,:hidden";
    //            return form.valid();
    //
    //
    //       },
    // onStepChanged: function (event, currentIndex, priorIndex)
    // {
    //     // Used to skip the "Warning" step if the user is old enough.
    //     // if (currentIndex === 2 && Number($("#nbselect").val()) >0)
    //     // {
    //     //     form.steps("next");
    //     // }
    //
    // },
    // onFinishing: function (event, currentIndex)
    // {
    //     form.validate().settings.ignore = ":disabled";
    //     return form.valid();
    // },
    // onFinished: function (event, currentIndex)
    // {
    //   form.submit();
    //
    // }
    //
    //
    //  });



   //  form.steps({
   //      headerTag: "h3",
   //      bodyTag: "fieldset",
   //      transitionEffect: "slideLeft",
   //      onStepChanging: function (event, currentIndex, newIndex)
   //       {
   //                   // Allways allow previous action even if the current form is not valid!
   //                   // var classeselected=$("#classeEtab").val();
   //                   // // alert(classeselected);
   //                   //
   //                   // if(classeselected=="")
   //                   // {
   //                   //   $("#classeEtab").rules( "add", {
   //                   //       required: true,
   //                   //       messages: {
   //                   //       required: "Merci de selectionner au moins une classe"
   //                   // }
   //                   //     });
   //                   //   // return false;
   //                   // }
   //            if (currentIndex > newIndex)
   //            {
   //                return true;
   //            }
   //
   //            // Forbid next action on "Warning" step if the user is to young
   //            // if (newIndex === 2 && Number($("#nbselect").val()) < 0)
   //            // {
   //            //     return false;
   //            // }
   //            // Needed in some cases if the user went back (clean up)
   //            if (currentIndex < newIndex)
   //            {
   //                // To remove error styles
   //                form.find(".body:eq(" + newIndex + ") label.error").remove();
   //                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
   //            }
   //            form.validate().settings.ignore = ":disabled,:hidden";
   //            return form.valid();
   //
   //
   //       },
   // onStepChanged: function (event, currentIndex, priorIndex)
   // {
   //     // Used to skip the "Warning" step if the user is old enough.
   //     // if (currentIndex === 2 && Number($("#nbselect").val()) >0)
   //     // {
   //     //     form.steps("next");
   //     // }
   //
   // },
   // onFinishing: function (event, currentIndex)
   // {
   //     form.validate().settings.ignore = ":disabled";
   //     return form.valid();
   // },
   // onFinished: function (event, currentIndex)
   // {
   //   form.submit();
   //
   // }
   //
   //
   //  });

    $("#parenta").select2();
    // $("#parentaold").select2();
    $("#sexe").select2();
    $("#classeEtab").select2();
    $("#cantineEtab").select2();
    $("#matriselect").select2();


    $("#deseasesinfant").select2({
      tags: true,
    tokenSeparators: [',', ' ']
    });
    $("#allergiestudents").select2({
      tags: true,
    tokenSeparators: [',', ' ']
    });

    $('#parentNew').click(function(){
          AddparentsRow();

        });

        $('#MedicalNew').click(function(){
              AddMedicalsRow();

            });

$("#matriculerowReinscript").hide();
$("#RowbtnDoublant").hide();


  });

  function afficherInfos(id)
  {
    $("#RowInfosOld").hide();

    selectparentsinfosOne(id);
  }



  function recalculallergiesnb()
  {
    var concatnballergies=$("#concatnballergies").val();

    var tab=concatnballergies.split("@");

    var nbtab=tab.length;

    var nbtabnew=parseInt(nbtab)-1;

    $("#nbconcatnballergie").val(nbtabnew);
  }

  function recalculinfantilesnb()
  {
    var concatnbinfantilesD=$("#concatnbinfantilesD").val();

    var tab=concatnbinfantilesD.split("@");

    var nbtab=tab.length;

    var nbtabnew=parseInt(nbtab)-1;

    $("#nbconcatnbinfantilesD").val(nbtabnew);
  }

  function addAllergies()
  {
    var nb=$("#nballergies").val();
    var nouveau= parseInt(nb)+1;
    $("#nballergies").val(nouveau);
    var concatallergies=$("#concatnballergies").val();
    $("#concatnballergies").val(concatallergies+nouveau+"@");

    recalculallergiesnb();

    $('#fields_allergies').append('<tr id="rowAllerg'+nouveau+'"><td><input type="text" name="Allergies_'+nouveau+'" id="Allergies_'+nouveau+'" placeholder="Entrer une allergie" class="form-control" /></td><td><button type="button" id="deleteAllerg'+nouveau+'" id="deleteAllerg'+nouveau+'"  onclick="deletedAllergies('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');

    for(var i=1;i<=nouveau;i++)
    {
      $("#Allergies_"+i).rules( "add", {
          required: true,
          messages: {
          required: "Merci de renseigner l'allergie de l'enfant"
}
        });
      }
  }

  function deletedAllergies(id)
  {
    var concatnballergies=$("#concatnballergies").val();

    $("#concatnballergies").val($("#concatnballergies").val().replace(id+"@", ""));

     $('#rowAllerg'+id+'').remove();

    recalculallergiesnb();
  }

  function deleteInfantiles(id)
  {
    var concatnbinfantilesD=$("#concatnbinfantilesD").val();

    $("#concatnbinfantilesD").val($("#concatnbinfantilesD").val().replace(id+"@", ""));

     $('#rowInfantiles'+id+'').remove();

    recalculinfantilesnb();
  }

  function addInfantiles()
  {
    var nb=$("#nbinfantilesD").val();
    var nouveau= parseInt(nb)+1;
    $("#nbinfantilesD").val(nouveau);
    var concatnbinfantilesD=$("#concatnbinfantilesD").val();
    $("#concatnbinfantilesD").val(concatnbinfantilesD+nouveau+"@");
    recalculinfantilesnb();

    $('#fields_infantiles').append('<tr id="rowInfantiles'+nouveau+'"><td><input type="text" name="Infantiles_'+nouveau+'" id="Infantiles_'+nouveau+'" placeholder="Entrer une maladie infantile" class="form-control" /></td><td><button type="button" id="deleteInfantiles'+nouveau+'" id="deleteInfantiles'+nouveau+'"  onclick="deleteInfantiles('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');

    for(var i=1;i<=nouveau;i++)
    {
      $("#Infantiles_"+i).rules( "add", {
          required: true,
          messages: {
          required: "Merci de renseigner la maladie infantiles"
}
        });
    }
  }

  function doublantN()
  {
    $("#doublant").val(0);

    $("#btn1").removeClass('notActive').addClass('active');
    $("#btn2").removeClass('active').addClass('notActive');
  }

  function doublant()
  {
  $("#doublant").val(1);
  $("#btn2").removeClass('notActive').addClass('active');
  $("#btn1").removeClass('active').addClass('notActive');
  }

  function checkmatri()
  {
    var matri=$("#matri").val();
    var codeEtab="<?php echo $codeEtabLocal ?>";
    var session="<?php echo $libellesessionencours ?>";
    var etape=11;
    $.ajax({
      url: '../ajax/admission.php',
      type: 'POST',
      async:true,
      data: 'matricule=' +matri+ '&etape=' + etape+'&session='+session+'&codeEtab='+codeEtab,
      dataType: 'text',
      success: function (content, statut) {

        // alert(content);

        if(content==1)
        {
          //nous allons rechercher les informations de l'élève

          $("#newStudent").val(2);

          var etape=12;
            $.ajax({
              url: '../ajax/admission.php',
              type: 'POST',
              async:true,
              data: 'matricule=' +matri+ '&etape=' + etape+'&session='+session+'&codeEtab='+codeEtab,
              dataType: 'text',
              success: function (content, statut) {

                Swal.fire({
  title: '<?php echo L::WarningLib ?>',
  text: content,
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Importer les informations',
  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
  if (result.value) {
// alert("oui");

var etape=13;
$.ajax({
  url: '../ajax/admission.php',
  type: 'POST',
  async:true,
  data: 'matricule=' +matri+ '&etape=' + etape+'&session='+session+'&codeEtab='+codeEtab,
  dataType: 'text',
  success: function (response, statut) {

    var nom=response.split("*")[0];
    var prenom=response.split("*")[1];
    var datenais=response.split("*")[2];
    var lieunais=response.split("*")[3];
    var sexeselect=response.split("*")[4];


    // alert(nom);

    $("#nomad").val(nom);
    $("#prenomad").val(prenom);
    $("#datenaisad").val(datenais);
    $("#lieunais").val(lieunais);
    $('#sexe option[value="'+sexeselect+'"]').attr('selected',true);

    //nous allons chercher les informations sur les parents de cet eleves

    var etape=14;

    $.ajax({
      url: '../ajax/admission.php',
      type: 'POST',
      async:true,
      data: 'matricule=' +matri+ '&etape=' + etape+'&session='+session+'&codeEtab='+codeEtab,
      dataType: 'text',
      success: function (response, statut) {



        var tab=response.split('*');

        // alert(tab.length);

        for(var i=0;i<tab.length;i++)
        {
          // alert(tab[i]);
          var parentstudentid=tab[i];

          var etape=15;

          $.ajax({
            url: '../ajax/admission.php',
            type: 'POST',
            async:true,
            data: 'parentstudentid='+parentstudentid+ '&etape=' + etape,
            dataType: 'text',
            success: function (response, statut) {



              var parentid=response.split("*")[11];
              var nom=response.split("*")[0];
              var prenom=response.split("*")[1];
              var phone=response.split("*")[2];
              var sexe=response.split("*")[6];

              Onloadoldparent(parentid,nom,prenom,phone,sexe);

            }
          });

        }

      }
    });



  }
});


  }else {
    //document.location.href="index.php";
    $("#matri").val("");
    $("#matri").focus();
  }
})

              }
            });
        }else {
          $("#newStudent").val(1);
        }

      }
    });
    // alert(matri);
    //nous allons verifier si cet matricule existe deja dans le système pour cet établissement
  }

  function Onloadoldparent(parentid,nom,prenom,phone,sexe)
  {
    var concatoldparents=$("#concatoldparents").val();

    var nb=$("#nboldparent").val();
    var nouveau= parseInt(nb)+1;
    // var concatparents=$("#concatparents").val();
    // $("#concatparents").val(concatparents+nouveau+"@");
    $("#concatoldparents").val(concatoldparents+parentid+"@");

    //recalcule nbconcatparent
    recalculoldparentnb();

    var ligne="<div class=\"form-group row\" id=\"ligneEnteteold"+parentid+"\">";
    ligne=ligne+"<label class=\"control-label col-md-8\" style=\"color:#e40d22\">INFORMATIONS ANCIEN PARENT "+nouveau+" <span class=\"required\">  </span></label>";
    ligne=ligne+"</div>";
    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowNameold"+parentid+"\">";
    ligne=ligne+"<label class=\"control-label col-md-3\">Nom <span class=\"required\"> * </span></label>";
    ligne=ligne+"<div class=\"col-md-6\">";
    ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"nomparentold"+parentid+"\"  id=\"nomparentold"+parentid+"\" value="+nom+" readonly >";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";
    //prenoms
    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPrenameold"+parentid+"\">";
    ligne=ligne+"<label class=\"control-label col-md-3\">Prénoms <span class=\"required\"> * </span></label>";
    ligne=ligne+"<div class=\"col-md-6\">";
    ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"prenomparentold"+parentid+"\"  id=\"prenomparentold"+parentid+"\" value="+prenom+" readonly>";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";
    //sexe
    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowSexeold"+parentid+"\">";
    ligne=ligne+"<label class=\"control-label col-md-3\">Genre <span class=\"required\"> * </span></label>";
    ligne=ligne+"<div class=\"col-md-6\">";
    ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"sexeparentold"+parentid+"\"  id=\"sexeparentold"+parentid+"\" value="+sexe+" readonly>";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";

    //telephone
    ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPhoneold"+parentid+"\">";
    ligne=ligne+"<label class=\"control-label col-md-3\">Téléphone <span class=\"required\"> * </span></label>";
    ligne=ligne+"<div class=\"col-md-6\">";
    ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"phoneparentold"+parentid+"\"  id=\"phoneparentold"+parentid+"\" value="+phone+" readonly>";
    ligne=ligne+"</div>";
    ligne=ligne+"</div>";
    //bouton de suppression
    ligne=ligne+"<div class=\"pull-right\" id=\"ligneRowold"+parentid+"\">";
    ligne=ligne+"<button type=\"button\" id=\"delete"+parentid+"\" name=\"delete"+parentid+"\"  onclick=\"deletedoldrow("+parentid+")\" class=\"btn btn-danger pull-right\"><i class=\"fa fa-trash\"></i></button>";
    ligne=ligne+"</div></br></br>";

    $('#dynamic_field').append(ligne);
  }

  function Home()
  {
    $("#localisation").val(1);
    $("#btn31").removeClass('notActive').addClass('active');
    $("#btn32").removeClass('active').addClass('notActive');
  }

  function HomeN()
  {
    $("#localisation").val(0);
    $("#btn32").removeClass('notActive').addClass('active');
    $("#btn31").removeClass('active').addClass('notActive');
  }

   function manuel()
   {
     $("#matriculestate").val(1);
     $("#matri").val("");
     $("#matri").attr("disabled",false);
     $("#etape").val(1);
     $("#matriculerow").show();

     $("#btn11").removeClass('notActive').addClass('active');
     $("#btn12").removeClass('active').addClass('notActive');




   }

   function inscription()
   {
     //val ==1
     $("#inscript").val(1);
     $("#newStudent").val(1);
     $("#RowbtnDoublant").hide();
     $("#matriculerowReinscript").hide();

      $("#RowbtnMatricule").show();
      $("#matriculerowInscript").show();
      $("#lastschoolRow").show();

      $("#btn21").removeClass('notActive').addClass('active');
      $("#btn22").removeClass('active').addClass('notActive');

   }

   function reinscription()
   {
     //val ==2
     $("#inscript").val(2);
     $("#newStudent").val(2);
     $("#RowbtnDoublant").show();
     $("#matriculerowReinscript").show();

     $("#RowbtnMatricule").hide();
     $("#matriculerowInscript").hide();
     $("#lastschoolRow").hide();

     $("#btn22").removeClass('notActive').addClass('active');
     $("#btn21").removeClass('active').addClass('notActive');
   }

   function automatique()
   {

     $("#matriculestate").val(2);
     $("#matri").val("");
     $("#matri").attr("disabled",true);
     $("#etape").val(2);
     $("#matriculerow").hide();
     $("#btn12").removeClass('notActive').addClass('active');
     $("#btn11").removeClass('active').addClass('notActive');
   }

   $('#radioBtn a').on('click', function(){
      var sel = $(this).data('title');
      var tog = $(this).data('toggle');
      $('#'+tog).prop('value', sel);

      $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
      $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
  })

  $('#radioBtn1 a').on('click', function(){
     var sel = $(this).data('title');
     var tog = $(this).data('toggle');
     $('#'+tog).prop('value', sel);

     $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
     $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
  })

  $('#radioBtn2 a').on('click', function(){
     var sel = $(this).data('title');
     var tog = $(this).data('toggle');
     $('#'+tog).prop('value', sel);

     $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
     $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
  })

  $('#radioBtn3 a').on('click', function(){
     var sel = $(this).data('title');
     var tog = $(this).data('toggle');
     $('#'+tog).prop('value', sel);

     $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
     $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
  })




      $('#exampleModal').on('hidden.bs.modal', function (e) {
          // window.location.reload(true);
          $("#parentaold").val("");
          $("#oldparentname").val("");
          $("#oldparentprename").val("");
          $("#oldparentphone").val("");

           // $('#parentaold option[value=" "]').prop('selected', true);
           $('#parentaold>option[value=""]').attr('selected', true);

          $("#addoldbtn").prop("disabled",true);
      });

      function oldbtnaction()
      {
        var parentid= $("#parentaold").val();
        var nom=$("#oldparentname").val();
        var prenom=$("#oldparentprename").val();
        var phone=$("#oldparentphone").val();
        var sexe=$("#oldparentsexe").val();

        var concatoldparents=$("#concatoldparents").val();

        var nb=$("#nboldparent").val();
        var nouveau= parseInt(nb)+1;
        // var concatparents=$("#concatparents").val();
        // $("#concatparents").val(concatparents+nouveau+"@");
        $("#concatoldparents").val(concatoldparents+parentid+"@");

        //recalcule nbconcatparent
        recalculoldparentnb();

        var ligne="<div class=\"form-group row\" id=\"ligneEnteteold"+parentid+"\">";
        ligne=ligne+"<label class=\"control-label col-md-8\" style=\"color:#e40d22\">INFORMATIONS ANCIEN PARENT "+nouveau+" <span class=\"required\">  </span></label>";
        ligne=ligne+"</div>";
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowNameold"+parentid+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\">Nom <span class=\"required\"> * </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"nomparentold"+parentid+"\"  id=\"nomparentold"+parentid+"\" value="+nom+" readonly >";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //prenoms
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPrenameold"+parentid+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\">Prénoms <span class=\"required\"> * </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"prenomparentold"+parentid+"\"  id=\"prenomparentold"+parentid+"\" value="+prenom+" readonly>";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //sexe
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowSexeold"+parentid+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\">Genre <span class=\"required\"> * </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"sexeparentold"+parentid+"\"  id=\"sexeparentold"+parentid+"\" value="+sexe+" readonly>";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";



        //telephone
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPhoneold"+parentid+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\">Téléphone <span class=\"required\"> * </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"phoneparentold"+parentid+"\"  id=\"phoneparentold"+parentid+"\" value="+phone+" readonly>";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //bouton de suppression
        ligne=ligne+"<div class=\"pull-right\" id=\"ligneRowold"+parentid+"\">";
        ligne=ligne+"<button type=\"button\" id=\"delete"+parentid+"\" name=\"delete"+parentid+"\"  onclick=\"deletedoldrow("+parentid+")\" class=\"btn btn-danger pull-right\"><i class=\"fa fa-trash\"></i></button>";
        ligne=ligne+"</div></br></br>";

        $('#dynamic_field').append(ligne);

        // $('#exampleModal').hide();
        $('#exampleModal').modal('hide');

      }

    function cantineoption()
    {
      var etape=1;
      var classe=$("#classeEtab").val();
      var session="<?php echo $libellesessionencours; ?>";
      var codeEtab="<?php echo $codeEtabLocal; ?>";



       $.ajax({
         url: '../ajax/cantine.php',
         type: 'POST',
         async:false,
         data: 'classeEtab=' +classe+ '&etape=' + etape+'&codeEtab='+codeEtab+'&session='+session,
         dataType: 'text',
         success: function (response, statut) {
           //nous allons voir si les frais sont obligatoires ou pas

           var etape=2;

          if(response==1)
          {
            //cantine obligaoires

            $.ajax({
              url: '../ajax/cantine.php',
              type: 'POST',
              async:false,
              data: 'classeEtab=' +classe+ '&etape=' + etape+'&codeEtab='+codeEtab+'&session='+session,
              dataType: 'text',
              success: function (response, statut) {
                $("#cantineEtab").html();
                $("#cantineEtab").html(response);


                $('#cantineEtab').rules( "add", {
                    required: true,
                    messages: {
                    required: "Merci de selectionner un frais de cantine"
             }
                  });

                $("#spancantine").addClass("required" );

                  //nous allons chercher le type de la classe
                  var etape=3;
                    $.ajax({
                      url: '../ajax/cantine.php',
                      type: 'POST',
                      async:false,
                      data: 'classeEtab=' +classe+ '&etape=' + etape+'&codeEtab='+codeEtab+'&session='+session,
                      dataType: 'text',
                      success: function (response, statut) {

                        if(response=="PREK"||response=="K1"||response=="K2"||response=="K3"||response=="GRADE1")
                        {
                          $("#lastschoolRow").hide();
                        }else {
                          $("#lastschoolRow").show();
                          //nous allons voir si nous sommes dans le cas d'un inscription ou réinscription

                          var inscript=$("#inscript").val();

                          // alert(inscript);

                          if(inscript==2)
                          {
                            var etabname=$("#etabname").val();
                            $("#lastschool").val(etabname);


                          }

                        }




                      }
                    });

              }
            });

          }else {
            //cantine non obligatoires

            $.ajax({
              url: '../ajax/cantine.php',
              type: 'POST',
              async:false,
              data: 'classeEtab=' +classe+ '&etape=' + etape+'&codeEtab='+codeEtab+'&session='+session,
              dataType: 'text',
              success: function (response, statut) {

                $("#cantineEtab").html();
                $("#cantineEtab").html(response);

                $('#cantineEtab').rules('remove');

                $("#spancantine").removeClass("required" );

              }
            });

          }

         }

     });

    }

    function selectparentsinfosOne(id)
    {



      // $("#parentaold").val(id);

      var etape=4;

      $.ajax({
        url: '../ajax/login.php',
        type: 'POST',
        async:false,
        data: 'parentid=' +id+ '&etape=' + etape,
        dataType: 'text',
        success: function (response, statut) {

          var nomparent=response.split("*")[0];
          var prenomparent=response.split("*")[1];
          var phoneparent=response.split("*")[2];
          var sexeparent=response.split("*")[6];
          var parentid=response.split("*")[11];

          // alert(parentid);

          $("#oldparentname").val(nomparent);
          $("#oldparentprename").val(prenomparent);
          $("#oldparentphone").val(phoneparent);
          $("#oldparentsexe").val(sexeparent);
          $("#parentaold").val(parentid);

          $("#RowInfosOld").show(1000);

          $("#addoldbtn").prop("disabled",false);


        }
      });
    }

      function selectparentsinfos()
      {
        var etape=4;
        $.ajax({
          url: '../ajax/login.php',
          type: 'POST',
          async:false,
          data: 'parentid=' + $("#parentaold").val()+ '&etape=' + etape,
          dataType: 'text',
          success: function (response, statut) {

            var nomparent=response.split("*")[0];
            var prenomparent=response.split("*")[1];
            var phoneparent=response.split("*")[2];
            var sexeparent=response.split("*")[6];

            $("#oldparentname").val(nomparent);
            $("#oldparentprename").val(prenomparent);
            $("#oldparentphone").val(phoneparent);
            $("#oldparentsexe").val(sexeparent);

            $("#addoldbtn").prop("disabled",false);


          }
        });
      }
     function activatebtn()
     {
       $("#submit1").prop("disabled",false);

     }

     function desactivatebtn()
     {
       $("#submit1").prop("disabled",true);
     }

     function deletedrow(id)
     {
       var concatparents=$("#concatparents").val();
       $("#concatparents").val($("#concatparents").val().replace(id+"@", ""));

       $("#ligneEntete"+id).remove();
       $("#ligneRowName"+id).remove();
       $("#ligneRowPrename"+id).remove();
       $("#ligneRowPhone"+id).remove();
       $("#ligneRowSexe"+id).remove();
       $("#ligneRow"+id).remove();

       recalculparentnb();

     }

     function deletedoldrow(parentid)
     {
       var concatparents=$("#concatparents").val();
       var concatoldparents=$("#concatoldparents").val();
       // $("#concatparents").val($("#concatparents").val().replace(id+"@", ""));
       $("#concatoldparents").val($("#concatoldparents").val().replace(parentid+"@", ""));

       $("#ligneEnteteold"+parentid).remove();
       $("#ligneRowNameold"+parentid).remove();
       $("#ligneRowPrenameold"+parentid).remove();
       $("#ligneRowPhoneold"+parentid).remove();
       $("#ligneRowSexeold"+parentid).remove();
       $("#ligneRowold"+parentid).remove();

       recalculparentnb();
     }

      function recalculparentnb()
      {
        //calcul du nombre de nouveau parent
        var concatparents=$("#concatparents").val();
        var tab=concatparents.split("@");
        var nbtab=tab.length;
        var nbtabnew=parseInt(nbtab)-1;
        $("#nbparent").val(nbtabnew);
        //calcul du nombre ancien parent
        var concatoldparents=$("#concatoldparents").val();
        var tabold=concatoldparents.split("@");
        var nbtabold=tabold.length;
        var nbtaboldnew=parseInt(nbtabold)-1;
        $("#nboldparent").val(nbtaboldnew);

        var etape=16;

        $.ajax({
          url: '../ajax/admission.php',
          type: 'POST',
          async:true,
          data: 'concatnew=' +nbtabnew+ '&etape=' + etape+'&concatold='+nbtaboldnew,
          dataType: 'text',
          success: function (content, statut) {

            $("#nbtotalparent").val(content);

            if(content==0)
            {
              $("#submitBtn").attr("disabled",true);
            }else if(content>0){
              $("#submitBtn").attr("disabled",false);
            }

          }
        });

        // if((nbtabnew==0)&&(nbtaboldnew==0))
        // {
        //   //actualiser la page
        //
        //    // location.reload();
        //     // desactivatebtn();
        //
        //     $("#nbtotalparent").val(0);
        // }else {
        //   var total=(parseInt(nbtabold)-1)+parseInt(nbtab)-1;
        //   $("#nbtotalparent").val(total);
        // }
      }

      function recalculoldparentnb()
      {
        //calcul du nombre de nouveau parent
        var concatparents=$("#concatparents").val();
        var tab=concatparents.split("@");
        var nbtab=tab.length;
        var nbtabnew=parseInt(nbtab)-1;
        $("#nbparent").val(nbtabnew);
        //calcul du nombre ancien parent
        var concatoldparents=$("#concatoldparents").val();
        var tabold=concatoldparents.split("@");
        var nbtabold=tabold.length;
        var nbtaboldnew=parseInt(nbtabold)-1;
        $("#nboldparent").val(nbtaboldnew);

        var etape=16;

        $.ajax({
          url: '../ajax/admission.php',
          type: 'POST',
          async:true,
          data: 'concatnew=' +nbtabnew+ '&etape=' + etape+'&concatold='+nbtaboldnew,
          dataType: 'text',
          success: function (content, statut) {

            $("#nbtotalparent").val(content);

            if(content==0)
            {
              $("#submitBtn").attr("disabled",true);
            }else if(content>0){
              $("#submitBtn").attr("disabled",false);
            }

          }
        });

        // if((nbtabnew==0)&&(nbtaboldnew==0))
        // {
        //   //actualiser la page
        //
        //    // location.reload();
        //    desactivatebtn();
        // }else if((nbtabnew>0)||(nbtaboldnew>0)){
        //   activatebtn();
        // }else if((nbtabnew<0)||(nbtaboldnew<0))
        // {
        //   desactivatebtn();
        // }
      }

      function telchecked(id)
      {
        // alert(id);
        // var telephone=$("#phoneparent"+id).val();
        var telephone=$("#mobileparent"+id).val();

        var etape=2;
        $.ajax({
          url: '../ajax/login.php',
          type: 'POST',
          async:true,
           data: 'telephone='+telephone+'&etape='+etape,
          dataType: 'text',
          success: function (response, statut) {

            if(response==0)
            {

            }else if(response>0)
            {
              Swal.fire({
                      type: 'warning',
                      title: '<?php echo L::WarningLib ?>',
                      text: "Nous avons dejà un parent avec ce numéro de téléphone dans le système",

                      })

                     deletedrow(id);

                     var etape=5;
                     $.ajax({
                       url: '../ajax/login.php',
                       type: 'POST',
                       async:true,
                        data: 'telephone='+telephone+'&etape='+etape,
                       dataType: 'text',
                       success: function (response, statut) {
                         var parentid=response.split("*")[11];
                         var nom=response.split("*")[0];
                         var prenom=response.split("*")[1];
                         var phone=response.split("*")[2];
                         var sexe=response.split("*")[6];



                         var concatoldparents=$("#concatoldparents").val();

                         var nb=$("#nboldparent").val();
                         var nouveau= parseInt(nb)+1;
                         // var concatparents=$("#concatparents").val();
                         // $("#concatparents").val(concatparents+nouveau+"@");
                         $("#concatoldparents").val(concatoldparents+parentid+"@");

                         //recalcule nbconcatparent
                         recalculoldparentnb();

                         var ligne="<div class=\"form-group row\" id=\"ligneEnteteold"+parentid+"\">";
                         ligne=ligne+"<label class=\"control-label col-md-8\" style=\"color:#e40d22\">INFORMATIONS ANCIEN PARENT "+nouveau+" <span class=\"required\">  </span></label>";
                         ligne=ligne+"</div>";
                         ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowNameold"+parentid+"\">";
                         ligne=ligne+"<label class=\"control-label col-md-3\">Nom <span class=\"required\"> * </span></label>";
                         ligne=ligne+"<div class=\"col-md-6\">";
                         ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"nomparentold"+parentid+"\"  id=\"nomparentold"+parentid+"\" value="+nom+" readonly >";
                         ligne=ligne+"</div>";
                         ligne=ligne+"</div>";
                         //prenoms
                         ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPrenameold"+parentid+"\">";
                         ligne=ligne+"<label class=\"control-label col-md-3\">Prénoms <span class=\"required\"> * </span></label>";
                         ligne=ligne+"<div class=\"col-md-6\">";
                         ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"prenomparentold"+parentid+"\"  id=\"prenomparentold"+parentid+"\" value="+prenom+" readonly>";
                         ligne=ligne+"</div>";
                         ligne=ligne+"</div>";
                         //sexe
                         ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowSexeold"+parentid+"\">";
                         ligne=ligne+"<label class=\"control-label col-md-3\">Genre <span class=\"required\"> * </span></label>";
                         ligne=ligne+"<div class=\"col-md-6\">";
                         ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"sexeparentold"+parentid+"\"  id=\"sexeparentold"+parentid+"\" value="+sexe+" readonly>";
                         ligne=ligne+"</div>";
                         ligne=ligne+"</div>";

                         //telephone
                         ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPhoneold"+parentid+"\">";
                         ligne=ligne+"<label class=\"control-label col-md-3\">Téléphone <span class=\"required\"> * </span></label>";
                         ligne=ligne+"<div class=\"col-md-6\">";
                         ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"phoneparentold"+parentid+"\"  id=\"phoneparentold"+parentid+"\" value="+phone+" readonly>";
                         ligne=ligne+"</div>";
                         ligne=ligne+"</div>";
                         //bouton de suppression
                         ligne=ligne+"<div class=\"pull-right\" id=\"ligneRowold"+parentid+"\">";
                         ligne=ligne+"<button type=\"button\" id=\"delete"+parentid+"\" name=\"delete"+parentid+"\"  onclick=\"deletedoldrow("+parentid+")\" class=\"btn btn-danger pull-right\"><i class=\"fa fa-trash\"></i></button>";
                         ligne=ligne+"</div></br></br>";

                         $('#dynamic_field').append(ligne);

                       }
                     });

            }

          }});
      }

      function Addlignestandard()
      {
        var ligne="<tr id=\"lignevide\">";
        ligne=ligne+"<td colspan=\"4\" style=\"text-align:center\">Aucune information</td>";
        ligne=ligne+"</tr>";

        $('#tablebody').append(ligne);

      }

      function deletedlineAnte(nouveau)
      {
        var concatantecedent=$("#concatantecedent").val();
        $("#concatantecedent").val($("#concatantecedent").val().replace(nouveau+"@", ""));
	      $("#ligneAnte"+nouveau).remove();

        var tabconcatligne=$("#concatantecedent").val().split("@");
        var nb=(tabconcatligne.length)-1;
        if(nb==0)
        {
          Addlignestandard();
        }
      }



      function getandecedentRow(nouveau)
      {
        var session="<?php echo $libellesessionencours; ?>";
        var codeEtab="<?php echo $codeEtabLocal; ?>";
        var etape=17;

         $.ajax({
           url: '../ajax/admission.php',
           type: 'POST',
           async:false,
           data: 'session=' +session+ '&etape=' + etape+'&codeEtab='+codeEtab,
           dataType: 'text',
           success: function (response, statut) {
             $("#libelleantecedet"+nouveau).html("");
             $("#libelleantecedet"+nouveau).html(response);
             // $("#heureLibelles"+nouveau).html(content);

           }
         });


      }

      function AddRow()
      {
        //var nb=$("#nbantecedent").val();
        var nb=$("#nbconcatantecedent").val();
      	var nouveau= parseInt(nb)+1;
      	var concatantecedent=$("#concatantecedent").val();
      	$("#concatantecedent").val(concatantecedent+nouveau+"@");
        // recalculantecedentnb();
        getandecedentRow(nouveau);

        var ligne="<tr  id=\"ligneAnte"+nouveau+"\" style=\"cursor:pointer\" ondblclick=\"deletedlineAnte("+nouveau+")\" >";
        ligne=ligne+"<td>";
        // ligne=ligne+"<select class=\"form-control \" id=\"libelleantecedet"+nouveau+"\" name=\"libelleantecedet"+nouveau+"\" style=\"width:100%;text-align:center\">";
        // ligne=ligne+"<option value=\"\">Selectionner un Antécédent Médical</option>";
        // ligne=ligne+"<option value=\"1\">Asthme / Asthma</option>";
        // ligne=ligne+"<option value=\"2\">Drépanocytose / Sickle Cell Disease</option>";
        // ligne=ligne+"<option value=\"3\">Cardiopathie / Heart Disease</option>";
        // ligne=ligne+"<option value=\"4\">Epilepsie / Epilepsy</option>";
        // ligne=ligne+"<option value=\"5\">Hypertension / High Blood Pressure</option>";
        // ligne=ligne+"<option value=\"6\">Rhumatisme / Rheumatism</option>";
        // ligne=ligne+"<option value=\"7\">Diabète / Diabetes</option>";
        // ligne=ligne+"</select>";
          ligne=ligne+"<input type=\"text\" class=\"form-control \" id=\"libelleantecedet"+nouveau+"\" name=\"libelleantecedet"+nouveau+"\" style=\"width:100%;text-align:center\" >";
        ligne=ligne+"</td>";

        ligne=ligne+"<td>";
        ligne=ligne+"<select class=\"form-control \" id=\"childantecedet"+nouveau+"\" name=\"childantecedet"+nouveau+"\" style=\"width:100%;text-align:center\">";
        ligne=ligne+"<option value=\"1\" selected>OUI</option>";
        ligne=ligne+"<option value=\"2\">NON</option>";
        ligne=ligne+"</select>";
      	ligne=ligne+"</td>";

        ligne=ligne+"<td>";
        ligne=ligne+"<select class=\"form-control \" id=\"fatherantecedet"+nouveau+"\" name=\"fatherantecedet"+nouveau+"\" style=\"width:100%;text-align:center\">";
        ligne=ligne+"<option value=\"1\" selected>OUI</option>";
        ligne=ligne+"<option value=\"2\">NON</option>";
        ligne=ligne+"</select>";
        ligne=ligne+"</td>";

        ligne=ligne+"<td>";
        ligne=ligne+"<select class=\"form-control \" id=\"motherantecedet"+nouveau+"\" name=\"motherantecedet"+nouveau+"\" style=\"width:100%;text-align:center\">";
        ligne=ligne+"<option value=\"1\" selected>OUI</option>";
        ligne=ligne+"<option value=\"2\">NON</option>";
        ligne=ligne+"</select>";
        ligne=ligne+"</td>";
        ligne=ligne+"</tr>";

        $("#nbconcatantecedent").val(nouveau);

        $('#tablebody').append(ligne);

      }

      function recalculantecedentnb()
      {

      }

      function AddMedicalsRow()
      {
        //var nb=$("#nbantecedent").val();
        var nb=$("#nbconcatantecedent").val();

        if(nb==0)
          {
          	$("#lignevide").remove();

          	AddRow();

          }else {
          	AddRow();
          }
      }




      function getInfantiles()
      {
        var session="<?php echo $libellesessionencours; ?>";
        var codeEtab="<?php echo $codeEtabLocal; ?>";
        var etape=18;

         $.ajax({
           url: '../ajax/admission.php',
           type: 'POST',
           async:false,
           data: 'session=' +session+ '&etape=' + etape+'&codeEtab='+codeEtab,
           dataType: 'text',
           success: function (response, statut) {
             $("#deseasesinfant").html("");
             $("#deseasesinfant").html(response);
             // $("#heureLibelles"+nouveau).html(content);

           }
         });
      }

      function getAllergies()
      {
        var session="<?php echo $libellesessionencours; ?>";
        var codeEtab="<?php echo $codeEtabLocal; ?>";
        var etape=19;

         $.ajax({
           url: '../ajax/admission.php',
           type: 'POST',
           async:false,
           data: 'session=' +session+ '&etape=' + etape+'&codeEtab='+codeEtab,
           dataType: 'text',
           success: function (response, statut) {
             $("#allergiestudents").html("");
             $("#allergiestudents").html(response);
             // $("#heureLibelles"+nouveau).html(content);

           }
         });
      }

      function AddparentsRow()
      {
        var nb=$("#nbparent").val();
        var nouveau= parseInt(nb)+1;
        var concatparents=$("#concatparents").val();
        $("#concatparents").val(concatparents+nouveau+"@");

        //recalcule nbconcatparent
        recalculparentnb();

        var ligne="<div class=\"form-group row\" id=\"ligneEntete"+nouveau+"\">";
        // ligne=ligne+"<label class=\"control-label col-md-8\" style=\"color:#e40d22\">INFORMATIONS PARENT "+nouveau+" <span class=\"required\">  </span></label>";
        ligne=ligne+"<span class=\"label label-md label-info\"  style=\"text-align:center;margin-left:400px;font-family: \"Trebuchet MS\", Verdana, sans-serif;font-style: italic;font-variant-ligatures: no-common-ligatures;font-size:large;\">INFORMATIONS PARENT " +nouveau+"</span>";
        ligne=ligne+"</div>";
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowMobilephone"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\">Tel Mobile <span class=\"required\"> * </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"mobileparent"+nouveau+"\"  id=\"mobileparent"+nouveau+"\" onchange=\"telchecked("+nouveau+")\"  placeholder=\"Renseigner le téléphone mobile du parent\">";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //telephone mobile
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowName"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\">Nom <span class=\"required\"> * </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"nomparent"+nouveau+"\"  id=\"nomparent"+nouveau+"\" placeholder=\"Renseigner le nom du parent\">";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //prenoms
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPrename"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\">Prénoms <span class=\"required\"> * </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"prenomparent"+nouveau+"\"  id=\"prenomparent"+nouveau+"\" placeholder=\"Renseigner le prénom du parent\">";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //sexe
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowSexe"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\">Genre<span class=\"required\"> * </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<select class=\"form-control \" id=\"sexeparent"+nouveau+"\" name=\"sexeparent"+nouveau+"\" style=\"width:100%\">";
        ligne=ligne+"<option value=\"\">Selectionner un Genre</option>";
        ligne=ligne+"<option value=\"M\">Masculin</option>";
        ligne=ligne+"<option value=\"F\">Feminin</option>";
        ligne=ligne+"</select>";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //telephone
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowMetier"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\">Profession<span class=\"required\">  </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"metierparent"+nouveau+"\"  id=\"metierparent"+nouveau+"\" placeholder=\"Renseigner la profession du parent\" >";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //profession
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowMetier"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\">Email<span class=\"required\">  </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"email\" class=\"form-control \" name=\"emailparent"+nouveau+"\"  id=\"emailrparent"+nouveau+"\" placeholder=\"Renseigner l'adresse email du parent\"  >";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //email
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowEmployeur"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\">Employeur<span class=\"required\">  </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"employeurparent"+nouveau+"\"  id=\"employeurparent"+nouveau+"\"  placeholder=\"Renseigner l'employeur du parent\">";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //employeur
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPostaleadress"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\">Adresse postale<span class=\"required\">  </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"postaleadressparent"+nouveau+"\"  id=\"postaleadressparent"+nouveau+"\"  placeholder=\"Renseigner l'adresse postale du parent\" >";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //adresse postale

        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPhonework"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\">Tel Bureau<span class=\"required\">  </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"phoneparent"+nouveau+"\"  id=\"phoneparent"+nouveau+"\"  placeholder=\"Renseigner le téléphone du bureau du parent\" >";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //telephone domicile
        ligne=ligne+"<div class=\"form-group row\" id=\"ligneRowPhonehome"+nouveau+"\">";
        ligne=ligne+"<label class=\"control-label col-md-3\">Tel domicile<span class=\"required\">  </span></label>";
        ligne=ligne+"<div class=\"col-md-6\">";
        ligne=ligne+"<input type=\"text\" class=\"form-control \" name=\"phonehomeparent"+nouveau+"\"  id=\"phonehomeparent"+nouveau+"\"  placeholder=\"Renseigner le téléphone domiciledu parent\"  >";
        ligne=ligne+"</div>";
        ligne=ligne+"</div>";
        //bouton de suppression
        ligne=ligne+"<div class=\"pull-right\" id=\"ligneRow"+nouveau+"\">";
        ligne=ligne+"<button type=\"button\" id=\"delete"+nouveau+"\" name=\"delete"+nouveau+"\"  onclick=\"deletedrow("+nouveau+")\" class=\"btn btn-danger pull-right\"><i class=\"fa fa-trash\"></i></button>";
        ligne=ligne+"</div></br></br>";

        $("#sexeparent"+nouveau).select2();

        $('#dynamic_field').append(ligne);

        $('#nomparent'+nouveau).rules( "add", {
            required: true,
            messages: {
            required: "Merci de renseigner le nom du parent"
     }
          });

       //    $('#metierparent'+nouveau).rules( "add", {
       //        required: true,
       //        messages: {
       //        required: "Merci de renseigner la profession  du parent"
       // }
       //      });
       //
       //      $('#employeurparent'+nouveau).rules( "add", {
       //          required: true,
       //          messages: {
       //          required: "Merci de renseigner l'employeur du parent"
       //   }
       //        });

          $('#sexeparent'+nouveau).rules( "add", {
              required: true,
              messages: {
              required: "Merci de renseigner le genre du parent"
       }
            });

          $('#prenomparent'+nouveau).rules( "add", {
              required: true,
              messages: {
              required: "Merci de renseigner le prénom du parent"
       }
            });

            $('#mobileparent'+nouveau).rules( "add", {
                required: true,
                digits:true,
                messages: {
                required: "Merci de renseigner le téléphone du parent",
                digits:"<?php echo L::DigitsOnly?>"
         }
              });

      }

  </script>
  </body>

</html>
