<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$imageprofile=$user->getImageProfile($emailUti);
$logindata=$user->getLoginProfile($emailUti);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);

$nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title"><?php echo L::AddLocalAdmins?></div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#"><?php echo L::LocalAdmins?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active"><?php echo L::AddLocalAdmins?></li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addlocalok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: '<?php echo L::Felicitations ?>',
  text: "<?php echo $_SESSION['user']['addlocalok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::AddNews ?>',
  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="index.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addlocalok']);
                    }

                     ?>

                     <div class="row">
                         <div class="col-md-12 col-sm-12">
                             <div class="card card-box">
                                 <div class="card-head">
                                     <header><?php echo L::LocalAdminsInfos?></header>

                                 </div>


                                 <div class="card-body" id="bar-parent">
                                     <form  id="FormAddLocalAd" class="form-horizontal" action="../controller/localadmin.php" method="post" enctype="multipart/form-data">
                                         <div class="form-body">
                                           <input type="hidden" id="libetab" name="libetab" value="<?php echo $codeEtabAssigner;?>" />
                                            <input type="hidden"  name="comptead" id="comptead" value="ADMIN LOCAL" />
                                            <div class="form-group row">
                                                 <label class="control-label col-md-3"><?php echo L::Name?>
                                                     <span class="required"> * </span>
                                                 </label>
                                                 <div class="col-md-5">
                                                     <input type="text" name="nomad" id="nomad" data-required="1" placeholder="<?php echo L::EnterName ?>" class="form-control input-height" /> </div>
                                             </div>
                                             <div class="form-group row">
                                                 <label class="control-label col-md-3"><?php echo L::PreName?>
                                                     <span class="required"> * </span>
                                                 </label>
                                                 <div class="col-md-5">
                                                     <input type="text" name="prenomad" id="prenomad" data-required="1" placeholder="<?php echo L::Enterparentprename ?>" class="form-control input-height" /> </div>
                                             </div>

                                             <div class="form-group row">
                                               <label class="control-label col-md-3"><?php echo L::BirthstudentTab?>
                                                   <span class="required"> * </span>
                                               </label>
                                                   <div class="col-md-5">
                                                       <input type="date" placeholder="Entrer la date de naissance" name="datenaisad" id="datenaisad"  class="form-control input-height">

                                                   </div>
                                               </div>
                                             <div class="form-group row">
                                                 <label class="control-label col-md-3">Contact
                                                     <span class="required">*  </span>
                                                 </label>
                                                 <div class="col-md-5">
                                                     <input name="contactad" id="contactad" type="text" placeholder="Entrer le contact " class="form-control input-height" /> </div>
                                             </div>
                                             <div class="form-group row">
                                                 <label class="control-label col-md-3"><?php echo L::Fonction?>
                                                     <span class="required">*  </span>
                                                 </label>
                                                 <div class="col-md-5">
                                                   <select name="fonctionad" id="fonctionad" placeholder="Entrer la fonction" class="form-control input-height">
                                                              <option value=""> <?php echo L::SelectFonction?> </option>
                                                              <option value="Proviseur"> Proviseur </option>
                                                              <option value="Censeur"> Censeur </option>
                                                              <option value="Surveillant"> Surveillant Genaral</option>
                                                              <option value="Professeur">Professeur</option>
                                                              <option value="Intendant">Intendant</option>
                                                              <option value="Autre">Autres</option>
                                                        </select>

                                                    </div>
                                             </div>

                                             <div class="form-group row">
                                                 <label class="control-label col-md-3"><?php echo L::EmailstudentTab?>
                                                   <span class="required">*  </span>
                                                 </label>
                                                 <div class="col-md-5">
                                                     <div class="input-group">
                                                         <span class="input-group-addon">
                                                                 <i class="fa fa-envelope"></i>
                                                             </span>
                                                         <input type="text" class="form-control input-height" name="emailad" id="emailad" placeholder="Entrer votre Adresse email"> </div>
                                                 </div>
                                             </div>
                                             <div class="form-group row">
                                                 <label class="control-label col-md-3"><?php echo L::Logincnx?>
                                                     <span class="required">*  </span>
                                                 </label>
                                                 <div class="col-md-5">
                                                     <input name="loginad" id="loginad" type="text" placeholder="Entrer le Login " class="form-control input-height" /> </div>
                                             </div>
                                             <div class="form-group row">
                                                 <label class="control-label col-md-3"><?php echo L::Passcnx?>
                                                     <span class="required">*  </span>
                                                 </label>
                                                 <div class="col-md-5">
                                                     <input name="passad" id="passad" type="password" placeholder="Entrer le Mot de passe " class="form-control input-height" /> </div>
                                             </div>
                                             <div class="form-group row">
                                                 <label class="control-label col-md-3"><?php echo L::ConfirmPasscnx?>
                                                     <span class="required">*  </span>
                                                 </label>
                                                 <div class="col-md-5">
                                                     <input name="confirmtad" id="confirmtad" type="password" placeholder="Confirmer le Mot de passe " class="form-control input-height" /> </div>
                                             </div>

                                             <div class="form-group row">
                                                 <label class="control-label col-md-3"><?php echo L::Pictures?>
                                                   <span class="required">  </span>
                                                 </label>
                                                 <div class="compose-editor">
                                                   <input type="file" id="photoad" name="photoad" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="../assets2/img/user/user5.jpg" data-allowed-file-extensions="gif png jpg jpeg pjpeg" />
                                                   <input type="hidden" name="etape" id="etape" value="1"/>
                                               </div>
                                             </div>

                       <div class="form-actions">
                                             <div class="row">
                                                 <div class="offset-md-3 col-md-9">
                                                     <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                                     <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                 </div>
                                               </div>
                                            </div>
                     </div>
                                     </form>
                                 </div>
                             </div>
                         </div>
                     </div>


          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <script src="../assets2/plugins/select2/js/select2.js" ></script>
   <script src="../assets2/js/pages/select2/select2-init.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>

 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script>
 function addFrench()
 {
   var etape=1;
   var lang="fr";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function addEnglish()
 {
   var etape=1;
   var lang="en";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 jQuery(document).ready(function() {

 $("#libetab").select2();
 $("#fonctionad").select2();


   $("#FormAddLocalAd").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{
        passad: {
            required: true,
            minlength: 6
        },
        confirmtad:{
            required: true,
            minlength: 6,
            equalTo:'#passad'
        },
        fonctionad:"required",

        loginad:"required",
        emailad: {
                   required: true,
                   email: true
               },
        contactad:"required",
        datenaisad:"required",
        prenomad:"required",
        nomad:"required",
        libetab:"required"


      },
      messages: {
        confirmtad:{
            required:"<?php echo L::Confirmcheck?>",
            minlength:"<?php echo L::Confirmincheck?>",
            equalTo: "<?php echo L::ConfirmSamecheck?>"
        },
        passad: {
            required:"<?php echo L::Passcheck?>",
            minlength:"<?php echo L::Confirmincheck?>"
        },
        loginad:"<?php echo L::Logincheck?>",
        emailad:"<?php echo L::PleaseEnterEmailAdress?>",
        contactad:"<?php echo L::PleaseEnterPhoneNumber?>",
        datenaisad:"<?php echo L::PleaseEnterPhonestudentTab?>",
        prenomad:"<?php echo L::PleaseEnterPrename?>",
        nomad:"<?php echo L::PleaseEnterName?>",
        fonctionad:"<?php echo L::PleaseEnterFonction?>",
        libetab:"<?php echo L::PleaseSelectSchoolEnseignement?>"
      },
      submitHandler: function(form) {


   }


 });

 });

 </script>
    <!-- end js include path -->
  </body>

</html>
