<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

  <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<!-- <link rel="stylesheet" href="assets2/plugins/iconic/css/material-design-iconic-font.min.css"> -->
	<link rel="stylesheet" href="assets2/cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <!-- bootstrap -->
	<link href="assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- style -->
    <link rel="stylesheet" href="assets2/css/pages/extra_pages.css">
      <link rel="stylesheet" href="assets/css/sweetalert2.min.css"/>
	<!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon/favicon.ico"  />
</head>
<body>
    <div class="limiter">
		<div class="container-login100 page-background">
			<div class="wrap-login100">
				<form class="login100-form validate-form" id="Formcnx" method="post" action="controller/connexion.php">
					<!--span class="login100-form-logo">
						<img alt="" src="assets/img/logo/logo1.png">
					</span-->
          <img alt="" src="assets/img/favicon/logo.jpg" style="margin-left:85px;border-radius:10px">
					<span class="login100-form-title p-b-34 p-t-27">
						Connexion
					</span>
					<div class="wrap-input100 validate-input" data-validate = "Entrer votre Login">
						<input class="input100" type="text" name="login" id="login" placeholder="Login">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>
					<div class="wrap-input100 validate-input" data-validate="Entrer votre Mot de passe">
						<input class="input100" type="password" name="pass" id="pass" placeholder="Mot de passe">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>
					<!--div class="contact100-form-checkbox">
						<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
						<label class="label-checkbox100" for="ckb1">
							Remember me
						</label>
					</div-->
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							se connecter
						</button>
					</div>
					<!--div class="text-center p-t-30">
						<a class="txt1" href="forgot_password.html">
							Forgot Password?
						</a>
					</div-->
				</form>
			</div>
		</div>
	</div>
    <!-- start js include path -->
     <script src="assets2/plugins/jquery/jquery.min.js" ></script>
    <!-- bootstrap -->
    <script src="assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <script src="assets2/js/pages/extra_pages/extra_pages.js"></script>
    <script type="text/javascript" src="assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="assets/js/libs/form-validator/jquery.validate.min.js"></script>
    <script>
               showSwal = function(type){
                  'use strict';
                  if(type === 'basic'){
              	swal({
                  title:"Erreur",
                  text:"Le login saisi n'existe pas dans notre base de données",
                  type:'warning'
                })

          	}else if(type === 'basic1')
            {
              swal({
                title:"Compte inactif",
                text:"Merci de prendre contact avec le support IT",
                type:'warning'
              })
            }else if(type === 'basic2')
            {
              swal({
                title:"Mot de passe incorrect",
                text:"Merci de réessayer avec le bon Mot de passe",
                type:'warning'
              })
            }else if(type === 'bonsoir')
              {
                  swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Great '
                  }).then(function () {
                  swal(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                  )
                })
              }
               }
              </script>
    <script>
     jQuery(document).ready(function() {

       $("#Formcnx").validate({

         errorPlacement: function(label, element) {
         label.addClass('mt-2 text-danger');
         label.insertAfter(element);
       },
       highlight: function(element, errorClass) {
         $(element).parent().addClass('has-danger')
         $(element).addClass('form-control-danger')
       },
       success: function (e) {
             $(e).closest('.control-group').removeClass('error').addClass('info');
             $(e).remove();
         },
         rules:{

           login:{
              required: true
              //email: true
            },
           pass:{
            required: true,
            minlength: 6
          }
         },
         messages: {
            login:"Merci de renseigner vôtre Login.",
            pass:{
          required: "Merci de renseigner votre mot de passe",
          minlength: "Votre mot de passe doit contenir au moins 6 caractères"
          }

        },

        submitHandler: function(form) {

          var etape=1;
          $.ajax({

            url: 'ajax/connexion.php',
            type: 'POST',
            async:false,
            data: 'login=' + $("#login").val()+ '&pass=' + $("#pass").val()+ '&etape=' + etape,
            dataType: 'text',
            success: function (content, statut) {

              if(content==0)
              {
                //showSwal('basic');
                Swal.fire({
                type: 'warning',
                title: 'Erreur',
                text: "Le login saisi n'existe pas dans notre base de données",

                })
              }else if(content==1) {
                //showSwal('basic1');
                Swal.fire({
                type: 'warning',
                title: 'Compte Inactif',
                text: "Merci de prendre contact avec le support IT",

                })
              }else if(content==2)
              {
                //showSwal('basic2');
                Swal.fire({
                type: 'warning',
                title: 'Mot de passe incorrect',
                text: "Merci de réessayer avec le bon Mot de passe",

                })
              }else {
                form.submit();
              }

            }
          });

        }

       });
     });
     </script>

    <!-- end js include path -->
</body>

</html>
