<?php
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
class mysqlConnector {

    public $dataBase;

    function __construct() {
        //Authentification à mysql
        $host="localhost";
        $dbName="xschool";
        $username = "root";
        $passwd="";
        try {
            //dbName si bug
            $dsn="mysql:host=".$host.";dbname=".$dbName;
            $this->dataBase = new PDO($dsn, $username, $passwd);
            $this->dataBase->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->dataBase->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        } catch (PDOException $exc) {
            echo $exc->getMessage();
            echo "<br>IMPOSSIBLE DE SE CONNECTER A LA BDD";
            die();
        }
    }

}
