<?php
class Localadmin{

public $db;
function __construct() {
    require_once('../class/cnx.php');

  $db = new mysqlConnector();
  $this->db= $db->dataBase;
    }

    function generateficheLocalpdf($compte,$codeEtab,$photoprofil)
    {
      require_once('../class_html2PDF/html2pdf.class.php');
      require_once('../class_html2PDF/_tcpdf_5.0.002/config/lang/eng.php');
      require_once('../class_html2PDF/_tcpdf_5.0.002/tcpdf.php');
      require_once('../class/Etablissement.php');
      //recuperation des informations de l'etablissement
      $etabs=new Etab();

      $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
      $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
      $lienlogo="../logo_etab/".$codeEtab."/".$logoEtab;
      $contacts="";



      $infosEtab=$etabs->getEtabInfosbyCode($codeEtab);
      $tabEtab=explode('*',$infosEtab);

       if($tabEtab[3]!="")
       {
         $contacts=$tabEtab[2]." / ".$tabEtab[3];
       }else {
         $contacts=$tabEtab[2];
       }

      $admin= new Localadmin();
      $admins=$admin->getAdminlocalbyId($compte);
      $tabAdmin=explode("*",$admins);

      $photoprofil="";
      $photoprofilLink="";
      if($tabAdmin[8]!="")
      {
        $photoprofil=$tabAdmin[8];
        $photoprofilLink="../photo/".$tabAdmin[6]."/".$photoprofil;
      }else {
          $photoprofilLink="../photo/user5.jpg";
      }


       ob_start();

       ?>
       <page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
         <page_header>
         <table style="width: 100%; border: solid 0px #000; font-size: 12px; font-family: Arial, Helvetica, sans-serif; margin-top:-8px;">
               <tr>
               <td width="25%" height="100" style="width: 30%;"><img src="<?php echo $lienlogo ?>" width="120" height="70" /></td>
               <td style="width: 30%;">
               <table width="70%" height="38" border="0" align="center" cellspacing="0">
                 <tr></tr>
               </table>     </td>
               </tr>
               <tr>
               <td valign="top" style="width: 100%; font-size: 12px; font-weight: normal;" rowspan="">

               <p style="border:1px solid black; padding:10px; width:60%;">

                 <span class="Style3"><b>EmaiL </b></span> : <?php echo $tabEtab[4];?><span></span><br /><br />
                 <span class="Style3"><b>Telephone </b></span> :<?php echo $contacts; ?><span></span><br /><br />

                 </p>

               <br/>
               </td>

               </tr>

               </table>
         </page_header>
         <h3><span style='margin-top:140px;margin-left:240px;'>DETAILS ADMIN LOCAL</span></h3>
         <table border='1' cellpadding='3' cellspacing='0' style="width: 100%; border: solid 0px #000; font-size: 12px; font-family: Arial, Helvetica, sans-serif; margin-top:170px;margin-left:550px;">
               <tr>
               <td width="25%" height="100" style="width: 30%;"><img src="<?php echo $photoprofilLink ; ?>" width="120" height="70" /></td>

               </tr>


               </table>
         <table id='table' border='1'cellpadding='3' cellspacing='0' style='margin-top:70px;width:100%;margin-left:-25px;width:1625px;'>
      <tr>
      <td style='width:10%;height:20px;text-align:center;'>Nom</td>
      <td style='width:30%;'><?php echo $tabAdmin[1];?></td>
      </tr>
      <tr>
    	<td style='width:10%;height:20px;text-align:center;'>Pr&eacute;noms</td>
    		<td style='width:20%;'><?php echo $tabAdmin[2];?></td>

    	</tr>
      <tr>
    	<td style='width:10%;height:20px;text-align:center;'>Date naissance</td>
    		<td style='width:20%;'><?php echo $tabAdmin[2];?></td>

    	</tr>
      <tr>
    	<td style='width:10%;height:20px;text-align:center;'>Fonction</td>
    		<td style='width:20%;'><?php echo $tabAdmin[2];?></td>

    	</tr>
      <tr>
    	<td style='width:10%;height:20px;text-align:center;'>Contact(s)</td>
    		<td style='width:20%;'><?php echo $tabAdmin[2];?></td>

    	</tr>
      <tr>
    	<td style='width:10%;height:20px;text-align:center;'>Email</td>
    		<td style='width:20%;'><?php echo $tabAdmin[2];?></td>

    	</tr>

      </table>

      </page>
       <?php
       $content=ob_get_clean();

$html2pdf = new HTML2PDF('P', 'A4', 'fr');
	$html2pdf->pdf->SetDisplayMode('real');

	$html2pdf->writeHTML($content);
	 //ob_clean();
   $completName=$tabAdmin[1]."_".$tabAdmin[2]."-".$compte;

		$html2pdf->Output('../generated/'.$completName.'.pdf','F');

  return '../generated/'.$completName.'.pdf';




    }

    function getNumberOfLocalAdmin()
    {
      $type_compte="Admin_locale";
        $req = $this->db->prepare("SELECT * FROM compte where type_compte=?");
        $req->execute([$type_compte]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
    }

    function suppressionLocalInThisSystem($compte)
    {
      $req = $this->db->prepare("DELETE FROM compte where id_compte=?");
      $req->execute([$compte]);


    }

    function getAllAdminLocalByCodeEtabAndId($adminlo,$codeetab)
    {
        $req = $this->db->prepare("SELECT * FROM compte,assigner,etablissement where compte.id_compte=assigner.id_adLocal and etablissement.code_etab=assigner.codeEtab_assign and compte.id_compte=? and assigner.codeEtab_assign=?");
        $req->execute([$adminlo,$codeetab]);
        return $req->fetchAll();
    }

    function Updatelocalwithfile($nomad,$prenomad,$datenaisad,$contactad,$emailad,$loginad,$fonctionad,$codeetab,$idadLocal,$fichierad,$oldcodeetab)
    {
        //mise à jour de la table compte

          $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,login_compte=?,fonction_compte=?,photo_compte=? WHERE id_compte=?");
            $req->execute([
              $nomad,
              $prenomad,
              $datenaisad,
              $contactad,
              $emailad,
              $loginad,
              $fonctionad,
              $fichierad,
              $idadLocal
            ]);

            //changer l'assignation
            $req = $this->db->prepare("UPDATE assigner SET codeEtab_assign=? WHERE id_adLocal=? and codeEtab_assign=?");
              $req->execute([$codeetab,$idadLocal,$oldcodeetab]);



             //header("Location:../manager/localadmins.php");
    }


    function Updatelocalwithoutfile($nomad,$prenomad,$datenaisad,$contactad,$emailad,$loginad,$fonctionad,$codeetab,$idadLocal,$oldcodeetab)
    {
      //mise à jour de la table compte

        $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,login_compte=?,fonction_compte=? WHERE id_compte=?");
          $req->execute([
            $nomad,
            $prenomad,
            $datenaisad,
            $contactad,
            $emailad,
            $loginad,
            $fonctionad,
            $idadLocal
          ]);

          //changer l'assignation
          $req = $this->db->prepare("UPDATE assigner SET codeEtab_assign=? WHERE id_adLocal=? and codeEtab_assign=?");
            $req->execute([$codeetab,$idadLocal,$oldcodeetab]);


           //header("Location:../manager/localadmins.php");

    }


  function getAdminlocalbyId($compte)
  {
    $req = $this->db->prepare("SELECT  * FROM compte,assigner,etablissement where compte.id_compte=assigner.id_adLocal and assigner.codeEtab_assign=etablissement.code_etab and compte.id_compte=? ");
    $req->execute([$compte]);
    $data=$req->fetchAll();
      $array=json_encode($data,true);
      $someArray = json_decode($array, true);

      $donnees=$someArray[0]["libelle_etab"]."*".$someArray[0]["nom_compte"]."*".$someArray[0]["prenom_compte"]."*".$someArray[0]["datenais_compte"]."*".$someArray[0]["tel_compte"]."*".$someArray[0]["fonction_compte"];
      $donnees.="*".$someArray[0]["email_compte"]."*".$someArray[0]["login_compte"]."*".$someArray[0]["photo_compte"]."*".$someArray[0]["code_etab"];
      return $donnees;

  }

  function getAdminlocalbyIdNew($compte)
  {
    $req = $this->db->prepare("SELECT  * FROM compte,assigner,etablissement where compte.id_compte=assigner.id_adLocal and assigner.codeEtab_assign=etablissement.code_etab and compte.id_compte=? ");
    $req->execute([$compte]);
    $data=$req->fetchAll();
    return $data;

  }

function getEtabAssigner($idcompte)
{
  $req = $this->db->prepare("SELECT  * FROM compte,assigner,etablissement where compte.id_compte=assigner.id_adLocal and assigner.codeEtab_assign=etablissement.code_etab and assigner.id_adLocal=? ");
  $req->execute([$idcompte]);
  $data=$req->fetchAll();

    $array=json_encode($data,true);
    $someArray = json_decode($array, true);

    $donnees=$someArray[0]["libelle_etab"];

    return $donnees;
}


function getLocalbycompteId($id)
{
  $req = $this->db->prepare("SELECT  * FROM compte where id_compte=?");
  $req->execute([$id]);
  return $req->fetchAll();
}

function getAllAdmibyschoolCode($code,$localId)
{
  $req = $this->db->prepare("SELECT  * FROM assigner,compte,etablissement where etablissement.code_etab=assigner.codeEtab_assign and compte.id_compte=assigner.id_adLocal  and assigner.codeEtab_assign=? and compte.id_compte!=? ");
  $req->execute([$code,$localId]);
  return $req->fetchAll();
}

function AssignerEtab($codeetab,$idadLocal)
{

  $req = $this->db->prepare("INSERT INTO assigner SET codeEtab_assign=?,id_adLocal=?");
    $req->execute([$codeetab,$idadLocal]);

  $_SESSION['user']['addlocalok']="Admin Local ajouté avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

header("Location:../manager/addlocal.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/addlocal.php");
      }else {
        header("Location:../locale/addlocal.php");
      }


      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }

   //header("Location:../manager/addlocal.php");

}

function getIdlocal($email)
{
  $req = $this->db->prepare("SELECT  id_compte FROM compte where email_compte=?");
  $req->execute([$email]);
  $data=$req->fetchAll();

    $array=json_encode($data,true);
    $someArray = json_decode($array, true);

    $donnees=$someArray[0]["id_compte"];

    return $donnees;
}

function existAdminbySchoolCode($email,$idAdlocal)
{
  $type_cpte="Admin_local";
  $req = $this->db->prepare("SELECT * FROM compte,assigner where assigner.id_adLocal=compte.id_compte and type_compte=? and compte.id_compte=? ");
  $req->execute([$type_cpte,$idAdlocal]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function existAdminLocal($login,$email)
{
  $type_cpte="Admin_local";
  $req = $this->db->prepare("SELECT * FROM compte where type_compte=? and  (login_compte=? or email_compte=?)");
  $req->execute([$type_cpte,$login,$email]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;

}

function Addlocalwithfile($nomad,$prenomad,$datenaisad,$contactad,$emailad,$fonctionad,$loginad,$passad,$type_cpte,$statut,$datecrea,$fichierad,$codeEtab)
{



$req = $this->db->prepare("INSERT INTO compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=?,login_compte=?,pass_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?,photo_compte=?");
  $req->execute([
    $nomad,
    $prenomad,
    $datenaisad,
    $contactad,
    $emailad,
    $fonctionad,
    $loginad,
    $passad,
    $type_cpte,
    $statut,
    $datecrea,
    $fichierad
]);

//recuperer l'id du compte

$nbcompte=$this->db->lastInsertId();

//insertion dans la table assigner

  $req = $this->db->prepare("INSERT INTO assigner SET codeEtab_assign=?,id_adLocal=?");
  $req->execute([$codeEtab,$nbcompte]);




 //header("Location:../manager/addlocal.php");


}

function Addlocalwithoutfile($nomad,$prenomad,$datenaisad,$contactad,$emailad,$fonctionad,$loginad,$passad,$type_cpte,$statut,$datecrea,$codeEtab)
{


  $req=$this->db->prepare("INSERT INTO compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=?,login_compte=?,pass_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?");
  $req->execute([
    $nomad,
    $prenomad,
    $datenaisad,
    $contactad,
    $emailad,
    $fonctionad,
    $loginad,
    $passad,
    $type_cpte,
    $statut,
    $datecrea

]);

//recuperer l'id du compte

$nbcompte=$this->db->lastInsertId();

//insertion dans la table assigner

  $req = $this->db->prepare("INSERT INTO assigner SET codeEtab_assign=?,id_adLocal=?");
  $req->execute([$codeEtab,$nbcompte]);



 //header("Location:../manager/addlocal.php");
}

function getAllAdminLocalByUserId($codeEtabAssigner,$idcompte)
{
  $type_compte="Admin_locale";
  $req = $this->db->prepare("SELECT * FROM compte,assigner,etablissement where compte.id_compte=assigner.id_adLocal and assigner.codeEtab_assign=etablissement.code_etab and   compte.type_compte=? and etablissement.code_etab=? and compte.id_compte!=? ");
   $req->execute([$type_compte,$codeEtabAssigner,$idcompte]);
   return $req->fetchAll();
}

function getAllAdminLocal()
{
  $type_compte="Admin_locale";
  $req = $this->db->prepare("SELECT * FROM compte where type_compte=? order by datecrea_compte desc  ");
   $req->execute([$type_compte]);
   return $req->fetchAll();
}

function getAllAdminLocalBysearch($compte)
{
  $type_compte="Admin_local";
  $req = $this->db->prepare("SELECT * FROM compte where type_compte=? and id_compte=? ");
   $req->execute([$type_compte,$compte]);
   return $req->fetchAll();
}

function getAllAdminLocalBysearchCode($code)
{
  $req = $this->db->prepare("SELECT  * FROM assigner,compte,etablissement where etablissement.code_etab=assigner.codeEtab_assign and compte.id_compte=assigner.id_adLocal  and assigner.codeEtab_assign=? ");
  $req->execute([$code]);
  return $req->fetchAll();
}


}
  ?>
