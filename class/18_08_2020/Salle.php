<?php

class Salle{

public $db;
function __construct() {
  require_once('../class/cnx.php');

  $db = new mysqlConnector();
  $this->db= $db->dataBase;
}

function getAllSallesOfThisSchool($compte)
{
  $req = $this->db->prepare("SELECT * FROM salle where  codeEtab_salle=? order by datecrea_salle DESC");
  $req->execute([$compte]);
  return $req->fetchAll();
}

function ExisteSalles($libetab,$salle)
  {
    $req = $this->db->prepare("SELECT * FROM salle where libelle_salle=? and codeEtab_salle=?");
    $req->execute([$salle,$libetab]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;
  }

  function getOccupyby($idsalle)
  {
    $req = $this->db->prepare("SELECT * FROM salle,attribution where salle.id_salle=attribution.idsalle_attrib and salle.id_salle=? ");
    $req->execute([$idsalle]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;

  }

  function AddsalleLibre($libellesalle,$codeEtab,$capacitesalle)
  {
    $dateday=date("Y-m-d");
    $statut=0;
    $req = $this->db->prepare("INSERT INTO salle SET libelle_salle=?,capacite_salle=?,codeEtab_salle=?,statut_salle=?,datecrea_salle=?");
    $req->execute([$libellesalle,$capacitesalle,$codeEtab,$statut,$dateday]);



  }

  function getClasseOccupyby($idsalle)
  {
    $req = $this->db->prepare("SELECT * FROM salle,classe,attribution where salle.id_salle=attribution.idsalle_attrib and attribution.idclasse_attrib=classe.id_classe and salle.id_salle=? ");
    $req->execute([$idsalle]);
    $data=$req->fetchAll();
    $array=json_encode($data,true);
    $someArray =json_decode($array, true);
    $donnees=$someArray[0]["libelle_classe"];
    return $donnees;

  }

  function Addsalle($libellesalle,$codeEtab,$capacitesalle,$classe)
  {
    $dateday=date("Y-m-d");
    $statut=1;
    $req = $this->db->prepare("INSERT INTO salle SET libelle_salle=?,capacite_salle=?,codeEtab_salle=?,statut_salle=?,datecrea_salle=?");
    $req->execute([$libellesalle,$capacitesalle,$codeEtab,$statut,$dateday]);

    //insertion dans la attribution

    $salleid=$this->db->lastInsertId();

    $reqX = $this->db->prepare("INSERT INTO attribution SET idclasse_attrib=?,idsalle_attrib=?,date_attrib=?");
    $reqX->execute([$classe,$salleid,$dateday]);

    //nous allons mettre le statut de la classe à 1

    $reqY = $this->db->prepare("UPDATE classe set statut_classe=1 where id_classe=?");
    $reqY->execute([$classe]);



     //header("Location:../manager/addsalle.php");

  }

  function Updatesalle($libellesalle,$codeEtab,$capacitesalle,$idsalle)
  {
    $req = $this->db->prepare("UPDATE  SALLE SET libelle_salle=?,capacite_salle=? where codeEtab_salle=? and id_salle=?");
    $req->execute([$libellesalle,$capacitesalle,$codeEtab,$idsalle]);

  }

  function Deletedsalle($idsalle,$codeEtab)
  {
    $req = $this->db->prepare("DELETE FROM salle WHERE id_salle=?");
    $req->execute([$idsalle]);
  
  }

  function getAllsalles()
  {
    $req = $this->db->prepare("SELECT * FROM salle order by datecrea_salle DESC");
    $req->execute([]);
    return $req->fetchAll();

  }

  function getAllSallesByschoolCode($code)
  {
        $req = $this->db->prepare("SELECT * FROM salle,etablissement WHERE salle.codeEtab_salle=etablissement.code_etab and etablissement.code_etab=? ");
        $req->execute([$code]);
        return $req->fetchAll();
  }

  function getSallesByschoolCodewithId($codeetab,$sallex)
  {
    $req = $this->db->prepare("SELECT * FROM salle,etablissement WHERE salle.codeEtab_salle=etablissement.code_etab and etablissement.code_etab=? and 	id_salle=?");
    $req->execute([$codeetab,$sallex]);
    return $req->fetchAll();
  }

  /*function getAllClassesByschoolCode($codeetab)
  {
      $req = $this->db->prepare("SELECT * FROM classe,etablissement WHERE classe.codeEtab_classe=etablissement.code_etab and etablissement.code_etab=?");
      $req->execute([$codeetab]);
      return $req->fetchAll();
  }*/

  function getAllSallesByClasseId($sallex)
  {
    $req = $this->db->prepare("SELECT * FROM salle,etablissement WHERE salle.codeEtab_salle=etablissement.code_etab and id_salle=?");
    $req->execute([$sallex]);
    return $req->fetchAll();
  }

  function getShoolSallesByClasseId($sallex)
  {
    $req = $this->db->prepare("SELECT * FROM salle,etablissement WHERE salle.codeEtab_salle=etablissement.code_etab and salle.id_salle=?");
    $req->execute([$sallex]);
    $data=$req->fetchAll();
      $array=json_encode($data,true);
      $someArray =json_decode($array, true);
      $donnees=$someArray[0]["libelle_salle"]."*".$someArray[0]["capacite_salle"];
      return $donnees;
  }


  //function getAllSallesbyschoolCode($code)





}

 ?>
