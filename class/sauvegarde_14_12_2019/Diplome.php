<?php

class Diplome{

public $db;
function __construct() {
  require_once('../class/cnx.php');

  $db = new mysqlConnector();
  $this->db= $db->dataBase;
}

function getDiplomebyTeatcherId($compte)
{
  $req = $this->db->prepare("SELECT  * FROM compte,enseignant,diplome WHERE compte.id_compte=diplome.idteatcher_diplo and compte.email_compte=enseignant.email_enseignant and compte.id_compte=?");
  $req->execute([$compte]);
  return $req->fetchAll();

}

function getTeatcherMail($idcompte)
{
  $req = $this->db->prepare("SELECT  * FROM compte where id_compte=?");
  $req->execute([$idcompte]);
  $data=$req->fetchAll();

    $array=json_encode($data,true);
    $someArray = json_decode($array, true);

    $donnees=$someArray[0]["email_compte"];

    return $donnees;
}

function AddDiplomewithFile($libellediplo,$libecole,$speciale,$nivodiplo,$fichierad,$dateo,$idcompte)
{
    $req = $this->db->prepare("INSERT INTO diplome SET libelle_diplo=?,ecole_diplo=?,specialite_diplo=?,niveau_diplo=?,fichier_diplo=?,dateobt_diplo=?,idteatcher_diplo=? ");
    $req->execute([
    $libellediplo,
    $libecole,
    $speciale,
    $nivodiplo,
    $fichierad,
    $dateo,
    $idcompte
  ]);

  $_SESSION['user']['adddiplomeok']="Diplôme ajouté avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

  header("Location:../manager/adddiplomes.php?compte=".$idcompte);
    }else if($_SESSION['user']['profile'] == "Admin_locale") {

  header("Location:../locale/adddiplomes.php?compte=".$idcompte);

      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }


   //header("Location:../manager/adddiplomes.php?compte=".$idcompte);

}


}

 ?>
