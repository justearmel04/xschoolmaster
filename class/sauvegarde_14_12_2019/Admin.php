<?php
class Admin{

public $db;
function __construct() {
  require_once('../class/cnx.php');

  $db = new mysqlConnector();
  $this->db= $db->dataBase;
    }
    function Updateclasses($classe,$codeEtab,$idclasse,$session,$section)
    {
      $req = $this->db->prepare("UPDATE classe set 	libelle_classe=? where  classe.id_classe=? and classe.codeEtab_classe=? and classe.session_classe=? and classe.section_classe=?");
      $req->execute([$classe,$idclasse,$codeEtab,$session,$section]);

      $_SESSION['user']['updateclasseok']="Classe modifié avec succès";

      if($_SESSION['user']['profile'] == "Admin_globale") {
    header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
      // header("Location:../manager/addclasses.php");
        }else if($_SESSION['user']['profile'] == "Admin_locale") {

      header("Location:../locale/classes.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

                header("Location:../teatcher/index.php");

            }else if($_SESSION['user']['profile'] == "Student") {

                  header("Location:../student/index.php");

              }else if($_SESSION['user']['profile'] == "Parent") {

                    header("Location:../parent/index.php");

                }


    }
    function getAdminlocalbyId($compte)
    {
      $req =$this->db->prepare("SELECT  * FROM compte,assigner,etablissement where  compte.id_compte=assigner.id_adLocal and etablissement.code_etab=assigner.codeEtab_assign and  compte.id_compte=? ");
      $req->execute([$compte]);

      $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray =json_decode($array, true);


        $donnees=$someArray[0]["libelle_etab"]."*".$someArray[0]["nom_compte"]."*".$someArray[0]["prenom_compte"]."*".$someArray[0]["datenais_compte"]."*".$someArray[0]["tel_compte"]."*".$someArray[0]["fonction_compte"];
        $donnees.="*".$someArray[0]["email_compte"]."*".$someArray[0]["login_compte"]."*".$someArray[0]["photo_compte"]."*".$someArray[0]["code_etab"];
        return $donnees;

    }

function getAllAdminByIdCompte($idcompte)
{
  $type_compte="Admin_globale";
  $req = $this->db->prepare("SELECT * FROM compte where type_compte=? and id_compte=? order by datecrea_compte desc  ");
   $req->execute([$type_compte,$idcompte]);
   return $req->fetchAll();
}
function getAllpresencesOfThisDay()
{
  $statut=1;
  $date=date("Y-m-d");
  $req = $this->db->prepare("SELECT  * FROM presences where statut_presence=? and date_presence=?");
  $req->execute([$statut,$date]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getAllStudentOfThisSystem()
{
  $type_compte="Student";
  $req = $this->db->prepare("SELECT  * FROM compte where type_compte=?");
  $req->execute([$type_compte]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getAllEtabOfThisSystem()
{
  $req = $this->db->prepare("SELECT  * FROM etablissement");
  $req->execute([]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getAllAdminLocalOfThisSystem()
{
  $type_compte="Admin_locale";
  $req = $this->db->prepare("SELECT  * FROM compte where type_compte=?");
  $req->execute([$type_compte]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getAllTeatcherOfThisSystem()
{
  $type_compte="Teatcher";
  $req = $this->db->prepare("SELECT  * FROM compte where type_compte=?");
  $req->execute([$type_compte]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getAllParentOfThisSystem()
{
  $type_compte="Parent";
  $req = $this->db->prepare("SELECT  * FROM compte where type_compte=?");
  $req->execute([$type_compte]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function UpdateadminwithfilePass($nomad,$prenomad,$datenaisad,$contactad,$emailad,$loginad,$passad,$fonctionad,$idcompte,$fichierad)
{
  $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,login_compte=?,pass_compte=?,fonction_compte=?,photo_compte=? WHERE id_compte=?");
  $req->execute([
  $nomad,
  $prenomad,
  $datenaisad,
  $contactad,
  $emailad,
  $loginad,
  $passad,
  $fonctionad,
  $fichierad,
  $idcompte
  ]);

  $_SESSION['user']['Updateadminok']="Administrateur modifié avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

        header("Location:../manager/admins.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale") {

          header("Location:../locale/admins.php");

      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }


}


function UpdateadminwithfilePassNot($nomad,$prenomad,$datenaisad,$contactad,$emailad,$loginad,$fonctionad,$idcompte,$fichierad)
{
  $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,login_compte=?,fonction_compte=?,photo_compte=? WHERE id_compte=?");
  $req->execute([
  $nomad,
  $prenomad,
  $datenaisad,
  $contactad,
  $emailad,
  $loginad,
  $fonctionad,
  $fichierad,
  $idcompte
  ]);

  $_SESSION['user']['Updateadminok']="Administrateur modifié avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

        header("Location:../manager/admins.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale") {

          header("Location:../locale/admins.php");

      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }

   //header("Location:../manager/admins.php");
}

function UpdateadminwithOutfilePass($nomad,$prenomad,$datenaisad,$contactad,$emailad,$loginad,$passad,$fonctionad,$idcompte)
{
  $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,login_compte=?,fonction_compte=?,pass_compte=? WHERE id_compte=?");
  $req->execute([
  $nomad,
  $prenomad,
  $datenaisad,
  $contactad,
  $emailad,
  $loginad,
  $fonctionad,
  $passad,
  $idcompte
  ]);

  $_SESSION['user']['Updateadminok']="Administrateur modifié avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

        header("Location:../manager/admins.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale") {

          header("Location:../locale/admins.php");

      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }

   //header("Location:../manager/admins.php");
}

function UpdateadminwithOutfilePassNot($nomad,$prenomad,$datenaisad,$contactad,$emailad,$loginad,$fonctionad,$idcompte)
{
  $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,login_compte=?,fonction_compte=? WHERE id_compte=?");
  $req->execute([
  $nomad,
  $prenomad,
  $datenaisad,
  $contactad,
  $emailad,
  $loginad,
  $fonctionad,

  $idcompte
  ]);

  $_SESSION['user']['Updateadminok']="Administrateur modifié avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

        header("Location:../manager/admins.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale") {

          header("Location:../locale/admins.php");

      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }

   //header("Location:../manager/admins.php");
}




function getAllAdministrateurbyId($compte)
{
  $req = $this->db->prepare("SELECT  * FROM compte where  compte.id_compte=?");
  $req->execute([$compte]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);

  $donnees=$someArray[0]["nom_compte"]."*".$someArray[0]["prenom_compte"]."*".$someArray[0]["datenais_compte"]."*".$someArray[0]["tel_compte"]."*".$someArray[0]["fonction_compte"]."*".$someArray[0]["email_compte"];
  $donnees.="*".$someArray[0]["login_compte"]."*".$someArray[0]["photo_compte"];
  return $donnees;
}

function getAllAdministateur()
{
  $type_compte="Admin_globale";
  $req = $this->db->prepare("SELECT * FROM compte where type_compte=? order by datecrea_compte desc  ");
   $req->execute([$type_compte]);
   return $req->fetchAll();
}

function existAdmin($login,$email)
{
  $type_cpte="Admin_globale";
  $req = $this->db->prepare("SELECT * FROM compte where type_compte=? and  (login_compte=? or email_compte=?)");
  $req->execute([$type_cpte,$login,$email]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;

}

function Addminwithfile($nomad,$prenomad,$datenaisad,$contactad,$emailad,$fonctionad,$loginad,$passad,$type_cpte,$statut,$datecrea,$fichierad)
{



$req = $this->db->prepare("INSERT INTO compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=?,login_compte=?,pass_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?,photo_compte=?");
  $req->execute([
    $nomad,
    $prenomad,
    $datenaisad,
    $contactad,
    $emailad,
    $fonctionad,
    $loginad,
    $passad,
    $type_cpte,
    $statut,
    $datecrea,
    $fichierad
]);

$_SESSION['user']['addadminok']="Admin Globale ajouté avec succès";

if($_SESSION['user']['profile'] == "Admin_globale") {

    header("Location:../manager/addAdmin.php");

  }else if($_SESSION['user']['profile'] == "Admin_locale") {

      header("Location:../locale/addAdmin.php");

    }else if($_SESSION['user']['profile'] == "Teatcher") {

          header("Location:../teatcher/index.php");

      }else if($_SESSION['user']['profile'] == "Student") {

            header("Location:../student/index.php");

        }else if($_SESSION['user']['profile'] == "Parent") {

              header("Location:../parent/index.php");

          }

 //header("Location:../manager/addAdmin.php");


}

function Addminwithoutfile($nomad,$prenomad,$datenaisad,$contactad,$emailad,$fonctionad,$loginad,$passad,$type_cpte,$statut,$datecrea)
{


  $req=$this->db->prepare("INSERT INTO compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=?,login_compte=?,pass_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?");
  $req->execute([
    $nomad,
    $prenomad,
    $datenaisad,
    $contactad,
    $emailad,
    $fonctionad,
    $loginad,
    $passad,
    $type_cpte,
    $statut,
    $datecrea

]);

$_SESSION['user']['addadminok']="Admin Globale ajouté avec succès";

if($_SESSION['user']['profile'] == "Admin_globale") {

    header("Location:../manager/addAdmin.php");

  }else if($_SESSION['user']['profile'] == "Admin_locale") {

      header("Location:../locale/addAdmin.php");

    }else if($_SESSION['user']['profile'] == "Teatcher") {

          header("Location:../teatcher/index.php");

      }else if($_SESSION['user']['profile'] == "Student") {

            header("Location:../student/index.php");

        }else if($_SESSION['user']['profile'] == "Parent") {

              header("Location:../parent/index.php");

          }

 //header("Location:../manager/addAdmin.php");
}

function getAllAdmin()
{
  $type_compte="Admin_local";
  $req = $this->db->prepare("SELECT * FROM compte where type_compte=? order by datecrea_compte desc  ");
   $req->execute([$type_compte]);
   return $req->fetchAll();
}


}
  ?>
