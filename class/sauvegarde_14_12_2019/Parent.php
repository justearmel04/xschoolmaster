<?php
class ParentX{

public $db;
function __construct() {
  require_once('../class/cnx.php');

  $db = new mysqlConnector();
  $this->db= $db->dataBase;
}

function AddParentBoucle($nomTea,$prenomTea,$sexeTea,$contactTea,$type_cpte,$codeEtab,$sessionEtab,$statut,$datecrea)
{
  $req1 = $this->db->prepare("INSERT INTO  compte SET nom_compte=?,prenom_compte=?,tel_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?");
  $req1->execute([
    $nomTea,
    $prenomTea,
    $contactTea,
    $type_cpte,
    $statut,
    $datecrea
  ]);

  $idlastcompte=$this->db->lastInsertId();

  //insertion dans la table parent

  $req = $this->db->prepare("INSERT INTO  parent SET nom_parent=?,prenom_parent=?,tel_parent=?,statut_parent=?,sexe_parent=?,idcompte_parent=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $contactTea,
    $statut,
    $sexeTea,
    $idlastcompte
  ]);

  //insertion dans la table enregistrer

    $reqY = $this->db->prepare("INSERT INTO  enregistrer SET idparent_enreg=?,codeEtab_enreg=?");
    $reqY->execute([
      $idlastcompte,
      $codeEtab
    ]);
}

function determineParentOfStudentbymobilephone($phonenumber,$codeEtab)
{
  $type_cpte="Parent";
  $req = $this->db->prepare("SELECT * from compte,enregistrer where  compte.id_compte=enregistrer.idparent_enreg and  tel_compte=? and enregistrer.codeEtab_enreg=? and compte.type_compte=?");
  $req->execute([$phonenumber,$codeEtab,$type_cpte]);
  return $req->fetchAll();
}

 function SendSouscriptionMailler()
{
  require_once('../PHPMailer/class.phpmailer.php');
  require_once('../PHPMailer/class.smtp.php');
  require_once('../controller/functions.php');
  $libelleEtab="Important";

  $client1="justearmel04@gmail.com";
  $client2="fabienekoute@gmail.com";
  $mail = new PHPMailer();
  $mail->isHTML(true);
  $mail->CharSet="UTF-8";
  $mail->isSMTP();
  $mail->SMTPOptions = array (
 'ssl' => array(
 'verify_peer'  => false,
 'verify_peer_name'  => false,
 'allow_self_signed' => true));
 $mail->Host='mail.proximity-cm.com';
 $mail->SMTPAuth = true;
 $mail->Port = 25;
 $mail->SMTPSecure = "tls";
 $mail->Username = "xschool@proximity-cm.com";
 $mail->Password ="123psa@456";
 $mail->From='xschool@proximity-cm.com';
 $mail->FromName=$libelleEtab;
 $mail->AddAddress($client1);
 $mail->AddAddress($client2);
 $mail->Subject ="Paiement Souscription";
 $mail->Body = "Bonjour Cher Administrateur<br>";
 $mail->Body .="Un paiement à été effectuer par un parent et reste en attente de validation <br>";
 $mail->Body .="Merci de vous connecter à la plateforme afin de traiter le paiement <br>";
 $mail->Body .="Cordialement<br>";
 $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
 $mail->AddEmbeddedImage("../assets/img/logo/x2bis.png");

 if(!$mail->Send())

 {

 $msg="nok";

 }else
 {

 $msg="ok";

 }

return $msg;

}

function AddParentPaiement($datecrea,$idparentcpte,$studentIdentarray,$montanttotale,$selectmobileop,$transacId,$paymentno,$statutpaie,$nbselect,$subscribeIdarray,$devisecontry)
{
  $req = $this->db->prepare("INSERT INTO  paiementab SET date_paiab=?,parentid_paiab=?,studentsid_paiab=?,montant_paiab=?,operateur_paiab=?,transacid_paiab=?,number_paiab=?,statut_paiab=?,nbstudent_paiab=?,subscribes_paiab=?,devises_paiab=?");
  $req->execute([
  $datecrea,
  $idparentcpte,
  $studentIdentarray,
  $montanttotale,
  $selectmobileop,
  $transacId,
  $paymentno,
  $statutpaie,
  $nbselect,
  $subscribeIdarray,
  $devisecontry
  ]);
}

function AddBySchool($idparentcpte,$codeEtab)
{
  $req = $this->db->prepare("INSERT INTO  enregistrer SET idparent_enreg=?,codeEtab_enreg=?");
  $req->execute([
    $idparentcpte,
    $codeEtab
  ]);
}

function AddParentSubscribe($nomTea,$prenomTea,$telTea,$fonctionTea,$cniTea,$emailTea,$datenaisTea,$loginTea,$passTea,$type_cpte,$statut,$datecrea,$sexeTea)
{
  $req1 = $this->db->prepare("INSERT INTO  compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=?,login_compte=?,pass_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?");
  $req1->execute([
    $nomTea,
    $prenomTea,
    $datenaisTea,
    $telTea,
    $emailTea,
    $fonctionTea,
    $loginTea,
    $passTea,
    $type_cpte,
    $statut,
    $datecrea

  ]);

    $idlastcompte=$this->db->lastInsertId();

  $req = $this->db->prepare("INSERT INTO  parent SET nom_parent=?,prenom_parent=?,tel_parent=?,profession_parent=?,cni_parent=?,email_parent=?,statut_parent=?,sexe_parent=?,idcompte_parent=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $telTea,
    $fonctionTea,
    $cniTea,
    $emailTea,
    $statut,
    $sexeTea,
    $idlastcompte
  ]);

  return $idlastcompte;

}

function LoginExiste($login)
{
    $req = $this->db->prepare("SELECT * from compte where login_compte=?");
    $req->execute([$login]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;
}

function getAllParentSchool($codeEtabLocal)
{
  $req = $this->db->prepare("SELECT * from parent,enregistrer,compte where parent.idcompte_parent=compte.id_compte and parent.idcompte_parent=enregistrer.idparent_enreg and enregistrer.codeEtab_enreg=?");
  $req->execute([$codeEtabLocal]);
   return $req->fetchAll();
}

function getStudentCurrentlyinscription($IdCompte,$ideleve)
{
  $req = $this->db->prepare("SELECT * from eleve,parent,parenter,inscription,classe,etablissement where   parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and classe.session_classe=inscription.session_inscrip and  inscription.idclasse_inscrip=classe.id_classe and etablissement.code_etab=classe.codeEtab_classe and parent.idcompte_parent=? and eleve.idcompte_eleve=? order by id_inscrip DESC limit 1  ");
  $req->execute([$IdCompte,$ideleve]);
   return $req->fetchAll();
}

function getDifferentStudentByParentId($IdCompte)
{
    $req = $this->db->prepare("SELECT * from eleve,parent,parenter,compte where compte.id_compte=eleve.idcompte_eleve and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and parent.idcompte_parent=? ");
    $req->execute([$IdCompte]);
    return $req->fetchAll();
}

function getParentOfStudentsSchools($codeEtab,$libellesessionencours)
{
    $req = $this->db->prepare("SELECT  distinct  compte.id_compte,compte.nom_compte,compte.prenom_compte,compte.tel_compte,compte.email_compte from compte,parent,parenter,enregistrer,eleve,inscription where parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.codeEtab_inscrip=enregistrer.codeEtab_enreg and   compte.id_compte=parent.idcompte_parent and compte.id_compte=enregistrer.idparent_enreg and enregistrer.codeEtab_enreg=? and inscription.session_inscrip=? ");
    $req->execute([$codeEtab,$libellesessionencours]);
    return $req->fetchAll();

}

function getAllstudentofthisclassesParentSchool($classeid,$codeEtab,$parentid)
{
  $session="2019-2020";
  $req = $this->db->prepare("SELECT * from eleve,parenter,classe,inscription where parenter.eleveid_parenter=eleve.idcompte_eleve and  inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=eleve.idcompte_eleve and classe.id_classe=? and classe.codeEtab_classe=? and inscription.session_inscrip=? and parenter.parentid_parenter=?");
  $req->execute([$classeid,$codeEtab,$session,$parentid]);
  return $req->fetchAll();
}

function getstudentofthisclassesSchoolParent($classeid,$codeEtab,$student,$parentid)
{
  $session="2019-2020";
  $req = $this->db->prepare("SELECT * from eleve,parenter,classe,inscription where parenter.eleveid_parenter=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=eleve.idcompte_eleve and classe.id_classe=? and classe.codeEtab_classe=? and eleve.idcompte_eleve=? and inscription.session_inscrip=? and parenter.parentid_parenter=?");
  $req->execute([$classeid,$codeEtab,$student,$session,$parentid]);
  return $req->fetchAll();
}

function getTheSpecificMatiereForSchool($codeEtab,$classeid,$matiereid)
{
  $req = $this->db->prepare("SELECT * from programme,classe,matiere where programme.idclasse_prog=classe.id_classe and programme.idmat_prog=matiere.id_mat and programme.codeEtab_prog=? and programme.idclasse_prog=? and programme.idmat_prog=? ");
  $req->execute([$codeEtab,$classeid,$matiereid]);
  return $req->fetchAll();
}

function getAllMatiereOfThisClasseSchool($codeEtab,$classeid)
{
  $req = $this->db->prepare("SELECT * from programme,classe,matiere where programme.idclasse_prog=classe.id_classe and programme.idmat_prog=matiere.id_mat and programme.codeEtab_prog=? and programme.idclasse_prog=? ");
  $req->execute([$codeEtab,$classeid]);
  return $req->fetchAll();
}

function getAllTeatcherOfThisClasseSchool($codeEtab,$classeid)
{
  $req = $this->db->prepare("SELECT distinct compte.id_compte,compte.nom_compte,compte.prenom_compte,enseignant.nat_enseignant,compte.tel_compte,compte.email_compte FROM compte,enseignant,dispenser where compte.id_compte=enseignant.idcompte_enseignant and dispenser.id_enseignant=enseignant.idcompte_enseignant and dispenser.idclasse_disp=? and dispenser.codeEtab=? ");
  $req->execute([$classeid,$codeEtab]);
  return $req->fetchAll();
}

function getTheSpecificTeatcherForSchool($codeEtab,$classeid,$teatcherid)
{
  $req = $this->db->prepare("SELECT distinct compte.id_compte,compte.nom_compte,compte.prenom_compte,enseignant.nat_enseignant,compte.tel_compte,compte.email_compte FROM compte,enseignant,dispenser where compte.id_compte=enseignant.idcompte_enseignant and dispenser.id_enseignant=enseignant.idcompte_enseignant and dispenser.idclasse_disp=? and dispenser.codeEtab=? and compte.id_compte=?");
  $req->execute([$classeid,$codeEtab,$teatcherid]);
  return $req->fetchAll();
}

function getAlletabOfStudentParent($IdCompte)
{
  $session="2019-2020";
  $req = $this->db->prepare("SELECT  distinct 	code_etab,libelle_etab FROM etablissement,classe,eleve,inscription,parent,parenter where parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and classe.codeEtab_classe=etablissement.code_etab and inscription.session_inscrip=? and parent.idcompte_parent=? ");
  $req->execute([$session,$IdCompte]);
  return $req->fetchAll();
}

function getAllclassesOfStudentParent($IdCompte)
{
  $session="2019-2020";
  $req = $this->db->prepare("SELECT  distinct 	libelle_classe,id_classe FROM classe,eleve,inscription,parent,parenter where parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=? and parent.idcompte_parent=? ");
  $req->execute([$session,$IdCompte]);
  return $req->fetchAll();
}

function getNamePreOfTheCompte($idcompte_parent)
{
    $req = $this->db->prepare("SELECT  * FROM compte,parent where parent.idcompte_parent=compte.id_compte and compte.id_compte=?");
    $req->execute([$idcompte_parent]);
    $data=$req->fetchAll();
    $array=json_encode($data,true);
    $someArray = json_decode($array, true);
    $nom=$someArray[0]["nom_parent"]." - ".$someArray[0]["prenom_parent"];

    return $nom;
}

function getParentInfosbyId($studentparentid)
{
  $req = $this->db->prepare("SELECT  * FROM compte,parent where compte.email_compte=parent.email_parent and compte.id_compte=?");
  $req->execute([$studentparentid]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);
  $donnees=$someArray[0]["nom_parent"]."*".$someArray[0]["prenom_parent"]."*".$someArray[0]["tel_parent"]."*".$someArray[0]["profession_parent"]."*".$someArray[0]["email_parent"]."*".$someArray[0]["datenais_compte"];
  $donnees.="*".$someArray[0]["sexe_parent"]."*".$someArray[0]["cni_parent"]."*".$someArray[0]["fonction_compte"]."*".$someArray[0]["login_compte"]."*".$someArray[0]["photo_compte"]."*".$someArray[0]["id_compte"];
  return $donnees;
}

function ParentInfostudent($studentparentid)
{
    $req = $this->db->prepare("SELECT  * FROM compte,parent,parenter where compte.id_compte=parent.idcompte_parent and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=?");
    $req->execute([$studentparentid]);
    return $req->fetchAll();
}

function getAllcniesbySchoolCode($codeEtabAssigner)
{
  $typecompte="Parent";
$req = $this->db->prepare("SELECT  compte.id_compte FROM compte,parent,etablissement,eleve,parenter where compte.id_compte=parent.idcompte_parent and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and etablissement.code_etab=eleve.codeEtab_eleve and eleve.codeEtab_eleve=? and compte.type_compte=?");
$req->execute([$codeEtabAssigner,$typecompte]);
return $req->fetchAll();
}

function getAllcnies()
{
  $typecompte="Parent";
$req = $this->db->prepare("SELECT  parent.cni_parent,compte.id_compte FROM compte,parent,etablissement,eleve,parenter where compte.email_compte=parent.email_parent and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and etablissement.code_etab=eleve.codeEtab_eleve and compte.type_compte=?");
$req->execute([$typecompte]);
return $req->fetchAll();
}

function getAllParentsNamebySchoolCode($codeEtabAssigner)
{
  $typecompte="Parent";
// $req = $this->db->prepare("SELECT  distinct parent.nom_parent,parent.prenom_parent,compte.id_compte FROM compte,parent,etablissement,eleve,parenter where compte.email_compte=parent.email_parent and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and etablissement.code_etab=eleve.codeEtab_eleve and eleve.codeEtab_eleve=? and compte.type_compte=?");
$req = $this->db->prepare("SELECT  distinct parent.nom_parent,parent.prenom_parent,compte.id_compte FROM compte,parent,etablissement,eleve,parenter where compte.id_compte=parent.idcompte_parent and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and etablissement.code_etab=eleve.codeEtab_eleve and eleve.codeEtab_eleve=? and compte.type_compte=?");
$req->execute([$codeEtabAssigner,$typecompte]);
return $req->fetchAll();
}

function getAllParentsName()
{
  $typecompte="Parent";
$req = $this->db->prepare("SELECT  parent.nom_parent,parent.prenom_parent,compte.id_compte FROM compte,parent,etablissement,eleve,parenter where compte.email_compte=parent.email_parent and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and etablissement.code_etab=eleve.codeEtab_eleve  and compte.type_compte=?");
$req->execute([$typecompte]);
return $req->fetchAll();
}

function Addoldparent($idcompte,$codeEtab)
{
  $reqY = $this->db->prepare("INSERT INTO  enregistrer SET idparent_enreg=?,codeEtab_enreg=?");
  $reqY->execute([
    $idcompte,
    $codeEtab
  ]);

$_SESSION['user']['addparok']="Parent ajouté avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/addparent.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {
header("Location:../locale/addparent.php");
  }



}

function getIdcompteParentByCniandEmail($cni,$emailTea,$codeEtab)
{
  $req = $this->db->prepare("SELECT  * FROM compte,parent,enregistrer where compte.email_compte=parent.email_parent and compte.id_compte=enregistrer.idparent_enreg and parent.cni_parent=? and compte.email_compte=? and enregistrer.codeEtab_enreg=? ");
  $req->execute([
    $cni,
    $emailTea,
    $codeEtab
  ]);

  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);
  $donnees=$someArray[0]["id_compte"];

  return $donnees;

}

function UpdateParent($nomTea,$prenomTea,$datenaisTea,$sexeTea,$contactTea,$fonction,$cni,$emailTea,$nationalite,$lieuH,$nbchild,$nbchildsco,$adrespro,$societe,$session,$codeEtab,$idcompte)
{
  $req = $this->db->prepare("UPDATE parent SET nom_parent=?,prenom_parent=?,tel_parent=?,cni_parent=?,email_parent=?,sexe_parent=?,nationalite_parent=?,lieuH_parent=?,nbchild_parent=?,nbchidsco_parent=?,adressepro_parent=?,societe_parent=?,profession_parent=?		 WHERE idcompte_parent=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $contactTea,
    $cni,
    $emailTea,
    $sexeTea,
    $nationalite,
    $lieuH,
    $nbchild,
    $nbchildsco,
    $adrespro,
    $societe,
    $fonction,
    $idcompte
  ]);

  $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=? WHERE id_compte=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $datenaisTea,
    $contactTea,
    $emailTea,
    $fonction,
    $idcompte
  ]);

  $_SESSION['user']['updateparentok']="Parent modifier avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/parents.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {
header("Location:../locale/parents.php");
  }

}


function UpdateParentwithfilePass($nomTea,$prenomTea,$datenaisTea,$sexeTea,$contactTea,$fonction,$cni,$emailTea,$loginTea,$passTea,$fichierad,$idcompte)
{
  $req = $this->db->prepare("UPDATE parent SET nom_parent=?,prenom_parent=?,tel_parent=?,cni_parent=?,email_parent=?,sexe_parent=? WHERE idcompte_parent=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $contactTea,
    $cni,
    $emailTea,
    $sexeTea,
    $idcompte
  ]);

  //modification dans la table compte

  $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,login_compte=?,pass_compte=?,photo_compte=? WHERE id_compte=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $datenaisTea,
    $contactTea,
    $emailTea,
    $loginTea,
    $passTea,
    $fichierad,
    $idcompte
  ]);

  $_SESSION['user']['updateparentok']="Parent modifier avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/parents.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {
header("Location:../locale/parents.php");
  }

   //header("Location:../manager/parents.php");

}

function UpdateParentwithfilePassNot($nomTea,$prenomTea,$datenaisTea,$sexeTea,$contactTea,$fonction,$cni,$emailTea,$loginTea,$fichierad,$idcompte)
{
  $req = $this->db->prepare("UPDATE parent SET nom_parent=?,prenom_parent=?,tel_parent=?,cni_parent=?,email_parent=?,sexe_parent=? WHERE idcompte_parent=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $contactTea,
    $cni,
    $emailTea,
    $sexeTea,
    $idcompte
  ]);

  //modification dans la table compte

  $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,login_compte=?,photo_compte=? WHERE id_compte=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $datenaisTea,
    $contactTea,
    $emailTea,
    $loginTea,
    $fichierad,
    $idcompte
  ]);

  $_SESSION['user']['updateparentok']="Parent modifier avec succès";
  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/parents.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {
header("Location:../locale/parents.php");
  }

  // header("Location:../manager/parents.php");
}


function UpdateParentwithOutfilePass($nomTea,$prenomTea,$datenaisTea,$sexeTea,$contactTea,$fonction,$cni,$emailTea,$loginTea,$passTea,$idcompte)
{
  $req = $this->db->prepare("UPDATE parent SET nom_parent=?,prenom_parent=?,tel_parent=?,cni_parent=?,email_parent=?,sexe_parent=? WHERE idcompte_parent=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $contactTea,
    $cni,
    $emailTea,
    $sexeTea,
    $idcompte
  ]);

  //modification dans la table compte

  $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,login_compte=?,pass_compte=? WHERE id_compte=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $datenaisTea,
    $contactTea,
    $emailTea,
    $loginTea,
    $passTea,
    $idcompte
  ]);

  $_SESSION['user']['updateparentok']="Parent modifier avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/parents.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {
header("Location:../locale/parents.php");
  }
}


function UpdateParentwithOutfilePassNot($nomTea,$prenomTea,$datenaisTea,$sexeTea,$contactTea,$fonction,$cni,$emailTea,$loginTea,$idcompte)
{
  $req = $this->db->prepare("UPDATE parent SET nom_parent=?,prenom_parent=?,tel_parent=?,email_parent=?,sexe_parent=? WHERE idcompte_parent=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $contactTea,
    $emailTea,
    $sexeTea,
    $idcompte
  ]);

  //modification dans la table compte

  $reqx = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,login_compte=? WHERE id_compte=?");
  $reqx->execute([
    $nomTea,
    $prenomTea,
    $datenaisTea,
    $contactTea,
    $emailTea,
    $loginTea,
    $idcompte
  ]);

  $_SESSION['user']['updateparentok']="Parent modifier avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/parents.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {
header("Location:../locale/parents.php");
  }
}

function UpdateConnexionInfos($loginTea,$passTea,$idparent)
{
  $req = $this->db->prepare("UPDATE compte SET login_compte=?,pass_compte=? where compte.id_compte=?");
  $req->execute([
    $loginTea,
    $passTea,
    $idparent
  ]);

  $_SESSION['user']['updateparentok']="Parent modifier avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/parents.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {
header("Location:../locale/parents.php");
  }

}

function UpdateParentAll($nomTea,$prenomTea,$datenaisTea,$lieunaisTea,$lieuH,$sexeTea,$natTea,$emailTea,$contactTea,$situationTea,$nbchildscoTea,$nbchildTea,$societe,$adresspro,$fonctionad,$idcompte)
{
  $req = $this->db->prepare("UPDATE parent SET nom_parent=?,prenom_parent=?,tel_parent=?,email_parent=?,sexe_parent=?,nationalite_parent=?,lieuH_parent=?,nbchild_parent=?,nbchidsco_parent=?,adressepro_parent=?,societe_parent=?,profession_parent=?		 WHERE idcompte_parent=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $contactTea,
    $emailTea,
    $sexeTea,
    $natTea,
    $lieuH,
    $nbchildTea,
    $nbchildscoTea,
    $adresspro,
    $societe,
    $fonctionad,
    $idcompte
  ]);

  $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=? WHERE id_compte=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $datenaisTea,
    $contactTea,
    $emailTea,
    $fonctionad,
    $idcompte
  ]);

  $_SESSION['user']['updateparentok']="Parent modifier avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/parents.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {
header("Location:../locale/parents.php");
  }
}

function getAllparentInfobyId($compte)
{
  $req = $this->db->prepare("SELECT  * FROM compte,parent where compte.email_compte=parent.email_parent and compte.id_compte=?");
  $req->execute([$compte]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);

  $donnees=$someArray[0]["nom_compte"]."*".$someArray[0]["prenom_compte"]."*".$someArray[0]["datenais_compte"]."*".$someArray[0]["sexe_parent"]."*".$someArray[0]["tel_compte"]."*".$someArray[0]["fonction_compte"];
  $donnees.="*".$someArray[0]["cni_parent"]."*".$someArray[0]["email_parent"]."*".$someArray[0]["login_compte"]."*".$someArray[0]["photo_compte"]."*".$someArray[0]["nationalite_parent"]."*".$someArray[0]["lieuH_parent"];
  $donnees.="*".$someArray[0]["nbchild_parent"]."*".$someArray[0]["nbchidsco_parent"]."*".$someArray[0]["adressepro_parent"]."*".$someArray[0]["societe_parent"]."*".$someArray[0]["situation_parent"]."*".$someArray[0]["lieunais_parent"];
  $donnees.="*".$someArray[0]["adressepro_parent"];
  return $donnees;
}

function getAllParent()
{
  $type_compte="Parent";
  // $req = $this->db->prepare("SELECT * FROM compte,parent where compte.email_compte=parent.email_parent and  type_compte=? order by datecrea_compte desc  ");
  $req = $this->db->prepare("SELECT * FROM compte,parent where compte.id_compte=parent.idcompte_parent and  type_compte=? order by datecrea_compte desc  ");
   $req->execute([$type_compte]);
   return $req->fetchAll();
}

function getSelectionParentById($idcompte)
{
  $type_compte="Parent";
  $req = $this->db->prepare("SELECT * FROM compte,parent where compte.id_compte=parent.idcompte_parent and  compte.type_compte=? and compte.id_compte=?");
   $req->execute([$type_compte,$idcompte]);
   return $req->fetchAll();
}

function Deleteparent($compte,$statut)
{
   $req = $this->db->prepare("UPDATE compte set statut_compte=? where 	id_compte=? ");
   $req->execute([$statut,$compte]);
   $_SESSION['user']['updateparentok']="Compte supprimer avec succès";
   if($_SESSION['user']['profile'] == "Admin_globale")
   {
 header("Location:../manager/parents.php");
   }else if($_SESSION['user']['profile'] == "Admin_locale")
   {
 header("Location:../locale/parents.php");
   }

}

function determineParentOfStudent($idcompte,$codeEtab)
{
   $req = $this->db->prepare("SELECT distinct id_compte,nom_compte,prenom_compte FROM compte,parent,eleve,parenter,inscription where compte.id_compte=parent.idcompte_parent and parenter.parentid_parenter=parent.idcompte_parent  and parenter.eleveid_parenter=eleve.idcompte_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and parenter.eleveid_parenter=? and inscription.codeEtab_inscrip=?  ");
   $req->execute([$idcompte,$codeEtab]);
   return $req->fetchAll();
}

function getNbofparametres($compteid)
{
  $req = $this->db->prepare("SELECT * FROM compte where compte.id_compte=? and compte.email_compte!='' and compte.login_compte!='' and compte.pass_compte!='' ");
  $req->execute([$compteid]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getAllParentBySchoolCode($codeEtabAssigner)
{
  $type_compte="Parent";
  // $req = $this->db->prepare("SELECT distinct id_compte,nom_compte,prenom_compte,fonction_compte,email_compte,tel_compte,photo_compte,tel_parent FROM compte,parent,enregistrer where compte.email_compte=parent.email_parent and compte.id_compte=enregistrer.idparent_enreg  and compte.statut_compte=1 and  compte.type_compte=? and enregistrer.codeEtab_enreg=?  ");
  //  $req->execute([$type_compte,$codeEtabAssigner]);
  $req = $this->db->prepare("SELECT distinct id_compte,nom_compte,prenom_compte,fonction_compte,email_compte,tel_compte,photo_compte,tel_parent FROM compte,parent,enregistrer where enregistrer.idparent_enreg=compte.id_compte and parent.idcompte_parent=compte.id_compte and compte.statut_compte=1 and  compte.type_compte=? and enregistrer.codeEtab_enreg=?  ");
   $req->execute([$type_compte,$codeEtabAssigner]);
   return $req->fetchAll();
}

function getSelectionParentBySchoolCode($codeEtab,$idcompte)
{
  $req = $this->db->prepare("SELECT * FROM parent,compte,enregistrer  where parent.email_parent=compte.email_compte and enregistrer.idparent_enreg=compte.id_compte and enregistrer.codeEtab_enreg=? and compte.id_compte=?");
  $req->execute([$codeEtab,$idcompte]);
  return $req->fetchAll();
}

function AddParent($nomTea,$prenomTea,$datenaisTea,$emailTea,$sexeTea,$contactTea,$fonction,$cni,$codeEtab,$nationalite,$lieuH,$nbchild,$nbchildsco,$adrespro,$societe,$session,$datecrea,$type_cpte,$statut)
{
  $req1 = $this->db->prepare("INSERT INTO  compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?");
  $req1->execute([
    $nomTea,
    $prenomTea,
    $datenaisTea,
    $contactTea,
    $emailTea,
    $fonction,
    $type_cpte,
    $statut,
    $datecrea
  ]);

  $idlastcompte=$this->db->lastInsertId();

  //insertion dans la table parent

  $req = $this->db->prepare("INSERT INTO  parent SET nom_parent=?,prenom_parent=?,tel_parent=?,profession_parent=?,cni_parent=?,email_parent=?,statut_parent=?,sexe_parent=?,idcompte_parent=?,nationalite_parent=?,lieuH_parent=?,nbchild_parent=?,nbchidsco_parent=?,adressepro_parent=?,societe_parent=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $contactTea,
    $fonction,
    $cni,
    $emailTea,
    $statut,
    $sexeTea,
    $idlastcompte,
    $nationalite,
    $lieuH,
    $nbchild,
    $nbchildsco,
    $adrespro,
    $societe,
  ]);

  //insertion dans la table enregistrer

    $reqY = $this->db->prepare("INSERT INTO  enregistrer SET idparent_enreg=?,codeEtab_enreg=?");
    $reqY->execute([
      $idlastcompte,
      $codeEtab
    ]);

    $_SESSION['user']['addparok']="Parent ajouté avec succès";

    if($_SESSION['user']['profile'] == "Admin_globale")
    {
  header("Location:../manager/addparent.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale")
    {
  header("Location:../locale/addparent.php");
    }


}

function AddParentwithfile($nomTea,$prenomTea,$contactTea,$fonction,$cni,$emailTea,$datenaisTea,$loginTea,$passTea,$type_cpte,$statut,$datecrea,$fichierad,$sexeTea,$codeEtab)
{
  require_once('../class/Sessionsacade.php');

  $session= new Sessionacade();

  $req1 = $this->db->prepare("INSERT INTO  compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=?,login_compte=?,pass_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?,photo_compte=?");
  $req1->execute([
    $nomTea,
    $prenomTea,
    $datenaisTea,
    $contactTea,
    $emailTea,
    $fonction,
    $loginTea,
    $passTea,
    $type_cpte,
    $statut,
    $datecrea,
    $fichierad

  ]);

  //recuperation de l'id compte du parent
  $idlastcompte=$this->db->lastInsertId();

  $req = $this->db->prepare("INSERT INTO  parent SET nom_parent=?,prenom_parent=?,tel_parent=?,profession_parent=?,cni_parent=?,email_parent=?,statut_parent=?,sexe_parent=?,idcompte_parent=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $contactTea,
    $fonction,
    $cni,
    $emailTea,
    $statut,
    $sexeTea,
    $idlastcompte

  ]);
  //insertion dans la table enregistrer

    $reqY = $this->db->prepare("INSERT INTO  enregistrer SET idparent_enreg=?,codeEtab_enreg=?");
    $reqY->execute([
      $idlastcompte,
      $codeEtab
    ]);

    //nous allons ajouter dans la table notificationstate

    //nous allons recuperer la session de cet etablissement

    // $sessionencours=$session->getSessionEncours($codeEtab);
    // $tabsessionencours=explode("*",$sessionencours);
    // $libellesessionencours=$tabsessionencours[0];
    //
    // $statusstate=1;
    // //ajoute dans la table
    //
    // $reqZ = $this->db->prepare("INSERT INTO  notificationstate SET idparent_state=?,session_state=?,status_state=?");
    // $reqZ->execute([
    // $idlastcompte,
    // $libellesessionencours,
    // $statusstate
    // ]);




  $_SESSION['user']['addparok']="Parent ajouté avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/addparent.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {
header("Location:../locale/addparent.php");
  }

   //header("Location:../manager/addparent.php");

}

function AddParentwithoutfile($nomTea,$prenomTea,$contactTea,$fonction,$cni,$emailTea,$datenaisTea,$loginTea,$passTea,$type_cpte,$statut,$datecrea,$sexeTea,$codeEtab)
{
  $req1 = $this->db->prepare("INSERT INTO  compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=?,login_compte=?,pass_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?");
  $req1->execute([
    $nomTea,
    $prenomTea,
    $datenaisTea,
    $contactTea,
    $emailTea,
    $fonction,
    $loginTea,
    $passTea,
    $type_cpte,
    $statut,
    $datecrea

  ]);

    $idlastcompte=$this->db->lastInsertId();

  $req = $this->db->prepare("INSERT INTO  parent SET nom_parent=?,prenom_parent=?,tel_parent=?,profession_parent=?,cni_parent=?,email_parent=?,statut_parent=?,sexe_parent=?,idcompte_parent=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $contactTea,
    $fonction,
    $cni,
    $emailTea,
    $statut,
    $sexeTea,
    $idlastcompte
  ]);


  //insertion dans la table enregistrer

    $reqY = $this->db->prepare("INSERT INTO  enregistrer SET idparent_enreg=?,codeEtab_enreg=?");
    $reqY->execute([
      $idlastcompte,
      $codeEtab
    ]);

  $_SESSION['user']['addparok']="Parent ajouté avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/addparent.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {
header("Location:../locale/addparent.php");
  }

   //header("Location:../manager/addparent.php");

}

function DetermineNameAndPrenomsParent($ideleve)
{
  $req = $this->db->prepare("SELECT * FROM eleve,parenter  where parenter.eleveid_parenter=eleve.idcompte_eleve and idcompte_eleve=?");
  $req->execute([$ideleve]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);
  $donnees=$someArray[0]["idparent_eleve"];
  return $donnees;
}

function ExistwithCniId($cni,$codeEtab)
{
  $req = $this->db->prepare("SELECT * FROM parent,compte,enregistrer where compte.email_compte=parent.email_parent and compte.id_compte=enregistrer.idparent_enreg and parent.cni_parent=? and enregistrer.codeEtab_enreg=?");
  $req->execute([$cni,$codeEtab]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function ExistParentwithMail($email)
{
  $type="Teacher";
  $req = $this->db->prepare("SELECT * FROM parent,compte where parent.email_parent=compte.email_compte and compte.type_compte=? and compte.email_compte=? ");
  $req->execute([$type,$email]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function parentcptemail($email)
{
  $req = $this->db->prepare("SELECT * FROM compte where compte.email_compte=? ");
  $req->execute([$email]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getContactOfParentById($id_compte)
{
  $req = $this->db->prepare("SELECT  tel_parent FROM parent where idcompte_parent=?");
  $req->execute([$id_compte]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);

  $donnees=$someArray[0]["tel_parent"];
  return $donnees;
}


}
 ?>
