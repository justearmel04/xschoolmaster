<?php
// require '../src/ci/Osms.php';
// use \Osms\Osms;
class Etab{

  public $db;
  function __construct() {
      require_once('../class/cnx.php');
      require_once('../controller/functions.php');
      require_once('../class/Matiere.php');
      require_once('../class/User.php');

    $db = new mysqlConnector();
    $this->db= $db->dataBase;
      }

      function getAlltransportationSession($codeEtabLocal,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM transportations where codeEtab_trans=? and sessionEtab_trans=? ");
        $req->execute([$codeEtabLocal,$libellesessionencours]);
        $data=$req->fetchAll();
        return $data;
      }

      function UpdateTransportations($montfrais,$devisefrais,$transid,$typefrais,$codeEtab,$sessionEtab)
      {
        $req = $this->db->prepare("UPDATE transportations SET frais_trans=?,devises_trans=? where id_trans=? and type_trans=? and codeEtab_trans=? and sessionEtab_trans=?");
        $req->execute([$montfrais,$devisefrais,$transid,$typefrais,$codeEtab,$sessionEtab]);
      }

      function AddMensualTransportation($fraismensuel,$devises,$codeEtab,$sessionEtab,$mensualtype,$idcompte,$dateday)
      {
        $req = $this->db->prepare("INSERT INTO transportations SET type_trans=?,frais_trans=?,devises_trans=?,addby_trans=?,datecrea_trans=?,codeEtab_trans=?,sessionEtab_trans=? ");
        $req->execute([$mensualtype,$fraismensuel,$devises,$idcompte,$dateday,$codeEtab,$sessionEtab]);
      }

      function AddTrimestrialTransportation($fraistrimes,$devises,$codeEtab,$sessionEtab,$trimestrialtype,$idcompte,$dateday)
      {
        $req = $this->db->prepare("INSERT INTO transportations SET type_trans=?,frais_trans=?,devises_trans=?,addby_trans=?,datecrea_trans=?,codeEtab_trans=?,sessionEtab_trans=? ");
        $req->execute([$trimestrialtype,$fraistrimes,$devises,$idcompte,$dateday,$codeEtab,$sessionEtab]);
      }

      function AddannualTransportation($fraisannuel,$devises,$codeEtab,$sessionEtab,$annualtype,$idcompte,$dateday)
      {
        $req = $this->db->prepare("INSERT INTO transportations SET type_trans=?,frais_trans=?,devises_trans=?,addby_trans=?,datecrea_trans=?,codeEtab_trans=?,sessionEtab_trans=? ");
        $req->execute([$annualtype,$fraisannuel,$devises,$idcompte,$dateday,$codeEtab,$sessionEtab]);
      }

      function UpdateStudentRanking($raking,$mention,$idrating,$studentid,$classeId,$matiereid,$teatcherid,$codeEtab,$sessionlibelle,$typesessionNotes)
      {
        $req = $this->db->prepare("UPDATE rating SET raking_rating=?,mention_rating=? where id_rating=? and ideleve_rating=? and classe_rating=? and matiereid_rating=? and idprof_rating=? and codeEtab_rating=? and session_rating=? and typsession_rating=?");
        $req->execute([$raking,$mention,$idrating,$studentid,$classeId,$matiereid,$teatcherid,$codeEtab,$sessionlibelle,$typesessionNotes]);
      }

      function UpdateMentionRatingStudent($mention,$idrating,$studentid,$matiere,$semester)
      {
        $req = $this->db->prepare("UPDATE rating SET mention_rating=? where id_rating=? and ideleve_rating=? and matiereid_rating=? and typsession_rating=?");
          $req->execute([$mention,$idrating,$studentid,$matiere,$semester]);
      }

      function getratinggeneraleStudent($classeid,$codeEtabAssigner,$libellesessionencours,$compteid,$semester)
      {
          $req = $this->db->prepare("SELECT * FROM generalerating where classe_glerating=? and codeEtab_glerating=? and session_glerating=? and ideleve_glerating=? and typesesion_glerating=?");
          $req->execute([$classeid,$codeEtabAssigner,$libellesessionencours,$compteid,$semester]);
          $data=$req->fetchAll();
          return $data;
      }


      function RecalculstudentRang($classeId,$matiereid,$teatcherid,$codeEtab,$sessionlibelle,$typesessionNotes)
      {
        $etabs=new Etab();
        $req = $this->db->prepare("SELECT * FROM rating where classe_rating=? and matiereid_rating=? and idprof_rating=? and codeEtab_rating=? and session_rating=? and typsession_rating=? order by rating DESC");
        $req->execute([$classeId,$matiereid,$teatcherid,$codeEtab,$sessionlibelle,$typesessionNotes]);
        $data=$req->fetchAll();
        $i=1;
        foreach ($data as $value):
          $studentid=$value->ideleve_rating;
          $idrating=$value->id_rating;
          $rating=$value->rating;
          $mention=$etabs->getratingMention($rating);
          $raking=$i;
          $etabs->UpdateStudentRanking($raking,$mention,$idrating,$studentid,$classeId,$matiereid,$teatcherid,$codeEtab,$sessionlibelle,$typesessionNotes);
          $i++;
        endforeach;

      }

      function getratingMention($rating)
      {
        $mention="";
        if($rating<5)
        {
          $mention="Faible";
        }else if($rating>5 && $rating<10 )
        {
          $mention="Insuffisant";
        } else if($rating>=10 && $rating<12 )
        {
          $mention="Passable";
        }else if($rating>=12 && $rating<14 )
        {
          $mention="Assez bien";
        }else if($rating>=14 && $rating<16 )
        {
          $mention="Bien";
        }else if($rating>=16 && $rating<17.5 )
        {
          $mention="Très bien";
        }else if($rating>18)
        {
          $mention="Excellent";
        }

        return $mention;

      }

      function UpdateStudentRankinggle($raking,$mention,$idrating,$studentid,$classeId,$codeEtab,$sessionlibelle,$typesessionNotes)
      {
        $req = $this->db->prepare("UPDATE generalerating SET ranking_glerating=?,mention_glerating=? where 	id_glerating=? and ideleve_glerating=? and classe_glerating=? and codeEtab_glerating=? and session_glerating=? and typesesion_glerating=?");
        $req->execute([$raking,$mention,$idrating,$studentid,$classeId,$codeEtab,$sessionlibelle,$typesessionNotes]);
      }

      function RecalculstudentRanggle($classeId,$codeEtab,$sessionlibelle,$typesessionNotes)
      {
        $etabs=new Etab();
        $req = $this->db->prepare("SELECT * FROM generalerating where classe_glerating=? and codeEtab_glerating=? and session_glerating=? and typesesion_glerating=? order by rating_glerating DESC");
        $req->execute([$classeId,$codeEtab,$sessionlibelle,$typesessionNotes]);
        $data=$req->fetchAll();
        $i=1;
        foreach ($data as $value):
          $studentid=$value->ideleve_glerating;
          $idrating=$value->id_glerating;
          $rating=$value->rating_glerating;
          $mention=$etabs->getratingMention($rating);
          $raking=$i;
          $etabs->UpdateStudentRankinggle($raking,$mention,$idrating,$studentid,$classeId,$codeEtab,$sessionlibelle,$typesessionNotes);
          $i++;
        endforeach;

      }

      function getNbpaiementperiode($codeEtab,$sessionEtab,$start,$end)
      {
        $req = $this->db->prepare("SELECT * FROM versement where codeEtab_versement=? and session_versement=? and motif_versement IN('SCOLARITES','INSCRIPTIONS') and date_versement BETWEEN  ? and ?");
        $req->execute([$codeEtab,$sessionEtab,$start,$end]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function getNbpaiementperiodestudents($id_compte,$codeEtab,$sessionEtab,$first,$last)
      {
        $req = $this->db->prepare("SELECT * FROM versement where 	ideleve_versement=? and  codeEtab_versement=? and session_versement=? and motif_versement IN('SCOLARITES','INSCRIPTIONS') and date_versement BETWEEN  ? and ?");
        $req->execute([$id_compte,$codeEtab,$sessionEtab,$first,$last]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function getgleratingNumber($classeid,$codeEtab,$sessionEtab,$compteid,$semesterid)
      {
          $req = $this->db->prepare("SELECT * FROM generalerating where classe_glerating=? and codeEtab_glerating=? and session_glerating=? and ideleve_glerating=? and typesesion_glerating=?");
          $req->execute([$classeid,$codeEtab,$sessionEtab,$compteid,$semesterid]);
          $data=$req->fetchAll();
          $nb=count($data);
          return $nb;
      }

      function getMountpaiementperiode($codeEtab,$sessionEtab,$start,$end)
      {
        $req = $this->db->prepare("SELECT SUM(montant_versement) as somme FROM versement where codeEtab_versement=? and session_versement=? and motif_versement IN('SCOLARITES','INSCRIPTIONS') and date_versement BETWEEN  ? and ?");
        $req->execute([$codeEtab,$sessionEtab,$start,$end]);
        $data=$req->fetchAll();
        foreach ($data as $value):
          $montant=$value->somme;
        endforeach;

        return $montant;
      }

      function getLastpaiementStudentperiode($id_compte,$codeEtab,$sessionEtab,$first,$last)
      {
        $req = $this->db->prepare("SELECT solde_versement FROM versement where 	ideleve_versement=? and codeEtab_versement=? and session_versement=? and motif_versement IN('SCOLARITES','INSCRIPTIONS') and date_versement BETWEEN  ? and ? order by id_versement DESC limit 1");
        $req->execute([$id_compte,$codeEtab,$sessionEtab,$first,$last]);
        $data=$req->fetchAll();
        foreach ($data as $value):
          $montant=$value->solde_versement;
        endforeach;

        return $montant;

      }

      function getMonthRevenustandbyOld($codeEtab,$sessionEtab)
      {
        $etabs=new Etab();
        $user=new User();
        $req = $this->db->prepare("SELECT * FROM sessions where sessions.codeEtab_sess=? and sessions.libelle_sess=?");
        $req->execute([$codeEtab,$sessionEtab]);
        $data=$req->fetchAll();
        foreach ($data as $value):
          $start    = new DateTime($value->datedeb_sess);
          $start->modify('first day of this month');
          $end      = new DateTime($value->datefin_sess);
          $end->modify('first day of next month');
          $interval = DateInterval::createFromDateString('1 month');
          $period   = new DatePeriod($start, $interval, $end);
        endforeach;



        $montantstandby="";

        foreach ($period as $dt) {

          //nous allons recuperer l'ensemble des mois pour lesquels nous avons eu des paiements

          $first=$dt->format("Y-m-")."01";
          $last=date("Y-m-t", strtotime($first));

          // la liste de l'ensemble des eleves inscrits cette année

          $nb=$etabs->getNbpaiementperiode($codeEtab,$sessionEtab,$first,$last);
          $sommemontant=0;

          if($nb>0)
          {
            $allstudents=$user->getNbofStudentforThisschoolSessionList($codeEtab,$sessionEtab);

            foreach ($allstudents as $values):
              //nous allons compter le nombre de versement pour cet eleve au cours de la periode
                $nbpaies=$etabs->getNbpaiementperiodestudents($values->id_compte,$codeEtab,$sessionEtab,$first,$last);
                if($nbpaies>0)
                {
                  //nous recuperons le dernier montant a payer
                  $montant=$etabs->getLastpaiementStudentperiode($values->id_compte,$codeEtab,$sessionEtab,$first,$last);
                  $sommemontant=$sommemontant+$montant;

                }

            endforeach;
          }

          $montantstandby=$montantstandby.$sommemontant.",";





        }

        return substr($montantstandby, 0, -1);

    }


      function getMonthRevenustandby($codeEtab,$sessionEtab)
      {
        $etabs=new Etab();
        $user=new User();
        $req = $this->db->prepare("SELECT * FROM sessions where sessions.codeEtab_sess=? and sessions.libelle_sess=?");
        $req->execute([$codeEtab,$sessionEtab]);
        $data=$req->fetchAll();
        foreach ($data as $value):
          $start    = new DateTime($value->datedeb_sess);
          $start->modify('first day of this month');
          $end      = new DateTime($value->datefin_sess);
          $end->modify('first day of next month');
          $interval = DateInterval::createFromDateString('1 month');
          $period   = new DatePeriod($start, $interval, $end);
        endforeach;



        $montantstandby="";

        foreach ($period as $dt) {

          //nous allons recuperer l'ensemble des mois pour lesquels nous avons eu des paiements

          $first=$dt->format("Y-m-")."01";
          $last=date("Y-m-t", strtotime($first));

          // la liste de l'ensemble des eleves inscrits cette année

          $nb=$etabs->getNbpaiementperiode($codeEtab,$sessionEtab,$first,$last);


          if($nb>0)
          {
            $allstudents=$user->getNbofStudentforThisschoolSessionList($codeEtab,$sessionEtab);
            $sommemontant=0;
            foreach ($allstudents as $values):
              //nous allons compter le nombre de versement pour cet eleve au cours de la periode
                $nbpaies=$etabs->getNbpaiementperiodestudents($values->id_compte,$codeEtab,$sessionEtab,$first,$last);
                if($nbpaies>0)
                {
                  //nous recuperons le dernier montant a payer
                  $montant=$etabs->getLastpaiementStudentperiode($values->id_compte,$codeEtab,$sessionEtab,$first,$last);
                  $sommemontant=$sommemontant+$montant;

                }

            endforeach;

              $montantstandby=$montantstandby.$sommemontant.",";
          }







        }

        return substr($montantstandby, 0, -1);

    }

      function getMonthRevenureceive($codeEtab,$sessionEtab)
      {
        $etabs=new Etab();
          $req = $this->db->prepare("SELECT * FROM sessions where sessions.codeEtab_sess=? and sessions.libelle_sess=?");
          $req->execute([$codeEtab,$sessionEtab]);
          $data=$req->fetchAll();
          foreach ($data as $value):
            $start    = new DateTime($value->datedeb_sess);
            $start->modify('first day of this month');
            $end      = new DateTime($value->datefin_sess);
            $end->modify('first day of next month');
            $interval = DateInterval::createFromDateString('1 month');
            $period   = new DatePeriod($start, $interval, $end);
          endforeach;



          $montantreceive="";
          foreach ($period as $dt) {

            //nous allons recuperer l'ensemble des mois pour lesquels nous avons eu des paiements

            $first=$dt->format("Y-m-")."01";
            $last=date("Y-m-t", strtotime($first));

            $nb=$etabs->getNbpaiementperiode($codeEtab,$sessionEtab,$first,$last);

            if($nb>0)
            {
              $montant=$etabs->getMountpaiementperiode($codeEtab,$sessionEtab,$first,$last);
              $montantreceive=$montantreceive.$montant.",";
            }else {
              // $montant=0;
              // $montantreceive=$montantreceive.$montant.",";
            }


          }

          return substr($montantreceive, 0, -1);

      }

      function getMonthRevenu($codeEtab,$sessionEtab)
      {
        $etabs=new Etab();
          $req = $this->db->prepare("SELECT * FROM sessions where sessions.codeEtab_sess=? and sessions.libelle_sess=?");
          $req->execute([$codeEtab,$sessionEtab]);
          $data=$req->fetchAll();
          foreach ($data as $value):
            $start    = new DateTime($value->datedeb_sess);
            $start->modify('first day of this month');
            $end      = new DateTime($value->datefin_sess);
            $end->modify('first day of next month');
            $interval = DateInterval::createFromDateString('1 month');
            $period   = new DatePeriod($start, $interval, $end);
          endforeach;



          $dataLibmonth="";
          $montantreceive="";
          foreach ($period as $dt) {

            //nous allons recuperer l'ensemble des mois pour lesquels nous avons eu des paiements

            $first=$dt->format("Y-m-")."01";
            $last=date("Y-m-t", strtotime($first));

            $nb=$etabs->getNbpaiementperiode($codeEtab,$sessionEtab,$first,$last);

            if($nb>0)
            {
              $libellemois=obtenirLibelleMois($dt->format('m'));
              $dataLibmonth=$dataLibmonth."\"$libellemois\"".",";
              $montant=$etabs->getMountpaiementperiode($codeEtab,$sessionEtab,$first,$last);
              $montantreceive=$montantreceive.$montant.",";
            }else {
              // $montant=0;
              // $montantreceive=$montantreceive.$montant.",";
            }


          }

          return substr($dataLibmonth, 0, -1);

      }

        function getstudentratingTrimestreNew($compteid,$semesterid,$classeid,$codeEtab,$sessionEtab)
        {
            $matiere=new Matiere();
            $etabs=new Etab();
            $datas=$etabs->getAllsubjectofclassesbyIdclasses($classeid,$codeEtab,$sessionEtab);
            $sommecoef=0;
            $sommemoycoef=0;
            $sommemoy=0;
            foreach ($datas as $value):
              $sommecoef=$sommecoef+$value->coef_mat;
              $data=$etabs->getratingChildInMatiere($classeid,$codeEtab,$sessionEtab,$compteid,$semesterid,$value->id_mat);
              if(count($data)>0)
              {
                  foreach ($data as $valuedata):
                    $sommemoy=$sommemoy+$valuedata->rating;
                    $sommemoycoef=$sommemoycoef+($valuedata->rating*$value->coef_mat);
                  endforeach;
              }else {
                $sommemoy=$sommemoy+0;
                $sommemoycoef=$sommemoycoef+0;
              }

            endforeach;

            $moyenne=$sommemoycoef/$sommecoef;

            //nous allons mettre a jour dans la table generale rating

            $nbglerating=$etabs->getgleratingNumber($classeid,$codeEtab,$sessionEtab,$compteid,$semesterid);
            if($nbglerating>0)
            {
              $etabs->UpdategleratingStudent($sommemoycoef,$sommecoef,$moyenne,$classeid,$codeEtab,$sessionEtab,$compteid,$semesterid);
            }else {
              $etabs->AddgleratingStudent($sommemoycoef,$sommecoef,$moyenne,$classeid,$codeEtab,$sessionEtab,$compteid,$semesterid);
            }

            $etabs->RecalculstudentRanggle($classeid,$codeEtab,$sessionEtab,$semesterid);

            return $moyenne;


        }

      function getstudentratingTrimestre($compteid,$semesterid,$classeid,$codeEtab,$sessionEtab)
      {
        $matiere=new Matiere();
        $req = $this->db->prepare("SELECT * FROM rating where classe_rating=? and codeEtab_rating=? and session_rating=? and typsession_rating=? and ideleve_rating=? ");
        $req->execute([$classeid,$codeEtab,$sessionEtab,$semesterid,$compteid]);
        $data=$req->fetchAll();
        $sommecoef=0;
        $sommemoycoef=0;
        $sommemoy=0;

        foreach ($data as $value):
          $sommemoy=$sommemoy+$value->rating;
          $sommecoef=$sommecoef+$matiere->getMatcoef($value->matiereid_rating,$codeEtab,$sessionEtab);
          $sommemoycoef=$sommemoycoef+($value->rating*$matiere->getMatcoef($value->matiereid_rating,$codeEtab,$sessionEtab));

        endforeach;

        $moyenne=$sommemoycoef/$sommecoef;

        $nbglerating=$etabs->getgleratingNumber($classeid,$codeEtab,$sessionEtab,$compteid,$semesterid);
        if($nbglerating>0)
        {
          $etabs->UpdategleratingStudent($sommemoycoef,$sommecoef,$moyenne,$classeid,$codeEtab,$sessionEtab,$compteid,$semesterid);
        }else {
          $etabs->AddgleratingStudent($sommemoycoef,$sommecoef,$moyenne,$classeid,$codeEtab,$sessionEtab,$compteid,$semesterid);
        }

        $etabs->RecalculstudentRanggle($classeid,$codeEtab,$sessionEtab,$semesterid);

        return $moyenne;


      }

      function AddgleratingStudent($sommemoycoef,$sommecoef,$moyenne,$classeid,$codeEtab,$sessionEtab,$compteid,$semesterid)
      {
          $req = $this->db->prepare("INSERT INTO generalerating SET totalmoycoef_glerating=?,totalcoef_glerating=?,rating_glerating=?,classe_glerating=?,codeEtab_glerating=?,session_glerating=?,ideleve_glerating=?,typesesion_glerating=? ");
          $req->execute([$sommemoycoef,$sommecoef,$moyenne,$classeid,$codeEtab,$sessionEtab,$compteid,$semesterid]);
      }

      function UpdategleratingStudent($sommemoycoef,$sommecoef,$moyenne,$classeid,$codeEtab,$sessionEtab,$compteid,$semesterid)
      {
          $req = $this->db->prepare("UPDATE  generalerating SET totalmoycoef_glerating=?,totalcoef_glerating=?,rating_glerating=? where classe_glerating=? and codeEtab_glerating=? and session_glerating=? and ideleve_glerating=? and typesesion_glerating=?");
          $req->execute([$sommemoycoef,$sommecoef,$moyenne,$classeid,$codeEtab,$sessionEtab,$compteid,$semesterid]);
      }

      function getratingChildInMatiere($classeid,$codeEtab,$sessionEtab,$compteid,$semesterid,$matiereid)
      {
        $req = $this->db->prepare("SELECT * FROM rating where classe_rating=? and codeEtab_rating=? and session_rating=? and typsession_rating=? and 	ideleve_rating=? and matiereid_rating=? ");
        $req->execute([$classeid,$codeEtab,$sessionEtab,$semesterid,$compteid,$matiereid]);
        return $req->fetchAll();
      }

      function getratingNumberofthisclasse($classeEtab,$codeEtab,$sessionEtab,$trimestre)
      {
        $req = $this->db->prepare("SELECT * FROM rating where classe_rating=? and codeEtab_rating=? and session_rating=? and typsession_rating=? ");
        $req->execute([$classeEtab,$codeEtab,$sessionEtab,$trimestre]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function getnumberOfwomenchild($codeEtab,$sessionEtab)
      {
        $req = $this->db->prepare("SELECT * FROM compte,eleve,inscription where compte.id_compte=eleve.idcompte_eleve and compte.id_compte=inscription.ideleve_inscrip and inscription.codeEtab_inscrip=? and inscription.session_inscrip=? and eleve.sexe_eleve='F' ");
        $req->execute([$codeEtab,$sessionEtab]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function getnumberOfmenchild($codeEtab,$sessionEtab)
      {
        $req = $this->db->prepare("SELECT * FROM compte,eleve,inscription where compte.id_compte=eleve.idcompte_eleve and compte.id_compte=inscription.ideleve_inscrip and inscription.codeEtab_inscrip=? and inscription.session_inscrip=? and eleve.sexe_eleve='M' ");
        $req->execute([$codeEtab,$sessionEtab]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function getListeAttendanceLast($matricule,$classe,$codeEtab,$session)
      {
               $etabs=new Etab();
      	 $req = $this->db->prepare("SELECT * FROM presences,heure where presences.libelleheure_presence=heure.id_heure and  matricule_presence=? and classe_presence=? and codeEtab_presence=? and session_presence=? and statut_presence=0 order by date_presence DESC limit 10 ");
               $req->execute([$matricule,$classe,$codeEtab,$session]);
               return $req->fetchAll();


      }

      function getPropositionsOfQuestions($questid)
      {
         $req = $this->db->prepare("SELECT * FROM question,propositionrep where question.id_quest=propositionrep.idquest_proprep and question.id_quest=?");
         $req->execute([$questid]);
         return $req->fetchAll();
      }

      function ChangePropositionFalseToTrueByQuestionId($questionid)
      {
         $req = $this->db->prepare("UPDATE propositionrep SET libelle_proprep='VRAI' where 	libelle_proprep='FAUX' and idquest_proprep=?");
         $req->execute([$questionid]);
      }

      function ChangePropositionTrueToFalseByQuestionId($questionid)
      {
        $req = $this->db->prepare("UPDATE propositionrep SET libelle_proprep='FAUX' where 	libelle_proprep='VRAI' and idquest_proprep=?");
        $req->execute([$questionid]);
      }

      function UpdateQuizInfos($libellequiz,$classeEtab,$codeEtab,$sessionEtab,$durationquiz,$instructionquiz,$datelimite,$matiereid,$teatcherid,$verouillerquiz,$idquiz)
      {
        $req = $this->db->prepare("UPDATE quiz SET libelle_quiz=?,duree_quiz=?,instruction_quiz=?,datelimite_quiz=?,matiere_quiz=?,verouiller_quiz=?,classe_quiz=? where teatcher_quiz=? and codeEtab_quiz=? and sessionEtab_quiz=? and 	id_quiz=?  ");
        $req->execute([$libellequiz,$durationquiz,$instructionquiz,$datelimite,$matiereid,$verouillerquiz,$classeEtab,$teatcherid,$codeEtab,$sessionEtab,$idquiz]);
      }

      function UpadtepropositionVal($valeurproposition,$propositionid,$questionid)
      {
        $req = $this->db->prepare("UPDATE propositionrep SET valeur_proprep=? where id_proprep=? and idquest_proprep=?");
        $req->execute([$valeurproposition,$propositionid,$questionid]);
      }

      function ChangePropositionTrueToFalse($libelle,$valeurproposition,$propositionid,$questionid)
      {
         $req = $this->db->prepare("UPDATE propositionrep SET libelle_proprep=?,valeur_proprep=? where 	id_proprep=? and idquest_proprep=?");
         $req->execute([$libelle,$valeurproposition,$propositionid,$questionid]);
      }

      function getPropositionsOfQuestionsolution($questid)
      {
        $req = $this->db->prepare("SELECT * FROM question,propositionrep where question.id_quest=propositionrep.idquest_proprep and question.id_quest=? and propositionrep.valeur_proprep=1");
        $req->execute([$questid]);
        return $req->fetchAll();
      }

      function getCodeEtabOfStudentInscript($compteuserid)
      {
         $req = $this->db->prepare("SELECT * from inscription where inscription.ideleve_inscrip=? order by inscription.id_inscrip desc limit 1");
         $req->execute([$compteuserid]);
         return $req->fetchAll();
      }

      function getListeAttendanceLastTeatcher($matricule,$classe,$codeEtab,$session,$teatcherid)
      {
               $etabs=new Etab();
         $req = $this->db->prepare("SELECT * FROM presences,heure where presences.libelleheure_presence=heure.id_heure and  matricule_presence=? and classe_presence=? and codeEtab_presence=? and session_presence=? and presences.teatcher_presence=? and statut_presence=0 order by date_presence DESC limit 10 ");
               $req->execute([$matricule,$classe,$codeEtab,$session,$teatcherid]);
               return $req->fetchAll();


      }

      function getAllMedicalesAnteOfChild($compteid)
      {
              $req = $this->db->prepare("SELECT * FROM antecedents,desease where desease.id_desease=antecedents.libante_ante and studentid_ante=?");
              $req->execute([$compteid]);
              return $req->fetchAll();
      }

      function getAllMedicalesAllergiesOfChild($compteid)
      {
              $req = $this->db->prepare("SELECT allergies_medical FROM medicalform where studentid_medical=? ");
              $req->execute([$compteid]);
              return $req->fetchAll();
      }

      function getAllMedicalesInfantDiseaseOfChild($compteid)
      {
              $req = $this->db->prepare("SELECT deseases_medical FROM medicalform where studentid_medical=? ");
              $req->execute([$compteid]);
              return $req->fetchAll();
      }

      function getAllInscriptionVersmentChild($codeEtabAssigner,$libellesessionencours,$compteid,$classeid)
      {
        $motif="INSCRIPTIONS";
        $req = $this->db->prepare("SELECT * FROM versement where codeEtab_versement=? and session_versement=? and ideleve_versement=? and classe_versement=? and motif_versement=? ");
        $req->execute([$codeEtabAssigner,$libellesessionencours,$compteid,$classeid,$motif]);
        return $req->fetchAll();
      }

      function getAllScolaritesVersmentChild($codeEtabAssigner,$libellesessionencours,$compteid,$classeid)
      {
        $motif="SCOLARITES";
        $req = $this->db->prepare("SELECT * FROM versement where codeEtab_versement=? and session_versement=? and ideleve_versement=? and classe_versement=? and motif_versement=? order by versement.id_versement DESC ");
        $req->execute([$codeEtabAssigner,$libellesessionencours,$compteid,$classeid,$motif]);
        return $req->fetchAll();
      }

      function getAllCantinesVersmentChild($codeEtabAssigner,$libellesessionencours,$compteid,$classeid)
      {
        $motif="CANTINES";
        $req = $this->db->prepare("SELECT * FROM versement where codeEtab_versement=? and session_versement=? and ideleve_versement=? and classe_versement=? and motif_versement=? ");
        $req->execute([$codeEtabAssigner,$libellesessionencours,$compteid,$classeid,$motif]);
        return $req->fetchAll();
      }

      function getAllAesVersmentChild($codeEtabAssigner,$libellesessionencours,$compteid,$classeid)
      {
        $motif="AES";
        $req = $this->db->prepare("SELECT * FROM versement where codeEtab_versement=? and session_versement=? and ideleve_versement=? and classe_versement=? and motif_versement=? ");
        $req->execute([$codeEtabAssigner,$libellesessionencours,$compteid,$classeid,$motif]);
        return $req->fetchAll();
      }

      function getFormMedicalInfosChild($compteid)
      {
        $req = $this->db->prepare("SELECT * FROM medicalform where studentid_medical=? ");
        $req->execute([$compteid]);
        return $req->fetchAll();
      }

      function getLibelleOfDiseaseById($id)
      {
          $req = $this->db->prepare("SELECT libelle_desease FROM desease where 	id_desease=? ");
          $req->execute([$id]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          return $someArray[0]["libelle_desease"];
      }


      function getCantineOption($codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement where code_etab=?");
        $req->execute([$codeEtab]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["cantine_etab"];
        return $donnees;
      }

      function getPropositionTrueidofQuestion($questionid)
      {
        $req = $this->db->prepare("SELECT * FROM propositionrep where idquest_proprep=? and libelle_proprep='VRAI'");
        $req->execute([$questionid]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["id_proprep"];
        return $donnees;
      }

      function getPropositionofQuestion($questionid)
      {
        $req = $this->db->prepare("SELECT * FROM propositionrep where idquest_proprep=?");
        $req->execute([$questionid]);
        $data=$req->fetchAll();
        return $data;
      }

      function getPropositionFalseidofQuestion($questionid)
      {
        $req = $this->db->prepare("SELECT * FROM propositionrep where idquest_proprep=? and libelle_proprep='FAUX'");
        $req->execute([$questionid]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["id_proprep"];
        return $donnees;
      }

      function getMatiereLibelleByIdMat($matiereid,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM matiere where id_mat=? and codeEtab_mat=?");
        $req->execute([$matiereid,$codeEtab]);
        $data=$req->fetchAll();

         $array=json_encode($data,true);
         $someArray = json_decode($array, true);

         $donnees=$someArray[0]["libelle_mat"];
           return $donnees;
      }


      function getUtilisateurName($idcompte)
      {
        $req = $this->db->prepare("SELECT nom_compte,prenom_compte from compte where compte.id_compte=? ");
        $req->execute([$idcompte]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["nom_compte"]." ".$someArray[0]["prenom_compte"];
        return $donnees;
      }

      function getUtilisateurSexe($idcompte)
      {
        $req = $this->db->prepare("SELECT sexe_compte from compte where compte.id_compte=? ");
        $req->execute([$idcompte]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["sexe_compte"];
        return $donnees;
      }

      function getallselectedlibidbase($codeEtab,$sessionEtab)
      {
        $req = $this->db->prepare("SELECT * from heure,heurelib where heure.idlib_heure=heurelib.id_heurelib and codeEtab_heurelib=? and session_heurelib=?");
        $req->execute([$codeEtab,$sessionEtab]);
        $data=$req->fetchAll();
        $nb=count($data);
        $retour="";
        if($nb>0)
        {
          foreach ($data as $value):
            $retour=$retour.$value->id_heure.",";
          endforeach;
        }

        return $retour;
      }

      function getRoutineDaysHourswithoutold($codeEtab,$sessionEtab,$day,$hoursLibroutine)
      {
          $req = $this->db->prepare("SELECT DISTINCT libelleHeure_route	  from routine where  etab_route=? and session_route=? and routine.day_route=? and routine.libelleHeure_route NOT IN(?) ");
          $req->execute([$codeEtab,$sessionEtab,$day,$hoursLibroutine]);
          $data=$req->fetchAll();
          $nb=count($data);
          $retour="";
          if($nb>0)
          {
            foreach ($data as $value):
              if($value->libelleHeure_route==$hoursLibroutine)
              {

              }else {
                $retour=$retour.$value->libelleHeure_route.",";
              }


            endforeach;
          }

          return $retour;
      }

      function getRoutineInfos($idroutine,$codeEtab,$sessionEtab,$classeid)
      {
          $req = $this->db->prepare("SELECT * from routine,heure,heurelib where routine.libelleHeure_route=heure.id_heure and heure.idlib_heure=heurelib.id_heurelib and routine.id_route=? and routine.etab_route=? and routine.session_route=? and routine.classe_route=? ");
          $req->execute([$idroutine,$codeEtab,$sessionEtab,$classeid]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["heuredeb_heure"]."*".$someArray[0]["heurefin_heure"];

          return $donnees;

      }

      function getRoutineDaysHours($codeEtab,$sessionEtab,$day)
      {

            $req = $this->db->prepare("SELECT DISTINCT libelleHeure_route	  from routine where  etab_route=? and session_route=? and routine.day_route=? ");
            $req->execute([$codeEtab,$sessionEtab,$day]);
            $data=$req->fetchAll();
            $nb=count($data);
            $retour="";
            if($nb>0)
            {
              foreach ($data as $value):
                $retour=$retour.$value->libelleHeure_route.",";

              endforeach;
            }

          return $retour;
      }

      function getLibelleHeureAll($codeEtab,$sessionEtab)
      {
        // $req = $this->db->prepare("SELECT * from heure,heurelib where heure.idlib_heure=heurelib.id_heurelib and codeEtab_heurelib=? and session_heurelib=?");
          // $req = $this->db->prepare("SELECT * from heurelib,heure where heure.idlib_heure=heurelib.id_heurelib and  codeEtab_heurelib=? and session_heurelib=?");
  $req = $this->db->prepare("SELECT * from heurelib where   codeEtab_heurelib=? and session_heurelib=?");
        $req->execute([$codeEtab,$sessionEtab]);
        return $req->fetchAll();
      }

      function getalotofHoursLibelle($codeEtab,$session,$datasids)
      {
            // $req = $this->db->prepare("SELECT * from heure,heurelib where heure.idlib_heure=heurelib.id_heurelib and codeEtab_heurelib=? and session_heurelib=? and id_heurelib NOT IN(?)");
            $req = $this->db->prepare("SELECT * from heure,heurelib where heure.idlib_heure=heurelib.id_heurelib and codeEtab_heurelib=? and session_heurelib=? and id_heure NOT IN(?)");
            //  $req = $this->db->prepare("SELECT * from heure where and codeEtab_heurelib=? and session_heurelib=? and id_heure NOT IN(?)");
            $req->execute([$codeEtab,$session,$datasids]);
            return $req->fetchAll();
      }

      function getselectionHeureAll($codeEtab,$sessionEtab)
      {
        $req = $this->db->prepare("SELECT * from heure,heurelib where heure.idlib_heure=heurelib.id_heurelib and codeEtab_heurelib=? and session_heurelib=?");
      $req->execute([$codeEtab,$sessionEtab]);
      return $req->fetchAll();
      }

      function getHoursAllLibs($codeEtab,$sessionEtab)
      {
         $req = $this->db->prepare("SELECT * from heure,heurelib where heure.idlib_heure=heurelib.id_heurelib and codeEtab_heurelib=? and session_heurelib=?");
        $req->execute([$codeEtab,$sessionEtab]);
        return $req->fetchAll();
      }

      function getListehoursofcourseMat($classe,$codeEtab,$session,$matiereid,$libellejour)
      {
         // $req = $this->db->prepare("SELECT distinct id_heure,id_heurelib,libelle_heurelib from heure,heurelib,routine where heure.idlib_heure=heurelib.id_heurelib and heure.idlib_heure=routine.libelleHeure_route and routine.classe_route=? and routine.etab_route=? and routine.session_route=? and routine.matiere_route=? and routine.day_route=? ");
         $req = $this->db->prepare("SELECT distinct id_heure,id_heurelib,libelle_heurelib from heure,heurelib,routine where heure.idlib_heure=heurelib.id_heurelib and heure.id_heure=routine.libelleHeure_route and routine.classe_route=? and routine.etab_route=? and routine.session_route=? and routine.matiere_route=? and routine.day_route=? ");
         $req->execute([$classe,$codeEtab,$session,$matiereid,$libellejour]);
         return $req->fetchAll();
      }

      function getListehoursofcourseMatDiff($classe,$codeEtab,$session,$matiereid,$libellejour,$allidsLibs)
      {
         // $req = $this->db->prepare("SELECT distinct id_heure,id_heurelib,libelle_heurelib from heure,heurelib,routine where heure.idlib_heure=heurelib.id_heurelib and heure.idlib_heure=routine.libelleHeure_route and routine.classe_route=? and routine.etab_route=? and routine.session_route=? and routine.matiere_route=? and routine.day_route=? and heure.idlib_heure NOT IN(?)  ");
          $req = $this->db->prepare("SELECT distinct id_heure,id_heurelib,libelle_heurelib from heure,heurelib,routine where heure.idlib_heure=heurelib.id_heurelib and heure.id_heure=routine.libelleHeure_route and routine.classe_route=? and routine.etab_route=? and routine.session_route=? and routine.matiere_route=? and routine.day_route=? and heure.id_heure NOT IN(?)  ");
         $req->execute([$classe,$codeEtab,$session,$matiereid,$libellejour,$allidsLibs]);
         return $req->fetchAll();
      }


      function getLibelleHeure($codeEtab,$session,$selectlibidbase)
      {
          // $req = $this->db->prepare("SELECT * from heurelib,heure where heure.idlib_heure=heurelib.id_heurelib and  codeEtab_heurelib=? and session_heurelib=? and id_heurelib NOT IN(?) order by id_heurelib ASC");
  $req = $this->db->prepare("SELECT * from heurelib where   codeEtab_heurelib=? and session_heurelib=? and id_heurelib NOT IN(?) order by id_heurelib ASC");
          $req->execute([$codeEtab,$session,$selectlibidbase]);
          return $req->fetchAll();
      }

      function getLibelleHeureSelecteurH($codeEtab,$session,$selectlibidbase)
      {
        $req = $this->db->prepare("SELECT * from heure,heurelib where heure.idlib_heure=heurelib.id_heurelib and codeEtab_heurelib=? and session_heurelib=? and id_heurelib NOT IN(?) order by id_heurelib ASC");
        $req->execute([$codeEtab,$session,$selectlibidbase]);
        return $req->fetchAll();

      }

      function getpresenceslibhoursnbdatechoice($datechoice,$classe,$matiereid,$profid,$codeEtab,$session)
      {
        $req = $this->db->prepare("SELECT distinct libelleheure_presence from presences where date_presence=? and classe_presence=? and matiere_presence=? and teatcher_presence=? and codeEtab_presence=? and session_presence=?");
        $req->execute([$datechoice,$classe,$matiereid,$profid,$codeEtab,$session]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function getNbofsoumisquiz($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
        $req = $this->db->prepare("SELECT * from reponsequiz where 	idquiz_repquiz=? and classeid_repquiz=? and codeEtab_repquiz=? and sessionEtab_repquiz=? and studentid_repquiz=?");
        $req->execute([$courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function getpresenceslibhoursnbdatechoiceAndHours($datechoice,$classe,$matiereid,$profid,$codeEtab,$session,$HeureLibEtab)
      {
        $req = $this->db->prepare("SELECT distinct libelleheure_presence from presences where date_presence=? and classe_presence=? and matiere_presence=? and teatcher_presence=? and codeEtab_presence=? and session_presence=? and libelleheure_presence=?");
        $req->execute([$datechoice,$classe,$matiereid,$profid,$codeEtab,$session,$HeureLibEtab]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function getallpresencesLibsIds($datechoice,$classe,$matiereid,$profid,$codeEtab,$session)
      {
        $req = $this->db->prepare("SELECT distinct libelleheure_presence from presences where date_presence=? and classe_presence=? and matiere_presence=? and teatcher_presence=? and codeEtab_presence=? and session_presence=?");
        $req->execute([$datechoice,$classe,$matiereid,$profid,$codeEtab,$session]);
        $data=$req->fetchAll();
        $nb=count($data);
        $retour="";
        if($nb>0)
        {
          foreach ($data as $value):
            $retour=$retour.$value->libelleheure_presence .",";

          endforeach;
        }

      return $retour;
      }

      function getselecteurHeurebyLibelle($codeEtab,$session,$libelleheure)
      {
         // $req = $this->db->prepare("SELECT * from heure,heurelib where heure.idlib_heure=heurelib.id_heurelib and codeEtab_heurelib=? and session_heurelib=? and idlib_heure=? ");
            $req = $this->db->prepare("SELECT * from heure,heurelib where heure.idlib_heure=heurelib.id_heurelib and codeEtab_heurelib=? and session_heurelib=? and id_heure=? ");
         $req->execute([$codeEtab,$session,$libelleheure]);
         $data=$req->fetchAll();
         $array=json_encode($data,true);
         $someArray = json_decode($array, true);
         $donnees=$someArray[0]["heuredeb_heure"]."*".$someArray[0]["heurefin_heure"]."*".$someArray[0]["id_heure"];
         return $donnees;

      }

      function AddHoursLibelle($libelleheure,$heuredeb,$heurefin)
      {
          $req = $this->db->prepare("INSERT INTO heure set idlib_heure=?,heuredeb_heure=?,heurefin_heure=?");
          $req->execute([
          $libelleheure,$heuredeb,$heurefin
          ]);
      }

      function Addsupports($courseid,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$classeEtab,$fichierad,$typesupport)
      {
        $req = $this->db->prepare("INSERT INTO supports set courseid_support=?,matiereid_support=?,teatcherid_support=?,codeEtab_support=?,sessionEtab_support=?,classeid_support=?,fichier_support=?,type_support=?");
        $req->execute([
        $courseid,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$classeEtab,$fichierad,$typesupport
        ]);
      }


      function AddcommentdiscussionOnlineStudent($courseid,$teatcherid,$studentid,$classeid,$matiereid,$codeEtab,$sessionEtab)
      {
        $req = $this->db->prepare("INSERT INTO onlinecommentcourse set courseid_onlinecom=?,teatcher_onlinecom=?,studentid_onlinecom=?,classe_onlinecom=?,matiere_onlinecom=?,codeEtab_onlinecom=?,sessionEtab_onlinecom=?");
        $req->execute([
        $courseid,$teatcherid,$studentid,$classeid,$matiereid,$codeEtab,$sessionEtab
        ]);
      }

      function LibellematiereExiste($matiere,$codeEtab,$session)
      {
        $req = $this->db->prepare("SELECT * from matierelib where libelle_matlib=? and codeEtab_matlib=? and session_matlib=?  ");
        $req->execute([$matiere,$codeEtab,$session]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function getNBcoursescommentsStudent($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
        $req = $this->db->prepare("SELECT * from onlinecommentcourse where courseid_onlinecom=? and classe_onlinecom=? and codeEtab_onlinecom=? and sessionEtab_onlinecom=? and studentid_onlinecom=?  ");
        $req->execute([$courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function ExisteHoursallready($libelleheure,$session,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * from heurelib where libelle_heurelib=? and codeEtab_heurelib=? and session_heurelib=? ");
        $req->execute([$libelleheure,$codeEtab,$session]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function AddHeureLib($libelleheure,$codeEtab,$libellesession)
      {
        $req = $this->db->prepare("INSERT INTO  heurelib SET 	libelle_heurelib=?,	codeEtab_heurelib=?,session_heurelib=?");
          $req->execute([
          $libelleheure,$codeEtab,$libellesession
          ]);

      }

      function getAllmatierelibellesEtab($libellesessionencours,$codeEtabLocal)
      {
        $req = $this->db->prepare("SELECT * FROM matierelib where  session_matlib=? and codeEtab_matlib=?");
        $req->execute([$libellesessionencours,$codeEtabLocal]);
        return $req->fetchAll();
      }

      function getNotecontrolevalidation($controleid,$eleveid,$matiereid)
      {
        $req = $this->db->prepare("SELECT * from notes where notes.type_notes=1 and notes.idtype_notes=? and notes.ideleve_notes=? and notes.idmat_notes=?");
        $req->execute([$controleid,$eleveid,$matiereid]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["valeur_notes"];
        return $donnees;
      }

      function getHoursinfos($codeEtab,$sessionEtab,$heureid)
      {
          $req = $this->db->prepare("SELECT * from heure,heurelib where heure.idlib_heure=heurelib.id_heurelib and heurelib.codeEtab_heurelib=? and  heurelib.session_heurelib=? and  heure.id_heure=?");
          $req->execute([$codeEtab,$sessionEtab,$heureid]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["libelle_heurelib"]."*".$someArray[0]["heuredeb_heure"]."*".$someArray[0]["heurefin_heure"];
          return $donnees;

      }

      function getNoteidcontrolevalidation($controleid,$eleveid,$matiereid)
      {
        $req = $this->db->prepare("SELECT * from notes where notes.type_notes=1 and notes.idtype_notes=? and notes.ideleve_notes=? and notes.idmat_notes=?");
        $req->execute([$controleid,$eleveid,$matiereid]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["id_notes"];
        return $donnees;
      }

      function getNotecontrolevalidationNb($controleid,$eleveid,$matiereid)
      {
        $req = $this->db->prepare("SELECT * from notes where notes.type_notes=1 and notes.idtype_notes=? and notes.ideleve_notes=? and notes.idmat_notes=?");
        $req->execute([$controleid,$eleveid,$matiereid]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function getNoteexamenvalidationNb($examenid,$eleveid,$matiereid)
      {
        $req = $this->db->prepare("SELECT * from notes where notes.type_notes=2 and notes.idtype_notes=? and notes.ideleve_notes=? and notes.idmat_notes=?");
        $req->execute([$examenid,$eleveid,$matiereid]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }
      function getNoteexamenvalidation($examenid,$eleveid,$matiereid)
      {
        $req = $this->db->prepare("SELECT * from notes where notes.type_notes=2 and notes.idtype_notes=? and notes.ideleve_notes=? and notes.idmat_notes=?");
        $req->execute([$examenid,$eleveid,$matiereid]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["valeur_notes"];
        return $donnees;
      }

      function getNoteidexamenvalidation($examenid,$eleveid,$matiereid)
      {
        $req = $this->db->prepare("SELECT * from notes where notes.type_notes=2 and notes.idtype_notes=? and notes.ideleve_notes=? and notes.idmat_notes=?");
        $req->execute([$examenid,$eleveid,$matiereid]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["id_notes"];
        return $donnees;
      }

      function getcontroleLibelle($controleid,$codeEtab)
      {
          $req = $this->db->prepare("SELECT distinct controle.libelle_ctrl from controle where id_ctrl=? and codeEtab_ctrl=?");
          $req->execute([$controleid,$codeEtab]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["libelle_ctrl"];
          return $donnees;

      }

      function getpaysidofschool($codeEtab)
      {
          $req = $this->db->prepare("SELECT * from etablissement where 	code_etab=?");
          $req->execute([$codeEtab]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["pays_etab"];
          return $donnees;
      }

      function getcontroleclassesession($classeid,$matiereid,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * from controle where classe_ctrl=? and mat_ctrl=? and codeEtab_ctrl=?");
        $req->execute([$classeid,$matiereid,$codeEtab]);
        return $req->fetchAll();
      }

      function getexamenLibelle($examenid,$codeEtab)
      {
        $req = $this->db->prepare("SELECT distinct examen.libelle_exam from examen where id_exam=? and codeEtab_exam=?");
        $req->execute([$examenid,$codeEtab]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["libelle_exam"];
        return $donnees;
      }

      function getstandbymodificationNotes($codeEtabAssigner)
      {
        $req = $this->db->prepare("SELECT DISTINCT notesmodification.idpiste_modifnote,notesmodification.designationid_modifnote,notesmodification.controle_modifnote,notesmodification.examen_modifnote,piste.codeEtab_piste,notesmodification.matiereid_modifnote,piste.user_piste FROM notesmodification,piste where  notesmodification.idpiste_modifnote=piste.id_piste and  notesmodification.statut_modifnote=0 and piste.codeEtab_piste=?");
        $req->execute([$codeEtabAssigner]);
        return $req->fetchAll();
      }

      function getstandbymodificationNotesbyId($validationid,$codeEtabAssigner)
      {
        $req = $this->db->prepare("SELECT * from notesmodification,piste where  notesmodification.idpiste_modifnote=piste.id_piste and  notesmodification.statut_modifnote=0 and notesmodification.idpiste_modifnote=? and piste.codeEtab_piste=?");
        $req->execute([$validationid,$codeEtabAssigner]);
        return $req->fetchAll();
      }


      function DetermineNumberOfcontrolenote($controleid,$classeid,$matiereid,$codeEtab,$session)
      {
        $req = $this->db->prepare("SELECT * FROM notes where type_notes=1 and idtype_notes=? and 	idclasse_notes=? and 	idmat_notes=? and codeEtab_notes=? and session_notes=?");
        $req->execute([$controleid,$classeid,$matiereid,$codeEtab,$session]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }


      function SendSouscriptionMailler()
      {

        if($_SESSION['user']['paysid']==4)
        {

          require_once('../../PHPMailer/class.phpmailer.php');
          require_once('../../PHPMailer/class.smtp.php');
          require_once('../../controller/functions.php');
        }else {
          require_once('../PHPMailer/class.phpmailer.php');
          require_once('../PHPMailer/class.smtp.php');
          require_once('../controller/functions.php');
        }

        $client1="justearmel04@gmail.com";
        $client2="fabienekoute@gmail.com";
        $mail = new PHPMailer();
        $mail->isHTML(true);
         $mail->CharSet="UTF-8";
         $mail->isSMTP();
         $mail->SMTPOptions = array (
         'ssl' => array(
         'verify_peer'  => false,
         'verify_peer_name'  => false,
         'allow_self_signed' => true));
         $mail->Host='mail.proximity-cm.com';
         $mail->SMTPAuth = true;
         $mail->Port = 25;
         $mail->SMTPSecure = "tls";
         $mail->Username = "xschool@proximity-cm.com";
         $mail->Password ="123psa@456";
         $mail->From='xschool@proximity-cm.com';
         $mail->FromName='noreplay@proximity-cm.com';
         $mail->AddAddress($client1);
         $mail->AddAddress($client2);
         $mail->Subject = 'Paiement Mobile';
         $mail->Body = "Bonjour Cher Administrateur <br>";
         $mail->Body .="Un paiement mobile viens d'être initier sur la plateforme Xschool à cet effet nous vous prions de bien vouloir vous connectez afin de traiter le paiement<br>";
         $mail->Body .="Cordialement<br>";
         $mail->Body .="<br>";
         $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
         $mail->AddEmbeddedImage("../assets/img/logo/logo2.png","mon_logo");
         if(!$mail->Send())

         {

            //echo $mail->ErrorInfo;

            //Affiche le message d'erreur (ATTENTION:voir section 7)

         $msg="nok";

         }

         else

         {

         $msg="ok";

         }

         return $msg;
        }

        function getteatchernotinlist($dataprofaffecterid,$codeEtabAssigner,$libellesessionencours)
        {
          $req = $this->db->prepare("SELECT * FROM enseignant,enseigner WHERE enseignant.idcompte_enseignant=enseigner.id_enseignant AND enseigner.codeEtab=? AND enseigner.session_enseigner=? AND enseigner.id_classe=0 and enseigner.id_enseignant not in (?) ");
          $req->execute([$codeEtabAssigner,$libellesessionencours,$dataprofaffecterid]);
          return $req->fetchAll();
        }

        function AffectedEnseignant($codeEtab,$classeid,$enseignantid,$session)
        {
          $req1= $this->db->prepare("UPDATE  enseigner set id_classe=? where codeEtab=? and id_enseignant=? and session_enseigner=?");
          $req1->execute([
          $classeid,
          $codeEtab,
          $enseignantid,
          $session
          ]);



        }

        function getnumberofstudentparenter($parentid)
        {
            $encours=1;
            $req = $this->db->prepare("SELECT * FROM parenter,inscription where parenter.eleveid_parenter=inscription.ideleve_inscrip and session_inscrip=(SELECT distinct libelle_sess from sessions,etablissement where sessions.codeEtab_sess=etablissement.code_etab and encours_sess=? ) and parentid_parenter=?");
            $req->execute([$encours,$parentid]);
            $data=$req->fetchAll();
            $nb=count($data);
            return $nb;
        }

        function getAllteatchersaffectedscolaryyears($codeEtabAssigner,$libellesessionencours)
        {
            $req = $this->db->prepare("SELECT * FROM enseignant,enseigner WHERE enseignant.idcompte_enseignant=enseigner.id_enseignant AND enseigner.codeEtab=? AND enseigner.session_enseigner=? AND enseigner.id_classe!=0 ");
            $req->execute([$codeEtabAssigner,$libellesessionencours]);
            return $req->fetchAll();
        }

      function Updatenotificationstatesingle($parentid,$studentid,$sessionEtab,$status)
      {
        $req1= $this->db->prepare("UPDATE  notificationstate set status_state=? where idparent_state=? and student_state=? and session_state=?");
        $req1->execute([
        $status,
        $parentid,
        $studentid,
        $sessionEtab
        ]);
      }

      function Updatenotificationstateboth($parentid,$studentid,$sessionEtab,$status)
      {
        $req1= $this->db->prepare("UPDATE  notificationstate set status_state=?,session_state=? where idparent_state=? and student_state=?");
        $req1->execute([
        $status,
        $sessionEtab,
        $parentid,
        $studentid
        ]);
      }

      function getNbOfnotifstateparentsession($parentid,$studentid,$sessionEtab)
      {
        $req = $this->db->prepare("SELECT * FROM  notificationstate  where idparent_state=? and student_state=? and session_state=?");
        $req->execute([$parentid,$studentid,$sessionEtab]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function getTearcherPrimaire($classe,$session,$codeEtab,$typecompte)
      {
          $req = $this->db->prepare("SELECT * FROM  enseignant,enseigner where enseignant.idcompte_enseignant=enseigner.id_enseignant and enseigner.id_classe=? and enseigner.session_enseigner=? and enseigner.codeEtab=? and enseignant.type_enseignant=? ");
          $req->execute([$classe,$session,$codeEtab,$typecompte]);
          return $req->fetchAll();
      }

      function AddNotificationState($parentid,$studentid,$sessionEtab,$status)
      {
        $req = $this->db->prepare("INSERT INTO  notificationstate SET 	idparent_state=?,student_state=?,session_state=?,status_state=?");
          $req->execute([
          $parentid,
          $studentid,
          $sessionEtab,
          $status
          ]);
      }

      function getNbOfnotifstateparent($parentid,$studentid)
      {
        $req = $this->db->prepare("SELECT * FROM  notificationstate  where idparent_state=? and student_state=?");
        $req->execute([$parentid,$studentid]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function AddHistoAbonnement($dateday,$parentid,$studentid,$datedebut,$datefin,$offreid,$codeEtab,$sessionEtab,$paiementid)
      {
        $req = $this->db->prepare("INSERT INTO abonnementhisto SET 	date_histoabn=?,parentid_histoabn=?,studentid_histoabn=?,datedeb_histoabn=?,datefin_histoabn=?,abonid_histoabn=?,codeEtab_histoabn=?,session_histoabn=?,paieid_histoabn=?");
          $req->execute([
          $dateday,
          $parentid,
          $studentid,
          $datedebut,
          $datefin,
          $offreid,
          $codeEtab,
          $sessionEtab,
          $paiementid
          ]);
      }

      function getCodeEtabAndSessionOfStudentsubscribe($studentid)
      {
        $req = $this->db->prepare("SELECT * FROM  inscription  where ideleve_inscrip=? order by id_inscrip desc");
        $req->execute([$studentid]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["codeEtab_inscrip"]."*".$someArray[0]["session_inscrip"];
        return $donnees;
      }

      function UpdatePaiementsubscribevalidation($statutpaie,$dateday,$validateurid,$paiementid,$parentid)
      {
          $req1= $this->db->prepare("UPDATE  paiementab set statut_paiab=?,datevalide_paiab=?,uservalide_paiab=? where paiementab.id_paiab=? and paiementab.parentid_paiab=?");
          $req1->execute([
            $statutpaie,
            $dateday,
            $validateurid,
            $paiementid,
            $parentid
          ]);
      }

      function getAbonnementdatas($offreid)
      {
        $req = $this->db->prepare("SELECT * FROM  abonnement where id_abn=?");
        $req->execute([$offreid]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array,true);
        $donnees=$someArray[0]["jours_abn"];
        return $donnees;
      }


      function getpaiementInfos($paiementid,$parentid)
      {
        $req = $this->db->prepare("SELECT * FROM paiementab where 	paiementab.id_paiab=? and paiementab.parentid_paiab=?");
        $req->execute([$paiementid,$parentid]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["nbstudent_paiab"]."*".$someArray[0]["subscribes_paiab"]."*".$someArray[0]["studentsid_paiab"];
        return $donnees;
      }

      function getLibelleOperateur($operateurid)
      {
        $req = $this->db->prepare("SELECT * FROM mobileoperator where 	id_mob=?");
        $req->execute([$operateurid]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["libelle_mob"];
        return $donnees;

      }

      function UpdateStateNotification($status,$parentid,$studentid,$session)
      {
        $req1= $this->db->prepare("UPDATE notificationstate set status_state=? where idparent_state=? and student_state=? and session_state=?");
          $req1->execute([
            $status,
            $parentid,
            $studentid,
            $session
          ]);
      }

      function UpdatecoursesFilesupport($fichierad,$courseid)
      {
        $req1= $this->db->prepare("UPDATE courses SET support_courses=? where id_courses=?");
          $req1->execute([
            $fichierad,$courseid
          ]);
      }

      function getAllParentsNotificationstate()
      {
        $req = $this->db->prepare("SELECT * FROM notificationstate,parent,compte where notificationstate.idparent_state=parent.idcompte_parent and compte.id_compte=parent.idcompte_parent");
        $req->execute([]);
        return $req->fetchAll();
      }

      function getNumberOfsatandbypaiement()
      {
          $req = $this->db->prepare("SELECT * FROM paiementab where statut_paiab=0");
          $req->execute([]);
          $data=$req->fetchAll();
          $nb=count($data);
          return $nb;
      }
      function getAllsouscriptionsH()
      {
        $req = $this->db->prepare("SELECT * FROM paiementab where statut_paiab=4");
        $req->execute([]);
        return $req->fetchAll();
      }

      function getAllsouscriptions()
      {
        $req = $this->db->prepare("SELECT * FROM paiementab where statut_paiab not in(-1,4)");
        $req->execute([]);
        return $req->fetchAll();
      }


      function getNumberOfStudentsclasses($classe,$session,$codeetab)
      {
          $req = $this->db->prepare("SELECT * FROM inscription where codeEtab_inscrip=? and session_inscrip=? and idclasse_inscrip in (?)");
          $req->execute([$codeetab,$session,$classe]);
          $data=$req->fetchAll();
          $nb=count($data);
          return $nb;
      }

      function getNumberOfStudentsclassesInfos($classe,$session,$codeetab)
      {
          $req = $this->db->prepare("SELECT * FROM inscription where codeEtab_inscrip=? and session_inscrip=? and idclasse_inscrip in (?)");
          $req->execute([$codeetab,$session,$classe]);
          return $req->fetchAll();

      }

      function getAllInteretOfactivities($id)
      {
          $req = $this->db->prepare("SELECT * FROM interetactivite where interetactivite.idactivite_interact=?");
          $req->execute([$id]);
          return $req->fetchAll();
      }

      function getDevisesOfThisEtab($codeetab)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement,pays where etablissement.pays_etab=pays.id_pays and etablissement.code_etab=?");
        $req->execute([$codeetab]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["devises_pays"];
        return $donnees;

      }

      function getEtabpaysid($codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement where etablissement.code_etab=?");
        $req->execute([$codeEtab]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["pays_etab"];
        return $donnees;
      }

      function getparascolaireMessageInfos($message)
      {
          $req = $this->db->prepare("SELECT * FROM messages,activites where 	messages.id_msg=activites.idnotif_act and messages.id_msg=?");
          $req->execute([$message]);
          return $req->fetchAll();
      }

      function getMessagesType($message,$addby)
      {
        $req = $this->db->prepare("SELECT * FROM messages where 	id_msg=? and 	addby_msg=? ");
        $req->execute([$message,$addby]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["parascolaire_msg"]."*".$someArray[0]["scola_msg"]."*".$someArray[0]["objet_msg"]."*".$someArray[0]["other_msg"]."*".$someArray[0]["libelle_msg"]."*".$someArray[0]["classes_msg"]."*".$someArray[0]["session_msg"]."*".$someArray[0]["codeEtab_msg"];
        return $donnees;
      }

      function getTypecompteOfuser($addby)
      {
        $req = $this->db->prepare("SELECT * FROM compte where id_compte=? ");
        $req->execute([$addby]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["type_compte"];
        return $donnees;

      }
      function getMessagesInfosselect($message)
      {
        $req = $this->db->prepare("SELECT * FROM messages where id_msg=? ");
        $req->execute([$message]);
        return $req->fetchAll();

      }

      function creationOfObservationNoteTeatcherPrecis($classe,$session,$codeetab,$eleves,$message)
      {



        require_once('../class/cnx.php');
        require_once('../class_html2PDF/html2pdf.class.php');
        require_once('../class_html2PDF/_tcpdf_5.0.002/config/lang/eng.php');
        require_once('../class_html2PDF/_tcpdf_5.0.002/tcpdf.php');
        require_once('Parent.php');
        require_once('Student.php');
        require_once('Classe.php');

        $schoolclasses=new Classe();
        $etabs=new Etab();
        $studentThis=new Student();
        $parentx=new ParentX();
        $datamessages=$etabs->getMessagesInfosselect($message);
        $array=json_encode($datamessages,true);
        $someArray = json_decode($array, true);
        $libelle_objet="";
        $objet=$someArray[0]["objet_msg"];
        if($objet==8)
        {
          $libelle_objet=$someArray[0]["other_msg"];
        }else {
          $libelle_objet=$someArray[0]["libelle_msg"];
        }

        $tabclasses=explode(",",$eleves);
        $nbligne=count($tabclasses);
        $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeetab);
        ob_start();
        for($i=0;$i<$nbligne;$i++)
        {
          $parentid=$parentx->DetermineNameAndPrenomsParent($tabclasses[$i]);
          $dataStudents=$studentThis->getAllInformationsOfStudent($tabclasses[$i],$session);
          $tabStudent=explode("*",$dataStudents);
            $dateday=date("d-m-Y");
          ?>
          <page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
          <page_header>

          </page_header>
          <h2 style="text-align:center;"> Note d'Observation </h2>
          <br>
          <table>
            <tr>
              <td height="50" >Date : <?php echo $dateday;  ?></td>
            </tr>
            <tr>
              <td height="30" >De : La Direction de l'etablissement <?php echo $libelleEtab;  ?></td>
            </tr>
            <tr>
              <td height="30" >A : <?php echo $parentx->getNamePreOfTheCompte($parentid); ?>, parent de l'eleve </td>
            </tr>
            <tr>
              <td height="30" ><?php echo $tabStudent[2]." ".$tabStudent[3]; ?> en classe de <?php echo $schoolclasses->getInfosofclassesbyId($tabStudent[15],$session) ?> </td>
            </tr>
            <tr>
              <td height="30" >Objet: <?php echo $libelle_objet; ?> </td>
            </tr>
            <tr>
              <td height="10" >Commentaire : <?php echo $libelle_objet; ?> </td>
            </tr>
            <tr>
              <td height="20" ><?php echo $someArray[0]["commentaire_msg"]; ?> </td>
            </tr>
          </table>


          <page_footer>

          </page_footer>
          </page>
          <?php
        }

        $content=ob_get_clean();
        $html2pdf = new HTML2PDF('P', 'A4', 'fr');
        $html2pdf->pdf->SetDisplayMode('real');
        $html2pdf->setTestTdInOnePage(false);
        $html2pdf->writeHTML($content);
         //ob_clean();
        $completName=substr($libelle_objet,3).$message;
        $html2pdf->Output('../generated/'.$completName.'.pdf','F');
         return '../generated/'.$completName.'.pdf';

      }

      function creationOfObservationNoteTeatcher($classe,$session,$codeetab,$eleves,$message)
      {
        require_once('../class_html2PDF/html2pdf.class.php');
        require_once('../class_html2PDF/_tcpdf_5.0.002/config/lang/eng.php');
        require_once('../class_html2PDF/_tcpdf_5.0.002/tcpdf.php');
        require_once('Parent.php');
        require_once('Student.php');
        require_once('Classe.php');
        $schoolclasses=new Classe();
        $etabs=new Etab();
        $studentThis=new Student();
        $parentx=new ParentX();
        $datamessages=$etabs->getMessagesInfosselect($message);
        $array=json_encode($datamessages,true);
        $someArray = json_decode($array, true);
        $libelle_objet="";
        $objet=$someArray[0]["objet_msg"];
        if($objet==8)
        {
          $libelle_objet=$someArray[0]["other_msg"];
        }else {
          $libelle_objet=$someArray[0]["libelle_msg"];
        }

        $datastudents=getNumberOfStudentsclassesInfos($classe,$session,$codeetab);
        $dateday=date("d-m-Y");
        $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeetab);
        ob_start();
       foreach ($datastudents as $value):
         $parentid=$parentx->DetermineNameAndPrenomsParent($value->ideleve_inscrip);
         $dataStudents=$studentThis->getAllInformationsOfStudent($value->ideleve_inscrip,$session);
         $tabStudent=explode("*",$dataStudents);
         ?>
         <page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
         <page_header>

         </page_header>
         <h2 style="text-align:center;"> Note d'Observation </h2>
         <br>
         <table>
           <tr>
             <td height="50" >Date : <?php echo $dateday;  ?></td>
           </tr>
           <tr>
             <td height="30" >De : La Direction de l'etablissement <?php echo $libelleEtab;  ?></td>
           </tr>
           <tr>
             <td height="30" >A : <?php echo $parentx->getNamePreOfTheCompte($parentid); ?>, parent de l'eleve </td>
           </tr>
           <tr>
             <td height="30" ><?php echo $tabStudent[2]." ".$tabStudent[3]; ?> en classe de <?php echo $schoolclasses->getInfosofclassesbyId($tabStudent[15],$session) ?> </td>
           </tr>
           <tr>
             <td height="30" >Objet: <?php echo $libelle_objet; ?> </td>
           </tr>
           <tr>
             <td height="10" >Commentaire : <?php echo $libelle_objet; ?> </td>
           </tr>
           <tr>
             <td height="20" ><?php echo $someArray[0]["commentaire_msg"]; ?> </td>
           </tr>
         </table>


         <page_footer>

         </page_footer>
         </page>
         <?php
       endforeach;
       $content=ob_get_clean();
       $html2pdf = new HTML2PDF('P', 'A4', 'fr');
       $html2pdf->pdf->SetDisplayMode('real');
       $html2pdf->setTestTdInOnePage(false);
       $html2pdf->writeHTML($content);
        //ob_clean();
       $completName=substr($libelle_objet,3).$message;
       $html2pdf->Output('../generated/'.$completName.'.pdf','F');
        return '../generated/'.$completName.'.pdf';
     }

      function createMessageEtatForAll($classe,$session,$codeetab,$eleves,$message)
      {
        require_once('../class_html2PDF/html2pdf.class.php');
        require_once('../class_html2PDF/_tcpdf_5.0.002/config/lang/eng.php');
        require_once('../class_html2PDF/_tcpdf_5.0.002/tcpdf.php');
        require_once('Parent.php');
        require_once('Student.php');
        require_once('Classe.php');
        $schoolclasses=new Classe();
        $etabs=new Etab();
        $studentThis=new Student();
        $parentx=new ParentX();
        $datamessages=$etabs->getMessagesInfosselect($message);
        $array=json_encode($datamessages,true);
        $someArray = json_decode($array, true);
        $libelle_objet="";
        $objet=$someArray[0]["objet_msg"];
        if($objet==8)
        {
          $libelle_objet=$someArray[0]["other_msg"];
        }else {
          $libelle_objet=$someArray[0]["libelle_msg"];
        }

        $datastudents=$etabs->getNumberOfStudentsclassesInfos($classe,$session,$codeetab);
        $dateday=date("d-m-Y");
        $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeetab);

         ob_start();
        foreach ($datastudents as $value):
          $parentid=$parentx->DetermineNameAndPrenomsParent($value->ideleve_inscrip);
          $dataStudents=$studentThis->getAllInformationsOfStudent($value->ideleve_inscrip,$session);
          $tabStudent=explode("*",$dataStudents);
          ?>
          <page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
          <page_header>

          </page_header>
          <h2 style="text-align:center;"> Note d'Observation </h2>
          <br>
          <table>
            <tr>
              <td height="50" >Date : <?php echo $dateday;  ?></td>
            </tr>
            <tr>
              <td height="30" >De : La Direction de l'etablissement <?php echo $libelleEtab;  ?></td>
            </tr>
            <tr>
              <td height="30" >A : <?php echo $parentx->getNamePreOfTheCompte($parentid); ?>, parent de l'eleve </td>
            </tr>
            <tr>
              <td height="30" ><?php echo $tabStudent[2]." ".$tabStudent[3]; ?> en classe de <?php echo $schoolclasses->getInfosofclassesbyId($tabStudent[15],$session) ?> </td>
            </tr>
            <tr>
              <td height="30" >Objet: <?php echo $libelle_objet; ?> </td>
            </tr>
            <tr>
              <td height="10" >Commentaire : <?php echo $libelle_objet; ?> </td>
            </tr>
            <tr>
              <td height="20" ><?php echo $someArray[0]["commentaire_msg"]; ?> </td>
            </tr>
          </table>


          <page_footer>

          </page_footer>
          </page>
          <?php
        endforeach;
        $content=ob_get_clean();
        $html2pdf = new HTML2PDF('P', 'A4', 'fr');
        $html2pdf->pdf->SetDisplayMode('real');
        $html2pdf->setTestTdInOnePage(false);
        $html2pdf->writeHTML($content);
         //ob_clean();
        $completName=substr($libelle_objet,3).$message;
        $html2pdf->Output('../generated/'.$completName.'.pdf','F');
         return '../generated/'.$completName.'.pdf';
      }

      function createMessageEtatForPrecise($classe,$session,$codeetab,$eleves,$message)
      {
        require_once('../class_html2PDF/html2pdf.class.php');
        require_once('../class_html2PDF/_tcpdf_5.0.002/config/lang/eng.php');
        require_once('../class_html2PDF/_tcpdf_5.0.002/tcpdf.php');
        require_once('Parent.php');
        require_once('Student.php');
        require_once('Classe.php');
        $schoolclasses=new Classe();
        $etabs=new Etab();
        $studentThis=new Student();
        $parentx=new ParentX();
        $datamessages=$etabs->getMessagesInfosselect($message);
        $array=json_encode($datamessages,true);
        $someArray = json_decode($array, true);
        $libelle_objet="";
        $objet=$someArray[0]["objet_msg"];
        if($objet==8)
        {
          $libelle_objet=$someArray[0]["other_msg"];
        }else {
          $libelle_objet=$someArray[0]["libelle_msg"];
        }
        $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeetab);
        $tabclasses=explode(",",$eleves);
        $nbligne=count($tabclasses);
          $dateday=date("d-m-Y");
        ob_start();

        for($i=0;$i<$nbligne;$i++)
        {
          $parentid=$parentx->DetermineNameAndPrenomsParent($tabclasses[$i]);
          $dataStudents=$studentThis->getAllInformationsOfStudent($tabclasses[$i],$session);
          $tabStudent=explode("*",$dataStudents);
          ?>
          <page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
          <page_header>

          </page_header>
          <h2 style="text-align:center;"> Note d'Observation </h2>
          <br>
          <table>
            <tr>
              <td height="50" >Date : <?php echo $dateday;  ?></td>
            </tr>
            <tr>
              <td height="30" >De : La Direction de l'etablissement <?php echo $libelleEtab;  ?></td>
            </tr>
            <tr>
              <td height="30" >A : <?php echo $parentx->getNamePreOfTheCompte($parentid); ?>, parent de l'eleve </td>
            </tr>
            <tr>
              <td height="30" ><?php echo $tabStudent[2]." ".$tabStudent[3]; ?> en classe de <?php echo $schoolclasses->getInfosofclassesbyId($tabStudent[15],$session) ?> </td>
            </tr>
            <tr>
              <td height="30" >Objet: <?php echo $libelle_objet; ?> </td>
            </tr>
            <tr>
              <td height="10" >Commentaire : <?php echo $libelle_objet; ?> </td>
            </tr>
            <tr>
              <td height="20" ><?php echo $someArray[0]["commentaire_msg"]; ?> </td>
            </tr>
          </table>
          <page_footer>

          </page_footer>
          </page>

          <?php
          $content=ob_get_clean();
          $html2pdf = new HTML2PDF('P', 'A4', 'fr');
          $html2pdf->pdf->SetDisplayMode('real');
          $html2pdf->setTestTdInOnePage(false);
          $html2pdf->writeHTML($content);
           //ob_clean();
          $completName=substr($libelle_objet,3).$message;
          $html2pdf->Output('../generated/'.$completName.'.pdf','F');
           return '../generated/'.$completName.'.pdf';
        }


      }


      function creationOfObservationNoteAllselect($classe,$session,$codeetab,$eleves,$message)
      {
        require_once('../class_html2PDF/html2pdf.class.php');
        require_once('../class_html2PDF/_tcpdf_5.0.002/config/lang/eng.php');
        require_once('../class_html2PDF/_tcpdf_5.0.002/tcpdf.php');
        $etabs=new Etab();
        //nous allons determiner le nombre d'eleves dans les classes selectionnées
        $nbligne=$etabs->getNumberOfStudentsclasses($classe,$session,$codeetab);

        $dataMessagesInfos=$etabs->getparascolaireMessageInfos($message);
        $array=json_encode($dataMessagesInfos,true);
        $someArray = json_decode($array, true);
        $dateday=date("d-m-Y");
        $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeetab);
        $datededeb=$someArray[0]['datedeb_act'];
        $datedefin=$someArray[0]['datefin_act'];

        $tabeleves=explode(",",$eleves);
        $nbeleves=count($tabeleves);

        $interetsOfActivities=$etabs->getAllInteretOfactivities($someArray[0]['id_act']);

        $heuredeb=$someArray[0]['heuredeb_act'];
        $heurefin=$someArray[0]['heurefin_act'];
        $deviseactivities=$etabs->getDevisesOfThisEtab($codeetab);

        $newheuredeb=str_replace("."," H ",$heuredeb);

        $newheurefin=str_replace("."," H ",$heurefin);

        $datagrat=$someArray[0]['payant_act'];

        $gratuit=0;

        if($datagrat==1)
        {
          $gratuit=1;
        }else {
          $gratuit=0;
        }

        $periode=0;

        if($datedefin==$datededeb)
        {
          $periode=1;
        }else {
          $periode=0;
        }
        $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeetab);

         ob_start();
         ?>
         <?php
         for($i=0;$i<$nbeleves;$i++)
         {
          ?>
          <page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
            <page_header>

            </page_header>

          <h2 style="text-align:center;"> Note d'Observation </h2>
          <br>
          <table>
                <tr>
                <td height="50" >Date : <?php echo $dateday;  ?></td>
                </tr>

                <tr>
                  <td height="30">La Direction de l'etabissement <b><?php echo  $libelleEtab;?></b>  </td>
                </tr>
                <tr>
                  <td height="30">tiens à porter à votre attention de l'organisation d'une activité denommée  <?php echo $someArray[0]["libelle_act"]; ?>  </td>
                </tr>
                <tr>
                  <?php
                    if($periode==1)
                    {
                      ?>
                      <td height="30">qui se tiendra du <?php echo $datededeb;  ?> au <?php echo $datedefin;  ?>  de <?php echo $newheuredeb; ?> a <?php echo $newheurefin; ?> a <b><?php echo $someArray[0]['lieuact_act'] ?></b>  </td>
                      <?php
                    }else {

                      ?>
                      <td height="30"> qui se tiendra le <?php echo $datededeb;  ?> de <?php echo $newheuredeb; ?> a <?php echo $newheurefin; ?> a <b><?php echo $someArray[0]['lieuact_act'] ?></b> </td>
                      <?php
                    }
                   ?>

                </tr>
                <tr>
                  <?php
                  if($gratuit==1)
                  {
                    ?>
                    <td height="30">Le cout de cette activité est de  <?php echo $someArray[0]['montant_act']." ".$deviseactivities; ?> </td>
                    <?php
                  }else {
                    ?>
                    <td></td>

                  <?php
                  }
                   ?>

                </tr>
                <tr>
                  <td height="30"><?php echo 	$someArray[0]['description_act']; ?></td>
                </tr>
                <?php
                foreach ($interetsOfActivities as $value):
                  ?>
                  <tr>
                    <td height="30"><?php echo $value->libelle_interact; ?></td>
                  </tr>
                  <?php
                endforeach;
                 ?>
                 <tr>
                   <td height="30"><b>Pour toutes informations complémentaire Merci de contacter</b> </td>
                 </tr>
                 <tr>
                   <td height="30">Monsieur / madame <?php echo $someArray[0]['respo_act']; ?></td>
                 </tr>
                 <tr>
                   <td height="30"><?php echo $someArray[0]['contactrespo_act']; ?></td>
                 </tr>

      </table>
      <br>
      <br>
      <table border=0>
        <tr>
          <td style="width:250px;"></td>
          <td style="width:250px;"></td>
          <td style="padding:5px;width:150px;text-align:center"><b><h3>Direction</h3> </b> </td>
        </tr>
      </table>


            <page_footer>


            </page_footer>
          </page>
          <?php
        }
           ?>
         <?php
          $content=ob_get_clean();
          $html2pdf = new HTML2PDF('P', 'A4', 'fr');
          $html2pdf->pdf->SetDisplayMode('real');
          $html2pdf->setTestTdInOnePage(false);
          $html2pdf->writeHTML($content);
           //ob_clean();
          $completName=substr($someArray[0]["libelle_act"],3).$message.$datededeb;
          $html2pdf->Output('../generated/'.$completName.'.pdf','F');
           return '../generated/'.$completName.'.pdf';

      }


      function creationOfObservationNote($classe,$session,$codeetab,$eleves,$message)
      {
        require_once('../class_html2PDF/html2pdf.class.php');
        require_once('../class_html2PDF/_tcpdf_5.0.002/config/lang/eng.php');
        require_once('../class_html2PDF/_tcpdf_5.0.002/tcpdf.php');
        $etabs=new Etab();
        // require_once('../class/Etablissement.php');
        //recuperation des informations de l'etablissement

        $dataMessagesInfos=$etabs->getparascolaireMessageInfos($message);
        $array=json_encode($dataMessagesInfos,true);
        $someArray = json_decode($array, true);
        $dateday=date("d-m-Y");
        $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeetab);
        $datededeb=$someArray[0]['datedeb_act'];
        $datedefin=$someArray[0]['datefin_act'];

        $tabeleves=explode(",",$eleves);
        $nbeleves=count($tabeleves);

        $interetsOfActivities=$etabs->getAllInteretOfactivities($someArray[0]['id_act']);

        $heuredeb=$someArray[0]['heuredeb_act'];
        $heurefin=$someArray[0]['heurefin_act'];
        $deviseactivities=$etabs->getDevisesOfThisEtab($codeetab);

        $newheuredeb=str_replace("."," H ",$heuredeb);

        $newheurefin=str_replace("."," H ",$heurefin);

        $datagrat=$someArray[0]['payant_act'];

        $gratuit=0;

        if($datagrat==1)
        {
          $gratuit=1;
        }else {
          $gratuit=0;
        }

        $periode=0;

        if($datedefin==$datededeb)
        {
          $periode=1;
        }else {
          $periode=0;
        }


         ob_start();

         ?>
         <?php
         for($i=0;$i<$nbeleves;$i++)
         {
          ?>
          <page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
            <page_header>

            </page_header>

          <h2 style="text-align:center;"> Note d'Observation </h2>
          <br>
          <table>
                <tr>
                <td height="50" >Date : <?php echo $dateday;  ?></td>
                </tr>

                <tr>
                  <td height="30">La Direction de l'etabissement <b><?php echo  $libelleEtab;?></b>  </td>
                </tr>
                <tr>
                  <td height="30">tiens à porter à votre attention de l'organisation d'une activité denommée  <?php echo $someArray[0]["libelle_act"]; ?>  </td>
                </tr>
                <tr>
                  <?php
                    if($periode==1)
                    {
                      ?>
                      <td height="30">qui se tiendra du <?php echo $datededeb;  ?> au <?php echo $datedefin;  ?>  de <?php echo $newheuredeb; ?> a <?php echo $newheurefin; ?> a <b><?php echo $someArray[0]['lieuact_act'] ?></b>  </td>
                      <?php
                    }else {

                      ?>
                      <td height="30"> qui se tiendra le <?php echo $datededeb;  ?> de <?php echo $newheuredeb; ?> a <?php echo $newheurefin; ?> a <b><?php echo $someArray[0]['lieuact_act'] ?></b> </td>
                      <?php
                    }
                   ?>

                </tr>
                <tr>
                  <?php
                  if($gratuit==1)
                  {
                    ?>
                    <td height="30">Le cout de cette activité est de  <?php echo $someArray[0]['montant_act']." ".$deviseactivities; ?> </td>
                    <?php
                  }else {
                    ?>
                    <td></td>

                  <?php
                  }
                   ?>

                </tr>
                <tr>
                  <td height="30"><?php echo 	$someArray[0]['description_act']; ?></td>
                </tr>
                <?php
                foreach ($interetsOfActivities as $value):
                  ?>
                  <tr>
                    <td height="30"><?php echo $value->libelle_interact; ?></td>
                  </tr>
                  <?php
                endforeach;
                 ?>
                 <tr>
                   <td height="30"><b>Pour toutes informations complémentaire Merci de contacter</b> </td>
                 </tr>
                 <tr>
                   <td height="30">Monsieur / madame <?php echo $someArray[0]['respo_act']; ?></td>
                 </tr>
                 <tr>
                   <td height="30"><?php echo $someArray[0]['contactrespo_act']; ?></td>
                 </tr>

      </table>
      <br>
      <br>
      <table border=0>
        <tr>
          <td style="width:250px;"></td>
          <td style="width:250px;"></td>
          <td style="padding:5px;width:150px;text-align:center"><b><h3>Direction</h3> </b> </td>
        </tr>
      </table>


            <page_footer>


            </page_footer>
          </page>
          <?php
          }
           ?>
         <?php
         $content=ob_get_clean();
         $html2pdf = new HTML2PDF('P', 'A4', 'fr');
         	$html2pdf->pdf->SetDisplayMode('real');
          $html2pdf->setTestTdInOnePage(false);
         	$html2pdf->writeHTML($content);
         	 //ob_clean();
            $completName=substr($someArray[0]["libelle_act"],3).$message.$datededeb;

         		$html2pdf->Output('../generated/'.$completName.'.pdf','F');

           return '../generated/'.$completName.'.pdf';

      }

      function Updateparascolairepayantenonprecise($parascolaireid,$messagesid,$precis,$paiecheck,$montantAct,$datededeb,$heurededeb,$datedefin,$heuredefin,$classes,$eleves,$respoactivite,$contactrespo,$emailvalue,$smsvalue)
      {
            // $req = $this->db->prepare("UPDATE messages set precis_msg=?,email_msg=?,sms_msg=?,classes_msg=?,eleves_msg=? where id_msg=?");
            // $req->execute([$precis,$emailvalue,$smsvalue,$classes,$eleves,$messagesid]);

            //modification de la tables activites

            $req1= $this->db->prepare("UPDATE activites set datedeb_act=?,heuredeb_act=?,datefin_act=?,heurefin_act=?,respo_act=?,contactrespo_act=?,classes_act=?,payant_act=?,montant_act=?,email_act=?,sms_act=? where id_act=? and idnotif_act=?");
            $req1->execute([
              $datededeb,
              $heurededeb,
              $datedefin,
              $heuredefin,
              $respoactivite,
              $contactrespo,
              $classes,
              $paiecheck,
              $montantAct,
              $emailvalue,
              $smsvalue,
              $parascolaireid,
              $messagesid
            ]);



      }

      function getAllSchoolmessageselected($classemessages,$session_msg,$codeEtab_msg)
      {
          // $req = $this->db->prepare("SELECT * FROM classe where classe.id_classe in (?) and classe.session_classe=? and classe.codeEtab_classe=? order by classe.libelle_classe ASC");
          $req = $this->db->prepare("SELECT * FROM classe where id_classe in (?) ");
          // $req->execute([$classemessages,$session_msg,$codeEtab_msg]);
          $req->execute([$classemessages]);
          return $req->fetchAll();
      }

      function getMessagesInfos($notifid)
      {
          $req = $this->db->prepare("SELECT * FROM messages,activites where messages.id_msg=activites.idnotif_act and id_msg=?");
          $req->execute([$notifid]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["destinataires_msg"]."*".$someArray[0]["classes_msg"]."*".$someArray[0]["codeEtab_msg"]."*".$someArray[0]["sms_msg"]."*".$someArray[0]["email_msg"]."*".$someArray[0]["precis_msg"]."*".$someArray[0]["eleves_msg"];
          $donnees.="*".$someArray[0]["libelle_act"]."*".$someArray[0]["description_act"]."*".$someArray[0]["date_msg"]."*".$someArray[0]["parascolaire_msg"];
          return $donnees;
      }

      function getMessagesInfosOnly($notifid)
      {
          $req = $this->db->prepare("SELECT * FROM messages where  id_msg=?");
          $req->execute([$notifid]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["destinataires_msg"]."*".$someArray[0]["classes_msg"]."*".$someArray[0]["codeEtab_msg"]."*".$someArray[0]["sms_msg"]."*".$someArray[0]["email_msg"]."*".$someArray[0]["precis_msg"]."*".$someArray[0]["eleves_msg"];
          $donnees.="*".$someArray[0]["date_msg"]."*".$someArray[0]["parascolaire_msg"]."*".$someArray[0]["scola_msg"]."*".$someArray[0]["libelle_msg"]."*".$someArray[0]["objet_msg"]."*".$someArray[0]["other_msg"]."*".$someArray[0]["commentaire_msg"];
          return $donnees;
      }

      function smssending($message,$msisdn)
      {
        $url = 'https://api.allmysms.com/http/9.0/sendSms/';
        $login = 'proximitysa';    //votre identifant allmysms
        $apiKey   = '130508f11954f33';    //votre mot de passe allmysms
        // $message    = 'Envoi de SMS test ';    //le message SMS, attention pas plus de 160 caractères
        $sender     = 'XSCHOOL SMS';  //l'expediteur, attention pas plus de 11 caractères alphanumériques
        $smsData="";
        // $contacts=substr($contacts,0,-1);



          $smsData="<DATA>
             <MESSAGE><![CDATA[".$message."]]></MESSAGE>
             <TPOA>$sender</TPOA>
             <SMS>
                <MOBILEPHONE>$msisdn</MOBILEPHONE>
             </SMS>
          </DATA>";


        $fields = array(
        'login'    => urlencode($login),
        'apiKey'      => urlencode($apiKey),
        'smsData'       => urlencode($smsData),
        );

        $fieldsString = "";
        foreach($fields as $key=>$value) {
            $fieldsString .= $key.'='.$value.'&';
        }
        rtrim($fieldsString, '&');

        try {

            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_POST, count($fields));
            curl_setopt($ch,CURLOPT_POSTFIELDS, $fieldsString);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);

            echo $result;

            curl_close($ch);

        } catch (Exception $e) {
            echo 'Api allmysms injoignable ou trop longue a repondre ' . $e->getMessage();
        }


      }



      function getMessagesInformations($messagedid,$parascolaire,$precis,$addby)
      {
            $req = $this->db->prepare("SELECT * FROM messages where messages.id_msg=? and messages.parascolaire_msg=? and messages.precis_msg=? and messages.addby_msg=?");
            $req->execute([$messagedid,$parascolaire,$precis,$addby]);
            return $req->fetchAll();
      }

      function Attendancesmssending($phonenumber,$emailparent,$EtabName,$nomEleve,$classeName,$libellematiere,$debutHours,$finHours,$codeEtab,$imageEtab)
      {
        $etabs=new Etab();
        $day=date("d-m-Y");
        $message="Bonjour Chers Parent,";
        $message.="l'élève ".$nomEleve." est absent au cours de ".$libellematiere." prévu de".$debutHours;
        $message.="à ".$finHours."aujourd'hui";

        // $etabs->clicksendersms($phonenumber,$message);
        $etabs->d7networkssmssender($phonenumber,$message);

      }

      function getMessagesParascosInformations($messagedid,$parascolaire,$precis,$addby)
      {
        $req = $this->db->prepare("SELECT * FROM messages,activites where activites.idnotif_act=messages.id_msg and messages.id_msg=? and messages.parascolaire_msg=? and messages.precis_msg=? and messages.addby_msg=?");
        $req->execute([$messagedid,$parascolaire,$precis,$addby]);
        return $req->fetchAll();
      }

      function AddNotificationToAllparentsclasseselectwithOtherSend($objetid,$commentaire,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$libelleobjet,$useradd)
      {
        $req = $this->db->prepare("INSERT INTO messages SET objet_msg=?,commentaire_msg=?,destinataires_msg=?,classes_msg=?,precis_msg=?,parascolaire_msg=?,statut_msg=?,date_msg=?,email_msg=?,sms_msg=?,session_msg=?,codeEtab_msg=?,other_msg=?,libelle_msg=?,addby_msg=?");
        $req->execute([
        $objetid,
        $commentaire,
        $destinataires,
        $classes,
        $precis,
        $paranotif,
        $statutNotif,
        $datemsg,
        $emailvalue,
        $smsvalue,
        $libellesession,
        $codeEtab,
        $other,
        $libelleobjet,
        $useradd
        ]);

        $idnotif=$this->db->lastInsertId();
        return $idnotif;

      }

      function AddNotificationToAllparentsclasseselectSend($objetid,$commentaire,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$libellesession,$codeEtab,$libelleobjet,$useradd)
      {
        $req = $this->db->prepare("INSERT INTO messages SET objet_msg=?,commentaire_msg=?,destinataires_msg=?,classes_msg=?,precis_msg=?,parascolaire_msg=?,statut_msg=?,date_msg=?,email_msg=?,sms_msg=?,session_msg=?,codeEtab_msg=?,libelle_msg=?,addby_msg=?");
        $req->execute([
        $objetid,
        $commentaire,
        $destinataires,
        $classes,
        $precis,
        $paranotif,
        $statutNotif,
        $datemsg,
        $emailvalue,
        $smsvalue,
        $libellesession,
        $codeEtab,
        $libelleobjet,
        $useradd
        ]);

        $idnotif=$this->db->lastInsertId();
        return $idnotif;

      }

      function AddNotificationToParentStudentSelectedwithOtherSend($objetid,$commentaire,$destinataires,$classes,$precis,$studentSchool,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$libelleobjet,$useradd)
      {
        $req = $this->db->prepare("INSERT INTO messages SET objet_msg=?,commentaire_msg=?,destinataires_msg=?,classes_msg=?,precis_msg=?,eleves_msg=?,parascolaire_msg=?,statut_msg=?,date_msg=?,email_msg=?,sms_msg=?,session_msg=?,codeEtab_msg=?,other_msg=?,libelle_msg=?,addby_msg=?");
        $req->execute([
        $objetid,
        $commentaire,
        $destinataires,
        $classes,
        $precis,
        $studentSchool,
        $paranotif,
        $statutNotif,
        $datemsg,
        $emailvalue,
        $smsvalue,
        $libellesession,
        $codeEtab,
        $other,
        $libelleobjet,
        $useradd
      ]);

      $idnotif=$this->db->lastInsertId();
      return $idnotif;

      }

      function AddNotificationToParentStudentSelectedSend($objetid,$commentaire,$destinataires,$classes,$precis,$studentSchool,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$libellesession,$codeEtab,$libelleobjet,$useradd)
      {
        $req = $this->db->prepare("INSERT INTO messages SET objet_msg=?,commentaire_msg=?,destinataires_msg=?,classes_msg=?,precis_msg=?,eleves_msg=?,parascolaire_msg=?,statut_msg=?,date_msg=?,email_msg=?,sms_msg=?,session_msg=?,codeEtab_msg=?,libelle_msg=?,addby_msg=?");
        $req->execute([
        $objetid,
        $commentaire,
        $destinataires,
        $classes,
        $precis,
        $studentSchool,
        $paranotif,
        $statutNotif,
        $datemsg,
        $emailvalue,
        $smsvalue,
        $libellesession,
        $codeEtab,
        $libelleobjet,
        $useradd
        ]);
        $idnotif=$this->db->lastInsertId();
        return $idnotif;
      }


      function getAllParascolairesActivityOfThisSchool($codeEtabLocal,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM activites where codeEtab_act=? and  session_act=? and  statut_act not in (0,3)");
        $req->execute([$codeEtabLocal,$libellesessionencours]);
        return $req->fetchAll();
      }

      function getAllParascolairesActivityForcalendar($codeEtabLocal,$libellesessionencours,$parascolaireid)
      {
        $req = $this->db->prepare("SELECT * FROM activites where codeEtab_act=? and  session_act=? and 	id_act=? ");
        $req->execute([$codeEtabLocal,$libellesessionencours,$parascolaireid]);
        return $req->fetchAll();
      }

      function getAllParascolairesActivityDetails($codeEtabLocal,$libellesessionencours,$parascolaireid)
      {
        $req = $this->db->prepare("SELECT * FROM messages where   codeEtab_msg=? and  session_msg=? and 	id_msg=? ");
        $req->execute([$codeEtabLocal,$libellesessionencours,$parascolaireid]);
        return $req->fetchAll();
      }

      function getallCompentencesOfthisSyllabus($idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabcomp where syllabus.id_syllab=syllabcomp.idsyllab_syllabcomp and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and  syllabus.idmatiere_syllab=? and syllabus.session_syllab=?");
        $req->execute([$idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab]);
        return $req->fetchAll();
      }

      function getallObjectifsOfthisSyllabus($idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab)
      {
          $req = $this->db->prepare("SELECT * FROM syllabus,syllabobjet where syllabus.id_syllab=syllabobjet.idsyllab_syllabob and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and  syllabus.idmatiere_syllab=? and syllabus.session_syllab=?");
          $req->execute([$idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab]);
          return $req->fetchAll();
      }

      function getAllprogrammesOfThisTeatcher($IdCompte,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,classe,enseignant,enseigner,matiere where  syllabus.idteatcher_syllab=enseignant.idcompte_enseignant and enseignant.idcompte_enseignant=enseigner.id_enseignant  and syllabus.idclasse_syllab=classe.id_classe  and syllabus.idmatiere_syllab=matiere.id_mat and syllabus.idteatcher_syllab=? and syllabus.session_syllab=?");
        $req->execute([$IdCompte,$libellesessionencours]);
        return $req->fetchAll();
      }

      function getAlltachesOfThisCahier($idcahier,$sessioncahier,$codeEtabcahier,$matierecahier,$teatchercahier)
      {
        $req = $this->db->prepare("SELECT * FROM cahier,tachecahier where cahier.id_cahier=tachecahier.idcahier_tacheca and cahier.id_cahier=? and cahier.session_cahier=? and cahier.codeEtab_cahier=? and cahier.matiere_cahier=? and cahier.teatcher_cahier=?");
        $req->execute([$idcahier,$sessioncahier,$codeEtabcahier,$matierecahier,$teatchercahier]);
        return $req->fetchAll();
      }

      function getAllsectionsOfThisCahier($idcahier,$sessioncahier,$codeEtabcahier,$matierecahier,$teatchercahier)
      {
          $req = $this->db->prepare("SELECT * FROM cahier,sectioncahier where cahier.id_cahier=sectioncahier.idcahier_secahier and cahier.id_cahier=? and cahier.session_cahier=? and cahier.codeEtab_cahier=? and cahier.matiere_cahier=? and cahier.teatcher_cahier=?");
          $req->execute([$idcahier,$sessioncahier,$codeEtabcahier,$matierecahier,$teatchercahier]);
          return $req->fetchAll();
      }

      function getAllcahiersOfteatcherId($idcompte,$session)
      {
        $req = $this->db->prepare("SELECT * FROM cahier,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and cahier.teatcher_cahier=enseignant.idcompte_enseignant and cahier.matiere_cahier=matiere.id_mat and cahier.classe_cahier=classe.id_classe and enseignant.idcompte_enseignant=? and cahier.session_cahier=? order by classe.libelle_classe ASC");
        $req->execute([$idcompte,$session]);
        return $req->fetchAll();
      }

      function Addtachecahier($idcahier,$tache)
      {
        $req = $this->db->prepare("INSERT INTO tachecahier SET 	idcahier_tacheca=?,libelle_tacheca=?");
          $req->execute([
        $idcahier,
        $tache
          ]);
      }

      function Addsectioncahier($idcahier,$section)
      {
        $req = $this->db->prepare("INSERT INTO sectioncahier SET idcahier_secahier=?,libelle_secahier=?");
          $req->execute([
          $idcahier,
          $section
          ]);
      }



      function Addcahier($libellesession,$codeEtab,$matiereid,$teatcherid,$dateday,$classe,$descri)
      {
        $req = $this->db->prepare("INSERT INTO cahier SET session_cahier=?,codeEtab_cahier=?,matiere_cahier=?,teatcher_cahier=?,datecrea_cahier=?,classe_cahier=?,retenir_cahier=?");
          $req->execute([
          $libellesession,
          $codeEtab,
          $matiereid,
          $teatcherid,
          $dateday,
          $classe,
          $descri
          ]);

          $idcahier=$this->db->lastInsertId();
          return $idcahier;

      }

      function AddInteretactivite($idactivity,$libelleinteret)
      {
        $req = $this->db->prepare("INSERT INTO interetactivite SET idactivite_interact=?,libelle_interact=?");
          $req->execute([
            $idactivity,
            $libelleinteret
          ]);
      }

      function AddparascolaireMessageToStudentselectParentNopay($objet_msg,$destinataires,$classes,$precis,$studentSchool,$paranotif,$statutpara,$dateday,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$useradd)
      {
        $req = $this->db->prepare("INSERT INTO messages SET objet_msg=?,destinataires_msg=?,classes_msg=?,precis_msg=?,eleves_msg=?,parascolaire_msg=?,statut_msg=?,date_msg=?,email_msg=?,sms_msg=?,session_msg=?,codeEtab_msg=?,other_msg=?,addby_msg=?");
          $req->execute([
            $objet_msg,
            $destinataires,
            $classes,
            $precis,
            $studentSchool,
            $paranotif,
            $statutpara,
            $dateday,
            $emailvalue,
            $smsvalue,
            $libellesession,
            $codeEtab,
            $other,
            $useradd
          ]);

          $idmessages=$this->db->lastInsertId();
          return $idmessages;
      }

      function DetermineTypeEtab($codeEtabAssigner)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement WHERE 	etablissement.code_etab=?");
        $req->execute([$codeEtabAssigner]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray =json_decode($array, true);
        $donnees=$someArray[0]["type_etab"];
        return $donnees;
      }

      function DetermineAgendaEtab($codeEtabAssigner)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement WHERE 	etablissement.code_etab=?");
        $req->execute([$codeEtabAssigner]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray =json_decode($array, true);
        $donnees=$someArray[0]["agenda_etab"];
        return $donnees;
      }

      function AddparascolaireMessageToStudentselectParent($objet_msg,$destinataires,$classes,$precis,$studentSchool,$paranotif,$statutpara,$dateday,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$useradd)
      {
        $req = $this->db->prepare("INSERT INTO messages SET objet_msg=?,destinataires_msg=?,classes_msg=?,precis_msg=?,eleves_msg=?,parascolaire_msg=?,statut_msg=?,date_msg=?,email_msg=?,sms_msg=?,session_msg=?,codeEtab_msg=?,other_msg=?,addby_msg=?");
          $req->execute([
            $objet_msg,
            $destinataires,
            $classes,
            $precis,
            $studentSchool,
            $paranotif,
            $statutpara,
            $dateday,
            $emailvalue,
            $smsvalue,
            $libellesession,
            $codeEtab,
            $other,
            $useradd
          ]);

          $idmessages=$this->db->lastInsertId();
          return $idmessages;
      }

      function AddparascolaireMessageNoPayantAll($objet_msg,$destinataires,$classes,$precis,$paranotif,$statutpara,$dateday,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$useradd)
      {
        $req = $this->db->prepare("INSERT INTO messages SET objet_msg=?,destinataires_msg=?,classes_msg=?,precis_msg=?,parascolaire_msg=?,statut_msg=?,date_msg=?,email_msg=?,sms_msg=?,session_msg=?,codeEtab_msg=?,other_msg=?,addby_msg=?");
          $req->execute([
            $objet_msg,
            $destinataires,
            $classes,
            $precis,
            $paranotif,
            $statutpara,
            $dateday,
            $emailvalue,
            $smsvalue,
            $libellesession,
            $codeEtab,
            $other,
            $useradd
          ]);

          $idmessages=$this->db->lastInsertId();
          return $idmessages;
      }

      function AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin,$typesession,$emailvalue,$smsvalue,$othertype)
      {
        $req = $this->db->prepare("INSERT INTO activites SET session_act=?,libelle_act=?,typeact_act=?,lieuact_act=?,respo_act=?,contactrespo_act=?,classes_act=?,description_act=?,codeEtab_act=?,montant_act=?,payant_act=?,idnotif_act=?,statut_act=?,datedeb_act=?,datefin_act=?,	heuredeb_act=?,heurefin_act=?,typesession_act=?,email_act=?,sms_act=?,othertype_act=?");
        $req->execute([
        $libellesession,
        $denomination,
        $typeactivite,
        $locationactivite,
        $respoactivite,
        $contactrespo,
        $classes,
        $descripactivite,
        $codeEtab,
        $montantAct,
        $paiecheck,
        $idmessage,
        $statutpara,
        $datededeb,
        $datedefin,
        $heurededeb,
        $heuredefin,
        $typesession,
        $emailvalue,
        $smsvalue,
        $othertype
        ]);
        $idpara=$this->db->lastInsertId();
        return $idpara;
      }

      function AddActiviteparascolairesPrimary($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin,$emailvalue,$smsvalue,$othertype)
      {
        $req = $this->db->prepare("INSERT INTO activites SET session_act=?,libelle_act=?,typeact_act=?,lieuact_act=?,respo_act=?,contactrespo_act=?,classes_act=?,description_act=?,codeEtab_act=?,montant_act=?,payant_act=?,idnotif_act=?,statut_act=?,datedeb_act=?,datefin_act=?,	heuredeb_act=?,heurefin_act=?,email_act=?,sms_act=?,othertype_act=?");
        $req->execute([
        $libellesession,
        $denomination,
        $typeactivite,
        $locationactivite,
        $respoactivite,
        $contactrespo,
        $classes,
        $descripactivite,
        $codeEtab,
        $montantAct,
        $paiecheck,
        $idmessage,
        $statutpara,
        $datededeb,
        $datedefin,
        $heurededeb,
        $heuredefin,
        $emailvalue,
        $smsvalue,
        $othertype
        ]);
        $idpara=$this->db->lastInsertId();
        return $idpara;
      }

      function AddparascolaireMessagePayantAll($objet_msg,$destinataires,$classes,$precis,$paranotif,$statutpara,$dateday,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$useradd)
      {
        // $req = $this->db->prepare("INSERT INTO messages SET objet_msg=?,destinataires_msg=?,classes_msg=?,precis_msg=?,parascolaire_msg=?,statut_msg=?,date_msg=?,email_msg=?,sms_msg=?,session_msg=?,codeEtab_msg=?,other_msg=?");
        $req = $this->db->prepare("INSERT INTO messages SET objet_msg=?,destinataires_msg=?,classes_msg=?,precis_msg=?,parascolaire_msg=?,date_msg=?,email_msg=?,sms_msg=?,session_msg=?,codeEtab_msg=?,other_msg=?");

          $req->execute([
            $objet_msg,
            $destinataires,
            $classes,
            $precis,
            $paranotif,
            // $statutpara,
            $dateday,
            $emailvalue,
            $smsvalue,
            $libellesession,
            $codeEtab,
            $other
          ]);

          $idmessages=$this->db->lastInsertId();
          return $idmessages;
      }

      function AddNotificationToAllparentsclasseselectwithOther($objet,$commentaire,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$libelleobjet,$useradd)
      {
        $req = $this->db->prepare("INSERT INTO messages SET objet_msg=?,commentaire_msg=?,destinataires_msg=?,classes_msg=?,precis_msg=?,parascolaire_msg=?,statut_msg=?,date_msg=?,email_msg=?,sms_msg=?,session_msg=?,codeEtab_msg=?,other_msg=?,libelle_msg=?,addby_msg=?");
        $req->execute([
        $objet,
        $commentaire,
        $destinataires,
        $classes,
        $precis,
        $paranotif,
        $statutNotif,
        $datemsg,
        $emailvalue,
        $smsvalue,
        $libellesession,
        $codeEtab,
        $other,
        $libelleobjet,
        $useradd
        ]);
      }

      function AddNotificationToParentStudentSelectedwithOther($objet,$commentaire,$destinataires,$classes,$precis,$studentSchool,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$libelleobjet,$useradd)
      {
        $req = $this->db->prepare("INSERT INTO messages SET objet_msg=?,commentaire_msg=?,destinataires_msg=?,classes_msg=?,precis_msg=?,eleves_msg=?,parascolaire_msg=?,statut_msg=?,date_msg=?,email_msg=?,sms_msg=?,session_msg=?,codeEtab_msg=?,other_msg=?,libelle_msg=?,addby_msg=?");
        $req->execute([
        $objet,
        $commentaire,
        $destinataires,
        $classes,
        $precis,
        $studentSchool,
        $paranotif,
        $statutNotif,
        $datemsg,
        $emailvalue,
        $smsvalue,
        $libellesession,
        $codeEtab,
        $other,
        $libelleobjet,
        $useradd
      ]);
      }


      function AddNotificationToParentStudentSelected($objet,$commentaire,$destinataires,$classes,$precis,$studentSchool,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$libellesession,$codeEtab,$libelleobjet,$useradd)
      {
        $req = $this->db->prepare("INSERT INTO messages SET objet_msg=?,commentaire_msg=?,destinataires_msg=?,classes_msg=?,precis_msg=?,eleves_msg=?,parascolaire_msg=?,statut_msg=?,date_msg=?,email_msg=?,sms_msg=?,session_msg=?,codeEtab_msg=?,libelle_msg=?,addby_msg=?");
        $req->execute([
        $objet,
        $commentaire,
        $destinataires,
        $classes,
        $precis,
        $studentSchool,
        $paranotif,
        $statutNotif,
        $datemsg,
        $emailvalue,
        $smsvalue,
        $libellesession,
        $codeEtab,
        $libelleobjet,
        $useradd
        ]);
      }

      function AddNotificationToAllparentsclasseselect($objet,$commentaire,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$libellesession,$codeEtab,$libelleobjet,$useradd)
      {
        $req = $this->db->prepare("INSERT INTO messages SET objet_msg=?,commentaire_msg=?,destinataires_msg=?,classes_msg=?,precis_msg=?,parascolaire_msg=?,statut_msg=?,date_msg=?,email_msg=?,sms_msg=?,session_msg=?,codeEtab_msg=?,libelle_msg=?,addby_msg=?");
        $req->execute([
        $objet,
        $commentaire,
        $destinataires,
        $classes,
        $precis,
        $paranotif,
        $statutNotif,
        $datemsg,
        $emailvalue,
        $smsvalue,
        $libellesession,
        $codeEtab,
        $libelleobjet,
        $useradd
        ]);
      }

      function AddRoutineRow($classe,$codeEtab,$heuredeb,$heurefin,$matiere,$day,$sessioncourante,$heureLibelles)
      {
        $req = $this->db->prepare("INSERT INTO routine SET classe_route=?,etab_route=?,debut_route=?,fin_route=?,matiere_route=?,day_route=?,session_route=?,libelleHeure_route=?");
        $req->execute([
        $classe,
        $codeEtab,
        $heuredeb,
        $heurefin,
        $matiere,
        $day,
        $sessioncourante,
        $heureLibelles
        ]);
      }

      function AddReglesyllabus($idsyllabus,$regle,$dateday)
      {
        $req = $this->db->prepare("INSERT INTO syllabregle SET idsyllab_syllabregle=?,libelle_syllabregle=?,date_syllabregle=?");
        $req->execute([$idsyllabus,$regle,$dateday]);
      }

      function AddEvaluationsyllabus($idsyllabus,$dateEvaluation,$typeEvaluation,$competenceEvaluation,$ponderationEvaluation,$dateday)
      {
        $req = $this->db->prepare("INSERT INTO syllabeval SET idsyllab_syllabeval=?,date_syllabeval=?,type_syllabeval=?,competence_syllabeval=?,ponderation_syllabeval=?,datecrea_syllabeval=? ");
        $req->execute([$idsyllabus,$dateEvaluation,$typeEvaluation,$competenceEvaluation,$ponderationEvaluation,$dateday]);
      }

      function Addcalendarsyllabus($idsyllabus,$dateCalandar,$seanceCalandar,$contentCalandar,$workCalandar,$dateday)
      {
        $req = $this->db->prepare("INSERT INTO syllabcalendar SET idsyllab_syllabcal=?,date_syllabcal=?,seance_syllabcal=?,contenu_syllabcal=?,prealable_syllabcal=?,datecrea_syllabcal=? ");
        $req->execute([$idsyllabus,$dateCalandar,$seanceCalandar,$contentCalandar,$workCalandar,$dateday]);
      }

      function Adddocfacsyllabus($idsyllabus,$docfac,$facultatif,$dateday)
      {
        $req = $this->db->prepare("INSERT INTO syllabdoc SET idsyllab_syllabdoc=?,libelle_syllabdoc=?,facultatif_syllabdoc=?,date_syllabdoc=? ");
        $req->execute([$idsyllabus,$docfac,$facultatif,$dateday]);

      }

      function Adddocsyllabus($idsyllabus,$doc,$dateday)
      {
        $req = $this->db->prepare("INSERT INTO syllabdoc SET idsyllab_syllabdoc=?,libelle_syllabdoc=?,date_syllabdoc=? ");
        $req->execute([$idsyllabus,$doc,$dateday]);
      }

      function Addcompsyllabus($idsyllabus,$comp,$dateday)
      {
        $req = $this->db->prepare("INSERT INTO syllabcomp SET 	idsyllab_syllabcomp=?,libelle_syllabcomp=?,date_syllabcomp=? ");
        $req->execute([$idsyllabus,$comp,$dateday]);
      }

      function Addrequissyllabus($idsyllabus,$requis,$dateday)
      {
        $req = $this->db->prepare("INSERT INTO syllabrequis SET 	idsyllab_syllabreq=?,libelle_syllabreq=?,date_syllabreq=? ");
        $req->execute([$idsyllabus,$requis,$dateday]);
      }

      function Addthemesyllabus($idsyllabus,$contenu,$dateday)
      {
        $req = $this->db->prepare("INSERT INTO syllabtheme SET 	idsyllab_syllabth=?,libelle_syllabth=?,date_syllabth=? ");
        $req->execute([$idsyllabus,$contenu,$dateday]);
      }

      function Addobjectisyllabus($idsyllabus,$objectif,$dateday)
      {
        $req = $this->db->prepare("INSERT INTO syllabobjet SET idsyllab_syllabob=?,libelle_syllabob=?,date_syllabob=? ");
        $req->execute([$idsyllabus,$objectif,$dateday]);
      }

      function Addsyllabus($teatcherid,$classe,$matiereid,$sessionsyllabus,$descri,$addby,$dateday,$codeEtab)
      {
        $req = $this->db->prepare("INSERT INTO syllabus SET idteatcher_syllab=?,idclasse_syllab=?,idmatiere_syllab=?,session_syllab=?,descri_syllab=?,addby_syllab=?,datecrea_syllab=?,codeEtab_syllab=? ");
        $req->execute([$teatcherid,$classe,$matiereid,$sessionsyllabus,$descri,$addby,$dateday,$codeEtab]);
        $idsyllabus=$this->db->lastInsertId();
        return $idsyllabus;

      }

      function getClassesByschoolCodewithIdspec($codeetab,$classex,$session)
      {
        $req = $this->db->prepare("SELECT * FROM classe,etablissement WHERE classe.codeEtab_classe=etablissement.code_etab and etablissement.code_etab=? and 	classe.id_classe=? and classe.session_classe=?");
        $req->execute([$codeetab,$classex,$session]);
        return $req->fetchAll();
      }

    function getThelastIdcompte()
    {
      $req = $this->db->prepare("SELECT * FROM compte order by id_compte DESC limit 1");
      $req->execute();
      $data=$req->fetchAll();
      $array=json_encode($data,true);
      $someArray = json_decode($array, true);
      //id_rating,	totalnotes_rating,totalnotescoef_rating,	totalcoef_rating,rating
      $donnees=$someArray[0]["id_compte"];
       return $donnees;
    }

      function AddReglementPiste($idpiste,$fisrtreglement,$soldeancien,$montvers,$idversement,$studentid)
      {
          $req = $this->db->prepare("INSERT INTO pistereglement SET idpiste_pisteregl=?,first_pisteregl=?,montant_pisteregl=?,new_pisteregl=?,idreglement_pisteregl=?,ideleve_pisteregl=?");
          $req->execute([$idpiste,$fisrtreglement,$soldeancien,$montvers,$idversement,$studentid]);
      }

      function AddpisteForReglement($datepiste,$IdCompte,$classe,$codeEtab,$libelleclasse,$controlepiste,$reglementpiste,$examenpiste,$actionpiste)
      {
        $req = $this->db->prepare("INSERT INTO piste SET date_piste=?,user_piste=?,classe_piste=?,codeEtab_piste=?,session_piste=?,controle_piste=?,reglement_piste=?,examen_piste=?,action_piste=?");
          $req->execute([$datepiste,$IdCompte,$classe,$codeEtab,$libelleclasse,$controlepiste,$reglementpiste,$examenpiste,$actionpiste]);
          $idpiste=$this->db->lastInsertId();

          return $idpiste;
      }

      function AddNotesPisteAfter($idpiste,$idtypenote,$firstnote,$notes,$ideleve,$oldnotes)
      {
        $req = $this->db->prepare("INSERT INTO pistenotes set idpiste_pistenote=?,controleid_pistenote=?,first_pistenote=?,note_pistenote=?,new_pistenote=?,ideleve_pistenote=?");
        $req->execute([$idpiste,$idtypenote,$firstnote,$oldnotes,$notes,$ideleve]);
      }

      function getLastNotescontroleAddedd($ideleve,$typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$codeEtab,$libellesession,$typesessionNotes)
      {
        $req = $this->db->prepare("SELECT * FROM notes where ideleve_notes=? and type_notes=? and idtype_notes=? and 	idclasse_notes=? and idmat_notes=? and idprof_notes=? and codeEtab_notes=? and session_notes=?");
        $req->execute([$ideleve,$typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$codeEtab,$libellesession]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        //id_rating,	totalnotes_rating,totalnotescoef_rating,	totalcoef_rating,rating
        $donnees=$someArray[0]["valeur_notes"];
         return $donnees;
      }

      function getStudentRatingInfos($ideleve,$typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$codeEtab,$libellesession,$typesessionNotes)
      {
        $req = $this->db->prepare("SELECT * FROM rating where ideleve_rating=? and codeEtab_rating=? and classe_rating=? and matiereid_rating=? and session_rating=? and typsession_rating=?");
        $req->execute([$ideleve,$codeEtab,$classeId,$matiereid,$libellesession,$typesessionNotes]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        //id_rating,	totalnotes_rating,totalnotescoef_rating,	totalcoef_rating,rating
        $donnees=$someArray[0]["id_rating"]."*".$someArray[0]["totalnotes_rating"]."*".$someArray[0]["totalnotescoef_rating"]."*".$someArray[0]["totalcoef_rating"]."*".$someArray[0]["rating"];
        return $donnees;
      }

      function getStudentRatingInfosNew($ideleve,$typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$codeEtab,$libellesession,$typesessionNotes)
      {
        $req = $this->db->prepare("SELECT * FROM rating where ideleve_rating=? and codeEtab_rating=? and classe_rating=? and matiereid_rating=? and session_rating=? and typsession_rating=?");
        $req->execute([$ideleve,$codeEtab,$classeId,$matiereid,$libellesession,$typesessionNotes]);
        $data=$req->fetchAll();
        return $data;
      }

      function AddNotesPisteExam($idpiste,$idtypenote,$firstnote,$notes,$ideleve)
      {
        $req = $this->db->prepare("INSERT INTO pistenotes set idpiste_pistenote=?,examenid_pistenote=?,first_pistenote=?,note_pistenote=?,ideleve_pistenote=?");
        $req->execute([$idpiste,$idtypenote,$firstnote,$notes,$ideleve]);
      }

      function getSessionTypeExam($idtypenote,$codeEtab,$sessionlibelle)
      {
          $req = $this->db->prepare("SELECT * FROM examen where id_exam=? and codeEtab_exam=? and session_exam=?");
          $req->execute([$idtypenote,$codeEtab,$sessionlibelle]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["typesess_exam"];
          return $donnees;
      }

      function AddNotesPiste($idpiste,$idtypenote,$firstnote,$notes,$ideleve)
      {
          $req = $this->db->prepare("INSERT INTO pistenotes set idpiste_pistenote=?,controleid_pistenote=?,first_pistenote=?,note_pistenote=?,ideleve_pistenote=?");
          $req->execute([$idpiste,$idtypenote,$firstnote,$notes,$ideleve]);
      }

      function Addpiste($datepiste,$IdCompte,$classeId,$codeEtab,$sessionlibelle,$typesessionNotes,$controlepiste,$reglementpiste,$examenpiste,$actionpiste)
      {
          $req = $this->db->prepare("INSERT INTO piste SET date_piste=?,user_piste=?,classe_piste=?,codeEtab_piste=?,session_piste=?,typesession_piste=?,controle_piste=?,reglement_piste=?,examen_piste=?,action_piste=?");
            $req->execute([$datepiste,$IdCompte,$classeId,$codeEtab,$sessionlibelle,$typesessionNotes,$controlepiste,$reglementpiste,$examenpiste,$actionpiste]);
            $idpiste=$this->db->lastInsertId();

            return $idpiste;
      }

      function AddpistePrimary($datepiste,$IdCompte,$classeId,$codeEtab,$sessionlibelle,$controlepiste,$reglementpiste,$examenpiste,$actionpiste)
      {
          $req = $this->db->prepare("INSERT INTO piste SET date_piste=?,user_piste=?,classe_piste=?,codeEtab_piste=?,session_piste=?,controle_piste=?,reglement_piste=?,examen_piste=?,action_piste=?");
          $req->execute([$datepiste,$IdCompte,$classeId,$codeEtab,$sessionlibelle,$controlepiste,$reglementpiste,$examenpiste,$actionpiste]);
          $idpiste=$this->db->lastInsertId();
          return $idpiste;
      }

      function UpdateRatingStudent($sessionlibelle,$typesessionNotes,$ideleve,$classeId,$teatcherid,$matiereid,$sommesNotes,$sommesNotescoef,$sommescoef,$moyenne,$ratingId,$codeEtab)
      {
        $req = $this->db->prepare("UPDATE rating set totalnotes_rating=?,totalnotescoef_rating=?,totalcoef_rating=?,rating=? where id_rating=? and session_rating=? and typsession_rating=? and classe_rating=? and matiereid_rating=? and ideleve_rating=? and codeEtab_rating=?");
        $req->execute([$sommesNotes,$sommesNotescoef,$sommescoef,$moyenne,$ratingId,$sessionlibelle,$typesessionNotes,$classeId,$matiereid,$ideleve,$codeEtab]);
      }

      function getRatingtypesessionInfos($ideleve,$idtypenote,$matiereid,$teatcherid,$codeEtab,$sessionlibelle,$classeId,$typesessionNotes)
      {
        $req = $this->db->prepare("SELECT * FROM rating where ideleve_rating=? and matiereid_rating=? and classe_rating=? and typsession_rating=? and session_rating=? ");
        $req->execute([$ideleve,$matiereid,$classeId,$typesessionNotes,$sessionlibelle]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["totalnotes_rating"]."*".$someArray[0]["totalnotescoef_rating"]."*".$someArray[0]["totalcoef_rating"]."*".$someArray[0]["id_rating"];
        return $donnees;
      }

      function getRatingtypesessionInfosNew($ideleve,$idtypenote,$matiereid,$teatcherid,$codeEtab,$sessionlibelle,$classeId,$typesessionNotes)
      {
        $req = $this->db->prepare("SELECT * FROM rating where ideleve_rating=? and matiereid_rating=? and classe_rating=? and typsession_rating=? and session_rating=? ");
        $req->execute([$ideleve,$matiereid,$classeId,$typesessionNotes,$sessionlibelle]);
        $data=$req->fetchAll();
        return $data;
      }



      function AddOldRatingStudent($sessionlibelle,$typesessionNotes,$ideleve,$classeId,$teatcherid,$matiereid,$notes,$notescoef,$coefficientNotes,$moyennestudent,$codeEtab)
      {
        $req = $this->db->prepare("INSERT INTO rating SET session_rating=?,	typsession_rating=?,ideleve_rating=?,classe_rating=?,idprof_rating=?,matiereid_rating=?,totalnotes_rating=?,totalnotescoef_rating=?,totalcoef_rating=?,rating=?,codeEtab_rating=?");
        $req->execute([$sessionlibelle,$typesessionNotes,$ideleve,$classeId,$teatcherid,$matiereid,$notes,$notescoef,$coefficientNotes,$moyennestudent,$codeEtab]);
      }

      function DeterminationOfNotesNumberForThisclasses($matiereid,$teatcherid,$codeEtab,$sessionlibelle,$typesessionNotes,$classeId)
      {
        $req = $this->db->prepare("SELECT * FROM controle,notes where controle.id_ctrl=notes.idtype_notes and controle.codeEtab_ctrl=? and controle.session_ctrl=? and controle.typesess_ctrl=? and notes.idclasse_notes=? and controle.mat_ctrl=?");
        $req->execute([$codeEtab,$sessionlibelle,$typesessionNotes,$classeId,$matiereid]);
        $data=$req->fetchAll();
        $nb=count($data);

        return $nb;

      }

      function getControlecoef($idtypenote,$matiereid,$teatcherid,$codeEtab,$sessionlibelle)
      {
        $req = $this->db->prepare("SELECT * FROM controle where id_ctrl=? and mat_ctrl=? and 	teatcher_ctrl=? and codeEtab_ctrl=? and session_ctrl=?");
        $req->execute([$idtypenote,$matiereid,$teatcherid,$codeEtab,$sessionlibelle]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["coef_ctrl"]."*".$someArray[0]["typesess_ctrl"];
        return $donnees;
      }

      function getParascolairesInfos($paraid,$codeEtab,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM activites where 	id_act=? and codeEtab_act=? and session_act=?");
        $req->execute([$paraid,$codeEtab,$libellesessionencours]);
        return $req->fetchAll();
      }

      function DetermineScolarityStateOfStudent($codeEtabAssigner,$libellesessionencours,$idcompte)
      {
        $req = $this->db->prepare("SELECT * FROM versement where codeEtab_versement=? and session_versement=? and ideleve_versement=? order by versement.id_versement DESC");
        $req->execute([$codeEtabAssigner,$libellesessionencours,$idcompte]);
        return $req->fetchAll();
      }

      function DetermineScolarityStateOfStudentHisto($codeEtabAssigner,$libellesessionencours,$idcompte)
      {
        $req = $this->db->prepare("SELECT * FROM versement where codeEtab_versement=? and session_versement=? and ideleve_versement IN( SELECT distinct id_compte from eleve,parent,parenter,compte where compte.id_compte=eleve.idcompte_eleve and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve  and parent.idcompte_parent=? ) order by versement.id_versement DESC");
        $req->execute([$codeEtabAssigner,$libellesessionencours,$idcompte]);
        return $req->fetchAll();
      }

      function DetermineScolarityStateOfStudentLast($codeEtabAssigner,$libellesessionencours,$idcompte)
      {
        $req = $this->db->prepare("SELECT * FROM versement where codeEtab_versement=? and session_versement=? and ideleve_versement=? and motif_versement='SCOLARITES' order by versement.id_versement DESC limit 1");
        $req->execute([$codeEtabAssigner,$libellesessionencours,$idcompte]);
        return $req->fetchAll();
      }

      function DetermineIinscriptionStateOfStudentLast($codeEtabAssigner,$libellesessionencours,$idcompte)
      {
        $req = $this->db->prepare("SELECT * FROM versement where codeEtab_versement=? and session_versement=? and ideleve_versement=? and motif_versement='INSCRIPTIONS' order by versement.id_versement DESC limit 1");
        $req->execute([$codeEtabAssigner,$libellesessionencours,$idcompte]);
        return $req->fetchAll();
      }

      function UpdateSolderetardNotificationWithoutClasses($titre,$message,$codeEtab,$libellesession,$smsvalue,$emailvalue,$soldeid,$notifid)
      {
        // $req = $this->db->prepare("UPDATE notification set libelle_notif=? where id_notif=? ");
        // $req->execute([$titre,$notifid]);


        $req = $this->db->prepare("UPDATE notification set libelle_notif=?,message_notif=?,sms_notif=?,email_notif=? where id_notif=? and session_notif=? and codeEtab_notif=?");
        $req->execute([$titre,$message,$smsvalue,$emailvalue,$notifid,$libellesession,$codeEtab]);

        $req1 = $this->db->prepare("UPDATE soldenotification set libelle_soldenot=? where 	id_soldenot=? and codeEtab_soldenot=? and session_soldenot=? and idnotif_soldenot=?");
        $req1->execute([$titre,$soldeid,$codeEtab,$libellesession,$notifid]);


      }

      function clicksendersms($contacts,$message)
      {
        $username="ja.assua@proximity-cm.com";
        $apikey="82868753-727E-2218-D572-E56F5889679F";
        $senderid="XSCHOOL";
        $url="https://api-mapper.clicksend.com/http/v2/send.php?method=http&username=".$username."&key=".$apikey."&to=".$contacts."&message=".$message."&senderid=".$senderid;
        // header("Location:".$url);
        $homepage = file_get_contents($url);
        return $homepage;
      }

      function Orangecismssender($contacts,$message)
      {


        $config = array(
            'clientId' => 'TcDibeD9jxOkJnomxJ6ahCplCBNJVGD5',
            'clientSecret' => 'Z5gfKqcJ2EhB7yb6'
        );

        $osms = new Osms($config);
        // retrieve an access token
        $response = $osms->getTokenFromConsumerKey();

        if (!empty($response['access_token'])) {
            $senderAddress = 'tel:+22578092974';
            $receiverAddress = $contacts;
            // $receiverAddress = 'tel:+22587290944';
            // $message = 'ceci est un message test';
            $senderName = 'XSHOOL';

            $osms->sendSMS($senderAddress, $receiverAddress, $message, $senderName);
        } else {
            echo $response['error'];
        }


      }

      function d7networkssmssender($contacts,$message)
      {
        $baseurl = 'http://smsc.d7networks.com:1401/send';

        $params = '?username=jaa9852';
        $params.= '&password=7ndPf87Y';
        $params.= '&to='.urlencode($contacts);
        $params.= '&from='.urlencode('XSCHOOL');
        $params.= '&content='.urlencode($message);

        $response = file_get_contents($baseurl.$params);
      }

      function paiementsmssender($mobilephones,$newcodeVers,$emailparent,$nomEleve,$classeName,$EtabName,$imageEtab,$montvers,$montrest,$modepaie,$datecrea,$devisepaie,$codeEtab)
      {
        $etabs=new Etab();
        if($modepaie==1)
        {
        $paiementmode="Chèque";
        }else {
          $paiementmode="Espèce";
        }

        $message="Bonjour Chers Parent, ";
        $message.="un versement en ". $paiementmode ."a été effectuer pour le paiement des frais de scolarité de vôtre enfant ".$nomEleve;
        $message.=" Le montant du versement est de  ".$montvers." ".$devisepaie. " avec un solde à regler de  ".$montrest." ".$devisepaie;

        // $etabs->clicksendersms($mobilephones,$message);

        $etabs->d7networkssmssender($mobilephones,$message);

      }

      function UpdateSolderetardNotificationWithClasses($titre,$message,$codeEtab,$libellesession,$smsvalue,$emailvalue,$classes,$soldeid,$notifid)
      {
        $req = $this->db->prepare("UPDATE notification set libelle_notif=?,message_notif=?,sms_notif=?,email_notif=?,	classes_notif=? where id_notif=? and session_notif=? and codeEtab_notif=?");
        $req->execute([$titre,$message,$smsvalue,$emailvalue,$classes,$notifid,$libellesession,$codeEtab]);

        $req1 = $this->db->prepare("UPDATE soldenotification set libelle_soldenot=?,classes_soldenot=? where 	id_soldenot=? and codeEtab_soldenot=? and session_soldenot=? and idnotif_soldenot=?");
        $req1->execute([$titre,$classes,$soldeid,$codeEtab,$libellesession,$notifid]);



      }

      function getsSoldesScolairesOfThisSchool($codeEtabLocal,$libellesessionencours,$paraid)
      {
        $req = $this->db->prepare("SELECT * FROM soldenotification,notification where soldenotification.idnotif_soldenot=notification.id_notif  and soldenotification.codeEtab_soldenot=? and soldenotification.session_soldenot=? and soldenotification.id_soldenot=?");
        $req->execute([$codeEtabLocal,$libellesessionencours,$paraid]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["libelle_soldenot"]."*".$someArray[0]["message_notif"]."*".$someArray[0]["classes_notif"]."*".$someArray[0]["destinataires_notif"];
         $donnees.="*".$someArray[0]["sms_notif"]."*".$someArray[0]["email_notif"]."*".$someArray[0]["id_notif"]."*".$someArray[0]["id_soldenot"];
        // $donnees.="*".$someArray[0]["classes_notif"]."*".$someArray[0]["destinataires_notif"]."*".$someArray[0]["payant_para"];
        // $donnees.="*".$someArray[0]["montant_para"]."*".$someArray[0]["sms_notif"]."*".$someArray[0]["email_notif"]."*".$someArray[0]["date_notif"]."*".$someArray[0]["id_notif"]."*".$someArray[0]["date_notif"];
        return $donnees;
      }

      function DeleteNotificationScolarite($scolaid,$notifid,$statusNotification,$statutsolde)
      {
        $req = $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=?");
        $req->execute([$statusNotification,$notifid]);

        $req1 = $this->db->prepare("UPDATE soldenotification set statut_soldenot=? where idnotif_soldenot=? and id_soldenot=?");
        $req1->execute([$statutsolde,$notifid,$scolaid]);


      }


      function DeleteNotificationScolariteActivities($scolaid,$notifid,$statusNotification,$statutsolde)
      {
        $req = $this->db->prepare("UPDATE messages set statut_msg=? where id_msg=?");
        $req->execute([$statusNotification,$notifid]);

        $req1 = $this->db->prepare("UPDATE soldenotification set statut_soldenot=? where idnotif_soldenot=? and id_soldenot=?");
        $req1->execute([$statutsolde,$notifid,$scolaid]);

        $_SESSION['user']['updateteaok']="Notification supprimer avec succès";
        if($_SESSION['user']['profile'] == "Admin_globale") {

              //header("Location:../manager/index.php");
              header("Location:../manager/allmessages.php");

          }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/allmessages.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/allmessages.php");

              }
      }


      function ArchivedScolaritesoldeScolar($soldeid,$notifid,$session,$codeEtab,$notificationstatus,$notificationsoldesatatus)
      {
        $req= $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$notificationstatus,$notifid,$codeEtab]);

        $reqX= $this->db->prepare("UPDATE  soldenotification set statut_soldenot=? where idnotif_soldenot=? and 	codeEtab_soldenot=? and id_soldenot=?");
        $reqX->execute([$notificationsoldesatatus,$notifid,$codeEtab,$soldeid]);


      }

      function ArchivedScolaritesolde($scolarid,$notifid,$session,$codeEtab,$notificationstatus,$notificationsoldesatatus)
      {
        $req= $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$notificationstatus,$notifid,$codeEtab]);

        $reqX= $this->db->prepare("UPDATE  soldenotification set statut_soldenot=? where idnotif_soldenot=? and 	codeEtab_soldenot=? and id_soldenot=?");
        $reqX->execute([$notificationsoldesatatus,$notifid,$codeEtab,$scolarid]);


      }

      function ArchivedScolaritesoldeActivities($scolarid,$notifid,$session,$codeEtab,$notificationstatus,$notificationsoldesatatus)
      {
        $req= $this->db->prepare("UPDATE messages set statut_msg=? where id_msg=? and codeEtab_msg=?");
        $req->execute([$notificationstatus,$notifid,$codeEtab]);

        $reqX= $this->db->prepare("UPDATE  soldenotification set statut_soldenot=? where idnotif_soldenot=? and 	codeEtab_soldenot=? and id_soldenot=?");
        $reqX->execute([$notificationsoldesatatus,$notifid,$codeEtab,$scolarid]);


      }


      function ArchivedParascolaireActivity($paraid,$notifid,$session,$codeEtab,$notificationstatut,$parascolairestatut)
      {
        $req= $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$notificationstatut,$notifid,$codeEtab]);

        $reqX= $this->db->prepare("UPDATE parascolaires set statut_para=? where idnotif_para=? and 	codeEtab_para=? and id_para=?");
        $reqX->execute([$parascolairestatut,$notifid,$codeEtab,$paraid]);



      }




      function ArchivedParascolaireActivitiesX($paraid,$notifid,$session,$codeEtab,$notificationstatut,$parascolairestatut)
      {
        $req= $this->db->prepare("UPDATE messages set statut_msg=? where id_msg=? and codeEtab_msg=?");
        $req->execute([$notificationstatut,$notifid,$codeEtab]);

        $reqX= $this->db->prepare("UPDATE activites set statut_act=? where idnotif_act=? and 	codeEtab_act=? and id_act=?");
        $reqX->execute([$parascolairestatut,$notifid,$codeEtab,$paraid]);



      }

      function ArchivedParascolaireActivitiesXMixte($paraid,$notifid,$session,$codeEtab,$notificationstatut,$parascolairestatut)
      {
        $req= $this->db->prepare("UPDATE messages set statut_msg=? where id_msg=? and codeEtab_msg=?");
        $req->execute([$notificationstatut,$notifid,$codeEtab]);

        $reqX= $this->db->prepare("UPDATE activites set statut_act=? where idnotif_act=? and 	codeEtab_act=? and id_act=?");
        $reqX->execute([$parascolairestatut,$notifid,$codeEtab,$paraid]);



      }


      function ArchivedParascolaireActivities($paraid,$notifid,$session,$codeEtab,$notificationstatut,$parascolairestatut)
      {
        $req= $this->db->prepare("UPDATE messages set statut_msg=? where id_msg=? and codeEtab_msg=?");
        $req->execute([$notificationstatut,$notifid,$codeEtab]);

        $reqX= $this->db->prepare("UPDATE activites set statut_act=? where idnotif_act=? and 	codeEtab_act=? and 	id_act=?");
        $reqX->execute([$parascolairestatut,$notifid,$codeEtab,$paraid]);



      }


      function UpdateNotificationStatusAndParascolairesParascolar($notifid,$notificationsStatus,$codeEtab,$paraid,$parascolaireStatus)
      {
        $req= $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$notificationsStatus,$notifid,$codeEtab]);

        $reqX= $this->db->prepare("UPDATE  soldenotification set statut_soldenot=? where 	idnotif_soldenot=? and 	codeEtab_soldenot=? and id_soldenot=?");
        $reqX->execute([$parascolaireStatus,$notifid,$codeEtab,$paraid]);

      }

      function UpdateNotificationStatusAndParascolairesPara($notifid,$notificationsStatus,$codeEtab,$paraid,$parascolaireStatus)
      {
        $req= $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$notificationsStatus,$notifid,$codeEtab]);

        $reqX= $this->db->prepare("UPDATE parascolaires set statut_para=? where 	idnotif_para=? and 	codeEtab_para=? and id_para=?");
        $reqX->execute([$parascolaireStatus,$notifid,$codeEtab,$paraid]);

      }

      function UpdateNotificationStatusAndParascolairesParaActivity($notifid,$notificationsStatus,$codeEtab,$paraid,$parascolaireStatus)
      {
        $req= $this->db->prepare("UPDATE messages set statut_msg=? where id_msg=? and codeEtab_msg=?");
        $req->execute([$notificationsStatus,$notifid,$codeEtab]);

        $reqX= $this->db->prepare("UPDATE activites set statut_act=? where 	idnotif_act=? and 	codeEtab_act=? and id_act=?");
        $reqX->execute([$parascolaireStatus,$notifid,$codeEtab,$paraid]);

      }


      function UpdateNotificationStatusParasclar($notifid,$notificationsStatus,$codeEtab)
      {
        $req= $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$notificationsStatus,$notifid,$codeEtab]);
      }

      function UpdateNotificationStatusPara($notifid,$notificationsStatus,$codeEtab)
      {
        $req= $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$notificationsStatus,$notifid,$codeEtab]);
      }

      function UpdateNotificationStatusParaActivity($notifid,$notificationsStatus,$codeEtab)
      {
        $req= $this->db->prepare("UPDATE messages set statut_msg=? where id_msg=? and codeEtab_msg=?");
        $req->execute([$notificationsStatus,$notifid,$codeEtab]);
      }

      function  DetermineAllScolairesNoitications($codeEtabLocal,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * from soldenotification,notification where soldenotification.idnotif_soldenot=notification.id_notif and soldenotification.codeEtab_soldenot=? and soldenotification.session_soldenot=? and soldenotification.statut_soldenot not in (3,4) order by soldenotification.id_soldenot DESC	");
        $req->execute([$codeEtabLocal,$libellesessionencours]);
        return $req->fetchAll();
      }

      function AddSoldeNotification($titre,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$paranotif,$libellesession,$scolanotif,$idscolanotifications)
      {
        $req = $this->db->prepare("INSERT INTO soldenotification SET libelle_soldenot=?,codeEtab_soldenot=?,session_soldenot=?,classes_soldenot=?,idnotif_soldenot=?,statut_soldenot=?");
        $req->execute([$titre,$codeEtab,$libellesession,$classes,$idscolanotifications,$scolanotif]);
        $_SESSION['user']['addctrleok']="Notification ajouté avec succès";

        if($_SESSION['user']['profile'] == "Admin_locale") {
          if($_SESSION['user']['paysid']==4)
          {
            header("Location:../localecmr/soldenotifications.php");
          }else {
            header("Location:../locale/soldenotifications.php");
          }


          }
      }

    function  AddNotificationScola($titre,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$paranotif,$libellesession,$scolanotif)
      {
        $req = $this->db->prepare("INSERT INTO notification SET libelle_notif=?,destinataires_notif=?,classes_notif=?,message_notif=?,join_notif=?,statut_notif=?,date_notif=?,codeEtab_notif=?,sms_notif=?,email_notif=?,para_notif=?,session_notif=?,scola_notif=?");
        $req->execute([$titre,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$paranotif,$libellesession,$scolanotif]);
        $idnotif=$this->db->lastInsertId();
        // return $idlastcompte;

        $reqX = $this->db->prepare("INSERT INTO soldenotification SET libelle_soldenot=?,codeEtab_soldenot=?,session_soldenot=?,classes_soldenot=?,idnotif_soldenot=?,statut_soldenot=?");
        $reqX->execute([$titre,$codeEtab,$libellesession,$classes,$idnotif,$scolanotif]);


      }

      function DetermineAllversements($codeEtabLocal,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * from versement where versement.codeEtab_versement=? and versement.session_versement=?");
        $req->execute([$codeEtabLocal,$libellesessionencours]);
        return $req->fetchAll();
      }

      function DetermineAllversementstudent($codeEtabLocal,$libellesessionencours,$eleveid,$classeid)
      {
        $req = $this->db->prepare("SELECT * from versement where versement.ideleve_versement=? and versement.classe_versement=? and versement.codeEtab_versement=? and versement.session_versement=?");
        $req->execute([$eleveid,$classeid,$codeEtabLocal,$libellesessionencours]);
        return $req->fetchAll();
      }

      function getCountrydeviseByCodeEtab($codeEtabAssigner)
      {
          $req = $this->db->prepare("SELECT * from etablissement,pays where pays.id_pays=etablissement.pays_etab and etablissement.code_etab=?");
          $req->execute([$codeEtabAssigner]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["devises_pays"];
          return $donnees;
      }

      function getExamenNotesWithoutLibctrl($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession,$libsemes)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,examen,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and examen.session_exam=classe.session_classe and
          notes.idclasse_notes=classe.id_classe and notes.idtype_notes=examen.id_exam and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=examen.session_exam and notes.ideleve_notes=? and notes.idclasse_notes=?
          and notes.idmat_notes=? and examen.typesess_exam=? and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=?  ");

          $req->execute([$idcompte,$classe,$matclasse,$libsemes,$notetype,$codeEtab,$libellesession]);
          return $req->fetchAll();
      }

      function getControleNotesWithoutLibctrl($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession,$libsemes)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
          and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
          and notes.idmat_notes=? and controle.typesess_ctrl=? and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=?  ");

          $req->execute([$idcompte,$classe,$matclasse,$libsemes,$notetype,$codeEtab,$libellesession]);
          return $req->fetchAll();

      }

      function getControleNotesWithoutLibctrlOther($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
          and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
          and notes.idmat_notes=?  and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=?  ");

          $req->execute([$idcompte,$classe,$matclasse,$notetype,$codeEtab,$libellesession]);
          return $req->fetchAll();

      }

      function getNoteStudentAllByTypeAndSessionExam($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession,$libsemes)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,examen,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and examen.session_exam=classe.session_classe and
          notes.idclasse_notes=classe.id_classe and notes.idtype_notes=examen.id_exam and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=examen.session_exam and notes.ideleve_notes=? and notes.idtype_notes in (?) and notes.type_notes=? and notes.idmat_notes=? and notes.idclasse_notes=?
          and notes.codeEtab_notes=? and notes.session_notes=? and examen.typesess_exam=?");
          $req->execute([$idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession,$libsemes]);
          return $req->fetchAll();
      }

      function getPreciseNotesExamenStudent($idcompte,$examenid,$notetype,$teatcherid,$matclasse,$classe,$codeEtab,$libellesession,$libsemes)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,examen,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and examen.session_exam=classe.session_classe and
          notes.idclasse_notes=classe.id_classe and notes.idtype_notes=examen.id_exam and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=examen.session_exam and notes.ideleve_notes=? and notes.idtype_notes=? and notes.type_notes=? and notes.idprof_notes=?
          and notes.idmat_notes=? and notes.idclasse_notes=? and notes.codeEtab_notes=? and notes.session_notes=? and examen.typesess_exam=?  ");
          $req->execute([$idcompte,$examenid,$notetype,$teatcherid,$matclasse,$classe,$codeEtab,$libellesession,$libsemes]);
          return $req->fetchAll();
      }

      function getNoteStudentAllByTypeAndSession($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession,$libsemes)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
          and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl
           and notes.idtype_notes=controle.id_ctrl and notes.ideleve_notes=? and notes.idtype_notes in (?) and notes.type_notes=? and notes.idmat_notes=? and notes.idclasse_notes=? and notes.codeEtab_notes=? and notes.session_notes=? and controle.typesess_ctrl=? ");
           $req->execute([$idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession,$libsemes]);



           return $req->fetchAll();
      }

      function getPreciseNotesControleStudent($idcompte,$controleid,$notetype,$teatcherid,$matclasse,$classe,$codeEtab,$libellesession,$libsemes)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
          and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl
           and notes.idtype_notes=controle.id_ctrl and notes.ideleve_notes=? and notes.idtype_notes=? and notes.type_notes=? and notes.idprof_notes=? and notes.idmat_notes=? and notes.idclasse_notes=? and notes.codeEtab_notes=? and notes.session_notes=? and controle.typesess_ctrl=?");
          $req->execute([$idcompte,$controleid,$notetype,$teatcherid,$matclasse,$classe,$codeEtab,$libellesession,$libsemes]);
          return $req->fetchAll();

      }

      function getPreciseNotesControleStudentOther($idcompte,$controleid,$notetype,$teatcherid,$matclasse,$classe,$codeEtab,$libellesession)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
          and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl
           and notes.idtype_notes=controle.id_ctrl and notes.ideleve_notes=? and notes.idtype_notes=? and notes.type_notes=? and notes.idprof_notes=? and notes.idmat_notes=? and notes.idclasse_notes=? and notes.codeEtab_notes=? and notes.session_notes=?");
          $req->execute([$idcompte,$controleid,$notetype,$teatcherid,$matclasse,$classe,$codeEtab,$libellesession]);
          return $req->fetchAll();

      }

      function getStudentExam($idcompte,$classe,$matiere,$semestre,$notetype,$codeEtab,$session)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,examen,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and examen.session_exam=classe.session_classe and
          notes.idclasse_notes=classe.id_classe and notes.idtype_notes=examen.id_exam and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=examen.session_exam and notes.ideleve_notes=? and notes.idclasse_notes=?
          and notes.idmat_notes=? and examen.typesess_exam=? and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=?  ");

          $req->execute([$idcompte,$classe,$matiere,$semestre,$notetype,$codeEtab,$session]);
          return $req->fetchAll();
      }

      function getNumberOfNotesExamStudentByClassesIdAndSubject($idcompte,$classe,$matiere,$semestre,$notetype,$codeEtab,$session)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,examen,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and examen.session_exam=classe.session_classe and
          notes.idclasse_notes=classe.id_classe and notes.idtype_notes=examen.id_exam and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=examen.session_exam and notes.ideleve_notes=? and notes.idclasse_notes=?
          and notes.idmat_notes=? and examen.typesess_exam=? and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=?  ");

          $req->execute([$idcompte,$classe,$matiere,$semestre,$notetype,$codeEtab,$session]);
          $data=$req->fetchAll();
          $nb=count($data);

          return $nb;
      }

      function getStudentControle($idcompte,$classe,$matiere,$semestre,$notetype,$codeEtab,$session)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
          and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
          and notes.idmat_notes=? and controle.typesess_ctrl=? and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=?   ");

          $req->execute([$idcompte,$classe,$matiere,$semestre,$notetype,$codeEtab,$session]);
          return $req->fetchAll();

      }

      function getStudentControleother($idcompte,$classe,$matiere,$notetype,$codeEtab,$session)
      {
        $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
          and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
          and notes.idmat_notes=? and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=?   ");

          $req->execute([$idcompte,$classe,$matiere,$notetype,$codeEtab,$session]);
          return $req->fetchAll();

      }

      function getNumberOfNotesControleStudentByClassesIdAndSubject($idcompte,$classe,$matiere,$semestre,$notetype,$codeEtab,$session)
      {
          $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
            and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and  notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl
            and notes.ideleve_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and controle.typesess_ctrl=? and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=?   ");

            $req->execute([$idcompte,$classe,$matiere,$semestre,$notetype,$codeEtab,$session]);
            $data=$req->fetchAll();
            $nb=count($data);

            return $nb;
      }

      function getNumberOfNotesControleStudentByClassesIdAndSubjectother($idcompte,$classe,$matiere,$notetype,$codeEtab,$session)
      {
          $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
            and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and  notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl
            and notes.ideleve_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=?   ");

            $req->execute([$idcompte,$classe,$matiere,$notetype,$codeEtab,$session]);
            $data=$req->fetchAll();
            $nb=count($data);

            return $nb;
      }

      function getLessonsbyMatiereidOnly($matiereid,$lessonclasse,$lessonEtab,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT distinct fiche.mat_fiche  FROM fiche,sousfiche where fiche.id_fiche=sousfiche.idfic_sousfic and fiche.mat_fiche=? and fiche.classe_fiche=? and fiche.codeEtab_fiche=? and fiche.session_fiche=? ");
        $req->execute([$matiereid,$lessonclasse,$lessonEtab,$libellesessionencours]);
        return $req->fetchAll();
      }

      function getLessonsbyMatiereidAndPeriode($classeid,$codeEtab,$libellesession,$matclasse,$datedu,$dateau)
      {
            $req = $this->db->prepare("SELECT distinct fiche.mat_fiche FROM fiche,sousfiche,classe,matiere where fiche.classe_fiche=classe.id_classe and fiche.id_fiche=sousfiche.idfic_sousfic and fiche.mat_fiche=matiere.id_mat and fiche.classe_fiche=? and fiche.codeEtab_fiche=? and fiche.session_fiche=? and fiche.mat_fiche=? and (fiche.date_fiche>=? and fiche.date_fiche<=?)  ");
            $req->execute([$classeid,$codeEtab,$libellesession,$matclasse,$datedu,$dateau]);
            return $req->fetchAll();
      }

      function getLessonsbyMatiere($matiereid,$lessonclasse,$lessonEtab,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT *  FROM fiche,sousfiche where fiche.id_fiche=sousfiche.idfic_sousfic and fiche.mat_fiche=? and fiche.classe_fiche=? and fiche.codeEtab_fiche=? and fiche.session_fiche=? ");
        $req->execute([$matiereid,$lessonclasse,$lessonEtab,$libellesessionencours]);
        return $req->fetchAll();
      }

      function getSpecificMatieresOfProgrammesClasses($lessonclasse,$lessonEtab,$libellesessionencours)
      {
          $req = $this->db->prepare("SELECT distinct fiche.mat_fiche FROM fiche,sousfiche,classe,matiere where fiche.classe_fiche=classe.id_classe and fiche.id_fiche=sousfiche.idfic_sousfic and fiche.mat_fiche=matiere.id_mat and classe.id_classe=? and classe.codeEtab_classe=? and classe.session_classe=?");
          $req->execute([$lessonclasse,$lessonEtab,$libellesessionencours]);
          return $req->fetchAll();
      }

      function getAllprogrammesOfTeatcherSession($teatcherid,$codeEtab,$libellesessionencours)
      {
        //$req = $this->db->prepare("SELECT * FROM programme where idprof_prog=? and codeEtab_prog=? and session_prog=?");
        $req = $this->db->prepare("SELECT * FROM programme,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and programme.idprof_prog=enseignant.idcompte_enseignant and programme.idmat_prog=matiere.id_mat and programme.idclasse_prog=classe.id_classe and enseignant.idcompte_enseignant=? and classe.codeEtab_classe=? and  programme.session_prog=? order by programme.id_prog ASC");
        $req->execute([$teatcherid,$codeEtab,$libellesessionencours]);
        return $req->fetchAll();
      }

      function getAllprogrammesOfTeatcherSessionClasses($codeEtab,$session)
      {
        $req = $this->db->prepare("SELECT * FROM programme,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and programme.idprof_prog=enseignant.idcompte_enseignant and programme.idmat_prog=matiere.id_mat and programme.idclasse_prog=classe.id_classe and classe.codeEtab_classe=? and  programme.session_prog=? order by programme.id_prog ASC");
        $req->execute([$codeEtab,$session]);
        return $req->fetchAll();
      }

      function getAllprogrammesOfTeatcherSessionActivities($teatcherid,$codeEtab,$libellesessionencours)
      {
        //$req = $this->db->prepare("SELECT * FROM programme where idprof_prog=? and codeEtab_prog=? and session_prog=?");
        $req = $this->db->prepare("SELECT * FROM syllabus,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and syllabus.idteatcher_syllab=enseignant.idcompte_enseignant and syllabus.idmatiere_syllab=matiere.id_mat and syllabus.idclasse_syllab=classe.id_classe and enseignant.idcompte_enseignant=? and classe.codeEtab_classe=? and  syllabus.session_syllab=? order by syllabus.id_syllab ASC");
        $req->execute([$teatcherid,$codeEtab,$libellesessionencours]);
        return $req->fetchAll();
      }

      function getAllprogrammesOfTeatcherSessionActivitiesEtab($classeid,$codeEtab,$libellesessionencours)
      {
        //$req = $this->db->prepare("SELECT * FROM programme where idprof_prog=? and codeEtab_prog=? and session_prog=?");
        $req = $this->db->prepare("SELECT * FROM syllabus,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and syllabus.idteatcher_syllab=enseignant.idcompte_enseignant and syllabus.idmatiere_syllab=matiere.id_mat and syllabus.idclasse_syllab=classe.id_classe and classe.id_classe=? and classe.codeEtab_classe=? and  syllabus.session_syllab=? order by syllabus.id_syllab ASC");
        $req->execute([$classeid,$codeEtab,$libellesessionencours]);
        return $req->fetchAll();
      }

      function getprogrammesallteatchers($classeEtab,$codeEtab,$libellesessionencours)
      {
    $req = $this->db->prepare("SELECT * FROM syllabus,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and syllabus.idteatcher_syllab=enseignant.idcompte_enseignant and syllabus.idmatiere_syllab=matiere.id_mat and syllabus.idclasse_syllab=classe.id_classe and classe.id_classe=? and classe.codeEtab_classe=?  and  syllabus.session_syllab=? order by syllabus.id_syllab ASC");
    $req->execute([$classeEtab,$codeEtab,$libellesessionencours]);
    return $req->fetchAll();
      }


      function getSyllabusInfos($programme,$teatcher,$codeEtab)
      {
          // $req = $this->db->prepare("SELECT * FROM syllabus,classe,matiere,syllabobjet,syllabtheme,syllabregle,syllabeval,syllabcalendar,syllabdoc,syllabcomp,syllabrequis where syllabus.idclasse_syllab=classe.id_classe and matiere.codeEtab_mat=classe.codeEtab_classe and syllabobjet.idsyllab_syllabob=syllabus.id_syllab and syllabtheme.idsyllab_syllabth=syllabus.id_syllab and syllabrequis.idsyllab_syllabreq=syllabus.id_syllab and syllabcomp.idsyllab_syllabcomp=syllabus.id_syllab and syllabdoc.idsyllab_syllabdoc=syllabus.id_syllab and syllabcalendar.idsyllab_syllabcal=syllabus.id_syllab and syllabeval.idsyllab_syllabeval=syllabus.id_syllab and  syllabregle.idsyllab_syllabregle=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=?");
          //
          $req = $this->db->prepare("SELECT * FROM syllabus,classe,matiere,compte  where syllabus.idclasse_syllab=classe.id_classe and matiere.codeEtab_mat=classe.codeEtab_classe  and compte.id_compte=syllabus.idteatcher_syllab  and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and classe.codeEtab_classe=?");
         $req->execute([$programme,$teatcher,$codeEtab]);
          return $req->fetchAll();
      }

      function getSyllabusObjectifsInfos($programme,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabobjet where syllabobjet.idsyllab_syllabob=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=?  ");
        $req->execute([$programme,$teatcher]);
         return $req->fetchAll();
      }
      function getSyllabusThemesInfos($programme,$teatcher,$codeEtab)
      {
            $req = $this->db->prepare("SELECT * FROM syllabus,syllabtheme where syllabtheme.idsyllab_syllabth=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
            $req->execute([$programme,$teatcher]);
            return $req->fetchAll();
      }

      function getSyllabusRequisInfos($programme,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabrequis where syllabrequis.idsyllab_syllabreq=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
        $req->execute([$programme,$teatcher]);
        return $req->fetchAll();
      }

      function getSyllabusCompInfos($programme,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabcomp where syllabcomp.idsyllab_syllabcomp=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
        $req->execute([$programme,$teatcher]);
        return $req->fetchAll();
      }

      function getSyllabusDocInfos($programme,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabdoc where syllabdoc.idsyllab_syllabdoc=syllabus.id_syllab and syllabdoc.facultatif_syllabdoc=0  and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
        $req->execute([$programme,$teatcher]);
        return $req->fetchAll();
      }

      function getSyllabusAllDocInfos($programme,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabdoc where syllabdoc.idsyllab_syllabdoc=syllabus.id_syllab and (syllabdoc.facultatif_syllabdoc=0 or syllabdoc.facultatif_syllabdoc=1 ) and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
        $req->execute([$programme,$teatcher]);
        return $req->fetchAll();
      }

      function getSyllabusDocfacInfos($programme,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabdoc where syllabdoc.idsyllab_syllabdoc=syllabus.id_syllab and syllabdoc.facultatif_syllabdoc=1  and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
        $req->execute([$programme,$teatcher]);
        return $req->fetchAll();
      }

      function getSyllabusCalendarInfos($programme,$teatcher,$codeEtab)
      {
          $req = $this->db->prepare("SELECT * FROM syllabus,syllabcalendar where syllabcalendar.idsyllab_syllabcal=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? order by date_syllabcal Asc ");
          $req->execute([$programme,$teatcher]);
          return $req->fetchAll();
      }

      function getSyllabusModeEvalInfos($programme,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabeval where syllabeval.idsyllab_syllabeval=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? order by date_syllabeval ASC");
        $req->execute([$programme,$teatcher]);
        return $req->fetchAll();
      }

      function getSyllabusRegleInfos($programme,$teatcher,$codeEtab)
      {
          $req = $this->db->prepare("SELECT * FROM syllabus, syllabregle  where syllabregle.idsyllab_syllabregle=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
        $req->execute([$programme,$teatcher]);
        return $req->fetchAll();
      }



      function ActivateNotificationState($idcompte,$session,$statut,$codeEtab)
      {
          $req= $this->db->prepare("UPDATE notificationstate set status_state=? where idparent_state=? and session_state=?");
          $req->execute([$statut,$idcompte,$session]);


      }

      function desActivateNotificationState($idcompte,$session,$statut,$codeEtab)
      {
        $req= $this->db->prepare("UPDATE notificationstate set status_state=? where idparent_state=? and session_state=?");
        $req->execute([$statut,$idcompte,$session]);


      }

      function getNotificationStateParent($idcompte,$libellesessionencours)
      {
          $req = $this->db->prepare("SELECT status_state FROM notificationstate where idparent_state=? and session_state=?");
          $req->execute([$idcompte,$libellesessionencours]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["status_state"];
          return $donnees;
      }

      function UpdateNotificationStatusAndParascolaires($notifid,$notificationsStatus,$codeEtab,$paraid,$parascolaireStatus)
      {
        $req= $this->db->prepare("UPDATE notification SET statut_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$notificationsStatus,$notifid,$codeEtab]);

        $req1= $this->db->prepare("UPDATE parascolaires SET statut_para=? where idnotif_para=? and id_para=? and codeEtab_para=?");
        $req1->execute([$parascolaireStatus,$notifid,$paraid,$codeEtab]);
      }

      function UpdateNotificationStatusAndParascolairesActivities($notifid,$notificationsStatus,$codeEtab,$paraid,$parascolaireStatus)
      {
        $req= $this->db->prepare("UPDATE messages SET statut_msg=? where statut_msg=? and codeEtab_msg=?");
        $req->execute([$notificationsStatus,$notifid,$codeEtab]);

        $req1= $this->db->prepare("UPDATE activites SET statut_act=? where idnotif_act=? and id_act=? and codeEtab_act=?");
        $req1->execute([$parascolaireStatus,$notifid,$paraid,$codeEtab]);
      }


      function NotificationAndParascolairesInfosUpdateWithOutPaieWithoutFile($libelactivity,$typesess,$datedeb,$datefin,$codeEtab,$libellesession,$paraid,$message,$montantAct,$smsvalue,$emailvalue,$notifid,$statutFile)
      {
        $payant=0;
        $req= $this->db->prepare("UPDATE parascolaires set libelle_para=?,debut_para=?,fin_para=?,payant_para=?,montant_para=? where id_para=? and idnotif_para=? and codeEtab_para=? and session_para=?");
        $req->execute([$libelactivity,$datedeb,$datefin,$payant,$montantAct,$paraid,$notifid,$codeEtab,$libellesession]);

        //modification dans notifications

        $req= $this->db->prepare("UPDATE notification set libelle_notif=?,message_notif=?,sms_notif=?,email_notif=?,join_notif=? where id_notif=? and codeEtab_notif=? and 	session_notif=?");
        $req->execute([$libelactivity,$message,$smsvalue,$emailvalue,$statutFile,$notifid,$codeEtab,$libellesession]);

      }

      function NotificationAndParascolairesInfosUpdateWithPaieWithoutFile($libelactivity,$typesess,$datedeb,$datefin,$codeEtab,$libellesession,$paraid,$message,$montantAct,$smsvalue,$emailvalue,$notifid,$statutFile)
      {
        $payant=1;
        $req= $this->db->prepare("UPDATE parascolaires set libelle_para=?,debut_para=?,fin_para=?,payant_para=?,montant_para=? where id_para=? and idnotif_para=? and codeEtab_para=? and session_para=?");
        $req->execute([$libelactivity,$datedeb,$datefin,$payant,$montantAct,$paraid,$notifid,$codeEtab,$libellesession]);

        //modification dans notifications

        $req= $this->db->prepare("UPDATE notification set libelle_notif=?,message_notif=?,sms_notif=?,email_notif=?,join_notif=? where id_notif=? and codeEtab_notif=? and 	session_notif=?");
        $req->execute([$libelactivity,$message,$smsvalue,$emailvalue,$statutFile,$notifid,$codeEtab,$libellesession]);


      }

      function NotificationAndParascolairesInfosUpdateWithPaie($libelactivity,$typesess,$datedeb,$datefin,$codeEtab,$libellesession,$paraid,$message,$montantAct,$smsvalue,$emailvalue,$notifid,$statutFile,$file)
      {
        $payant=1;
        $req= $this->db->prepare("UPDATE parascolaires set libelle_para=?,debut_para=?,fin_para=?,payant_para=?,montant_para=? where id_para=? and idnotif_para=? and codeEtab_para=? and session_para=?");
        $req->execute([$libelactivity,$datedeb,$datefin,$payant,$montantAct,$paraid,$notifid,$codeEtab,$libellesession]);

        //modification dans notifications

        $req= $this->db->prepare("UPDATE notification set libelle_notif=?,message_notif=?,sms_notif=?,email_notif=?,join_notif=?,fichier_notif=? where id_notif=? and codeEtab_notif=? and 	session_notif=?");
        $req->execute([$libelactivity,$message,$smsvalue,$emailvalue,$statutFile,$file,$notifid,$codeEtab,$libellesession]);


      }

      function NotificationAndParascolairesInfosUpdateWithOutPaie($libelactivity,$typesess,$datedeb,$datefin,$codeEtab,$libellesession,$paraid,$message,$montantAct,$smsvalue,$emailvalue,$notifid,$statutFile,$file)
      {
        $payant=0;
        $req= $this->db->prepare("UPDATE parascolaires set libelle_para=?,debut_para=?,fin_para=?,payant_para=?,montant_para=? where id_para=? and idnotif_para=? and codeEtab_para=? and session_para=?");
        $req->execute([$libelactivity,$datedeb,$datefin,$payant,$montantAct,$paraid,$notifid,$codeEtab,$libellesession]);

        //modification dans notifications

        $req= $this->db->prepare("UPDATE notification set libelle_notif=?,message_notif=?,sms_notif=?,email_notif=?,join_notif=?,fichier_notif=? where id_notif=? and codeEtab_notif=? and 	session_notif=?");
        $req->execute([$libelactivity,$message,$smsvalue,$emailvalue,$statutFile,$file,$notifid,$codeEtab,$libellesession]);

      }



      function UpdateDestinatairesNotificationAndParascolaire($destinataires,$paraid,$codeEtab,$libellesession,$notifid)
      {
        $req1= $this->db->prepare("UPDATE parascolaires set classes_para=? where idnotif_para=? and codeEtab_para=? and session_para=? and id_para=?");
        $req1->execute([$classes,$notifid,$codeEtab,$libellesession,$paraid]);
      }

      function UpdateClassesNotificationAndParascolaire($classes,$paraid,$codeEtab,$libellesession,$notifid)
      {
        $req= $this->db->prepare("UPDATE notification set classes_notif=? where id_notif=? and codeEtab_notif=? and session_notif=?");
        $req->execute([$classes,$notifid,$codeEtab,$libellesession]);

        //modification dans Parascolaires
          $req1= $this->db->prepare("UPDATE parascolaires set classes_para=? where idnotif_para=? and codeEtab_para=? and session_para=? and id_para=?");
          $req1->execute([$classes,$notifid,$codeEtab,$libellesession,$paraid]);
      }

      function getParascolairesOfThisSchool($codeEtabLocal,$libellesessionencours,$paraid)
      {
          $req = $this->db->prepare("SELECT * FROM parascolaires,notification where parascolaires.idnotif_para=notification.id_notif and notification.para_notif=1 and parascolaires.codeEtab_para=? and parascolaires.session_para=? and parascolaires.id_para=?");
          $req->execute([$codeEtabLocal,$libellesessionencours,$paraid]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["libelle_para"]."*".$someArray[0]["typesession_para"]."*".$someArray[0]["debut_para"]."*".$someArray[0]["fin_para"];
          $donnees.="*".$someArray[0]["message_notif"]."*".$someArray[0]["join_notif"]."*".$someArray[0]["fichier_notif"];
          $donnees.="*".$someArray[0]["classes_notif"]."*".$someArray[0]["destinataires_notif"]."*".$someArray[0]["payant_para"];
          $donnees.="*".$someArray[0]["montant_para"]."*".$someArray[0]["sms_notif"]."*".$someArray[0]["email_notif"]."*".$someArray[0]["date_notif"]."*".$someArray[0]["id_notif"]."*".$someArray[0]["date_notif"];
          return $donnees;

      }

      function getAllParascolairesOfThisSchool($codeEtabLocal,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM parascolaires where codeEtab_para=? and  session_para=? and  statut_para not in (0,3)");
        $req->execute([$codeEtabLocal,$libellesessionencours]);
        return $req->fetchAll();

      }

      function getAllParascolairesOfThisSchoolclasses($id_para,$classeid,$session,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM parascolaires where id_para=? and session_para=? and codeEtab_para=? and statut_para=2");
        $req->execute([$id_para,$session,$codeEtab]);
        return $req->fetchAll();

      }

      function getAllNotificationbySchoolCodesender($id_msg,$classeid,$session,$codeEtab)
      {
          $req = $this->db->prepare("SELECT * FROM messages where id_msg=? and session_msg=? and codeEtab_msg=? and statut_msg=1");
          $req->execute([$id_msg,$session,$codeEtab]);
          return $req->fetchAll();
      }



      function AddParascolaireWithPaiement($libelactivity,$datedeb,$datefin,$codeEtab,$libellesession,$typesess,$montantAct,$paiecheck,$classes,$idnotif,$statutpara)
      {
        $req = $this->db->prepare("INSERT INTO parascolaires SET 	libelle_para=?,debut_para=?,fin_para=?,codeEtab_para=?,session_para=?,typesession_para=?,montant_para=?,payant_para=?,classes_para=?,idnotif_para=?,statut_para=?");
        $req->execute([$libelactivity,$datedeb,$datefin,$codeEtab,$libellesession,$typesess,$montantAct,$paiecheck,$classes,$idnotif,$statutpara]);


      }

      function AddParascolaireWithOutPaiement($libelactivity,$datedeb,$datefin,$codeEtab,$libellesession,$typesess,$paiecheck,$classes,$idnotif,$statutpara)
      {
          $req = $this->db->prepare("INSERT INTO parascolaires SET 	libelle_para=?,debut_para=?,fin_para=?,codeEtab_para=?,session_para=?,typesession_para=?,payant_para=?,classes_para=?,idnotif_para=?,statut_para=?");
          $req->execute([$libelactivity,$datedeb,$datefin,$codeEtab,$libellesession,$typesess,$paiecheck,$classes,$idnotif,$statutpara]);

      }

      function ArchivedNotification($status,$notifid,$session,$codeEtab)
      {
        $req= $this->db->prepare("UPDATE notification SET statut_notif=? where id_notif=? and session_notif=? and codeEtab_notif=?");
        $req->execute([$status,$notifid,$session,$codeEtab]);

      }

      function ArchivedNotificationActivities($status,$notifid,$session,$codeEtab)
      {
        $req= $this->db->prepare("UPDATE messages SET statut_msg=? where id_msg=? and session_msg=? and codeEtab_msg=?");
        $req->execute([$status,$notifid,$session,$codeEtab]);

      }


      function DeleteNotificationParasco($paraid,$notifid,$statusNotification,$statusparasco)
      {
        $req = $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=?");
        $req->execute([$statusNotification,$notifid]);

        $req1 = $this->db->prepare("UPDATE parascolaires set statut_para=? where idnotif_para=? and id_para=?");
        $req1->execute([$statusparasco,$notifid,$paraid]);




      }

      function DeleteNotificationParascoActivity($paraid,$notifid,$statusNotification,$statusparasco)
      {
        $req = $this->db->prepare("UPDATE messages set statut_msg=? where id_msg=?");
        $req->execute([$statusNotification,$notifid]);

        $req1 = $this->db->prepare("UPDATE activites set statut_act=? where idnotif_act=? and id_act=?");
        $req1->execute([$statusparasco,$notifid,$paraid]);



      }

      function DeleteNotificationParascoActivityMixte($paraid,$notifid,$statusNotification,$statusparasco)
      {
        $req = $this->db->prepare("UPDATE messages set statut_msg=? where id_msg=?");
        $req->execute([$statusNotification,$notifid]);

        $req1 = $this->db->prepare("UPDATE activites set statut_act=? where idnotif_act=? and id_act=?");
        $req1->execute([$statusparasco,$notifid,$paraid]);




      }


      function DeleteNotification($notifid,$statusNotification)
      {
        // $req = $this->db->prepare("DELETE FROM notification where id_notif=?");
        // $req->execute([$notifid]);
        $req = $this->db->prepare("UPDATE notification set statut_notif=? where id_notif=?");
        $req->execute([$statusNotification,$notifid]);

      }

      function DeleteNotificationActivities($notifid,$statusNotification)
      {
        // $req = $this->db->prepare("DELETE FROM notification where id_notif=?");
        // $req->execute([$notifid]);
        $req = $this->db->prepare("UPDATE messages set statut_msg=? where id_msg=?");
        $req->execute([$statusNotification,$notifid]);

      }

      function DeleteNotificationActivitiesMixte($notifid,$statusNotification)
      {
        // $req = $this->db->prepare("DELETE FROM notification where id_notif=?");
        // $req->execute([$notifid]);
        $req = $this->db->prepare("UPDATE messages set statut_msg=? where id_msg=?");
        $req->execute([$statusNotification,$notifid]);

      }


      function UpdateNotificationStatus($notifid,$notificationsStatus,$codeEtab)
      {
        $req= $this->db->prepare("UPDATE notification SET statut_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$notificationsStatus,$notifid,$codeEtab]);
      }

      function UpdateNotificationStatusActivities($notifid,$notificationsStatus,$codeEtab)
      {
        $req= $this->db->prepare("UPDATE messages SET statut_msg=? where id_msg=? and codeEtab_msg=?");
        $req->execute([$notificationsStatus,$notifid,$codeEtab]);
      }

      function SendAddedExamenNotesMailerToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$sessionlibelle,$codeEtab)
      {
        require_once('../PHPMailer/class.phpmailer.php');
        require_once('../PHPMailer/class.smtp.php');
        require_once('../controller/functions.php');
        $client1="justearmel04@gmail.com";
        $client2="fabienekoute@gmail.com";
        $mail = new PHPMailer();
        $mail->isHTML(true);
        $mail->CharSet="UTF-8";
        $mail->isSMTP();
        $mail->SMTPOptions = array (
       'ssl' => array(
       'verify_peer'  => false,
       'verify_peer_name'  => false,
       'allow_self_signed' => true));
       $mail->Host='mail.proximity-cm.com';
       $mail->SMTPAuth = true;
       $mail->Port = 25;
       $mail->SMTPSecure = "tls";
       $mail->Username = "xschool@proximity-cm.com";
       $mail->Password ="123psa@456";
       $mail->From='xschool@proximity-cm.com';
       $mail->FromName=$libelleEtab;
       $mail->AddAddress($client1);
       $mail->AddAddress($client2);
       $mail->AddAddress($destimails);
       $mail->Subject ="Modification de Note";
       $mail->Body = "Bonjour Cher Parent<br>";
       $mail->Body .="La direction de l'établissement ".$libelleEtab." tiens à vous informer de l'ajout d'une nouvelle de note d'examen de votre enfant  ".$nomcompletStudent."<br>";
       $mail->Body .="régulièrement inscrit en classe de ". $libelleclasse ." au cours de ". $libellematiere;
       $mail->Body .="<br>";
       $mail->Body .="Merci de vous connecter à la plateforme afin de consulter <br>";
       $mail->Body .="Cordialement<br>";
       $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
       $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");

       if(!$mail->Send())

       {

       $msg="nok";

       }else
       {

       $msg="ok";

       }

  return $msg;
      }

      function Addnotecontrolessmssending($destiphone,$libellematiere,$notes,$nomEleve)
      {
        $etabs=new Etab();

        $message="Bonjour Chers Parent,";
        $message.="l'élève ".$nomEleve." à obtenu la note de ".$notes." au contrôle de".$libellematiere;
        // $etabs->clicksendersms($destiphone,$message);
        $etabs->d7networkssmssender($destiphone,$message);
      }

      function SendAddedControleNotesMailerToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$sessionlibelle,$codeEtab,$libellematiere)
      {
        require_once('../PHPMailer/class.phpmailer.php');
        require_once('../PHPMailer/class.smtp.php');
        require_once('../controller/functions.php');
        $client1="justearmel04@gmail.com";
        $client2="fabienekoute@gmail.com";
        $mail = new PHPMailer();
        $mail->isHTML(true);
        $mail->CharSet="UTF-8";
        $mail->isSMTP();
        $mail->SMTPOptions = array (
       'ssl' => array(
       'verify_peer'  => false,
       'verify_peer_name'  => false,
       'allow_self_signed' => true));
       $mail->Host='mail.proximity-cm.com';
       $mail->SMTPAuth = true;
       $mail->Port = 25;
       $mail->SMTPSecure = "tls";
       $mail->Username = "xschool@proximity-cm.com";
       $mail->Password ="123psa@456";
       $mail->From='xschool@proximity-cm.com';
       $mail->FromName=$libelleEtab;
       $mail->AddAddress($client1);
       $mail->AddAddress($client2);
       $mail->AddAddress($destimails);
       $mail->Subject ="Nouvelle Note";
       $mail->Body = "Bonjour Cher Parent<br>";
       $mail->Body .="La direction de l'établissement ".$libelleEtab." tiens à vous informer de l'ajout d'une nouvelle de note de contrôle de votre enfant  ".$nomcompletStudent."<br>";
       $mail->Body .="régulièrement inscrit en classe de ". $libelleclasse ." au cours de ". $libellematiere;
       $mail->Body .="<br>";
       $mail->Body .="Merci de vous connecter à la plateforme afin de consulter <br>";
       $mail->Body .="Cordialement<br>";
       $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
       $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");

       if(!$mail->Send())

       {

       $msg="nok";

       }else
       {

       $msg="ok";

       }

  return $msg;

      }

      function SendModificationNotesToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$libellematiere,$codeEtab)
      {
        require_once('../PHPMailer/class.phpmailer.php');
        require_once('../PHPMailer/class.smtp.php');
        require_once('../controller/functions.php');
        $client1="justearmel04@gmail.com";
        $client2="fabienekoute@gmail.com";
        $mail = new PHPMailer();
        $mail->isHTML(true);
        $mail->CharSet="UTF-8";
        $mail->isSMTP();
        $mail->SMTPOptions = array (
       'ssl' => array(
       'verify_peer'  => false,
       'verify_peer_name'  => false,
       'allow_self_signed' => true));
       $mail->Host='mail.proximity-cm.com';
       $mail->SMTPAuth = true;
       $mail->Port = 25;
       $mail->SMTPSecure = "tls";
       $mail->Username = "xschool@proximity-cm.com";
       $mail->Password ="123psa@456";
       $mail->From='xschool@proximity-cm.com';
       $mail->FromName=$libelleEtab;
       $mail->AddAddress($client1);
       $mail->AddAddress($client2);
       $mail->AddAddress($destimails);
       $mail->Subject ="Modification de Note";
       $mail->Body = "Bonjour Cher Parent<br>";
       $mail->Body .="La direction de l'établissement ".$libelleEtab." tiens à vous informer d'une modification de note de votre enfant  ".$nomcompletStudent."<br>";
       $mail->Body .="régulièrement inscrit en classe de ". $libelleclasse ." au cours de ". $libellematiere;
       $mail->Body .="<br>";
       $mail->Body .="Merci de vous connecter à la plateforme afin de consulter <br>";
       $mail->Body .="Cordialement<br>";
       $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
       $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");

       if(!$mail->Send())

       {

       $msg="nok";

       }else
       {

       $msg="ok";

       }

  return $msg;

      }

    function SendNotifiactionWithToDestinataires($destimails,$titremessage,$contenumessage,$libelleEtab,$logoEtab,$codeEtab,$datenotif,$file)
    {
      require_once('../PHPMailer/class.phpmailer.php');
      require_once('../PHPMailer/class.smtp.php');
      require_once('../controller/functions.php');
      $day=date("d-m-Y");
      $client1="justearmel04@gmail.com";
      $client2="fabienekoute@gmail.com";
      $mail = new PHPMailer();
      $mail->isHTML(true);
      $mail->CharSet="UTF-8";
      $mail->isSMTP();
      $mail->SMTPOptions = array (
     'ssl' => array(
     'verify_peer'  => false,
     'verify_peer_name'  => false,
     'allow_self_signed' => true));
     $mail->Host='mail.proximity-cm.com';
     $mail->SMTPAuth = true;
     $mail->Port = 25;
     $mail->SMTPSecure = "tls";
     $mail->Username = "xschool@proximity-cm.com";
     $mail->Password ="123psa@456";
     $mail->From='xschool@proximity-cm.com';
     $mail->FromName=$libelleEtab;
     $mail->AddAddress($client1);
     $mail->AddAddress($client2);
     $tabdesti=explode('*',$destimails);
     $nb=count($tabdesti);

     for($i=0;$i<$nb;$i++){
       $mail->AddAddress($tabdesti[$i]);
     }
     $mail->Subject =$titremessage;
     $mail->Body =$contenumessage;
     $mail->Body .="<br>";
     $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
     $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");
     $mail->addAttachment("../notifications/".$datenotif."/".$file);

     if(!$mail->Send())

     {

     $msg="nok";

     }else
     {

     $msg="ok";

     }

return $msg;



    }

    function ParentsRoutineUpdateMailerOne($mailteatcher,$libelleEtab,$libelleclasse,$libellematieres,$codeEtab,$logoEtab,$libellejourold,$heuredeb,$heurefin)
    {
      require_once('../PHPMailer/class.phpmailer.php');
      require_once('../PHPMailer/class.smtp.php');
      require_once('../controller/functions.php');

      $titremessage="Modification Emploi du temps";
      $client1="justearmel04@gmail.com";
      $client2="fabienekoute@gmail.com";
      $mail = new PHPMailer();
      $mail->isHTML(true);
      $mail->CharSet="UTF-8";
      $mail->isSMTP();
      $mail->SMTPOptions = array (
     'ssl' => array(
     'verify_peer'  => false,
     'verify_peer_name'  => false,
     'allow_self_signed' => true));
     $mail->Host='mail.proximity-cm.com';
     $mail->SMTPAuth = true;
     $mail->Port = 25;
     $mail->SMTPSecure = "tls";
     $mail->Username = "xschool@proximity-cm.com";
     $mail->Password ="123psa@456";
     $mail->From='xschool@proximity-cm.com';
     $mail->FromName=$libelleEtab;
     $mail->AddAddress($client1);
     $mail->AddAddress($client2);
     $mail->AddAddress($mailteatcher);
     $mail->Subject =$titremessage;
     $mail->Body = "Bonjour Chers Professeur<br>";
     $mail->Body .="La direction de l'établissement ".$libelleEtab." tiens à vous informer d'une modification dans votre emplois du temps de votre enfant";
     $mail->Body .="<br>";
     $mail->Body .="désormais le cours de " . $libellematieres."  se déroulera le ".$libellejourold ."  de ".$heuredeb . "à ".$heurefin ." pour la classe de " .$libelleclasse;
     $mail->Body .="<br>";
     $mail->Body .="La Direction vous remercie de vôtre compréhension";
     $mail->Body .="<br>";
     $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
     $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");
     if(!$mail->Send())

     {

     $msg="nok";

     }else
     {

     $msg="ok";

     }

  return $msg;
    }

  function TeatcherRoutineUpdateMailerOne($mailteatcher,$libelleEtab,$libelleclasse,$libellematieres,$codeEtab,$logoEtab,$libellejourold,$heuredeb,$heurefin)
  {
    require_once('../PHPMailer/class.phpmailer.php');
    require_once('../PHPMailer/class.smtp.php');
    require_once('../controller/functions.php');

    $titremessage="Modification Emploi du temps";
    $client1="justearmel04@gmail.com";
    $client2="fabienekoute@gmail.com";
    $mail = new PHPMailer();
    $mail->isHTML(true);
    $mail->CharSet="UTF-8";
    $mail->isSMTP();
    $mail->SMTPOptions = array (
   'ssl' => array(
   'verify_peer'  => false,
   'verify_peer_name'  => false,
   'allow_self_signed' => true));
   $mail->Host='mail.proximity-cm.com';
   $mail->SMTPAuth = true;
   $mail->Port = 25;
   $mail->SMTPSecure = "tls";
   $mail->Username = "xschool@proximity-cm.com";
   $mail->Password ="123psa@456";
   $mail->From='xschool@proximity-cm.com';
   $mail->FromName=$libelleEtab;
   $mail->AddAddress($client1);
   $mail->AddAddress($client2);
   $mail->AddAddress($mailteatcher);
   $mail->Subject =$titremessage;
   $mail->Body = "Bonjour Chers Parent<br>";
   $mail->Body .="La direction de l'établissement ".$libelleEtab." tiens à vous informer d'une modification dans votre emplois du temps de votre enfant";
   $mail->Body .="<br>";
   $mail->Body .="désormais le cours de " . $libellematieres."  se déroulera le ".$libellejourold ."  de ".$heuredeb . "à ".$heurefin ." pour la classe de " .$libelleclasse;
   $mail->Body .="<br>";
   $mail->Body .="La Direction vous remercie de vôtre compréhension";
   $mail->Body .="<br>";
   $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
   $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");
   if(!$mail->Send())

   {

   $msg="nok";

   }else
   {

   $msg="ok";

   }

return $msg;
  }


      function TeatcherRoutineUpdateMailer($mailteatcher,$libelleclasse,$libellematieres,$libellejourold,$libelledaymodify,$debhourold,$heuredeb,$finhourold,$heurefin,$libelleEtab,$logoEtab,$codeEtab)
      {
        require_once('../PHPMailer/class.phpmailer.php');
        require_once('../PHPMailer/class.smtp.php');
        require_once('../controller/functions.php');

        $titremessage="Modification Emploi du temps";

        $client1="justearmel04@gmail.com";
        $client2="fabienekoute@gmail.com";
        $mail = new PHPMailer();
        $mail->isHTML(true);
        $mail->CharSet="UTF-8";
        $mail->isSMTP();
        $mail->SMTPOptions = array (
       'ssl' => array(
       'verify_peer'  => false,
       'verify_peer_name'  => false,
       'allow_self_signed' => true));
       $mail->Host='mail.proximity-cm.com';
       $mail->SMTPAuth = true;
       $mail->Port = 25;
       $mail->SMTPSecure = "tls";
       $mail->Username = "xschool@proximity-cm.com";
       $mail->Password ="123psa@456";
       $mail->From='xschool@proximity-cm.com';
       $mail->FromName=$libelleEtab;
       $mail->AddAddress($client1);
       $mail->AddAddress($client2);
       $mail->AddAddress($mailteatcher);
       $mail->Subject =$titremessage;
       $mail->Body = "Bonjour Chers Professeur<br>";
       $mail->Body .="La direction de l'établissement ".$libelleEtab." tiens à vous informer d'une modification dans votre emploi du temps pour les cours de ".$libellematieres;
       $mail->Body .="<br>";
       $mail->Body .="Initialement prévu chaque ".$libellejourold ."  de ".$debhourold . "à ".$finhourold ." pour la classe de " .$libelleclasse . "  qui se déroulerons désormais chaque ".$libelledaymodify . " de ".$heuredeb . " à ".$heurefin  ;
       $mail->Body .="<br>";
       $mail->Body .="La Direction vous remercie de vôtre compréhension";
       $mail->Body .="<br>";
       $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
       $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");
       if(!$mail->Send())

       {

       $msg="nok";

       }else
       {

       $msg="ok";

       }

  return $msg;

      }

      function SendSyllabusModifiedbyCodeEtab($localMails,$libelleEtab,$logoEtab,$teatcherName,$libellemat,$classelibelle,$codeEtab)
      {
        require_once('../PHPMailer/class.phpmailer.php');
        require_once('../PHPMailer/class.smtp.php');
        require_once('../controller/functions.php');

        $titremessage="Modification  Programme scolaire";
        $client1="justearmel04@gmail.com";
        $client2="fabienekoute@gmail.com";
        $mail = new PHPMailer();
        $mail->isHTML(true);
        $mail->CharSet="UTF-8";
        $mail->isSMTP();
        $mail->SMTPOptions = array (
       'ssl' => array(
       'verify_peer'  => false,
       'verify_peer_name'  => false,
       'allow_self_signed' => true));
       $mail->Host='mail.proximity-cm.com';
       $mail->SMTPAuth = true;
       $mail->Port = 25;
       $mail->SMTPSecure = "tls";
       $mail->Username = "xschool@proximity-cm.com";
       $mail->Password ="123psa@456";
       $mail->From='xschool@proximity-cm.com';
       $mail->FromName=$libelleEtab;
       $mail->AddAddress($client1);
       $mail->AddAddress($client2);
       $tabdesti=explode('*',$localMails);
       $nb=count($tabdesti);

       for($i=0;$i<$nb;$i++){
         $mail->AddAddress($tabdesti[$i]);
       }
       $mail->Subject =$titremessage;
       $mail->Body = "Bonjour Responsable<br>";
       $mail->Body = "Le programme academique du cours ".$libellemat ." dispenser par le professeur ".$teatcherName . " dans la classe de ".$classelibelle . " à bien été modifié ce jour";
       $mail->Body .="<br>";
       $mail->Body .="Merci de prendre connaissance du programme ";
       $mail->Body .="<br>";
       $mail->Body .="Cordialement";
       $mail->Body .="<br>";
       $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
       $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");
       if(!$mail->Send())

       {

       $msg="nok";

       }else
       {

       $msg="ok";

       }

  return $msg;



      }

      function SendSyllabusCreatedbyCodeEtab($codeEtab,$localMails,$libelleEtab,$logoEtab,$libellemat,$libelleclasse,$teatcherName)
      {
        require_once('../PHPMailer/class.phpmailer.php');
        require_once('../PHPMailer/class.smtp.php');
        require_once('../controller/functions.php');

        $titremessage="Ajout de Programme scolaire";

        $client1="justearmel04@gmail.com";
        $client2="fabienekoute@gmail.com";
        $mail = new PHPMailer();
        $mail->isHTML(true);
        $mail->CharSet="UTF-8";
        $mail->isSMTP();
        $mail->SMTPOptions = array (
       'ssl' => array(
       'verify_peer'  => false,
       'verify_peer_name'  => false,
       'allow_self_signed' => true));
       $mail->Host='mail.proximity-cm.com';
       $mail->SMTPAuth = true;
       $mail->Port = 25;
       $mail->SMTPSecure = "tls";
       $mail->Username = "xschool@proximity-cm.com";
       $mail->Password ="123psa@456";
       $mail->From='xschool@proximity-cm.com';
       $mail->FromName=$libelleEtab;
       $mail->AddAddress($client1);
       $mail->AddAddress($client2);
       $tabdesti=explode('*',$localMails);
       $nb=count($tabdesti);

       for($i=0;$i<$nb;$i++){
         $mail->AddAddress($tabdesti[$i]);
       }
       $mail->Subject =$titremessage;
        $mail->Body = "Bonjour Responsable<br>";
        $mail->Body = "Le programme academique du cours ".$libellemat ." dispenser par le professeur ".$teatcherName . " dans la classe de ".$libelleclasse . " à bien été ajouter ce jour";
        $mail->Body .="<br>";
        $mail->Body .="Merci de prendre connaissance du programme ";
        $mail->Body .="<br>";
        $mail->Body .="Cordialement";
        $mail->Body .="<br>";
        $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
        $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");
        if(!$mail->Send())

        {

        $msg="nok";

        }else
        {

        $msg="ok";

        }

   return $msg;

      }

      function ParentsRoutineUpdateMailer($parentsMails,$libelleclasse,$libellematieres,$libellejourold,$libelledaymodify,$debhourold,$heuredeb,$finhourold,$heurefin,$libelleEtab,$logoEtab,$codeEtab)
      {
        require_once('../PHPMailer/class.phpmailer.php');
        require_once('../PHPMailer/class.smtp.php');
        require_once('../controller/functions.php');

        $titremessage="Modification Emploi du temps";

        $client1="justearmel04@gmail.com";
        $client2="fabienekoute@gmail.com";
        $mail = new PHPMailer();
        $mail->isHTML(true);
        $mail->CharSet="UTF-8";
        $mail->isSMTP();
        $mail->SMTPOptions = array (
       'ssl' => array(
       'verify_peer'  => false,
       'verify_peer_name'  => false,
       'allow_self_signed' => true));
       $mail->Host='mail.proximity-cm.com';
       $mail->SMTPAuth = true;
       $mail->Port = 25;
       $mail->SMTPSecure = "tls";
       $mail->Username = "xschool@proximity-cm.com";
       $mail->Password ="123psa@456";
       $mail->From='xschool@proximity-cm.com';
       $mail->FromName=$libelleEtab;
       $mail->AddAddress($client1);
       $mail->AddAddress($client2);
       $tabdesti=explode('*',$parentsMails);
       $nb=count($tabdesti);

       for($i=0;$i<$nb;$i++){
         $mail->AddAddress($tabdesti[$i]);
       }
       $mail->Subject =$titremessage;
       $mail->Body = "Bonjour Chers Parent<br>";
       $mail->Body .="La direction de l'établissement ".$libelleEtab." tiens à vous informer d'une modification dans l'emploi du temps de votre enfant pour les cours de ".$libellematieres;
       $mail->Body .="<br>";
       $mail->Body .="Initialement prévu chaque ".$libellejourold ."  de ".$debhourold . "à ".$finhourold ." pour la classe de " .$libelleclasse . "  qui se déroulerons désormais chaque ".$libelledaymodify . " de ".$heuredeb . " à ".$heurefin  ;
       $mail->Body .="<br>";
       $mail->Body .="La Direction vous remercie de vôtre compréhension";
       $mail->Body .="<br>";
       $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
       $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");
       if(!$mail->Send())

       {

       $msg="nok";

       }else
       {

       $msg="ok";

       }

  return $msg;
      }

      function SendNotifiactionWithoutToDestinataires($destimails,$titremessage,$contenumessage,$libelleEtab,$logoEtab,$codeEtab)
      {
        require_once('../PHPMailer/class.phpmailer.php');
        require_once('../PHPMailer/class.smtp.php');
        require_once('../controller/functions.php');

        $day=date("d-m-Y");
        $client1="justearmel04@gmail.com";
        $client2="fabienekoute@gmail.com";
        $mail = new PHPMailer();
         $mail->isHTML(true);
         $mail->CharSet="UTF-8";
         $mail->isSMTP();
         $mail->SMTPOptions = array (
        'ssl' => array(
        'verify_peer'  => false,
        'verify_peer_name'  => false,
        'allow_self_signed' => true));
        $mail->Host='mail.proximity-cm.com';
        $mail->SMTPAuth = true;
        $mail->Port = 25;
        $mail->SMTPSecure = "tls";
        $mail->Username = "xschool@proximity-cm.com";
        $mail->Password ="123psa@456";
        $mail->From='xschool@proximity-cm.com';
        $mail->FromName=$libelleEtab;
        $mail->AddAddress($client1);
        $mail->AddAddress($client2);
        $tabdesti=explode('*',$destimails);
        $nb=count($tabdesti);

        for($i=0;$i<$nb;$i++){
          $mail->AddAddress($tabdesti[$i]);
        }
        $mail->Subject =$titremessage;
        $mail->Body =$contenumessage;
        $mail->Body .="<br>";
        $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
        $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");

        if(!$mail->Send())

        {

        $msg="nok";

        }else
        {

        $msg="ok";

        }
        return $msg;

      }

      function deleteRoutineById($routineid,$classe)
      {
        $req = $this->db->prepare("DELETE FROM routine where id_route=?");
        $req->execute([$routineid]);

      }

      function deleteRoutinesById($routineid,$classeid,$codeEtab)
      {
        $req = $this->db->prepare("DELETE FROM routine where id_route=?");
        $req->execute([$routineid]);
        $_SESSION['user']['updateroutineok']=" Cours supprimer avec succès";

         // header("Location:../manager/matieres.php?classe=".$classe);
         if($_SESSION['user']['profile'] == "Admin_globale") {

               //header("Location:../manager/index.php");
               header("Location:../manager/routines.php?classe=".$classeid."&codeEtab=".$codeEtab);

           }else if($_SESSION['user']['profile'] == "Admin_locale") {
             if($_SESSION['user']['paysid']==4)
             {
               header("Location:../localecmr/routines.php?classe=".$classeid);
             }else {
               header("Location:../locale/routines.php?classe=".$classeid);
             }


             }
      }

      function getIndictatifOfThisSchool($codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement,pays where etablissement.pays_etab=pays.id_pays and etablissement.code_etab=?");
        $req->execute([$codeEtab]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["indicatif_pays"];
        return $donnees;
      }

      function getnotescrontroleInformations($noteid,$eleveid,$codeEtab,$typenote)
      {
          $req = $this->db->prepare("SELECT * FROM notes where id_notes=? and ideleve_notes=? and codeEtab_notes=? and 	type_notes=?");
          $req->execute([$noteid,$eleveid,$codeEtab,$typenote]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["idtype_notes"]."*".$someArray[0]["idclasse_notes"]."*".$someArray[0]["idmat_notes"]."*".$someArray[0]["idprof_notes"]."*".$someArray[0]["session_notes"];
          return $donnees;
      }

      function getnotescrontroleInformationsecondary($noteid,$eleveid,$codeEtab,$typenote)
      {
          $req = $this->db->prepare("SELECT * FROM notes,controle where notes.idtype_notes=controle.id_ctrl and type_notes=?  id_notes=? and ideleve_notes=? and codeEtab_notes=? ");
          $req->execute([$typenote,$noteid,$eleveid,$codeEtab]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["idtype_notes"]."*".$someArray[0]["idclasse_notes"]."*".$someArray[0]["idmat_notes"]."*".$someArray[0]["idprof_notes"]."*".$someArray[0]["session_notes"]."*".$someArray[0]["typesess_ctrl"];
          return $donnees;
      }

      function getnotesexamenInformations($noteid,$eleveid,$codeEtab,$typenote)
      {
          $req = $this->db->prepare("SELECT * FROM notes where id_notes=? and ideleve_notes=? and codeEtab_notes=? and 	type_notes=?");
          $req->execute([$noteid,$eleveid,$codeEtab,$typenote]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["idtype_notes"]."*".$someArray[0]["idclasse_notes"]."*".$someArray[0]["idmat_notes"]."*".$someArray[0]["idprof_notes"]."*".$someArray[0]["session_notes"];
          return $donnees;
      }
      function Updatenotesmodification($statut,$datemodif,$userid,$pisteid,$idtypenote,$typenote,$eleveid,$classeid,$teatcherid)
      {
          $req = $this->db->prepare("UPDATE notesmodification set statut_modifnote=?,date_validation=?,validateur=? where eleveid_modifnote=? and	idpiste_modifnote=? and controle_modifnote=1 and designationid_modifnote=? and classeid_modifnote=? and teatcherid_modifnote=?");
          $req->execute([$statut,$datemodif,$userid,$eleveid,$pisteid,$idtypenote,$classeid,$teatcherid]);

      }

      function UpdatenotesmodificationNew($statut,$datemodif,$userid,$pisteid,$idtypenote,$typenote,$eleveid,$classeid,$teatcherid,$modifnoteid)
      {
          $req = $this->db->prepare("UPDATE notesmodification set statut_modifnote=?,date_validation=?,validateur=? where id_modifnote=? and eleveid_modifnote=? and	idpiste_modifnote=? and controle_modifnote=1 and designationid_modifnote=? and classeid_modifnote=? and teatcherid_modifnote=?");
          $req->execute([$statut,$datemodif,$userid,$modifnoteid,$eleveid,$pisteid,$idtypenote,$classeid,$teatcherid]);

      }

      function DesactivationmodificationNotes($statut,$datemodif,$userid,$pisteid,$idtypenote,$typenote,$eleveid,$classeid,$teatcherid)
      {
        $req = $this->db->prepare("UPDATE notesmodification set statut_modifnote=?,date_validation=?,validateur=? where eleveid_modifnote=? and	idpiste_modifnote=? and controle_modifnote=1 and designationid_modifnote=? and classeid_modifnote=? and teatcherid_modifnote=?");
        $req->execute([$statut,$datemodif,$userid,$eleveid,$pisteid,$idtypenote,$classeid,$teatcherid]);

      }

      function DesactivationmodificationNotesExam($statut,$datemodif,$userid,$pisteid,$idtypenote,$typenote,$eleveid,$classeid,$teatcherid)
      {
        $req = $this->db->prepare("UPDATE notesmodification set statut_modifnote=?,date_validation=?,validateur=? where eleveid_modifnote=? and	idpiste_modifnote=? and examen_modifnote=1 and designationid_modifnote=? and classeid_modifnote=? and teatcherid_modifnote=?");
        $req->execute([$statut,$datemodif,$userid,$eleveid,$pisteid,$idtypenote,$classeid,$teatcherid]);

      }


      function Updatenotesmodificationexam($statut,$datemodif,$userid,$pisteid,$idtypenote,$typenote,$eleveid,$classeid,$teatcherid)
      {
          $req = $this->db->prepare("UPDATE notesmodification set statut_modifnote=?,date_validation=?,validateur=? where eleveid_modifnote=? and	idpiste_modifnote=? and examen_modifnote=1 and designationid_modifnote=?  and classeid_modifnote=? and teatcherid_modifnote=?");
          $req->execute([$statut,$datemodif,$userid,$eleveid,$pisteid,$idtypenote,$classeid,$teatcherid]);

      }

      function gettypenoteidbymodifnoteid($modifnoteid,$eleveid)
      {
        $req = $this->db->prepare("SELECT * FROM notesmodification,controle where notesmodification.designationid_modifnote=controle.id_ctrl and notesmodification.controle_modifnote=1 and id_modifnote=? and eleveid_modifnote=?");
        $req->execute([$modifnoteid,$eleveid]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["designationid_modifnote"]."*".$someArray[0]["classeid_modifnote"]."*".$someArray[0]["matiereid_modifnote"]."*".$someArray[0]["teatcherid_modifnote"];
        return $donnees;
      }

      function gettypenoteidbymodifnoteidsecondary($modifnoteid,$eleveid)
      {
        $req = $this->db->prepare("SELECT * FROM notesmodification,controle where notesmodification.designationid_modifnote=controle.id_ctrl and notesmodification.controle_modifnote=1 and id_modifnote=? and eleveid_modifnote=?");
        $req->execute([$modifnoteid,$eleveid]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["designationid_modifnote"]."*".$someArray[0]["classeid_modifnote"]."*".$someArray[0]["matiereid_modifnote"]."*".$someArray[0]["teatcherid_modifnote"]."*".$someArray[0]["typesess_ctrl"];
        return $donnees;
      }


      function getnotesexamenInformationsecondary($noteid,$eleveid,$codeEtab,$typenote)
      {
          $req = $this->db->prepare("SELECT * FROM notes,examen where notes.idtype_notes=examen.id_exam and type_notes=? and  id_notes=? and ideleve_notes=? and codeEtab_notes=? ");
          $req->execute([$typenote,$noteid,$eleveid,$codeEtab]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["idtype_notes"]."*".$someArray[0]["idclasse_notes"]."*".$someArray[0]["idmat_notes"]."*".$someArray[0]["idprof_notes"]."*".$someArray[0]["session_notes"]."*".$someArray[0]["typesess_exam"];
          return $donnees;
      }

      function getNotificationInfos($notifid)
      {
          $req = $this->db->prepare("SELECT * FROM notification where id_notif=?");
          $req->execute([$notifid]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["destinataires_notif"]."*".$someArray[0]["classes_notif"]."*".$someArray[0]["codeEtab_notif"]."*".$someArray[0]["sms_notif"]."*".$someArray[0]["email_notif"]."*".$someArray[0]["join_notif"]."*".$someArray[0]["fichier_notif"];
          $donnees.="*".$someArray[0]["libelle_notif"]."*".$someArray[0]["message_notif"]."*".$someArray[0]["date_notif"]."*".$someArray[0]["para_notif"];
          return $donnees;
      }

      function getEmailsAndPhoneOfParentOfStudentclasses($classes,$eleves,$sessionEtab,$codeEtab)
      {

          $req = $this->db->prepare("SELECT distinct parent.email_parent,parent.tel_parent FROM eleve,inscription,classe,parent,parenter where inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.session_inscrip=? and parenter.parentid_parentere=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and classe.id_classe in(?) and classe.codeEtab_classe=? and eleve.idcompte_eleve in(?)");
          $req->execute([$sessionEtab,$classes,$codeEtab,$eleves]);
          return $req->fetchAll();

      }

      function getEmailsAndPhoneOfParentOfStudentclassesParenter($classes,$eleves,$sessionEtab,$codeEtab)
      {
          $this->db->query('SET SQL_BIG_SELECTS=1');
          $req = $this->db->prepare("SELECT distinct parent.email_parent,parent.tel_parent FROM eleve,inscription,classe,parent,parenter where inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=eleve.idcompte_eleve and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and inscription.session_inscrip=?  and classe.id_classe in(?) and classe.codeEtab_classe=? and eleve.idcompte_eleve in(?)");
          $req->execute([$sessionEtab,$classes,$codeEtab,$eleves]);
          return $req->fetchAll();

      }

      function UpdateRoutineOnly($classeid,$matierid,$idroutine,$codeEtab,$jourold,$sessionEtab)
      {
          $req = $this->db->prepare("UPDATE routine SET matiere_route=? where classe_route=? and 	etab_route=? and day_route=? and session_route=? and 	id_route=?");
          $req->execute([$matierid,$classeid,$codeEtab,$jourold,$sessionEtab,$idroutine]);
          // return $req->fetchAll();
       }

      function getEmailOfLocaladminOfThisClasses($classeid,$typecompte,$codeEtab)
      {
        $req = $this->db->prepare("SELECT distinct compte.email_compte,compte.tel_compte FROM compte,assigner where assigner.id_adLocal=compte.id_compte and assigner.codeEtab_assign=? and compte.type_compte=?");
        $req->execute([$codeEtab,$typecompte]);
        return $req->fetchAll();
      }

      function getEmailsOfTeatcherOfThisClasses($classeid,$typecompte,$codeEtab)
      {
        $session="2019-2020";
        $req = $this->db->prepare("SELECT distinct compte.email_compte,compte.tel_compte FROM compte,dispenser,classe where dispenser.id_enseignant=compte.id_compte and dispenser.idclasse_disp=classe.id_classe and classe.id_classe=? and dispenser.codeEtab=? and compte.type_compte=?");
        $req->execute([$classeid,$codeEtab,$typecompte]);
        return $req->fetchAll();
      }

      function getEmailsOfStudentOfThisClasses($classeid,$typecompte,$codeEtab)
      {
        //typecompte==Student
        $session="2019-2020";
        $req = $this->db->prepare("SELECT distinct eleve.email_eleve,compte.tel_compte FROM eleve,inscription,classe,compte where inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=eleve.idcompte_eleve and compte.id_compte=eleve.idcompte_eleve and inscription.session_inscrip=? and classe.id_classe=? and classe.codeEtab_classe=? ");
        $req->execute([$session,$classeid,$codeEtab]);
        return $req->fetchAll();

      }

      function getEmailsOfParentOfStudentInThisClasses($classeid,$typecompte,$codeEtab,$session)
      {
        //typecompte==Parent
        //$session="2019-2020";
        $this->db->query('SET SQL_BIG_SELECTS=1');
          $req = $this->db->prepare("SELECT distinct parent.email_parent,parent.tel_parent FROM eleve,inscription,classe,parent,parenter where inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.session_inscrip=? and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and classe.id_classe=? and classe.codeEtab_classe=?");
          $req->execute([$session,$classeid,$codeEtab]);
          return $req->fetchAll();
      }

      function getEmailsOfParentOfOneStudentInThisClasses($classeid,$typecompte,$codeEtab,$session,$eleveid)
      {
        //typecompte==Parent
        //$session="2019-2020";
          $req = $this->db->prepare("SELECT distinct parent.email_parent,parent.tel_parent FROM eleve,inscription,classe,parent,parenter where inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.session_inscrip=? and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and classe.id_classe=? and classe.codeEtab_classe=? and eleve.idcompte_eleve=? ");
          $req->execute([$session,$classeid,$codeEtab,$eleveid]);
          return $req->fetchAll();
      }

      function getEmailsOfParentOfStudentInThisClassesParenter($classeid,$typecompte,$codeEtab,$session)
      {
        //typecompte==Parent
        //$session="2019-2020";
          $this->db->query('SET SQL_BIG_SELECTS=1');
          $req = $this->db->prepare("SELECT distinct parent.email_parent,parent.tel_parent FROM eleve,inscription,classe,parent,parenter where inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=eleve.idcompte_eleve and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and inscription.session_inscrip=? and classe.id_classe=? and classe.codeEtab_classe=?");
          $req->execute([$session,$classeid,$codeEtab]);
          return $req->fetchAll();
      }

      function getAllNotificationbySchoolCode($codeEtabAssigner,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM notification where codeEtab_notif=? and session_notif=? and statut_notif not in (-1,4) order by id_notif DESC");
        $req->execute([$codeEtabAssigner,$libellesessionencours]);
        return $req->fetchAll();
      }

      function getparacolaireDesignation($messageid)
      {
        $req = $this->db->prepare("SELECT * FROM messages,activites where messages.id_msg=activites.idnotif_act order by id_msg DESC");
        $req->execute([$messageid]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["libelle_act"];
        return $donnees;
      }

      function getAllMessagesTeatcher($codeEtabAssigner,$libellesessionencours,$idcompte)
      {
        $req = $this->db->prepare("SELECT * FROM messages where   messages.codeEtab_msg=? and messages.session_msg=? and messages.addby_msg=? and messages.statut_msg not in (-1,4) order by id_msg DESC");
        $req->execute([$codeEtabAssigner,$libellesessionencours,$idcompte]);
        return $req->fetchAll();
      }

      function getAllMessages($codeEtabAssigner,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM messages where   messages.codeEtab_msg=? and messages.session_msg=? and messages.statut_msg not in (-1,4) order by id_msg DESC");
        $req->execute([$codeEtabAssigner,$libellesessionencours]);
        return $req->fetchAll();
      }

      function getAllMessagesAny()
      {
        $req = $this->db->prepare("SELECT * FROM messages where   messages.statut_msg not in (-1,4) order by id_msg DESC");
        $req->execute([]);
        return $req->fetchAll();
      }

      function getAllNotificationActivitybySchoolCode($codeEtabAssigner,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM messages,activites where messages.id_msg=activites.idnotif_act and  messages.codeEtab_msg=? and messages.session_msg=? and messages.statut_msg not in (-1,4) order by id_msg DESC");
        $req->execute([$codeEtabAssigner,$libellesessionencours]);
        return $req->fetchAll();
      }

      function AddNotificationWithoutFi($titre,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$session,$paranotif)
      {
        $req = $this->db->prepare("INSERT INTO notification SET libelle_notif=?,destinataires_notif=?,classes_notif=?,message_notif=?,join_notif=?,statut_notif=?,date_notif=?,codeEtab_notif=?,sms_notif=?,email_notif=?,session_notif=?,para_notif=?");
        $req->execute([$titre,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$session,$paranotif]);
        $idlastcompte=$this->db->lastInsertId();

        return $idlastcompte;
      }

      function AddNotificationWithoutFile($titre,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$paranotif,$libellesession)
      {
        $req = $this->db->prepare("INSERT INTO notification SET libelle_notif=?,destinataires_notif=?,classes_notif=?,message_notif=?,join_notif=?,statut_notif=?,date_notif=?,codeEtab_notif=?,sms_notif=?,email_notif=?,para_notif=?,session_notif=?");
        $req->execute([$titre,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$paranotif,$libellesession]);

      }

      function UpdateNotificationFile($fichierad,$idnotif,$codeEtab)
      {
        $req= $this->db->prepare("UPDATE notification SET fichier_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$fichierad,$idnotif,$codeEtab]);
      }

      function UpdateNotificationFileName($fichierad,$idnotif,$codeEtab)
      {
        $req= $this->db->prepare("UPDATE notification SET fichier_notif=? where id_notif=? and codeEtab_notif=?");
        $req->execute([$fichierad,$idnotif,$codeEtab]);

      }

      function AddNotificationWithFile($titre,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$session,$paranotif)
      {
        $req = $this->db->prepare("INSERT INTO notification SET libelle_notif=?,destinataires_notif=?,classes_notif=?,message_notif=?,join_notif=?,statut_notif=?,date_notif=?,codeEtab_notif=?,sms_notif=?,email_notif=?,session_notif=?,para_notif=?");
        $req->execute([$titre,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$session,$paranotif]);
        $idfiche=$this->db->lastInsertId();
        return $idfiche;
      }

      function getEtabLibellebyCodeEtab($codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement where code_etab=?");
        $req->execute([$codeEtab]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["libelle_etab"];
        return $donnees;
      }

      function getEtabLogobyCodeEtab($codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement where code_etab=?");
        $req->execute([$codeEtab]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["logo_etab"];
        return $donnees;
      }

      function getHoursOfSubject($classeId,$matiereid,$profid,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM routine where classe_route=? and matiere_route=? and etab_route=?");
        $req->execute([$classeId,$matiereid,$codeEtab]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["debut_route"]."*".$someArray[0]["fin_route"];
        return $donnees;
      }

      function getCodeEtabOfEnseignerId($IdCompte)
      {
        $req = $this->db->prepare("SELECT * FROM enseigner where id_enseignant=?");
        $req->execute([$IdCompte]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["codeEtab"];
        return $donnees;
      }

      function UpdateFicheLectureWithFile($fichierad,$ficheid,$codeEtab,$classeEtab,$matclasse)
      {
        $req= $this->db->prepare("UPDATE fiche SET support_fiche=? where id_fiche=? and codeEtab_fiche=? and classe_fiche=? and mat_fiche=?");
        $req->execute([$fichierad,$ficheid,$codeEtab,$classeEtab,$matclasse]);


      }

      function UpdateSousFiches($desi,$ficheid,$sousficheid)
      {
        $req= $this->db->prepare("UPDATE sousfiche SET libelle_sousfic=? where idfic_sousfic=? and 	id_sousfic=?");
        $req->execute([$desi,$ficheid,$sousficheid]);
      }

      function DeletedDousFichesById($sousficheid,$ficheid)
      {
        $req = $this->db->prepare("DELETE FROM sousfiche where id_sousfic=?");
        $req->execute([$sousficheid]);

      }

      function getNumberOfSousFiches($ficheid)
      {
        $req = $this->db->prepare("SELECT * FROM sousfiche where idfic_sousfic=?");
        $req->execute([$ficheid]);
        $data=$req->fetchAll();
        $nb=count($data);

        return $nb;
      }

      function getparentednumber($studentid,$parentid)
      {
        $req = $this->db->prepare("SELECT * FROM parenter where 	eleveid_parenter=? and parentid_parenter=?");
        $req->execute([$studentid,$parentid]);
        $data=$req->fetchAll();
        $nb=count($data);

        return $nb;
      }

      function getAllSousFichesOfThisFicheId($sousfiche)
      {
          $req = $this->db->prepare("SELECT * FROM sousfiche where idfic_sousfic=?");
          $req->execute([$sousfiche]);
          return $req->fetchAll();
      }

      function getSectionfiche($fiche,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM cahier,sectioncahier where cahier.id_cahier=sectioncahier.idcahier_secahier	and cahier.id_cahier=? and cahier.teatcher_cahier=? and cahier.codeEtab_cahier=?");
        $req->execute([$fiche,$teatcher,$codeEtab]);
        return $req->fetchAll();
      }

      function getTravailafairefiche($fiche,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM cahier,tachecahier where cahier.id_cahier=tachecahier.idcahier_tacheca	and cahier.id_cahier=? and cahier.teatcher_cahier=? and cahier.codeEtab_cahier=?");
        $req->execute([$fiche,$teatcher,$codeEtab]);
        return $req->fetchAll();
      }

      function getCahierInfos($fiche,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM cahier,classe,matiere  where cahier.matiere_cahier=matiere.id_mat and cahier.id_cahier=classe.id_classe and cahier.id_cahier=? and cahier.teatcher_cahier=? and cahier.codeEtab_cahier=?");
        $req->execute([$fiche,$teatcher,$codeEtab]);
        return $req->fetchAll();
      }

      function getAllFichesOfTeatcherClasses($teatcher,$libellesessionencours,$codeEtab)
      {
          $req = $this->db->prepare("SELECT * FROM cahier,classe,matiere where cahier.classe_cahier=classe.id_classe and cahier.matiere_cahier=matiere.id_mat and  teatcher_cahier=? and session_cahier=? and codeEtab_cahier=? order by datecrea_cahier Desc");
          $req->execute([$teatcher,$libellesessionencours,$codeEtab]);
          return $req->fetchAll();
      }

      function getAllFichesClassesOfTeatchers($codeEtab,$session)
      {
        $req = $this->db->prepare("SELECT * FROM cahier,classe,matiere where cahier.classe_cahier=classe.id_classe and cahier.matiere_cahier=matiere.id_mat and session_cahier=? and codeEtab_cahier=? order by datecrea_cahier Desc");
        $req->execute([$session,$codeEtab]);
        return $req->fetchAll();
      }

      function getAllfichesOfTeatcherClassesAndpgrmeId($IdCompte,$idfiche)
      {
        $req = $this->db->prepare("SELECT * FROM fiche,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and fiche.teatcher_fiche=enseignant.idcompte_enseignant and fiche.mat_fiche=matiere.id_mat and fiche.classe_fiche=classe.id_classe and enseignant.idcompte_enseignant=? and fiche.id_fiche=?");
        $req->execute([$IdCompte,$idfiche]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);
        $donnees=$someArray[0]["id_fiche"]."*".$someArray[0]["mat_fiche"]."*".$someArray[0]["classe_fiche"]."*".$someArray[0]["teatcher_fiche"]."*".$someArray[0]["codeEtab_fiche"]."*".$someArray[0]["support_fiche"]."*".$someArray[0]["date_fiche"];
        return $donnees;


      }
      function getAllSousFicheOfThisFicheId($idfiche)
      {
          $req = $this->db->prepare("SELECT * FROM sousfiche where idfic_sousfic=?");
          $req->execute([$idfiche]);
          return $req->fetchAll();
      }

      function getAllFicesOfTeatcherClasses($IdCompte,$session)
      {
        $req = $this->db->prepare("SELECT * FROM fiche,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and fiche.teatcher_fiche=enseignant.idcompte_enseignant and fiche.mat_fiche=matiere.id_mat and fiche.classe_fiche=classe.id_classe and enseignant.idcompte_enseignant=? and fiche.session_fiche=? order by classe.libelle_classe ASC");
        $req->execute([$IdCompte,$session]);
        return $req->fetchAll();
      }

      function AddSousFiche($idfiche,$details)
      {
          $req = $this->db->prepare("INSERT INTO sousfiche SET idfic_sousfic=?,libelle_sousfic=?");
          $req->execute([$idfiche,$details]);
      }

      function UpdateLectureFicheFileName($fichierad,$idfiche,$teatcherid,$codeEtab,$matiereid,$classeEtab)
      {
        $req= $this->db->prepare("UPDATE fiche SET support_fiche=? where id_fiche=? and teatcher_fiche=? and codeEtab_fiche=? and mat_fiche=? and classe_fiche=?");
        $req->execute([$fichierad,$idfiche,$teatcherid,$codeEtab,$matiereid,$classeEtab]);
      }

      function AddLectureFiche($datecrea,$matiereid,$classeEtab,$teatcherid,$codeEtab,$session)
      {
          $req = $this->db->prepare("INSERT INTO fiche SET date_fiche=?,mat_fiche=?,classe_fiche=?,teatcher_fiche=?,codeEtab_fiche=?,session_fiche=?");
          $req->execute([$datecrea,$matiereid,$classeEtab,$teatcherid,$codeEtab,$session]);
          $idfiche=$this->db->lastInsertId();
          return $idfiche;

      }

      function UpdateProgrammeAcademiqueWithOutFile($programmeid,$programme,$descri,$codeEtab)
      {
        $req= $this->db->prepare("UPDATE programme SET libelle_prog=?,descri_prog=? where id_prog=? and codeEtab_prog=?");
        $req->execute([$programme,$descri,$programmeid,$codeEtab]);

      }

      function UpdateProgrammeAcademiqueWithFile($programmeid,$programme,$descri,$fichierad,$codeEtab)
      {
          $req= $this->db->prepare("UPDATE programme SET libelle_prog=?,descri_prog=?,fichier_prog=? where id_prog=? and codeEtab_prog=?");
          $req->execute([$programme,$descri,$fichierad,$programmeid,$codeEtab]);


      }

      function getAllprogrammesOfTeatcherClasses($IdCompte,$session)
      {
        $req = $this->db->prepare("SELECT * FROM programme,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and programme.idprof_prog=enseignant.idcompte_enseignant and programme.idmat_prog=matiere.id_mat and programme.idclasse_prog=classe.id_classe and enseignant.idcompte_enseignant=? and programme.session_prog=? order by classe.libelle_classe ASC");
        $req->execute([$IdCompte,$session]);
        return $req->fetchAll();
      }

      function getAllprogrammesOfTeatcherClassesAndpgrmeId($IdCompte,$programme,$session)
      {
        $req = $this->db->prepare("SELECT * FROM programme,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and programme.idprof_prog=enseignant.idcompte_enseignant and programme.idmat_prog=matiere.id_mat and programme.idclasse_prog=classe.id_classe and enseignant.idcompte_enseignant=? and programme.id_prog=? and programme.session_prog=? ");
        $req->execute([$IdCompte,$programme,$session]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);

        $donnees=$someArray[0]["id_prog"]."*".$someArray[0]["libelle_prog"]."*".$someArray[0]["descri_prog"]."*".$someArray[0]["idprof_prog"]."*".$someArray[0]["idmat_prog"]."*".$someArray[0]["idclasse_prog"];
        $donnees.="*".$someArray[0]["codeEtab_prog"]."*".$someArray[0]["fichier_prog"]."*".$someArray[0]["years_prog"];
        return $donnees;

      }

      function AddprogrammeClasseMatTeatcher($programme,$descri,$teatcherid,$matiereid,$classeEtab,$codeEtab,$statut,$datecrea,$fichierad,$years,$session)
      {
        $req = $this->db->prepare("INSERT INTO programme SET libelle_prog=?,descri_prog=?,idprof_prog=?,idmat_prog=?,idclasse_prog=?,codeEtab_prog=?,statut_prog=?,date_prog=?,fichier_prog=?,years_prog=?,session_prog=?");
        $req->execute([
    $programme,$descri,$teatcherid,$matiereid,$classeEtab,$codeEtab,$statut,$datecrea,$fichierad,$years,$session
        ]);



      }

      function getNumberofPgrmeClasse($classeEtab,$matclasse,$codeEtab,$teatcher)
      {
          $req = $this->db->prepare("SELECT * FROM programme where idclasse_prog=? and idmat_prog=? and codeEtab_prog=? and idprof_prog=?");
          $req->execute([$classeEtab,$matclasse,$codeEtab,$teatcher]);
          $data=$req->fetchAll();
          $nb=count($data);

          return $nb;

      }

      function getAllMatiereTeatchByThisTeatcherOfClasses($teatcherId,$classe)
      {
          $req = $this->db->prepare("SELECT * FROM matiere,classe where matiere.classe_mat=classe.id_classe and matiere.teatcher_mat=? and matiere.classe_mat=? ");
          $req->execute([$teatcherId,$classe]);
          return $req->fetchAll();
      }

      function getNbMatiereTeatchByThisTeatcherOfClasses($teatcherId,$classe,$code)
      {
          $req = $this->db->prepare("SELECT * FROM matiere,classe where matiere.classe_mat=classe.id_classe and matiere.teatcher_mat=? and matiere.classe_mat=? and classe.codeEtab_classe=? ");
          $req->execute([$teatcherId,$classe,$code]);
          $data=$req->fetchAll();
          $nb=count($data);

          return $nb;
      }

      function getCompteEmail($compteid)
      {
          $req = $this->db->prepare("SELECT * FROM compte where compte.id_compte=?");
          $req->execute([$compteid]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);

          $donnees=$someArray[0]["email_compte"];
          return $donnees;
      }

      function getAllMatiereTeatchByThisTeatcherOfClassesSchool($teatcherId,$classe,$code)
      {
        $req = $this->db->prepare("SELECT * FROM matiere,classe where matiere.classe_mat=classe.id_classe and matiere.teatcher_mat=? and matiere.classe_mat=? and classe.codeEtab_classe=? ");
        $req->execute([$teatcherId,$classe,$code]);
        return $req->fetchAll();
      }

      function getCodeEtabofMatiereChoosen($teatcherId,$classe,$matiere)
      {
        $req = $this->db->prepare("SELECT * FROM matiere where teatcher_mat=? and classe_mat=? and 	id_mat=?");
        $req->execute([$teatcherId,$classe,$matiere]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray = json_decode($array, true);

        $donnees=$someArray[0]["codeEtab_mat"];
        return $donnees;

      }

function getNumberOfTeatcherRoutine($IdCompte)
{
  $req = $this->db->prepare("SELECT * FROM routine,classe,matiere,enseignant,dispenser where routine.matiere_route=matiere.id_mat and routine.classe_route=classe.id_classe and dispenser.id_enseignant=enseignant.idcompte_enseignant and dispenser.id_cours=matiere.id_mat and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=?");
  $req->execute([$IdCompte]);
  $data=$req->fetchAll();
  $nb=count($data);

  return $nb;
}
function getAllnotesOfExamensClasses($classe,$matiere,$teatcher,$examenid,$type)
{
  $req = $this->db->prepare("SELECT * FROM examen,notes,classe,matiere,enseignant,eleve where notes.ideleve_notes=eleve.idcompte_eleve and  notes.idtype_notes=examen.id_exam and notes.idclasse_notes=classe.id_classe and notes.idmat_notes=matiere.id_mat and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=? and notes.idmat_notes=? and notes.idprof_notes=? and notes.idtype_notes=? and notes.type_notes=? order by eleve.nom_eleve ASC");
  $req->execute([$classe,$matiere,$teatcher,$examenid,$type]);
  return $req->fetchAll();
}

function getAllnotesOfExamensClassesOfStudent($classe,$matiere,$teatcher,$examenid,$type,$studentid)
{
  $req = $this->db->prepare("SELECT * FROM examen,notes,classe,matiere,enseignant,eleve where notes.ideleve_notes=eleve.idcompte_eleve and  notes.idtype_notes=examen.id_exam and notes.idclasse_notes=classe.id_classe and notes.idmat_notes=matiere.id_mat and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=? and notes.idmat_notes=? and notes.idprof_notes=? and notes.idtype_notes=? and notes.type_notes=? and notes.ideleve_notes=? order by eleve.nom_eleve ASC");
  $req->execute([$classe,$matiere,$teatcher,$examenid,$type,$studentid]);
  return $req->fetchAll();
}

function getAllnotesOfExamensClassesOfStudentNB($classe,$matiere,$teatcher,$examenid,$type,$studentid)
{
  $req = $this->db->prepare("SELECT * FROM examen,notes,classe,matiere,enseignant,eleve where notes.ideleve_notes=eleve.idcompte_eleve and  notes.idtype_notes=examen.id_exam and notes.idclasse_notes=classe.id_classe and notes.idmat_notes=matiere.id_mat and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=? and notes.idmat_notes=? and notes.idprof_notes=? and notes.idtype_notes=? and notes.type_notes=? and notes.ideleve_notes=? order by eleve.nom_eleve ASC");
  $req->execute([$classe,$matiere,$teatcher,$examenid,$type,$studentid]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getAllnoteControlesClasses($classe,$matiere,$teatcher,$controleid,$type)
{
    $req = $this->db->prepare("SELECT * FROM controle,notes,classe,matiere,enseignant,eleve where notes.ideleve_notes=eleve.idcompte_eleve and notes.idtype_notes=controle.id_ctrl and notes.idclasse_notes=classe.id_classe and notes.idmat_notes=matiere.id_mat and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=? and notes.idmat_notes=? and notes.idprof_notes=? and notes.idtype_notes=? and notes.type_notes=? order by eleve.nom_eleve ASC ");
    $req->execute([$classe,$matiere,$teatcher,$controleid,$type]);
    return $req->fetchAll();
}

function getAllnoteControlesClassesOfStudent($classe,$matiere,$teatcher,$controleid,$type,$studentid)
{
    $req = $this->db->prepare("SELECT * FROM controle,notes,classe,matiere,enseignant,eleve where notes.ideleve_notes=eleve.idcompte_eleve and notes.idtype_notes=controle.id_ctrl and notes.idclasse_notes=classe.id_classe and notes.idmat_notes=matiere.id_mat and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=? and notes.idmat_notes=? and notes.idprof_notes=? and notes.idtype_notes=? and notes.type_notes=? and notes.ideleve_notes=? order by eleve.nom_eleve ASC ");
    $req->execute([$classe,$matiere,$teatcher,$controleid,$type,$studentid]);
    return $req->fetchAll();
}

function getAllnoteControlesClassesOfStudentNB($classe,$matiere,$teatcher,$controleid,$type,$studentid)
{
    $req = $this->db->prepare("SELECT * FROM controle,notes,classe,matiere,enseignant,eleve where notes.ideleve_notes=eleve.idcompte_eleve and notes.idtype_notes=controle.id_ctrl and notes.idclasse_notes=classe.id_classe and notes.idmat_notes=matiere.id_mat and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=? and notes.idmat_notes=? and notes.idprof_notes=? and notes.idtype_notes=? and notes.type_notes=? and notes.ideleve_notes=? order by eleve.nom_eleve ASC ");
    $req->execute([$classe,$matiere,$teatcher,$controleid,$type,$studentid]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;
}

function getStatusOfControleClasseMat($classe,$code,$matiere,$statut)
{
   // $req = $this->db->prepare("SELECT * FROM controle where classe_ctrl=? and codeEtab_ctrl=? and mat_ctrl=? and statut_ctrl!=?");
   // $req->execute([$classe,$code,$matiere,$statut]);
   $req = $this->db->prepare("SELECT * FROM controle where classe_ctrl=? and codeEtab_ctrl=? and mat_ctrl=?");
   $req->execute([$classe,$code,$matiere]);
   $data=$req->fetchAll();
   $nb=count($data);

   return $nb;
}

function getNbAllControleClasseConsignerNotes($classe,$code,$matiere,$statut)
{
  $req = $this->db->prepare("SELECT * FROM controle where classe_ctrl=? and codeEtab_ctrl=? and mat_ctrl=? and statut_ctrl=?");
  $req->execute([$classe,$code,$matiere,$statut]);
  $data=$req->fetchAll();
  $nb=count($data);

  return $nb;
}

function getStatusOfControleClasseMatTeatcher($classe,$code,$matiere,$statut,$teatcherid)
{
  $req = $this->db->prepare("SELECT * FROM controle where classe_ctrl=? and codeEtab_ctrl=? and mat_ctrl=? and statut_ctrl=? and teatcher_ctrl=?");
   $req->execute([$classe,$code,$matiere,$statut,$teatcherid]);
   $data=$req->fetchAll();
   $nb=count($data);

   return $nb;
}



function getNumberOfNotesSchoolClasses($classe,$code,$matiere,$type)
{
  $req = $this->db->prepare("SELECT * FROM examen,notes where notes.idtype_notes=examen.id_exam and notes.type_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and notes.codeEtab_notes=? ");
  $req->execute([$type,$classe,$matiere,$code]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;

}

function getNumberOfNotesSchoolClassesTeatcher($classe,$code,$matiere,$type,$teatcherid)
{
  $req = $this->db->prepare("SELECT * FROM examen,notes where notes.idtype_notes=examen.id_exam and notes.type_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and notes.codeEtab_notes=? and notes.idprof_notes=? ");
  $req->execute([$type,$classe,$matiere,$code,$teatcherid]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;

}


function getNumberOfNotesSchoolClassesExamen($classe,$code,$matiere,$type,$idExam)
{
  $req = $this->db->prepare("SELECT * FROM examen,notes where notes.idtype_notes=examen.id_exam and notes.type_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and notes.codeEtab_notes=? and examen.id_exam=? ");
  $req->execute([$type,$classe,$matiere,$code,$idExam]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;

}

function getNumberOfNotesSchoolClassesExamenTeatcher($classe,$code,$matiereid,$type,$idExam,$teatcherid)
{
  $req = $this->db->prepare("SELECT * FROM examen,notes where notes.idtype_notes=examen.id_exam and notes.type_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and notes.codeEtab_notes=? and examen.id_exam=? and notes.idprof_notes=?");
  $req->execute([$classe,$code,$matiereid,$type,$idExam,$teatcherid]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;

}

function getAllExamOfThisValue($concatidExamwithoutNotes,$code)
{
  $req = $this->db->prepare("SELECT * FROM examen WHERE codeEtab_exam=? and id_exam in($concatidExamwithoutNotes) ");
  $req->execute([$code]);
  // $req->execute([]);
  return $req->fetchAll();
}

function getAllCountriesOfSystem()
{
    $req = $this->db->prepare("SELECT * FROM pays ");
    $req->execute([]);
    return $req->fetchAll();

}


function getAllExamenOfSchoolClasses($classe,$code,$matiere,$type)
{
  $req = $this->db->prepare("SELECT distinct matiere.id_mat,enseignant.idcompte_enseignant,examen.id_exam,examen.libelle_exam FROM matiere,enseignant,examen,notes where notes.idmat_notes=matiere.id_mat and notes.idprof_notes=enseignant.idcompte_enseignant and  notes.idtype_notes=examen.id_exam and notes.type_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and notes.codeEtab_notes=? ");
  $req->execute([$type,$classe,$matiere,$code]);
  return $req->fetchAll();
}

function getAllExamenOfSchoolClassesTeatcher($classe,$code,$matiere,$type,$teatcherid)
{
  $req = $this->db->prepare("SELECT distinct matiere.id_mat,enseignant.idcompte_enseignant,examen.id_exam,examen.libelle_exam FROM matiere,enseignant,examen,notes where notes.idmat_notes=matiere.id_mat and notes.idprof_notes=enseignant.idcompte_enseignant and  notes.idtype_notes=examen.id_exam and notes.type_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and notes.codeEtab_notes=? and enseignant.idcompte_enseignant=? ");
  $req->execute([$type,$classe,$matiere,$code,$teatcherid]);
  return $req->fetchAll();
}

function getAllControlesOfThisClassesSchool($classe,$code,$matiere,$statut)
{
   $req = $this->db->prepare("SELECT * FROM controle,classe,enseignant,matiere where controle.mat_ctrl=matiere.id_mat and  controle.classe_ctrl=classe.id_classe and controle.teatcher_ctrl=enseignant.idcompte_enseignant and  classe_ctrl=? and codeEtab_ctrl=? and mat_ctrl=? and statut_ctrl=?");
   $req->execute([$classe,$code,$matiere,$statut]);
   return $req->fetchAll();

}

function getAllControlesOfThisClassesSchoolActive($classe,$code,$matiere,$statut)
{
   $req = $this->db->prepare("SELECT * FROM controle,classe,enseignant,matiere where controle.mat_ctrl=matiere.id_mat and  controle.classe_ctrl=classe.id_classe and controle.teatcher_ctrl=enseignant.idcompte_enseignant and  classe_ctrl=? and codeEtab_ctrl=? and mat_ctrl=? and statut_ctrl!=?");
   $req->execute([$classe,$code,$matiere,$statut]);
   return $req->fetchAll();

}

function getAllControleClasseConsignerNotes($classe,$code,$matiere,$statut)
{
  $req = $this->db->prepare("SELECT * FROM controle,classe,enseignant,matiere where controle.mat_ctrl=matiere.id_mat and  controle.classe_ctrl=classe.id_classe and controle.teatcher_ctrl=enseignant.idcompte_enseignant and  classe_ctrl=? and codeEtab_ctrl=? and mat_ctrl=? and statut_ctrl=? order by date_ctrl desc");
  $req->execute([$classe,$code,$matiere,$statut]);
  return $req->fetchAll();
}


function getAllControlesOfThisClassesSchoolTeatcher($classe,$code,$matiere,$statut,$teatcher)
{
   $req = $this->db->prepare("SELECT * FROM controle,classe,enseignant,matiere where controle.mat_ctrl=matiere.id_mat and  controle.classe_ctrl=classe.id_classe and controle.teatcher_ctrl=enseignant.idcompte_enseignant and  classe_ctrl=? and codeEtab_ctrl=? and mat_ctrl=? and statut_ctrl=? and enseignant.idcompte_enseignant=?");
   $req->execute([$classe,$code,$matiere,$statut,$teatcher]);
   return $req->fetchAll();

}

function getAllexamensOfSchool($codeEtabAssigner)
{
  $req = $this->db->prepare("SELECT * FROM examen where codeEtab_exam=?");
   $req->execute([$codeEtabAssigner]);
   return $req->fetchAll();
}

function getNumberOfEtablissement()
{
  $req = $this->db->prepare("SELECT * FROM etablissement");
   $req->execute([]);
   $data=$req->fetchAll();
   $nb=count($data);

   return $nb;
}

function getNbnotesControles($controleid,$codeEtab,$sessionEtab,$classe,$matiere)
{
  $req = $this->db->prepare("SELECT * FROM notes,controle where notes.idtype_notes=controle.id_ctrl and notes.type_notes=1 and controle.id_ctrl=? and controle.codeEtab_ctrl=? and controle.session_ctrl=? and controle.classe_ctrl=? and controle.mat_ctrl=?");
   $req->execute([$controleid,$codeEtab,$sessionEtab,$classe,$matiere]);
   $data=$req->fetchAll();
   $nb=count($data);
   return $nb;
}

function getAllControleOfThisClassesOfSchool($notetype,$classeEtab,$codeEtab)
{
  $req = $this->db->prepare("SELECT * FROM classe,controle,matiere,enseignant where controle.mat_ctrl=matiere.id_mat and controle.teatcher_ctrl=enseignant.idcompte_enseignant and controle.classe_ctrl=classe.id_classe and classe.codeEtab_classe=? and classe.id_classe=? and controle.statut_ctrl=0");
   $req->execute([$codeEtab,$classeEtab]);
   return $req->fetchAll();
}

function getAllDesignationNotesOfClasses($notetype,$classeEtab,$codeEtab)
{
  //$req = $this->db->prepare("SELECT * FROM classe,notes,matiere,enseignant,controle where controle.statut_ctrl=1 and classe.id_classe=notes.idclasse_notes and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idmat_notes=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.type_notes=1 and  notes.idtype_notes=? and notes.idclasse_notes=? and notes.codeEtab_notes=?  ");
  $req = $this->db->prepare("SELECT distinct id_ctrl,mat_ctrl,teatcher_ctrl,libelle_ctrl FROM controle,classe,matiere,enseignant,notes where controle.statut_ctrl=1 and controle.codeEtab_ctrl=? and controle.classe_ctrl=classe.id_classe and controle.mat_ctrl=matiere.id_mat and controle.teatcher_ctrl=enseignant.idcompte_enseignant and notes.idclasse_notes=classe.id_classe and classe.id_classe=? and notes.type_notes=1");
  $req->execute([$codeEtab,$classeEtab]);
   return $req->fetchAll();
}

function getAllDesignationNotesOfClassesE($notetype,$classeEtab,$codeEtab)
{
  $req = $this->db->prepare("SELECT * FROM classe,notes,matiere,enseignant,examen where  classe.id_classe=notes.idclasse_notes and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idmat_notes=matiere.id_mat and notes.idtype_notes=examen.id_exam and notes.type_notes=2 and  notes.idtype_notes=? and notes.idclasse_notes=? and notes.codeEtab_notes=? ");
  $req->execute([$notetype,$classeEtab,$codeEtab]);
   return $req->fetchAll();
}

function AddExamenSchool($exam,$datedeb,$datefin,$codeEtab,$session,$typesess)
{
  $req = $this->db->prepare("INSERT INTO examen SET libelle_exam=?,du_exam=?,au_exam=?,codeEtab_exam=?,session_exam=?,typesess_exam=?");
  $req->execute([
$exam,$datedeb,$datefin,$codeEtab,$session,$typesess
  ]);



}

function AddExamenSchoolMixte($exam,$datedeb,$datefin,$codeEtab,$session,$typesess)
{
  $req = $this->db->prepare("INSERT INTO examen SET libelle_exam=?,du_exam=?,au_exam=?,codeEtab_exam=?,session_exam=?,typesess_exam=?");
  $req->execute([
$exam,$datedeb,$datefin,$codeEtab,$session,$typesess
  ]);



}

function supprimerExamenOfthisSchool($examen,$codeEtab)
{
  $req = $this->db->prepare("DELETE FROM examen where id_exam=? and codeEtab_exam=?");
  $req->execute([
  $examen,$codeEtab
  ]);


}

function supprimerExamenOfthisSchoolMixte($examen,$codeEtab)
{
  $req = $this->db->prepare("DELETE FROM examen where id_exam=? and codeEtab_exam=?");
  $req->execute([
  $examen,$codeEtab
  ]);


}

function UpdateStudentAccountCnx($loginTea,$passTea,$idcompte)
{
  $req = $this->db->prepare("UPDATE compte SET login_compte=?,pass_compte=? where id_compte=?");
  $req->execute([
  $loginTea,$passTea,$idcompte
  ]);
}

function getAllExamensOfThisSchool($codeEtabLocal)
{
  $req = $this->db->prepare("SELECT * FROM examen where codeEtab_exam=? ");
  $req->execute([$codeEtabLocal]);
  return $req->fetchAll();

}

function UpdateExamenOfClasses($exam,$datedeb,$datefin,$codeEtab,$idexam)
{
  $req= $this->db->prepare("UPDATE examen SET libelle_exam=?,du_exam=?,au_exam=? where codeEtab_exam=? and id_exam=?");
  $req->execute([
  $exam,$datedeb,$datefin,$codeEtab,$idexam
  ]);


}

function AllreadyExistExamens($examen,$datedeb,$datefin,$codeEtab)
{
  $req = $this->db->prepare("SELECT * FROM examen where libelle_exam=? and du_exam=? and au_exam=? and codeEtab_exam=? ");
  $req->execute([$examen,$datedeb,$datefin,$codeEtab]);
  $data=$req->fetchAll();
  $nb=count($data);

  return $nb;
}

function UpdateControleClasseSchool($classe,$matiere,$teatcher,$controle,$codeEtab,$coef,$datectrl,$idctrl)
{
  $req= $this->db->prepare("UPDATE controle SET classe_ctrl=?,mat_ctrl=?,teatcher_ctrl=?,libelle_ctrl=?,coef_ctrl=?,date_ctrl=? where codeEtab_ctrl=? and id_ctrl=?");
  $req->execute([
  $classe,
  $matiere,
  $teatcher,
  $controle,
  $coef,
  $datectrl,
  $codeEtab,
  $idctrl
  ]);



}

function UpdateControleClasseSchoolMixte($classe,$matiere,$teatcher,$controle,$codeEtab,$coef,$datectrl,$idctrl)
{
  $req= $this->db->prepare("UPDATE controle SET classe_ctrl=?,mat_ctrl=?,teatcher_ctrl=?,libelle_ctrl=?,coef_ctrl=?,date_ctrl=? where codeEtab_ctrl=? and id_ctrl=?");
  $req->execute([
  $classe,
  $matiere,
  $teatcher,
  $controle,
  $coef,
  $datectrl,
  $codeEtab,
  $idctrl
  ]);



}

function deletedControleClassesSchool($controleid,$codeEtab,$classe,$matiere)
{
    $req=$this->db->prepare("DELETE FROM controle where id_ctrl=? and codeEtab_ctrl=? and classe_ctrl=? and mat_ctrl=? ");
    $req->execute([
  $controleid,$codeEtab,$classe,$matiere
    ]);

}

function UpdateRoutine($classe,$Etab,$heuredeb,$heurefin,$matiere,$day,$idroutine)
{
    //$req= $this->db->prepare("UPDATE routine SET classe_route=?,debut_route=?,fin_route=?,matiere_route=?,day_route where etab_route=? and id_route=?");
    $req= $this->db->prepare("UPDATE routine SET classe_route=?,debut_route=?,fin_route=?,matiere_route=?,day_route=? where etab_route=? and id_route=?");

    $req->execute([
    $classe,
   $heuredeb,
    $heurefin,
    $matiere,
     $day,
    $Etab,
    $idroutine
    ]);

    // routine modifier avec succès



}

function UpdateRoutineAndLib($classe,$Etab,$heuredeb,$heurefin,$matiere,$day,$libhours,$idroutine)
{
    //$req= $this->db->prepare("UPDATE routine SET classe_route=?,debut_route=?,fin_route=?,matiere_route=?,day_route where etab_route=? and id_route=?");
    $req= $this->db->prepare("UPDATE routine SET classe_route=?,debut_route=?,fin_route=?,matiere_route=?,day_route=?,libelleHeure_route=? where etab_route=? and id_route=?");

    $req->execute([
    $classe,
   $heuredeb,
    $heurefin,
    $matiere,
     $day,
    $libhours,
    $Etab,
    $idroutine
    ]);

    // routine modifier avec succès



}

function getRouteInfosByIdRoute($idroute)
{
  $req = $this->db->prepare("SELECT * from routine,heure where routine.libelleHeure_route=heure.id_heure and  id_route=?");
  $req->execute([$idroute]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);

  $donnees=$someArray[0]["classe_route"]."*".$someArray[0]["matiere_route"]."*".$someArray[0]["day_route"]."*".$someArray[0]["debut_route"]."*".$someArray[0]["fin_route"]."*".$someArray[0]["etab_route"]."*".$someArray[0]["libelleHeure_route"];
  $donnees.="*".$someArray[0]["heuredeb_heure"]."*".$someArray[0]["heurefin_heure"];
  return $donnees;
}

function getLibelleHoursbylibhoursid($libid)
{
  $req = $this->db->prepare("SELECT * from heure,heurelib where heure.idlib_heure=heurelib.id_heurelib and 	id_heure=?");
  $req->execute([$libid]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);

  $donnees=$someArray[0]["libelle_heurelib"];
  return $donnees;
}

function getspecificRoutine($id_days,$short_days,$classe)
{
  $req = $this->db->prepare("SELECT * from daysweek,routine,matiere where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=? and daysweek.short_days=? order by debut_route ASC");
  $req->execute([$classe,$short_days]);
  return $req->fetchAll();
}

function getspecificRoutineTeatcher($id_days,$short_days,$classe,$idteatcher)
{
  $req = $this->db->prepare("SELECT * FROM routine,matiere,enseignant,dispenser,classe where routine.matiere_route=matiere.id_mat and enseignant.idcompte_enseignant=matiere.teatcher_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and dispenser.id_cours=matiere.id_mat and classe.id_classe=routine.classe_route and classe.id_classe=? and routine.day_route=? and enseignant.idcompte_enseignant=? order by debut_route ASC");
  $req->execute([$classe,$short_days,$idteatcher]);
  $data=$req->fetchAll();
$content="";
  foreach ($data as $value) {
    $content.='<div class="btn-group" role="group" aria-label="Button group with nested dropdown">';
    $content.='<div class="btn-group" role="group">';
    $content.='  <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
    $content.='  <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
    $content.=$value->libelle_mat."(".$value->debut_route." - ".$value->fin_route.")";
    $content.='</button>&nbsp; &nbsp;';

  }
  return $content;
}

      function getNumberofRoutinebyIdroute($id_days,$short_days,$classe)
      {
        $req = $this->db->prepare("SELECT * from daysweek,routine where  routine.day_route=daysweek.short_days and routine.classe_route=? and daysweek.short_days=? ");
        $req->execute([$classe,$short_days]);
        $data=$req->fetchAll();
        $nb=count($data);

        return $nb;
      }

      function getAllweeks()
      {
        $req = $this->db->prepare("SELECT * from daysweek");
        $req->execute([]);

        return $req->fetchAll();


      }

      function getnumberofmatierethisday($idheure,$libelleday,$codeEtab,$sessionEtab,$classe)
      {
          $req = $this->db->prepare("SELECT * from matiere,routine where matiere.id_mat=routine.matiere_route and libelleHeure_route=? and day_route=? and etab_route=? and session_route=? and classe_route=?  ");
          $req->execute([$idheure,$libelleday,$codeEtab,$sessionEtab,$classe]);
          $data=$req->fetchAll();
          $nb=count($data);

          return $nb;
      }

      function getnumberofmatiereTeathisday($idheure,$libelleday,$codeEtab,$sessionEtab,$teatcherid)
      {
          $req = $this->db->prepare("SELECT * from matiere,routine where matiere.id_mat=routine.matiere_route and libelleHeure_route=? and day_route=? and etab_route=? and session_route=? and matiere.teatcher_mat=?  ");
          $req->execute([$idheure,$libelleday,$codeEtab,$sessionEtab,$teatcherid]);
          $data=$req->fetchAll();
          $nb=count($data);

          return $nb;
      }

      function getMatierethisday($idheure,$libelleday,$codeEtab,$sessionEtab,$classe)
      {
          $req = $this->db->prepare("SELECT * from matiere,routine where matiere.id_mat=routine.matiere_route and libelleHeure_route=? and day_route=? and etab_route=? and session_route=? and classe_route=?  ");
          $req->execute([$idheure,$libelleday,$codeEtab,$sessionEtab,$classe]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["libelle_mat"]."*".$someArray[0]["id_route"];
          return $donnees;
      }

      function getMatierethisdayTea($idheure,$libelleday,$codeEtab,$sessionEtab,$teatcherid)
      {
          $req = $this->db->prepare("SELECT * from matiere,routine,classe where matiere.id_mat=routine.matiere_route and classe.id_classe=routine.classe_route and libelleHeure_route=? and day_route=? and etab_route=? and session_route=? and matiere.teatcher_mat=?  ");
          $req->execute([$idheure,$libelleday,$codeEtab,$sessionEtab,$teatcherid]);
          $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["libelle_mat"]."*".$someArray[0]["id_route"]."*".$someArray[0]["libelle_classe"];
          return $donnees;
      }

      function getAllRoutineByClassesandTheDay($classe,$day)
      {
            $req = $this->db->prepare("SELECT * from routine where classe_route=? and day_route=? order by debut_route ASC");
            $req->execute([
            $classe,
            $day
            ]);

            return $req->fetchAll();
      }

      function getAllcoursesdetails($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
        $req = $this->db->prepare("SELECT * FROM courses,matiere,classe,compte where courses.mat_courses=matiere.id_mat and courses.classe_courses=classe.id_classe and courses.teatcher_courses=compte.id_compte and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=? and courses.teatcher_courses=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid
        ]);

        return $req->fetchAll();
      }

      function getAllcoursesdetailsEtab($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM courses,matiere,classe,compte where courses.mat_courses=matiere.id_mat and courses.classe_courses=classe.id_classe and courses.teatcher_courses=compte.id_compte and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?");
          $req->execute([$courseid,$classeid,$codeEtabsession,$libellesessionencours]);
          return $req->fetchAll();
      }


      function getAlldevoirsdetails($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
        $req = $this->db->prepare("SELECT * FROM devoirs,matiere,classe,compte where devoirs.matiere_dev=matiere.id_mat and devoirs.classe_dev=classe.id_classe and devoirs.teatcher_dev=compte.id_compte and devoirs.id_dev=? and devoirs.classe_dev=? and devoirs.codeEtab_dev=? and devoirs.sessionEtab_dev=? and devoirs.teatcher_dev=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid
        ]);

        return $req->fetchAll();
      }

      function getAlldevoirsdetailsEtab($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM devoirs,matiere,classe,compte where devoirs.matiere_dev=matiere.id_mat and devoirs.classe_dev=classe.id_classe and devoirs.teatcher_dev=compte.id_compte and devoirs.id_dev=? and devoirs.classe_dev=? and devoirs.codeEtab_dev=? and devoirs.sessionEtab_dev=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

        return $req->fetchAll();
      }


      function getAlldevoirsdetailstudent($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM devoirs,matiere,classe,compte where devoirs.matiere_dev=matiere.id_mat and devoirs.classe_dev=classe.id_classe and devoirs.teatcher_dev=compte.id_compte and devoirs.id_dev=? and devoirs.classe_dev=? and devoirs.codeEtab_dev=? and devoirs.sessionEtab_dev=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

        return $req->fetchAll();
      }

      function Addrendustudent($type,$iddevoir,$studentId,$classeEtab,$matiereid,$codeEtab,$sessionEtab,$fichierad,$teatcherid,$statut)
      {
          $req = $this->db->prepare("INSERT INTO rendus SET type_rend=?,idrendu_rend=?,student_rend=?,classe_rend=?,matiere_rend=?,codeEtab_rend=?,sessionEtab_rend=?,fichier_rend=?,teatcher_rend=?,statut_rend=?");
          $req->execute([
          $type,$iddevoir,$studentId,$classeEtab,$matiereid,$codeEtab,$sessionEtab,$fichierad,$teatcherid,$statut
          ]);
      }

      function getNbOfRenduDevoirstudent($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
          $req = $this->db->prepare("SELECT * from rendus where type_rend='DEVOIR' and  idrendu_rend=? and 	classe_rend=? and codeEtab_rend=? and sessionEtab_rend=? and student_rend=?");
          $req->execute([
          $courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid
          ]);

          $data=$req->fetchAll();
          $nb=count($data);
          return $nb;

      }

      function getRenduDevoirstudentbydevoirid($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
          $req = $this->db->prepare("SELECT * from rendus where type_rend='DEVOIR' and  idrendu_rend=? and 	classe_rend=? and codeEtab_rend=? and sessionEtab_rend=? and student_rend=?");
          $req->execute([
          $courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid
          ]);

          $data=$req->fetchAll();
          return $data;


      }

      function getNbOfRenduDevoirsOfTea($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
          $req = $this->db->prepare("SELECT * from rendus,compte where rendus.student_rend=compte.id_compte and  type_rend='DEVOIR' and  idrendu_rend=? and 	classe_rend=? and codeEtab_rend=? and sessionEtab_rend=?");
          $req->execute([
          $courseid,$classeid,$codeEtabsession,$libellesessionencours
          ]);

          $data=$req->fetchAll();
          $nb=count($data);
          return $nb;

      }

      function getAllRenduDevoirsOfTea($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
          $req = $this->db->prepare("SELECT * from rendus,compte where rendus.student_rend=compte.id_compte and  type_rend='DEVOIR' and  idrendu_rend=? and 	classe_rend=? and codeEtab_rend=? and sessionEtab_rend=?");
          $req->execute([
          $courseid,$classeid,$codeEtabsession,$libellesessionencours
          ]);

          $data=$req->fetchAll();
          return $data;

      }

      function getAllquizsdetails($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
        $req = $this->db->prepare("SELECT * FROM quiz,matiere,classe,compte where quiz.matiere_quiz=matiere.id_mat and quiz.classe_quiz=classe.id_classe and quiz.teatcher_quiz=compte.id_compte and quiz.id_quiz=? and quiz.classe_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? and quiz.teatcher_quiz=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid
        ]);

        return $req->fetchAll();
      }

      function getAllquizsdetailsEtab($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM quiz,matiere,classe,compte where quiz.matiere_quiz=matiere.id_mat and quiz.classe_quiz=classe.id_classe and quiz.teatcher_quiz=compte.id_compte and quiz.id_quiz=? and quiz.classe_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

        return $req->fetchAll();
      }


      function getAllquizsdetailstudent($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM quiz,matiere,classe,compte where quiz.matiere_quiz=matiere.id_mat and quiz.classe_quiz=classe.id_classe and quiz.teatcher_quiz=compte.id_compte and quiz.id_quiz=? and quiz.classe_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

        return $req->fetchAll();
      }


      function getAllcoursesdetailsofClasses($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM courses,matiere,classe,compte where courses.mat_courses=matiere.id_mat and courses.classe_courses=classe.id_classe and courses.teatcher_courses=compte.id_compte and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

        return $req->fetchAll();
      }

      function getLastsupportFilesCourses($courseid,$codeEtab,$sessionEtab,$classeEtab,$typecourses,$libellematcourses)
      {
          $req = $this->db->prepare("SELECT * FROM supports where courseid_support=? and codeEtab_support=? and sessionEtab_support=? and classeid_support=? and type_support=? order by 	id_support DESC limit 1 ");
          $req->execute([
          $courseid,$codeEtab,$sessionEtab,$classeEtab,$typecourses
          ]);
          $data=$req->fetchAll();
          foreach ($data as $value):
            $files=$value->fichier_support;
            $tabfiles=explode(".",$files);
            $namesfiles=$tabfiles[0];
            $search=strtoupper(substr($libellematcourses, 0, 3));
            $tabnamesfiles=explode($search,$namesfiles);
            $indicefiles=$tabnamesfiles[1];
          endforeach;
          return $indicefiles;
      }


      function getAllcoursesSection($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
        $req = $this->db->prepare("SELECT * FROM courses,coursessection where courses.id_courses=coursessection.idcourse_coursesec and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=? and courses.teatcher_courses=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid
        ]);

        return $req->fetchAll();
      }

      function getAllcoursesSectionEtab($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM courses,coursessection where courses.id_courses=coursessection.idcourse_coursesec and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

        return $req->fetchAll();
      }

      function getAllquizQuestion($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
        $req = $this->db->prepare("SELECT * FROM quiz,question where quiz.id_quiz=question.idquiz_quest and 	quiz.id_quiz=? and quiz.classe_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? and quiz.teatcher_quiz=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid
        ]);

        return $req->fetchAll();
      }

      function getAllquizQuestionEtab($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM quiz,question where quiz.id_quiz=question.idquiz_quest and 	quiz.id_quiz=? and quiz.classe_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

        return $req->fetchAll();
      }

      function getAllquizQuestionTrueOrfalse($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
        $req = $this->db->prepare("SELECT * FROM quiz,question where quiz.id_quiz=question.idquiz_quest and 	quiz.id_quiz=? and quiz.classe_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? and quiz.teatcher_quiz=? and question.mode_quest=1");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid
        ]);

        return $req->fetchAll();
      }

      function getlibellepropositionbyId($idproposition)
      {
          $req = $this->db->prepare("SELECT * FROM propositionrep where 	id_proprep=?");
          $req->execute([
          $idproposition
          ]);
          $data=$req->fetchAll();
           $array=json_encode($data,true);
           $someArray = json_decode($array, true);
           $donnees=$someArray[0]["libelle_proprep"];
             return $donnees;


      }

      function getsolutionsPropoReponses($quizid,$questionid)
      {
          $req = $this->db->prepare("SELECT * FROM propositionrep where 	idquest_proprep=? and valeur_proprep=1");
          $req->execute([
          $questionid
          ]);
            return $req->fetchAll();
      }

      function getValeurOfpropositionchoice($questionid,$courseid,$propositionid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
          $req = $this->db->prepare("SELECT * FROM reponsequiz where questionid_repquiz=? and idquiz_repquiz=? and propositionid_repquiz=?  and codeEtab_repquiz=? and sessionEtab_repquiz=? and studentid_repquiz=?  ");
          $req->execute([
          $questionid,$courseid,$propositionid,$codeEtabsession,$libellesessionencours,$compteuserid
          ]);
          $data=$req->fetchAll();
           $array=json_encode($data,true);
           $someArray = json_decode($array, true);
           $donnees=$someArray[0]["reponse_repquiz"];
             return $donnees;
      }

      function getcompteNotequiz($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
          $req = $this->db->prepare("SELECT SUM(notesquestions.valeur_notesquest) as notequiz from notesquestions where notesquestions.quizid_notesquest=? and notesquestions.studentid_notesquest=? and notesquestions.codEtab_notesquest=? and notesquestions.sessionEtab_notesquest=?");
          $req->execute([
          $courseid,$compteuserid,$codeEtabsession,$libellesessionencours
          ]);
          $data=$req->fetchAll();
           $array=json_encode($data,true);
           $someArray = json_decode($array, true);
           $donnees=$someArray[0]["notequiz"];
             return $donnees;
      }

      function getquiztotalnotes($courseid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT SUM(question.point_quest) as point from question where question.idquiz_quest=?");
        $req->execute([
      $courseid
        ]);
        $data=$req->fetchAll();
         $array=json_encode($data,true);
         $someArray = json_decode($array, true);
         $donnees=$someArray[0]["point"];
           return $donnees;
      }

      function getNbquizsoumis($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT COUNT(reponsequiz.id_repquiz) as number from reponsequiz where reponsequiz.idquiz_repquiz=? and reponsequiz.classeid_repquiz=? and reponsequiz.codeEtab_repquiz=? and reponsequiz.sessionEtab_repquiz=?");
        $req->execute([
      $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);
        $data=$req->fetchAll();
         $array=json_encode($data,true);
         $someArray = json_decode($array, true);
         $donnees=$someArray[0]["number"];
           return $donnees;
      }

      function soumisquizstudent($studentid,$courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM reponsequiz where reponsequiz.studentid_repquiz=? and reponsequiz.idquiz_repquiz=? and reponsequiz.classeid_repquiz=? and reponsequiz.codeEtab_repquiz=? and reponsequiz.sessionEtab_repquiz=?");
        $req->execute([
      $studentid,$courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

          return $req->fetchAll();
      }

      function getPropositionsAndreponsesChild($compteuserid,$questionid,$quizid,$codeEtabsession,$libellesessionencours)
      {
          $req = $this->db->prepare("SELECT * FROM reponsequiz where  reponsequiz.studentid_repquiz=? and reponsequiz.questionid_repquiz=? and 	reponsequiz.idquiz_repquiz=? and reponsequiz.codeEtab_repquiz=? and reponsequiz.sessionEtab_repquiz=? ");
          $req->execute([
          $compteuserid,$questionid,$quizid,$codeEtabsession,$libellesessionencours
          ]);

          return $req->fetchAll();

      }

      function getAllquizQuestionMultiplechoice($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
        $req = $this->db->prepare("SELECT * FROM quiz,question where quiz.id_quiz=question.idquiz_quest and 	quiz.id_quiz=? and quiz.classe_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? and quiz.teatcher_quiz=? and question.mode_quest=2");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid
        ]);

        return $req->fetchAll();
      }


      function getAllcoursesSectionsofClasses($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM courses,coursessection where courses.id_courses=coursessection.idcourse_coursesec and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

        return $req->fetchAll();
      }

      function getAllcoursesComp($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
        $req = $this->db->prepare("SELECT * FROM courses,coursescomp where courses.id_courses=coursescomp.idcourse_coursescomp and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=? and courses.teatcher_courses=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid
        ]);

        return $req->fetchAll();
      }

      function getAllcoursesCompEtab($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM courses,coursescomp where courses.id_courses=coursescomp.idcourse_coursescomp and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

        return $req->fetchAll();
      }

      function getAllcoursesCompsofClasses($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM courses,coursescomp where courses.id_courses=coursescomp.idcourse_coursescomp and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

        return $req->fetchAll();
      }


      function getAllcoursesHomeworks($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
        $req = $this->db->prepare("SELECT * FROM courses,coursesworkh where courses.id_courses=coursesworkh.idcourse_coursesworkh and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=? and courses.teatcher_courses=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid
        ]);

        return $req->fetchAll();
      }

      function getAllcoursesHomeworksEtab($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM courses,coursesworkh where courses.id_courses=coursesworkh.idcourse_coursesworkh and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

        return $req->fetchAll();
      }

      function getAllsuppourts($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
        $req = $this->db->prepare("SELECT * FROM courses,supports where courses.id_courses=supports.courseid_support and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=? and courses.teatcher_courses=? and type_support='COURSES'");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid
        ]);

        return $req->fetchAll();
      }

      function getAllsuppourtsEtab($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM courses,supports where courses.id_courses=supports.courseid_support and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?  and type_support='COURSES'");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

        return $req->fetchAll();
      }


      function getAllsuppourtsStudent($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM courses,supports where courses.id_courses=supports.courseid_support and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=? and type_support='COURSES'");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

        return $req->fetchAll();
      }

      function getAllsuppourtDev($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
        $req = $this->db->prepare("SELECT * FROM devoirs,supports where devoirs.id_dev=supports.courseid_support and devoirs.id_dev=? and devoirs.classe_dev=? and devoirs.codeEtab_dev=? and devoirs.sessionEtab_dev=? and devoirs.teatcher_dev=? and type_support='DEVOIRS'");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid
        ]);

        return $req->fetchAll();
      }

      function getAllsuppourtDevEtab($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM devoirs,supports where devoirs.id_dev=supports.courseid_support and devoirs.id_dev=? and devoirs.classe_dev=? and devoirs.codeEtab_dev=? and devoirs.sessionEtab_dev=? and type_support='DEVOIRS'");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

        return $req->fetchAll();
      }

      function getAllsuppourtDevStudent($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM devoirs,supports where devoirs.id_dev=supports.courseid_support and devoirs.id_dev=? and devoirs.classe_dev=? and devoirs.codeEtab_dev=? and devoirs.sessionEtab_dev=? and type_support='DEVOIRS'");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

        return $req->fetchAll();
      }

      function getAllcoursesHomeworkssofClasses($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM courses,coursesworkh where courses.id_courses=coursesworkh.idcourse_coursesworkh and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

        return $req->fetchAll();
      }

      function AddcoursesPublication($courseid,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab)
      {
        $req = $this->db->prepare("UPDATE courses SET statut_courses=1 where id_courses=? and teatcher_courses=? and classe_courses=? and mat_courses=? and codeEtab_courses=? and sessionEtab_courses=?");
        $req->execute([
        $courseid,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab
        ]);
      }

      function StartcoursesDiscussion($courseid,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab,$dateday)
      {
        $req = $this->db->prepare("UPDATE courses SET discussion_courses=1,startdiscus_courses=? where id_courses=? and teatcher_courses=? and classe_courses=? and mat_courses=? and codeEtab_courses=? and sessionEtab_courses=?");
        $req->execute([
        $dateday,$courseid,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab
        ]);
      }

      function StopcoursesDiscussion($courseid,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab,$dateday)
      {
        $req = $this->db->prepare("UPDATE courses SET discussion_courses=2,stopdiscus_courses=? where id_courses=? and teatcher_courses=? and classe_courses=? and mat_courses=? and codeEtab_courses=? and sessionEtab_courses=?");
        $req->execute([
        $dateday,$courseid,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab
        ]);
      }

      function AdddevoirsPublication($courseid,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab)
      {
          $req = $this->db->prepare("UPDATE devoirs SET statut_dev=1 where	id_dev=? and teatcher_dev=? and classe_dev=? and matiere_dev=? and 	codeEtab_dev=? and sessionEtab_dev=?");
          $req->execute([
          $courseid,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab
          ]);
      }

      function AddquizPublication($courseid,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab)
      {
          $req = $this->db->prepare("UPDATE quiz SET statut_quiz=1 where id_quiz=? and teatcher_quiz=? and classe_quiz=? and matiere_quiz=? and codeEtab_quiz=? and sessionEtab_quiz=?");
          $req->execute([
          $courseid,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab
          ]);
      }

      function UpdateCoursesDetails($libellecourse,$detailscourse,$durationcourse,$classeEtab,$matiereid,$datecourse,$courseid,$teatcherid,$codeEtab,$sessionEtab)
      {
        $req = $this->db->prepare("UPDATE courses SET libelle_courses=?,descri_courses=?,duree_courses=?,classe_courses=?,mat_courses=?,date_courses=? where id_courses=? and teatcher_courses=? and  codeEtab_courses=? and sessionEtab_courses=?");
        $req->execute([
        $libellecourse,$detailscourse,$durationcourse,$classeEtab,$matiereid,$datecourse,$courseid,$teatcherid,$codeEtab,$sessionEtab
        ]);
      }

      function UpdateDevoirWithoutFile($libelledevoir,$datelimitedevoir,$classeEtab,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$instructionsdevoir,$verouillerdevoir,$destinataires,$devoirid)
      {
        $req = $this->db->prepare("UPDATE devoirs SET libelle_dev=?,datelimite_dev=?,classe_dev=?,matiere_dev=?,instructions_dev=?,verouiller_dev=?,destinataires_dev=? where 	id_dev=? and teatcher_dev=? and codeEtab_dev=? and sessionEtab_dev=?");
        $req->execute([
          $libelledevoir,$datelimitedevoir,$classeEtab,$matiereid,$instructionsdevoir,$verouillerdevoir,$destinataires,$devoirid,$teatcherid,$codeEtab,$sessionEtab
        ]);
      }

      function Addcourses($libellecourse,$datecourse,$matiereid,$teatcherid,$classeEtab,$codeEtab,$sessionEtab,$detailscourse,$durationcourse)
      {
          $req = $this->db->prepare("INSERT INTO courses SET 	libelle_courses=?,date_courses=?,mat_courses=?,teatcher_courses=?,classe_courses=?,codeEtab_courses=?,sessionEtab_courses=?,descri_courses=?,duree_courses=?");
          $req->execute([
                $libellecourse,
                $datecourse,
                $matiereid,
                $teatcherid,
                $classeEtab,
                $codeEtab,
                $sessionEtab,
                $detailscourse,
                $durationcourse
          ]);

        $idcourse=$this->db->lastInsertId();
        return $idcourse;

      }

      function AddEvaluationExamen($controle,$typeEvaluation,$codeEtab,$libellesession,$typesess,$classes,$datedeb,$datefin)
      {
        $req = $this->db->prepare("INSERT INTO examen SET 	libelle_exam=?,du_exam=?,au_exam=?,codeEtab_exam=?,session_exam=?,typesess_exam=?,classes_exam=?");
        $req->execute([
            $controle,$datedeb,$datefin,$codeEtab,$libellesession,$typesess,$classes
        ]);

         $idexamen=$this->db->lastInsertId();

         //insertions dans la table evaluation

         $req1 = $this->db->prepare("INSERT INTO evaluations SET 	idtype_evaltions=?,libelle_evaltions=?,type_evaltions=?,codeEtab_evaltions=?,session_evaltions=?,periode_evaltions=?,classes_evaltions=?");
         $req1->execute([
             $idexamen,$controle,$typeEvaluation,$codeEtab,$libellesession,$typesess,$classes
         ]);

      }

      function AddnotesQuizquestion($questionid,$compteuserid,$courseid,$point,$codeEtabsession,$libellesessionencours)
      {
        $req = $this->db->prepare("INSERT INTO notesquestions SET questid_notesquest=?,studentid_notesquest=?,quizid_notesquest=?,valeur_notesquest=?,codEtab_notesquest=?,sessionEtab_notesquest=?");
        $req->execute([
              $questionid,$compteuserid,$courseid,$point,$codeEtabsession,$libellesessionencours
        ]);

      }

      function getNumberOfstudentquizquestion($questionid,$compteuserid,$courseid,$codeEtabsession,$libellesessionencours)
      {
          $req = $this->db->prepare("SELECT * FROM notesquestions where questid_notesquest=? and studentid_notesquest=? and quizid_notesquest=? and codEtab_notesquest=? and sessionEtab_notesquest=? ");
          $req->execute([
                $questionid,$compteuserid,$courseid,$codeEtabsession,$libellesessionencours
          ]);
          $data=$req->fetchAll();
          $nb=count($data);
          return $nb;

      }

      function AddEvaluationExamenNb($controle,$typeEvaluation,$codeEtab,$libellesession,$typesess,$classes,$datedeb,$datefin)
      {
        $req = $this->db->prepare("SELECT * FROM examen Where 	libelle_exam=? and du_exam=? and au_exam=? and codeEtab_exam=? and session_exam=? and typesess_exam=? and classes_exam=?");
        $req->execute([
            $controle,$datedeb,$datefin,$codeEtab,$libellesession,$typesess,$classes
        ]);

        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;


      }

      function Addcommentdiscussion($courseid,$addby,$commentaire,$classeid,$codeEtab,$sessionEtab,$matiereid,$teatcherid)
      {
        $req = $this->db->prepare("INSERT INTO commentcourse SET 	courseid_commentc=?,addby_commentc=?,message_commentc=?,classe_commentc=?,codeEtab_commentc=?,sessionEtab_commentc=?,matiere_commentc=?,teatcherid_commentc=?");
        $req->execute([
              $courseid,$addby,$commentaire,$classeid,$codeEtab,$sessionEtab,$matiereid,$teatcherid
        ]);
      }




      function AddCourseSection($section,$courseid)
      {
          $req = $this->db->prepare("INSERT INTO coursessection SET libelle_coursesec=?,idcourse_coursesec=?");
          $req->execute([
                $section,$courseid
          ]);
      }

      function AddCourseComp($competence,$courseid)
      {
        $req = $this->db->prepare("INSERT INTO coursescomp SET libelle_coursecomp=?,idcourse_coursescomp=?");
        $req->execute([
              $competence,$courseid
        ]);
      }

      function AddCourseHomework($tache,$courseid)
      {
        $req = $this->db->prepare("INSERT INTO coursesworkh SET libelle_coursewh=?,idcourse_coursesworkh=?");
        $req->execute([
              $tache,$courseid
        ]);
      }

      function getAllTeatchercourses($compteuserid,$codeEtabsession,$libellesessionencours)
      {
          $req = $this->db->prepare("SELECT * FROM courses,matiere,classe where courses.mat_courses=matiere.id_mat and courses.classe_courses=classe.id_classe and courses.teatcher_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?");
          $req->execute([
                $compteuserid,$codeEtabsession,$libellesessionencours
          ]);

          $data=$req->fetchAll();

          return $data;


      }

      function getAllTeatchercoursesEtab($codeEtabsession,$libellesessionencours)
      {
          $req = $this->db->prepare("SELECT * FROM courses,matiere,classe where courses.mat_courses=matiere.id_mat and courses.classe_courses=classe.id_classe and courses.teatcher_courses=matiere.teatcher_mat and courses.codeEtab_courses=? and courses.sessionEtab_courses=?");
          $req->execute([
                $codeEtabsession,$libellesessionencours
          ]);

          $data=$req->fetchAll();

          return $data;


      }

      function getAllcoursescommentsTea($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
          $req = $this->db->prepare(" SELECT * from commentcourse where courseid_commentc=? and classe_commentc=? and codeEtab_commentc=? and sessionEtab_commentc=? order by date_commentc ASC ");
          $req->execute([
                $courseid,$classeid,$codeEtabsession,$libellesessionencours
          ]);

          $data=$req->fetchAll();

          return $data;
      }

      function getAllcoursescommentonlinesTea($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
          $req = $this->db->prepare(" SELECT * from onlinecommentcourse,compte where onlinecommentcourse.studentid_onlinecom=compte.id_compte and  courseid_onlinecom=? and classe_onlinecom=? and codeEtab_onlinecom=? and sessionEtab_onlinecom=? order by nom_compte ASC ");
          $req->execute([
                $courseid,$classeid,$codeEtabsession,$libellesessionencours
          ]);

          $data=$req->fetchAll();

          return $data;
      }

      function getAllTeatcherquizs($compteuserid,$codeEtabsession,$libellesessionencours)
      {
          $req = $this->db->prepare("SELECT * FROM quiz,matiere,classe where quiz.matiere_quiz=matiere.id_mat and quiz.classe_quiz=classe.id_classe and quiz.teatcher_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=?");
          $req->execute([
                $compteuserid,$codeEtabsession,$libellesessionencours
          ]);

          $data=$req->fetchAll();

          return $data;


      }

      function getAllTeatcherquizsEtab($codeEtabsession,$libellesessionencours)
      {
          $req = $this->db->prepare("SELECT * FROM quiz,matiere,classe,compte where quiz.matiere_quiz=matiere.id_mat and quiz.classe_quiz=classe.id_classe and quiz.teatcher_quiz=compte.id_compte and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=?");
          $req->execute([
                $codeEtabsession,$libellesessionencours
          ]);

          $data=$req->fetchAll();

          return $data;


      }

      function getAllStudentquizs($classeid,$codeEtabsession,$libellesessionencours)
      {
          $req = $this->db->prepare("SELECT * FROM quiz,matiere,classe where quiz.matiere_quiz=matiere.id_mat and quiz.classe_quiz=classe.id_classe and quiz.classe_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? and quiz.statut_quiz=1");
          $req->execute([
                $classeid,$codeEtabsession,$libellesessionencours
          ]);

          $data=$req->fetchAll();

          return $data;


      }

      function getAllTeatcherdevoirs($compteuserid,$codeEtabsession,$libellesessionencours)
      {
          $req = $this->db->prepare("SELECT * FROM devoirs,matiere,classe where devoirs.matiere_dev=matiere.id_mat and devoirs.classe_dev=classe.id_classe and devoirs.teatcher_dev=? and devoirs.codeEtab_dev=? and devoirs.sessionEtab_dev=?");
          $req->execute([
                $compteuserid,$codeEtabsession,$libellesessionencours
          ]);

          $data=$req->fetchAll();

          return $data;


      }

      function getAllTeatcherdevoirsEtab($codeEtabsession,$libellesessionencours)
      {
          $req = $this->db->prepare("SELECT * FROM devoirs,matiere,classe,compte where devoirs.matiere_dev=matiere.id_mat and devoirs.classe_dev=classe.id_classe and devoirs.teatcher_dev=compte.id_compte and devoirs.codeEtab_dev=? and devoirs.sessionEtab_dev=? order by devoirs.datecrea_dev desc,classe.libelle_classe ASC");
          $req->execute([
                $codeEtabsession,$libellesessionencours
          ]);

          $data=$req->fetchAll();

          return $data;


      }

      function getAllCoursesofClasses($classeid,$codeEtabsession,$libellesessionencours)
      {
          $req = $this->db->prepare("SELECT * FROM courses,matiere,classe where courses.mat_courses=matiere.id_mat and courses.classe_courses=classe.id_classe and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=? and courses.statut_courses=1");
          $req->execute([
                $classeid,$codeEtabsession,$libellesessionencours
          ]);

          $data=$req->fetchAll();

          return $data;


      }

      function getAllDevoirsofClasses($classeid,$codeEtabsession,$libellesessionencours)
      {
          $req = $this->db->prepare("SELECT * FROM devoirs,matiere,classe where devoirs.matiere_dev=matiere.id_mat and devoirs.classe_dev=classe.id_classe and devoirs.classe_dev=? and devoirs.codeEtab_dev=? and devoirs.sessionEtab_dev=? and devoirs.statut_dev=1");
          $req->execute([
                $classeid,$codeEtabsession,$libellesessionencours
          ]);

          $data=$req->fetchAll();

          return $data;


      }

      function AddRoutine($classe,$Etab,$heuredeb,$heurefin,$matiere,$day,$session)
      {
        require_once('Classe.php');
        $classeEtab=new Classe();

        $req = $this->db->prepare("INSERT INTO routine SET classe_route=?,etab_route=?,debut_route=?,fin_route=?,matiere_route=?,day_route=?,session_route=?");
        $req->execute([
        $classe,
        $Etab,
        $heuredeb,
        $heurefin,
        $matiere,
        $day,
        $session
        ]);

        //routine ajouté avec succès

        $datas=$classeEtab->getAllInformationsOfclassesByClasseId($classe,$session);
        $tabdatas=explode("*",$datas);

        $codeEtab=$tabdatas[2];


      }

function verifyRoutineExistwithThisSubject($heuredeb,$heurefin,$day,$classe,$codeEtab,$matiere)
{
  $req = $this->db->prepare("SELECT * FROM routine where (debut_route>=? and fin_route<=?) and day_route=? and classe_route=? and etab_route=? and matiere_route=?");
  $req->execute([
    $heuredeb,
    $heurefin,
    $day,
    $classe,
    $codeEtab,
    $matiere
  ]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function verifyRoutineExistInTimePeriode($heuredeb,$heurefin,$day,$classe,$codeEtab)
{
    $req = $this->db->prepare("SELECT * FROM routine where (debut_route>=? and fin_route<=?) and day_route=? and classe_route=? and etab_route=?");
    $req->execute([
      $heuredeb,
      $heurefin,
      $day,
      $classe,
      $codeEtab
  ]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getAllsubjectofclassesbyIdclasses($classe,$code,$session)
{
    $req = $this->db->prepare("SELECT * FROM matiere where classe_mat=? and codeEtab_mat=? and session_mat=?");
    $req->execute([
    $classe,
    $code,
    $session
  ]);
   return $req->fetchAll();
}

function getAllsubjectofclassesbyIdclassesTea($classe,$code,$session,$teatcherid)
{
    // $req = $this->db->prepare("SELECT * FROM matiere where classe_mat=? and codeEtab_mat=? and teatcher_mat=?");
    $req = $this->db->prepare("SELECT * FROM matiere where classe_mat=? and codeEtab_mat=? and session_mat=? and teatcher_mat=? ");
    $req->execute([
    $classe,
    $code,
    $session,
    $teatcherid
  ]);
   return $req->fetchAll();



}

function getAllsubjectofclassesbyIdclassesAndTeatcherId($classe,$code)
{
    $req = $this->db->prepare("SELECT * FROM matiere where classe_mat=? and codeEtab_mat=?");
    $req->execute([
    $classe,
    $code
  ]);
   return $req->fetchAll();
}

function getAllsubjectofclassesbyIdclassesAndTeatcherIdDays($classe,$code,$libellesessionencours,$libellejour)
{
     $req = $this->db->prepare("SELECT distinct matiere.id_mat,matiere.libelle_mat,matiere.teatcher_mat FROM matiere,routine where matiere.id_mat=routine.matiere_route and routine.classe_route=? and routine.etab_route=? and routine.session_route=? and routine.day_route=? ");
     $req->execute([
     $classe,
     $code,
     $libellesessionencours,
     $libellejour
   ]);
    return $req->fetchAll();
}

function getAllsubjectofclassesbyIdclassesAndTeatcherIdDaysTea($classe,$code,$libellesessionencours,$libellejour,$teatcherid)
{
     $req = $this->db->prepare("SELECT distinct matiere.id_mat,matiere.libelle_mat,matiere.teatcher_mat FROM matiere,routine where matiere.id_mat=routine.matiere_route and routine.classe_route=? and routine.etab_route=? and routine.session_route=? and routine.day_route=? and matiere.teatcher_mat=? ");
     $req->execute([
     $classe,
     $code,
     $libellesessionencours,
     $libellejour,
     $teatcherid
   ]);
    return $req->fetchAll();
}

function UpdateMatiere($matiere,$classe,$teatcher,$codeEtab,$coefficient,$idmat)
{
  $req = $this->db->prepare("UPDATE matiere SET libelle_mat=?,classe_mat=?,teatcher_mat=?,coef_mat=? where codeEtab_mat=? and id_mat=?");
  $req->execute([
  $matiere,
  $classe,
  $teatcher,
  $coefficient,
  $codeEtab,
  $idmat
]);

  //modification dans la table dispenser

  $reqx= $this->db->prepare("UPDATE dispenser SET id_enseignant=?,idclasse_disp=? where codeEtab=? and id_cours=?");
  $reqx->execute([
  $teatcher,
  $classe,
  $codeEtab,
  $idmat
  ]);



}

function AddMatiereOne($matiere,$classe,$teatcher,$codeEtab,$coefficient,$session)
{
  $req = $this->db->prepare("INSERT INTO matiere SET libelle_mat=?,classe_mat=?,teatcher_mat=?,	codeEtab_mat=?,coef_mat=?,session_mat=?");
  $req->execute([
  $matiere,
  $classe,
  $teatcher,
  $codeEtab,
  $coefficient,
  $session
]);

//insertion dans la table dispenser
$idcours=$this->db->lastInsertId();

$reqx= $this->db->prepare("INSERT INTO dispenser SET id_enseignant=?,id_cours=?,idclasse_disp=?,codeEtab=?,session_disp=?");
$reqx->execute([
$teatcher,
$idcours,
$classe,
$codeEtab,
$session
]);



}

function AddMatiere($matiere,$classe,$teatcher,$codeEtab,$coefficient,$session)
{
  $req = $this->db->prepare("INSERT INTO matiere SET libelle_mat=?,classe_mat=?,teatcher_mat=?,	codeEtab_mat=?,coef_mat=?,session_mat=?");
  $req->execute([
  $matiere,
  $classe,
  $teatcher,
  $codeEtab,
  $coefficient,
  $session
]);

//insertion dans la table dispenser
$idcours=$this->db->lastInsertId();

$reqx= $this->db->prepare("INSERT INTO dispenser SET id_enseignant=?,id_cours=?,idclasse_disp=?,codeEtab=?,session_disp=?");
$reqx->execute([
$teatcher,
$idcours,
$classe,
$codeEtab,
$session
]);



}

function AddMatiereLibelle($matierelib,$codeEtab,$session)
{
  $req = $this->db->prepare("INSERT INTO matierelib SET libelle_matlib=?,codeEtab_matlib=?,session_matlib=?	");
  $req->execute([
  $matierelib,$codeEtab,$session
]);



}

function AddMatiereBoucle($matiere,$classe,$teatcher,$codeEtab,$coefficient,$session)
{
  $req = $this->db->prepare("INSERT INTO matiere SET libelle_mat=?,classe_mat=?,teatcher_mat=?,	codeEtab_mat=?,coef_mat=?,session_mat=?");
  $req->execute([
  $matiere,
  $classe,
  $teatcher,
  $codeEtab,
  $coefficient,
  $session
]);

//insertion dans la table dispenser
$idcours=$this->db->lastInsertId();

$reqx= $this->db->prepare("INSERT INTO dispenser SET id_enseignant=?,id_cours=?,idclasse_disp=?,codeEtab=?,session_disp=?");
$reqx->execute([
$teatcher,
$idcours,
$classe,
$codeEtab,
$session
]);



}


  function DelatedMatiere($idmat,$classe,$codeEtab)
  {
    $req = $this->db->prepare("DELETE FROM dispenser where id_cours=? and codeEtab=? and idclasse_disp=? ");
    $req->execute([$idmat,$codeEtab,$classe]);

    $reqx = $this->db->prepare("DELETE FROM matiere where id_mat=? ");
    $reqx->execute([$idmat]);

    //supprimer la table dispenser



  }

  function ExistControleAllready($matiere,$classe,$teatcher,$codeEtab,$coefficient,$datectrl)
  {
    $req = $this->db->prepare("SELECT * FROM controle where mat_ctrl=? and classe_ctrl=? and teatcher_ctrl=? and codeEtab_ctrl=? and coef_ctrl=? and date_ctrl=? ");
    $req->execute([$$matiere,$classe,$teatcher,$codeEtab,$coefficient,$datectrl]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;
  }

  function ExistControleAllreadyOne($matiere,$classe,$teatcher,$codeEtab,$coefficient,$datectrl,$libelle)
  {
    $req = $this->db->prepare("SELECT * FROM controle where libelle_ctrl=? and mat_ctrl=? and classe_ctrl=? and teatcher_ctrl=? and codeEtab_ctrl=? and coef_ctrl=? and date_ctrl=? ");
    $req->execute([$libelle,$matiere,$classe,$teatcher,$codeEtab,$coefficient,$datectrl]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;
  }

  function ExisteMatiereLib($libelleMat,$codeEtab,$sessionEtab)
  {
      $req = $this->db->prepare("SELECT * FROM matierelib where libelle_matlib=? and codeEtab_matlib=? and session_matlib=?");
      $req->execute([$libelleMat,$codeEtab,$sessionEtab]);
      $data=$req->fetchAll();
      $nb=count($data);
      return $nb;
  }


  function AddControleClasseSchool($controle,$datectrl,$classe,$matiere,$teatcher,$coef,$codeEtab,$session,$typesess)
  {
    $statut=0;
    $req = $this->db->prepare("INSERT INTO controle SET libelle_ctrl=?,date_ctrl=?,classe_ctrl=?,mat_ctrl=?,teatcher_ctrl=?,coef_ctrl=?,codeEtab_ctrl=?,statut_ctrl=?,session_ctrl=?,typesess_ctrl=?");
    $req->execute([
  $controle,
  $datectrl,
  $classe,
  $matiere,
  $teatcher,
  $coef,
  $codeEtab,
  $statut,
  $session,
  $typesess
  ]);



  }

  function AddEvaluationscontrolesNb($controle,$datectrl,$classe,$matiere,$teatcher,$coef,$codeEtab,$libellesession,$typesess,$typeEvaluation)
  {
      $statut=0;
      $req = $this->db->prepare("SELECT * FROM controle where libelle_ctrl=? and date_ctrl=? and classe_ctrl=? and mat_ctrl=? and teatcher_ctrl=? and coef_ctrl=? and codeEtab_ctrl=? and statut_ctrl=? and session_ctrl=? and typesess_ctrl=?");
      $req->execute([
    $controle,
    $datectrl,
    $classe,
    $matiere,
    $teatcher,
    $coef,
    $codeEtab,
    $statut,
    $libellesession,
    $typesess
    ]);

    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;



  }

  function getAllExamenDetails($codeEtab,$sessionEtab,$examid)
  {
      $req = $this->db->prepare("SELECT * FROM examen where id_exam=? and codeEtab_exam=? and session_exam=?");
      $req->execute([
    $examid,$codeEtab,$sessionEtab
    ]);
      $data=$req->fetchAll();
      return $data;
  }


  function getAllControlesDetails($codeEtab,$sessionEtab,$examid)
  {
      $req = $this->db->prepare("SELECT * FROM controle,matiere where controle.mat_ctrl=matiere.id_mat  and id_ctrl=? and codeEtab_ctrl=? and session_ctrl=?");
      $req->execute([
    $examid,$codeEtab,$sessionEtab
    ]);
      $data=$req->fetchAll();
      return $data;
  }

  function getAllControlesDetailsOne($codeEtab,$sessionEtab,$examid)
  {
      $req = $this->db->prepare("SELECT * FROM controle,matiere,classe where controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe  and id_ctrl=? and codeEtab_ctrl=? and session_ctrl=?");
      $req->execute([
    $examid,$codeEtab,$sessionEtab
    ]);
      $data=$req->fetchAll();
      return $data;
  }

  function UpdateEvaluationsExamens($libelleexam,$datedebmodexa,$datefinmodexa,$examid,$codeEtab,$sessionEtab,$typeEvaluation)
  {
      $req = $this->db->prepare("UPDATE evaluations SET libelle_evaltions=? where type_evaltions=? and idtype_evaltions=? and codeEtab_evaltions=? and session_evaltions=?");
      $req->execute([
    $libelleexam,$typeEvaluation,$examid,$codeEtab,$sessionEtab
    ]);

    $req1 = $this->db->prepare("UPDATE examen SET 	libelle_exam=?,du_exam=?,au_exam=? where 	id_exam=? and codeEtab_evaltions=? and session_evaltions=?");
    $req1->execute([
  $libelleexam,$datedebmodexa,$datefinmodexa,$examid,$codeEtab,$sessionEtab
  ]);


  }

  function UpdateEvaluationsControles($libellecontrole,$coefcontrole,$datecontrole,$controleid,$codeEtab,$sessionEtab,$typeEvaluation)
  {
    $req = $this->db->prepare("UPDATE evaluations SET libelle_evaltions=? where type_evaltions=? and idtype_evaltions=? and codeEtab_evaltions=? and session_evaltions=?");
    $req->execute([
  $libellecontrole,$typeEvaluation,$controleid,$codeEtab,$sessionEtab
  ]);

  $req1= $this->db->prepare("UPDATE controle SET 	libelle_ctrl=?,coef_ctrl=?,date_ctrl=? where id_ctrl=? and codeEtab_ctrl=? and session_ctrl=?");
  $req1->execute([
$libellecontrole,$coefcontrole,$datecontrole,$controleid,$codeEtab,$sessionEtab
]);


  }

  function DeletedEvaluationsControles($evalid,$typeEvaluation,$controleid,$classeid,$matiereid,$codeEtab,$sessionEtab)
  {
      $req = $this->db->prepare("DELETE FROM evaluations where id_evaltions=? and idtype_evaltions=? and type_evaltions=? and codeEtab_evaltions=? and session_evaltions=?");
      $req->execute([
    $evalid,$controleid,$typeEvaluation,$codeEtab,$sessionEtab
    ]);

    $req = $this->db->prepare("DELETE FROM controle where id_ctrl=? and classe_ctrl=? and 	mat_ctrl=? and codeEtab_ctrl=? and session_ctrl=?");
    $req->execute([
  $controleid,$classeid,$matiereid,$codeEtab,$sessionEtab
  ]);

  }

  function DeletedEvaluationsExamens($evalid,$typeEvaluation,$examenid,$codeEtab,$sessionEtab)
  {
    $req = $this->db->prepare("DELETE FROM evaluations where id_evaltions=? and idtype_evaltions=? and type_evaltions=? and codeEtab_evaltions=? and session_evaltions=?");
    $req->execute([
  $evalid,$examenid,$typeEvaluation,$codeEtab,$sessionEtab
  ]);

  $req1 = $this->db->prepare("DELETE FROM examen where 	id_exam=? and codeEtab_exam=? and session_exam=? ");
  $req1->execute([
$examenid,$codeEtab,$sessionEtab
]);

  }


  function AddEvaluationscontroles($controle,$datectrl,$classe,$matiere,$teatcher,$coef,$codeEtab,$libellesession,$typesess,$typeEvaluation)
  {
      $statut=0;
      $req = $this->db->prepare("INSERT INTO controle SET libelle_ctrl=?,date_ctrl=?,classe_ctrl=?,mat_ctrl=?,teatcher_ctrl=?,coef_ctrl=?,codeEtab_ctrl=?,statut_ctrl=?,session_ctrl=?,typesess_ctrl=?");
      $req->execute([
    $controle,
    $datectrl,
    $classe,
    $matiere,
    $teatcher,
    $coef,
    $codeEtab,
    $statut,
    $libellesession,
    $typesess
    ]);


    $idexamen=$this->db->lastInsertId();

             //insertions dans la table evaluation



             $req1 = $this->db->prepare("INSERT INTO evaluations SET 	idtype_evaltions=?,libelle_evaltions=?,type_evaltions=?,codeEtab_evaltions=?,session_evaltions=?,periode_evaltions=?,classes_evaltions=?");
             $req1->execute([
                 $idexamen,$controle,$typeEvaluation,$codeEtab,$libellesession,$typesess,$classe
             ]);


  }

  function AddControleClasseSchoolMixte($controle,$datectrl,$classe,$matiere,$teatcher,$coef,$codeEtab,$session,$typesess)
  {
    $statut=0;
    $req = $this->db->prepare("INSERT INTO controle SET libelle_ctrl=?,date_ctrl=?,classe_ctrl=?,mat_ctrl=?,teatcher_ctrl=?,coef_ctrl=?,codeEtab_ctrl=?,statut_ctrl=?,session_ctrl=?,typesess_ctrl=?");
    $req->execute([
  $controle,
  $datectrl,
  $classe,
  $matiere,
  $teatcher,
  $coef,
  $codeEtab,
  $statut,
  $session,
  $typesess
  ]);



  }

  function AddDevoirs($libelledevoir,$datelimitedevoir,$classeEtab,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$instructionsdevoir,$verouillerdevoir,$destinataires,$statutdevoir)
  {
    $req = $this->db->prepare("INSERT INTO devoirs SET libelle_dev=?,datelimite_dev=?,classe_dev=?,	matiere_dev=?,teatcher_dev=?,codeEtab_dev=?,sessionEtab_dev=?,instructions_dev=?,verouiller_dev=?,destinataires_dev=?,statut_dev=?");
    $req->execute([
    $libelledevoir,
    $datelimitedevoir,
    $classeEtab,
    $matiereid,
    $teatcherid,
    $codeEtab,
    $sessionEtab,
    $instructionsdevoir,
    $verouillerdevoir,
    $destinataires,
    $statutdevoir
  ]);

  $iddevoir=$this->db->lastInsertId();
  return $iddevoir;

  }

  function Addquiz($libellequiz,$durationquiz,$instructionquiz,$classeEtab,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$verouillerquiz,$statutquiz,$datemite)
  {
    $req = $this->db->prepare("INSERT INTO quiz SET 	libelle_quiz=?,duree_quiz=?,instruction_quiz=?,classe_quiz=?,matiere_quiz=?,teatcher_quiz=?,codeEtab_quiz=?,sessionEtab_quiz=?,verouiller_quiz=?,statut_quiz=?,datelimite_quiz=?");
    $req->execute([
  $libellequiz,
  $durationquiz,
  $instructionquiz,
  $classeEtab,
  $matiereid,
  $teatcherid,
  $codeEtab,
  $sessionEtab,
  $verouillerquiz,
  $statutquiz,
  $datemite
  ]);

  $idquiz=$this->db->lastInsertId();
  return $idquiz;
  }

  function AddQuestion($libellequestion,$moderep,$pointquestion,$quizid)
  {
      $req = $this->db->prepare("INSERT INTO question SET libelle_quest=?,mode_quest=?,point_quest=?,idquiz_quest=?");
      $req->execute([
  $libellequestion,
  $moderep,
  $pointquestion,
  $quizid
    ]);

    $idquest=$this->db->lastInsertId();
    return $idquest;
  }

  function AddPropositionTrueActive($questionid)
  {
      $req = $this->db->prepare("INSERT INTO propositionrep SET libelle_proprep='VRAI',idquest_proprep=?,valeur_proprep=1");
      $req->execute([
$questionid
    ]);
  }

  function AddPropositionFalse($questionid)
  {
    $req = $this->db->prepare("INSERT INTO propositionrep SET libelle_proprep='FAUX',idquest_proprep=?");
    $req->execute([
      $questionid
     ]);
  }

  function AddPropositionTrue($questionid)
  {
    $req = $this->db->prepare("INSERT INTO propositionrep SET libelle_proprep='VRAI',idquest_proprep=?");
    $req->execute([
      $questionid
        ]);
  }

  function AddPropositionFalseActive($questionid)
  {
    $req = $this->db->prepare("INSERT INTO propositionrep SET libelle_proprep='FAUX',idquest_proprep=?,valeur_proprep=1");
    $req->execute([
      $questionid
     ]);
  }

  function AddProposition($libelle_proposition,$questionid)
  {
    $req = $this->db->prepare("INSERT INTO propositionrep SET libelle_proprep=?,idquest_proprep=?");
    $req->execute([
      $libelle_proposition,$questionid
     ]);

     $idquest=$this->db->lastInsertId();
     return $idquest;
  }

  function UpdatePropositionValue($chk_proposition,$proposiionid)
  {
    $req = $this->db->prepare("UPDATE propositionrep SET valeur_proprep=? where id_proprep=?");
    $req->execute([
      $chk_proposition,$proposiionid
     ]);
  }

  function UpdatedevoirFileName($fichierad,$devoirid,$codeEtab,$sessionEtab)
  {
    $req = $this->db->prepare("UPDATE devoirs SET support_dev=? where 	id_dev=? and codeEtab_dev=? and sessionEtab_dev=?");
    $req->execute([
    $fichierad,
    $devoirid,
    $codeEtab,
    $sessionEtab
  ]);
  }

  function AddControleClasseSchoolPrimary($controle,$datectrl,$classe,$matiere,$teatcher,$coef,$codeEtab,$session)
  {
    $statut=0;
    $req = $this->db->prepare("INSERT INTO controle SET libelle_ctrl=?,date_ctrl=?,classe_ctrl=?,mat_ctrl=?,teatcher_ctrl=?,coef_ctrl=?,codeEtab_ctrl=?,statut_ctrl=?,session_ctrl=?");
    $req->execute([
  $controle,
  $datectrl,
  $classe,
  $matiere,
  $teatcher,
  $coef,
  $codeEtab,
  $statut,
  $session
  ]);



  }

      function ExistMatiereAllready($matiere,$classe,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM matiere where libelle_mat=? and classe_mat=? and codeEtab_mat=? ");
        $req->execute([$matiere,$classe,$codeEtab]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function ExistMatiereWithSameProf($matiere,$classe,$codeEtab,$teatcher)
      {
        $req = $this->db->prepare("SELECT * FROM matiere where libelle_mat=? and classe_mat=? and codeEtab_mat=? and teatcher_mat=? ");
        $req->execute([$matiere,$classe,$codeEtab,$teatcher]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function getcodeEtabByLocalId($userId)
      {
        $req = $this->db->prepare("SELECT * FROM assigner  where id_adLocal=?");
         $req->execute([$userId]);
         $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["codeEtab_assign"];
            return $donnees;

      }

      function getCoursesFileName($courseid,$teatcherid,$codeEtab,$sessionEtab)
      {
        $req = $this->db->prepare("SELECT * FROM courses  where id_courses=? and teatcher_courses=? and codeEtab_courses=? and sessionEtab_courses=?");
         $req->execute([$courseid,$teatcherid,$codeEtab,$sessionEtab]);
         $data=$req->fetchAll();
          $array=json_encode($data,true);
          $someArray = json_decode($array, true);
          $donnees=$someArray[0]["support_courses"];
            return $donnees;
      }

      function DeletedsectionCourses($sectionid,$courseid)
      {
        $req = $this->db->prepare("DELETE FROM coursessection where id_coursesec=? and idcourse_coursesec=?");
         $req->execute([$sectionid,$courseid]);
      }

      function DeletedcompCourses($compid,$courseid)
      {
        $req = $this->db->prepare("DELETE FROM coursescomp where 	id_coursecomp=? and idcourse_coursescomp=?");
         $req->execute([$compid,$courseid]);
      }

      function DeletedHomewkCourses($homeworkid,$courseid)
      {
        $req = $this->db->prepare("DELETE FROM coursesworkh where id_coursewh=? and idcourse_coursesworkh=?");
        $req->execute([$homeworkid,$courseid]);
      }

      function DeletedSupportsCourses($homeworkid,$courseid)
      {
        $req = $this->db->prepare("DELETE FROM supports where id_support=? and courseid_support=? and type_support='COURSES'");
        $req->execute([$homeworkid,$courseid]);
      }

      function DeletedSupportsDevoirs($homeworkid,$courseid)
      {
        $req = $this->db->prepare("DELETE FROM supports where id_support=? and courseid_support=? and type_support='DEVOIRS'");
        $req->execute([$homeworkid,$courseid]);
      }

      function suppressionClasse($idclasse)
      {
        $req = $this->db->prepare("DELETE FROM matiere where classe_mat=?");
        $req->execute([$idclasse]);

        $reqX = $this->db->prepare("DELETE FROM classe where 	id_classe=?");
        $reqX->execute([$idclasse]);

        $_SESSION['user']['delclasseok']="Classe supprimé avec succès";

        if($_SESSION['user']['profile'] == "Admin_globale") {
      header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
        // header("Location:../manager/addclasses.php");
          }else if($_SESSION['user']['profile'] == "Admin_locale") {
            if($_SESSION['user']['paysid']==4)
            {
              header("Location:../localecmr/classes.php");
            }else {
              header("Location:../locale/classes.php");
            }


            }else if($_SESSION['user']['profile'] == "Teatcher") {

                  header("Location:../teatcher/index.php");

              }else if($_SESSION['user']['profile'] == "Student") {

                    header("Location:../student/index.php");

                }else if($_SESSION['user']['profile'] == "Parent") {

                      header("Location:../parent/index.php");

                  }


      }

      function getEtablissementbyCodeEtab($codeetab)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement  where code_etab=?");
         $req->execute([$codeetab]);
         return $req->fetchAll();
      }



      function getAllcodesEtab()
      {
        $req = $this->db->prepare("SELECT etablissement.code_etab,etablissement.libelle_etab FROM etablissement order by id_etab desc ");
         $req->execute();
         return $req->fetchAll();
      }

      function getAllcodesEtabBycodeEtab($codeEtabAssigner)
      {
        $req = $this->db->prepare("SELECT etablissement.code_etab,etablissement.libelle_etab FROM etablissement where etablissement.code_etab=?");
         $req->execute([$codeEtabAssigner]);
         return $req->fetchAll();
      }

      function getEtabInfosbyCode($etab)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement  where code_etab=?");
         $req->execute([$etab]);
         $data=$req->fetchAll();

  $array=json_encode($data,true);
  $someArray = json_decode($array, true);

  $donnees=$someArray[0]["code_etab"]."*".$someArray[0]["libelle_etab"]."*".$someArray[0]["tel_etab"]."*".$someArray[0]["tel1_etab"]."*".$someArray[0]["email_etab"]."*".$someArray[0]["adresse_etab"];
  $donnees.="*".$someArray[0]["logo_etab"];
  return $donnees;
      }

      function getAllEtab()
      {
        $req = $this->db->prepare("SELECT * FROM etablissement order by libelle_etab Asc  ");
         $req->execute();
         return $req->fetchAll();
      }

      function getAllEtabByCountry($pays)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement,sessions where etablissement.code_etab=sessions.codeEtab_sess and sessions.encours_sess=1 and pays_etab=? order by libelle_etab Asc  ");
         $req->execute([$pays]);
         return $req->fetchAll();
      }
      function AddImportationFile($classeid,$codeEtab,$sessionEtab,$pays,$userid)
      {
          $req = $this->db->prepare("INSERT INTO importation SET 	classe_import=?,codeEtab_import=?,sessionEtb_import=?,pays_import=?,user_import=?");
          $req->execute([$classeid,$codeEtab,$sessionEtab,$pays,$userid]);
          $idlastcompte=$this->db->lastInsertId();
          return $idlastcompte;
      }

      function AddreponsequizStudentTrue($quizid,$studentid,$questionid,$propositionid,$answer,$matiereid,$classeid,$codeEtab,$sessionEtab)
      {
          $req = $this->db->prepare("INSERT INTO reponsequiz SET idquiz_repquiz=?,studentid_repquiz=?,questionid_repquiz=?,propositionid_repquiz=?,reponse_repquiz=?,matiereid_repquiz=?,classeid_repquiz=?,codeEtab_repquiz=?,sessionEtab_repquiz=?");
          $req->execute([$quizid,$studentid,$questionid,$propositionid,$answer,$matiereid,$classeid,$codeEtab,$sessionEtab]);
      }

      function AddreponsequizStudentFalse($quizid,$studentid,$questionid,$propositionid,$answer,$matiereid,$classeid,$codeEtab,$sessionEtab)
      {
          $req = $this->db->prepare("INSERT INTO reponsequiz SET idquiz_repquiz=?,studentid_repquiz=?,questionid_repquiz=?,propositionid_repquiz=?,reponse_repquiz=?,matiereid_repquiz=?,classeid_repquiz=?,codeEtab_repquiz=?,sessionEtab_repquiz=?");
          $req->execute([$quizid,$studentid,$questionid,$propositionid,$answer,$matiereid,$classeid,$codeEtab,$sessionEtab]);
      }

      function UpdateImportationFileName($fichierad,$importationid)
      {
        $req = $this->db->prepare("UPDATE importation SET file_import=? where id_import=?");
        $req->execute([$fichierad,$importationid]);
      }

      function existEtab($codeetab)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement where code_etab=?");
        $req->execute([$codeetab]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }

      function existEtabCountry($codeetab,$pays)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement where code_etab=? and pays_etab=?");
        $req->execute([$codeetab,$pays]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }



      function getAllAdminLocalBysearchCode($code)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement where code_etab=?  ");
         $req->execute([$code]);
         return $req->fetchAll();
      }

      function getAllAdminLocalBysearchperiode($datedu,$dateau)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement where datecrea_etab>=? and  datecrea_etab<=? ");
         $req->execute([$datedu,$dateau]);
         return $req->fetchAll();
      }


}

?>
