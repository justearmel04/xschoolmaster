<?php
class User{

public $db;
function __construct() {
  require_once('../class/cnx.php');
  require_once('../class/Etablissement.php');
  require_once('../class/Parent.php');

  /*require_once('../class_html2PDF/html2pdf.class.php');
  require_once('../class_html2PDF/_tcpdf_5.0.002/config/lang/eng.php');
  require_once('../class_html2PDF/_tcpdf_5.0.002/tcpdf.php');
  require_once('../PHPMailer/class.phpmailer.php');
  require_once('../PHPMailer/class.smtp.php');*/
  $db = new mysqlConnector();
  $this->db= $db->dataBase;
    }

    function getNbusernameexist($login,$studentid)
    {
        $req = $this->db->prepare("SELECT * FROM compte where login_compte=? and id_compte NOT IN(?)");
        $req->execute([$login,$studentid]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
    }

    function CheckCompteExisteOtherStudentMixte($login,$pass,$type,$studentid)
    {
      // $type="Student";
      $req = $this->db->prepare("SELECT * FROM compte where login_compte=? and pass_compte=? and 	type_compte=? and 	id_compte!=? and compte.statut_compte=1");
      $req->execute([$login,$pass,$type,$studentid]);
      $data=$req->fetchAll();
      $nb=count($data);
      return $nb;
    }

    function CheckCompteAndPassMixte($login,$pass)
    {
      $req = $this->db->prepare("SELECT * FROM compte where login_compte=? and pass_compte=? and compte.statut_compte=1");
      $req->execute([$login,$pass]);
      $data=$req->fetchAll();
      $nb=count($data);
      return $nb;
    }

    function AddEtabMixte($codeetab,$libetab,$etabphone1,$etabphone2,$emailetab,$addresetab,$fichieretab,$datecrea,$emailuser,$pays,$typeetab,$precisions,$cantine,$transportcar,$sigleetab)
    {
      $etabs=new Etab();

      $check=$etabs->existEtabCountry($codeetab,$pays);

      if($check==0)
      {
        //cet etablissment n'existe pas encore dans la base il faut l'ajouter

      $req = $this->db->prepare("INSERT INTO etablissement SET code_etab=?,libelle_etab=?,tel_etab=?,tel1_etab=?,email_etab=?,adresse_etab=?,logo_etab=?,datecrea_etab=?,createby_etab=?,pays_etab=?,type_etab=?,mixte_etab=?,cantine_etab=?,transport_etab=?,sigle_etab=?");
      $req->execute([
      $codeetab,
      $libetab,
      $etabphone1,
      $etabphone2,
      $emailetab,
      $addresetab,
      $fichieretab,
      $datecrea,
      $emailuser,
      $pays,
      $typeetab,
      $precisions,
      $cantine,
      $transportcar,
      $sigleetab
    ]);

      $_SESSION['user']['addetabok']="Nouvel Etablissement ajouté avec succès";

      if($_SESSION['user']['profile'] == "Admin_globale") {

          header("Location:../manager/addschool.php");
        }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/addschool.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

                header("Location:../teatcher/index.php");

            }else if($_SESSION['user']['profile'] == "Student") {

                  header("Location:../student/index.php");

              }else if($_SESSION['user']['profile'] == "Parent") {

                    header("Location:../parent/index.php");

                }

       //header("Location:../manager/addschool.php");
      }else {
      $_SESSION['user']['addetabexist']="Cet Etablissement existe dejà dans le système";
      if($_SESSION['user']['profile'] == "Admin_globale") {

        header("Location:../manager/index.php");
        }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/index.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

                header("Location:../teatcher/index.php");

            }else if($_SESSION['user']['profile'] == "Student") {

                  header("Location:../student/index.php");

              }else if($_SESSION['user']['profile'] == "Parent") {

                    header("Location:../parent/index.php");

                }
      //header("Location:../manager/index.php");
      }




    }

    function CheckingLoginCni($login,$cni)
    {
      $req = $this->db->prepare("SELECT * FROM compte,parent where compte.id_compte=parent.idcompte_parent and ( compte.login_compte=? or parent.cni_parent=?) and compte.statut_compte=1");
      $req->execute([$login,$cni]);
      $data=$req->fetchAll();
      $nbStudent=count($data);

      return $nbStudent;
    }

    function CheckingLogin($login)
    {
        $req = $this->db->prepare("SELECT * FROM compte where  compte.login_compte=? and compte.statut_compte=1");
        $req->execute([$login]);
        $data=$req->fetchAll();
        $nbStudent=count($data);

        return $nbStudent;
    }

function UpdateEtabwithFile($codeetab,$libetab,$etabphone1,$etabphone2,$emailetab,$addresetab,$fichieretab)
{
  $req = $this->db->prepare("UPDATE  etablissement SET libelle_etab=?,tel_etab=?,tel1_etab=?,email_etab=?,adresse_etab=?,logo_etab=? where code_etab=?");
  $req->execute([$libetab,$etabphone1,$etabphone2,$emailetab,$addresetab,$fichieretab,$codeetab]);



}

function UpdateEtabwithoutFile($codeetab,$libetab,$etabphone1,$etabphone2,$emailetab,$addresetab)
{
  $req = $this->db->prepare("UPDATE  etablissement SET libelle_etab=?,tel_etab=?,tel1_etab=?,email_etab=?,adresse_etab=? where code_etab=?");
  $req->execute([$libetab,$etabphone1,$etabphone2,$emailetab,$addresetab,$codeetab]);


}

function getNbofStudentforThisschool($localid)
{
  $req = $this->db->prepare("SELECT * FROM eleve,compte where eleve.email_eleve=compte.email_compte and eleve.codeEtab_eleve=? and compte.statut_compte=1");
  $req->execute([$localid]);
  $data=$req->fetchAll();
  $nbStudent=count($data);

  return $nbStudent;
}

function getNbofStudentforThisschoolSession($codeEtab,$session)
{
  $req = $this->db->prepare("SELECT * FROM eleve,compte,inscription WHERE eleve.idcompte_eleve=compte.id_compte AND inscription.ideleve_inscrip=eleve.idcompte_eleve AND inscription.codeEtab_inscrip=? AND inscription.session_inscrip=? and compte.statut_compte=1");
  $req->execute([$codeEtab,$session]);
  $data=$req->fetchAll();
  $nbStudent=count($data);

  return $nbStudent;
}

function getNbofStudentforThisAllschoolSession($userId,$session)
{
  $req = $this->db->prepare("SELECT * FROM eleve,compte,inscription WHERE eleve.idcompte_eleve=compte.id_compte AND inscription.ideleve_inscrip=eleve.idcompte_eleve AND inscription.codeEtab_inscrip IN (SELECT distinct codeEtab_assign from assigner where assigner.id_adLocal=?) AND inscription.session_inscrip=? and compte.statut_compte=1");
  $req->execute([$userId,$session]);
  $data=$req->fetchAll();
  $nbStudent=count($data);

  return $nbStudent;
}

function getNbofStudentforThisschoolSessionList($codeEtab,$session)
{
  $req = $this->db->prepare("SELECT * FROM eleve,compte,inscription WHERE eleve.idcompte_eleve=compte.id_compte AND inscription.ideleve_inscrip=eleve.idcompte_eleve AND inscription.codeEtab_inscrip=? AND inscription.session_inscrip=? and compte.statut_compte=1");
  $req->execute([$codeEtab,$session]);
  $data=$req->fetchAll();

  return $data;
}

function getNumberStudentAttendanceToday($localid)
{
  $dateday=date("Y-m-d");
  $statut=1;
  $req = $this->db->prepare("SELECT * FROM eleve,compte,presences where compte.email_compte=eleve.email_eleve and eleve.matricule_eleve=presences.matricule_presence and eleve.codeEtab_eleve=? and presences.date_presence=? and presences.statut_presence=?");
  $req->execute([$localid,$dateday,$statut]);
  $data=$req->fetchAll();
  $nbAttendanceday=count($data);

  return $nbAttendanceday;
}

function getNumberofParentforThisschool($localid)
{
    $typecompte="Parent";
    $req = $this->db->prepare("SELECT * FROM eleve,compte,parent,parenter where parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and compte.id_compte=parent.idcompte_parent and compte.type_compte=? and eleve.codeEtab_eleve=? and compte.statut_compte=1");
    $req->execute([$typecompte,$localid]);
    $data=$req->fetchAll();
    $nbParent=count($data);

    return $nbParent;
}

// function getNumberofParentforThisschoolElement($codeEtab)
// {
//     $typecompte="Parent";
//     $req = $this->db->prepare("SELECT * FROM eleve,compte,parent,parenter where parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and compte.id_compte=parent.idcompte_parent and compte.type_compte=? and eleve.codeEtab_eleve=? and compte.statut_compte=1");
//     $req->execute([$typecompte,$codeEtab]);
//     $data=$req->fetchAll();
//     $nbParent=count($data);
//
//     return $nbParent;
// }

function getNumberofParentforThisschoolElement($codeEtab,$sessionEtab)
{
    $typecompte="Parent";
    $req = $this->db->prepare("SELECT * FROM inscription,parent,parenter,compte WHERE inscription.ideleve_inscrip=parenter.eleveid_parenter AND parenter.parentid_parenter=parent.idcompte_parent AND compte.id_compte=parenter.parentid_parenter AND compte.type_compte=? AND compte.statut_compte=1 AND inscription.codeEtab_inscrip=? AND inscription.session_inscrip =?");
    $req->execute([$typecompte,$codeEtab,$sessionEtab]);
    $data=$req->fetchAll();
    $nbParent=count($data);

    return $nbParent;
}



function getNumberofParentforThisAllschool($userId)
{
    $typecompte="Parent";
    $req = $this->db->prepare("SELECT distinct compte.id_compte FROM eleve,compte,parent,parenter where parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and compte.id_compte=parent.idcompte_parent and compte.type_compte=? and eleve.codeEtab_eleve IN(SELECT distinct codeEtab_assign from assigner where id_adLocal=?) and compte.statut_compte=1");
    $req->execute([$typecompte,$userId]);
    $data=$req->fetchAll();
    $nbParent=count($data);

    return $nbParent;
}

function getNumberofTeatcherforThisschool($localid)
{
  $req = $this->db->prepare("SELECT * FROM enseignant,compte,enseigner where compte.id_compte=enseigner.id_enseignant and enseignant.idcompte_enseignant=compte.id_compte and enseigner.codeEtab=? and compte.statut_compte=1");
  $req->execute([$localid]);
  $data=$req->fetchAll();
  $nbTeatcher=count($data);

  return $nbTeatcher;
}

function getNumberofTeatcherforThisAllschool($userId)
{
  $req = $this->db->prepare("SELECT distinct compte.id_compte FROM compte,enseignant,enseigner where compte.id_compte=enseigner.id_enseignant and enseignant.idcompte_enseignant=compte.id_compte and enseigner.codeEtab IN(SELECT distinct codeEtab_assign	 from assigner where 	id_adLocal=?) and compte.statut_compte=1");
  $req->execute([$userId]);
  $data=$req->fetchAll();
  $nbTeatcher=count($data);

  return $nbTeatcher;
}

function getStatisById($localid)
{
  //nous allons recuperer le nombre d'etablissment
  $req = $this->db->prepare("SELECT * FROM etablissement where code_etab=?");
  $req->execute([$localid]);
  $data=$req->fetchAll();
  $nbEtab=count($data);

  //nous allons recuperer le nombre admin local

  $typeadl="Admin_locale";
  $req1 = $this->db->prepare("SELECT * FROM compte,assigner where compte.id_compte=assigner.id_adLocal and assigner.codeEtab_assign=? and  type_compte=? and compte.statut_compte=1");
  $req1->execute([$localid,$typeadl]);
  $data1=$req1->fetchAll();
  $nbAdl=count($data1);

  //nous allons determiner le nombre d'enseignant

  $typeTea="Teatcher";
  $req2 = $this->db->prepare("SELECT * FROM compte ,enseigner where compte.id_compte=enseigner.id_enseignant and enseigner.codeEtab=? and  type_compte=? and compte.statut_compte=1");
  $req2->execute([$localid,$typeTea]);
  $data2=$req2->fetchAll();
  $nbTea=count($data2);

  $typePar="Parent";
  $req3 = $this->db->prepare("SELECT * FROM compte,parent,eleve,parenter where compte.id_compte=parent.idcompte_parent and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and eleve.codeEtab_eleve=? and  type_compte=? and compte.statut_compte=1");
  $req3->execute([$localid,$typePar]);
  $data3=$req3->fetchAll();
  $nbPar=count($data3);

  $donnees=$nbEtab."*".$nbAdl."*".$nbTea."*".$nbPar;

  return $donnees;

}

    function getStatis()
    {
      //nous allons recuperer le nombre d'etablissment
      $req = $this->db->prepare("SELECT * FROM etablissement");
      $req->execute();
      $data=$req->fetchAll();
      $nbEtab=count($data);

      //nous allons recuperer le nombre admin local

      $typeadl="Admin_local";
      $req1 = $this->db->prepare("SELECT * FROM compte where type_compte=?");
      $req1->execute([$typeadl]);
      $data1=$req1->fetchAll();
      $nbAdl=count($data1);

      //nous allons determiner le nombre d'enseignant

      $typeTea="Teacher";
      $req2 = $this->db->prepare("SELECT * FROM compte where type_compte=?");
      $req2->execute([$typeTea]);
      $data2=$req2->fetchAll();
      $nbTea=count($data2);

      $typePar="Parent";
      $req3 = $this->db->prepare("SELECT * FROM compte where type_compte=?");
      $req3->execute([$typePar]);
      $data3=$req3->fetchAll();
      $nbPar=count($data3);

      $donnees=$nbEtab."*".$nbAdl."*".$nbTea."*".$nbPar;

      return $donnees;

    }

    function getLoginProfilebyId($compteid)
  {
      $req = $this->db->prepare("SELECT * FROM compte where  id_compte=?");
      $req->execute([$compteid]);
      $data=$req->fetchAll();
      $array=json_encode($data,true);
      $someArray = json_decode($array, true);
      $donnees=$someArray[0]["login_compte"]."*".$someArray[0]["type_compte"];

      return $donnees;

  }

  function getLoginProfile($email)
{
    $req = $this->db->prepare("SELECT * FROM compte where  email_compte=?");
    $req->execute([$email]);
    $data=$req->fetchAll();
    $array=json_encode($data,true);
    $someArray = json_decode($array, true);
    $donnees=$someArray[0]["login_compte"]."*".$someArray[0]["type_compte"];

    return $donnees;

}

function getImageProfile($email)
{
  $req = $this->db->prepare("SELECT * FROM compte where  email_compte=?");
  $req->execute([$email]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);
  $donnees=$someArray[0]["photo_compte"];

  return $donnees;

}

function getImageProfilebyId($compteid)
{
  $req = $this->db->prepare("SELECT * FROM compte where  id_compte=?");
  $req->execute([$compteid]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);
  $donnees=$someArray[0]["photo_compte"];

  return $donnees;

}


    function CheckCompte($login)
    {
      $req = $this->db->prepare("SELECT * FROM compte where  login_compte=?");
      $req->execute([$login]);
      $data=$req->fetchAll();
      $nb=count($data);
      return $nb;
    }

    function CheckComptestatut($login)
    {
      $statut=1;
      $req = $this->db->prepare("SELECT * FROM compte where  login_compte=? and statut_compte=?");
      $req->execute([$login,$statut]);
      $data=$req->fetchAll();
      $nb=count($data);
      return $nb;
    }

    function CheckComptepasswd($login,$pass)
    {

      $req = $this->db->prepare("SELECT * FROM compte where  login_compte=? and pass_compte=?");
      $req->execute([$login,$pass]);
      $data=$req->fetchAll();
      $nb=count($data);
      return $nb;
    }

    function LogMephone($login,$pass)
    {
      $req = $this->db->prepare("SELECT * FROM compte WHERE login_compte = ? AND pass_compte = ?");
      $req->execute([$login,$pass]);
      $data= $req->fetchAll();

      if(count($data)==1){

        return json_encode($data, JSON_FORCE_OBJECT);

      }else {
         return json_encode("Echec", JSON_FORCE_OBJECT);

      }



    }

    function LogMe($login,$pass)
    {

      $req = $this->db->prepare("SELECT * FROM compte WHERE login_compte = ? AND pass_compte = ? and statut_compte=1");
      $req->execute([$login,$pass]);
      $data= $req->fetchAll();

      if(count($data)==1){

        $_SESSION['user']=array(
    "login"=>$data['0']->login_compte,
    "email"=>$data['0']->email_compte,
    "password"=>$data['0']->pass_compte,
    "nom"=>$data['0']->nom_compte,
    "prenoms"=>$data['0']->prenom_compte,
    "profile"=>$data['0']->type_compte,
    "telephone"=>$data['0']->tel_compte,
    "IdCompte"=>$data['0']->id_compte,
    "fonctionuser"=>$data['0']->fonction_compte,

            );
      }

      //nous allons chercher le pays dans lequel l'etablissement se trouve dans quel pays
      $etabs=new Etab();
      $parents=new ParentX();

      $etabs->setonlineUser($_SESSION['user']['IdCompte']);

      if($_SESSION['user']['profile']=="Admin_locale")
      {
        $_SESSION['user']['groupeselect']="ALL";
          $nbEtabassigner=$etabs->getNbetabassigner($_SESSION['user']['IdCompte']);

          if($nbEtabassigner>1)
          {
            $_SESSION['user']['groupe']=1;

            $_SESSION['user']['nbEtab']=1;

            //nous allons verifier que cet etablissement est un groupe

            //recuperation du premier etablissement

            $codeEtab=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
            $paysid=$etabs->getEtabpaysid($codeEtab);
            $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etabs->DetermineTypeEtab($codeEtab);
            $_SESSION['user']['paysid']=$paysid;
            $_SESSION['user']['codeEtab']=$codeEtab;

            $primareEtab=$etabs->getPrimaireEtabValue($_SESSION['user']['codeEtab']);
            $_SESSION['user']['primaire']=$primareEtab;

          }else if($nbEtabassigner==1)
          {
            $_SESSION['user']['nbEtab']=0;
            $codeEtab=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
            $paysid=$etabs->getEtabpaysid($codeEtab);
            $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etabs->DetermineTypeEtab($codeEtab);

            $primareEtab=$etabs->getPrimaireEtabValue($_SESSION['user']['codeEtab']);
            $_SESSION['user']['primaire']=$primareEtab;

            $_SESSION['user']['paysid']=$paysid;
            // $_SESSION['user']['groupe']=0;
            $groupestate=$etabs->getgroupestateEtabs($codeEtab);
            if($groupestate==1)
            {
              $_SESSION['user']['groupe']=1;

            }else {
              $_SESSION['user']['groupe']=0;
            }
            $_SESSION['user']['codeEtab']=$codeEtab;
          }




          $_SESSION['user']['lang']='fr';

// echo $typeetab;
            if($typeetab==5)
            {
                header("Location:../locale".$libelleEtab."/index.php");
            }else {
                header("Location:../locale/index.php");
            }


      }else if($_SESSION['user']['profile'] == "Admin_globale")
      {
             header("Location:../manager/index.php");
      }else if($_SESSION['user']['profile'] == "Teatcher") {

        //nous allons chercher si l'enseignant dispense des cours dans un etablissement IBSA

        $IBSAschool=$etabs->getIbsaschools();
        $codeetabs="";
        foreach ($IBSAschool as  $value):
          $codeetabs=$codeetabs.$value->code_etab."*";
        endforeach;
        $tabetabs=explode("*",$codeetabs);

        $nbdispenser=0;

        $dispenserdatas=$etabs->getdispenserTeatcher($_SESSION['user']['IdCompte']);

        foreach ($dispenserdatas as $value):
          $code=$value->codeEtab;
          if (in_array($code, $tabetabs)) {
            $nbdispenser++;
           }
        endforeach;

        // echo $nbdispenser." - ".$code;

        $primareEtab=$etabs->getPrimaireEtabValue($code);
        $libelleEtab=$etabs->getEtabLibellebyCodeEtab($code);
        echo $libelleEtab;
        $_SESSION['user']['primaire']=$primareEtab;

        header("Location:../teatcher".$libelleEtab."/index.php");

        if($nbdispenser>0)
        {
          // h
        }else {
          // header("Location:../teatcher/index.php");
        }

            // header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

              $IBSAschool=$etabs->getIbsaschools();
              $codeetabs="";
              foreach ($IBSAschool as  $value):
                $codeetabs=$codeetabs.$value->code_etab."*";
              endforeach;
              $tabetabs=explode("*",$codeetabs);

              $parentlyStudent=$parents->getDifferentStudentByParentId($_SESSION['user']['IdCompte']);
              $nbinscripibsa=0;
              foreach ($parentlyStudent as $value):
                $code=$value->codeEtab_eleve;
                if (in_array($code, $tabetabs)) {
                  $nbinscripibsa++;
                 }
              endforeach;

              echo $nbinscripibsa;

              $libelleEtab=$etabs->getEtabLibellebyCodeEtab($code);

              header("Location:../parent".$libelleEtab."/index.php");

            // var_dump($parentlyStudent);

              // if($nbinscripibsa>0)
              // {
              //   header("Location:../parentIBSA/index.php");
              // }else {
              //   header("Location:../parent/index.php");
              // }

                // header("Location:../parent/index.php");

            }



      // echo $paysid;

      //nous allons mettre a jour la date de dernière connexion

      // if($typeetab==5)
      // {
      //   if($paysid==4)
      //   {
      //     if($_SESSION['user']['profile'] == "Admin_globale") {
      //
      //           header("Location:../manager/index.php");
      //
      //       }else if($_SESSION['user']['profile'] == "Admin_locale") {
      //
      //
      //
      //             header("Location:../localecmr/index.php");
      //
      //         }else if($_SESSION['user']['profile'] == "Teatcher") {
      //
      //               header("Location:../teatcher/index.php");
      //
      //           }else if($_SESSION['user']['profile'] == "Student") {
      //
      //                 header("Location:../student/index.php");
      //
      //             }else if($_SESSION['user']['profile'] == "Parent") {
      //
      //                   header("Location:../parent/index.php");
      //
      //               }
      //   }else {
      //     if($_SESSION['user']['profile'] == "Admin_globale") {
      //
      //           header("Location:../manager/index.php");
      //
      //       }else if($_SESSION['user']['profile'] == "Admin_locale") {
      //
      //
      //
      //             header("Location:../locale".$libelleEtab."/index.php");
      //
      //         }else if($_SESSION['user']['profile'] == "Teatcher") {
      //
      //               header("Location:../teatcher/index.php");
      //
      //           }else if($_SESSION['user']['profile'] == "Student") {
      //
      //                 header("Location:../student/index.php");
      //
      //             }else if($_SESSION['user']['profile'] == "Parent") {
      //
      //                   header("Location:../parent/index.php");
      //
      //               }
      //   }
      // }else {
      //
      //   if($paysid==4)
      //   {
      //     if($_SESSION['user']['profile'] == "Admin_globale") {
      //
      //           header("Location:../manager/index.php");
      //
      //       }else if($_SESSION['user']['profile'] == "Admin_locale") {
      //
      //
      //
      //             header("Location:../localecmr/index.php");
      //
      //         }else if($_SESSION['user']['profile'] == "Teatcher") {
      //
      //               header("Location:../teatcher/index.php");
      //
      //           }else if($_SESSION['user']['profile'] == "Student") {
      //
      //                 header("Location:../student/index.php");
      //
      //             }else if($_SESSION['user']['profile'] == "Parent") {
      //
      //                   header("Location:../parent/index.php");
      //
      //               }
      //   }else {
      //     if($_SESSION['user']['profile'] == "Admin_globale") {
      //
      //           header("Location:../manager/index.php");
      //
      //       }else if($_SESSION['user']['profile'] == "Admin_locale") {
      //
      //
      //
      //             header("Location:../locale/index.php");
      //
      //         }else if($_SESSION['user']['profile'] == "Teatcher") {
      //
      //               header("Location:../teatcher/index.php");
      //
      //           }else if($_SESSION['user']['profile'] == "Student") {
      //
      //                 header("Location:../student/index.php");
      //
      //             }else if($_SESSION['user']['profile'] == "Parent") {
      //
      //                   header("Location:../parent/index.php");
      //
      //               }
      //   }
      //
      // }





    }

    function AddEtab($codeetab,$libetab,$etabphone1,$etabphone2,$emailetab,$addresetab,$fichieretab,$datecrea,$emailuser,$pays,$typeetab,$sigleetab)
    {
      $etabs=new Etab();

      $check=$etabs->existEtabCountry($codeetab,$pays);

      if($check==0)
      {
        //cet etablissment n'existe pas encore dans la base il faut l'ajouter

      $req = $this->db->prepare("INSERT INTO etablissement SET code_etab=?,libelle_etab=?,tel_etab=?,tel1_etab=?,email_etab=?,adresse_etab=?,logo_etab=?,datecrea_etab=?,createby_etab=?,pays_etab=?,type_etab=?,sigle_etab=?");
      $req->execute([
      $codeetab,
      $libetab,
      $etabphone1,
      $etabphone2,
      $emailetab,
      $addresetab,
      $fichieretab,
      $datecrea,
      $emailuser,
      $pays,
      $typeetab,
      $sigleetab
    ]);

      $_SESSION['user']['addetabok']="Etablissement ajouté avec succès";

      if($_SESSION['user']['profile'] == "Admin_globale") {

          header("Location:../manager/addschool.php");
        }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/addschool.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

                header("Location:../teatcher/index.php");

            }else if($_SESSION['user']['profile'] == "Student") {

                  header("Location:../student/index.php");

              }else if($_SESSION['user']['profile'] == "Parent") {

                    header("Location:../parent/index.php");

                }

       //header("Location:../manager/addschool.php");
      }else {
      $_SESSION['user']['addetabexist']="Cet Etablissement existe dejà dans le système";
      if($_SESSION['user']['profile'] == "Admin_globale") {

        header("Location:../manager/index.php");
        }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale/index.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

                header("Location:../teatcher/index.php");

            }else if($_SESSION['user']['profile'] == "Student") {

                  header("Location:../student/index.php");

              }else if($_SESSION['user']['profile'] == "Parent") {

                    header("Location:../parent/index.php");

                }
      //header("Location:../manager/index.php");
      }




    }




}


 ?>
