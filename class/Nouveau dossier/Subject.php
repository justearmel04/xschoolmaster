<?php

class Subject{

public $db;
function __construct() {

  require_once('../class/cnx.php');

  $db = new mysqlConnector();
  $this->db= $db->dataBase;
}

//recherche des classes assigner à un professeur

function AllreadyExistMatiere()
{
  return 1;
}

function getNumberClassesOfTeatcherId($idcompte)
{
  $req = $this->db->prepare("SELECT * FROM classe,enseigner,compte WHERE compte.id_compte=enseigner.id_enseignant and enseigner.id_classe=classe.id_classe and compte.id_compte=? ");
  $req->execute([$idcompte]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getClassesOfTeatcherId($idcompte)
{
  $req = $this->db->prepare("SELECT * FROM classe,enseigner,compte WHERE compte.id_compte=enseigner.id_enseignant and enseigner.id_classe=classe.id_classe and compte.id_compte=?");
  $req->execute([$idcompte]);
  return $req->fetchAll();
}

function ExisteClasses($libetab,$classe)
  {
    $req = $this->db->prepare("SELECT * FROM classe where libelle_classe=? and codeEtab_classe=?");
    $req->execute([$classe,$libetab]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;
  }

  function Addclasse($libelleclasse,$codeEtab)
  {
    $dateday=date("Y-m-d");
    $statut=1;
    $req = $this->db->prepare("INSERT INTO classe SET libelle_classe=?,codeEtab_classe=?,datecrea_classe=?,statut_classe=?");
    $req->execute([$libelleclasse,$codeEtab,$dateday,$statut]);

    $_SESSION['user']['addclasseok']="Classe ajouté avec succès";

    if($_SESSION['user']['profile'] == "Admin_globale") {

    header("Location:../manager/addclasses.php");
      }else if($_SESSION['user']['profile'] == "Admin_locale") {

      header("Location:../locale/addclasses.php");

        }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/index.php");

          }else if($_SESSION['user']['profile'] == "Student") {

                header("Location:../student/index.php");

            }else if($_SESSION['user']['profile'] == "Parent") {

                  header("Location:../parent/index.php");

              }

     //header("Location:../manager/addclasses.php");

  }

  function getAllclassesOfassignated($idlocal)
  {
    $req = $this->db->prepare("SELECT * FROM classe,assigner,compte where classe.codeEtab_classe=assigner.codeEtab_assign and assigner.id_adLocal=compte.id_compte and compte.id_compte=? ");
    $req->execute([$idlocal]);
    return $req->fetchAll();
  }

  function getAllclasses()
  {
    $req = $this->db->prepare("SELECT * FROM classe order by datecrea_classe DESC");
    $req->execute([]);
    return $req->fetchAll();

  }

  function getAllClassesbyschoolCode($code)
  {
        $req = $this->db->prepare("SELECT * FROM classe,etablissement WHERE classe.codeEtab_classe=etablissement.code_etab and etablissement.code_etab=? ");
        $req->execute([$code]);
        return $req->fetchAll();
  }

  function getClassesByschoolCodewithId($codeetab,$classex)
  {
    $req = $this->db->prepare("SELECT * FROM classe,etablissement WHERE classe.codeEtab_classe=etablissement.code_etab and etablissement.code_etab=? and 	id_classe=?");
    $req->execute([$codeetab,$classex]);
    return $req->fetchAll();
  }

  /*function getAllClassesByschoolCode($codeetab)
  {
      $req = $this->db->prepare("SELECT * FROM classe,etablissement WHERE classe.codeEtab_classe=etablissement.code_etab and etablissement.code_etab=?");
      $req->execute([$codeetab]);
      return $req->fetchAll();
  }*/

  function getAllClassesByClasseId($classex)
  {
    $req = $this->db->prepare("SELECT * FROM classe,etablissement WHERE classe.codeEtab_classe=etablissement.code_etab and id_classe=?");
    $req->execute([$classex]);
    return $req->fetchAll();
  }





}

 ?>
