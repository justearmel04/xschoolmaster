<?php

class Student{



  public $db;

  function __construct() {



    require_once('../class/cnx.php');



    $db = new mysqlConnector();

    $this->db= $db->dataBase;

  }

  function UpdatenotesPrimary($ideleve,$idtypenote,$notes,$observation,$classeId,$matiereid,$codeEtab,$teatcherid,$sessionlibelle)
  {
      $req = $this->db->prepare("UPDATE notes set valeur_notes=?,	obser_notes=? where idtype_notes=? and idclasse_notes=?,idmat_notes=? and idprof_notes=? and ideleve_notes=? and 	codeEtab_notes=? and session_notes=? and notes.type_notes=1");
        $req->execute([
          $notes,
          $observation,
          $idtypenote,
          $classeId,
          $matiereid,
          $teatcherid,
          $ideleve,
          $codeEtab,
          $sessionlibelle
        ]);




  }


  function UpdatenoteExamPrimary($ideleve,$idtypenote,$notes,$observation,$classeId,$matiereid,$codeEtab,$teatcherid,$sessionlibelle)
  {
      $req = $this->db->prepare("UPDATE notes set valeur_notes=?,	obser_notes=? where idtype_notes=? and idclasse_notes=? and idmat_notes=? and idprof_notes=? and ideleve_notes=? and 	codeEtab_notes=? and session_notes=? and notes.type_notes=2");
        $req->execute([
          $notes,
          $observation,
          $idtypenote,
          $classeId,
          $matiereid,
          $teatcherid,
          $ideleve,
          $codeEtab,
          $sessionlibelle
        ]);




  }


  function getallidsofstudentprecis($message,$codeetab,$session)
  {
      $req = $this->db->prepare("SELECT * from messages where id_msg=? and codeEtab_msg=? and session_msg=?");
      $req->execute([$message,$codeetab,$session]);
      $data=$req->fetchAll();
      $array=json_encode($data,true);
      $someArray = json_decode($array, true);
      $donnees=$someArray[0]["eleves_msg"];
      return $donnees;

  }

  function DetermineNoteNumberexamen($eleveid,$classe,$examenid,$matiereid,$teatcherExamid,$codeEtabAssigner)
  {
    $req = $this->db->prepare("SELECT * from eleve,classe,matiere,notes,examen where notes.ideleve_notes=eleve.idcompte_eleve and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and examen.id_exam=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and classe.id_classe=?  and matiere.id_mat=? and examen.id_exam=? and classe.codeEtab_classe=? and notes.type_notes=2 and notes.ideleve_notes=?  ");
    $req->execute([$classe,$matiereid,$examenid,$codeEtabAssigner,$eleveid]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;
  }

  function getNotesexameninformations($idcompte_eleve,$classe,$matiereid,$examid,$codeEtabAssigner)
  {
    $req = $this->db->prepare("SELECT * from eleve,classe,matiere,notes,examen where notes.ideleve_notes=eleve.idcompte_eleve and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and examen.id_exam=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and  classe.id_classe=? and notes.idmat_notes=?  and examen.id_exam=?  and classe.codeEtab_classe=? and notes.type_notes=2 and notes.ideleve_notes=?  ");
    $req->execute([$classe,$matiereid,$examid,$codeEtabAssigner,$idcompte_eleve]);
    return $req->fetchAll();
  }

  function DetermineNoteNumberexamens($idcompte_eleve,$classe,$matiereid,$examid,$codeEtabAssigner)
  {
    $req = $this->db->prepare("SELECT * from eleve,classe,matiere,notes,examen where notes.ideleve_notes=eleve.idcompte_eleve and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and examen.id_exam=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and  classe.id_classe=? and notes.idmat_notes=?  and examen.id_exam=?  and classe.codeEtab_classe=? and notes.type_notes=2 and notes.ideleve_notes=?  ");

    $req->execute([$classe,$matiereid,$examid,$codeEtabAssigner,$idcompte_eleve]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;
  }

  function DetermineNoteNumbercontroles($idcompte_eleve,$classe,$matiereid,$controleid,$codeEtabAssigner)
  {
      $req = $this->db->prepare("SELECT * from eleve,classe,matiere,notes,controle where notes.ideleve_notes=eleve.idcompte_eleve and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and controle.id_ctrl=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and classe.id_classe=?  and matiere.id_mat=? and controle.id_ctrl=? and classe.codeEtab_classe=? and notes.type_notes=1 and notes.ideleve_notes=?  ");
      $req->execute([$classe,$matiereid,$controleid,$codeEtabAssigner,$idcompte_eleve]);
      $data=$req->fetchAll();
      $nb=count($data);
      return $nb;
  }

  function getNotescontroleinformations($idcompte_eleve,$classe,$matiereid,$controleid,$codeEtabAssigner)
  {
    $req = $this->db->prepare("SELECT * from eleve,classe,matiere,notes,controle where notes.ideleve_notes=eleve.idcompte_eleve and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and controle.id_ctrl=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and classe.id_classe=?  and matiere.id_mat=? and controle.id_ctrl=? and classe.codeEtab_classe=? and notes.type_notes=1 and notes.ideleve_notes=?  ");
    $req->execute([$classe,$matiereid,$controleid,$codeEtabAssigner,$idcompte_eleve]);
    return $req->fetchAll();
  }

  // function getNotesexameninformations($eleveid,$classe,$matiereid,$examenid,$codeEtabAssigner)
  // {
  //   $req = $this->db->prepare("SELECT * from eleve,classe,matiere,notes,examen where notes.ideleve_notes=eleve.idcompte_eleve and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and examen.id_exam=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and classe.id_classe=?  and matiere.id_mat=? and examen.id_exam=? and classe.codeEtab_classe=? and notes.type_notes=2 and notes.ideleve_notes=?  ");
  //   $req->execute([$classe,$matiereid,$examenid,$codeEtabAssigner,$eleveid]);
  //   return $req->fetchAll();
  // }


  function Listeinscritsthisyear($codeEtabAssigner,$libellesessionencours)
  {
    $req = $this->db->prepare("SELECT * from compte,eleve,inscription,classe where compte.id_compte=eleve.idcompte_eleve and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and inscription.codeEtab_inscrip=? and inscription.session_inscrip=? order by compte.nom_compte Asc");
    $req->execute([$codeEtabAssigner,$libellesessionencours]);
    return $req->fetchAll();
  }

  function getNbAttendanceDayMat($matricule,$dateday,$classe,$matiereid)
  {
    $req = $this->db->prepare("SELECT statut_presence FROM presences where matricule_presence=? and classe_presence=? and date_presence=? and matiere_presence=? ");

    $req->execute([$matricule,$classe,$dateday,$matiereid]);

    $data=$req->fetchAll();

    $nb=count($data);

    return $nb;
  }

  function getstatutAttendanceDayMat($matricule,$dateday,$classe,$matiereid)
  {
    $req = $this->db->prepare("SELECT * FROM presences where matricule_presence=? and classe_presence=? and date_presence=? and matiere_presence=? ");

    $req->execute([$matricule,$classe,$dateday,$matiereid]);

    return $req->fetchAll();
  }

  function getParentStudentSelected($studentid,$parentid)
  {
    $req = $this->db->prepare("SELECT * from compte,parent,parenter,eleve where compte.id_compte=parent.idcompte_parent and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and parenter.eleveid_parenter=? and parenter.parentid_parenter=?");
    $req->execute([$studentid,$parentid]);
    return $req->fetchAll();
  }

  function getParentStudentSelectedAll($studentid)
  {
    $req = $this->db->prepare("SELECT * from compte,parent,parenter,eleve where compte.id_compte=parent.idcompte_parent and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and parenter.eleveid_parenter=?");
    $req->execute([$studentid]);
    return $req->fetchAll();
  }

  function getNbAttendanceDay($matricule,$dateday,$classe)
  {
    $req = $this->db->prepare("SELECT statut_presence FROM presences where matricule_presence=? and classe_presence=? and date_presence=? ");

    $req->execute([$matricule,$classe,$dateday]);

    $data=$req->fetchAll();

    $nb=count($data);

    return $nb;
  }

  function generateRecapattendanceDay($codeEtab,$classeid,$session,$datedeb,$nb)
  {
    require_once('../class_html2PDF/html2pdf.class.php');
    require_once('../class_html2PDF/_tcpdf_5.0.002/config/lang/eng.php');
    require_once('../class_html2PDF/_tcpdf_5.0.002/tcpdf.php');
    require_once('../class/Etablissement.php');
    require_once('../controller/functions.php');

    $etabs=new Etab();
    $classeShool=new Classe();
    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
    $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
    $lienlogo="../logo_etab/".$codeEtab."/".$logoEtab;
    $contacts="";
    $infosEtab=$etabs->getEtabInfosbyCode($codeEtab);
    $tabEtab=explode('*',$infosEtab);

     if($tabEtab[3]!="")
     {
       $contacts=$tabEtab[2]." / ".$tabEtab[3];
     }else {
       $contacts=$tabEtab[2];
     }

     $datefin=date("d-m-Y", strtotime("+".$nb."day", strtotime($datedeb)));

     $student=new Student();
     $dataday=$etabs->getAllweeks();
     $libelleclasse=$classeShool->getInfosofclassesbyId($classeid,$session);
     $students=$student->getAllstudentofthisclassesSession($classeid,$session);

     ob_start();

     ?>
     <page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
       <page_header>
       <table style="width: 100%; border: solid 0px #000; font-size: 12px; font-family: Arial, Helvetica, sans-serif; margin-top:-8px;">
             <tr>
             <td width="25%" height="100" style="width: 30%;"><img src="<?php echo $lienlogo ?>" width="120" height="70" /></td>
             <td style="width: 30%;">
             <table width="70%" height="38" border="0" align="center" cellspacing="0">
               <tr></tr>
             </table>     </td>
             </tr>
             <tr>
             <td valign="top" style="width: 100%; font-size: 12px; font-weight: normal;" rowspan="">

             <p style="border:1px solid black; padding:10px; width:60%;">

               <span class="Style3"><b>EmaiL </b></span> : <?php echo $tabEtab[4];?><span></span><br /><br />
               <span class="Style3"><b>Telephone </b></span> :<?php echo $contacts; ?><span></span><br /><br />

               </p>

             <br/>
             </td>

             </tr>

             </table>
       </page_header>
        <h3><span style='margin-top:100px;margin-left:235px;'>RECAPITULATIF DES PRESENCES DE CLASSE : <?php echo $libelleclasse; ?> </span></h3>
        <h3><span style='margin-top:80px;margin-left:235px;'>PERIODE : <?php echo $datedeb.' au '.$datefin; ?> </span></h3>
           <table id='table' border='1'cellpadding='3' cellspacing='0' style='margin-top:100px;width:100%;margin-left:-35px;width:1625px;'>
             <thead>
               <tr>
                   <!--th style="width:15%;height:10px;padding:3%;margin:15%"-->
                     <th style="padding:20px;">
                       # ELEVES \ JOURS
                   </th>
                   <?php
                     for($x=0;$x<=$nb;$x++)
                     {
                     ?>
                     <th style="">
                         <?php
                         $deadLine = date("d-m", strtotime("+".$x."day", strtotime($datedeb)));
                         echo $deadLine ; ?>
                     </th>
                   <?php
                     }
                   ?>


               </tr>
             </thead>
             <tbody>
               <?php
               $i=1;

               foreach ($students as $value):
                ?>
               <tr>
                 <td style=""><?php  echo $value->nom_eleve." ".$value->prenom_eleve?></td>
                 <?php

                 for($x=0;$x<=$nb;$x++)
                   {
                   ?>
                      <td style="width:5px;margin:5px;padding:5px;text-align:center">
                       <?php

                        $dateday= date("Y-m-d", strtotime("+".$x."day", strtotime($datedeb)));
                       $matricule=$value->matricule_eleve;
                        $nombre=$student->getNbAttendanceDay($matricule,$dateday,$classeid);

                         if($nombre==0)
                         {
                           echo $nombre;
                         }else {
                           $number=$student->getstatutAttendanceDay($matricule,$dateday,$classeid);
                           $array=json_encode($number,true);
                           $someArray = json_decode($array, true);
                           echo $someArray[0]["statut_presence"];
                           //var_dump($number[0]['statut_presence']);
                         }

                         ?>
                   </td>
                 <?php
               }
                 ?>
              </tr>
              <?php
              $i++;

                  endforeach;
               ?>

             </tbody>
          </table>

     </page>


     <?php
     $content=ob_get_clean();

     $html2pdf = new HTML2PDF('L', 'A4', 'fr');
     $html2pdf->pdf->SetDisplayMode('real');

     $html2pdf->writeHTML($content);
     //ob_clean();
     $completName="Recapattendance_".$libelleclasse;

     $html2pdf->Output('../generated/'.$completName.'.pdf','F');

     return '../generated/'.$completName.'.pdf';


  }

  function getstatutAttendanceDay($matricule,$dateday,$classe)
  {
    $req = $this->db->prepare("SELECT * FROM presences where matricule_presence=? and classe_presence=? and date_presence=? ");

    $req->execute([$matricule,$classe,$dateday]);

    return $req->fetchAll();

  }

  function getStudentIdbyMat($matricule)
  {
      $req = $this->db->prepare("SELECT distinct idcompte_eleve FROM eleve where eleve.matricule_eleve=?");
      $req->execute([$matricule]);
      $data=$req->fetchAll();
      $array=json_encode($data,true);
      $someArray = json_decode($array, true);
      $donnees=$someArray[0]["idcompte_eleve"];
      return $donnees;
  }

  function getAllstudentofthisclassesSessionPrivate($classe,$session,$matriculedelete,$codeEtabAssigner)
  {
    // $req = $this->db->prepare("SELECT * FROM compte,eleve,inscription,classe where compte.id_compte=eleve.idcompte_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and  inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=? and inscription.codeEtab_inscrip=? and inscription.idclasse_inscrip=? and NOT eleve.idcompte_eleve  IN (SELECT eleve.idcompte_eleve FROM eleve WHERE eleve.idcompte_eleve in (?))");
    // $req = $this->db->prepare("SELECT * FROM eleve,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.codeEtab_inscrip=? and  NOT eleve.idcompte_eleve IN (SELECT eleve.idcompte_eleve FROM eleve,compte,inscription,classe WHERE compte.id_compte=eleve.idcompte_eleve and inscription.ideleve_inscrip=compte.id_compte and inscription.idclasse_inscrip=classe.id_classe and inscription.codeEtab_inscrip=? and inscription.session_inscrip=? and inscription.idclasse_inscrip=? and  eleve.idcompte_eleve in (?))");
    // $imploded_fruits = implode(',',$fruits);
    $req = $this->db->prepare("SELECT * FROM eleve where  idcompte_eleve in (?)");
    $req->execute([$matriculedelete]);
    // $req->execute([$codeEtabAssigner,$session,$classe,$matriculedelete]);
    // $req->execute([$session,$classe,$codeEtabAssigner,$matriculedelete]);
    return $req->fetchAll();
  }

  function getRatingSum($ideleve,$typesess,$session,$classe)
  {
      $req = $this->db->prepare("SELECT sum(rating) as totalrating, sum(totalcoef_rating) as totalcoef from rating where ideleve_rating=? and classe_rating=? and session_rating=? and typsession_rating=?");
      $req->execute([$ideleve,$classe,$session,$typesess]);
      $data=$req->fetchAll();
      $array=json_encode($data,true);
      $someArray = json_decode($array, true);
      $donnees=$someArray[0]["totalrating"]."*".$someArray[0]["totalcoef"];
      return $donnees;
  }

  function generateHonortabFile($codeEtab,$classeEtab,$session,$typesession,$libelleclasse)
  {
    require_once('../class_html2PDF/html2pdf.class.php');
    require_once('../class_html2PDF/_tcpdf_5.0.002/config/lang/eng.php');
    require_once('../class_html2PDF/_tcpdf_5.0.002/tcpdf.php');
    require_once('../class/Etablissement.php');
    require_once('../class/Matiere.php');
    require_once('../class/Sessionsacade.php');
    require_once('../controller/functions.php');

    $etabs=new Etab();
    $classeShool=new Classe();
    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
    $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
    $lienlogo="../logo_etab/".$codeEtab."/".$logoEtab;
    $contacts="";
    $infosEtab=$etabs->getEtabInfosbyCode($codeEtab);
    $tabEtab=explode('*',$infosEtab);

     if($tabEtab[3]!="")
     {
       $contacts=$tabEtab[2]." / ".$tabEtab[3];
     }else {
       $contacts=$tabEtab[2];
     }

     $subjectmat=new Matiere();
     $Etudiant= new Student();
     $sessionacademique= new Sessionacade();

     //recuperer le type session academique

     $libelletypeacademique=$sessionacademique->getTypesessionAcadem($codeEtab,$session,$typesession);


     $classesubject=$subjectmat->getAllSubjectOfClasses($classeEtab,$codeEtab,$session);

     $nbsubjectclasse=count($classesubject);

     //rechercher la liste des eleves inscrits dans cette classe

     $idStudentclasses=$Etudiant->RecuperationAllstudentofthisclassesSession($classeEtab,$codeEtab,$session);

     $tabsubjectclasses="";
     $tabsubjectTeatchers="";
     $tabidstudentclasses="";
     $tabsommeratingstudent="";

     if($nbsubjectclasse>0)
     {
       foreach ($classesubject as $value):
         $tabsubjectclasses=$tabsubjectclasses.$value->id_mat."*";
         $tabsubjectTeatchers=$tabsubjectTeatchers.$value->teatcher_mat."*";


       endforeach;

       foreach ($idStudentclasses as $valuestudent):


     //somme des moyennes etudiant
     $sommedatas=$Etudiant->getRatingSum($valuestudent->id_compte,$typesession,$session,$classeEtab);
     $tabsommedatas=explode("*",$sommedatas);
     $sommerating=$tabsommedatas[0];
     $sommecoeffrating=$tabsommedatas[1];

     $moyennetotale=$sommerating/$sommecoeffrating;

     // echo "Id student".$valuestudent->id_compte."<br>";
     // echo "Somme moyenne".$sommerating."<br>";
     // echo "Somme coefficient".$sommecoeffrating."<br>";
     // echo "moyenne totale".$moyennetotale."<br>";

       if($moyennetotale>=12)
       {
         $tabidstudentclasses=$tabidstudentclasses.$valuestudent->id_compte."*";
         $tabsommeratingstudent=$tabsommeratingstudent.$moyennetotale."*";
       }
       endforeach;




     //   foreach ($idStudentclasses as $valuestudent):
     // $tabidstudentclasses=$tabidstudentclasses.$valuestudent->id_compte."*";
     // //somme des moyennes etudiant
     // $ratingStud=$Etudiant->getRatingSum();
     //
     //   endforeach;

     $arraystudentid=explode("*",substr($tabidstudentclasses, 0, -1));
     $arraymoyennetrimes= explode("*",substr($tabsommeratingstudent, 0, -1));

     // var_dump($arraystudentid);
     $nbstudentHonneur=count($arraystudentid);

     // echo $nbstudentHonneur;

     }

      ob_start();

      ?>

      <page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
        <page_header>
        <table style="width: 100%; border: solid 0px #000; font-size: 12px; font-family: Arial, Helvetica, sans-serif; margin-top:-8px;">
              <tr>
              <td width="25%" height="100" style="width: 30%;"><img src="<?php echo $lienlogo ?>" width="120" height="70" /></td>
              <td style="width: 30%;">
              <table width="70%" height="38" border="0" align="center" cellspacing="0">
                <tr></tr>
              </table>     </td>
              </tr>
              <tr>
              <td valign="top" style="width: 100%; font-size: 12px; font-weight: normal;" rowspan="">

              <p style="border:1px solid black; padding:10px; width:60%;">

                <span class="Style3"><b>EmaiL </b></span> : <?php echo $tabEtab[4];?><span></span><br /><br />
                <span class="Style3"><b>Telephone </b></span> :<?php echo $contacts; ?><span></span><br /><br />

                </p>

              <br/>
              </td>

              </tr>

              </table>
        </page_header><br /><br />
         <h3><span style='margin-top:100px;margin-left:100px;'>LISTE DES MERITANTS TABLEAU D'HONNEUR : <?php echo $libelleclasse; ?>  </span></h3>
         <h3><span style='margin-top:80px;margin-left:275px;'><?php echo $libelletypeacademique; ?>  </span></h3>
            <table id='table' border='1'cellpadding='3' cellspacing='0' style='margin-top:100px;width:100%;margin-left:-40px;width:1625px;'>
              <thead>
                  <tr>
                      <td style="text-align:center;margin:3px;padding:4px;">N&deg;</td>
                      <th style='width:9%;height:20px;text-align:center;padding:4px;'>Matricule</th>
                      <th style='width:10%;height:20px;text-align:center;'> Nom </th>
                      <th style='width:22%;height:20px;text-align:center;'> Prénoms </th>
                       <th style='width:4%;height:20px;text-align:center;'> Moy </th>
                  </tr>
              </thead>
              <tbody>

                  <?php
                  $j=1;
                    for($i=0;$i<$nbstudentHonneur;$i++)
                    {
                      $studentInfos=$Etudiant->getAllInformationsOfStudent($arraystudentid[$i],$session);
                      $tabStudent=explode("*",$studentInfos);
                      ?>
                      <tr>
                        <td style="text-align:center;margin:3px;padding:4px;"><?php echo $j; ?></td>
                      <td style="text-align:center;margin:3px;padding:4px;"><?php echo $tabStudent[1]; ?></td>
                      <td><?php echo $tabStudent[2]; ?></td>
                      <td><?php echo $tabStudent[3]; ?></td>
                      <td style="text-align:center;margin:3px;padding:4px;"><?php echo $arraymoyennetrimes[$i]; ?></td>
                      </tr>
                      <?php
                      $j++;
                    }
                   ?>


             </tbody>
           </table>

      </page>

      <?php
      $content=ob_get_clean();

      $html2pdf = new HTML2PDF('P', 'A4', 'fr');
      $html2pdf->pdf->SetDisplayMode('real');

      $html2pdf->writeHTML($content);
      //ob_clean();
      $completName="Meritant_".$libelleclasse;

      $html2pdf->Output('../generated/'.$completName.'.pdf','F');

      return '../generated/'.$completName.'.pdf';

  }

  function generateRecapattendance($codeEtab,$classeid,$session,$moisconcat,$annee)
  {
    require_once('../class_html2PDF/html2pdf.class.php');
    require_once('../class_html2PDF/_tcpdf_5.0.002/config/lang/eng.php');
    require_once('../class_html2PDF/_tcpdf_5.0.002/tcpdf.php');
    require_once('../class/Etablissement.php');
    require_once('../controller/functions.php');

    $etabs=new Etab();
    $classeShool=new Classe();
    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
    $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
    $lienlogo="../logo_etab/".$codeEtab."/".$logoEtab;
    $contacts="";
    $infosEtab=$etabs->getEtabInfosbyCode($codeEtab);
    $tabEtab=explode('*',$infosEtab);

     if($tabEtab[3]!="")
     {
       $contacts=$tabEtab[2]." / ".$tabEtab[3];
     }else {
       $contacts=$tabEtab[2];
     }

     $student=new Student();
     $dataday=$etabs->getAllweeks();
     $libelleclasse=$classeShool->getInfosofclassesbyId($classeid,$session);
     $students=$student->getAllstudentofthisclassesSession($classeid,$session);

     $tabmoisconcat=explode("-",$moisconcat);
     $mois=$tabmoisconcat[0];
     $numbermois=$tabmoisconcat[1];
     $num = cal_days_in_month(CAL_GREGORIAN,$numbermois, $annee);

     $concat=$annee."-".regiveMois($numbermois)."-";



     ob_start();

     ?>
     <page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
       <page_header>
       <table style="width: 100%; border: solid 0px #000; font-size: 12px; font-family: Arial, Helvetica, sans-serif; margin-top:-8px;">
             <tr>
             <td width="25%" height="100" style="width: 30%;"><img src="<?php echo $lienlogo ?>" width="120" height="70" /></td>
             <td style="width: 30%;">
             <table width="70%" height="38" border="0" align="center" cellspacing="0">
               <tr></tr>
             </table>     </td>
             </tr>
             <tr>
             <td valign="top" style="width: 100%; font-size: 12px; font-weight: normal;" rowspan="">

             <p style="border:1px solid black; padding:10px; width:60%;">

               <span class="Style3"><b>EmaiL </b></span> : <?php echo $tabEtab[4];?><span></span><br /><br />
               <span class="Style3"><b>Telephone </b></span> :<?php echo $contacts; ?><span></span><br /><br />

               </p>

             <br/>
             </td>

             </tr>

             </table>
       </page_header>
        <h3><span style='margin-top:100px;margin-left:235px;'>RECAPITULATIF DES PRESENCES DE CLASSE : <?php echo $libelleclasse; ?> </span></h3>
        <h3><span style='margin-top:80px;margin-left:235px;'>PERIODE : <?php echo $mois." ".$annee?> </span></h3>
           <table id='table' border='1'cellpadding='3' cellspacing='0' style='margin-top:100px;width:100%;margin-left:-35px;width:1625px;'>
             <thead>
                 <tr>
                     <!--th style="width:15%;height:10px;padding:3%;margin:15%"-->
                       <th style="padding:20px;">
                         # ELEVES \ JOURS
                     </th>
                     <?php
                       for($x=1;$x<=$num;$x++)
                       {
                       ?>
                       <th style="text-align:center">
                           <?php echo $x; ?>
                       </th>
                     <?php
                       }
                     ?>


                 </tr>
             </thead>
             <tbody>
               <?php
               $i=1;

               foreach ($students as $value):
                ?>
               <tr>
                 <td style=""><?php  echo $value->nom_eleve." ".$value->prenom_eleve?></td>
                 <?php
                   for($x=1;$x<=$num;$x++)
                   {
                   ?>
                   <td style="width:5px;margin:5px;padding:5px;text-align:center">
                       <?php
                         if(strlen($x)==1)
                         {
                           $x="0".$x;
                         }
                        $data=$concat.$x;
                       $matricule=$value->matricule_eleve;
                        $nombre=$student->getNbAttendance($matricule,$data,$classeid);

                         if($nombre==0)
                         {
                           echo $nombre;
                         }else {
                           $number=$student->getstatutAttendance($matricule,$data,$classeid);
                           $array=json_encode($number,true);
                           $someArray = json_decode($array, true);
                           echo $someArray[0]["statut_presence"];
                           //var_dump($number[0]['statut_presence']);
                         }

                         ?>
                   </td>
                 <?php
                   }
                 ?>
              </tr>
              <?php
              $i++;

                  endforeach;
               ?>

             </tbody>
          </table>

     </page>
     <?php
     $content=ob_get_clean();

     $html2pdf = new HTML2PDF('L', 'A4', 'fr');
     $html2pdf->pdf->SetDisplayMode('real');

     $html2pdf->writeHTML($content);
     //ob_clean();
     $completName="Recapattendance_".$libelleclasse;

     $html2pdf->Output('../generated/'.$completName.'.pdf','F');

     return '../generated/'.$completName.'.pdf';
  }

  function generateRoutine($codeEtab,$classeid,$session)
  {
    require_once('../class_html2PDF/html2pdf.class.php');
    require_once('../class_html2PDF/_tcpdf_5.0.002/config/lang/eng.php');
    require_once('../class_html2PDF/_tcpdf_5.0.002/tcpdf.php');
    require_once('../class/Etablissement.php');

    $etabs=new Etab();
    $classeShool=new Classe();
    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
    $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
    $lienlogo="../logo_etab/".$codeEtab."/".$logoEtab;
    $contacts="";
    $infosEtab=$etabs->getEtabInfosbyCode($codeEtab);
    $tabEtab=explode('*',$infosEtab);

     if($tabEtab[3]!="")
     {
       $contacts=$tabEtab[2]." / ".$tabEtab[3];
     }else {
       $contacts=$tabEtab[2];
     }

     $student=new Student();
     $dataday=$etabs->getAllweeks();
     $libelleclasse=$classeShool->getInfosofclassesbyId($classeid,$session);
     ob_start();
     ?>
     <page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
       <page_header>
       <table style="width: 100%; border: solid 0px #000; font-size: 12px; font-family: Arial, Helvetica, sans-serif; margin-top:-8px;">
             <tr>
             <td width="25%" height="100" style="width: 30%;"><img src="<?php echo $lienlogo ?>" width="120" height="70" /></td>
             <td style="width: 30%;">
             <table width="70%" height="38" border="0" align="center" cellspacing="0">
               <tr></tr>
             </table>     </td>
             </tr>
             <tr>
             <td valign="top" style="width: 100%; font-size: 12px; font-weight: normal;" rowspan="">

             <p style="border:1px solid black; padding:10px; width:60%;">

               <span class="Style3"><b>EmaiL </b></span> : <?php echo $tabEtab[4];?><span></span><br /><br />
               <span class="Style3"><b>Telephone </b></span> :<?php echo $contacts; ?><span></span><br /><br />

               </p>

             <br/>
             </td>

             </tr>

             </table>
       </page_header>
       <h3><span style='margin-top:80px;margin-left:235px;'>EMPLOI DU TEMPS CLASSE : <?php echo $libelleclasse; ?> </span></h3>
       <table id='table' border='1'cellpadding='3' cellspacing='0' style='margin-top:100px;width:100%;margin-left:-25px;width:1625px;'>

      <?php
foreach ($dataday as $value):
       ?>
<tr >
  <td style="padding:15px;margin:10px;">
    <?php echo $value->libelle_days;?>
  </td>
  <td style="width:600px;">

    <?php
      $nbroute=$etabs->getNumberofRoutinebyIdroute($value->id_days,$value->short_days,$classeid);
      if($nbroute>0)
      {
          $routines=$etabs->getspecificRoutine($value->id_days,$value->short_days,$classeid);
          $j=1;
          foreach ($routines as $valueRoutine):
            ?>

            <?php echo $valueRoutine->libelle_mat."(".$valueRoutine->debut_route." - ".$valueRoutine->fin_route.")";?>
            <?php
          $j++;
          endforeach;
      }
     ?>
  </td>
</tr>
  <?php
endforeach;
   ?>


      </table>


     </page>
     <?php
     $content=ob_get_clean();

     $html2pdf = new HTML2PDF('L', 'A4', 'fr');
     $html2pdf->pdf->SetDisplayMode('real');

     $html2pdf->writeHTML($content);
     //ob_clean();
     $completName="Routines_".$libelleclasse;

     $html2pdf->Output('../generated/'.$completName.'.pdf','F');

     return '../generated/'.$completName.'.pdf';


  }

  function genereteVersement($codeEtab,$session,$eleveid,$versementid)
  {
    require_once('../class_html2PDF/html2pdf.class.php');
    require_once('../class_html2PDF/_tcpdf_5.0.002/config/lang/eng.php');
    require_once('../class_html2PDF/_tcpdf_5.0.002/tcpdf.php');
    require_once('../class/Etablissement.php');

    $etabs=new Etab();
    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
    $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
    $lienlogo="../logo_etab/".$codeEtab."/".$logoEtab;
    $contacts="";
    $infosEtab=$etabs->getEtabInfosbyCode($codeEtab);
    $tabEtab=explode('*',$infosEtab);

     if($tabEtab[3]!="")
     {
       $contacts=$tabEtab[2]." / ".$tabEtab[3];
     }else {
       $contacts=$tabEtab[2];
     }

     $student=new Student();

     $versementStudent=$student->getStudentversementInfos($codeEtab,$session,$eleveid,$versementid);
    $tabdataversement=explode("*",$versementStudent);
     ob_start();
     ?>
     <page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
       <page_header>
       <table style="width: 100%; border: solid 0px #000; font-size: 12px; font-family: Arial, Helvetica, sans-serif; margin-top:-8px;">
             <tr>
             <td width="25%" height="100" style="width: 30%;"><img src="<?php echo $lienlogo ?>" width="120" height="70" /></td>
             <td style="width: 30%;">
             <table width="70%" height="38" border="0" align="center" cellspacing="0">
               <tr></tr>
             </table>     </td>
             </tr>
             <tr>
             <td valign="top" style="width: 100%; font-size: 12px; font-weight: normal;" rowspan="">

             <p style="border:1px solid black; padding:10px; width:60%;">

               <span class="Style3"><b>EmaiL </b></span> : <?php echo $tabEtab[4];?><span></span><br /><br />
               <span class="Style3"><b>Telephone </b></span> :<?php echo $contacts; ?><span></span><br /><br />

               </p>

             <br/>
             </td>

             </tr>

             </table>
       </page_header>
       <fieldset style=' background-color:white;border-radius:10px;border:1px solid black;width:500px;height:100px; margin-top:200px;margin-left:-15px;'>
   <div>
   <table style='margin-top:2px;width:10%;height:20px;'>
     <tr>
     <td></td>
     <td></td>
     </tr>
   <tr>
   <td>Matricule:</td>
   <td><?php echo $tabdataversement[11];?></td>
   </tr>
   <tr>
   <td>Nom & Prénoms:</td>
   <td><?php echo  $tabdataversement[8]." ".$tabdataversement[9];?></td>
   </tr>
   <tr>
   <td>Classe:</td>
   <td><?php echo $tabdataversement[10];?></td>
   </tr>
   <tr>
   <td></td>
   <td></td>
   </tr>

   </table>
   </div>
   </fieldset>
       <h3><span style='margin-top:80px;margin-left:235px;'>VERSEMENT : <?php echo $tabdataversement[0]; ?></span></h3>

        <table id='table' border='1'cellpadding='3' cellspacing='0' style='margin-top:100px;width:100%;margin-left:-25px;width:1625px;'>
         <thead>
             <tr>
                <th style='width:10%;height:20px;text-align:center;padding:5px;'>Versements</th>

                 <th style='width:10%;height:20px;text-align:center;padding:2px;'> Mode versement </th>
                  <th style='width:15%;height:20px;text-align:center;padding:5px;'> Montant versement</th>
                  <th style='width:10%;height:20px;text-align:center;padding:5px;'> Solde</th>
             </tr>
         </thead>
         <tbody>

           <tr>
             <td style='width:10%;height:20px;padding:5px;margin:5px;'><?php echo  $tabdataversement[0]; ?></td>
              <td><?php
              if($tabdataversement[3]==1)
              {
                 echo "CHEQUES";
              }else {
                echo "ESPECES";
              } ?></td>
              <td><?php echo  $tabdataversement[4]." ".$tabdataversement[6]; ?></td>
              <td><?php echo  $tabdataversement[5]." ".$tabdataversement[6];?></td>
          </tr>

        </tbody>
       </table>

<span style='margin-top:60px;margin-left:5px;'><b>Paiement effectué le <?php echo " ".date_format(date_create($tabdataversement[1]),"d/m/Y");?></b></span>
<span style='margin-top:250px;margin-left:250px;'><b>Signature et Cachet </b></span>
     </page>
     <?php
     $content=ob_get_clean();

     $html2pdf = new HTML2PDF('P', 'A4', 'fr');
     $html2pdf->pdf->SetDisplayMode('real');

     $html2pdf->writeHTML($content);
     //ob_clean();
     $completName=$tabdataversement[0];

     $html2pdf->Output('../generated/'.$completName.'.pdf','F');

     return '../generated/'.$completName.'.pdf';



  }

  function getStudentversementInfos($codeEtab,$session,$eleveid,$versementid)
  {
    $req = $this->db->prepare("SELECT * from versement,compte,eleve,inscription,classe where versement.ideleve_versement=compte.id_compte and compte.id_compte=inscription.ideleve_inscrip and compte.id_compte=eleve.idcompte_eleve and  classe.id_classe=versement.classe_versement and versement.session_versement=inscription.session_inscrip and  versement.ideleve_versement=? and versement.id_versement=? and versement.codeEtab_versement=? and versement.session_versement=? ");
    $req->execute([$eleveid,$versementid,$codeEtab,$session]);
    $data=$req->fetchAll();
    $array=json_encode($data,true);
    $someArray = json_decode($array, true);
    $donnees=$someArray[0]["code_versement"]."*".$someArray[0]["date_versement"]."*".$someArray[0]["date_versement"]."*".$someArray[0]["mode_versement"]."*".$someArray[0]["montant_versement"]."*".$someArray[0]["solde_versement"]."*".$someArray[0]["devise_versement"];
    $donnees.="*".$someArray[0]["user_versement"]."*".$someArray[0]["nom_compte"]."*".$someArray[0]["prenom_compte"]."*".$someArray[0]["libelle_classe"]."*".$someArray[0]["matricule_eleve"];
    return $donnees;
  }

  function generatescolariteclasselpdf($codeEtab,$classe,$section,$session,$libelleclasse)
  {
    require_once('../class_html2PDF/html2pdf.class.php');
    require_once('../class_html2PDF/_tcpdf_5.0.002/config/lang/eng.php');
    require_once('../class_html2PDF/_tcpdf_5.0.002/tcpdf.php');
    require_once('../class/Etablissement.php');

    $etabs=new Etab();
    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
    $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
    $lienlogo="../logo_etab/".$codeEtab."/".$logoEtab;
    $contacts="";
    $infosEtab=$etabs->getEtabInfosbyCode($codeEtab);
    $tabEtab=explode('*',$infosEtab);

     if($tabEtab[3]!="")
     {
       $contacts=$tabEtab[2]." / ".$tabEtab[3];
     }else {
       $contacts=$tabEtab[2];
     }

     $student=new Student();

     $students=$student->getAllstudentofthisclassesSession($classe,$session);
      ob_start();
      ?>
      <page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
        <page_header>
        <table style="width: 100%; border: solid 0px #000; font-size: 12px; font-family: Arial, Helvetica, sans-serif; margin-top:-8px;">
              <tr>
              <td width="25%" height="100" style="width: 30%;"><img src="<?php echo $lienlogo ?>" width="120" height="70" /></td>
              <td style="width: 30%;">
              <table width="70%" height="38" border="0" align="center" cellspacing="0">
                <tr></tr>
              </table>     </td>
              </tr>
              <tr>
              <td valign="top" style="width: 100%; font-size: 12px; font-weight: normal;" rowspan="">

              <p style="border:1px solid black; padding:10px; width:60%;">

                <span class="Style3"><b>EmaiL </b></span> : <?php echo $tabEtab[4];?><span></span><br /><br />
                <span class="Style3"><b>Telephone </b></span> :<?php echo $contacts; ?><span></span><br /><br />

                </p>

              <br/>
              </td>

              </tr>

              </table>
        </page_header>
         <h3><span style='margin-top:140px;margin-left:240px;'>ETAT SCOLARITES</span></h3>
          <table id='table' border='1'cellpadding='3' cellspacing='0' style='margin-top:150px;width:100%;margin-left:-25px;width:1625px;'>
            <thead>
                <tr>
                   <th style='width:10%;height:20px;text-align:center;'>Matricule</th>
                    <th style='width:15%;height:20px;text-align:center;'>Nom & Prénoms</th>
                    <th style='width:10%;height:20px;text-align:center;'> Montant versé </th>
                     <th style='width:10%;height:20px;text-align:center;'> Reste à payer</th>
                </tr>
            </thead>
            <tbody>
              <?php
              $sommemontantvers=0;
              $sommeresteapayer=0;
              $devise="";
              foreach ($students as $value):
               ?>
              <tr>
                <td style='width:10%;height:20px;'><?php echo $value->matricule_eleve;?></td>
                 <td ><?php echo $value->nom_eleve." ".$value->prenom_eleve;?></td>
                 <td ><?php
                 //nous allons compter le nombre de versement

                   $versementNb=$student->getNumberOfversementStudent($value->id_compte,$classe,$codeEtab,$session);
                   //echo $versementNb;
                   if($versementNb==0)
                   {
                     ?>
                      Aucun versement
                     <?php
                   }else if($versementNb>0)
                   {
                     $datavers=$student->getSommeversement($value->id_compte,$classe,$codeEtab,$session,$section);
                     $tabdata=explode("*",$datavers);
                     $sommemontantvers=$sommemontantvers+$tabdata[0];
                     $devise=$tabdata[1];

                     ?>
                     <?php echo $tabdata[0]." ".$tabdata[1]; ?>
                     <?php
                   }
                 //echo $student->getSommeversement($value->id_compte,$classesearch,$codeEtabsearch,$libellesessionsearch,$section);

                  ?></td>
                 <td >
                   <?php
                   if($versementNb==0)
                   {
                     //nous allons afficher le montant de scolarité de la section

                     $sectiondetails=$student->getScolaritefraisSection($codeEtab,$session,$section);
                     $datasectiondetails=explode("*",$sectiondetails);
                     $sommeresteapayer=$sommeresteapayer+$datasectiondetails[0];

                     ?>
                       <?php echo $datasectiondetails[0]." ".$datasectiondetails[1]; ?>
                     <?php

                   }else if($versementNb>0)
                   {
                     //retrouver le dernier Enregistrement du versement

                     $datavers=$student->SelectInformationsOfLastVersement($codeEtab,$classe,$session,$value->id_compte);
                     $tabdatavers=explode("*",$datavers);
                     $sommeresteapayer=$sommeresteapayer+$tabdatavers[0];
                     $content=$tabdatavers[0]." ".$tabdatavers[1];
                     ?>
                        <?php echo $content; ?>
                     <?php
                   }
                    ?>
                 </td>
             </tr>
             <?php
           endforeach;
              ?>
           </tbody>
           <tfoot>
             <tr style='width:20%;background-color:#DCDCDC'>
                 <th colspan="2" ><span style="">Total</span></th>

                 <th ><?php echo number_format($sommemontantvers, 0, ',', ' ')." ".$devise; ?></th>
                 <th ><?php echo number_format($sommeresteapayer, 0, ',', ' ')." ".$devise; ?></th>

             </tr>
         </tfoot>
         </table>

      </page>
      <?php
      $content=ob_get_clean();

      $html2pdf = new HTML2PDF('P', 'A4', 'fr');
      $html2pdf->pdf->SetDisplayMode('real');

      $html2pdf->writeHTML($content);
      //ob_clean();
      $completName="Etat_scolarites-".$libelleclasse;

      $html2pdf->Output('../generated/'.$completName.'.pdf','F');

      return '../generated/'.$completName.'.pdf';


  }

  function getScolaritefraisSection($codeEtabsearch,$libellesessionsearch,$section)
  {
    $req = $this->db->prepare("SELECT montant_section,devises_section from section where codeEtab_section=? and libellesession_section=? and 	id_section=? ");
    $req->execute([$codeEtabsearch,$libellesessionsearch,$section]);
    $data=$req->fetchAll();
    $array=json_encode($data,true);
    $someArray = json_decode($array, true);
    return $someArray[0]["montant_section"]."*".$someArray[0]["devises_section"];
  }

  function getNumberOfversementStudent($ideleve,$classesearch,$codeEtabsearch,$libellesessionsearch)
  {
      $req = $this->db->prepare("SELECT * from versement where ideleve_versement=? and classe_versement=? and codeEtab_versement=? and session_versement=? ");
      $req->execute([$ideleve,$classesearch,$codeEtabsearch,$libellesessionsearch]);
      $data=$req->fetchAll();
      $nb=count($data);
      return $nb;
  }

  function getSommeversement($ideleve,$classesearch,$codeEtabsearch,$libellesessionsearch,$section)
  {
      $req = $this->db->prepare("SELECT SUM(montant_versement) as somme,devise_versement FROM versement where ideleve_versement=? and classe_versement=? and codeEtab_versement=? and session_versement=?");
      $req->execute([$ideleve,$classesearch,$codeEtabsearch,$libellesessionsearch]);
      $data=$req->fetchAll();
      $array=json_encode($data,true);
      $someArray = json_decode($array, true);
      return $someArray[0]["somme"]."*".$someArray[0]["devise_versement"];
      //return $req->fetchAll();
  }

  function getNombreAbsentdayClasse($classe,$session,$datedeb,$datefin,$codeEtab)
  {
    $req = $this->db->prepare("SELECT * from presences where   classe_presence=? and codeEtab_presence=? and statut_presence=0 and date_presence >= ? and date_presence<= ?  ");
    $req->execute([$classe,$codeEtab,$datedeb,$datefin]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;
  }

  function getNombrePresentdeyClasse($classe,$session,$datedeb,$datefin,$codeEtab)
  {
    $req = $this->db->prepare("SELECT * from presences where   classe_presence=? and codeEtab_presence=? and statut_presence=1 and date_presence >= ? and date_presence<= ?  ");
    $req->execute([$classe,$codeEtab,$datedeb,$datefin]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;
  }
  function getNombreAbsentdayStudent($matriculestudent,$classe,$session,$datedeb,$datefin,$codeEtab)
  {
    $req = $this->db->prepare("SELECT * from presences where  matricule_presence=? and classe_presence=? and codeEtab_presence=? and statut_presence=0 and date_presence >= ? and date_presence<= ?  ");
    $req->execute([$matriculestudent,$classe,$codeEtab,$datedeb,$datefin]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;
  }

  function getNombrePresentdayStudent($matriculestudent,$classe,$session,$datedeb,$datefin,$codeEtab)
  {
    $req = $this->db->prepare("SELECT * from presences where  matricule_presence=? and classe_presence=? and codeEtab_presence=? and statut_presence=1 and date_presence >= ? and date_presence<= ?  ");
    $req->execute([$matriculestudent,$classe,$codeEtab,$datedeb,$datefin]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;
  }


  function getNBAbsenceOfThisStudentTimely($studentmatri,$classeEtab,$sessionEtab,$datedeb,$datefin,$codeEtab)
  {
    $req = $this->db->prepare("SELECT distinct date_presence from presences where  matricule_presence=? and classe_presence=? and codeEtab_presence=? and statut_presence=0 and date_presence >= ? and date_presence<= ?  ");
          $req->execute([
            //$studentmatri,$classeEtab,$codeEtab,$datefin,$datefin
            $studentmatri,$classeEtab,$codeEtab,$datedeb,$datefin

          ]);

          return $req->fetchAll();
  }

  function getNBOfAttendanceTimely($studentmatri,$classeEtab,$sessionEtab,$datedeb,$datefin,$codeEtab)
  {
    $req = $this->db->prepare("SELECT distinct date_presence from presences where  matricule_presence=? and classe_presence=? and codeEtab_presence=? and date_presence >= ? and date_presence<= ?  ");
          $req->execute([
            //$studentmatri,$classeEtab,$codeEtab,$datefin,$datefin
            $studentmatri,$classeEtab,$codeEtab,$datedeb,$datefin

          ]);

          return $req->fetchAll();
  }

  function DeterminationAbsentOfNbpresenceClasse($classeEtab,$sessionEtab,$datedeb,$datefin,$codeEtab)
  {
    $req = $this->db->prepare("SELECT distinct date_presence from presences where  classe_presence=? and codeEtab_presence=? and statut_presence=0 and date_presence >= ? and date_presence<= ?  ");
          $req->execute([
            //$studentmatri,$classeEtab,$codeEtab,$datefin,$datefin
            $classeEtab,$codeEtab,$datedeb,$datefin

          ]);

          $data=$req->fetchAll();
          $nb=count($data);
          return $nb;
  }

  function DeterminationOfNbpresenceClasse($classeEtab,$sessionEtab,$datedeb,$datefin,$codeEtab)
  {
    $req = $this->db->prepare("SELECT distinct date_presence from presences where   classe_presence=? and codeEtab_presence=?  and date_presence >= ? and date_presence<= ?  ");
          $req->execute([
            //$studentmatri,$classeEtab,$codeEtab,$datefin,$datefin
            $classeEtab,$codeEtab,$datedeb,$datefin

          ]);

          $data=$req->fetchAll();
          $nb=count($data);
          return $nb;
  }

  function DetermineAbsenceOfThisStudentTimely($studentmatri,$classeEtab,$sessionEtab,$datedeb,$datefin,$codeEtab)
  {
    $req = $this->db->prepare("SELECT distinct date_presence from presences where  matricule_presence=? and classe_presence=? and codeEtab_presence=? and statut_presence=0 and date_presence >= ? and date_presence<= ?  ");
          $req->execute([
            //$studentmatri,$classeEtab,$codeEtab,$datefin,$datefin
            $studentmatri,$classeEtab,$codeEtab,$datedeb,$datefin

          ]);

          $data=$req->fetchAll();
          $nb=count($data);
          return $nb;
  }

  function DetermineNumberOfAttendanceTimely($studentmatri,$classeEtab,$sessionEtab,$datedeb,$datefin,$codeEtab)
  {
        //$req = $this->db->prepare("SELECT distinct date_presence from presences where  matricule_presence=? and classe_presence=? and codeEtab_presence=? and (date_presence >= ? and date_presence<= ? ) ");
  $req = $this->db->prepare("SELECT distinct date_presence from presences where  matricule_presence=? and classe_presence=? and codeEtab_presence=? and date_presence >= ? and date_presence<= ?  ");
        $req->execute([
          //$studentmatri,$classeEtab,$codeEtab,$datefin,$datefin
          $studentmatri,$classeEtab,$codeEtab,$datedeb,$datefin

        ]);

        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
  }

  function SolderInscription($statesolde,$studentid,$codeEtab,$classe,$libellesession)
  {
    $req = $this->db->prepare("UPDATE inscription SET solder_inscrip=? where ideleve_inscrip=? and codeEtab_inscrip=? and idclasse_inscrip=? and session_inscrip=?");
    $req->execute([
      $statesolde,$studentid,$codeEtab,$classe,$libellesession

    ]);
  }

  function SendVersementsMaillerToParent($newcodeVers,$emailparent,$nomEleve,$classeName,$EtabName,$imageEtab,$montvers,$montrest,$modepaie,$datecrea,$devisepaie,$codeEtab)
  {

    require_once('../PHPMailer/class.phpmailer.php');
    require_once('../PHPMailer/class.smtp.php');
    require_once('../controller/functions.php');

    if($modepaie==1)
    {
    $paiementmode="Chèque";
    }else {
      $paiementmode="Espèce";
    }

    $client1="justearmel04@gmail.com";
    $client2="fabienekoute@gmail.com";
    $mail = new PHPMailer();
    $mail->isHTML(true);
     $mail->CharSet="UTF-8";
     $mail->isSMTP();
     $mail->SMTPOptions = array (
     'ssl' => array(
     'verify_peer'  => false,
     'verify_peer_name'  => false,
     'allow_self_signed' => true));
     $mail->Host='mail.proximity-cm.com';
     $mail->SMTPAuth = true;
     $mail->Port = 25;
     $mail->SMTPSecure = "tls";
     $mail->Username = "xschool@proximity-cm.com";
     $mail->Password ="123psa@456";
     $mail->From='xschool@proximity-cm.com';
     $mail->FromName=$EtabName;
     $mail->AddAddress($emailparent);
     $mail->AddAddress($client1);
     $mail->AddAddress($client2);
     $mail->Subject = 'Versement Scolarité';
     $mail->Body = "Bonjour Chers Parent<br>";
     $mail->Body .="La direction de l'établissement ".$EtabName." tiens à vous informer qu'un versement en ". $paiementmode ."  à lieu pour le paiement des frais de scolarité de vôtre enfant<br>";
     $mail->Body .="en la personne de ".$nomEleve." regulièrement inscrit en classe de  ".$classeName .".";
     $mail->Body .=" Le montant du versement est de  ".$montvers." ".$devisepaie. " avec un solde à regler de  ".$montrest." ".$devisepaie. "<br>";
     $mail->Body .="Cordialement<br>";
     $mail->Body .="<br>";
     $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
     $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$imageEtab,"mon_logo");

    if(!$mail->Send())

    {

       //echo $mail->ErrorInfo;

       //Affiche le message d'erreur (ATTENTION:voir section 7)

    $msg="nok";

    }

    else

    {

    $msg="ok";

    }

    return $msg;
  }

  function getNameInfos($studentid)
  {
    $req = $this->db->prepare("SELECT * from eleve where eleve.idcompte_eleve=?");
    $req->execute([$studentid]);

    $data=$req->fetchAll();
    $array=json_encode($data,true);
    $someArray = json_decode($array, true);
    $donnees=$someArray[0]["nom_eleve"]." ".$someArray[0]["prenom_eleve"];

    return $donnees;
  }

  function getEmailParentOfThisStudentByID($studentid,$libellesession)
  {
    $req = $this->db->prepare("SELECT * from eleve,parent,parenter,inscription,classe,etablissement where classe.codeEtab_classe=etablissement.code_etab and classe.codeEtab_classe=etablissement.code_etab and  inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=eleve.idcompte_eleve and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and eleve.idcompte_eleve=? and inscription.session_inscrip=?");

    $req->execute([$studentid,$libellesession]);

    $data=$req->fetchAll();

    $array=json_encode($data,true);

    $someArray = json_decode($array, true);

    $donnees=$someArray[0]["email_parent"]."*".$someArray[0]["nom_eleve"]."*".$someArray[0]["prenom_eleve"]."*".$someArray[0]["libelle_classe"]."*".$someArray[0]["libelle_etab"]."*".$someArray[0]["logo_etab"]."*".$someArray[0]["idparent_eleve"];

    return $donnees;

  }

  function UpdateCodeversement($newcodeVers,$idversement,$libellesession,$codeEtab,$classe,$studentid)
  {
    $req = $this->db->prepare("UPDATE versement SET code_versement=? where id_versement=? and session_versement=? and codeEtab_versement=? and classe_versement=? and ideleve_versement=?");
    $req->execute([
      $newcodeVers,$idversement,$libellesession,$codeEtab,$classe,$studentid

    ]);
  }

  function AddVersement($codeVers,$datecrea,$modepaie,$montvers,$montrest,$classe,$studentid,$libellesession,$codeEtab,$devisepaie,$iduser)
  {
    $reqX = $this->db->prepare("INSERT INTO  versement SET code_versement=?,date_versement=?,mode_versement=?,montant_versement=?,solde_versement=?,classe_versement=?,ideleve_versement=?,session_versement=?,codeEtab_versement=?,devise_versement=?,user_versement=?");

    $reqX->execute([

  $codeVers,$datecrea,$modepaie,$montvers,$montrest,$classe,$studentid,$libellesession,$codeEtab,$devisepaie,$iduser
    ]);


    $idlastcompte=$this->db->lastInsertId();

    return $idlastcompte;

  }

  function getNotesAndObservControleSpec($idcompte_eleve,$notetype,$classeEtab,$libellesessionencours,$codeEtab)
  {
    $req = $this->db->prepare("SELECT * from eleve,enseignant,classe,matiere,notes,controle where
      notes.ideleve_notes=eleve.idcompte_eleve and notes.idprof_notes=enseignant.idcompte_enseignant and
       notes.idclasse_notes=classe.id_classe and notes.idmat_notes=matiere.id_mat and notes.idtype_notes=controle.id_ctrl and notes.ideleve_notes=? and notes.type_notes=?
       and notes.idclasse_notes=? and notes.session_notes=? and notes.codeEtab_notes=?");

    $req->execute([$idcompte_eleve,$notetype,$classeEtab,$libellesessionencours,$codeEtab]);
    $data=$req->fetchAll();

    $array=json_encode($data,true);

    $someArray = json_decode($array, true);

    $donnees=$someArray[0]["valeur_notes"]."*".$someArray[0]["obser_notes"];

    return $donnees;
  }

  function getNotesAndObservExamSpec($idcompte_eleve,$notetype,$classeEtab,$libellesessionencours,$codeEtab)
  {
    $req = $this->db->prepare("SELECT * from eleve,enseignant,classe,matiere,notes,examen where notes.ideleve_notes=eleve.idcompte_eleve and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and examen.id_exam=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and eleve.idcompte_eleve=? and notes.type_notes=? and  classe.id_classe=? and examen.session_exam  and classe.codeEtab_classe=? ");
    $req->execute([$idcompte_eleve,$notetype,$classeEtab,$libellesessionencours,$codeEtab]);
    $data=$req->fetchAll();

    //return $data;

    $array=json_encode($data,true);

    $someArray = json_decode($array, true);

    $donnees=$someArray[0]["valeur_notes"]."*".$someArray[0]["obser_notes"];

    return $donnees;
  }

  function AddstandbyUpdatenotes($idpiste,$controlepiste,$examenpiste,$idtypenote,$matiereid,$teatcherid,$classeId,$validationstatus,$ideleve,$notes,$observation)
  {
    $req = $this->db->prepare("INSERT INTO  notesmodification SET idpiste_modifnote=?,controle_modifnote=?,examen_modifnote=?,designationid_modifnote=?,matiereid_modifnote=?,teatcherid_modifnote=?,classeid_modifnote=?,statut_modifnote=?,eleveid_modifnote=?,notes_modifnote=?,observ_modifnote=?");

   $req->execute([

     $idpiste,$controlepiste,$examenpiste,$idtypenote,$matiereid,$teatcherid,$classeId,$validationstatus,$ideleve,$notes,$observation

   ]);
  }

  function getNotesAndObservControleNbSpec($idcompte_eleve,$notetype,$classeEtab,$libellesessionencours,$codeEtab)
  {
    $req = $this->db->prepare("SELECT * from eleve,enseignant,classe,matiere,notes,controle where
      notes.ideleve_notes=eleve.idcompte_eleve and notes.idprof_notes=enseignant.idcompte_enseignant and
       notes.idclasse_notes=classe.id_classe and notes.idmat_notes=matiere.id_mat and notes.idtype_notes=controle.id_ctrl and notes.ideleve_notes=? and notes.type_notes=?
       and notes.idclasse_notes=?  and notes.session_notes=? and notes.codeEtab_notes=?");

    $req->execute([$idcompte_eleve,$notetype,$classeEtab,$libellesessionencours,$codeEtab]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;
  }

  function getNotesAndObservExamNbSpec($idcompte_eleve,$notetype,$classeEtab,$libellesessionencours,$codeEtab)
  {
    $req = $this->db->prepare("SELECT * from eleve,enseignant,classe,matiere,notes,examen where notes.ideleve_notes=eleve.idcompte_eleve and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and examen.id_exam=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and eleve.idcompte_eleve=? and notes.type_notes=? and  classe.id_classe=? and examen.session_exam  and classe.codeEtab_classe=? ");
    $req->execute([$idcompte_eleve,$notetype,$classeEtab,$libellesessionencours,$codeEtab]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;
  }

  function DetermineInscriptionSolder($classe,$codeEtab,$studentid,$libellesession)
  {
      $req = $this->db->prepare("SELECT * from inscription where idclasse_inscrip=? and codeEtab_inscrip=? and ideleve_inscrip=? and session_inscrip=? order by id_inscrip DESC limit 1");
      $req->execute([$classe,$codeEtab,$studentid,$libellesession]);
      $data=$req->fetchAll();
      $array=json_encode($data,true);

      $someArray = json_decode($array, true);

      $donnees=$someArray[0]["solder_inscrip"];

      return $donnees;
  }

  function SelectInformationsOfLastVersement($codeEtab,$classe,$session,$studentid)
  {
      $req = $this->db->prepare("SELECT * from versement where codeEtab_versement=? and classe_versement=? and session_versement=? and ideleve_versement=? order by id_versement DESC limit 1");
      $req->execute([$codeEtab,$classe,$session,$studentid]);
      $data=$req->fetchAll();
      $array=json_encode($data,true);

      $someArray = json_decode($array, true);

      $donnees=$someArray[0]["solde_versement"]."*".$someArray[0]["devise_versement"];

      return $donnees;
  }

  function DetermineNumberOfversement($codeEtab,$classe,$session,$studentid)
  {
        $req = $this->db->prepare("SELECT * from versement where codeEtab_versement=? and classe_versement=? and session_versement=? and ideleve_versement=?");
        $req->execute([$codeEtab,$classe,$session,$studentid]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
  }


function getNotesAndObservControleNb($eleveid,$notetype,$classeEtab,$controleid,$matiereid,$teatcherid,$libellesessionencours,$codeEtab)
{
  $req = $this->db->prepare("SELECT * from eleve,enseignant,classe,matiere,notes,controle where
    notes.ideleve_notes=eleve.idcompte_eleve and notes.idprof_notes=enseignant.idcompte_enseignant and
     notes.idclasse_notes=classe.id_classe and notes.idmat_notes=matiere.id_mat and notes.idtype_notes=controle.id_ctrl and notes.ideleve_notes=? and notes.type_notes=?
     and notes.idclasse_notes=? and notes.idtype_notes=? and notes.idmat_notes=? and notes.idprof_notes=? and notes.session_notes=? and notes.codeEtab_notes=?");

  $req->execute([$eleveid,$notetype,$classeEtab,$controleid,$matiereid,$teatcherid,$libellesessionencours,$codeEtab]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;

}

function getNotesAndObservControle($eleveid,$notetype,$classeEtab,$controleid,$matiereid,$teatcherid,$libellesessionencours,$codeEtab)
{
  $req = $this->db->prepare("SELECT * from eleve,enseignant,classe,matiere,notes,controle where
    notes.ideleve_notes=eleve.idcompte_eleve and notes.idprof_notes=enseignant.idcompte_enseignant and
     notes.idclasse_notes=classe.id_classe and notes.idmat_notes=matiere.id_mat and notes.idtype_notes=controle.id_ctrl and notes.ideleve_notes=? and notes.type_notes=?
     and notes.idclasse_notes=? and notes.idtype_notes=? and notes.idmat_notes=? and notes.idprof_notes=? and notes.session_notes=? and notes.codeEtab_notes=?");

  $req->execute([$eleveid,$notetype,$classeEtab,$controleid,$matiereid,$teatcherid,$libellesessionencours,$codeEtab]);
  $data=$req->fetchAll();

  $array=json_encode($data,true);

  $someArray = json_decode($array, true);

  $donnees=$someArray[0]["valeur_notes"]."*".$someArray[0]["obser_notes"];

  return $donnees;

  // return $data;

}


function getNotesAndObservExamNb($eleveid,$notetype,$classeEtab,$examid,$matiereid,$teatcherid,$libellesessionencours,$codeEtab)
{
  $req = $this->db->prepare("SELECT * from eleve,enseignant,classe,matiere,notes,examen where notes.ideleve_notes=eleve.idcompte_eleve and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and examen.id_exam=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and eleve.idcompte_eleve=? and notes.type_notes=? and  classe.id_classe=? and examen.id_exam=? and matiere.id_mat=? and enseignant.idcompte_enseignant=? and examen.session_exam=?  and classe.codeEtab_classe=? ");
  $req->execute([$eleveid,$notetype,$classeEtab,$examid,$matiereid,$teatcherid,$libellesessionencours,$codeEtab]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getNotesAndObservExam($eleveid,$notetype,$classeEtab,$examid,$matiereid,$teatcherid,$libellesessionencours,$codeEtab)
{
  $req = $this->db->prepare("SELECT * from eleve,enseignant,classe,matiere,notes,examen where notes.ideleve_notes=eleve.idcompte_eleve and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and examen.id_exam=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and eleve.idcompte_eleve=? and notes.type_notes=? and  classe.id_classe=? and examen.id_exam=? and matiere.id_mat=? and enseignant.idcompte_enseignant=? and examen.session_exam  and classe.codeEtab_classe=? ");
  $req->execute([$eleveid,$notetype,$classeEtab,$examid,$matiereid,$teatcherid,$libellesessionencours,$codeEtab]);
  $data=$req->fetchAll();

  //return $data;

  $array=json_encode($data,true);

  $someArray = json_decode($array, true);

  $donnees=$someArray[0]["valeur_notes"]."*".$someArray[0]["obser_notes"];

  return $donnees;
}



  function DeleteStudent($compte,$statut,$classe,$codeEtab)
  {
    $req = $this->db->prepare("UPDATE compte SET statut_compte=? where id_compte=? ");
    $req->execute([
      $statut,
      $compte

    ]);

    $_SESSION['user']['updateteaok']=L::fichedesactiver;

    if($_SESSION['user']['profile'] == "Admin_globale")

    {

  header("Location:../manager/showInfosclasse.php?classe=".$classe."&codeEtab=".$codeEtab);

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {

  header("Location:../locale/classinfos.php?classe=".$classe."&codeEtab=".$codeEtab);

    }

  }

  function NBofcontrolestandbyvalidation($controle,$matiere,$teatcher,$classe,$session,$codeEtab)
  {
    $req = $this->db->prepare("SELECT * FROM notesmodification where designationid_modifnote=? and matiereid_modifnote=? and teatcherid_modifnote=? and classeid_modifnote=? and statut_modifnote=0 and controle_modifnote=1 ");
    $req->execute([$controle,$matiere,$teatcher,$classe]);
    $data=$req->fetchAll();
    $nb=count($data);

    return $nb;
  }

  function NBofcontroleStudentstandbyvalidation($controle,$matiere,$teatcher,$classe,$session,$codeEtab,$matriculedelete)
  {
    $req = $this->db->prepare("SELECT * FROM notesmodification where designationid_modifnote=? and matiereid_modifnote=? and teatcherid_modifnote=? and classeid_modifnote=? and statut_modifnote=0 and controle_modifnote=1 and eleveid_modifnote=? ");
    $req->execute([$controle,$matiere,$teatcher,$classe,$matriculedelete]);
    $data=$req->fetchAll();
    $nb=count($data);

    return $nb;
  }

  function NBofexamenstandbyvalidation($examen,$matiere,$teatcher,$classe,$session,$codeEtab)
  {
    $req = $this->db->prepare("SELECT * FROM notesmodification where designationid_modifnote=? and matiereid_modifnote=? and teatcherid_modifnote=? and classeid_modifnote=? and statut_modifnote=0 and examen_modifnote=1 ");
    $req->execute([$examen,$matiere,$teatcher,$classe]);
    $data=$req->fetchAll();
    $nb=count($data);

    return $nb;
  }

  function NBofexamenStudentstandbyvalidation($examen,$matiere,$teatcher,$classe,$session,$codeEtab,$matriculedelete)
  {
    $req = $this->db->prepare("SELECT * FROM notesmodification where designationid_modifnote=? and matiereid_modifnote=? and teatcherid_modifnote=? and classeid_modifnote=? and statut_modifnote=0 and examen_modifnote=1 and eleveid_modifnote=? ");
    $req->execute([$controle,$matiere,$teatcher,$classe,$matriculedelete]);
    $data=$req->fetchAll();
    $nb=count($data);

    return $nb;
  }

  function UpdateStudentInfoWithPassword($nomad,$prenomad,$datenaisad,$lieunais,$sexe,$contactad,$emailad,$loginad,$codeEtab,$idcompte,$passad)
  {
    $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,login_compte=?,pass_compte=? where id_compte=? ");

    $req->execute([
      $nomad,
      $prenomad,
      $datenaisad,
      $contactad,
      $emailad,
      $loginad,
      $passad,
      $idcompte
    ]);

    //mise a jour dans la table Eleve

    $req1 = $this->db->prepare("UPDATE eleve SET nom_eleve=?,prenom_eleve=?,datenais_eleve=?,lieunais_eleve=?,sexe_eleve=?,email_eleve=?,login_eleve=?  where codeEtab_eleve=? and idcompte_eleve=?");
    $req1->execute([
    $nomad,
    $prenomad,
    $datenaisad,
    $lieunais,
    $sexe,
    $emailad,
    $loginad,
    $codeEtab,
    $idcompte
    ]);
      $_SESSION['user']['updateroutineok']="Modification effectuée avec succès";

      if($_SESSION['user']['profile'] == "Admin_globale")

      {

    header("Location:../manager/updatestudent.php?compte=".$idcompte);

      }else if($_SESSION['user']['profile'] == "Admin_locale")

      {

    header("Location:../locale/updatestudent.php?compte=".$idcompte);

      }
  }

  function UpdateStudentwithoutFile($nomad,$prenomad,$datenaisad,$lieunais,$sexe,$contactad,$codeEtab,$idcompte,$allergie,$condphy)
  {
      //mis a jour dans la table $compte

      $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=? where id_compte=? ");

      $req->execute([
        $nomad,
        $prenomad,
        $datenaisad,
        $contactad,
        $idcompte
      ]);

      //mise a jour dans la table Eleve

      $req1 = $this->db->prepare("UPDATE eleve SET nom_eleve=?,prenom_eleve=?,datenais_eleve=?,lieunais_eleve=?,sexe_eleve=?,allergie_eleve=?,condphy_eleve=?  where codeEtab_eleve=? and idcompte_eleve=?");
      $req1->execute([
      $nomad,
      $prenomad,
      $datenaisad,
      $lieunais,
      $sexe,
      $allergie,
      $condphy,
      $codeEtab,
      $idcompte
      ]);
        $_SESSION['user']['updateroutineok']="Modification effectuée avec succès";

        if($_SESSION['user']['profile'] == "Admin_globale")

        {

      header("Location:../manager/updatestudent.php?compte=".$idcompte."&codeEtab=".$codeEtab);

        }else if($_SESSION['user']['profile'] == "Admin_locale")

        {

      header("Location:../locale/updatestudent.php?compte=".$idcompte."&codeEtab=".$codeEtab);

        }

  }

  function UpdateStudentInfoWithoutFile($nomad,$prenomad,$datenaisad,$lieunais,$sexe,$contactad,$codeEtab,$idcompte)
  {
      //mis a jour dans la table $compte

      $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=? where id_compte=? ");

      $req->execute([
        $nomad,
        $prenomad,
        $datenaisad,
        $contactad,
        $idcompte
      ]);

      //mise a jour dans la table Eleve

      $req1 = $this->db->prepare("UPDATE eleve SET nom_eleve=?,prenom_eleve=?,datenais_eleve=?,lieunais_eleve=?,sexe_eleve=?  where codeEtab_eleve=? and idcompte_eleve=?");
      $req1->execute([
      $nomad,
      $prenomad,
      $datenaisad,
      $lieunais,
      $sexe,
      $codeEtab,
      $idcompte
      ]);
        $_SESSION['user']['updateroutineok']="Modification effectuée avec succès";

        if($_SESSION['user']['profile'] == "Admin_globale")

        {

      header("Location:../manager/updatestudent.php?compte=".$idcompte."&codeEtab=".$codeEtab);

        }else if($_SESSION['user']['profile'] == "Admin_locale")

        {

      header("Location:../locale/updatestudent.php?compte=".$idcompte."&codeEtab=".$codeEtab);

        }

  }

  function UpdateStudentInfoWithoutPassword($nomad,$prenomad,$datenaisad,$lieunais,$sexe,$contactad,$emailad,$loginad,$codeEtab,$idcompte)
  {
      //mis a jour dans la table $compte

      $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,login_compte=? where id_compte=? ");

      $req->execute([
        $nomad,
        $prenomad,
        $datenaisad,
        $contactad,
        $emailad,
        $loginad,
        $idcompte
      ]);

      //mise a jour dans la table Eleve

      $req1 = $this->db->prepare("UPDATE eleve SET nom_eleve=?,prenom_eleve=?,datenais_eleve=?,lieunais_eleve=?,sexe_eleve=?,email_eleve=?,login_eleve=?  where codeEtab_eleve=? and idcompte_eleve=?");
      $req1->execute([
      $nomad,
      $prenomad,
      $datenaisad,
      $lieunais,
      $sexe,
      $emailad,
      $loginad,
      $codeEtab,
      $idcompte
      ]);
        $_SESSION['user']['updateroutineok']="Modification effectuée avec succès";

        if($_SESSION['user']['profile'] == "Admin_globale")

        {

      header("Location:../manager/updatestudent.php?compte=".$idcompte."&codeEtab=".$codeEtab);

        }else if($_SESSION['user']['profile'] == "Admin_locale")

        {

      header("Location:../locale/updatestudent.php?compte=".$idcompte."&codeEtab=".$codeEtab);

        }

  }

function generateficheLocalpdf($compte,$codeEtab)
{

}


  function AbsenceParentMailler($emailparent,$EtabName,$nomEleve,$classeName,$libellematiere,$debutHours,$finHours,$codeEtab,$imageEtab)

  {

    // session_start();

    require_once('../PHPMailer/class.phpmailer.php');

    require_once('../PHPMailer/class.smtp.php');

    require_once('../controller/functions.php');

    // $logoetab="../logo_etab/".$codeEtab."/".$imageEtab;

    $day=date("d-m-Y");

$client1="justearmel04@gmail.com";
$client2="fabienekoute@gmail.com";
    $mail = new PHPMailer();

$mail->isHTML(true);

 $mail->CharSet="UTF-8";

 $mail->isSMTP();

 $mail->SMTPOptions = array (

        'ssl' => array(

            'verify_peer'  => false,

            'verify_peer_name'  => false,

            'allow_self_signed' => true));

 $mail->Host='mail.proximity-cm.com';

 $mail->SMTPAuth = true;

 $mail->Port = 25;

 $mail->SMTPSecure = "tls";

 $mail->Username = "xschool@proximity-cm.com";

 $mail->Password ="123psa@456";

 $mail->From='xschool@proximity-cm.com';


  $mail->FromName=$EtabName;

 $mail->AddAddress($emailparent);

 $mail->AddAddress($client1);
$mail->AddAddress($client2);

 $mail->Subject = 'Abscence au cours';

 $mail->Body = "Hello Chers Parent<br>";

 $mail->Body .="La direction de l'établissement ".$EtabName." tiens à vous informer de l'absence ";

 $mail->Body .="de votre enfant en la personne de ".$nomEleve." au cours de ".$libellematiere." initialement prévu à ".$debutHours;

 $mail->Body .=" pour prendre fin à ".$finHours." ce jour ".$day."<br>";

 $mail->Body .="Cordialement<br>";

 $mail->Body .="<br>";

$mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";

$mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$imageEtab,"mon_logo");

if(!$mail->Send())

{

   //echo $mail->ErrorInfo;

   //Affiche le message d'erreur (ATTENTION:voir section 7)

$msg="nok";

}

else

{

$msg="ok";

}

return $msg;
//return $mail->Body;

// $mail->AddEmbeddedImage('../logo_etab/assets/images/adwowilogo1.png','mon_logo');

  }



  function getEmailParentOfThisStudent($matricule)

  {

      $req = $this->db->prepare("SELECT * from eleve,parent,parenter,inscription,classe,etablissement where classe.codeEtab_classe=etablissement.code_etab and classe.codeEtab_classe=etablissement.code_etab and  inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=eleve.idcompte_eleve and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and eleve.matricule_eleve=?");

      $req->execute([$matricule]);

      $data=$req->fetchAll();

      $array=json_encode($data,true);

      $someArray = json_decode($array, true);

      $donnees=$someArray[0]["email_parent"]."*".$someArray[0]["nom_eleve"]."*".$someArray[0]["prenom_eleve"]."*".$someArray[0]["libelle_classe"]."*".$someArray[0]["libelle_etab"]."*".$someArray[0]["logo_etab"];

      return $donnees;

  }



  function getNumberOfExamNoteOfStudent($compte)

  {

    $req = $this->db->prepare("SELECT * from notes,examen,eleve where notes.ideleve_notes=eleve.idcompte_eleve and notes.idtype_notes=examen.id_exam and notes.type_notes=2 and eleve.idcompte_eleve=? ");

    $req->execute([$compte]);

    $data=$req->fetchAll();

    $nb=count($data);

    return $nb;

  }



  function getExameNotesOfStudent($compte)

  {

    $req = $this->db->prepare("SELECT * from notes,examen,eleve where notes.ideleve_notes=eleve.idcompte_eleve and notes.idtype_notes=examen.id_exam and notes.type_notes=2 and eleve.idcompte_eleve=? ");

    $req->execute([$compte]);

    return $req->fetchAll();



  }



  function getNumberOfControleNoteOfStudent($compte)

  {

    $req = $this->db->prepare("SELECT * from notes,controle,eleve where notes.ideleve_notes=eleve.idcompte_eleve and notes.idtype_notes=controle.id_ctrl and notes.type_notes=1 and eleve.idcompte_eleve=? ");

    $req->execute([$compte]);

    $data=$req->fetchAll();

    $nb=count($data);

    return $nb;

  }



   function getControleNotesOfStudent($compte)

  {

    $req = $this->db->prepare("SELECT * from notes,controle,eleve where notes.ideleve_notes=eleve.idcompte_eleve and notes.idtype_notes=controle.id_ctrl and notes.type_notes=1 and eleve.idcompte_eleve=? ");

    $req->execute([$compte]);

    return $req->fetchAll();



  }



  function UpdateControleNotes($notes,$observation,$ideleve,$typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$codeEtab)

  {

    $req = $this->db->prepare("UPDATE notes SET valeur_notes=?,obser_notes=? where notes.ideleve_notes=? and notes.	type_notes=? and notes.idtype_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and notes.idprof_notes=? and notes.codeEtab_notes=?");

    $req->execute([

    $notes,$observation,$ideleve,$typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$codeEtab

    ]);



    $_SESSION['user']['addattendailyok']="Notes modifier avec succès";



    if($_SESSION['user']['profile'] == "Admin_globale")

    {
// ?codeEtab=C00123
  header("Location:../manager/notes.php?codeEtab=".$codeEtab);

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {

  header("Location:../locale/notes.php");

    }

    else if($_SESSION['user']['profile'] == "Teatcher")

    {

  header("Location:../teatcher/notes.php");

    }



  }



  function UpdateNotesOfStudent($noteid,$codeEtab,$studentid,$classeid,$note,$observation)

  {

    $reqX = $this->db->prepare("UPDATE notes SET valeur_notes=?,obser_notes=? where id_notes=? and codeEtab_notes=? and ideleve_notes=? and idclasse_notes=?");

    $reqX->execute([

    $note,$observation,$noteid,$codeEtab,$studentid,$classeid

    ]);



    $_SESSION['user']['addattendailyok']="Notes modifier avec succès";



    if($_SESSION['user']['profile'] == "Admin_globale")

    {
header("Location:../manager/notes.php?codeEtab=".$codeEtab);
  //header("Location:../manager/notes.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {

  header("Location:../locale/notes.php");

}else if($_SESSION['user']['profile'] == "Teatcher")

    {

  header("Location:../teatcher/notes.php");

    }





  }

  function getAllstudentNotesExamofthisclasses($classe,$examid,$codeEtabAssigner)

  {

    $req = $this->db->prepare("SELECT * from eleve,enseignant,classe,matiere,notes,examen where notes.ideleve_notes=eleve.idcompte_eleve and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and examen.id_exam=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and  classe.id_classe=? and examen.id_exam=?  and classe.codeEtab_classe=? and notes.type_notes=2");

    $req->execute([$classe,$examid,$codeEtabAssigner]);

    return $req->fetchAll();

  }

  function getAllstudentNotesExamofthisclassespecifiq($classe,$examid,$codeEtabAssigner,$eleveid)

  {

    $req = $this->db->prepare("SELECT * from eleve,enseignant,classe,matiere,notes,examen where notes.ideleve_notes=eleve.idcompte_eleve and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and examen.id_exam=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and  classe.id_classe=? and examen.id_exam=?  and classe.codeEtab_classe=? and notes.type_notes=2 and eleve.idcompte_eleve=?");

    $req->execute([$classe,$examid,$codeEtabAssigner,$eleveid]);

    return $req->fetchAll();

  }

  function getInformationsOfNotesStudent($noteid,$typenote,$ideleve,$codeEtabAssigner)

  {

    $req = $this->db->prepare("SELECT * from eleve,compte,notes,classe where eleve.idcompte_eleve=compte.id_compte and notes.ideleve_notes=eleve.idcompte_eleve and notes.idclasse_notes=classe.id_classe and notes.id_notes=? and notes.type_notes=? and eleve.idcompte_eleve=? and notes.codeEtab_notes=?");

    $req->execute([$noteid,$typenote,$ideleve,$codeEtabAssigner]);

    $data=$req->fetchAll();

    $array=json_encode($data,true);

    $someArray = json_decode($array, true);

    $donnees=$someArray[0]["matricule_eleve"]."*".$someArray[0]["nom_eleve"]."*".$someArray[0]["prenom_eleve"]."*".$someArray[0]["libelle_classe"]."*".$someArray[0]["id_classe"]."*".$someArray[0]["valeur_notes"];

    $donnees.="*".$someArray[0]["obser_notes"]."*".$someArray[0]["codeEtab_notes"]."*".$someArray[0]["id_notes"]."*".$someArray[0]["idtype_notes"]."*".$someArray[0]["idcompte_eleve"];

    return $donnees;



  }



  function getControleInfos($controleid)

  {

  $req = $this->db->prepare("SELECT * from controle where id_ctrl=?");

  $req->execute([$controleid]);

  $data=$req->fetchAll();

  $array=json_encode($data,true);

  $someArray = json_decode($array, true);

  $donnees=$someArray[0]["libelle_ctrl"];

  return $donnees;



  }



  function getAllstudentNotesofthisclasses($classe,$matiereid,$controleid,$teatcherid,$codeEtabAssigner)

  {

    $req = $this->db->prepare("SELECT * from eleve,enseignant,classe,matiere,notes,controle where notes.ideleve_notes=eleve.idcompte_eleve and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and controle.id_ctrl=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and classe.id_classe=?  and matiere.id_mat=? and controle.id_ctrl=? and enseignant.idcompte_enseignant=? and classe.codeEtab_classe=? and notes.type_notes=1  ");

    //$req.=."and controle.id_ctrl=?");

    $req->execute([$classe,$matiereid,$controleid,$teatcherid,$codeEtabAssigner]);

    return $req->fetchAll();

  }



  function getAllstudentNotesofthisclassespecifiq($classe,$matiereid,$controleid,$teatcherid,$codeEtabAssigner,$eleveid)

  {

    $req = $this->db->prepare("SELECT * from eleve,enseignant,classe,matiere,notes,controle where notes.ideleve_notes=eleve.idcompte_eleve and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and controle.id_ctrl=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and classe.id_classe=?  and matiere.id_mat=? and controle.id_ctrl=? and enseignant.idcompte_enseignant=? and classe.codeEtab_classe=? and notes.type_notes=1 and eleve.idcompte_eleve=?  ");

    //$req.=."and controle.id_ctrl=?");

    $req->execute([$classe,$matiereid,$controleid,$teatcherid,$codeEtabAssigner,$eleveid]);

    return $req->fetchAll();

  }



  function AddNotesExamen($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$idstudent,$codeEtab,$notes,$observation,$session)

  {

    $req = $this->db->prepare("INSERT INTO  notes SET type_notes=?,idtype_notes=?,idclasse_notes=?,idmat_notes=?,idprof_notes=?,ideleve_notes=?,codeEtab_notes=?,valeur_notes=?,obser_notes=?,session_notes=?");

    $req->execute([

      $typenote,

      $idtypenote,

      $classeId,

      $matiereid,

      $teatcherid,

      $idstudent,

      $codeEtab,

      $notes,

      $observation,
      $session

    ]);



    //nous allons mettre le statut du controle à 1 pour dire que notes ajouter



//   $statut=1;

//

//     $reqX = $this->db->prepare("UPDATE controle SET statut_ctrl=? where id_ctrl=? and codeEtab_ctrl=?");

//     $reqX->execute([

//       $statut,$idtypenote,$codeEtab

// ]);







  }



  function AddNotesControle($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$idstudent,$codeEtab,$notes,$observation,$session)

  {

    $req = $this->db->prepare("INSERT INTO  notes SET type_notes=?,idtype_notes=?,idclasse_notes=?,idmat_notes=?,idprof_notes=?,ideleve_notes=?,codeEtab_notes=?,valeur_notes=?,obser_notes=?,session_notes=?");

    $req->execute([

      $typenote,

      $idtypenote,

      $classeId,

      $matiereid,

      $teatcherid,

      $idstudent,

      $codeEtab,

      $notes,

      $observation,
      $session

    ]);



    //nous allons mettre le statut du controle à 1 pour dire que notes ajouter



  $statut=1;



    $reqX = $this->db->prepare("UPDATE controle SET statut_ctrl=? where id_ctrl=? and codeEtab_ctrl=?");

    $reqX->execute([

      $statut,$idtypenote,$codeEtab

]);







  }



  function getTheSpecificStudentForSchool($compteEtab,$classeId,$idstudent)

  {

        $req = $this->db->prepare("SELECT * from eleve,compte,inscription,classe,parent,parenter where compte.email_compte=eleve.email_eleve and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and classe.codeEtab_classe=? and classe.id_classe=? and compte.id_compte=?");

        $req->execute([$compteEtab,$classeId,$idstudent]);

        return $req->fetchAll();

  }



   function getAllStudentOfThisSchool($compte)

{

  $req = $this->db->prepare("SELECT * from eleve,compte,inscription,classe,parent,parenter where parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and  compte.email_compte=eleve.email_eleve and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and classe.codeEtab_classe=?");

  $req->execute([$compte]);

  return $req->fetchAll();

}



  function getNbAttendance($matricule,$date,$classeId)

  {

    $req = $this->db->prepare("SELECT statut_presence FROM presences where matricule_presence=? and classe_presence=? and date_presence=? ");

    $req->execute([$matricule,$classeId,$date]);

    $data=$req->fetchAll();

    $nb=count($data);

    return $nb;

  }



  function getstatutAttendance($matricule,$date,$classeId)

  {

      $req = $this->db->prepare("SELECT * FROM presences where matricule_presence=? and classe_presence=? and date_presence=? ");

      $req->execute([$matricule,$classeId,$date]);

      return $req->fetchAll();

      //$data=$req->fetchAll();

      /*$array=json_encode($data,true);

      $someArray = json_decode($array, true);

      $donnees=$someArray[0]["statut_presence"]."*".$someArray[0]["id_presence"];*/

  }

  function DetermineAllstudentofthisclassesSession($classe,$codeEtab,$session)
  {

    $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription where eleve.idcompte_eleve=compte.id_compte and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=? and classe.id_classe=? and classe.codeEtab_classe=? and compte.statut_compte!=0 and inscription.solder_inscrip=0 order by eleve.nom_eleve ASC");

    $req->execute([$session,$classe,$codeEtab]);

    return $req->fetchAll();

  }

  function DetermineAllstudentofthisclassesSessionT($classe,$codeEtab,$session)
  {

    $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription where eleve.idcompte_eleve=compte.id_compte and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=? and classe.id_classe=? and classe.codeEtab_classe=? and compte.statut_compte!=0  order by eleve.nom_eleve ASC");

    $req->execute([$session,$classe,$codeEtab]);

    return $req->fetchAll();

  }


  function getAllstudentofthisclassesSession($classe,$session)
  {

    $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription where eleve.idcompte_eleve=compte.id_compte and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=? and classe.id_classe=? and compte.statut_compte!=0 order by eleve.nom_eleve ASC");

    $req->execute([$session,$classe]);

    return $req->fetchAll();

  }

  function getDatacontroleInfoNb($idcompte,$controleid,$matierecontroleId,$teatchercontroleid,$classeid)
  {
      $req = $this->db->prepare("SELECT * FROM controle,notes where controle.id_ctrl=notes.idtype_notes and notes.ideleve_notes=? and notes.idtype_notes=? and notes.idmat_notes=? and notes.idprof_notes=? and notes.idclasse_notes=?");
      $req->execute([$idcompte,$controleid,$matierecontroleId,$teatchercontroleid,$classeid]);
      $data=$req->fetchAll();
      $nb=count($data);
      return $nb;
  }

  function getAllstudentofthisclassesSessionUnique($classe,$session,$eleveid)
  {

    $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription where eleve.idcompte_eleve=compte.id_compte and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=? and classe.id_classe=? and compte.statut_compte!=0 and compte.id_compte=? order by eleve.nom_eleve ASC");

    $req->execute([$session,$classe,$eleveid]);

    return $req->fetchAll();

  }

  function getAllstudentofMulticlassesSession($classe,$session)
  {
    $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription where eleve.idcompte_eleve=compte.id_compte and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=? and classe.id_classe in (?) and compte.statut_compte!=0 order by eleve.nom_eleve ASC");
    $req->execute([$session,$classe]);
    return $req->fetchAll();
  }

   function RecuperationAllstudentofthisclassesSession($classe,$codeEtab,$session)
  {

    $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription where eleve.idcompte_eleve=compte.id_compte and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=? and classe.id_classe=? and classe.codeEtab_classe=? and compte.statut_compte!=0 order by eleve.nom_eleve ASC");

    $req->execute([$session,$classe,$codeEtab]);

    return $req->fetchAll();

  }



  function getAllstudentofthisclasses($classe)

  {

    $years=date('Y');

    $nextyears=date("Y")+1;

    $session=$years."-".$nextyears;

    $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription where eleve.idcompte_eleve=compte.id_compte and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=? and classe.id_classe=? order by eleve.nom_eleve ASC");

    $req->execute([$session,$classe]);

    return $req->fetchAll();

  }



  function getAllInformationsOfStudent($compte,$session)

  {

      // $years=date('Y');
      //
      // $nextyears=date("Y")+1;
      //
      // $session=$years."-".$nextyears;

      // $req = $this->db->prepare("SELECT * FROM eleve,compte,parenter,classe,inscription where parenter.eleveid_parenter=eleve.idcompte_eleve and parenter.eleveid_parenter=eleve.idcompte_eleve and eleve.email_eleve=compte.email_compte and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=? and compte.id_compte=?");

      $req = $this->db->prepare("SELECT * from compte,eleve,parenter,classe,inscription where compte.id_compte=eleve.idcompte_eleve and eleve.idcompte_eleve=parenter.eleveid_parenter and inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and inscription.session_inscrip=? and parenter.eleveid_parenter=?");


      $req->execute([$session,$compte]);

      $data=$req->fetchAll();

      $array=json_encode($data,true);

      $someArray = json_decode($array, true);



      $donnees=$someArray[0]["id_compte"]."*".$someArray[0]["matricule_eleve"]."*".$someArray[0]["nom_eleve"]."*".$someArray[0]["prenom_eleve"]."*".$someArray[0]["datenais_eleve"]."*".$someArray[0]["lieunais_eleve"];

      $donnees.="*".$someArray[0]["sexe_eleve"]."*".$someArray[0]["email_eleve"]."*".$someArray[0]["parentid_parenter"]."*".$someArray[0]["libelle_classe"]."*".$someArray[0]["codeEtab_classe"]."*".$someArray[0]["photo_compte"];

      $donnees.="*".$someArray[0]["tel_compte"]."*".$someArray[0]["login_compte"]."*".$someArray[0]["codeEtab_inscrip"]."*".$someArray[0]["id_classe"]."*".$someArray[0]["allergie_eleve"]."*".$someArray[0]["condphy_eleve"];

      return $donnees;

  }

function UpdateStudent($nomad,$prenomad,$datenaisad,$lieunais,$sexe,$contactad,$codeEtab,$idcompte,$oldfile,$allergie,$condphy)
{
  $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,photo_compte=? where id_compte=? ");

  $req->execute([
    $nomad,
    $prenomad,
    $datenaisad,
    $contactad,
    $oldfile,
    $idcompte
  ]);

  //mise a jour dans la table Eleve

  $req1 = $this->db->prepare("UPDATE eleve SET nom_eleve=?,prenom_eleve=?,datenais_eleve=?,lieunais_eleve=?,sexe_eleve=?,allergie_eleve=?,condphy_eleve=? where codeEtab_eleve=? and idcompte_eleve=?");
  $req1->execute([
  $nomad,
  $prenomad,
  $datenaisad,
  $lieunais,
  $sexe,
  $allergie,
  $condphy,
  $codeEtab,
  $idcompte
  ]);
    $_SESSION['user']['updateroutineok']="Modification effectuée avec succès";

    if($_SESSION['user']['profile'] == "Admin_globale")

    {

  header("Location:../manager/updatestudent.php?compte=".$idcompte."&codeEtab=".$codeEtab);

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {

  header("Location:../locale/updatestudent.php?compte=".$idcompte."&codeEtab=".$codeEtab);

    }
}

  function getAllStudentOfClassesId($classe,$session)

  {



    //  $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription where compte.email_compte=eleve.email_eleve and classe.id_classe=inscription.idclasse_inscrip and compte.id_compte=inscription.ideleve_inscrip and classe.id_classe=? and inscription.session_inscrip=? and compte.statut_compte!=0 ");

    $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription where inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=compte.id_compte and compte.id_compte=eleve.idcompte_eleve and inscription.session_inscrip=classe.session_classe and classe.id_classe=? and inscription.session_inscrip=? and compte.statut_compte=1 ");

      $req->execute([$classe,$session]);

      return $req->fetchAll();



  }



  function getStudentIdcompteByMatricule($matri)

  {

      $req = $this->db->prepare("SELECT * FROM eleve,compte where compte.email_compte=eleve.email_eleve and eleve.matricule_eleve=?");

      $req->execute([$matri]);

      $data=$req->fetchAll();

      $array=json_encode($data,true);

      $someArray = json_decode($array, true);



      $donnees=$someArray[0]["id_compte"];



      return $donnees;

  }



  function AddAttendance($newdate,$libellemois,$annee,$classeId,$matricule,$statut,$num,$matiereid,$profid,$codeEtab,$session)

  {

    $req = $this->db->prepare("INSERT INTO  presences SET date_presence=?,mois_presence=?,annee_presence=?,classe_presence=?,matricule_presence=?,statut_presence=?,nbjoursmois=?,matiere_presence=?,teatcher_presence=?,codeEtab_presence=?,session_presence=?");

    $req->execute([$newdate,$libellemois,$annee,$classeId,$matricule,$statut,$num,$matiereid,$profid,$codeEtab,$session]);

  }



  function AddInscription($classeEtab,$idstudentcpte,$session,$datecrea)

  {

      $req = $this->db->prepare("INSERT INTO  inscription SET idclasse_inscrip=?,ideleve_inscrip=?,session_inscrip=?,date_inscrip=?,codeEtab_inscrip=?");

      $req->execute([$classeEtab,$idstudentcpte,$session,$datecrea,$codeEtab]);



      $_SESSION['user']['addStudok']="Inscription à bien été pris en compte dans le système avec succès";



      if($_SESSION['user']['profile'] == "Admin_globale")

      {

    header("Location:../manager/admission.php");

      }else if($_SESSION['user']['profile'] == "Admin_locale")

      {

    header("Location:../locale/admission.php");

      }





  }



  function ExisteStudent($matricule)

  {



    //$req = $this->db->prepare("SELECT * FROM eleve,compte,inscription where eleve.email_eleve=compte.email_compte and compte.id_compte=inscription.ideleve_inscrip and eleve.matricule_eleve=? and inscription.idclasse_inscrip=? and eleve.idparent_eleve=? and inscription.session_inscrip=?");

    $req = $this->db->prepare("SELECT * FROM eleve where eleve.matricule_eleve=?");

    //$req->execute([$matricule,$classe,$parent,$session]);

    $req->execute([$matricule]);

    $data=$req->fetchAll();

    $nb=count($data);

    return $nb;

  }



  function presencesExistByClassesAndDate($classe,$newdate)

  {

    $req = $this->db->prepare("SELECT * FROM presences where classe_presence=? and date_presence=?");

    $req->execute([$classe,$newdate]);

    $data=$req->fetchAll();

    $nb=count($data);

    return $nb;

  }



  function InscriptionAllReady($matricule,$classe,$parent,$session)

  {

    $req = $this->db->prepare("SELECT * FROM eleve,compte,inscription,parenter where eleve.email_eleve=compte.email_compte and compte.id_compte=inscription.ideleve_inscrip and parenter.eleveid_parenter=eleve.idcompte_eleve and eleve.matricule_eleve=? and inscription.idclasse_inscrip=? and parenter.parentid_parenter=? and inscription.session_inscrip=?");

    $req->execute([$matricule,$classe,$parent,$session]);

    $data=$req->fetchAll();

    $nb=count($data);

    return $nb;

  }


  function AddStudentFile($matri,$nomad,$prenomad,$classeEtab,$datenaisad,$sexe,$datecrea,$type_cpte,$statut,$lieunais,$sessionscolaire,$codeEtab,$fichierad,$allergie,$condphy)
  {
    $fonction="Eleve";

    $reqX = $this->db->prepare("INSERT INTO  compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,fonction_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?,photo_compte=?");
    $reqX->execute([
      $nomad,
      $prenomad,
      $datenaisad,
      $fonction,
      $type_cpte,
      $statut,
      $datecrea,
      $fichierad
    ]);

    // ajout des données dans la table inscription

    $idlastcompte=$this->db->lastInsertId();

    $req = $this->db->prepare("INSERT INTO  eleve SET matricule_eleve=?,nom_eleve=?,prenom_eleve=?,datenais_eleve=?,lieunais_eleve=?,sexe_eleve=?,codeEtab_eleve=?,idcompte_eleve=?,allergie_eleve=?,condphy_eleve=?");
    $req->execute([
      $matri,
      $nomad,
      $prenomad,
      $datenaisad,
      $lieunais,
      $sexe,
      $codeEtab,
      $idlastcompte,
      $allergie,
      $condphy
    ]);

    $reqY = $this->db->prepare("INSERT INTO  inscription SET 	idclasse_inscrip=?,	ideleve_inscrip=?,session_inscrip=?,date_inscrip=?,codeEtab_inscrip=?");

    $reqY->execute([

      $classeEtab,
      $idlastcompte,
      $sessionscolaire,
      $datecrea,
      $codeEtab

    ]);



    return $idlastcompte;


  }


  function AddStudent($matri,$nomad,$prenomad,$classeEtab,$datenaisad,$sexe,$datecrea,$type_cpte,$statut,$lieunais,$sessionscolaire,$codeEtab,$allergie,$condphy)
  {
    $fonction="Eleve";

    $reqX = $this->db->prepare("INSERT INTO  compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,fonction_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?");
    $reqX->execute([
      $nomad,
      $prenomad,
      $datenaisad,
      $fonction,
      $type_cpte,
      $statut,
      $datecrea
    ]);

    // ajout des données dans la table inscription

    $idlastcompte=$this->db->lastInsertId();

    $req = $this->db->prepare("INSERT INTO  eleve SET matricule_eleve=?,nom_eleve=?,prenom_eleve=?,datenais_eleve=?,lieunais_eleve=?,sexe_eleve=?,codeEtab_eleve=?,idcompte_eleve=?,allergie_eleve=?,condphy_eleve=?");
    $req->execute([
      $matri,
      $nomad,
      $prenomad,
      $datenaisad,
      $lieunais,
      $sexe,
      $codeEtab,
      $idlastcompte,
      $allergie,
      $condphy
    ]);

    $reqY = $this->db->prepare("INSERT INTO  inscription SET 	idclasse_inscrip=?,	ideleve_inscrip=?,session_inscrip=?,date_inscrip=?,codeEtab_inscrip=?");

    $reqY->execute([

      $classeEtab,
      $idlastcompte,
      $sessionscolaire,
      $datecrea,
      $codeEtab

    ]);



    return $idlastcompte;


  }

  function Addparenter($idparent,$studentid)
  {
    $req= $this->db->prepare("INSERT INTO  parenter SET parentid_parenter=?,eleveid_parenter=?");
    $req->execute([$idparent,$studentid]);

    //insertion dans la table notificationstate

    $reqX= $this->db->prepare("INSERT INTO ");
  }

  function DetermineNumberOfparenter($idparent,$studentid)
  {
    $req = $this->db->prepare("SELECT * from parenter where   parentid_parenter=? and eleveid_parenter=? ");
         $req->execute([
           //$studentmatri,$classeEtab,$codeEtab,$datefin,$datefin
           $idparent,$studentid

         ]);

         $data=$req->fetchAll();
         $nb=count($data);
         return $nb;
  }



  function AddStudentwithFile($matri,$nomad,$prenomad,$parenta,$classeEtab,$datenaisad,$sexe,$contactad,$emailad,$loginad,$passad,$codeEtab,$datecrea,$type_cpte,$statut,$fichierad,$lieunais,$session)

  {

    //ajout dans la table eleve







    //ajout dans la table compte



    $fonction="Eleve";



    $reqX = $this->db->prepare("INSERT INTO  compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=?,login_compte=?,pass_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?,photo_compte=?");

    $reqX->execute([

    $nomad,

    $prenomad,

    $datenaisad,

    $contactad,

    $emailad,

    $fonction,

    $loginad,

    $passad,

    $type_cpte,

    $statut,

    $datecrea,

    $fichierad,



    ]);



    // ajout des données dans la table inscription

    $idlastcompte=$this->db->lastInsertId();



    $req = $this->db->prepare("INSERT INTO  eleve SET matricule_eleve=?,nom_eleve=?,prenom_eleve=?,datenais_eleve=?,lieunais_eleve=?,sexe_eleve=?,email_eleve=?,login_eleve=?,idparent_eleve=?,codeEtab_eleve=?,idcompte_eleve=?");

    $req->execute([
    $matri,
    $nomad,
    $prenomad,
    $datenaisad,
    $lieunais,
    $sexe,
    $emailad,
    $loginad,
    $parenta,
    $codeEtab,
    $idlastcompte
    ]);



    $reqY = $this->db->prepare("INSERT INTO  inscription SET 	idclasse_inscrip=?,	ideleve_inscrip=?,session_inscrip=?,date_inscrip=?,codeEtab_inscrip=?");

    $reqY->execute([

      $classeEtab,
      $idlastcompte,
      $session,
      $datecrea,
      $codeEtab

    ]);



    $_SESSION['user']['addStudok']="Inscription à bien été pris en compte dans le système avec succès";



    if($_SESSION['user']['profile'] == "Admin_globale")

    {

  header("Location:../manager/admission.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {

  header("Location:../locale/admission.php");

    }









  }



  function AddStudentwithoutFile($matri,$nomad,$prenomad,$parenta,$classeEtab,$datenaisad,$sexe,$contactad,$emailad,$loginad,$passad,$codeEtab,$datecrea,$type_cpte,$statut,$lieunais,$session)

  {

    //ajout dans la table eleve







    //ajout dans la table compte



    $fonction="Eleve";



    $reqX = $this->db->prepare("INSERT INTO  compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=?,login_compte=?,pass_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?");

    $reqX->execute([

    $nomad,

    $prenomad,

    $datenaisad,

    $contactad,

    $emailad,

    $fonction,

    $loginad,

    $passad,

    $type_cpte,

    $statut,

    $datecrea

    ]);



    // ajout des données dans la table inscription

    $idlastcompte=$this->db->lastInsertId();



    $req = $this->db->prepare("INSERT INTO  eleve SET matricule_eleve=?,nom_eleve=?,prenom_eleve=?,datenais_eleve=?,lieunais_eleve=?,sexe_eleve=?,email_eleve=?,login_eleve=?,idparent_eleve=?,codeEtab_eleve=?,idcompte_eleve=?");

    $req->execute([

    $matri,

    $nomad,

    $prenomad,

    $datenaisad,

    $lieunais,

    $sexe,

    $emailad,

    $loginad,

    $parenta,

    $codeEtab,

    $idlastcompte

    ]);



    $reqY = $this->db->prepare("INSERT INTO  inscription SET 	idclasse_inscrip=?,	ideleve_inscrip=?,session_inscrip=?,date_inscrip=?,codeEtab_inscrip=?");

    $reqY->execute([



      $classeEtab,
      $idlastcompte,
      $session,
      $datecrea,
      $codeEtab

    ]);



    $_SESSION['user']['addStudok']="Inscription à bien été pris en compte dans le système avec succès";



    if($_SESSION['user']['profile'] == "Admin_globale")

    {

  header("Location:../manager/admission.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {

  header("Location:../locale/admission.php");

    }

  }



}



 ?>
