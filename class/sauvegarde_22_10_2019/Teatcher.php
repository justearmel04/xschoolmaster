<?php

class Teatcher{

public $db;
function __construct() {
  require_once('../class/cnx.php');

  $db = new mysqlConnector();
  $this->db= $db->dataBase;
}

function getnumberofclasseswithprogrammeTeatcher($teatcherid,$libellesessionencours)
{
  $req = $this->db->prepare("SELECT distinct programme.id_prog FROM programme,compte,classe,dispenser,matiere where programme.idprof_prog=compte.id_compte and dispenser.idclasse_disp=classe.id_classe and dispenser.id_enseignant=compte.id_compte and matiere.id_mat=dispenser.id_cours and classe.session_classe=dispenser.session_disp and compte.id_compte=? and dispenser.session_disp=?");
  $req->execute([$teatcherid,$libellesessionencours]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;

}

function getTeatcherEtabCode($idcompte)
{
  $req = $this->db->prepare("SELECT * FROM enseigner where id_enseignant=?");
  $req->execute([$idcompte]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);
  $codeEtab=$someArray[0]["codeEtab"];
  return $codeEtab;
}

function getAllTeatchersOfThisClassesEtab($code,$classeEtab)
{
  $req = $this->db->prepare("SELECT distinct compte.id_compte,compte.nom_compte,compte.prenom_compte FROM compte,enseignant,dispenser where compte.id_compte=enseignant.idcompte_enseignant and dispenser.id_enseignant=compte.id_compte and dispenser.codeEtab=? and dispenser.idclasse_disp=?");
  $req->execute([$code,$classeEtab]);
  return $req->fetchAll();

}
function deletedTeatcherSystem($compte)
{
  $req = $this->db->prepare("UPDATE compte set compte.statut_compte=0 where compte.id_compte=?");
  $req->execute([$compte]);

  $_SESSION['user']['deleteprofaok']="Professeur supprimer avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

  header("Location:../manager/teatchers.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale") {

      header("Location:../locale/teatchers.php");

      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }
}

function suppressionTeatcherSystem($compte)
{
  $req = $this->db->prepare("DELETE FROM enseignant where idcompte_enseignant=?");
  $req->execute([$compte]);

  //suppression de la table enseigner

  $reqX = $this->db->prepare("DELETE FROM enseigner  where id_enseignant=?");
  $reqX->execute([$compte]);

  //supprimer la table comptead

  $reqY= $this->db->prepare("DELETE FROM compte where id_compte=?");
  $reqY->execute([$compte]);

  $_SESSION['user']['deleteprofaok']="Professeur supprimer avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

  header("Location:../manager/teatchers.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale") {

      header("Location:../locale/teatchers.php");

      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }
}

function getNumberOfTeatcherClasseSchool($idclasse,$compteEtab)
{
$req = $this->db->prepare("SELECT * FROM compte,enseignant,dispenser where compte.id_compte=enseignant.idcompte_enseignant and dispenser.id_enseignant=compte.id_compte and dispenser.codeEtab=? and dispenser.idclasse_disp=?");
$req->execute([$compteEtab,$idclasse]);
$data=$req->fetchAll();
$nb=count($data);
return $nb;
}

function getNbOfTeatcherClasseSchool($idclasse,$compteEtab,$libellesessionencours)
{
  $req = $this->db->prepare("SELECT * FROM compte,enseignant,dispenser where compte.id_compte=enseignant.idcompte_enseignant and compte.id_compte=dispenser.id_enseignant and dispenser.codeEtab=? and dispenser.idclasse_disp=?");
  $req->execute([$compteEtab,$idclasse]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getNumberOfSystemTeatcher()
{
    $req = $this->db->prepare("SELECT * FROM enseignant");
    $req->execute([]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;
}

function getNameofTeatcherById($idTeatcher)
{
  $req = $this->db->prepare("SELECT * FROM compte,enseignant where compte.id_compte=enseignant.idcompte_enseignant and compte.id_compte=?");
  $req->execute([$idTeatcher]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);
  $nom=$someArray[0]["nom_enseignant"]." ".$someArray[0]["prenom_enseignant"];

  return $nom;
}

function getAllTeatcherOfThisSchool($compte)
{
      $req = $this->db->prepare("SELECT * FROM compte,enseignant,enseigner where compte.id_compte=enseignant.idcompte_enseignant and compte.id_compte=enseigner.id_enseignant and enseigner.codeEtab=?");
      $req->execute([$compte]);
      return $req->fetchAll();
}

function getAllTeatcherOfThisClasseSchool($idclasse,$compteEtab)
{
      $req = $this->db->prepare("SELECT * FROM compte,enseignant,enseigner where compte.id_compte=enseignant.idcompte_enseignant and compte.id_compte=enseigner.id_enseignant and enseigner.codeEtab=? and enseigner.id_classe=?");
      $req->execute([$compteEtab,$idclasse]);
      return $req->fetchAll();
}

function getTeatcherOfschooClasses($code,$classe,$matiere)
{
  $req = $this->db->prepare("SELECT * FROM compte,enseignant,dispenser where compte.id_compte=enseignant.idcompte_enseignant and dispenser.id_enseignant=compte.id_compte and dispenser.codeEtab=? and dispenser.idclasse_disp=? and dispenser.id_cours=?");
    $req->execute([$code,$classe,$matiere]);
    return $req->fetchAll();
}

function getterMailOfTeatcherBySubjectidAndClasses($matiere,$classe,$Etab)
{
  $req = $this->db->prepare("SELECT * FROM compte,enseignant,dispenser where compte.id_compte=enseignant.idcompte_enseignant and dispenser.id_enseignant=compte.id_compte and dispenser.codeEtab=? and dispenser.idclasse_disp=? and dispenser.id_cours=?");
    $req->execute([$Etab,$classe,$matiere]);
    $data=$req->fetchAll();
    $array=json_encode($data,true);
    $someArray = json_decode($array, true);
    $donnees=$someArray[0]["email_compte"];
    return $donnees;
}

function getTeatcherOfschooClassesNb($code,$classe,$matiere)
{
    $req = $this->db->prepare("SELECT * FROM compte,enseignant,dispenser where compte.id_compte=enseignant.idcompte_enseignant and dispenser.id_enseignant=compte.id_compte and dispenser.codeEtab=? and dispenser.idclasse_disp=? and dispenser.id_cours=?");
    $req->execute([$code,$classe,$matiere]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;
}

function updateTeatcherwithOutFile($nomTea,$prenomTea,$datenaisTea,$lieunaisTea,$sexeTea,$emailTea,$natTea,$contactTea,$situationTea,$nbchildTea,$cnpsTea,$matTea,$mutuelTea,$dateEmbTea,$brutTea,$netTea,$fichierad,$idcompte)
{
  // modification dans la table enseignant
    $req = $this->db->prepare("UPDATE enseignant SET nom_enseignant=?,prenom_enseignant=?,lieunais_enseignant=?,sexe_enseignant=?,email_enseignant=?,nat_enseignant=?,situation_enseignant=?,nbchild_enseignant=?,cnps_enseignant=?,matricule_enseignant=?,mutuelle_enseignant=?,brut_enseignant=?,net_enseignant=? where idcompte_enseignant=? ");
    $req->execute([
    $nomTea,
    $prenomTea,
    $lieunaisTea,
    $sexeTea,
    $emailTea,
    $natTea,
    $situationTea,
    $nbchildTea,
    $cnpsTea,
    $matTea,
    $mutuelTea,
    $brutTea,
    $netTea,
    $idcompte
  ]);

  //modification dans la table compte

  $reqx = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=? where id_compte=? ");
  $reqx->execute([
$nomTea,
$prenomTea,
$datenaisTea,
$contactTea,
$emailTea,
$idcompte
]);

$_SESSION['user']['updateteaok']="Professeur modifier avec succès";

if($_SESSION['user']['profile'] == "Admin_globale") {

header("Location:../manager/teatchers.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale") {

    header("Location:../locale/teatchers.php");

    }else if($_SESSION['user']['profile'] == "Teatcher") {

          header("Location:../teatcher/index.php");

      }else if($_SESSION['user']['profile'] == "Student") {

            header("Location:../student/index.php");

        }else if($_SESSION['user']['profile'] == "Parent") {

              header("Location:../parent/index.php");

          }

 //header("Location:../manager/teatchers.php");

}

function updateTeacherInfosPersowithFile($nomTea,$prenomTea,$datenaisTea,$lieunaisTea,$sexeTea,$emailTea,$natTea,$contactTea,$situationTea,$nbchildTea,$idcompte,$fichierad)
{
    // modification dans la table enseignant
    $req = $this->db->prepare("UPDATE enseignant SET nom_enseignant=?,prenom_enseignant=?,lieunais_enseignant=?,sexe_enseignant=?,email_enseignant=?,nat_enseignant=?,situation_enseignant=?,nbchild_enseignant=? where idcompte_enseignant=? ");
    $req->execute([
    $nomTea,
    $prenomTea,
    $lieunaisTea,
    $sexeTea,
    $emailTea,
    $natTea,
    $situationTea,
    $nbchildTea,
    $idcompte
  ]);

  //modification dans la table compte

  $reqx = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,photo_compte=? where id_compte=? ");
  $reqx->execute([
$nomTea,
$prenomTea,
$datenaisTea,
$contactTea,
$emailTea,
$fichierad,
$idcompte
]);


$_SESSION['user']['updateteaok']="Professeur modifier avec succès";

if($_SESSION['user']['profile'] == "Admin_globale") {

header("Location:../manager/teatchers.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale") {

    header("Location:../locale/teatchers.php");

    }else if($_SESSION['user']['profile'] == "Teatcher") {

          header("Location:../teatcher/index.php");

      }else if($_SESSION['user']['profile'] == "Student") {

            header("Location:../student/index.php");

        }else if($_SESSION['user']['profile'] == "Parent") {

              header("Location:../parent/index.php");

          }


}

function updateTeatcherwithFile($nomTea,$prenomTea,$datenaisTea,$lieunaisTea,$sexeTea,$emailTea,$natTea,$contactTea,$situationTea,$nbchildTea,$cnpsTea,$matTea,$mutuelTea,$dateEmbTea,$brutTea,$netTea,$fichierad,$idcompte)
{
  // modification dans la table enseignant
    $req = $this->db->prepare("UPDATE enseignant SET nom_enseignant=?,prenom_enseignant=?,lieunais_enseignant=?,sexe_enseignant=?,email_enseignant=?,nat_enseignant=?,situation_enseignant=?,nbchild_enseignant=?,cnps_enseignant=?,matricule_enseignant=?,mutuelle_enseignant=?,brut_enseignant=?,net_enseignant=? where idcompte_enseignant=? ");
    $req->execute([
    $nomTea,
    $prenomTea,
    $lieunaisTea,
    $sexeTea,
    $emailTea,
    $natTea,
    $situationTea,
    $nbchildTea,
    $cnpsTea,
    $matTea,
    $mutuelTea,
    $brutTea,
    $netTea,
    $idcompte
  ]);

  //modification dans la table compte

  $reqx = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,photo_compte=? where id_compte=? ");
  $reqx->execute([
$nomTea,
$prenomTea,
$datenaisTea,
$contactTea,
$emailTea,
$fichierad,
$idcompte
]);

$_SESSION['user']['updateteaok']="Professeur modifier avec succès";

if($_SESSION['user']['profile'] == "Admin_globale") {

header("Location:../manager/teatchers.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale") {

    header("Location:../locale/teatchers.php");

    }else if($_SESSION['user']['profile'] == "Teatcher") {

          header("Location:../teatcher/index.php");

      }else if($_SESSION['user']['profile'] == "Student") {

            header("Location:../student/index.php");

        }else if($_SESSION['user']['profile'] == "Parent") {

              header("Location:../parent/index.php");

          }

 //header("Location:../manager/teatchers.php");

}

function updateTeacherInfosPersowithOutFile($nomTea,$prenomTea,$datenaisTea,$lieunaisTea,$sexeTea,$emailTea,$natTea,$contactTea,$situationTea,$nbchildTea,$idcompte,$fichierad)
{
  // modification dans la table enseignant
  $req = $this->db->prepare("UPDATE enseignant SET nom_enseignant=?,prenom_enseignant=?,lieunais_enseignant=?,sexe_enseignant=?,email_enseignant=?,nat_enseignant=?,situation_enseignant=?,nbchild_enseignant=? where idcompte_enseignant=? ");
  $req->execute([
  $nomTea,
  $prenomTea,
  $lieunaisTea,
  $sexeTea,
  $emailTea,
  $natTea,
  $situationTea,
  $nbchildTea,
  $idcompte
  ]);

  //modification dans la table compte

  $reqx = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=? where id_compte=? ");
  $reqx->execute([
  $nomTea,
  $prenomTea,
  $datenaisTea,
  $contactTea,
  $emailTea,

  $idcompte
  ]);


  $_SESSION['user']['updateteaok']="Professeur modifier avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

  header("Location:../manager/teatchers.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale") {

  header("Location:../locale/teatchers.php");

  }else if($_SESSION['user']['profile'] == "Teatcher") {

        header("Location:../teatcher/index.php");

    }else if($_SESSION['user']['profile'] == "Student") {

          header("Location:../student/index.php");

      }else if($_SESSION['user']['profile'] == "Parent") {

            header("Location:../parent/index.php");

        }
}

function updateTeatcherPrfessionnelleInfos($cnpsTea,$matTea,$mutuelTea,$dateEmbTea,$brutTea,$netTea,$idcompte)
{
  // modification dans la table enseignant
  $req = $this->db->prepare("UPDATE enseignant SET cnps_enseignant=?,matricule_enseignant=?,mutuelle_enseignant=?,dateemb_enseignant=?,brut_enseignant=?,net_enseignant=?	 where idcompte_enseignant=? ");
  $req->execute([
  $cnpsTea,
  $matTea,
  $mutuelTea,
  $dateEmbTea,
  $brutTea,
  $netTea,
  $idcompte
  ]);
  $_SESSION['user']['updateteaok']="Professeur modifier avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

  header("Location:../manager/teatchers.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale") {

  header("Location:../locale/teatchers.php");

  }else if($_SESSION['user']['profile'] == "Teatcher") {

        header("Location:../teatcher/index.php");

    }else if($_SESSION['user']['profile'] == "Student") {

          header("Location:../student/index.php");

      }else if($_SESSION['user']['profile'] == "Parent") {

            header("Location:../parent/index.php");

        }
}

function updateTeacherCompteInfos($loginTea,$passTea,$idcompte)
{
  //modificaion des infos du comptead

  $req = $this->db->prepare("UPDATE compte SET login_compte=?,pass_compte=? where id_compte=?");
  $req->execute([
  $loginTea,
  $passTea,
  $idcompte
  ]);
  $_SESSION['user']['updateteaok']="Professeur modifier avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

  header("Location:../manager/teatchers.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale") {

  header("Location:../locale/teatchers.php");

  }else if($_SESSION['user']['profile'] == "Teatcher") {

        header("Location:../teatcher/index.php");

    }else if($_SESSION['user']['profile'] == "Student") {

          header("Location:../student/index.php");

      }else if($_SESSION['user']['profile'] == "Parent") {

            header("Location:../parent/index.php");

        }

}

function getAllTeatchersByschoolCode($code)
{
    $req = $this->db->prepare("SELECT * FROM compte,enseignant,enseigner,etablissement WHERE compte.email_compte=enseignant.email_enseignant and compte.id_compte=enseigner.id_enseignant and enseigner.codeEtab=etablissement.code_etab and etablissement.code_etab=? and compte.statut_compte=1 ");
    $req->execute([$code]);
    return $req->fetchAll();
}

function getAllTeatchersByschoolCodewithId($code,$compte)
{
  $req = $this->db->prepare("SELECT * FROM compte,enseignant,enseigner,etablissement WHERE compte.email_compte=enseignant.email_enseignant and compte.id_compte=enseigner.id_enseignant and enseigner.codeEtab=etablissement.code_etab and etablissement.code_etab=? and compte.id_compte=? ");
  $req->execute([$code,$compte]);
  return $req->fetchAll();
}

function getAllTeatchersByTeatcherId($compte)
{
  $req = $this->db->prepare("SELECT * FROM compte,enseignant,enseigner,etablissement WHERE compte.email_compte=enseignant.email_enseignant and compte.id_compte=enseigner.id_enseignant and enseigner.codeEtab=etablissement.code_etab and compte.id_compte=? ");
  $req->execute([$compte]);
  return $req->fetchAll();

}

function getTeatcherInfobyId($id_compte)
{
  $req = $this->db->prepare("SELECT  * FROM compte,etablissement,enseigner,enseignant where enseignant.email_enseignant=compte.email_compte and compte.id_compte=enseigner.id_enseignant and enseigner.codeEtab=etablissement.code_etab and compte.id_compte=? ");
  $req->execute([$id_compte]);
  $data=$req->fetchAll();

    $array=json_encode($data,true);
    $someArray = json_decode($array, true);

    $donnees=$someArray[0]["code_etab"]."*".$someArray[0]["nom_compte"]."*".$someArray[0]["prenom_compte"]."*".$someArray[0]["datenais_compte"]."*".$someArray[0]["sexe_enseignant"]."*".$someArray[0]["tel_compte"];
    $donnees.="*".$someArray[0]["nat_enseignant"]."*".$someArray[0]["dateemb_enseignant"]."*".$someArray[0]["email_compte"]."*".$someArray[0]["login_compte"]."*".$someArray[0]["photo_compte"]."*".$someArray[0]["lieunais_enseignant"]."*".$someArray[0]["situation_enseignant"];
    $donnees.="*".$someArray[0]["nbchild_enseignant"]."*".$someArray[0]["cnps_enseignant"]."*".$someArray[0]["matricule_enseignant"]."*".$someArray[0]["mutuelle_enseignant"]."*".$someArray[0]["brut_enseignant"]."*".$someArray[0]["net_enseignant"];
    $donnees.="*".$someArray[0]["login_compte"];
    return $donnees;
}

function getAllTeatcherbyschoolCode($code)
{
  $req = $this->db->prepare("SELECT  * FROM enseigner,compte,etablissement where etablissement.code_etab=enseigner.codeEtab and compte.id_compte=enseigner.id_enseignant  and enseigner.codeEtab=? ");
  $req->execute([$code]);
  return $req->fetchAll();
}


function EnsignerAdd($codeetab,$idTeatcher)
{
  $req = $this->db->prepare("INSERT INTO enseigner SET id_enseignant=?,codeEtab=?");
    $req->execute([$idTeatcher,$codeetab]);
    $_SESSION['user']['addteaok']="Enseignant ajouté avec succès";

    if($_SESSION['user']['profile'] == "Admin_globale") {

  header("Location:../manager/addteatcher.php");
      }else if($_SESSION['user']['profile'] == "Admin_locale") {

      header("Location:../locale/addteatcher.php");

        }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/index.php");

          }else if($_SESSION['user']['profile'] == "Student") {

                header("Location:../student/index.php");

            }else if($_SESSION['user']['profile'] == "Parent") {

                  header("Location:../parent/index.php");

              }

     //header("Location:../manager/addteatcher.php");
}

function existTeatcherbySchoolCode($email,$idTeatcher,$codeetab)
{
  $type_cpte="Teatcher";
  $req = $this->db->prepare("SELECT * FROM compte,assigner,enseignant where enseigner.id_enseignant=compte.id_compte and enseignant.email_enseignant=compte.email_compte and  compte.id_compte=? and enseigner.codeEtab=? and enseignant.email_enseignant=? and type_compte=?  ");
  $req->execute([$idTeatcher,$codeetab,$email]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getIdTeatcher($email)
{
  $req = $this->db->prepare("SELECT  id_compte FROM compte where email_compte=?");
  $req->execute([$email]);
  $data=$req->fetchAll();

    $array=json_encode($data,true);
    $someArray = json_decode($array, true);

    $donnees=$someArray[0]["id_compte"];

    return $donnees;
}

function getTheSpecificTeatcherForSchool($compte,$matTeatcher,$idprof)
{
    $req = $this->db->prepare("SELECT * FROM compte,enseignant,enseigner where compte.id_compte=enseignant.idcompte_enseignant and compte.id_compte=enseigner.id_enseignant and enseigner.codeEtab=? and enseignant.matricule_enseignant=? and enseignant.idcompte_enseignant=?");
    $req->execute([$compte,$matTeatcher,$idprof]);
    return $req->fetchAll();
}

function getTheTeatcherOfThisMatricule($matricule)
{
$req = $this->db->prepare("SELECT * FROM compte,enseignant where compte.id_compte=enseignant.idcompte_enseignant and enseignant.matricule_enseignant=?");
$req->execute([$matricule]);
return $req->fetchAll();
}

function getAllTeatchers()
{
  $type_cpte="Teatcher";
  $req = $this->db->prepare("SELECT * FROM compte,enseignant where enseignant.email_enseignant=compte.email_compte and compte.type_compte=?");
  $req->execute([$type_cpte]);
  return $req->fetchAll();
}

function existTeatcher($login,$email)
{
  $type_cpte="Teatcher";
  $req = $this->db->prepare("SELECT * FROM compte where type_compte=? and  (login_compte=? or email_compte=?)");
  $req->execute([$type_cpte,$login,$email]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;

}

function AddTeacherwithfile($nomTea,$prenomTea,$emailTea,$sexeTea,$statut,$dateEmbTea,$datenaisTea,$contactTea,$fonction,$loginTea,$passTea,$type_cpte,$datecrea,$fichierad,$lieunaisTea,$natTea,$situationTea,$nbchildTea,$cnpsTea,$matTea,$mutuelTea,$brutTea,$netTea,$libetab)
{

  //insertion dans la table compte

  $req1 = $this->db->prepare("INSERT INTO compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=?,login_compte=?,pass_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?,photo_compte=? ");
  $req1->execute([
    $nomTea,
    $prenomTea,
    $datenaisTea,
    $contactTea,
    $emailTea,
    $fonction,
    $loginTea,
    $passTea,
    $type_cpte,
    $statut,
    $datecrea,
    $fichierad
  ]);

  $nbcompte=$this->db->lastInsertId();

    //inserion dans la table enseignant
  $req = $this->db->prepare("INSERT INTO enseignant SET nom_enseignant=?,prenom_enseignant=?,email_enseignant=?,sexe_enseignant=?,statut_enseignant=?,nat_enseignant=?,dateemb_enseignant=?,situation_enseignant=?,nbchild_enseignant=?,cnps_enseignant=?,matricule_enseignant=?,mutuelle_enseignant=?,brut_enseignant=?,net_enseignant=?,lieunais_enseignant=?,idcompte_enseignant=?");
  $req->execute([
  $nomTea,
  $prenomTea,
  $emailTea,
  $sexeTea,
  $statut,
  $natTea,
  $dateEmbTea,
  $situationTea,$nbchildTea,$cnpsTea,$matTea,$mutuelTea,$brutTea,$netTea,$lieunaisTea,$nbcompte
]);



  $reqx = $this->db->prepare("INSERT INTO enseigner SET id_enseignant=?,codeEtab=?");
  $reqx->execute([$nbcompte,$libetab]);

  $_SESSION['user']['addteaok']="Professeur ajouté avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

header("Location:../manager/addteatcher.php?compte=".$nbcompte);
    }else if($_SESSION['user']['profile'] == "Admin_locale") {

    header("Location:../locale/addteatcher.php?compte=".$nbcompte);

      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }

   //header("Location:../manager/addteatcher.php?compte=".$nbcompte);

}

function AddTeacherwithoutfile($nomTea,$prenomTea,$emailTea,$sexeTea,$statut,$dateEmbTea,$datenaisTea,$contactTea,$fonction,$loginTea,$passTea,$type_cpte,$datecrea,$lieunaisTea,$natTea,$situationTea,$nbchildTea,$cnpsTea,$matTea,$mutuelTea,$brutTea,$netTea,$libetab)
{
  //insertion dans la table compte

  $req1 = $this->db->prepare("INSERT INTO compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=?,login_compte=?,pass_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?");
  $req1->execute([
    $nomTea,
    $prenomTea,
    $datenaisTea,
    $contactTea,
    $emailTea,
    $fonction,
    $loginTea,
    $passTea,
    $type_cpte,
    $statut,
    $datecrea

  ]);
  $nbcompte=$this->db->lastInsertId();

  //inserion dans la table enseignant
$req = $this->db->prepare("INSERT INTO enseignant SET nom_enseignant=?,prenom_enseignant=?,email_enseignant=?,sexe_enseignant=?,statut_enseignant=?,nat_enseignant=?,dateemb_enseignant=?,situation_enseignant=?,nbchild_enseignant=?,cnps_enseignant=?,matricule_enseignant=?,mutuelle_enseignant=?,brut_enseignant=?,net_enseignant=?,lieunais_enseignant=?,idcompte_enseignant=?");
$req->execute([$nomTea,
$prenomTea,
$emailTea,
$sexeTea,
$statut,
$natTea,
$dateEmbTea,
$situationTea,$nbchildTea,$cnpsTea,$matTea,$mutuelTea,$brutTea,$netTea,$lieunaisTea,$nbcompte

]);


$reqx = $this->db->prepare("INSERT INTO enseigner SET id_enseignant=?,codeEtab=?");
$reqx->execute([$nbcompte,$libetab]);

$_SESSION['user']['addteaok']="Professeur ajouté avec succès";

if($_SESSION['user']['profile'] == "Admin_globale") {

header("Location:../manager/addteatcher.php?compte=".$nbcompte);
  }else if($_SESSION['user']['profile'] == "Admin_locale") {

  header("Location:../locale/addteatcher.php?compte=".$nbcompte);

    }else if($_SESSION['user']['profile'] == "Teatcher") {

          header("Location:../teatcher/index.php");

      }else if($_SESSION['user']['profile'] == "Student") {

            header("Location:../student/index.php");

        }else if($_SESSION['user']['profile'] == "Parent") {

              header("Location:../parent/index.php");

          }

 //header("Location:../manager/addteatcher.php?compte=".$nbcompte);

}


}

 ?>
