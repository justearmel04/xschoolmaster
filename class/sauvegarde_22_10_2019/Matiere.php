<?php

class Matiere{

  public $db;
  function __construct() {
    require_once('../class/cnx.php');
    $db = new mysqlConnector();
    $this->db= $db->dataBase;
      }

      function getMatiereLibelleByIdMat($matiereid,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM matiere where id_mat=? and codeEtab_mat=?");
        $req->execute([$matiereid,$codeEtab]);
        $data=$req->fetchAll();

         $array=json_encode($data,true);
         $someArray = json_decode($array, true);

         $donnees=$someArray[0]["libelle_mat"];
           return $donnees;
      }

      function getAllSubjectTeatchByTeatcherId($IdCompte)
      {
        $req = $this->db->prepare("SELECT * FROM matiere where teatcher_mat=?");
        $req->execute([$IdCompte]);
        return $req->fetchAll();
      }

      function getAllMatiereOfThisSchool($compte)
      {
        $req = $this->db->prepare("SELECT * FROM matiere,classe,dispenser,compte where compte.id_compte=dispenser.id_enseignant and dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and matiere.codeEtab_mat=? order by libelle_mat ASC");
        $req->execute([$compte]);
        return $req->fetchAll();
      }

      function ExistMatiereAllready($matiere,$classe,$codeEtab)
      {

      }


      function getAllControleMatiereOfThisSchool($codeEtabLocal,$session)
      {
        $req = $this->db->prepare("SELECT * FROM matiere,controle,compte,enseignant,etablissement,classe where compte.id_compte=enseignant.idcompte_enseignant and controle.teatcher_ctrl=compte.id_compte and controle.codeEtab_ctrl=etablissement.code_etab and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and etablissement.code_etab=? and controle.session_ctrl=? ");
        $req->execute([$codeEtabLocal,$session]);
        return $req->fetchAll();
      }

      function getAllControleMatiereOfThisTeatcherId($idcompte)
      {
        $req = $this->db->prepare("SELECT * FROM matiere,controle,compte,enseignant,etablissement,classe where compte.id_compte=enseignant.idcompte_enseignant and controle.teatcher_ctrl=compte.id_compte and controle.codeEtab_ctrl=etablissement.code_etab and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and controle.teatcher_ctrl=? ");
        $req->execute([$idcompte]);
        return $req->fetchAll();
      }

      function getAllSubjectOfClassesByEtab($codeEtabLocal,$session)
      {
        $req = $this->db->prepare("SELECT * FROM matiere where codeEtab_mat=? and session_mat=?");
         $req->execute([$codeEtabLocal,$session]);
         return $req->fetchAll();
      }


      function getAllSubjectOfClasses($classeId,$codeEtabLocal,$session)
      {
        $req = $this->db->prepare("SELECT * FROM matiere,classe,compte where compte.id_compte=matiere.teatcher_mat and   matiere.classe_mat=classe.id_classe and  matiere.classe_mat=? and matiere.codeEtab_mat=? and matiere.session_mat=?");
         $req->execute([$classeId,$codeEtabLocal,$session]);
         return $req->fetchAll();
      }

      function getcodeEtabByLocalId($userId)
      {
        $req = $this->db->prepare("SELECT * FROM assigner  where id_adLocal=?");
         $req->execute([$userId]);
         $data=$req->fetchAll();

          $array=json_encode($data,true);
          $someArray = json_decode($array, true);

          $donnees=$someArray[0]["codeEtab_assign"];
            return $donnees;

      }

      function getEtablissementbyCodeEtab($codeetab)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement  where code_etab=?");
         $req->execute([$codeetab]);
         return $req->fetchAll();
      }

      function getAllcodesEtab()
      {
        $req = $this->db->prepare("SELECT etablissement.code_etab,etablissement.libelle_etab FROM etablissement order by id_etab desc ");
         $req->execute();
         return $req->fetchAll();
      }

      function getAllcodesEtabBycodeEtab($codeEtabAssigner)
      {
        $req = $this->db->prepare("SELECT etablissement.code_etab,etablissement.libelle_etab FROM etablissement where etablissement.code_etab=?");
         $req->execute([$codeEtabAssigner]);
         return $req->fetchAll();
      }

      function getEtabInfosbyCode($etab)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement  where code_etab=?");
         $req->execute([$etab]);
         $data=$req->fetchAll();

  $array=json_encode($data,true);
  $someArray = json_decode($array, true);

  $donnees=$someArray[0]["code_etab"]."*".$someArray[0]["libelle_etab"]."*".$someArray[0]["tel_etab"]."*".$someArray[0]["tel1_etab"]."*".$someArray[0]["email_etab"]."*".$someArray[0]["adresse_etab"];
  $donnees.="*".$someArray[0]["logo_etab"];
  return $donnees;
      }

      function getAllEtab()
      {
        $req = $this->db->prepare("SELECT * FROM etablissement order by createby_etab desc  ");
         $req->execute();
         return $req->fetchAll();
      }

      function existEtab($codeetab)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement where code_etab=? ");
        $req->execute([$codeetab]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }



      function getAllAdminLocalBysearchCode($code)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement where code_etab=?  ");
         $req->execute([$code]);
         return $req->fetchAll();
      }

      function getAllAdminLocalBysearchperiode($datedu,$dateau)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement where datecrea_etab>=? and  datecrea_etab<=? ");
         $req->execute([$datedu,$dateau]);
         return $req->fetchAll();
      }


}

?>
