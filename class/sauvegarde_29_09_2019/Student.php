<?php

class Student{



  public $db;

  function __construct() {



    require_once('../class/cnx.php');



    $db = new mysqlConnector();

    $this->db= $db->dataBase;

  }



function getNotesAndObservControleNb($eleveid,$notetype,$classeEtab,$controleid,$matiereid,$teatcherid,$libellesessionencours,$codeEtab)
{
  $req = $this->db->prepare("SELECT * from eleve,enseignant,classe,matiere,notes,controle where
    notes.ideleve_notes=eleve.idcompte_eleve and notes.idprof_notes=enseignant.idcompte_enseignant and
     notes.idclasse_notes=classe.id_classe and notes.idmat_notes=matiere.id_mat and notes.idtype_notes=controle.id_ctrl and notes.ideleve_notes=? and notes.type_notes=?
     and notes.idclasse_notes=? and notes.idtype_notes=? and notes.idmat_notes=? and notes.idprof_notes=? and notes.session_notes=? and notes.codeEtab_notes=?");

  $req->execute([$eleveid,$notetype,$classeEtab,$controleid,$matiereid,$teatcherid,$libellesessionencours,$codeEtab]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;

}

function getNotesAndObservControle($eleveid,$notetype,$classeEtab,$controleid,$matiereid,$teatcherid,$libellesessionencours,$codeEtab)
{
  $req = $this->db->prepare("SELECT * from eleve,enseignant,classe,matiere,notes,controle where
    notes.ideleve_notes=eleve.idcompte_eleve and notes.idprof_notes=enseignant.idcompte_enseignant and
     notes.idclasse_notes=classe.id_classe and notes.idmat_notes=matiere.id_mat and notes.idtype_notes=controle.id_ctrl and notes.ideleve_notes=? and notes.type_notes=?
     and notes.idclasse_notes=? and notes.idtype_notes=? and notes.idmat_notes=? and notes.idprof_notes=? and notes.session_notes=? and notes.codeEtab_notes=?");

  $req->execute([$eleveid,$notetype,$classeEtab,$controleid,$matiereid,$teatcherid,$libellesessionencours,$codeEtab]);
  $data=$req->fetchAll();

  $array=json_encode($data,true);

  $someArray = json_decode($array, true);

  $donnees=$someArray[0]["valeur_notes"]."*".$someArray[0]["obser_notes"];

  return $donnees;

  // return $data;

}


function getNotesAndObservExamNb($eleveid,$notetype,$classeEtab,$examid,$matiereid,$teatcherid,$libellesessionencours,$codeEtab)
{
  $req = $this->db->prepare("SELECT * from eleve,enseignant,classe,matiere,notes,examen where notes.ideleve_notes=eleve.idcompte_eleve and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and examen.id_exam=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and eleve.idcompte_eleve=? and notes.type_notes=? and  classe.id_classe=? and examen.id_exam=? and matiere.id_mat=? and enseignant.idcompte_enseignant=? and examen.session_exam  and classe.codeEtab_classe=? ");
  $req->execute([$eleveid,$notetype,$classeEtab,$examid,$matiereid,$teatcherid,$libellesessionencours,$codeEtab]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getNotesAndObservExam($eleveid,$notetype,$classeEtab,$examid,$matiereid,$teatcherid,$libellesessionencours,$codeEtab)
{
  $req = $this->db->prepare("SELECT * from eleve,enseignant,classe,matiere,notes,examen where notes.ideleve_notes=eleve.idcompte_eleve and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and examen.id_exam=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and eleve.idcompte_eleve=? and notes.type_notes=? and  classe.id_classe=? and examen.id_exam=? and matiere.id_mat=? and enseignant.idcompte_enseignant=? and examen.session_exam  and classe.codeEtab_classe=? ");
  $req->execute([$eleveid,$notetype,$classeEtab,$examid,$matiereid,$teatcherid,$libellesessionencours,$codeEtab]);
  $data=$req->fetchAll();

  return $data;

  // $array=json_encode($data,true);
  //
  // $someArray = json_decode($array, true);
  //
  // $donnees=$someArray[0]["valeur_notes"]."*".$someArray[0]["obser_notes"];
  //
  // return $donnees;
}



  function DeleteStudent($compte,$statut,$classe,$codeEtab)
  {
    $req = $this->db->prepare("UPDATE compte SET statut_compte=? where id_compte=? ");
    $req->execute([
      $statut,
      $compte

    ]);

    $_SESSION['user']['updateteaok']="Suppression effectuée avec succès";

    if($_SESSION['user']['profile'] == "Admin_globale")

    {

  header("Location:../manager/showInfosclasse.php?classe=".$classe."&codeEtab=".$codeEtab);

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {

  header("Location:../locale/classinfos.php?classe=".$classe);

    }

  }

  function UpdateStudentInfoWithPassword($nomad,$prenomad,$datenaisad,$lieunais,$sexe,$contactad,$emailad,$loginad,$codeEtab,$idcompte,$passad)
  {
    $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,login_compte=?,pass_compte=? where id_compte=? ");

    $req->execute([
      $nomad,
      $prenomad,
      $datenaisad,
      $contactad,
      $emailad,
      $loginad,
      $passad,
      $idcompte
    ]);

    //mise a jour dans la table Eleve

    $req1 = $this->db->prepare("UPDATE eleve SET nom_eleve=?,prenom_eleve=?,datenais_eleve=?,lieunais_eleve=?,sexe_eleve=?,email_eleve=?,login_eleve=?  where codeEtab_eleve=? and idcompte_eleve=?");
    $req1->execute([
    $nomad,
    $prenomad,
    $datenaisad,
    $lieunais,
    $sexe,
    $emailad,
    $loginad,
    $codeEtab,
    $idcompte
    ]);
      $_SESSION['user']['updateroutineok']="Modification effectuée avec succès";

      if($_SESSION['user']['profile'] == "Admin_globale")

      {

    header("Location:../manager/updatestudent.php?compte=".$idcompte);

      }else if($_SESSION['user']['profile'] == "Admin_locale")

      {

    header("Location:../locale/updatestudent.php?compte=".$idcompte);

      }
  }

  function UpdateStudentInfoWithoutPassword($nomad,$prenomad,$datenaisad,$lieunais,$sexe,$contactad,$emailad,$loginad,$codeEtab,$idcompte)
  {
      //mis a jour dans la table $compte

      $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,login_compte=? where id_compte=? ");

      $req->execute([
        $nomad,
        $prenomad,
        $datenaisad,
        $contactad,
        $emailad,
        $loginad,
        $idcompte
      ]);

      //mise a jour dans la table Eleve

      $req1 = $this->db->prepare("UPDATE eleve SET nom_eleve=?,prenom_eleve=?,datenais_eleve=?,lieunais_eleve=?,sexe_eleve=?,email_eleve=?,login_eleve=?  where codeEtab_eleve=? and idcompte_eleve=?");
      $req1->execute([
      $nomad,
      $prenomad,
      $datenaisad,
      $lieunais,
      $sexe,
      $emailad,
      $loginad,
      $codeEtab,
      $idcompte
      ]);
        $_SESSION['user']['updateroutineok']="Modification effectuée avec succès";

        if($_SESSION['user']['profile'] == "Admin_globale")

        {

      header("Location:../manager/updatestudent.php?compte=".$idcompte);

        }else if($_SESSION['user']['profile'] == "Admin_locale")

        {

      header("Location:../locale/updatestudent.php?compte=".$idcompte);

        }

  }

function generateficheLocalpdf($compte,$codeEtab)
{

}


  function AbsenceParentMailler($emailparent,$EtabName,$nomEleve,$classeName,$libellematiere,$debutHours,$finHours,$codeEtab,$imageEtab)

  {

    // session_start();

    require_once('../PHPMailer/class.phpmailer.php');

    require_once('../PHPMailer/class.smtp.php');

    require_once('../controller/functions.php');

    // $logoetab="../logo_etab/".$codeEtab."/".$imageEtab;

    $day=date("d-m-Y");

$client1="justearmel04@gmail.com";
$client2="fabienekoute@gmail.com";
    $mail = new PHPMailer();

$mail->isHTML(true);

 $mail->CharSet="UTF-8";

 $mail->isSMTP();

 $mail->SMTPOptions = array (

        'ssl' => array(

            'verify_peer'  => false,

            'verify_peer_name'  => false,

            'allow_self_signed' => true));

 $mail->Host='mail.proximity-cm.com';

 $mail->SMTPAuth = true;

 $mail->Port = 25;

 $mail->SMTPSecure = "tls";

 $mail->Username = "xschool@proximity-cm.com";

 $mail->Password ="123psa@456";

 $mail->From='xschool@proximity-cm.com';


  $mail->FromName=$EtabName;

 $mail->AddAddress($emailparent);

 $mail->AddAddress($client1);
$mail->AddAddress($client2);

 $mail->Subject = 'Abscence au cours';

 $mail->Body = "Hello Chers Parent<br>";

 $mail->Body .="La direction de l'établissement ".$EtabName." tiens à vous informer de l'absence ";

 $mail->Body .="de votre enfant en la personne de ".$nomEleve." au cours de ".$libellematiere." initialement prévu à ".$debutHours;

 $mail->Body .=" pour prendre fin à ".$finHours." ce jour ".$day."<br>";

 $mail->Body .="Cordialement<br>";

 $mail->Body .="<br>";

$mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";

$mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$imageEtab,"mon_logo");

if(!$mail->Send())

{

   //echo $mail->ErrorInfo;

   //Affiche le message d'erreur (ATTENTION:voir section 7)

$msg="nok";

}

else

{

$msg="ok";

}

return $msg;
//return $mail->Body;

// $mail->AddEmbeddedImage('../logo_etab/assets/images/adwowilogo1.png','mon_logo');

  }



  function getEmailParentOfThisStudent($matricule)

  {

      $req = $this->db->prepare("SELECT * from eleve,parent,inscription,classe,etablissement where classe.codeEtab_classe=etablissement.code_etab and classe.codeEtab_classe=etablissement.code_etab and  inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=eleve.idcompte_eleve and eleve.idparent_eleve=parent.idcompte_parent and eleve.matricule_eleve=?");

      $req->execute([$matricule]);

      $data=$req->fetchAll();

      $array=json_encode($data,true);

      $someArray = json_decode($array, true);

      $donnees=$someArray[0]["email_parent"]."*".$someArray[0]["nom_eleve"]."*".$someArray[0]["prenom_eleve"]."*".$someArray[0]["libelle_classe"]."*".$someArray[0]["libelle_etab"]."*".$someArray[0]["logo_etab"];

      return $donnees;

  }



  function getNumberOfExamNoteOfStudent($compte)

  {

    $req = $this->db->prepare("SELECT * from notes,examen,eleve where notes.ideleve_notes=eleve.idcompte_eleve and notes.idtype_notes=examen.id_exam and notes.type_notes=2 and eleve.idcompte_eleve=? ");

    $req->execute([$compte]);

    $data=$req->fetchAll();

    $nb=count($data);

    return $nb;

  }



  function getExameNotesOfStudent($compte)

  {

    $req = $this->db->prepare("SELECT * from notes,examen,eleve where notes.ideleve_notes=eleve.idcompte_eleve and notes.idtype_notes=examen.id_exam and notes.type_notes=2 and eleve.idcompte_eleve=? ");

    $req->execute([$compte]);

    return $req->fetchAll();



  }



  function getNumberOfControleNoteOfStudent($compte)

  {

    $req = $this->db->prepare("SELECT * from notes,controle,eleve where notes.ideleve_notes=eleve.idcompte_eleve and notes.idtype_notes=controle.id_ctrl and notes.type_notes=1 and eleve.idcompte_eleve=? ");

    $req->execute([$compte]);

    $data=$req->fetchAll();

    $nb=count($data);

    return $nb;

  }



   function getControleNotesOfStudent($compte)

  {

    $req = $this->db->prepare("SELECT * from notes,controle,eleve where notes.ideleve_notes=eleve.idcompte_eleve and notes.idtype_notes=controle.id_ctrl and notes.type_notes=1 and eleve.idcompte_eleve=? ");

    $req->execute([$compte]);

    return $req->fetchAll();



  }



  function UpdateControleNotes($notes,$observation,$ideleve,$typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$codeEtab)

  {

    $req = $this->db->prepare("UPDATE notes SET valeur_notes=?,obser_notes=? where notes.ideleve_notes=? and notes.	type_notes=? and notes.idtype_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and notes.idprof_notes=? and notes.codeEtab_notes=?");

    $req->execute([

    $notes,$observation,$ideleve,$typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$codeEtab

    ]);



    $_SESSION['user']['addattendailyok']="Notes modifier avec succès";



    if($_SESSION['user']['profile'] == "Admin_globale")

    {
// ?codeEtab=C00123
  header("Location:../manager/notes.php?codeEtab=".$codeEtab);

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {

  header("Location:../locale/notes.php");

    }

    else if($_SESSION['user']['profile'] == "Teatcher")

    {

  header("Location:../teatcher/notes.php");

    }



  }



  function UpdateNotesOfStudent($noteid,$codeEtab,$studentid,$classeid,$note,$observation)

  {

    $reqX = $this->db->prepare("UPDATE notes SET valeur_notes=?,obser_notes=? where id_notes=? and codeEtab_notes=? and ideleve_notes=? and idclasse_notes=?");

    $reqX->execute([

    $note,$observation,$noteid,$codeEtab,$studentid,$classeid

    ]);



    $_SESSION['user']['addattendailyok']="Notes modifier avec succès";



    if($_SESSION['user']['profile'] == "Admin_globale")

    {
header("Location:../manager/notes.php?codeEtab=".$codeEtab);
  //header("Location:../manager/notes.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {

  header("Location:../locale/notes.php");

}else if($_SESSION['user']['profile'] == "Teatcher")

    {

  header("Location:../teatcher/notes.php");

    }





  }

  function getAllstudentNotesExamofthisclasses($classe,$examid,$codeEtabAssigner)

  {

    $req = $this->db->prepare("SELECT * from eleve,enseignant,classe,matiere,notes,examen where notes.ideleve_notes=eleve.idcompte_eleve and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and examen.id_exam=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and  classe.id_classe=? and examen.id_exam=?  and classe.codeEtab_classe=? and notes.type_notes=2");

    $req->execute([$classe,$examid,$codeEtabAssigner]);

    return $req->fetchAll();

  }

  function getInformationsOfNotesStudent($noteid,$typenote,$ideleve,$codeEtabAssigner)

  {

    $req = $this->db->prepare("SELECT * from eleve,compte,notes,classe where eleve.idcompte_eleve=compte.id_compte and notes.ideleve_notes=eleve.idcompte_eleve and notes.idclasse_notes=classe.id_classe and notes.id_notes=? and notes.type_notes=? and eleve.idcompte_eleve=? and notes.codeEtab_notes=?");

    $req->execute([$noteid,$typenote,$ideleve,$codeEtabAssigner]);

    $data=$req->fetchAll();

    $array=json_encode($data,true);

    $someArray = json_decode($array, true);

    $donnees=$someArray[0]["matricule_eleve"]."*".$someArray[0]["nom_eleve"]."*".$someArray[0]["prenom_eleve"]."*".$someArray[0]["libelle_classe"]."*".$someArray[0]["id_classe"]."*".$someArray[0]["valeur_notes"];

    $donnees.="*".$someArray[0]["obser_notes"]."*".$someArray[0]["codeEtab_notes"]."*".$someArray[0]["id_notes"]."*".$someArray[0]["idtype_notes"]."*".$someArray[0]["idcompte_eleve"];

    return $donnees;



  }



  function getControleInfos($controleid)

  {

  $req = $this->db->prepare("SELECT * from controle where id_ctrl=?");

  $req->execute([$controleid]);

  $data=$req->fetchAll();

  $array=json_encode($data,true);

  $someArray = json_decode($array, true);

  $donnees=$someArray[0]["libelle_ctrl"];

  return $donnees;



  }



  function getAllstudentNotesofthisclasses($classe,$matiereid,$controleid,$teatcherid,$codeEtabAssigner)

  {

    $req = $this->db->prepare("SELECT * from eleve,enseignant,classe,matiere,notes,controle where notes.ideleve_notes=eleve.idcompte_eleve and notes.idprof_notes=enseignant.idcompte_enseignant and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and controle.id_ctrl=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and classe.id_classe=?  and matiere.id_mat=? and controle.id_ctrl=? and enseignant.idcompte_enseignant=? and classe.codeEtab_classe=? and notes.type_notes=1  ");

    //$req.=."and controle.id_ctrl=?");

    $req->execute([$classe,$matiereid,$controleid,$teatcherid,$codeEtabAssigner]);

    return $req->fetchAll();

  }



  function AddNotesExamen($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$idstudent,$codeEtab,$notes,$observation,$session)

  {

    $req = $this->db->prepare("INSERT INTO  notes SET type_notes=?,idtype_notes=?,idclasse_notes=?,idmat_notes=?,idprof_notes=?,ideleve_notes=?,codeEtab_notes=?,valeur_notes=?,obser_notes=?,session_notes=?");

    $req->execute([

      $typenote,

      $idtypenote,

      $classeId,

      $matiereid,

      $teatcherid,

      $idstudent,

      $codeEtab,

      $notes,

      $observation,
      $session

    ]);



    //nous allons mettre le statut du controle à 1 pour dire que notes ajouter



//   $statut=1;

//

//     $reqX = $this->db->prepare("UPDATE controle SET statut_ctrl=? where id_ctrl=? and codeEtab_ctrl=?");

//     $reqX->execute([

//       $statut,$idtypenote,$codeEtab

// ]);





    $_SESSION['user']['addattendailyok']="Notes de classe ajouter avec succès";



    if($_SESSION['user']['profile'] == "Admin_globale")

    {

  header("Location:../manager/notes.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {

  header("Location:../locale/notes.php");

}else if($_SESSION['user']['profile'] == "Teatcher")

    {

  header("Location:../teatcher/notes.php");

    }

  }



  function AddNotesControle($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$idstudent,$codeEtab,$notes,$observation,$session)

  {

    $req = $this->db->prepare("INSERT INTO  notes SET type_notes=?,idtype_notes=?,idclasse_notes=?,idmat_notes=?,idprof_notes=?,ideleve_notes=?,codeEtab_notes=?,valeur_notes=?,obser_notes=?,session_notes=?");

    $req->execute([

      $typenote,

      $idtypenote,

      $classeId,

      $matiereid,

      $teatcherid,

      $idstudent,

      $codeEtab,

      $notes,

      $observation,
      $session

    ]);



    //nous allons mettre le statut du controle à 1 pour dire que notes ajouter



  $statut=1;



    $reqX = $this->db->prepare("UPDATE controle SET statut_ctrl=? where id_ctrl=? and codeEtab_ctrl=?");

    $reqX->execute([

      $statut,$idtypenote,$codeEtab

]);





    $_SESSION['user']['addattendailyok']="Notes de classe ajouter avec succès";



    if($_SESSION['user']['profile'] == "Admin_globale")

    {

  header("Location:../manager/notes.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {

  header("Location:../locale/notes.php");

}else if($_SESSION['user']['profile'] == "Teatcher")

    {

  header("Location:../teatcher/notes.php");

    }

  }



  function getTheSpecificStudentForSchool($compteEtab,$classeId,$idstudent)

  {

        $req = $this->db->prepare("SELECT * from eleve,compte,inscription,classe,parent where compte.email_compte=eleve.email_eleve and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and parent.idcompte_parent=eleve.idparent_eleve and classe.codeEtab_classe=? and classe.id_classe=? and compte.id_compte=?");

        $req->execute([$compteEtab,$classeId,$idstudent]);

        return $req->fetchAll();

  }



   function getAllStudentOfThisSchool($compte)

{

  $req = $this->db->prepare("SELECT * from eleve,compte,inscription,classe,parent where eleve.idparent_eleve=parent.idcompte_parent and  compte.email_compte=eleve.email_eleve and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and classe.codeEtab_classe=?");

  $req->execute([$compte]);

  return $req->fetchAll();

}



  function getNbAttendance($matricule,$date,$classeId)

  {

    $req = $this->db->prepare("SELECT statut_presence FROM presences where matricule_presence=? and classe_presence=? and date_presence=? ");

    $req->execute([$matricule,$classeId,$date]);

    $data=$req->fetchAll();

    $nb=count($data);

    return $nb;

  }



  function getstatutAttendance($matricule,$date,$classeId)

  {

      $req = $this->db->prepare("SELECT * FROM presences where matricule_presence=? and classe_presence=? and date_presence=? ");

      $req->execute([$matricule,$classeId,$date]);

      return $req->fetchAll();

      //$data=$req->fetchAll();

      /*$array=json_encode($data,true);

      $someArray = json_decode($array, true);

      $donnees=$someArray[0]["statut_presence"]."*".$someArray[0]["id_presence"];*/

  }

  function getAllstudentofthisclassesSession($classe,$session)
  {

    // $years=date('Y');
    //
    // $nextyears=date("Y")+1;
    //
    // $session=$years."-".$nextyears;

    $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription where eleve.idcompte_eleve=compte.id_compte and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=? and classe.id_classe=? and compte.statut_compte!=0 order by eleve.nom_eleve ASC");

    $req->execute([$session,$classe]);

    return $req->fetchAll();

  }



  function getAllstudentofthisclasses($classe)

  {

    $years=date('Y');

    $nextyears=date("Y")+1;

    $session=$years."-".$nextyears;

    $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription where eleve.idcompte_eleve=compte.id_compte and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=? and classe.id_classe=? order by eleve.nom_eleve ASC");

    $req->execute([$session,$classe]);

    return $req->fetchAll();

  }



  function getAllInformationsOfStudent($compte)

  {

      $years=date('Y');

      $nextyears=date("Y")+1;

      $session=$years."-".$nextyears;

      $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription where eleve.email_eleve=compte.email_compte and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=? and compte.id_compte=?");

      $req->execute([$session,$compte]);

      $data=$req->fetchAll();

      $array=json_encode($data,true);

      $someArray = json_decode($array, true);



      $donnees=$someArray[0]["id_compte"]."*".$someArray[0]["matricule_eleve"]."*".$someArray[0]["nom_eleve"]."*".$someArray[0]["prenom_eleve"]."*".$someArray[0]["datenais_eleve"]."*".$someArray[0]["lieunais_eleve"];

      $donnees.="*".$someArray[0]["sexe_eleve"]."*".$someArray[0]["email_eleve"]."*".$someArray[0]["idparent_eleve"]."*".$someArray[0]["libelle_classe"]."*".$someArray[0]["codeEtab_classe"]."*".$someArray[0]["photo_compte"];

      $donnees.="*".$someArray[0]["tel_compte"]."*".$someArray[0]["login_compte"]."*".$someArray[0]["codeEtab_inscrip"]."*".$someArray[0]["id_classe"];

      return $donnees;

  }



  function getAllStudentOfClassesId($classe,$session)

  {



    //  $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription where compte.email_compte=eleve.email_eleve and classe.id_classe=inscription.idclasse_inscrip and compte.id_compte=inscription.ideleve_inscrip and classe.id_classe=? and inscription.session_inscrip=? and compte.statut_compte!=0 ");

    $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription where inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=compte.id_compte and compte.id_compte=eleve.idcompte_eleve and inscription.session_inscrip=classe.session_classe and classe.id_classe=? and inscription.session_inscrip=? ");

      $req->execute([$classe,$session]);

      return $req->fetchAll();



  }



  function getStudentIdcompteByMatricule($matri)

  {

      $req = $this->db->prepare("SELECT * FROM eleve,compte where compte.email_compte=eleve.email_eleve and eleve.matricule_eleve=?");

      $req->execute([$matri]);

      $data=$req->fetchAll();

      $array=json_encode($data,true);

      $someArray = json_decode($array, true);



      $donnees=$someArray[0]["id_compte"];



      return $donnees;

  }



  function AddAttendance($newdate,$libellemois,$annee,$classeId,$matricule,$statut,$num,$matiereid,$profid,$codeEtab,$session)

  {

    $req = $this->db->prepare("INSERT INTO  presences SET date_presence=?,mois_presence=?,annee_presence=?,classe_presence=?,matricule_presence=?,statut_presence=?,nbjoursmois=?,matiere_presence=?,teatcher_presence=?,codeEtab_presence=?,session_presence=?");

    $req->execute([$newdate,$libellemois,$annee,$classeId,$matricule,$statut,$num,$matiereid,$profid,$codeEtab,$session]);

  }



  function AddInscription($classeEtab,$idstudentcpte,$session,$datecrea)

  {

      $req = $this->db->prepare("INSERT INTO  inscription SET idclasse_inscrip=?,ideleve_inscrip=?,session_inscrip=?,date_inscrip=?,codeEtab_inscrip=?");

      $req->execute([$classeEtab,$idstudentcpte,$session,$datecrea,$codeEtab]);



      $_SESSION['user']['addStudok']="Inscription à bien été pris en compte dans le système avec succès";



      if($_SESSION['user']['profile'] == "Admin_globale")

      {

    header("Location:../manager/admission.php");

      }else if($_SESSION['user']['profile'] == "Admin_locale")

      {

    header("Location:../locale/admission.php");

      }





  }



  function ExisteStudent($matricule)

  {



    //$req = $this->db->prepare("SELECT * FROM eleve,compte,inscription where eleve.email_eleve=compte.email_compte and compte.id_compte=inscription.ideleve_inscrip and eleve.matricule_eleve=? and inscription.idclasse_inscrip=? and eleve.idparent_eleve=? and inscription.session_inscrip=?");

    $req = $this->db->prepare("SELECT * FROM eleve where eleve.matricule_eleve=?");

    //$req->execute([$matricule,$classe,$parent,$session]);

    $req->execute([$matricule]);

    $data=$req->fetchAll();

    $nb=count($data);

    return $nb;

  }



  function presencesExistByClassesAndDate($classe,$newdate)

  {

    $req = $this->db->prepare("SELECT * FROM presences where classe_presence=? and date_presence=?");

    $req->execute([$classe,$newdate]);

    $data=$req->fetchAll();

    $nb=count($data);

    return $nb;

  }



  function InscriptionAllReady($matricule,$classe,$parent,$session)

  {

    $req = $this->db->prepare("SELECT * FROM eleve,compte,inscription where eleve.email_eleve=compte.email_compte and compte.id_compte=inscription.ideleve_inscrip and eleve.matricule_eleve=? and inscription.idclasse_inscrip=? and eleve.idparent_eleve=? and inscription.session_inscrip=?");

    $req->execute([$matricule,$classe,$parent,$session]);

    $data=$req->fetchAll();

    $nb=count($data);

    return $nb;

  }



  function AddStudentwithFile($matri,$nomad,$prenomad,$parenta,$classeEtab,$datenaisad,$sexe,$contactad,$emailad,$loginad,$passad,$codeEtab,$datecrea,$type_cpte,$statut,$fichierad,$lieunais,$session)

  {

    //ajout dans la table eleve







    //ajout dans la table compte



    $fonction="Eleve";



    $reqX = $this->db->prepare("INSERT INTO  compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=?,login_compte=?,pass_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?,photo_compte=?");

    $reqX->execute([

    $nomad,

    $prenomad,

    $datenaisad,

    $contactad,

    $emailad,

    $fonction,

    $loginad,

    $passad,

    $type_cpte,

    $statut,

    $datecrea,

    $fichierad,



    ]);



    // ajout des données dans la table inscription

    $idlastcompte=$this->db->lastInsertId();



    $req = $this->db->prepare("INSERT INTO  eleve SET matricule_eleve=?,nom_eleve=?,prenom_eleve=?,datenais_eleve=?,lieunais_eleve=?,sexe_eleve=?,email_eleve=?,login_eleve=?,idparent_eleve=?,codeEtab_eleve=?,idcompte_eleve=?");

    $req->execute([

    $matri,
    $nomad,
    $prenomad,
    $datenaisad,
    $lieunais,
    $sexe,
    $emailad,
    $loginad,
    $parenta,
    $codeEtab,
    $idlastcompte

    ]);



    $reqY = $this->db->prepare("INSERT INTO  inscription SET 	idclasse_inscrip=?,	ideleve_inscrip=?,session_inscrip=?,date_inscrip=?,codeEtab_inscrip=?");

    $reqY->execute([

      $classeEtab,
      $idlastcompte,
      $session,
      $datecrea,
      $codeEtab

    ]);



    $_SESSION['user']['addStudok']="Inscription à bien été pris en compte dans le système avec succès";



    if($_SESSION['user']['profile'] == "Admin_globale")

    {

  header("Location:../manager/admission.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {

  header("Location:../locale/admission.php");

    }









  }



  function AddStudentwithoutFile($matri,$nomad,$prenomad,$parenta,$classeEtab,$datenaisad,$sexe,$contactad,$emailad,$loginad,$passad,$codeEtab,$datecrea,$type_cpte,$statut,$lieunais,$session)

  {

    //ajout dans la table eleve







    //ajout dans la table compte



    $fonction="Eleve";



    $reqX = $this->db->prepare("INSERT INTO  compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=?,login_compte=?,pass_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?");

    $reqX->execute([

    $nomad,

    $prenomad,

    $datenaisad,

    $contactad,

    $emailad,

    $fonction,

    $loginad,

    $passad,

    $type_cpte,

    $statut,

    $datecrea

    ]);



    // ajout des données dans la table inscription

    $idlastcompte=$this->db->lastInsertId();



    $req = $this->db->prepare("INSERT INTO  eleve SET matricule_eleve=?,nom_eleve=?,prenom_eleve=?,datenais_eleve=?,lieunais_eleve=?,sexe_eleve=?,email_eleve=?,login_eleve=?,idparent_eleve=?,codeEtab_eleve=?,idcompte_eleve=?");

    $req->execute([

    $matri,

    $nomad,

    $prenomad,

    $datenaisad,

    $lieunais,

    $sexe,

    $emailad,

    $loginad,

    $parenta,

    $codeEtab,

    $idlastcompte

    ]);



    $reqY = $this->db->prepare("INSERT INTO  inscription SET 	idclasse_inscrip=?,	ideleve_inscrip=?,session_inscrip=?,date_inscrip=?,codeEtab_inscrip=?");

    $reqY->execute([



      $classeEtab,
      $idlastcompte,
      $session,
      $datecrea,
      $codeEtab

    ]);



    $_SESSION['user']['addStudok']="Inscription à bien été pris en compte dans le système avec succès";



    if($_SESSION['user']['profile'] == "Admin_globale")

    {

  header("Location:../manager/admission.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {

  header("Location:../locale/admission.php");

    }

  }



}



 ?>
