<?php

class Classe{

public $db;
function __construct() {

  require_once('../class/cnx.php');

  $db = new mysqlConnector();
  $this->db= $db->dataBase;
}

function getAllStudentOfParentSchool($code,$classeEtab,$parentid)
{
    $session="2019-2020";
    $req = $this->db->prepare("SELECT * from inscription,eleve,classe where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and classe.codeEtab_classe=? and classe.id_classe=? and eleve.idparent_eleve=? and inscription.session_inscrip=? ");
    $req->execute([$code,$classeEtab,$parentid,$session]);
    return $req->fetchAll();
}

function getAllClassesOfParentHadStudent($code,$parentid)
{
  $session="2019-2020";
  $req = $this->db->prepare("SELECT  distinct 	libelle_classe,id_classe FROM etablissement,classe,eleve,inscription,parent where parent.idcompte_parent=eleve.idparent_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and classe.codeEtab_classe=etablissement.code_etab and  inscription.session_inscrip=? and parent.idcompte_parent=? and etablissement.code_etab=? ");
  $req->execute([$session,$parentid,$code]);
  return $req->fetchAll();
}

function getAllclassesT()
{
  return "bonjour";
}

function getClassesAndMatiereOfThisTeatcherSchool($etabEnseigner,$idcpteTeatcher)
{
  $req = $this->db->prepare("SELECT * from classe,matiere,enseignant,dispenser where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and dispenser.codeEtab=? and dispenser.id_enseignant=? ");
  $req->execute([$etabEnseigner,$idcpteTeatcher]);
  return $req->fetchAll();

}

function getCodeEtabOfClassesByClasseId($id_classe)
{
  $req = $this->db->prepare("SELECT * from classe where id_classe=? ");
  $req->execute([$id_classe]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);

  $donnees=$someArray[0]["codeEtab_classe"];
  return $donnees;
}

function getControleInfosByIdCtrl($idctrl,$classe,$codeEtabAssigner)
{
  $req = $this->db->prepare("SELECT * from controle where id_ctrl=? and classe_ctrl=? and codeEtab_ctrl=?");
  $req->execute([$idctrl,$classe,$codeEtabAssigner]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);

  $donnees=$someArray[0]["libelle_ctrl"];
  return $donnees;
}

function getExamInfosByIdExam($idexam,$codeEtabAssigner)
{
  $req = $this->db->prepare("SELECT * from examen where id_exam=? and codeEtab_exam=?");
  $req->execute([$idexam,$codeEtabAssigner]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);

  $donnees=$someArray[0]["libelle_exam"];
  return $donnees;
}

function getNumberOfClassesOfThisClasses($codeEtab)
{
  $req = $this->db->prepare("SELECT * from classe where codeEtab_classe=?");
  $req->execute([$codeEtab]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getAllStudentOfThisClasses($classeId)
{
    $req = $this->db->prepare("SELECT * from classe,eleve,compte,inscription where compte.email_compte=eleve.email_eleve and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and classe.id_classe=? order by eleve.nom_eleve ASC");
    $req->execute([$classeId]);
    return $req->fetchAll();
}

function getAllClassesOfThisSchool($compte)
{
  $req = $this->db->prepare("SELECT * from classe where codeEtab_classe=? order by datecrea_classe DESC");
  $req->execute([$compte]);
  return $req->fetchAll();
}


function getNumberClassesOfTeatcherId($idcompte)
{
  $req = $this->db->prepare("SELECT * FROM classe,enseigner,compte WHERE compte.id_compte=enseigner.id_enseignant and enseigner.id_classe=classe.id_classe and compte.id_compte=? ");
  $req->execute([$idcompte]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getClassesNumberOfTeatcherId($idcompte,$session)
{
  $req = $this->db->prepare("SELECT * FROM classe,dispenser,compte WHERE compte.id_compte=dispenser.id_enseignant and dispenser.idclasse_disp=classe.id_classe and compte.id_compte=? and dispenser.session_disp=?");
  $req->execute([$idcompte,$session]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getClassesOfTeatcherId($idcompte)
{
  $req = $this->db->prepare("SELECT distinct classe.id_classe,classe.libelle_classe FROM classe,dispenser,compte WHERE compte.id_compte=dispenser.id_enseignant and dispenser.idclasse_disp=classe.id_classe and compte.id_compte=? order by classe.id_classe ASC");
  $req->execute([$idcompte]);
  return $req->fetchAll();
}

function getTeatcherClassesId($idcompte)
{
  $req = $this->db->prepare("SELECT distinct id_classe,codeEtab,libelle_classe  FROM classe,dispenser,compte WHERE compte.id_compte=dispenser.id_enseignant and dispenser.idclasse_disp=classe.id_classe and compte.id_compte=?");
  $req->execute([$idcompte]);
  return $req->fetchAll();
}

function ExisteClasses($libetab,$classe)
  {
    $req = $this->db->prepare("SELECT * FROM classe where libelle_classe=? and codeEtab_classe=?");
    $req->execute([$classe,$libetab]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;
  }

  function suppressionClasse($idclasse)
  {
    $req = $this->db->prepare("DELETE FROM matiere where classe_mat=?");
    $req->execute([$idclasse]);

    $reqX = $this->db->prepare("DELETE FROM classe where 	id_classe=?");
    $reqX->execute([$idclasse]);

    $_SESSION['user']['delclasseok']="Classe supprimé avec succès";

    if($_SESSION['user']['profile'] == "Admin_globale") {
  header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
    // header("Location:../manager/addclasses.php");
      }else if($_SESSION['user']['profile'] == "Admin_locale") {

    header("Location:../locale/classes.php");

        }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/index.php");

          }else if($_SESSION['user']['profile'] == "Student") {

                header("Location:../student/index.php");

            }else if($_SESSION['user']['profile'] == "Parent") {

                  header("Location:../parent/index.php");

              }


  }

  function Addclasse($libelleclasse,$codeEtab,$libellesession)
  {
    $dateday=date("Y-m-d");
    $statut=1;
    $req = $this->db->prepare("INSERT INTO classe SET libelle_classe=?,codeEtab_classe=?,datecrea_classe=?,statut_classe=?,session_classe=?");
    $req->execute([$libelleclasse,$codeEtab,$dateday,$statut,$libellesession]);

    $_SESSION['user']['addclasseok']="Classe ajouté avec succès";

    if($_SESSION['user']['profile'] == "Admin_globale") {
  header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
    // header("Location:../manager/addclasses.php");
      }else if($_SESSION['user']['profile'] == "Admin_locale") {

    header("Location:../locale/classes.php");

        }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/index.php");

          }else if($_SESSION['user']['profile'] == "Student") {

                header("Location:../student/index.php");

            }else if($_SESSION['user']['profile'] == "Parent") {

                  header("Location:../parent/index.php");

              }


    // header("Location:../manager/addclasses.php");

  }

  function getAllclassesOfassignated($idlocal)
  {
    $req = $this->db->prepare("SELECT * FROM classe,assigner,compte where classe.codeEtab_classe=assigner.codeEtab_assign and assigner.id_adLocal=compte.id_compte and compte.id_compte=? ");
    $req->execute([$idlocal]);
    return $req->fetchAll();
  }

  function getAllclasses()
  {
    $req = $this->db->prepare("SELECT * FROM classe order by datecrea_classe DESC");
    $req->execute([]);
    return $req->fetchAll();

  }

  function getAllClassesbyschoolCode($code)
  {
        $req = $this->db->prepare("SELECT * FROM classe,etablissement WHERE classe.codeEtab_classe=etablissement.code_etab and etablissement.code_etab=? ");
        $req->execute([$code]);
        return $req->fetchAll();
  }

  function getClassesByschoolCodewithId($codeetab,$classex)
  {
    $req = $this->db->prepare("SELECT * FROM classe,etablissement WHERE classe.codeEtab_classe=etablissement.code_etab and etablissement.code_etab=? and 	id_classe=?");
    $req->execute([$codeetab,$classex]);
    return $req->fetchAll();
  }

  function getInfosofclassesbyId($classe)
  {
    $req = $this->db->prepare("SELECT * FROM classe where  classe.id_classe=?");
    $req->execute([$classe]);
    $data=$req->fetchAll();
    $array=json_encode($data,true);
    $someArray = json_decode($array, true);

    $donnees=$someArray[0]["libelle_classe"];
    return $donnees;
  }

  function Updateclasses($classe,$codeEtab,$idclasse)
  {
    $req = $this->db->prepare("UPDATE classe set 	libelle_classe=? where  classe.id_classe=? and classe.codeEtab_classe=?");
    $req->execute([$classe,$idclasse,$codeEtab]);

    $_SESSION['user']['updateclasseok']="Classe modifié avec succès";

    if($_SESSION['user']['profile'] == "Admin_globale") {
  header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
    // header("Location:../manager/addclasses.php");
      }else if($_SESSION['user']['profile'] == "Admin_locale") {

    header("Location:../locale/classes.php");

        }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/index.php");

          }else if($_SESSION['user']['profile'] == "Student") {

                header("Location:../student/index.php");

            }else if($_SESSION['user']['profile'] == "Parent") {

                  header("Location:../parent/index.php");

              }


  }

  /*function getAllClassesByschoolCode($codeetab)
  {
      $req = $this->db->prepare("SELECT * FROM classe,etablissement WHERE classe.codeEtab_classe=etablissement.code_etab and etablissement.code_etab=?");
      $req->execute([$codeetab]);
      return $req->fetchAll();
  }*/

  function getAllClassesByClasseId($classex)
  {
    $req = $this->db->prepare("SELECT * FROM classe,etablissement WHERE classe.codeEtab_classe=etablissement.code_etab and id_classe=?");
    $req->execute([$classex]);
    return $req->fetchAll();
  }





}

 ?>
