<?php

class Sessionacade{

public $db;
function __construct() {

  require_once('../class/cnx.php');

  $db = new mysqlConnector();
  $this->db= $db->dataBase;
}

function UpdateSessionType($sessionid,$typesess,$status)
{
  $req = $this->db->prepare("UPDATE semestre SET 	statut_semes=? WHERE id_semes=? and idsess_semes=?");
  $req->execute([
  $status,$typesess,$sessionid
  ]);

$_SESSION['user']['Updateadminok']="Type session Clôturer avec succès";

if($_SESSION['user']['profile'] == "Admin_globale") {
header("Location:../manager/sessions.php");

   }else if($_SESSION['user']['profile'] == "Admin_locale") {

 header("Location:../locale/sessions.php");
}


}

  function getAllsemesterOfthisSession($sessionencoursid,$codeEtabAssigner)
  {
    $statutfinal=2;
    $req = $this->db->prepare("SELECT * from semestre where codeEtab_semes=? and idsess_semes=?");
    $req->execute([$codeEtabAssigner,$sessionencoursid]);
    return $req->fetchAll();
  }


function UpdateSessionSchool($sessionid,$sessionlib,$typesession,$codeEtab)
{
    $req = $this->db->prepare("UPDATE sessions SET libelle_sess=?,type_sess=? WHERE id_sess=? and codeEtab_sess=?");
    $req->execute([
      $sessionlib,
      $typesession,
      $sessionid,
      $codeEtab
    ]);

    //supprimer les semestres de cette session

    $req1= $this->db->prepare("DELETE FROM semestre where idsess_semes=? AND codeEtab_semes=?");
    $req1->execute([$sessionid,$codeEtab]);


}

function AddSemestre($libellesemestre,$idsession,$statut,$codeEtab)
{
  $req = $this->db->prepare("INSERT INTO semestre SET idsess_semes=?,libelle_semes=?,statut_semes=?,codeEtab_semes=?");
  $req->execute([$idsession,$libellesemestre,$statut,$codeEtab]);
   $_SESSION['user']['Updateadminok']="Nouvelle Session ajouté avec succès";
   if($_SESSION['user']['profile'] == "Admin_globale") {
  header("Location:../manager/sessions.php=");
    // header("Location:../manager/addclasses.php");
      }else if($_SESSION['user']['profile'] == "Admin_locale") {

    header("Location:../locale/sessions.php");
  }
}

function getAllSessionsOfThisSchoolCode($codeEtabAssigner)
{
  $req = $this->db->prepare("SELECT * from sessions where codeEtab_sess=?");
  $req->execute([$codeEtabAssigner]);
  return $req->fetchAll();
}

function ExisteSessionForThisSchool($titre,$codeEtab)
{
  $req = $this->db->prepare("SELECT * from sessions where libelle_sess=? and codeEtab_sess=?");
  $req->execute([$titre,$codeEtab]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function AddSessionSchool($libellesession,$codeEtab,$statut,$encours,$semestre)
{
  $req = $this->db->prepare("INSERT INTO sessions SET libelle_sess=?,codeEtab_sess=?,statut_sess=?,encours_sess=?,type_sess=?");
  $req->execute([$libellesession,$codeEtab,$statut,$encours,$semestre]);
  $idlastsession=$this->db->lastInsertId();
  return $idlastsession;

}

function getNumberOfSemestreDown($sessionid,$sessiontype,$codeEtab)
{
  $statutfinal=2;
  $req = $this->db->prepare("SELECT * from semestre where idsess_semes=? and codeEtab_semes=? and statut_semes=?");
  $req->execute([$sessionid,$codeEtab,$statutfinal]);
  $data=$req->fetchAll();
  $nb=count($data);
  $content="";
    //return $nb;
    if($sessiontype==2)
    {
      //il est question d'un semestre

      if($nb==2)
      {
        $content=1;
      }else if($nb<2)
      {
        $content=0;
      }

    }else if($sessiontype==3)
    {
      //il est question d'un trimestre

      if($nb==3)
      {
  $content=1;
      }else if($nb<3)
      {
  $content=0;
      }
    }

    return $content;
}

function getNbOfSemestreDown($sessionid,$sessiontype,$codeEtab)
{
  $statutfinal=2;
  $req = $this->db->prepare("SELECT * from semestre where idsess_semes=? and codeEtab_semes=? and statut_semes=?");
  $req->execute([$sessionid,$codeEtab,$statutfinal]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getNumberSessionEncoursOn($codeEtabAssigner)
{
  $encours=1;
  $req = $this->db->prepare("SELECT * from sessions where codeEtab_sess=? and encours_sess=?");
  $req->execute([$codeEtabAssigner,$encours]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getActiveAllSemestrebyIdsession($sessionencoursid)
{
  $statut=1;
  $req = $this->db->prepare("SELECT * from sessions,semestre where semestre.idsess_semes=sessions.id_sess and sessions.id_sess=? and semestre.statut_semes=?");
  $req->execute([$sessionencoursid,$statut]);
  return $req->fetchAll();
}

function getSessionEncours($codeEtabAssigner)
{
  $encours=1;
  $req = $this->db->prepare("SELECT * from sessions where codeEtab_sess=? and encours_sess=?");
  $req->execute([$codeEtabAssigner,$encours]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);

$donnees=$someArray[0]["libelle_sess"]."*".$someArray[0]["id_sess"]."*".$someArray[0]["type_sess"];
return $donnees;
}







}

 ?>
