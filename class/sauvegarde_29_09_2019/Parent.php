<?php
class ParentX{

public $db;
function __construct() {
  require_once('../class/cnx.php');

  $db = new mysqlConnector();
  $this->db= $db->dataBase;
}

function getParentOfStudentsSchools($codeEtab,$libellesessionencours)
{
    $req = $this->db->prepare("SELECT  distinct  compte.id_compte,compte.nom_compte,compte.prenom_compte,compte.tel_compte,compte.email_compte from compte,parent,enregistrer,eleve,inscription where parent.idcompte_parent=eleve.idparent_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.codeEtab_inscrip=enregistrer.codeEtab_enreg and   compte.id_compte=parent.idcompte_parent and compte.id_compte=enregistrer.idparent_enreg and enregistrer.codeEtab_enreg=? and inscription.session_inscrip=? ");
    $req->execute([$codeEtab,$libellesessionencours]);
    return $req->fetchAll();

}

function getAllstudentofthisclassesParentSchool($classeid,$codeEtab,$parentid)
{
  $session="2019-2020";
  $req = $this->db->prepare("SELECT * from eleve,classe,inscription where inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=eleve.idcompte_eleve and classe.id_classe=? and classe.codeEtab_classe=? and inscription.session_inscrip=? and eleve.idparent_eleve=?");
  $req->execute([$classeid,$codeEtab,$session,$parentid]);
  return $req->fetchAll();
}

function getstudentofthisclassesSchoolParent($classeid,$codeEtab,$student,$parentid)
{
  $session="2019-2020";
  $req = $this->db->prepare("SELECT * from eleve,classe,inscription where inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=eleve.idcompte_eleve and classe.id_classe=? and classe.codeEtab_classe=? and eleve.idcompte_eleve=? and inscription.session_inscrip=? and eleve.idparent_eleve=?");
  $req->execute([$classeid,$codeEtab,$student,$session,$parentid]);
  return $req->fetchAll();
}

function getTheSpecificMatiereForSchool($codeEtab,$classeid,$matiereid)
{
  $req = $this->db->prepare("SELECT * from programme,classe,matiere where programme.idclasse_prog=classe.id_classe and programme.idmat_prog=matiere.id_mat and programme.codeEtab_prog=? and programme.idclasse_prog=? and programme.idmat_prog=? ");
  $req->execute([$codeEtab,$classeid,$matiereid]);
  return $req->fetchAll();
}

function getAllMatiereOfThisClasseSchool($codeEtab,$classeid)
{
  $req = $this->db->prepare("SELECT * from programme,classe,matiere where programme.idclasse_prog=classe.id_classe and programme.idmat_prog=matiere.id_mat and programme.codeEtab_prog=? and programme.idclasse_prog=? ");
  $req->execute([$codeEtab,$classeid]);
  return $req->fetchAll();
}

function getAllTeatcherOfThisClasseSchool($codeEtab,$classeid)
{
  $req = $this->db->prepare("SELECT distinct compte.id_compte,compte.nom_compte,compte.prenom_compte,enseignant.nat_enseignant,compte.tel_compte,compte.email_compte FROM compte,enseignant,dispenser where compte.id_compte=enseignant.idcompte_enseignant and dispenser.id_enseignant=enseignant.idcompte_enseignant and dispenser.idclasse_disp=? and dispenser.codeEtab=? ");
  $req->execute([$classeid,$codeEtab]);
  return $req->fetchAll();
}

function getTheSpecificTeatcherForSchool($codeEtab,$classeid,$teatcherid)
{
  $req = $this->db->prepare("SELECT distinct compte.id_compte,compte.nom_compte,compte.prenom_compte,enseignant.nat_enseignant,compte.tel_compte,compte.email_compte FROM compte,enseignant,dispenser where compte.id_compte=enseignant.idcompte_enseignant and dispenser.id_enseignant=enseignant.idcompte_enseignant and dispenser.idclasse_disp=? and dispenser.codeEtab=? and compte.id_compte=?");
  $req->execute([$classeid,$codeEtab,$teatcherid]);
  return $req->fetchAll();
}

function getAlletabOfStudentParent($IdCompte)
{
  $session="2019-2020";
  $req = $this->db->prepare("SELECT  distinct 	code_etab,libelle_etab FROM etablissement,classe,eleve,inscription,parent where parent.idcompte_parent=eleve.idparent_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and classe.codeEtab_classe=etablissement.code_etab and inscription.session_inscrip=? and parent.idcompte_parent=? ");
  $req->execute([$session,$IdCompte]);
  return $req->fetchAll();
}

function getAllclassesOfStudentParent($IdCompte)
{
  $session="2019-2020";
  $req = $this->db->prepare("SELECT  distinct 	libelle_classe,id_classe FROM classe,eleve,inscription,parent where parent.idcompte_parent=eleve.idparent_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=? and parent.idcompte_parent=? ");
  $req->execute([$session,$IdCompte]);
  return $req->fetchAll();
}

function getNamePreOfTheCompte($idcompte_parent)
{
    $req = $this->db->prepare("SELECT  * FROM compte,parent where parent.idcompte_parent=compte.id_compte and compte.id_compte=?");
    $req->execute([$idcompte_parent]);
    $data=$req->fetchAll();
    $array=json_encode($data,true);
    $someArray = json_decode($array, true);
    $nom=$someArray[0]["nom_parent"]." - ".$someArray[0]["prenom_parent"];

    return $nom;
}

function getParentInfosbyId($studentparentid)
{
  $req = $this->db->prepare("SELECT  * FROM compte,parent where compte.email_compte=parent.email_parent and compte.id_compte=?");
  $req->execute([$studentparentid]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);
  $donnees=$someArray[0]["nom_parent"]."*".$someArray[0]["prenom_parent"]."*".$someArray[0]["tel_parent"]."*".$someArray[0]["profession_parent"]."*".$someArray[0]["email_parent"]."*".$someArray[0]["datenais_compte"]."*".$someArray[0]["sexe_parent"];
  $donnees.="*".$someArray[0]["cni_parent"]."*".$someArray[0]["fonction_compte"]."*".$someArray[0]["login_compte"]."*".$someArray[0]["photo_compte"]."*".$someArray[0]["id_compte"];
  return $donnees;
}

function getAllcniesbySchoolCode($codeEtabAssigner)
{
  $typecompte="Parent";
$req = $this->db->prepare("SELECT  parent.cni_parent,compte.id_compte FROM compte,parent,etablissement,eleve where compte.email_compte=parent.email_parent and compte.id_compte=eleve.idparent_eleve and etablissement.code_etab=eleve.codeEtab_eleve and eleve.codeEtab_eleve=? and compte.type_compte=?");
$req->execute([$codeEtabAssigner,$typecompte]);
return $req->fetchAll();
}

function getAllcnies()
{
  $typecompte="Parent";
$req = $this->db->prepare("SELECT  parent.cni_parent,compte.id_compte FROM compte,parent,etablissement,eleve where compte.email_compte=parent.email_parent and compte.id_compte=eleve.idparent_eleve and etablissement.code_etab=eleve.codeEtab_eleve and compte.type_compte=?");
$req->execute([$typecompte]);
return $req->fetchAll();
}

function getAllParentsNamebySchoolCode($codeEtabAssigner)
{
  $typecompte="Parent";
$req = $this->db->prepare("SELECT  parent.nom_parent,parent.prenom_parent,compte.id_compte FROM compte,parent,etablissement,eleve where compte.email_compte=parent.email_parent and compte.id_compte=eleve.idparent_eleve and etablissement.code_etab=eleve.codeEtab_eleve and eleve.codeEtab_eleve=? and compte.type_compte=?");
$req->execute([$codeEtabAssigner,$typecompte]);
return $req->fetchAll();
}

function getAllParentsName()
{
  $typecompte="Parent";
$req = $this->db->prepare("SELECT  parent.nom_parent,parent.prenom_parent,compte.id_compte FROM compte,parent,etablissement,eleve where compte.email_compte=parent.email_parent and compte.id_compte=eleve.idparent_eleve and etablissement.code_etab=eleve.codeEtab_eleve  and compte.type_compte=?");
$req->execute([$typecompte]);
return $req->fetchAll();
}

function Addoldparent($idcompte,$codeEtab)
{
  $reqY = $this->db->prepare("INSERT INTO  enregistrer SET idparent_enreg=?,codeEtab_enreg=?");
  $reqY->execute([
    $idcompte,
    $codeEtab
  ]);

$_SESSION['user']['addparok']="Parent ajouté avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/addparent.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {
header("Location:../locale/addparent.php");
  }



}

function getIdcompteParentByCniandEmail($cni,$emailTea,$codeEtab)
{
  $req = $this->db->prepare("SELECT  * FROM compte,parent,enregistrer where compte.email_compte=parent.email_parent and compte.id_compte=enregistrer.idparent_enreg and parent.cni_parent=? and compte.email_compte=? and enregistrer.codeEtab_enreg=? ");
  $req->execute([
    $cni,
    $emailTea,
    $codeEtab
  ]);

  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);
  $donnees=$someArray[0]["id_compte"];

  return $donnees;

}


function UpdateParentwithfilePass($nomTea,$prenomTea,$datenaisTea,$sexeTea,$contactTea,$fonction,$cni,$emailTea,$loginTea,$passTea,$fichierad,$idcompte)
{
  $req = $this->db->prepare("UPDATE parent SET nom_parent=?,prenom_parent=?,tel_parent=?,cni_parent=?,email_parent=?,sexe_parent=? WHERE idcompte_parent=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $contactTea,
    $cni,
    $emailTea,
    $sexeTea,
    $idcompte
  ]);

  //modification dans la table compte

  $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,login_compte=?,pass_compte=?,photo_compte=? WHERE id_compte=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $datenaisTea,
    $contactTea,
    $emailTea,
    $loginTea,
    $passTea,
    $fichierad,
    $idcompte
  ]);

  $_SESSION['user']['updateparentok']="Parent modifier avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/parents.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {
header("Location:../locale/parents.php");
  }

   //header("Location:../manager/parents.php");

}

function UpdateParentwithfilePassNot($nomTea,$prenomTea,$datenaisTea,$sexeTea,$contactTea,$fonction,$cni,$emailTea,$loginTea,$fichierad,$idcompte)
{
  $req = $this->db->prepare("UPDATE parent SET nom_parent=?,prenom_parent=?,tel_parent=?,cni_parent=?,email_parent=?,sexe_parent=? WHERE idcompte_parent=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $contactTea,
    $cni,
    $emailTea,
    $sexeTea,
    $idcompte
  ]);

  //modification dans la table compte

  $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,login_compte=?,photo_compte=? WHERE id_compte=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $datenaisTea,
    $contactTea,
    $emailTea,
    $loginTea,
    $fichierad,
    $idcompte
  ]);

  $_SESSION['user']['updateparentok']="Parent modifier avec succès";
  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/parents.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {
header("Location:../locale/parents.php");
  }

  // header("Location:../manager/parents.php");
}


function UpdateParentwithOutfilePass($nomTea,$prenomTea,$datenaisTea,$sexeTea,$contactTea,$fonction,$cni,$emailTea,$loginTea,$passTea,$idcompte)
{
  $req = $this->db->prepare("UPDATE parent SET nom_parent=?,prenom_parent=?,tel_parent=?,cni_parent=?,email_parent=?,sexe_parent=? WHERE idcompte_parent=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $contactTea,
    $cni,
    $emailTea,
    $sexeTea,
    $idcompte
  ]);

  //modification dans la table compte

  $req = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,login_compte=?,pass_compte=? WHERE id_compte=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $datenaisTea,
    $contactTea,
    $emailTea,
    $loginTea,
    $passTea,
    $idcompte
  ]);

  $_SESSION['user']['updateparentok']="Parent modifier avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/parents.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {
header("Location:../locale/parents.php");
  }
}


function UpdateParentwithOutfilePassNot($nomTea,$prenomTea,$datenaisTea,$sexeTea,$contactTea,$fonction,$cni,$emailTea,$loginTea,$idcompte)
{
  $req = $this->db->prepare("UPDATE parent SET nom_parent=?,prenom_parent=?,tel_parent=?,cni_parent=?,email_parent=?,sexe_parent=? WHERE idcompte_parent=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $contactTea,
    $cni,
    $emailTea,
    $sexeTea,
    $idcompte
  ]);

  //modification dans la table compte

  $reqx = $this->db->prepare("UPDATE compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,login_compte=? WHERE id_compte=?");
  $reqx->execute([
    $nomTea,
    $prenomTea,
    $datenaisTea,
    $contactTea,
    $emailTea,
    $loginTea,
    $idcompte
  ]);

  $_SESSION['user']['updateparentok']="Parent modifier avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/parents.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {
header("Location:../locale/parents.php");
  }
}



function getAllparentInfobyId($compte)
{
  $req = $this->db->prepare("SELECT  * FROM compte,parent where compte.email_compte=parent.email_parent and compte.id_compte=?");
  $req->execute([$compte]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);

  $donnees=$someArray[0]["nom_compte"]."*".$someArray[0]["prenom_compte"]."*".$someArray[0]["datenais_compte"]."*".$someArray[0]["sexe_parent"]."*".$someArray[0]["tel_compte"]."*".$someArray[0]["fonction_compte"];
  $donnees.="*".$someArray[0]["cni_parent"]."*".$someArray[0]["email_parent"]."*".$someArray[0]["login_compte"]."*".$someArray[0]["photo_compte"];
  return $donnees;
}

function getAllParent()
{
  $type_compte="Parent";
  $req = $this->db->prepare("SELECT * FROM compte,parent where compte.email_compte=parent.email_parent and  type_compte=? order by datecrea_compte desc  ");
   $req->execute([$type_compte]);
   return $req->fetchAll();
}

function getSelectionParentById($idcompte)
{
  $type_compte="Parent";
  $req = $this->db->prepare("SELECT * FROM compte,parent where compte.id_compte=parent.idcompte_parent and  compte.type_compte=? and compte.id_compte=?");
   $req->execute([$type_compte,$idcompte]);
   return $req->fetchAll();
}

function getAllParentBySchoolCode($codeEtabAssigner)
{
  $type_compte="Parent";
  $req = $this->db->prepare("SELECT * FROM compte,parent,enregistrer where compte.id_compte=enregistrer.idparent_enreg and compte.email_compte=parent.email_parent and  type_compte=? and enregistrer.codeEtab_enreg=?  ");
   $req->execute([$type_compte,$codeEtabAssigner]);
   return $req->fetchAll();
}

function getSelectionParentBySchoolCode($codeEtab,$idcompte)
{
  $req = $this->db->prepare("SELECT * FROM parent,compte,enregistrer  where parent.email_parent=compte.email_compte and enregistrer.idparent_enreg=compte.id_compte and enregistrer.codeEtab_enreg=? and compte.id_compte=?");
  $req->execute([$codeEtab,$idcompte]);
  return $req->fetchAll();
}

function AddParentwithfile($nomTea,$prenomTea,$contactTea,$fonction,$cni,$emailTea,$datenaisTea,$loginTea,$passTea,$type_cpte,$statut,$datecrea,$fichierad,$sexeTea,$codeEtab)
{
  require_once('../class/Sessionsacade.php');

  $session= new Sessionacade();

  $req1 = $this->db->prepare("INSERT INTO  compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=?,login_compte=?,pass_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?,photo_compte=?");
  $req1->execute([
    $nomTea,
    $prenomTea,
    $datenaisTea,
    $contactTea,
    $emailTea,
    $fonction,
    $loginTea,
    $passTea,
    $type_cpte,
    $statut,
    $datecrea,
    $fichierad

  ]);

  //recuperation de l'id compte du parent
  $idlastcompte=$this->db->lastInsertId();

  $req = $this->db->prepare("INSERT INTO  parent SET nom_parent=?,prenom_parent=?,tel_parent=?,profession_parent=?,cni_parent=?,email_parent=?,statut_parent=?,sexe_parent=?,idcompte_parent=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $contactTea,
    $fonction,
    $cni,
    $emailTea,
    $statut,
    $sexeTea,
    $idlastcompte

  ]);
  //insertion dans la table enregistrer

    $reqY = $this->db->prepare("INSERT INTO  enregistrer SET idparent_enreg=?,codeEtab_enreg=?");
    $reqY->execute([
      $idlastcompte,
      $codeEtab
    ]);

    //nous allons ajouter dans la table notificationstate

    //nous allons recuperer la session de cet etablissement

    $sessionencours=$session->getSessionEncours($codeEtab);
    $tabsessionencours=explode("*",$sessionencours);
    $libellesessionencours=$tabsessionencours[0];

    $statusstate=1;
    //ajoute dans la table

    $reqZ = $this->db->prepare("INSERT INTO  notificationstate SET idparent_state=?,session_state=?,status_state=?");
    $reqZ->execute([
    $idlastcompte,
    $libellesessionencours,
    $statusstate
    ]);




  $_SESSION['user']['addparok']="Parent ajouté avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/addparent.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {
header("Location:../locale/addparent.php");
  }

   //header("Location:../manager/addparent.php");

}

function AddParentwithoutfile($nomTea,$prenomTea,$contactTea,$fonction,$cni,$emailTea,$datenaisTea,$loginTea,$passTea,$type_cpte,$statut,$datecrea,$sexeTea,$codeEtab)
{
  $req1 = $this->db->prepare("INSERT INTO  compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=?,login_compte=?,pass_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?");
  $req1->execute([
    $nomTea,
    $prenomTea,
    $datenaisTea,
    $contactTea,
    $emailTea,
    $fonction,
    $loginTea,
    $passTea,
    $type_cpte,
    $statut,
    $datecrea

  ]);

    $idlastcompte=$this->db->lastInsertId();

  $req = $this->db->prepare("INSERT INTO  parent SET nom_parent=?,prenom_parent=?,tel_parent=?,profession_parent=?,cni_parent=?,email_parent=?,statut_parent=?,sexe_parent=?,idcompte_parent=?");
  $req->execute([
    $nomTea,
    $prenomTea,
    $contactTea,
    $fonction,
    $cni,
    $emailTea,
    $statut,
    $sexeTea,
    $idlastcompte
  ]);


  //insertion dans la table enregistrer

    $reqY = $this->db->prepare("INSERT INTO  enregistrer SET idparent_enreg=?,codeEtab_enreg=?");
    $reqY->execute([
      $idlastcompte,
      $codeEtab
    ]);

  $_SESSION['user']['addparok']="Parent ajouté avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/addparent.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {
header("Location:../locale/addparent.php");
  }

   //header("Location:../manager/addparent.php");

}

function ExistwithCniId($cni,$codeEtab)
{
  $req = $this->db->prepare("SELECT * FROM parent,compte,enregistrer where compte.email_compte=parent.email_parent and compte.id_compte=enregistrer.idparent_enreg and parent.cni_parent=? and enregistrer.codeEtab_enreg=?");
  $req->execute([$cni,$codeEtab]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function ExistParentwithMail($email)
{
  $type="Teacher";
  $req = $this->db->prepare("SELECT * FROM parent,compte where parent.email_parent=compte.email_compte and compte.type_compte=? and compte.email_compte=? ");
  $req->execute([$type,$email]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function parentcptemail($email)
{
  $req = $this->db->prepare("SELECT * FROM compte where compte.email_compte=? ");
  $req->execute([$email]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getContactOfParentById($id_compte)
{
  $req = $this->db->prepare("SELECT  tel_parent FROM parent where idcompte_parent=?");
  $req->execute([$id_compte]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);

  $donnees=$someArray[0]["tel_parent"];
  return $donnees;
}


}
 ?>
