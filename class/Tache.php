<?php

class Tache{

public $db;
function __construct() {
  require_once('../class/cnx.php');

  $db = new mysqlConnector();
  $this->db= $db->dataBase;
}

//fonction d'ajout de tache
  function Addtache($libelletache,$description,$deadline,$status, $dateday,$codeEtab,$sessionEtab,$addby)
  {
    $dateday=date("Y-m-d");
    $status=0;
    $req = $this->db->prepare("INSERT INTO tache SET lib_tache=?,description=?,deadline=?,status=?,date_enreg=?,codeEtab_tch=?,sessionEtab_tch=?,addby_tache=?");
    $req->execute([$libelletache,$description,$deadline,$status, $dateday,$codeEtab,$sessionEtab,$addby]);
    $idtache=$this->db->lastInsertId();
    // return $idcahier;

    //insertion dans la table rappel

    $datedaytime=date("Y-m-d H:i:s");
    $daterelance=date("Y-m-d H:i:s", strtotime("-30 minutes", strtotime($deadline)));

    // $req1="";
    $req1 = $this->db->prepare("INSERT INTO rappel SET idtache_rappel=?,date_rappel=?");
    $req1->execute([$idtache,$daterelance]);

  }
/////////////

function getAllrappeltaches($idcompte,$dateday)
{
  $req = $this->db->prepare("SELECT * from tache,rappel where tache.id_tache=rappel.idtache_rappel and tache.assignated=? and tache.status=1 and statut_rappel=0 and date_rappel>=? ");
  $req->execute([$idcompte,$dateday]);
}

function getAllrappeltachesD($idcompte,$dateday)
{
  $req = $this->db->prepare("SELECT * from tache,rappel where tache.id_tache=rappel.idtache_rappel and tache.assignated=? and tache.status=1 and statut_rappel=0 and date_rappel>=? limit 1");
  $req->execute([$idcompte,$dateday]);
}

  function Assigntaches($idtache,$codeEtab,$assign)
{
  $req = $this->db->prepare("UPDATE tache SET assignation=?, assignated = '1' WHERE	id_tache=? and  codeEtab_tch=? ");
  $req->execute([$assign,$idtache,$codeEtab]);

}

function stoprappeltaches($tacheid)
{
  $req = $this->db->prepare("UPDATE rappel SET statut_rappel=1, ' WHERE		id_rappel=? ");
  $req->execute([$tacheid]);
}

function AssignatedUsertotask($status,$assign,$tacheid,$codeEtab,$sessionEtab)
{
$req = $this->db->prepare("UPDATE tache SET status=?,assignated =? WHERE	id_tache=? and  codeEtab_tch=? and sessionEtab_tch=? ");
$req->execute([$status,$assign,$tacheid,$codeEtab,$sessionEtab]);

}

function gettacherassignerTouserlast($compteid)
{
  $status=1;
  $req = $this->db->prepare("SELECT * from tache where assignated=? and status=? order by deadline ASC limit 10");
  $req->execute([$compteid,$status]);
  return $req->fetchAll();
}

function getNumberOfTaches($compteid)
{
  $status=1;
  $req = $this->db->prepare("SELECT * from tache where assignated=? and status=? order by deadline ASC ");
  $req->execute([$compteid,$status]);
  return $req->fetchAll();
}

function gettacherassignerTouserlastby($compteid)
{
  $status=1;
  $req = $this->db->prepare("SELECT * from tache where addby_tache=? and status=? order by deadline ASC limit 10");
  $req->execute([$compteid,$status]);
  return $req->fetchAll();
}

function Terminertaches($senderid,$receiverid)
{
  $status=2;
  $req = $this->db->prepare("UPDATE tache SET status=? WHERE	id_tache=? and assignated =? ");
  $req->execute([$status,$receiverid,$senderid]);
}

//fonction  d'assignation de la tache
/*function Assigntache($idtache,$codeEtab,$assign)
{
  $req = $this->db->prepare("UPDATE  tache SET assignation=? where codeEtab_tch=? and id_tache=?");
  $req->execute([$idtache,$codeEtab,$assign]);

}

function Assignedtache($idtache,$codeEtab,$assigned)
{
  $req = $this->db->prepare("UPDATE  tache SET assignated=? where codeEtab_tch=? and id_tache=?");
  $req->execute([$idtache,$codeEtab,$assigned]);

}
*/
  function Updatetache($libelletache,$description,$deadline, $codeEtab,$idtache)
  {
    $req = $this->db->prepare("UPDATE  tache SET lib_tache=?,description=?,deadline=? where codeEtab_tch=? and id_tache=?");
    $req->execute([$libelletache,$description,$deadline, $codeEtab,$idtache]);

  }

  function Deletedtache($idtache,$codeEtab)
  {
    $req = $this->db->prepare("DELETE FROM tache WHERE id_tache=?");
    $req->execute([$idtache]);

  }


  function getAlltachesOfThisSchool($compte)
  {
    $req = $this->db->prepare("SELECT * FROM tache where  addby_tache=? order by 	id_tache DESC,deadline ASC");
    $req->execute([$compte]);
    return $req->fetchAll();
  }

  function gettacheByTacheId($idtache)
  {
    $req = $this->db->prepare("SELECT * FROM tache WHERE tache.id_tache=?");
    $req->execute([$idtache]);
    return $req->fetchAll();

  }


    function getAllUtilisateursEtab($codeEtab,$compteid)
    {
      $type_compte="Admin_locale";
      $req = $this->db->prepare("SELECT * FROM compte,assigner,etablissement where compte.id_compte=assigner.id_adLocal and assigner.codeEtab_assign=etablissement.code_etab and compte.type_compte=? and assigner.codeEtab_assign=? and compte.id_compte!=? and statut_compte=1");
       $req->execute([$type_compte,$codeEtab,$compteid]);
       return $req->fetchAll();
    }



}

 ?>
