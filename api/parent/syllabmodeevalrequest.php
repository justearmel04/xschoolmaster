<?php
// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
    // you want to allow, and if so:
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}
require_once('../class/User.php');
require_once('../class/cnx.php');
require_once('../class/Etablissement.php');

$users = new User();
$parents=new Etab();

if ($_SERVER['REQUEST_METHOD'] === 'POST')

{

//Recuperation des donnees de la premiere etape : Infos Client

$sessionEtab=$_GET['sessionEtab'];
$matiereid=$_GET['matiereid'];
$teatcherid=$_GET['teatcherid'];
$programmeid=$_GET['programmeid'];



 echo $parents->getallObjectifsOfthisSyllabus($programmeid,$teatcherid,$matiereid,$sessionEtab);




}else if ($_SERVER['REQUEST_METHOD'] === 'GET')

{
  $codeEtab=$_GET['codeEtab'];
  $teatcherid=$_GET['teatcherid'];
  $programmeid=$_GET['programmeid'];



  echo $parents->getSyllabusModeEvalInfos($programmeid,$teatcherid,$codeEtab);


}

?>
