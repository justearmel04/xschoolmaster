<?php

class Etab{

  public $db;
  function __construct() {
      require_once('../class/cnx.php');
      // require_once('../class/functions.php');
    $db = new mysqlConnector();
    $this->db= $db->dataBase;
      }

      function getControleNotesOfStudentLimited($compte)

     {

       $req = $this->db->prepare("SELECT * from notes,controle,eleve where notes.ideleve_notes=eleve.idcompte_eleve and notes.idtype_notes=controle.id_ctrl and notes.type_notes=1 and eleve.idcompte_eleve=? order by notes.id_notes desc limit 10");
       $req->execute([$compte]);
       $data=$req->fetchAll();
       return  json_encode($data);


     }

     // les nouveaux elements ajouter

      function getSyllabusRequisInfos($programme,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabrequis where syllabrequis.idsyllab_syllabreq=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
        $req->execute([$programme,$teatcher]);
        $data=$req->fetchAll();
        return  json_encode($data);
      }
      function getSyllabusDocInfos($programme,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabdoc where syllabdoc.idsyllab_syllabdoc=syllabus.id_syllab and syllabdoc.facultatif_syllabdoc=0  and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
        $req->execute([$programme,$teatcher]);
        $data=$req->fetchAll();
        return  json_encode($data);
      }
      function getSyllabusDocfacInfos($programme,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabdoc where syllabdoc.idsyllab_syllabdoc=syllabus.id_syllab and syllabdoc.facultatif_syllabdoc=1  and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
        $req->execute([$programme,$teatcher]);
        $data=$req->fetchAll();
        return  json_encode($data);
      }

      function getSyllabusCalendarInfos($programme,$teatcher,$codeEtab)
      {
          $req = $this->db->prepare("SELECT * FROM syllabus,syllabcalendar where syllabcalendar.idsyllab_syllabcal=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? order by date_syllabcal Asc ");
          $req->execute([$programme,$teatcher]);
          $data=$req->fetchAll();
          return  json_encode($data);
      }
      function getSyllabusModeEvalInfos($programme,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabeval where syllabeval.idsyllab_syllabeval=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? order by date_syllabeval ASC");
        $req->execute([$programme,$teatcher]);
        $data=$req->fetchAll();
        return  json_encode($data);
      }

      function getSyllabusRegleInfos($programme,$teatcher,$codeEtab)
      {
          $req = $this->db->prepare("SELECT * FROM syllabus, syllabregle  where syllabregle.idsyllab_syllabregle=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
        $req->execute([$programme,$teatcher]);
        $data=$req->fetchAll();
        return  json_encode($data);
      }

      function getstatutAttendanceDay($matricule,$dateday,$classe)
      {
        $req = $this->db->prepare("SELECT * FROM presences where matricule_presence=? and classe_presence=? and date_presence=? ");

        $req->execute([$matricule,$classe,$dateday]);

        return $req->fetchAll();

      }

function nbreJour($date1, $date2) {

// $date1 = str_replace("/", "-", $date1);
// $date2 = str_replace("/", "-", $date2);
$date1 = strtotime($date1);
$date2 = strtotime($date2);
$nbJoursTimestamp = $date2 - $date1;
$nbJours = $nbJoursTimestamp/86400; // 86 400 = 60*60*24
return intval($nbJours);


}

function getSyllabusThemesInfos($programme,$teatcher,$codeEtab)
{
      $req = $this->db->prepare("SELECT * FROM syllabus,syllabtheme where syllabtheme.idsyllab_syllabth=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
      $req->execute([$programme,$teatcher]);
      $data=$req->fetchAll();
      return  json_encode($data);
}

function getAllSubjectOfClasses($classeId,$codeEtabLocal,$session)
{
  $req = $this->db->prepare("SELECT * FROM matiere,classe,compte where compte.id_compte=matiere.teatcher_mat and   matiere.classe_mat=classe.id_classe and  matiere.classe_mat=? and matiere.codeEtab_mat=? and matiere.session_mat=?");
   $req->execute([$classeId,$codeEtabLocal,$session]);
   $data=$req->fetchAll();
   return  json_encode($data);
}

function getAllInformationsOfStudent($compte,$session)
{
  $req = $this->db->prepare("SELECT * from compte,eleve,parenter,classe,inscription where compte.id_compte=eleve.idcompte_eleve and eleve.idcompte_eleve=parenter.eleveid_parenter and inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and inscription.session_inscrip=? and parenter.eleveid_parenter=?");
    $req->execute([$session,$compte]);
    $data=$req->fetchAll();
    return  json_encode($data);


}

function ParentInfostudent($studentparentid)
{
    $req = $this->db->prepare("SELECT  * FROM compte,parent,parenter where compte.id_compte=parent.idcompte_parent and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=?");
    $req->execute([$studentparentid]);
    $data=$req->fetchAll();
    return json_encode($data);
}

function DetermineScolarityStateOfStudent($codeEtab,$libellesessionencours,$idcompte)
{
  $req = $this->db->prepare("SELECT * FROM versement where codeEtab_versement=? and session_versement=? and ideleve_versement=? order by versement.id_versement DESC");
  $req->execute([$codeEtabAssigner,$libellesessionencours,$idcompte]);
  $data=$req->fetchAll();
  return json_encode($data);
}

function getControleNotesWithoutLibctrlOther($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession)
{
  $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
    and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
    and notes.idmat_notes=?  and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=?  ");
    $req->execute([$idcompte,$classe,$matclasse,$notetype,$codeEtab,$libellesession]);
    $data=$req->fetchAll();

    return json_encode($data);

}

function getControleNotesWithoutLibctrlOtherOne($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession)
{
  $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
    and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
    and notes.idmat_notes=?  and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=? and controle.typesess_ctrl=(SELECT DISTINCT semestre.id_semes FROM semestre,sessions,notes WHERE notes.codeEtab_notes=semestre.codeEtab_semes and semestre.idsess_semes=sessions.id_sess and semestre.codeEtab_semes=? AND semestre.statut_semes=1 ORDER BY semestre.id_semes)  ");

    $req->execute([$idcompte,$classe,$matclasse,$notetype,$codeEtab,$libellesession,$codeEtab]);
    $data=$req->fetchAll();

    return json_encode($data);

}

function getControleNotesWithoutLibctrlOtherLast($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession)
{
  $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
    and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
    and notes.idmat_notes=?  and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=? order by notes.id_notes desc limit 10 ");

    $req->execute([$idcompte,$classe,$matclasse,$notetype,$codeEtab,$libellesession]);
    $data=$req->fetchAll();

    return json_encode($data);

}

function getlasnotestudent($idcompte,$listdesignation,$notetype,$classe,$codeEtab,$libellesession)
{
  $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
    and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
    and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=? order by notes.id_notes desc limit 10 ");

    $req->execute([$idcompte,$classe,$notetype,$codeEtab,$libellesession]);
    $data=$req->fetchAll();

    return json_encode($data);

}





      function getstudentattencances($matricule,$classe,$session,$datedeb,$datefin)
      {
        $etabs=new Etab();
        $nbjours=$etabs->nbreJour($datedeb,$datefin);
        $outp = "";
        if ($outp != "") {$outp .= ",";}
        for ($i=0;$i<$nbjours;$i++)
        {
          $dateday= date("Y-m-d", strtotime("+".$i."day", strtotime($datedeb)));
          $nombre=$etabs->getNbAttendanceDay($matricule,$dateday,$classe);
          $statut="";
          if($nombre==0)
          {
            $statut="A";
          }else {
            $number=$etab->getstatutAttendanceDay($matricule,$dateday,$classe);
            $array=json_encode($number,true);
            $someArray = json_decode($array, true);
            $presence=$someArray[0]["statut_presence"];
            if($presence==0)
            {
              $statut="A";
            }else {
              $statut="P";
            }
          }
          if ($outp != "") {$outp .= ",";}
          $outp .= '{"date":"'.date_format(date_create($dateday), "d/m/Y").'",';
          $outp .= '{"presence":"'.$statut.'"}';

        }

        return $outp;

      }

      function lastattendances($matricule,$classe,$session)
      {
          $statut=0;
  	      $etabs=new Etab();
          $req = $this->db->prepare("SELECT * FROM presences,matiere WHERE presences.matiere_presence=matiere.id_mat AND  presences.matricule_presence=? AND presences.classe_presence=? AND presences.session_presence=? and presences.statut_presence=? ORDER BY presences.date_presence DESC LIMIT 10 ");
          $req->execute([$matricule,$classe,$session,$statut]);
          $data=$req->fetchAll();


        $outp = "";
        if ($outp != "") {$outp .= ",";}
        foreach ($data as $value):
          $nombre=$etabs->getNbAttendanceDay($matricule,$value->date_presence,$classe);
          $statut="";
          if($nombre==0)
          {
            $statut="A";
          }else {
            $number=$student->getstatutAttendanceDay($matricule,$value->date_presence,$classe);
            $array=json_encode($number,true);
            $someArray = json_decode($array, true);
            $presence=$someArray[0]["statut_presence"];
            if($presence==0)
            {
              $statut="A";
            }else {
              $statut="P";
            }
          }

          if ($outp != "") {$outp .= ",";}
          $outp .= '{"date":"'.date_format(date_create($value->date_presence), "d/m/Y").'",';
          $outp .= '{"matiere":"'.$value->libelle_mat.'",';
          $outp .= '{"statut":"'.$statut.'"}';

        endforeach;

        return $outp;

      }



      function getNbAttendanceDay($matricule,$dateday,$classe)
      {
        $req = $this->db->prepare("SELECT statut_presence FROM presences where matricule_presence=? and classe_presence=? and date_presence=? ");

        $req->execute([$matricule,$classe,$dateday]);

        $data=$req->fetchAll();

        $nb=count($data);

        return $nb;
      }

      function getstudentroutines($classeid)
      {
        $etabs=new Etab();
        $dataday=$etabs->getAllweeks();
        $tab=array();
        $i=0;
        foreach ($dataday as $value) :
          $tab[$i]=$etabs->getspecificRoutine($value->id_days,$value->short_days,$classeid);
          $i++;
        endforeach;

        return $tab;
      }

      function getspecificRoutine($id_days,$short_days,$classe)
      {
        $req = $this->db->prepare("SELECT * from daysweek,routine,matiere where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=? and daysweek.short_days=? order by debut_route ASC");
        $req->execute([$classe,$short_days]);
        $data=$req->fetchAll();
        return json_encode($data);
      }

      function getAllweeks()
      {
        $req = $this->db->prepare("SELECT * from daysweek");
        $req->execute([]);
        return $req->fetchAll();

      }

      function getallCompentencesOfthisSyllabus($idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabcomp where syllabus.id_syllab=syllabcomp.idsyllab_syllabcomp and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and  syllabus.idmatiere_syllab=? and syllabus.session_syllab=?");
        $req->execute([$idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab]);
        $data=$req->fetchAll();
        $outp = "";
        if ($outp != "") {$outp .= ",";}
        foreach ($data as $value):
          $outp .= '{"Id":"'.$value->id_syllabcomp.'",';
          $outp .= '{"libelle_comp":"'. $value->libelle_syllabcomp.'"}';

        endforeach;

        return $outp;



      }

      function getallObjectifsOfthisSyllabus($idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab)
      {
          $req = $this->db->prepare("SELECT * FROM syllabus,syllabobjet where syllabus.id_syllab=syllabobjet.idsyllab_syllabob and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and  syllabus.idmatiere_syllab=? and syllabus.session_syllab=?");
          $req->execute([$idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab]);
          $data=$req->fetchAll();
          $outp = "";
          if ($outp != "") {$outp .= ",";}
          foreach ($data as $value):
            $outp .= '{"Id":"'  . $value->id_syllabob . '",';
            $outp .= '{"libelle_object":"'   . $value->libelle_syllabob  . '"}';

          endforeach;

          return $outp;
      }

      function getSyllabusInfos($programme,$teatcher,$codeEtab)
      {

          $req = $this->db->prepare("SELECT * FROM syllabus,classe,matiere  where syllabus.idclasse_syllab=classe.id_classe and matiere.codeEtab_mat=classe.codeEtab_classe and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and classe.codeEtab_classe=?");
         $req->execute([$programme,$teatcher,$codeEtab]);
          return $req->fetchAll();
      }

    //   function getprogrammesallteatchers($classeEtab,$codeEtab,$libellesessionencours)
    //   {
    // $req = $this->db->prepare("SELECT * FROM syllabus,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and syllabus.idteatcher_syllab=enseignant.idcompte_enseignant and syllabus.idmatiere_syllab=matiere.id_mat and syllabus.idclasse_syllab=classe.id_classe and classe.id_classe=? and classe.codeEtab_classe=?  and  syllabus.session_syllab=? order by syllabus.id_syllab ASC");
    // $req->execute([$classeEtab,$codeEtab,$libellesessionencours]);
    // return $req->fetchAll();
    //   }

      function getnumberofstudentparenter($parentid)
      {
          $encours=1;
          $req = $this->db->prepare("SELECT * FROM parenter,inscription where parenter.eleveid_parenter=inscription.ideleve_inscrip and session_inscrip=(SELECT distinct libelle_sess from sessions,etablissement where sessions.codeEtab_sess=etablissement.code_etab and encours_sess=? ) and parentid_parenter=?");
          $req->execute([$encours,$parentid]);
          $data=$req->fetchAll();
          $nb=count($data);
          return $nb;
      }

      function getprogrammesallteatchers($classeEtab,$codeEtab,$libellesessionencours)
      {
      $etabs=new Etab();
      $req = $this->db->prepare("SELECT * FROM syllabus,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and syllabus.idteatcher_syllab=enseignant.idcompte_enseignant and syllabus.idmatiere_syllab=matiere.id_mat and syllabus.idclasse_syllab=classe.id_classe and classe.id_classe=? and classe.codeEtab_classe=?  and  syllabus.session_syllab=? order by syllabus.id_syllab ASC");
      $req->execute([$classeEtab,$codeEtab,$libellesessionencours]);
      $data= $req->fetchAll();
      //return json_encode($data, JSON_FORCE_OBJECT);
      $outp = "";
      if ($outp != "") {$outp .= ",";}
      foreach ($data as $value):
        $competences=$etabs->getallCompentencesOfthisSyllabus($value->id_syllab,$value->idteatcher_syllab,$value->idmatiere_syllab,$value->session_syllab);
        $objectifs=$etabs->getallObjectifsOfthisSyllabus($value->id_syllab,$value->idteatcher_syllab,$value->idmatiere_syllab,$value->session_syllab);
        $outp .= '{"Id":"'  .$value->id_syllab. '",';
        $outp .= '{"matiere":"'  .$value->libelle_mat. '",';
        $outp .= '{"description":"'  .$value->descri_syllab. '",';
        $outp .= '{"objectifs":"'  .$objectifs. '",';
        $outp .= '{"competences":"'  .$competences. '",';
        $outp .= '"classe":"'.$value->libelle_classe. '"}';

      endforeach;

      return $outp;



      }


}


?>
