<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Teatcher.php');
require_once('../controller/functions.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$localadmins= new Localadmin();
$parents=new ParentX();
$matiere=new Matiere();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);

$matieres=$matiere->getAllSubjectTeatchByTeatcherId($_SESSION['user']['IdCompte']);
$controles=$matiere->getAllControleMatiereOfThisTeatcherId($_SESSION['user']['IdCompte']);
//$dataclasses=$classe->getAllClassesByClasseId($classeschoolid);
$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$etablissementType=$etabs->DetermineTypeEtab($codeEtabsession);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($etablissementType==1||$etablissementType==3)
{
  if($nbsessionOn>0)
  {
    //recuperer la session en cours
    $sessionencours=$session->getSessionEncours($codeEtabsession);
    $tabsessionencours=explode("*",$sessionencours);
    $libellesessionencours=$tabsessionencours[0];
    $sessionencoursid=$tabsessionencours[1];
    $typesessionencours=$tabsessionencours[2];

    // $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);

    $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
  }else {
    $nbclasse=0;
  }
}else {
  if($nbsessionOn>0)
  {
    //recuperer la session en cours
    $sessionencours=$session->getSessionEncours($codeEtabsession);
    $tabsessionencours=explode("*",$sessionencours);
    $libellesessionencours=$tabsessionencours[0];
    $sessionencoursid=$tabsessionencours[1];
    $typesessionencours=$tabsessionencours[2];

    $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);

    $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
  }else {
    $nbclasse=0;
  }
}


$nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);

$competences=$etabs->getAllcompetenceByTeatchers($_SESSION['user']['IdCompte'],$codeEtabsession,$libellesessionencours);

// var_dump($competences);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
        <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
  <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::MatiereGestions ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">

                              <li><a class="parent-item" href="matieres.php"><?php echo L::MatiereMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::MatiereGestions ?></li>
                          </ol>
                      </div>
                  </div>
                  <?php
                  if(isset($_SESSION['user']['messages']))
                  {
                    ?>
                    <div class="alert alert-success" role="alert">
                    <?php echo $_SESSION['user']['messages']; ?>
                  </div>
                    <?php
                    unset($_SESSION['user']['messages']);
                    // unset($_SESSION['user']['rappelid']);messagesnonok
                  }
                   ?>
                   <?php
                   if(isset($_SESSION['user']['messagesnonok']))
                   {
                     ?>
                     <div class="alert alert-danger" role="alert">
                     <?php echo $_SESSION['user']['messagesnonok']; ?>
                   </div>
                     <?php
                     unset($_SESSION['user']['messagesnonok']);
                     // unset($_SESSION['user']['rappelid']);
                   }
                    ?>
					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['addctrleok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
          <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
          <script src="../assets/js/sweetalert2.min.js"></script>

            <script>

            Swal.fire({
            type: 'success',
            title: 'Félicitations',
            text: '<?php echo $_SESSION['user']['addctrleok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['addctrleok']);
                }

                 ?>




          <?php

                if(isset($_SESSION['user']['deletesubjectok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
<link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
<script src="../assets/js/sweetalert2.min.js"></script>

            <script>

            Swal.fire({
          title: '<?php echo L::Felicitations ?>',
          text: "<?php echo $_SESSION['user']['deletesubjectok']; ?>",
          type: 'success',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'OK',

          }).then((result) => {
          if (result.value) {
          window.location.reload();
          }
          })


            </script>
                  <?php
                  unset($_SESSION['user']['deletesubjectok']);
                }

                 ?>


          <?php

                if(isset($_SESSION['user']['updatesubjectok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
<link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
<script src="../assets/js/sweetalert2.min.js"></script>

            <script>

            Swal.fire({
          title: '<?php echo L::Felicitations ?>',
          text: "<?php echo $_SESSION['user']['updatesubjectok']; ?>",
          type: 'success',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'OK',

          }).then((result) => {
          if (result.value) {
          window.location.reload();
          }
          })


            </script>
                  <?php
                  unset($_SESSION['user']['updatesubjectok']);
                }

                 ?>
          <?php

                if(isset($_SESSION['user']['addsubjectok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
<link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
<script src="../assets/js/sweetalert2.min.js"></script>

            <script>


            Swal.fire({
          title: '<?php echo L::Felicitations ?>',
          text: "<?php echo $_SESSION['user']['addsubjectok']; ?>",
          type: 'success',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'OK',

          }).then((result) => {
          if (result.value) {
          window.location.reload();
          }
          })
            </script>
                  <?php
                  unset($_SESSION['user']['addsubjectok']);
                }

                 ?>


<br/><br/>
          <div class="row">

            <div class="col-md-12 col-sm-12">
                          <div class="panel tab-border card-box">
                              <header class="panel-heading panel-heading-gray custom-tab ">
                                  <ul class="nav nav-tabs">
                                      <li class="nav-item"><a href="#home" data-toggle="tab" class="active"><i class="fa fa-bars"></i> <?php echo L::SubjectsListes ?></a>
                                      </li>
                                      <?php

                                       ?>

                                       <?php

                                       ?>
                                       <li class="nav-item"><a href="#about" data-toggle="tab"><i class="fa fa-plus-circle"></i> <?php echo L::AddCompetences ?></a>
                                       </li>
                                       <li class="nav-item"><a href="#about1" data-toggle="tab"><i class="fa fa-plus-circle"></i> <?php echo L::AddTheLibellecompetence ?></a>
                                       </li>
                                       <?php

                                        ?>

                                  </ul>
                              </header>
                              <div class="panel-body">
                                  <div class="tab-content">
                                      <div class="tab-pane active" id="home">
                                        <!-- <div class="row"> -->
               <!-- <div class="col-md-12"> -->
                 <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                     <thead>
                         <tr>

                             <th> <?php echo L::ClasseMenu ?></th>
                             <th> <?php echo L::MatiereMenusingle ?></th>
                             <!--th> <?php //echo L::ProfsMenusingle ?> </th-->
                             <th> <?php echo L::matierecompetences ?> </th>
                               <!-- <th> <?php //echo L::Coefs ?></th> -->
                             <th> <?php echo L::Actions ?> </th>
                         </tr>
                     </thead>
                     <tbody>
                       <?php
                         //$matieres

                         $i=1;
                           foreach ($matieres as $value):
                        ?>
                         <tr class="odd gradeX">

                             <td> <?php echo $value->libelle_mat;?></td>
                             <td>
                                 <?php echo $classe->getInfosofclassesbyId($value->classe_mat,$value->session_mat)?>
                             </td>
                             <!--td>
                               <?php //echo $value->nom_compte." ".$value->prenom_compte;?>
                             </td-->
                             <td>
                               <?php
                              $competence=$value->competenece_mat;
                              if($competence==0)
                              {
                                ?>
                                <span class="label label-danger"><?php echo L::NoCompetence ?></span>
                                <?php
                              }else if($competence>0) {
                                $competencesmatieres=$etabs->getAllcompetenceClassematieres($value->classe_mat,$value->id_mat);
                                // var_dump($competencesmatieres);
                                foreach ($competencesmatieres as $values):
                                  ?>


                              <span class="label label-success"   onclick="demandaisons(<?php echo $values->id_sousmat ?>,<?php echo $values->idmat_sousmat ?>,<?php echo $values->teatcher_sousmat ?>,'<?php echo $values->libelle_sousmat?>')"><?php echo $values->libelle_sousmat ?></span>
                                  <?php
                                endforeach;
                                ?>

                                <?php
                              }
                               ?>
                             </td>
                             <!-- <td>
                               <?php //echo $value->coef_ctrl;?>
                             </td> -->

                             <td class="valigntop">
                               <a href="#"  data-toggle="modal" data-target="#exampleModal" id="pencilmodal" class="btn btn-info  btn-xs " style="border-radius:3px;">
                                 <i class="fa fa-pencil"></i>
                               </a>
                               <!--a href="#"  onclick="modify(<?php //echo $value->id_mat;?>)" class="btn btn-info  btn-md " style="border-radius:3px;">
                                 <i class="fa fa-pencil"></i>
                               </a-->
                               <a href="#"  class="btn btn-danger  btn-xs " style="border-radius:3px;">
                                 <i class="fa fa-trash-o"></i>
                               </a>
                             </td>
                         </tr>




                         <?php
                         $i++;
                         endforeach;
                         ?>


                     </tbody>
                 </table>
               <!-- </div> -->
           <!-- </div> -->
                                      </div>
                                      <div class="tab-pane" id="about">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="card card-box">
                                                    <div class="card-head">
                                                        <header></header>

                                                    </div>

                                                    <?php
                                                    // if($nbsessionOn>0)
                                                    // {
                                                     ?>

                                                    <div class="card-body" id="bar-parent">
                                                        <form  id="FormAddCtrl" class="form-horizontal" action="#" method="post">
                                                            <div class="form-body">
                                                              <div class="form-group row">
                                                                      <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                                                          <span class="required"> * </span>
                                                                      </label>
                                                                      <div class="col-md-5">
                                                                          <select class="form-control " name="classe" id="classe"  style="width:100%" onchange="searchmatiere(<?php echo $_SESSION['user']['IdCompte']; ?>)">
                                                                              <option value=""><?php echo L::Selectclasses ?></option>
                                                                              <?php
                                                                              $i=1;
                                                                                foreach ($classes as $value):
                                                                                ?>
                                                                                <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                                                <?php
                                                                                                                 $i++;
                                                                                                                 endforeach;
                                                                                                                 ?>

                                                                          </select>
                                                                  </div>
                                                                </div>
                                                              <div class="form-group row">
                                                                      <label class="control-label col-md-3"><?php echo L::MatiereMenusingle ?>
                                                                          <span class="required"> * </span>
                                                                      </label>
                                                                      <div class="col-md-5">
                                                                          <!--input type="text" name="matiere" id="matiere" data-required="1" placeholder="Entrer la classe" class="form-control input-height" /-->
                                                                          <select class="form-control " name="matiere" id="matiere"  style="width:100%" onchange="searchcodeEtab(<?php echo $_SESSION['user']['IdCompte']; ?>)">
                                                                              <option value=""><?php echo L::SelectSubjects ?></option>

                                                                          </select>
                                                                          <input type="hidden" id="teatcher" name="teatcher" value="<?php echo $_SESSION['user']['IdCompte']; ?>">

                                                                          <!--input type="hidden" name="etape" id="etape" value="1"/-->
                                                                          <input type="hidden" name="etape" id="etape" value="1"/>
                                                                          <input type="hidden" name="codeEtab" id="codeEtab" value=""/>
                                                                          <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                                                        </div>

                                                               </div>
                                                               <div class="form-group row">
                                                                       <label class="control-label col-md-3"><?php echo L::matierecompetence ?>
                                                                           <span class="required"> * </span>
                                                                       </label>
                                                                       <div class="col-md-5">

                                                                         <select class="form-control " id="competence" name="competence" style="width:100%" >
                                                                             <option value=""><?php echo L::selectCompetence ?></option>
                                                                             <?php
                                                                             $i=1;
                                                                               foreach ($competences as $value):
                                                                               ?>
                                                                               <option value="<?php echo $value->id_sousmat?>"><?php echo utf8_encode(utf8_decode($value->libelle_sousmat)) ?></option>

                                                                               <?php
                                                                                                                $i++;
                                                                                                                endforeach;
                                                                                                                ?>

                                                                         </select>

                                                                      </div>
                                                                   </div>


                                          <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="offset-md-3 col-md-9">
                                                                        <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                                                        <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                    </div>
                                                                  </div>
                                                               </div>
                                        </div>
                                                        </form>
                                                    </div>
                                                    <?php
                                                  // }
                                                     ?>
                                                </div>
                                            </div>

                                        </div>
                                       </div>
                                       <div class="tab-pane" id="about1">
                                         <div class="row">
                                             <div class="col-md-12 col-sm-12">
                                                 <div class="card card-box">
                                                     <div class="card-head">
                                                         <header></header>

                                                     </div>

                                                     <div class="card-body" id="bar-parent">
                                                         <form  id="FormAddmatlib" class="form-horizontal" action="../controller/matiere.php" method="post">
                                                             <div class="form-body">
                                                               <div class="form-group row">
                                                                       <label class="control-label col-md-3"><?php  echo L::LibelleSubjects; ?>
                                                                           <span class="required"> * </span>
                                                                       </label>
                                                                       <div class="col-md-5">
                                                                           <input type="text" name="matierelib" id="matierelib" data-required="1" placeholder="" class="form-control" />
                                                                           <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $_SESSION['user']['codeEtab']; ?>"/>
                                                                           <input type="hidden" name="teatcherid" id="teatcherid" value="<?php echo $_SESSION['user']['IdCompte']; ?>"/>
                                                                             <input type="hidden" name="etape" id="etape" value="5"/>
                                                                             <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>"/>
                                                                           <!-- input type="hidden" name="libellesession" id="libellesession" value="<?php //echo $libellesessionencours; ?>" -->

                                                                         </div>
                                                                </div>







                                           <div class="form-actions">
                                                                 <div class="row">
                                                                     <div class="offset-md-3 col-md-9">
                                                                         <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                                                         <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                     </div>
                                                                   </div>
                                                                </div>
                                         </div>
                                                         </form>
                                                     </div>
                                                 </div>
                                             </div>

                                         </div>
                                        </div>

                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                      <div class="modal-dialog modal-lg" role="document">
                                                          <div class="modal-content">
                                                              <div class="modal-header">
                                                                  <h4 class="modal-title" id="exampleModalLabel"><?php echo L::ModificationControle ?></h4>
                                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                      <span aria-hidden="true">&times;</span>
                                                                  </button>
                                                              </div>
                                                              <div class="modal-body">
                                                                <form  id="FormUpdateControle" class="form-horizontal" action="../controller/controle.php" method="post">
                                                                    <div class="form-body">
                                                                      <div class="form-group row">
                                                                              <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                                                                  <span class="required"> * </span>
                                                                              </label>
                                                                              <div class="col-md-5">
                                                                                  <select class="form-control " name="classe" id="classe"  style="width:171%" >
                                                                                      <option value=""><?php echo L::Selectclasses ?></option>


                                                                                  </select>

                                                                          </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                                <label class="control-label col-md-3"><?php echo L::MatiereMenusingle ?>
                                                                                    <span class="required"> * </span>
                                                                                </label>
                                                                                <div class="col-md-5">
                                                                                    <!--input type="text" name="matiere" id="matiere" data-required="1" placeholder="Entrer la classe" class="form-control " /-->
                                                                                    <select class="form-control " name="matiere" id="matiere"  style="width:171%">
                                                                                        <option value=""><?php echo L::SelectSubjects ?></option>


                                                                                    </select>

                                                                                    <input type="hidden" id="teatcher" name="teatcher" value="<?php echo $_SESSION['user']['IdCompte']; ?>">
                                                                                  </div>

                                                                         </div>

                                                                             <div class="form-group row">
                                                                                     <label class="control-label col-md-3"><?php echo L::ControlsMenusigle ?>
                                                                                         <span class="required"> * </span>
                                                                                     </label>
                                                                                     <div class="col-md-5">
                                                                                         <input type="text" name="controle" id="controle" value="" data-required="1" placeholder="Entrer la classe" class="form-control " style="width:171%" />




                                                                                         <input type="hidden" name="codeEtab" id="codeEtab" value=""/>
                                                                                         <input type="hidden" name="idctrl" id="idctrl" value=""/>

                                                                                       </div>

                                                                              </div>

                                                                              <div class="form-group row">
                                                                                      <label class="control-label col-md-3"><?php echo L::Coefs ?>
                                                                                          <span class="required"> * </span>
                                                                                      </label>
                                                                                      <div class="col-md-5">
                                                                                          <input type="number" min="1" value="" name="coef" id="coef" data-required="1" placeholder="<?php echo L::EnterCoefSubjectControlLib ?>" class="form-control " style="width:171%"/>

                                                                                        </div>
                                                                               </div>
                                                                               <div class="form-group row">
                                                                                 <label class="control-label col-md-3"><?php echo L::DatecontolsTea ?>
                                                                                     <span class="required"> * </span>
                                                                                 </label>
                                                                                     <div class="col-md-5">
                                                                                         <input type="date"  placeholder="<?php echo L::DatecontolsTea ?>" value="" name="datectrl" id="datectrl" data-mask="99/99/9999" class="form-control " style="width:171%">
                                                                                           <span class="help-block"><?php echo L::Datesymbole ?></span>

                                                                                     </div>
                                                                                 </div>




                                                  <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="offset-md-3 col-md-9">
                                                                                <button type="submit"  class="btn btn-info"><?php echo L::ModifierBtn ?></button>
                                                                                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo L::AnnulerBtn ?></button>
                                                                            </div>
                                                                          </div>
                                                                       </div>
                                                </div>
                                                                </form>
                                                              </div>

                                                          </div>
                                                      </div>
                                                  </div>


                                  </div>
                              </div>
                          </div>
                      </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>

   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function demandaisons(soumatiereid,matiereid,teatcherid,libellesousmatiere)
   {
     Swal.fire({
   title: '<?php echo L::WarningLib ?>',
   text: "<?php echo L::WhatdoyoudoInthiscompetence ?>",
   type: 'warning',
   showCancelButton: true,
   confirmButtonColor: '#3085d6',
   cancelButtonColor: '#d33',
   confirmButtonText: '<?php echo L::ModifierBtn ?>',
   cancelButtonText: '<?php echo L::Retirer ?>',
   }).then((result) => {
   if (result.value) {
   $("#pencilmodal").click();
   }else {
     alert('retirer');
   }
   })
   }

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function searchmatiere(id)
   {

       var classe=$("#classe").val();
       var teatcherId=id;
       var etape=6;


     $.ajax({

          url: '../ajax/matiere.php',
          type: 'POST',
          async:true,
          data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe,
          dataType: 'text',
          success: function (content, statut) {


            $("#matiere").html("");
            $("#matiere").html(content);

          }
        });
   }

   function searchcodeEtab(id)
   {
     var classe=$("#classe").val();
     var teatcherId=id;
     var etape=7;
     var matiere=$("#matiere").val();

     $.ajax({

          url: '../ajax/matiere.php',
          type: 'POST',
          async:true,
          data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
          dataType: 'text',
          success: function (content, statut) {


            $("#FormAddCtrl #codeEtab").val(content);

          }
        });

   }
   $("#classe").select2();
   $("#competence").select2();
   $("#classeEtab").select2();
   $("#matiere").select2();


   $(document).ready(function() {

     $("#FormAddCtrl").validate({

       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
     },
     highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
     },
     success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },
        rules:{

          // matiere:"required",
          // classe:"required",
          // teatcher:"required",
          // coef:"required",
          classe:"required",
          matiere:"required",
          controle:"required",
          coef:"required",
          teatcher:"required",
          datectrl:"required",
          typesess:"required",
          competence:"required"


        },
        messages: {
          // matiere:"Merci de renseigner la matière",
          // classe:"<?php echo L::PleaseSelectclasserequired ?>",
          // teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
          // coef:"Merci de renseigner le coefficient de la matière"
          classe:"<?php echo L::PleaseSelectclasserequired ?>",
          matiere:"<?php echo L::SubjectSelectedrequired ?>",
          controle:"<?php echo L::Controlsrequired ?>",
          coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
          teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
            datectrl:"<?php echo L::ControlsDaterequired ?>",
            typesess:"<?php echo L::PeriodRequired ?>",
            competence:"<?php echo L::PleaseselectCompetence ?>"


        },
        submitHandler: function(form) {


  // nous allons verifier un controle similaire n'existe pas
          var etape=2;

           $.ajax({
             url: '../ajax/competence.php',
             type: 'POST',
             async:true,
             data: 'matiere=' + $("#FormAddCtrl #matiere").val()+ '&etape=' + etape+'&classe='+$("#FormAddCtrl #classe").val()+'&teatcher='+$("#FormAddCtrl #teatcher").val()+'&codeEtab='+$("#FormAddCtrl #codeEtab").val()+'&sessionEtab='+$("#FormAddCtrl #libellesession").val()+'&competence='+$("#FormAddCtrl #competence").val(),
             dataType: 'text',
             success: function (content, statut) {


               window.location.reload();

             }
           });

               }


             });


             $("#FormAddmatlib").validate({

               errorPlacement: function(label, element) {
               label.addClass('mt-2 text-danger');
               label.insertAfter(element);
             },
             highlight: function(element, errorClass) {
               $(element).parent().addClass('has-danger')
               $(element).addClass('form-control-danger')
             },
             success: function (e) {
                   $(e).closest('.control-group').removeClass('error').addClass('info');
                   $(e).remove();
               },
                 rules:{
                   matierelib:"required"
                 },
                 messages:{
                   matierelib:"Merci de renseigner le libellé de la compétence"

                 },
                   submitHandler: function(form) {

                     var competencelib=$("#matierelib").val();
                     var codeEtab=$("#FormAddmatlib #codeEtab").val();
                     var sessionEtab=$("#FormAddmatlib #libellesession").val();
                     var id=$("#FormAddmatlib #teatcherid").val();
                     var etape=1;

                     //nous allons verifié si cet libelle de matiere n'existe pas deja au niveau de la base

                     // var etape=13;
                     //
                     $.ajax({
                       url: '../ajax/competence.php',
                       type: 'POST',
                       async:true,
                       data: 'matiere=' + competencelib+ '&etape=' + etape+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&teatcherid='+id,
                       dataType: 'text',
                       success: function (content, statut) {

                      window.location.reload();

                       }
                     });
                   }

             });



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
