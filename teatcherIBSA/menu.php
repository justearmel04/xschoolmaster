<div class="white-sidebar-color sidebar-container">
                <div class="sidemenu-container navbar-collapse collapse fixed-menu notScrollable">
                   <div id="remove-scroll">
                   <ul class="sidemenu  page-header-fixed sidemenu-hover-submenu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                        <li class="sidebar-toggler-wrapper hide">
                            <div class="sidebar-toggler">
                                <span></span>
                            </div>
                        </li>
	                        <li class="sidebar-user-panel">
	                            <li class="sidebar-user-panel">

                              <div class="user-panel">
                               <div class="" style="text-align:center" >
                                  <?php
                                  // $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);

                                  $lienlogoetab="../logo_etab/0001/0001.png";
                                   ?>
                                    <img src="<?php echo $lienlogoetab?>"  style="" class="" alt="User Image" />
                                    <!-- <img src="../assets/img/ibsaeduk.png"  style="" class="" alt="User Image" /> -->
                                </div>
                              </div>

                          </li>

	                        </li>

                          <li class="nav-item active open">

	                            <a href="index.php" class="nav-link nav-toggle">

	                                <i class="material-icons">dashboard</i>

	                                <span class="title"><?php echo L::Homestartindex ?></span>

	                                <span class="selected"></span>

	                            </a>

	                        </li>
                           <!--  debut des li de l'eleve -->
                          <li class="nav-item">
                              <a href="javascript:;" class="nav-link nav-toggle">
                                  <i class="material-icons">dashboard</i>
                                <span class="title"><?php echo L::studMenu ?></span>
                                   <span class="selected"></span>
                                  <span class="arrow open"></span>
                              </a>
                              <ul class="sub-menu" style="margin-left:225px">
                                <li class="nav-item">
                                    <a href="classes.php" class="nav-link">
                                        <?php echo L::ClassesMenu ?>
                                      </a>
                                </li>
                              </ul>
                            </li>

                          <!--  debut des li de l'enseignant -->

                          <li class="nav-item">
 	                            <a href="javascript:;" class="nav-link nav-toggle">
 	                                <i class="material-icons">dashboard</i>
 	                               <span class="title"><?php echo L::EnsigneMenu ?></span>
                                   <span class="selected"></span>
                                  <span class="arrow open"></span>
 	                            </a>
 	                            <ul class="sub-menu" style="margin-left:225px">
                                <li class="nav-item">
                                    <a href="programmes.php" class="nav-link">
                                       <?php echo L::syllabusplanning ?>
                                      </a>
                                </li>

                                <li class="nav-item">
                                    <a href="routines.php" class="nav-link">
                                       <?php echo L::RoutinesMenu ?>
                                      </a>
                                </li>

 	                            </ul>
 	                        </li>

                            <li class="nav-item">
                              <a href="javascript:;" class="nav-link nav-toggle">
                                  <i class="material-icons">dashboard</i>
                                    <span class="title"><?php echo L::AbsMenu ?></span>
                                   <span class="selected"></span>
                                  <span class="arrow open"></span>
                              </a>
                              <ul class="sub-menu" style="margin-left:225px">
                                <li class="nav-item">
                                    <a href="dailyattendance.php" class="nav-link">
                                        <?php echo L::AddAbsMenu ?>
                                      </a>
                                </li>

                                <li class="nav-item  " class="nav-link nav-toggle">
                                    <a href="#" class="nav-link nav-toggle" ><?php echo L::RecapsMenu ?>s</a>
                                  <ul class="sub-menu" style="">
                                    <li class="nav-item" ><a href="recapattendanceclasse.php"><?php echo L::ClasseMenu ?></a></li>
                                    <li class="nav-item"><a href="recapattendanceclasseeleves.php"><?php echo L::EleveMenu ?></a> </li>
                                    <li class="nav-item"> <a href="recapattendanceclassematiere.php"><?php echo L::MatiereMenu ?></a> </li>
                                  </ul>
                                    <!--a href="recapattendance.php" class="nav-link "> <span class="title">Récap des absences</span>
                                    </a-->
                                </li>
                                <li class="nav-item  " class="nav-link nav-toggle">
                                    <a href="#" class="nav-link nav-toggle" ><?php echo L::RecapCourbeMenu ?></a>
                                  <ul class="sub-menu" style="">
                                    <li class="nav-item" ><a href="attendancegraphclasse.php"><?php echo L::ClasseMenu ?></a></li>
                                    <li class="nav-item"><a href="attendancegraph.php"><?php echo L::EleveMenu ?></a> </li>

                                  </ul>
                                    <!--a href="recapattendance.php" class="nav-link "> <span class="title">Récap des absences</span>
                                    </a-->
                                </li>

                               </ul>
                            </li>

                              <li class="nav-item">
                              <a href="javascript:;" class="nav-link nav-toggle">
                                  <i class="material-icons">dashboard</i>
                                  <span class="title"><?php echo L::Evaluations ?></span>
                                   <span class="selected"></span>
                                  <span class="arrow open"></span>
                              </a>
                              <ul class="sub-menu" style="margin-left:225px">

                                <li class="nav-item  " class="nav-link nav-toggle">

                                    <a href="#" class="nav-link nav-toggle" ><?php echo L::AddnotesMenu ?></a>

                                  <ul class="sub-menu" style="">

                                    <li class="nav-item" ><a href="addnotes.php"><?php echo L::ClasseMenu ?></a></li>

                                    <li class="nav-item"><a href="addnoteseleves.php"><?php echo L::EleveMenu ?></a> </li>



                                  </ul>



                                </li>

                                <!-- <li class="nav-item  ">
                                    <a href="addnotes.php" class="nav-link "> <span class="title"><?php echo L::AddnotesMenu ?></span>
                                    </a>
                                </li> -->
                                <!-- <li class="nav-item  ">
                                      <a href="controles.php" class="nav-link "> <span class="title"><?php echo L::ControlsMenu ?></span>
                                      </a>
                                  </li> -->
                                  <!--li class="nav-item  ">
                                      <a href="notes.php" class="nav-link "> <span class="title">Toutes les Notes</span>
                                      </a>
                                  </li-->
                                  <li class="nav-item  " class="nav-link nav-toggle">
                                      <a href="#" class="nav-link nav-toggle" ><?php echo L::RecapNotesMenu ?></a>
                                    <ul class="sub-menu" style="">
                                      <li class="nav-item" ><a href="notes.php"><?php echo L::ClasseMenu ?></a></li>
                                      <li class="nav-item"><a href="noteseleves.php"><?php echo L::EleveMenu ?></a> </li>

                                    </ul>
                                      <!--a href="recapattendance.php" class="nav-link "> <span class="title">Récap des absences</span>
                                      </a-->
                                  </li>

                               </ul>
                            </li>
                            <?php
                            /*
                             ?>
                            <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="material-icons">dashboard</i>
                                <span class="title"><?php echo L::CourseMenu ?></span>
                                 <span class="selected"></span>
                                <span class="arrow open"></span>
                            </a>
                            <ul class="sub-menu" style="margin-left:225px">


                              <li class="nav-item  ">
                                  <a href="addcourses.php" class="nav-link "> <span class="title"><?php echo L::Newcourses ?></span>
                                  </a>
                              </li>
                              <li class="nav-item  ">
                                    <a href="listcourses.php" class="nav-link "> <span class="title"><?php echo L::CoursesLists ?></span>
                                    </a>
                                </li>

                             </ul>
                          </li>
                          <?php
                          */
                           ?>

                          <li class="nav-item">
                          <a href="javascript:;" class="nav-link nav-toggle">
                              <i class="material-icons">dashboard</i>
                              <span class="title"><?php echo L::DevoirsMenu ?></span>
                               <span class="selected"></span>
                              <span class="arrow open"></span>
                          </a>
                          <ul class="sub-menu" style="margin-left:225px">


                            <li class="nav-item  ">
                                <a href="adddevoirs.php" class="nav-link "> <span class="title"><?php echo L::NewsDevoirsMenu ?></span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                  <a href="listdevoirs.php" class="nav-link "> <span class="title"><?php echo L::DevoisLists ?></span>
                                  </a>
                              </li>

                           </ul>
                        </li>

                        <li class="nav-item">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="material-icons">dashboard</i>
                            <span class="title"><?php echo L::QuizMenu ?></span>
                             <span class="selected"></span>
                            <span class="arrow open"></span>
                        </a>
                        <ul class="sub-menu" style="margin-left:225px">


                          <li class="nav-item  ">
                              <a href="addquiz.php" class="nav-link "> <span class="title"><?php echo L::NewsQuizsMenu ?></span>
                              </a>
                          </li>
                          <li class="nav-item  ">
                                <a href="listquizs.php" class="nav-link "> <span class="title"><?php echo L::QuizsLists ?></span>
                                </a>
                            </li>

                         </ul>
                      </li>






                            <li class="nav-item ">
                                <a href="notifications.php" class="nav-link nav-toggle">
                                    <i class="material-icons">dashboard</i>
                                    <span class="title"><?php echo L::NotificationMenu; ?></span>
                                  </a>
                             </li>
                             <li class="nav-item " style="display:none">
                             <a href="javascript:;" class="nav-link nav-toggle">
      <i class="material-icons">dashboard</i>
      <span class="title"><?php echo L::titleProjet ?></span>
       <span class="selected"></span>
      <span class="arrow open"></span>
  </a>
  <ul class="sub-menu" style="margin-left:225px">


    <li class="nav-item  ">
        <a href="addprojet.php" class="nav-link "> <span class="title"><?php echo L::newsProject ?></span>
        </a>
    </li>
    <li class="nav-item  ">
          <a href="allprojet.php" class="nav-link "> <span class="title"><?php echo L::listeProjet ?></span>
          </a>
     </li>
   </ul>
</li>


                          </div>
                    </div>
