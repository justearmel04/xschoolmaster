<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$emailUti=$_SESSION['user']['email'];

$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$classe=new Classe();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);
// $classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);
// $nbclasse=$classe->getNumberClassesOfTeatcherId($_SESSION['user']['IdCompte']);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$parentlyStudent=$parents->getDifferentStudentByParentId($_SESSION['user']['IdCompte']);
$nbnotificationstandby=$parents->getParentnotificationstandbyNb($compteuserid);

//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($compteuserid);


// $notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

// $destinataires=$etabs->getMessagesdestinaires($_GET['msg'],$codeEtabAssigner,$libellesessionencours);

// var_dump($destinataires);

//recuperation des informations du message

$messagesInfos=$etabs->getMessagesInformationsDetailsParent($_GET['msg']);
$commentairesmessage="";
foreach ($messagesInfos as $value):
$commentairesmessage=$value->commentaire_msg;
$file_msg=$value->file_msg;
$addby_msg=$value->addby_msg;
$date_msg=$value->date_msg;
$codeEtab_msg=$value->codeEtab_msg;
$session_msg=$value->session_msg;
$id_msg=$value->id_msg;


$statutlecture=$etabs->getstatusLecture($id_msg,$codeEtab_msg,$session_msg,$compteuserid);

// echo $statutlecture;

if($statutlecture==0)
{
  $etabs->activeLecturemessage($id_msg,$codeEtab_msg,$session_msg,$compteuserid);
  // echo $statutlecture." * ".$codeEtab_msg." * ".$session_msg;
  header("refresh: 0");
}

endforeach;

$onlineUsers=$etabs->getonlinesUsers($codeEtab_msg);
$offlineUsers=$etabs->getofflinesUsers($codeEtab_msg);




 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
        <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="../cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
  <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
  <link href="../assets2/plugins/summernote/summernote.css" rel="stylesheet">
  <link href="../assets2/css/pages/inbox.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <style media="screen">
 .navbar-custom {
background: #71d40f;
float: left;
width: 100%;
}

#radioBtn .notActive{
color: #3276b1;
background-color: #fff;
}

#radioBtn1 .notActive{
color: #3276b1;
background-color: #fff;
}

//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
 color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
 color: #f8f9fa;
}
.app-file-list {
border: 1px solid #ebebeb;
}

.app-file-list .app-file-icon {
background-color: #f5f5f5;
padding: 2rem;
text-align: center;
font-size: 2rem;
border-bottom: 1px solid #ebebeb;
border-top-right-radius: 8px;
border-top-left-radius: 8px;
}

.app-file-list:hover {
border-color: #d7d7d7;
}
.dropdown-menu-right{right:0;left:auto}
body.dark .app-file-list {
border-color: #454c66;
}

.baseline .baseline-list .baseline-info {
margin-top: 10px;
margin-bottom: 10px;
margin-left: 20px;
}

.baseline-info {
border-color: #36a2f5 !important;
}
.baseline-border {
border-left: 1px solid #e5ebf8;
}
.baseline, .baseline .baseline-list {
position: relative;
border-color: #e5ebf8;
}
.baseline .baseline-list {
padding-bottom: 1px;
}
.baseline, .baseline .baseline-list {
position: relative;
border-color: #e5ebf8;
}
.baseline .baseline-list:before {
display: table;
content: " ";
}
.baseline .baseline-list:after {
display: table;
position: absolute;
top: 18px;
left: 0;
width: 12px;
height: 12px;
margin-left: -6px;
content: "";
border-width: 1px;
border-style: solid;
border-color: inherit;
border-radius: 10px;
background-color: #fff;
box-shadow: 0 0 0 3px #e5ebf8 inset;
}

.baseline-primary:after {
box-shadow: 0 0 0 3px #a768f3 inset !important;
}
.baseline-success:after {
box-shadow: 0 0 0 3px #34bfa3 inset !important;
}
.baseline-primary {
border-color: #a768f3 !important;
}
.baseline-success {
border-color: #34bfa3 !important;
}
.baseline-warning:after {
box-shadow: 0 0 0 3px #eac459 inset !important;
}
.baseline-info:after {
box-shadow: 0 0 0 3px #36a2f5 inset !important;
}
.baseline-warning {
border-color: #eac459 !important;
}
 </style>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::MessagesDetails ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li>&nbsp;<a class="parent-item" href="notifications.php"><?php echo L::NotificationMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::MessagesDetails ?></li>
                          </ol>
                      </div>
                  </div>




					<!-- start widget -->




					<div class="state-overview">

						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


          <div class="row">



              <div class="col-md-12 col-sm-12">

                <!-- <div class="state-overview"> -->

                  <div class="row">


                  </div>
                       <div class="row">


                             <div class="col-md-12">
                                 <div class="inbox-body">

                                     </div>
                                     <div class="inbox-body no-pad">
                                         <section class="mail-list">
                                             <div class="mail-sender">

                         <div class="media">

                           <div class="media-body">
                             <!-- <span class="date pull-right">4:15AM 04 April
                               2014</span> -->
                             <h4 class="text-primary"><?php echo $etabs->getUtilisateurName($addby_msg); ?></h4>
                             <small class="text-muted"><?php echo $etabs->getUtilisateurFonction($addby_msg) ?></small>
                           </div>
                         </div>
                       </div>
                                             <div class="view-mail">
                                                 <?php
                                                 echo $commentairesmessage;
                                                  ?>
                                             </div>
                                             <?php
                                             if($file_msg==1)
                                             {
                                               //compter le nombre de document
                                               $documents=$etabs->getAllmessagesFiles($_GET['msg']);



                                               ?>

                                               <div class="attachment-mail">
                                                   <p>
                                                       <span><i class="fa fa-paperclip"></i> <?php echo $etabs->getNbofmessagesFiles($_GET['msg']); ?> <?php echo L::attachment ?></span>

                                                   </p>

                                               </div>

                                               <div class="row">


                                                 <?php
                                                 foreach ($documents as  $value):
                                                   $filename=$value->files_mfiles;
                                                   $tabfiles=explode('.',$filename);
                                                   $typefiles=$tabfiles[1];
                                                   $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab_msg);
                                                   $lien="../messages/".$libelleEtab."/".$codeEtab_msg."/".$session_msg."/".$filename;

                                                   ?>

                                                   <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                                                 <div class="card app-file-list">
                                                                     <div class="app-file-icon">
                                                                       <?php
                                                                       // $filename=$carnetstudent;
                                                                       // $tabfiles=explode('.',$filename);
                                                                       // $typefiles=$tabfiles[1];


                                                                       if($typefiles=="csv"||$typefiles=="xlsx"||$typefiles=="xls")
                                                                       {
                                                                         ?>
                                                                         <i class="fa fa-file-excel-o text-success"></i>
                                                                         <?php

                                                                       }else if($typefiles=="docx"||$typefiles=="docx")
                                                                       {
                                                                         ?>
                                                                         <i class="fa fa-file-text-o text-primary"></i>
                                                                         <?php
                                                                       }else if($typefiles=="pdf")
                                                                       {
                                                                         ?>
                                                                         <i class="fa fa-file-pdf-o text-warning"></i>
                                                                         <?php

                                                                       }else {
                                                                         ?>
                                                                         <i class="fa fa-file-text-o text-warning"></i>
                                                                         <?php
                                                                       }

                                                                        ?>



                                                                     </div>
                                                                     <div class="p-2 small">
                                                                       <?php
                                                                       if($typefiles=="csv"||$typefiles=="xlsx"||$typefiles=="xls")
                                                                       {
                                                                         ?>

                                                                         <div style="text-align:center"> <span class="label label-success" ><?php echo $filename ?></span> </div>
                                                                         <?php

                                                                       }else if($typefiles=="docx"||$typefiles=="docx")
                                                                       {
                                                                         ?>

                                                                         <div style="text-align:center"> <span class="label label-primary" ><?php echo $filename ?></span> </div>
                                                                         <?php
                                                                       }else if($typefiles=="pdf")
                                                                       {
                                                                         ?>
                                                                         <div style="text-align:center"> <span class="label label-warning" ><?php echo $filename ?></span> </div>
                                                                         <?php

                                                                       }else {
                                                                         ?>

                                                                         <div style="text-align:center"> <span class="label label-warning" ><?php echo $filename ?></span> </div>
                                                                         <?php
                                                                       }
                                                                        ?>
                                                                        <br>

                                                                         <!-- <div class="text-muted" style="text-align:center"><?php //echo L::carnetCaps ?></div> -->

                                                                         <div class="pull-right">

                                                                           <a target="_blank" href="<?php echo $lien ?>" class="btn btn-xs btn-warning"> <i class="fa fa-download"></i> </a>
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                             </div>

                                                   <?php

                                                 endforeach;

                                                  ?>

                                               </div>

                                               <?php
                                             }
                                              ?>


                                         </section>
                                     </div>
                                 </div>





                         </div>
                <!-- </div> -->

              </div>





                    </div>




                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <!-- start chat sidebar -->

                        <div class="chat-sidebar-container" data-close-on-body-click="false">
                        <div class="chat-sidebar">
                          <ul class="nav nav-tabs">
                            <li class="nav-item">
                              <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                                  class="material-icons">
                                  chat</i>Chat
                                <!-- <span class="badge badge-danger">4</span> -->
                              </a>
                            </li>
                          </ul>
                          <div class="tab-content">
                            <!-- Start User Chat -->
                            <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                              id="quick_sidebar_tab_1"> -->
                              <div class="chat-sidebar-chat "
                                >
                              <div class="chat-sidebar-list">
                                <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                                  data-wrapper-class="chat-sidebar-list">
                                  <div class="chat-header">
                                    <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                                  </div>
                                  <ul class="media-list list-items">
                                    <?php

                                    foreach ($onlineUsers as  $valueUsers):
                                      $tofuser=$valueUsers->photo_compte;
                                      if(strlen($tofuser)>0)
                                      {
                                        $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                                      }else {
                                        $lientofuser="../photo/user5.jpg";
                                      }
                                      if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                                      {
                                        ?>
                                        <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                            width="35" height="35" alt="...">
                                          <i class="online dot red"></i>
                                          <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                            <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                            <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                                          </div>
                                        </li>
                                        <?php
                                      }
                                      ?>

                                      <?php
                                    endforeach;
                                     ?>

                                  </ul>
                                  <div class="chat-header">
                                    <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                                  </div>
                                  <ul class="media-list list-items">
                                    <?php

                                    foreach ($offlineUsers as  $valueUsers):
                                      $tofuser=$valueUsers->photo_compte;
                                      if(strlen($tofuser)>0)
                                      {
                                        $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                                      }else {
                                        $lientofuser="../photo/user5.jpg";
                                      }
                                      ?>
                                      <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                          width="35" height="35" alt="...">
                                        <i class="offline dot"></i>
                                        <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                          <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                          <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                                        </div>
                                      </li>
                                      <?php
                                    endforeach;
                                     ?>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <!-- End User Chat -->
                          </div>
                        </div>
                      </div>
                        <!-- end chat sidebar -->
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>

   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
  <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
  <script src="../cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" ></script>
  <script src="../cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
  <script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" ></script>
  <script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" ></script>
  <script src="../cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
   <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
  <script src="../assets2/js/dropify.js"></script>
  <script src="../assets2/plugins/select2/js/select2.js" ></script>
  <script src="../assets2/js/pages/select2/select2-init.js" ></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
  <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

  <script>
  function SetcodeEtab(codeEtab)
  {
   var etape=3;
   $.ajax({
     url: '../ajax/sessions.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
     dataType: 'text',
     success: function (content, statut) {

  window.location.reload();

     }
   });
  }
  function addFrench()
  {
   var etape=1;
   var lang="fr";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

  window.location.reload();

     }
   });
  }

  function addEnglish()
  {
   var etape=1;
   var lang="en";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

  window.location.reload();

     }
   });
  }


  jQuery(document).ready(function() {


   $('#example45').DataTable( {

     "scrollX": true,
     "language": {
         "lengthMenu": "_MENU_  ",
         "zeroRecords": "Aucune correspondance",
         "info": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
         "infoEmpty": "Aucun enregistrement disponible",
         "infoFiltered": "(filtered from _MAX_ total records)",
         "sEmptyTable":"Aucune donnée disponible dans le tableau",
          "sSearch":"Rechercher :",
          "oPaginate": {
     "sFirst":    "Premier",
     "sLast":     "Dernier",
     "sNext":     "Suivant",
     "sPrevious": "Précédent"
   }
   },

       dom: 'Bfrtip',
       buttons: [
           // 'copyHtml5',

           // 'excelHtml5',
           {
             extend: 'excelHtml5',
             title: 'Data export'
             // exportOptions: {
             //               columns: "thead th:not(.noExport)"
             //           }
           }
           // 'csvHtml5',
           // 'pdfHtml5'
       ]
   } );

  $("#classe").select2();
  $("#teatcher").select2();
  $("#classeEtab").select2();
  $("#matiere").select2();
  $("#typesess").select2();


   $("#FormAddCtrl").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

        // matiere:"required",
        // classe:"required",
        // teatcher:"required",
        // coef:"required",
        classe:"required",
        matiere:"required",
        controle:"required",
        coef:"required",
        teatcher:"required",
        datectrl:"required",
        typesess:"required"


      },
      messages: {
        // matiere:"Merci de renseigner la matière",
        // classe:"<?php echo L::PleaseSelectclasserequired ?>",
        // teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
        // coef:"Merci de renseigner le coefficient de la matière"
        classe:"<?php echo L::PleaseSelectclasserequired ?>",
        matiere:"<?php echo L::PleaseselectSubjects ?>",
        controle:"<?php echo L::Controlsrequired ?>",
        coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
        teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
          datectrl:"<?php echo L::PleaseEnterDateControls ?>",
          typesess:"<?php echo L::PeriodRequired ?>"


      },
      submitHandler: function(form) {


  // nous allons verifier un controle similaire n'existe pas
        var etape=1;

         $.ajax({
           url: '../ajax/controle.php',
           type: 'POST',
           async:true,
           data: 'matiere=' + $("#matiere").val()+ '&etape=' + etape+'&classe='+$("#classe").val()+'&teatcher='+$("#teatcher").val()+'&codeEtab='+$("#codeEtab").val()+'&coefficient='+$("#coef").val()+'&datectrl='+$("#datectrl").val()+'&libelle='+$("#controle").val(),
           dataType: 'text',
           success: function (content, statut) {


             if(content==0)
             {
               //cette matière n'existe pas encore pour cette classe

               form.submit();

             }else if(content==1)
             {
               //il est question d'un nouveau professeur pour cette matière
               Swal.fire({
               type: 'warning',
               title: '<?php echo L::WarningLib ?>',
               text: '<?php echo L::ControlAllreadyExist ?>',

               })

             }

           }
         });

             }


           });

           $("#FormAddCtrl1").validate({

             errorPlacement: function(label, element) {
             label.addClass('mt-2 text-danger');
             label.insertAfter(element);
           },
           highlight: function(element, errorClass) {
             $(element).parent().addClass('has-danger')
             $(element).addClass('form-control-danger')
           },
           success: function (e) {
                 $(e).closest('.control-group').removeClass('error').addClass('info');
                 $(e).remove();
             },
              rules:{

                // matiere:"required",
                // classe:"required",
                // teatcher:"required",
                // coef:"required",
                classe:"required",
                matiere:"required",
                controle:"required",
                coef:"required",
                teatcher:"required",
                datectrl:"required",
                typesess:"required"


              },
              messages: {
                // matiere:"Merci de renseigner la matière",
                // classe:"<?php echo L::PleaseSelectclasserequired ?>",
                // teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
                // coef:"Merci de renseigner le coefficient de la matière"


                  classe:"<?php echo L::PleaseSelectclasserequired ?>",
                  matiere:"<?php echo L::PleaseselectSubjects ?>",
                  controle:"<?php echo L::Controlsrequired ?>",
                  coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
                  teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
                    datectrl:"<?php echo L::PleaseEnterDateControls ?>",
                    typesess:"<?php echo L::PeriodRequired ?>"


              },
              submitHandler: function(form) {


        // nous allons verifier un controle similaire n'existe pas
                var etape=1;

                 $.ajax({
                   url: '../ajax/controle.php',
                   type: 'POST',
                   async:true,
                   data: 'matiere=' + $("#matiere").val()+ '&etape=' + etape+'&classe='+$("#classe").val()+'&teatcher='+$("#teatcher").val()+'&codeEtab='+$("#codeEtab").val()+'&coefficient='+$("#coef").val()+'&datectrl='+$("#datectrl").val(),
                   dataType: 'text',
                   success: function (content, statut) {


                     if(content==0)
                     {
                       //cette matière n'existe pas encore pour cette classe

                       form.submit();

                     }else if(content==1)
                     {
                       //il est question d'un nouveau professeur pour cette matière
                       Swal.fire({
                       type: 'warning',
                       title: '<?php echo L::WarningLib ?>',
                       text: '<?php echo L::ControlAllreadyExist ?>',

                       })

                     }

                   }
                 });

                     }


                   });
      });






  </script>    <!-- end js include path -->
  </body>

</html>
