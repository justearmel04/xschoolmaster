<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Student.php');
require_once('../controller/functions.php');
require_once('../class/Teatcher.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$student=new Student();
$localadmins= new Localadmin();
$parents=new ParentX();
$matiere=new Matiere();
$teatcher=new Teatcher();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);

$matieres=$matiere->getAllSubjectTeatchByTeatcherId($_SESSION['user']['IdCompte']);
$controles=$matiere->getAllControleMatiereOfThisTeatcherId($_SESSION['user']['IdCompte']);
//$dataclasses=$classe->getAllClassesByClasseId($classeschoolid);

$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $typesemestrealls=$session->getAllsemestrebyIdsession($sessionencoursid);
  foreach ($typesemestrealls as $value):
    if($value->statut_semes==1)
    {
      $trimestreEncours=$value->id_semes;
    }
  endforeach;

}

$nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);

$nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <!--link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" /-->

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::AddNotesclassesLib ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><!--i class="fa fa-home"></i-->&nbsp;<a class="parent-item" href="index.php"><?php echo L::ManagementNotesLib ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::AddNotesclassesLib ?></li>
                          </ol>
                      </div>
                  </div>
        <!-- start widget -->
        <div class="state-overview">
          <div class="row">
            <?php

                  if(isset($_SESSION['user']['addattendailyok']))
                  {

                    ?>
                    <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                  <?php
                  //echo $_SESSION['user']['addetabok'];
                  ?>
                  <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                     </a>
                  </div-->
          <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
          <script src="../assets/js/sweetalert2.min.js"></script>

              <script>
              Swal.fire({
              type: 'success',
              title: 'Félicitations',
              text: '<?php echo $_SESSION['user']['addattendailyok']; ?>',

              })
              </script>
                    <?php
                    unset($_SESSION['user']['addattendailyok']);
                  }

                   ?>
              </div>
          </div>
        <!-- end widget -->
        <?php

              if(isset($_SESSION['user']['addetabexist']))
              {

                ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <?php
              echo $_SESSION['user']['addetabexist'];
              ?>
              <a href="#" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
                 </a>
              </div>



                <?php
                unset($_SESSION['user']['addetabexist']);
              }

               ?>

  <br/>
  <div class="row">

    <div class="col-sm-12">
        <div class="card-box">
          <div class="card-head">
            <header><?php echo L::ManagementNotesclasseLib ?> </header>
          </div>
          <div class="card-body ">
            <form method="post" id="FormAddNotes" action="addnoteseleves.php">
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                  <!-- text input -->
                  <div class="form-group" style="margin-top:8px;">
                      <label><?php echo L::ClasseMenu ?></label>
                      <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                      <select class="form-control input-height" id="classeEtab" name="classeEtab" style="width:100%;" onchange="searchmatiere1(<?php echo $_SESSION['user']['IdCompte']; ?>)">
                          <option value=""><?php echo L::Selectclasses ?></option>
                          <?php
                          $i=1;
                            foreach ($classes as $value):
                              //compter le nombred'élève de cette classe

                              $nbclassestudents=$classe->getAllStudentOfThisClassesNb($value->id_classe,$value->session_classe);
                              // echo $nbclassestudents;
                              if($nbclassestudents>0)
                              {
                                ?>
                                  <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>
                                <?php
                              }
                            ?>


                            <?php
                                                             $i++;
                                                             endforeach;
                                                             ?>

                      </select>
                  </div>


              </div>
              <div class="col-md-6 col-sm-6">
              <!-- text input -->
              <div class="form-group" style="margin-top:8px;">
                  <label><?php echo L::MatiereMenusingle ?></label>
                  <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                  <!-- <select class="form-control input-height" id="matclasse" name="matclasse" style="width:100%;" onchange='searchcontrole()' > -->
                  <select class="form-control input-height" id="matclasse" name="matclasse" style="width:100%;" >
                      <option value=""><?php echo L::SelectSubjects ?></option>


                  </select>
              </div>


          </div>

          <div class="col-md-6 col-sm-6">
          <!-- text input -->
          <div class="form-group" style="margin-top:8px;">
              <label><?php echo L::EleveMenusingle  ?></label>
              <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
              <!-- <select class="form-control input-height" id="matclasse" name="matclasse" style="width:100%;" onchange='searchcontrole()' > -->
              <select class="form-control input-height" id="eleveid" name="eleveid" style="width:100%;" >
                  <option value=""><?php echo L::SelectOneStudent ?></option>


              </select>
          </div>


      </div>


          <div class="col-md-6 col-sm-6">
          <!-- text input -->
          <div class="form-group" style="margin-top:8px;">
              <label><?php echo L::Evaluation ?></label>
              <input type="text" id="libctrl" name="libctrl"  value="" style="width:100%;">
              <!-- input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..." -->

              <input type="hidden" name="search" id="search" value="1"/>
              <input type="hidden" name="codeEtab" id="codeEtab" value=""/>
              <input type="hidden" id="notetype" name="notetype" value="1">
              <input type="hidden" name="libellesession"  id="libellesession" value="<?php echo $libellesessionencours; ?>">
          </div>


      </div>
              <div class="col-md-3 col-sm-3">

              <button type="submit" class="btn btn-success"  style="width:200px;height:35px;margin-top:35px;text-align:center;"><?php echo L::ManagingNotes ?></button>


          </div>


                </div>


            </form>

            <div class="row" style="" id="affichage">
              <?php
                  if(isset($_POST['search'])&&($_POST['search']==1))
                  {
                      if(isset($_POST['notetype'])&&isset($_POST['classeEtab'])&&isset($_POST['libctrl'])&&isset($_POST['eleveid']))
                      {
                          //nous devons recupérer la liste des elèves de cette classe

                          //$students=$student->getAllstudentofthisclasses($_POST['classeEtab']);
                          // $students=$student->getAllstudentofthisclassesSession($_POST['classeEtab'],$libellesessionencours);
                          $students=$student->getAllstudentofthisclassesSessionId($_POST['eleveid'],$_POST['classeEtab'],$libellesessionencours);

                          $classeInfos=$classe->getInfosofclassesbyId($_POST['classeEtab'],$libellesessionencours);

                          $controleInfos=$_POST['libctrl'];

                            // $tabInfosx=explode("-",$_POST['libctrl']);
                            //
                            // $controleid=$tabInfosx[0];
                            // $matierecontroleId=$tabInfosx[1];
                            // $teatchercontroleid=$tabInfosx[2];

                          ///var_dump($students);notetype

                          // if($_POST['notetype']==1)
                          // {
                          //   // controle
                          //   $controleInfos=$classe->getControleInfosByIdCtrl($controleid,$_POST['classeEtab'],$_POST['codeEtab']);
                          // }else if($_POST['notetype']==2)
                          // {
                          //     // examen
                          // }

                          // var_dump($students);

                      }
                      ?>

                      <div class="offset-md-4 col-md-4"  id="affichage1">
                        <div class="card" style="">
                        <div class="card-body">
                          <h5 class="card-title"></h5>
                          <h4 style="font-style: italic;font-weight: bold;text-align:center;font-size:font-size: large;"><?php echo L::NotesClassing ?></h4>
                          <p class="card-text" style="text-align:center;font-weight: bold;"><?php echo $classeInfos; ?></p>
                          <p class="card-text" style="text-align:center;">(<?php echo $controleInfos?>)</p>

                        </div>
                      </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="affichage2">
                                              <div class="card card-topline-green">
                                                  <div class="card-head">
                                                      <header><?php echo L::NotesClassingbystudent ?></header>
                                                      <div class="tools">
                                                          <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                  <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                  <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                                      </div>
                                                  </div>
                                                  <div class="card-body ">

                                                    <form method="post" action="../controller/notes.php" id="Form">
                                              <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example5"  id="affichage3">
                                                  <thead>
                                                      <tr>
                                                          <!--th style="width:5%">
                                                              #
                                                          </th-->
                                                          <th style="width:20%;text-align:center"> <?php echo L::MatriculestudentTab ?></th>
                                                          <th style="width:50%;text-align:center"> <?php echo L::NamestudentTab ?> </th>
                                                          <th style="width:25%;text-align:center"> <?php echo L::Notelibelle ?> </th>
                                                          <!-- <th> <?php //echo L::NoteObservation ?> </th> -->

                                                      </tr>
                                                  </thead>
                                                  <tbody>
                                                    <?php
                                                    $matricules="";
                                                    $j=0;
                                                    $i=1;

                                                    foreach ($students as $value):

                                                      $matricules=$matricules.$value->idcompte_eleve."*";
                                                    ?>
                                                      <tr class="odd gradeX">
                                                          <!--td>
                                                            <?php //echo $i;?>
                                                          </td-->
                                                          <td> <?php echo $value->matricule_eleve;?></td>
                                                          <td>
                                                              <a href="#"><?php echo $value->nom_eleve." ".$value->prenom_eleve;?> </a>
                                                          </td>
                                                          <td style="text-align:center">
                                                              <input type="number" min=0 max=20 name="note<?php echo $value->idcompte_eleve;?>" id="note<?php echo $value->idcompte_eleve;?>" style="width:100px;text-align:center" onclick="erasedNote(<?php echo $value->idcompte_eleve;?>)" />
                                                              <p id="messageNote<?php echo $value->idcompte_eleve;?>"></p>
                                                          </td>
                                                          <!-- <td>

                                                                <textarea name="obser<?php //echo $value->idcompte_eleve;?>" id="obser<?php //echo $value->idcompte_eleve;?>" rows="3" cols="50" onclick="erasedObserv(<?php //echo $value->idcompte_eleve;?>)"></textarea>
                                                                <p id="messageObserv<?php //echo $value->idcompte_eleve;?>"></p>
                                                          </td> -->

                                                      </tr>

                                                      <?php
                                                         $i++;
                                                         $j++;
                                                             endforeach;
                                                           ?>



                                                  </tbody>
                                              </table>

                                              <?php
                                              //echo $matricules;
                                              $tabMat=explode("*",$matricules);
                                              $nb=count($tabMat);
                                              $matclasse=$_POST['matclasse'];

                                              $tabmatclasse=explode("-",$matclasse);

                                              ?>
                                              <input type="hidden" name="studentmat" id="studentmat" value="<?php echo $matricules;?>"/>
                                              <input type="hidden" name="etape" id="etape" value="8"/>

                                              <input type="hidden" name="nbstudent" id="nbstudent" value="<?php echo $j;?>"/>

                                              <input type="hidden" name="classeId" id="classeId" value="<?php echo $_POST['classeEtab']?>"/>
                                              <input type="hidden" name="typenote" id="typenote" value="<?php echo $_POST['notetype']?>"/>
                                              <input type="hidden" name="libellenote" id="libellenote" value="<?php echo $_POST['libctrl']?>"/>
                                              <input type="hidden" name="idtypenote" id="idtypenote" value="1"/>
                                              <input type="hidden" name="typesession" id="typesession" value="<?php echo $trimestreEncours ?>">
                                              <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $_POST['codeEtab'];?>"/>
                                              <input type="hidden" name="matiereid" id="matiereid" value=" <?php echo $tabmatclasse[0];?> ">
                                              <input type="hidden" name="teatcherid" id="teatcherid" value=" <?php echo  $tabmatclasse[1];?> ">
                                              <input type="hidden" name="sessionlibelle" id="sessionlibelle" value="<?php echo $_POST['libellesession']; ?>">
                                              <center><button type="submit"  onclick="check()" class="btn btn-success"><i class="fa fa-check-circle"></i><?php echo L::Gradevalider ?></button></center>
                                            </form>

                                          </div>
                                              </div>
                                          </div>


                      <?php
                  }
               ?>



                      </div>


          </div>
        </div>
      </div>
  </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- calendar -->
     <!--script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script-->
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   $("#classeEtab").select2();
   $("#matclasse").select2();
    $("#eleveid").select2();

    $("#classeEtab1").select2();
    $("#matiere1").select2();
    $("#libctrl1").select2();

    function addFrench()
    {
      var etape=1;
      var lang="fr";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

    function addEnglish()
    {
      var etape=1;
      var lang="en";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

   function autorise()
   {
       $("#Form").submit();
   }

   function autorise1()
   {
       $("#FormE").submit();
   }

   function check()
   {
     var tabidstudent=$("#studentmat").val();
     var nbligne=$("#nbstudent").val();
     var tab=$("#studentmat").val().split("*");
     var i;

     // var note=$("#note"+tab[i]).val()

     for(i=0;i<nbligne;i++)
     {
         var note=$("#note"+tab[i]).val();
         var obser=$("#obser"+tab[i]).val();
     event.preventDefault();


         if(note==""||obser=="")
         {
           if(note=="")
           {
             document.getElementById("messageNote"+tab[i]).innerHTML = "<font color=\"red\"><?php echo L::PleaseAddNotes ?></font>";

           }

           if(note=="")
           {
             document.getElementById("messageObserv"+tab[i]).innerHTML = "<font color=\"red\"><?php echo L::PleaseAddObservations ?></font>";

           }
         }else {

           autorise();
         }


     }
   }

   function check1()
   {
     var tabidstudent=$("#studentmat").val();
     var nbligne=$("#nbstudent").val();
     var tab=$("#studentmat").val().split("*");
     var i;

     // var note=$("#note"+tab[i]).val()

     for(i=0;i<nbligne;i++)
     {
         var note=$("#noteE"+tab[i]).val();
         var obser=$("#obserE"+tab[i]).val();
     event.preventDefault();


         if(note==""||obser=="")
         {
           if(note=="")
           {
             document.getElementById("messageNoteE"+tab[i]).innerHTML = "<font color=\"red\"><?php echo L::PleaseAddNotes ?></font>";

           }

           if(note=="")
           {
             document.getElementById("messageObservE"+tab[i]).innerHTML = "<font color=\"red\"><?php echo L::PleaseAddObservations ?></font>";

           }
         }else {
           autorise1();
         }


     }
   }

   function erasedNote(id)
   {
   document.getElementById("messageNote"+id).innerHTML = "";
   }

   function erasedObserv(id)
   {
   document.getElementById("messageObserv"+id).innerHTML = "";
   }

   function erasedNoteE(id)
   {
   document.getElementById("messageNoteE"+id).innerHTML = "";
   }

   function erasedObservE(id)
   {
   document.getElementById("messageObservE"+id).innerHTML = "";
   }

   function searchcontrole()
   {
     var classeEtab=$("#FormAddNotes #classeEtab").val();
     // var codeEtab=$("#FormAddNotes #codeEtab").val();
     var codeEtab="<?php echo $codeEtabsession ?>";

     var matiere=$("#FormAddNotes #matclasse").val();

     //nous allons chercher la liste des controles de cette classe dont le statut est à 0
     var etape=2;

     $.ajax({

       url: '../ajax/controle.php',
       type: 'POST',
       async:true,
        data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab+'&matiere='+matiere,
        dataType: 'text',
        success: function (content, statut) {

          $("#FormAddNotes #libctrl").html("");
          $("#FormAddNotes #libctrl").html(content);

        }

     });

   }

   function searchmatiere2(id)
   {
     var classeEtab=$("#FormAddNotesE #classeEtab1").val();
     // var codeEtab="";
     var codeEtab="<?php echo $codeEtabsession ?>";
     var etape=4;

     $.ajax({

       url: '../ajax/classe.php',
       type: 'POST',
       async:true,
        data:'etape=' + etape+'&classe='+classeEtab,
        dataType: 'text',
        success: function (content, statut) {

        // $("#FormAddNotesE #codeEtab1").val(content);
        $("#FormAddNotesE #codeEtab1").val(codeEtab);

        var etape1=8;

        $.ajax({

          url: '../ajax/matiere.php',
          type: 'POST',
          async:true,
           // data:'etape=' + etape1+'&classe='+classeEtab+'&code='+content+'&teatcher='+id,
           data:'etape=' + etape1+'&classe='+classeEtab+'&code='+codeEtab+'&teatcher='+id,
           dataType: 'text',
           success: function (content, statut) {

             $("#FormAddNotesE #matiere1").html("");
             $("#FormAddNotesE #matiere1").html(content);

           }

        })

        }

     });


   /*
     $.ajax({

       url: '../ajax/matiere.php',
       type: 'POST',
       async:true,
        data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab,
        dataType: 'text',
        success: function (content, statut) {

          $("#matiere1").html("");
          $("#matiere1").html(content);

        }

     });*/
   }



   function searchexams(id)
   {
     var classeEtab=$("#FormAddNotesE #classeEtab1").val();
     // var codeEtab=$("#FormAddNotesE #codeEtab1").val();
     var codeEtab="<?php echo $codeEtabsession ?>";
     var matiere=$("#FormAddNotesE #matiere1").val();
     var etape=5;



     $.ajax({

       url: '../ajax/examen.php',
       type: 'POST',
       async:true,
        data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab+'&matiere='+matiere+'&teatcher='+id,
        dataType: 'text',
        success: function (content, statut) {

          $("#libctrl1").html("");
          $("#libctrl1").html(content);

        }

     });

   }


   function searchmatiere1(id)
   {
     var classeEtab=$("#FormAddNotes #classeEtab").val();
     var codeEtab="<?php echo $codeEtabsession ?>";
     var etape=4;

     //retrouver le code etablissement de cette classe

     $.ajax({

       url: '../ajax/classe.php',
       type: 'POST',
       async:true,
        data:'etape=' + etape+'&classe='+classeEtab,
        dataType: 'text',
        success: function (content, statut) {

        // $("#FormAddNotes #codeEtab").val(content);
        $("#FormAddNotes #codeEtab").val(codeEtab);


        //rechercher les matieres dispenser dans cette classe par ce professeur
        var etape1=8;
        $.ajax({

          url: '../ajax/matiere.php',
          type: 'POST',
          async:true,
           // data:'etape=' + etape1+'&classe='+classeEtab+'&code='+content+'&teatcher='+id,
           data:'etape=' + etape1+'&classe='+classeEtab+'&code='+codeEtab+'&teatcher='+id,
           dataType: 'text',
           success: function (content, statut) {

             $("#FormAddNotes #matclasse").html("");
             $("#FormAddNotes #matclasse").html(content);


             //nous allons chercher la liste des élèves de cette classe

             var etape=6;
             var session="<?php echo $libellesessionencours ?>";

             $.ajax({
               url: '../ajax/admission.php',
               type: 'POST',
               async:true,
                // data:'etape=' + etape1+'&classe='+classeEtab+'&code='+content+'&teatcher='+id,
                data:'etape=' + etape+'&classe='+classeEtab+'&session='+session,
                dataType: 'text',
                success: function (content, statut) {
                  $("#FormAddNotes #eleveid").html("");
                  $("#FormAddNotes #eleveid").html(content);
                }
             });


           }

        });

        }

     });

     // $.ajax({
     //
     //   url: '../ajax/matiere.php',
     //   type: 'POST',
     //   async:true,
     //    data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab,
     //    dataType: 'text',
     //    success: function (content, statut) {
     //
     //      $("#matclasse").html("");
     //      $("#matclasse").html(content);
     //
     //    }
     //
     // });

   }
/*
   $("#classeEtab").select2();
   $("#notetype").select2();
   $("#libctrl").select2();
   $("#matclasse").select2();

   function searchDesignation()
   {
     var classeEtab=$("#classeEtab").val();
     var codeEtab=$("#codeEtab").val();
     var matiere=$("#matclasse").val();
     var typenote=$("#notetype").val();
     var etape=2;
     //nous allons verifier si nous avons des notes pour ce controle ou examen
     $.ajax({

       url: '../ajax/designation.php',
       type: 'POST',
       async:true,
        data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab+'&matiere='+matiere+'&typenote='+typenote,
        dataType: 'text',
        success: function (content, statut) {




          $("#libctrl").html("");
          $("#libctrl").html(content);

        }

     });

   }

   function searchcodeEtab(id)
   {
     var classe=$("#classeEtab").val();
     var teatcherId=id;
     var etape=7;
     var matiere=$("#matclasse").val();

     $.ajax({

          url: '../ajax/matiere.php',
          type: 'POST',
          async:true,
          data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
          dataType: 'text',
          success: function (content, statut) {

             $("#FormNotes #codeEtab").val(content);

          }
        });

   }

   function searchmatiere(id)
   {

       var classe=$("#classeEtab").val();
       var teatcherId=id;
       var etape=6;


     $.ajax({

          url: '../ajax/matiere.php',
          type: 'POST',
          async:true,
          data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe,
          dataType: 'text',
          success: function (content, statut) {


            $("#matclasse").html("");
            $("#matclasse").html(content);

          }
        });
   }
*/

   $(document).ready(function() {

     $("#FormAddNotes").validate({
       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
      },
      success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },
       rules:{


         classeEtab:"required",
         datepre:"required",
         notetype:"required",
         classeEtab:"required",
         matclasse:"required",
         libctrl:"required",
         eleveid:"required"



       },
       messages: {
         classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
         datepre:"<?php echo L::PleaseSelectdateprerequired ?>",
         notetype:"<?php echo L::PleaseSelectnotetyperequired ?>",
         classeEtab:"<?php echo L::PleaseSelectclasseOnerequired ?>",
         libctrl:"<?php echo L::PleaseEnterLibellenote ?>",
         matclasse:"<?php echo L::SubjectSelectedrequired ?>",
         eleveid:"<?php echo L::PleaseSelectOneStudent ?>"

       },
       submitHandler: function(form) {
         form.submit();
       }
     });

     $("#FormAddNotesE").validate({
       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
      },
      success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },
       rules:{



         classeEtab1:"required",
         matiere1:"required",
         libctrl1:"required"



       },
       messages: {

         classeEtab1:"<?php echo L::PleaseSelectclasseOnerequired ?>",
         matiere1:"<?php echo L::SubjectSelectedrequired ?>",
         libctrl1:"<?php echo L::DesignationSelectedrequired ?>"

       },
       submitHandler: function(form) {
         form.submit();
       }
     });

   });

   </script>
    <!-- end js include path -->
  </body>

</html>
