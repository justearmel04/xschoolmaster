<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Student.php');
require_once('../controller/functions.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Teatcher.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();
$teatcher= new Teatcher();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$matiere=new Matiere();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);

$matieres=$matiere->getAllSubjectTeatchByTeatcherId($_SESSION['user']['IdCompte']);
$controles=$matiere->getAllControleMatiereOfThisTeatcherId($_SESSION['user']['IdCompte']);

$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
}
$nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);

$programmes=$etabs->getAllprogrammesOfTeatcherClassesAndpgrmeId($_SESSION['user']['IdCompte'],$_GET['programme'],$libellesessionencours);
//$dataclasses=$classe->getAllClassesByClasseId($classeschoolid);

$tabprogramme=explode("*",$programmes);

$nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Modification Programmes académiques</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li>&nbsp;<a class="parent-item" href="#">Gestion des Matières</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Modification Programme académique</li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">
              <?php

                    if(isset($_SESSION['user']['addprogra']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
            <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['addprogra'] ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addprogra']);
                    }

                     ?>
					      </div>
						</div>
					<!-- end widget -->



          <div class="row">

            <div class="col-sm-12">
								<div class="card-box">
									<div class="card-head">
										<header></header>
									</div>
									<div class="card-body ">
						            <div class = "mdl-tabs mdl-js-tabs">
						               <div class = "mdl-tabs__tab-bar tab-left-side">

						                  <a href = "#tab5-panel" class = "mdl-tabs__tab is-active">Modifier programme</a>

						               </div>

						               <div class = "mdl-tabs__panel is-active p-t-20" id = "tab5-panel">
                             <div class="row">
                                 <div class="col-md-12 col-sm-12">
                                     <div class="card card-box">
                                         <div class="card-head">
                                             <header></header>

                                         </div>

                                         <div class="card-body" id="bar-parent">
                                             <form  id="FormAddAcademique" class="form-horizontal" action="../controller/academique.php" method="post" enctype="multipart/form-data" >
                                                 <div class="form-body">

                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3">Programme
                                                           <span class="required">  *</span>
                                                       </label>
                                                       <div class="col-md-5">
                                                         <input type="text" class="form-control input-height" placeholder="Entrer le libellé du programme" name="programme" id="programme" value="<?php echo $tabprogramme[1]; ?>" ></div>
                                                         <input type="hidden" name="etape" id="etape" value="2" />
                                                         <input type="hidden" id="codeEtab" name="codeEtab" value="<?php echo $tabprogramme[6]; ?>"/>
                                                   </div>

                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3">Description
                                                           <span class="required">  *</span>
                                                       </label>
                                                       <div class="col-md-5">
                                                         <textarea class="form-control" rows="4" name="descri" id="descri" placeholder="Entrer la description du programme"><?php echo $tabprogramme[2]; ?></textarea>
                                                       </div>

                                                   </div>

                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                                           <span class="required"> * </span>
                                                       </label>
                                                       <div class="col-md-5">
                                                         <select class="form-control input-height" id="classeEtab" name="classeEtab" style="width:100%;" onchange="searchmatiere(<?php echo $_SESSION['user']['IdCompte']; ?>)" readonly>
                                                             <option value=""><?php echo L::Selectclasses ?></option>
                                                             <?php
                                                             $i=1;
                                                               foreach ($classes as $value):
                                                               ?>
                                                               <option <?php if($tabprogramme[5]==$value->id_classe){echo "selected";} ?> value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                               <?php
                                                                                                $i++;
                                                                                                endforeach;
                                                                                                ?>

                                                         </select>
                                                         <input type="hidden" name="classeEtab" id="classeEtab" value="<?php echo $tabprogramme[5]; ?>" />
                                                       </div>

                                                   </div>
                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3">Matière
                                                           <span class="required">  *</span>
                                                       </label>
                                                       <div class="col-md-5">
                                                         <select readonly class="form-control input-height" id="matclasse" name="matclasse" style="width:100%;" onchange="searchcodeEtab(<?php echo $_SESSION['user']['IdCompte']; ?>)">
                                                             <option value=""><?php echo L::SelectSubjects ?></option>
                                                             <option value="<?php echo $tabprogramme[4]; ?>" selected><?php echo $matiere->getMatiereLibelleByIdMat($tabprogramme[4],$tabprogramme[6]) ?></option>

                                                         </select>
                                                         <input type="hidden" name="matclasse" id="matclasse" value="<?php echo $tabprogramme[4]; ?>" />
                                                       </div>

                                                   </div>

                                                   <div class="form-group row">
                                                       <label class="control-label col-md-3"><?php echo L::File?>
                                                           <span class="required">  *</span>
                                                       </label>
                                                       <div class="col-md-5">
                                                         <?php
                                                         //determiner le lien du fichier
                                                         $libelleclasse=$classe->getInfosofclassesbyId($tabprogramme[5],$libellesessionencours);

                                                          $lien="../programmes/".$tabprogramme[8]."/".$libelleclasse."/".$tabprogramme[7];
                                                          $tabfic=explode(".",$tabprogramme[7]);
                                                         $type="";
                                                         if($tabfic[1]=="pdf")
                                                         {
                                                           $type="../assets/img/pdf.png";
                                                         }else if(($tabfic[1]=="doc")||($tabfic[1]=="docx"))
                                                         {
                                                           $type="../assets/img/word.png";
                                                         }
                                                          ?>
                                                      <input type="file" id="fichier" name="fichier" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="<?php echo $lien; ?>" data-allowed-file-extensions="doc docx pdf" />
                                                      <input type="hidden" name="oldfichier" id="oldfichier" value="<?php echo $tabprogramme[7]; ?>">
                                                      <input type="hidden" name="programmeid"  id="programmeid"  value="<?php echo $tabprogramme[0]; ?>">
                                                      <input type="hidden" name="years" id="years" value="<?php echo $tabprogramme[8]; ?>">
                                                      <input type="hidden" name="classelibelle" id="classelibelle" value="<?php echo $libelleclasse; ?>">
                                                      <input type="hidden" name="teatcherid" id="teatcherid" value="<?php echo $_SESSION['user']['IdCompte']; ?>">
                                                       </div>

                                                   </div>




                               <div class="form-actions">
                                                     <div class="row">
                                                         <div class="offset-md-3 col-md-9">
                                                             <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                                             <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                         </div>
                                                       </div>
                                                    </div>
                             </div>
                                             </form>
                                         </div>
                                     </div>
                                 </div>
                             </div>
						               </div>

						            </div>
									</div>
								</div>
							</div>

          </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
  <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
  <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
  <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
  <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
  <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
  <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
  <script src="../assets2/js/pages/table/table_data.js" ></script>
  <script src="../assets2/plugins/select2/js/select2.js" ></script>
  <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <!-- calendar -->
     <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
  <script src="../assets2/plugins/moment/moment.min.js" ></script>
  <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
  <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
  <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
  <script src="../assets2/js/dropify.js"></script>
   <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }
   $("#fichier").dropify({
     messages: {
         "default": "Merci de selectionner le fichier",
         "replace": "Modifier le fichier",
         "remove" : "Supprimer le fichier",
         "error"  : "Erreur"
     }
   });

$("#matclasse").select2({disabled: true});
$("#classeEtab").select2({disabled: true});

function searchcodeEtab(id)
{
  var classe=$("#classeEtab").val();
  var teatcherId=id;
  var etape=7;
  var matiere=$("#matclasse").val();

  $.ajax({

       url: '../ajax/matiere.php',
       type: 'POST',
       async:true,
       data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
       dataType: 'text',
       success: function (content, statut) {

          $("#FormAddAcademique #codeEtab").val(content);

       }
     });

}

function searchmatiere(id)
{

    var classe=$("#classeEtab").val();
    var teatcherId=id;
    var etape=6;


  $.ajax({

       url: '../ajax/matiere.php',
       type: 'POST',
       async:true,
       data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe,
       dataType: 'text',
       success: function (content, statut) {


         $("#matclasse").html("");
         $("#matclasse").html(content);

       }
     });
}

   $(document).ready(function() {

$("#FormAddAcademique").validate({

  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
 },
 highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
 },
 success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{

    programme:"required",
    descri:"required",
    classeEtab:"required",
    matclasse:"required"


  },
messages: {
  programme:"Merci de renseigner le libellé du programme",
  descri:"Merci de renseigner la description du programme",
  classeEtab:"<?php echo L::PleaseSelectclasseOnerequired ?>",
  matclasse:"Merci de <?php echo L::SelectSubjects ?>"

},
submitHandler: function(form) {

//nous allons verifier si le programme n'existe pas dans la base de données

 var etape=1;
 var teatcher="<?php echo $_SESSION['user']['IdCompte']; ?>";
 $.ajax({
   url: '../ajax/programme.php',
   type: 'POST',
   async:false,
   data: 'programme=' + $("#programme").val()+ '&classeEtab=' + $("#classeEtab").val() + '&matclasse=' + $("#matclasse").val() + '&etape=' + etape+'&codeEtab='+$("#codeEtab").val()+'&teatcher='+teatcher,
   dataType: 'text',
   success: function (content, statut) {
form.submit();
/*
     if(content==0)
     {
       //le compte n'existe pas dans la base on peut l'ajouter

       form.submit();
     }else if(content==1)
     {
       Swal.fire({
       type: 'warning',
       title: '<?php echo L::WarningLib ?>',
       text: 'Ce Programme existe dejà dans le système',

       })
     }*/

   }


 });


}

});

   });

   </script>
    <!-- end js include path -->
  </body>

</html>
