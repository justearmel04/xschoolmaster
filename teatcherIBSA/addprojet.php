<<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Teatcher.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();




$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$matiere=new Matiere();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$localadmins= new Localadmin();
$parents=new ParentX();
$classe=new Classe();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$_SESSION['user']['codeEtab']=$codeEtabsession;

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $_SESSION['user']['session']=$libellesessionencours;
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];


  $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
}else {
  $nbclasse=0;
}

$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);



if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();

$nbnotificationstandby=$parents->getParentnotificationstandbyNb($compteuserid);
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($compteuserid);


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
    <!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!--bootstrap -->
    <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

    <!-- data tables -->
    <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>

    <!-- Material Design Lite CSS -->
    <link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
    <link href="../assets2/css/material_style.css" rel="stylesheet">

    <!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- wizard -->
    	<link rel="stylesheet" href="../assets2/css/pages/steps.css">

    <!-- Theme Styles -->
      <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
      <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="../assets2/css/pages/inbox.min.css" rel="stylesheet" type="text/css" />

      <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <!-- Theme Styles -->

    <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />





 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::espaceProjet ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="#"><?php echo L::Projet ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::espaceProjet ?></li>
                          </ol>
                      </div>
                  </div>
					<!-- start widget -->
                  <div class="inbox">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-topline-gray">
                                <div class="card-body no-padding height-9">
									                 <div class="row">

			                            <div class="col-md-9">
			                                <div class="inbox-body">
                                          <form class="" action="../controller/projet.php" method="post" id="FormAddExam" >
                                            <div class="form-group">
                                               <label class="control-label col-md-12"> <?php echo L::datedebProjet ?>
                                                   <span class="required"> * </span>
                                               </label>

                                               <div class="col-md-12">
                                                 <input type="hidden" name="etape" id="etape"  value="1">
                                                 <input type="date"   style="width:100%"  id="etape" class="control-label col-md-12" name="date_deb" value="">
                                                 <input type="hidden" name="teatcher" id="teatcher" value="<?php echo $compteuserid; ?>">
                                                  <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $_SESSION['user']['codeEtab']; ?>">
                                                  <input type="hidden" name="status" value="0">
                                                  <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                                 </div>
                                        </div>
                                        <div class="form-group">
                                           <label class="control-label col-md-12"> <?php echo L::datefinProjet ?>
                                               <span class="required"> * </span>
                                           </label>
                                           <div class="col-md-12">
                                            <input type="date"  style="width:100%"  id="etape" class="control-label col-md-12" name="date_fin" value="">
                                           </div>
                                        </div>
                                        <div class="form-group" id="rowprecision">
                                         <label class="control-label col-md-12"> <?php echo L::nomProjet ?>
                                             <span class="required"> * </span>
                                         </label>
                                         <div class="col-md-12">
                                          <input size="32" maxlength="225"  type="text"  style="width:100%"  id="etape" class="control-label col-md-12" name="title" value="">
                                           </div>
                                       </div>
                                    </div>
		                                    <div class="inbox-body no-pad">
		                                        <div class="mail-list">
		                                            <div class="compose-mail">
                                                   <div class="compose-editor">
                                                     <textarea class="form-control" name="content" id="summernote" style="height: 400px;"></textarea>
		                                                    </div>
		                                              <div class="btn-group margin-top-20 " style="text-align:center">
                                                  <button class="btn btn-success btn-sm margin-right-10" onclick="changeEtape(1)"><i class="fa fa-check"></i> <?php echo L::Added ?></button>
		                                            </div>
		                                        </div>
		                                      </div>
                                        </form>
		                                 </div>
			                            </div>
			                        </div>
								             </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- corps -->

              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
    <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
	<script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js"></script>
    <!-- bootstrap -->
<script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
<script src="../assets2/plugins/select2/js/select2.js" ></script>
<script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <!-- wizard -->
  <script src="../assets2/plugins/steps/jquery.steps.js" ></script>
 <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
 <script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
 <script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
     <!-- calendar -->
  <script src="../assets2/plugins/moment/moment.min.js" ></script>
  <script src="../assets2/js/app.js" ></script>
  <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/jquery-dateformat.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
  <script src="../assets/js/formatter/jquery.formatter.min.js"></script>
  <script src="../assets2/plugins/summernote/summernote.js" ></script>




  <!-- start widget -->
  <div class="state-overview">
    <div class="row">
      <?php

            // if(isset($_SESSION['user']['addattendailyok']))
            // {

              ?>
              <!--div class="alert alert-success alert-dismissible fade show" role="alert">
            <?php
            //echo $_SESSION['user']['addetabok'];
            ?>
            <a href="#" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
               </a>
            </div-->


<!--
               <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
               <script src="../assets/js/sweetalert2.min.js"></script>

                   <script>
                   Swal.fire({
                   type: 'success',
                   title: 'Félicitations',
                   text: '<?php //echo $_SESSION['user']['addprojetok']; ?>',

                   })
                   </script>


                   </div>
               </div> -->
               <!-- end widget -->



  <script type="text/javascript">

  function SetcodeEtab(codeEtab)
  {
    var etape=3;
    $.ajax({
      url: '../ajax/sessions.php',
      type: 'POST',
      async:false,
      data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
      dataType: 'text',
      success: function (content, statut) {

  window.location.reload();

      }
    });
  }
  function addFrench()
  {
    var etape=1;
    var lang="fr";
    $.ajax({
      url: '../ajax/langue.php',
      type: 'POST',
      async:false,
      data: 'etape=' + etape+ '&lang=' +lang,
      dataType: 'text',
      success: function (content, statut) {

  window.location.reload();

      }
    });
  }

  function addEnglish()
  {
    var etape=1;
    var lang="en";
    $.ajax({
      url: '../ajax/langue.php',
      type: 'POST',
      async:false,
      data: 'etape=' + etape+ '&lang=' +lang,
      dataType: 'text',
      success: function (content, statut) {

  window.location.reload();

      }
    });
  }

  function changeEtape(id)
  {

    // var sauvegarde=$("#sauvegarde").val();

    // const code = $('[name="summernote"]').summernote('code');
    // const code1=$('#summernote').val();
    //
    // alert(code+ " "+code1);

    $("#etape").val(id);
  }


  function checkobjet()
  {
    var objet=$("#objet").val();
    var tabobjet=objet.split("-");

    var objetid=tabobjet[0];

    if(objetid==8)
    {
        // createOther();
        $("#rowother").show();
    }else {
      // deleteOther();
      $("#rowother").hide();
    }

  }

  function restauration()
  {
    $("#rowclasseEtab").hide();
    $("#rowprecisioneleves").hide();
    $("#rowprecisiondestinataires").hide();
  }

  function selectiondestinataires()
  {
    restauration();
    var destinataires=$("#destinataires").val();
    if(destinataires=="Admin_locale")
    {
      $("#rowclasseEtab").hide();
    }else {
      $("#rowclasseEtab").show();
    }
  }

  function createdestinatairesTeatchers()
  {
    var classeselected=$("#classeEtab").val();
    var session=$("#libellesession").val();
    var codeEtab="<?php echo $_SESSION['user']['codeEtab'] ?>";
    var etape=32;
    $.ajax({
      url: '../ajax/admission.php',
      type: 'POST',
      async:true,
      data: 'classe=' +classeselected+ '&etape=' + etape+'&session='+session+'&codeEtab='+codeEtab,
      dataType: 'text',
      success: function (content, statut) {

        $("#precisiondestinataires").html("");
        $("#precisiondestinataires").html(content);



      }
    });
  }

  function createdestinatairesRespos()
  {
    // var classeselected=$("#classeEtab").val();
    var session=$("#libellesession").val();
    var codeEtab="<?php echo $_SESSION['user']['codeEtab'] ?>";
    var etape=33;
    $.ajax({
      url: '../ajax/admission.php',
      type: 'POST',
      async:true,
      data: 'etape=' + etape+'&session='+session+'&codeEtab='+codeEtab,
      dataType: 'text',
      success: function (content, statut) {

        $("#precisiondestinataires").html("");
        $("#precisiondestinataires").html(content);



      }
    });
  }

  function createdestinatairesEleves()
  {

    //nous allons chercher la liste des eleves de ces classes

    var classeselected=$("#classeEtab").val();
    var session=$("#libellesession").val();
    var codeEtab="<?php echo $_SESSION['user']['codeEtab'] ?>";
    var etape=31;
    $.ajax({
      url: '../ajax/admission.php',
      type: 'POST',
      async:true,
      data: 'classe=' +classeselected+ '&etape=' + etape+'&session='+session+'&codeEtab='+codeEtab,
      dataType: 'text',
      success: function (content, statut) {

        $("#eleves").html("");
        $("#eleves").html(content);



      }
    });


  }


  function selectionprecis()
  {
    var precis=$("#precis").val();
    var destinataires=$("#destinataires").val();

    if(precis==2)
    {
       if(destinataires=="Parent")
       {
         //liste des eleves de cette classe
         $("#rowprecisioneleves").show();

         //nous allons chercher la liste des élèves de ces classes
         createdestinatairesEleves();


       }else if(destinataires=="Teatcher")
       {
         $("#rowprecisioneleves").hide();
         $("#rowprecisiondestinataires").show();

         createdestinatairesTeatchers();

       }else if(destinataires=="Admin_locale")
       {
         $("#rowprecisioneleves").hide();
         $("#rowprecisiondestinataires").show();
         createdestinatairesRespos();
         //liste des responsables de l'établissement
       }
    }else if(precis==1)
    {
      $("#rowprecisioneleves").hide();
      $("#rowprecisiondestinataires").hide();
    }

    // if(destinataires=="Parent" && precis==2)
    // {
    //   //liste des enfants de cette classe
    //   // $("#rowclasseEtab").hide();
    //   $("#rowprecisioneleves").show();
    // }else if(destinataires=="Teatcher" && precis==1)
    // {
    //   //liste des enseignants de cette classe
    // }else if(destinataires=="Admin_locale" && precis==1)
    // {
    //   //liste des responsables de l'établissement
    // }

  }

   jQuery(document).ready(function() {

     selectiondestinataires();
     var form = $("#FormAddExam").show();

     form.validate({
       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
     },
     highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
     },
     success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },

       rules:{
         classeEtab:"required",
         matiere:"required",
         classe:"required",
         teatcher:"required",
         coef:"required",
         examen:"required",
         datedeb:"required",
         datefin:"required",
         typesess:"required",
         montantAct:"required",
         libelactivity:"required",
         message:"required",
         denomination:"required",
         typeactivite:"required",
         locationactivite:"required",
         descripactivite:"required",
         respoactivite:"required",
         contactrespo:"required",
         destinataires:"required",
         eleves:"required",
         dateobjet:"required",
         objet:"required",
         commentaire:"required",
         // summernote:{
         //   'required':{
         //     depends:function(element){
         //       // const code = $('#summernote').summernote('code');
         //       // return ($('#summernote').val()=='<p><br></p>');
         //       return ($('#summernote').summernote('code') == "" || $('#summernote').summernote('code')== "<p><br></p>");
         //     }
         //   }
         // }





       },
       messages: {
         classeEtab:"<?php echo L::PleaseSelectaumoinsClasse ?>",
         matiere:"<?php echo L::PleaseEnterMatiere ?>",
         classe:"<?php echo L::PleaseSelectclasserequired ?>",
         teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
         coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
         examen:"<?php echo L::PleaseEnterExamLib ?>",
         datedeb:"<?php echo L::PleaseEnterParascoActivityDateStart ?>",
         datefin:"<?php echo L::PleaseEnterParascoActivityDateEnd ?>",
         typesess:"<?php echo L::PeriodRequired ?>",
         montantAct:"<?php echo L::PleaseEnterParascoActivityAmount ?>",
         libelactivity:"<?php echo L::PleaseEnterParascoActivityName ?>",
         message:"<?php echo L::PleaseEnterActivityMesaage ?>",
         denomination:"<?php echo L::PleaseEnterActivityName ?>",
         typeactivite:"<?php echo L::PleaseEnterActivityType ?>",
         locationactivite:"<?php echo L::PleaseEnterActivityLocation ?>",
         descripactivite:"<?php echo L::PleaseEnterActivityDescription ?>",
         respoactivite:"<?php echo L::PleaseEnterRespoNameMessage ?>",
         contactrespo:"<?php echo L::PleaseEnterContactRespoMessage ?>",
         destinataires:"<?php echo L::PleaseSelectaumoinsDestinatairesMessage ?>",
         eleves:"<?php echo L::PleaseSelectStudents ?>",
         dateobjet:"<?php echo L::ControlsDaterequired ?>",
         objet:"<?php echo L::PleaseSelectObjetMessage ?>",
         commentaire:"<?php echo L::PleaseEnterCommentMessage ?>",
         // summernote:"Merci de renseigner le message"


       }
     });

     $("#rowclasseEtab").hide();
     $("#rowprecisioneleves").hide();
     $("#objet").select2();
     $("#rowother").hide();
     $("#rowprecisiondestinataires").hide();
     $("#destinataires").select2();
     $("#precis").select2();
     $("#classeEtab").select2({
       tags: true,
     tokenSeparators: [',', ' ']
     });
     $("#precisiondestinataires").select2({
       tags: true,
     tokenSeparators: [',', ' ']
     });

     $("#eleves").select2({
       tags: true,
     tokenSeparators: [',', ' ']
     });
     $('[name="content"]')
        .summernote({
          placeholder: '',
          tabsize: 2,
          height: 200,
          fontSize: 20,

      toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']]
      ]
        });
 $('#summernote').summernote({
       placeholder: '',
       tabsize: 2,
       height: 200,
       fontSize: 20,

   toolbar: [
     // [groupName, [list of button]]
     ['style', ['bold', 'italic', 'underline', 'clear']],
     ['font', ['strikethrough', 'superscript', 'subscript']],
     ['fontsize', ['fontsize']],
     ['color', ['color']],
     ['para', ['ul', 'ol', 'paragraph']],
     ['height', ['height']]
   ]

     });



   });

</script>
    <!-- end js include path -->
  </body>

</html>
