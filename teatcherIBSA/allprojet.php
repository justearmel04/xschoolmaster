<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Student.php');
require_once('../class/User.php');
require_once('../class/Projet.php');
require_once('../class/Teatcher.php');
require_once('../class/Classe.php');
require_once('../controller/functions.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$emailUti=$_SESSION['user']['email'];
$session= new Sessionacade();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$Projet=new Projet();
$teatcher=new Teatcher();
$classe=new Classe();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);
 // var_dump($codeEtabsession);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);
$teatcherProjet=$parents->getDifferentStudentByParentId($compteuserid,);
//recuperer la session en cours
$sessionencours=$session->getSessionEncours($codeEtabsession);
$tabsessionencours=explode("*",$sessionencours);
$libellesessionencours=$tabsessionencours[0];
$sessionencoursid=$tabsessionencours[1];
$typesessionencours=$tabsessionencours[2];
// var_dump($libellesessionencours);

$nbnotificationstandby=$parents->getParentnotificationstandbyNb($compteuserid);
//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($compteuserid);
$allnotificationparents=$etabs->getParenallnotification($compteuserid);

$allprojet=$Projet->getProjetbyteacher($codeEtabsession,$libellesessionencours,$compteuserid);
// var_dump($allprojet);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::listeProjet ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="allprojet.php"><?php echo L::listeProjet ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>

                                <li><a class="parent-item" href="#"><?php echo L::Projet ?>S</a>
                                </li>

                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addprojetok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: 'Félicitations',
            text: '<?php echo $_SESSION['user']['addprojetok'] ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['addprojetok']);
                }

                 ?>

            <div class="row">
              <div class="col-md-12">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header></header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>

                                    </div>
                                </div>
                                <div class="card-body ">

                                    <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                        <thead>
                                          <!-- <th> <?php //echo L::NotificationsLibs ?> </th> -->
                                            <tr>
                                                <th><?php echo L::datedebTable?> </th>
                                                <th><?php echo L::datefinTable?></th>
                                                <th><?php echo L::nomEnsg?></th>
                                                <th><?php echo L::nomProjet?></th>
                                                <th> <?php echo L::Actions?> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                          // var_dump($allprojet);
                                            foreach ($allprojet as $value):
                                              // var_dump($value);
                                           ?>
                                            <tr class="odd gradeX">
                                                  <td><?php echo date_format(date_create($value->date_deb),"d/m/Y"); ?></td>
                                                  <td> <?php echo date_format(date_create($value->date_fin),"d/m/Y"); ?> </td>
                                                  <td><?php  echo $value->projet_title?></td>
                                                <td>
                                                  <?php  echo $value->nom_compte."".$value->prenom_compte ;?>
                                                </td>

                                                <td>

                                                  <!-- Button trigger modal -->
                                                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                   <a type="button" href="updateprojet.php?id_projet=<?php echo $value->id_projet ?>&id_enseignant=<?php echo  $value->id_enseignant; ?>" style="margin-left:-15px;" class="btn btn-md btn-success btn-xs" style="border-radius:5px;"> <i class="fa fa-pencil"></i> </a>
                                                   <!-- <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#vewProjet"> -->
                                                     <!-- <i class="fa fa-info"></i> -->
                                                     <a href="#"  data-toggle="modal" data-target="#vewProjet"class="btn btn-warning btn-xs " onclick="ActionBtn(<?php echo $value->id_projet ?>,'<?php echo $value->id_enseignant ?>','<?php echo $value->session_etab ?>','<?php echo $value->code_etab ?>','<?php echo $value->projet_content ?>')">
                                                       <i class="fa fa-info-circle"></i>
                                                       </a>

                                                   <!-- Modal -->
                                                </td>

                                            </tr>
                                            <?php
                                              endforeach;
                                             ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                      </div>
                </div>
            </div>

            <div class="modal fade" id="vewProjet" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLongTitle"><?php echo L::DescriProjet?></h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <textarea name="contenu" id="contenu"  rows="8" cols="60"> </textarea>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                        </div>
                      </div>
                    </div>
                  </div>





            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>


    <script type="text/javascript">
      function myFunction(idcompte,idclasse,codeEtab)
      {
        alert(idcompte+" "+idclasse+" "+codeEtab);
        document.location.href="updateProjet.php?compte="+idcompte+"&idclasse="+idclasse+"&codeEtab="+codeEtab;
      }
    </script>





    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- calendar -->
     <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
   <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
    <script src="../assets2/js/pages/table/table_data.js" ></script>
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

function ActionBtn(projetid,teatcherid,codeEtab,sessionEtab,contenu)
{
  // alert(projetid+" - "+teatcherid+" - "+codeEtab+" - "+sessionEtab);

  $("#vewProjet #contenu").val(contenu);

}

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }
   $(document).ready(function() {



   });

   </script>


    <!-- end js include path -->
  </body>

</html>
