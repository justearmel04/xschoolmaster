<?php
require '../src/ci/Osms.php';
use \Osms\Osms;


function tronquer ($chaine, $lg_max) {
    if (strlen($chaine) > $lg_max)
    {
    $chaine = substr($chaine, 0, $lg_max);
    $last_space = strrpos($chaine, " ");
    // $chaine = substr($chaine, 0, $last_space)."...";
    $chaine = substr($chaine, 0, $last_space)." ...";
    return $chaine;
    }
    }

    function prixMill($prix) {
     $str=""; $long =strlen($prix)-1; for($i = $long ; $i>=0; $i--) { $j=$long -$i; if( ($j%3 == 0) && $j!=0) { $str= " ".$str; } $p= $prix[$i]; $str = $p.$str; }
     return($str);
   }


function getRandomColorHex() {
  $r=dechex(rand(0,255));
$v=dechex(rand(0,255));
$b=dechex(rand(0,255));

return "#".$r.$v.$b;
  }

function rand_chars($c, $l, $u = FALSE) {
if (!$u) for ($s = '', $i = 0, $z = strlen($c)-1; $i < $l; $x = rand(0,$z), $s .= $c{$x}, $i++);
else for ($i = 0, $z = strlen($c)-1, $s = $c{rand(0,$z)}, $i = 1; $i != $l; $x = rand(0,$z), $s .= $c{$x}, $s = ($s{$i} == $s{$i-1} ? substr($s,0,-1) : $s), $i=strlen($s));
return $s;
}
function regiveMois($mois) {
switch($mois) {
    case '1': $mois = '01'; break;
    case '2': $mois = '02'; break;
    case '3': $mois = '03'; break;
    case '4': $mois = '04'; break;
    case '5': $mois = '05'; break;
    case '6': $mois = '06'; break;
    case '7': $mois = '07'; break;
    case '8': $mois = '08'; break;
    case '9': $mois = '09'; break;
    case '10': $mois = '10'; break;
    case '11': $mois = '11'; break;
    case '12': $mois = '12'; break;
    default: $mois =''; break;
  }
  return $mois;
}

function raccoursisseur($mot)
{
  $retour="";
  $pos = strpos($mot," ");
  if($pos==FALSE)
  {
    $retour=substr($mot, 0, 4);
  }else {

  $tabmot=explode("",$mot);
  $nb=count($tabmot);

  for($i=0;$i<$nb;$i++)
  {
    $retour=$retour.substr($tabmot[$i], 0, 4)." ";
  }


  }

return $retour;

}

function retranscrireMois($mois) {
switch($mois) {
    case '01': $mois = '1'; break;
    case '02': $mois = '2'; break;
    case '03': $mois = '3'; break;
    case '04': $mois = '4'; break;
    case '05': $mois = '5'; break;
    case '06': $mois = '6'; break;
    case '07': $mois = '7'; break;
    case '08': $mois = '8'; break;
    case '09': $mois = '9'; break;
    case '10': $mois = '10'; break;
    case '11': $mois = '11'; break;
    case '12': $mois = '12'; break;
    default: $mois =''; break;
  }
  return $mois;
}
function obtenirLibelleMois($mois) {
switch($mois) {
    case '01': $mois = 'JANVIER'; break;
    case '02': $mois = 'FEVRIER'; break;
    case '03': $mois = 'MARS'; break;
    case '04': $mois = 'AVRIL'; break;
    case '05': $mois = 'MAI'; break;
    case '06': $mois = 'JUIN'; break;
    case '07': $mois = 'JUILLET'; break;
    case '08': $mois = 'AOUT'; break;
    case '09': $mois = 'SEPTEMBRE'; break;
    case '10': $mois = 'OCTOBRE'; break;
    case '11': $mois = 'NOVEMBRE'; break;
    case '12': $mois = 'DECEMBRE'; break;
    default: $mois =''; break;
  }
  return $mois;
}


function obtenirindiceMois($mois) {
switch($mois) {
    case 'JANVIER': $mois = '1'; break;
    case 'FEVRIER': $mois = '2'; break;
    case 'MARS': $mois = '3'; break;
    case 'AVRIL': $mois = '4'; break;
    case 'MAI': $mois = '5'; break;
    case 'JUIN': $mois = '6'; break;
    case 'JUILLET': $mois = '7'; break;
    case 'AOUT': $mois = '8'; break;
    case 'SEPTEMBRE': $mois = '9'; break;
    case 'OCTOBRE': $mois = '10'; break;
    case 'NOVEMBRE': $mois = '11'; break;
    case 'DECEMBRE': $mois = '12'; break;
    default: $mois =''; break;
  }
  return $mois;
}

function obtenirMoisbyindice($mois) {
switch($mois) {
    case '1': $mois = 'JANVIER'; break;
    case '2': $mois = 'FEVRIER'; break;
    case '3': $mois = 'MARS'; break;
    case '4': $mois = 'AVRIL'; break;
    case '5': $mois = 'MAI'; break;
    case '6': $mois = 'JUIN'; break;
    case '7': $mois = 'JUILLET'; break;
    case '8': $mois = 'AOUT'; break;
    case '9': $mois = 'SEPTEMBRE'; break;
    case '10': $mois = 'OCTOBRE'; break;
    case '11': $mois = 'NOVEMBRE'; break;
    case '12': $mois = 'DECEMBRE'; break;
    default: $mois =''; break;
  }
  return $mois;
}


function nom_jour($date) {

$jour_semaine = array(1=>"Monday", 2=>"Tuesday", 3=>"Wednesday", 4=>"Thursday", 5=>"Friday", 6=>"Saturday", 7=>"Sunday");

list($annee, $mois, $jour) = explode ("-", $date);

$timestamp = mktime(0,0,0, date($mois), date($jour), date($annee));
$njour = date("N",$timestamp);

return $jour_semaine[$njour];

}

function obtenirLibelleJours($jour) {
switch($jour) {
    case 'Sunday': $jour = 'DIM'; break;
    case 'Monday': $jour = 'LUN'; break;
    case 'Tuesday': $jour = 'MAR'; break;
    case 'Wednesday': $jour = 'MER'; break;
    case 'Thursday': $jour = 'JEU'; break;
    case 'Friday': $jour = 'VEN'; break;
    case 'Saturday': $jour = 'SAM'; break;
    default: $jour =''; break;
  }
  return $jour;
}

function returnHours($hours)
{
  $tabhours=explode(":",$hours);
  $heures=$tabhours[0];
  $minutes=$tabhours[1];

  return $heures."H".$minutes;
}


function dateFormat($date) {

   $date = substr($date, 6, 4) . "-" . substr($date, 3, 2) . "-" . substr($date, 0, 2);

   return $date;

  }

  function AfficherSemaine($day)
  {
    $valeur="";
    if($day=="LUN")
    {
      $valeur="LUNDI";
    }else if($day=="MAR")
    {
      $valeur="MARDI";
    }else if($day=="MER")
    {
      $valeur="MERCREDI";
    }else if($day=="JEU")
    {
      $valeur="JEUDI";
    }else if($day=="VEN")
    {
      $valeur="VENDREDI";
    }else if($day=="SAM")
    {
      $valeur="SAMEDI";
    }else if($day=="DIM")
    {
      $valeur="DIMANCHE";
    }

    return $valeur;
  }

  function creationMatricule(){

    $str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$shuffled = str_shuffle($str);
    $concatone= rand(1, 999);
    $datemin=date("d");
    // return date('Y', time()).'-'.sprintf("%05d", $id).'-M';
}


function Orangecismssender($contacts,$message)
{



  $config = array(
      'clientId' => 'TcDibeD9jxOkJnomxJ6ahCplCBNJVGD5',
      'clientSecret' => 'Z5gfKqcJ2EhB7yb6'
  );

  $osms = new Osms($config);
  // retrieve an access token
  $response = $osms->getTokenFromConsumerKey();

  if (!empty($response['access_token'])) {
      $senderAddress = 'tel:+22578092974';
      // $receiverAddress = $contacts;
      $receiverAddress ='tel:+'.$contacts;
      // $message = 'ceci est un message test1';
      $senderName = 'XSHOOL';

      $receiverAddress=str_replace(' ', '',$receiverAddress);

      // echo $receiverAddress;

      $retour=$osms->sendSMS($senderAddress, $receiverAddress, $message, $senderName);

      // var_dump($retour);
  } else {
      echo $response['error'];
  }


}

function OrangecismsAttendance($contacts,$emailparent,$EtabName,$nomEleve,$classeName,$libellematiere,$debutHours,$finHours,$codeEtab,$imageEtab)
{

  $day=date("d-m-Y");
  $message="Bonjour Chers Parent,";
  $message.="l'élève ".$nomEleve." est absent au cours de ".$libellematiere." prévu de " .$debutHours;
  $message.="à ".$finHours." aujourd'hui";


  $config = array(
      'clientId' => 'TcDibeD9jxOkJnomxJ6ahCplCBNJVGD5',
      'clientSecret' => 'Z5gfKqcJ2EhB7yb6'
  );

  $osms = new Osms($config);
  // retrieve an access token
  $response = $osms->getTokenFromConsumerKey();

  if (!empty($response['access_token'])) {
      $senderAddress = 'tel:+22578092974';
      // $receiverAddress = $contacts;
      $receiverAddress ='tel:'.$contacts;
      // $message = 'ceci est un message test1';
      $senderName = 'XSHOOL';

      $receiverAddress=str_replace(' ', '',$receiverAddress);

      // echo $message;

      $retour=$osms->sendSMS($senderAddress, $receiverAddress, $message, $senderName);

      // var_dump($retour);
  } else {
      echo $response['error'];
  }


}


?>
