<?php
session_start();
require_once('../class/Student.php');
require_once('../class/Etablissement.php');
require_once('../controller/functions.php');

$student = new Student();
$etabs=new Etab();


if(isset($_POST['etape']))
{
  if($_POST['etape']==1||$_POST['etape']==2)
  {
    echo "enregistrer";

    //recuperation des variables

    $objet=htmlspecialchars($_POST['objet']);
    $codeEtab=htmlspecialchars($_POST['codeEtab']);
    $sessionEtab=htmlspecialchars($_POST['libellesession']);
    $otherobjet=htmlspecialchars($_POST['otherobjet']);
    $tabobjet=explode('-',$objet);
    $objetid=$tabobjet[0];
    $libelleobjet=$tabobjet[1];
    $destinataires=htmlspecialchars($_POST['destinataires']);
    $desti=htmlspecialchars($_POST['precis']);
    $summernote=$_POST['summernote'];
    $useradd=$_SESSION['user']['IdCompte'];
    $studentSchool="";
    $classes="";
    $statutNotif=0;
    $paranotif=0;
    $emailvalue=1;
    $smsvalue=1;
    $dateday=date("Y-m-d");
    $datemsg=date("Y-m-d");
    $type="INFORMATIONS";
    $precisiondestinataires="";

    if(strlen($summernote)>0)
    {

      if($destinataires=="Parent")
      {
        if($desti==1)
        {
          $precis=0;

          //recuperation des classes

          foreach($_POST['classeEtab'] as $valeur)
         {
           $classes=$classes.$valeur.'-';

         }

         $tabclasses=explode("-",$classes);
         $nbtabclasses=count($tabclasses);
         $cpteclassesval=$nbtabclasses-1;

         if(strlen($otherobjet)>0)
         {

           $messagesid=$etabs->AddnotificationsToparentsclasseselectedOthernew($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$otherobjet,$libelleobjet,$useradd,$type);
         }else {
           $messagesid=$etabs->AddNotificationToAllparentsclasseselect($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$libelleobjet,$useradd,$type);
         }

         for($i=0;$i<$cpteclassesval;$i++)
         {
           $dataParents=$etabs->getEmailsOfParentOfStudentInThisClassesParenterid($tabclasses[$i],$destinataires,$codeEtab,$sessionEtab);
           // $type="INFORMATIONS";

           foreach ($dataParents as $value):
             $etabs->AddparentLecturemessages($messagesid,$value->id_compte,$dateday,$type,$codeEtab,$sessionEtab);
           endforeach;


         }


        }else if($desti==2)
        {
          $precis=1;

          foreach($_POST['eleves'] as $valeur)
         {
           $studentSchool=$studentSchool.$valeur.'-';

         }

         $tabeleves=explode("-",$studentSchool);
         $nbtabeleves=count($tabeleves);
         $cpteelevesval=$nbtabeleves-1;

         $classes=substr($classes, 0, -1);
         $studentSchool=substr($studentSchool, 0, -1);
         $classes = str_replace("-", ",",$classes);
         $studentSchool=str_replace("-", ",",$studentSchool);

         if(strlen($otherobjet)>0)
         {
           $messagesid=$etabs->AddnotificationsToparentStudentclasseselectedOthernew($objetid,$summernote,$destinataires,$classes,$precis,$studentSchool,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$otherobjet,$libelleobjet,$useradd,$type);

         }else {
           $messagesid=$etabs->AddNotificationToAllparentStudentclasseselect($objetid,$summernote,$destinataires,$classes,$precis,$studentSchool,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$libelleobjet,$useradd,$type);

         }

         $dataParents=$etabs->getEmailsAndPhoneOfParentOfStudentclassesParenter($classes,$studentSchool,$sessionEtab,$codeEtab);
         foreach ($dataParents as $value):
                 $etabs->AddparentLecturemessages($messagesid,$value->idcompte_parent,$dateday,$type,$codeEtab,$sessionEtab);
         endforeach;


        }

        $datas=$etabs->getMessagesInfosOnly($messagesid);

        $tabcontent=explode("*",$datas);

        $smssender=$tabcontent[3];
        $emailsender=$tabcontent[4];
        $codeEtab=$tabcontent[2];
        $destinataires=$tabcontent[0];
        $classes=$tabcontent[1];
        // $sessionEtab=$libellesession;
        $precis=$tabcontent[5];
        $eleves=$tabcontent[6];

        $tabdatadestinataires=explode("-",$destinataires);
        $nbtabdestinataires=count($tabdatadestinataires);
        $cptedestinatairesval=$nbtabdestinataires-1;

        $tabclasses=explode("-",$classes);
        $nbtabclasses=count($tabclasses);
        $cpteclassesval=$nbtabclasses-1;


          //determiner l'indicatif de ce pays

          $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);
          $destimails="";
          $destiphone="";
          if($precis==0)
          {
          //la liste des parents de toutes les classes selectionnées

          for($i=0;$i<$cpteclassesval;$i++)
          {
            for($j=0;$j<$cptedestinatairesval;$j++)
            {

            if($tabdatadestinataires[$j]=="Parent")
            {
                // echo "Parent de la classe ".$tabclasses[$i]."<br>";
                //rechercher le mail des parents dont un eleve appartient à cette classeEtab

                // $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab,$sessionEtab);

                $dataParents=$etabs->getEmailsOfParentOfStudentInThisClassesParenter($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab,$sessionEtab);


                 // var_dump($dataParents);
                 $ka=1;
                 foreach ($dataParents as $parents):

                   $destimails=$destimails.$parents->email_parent."*";
                   $destiphone=$destiphone.$indicatifEtab.$parents->tel_parent."*";


                   $ka++;
                 endforeach;

            }
          }
          }



          }else if($precis==1)
          {
              //listes des parents en fonction des eleves selectionnées

              $tabeleves=explode("-",$eleves);
              $nbtabeleves=count($tabeleves);
              $cpteelevesval=$nbtabeleves-1;

              // echo $classes." ".$eleves;

              $classes=substr($classes, 0, -1);
              $eleves=substr($eleves, 0, -1);

              $classes = str_replace("-", ",",$classes);
              $eleves=str_replace("-", ",",$eleves);

              // $dataParents=$etabs->getEmailsAndPhoneOfParentOfStudentclasses($classes,$eleves,$sessionEtab,$codeEtab);
              $dataParents=$etabs->getEmailsAndPhoneOfParentOfStudentclassesParenter($classes,$eleves,$sessionEtab,$codeEtab);


              // var_dump($dataParents);
              $ka=1;
              foreach ($dataParents as $parents):

                $destimails=$destimails.$parents->email_parent."*";
                $destiphone=$destiphone.$indicatifEtab.$parents->tel_parent."*";


                $ka++;
              endforeach;


          }





      }else if($destinataires=="Teatcher")
      {
        if($desti==1)
        {
          $precis=0;

          foreach($_POST['classeEtab'] as $valeur)
         {
           $classes=$classes.$valeur.'-';

         }

         $tabclasses=explode("-",$classes);
         $nbtabclasses=count($tabclasses);
         $cpteclassesval=$nbtabclasses-1;

         if(strlen($otherobjet)>0)
         {

           $messagesid=$etabs->AddnotificationsToparentsclasseselectedOthernew($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$otherobjet,$libelleobjet,$useradd,$type);
         }else {
           $messagesid=$etabs->AddNotificationToAllparentsclasseselect($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$libelleobjet,$useradd,$type);
         }

         for($i=0;$i<$cpteclassesval;$i++)
         {
           $dataTeatchers=$etabs->getEmailsOfTeatchersInThisclasses($tabclasses[$i],$destinataires,$codeEtab,$sessionEtab);
           // $type="INFORMATIONS";

           foreach ($dataTeatchers as $value):
             $etabs->AddparentLecturemessages($messagesid,$value->id_compte,$dateday,$type,$codeEtab,$sessionEtab);
           endforeach;


         }



        }else if($desti==2)
        {
          $precis=1;

          foreach($_POST['precisiondestinataires'] as $valeur)
         {
           $precisiondestinataires=$precisiondestinataires.$valeur.'-';

         }

         $precisiondestinataires=substr($precisiondestinataires, 0, -1);
         $precisiondestinataires = str_replace("-", ",",$precisiondestinataires);

         $dataTeatchers=$etabs->getEmailsOfTeatchersInThisclassesByIds($destinataires,$precisiondestinataires);

         foreach ($dataTeatchers as $value):
           $etabs->AddparentLecturemessages($messagesid,$value->id_compte,$dateday,$type,$codeEtab,$sessionEtab);
         endforeach;


        }
      }else if($destinataires=="Admin_locale")
      {
        if($desti==1)
        {
          $precis=0;
          //liste des responsables de l'établissement

          if(strlen($otherobjet)>0)
          {
             $messagesid=$etabs->AddnotificationsToparentsclasseselectedOthernew($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$otherobjet,$libelleobjet,$useradd,$type);
          }else {
            $messagesid=$etabs->AddNotificationToAllparentsclasseselect($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$libelleobjet,$useradd,$type);
          }

          $dataRespos=$etabs->getresponsablesbyEtabs($destinataires,$codeEtab);

          foreach ($dataRespos as $value):
            $etabs->AddparentLecturemessages($messagesid,$value->id_compte,$dateday,$type,$codeEtab,$sessionEtab);
          endforeach;

        }else if($desti==2)
        {
          $precis=1;

          if(strlen($otherobjet)>0)
          {
             $messagesid=$etabs->AddnotificationsToparentsclasseselectedOthernew($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$otherobjet,$libelleobjet,$useradd,$type);
          }else {
            $messagesid=$etabs->AddNotificationToAllparentsclasseselect($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$libelleobjet,$useradd,$type);
          }

          //la liste precise des responsables d'etablissements

          foreach($_POST['precisiondestinataires'] as $valeur)
         {
           $precisiondestinataires=$precisiondestinataires.$valeur.'-';

         }

         $precisiondestinataires=substr($precisiondestinataires, 0, -1);
         $precisiondestinataires = str_replace("-", ",",$precisiondestinataires);

         $dataRespos=$etabs->getresponsablesEtabsByIds($destinataires,$codeEtab,$precisiondestinataires);

         foreach ($dataRespos as $value):
           $etabs->AddparentLecturemessages($messagesid,$value->id_compte,$dateday,$type,$codeEtab,$sessionEtab);
         endforeach;


        }

        

      }

    }else {

    echo "Aucun message";
    }



  }
}


?>
