<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$classe = new Classe();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recuperation des variables
  $emailvalue=htmlspecialchars(addslashes($_POST['emailvalue']));
  $smsvalue=htmlspecialchars(addslashes($_POST['smsvalue']));
  $libelactivity=htmlspecialchars(addslashes($_POST['libelactivity']));
  $typesess=htmlspecialchars(addslashes($_POST['typesess']));
  $datedeb=htmlspecialchars(addslashes($_POST['datedeb']));
  $datefin=htmlspecialchars(addslashes($_POST['datefin']));
  $message=htmlspecialchars(addslashes($_POST['message']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
  $gratuitcheck=htmlspecialchars(addslashes($_POST['gratuitcheck']));
  $paiecheck=htmlspecialchars(addslashes($_POST['paiecheck']));
  $montantAct=htmlspecialchars(addslashes($_POST['montantAct']));
  $statutNotif=0;
  $dateday=date("Y-m-d");
  $classes="";
  $destinataires="";
  $paranotif=1;
  $statutpara=1;

  foreach($_POST['classeEtab'] as $valeur)
  {
   $classes=$classes.$valeur.'-';

  }

  foreach($_POST['destinataires'] as $valeur)
 {
   $destinataires=$destinataires.$valeur.'-';

 }


  $fileError = $_FILES['joinfile']['error'];


  if($fileError==0)
  {
    //avec fichier joint

    $statutFile=1;

    $file_name = @$_FILES['joinfile']['name'];

    $_SESSION["joinfile"] = $file_name;

    $file_size =@$_FILES['joinfile']['size'];

    $file_tmp =@$_FILES['joinfile']['tmp_name'];

    $file_type=@$_FILES['joinfile']['type'];

    @$file_ext=strtolower(end(explode('.',@$_FILES['joinfile']['name'])));

    $fichierTemp = uniqid() . "." . $file_ext;

    $transactionId =  "NOTIF".date("Y").strtoupper(substr($libelactivity, 0, 4));

    move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

    $idnotif=$etabs->AddNotificationWithFile($libelactivity,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$libellesession,$paranotif);

    $fichierad=$transactionId.$idnotif.".".$file_ext;

    $dossier="../notifications/";
    $dossier1="../notifications/".$dateday."/";

    if(!is_dir($dossier)) {

          //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation

          @mkdir($dossier);

          @mkdir($dossier1);
              }else
              {
                 @mkdir($dossier1);

              }

              @rename('../temp/' . $fichierTemp , "../notifications/".$dateday."/".$fichierad);

               @unlink("../temp/" . $fichierTemp);

        $etabs->UpdateNotificationFileName($fichierad,$idnotif,$codeEtab);

        //insertion dans la table parascolaires

        if($paiecheck==1)
        {
          //activité payante


          $etab->AddParascolaireWithPaiement($libelactivity,$datedeb,$datefin,$codeEtab,$libellesession,$typesess,$montantAct,$paiecheck,$classes,$idnotif,$statutpara);



        }else if($paiecheck==0){
          //activité non payante
          $etab->AddParascolaireWithOutPaiement($libelactivity,$datedeb,$datefin,$codeEtab,$libellesession,$typesess,$paiecheck,$classes,$idnotif,$statutpara);


        }

        $_SESSION['user']['deletesubjectok']="Activité parascolaire ajoutée avec succès";

        if($_SESSION['user']['profile'] == "Admin_globale") {

              //header("Location:../manager/index.php");
              header("Location:../manager/parascolaires.php");

          }else if($_SESSION['user']['profile'] == "Admin_locale") {
            // if($_SESSION['user']['paysid']==4)
            // {
            //   header("Location:../localecmr/parascolaires.php");
            // }else {
            //   header("Location:../locale/parascolaires.php");
            // }

            $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
      $typeetab=$etab->DetermineTypeEtab($codeEtab);

      if($typeetab==5)
      {
        header("Location:../locale".$libelleEtab."/parascolaires.php");
      }else {
        header("Location:../locale/parascolaires.php");
      }


          }else if($_SESSION['user']['profile'] == "Teatcher") {

              // header("Location:../teatcher/parascolaires.php");

              $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
                          $typeetab=$etab->DetermineTypeEtab($codeEtab);
                          $IBSAschool=$etab->getIbsaschools();
                          $codeetabs="";
                          foreach ($IBSAschool as  $value):
                            $codeetabs=$codeetabs.$value->code_etab."*";
                          endforeach;
                          $tabetabs=explode("*",$codeetabs);

                          $nbdispenser=0;

                          $dispenserdatas=$etab->getdispenserTeatcher($_SESSION['user']['IdCompte']);

                          foreach ($dispenserdatas as $value):
                            $code=$value->codeEtab;
                            if (in_array($code, $tabetabs)) {
                              $nbdispenser++;
                             }
                          endforeach;

                          echo $nbdispenser;

                          if($nbdispenser>0)
                          {
                            header("Location:../teatcher".$libelleEtab."/parascolaires.php");
                          }else {
                            header("Location:../teatcher/parascolaires.php");
                          }





              }



  }else {
    // pas de fichier joint
    $statutFile=0;
    $idnotif=$etab->AddNotificationWithoutFi($libelactivity,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$libellesession,$paranotif);
    if($paiecheck==1)
    {
      //activité payante


      $etab->AddParascolaireWithPaiement($libelactivity,$datedeb,$datefin,$codeEtab,$libellesession,$typesess,$montantAct,$paiecheck,$classes,$idnotif,$statutpara);

    }else if($paiecheck==0){
      //activité non payante
      $etab->AddParascolaireWithOutPaiement($libelactivity,$datedeb,$datefin,$codeEtab,$libellesession,$typesess,$paiecheck,$classes,$idnotif,$statutpara);
    }

    $_SESSION['user']['deletesubjectok']="Activité parascolaire ajoutée avec succès";

    if($_SESSION['user']['profile'] == "Admin_globale") {

          //header("Location:../manager/index.php");
          header("Location:../manager/parascolaires.php");

      }else if($_SESSION['user']['profile'] == "Admin_locale") {
        // if($_SESSION['user']['paysid']==4)
        // {
        //   header("Location:../localecmr/parascolaires.php");
        // }else {
        //   header("Location:../locale/parascolaires.php");
        // }

        $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
      $typeetab=$etab->DetermineTypeEtab($codeEtab);

      if($typeetab==5)
      {
        header("Location:../locale".$libelleEtab."/parascolaires.php");
      }else {
        header("Location:../locale/parascolaires.php");
      }


      }else if($_SESSION['user']['profile'] == "Teatcher") {

          // header("Location:../teatcher/parascolaires.php");

          libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etab->DetermineTypeEtab($codeEtab);
            $IBSAschool=$etab->getIbsaschools();
            $codeetabs="";
            foreach ($IBSAschool as  $value):
              $codeetabs=$codeetabs.$value->code_etab."*";
            endforeach;
            $tabetabs=explode("*",$codeetabs);

            $nbdispenser=0;

            $dispenserdatas=$etab->getdispenserTeatcher($_SESSION['user']['IdCompte']);

            foreach ($dispenserdatas as $value):
              $code=$value->codeEtab;
              if (in_array($code, $tabetabs)) {
                $nbdispenser++;
               }
            endforeach;

            echo $nbdispenser;

            if($nbdispenser>0)
            {
              header("Location:../teatcher".$libelleEtab."/parascolaires.php");
            }else {
              header("Location:../teatcher/parascolaires.php");
            }






          }

  }


/*

  if($paiecheck==1)
  {
    //activité payante


    $etab->AddParascolaireWithPaiement($libelactivity,$datedeb,$datefin,$codeEtab,$libellesession,$typesess,$montantAct,$paiecheck,$classes);

  }else if($paiecheck==0){
    //activité non payante
    $etab->AddParascolaireWithOutPaiement($libelactivity,$datedeb,$datefin,$codeEtab,$libellesession,$typesess,$paiecheck,$classes);
  }
*/




}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //modification d'une activité parascolaire
  //recupération des variables


  $libelactivity=htmlspecialchars(addslashes($_POST['libelactivity']));
  $typesess=htmlspecialchars(addslashes($_POST['typesess']));
  $datedeb=htmlspecialchars(addslashes($_POST['datedeb']));
  $datefin=htmlspecialchars(addslashes($_POST['datefin']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
  $gratuitcheck=htmlspecialchars(addslashes($_POST['gratuitcheck']));
  $paiecheck=htmlspecialchars(addslashes($_POST['paiecheck']));
  $paraid=htmlspecialchars(addslashes($_POST['paraid']));
  $message=htmlspecialchars(addslashes($_POST['message']));
  $montantAct=htmlspecialchars(addslashes($_POST['montantAct']));
  $smsvalue=htmlspecialchars(addslashes($_POST['smsvalue']));
  $emailvalue=htmlspecialchars(addslashes($_POST['emailvalue']));
  $notifid=htmlspecialchars(addslashes($_POST['notifid']));
  $oldfilename=htmlspecialchars(addslashes($_POST['oldfilename']));
  $datenotif=htmlspecialchars(addslashes($_POST['datenotif']));
  $datafichierold=explode(".",$oldfilename);
  $nomoldfichier=$datafichierold[0];
  $classes="";
  $destinataires="";

  $modclasses=0;
  $moddesti=0;

  if(isset($_POST['destinataires']))
  {
$moddesti=1;
  }

  if(isset($_POST['classeEtab']))
  {
  $modclasses=1;
  }

  if($modclasses==1)
  {
    //nous avons une modification des classes

    foreach($_POST['classeEtab'] as $valeur)
    {
     $classes=$classes.$valeur.'-';
    }
    //nous allons modifier les classes dans les tables notification et parascolaires

    $etab->UpdateClassesNotificationAndParascolaire($classes,$paraid,$codeEtab,$libellesession,$notifid);
  }

  if($moddesti==1)
  {
    //nous avons une modification des destinataires

    foreach($_POST['destinataires'] as $valeur)
    {
     $destinataires=$destinataires.$valeur.'-';

    }

    //nous allons modifier les destinataires dans les tables notification et parascolaires
$etab->UpdateDestinatairesNotificationAndParascolaire($destinataires,$paraid,$codeEtab,$libellesession,$notifid);


  }

  $fileError = $_FILES['joinfile']['error'];

  if($fileError==0)
  {
    //nous avons un fichier joint
    $statutFile=1;

    $file_name = @$_FILES['joinfile']['name'];

    $_SESSION["joinfile"] = $file_name;

    $file_size =@$_FILES['joinfile']['size'];

    $file_tmp =@$_FILES['joinfile']['tmp_name'];

    $file_type=@$_FILES['joinfile']['type'];

    @$file_ext=strtolower(end(explode('.',@$_FILES['joinfile']['name'])));

    $fichierTemp = uniqid() . "." . $file_ext;

    $transactionId =  "NOTIF".date("Y").strtoupper(substr($libelactivity, 0, 4));

    move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

    $fichierad=$transactionId.$notifid.".".$file_ext;

    $dossier="../notifications/";
    $dossier1="../notifications/".$dateday."/";

    if(!is_dir($dossier)) {

          //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation

          @mkdir($dossier);

          @mkdir($dossier1);
              }else
              {
                 @mkdir($dossier1);

              }
// $datenotif
//supprimer l'ancien fichier
@unlink("../notifications/".$datenotif."/".$oldfilename);

@rename('../temp/' . $fichierTemp , "../notifications/".$datenotif."/".$fichierad);

 @unlink("../temp/" . $fichierTemp);

 $etabs->UpdateNotificationFileName($fichierad,$idnotif,$codeEtab);

 if($paiecheck==1)
 {
   //nous avons un montant pour l'activité

    $etab->NotificationAndParascolairesInfosUpdateWithPaie($libelactivity,$typesess,$datedeb,$datefin,$codeEtab,$libellesession,$paraid,$message,$montantAct,$smsvalue,$emailvalue,$notifid,$statutFile,$fichierad);

 }else if ($paiecheck==0) {
   // nous n'avons pas de montant pour l'activité
   $montantAct=0;

    $etab->NotificationAndParascolairesInfosUpdateWithOutPaie($libelactivity,$typesess,$datedeb,$datefin,$codeEtab,$libellesession,$paraid,$message,$montantAct,$smsvalue,$emailvalue,$notifid,$statutFile,$fichierad);
 }


 $_SESSION['user']['Updateadminok']="Activité Parascolaire modifiée avec succès";
 if($_SESSION['user']['profile'] == "Admin_globale") {

       //header("Location:../manager/index.php");
       header("Location:../manager/parascolaires.php");

   }else if($_SESSION['user']['profile'] == "Admin_locale") {
     // if($_SESSION['user']['paysid']==4)
     // {
     //   header("Location:../localecmr/parascolaires.php");
     // }else {
     //   header("Location:../locale/parascolaires.php");
     // }

     $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
      $typeetab=$etab->DetermineTypeEtab($codeEtab);

      if($typeetab==5)
      {
        header("Location:../locale".$libelleEtab."/parascolaires.php");
      }else {
        header("Location:../locale/parascolaires.php");
      }


   }else if($_SESSION['user']['profile'] == "Teatcher") {

       // header("Location:../teatcher/parascolaires.php");

       $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etab->DetermineTypeEtab($codeEtab);
            $IBSAschool=$etab->getIbsaschools();
            $codeetabs="";
            foreach ($IBSAschool as  $value):
              $codeetabs=$codeetabs.$value->code_etab."*";
            endforeach;
            $tabetabs=explode("*",$codeetabs);

            $nbdispenser=0;

            $dispenserdatas=$etab->getdispenserTeatcher($_SESSION['user']['IdCompte']);

            foreach ($dispenserdatas as $value):
              $code=$value->codeEtab;
              if (in_array($code, $tabetabs)) {
                $nbdispenser++;
               }
            endforeach;

            echo $nbdispenser;

            if($nbdispenser>0)
            {
              header("Location:../teatcher".$libelleEtab."/parascolaires.php");
            }else {
              header("Location:../teatcher/parascolaires.php");
            }






       }



  }else {
    //pas de fichier joint
$statutFile=0;
$file="";
    if($paiecheck==1)
    {
      //nous avons un montant pour l'activité

       $etab->NotificationAndParascolairesInfosUpdateWithPaieWithoutFile($libelactivity,$typesess,$datedeb,$datefin,$codeEtab,$libellesession,$paraid,$message,$montantAct,$smsvalue,$emailvalue,$notifid,$statutFile);

    }else if ($paiecheck==0) {
      // nous n'avons pas de montant pour l'activité
      $montantAct=0;

       $etab->NotificationAndParascolairesInfosUpdateWithOutPaieWithoutFile($libelactivity,$typesess,$datedeb,$datefin,$codeEtab,$libellesession,$paraid,$message,$montantAct,$smsvalue,$emailvalue,$notifid,$statutFile);
    }

    $_SESSION['user']['Updateadminok']="Activité Parascolaire modifiée avec succès";
    if($_SESSION['user']['profile'] == "Admin_globale") {

          //header("Location:../manager/index.php");
          header("Location:../manager/parascolaires.php");

      }else if($_SESSION['user']['profile'] == "Admin_locale") {
        // if($_SESSION['user']['paysid']==4)
        // {
        //   header("Location:../localecmr/parascolaires.php");
        // }else {
        //   header("Location:../locale/parascolaires.php");
        // }

        $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
      $typeetab=$etab->DetermineTypeEtab($codeEtab);

      if($typeetab==5)
      {
        header("Location:../locale".$libelleEtab."/parascolaires.php");
      }else {
        header("Location:../locale/parascolaires.php");
      }


      }else if($_SESSION['user']['profile'] == "Teatcher") {

          // header("Location:../teatcher/parascolaires.php");

          $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etab->DetermineTypeEtab($codeEtab);
            $IBSAschool=$etab->getIbsaschools();
            $codeetabs="";
            foreach ($IBSAschool as  $value):
              $codeetabs=$codeetabs.$value->code_etab."*";
            endforeach;
            $tabetabs=explode("*",$codeetabs);

            $nbdispenser=0;

            $dispenserdatas=$etab->getdispenserTeatcher($_SESSION['user']['IdCompte']);

            foreach ($dispenserdatas as $value):
              $code=$value->codeEtab;
              if (in_array($code, $tabetabs)) {
                $nbdispenser++;
               }
            endforeach;

            echo $nbdispenser;

            if($nbdispenser>0)
            {
              header("Location:../teatcher".$libelleEtab."/parascolaires.php");
            }else {
              header("Location:../teatcher/parascolaires.php");
            }



          }
  }


}

if(isset($_GET['etape'])&&($_GET['etape']==3))
{
  //recuperation des variables

  $paraid=htmlspecialchars(addslashes($_GET['paraid']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
  $notifid=htmlspecialchars(addslashes($_GET['notifid']));

  $statusNotification=-1;
  $statusparasco=0;

  if($paraid>0)
  {

$etab->DeleteNotificationParasco($paraid,$notifid,$statusNotification,$statusparasco);
  }else if($paraid==0)
  {

    $etab->DeleteNotification($notifid,$statusNotification);
  }

  $_SESSION['user']['Updateadminok']="Notification supprimer avec succès";
  if($_SESSION['user']['profile'] == "Admin_globale") {

        //header("Location:../manager/index.php");
        header("Location:../manager/messages.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      // if($_SESSION['user']['paysid']==4)
      // {
      //   header("Location:../localecmr/messages.php");
      // }else {
      //   header("Location:../locale/messages.php");
      // }

      $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
      $typeetab=$etab->DetermineTypeEtab($codeEtab);

      if($typeetab==5)
      {
        header("Location:../locale".$libelleEtab."/messages.php");
      }else {
        header("Location:../locale/messages.php");
      }


    }else if($_SESSION['user']['profile'] == "Teatcher") {

        // header("Location:../teatcher/messages.php");

        $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etab->DetermineTypeEtab($codeEtab);
            $IBSAschool=$etab->getIbsaschools();
            $codeetabs="";
            foreach ($IBSAschool as  $value):
              $codeetabs=$codeetabs.$value->code_etab."*";
            endforeach;
            $tabetabs=explode("*",$codeetabs);

            $nbdispenser=0;

            $dispenserdatas=$etab->getdispenserTeatcher($_SESSION['user']['IdCompte']);

            foreach ($dispenserdatas as $value):
              $code=$value->codeEtab;
              if (in_array($code, $tabetabs)) {
                $nbdispenser++;
               }
            endforeach;

            echo $nbdispenser;

            if($nbdispenser>0)
            {
              header("Location:../teatcher".$libelleEtab."/messages.php");
            }else {
              header("Location:../teatcher/messages.php");
            }


        }


}if(isset($_GET['etape'])&&($_GET['etape']==4))
{

  //recuperation des variables

  $notifid=htmlspecialchars(addslashes($_GET['notifid']));
  $destimails=htmlspecialchars(addslashes($_GET['destimails']));
  $destiphones=htmlspecialchars(addslashes($_GET['destiphones']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
  $smssender=htmlspecialchars(addslashes($_GET['smssender']));
  $emailsender=htmlspecialchars(addslashes($_GET['emailsender']));
  $joinfile=htmlspecialchars(addslashes($_GET['joinfile']));
  $file=htmlspecialchars(addslashes($_GET['file']));
  $paraid=htmlspecialchars(addslashes($_GET['paraid']));
  $notificationsStatus=1;
  $parascolaireStatus=2;

  //recuperer les informations du message(titre & contenu)
  $destimails=substr($destimails, 0, -1);
  $destiphones=substr($destiphones, 0, -1);

  //recuperer la date de notification

  $datanotification=$etab->getNotificationInfos($notifid);
  $tabnotification=explode("*",$datanotification);
  $datenotif=$tabnotification[9];

  //nous allons recuperer le nom de l'etablissement et le logo

  $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
  $logoEtab=$etab->getEtabLogobyCodeEtab($codeEtab);


  $datas=$etab->getNotificationInfos($notifid);
  $tabdatas=explode("*",$datas);
  $titremessage=$tabdatas[7];
  $contenumessage=$tabdatas[8];

  if($joinfile==0)
  {
  //pas de fichier joint
  if($emailsender==1)
  {
  //envoi de mail aux destinataires
  if($smssender==1)
  {
  //envoi de mail plus sms
  $etab->SendNotifiactionWithoutToDestinataires($destimails,$titremessage,$contenumessage,$libelleEtab,$logoEtab,$codeEtab);

  }else {
    //mail seulement

  $etab->SendNotifiactionWithoutToDestinataires($destimails,$titremessage,$contenumessage,$libelleEtab,$logoEtab,$codeEtab);

  }



}else if($emailsender==0)
{
  //envoi de sms seulement
}

  }else if($joinfile==1) {
  // fichier joint

  if($emailsender==1)
  {
  //envoi de mail aux destinataires
  if($smssender==1)
  {
  //envoi de mail plus sms
  $etab->SendNotifiactionWithToDestinataires($destimails,$titremessage,$contenumessage,$libelleEtab,$logoEtab,$codeEtab,$datenotif,$file);
  }else {
  //envoi mail seul
  $etab->SendNotifiactionWithToDestinataires($destimails,$titremessage,$contenumessage,$libelleEtab,$logoEtab,$codeEtab,$datenotif,$file);

  }



}else if($emailsender==1)
{
  //envoi de sms seulement
}

  }

  //message notification envoyer avec succès

  //changer le statut de la notification à 1

    if($paraid==0)
    {
      echo "bonjour";
      $etab->UpdateNotificationStatusPara($notifid,$notificationsStatus,$codeEtab);
    }else if($paraid>0)
    {
      //changer le statut de notification à 1 puis parascolaire à 2
      echo "bonsoir";
      $etab->UpdateNotificationStatusAndParascolairesPara($notifid,$notificationsStatus,$codeEtab,$_GET['paraid'],$parascolaireStatus);
    }



    // $_SESSION['user']['updateExamok']="Notification Activité parascolaire envoyé avec succès";
    $_SESSION['user']['updateExamok']=L::ParascoNotifySendMessageSuccess;

    if($_SESSION['user']['profile'] == "Admin_globale") {

          //header("Location:../manager/index.php");
          header("Location:../manager/parascolaires.php");

      }else if($_SESSION['user']['profile'] == "Admin_locale") {
        // if($_SESSION['user']['paysid']==4)
        // {
        //   header("Location:../localecmr/parascolaires.php");
        // }else {
        //   header("Location:../locale/parascolaires.php");
        // }

        $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
      $typeetab=$etab->DetermineTypeEtab($codeEtab);

      if($typeetab==5)
      {
        header("Location:../locale".$libelleEtab."/parascolaires.php");
      }else {
        header("Location:../locale/parascolaires.php");
      }


        }


}if(isset($_GET['etape'])&&($_GET['etape']==5))
{
  //archivage activité parascolaire

  //recupération des variables

  //statut notification==2 pour archiver
  //statut parascolaire==3 pour archiver

  $paraid=htmlspecialchars(addslashes($_GET['paraid']));
  $notifid=htmlspecialchars(addslashes($_GET['notifid']));
  $session=htmlspecialchars(addslashes($_GET['session']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
  $notificationstatut=2;
  $parascolairestatut=3;

  $etab->ArchivedParascolaireActivity($paraid,$notifid,$session,$codeEtab,$notificationstatut,$parascolairestatut);

  $_SESSION['user']['updateExamok']="Activité parascolaire archivé avec succès";
  if($_SESSION['user']['profile'] == "Admin_locale") {

      // header("Location:../locale/parascolaires.php");

      $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
      $typeetab=$etab->DetermineTypeEtab($codeEtab);

      if($typeetab==5)
      {
        header("Location:../locale".$libelleEtab."/parascolaires.php");
      }else {
        header("Location:../locale/parascolaires.php");
      }

    }


}


 ?>
