<?php
session_start();
require_once('../class/Student.php');
require_once('../class/Parent.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Etablissement.php');
require_once('../controller/functions.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$student = new Student();
$parentx = new ParentX();
$sessionX= new Sessionacade();
$etabs=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //nous sommes dans un cas de matricule manuel

//recuperation des informations de l'eleve

$matri=htmlspecialchars($_POST['matri']);
$nomad=htmlspecialchars($_POST['nomad']);
$prenomad=htmlspecialchars($_POST['prenomad']);
$statuseleve=htmlspecialchars($_POST['statuseleve']);
$doublant=htmlspecialchars($_POST['doublant']);
$classeEtab=htmlspecialchars($_POST['classeEtab']);
$datenaisad=htmlspecialchars($_POST['datenaisad']);
$datenaisad=dateFormat($datenaisad);
$sexe=htmlspecialchars($_POST['sexe']);
$lieunais=htmlspecialchars($_POST['lieunais']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$newStudent=htmlspecialchars($_POST['newStudent']);
$sessionscolaire=htmlspecialchars($_POST['sessionscolaire']);
$allergie=htmlspecialchars($_POST['allergie']);
$condphy=htmlspecialchars($_POST['condphy']);
$datecrea=date("Y-m-d");
$type_cpte="Student";
$statut=1;

$nbsessionOn=$sessionX->getNumberSessionEncoursOn($codeEtab);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$sessionX->getSessionEncours($codeEtab);
  $tabsessionencours=explode("*",$sessionencours);
  $session=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
}

//gestion des parents
$concatoldparent=htmlspecialchars($_POST['concatoldparents']);
$concatparents=htmlspecialchars($_POST['concatparents']);

$tabnewparent=explode("@",$concatparents);
$taboldparent=explode("@",$concatoldparent);
$nbnewparent=count($tabnewparent);
$nbparent=htmlspecialchars($_POST['nbparent']);
$nbparentold=htmlspecialchars($_POST['nboldparent']);

$allparentids="";


for($i=0;$i<$nbparent;$i++)
{
  //insertion dans la table parent et retour de l'id


  $datecrea=date("Y-m-d");
  $type_cpteP="Parent";
  $statut=1;
  $nom=htmlspecialchars($_POST['nomparent'.$tabnewparent[$i]]);
  $prenom=htmlspecialchars($_POST['prenomparent'.$tabnewparent[$i]]);
  $phone=htmlspecialchars($_POST['phoneparent'.$tabnewparent[$i]]);
  $sexe=htmlspecialchars($_POST['sexeparent'.$tabnewparent[$i]]);

  //insertion dans la table compte
  $idparentcompte=$parentx->Addparentchap($nom,$prenom,$phone,$datecrea,$type_cpteP,$statut,$sexe,$codeEtab);

  $allparentids=$allparentids.$idparentcompte."@";

}

$allparentids=$allparentids.$concatoldparent;

//nous allons voir si les anciens parents sont enregister dans cet etablissement



for($i=0;$i<$nbparentold;$i++)
{

  $parentid=$taboldparent[$i];

  $nombreP=$parentx->getspecificParentSchool($parentid,$codeEtab);

  if($nombreP>0)
  {

  }else {
    //nous allons ajouter ce parent dans cet etablissement

    $parentx->Addoldparent($parentid,$codeEtab);
  }
}


$taballparents=explode("@",$allparentids);
$nball=count($taballparents)-1;

//nous allons voir si nous avons charger le photo de l'eleve

$fileError = $_FILES['photoad']['error'];

if($fileError==0)
{
//photo uploadé

$file_name = @$_FILES['photoad']['name'];

$_SESSION["photoad"] = $file_name;

$file_size =@$_FILES['photoad']['size'];

$file_tmp =@$_FILES['photoad']['tmp_name'];

$file_type=@$_FILES['photoad']['type'];

@$file_ext=strtolower(end(explode('.',@$_FILES['photoad']['name'])));

$fichierTemp = uniqid() . "." . $file_ext;

$transactionId =  "STU".date("Y") . date("m").strtoupper(substr($nomad, 0, 2)).$matri;

$fichierad=$transactionId.".".$file_ext;

move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

$dossier="../photo/Students/";

$dossier1="../photo/Students/".$matri;

if(!is_dir($dossier)) {
      //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
      @mkdir($dossier);
      @mkdir($dossier1);

          }else
          {
     @mkdir($dossier1);
          }
    @rename('../temp/' . $fichierTemp , "../photo/Students/" . $matri."/".$fichierad);

         //Suppression du fichier se trouvant dans le dossier temp

    @unlink("../temp/" . $fichierTemp);

    $studentid=$student->AddStudentFile($matri,$nomad,$prenomad,$classeEtab,$datenaisad,$sexe,$datecrea,$type_cpte,$statut,$lieunais,$sessionscolaire,$codeEtab,$fichierad,$allergie,$condphy,$statuseleve,$doublant);
    //insertion dans la table parenter

    for($i=0;$i<$nball;$i++)
    {
      $parentid=$taballparents[$i];

      $verif=$student->DetermineNumberOfparenter($parentid,$studentid);

      if($verif==0)
      {
        $student->Addparenter($parentid,$studentid);
      }
    }

    // $_SESSION['user']['addStudok']="Inscription à bien été pris en compte dans le système avec succès";

    $_SESSION['user']['addStudok']=L::InscriptionMessageSuccess;


    if($_SESSION['user']['profile'] == "Admin_globale")

    {

    header("Location:../manager/admission.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {
      if($_SESSION['user']['paysid']==4)
      {

        header("Location:../localecmr/admission.php");
      }else {
        header("Location:../locale/admission.php");
      }


    }

}else {
  //aucune photo uploadé

  $studentid=$student->AddStudent($matri,$nomad,$prenomad,$classeEtab,$datenaisad,$sexe,$datecrea,$type_cpte,$statut,$lieunais,$sessionscolaire,$codeEtab,$allergie,$condphy,$statuseleve,$doublant);
  for($i=0;$i<$nball;$i++)
  {
    $parentid=$taballparents[$i];

    $verif=$student->DetermineNumberOfparenter($parentid,$studentid);

    if($verif==0)
    {
      $student->Addparenter($parentid,$studentid);
    }
  }

  // $_SESSION['user']['addStudok']="Une Inscription à bien été prise en compte dans le système avec succès";
  $_SESSION['user']['addStudok']=L::InscriptionMessageSuccess;



  if($_SESSION['user']['profile'] == "Admin_globale")

  {

  header("Location:../manager/admission.php");

  }else if($_SESSION['user']['profile'] == "Admin_locale")

  {

    if($_SESSION['user']['paysid']==4)
    {

      header("Location:../localecmr/admission.php");
    }else {
      header("Location:../locale/admission.php");
    }

  }

}


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  // nous sommes dans un cas de matricule automatique
}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{


  // nous sommes dans le cas d'un etablissement mixte

  $inscript=htmlspecialchars($_POST['inscript']);

  if($inscript==1)
  {
    //nouvelle inscription
    //recuperation des variables

    $matri=htmlspecialchars($_POST['matri']);
    $nomad=htmlspecialchars($_POST['nomad']);
    $prenomad=htmlspecialchars($_POST['prenomad']);
    $datenaisad=htmlspecialchars($_POST['datenaisad']);
    $datenaisad=dateFormat($datenaisad);
    $lieunais=htmlspecialchars($_POST['lieunais']);
    $sexe=htmlspecialchars($_POST['sexe']);
    $classeEtab=htmlspecialchars($_POST['classeEtab']);
    $lastschool=htmlspecialchars($_POST['lastschool']);
    $cantineEtab=htmlspecialchars($_POST['cantineEtab']);
    $sessionscolaire=htmlspecialchars($_POST['sessionscolaire']);
    $codeEtab=htmlspecialchars($_POST['codeEtab']);
    $datecrea=date("Y-m-d");
    $type_cpteStudent="Student";
    $statut=1;
    $blood=htmlspecialchars($_POST['blood']);
    $pinchildhopial=htmlspecialchars($_POST['pinchildhopial']);
    $hopital=htmlspecialchars($_POST['hopital']);
    $doctor=htmlspecialchars($_POST['doctor']);
    $phonedoctor=htmlspecialchars($_POST['phonedoctor']);
    $localisation=htmlspecialchars($_POST['localisation']);
    $adresgeodetails=htmlspecialchars($_POST['adresgeodetails']);
    $guardian=htmlspecialchars($_POST['guardian']);
    $telBguardian=htmlspecialchars($_POST['telBguardian']);
    $telMobguardian=htmlspecialchars($_POST['telMobguardian']);
    $domicileguardian=htmlspecialchars($_POST['domicileguardian']);
    $allergies="";
    $infantiles="";




    foreach($_POST['allergiestudents'] as $valeur)
    {
     $allergies=$allergies.$valeur.',';

    }

    foreach($_POST['deseasesinfant'] as $valeur)
    {
     $infantiles=$infantiles.$valeur.',';

    }




    //gestion des parents
    $concatoldparent=htmlspecialchars($_POST['concatoldparents']);
    $concatparents=htmlspecialchars($_POST['concatparents']);

    $tabnewparent=explode("@",$concatparents);
    $taboldparent=explode("@",$concatoldparent);
    $nbnewparent=count($tabnewparent);
    $nbparent=htmlspecialchars($_POST['nbparent']);
    $nbparentold=htmlspecialchars($_POST['nboldparent']);
    $allparentids="";

    for($i=0;$i<$nbparent;$i++)
    {
      //insertion dans la table parent et retour de l'id


      $datecreaparent=date("Y-m-d");
      $type_cpteParent="Parent";
      $statutparent=1;
      $nomparent=htmlspecialchars($_POST['nomparent'.$tabnewparent[$i]]);
      $prenomparent=htmlspecialchars($_POST['prenomparent'.$tabnewparent[$i]]);
      $phoneparent=htmlspecialchars($_POST['phoneparent'.$tabnewparent[$i]]);
      $sexeparent=htmlspecialchars($_POST['sexeparent'.$tabnewparent[$i]]);
      $metierparent=htmlspecialchars($_POST['metierparent'.$tabnewparent[$i]]);
      $employeurparent=htmlspecialchars($_POST['employeurparent'.$tabnewparent[$i]]);
      $postaleadressparent=htmlspecialchars($_POST['postaleadressparent'.$tabnewparent[$i]]);
      $phonehomeparent=htmlspecialchars($_POST['phonehomeparent'.$tabnewparent[$i]]);
      $emailparent=htmlspecialchars($_POST['emailparent'.$tabnewparent[$i]]);

      $idparentcompte=$parentx->AddparentchapMixte($nomparent,$prenomparent,$phoneparent,$sexeparent,$metierparent,$employeurparent,$postaleadressparent,$phonehomeparent,$emailparent,$datecreaparent,$type_cpteParent,$statutparent,$codeEtab);

      $allparentids=$allparentids.$idparentcompte."@";

    }

    $allparentids=$allparentids.$concatoldparent;

    for($i=0;$i<$nbparentold;$i++)
    {

      $parentid=$taboldparent[$i];

      $nombreP=$parentx->getspecificParentSchool($parentid,$codeEtab);

      if($nombreP>0)
      {

      }else {
        //nous allons ajouter ce parent dans cet etablissement

        $parentx->Addoldparent($parentid,$codeEtab);
      }
    }

    $taballparents=explode("@",$allparentids);
    $nball=count($taballparents)-1;

    //ajout du nouveau eleve

    $studentid=$student->AddStudentMIxte($matri,$nomad,$prenomad,$classeEtab,$datenaisad,$sexe,$datecrea,$type_cpteStudent,$statut,$lieunais,$sessionscolaire,$codeEtab,$cantineEtab,$lastschool);

    for($i=0;$i<$nball;$i++)
    {
      $parentid=$taballparents[$i];

      $verif=$student->DetermineNumberOfparenter($parentid,$studentid);

      if($verif==0)
      {
        // echo "ajouter";
        $student->Addparenter($parentid,$studentid);
      }else {
        // echo "non ajouter";
      }
    }

    //ajouter dans le medicalform

    $medicalformid=$student->AddMedicalForm($codeEtab,$studentid,$blood,$pinchildhopial,$hopital,$doctor,$phonedoctor,$allergies,$infantiles,$localisation,$adresgeodetails,$guardian,$telBguardian,$telMobguardian,$domicileguardian);

    //nous allons ajouter les antecedents

    $concatantecedent=htmlspecialchars($_POST['concatantecedent']);
    $tabantecedent=explode("@",$concatantecedent);
    $nbconcatantecedent=htmlspecialchars($_POST['nbconcatantecedent']);

    for($i=0;$i<$nbconcatantecedent;$i++)
    {
      $libelleantecedet=htmlspecialchars($_POST['libelleantecedet'.$tabantecedent[$i]]);
      $childantecedet=htmlspecialchars($_POST['childantecedet'.$tabantecedent[$i]]);
      $fatherantecedet=htmlspecialchars($_POST['fatherantecedet'.$tabantecedent[$i]]);
      $motherantecedet=htmlspecialchars($_POST['motherantecedet'.$tabantecedent[$i]]);

      $student->AddantecedentMedical($studentid,$medicalformid,$libelleantecedet,$childantecedet,$fatherantecedet,$motherantecedet);
    }


    //nous allons uploader les fichiers

    $fileError = $_FILES['photoad']['error'];
    $fileCarnetError = $_FILES['carnet']['error'];
    $fileContratError = $_FILES['contrat']['error'];

    if($fileError==0)
    {
      $file_name = @$_FILES['photoad']['name'];

      $_SESSION["photoad"] = $file_name;

      $file_size =@$_FILES['photoad']['size'];

      $file_tmp =@$_FILES['photoad']['tmp_name'];

      $file_type=@$_FILES['photoad']['type'];

      @$file_ext=strtolower(end(explode('.',@$_FILES['photoad']['name'])));

      $fichierTemp = uniqid() . "." . $file_ext;

      $transactionId =  "STU".date("Y") . date("m").strtoupper(substr($nomad, 0, 2)).$matri;

      $fichierad=$transactionId.".".$file_ext;

      move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

      $dossier="../photo/Students/";

      $dossier1="../photo/Students/".$matri;

      if(!is_dir($dossier)) {
            //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
            @mkdir($dossier);
            @mkdir($dossier1);

                }else
                {
           @mkdir($dossier1);
                }
          @rename('../temp/' . $fichierTemp , "../photo/Students/" . $matri."/".$fichierad);

               //Suppression du fichier se trouvant dans le dossier temp

          @unlink("../temp/" . $fichierTemp);

          //nous allons mettre a jour l'image dans le compte de l'eleve

          $student->UpdateStudentProfiletof($fichierad,$studentid);


    }

    if($fileCarnetError==0)
    {
      $file_name = @$_FILES['carnet']['name'];

      $_SESSION["carnet"] = $file_name;

      $file_size =@$_FILES['carnet']['size'];

      $file_tmp =@$_FILES['carnet']['tmp_name'];

      $file_type=@$_FILES['carnet']['type'];

      @$file_ext=strtolower(end(explode('.',@$_FILES['carnet']['name'])));

      $fichierTemp = uniqid() . "." . $file_ext;

      $transactionId =  "STU".date("Y") . date("m").strtoupper(substr($nomad, 0, 2)).$matri;

      $fichierad=$transactionId.".".$file_ext;

      move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

      $dossier="../photo/Carnets/";

      $dossier1="../photo/Carnets/".$matri;

      if(!is_dir($dossier)) {
            //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
            @mkdir($dossier);
            @mkdir($dossier1);

                }else
                {
           @mkdir($dossier1);
                }

                @rename('../temp/' . $fichierTemp , "../photo/Carnets/" . $matri."/".$fichierad);

                     //Suppression du fichier se trouvant dans le dossier temp

                @unlink("../temp/" . $fichierTemp);

        $student->UpdateStudentCarnetFile($fichierad,$medicalformid,$studentid);



    }

    if($fileContratError==0)
    {
      $file_name = @$_FILES['contrat']['name'];

      $_SESSION["contrat"] = $file_name;

      $file_size =@$_FILES['contrat']['size'];

      $file_tmp =@$_FILES['contrat']['tmp_name'];

      $file_type=@$_FILES['contrat']['type'];

      @$file_ext=strtolower(end(explode('.',@$_FILES['contrat']['name'])));

      $fichierTemp = uniqid() . "." . $file_ext;

      $transactionId =  "STU".date("Y") . date("m").strtoupper(substr($nomad, 0, 2)).$matri;

      $fichierad=$transactionId.".".$file_ext;

      move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

      $dossier="../photo/Contrats/";

      $dossier1="../photo/Contrats/".$matri;

      if(!is_dir($dossier)) {
            //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
            @mkdir($dossier);
            @mkdir($dossier1);

                }else
                {
           @mkdir($dossier1);
                }

                @rename('../temp/' . $fichierTemp , "../photo/Contrats/" . $matri."/".$fichierad);

                     //Suppression du fichier se trouvant dans le dossier temp

                @unlink("../temp/" . $fichierTemp);

$student->UpdateStudentContratFile($fichierad,$codeEtab,$sessionscolaire,$studentid);

    }







  }else if($inscript==2)
  {
    //une reinscription d'un ancien eleve

    $matri=htmlspecialchars($_POST['matriselect']);
    $studentid=$student->getstudentcompteid($matri);
    $classeEtab=htmlspecialchars($_POST['classeEtab']);
    $lastschool=htmlspecialchars($_POST['lastschool']);
    $cantineEtab=htmlspecialchars($_POST['cantineEtab']);
    $sessionscolaire=htmlspecialchars($_POST['sessionscolaire']);
    $codeEtab=htmlspecialchars($_POST['codeEtab']);
    $datecrea=date("Y-m-d");
    $type_cpteStudent="Student";
    $statut=1;
    $blood=htmlspecialchars($_POST['blood']);
    $pinchildhopial=htmlspecialchars($_POST['pinchildhopial']);
    $hopital=htmlspecialchars($_POST['hopital']);
    $doctor=htmlspecialchars($_POST['doctor']);
    $phonedoctor=htmlspecialchars($_POST['phonedoctor']);
    $localisation=htmlspecialchars($_POST['localisation']);
    $adresgeodetails=htmlspecialchars($_POST['adresgeodetails']);
    $guardian=htmlspecialchars($_POST['guardian']);
    $telBguardian=htmlspecialchars($_POST['telBguardian']);
    $telMobguardian=htmlspecialchars($_POST['telMobguardian']);
    $domicileguardian=htmlspecialchars($_POST['domicileguardian']);
    $allergies="";
    $infantiles="";


    foreach($_POST['allergiestudents'] as $valeur)
    {
     $allergies=$allergies.$valeur.',';

    }

    foreach($_POST['deseasesinfant'] as $valeur)
    {
     $infantiles=$infantiles.$valeur.',';

    }




    //gestion des parents
    $concatoldparent=htmlspecialchars($_POST['concatoldparents']);
    $concatparents=htmlspecialchars($_POST['concatparents']);

    $tabnewparent=explode("@",$concatparents);
    $taboldparent=explode("@",$concatoldparent);
    $nbnewparent=count($tabnewparent);
    $nbparent=htmlspecialchars($_POST['nbparent']);
    $nbparentold=htmlspecialchars($_POST['nboldparent']);
    $allparentids="";

    for($i=0;$i<$nbparent;$i++)
    {
      //insertion dans la table parent et retour de l'id


      $datecreaparent=date("Y-m-d");
      $type_cpteParent="Parent";
      $statutparent=1;
      $nomparent=htmlspecialchars($_POST['nomparent'.$tabnewparent[$i]]);
      $prenomparent=htmlspecialchars($_POST['prenomparent'.$tabnewparent[$i]]);
      $phoneparent=htmlspecialchars($_POST['phoneparent'.$tabnewparent[$i]]);
      $sexeparent=htmlspecialchars($_POST['sexeparent'.$tabnewparent[$i]]);
      $metierparent=htmlspecialchars($_POST['metierparent'.$tabnewparent[$i]]);
      $employeurparent=htmlspecialchars($_POST['employeurparent'.$tabnewparent[$i]]);
      $postaleadressparent=htmlspecialchars($_POST['postaleadressparent'.$tabnewparent[$i]]);
      $phonehomeparent=htmlspecialchars($_POST['phonehomeparent'.$tabnewparent[$i]]);
      $emailparent=htmlspecialchars($_POST['emailparent'.$tabnewparent[$i]]);

      $idparentcompte=$parentx->AddparentchapMixte($nomparent,$prenomparent,$phoneparent,$sexeparent,$metierparent,$employeurparent,$postaleadressparent,$phonehomeparent,$emailparent,$datecreaparent,$type_cpteParent,$statutparent,$codeEtab);

      $allparentids=$allparentids.$idparentcompte."@";

    }

    $allparentids=$allparentids.$concatoldparent;

    for($i=0;$i<$nbparentold;$i++)
    {

      $parentid=$taboldparent[$i];

      $nombreP=$parentx->getspecificParentSchool($parentid,$codeEtab);

      if($nombreP>0)
      {

      }else {
        //nous allons ajouter ce parent dans cet etablissement

        $parentx->Addoldparent($parentid,$codeEtab);
      }
    }

    $taballparents=explode("@",$allparentids);
    $nball=count($taballparents)-1;

    for($i=0;$i<$nball;$i++)
    {
      $parentid=$taballparents[$i];

      $verif=$student->DetermineNumberOfparenter($parentid,$studentid);

      if($verif==0)
      {
        $student->Addparenter($parentid,$studentid);
      }
    }

    //ajouter dans le medicalform

    $medicalformid=$student->AddMedicalForm($codeEtab,$studentid,$blood,$pinchildhopial,$hopital,$doctor,$phonedoctor,$allergies,$infantiles,$localisation,$adresgeodetails,$guardian,$telBguardian,$telMobguardian,$domicileguardian);

    //nous allons ajouter les antecedents

    $concatantecedent=htmlspecialchars($_POST['concatantecedent']);
    $tabantecedent=explode("@",$concatantecedent);
    $nbconcatantecedent=htmlspecialchars($_POST['nbconcatantecedent']);

    for($i=0;$i<$nbconcatantecedent;$i++)
    {
      $libelleantecedet=htmlspecialchars($_POST['libelleantecedet'.$tabantecedent[$i]]);
      $childantecedet=htmlspecialchars($_POST['childantecedet'.$tabantecedent[$i]]);
      $fatherantecedet=htmlspecialchars($_POST['fatherantecedet'.$tabantecedent[$i]]);
      $motherantecedet=htmlspecialchars($_POST['motherantecedet'.$tabantecedent[$i]]);

      $student->AddantecedentMedical($studentid,$medicalformid,$libelleantecedet,$childantecedet,$fatherantecedet,$motherantecedet);
    }


    //nous allons uploader les fichiers

    $fileError = $_FILES['photoad']['error'];
    $fileCarnetError = $_FILES['carnet']['error'];
    $fileContratError = $_FILES['contrat']['error'];

    if($fileError==0)
    {
      $file_name = @$_FILES['photoad']['name'];

      $_SESSION["photoad"] = $file_name;

      $file_size =@$_FILES['photoad']['size'];

      $file_tmp =@$_FILES['photoad']['tmp_name'];

      $file_type=@$_FILES['photoad']['type'];

      @$file_ext=strtolower(end(explode('.',@$_FILES['photoad']['name'])));

      $fichierTemp = uniqid() . "." . $file_ext;

      $transactionId =  "STU".date("Y") . date("m").strtoupper(substr($nomad, 0, 2)).$matri;

      $fichierad=$transactionId.".".$file_ext;

      move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

      $dossier="../photo/Students/";

      $dossier1="../photo/Students/".$matri;

      if(!is_dir($dossier)) {
            //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
            @mkdir($dossier);
            @mkdir($dossier1);

                }else
                {
           @mkdir($dossier1);
                }
          @rename('../temp/' . $fichierTemp , "../photo/Students/" . $matri."/".$fichierad);

               //Suppression du fichier se trouvant dans le dossier temp

          @unlink("../temp/" . $fichierTemp);
    }

    if($fileCarnetError==0)
    {
      $file_name = @$_FILES['carnet']['name'];

      $_SESSION["carnet"] = $file_name;

      $file_size =@$_FILES['carnet']['size'];

      $file_tmp =@$_FILES['carnet']['tmp_name'];

      $file_type=@$_FILES['carnet']['type'];

      @$file_ext=strtolower(end(explode('.',@$_FILES['carnet']['name'])));

      $fichierTemp = uniqid() . "." . $file_ext;

      $transactionId =  "STU".date("Y") . date("m").strtoupper(substr($nomad, 0, 2)).$matri;

      $fichierad=$transactionId.".".$file_ext;

      move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

      $dossier="../photo/Carnets/";

      $dossier1="../photo/Carnets/".$matri;

      if(!is_dir($dossier)) {
            //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
            @mkdir($dossier);
            @mkdir($dossier1);

                }else
                {
           @mkdir($dossier1);
                }

                @rename('../temp/' . $fichierTemp , "../photo/Carnets/" . $matri."/".$fichierad);

                     //Suppression du fichier se trouvant dans le dossier temp

                @unlink("../temp/" . $fichierTemp);



    }

    if($fileContratError==0)
    {
      $file_name = @$_FILES['contrat']['name'];

      $_SESSION["contrat"] = $file_name;

      $file_size =@$_FILES['contrat']['size'];

      $file_tmp =@$_FILES['contrat']['tmp_name'];

      $file_type=@$_FILES['contrat']['type'];

      @$file_ext=strtolower(end(explode('.',@$_FILES['contrat']['name'])));

      $fichierTemp = uniqid() . "." . $file_ext;

      $transactionId =  "STU".date("Y") . date("m").strtoupper(substr($nomad, 0, 2)).$matri;

      $fichierad=$transactionId.".".$file_ext;

      move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

      $dossier="../photo/Contrats/";

      $dossier1="../photo/Contrats/".$matri;

      if(!is_dir($dossier)) {
            //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
            @mkdir($dossier);
            @mkdir($dossier1);

                }else
                {
           @mkdir($dossier1);
                }

                @rename('../temp/' . $fichierTemp , "../photo/Contrats/" . $matri."/".$fichierad);

                     //Suppression du fichier se trouvant dans le dossier temp

                @unlink("../temp/" . $fichierTemp);



    }




  }


  //nous avons fini les opérations alors nous allons faire la redirection

  // $_SESSION['user']['addStudok']="Une nouvelle inscription à bien été prise en compte dans le système avec succès";
  $_SESSION['user']['addStudok']=L::InscriptionMessageSuccess;

    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

  if($_SESSION['user']['profile'] == "Admin_globale")

  {

  header("Location:../manager/admission.php");

  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {
header("Location:../locale".$libelleEtab."/admission.php");


  }


}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //il est question d'un etablissement mixte mais avec saisie des allergies et maladies infantiles


$inscript=htmlspecialchars($_POST['inscript']);

if($inscript==1)
{
  //nouvelle inscription
  //recuperation des variables

  $matri=htmlspecialchars($_POST['matri']);
  $nomad=htmlspecialchars($_POST['nomad']);
  $prenomad=htmlspecialchars($_POST['prenomad']);
  $datenaisad=htmlspecialchars($_POST['datenaisad']);
  $datenaisad=dateFormat($datenaisad);
  $lieunais=htmlspecialchars($_POST['lieunais']);
  $sexe=htmlspecialchars($_POST['sexe']);
  $classeEtab=htmlspecialchars($_POST['classeEtab']);
  $lastschool=htmlspecialchars($_POST['lastschool']);
  $cantineEtab=htmlspecialchars($_POST['cantineEtab']);
  $sessionscolaire=htmlspecialchars($_POST['sessionscolaire']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $datecrea=date("Y-m-d");
  $type_cpteStudent="Student";
  $statut=1;
  $blood=htmlspecialchars($_POST['blood']);
  $pinchildhopial=htmlspecialchars($_POST['pinchildhopial']);
  $hopital=htmlspecialchars($_POST['hopital']);
  $doctor=htmlspecialchars($_POST['doctor']);
  $phonedoctor=htmlspecialchars($_POST['phonedoctor']);
  $localisation=htmlspecialchars($_POST['localisation']);
  $adresgeodetails=htmlspecialchars($_POST['adresgeodetails']);
  $guardian=htmlspecialchars($_POST['guardian']);
  $telBguardian=htmlspecialchars($_POST['telBguardian']);
  $telMobguardian=htmlspecialchars($_POST['telMobguardian']);
  $domicileguardian=htmlspecialchars($_POST['domicileguardian']);
  //le nombre de chaque element

  $nbparent=htmlspecialchars($_POST['nbparent']);
  $nbparentold=htmlspecialchars($_POST['nboldparent']);
  $nbconcatantecedent=htmlspecialchars($_POST['nbconcatantecedent']);

  //gestion des parents

  $concatoldparent=htmlspecialchars($_POST['concatoldparents']);
  $concatparents=htmlspecialchars($_POST['concatparents']);
  $tabnewparent=explode("@",$concatparents);
  $taboldparent=explode("@",$concatoldparent);
  $nbnewparent=count($tabnewparent);
  $allparentids="";

  for($i=0;$i<$nbparent;$i++)
  {
    //insertion dans la table parent et retour de l'id


    $datecreaparent=date("Y-m-d");
    $type_cpteParent="Parent";
    $statutparent=1;
    $nomparent=htmlspecialchars($_POST['nomparent'.$tabnewparent[$i]]);
    $prenomparent=htmlspecialchars($_POST['prenomparent'.$tabnewparent[$i]]);
    $phoneparent=htmlspecialchars($_POST['phoneparent'.$tabnewparent[$i]]);
    $sexeparent=htmlspecialchars($_POST['sexeparent'.$tabnewparent[$i]]);
    $metierparent=htmlspecialchars($_POST['metierparent'.$tabnewparent[$i]]);
    $employeurparent=htmlspecialchars($_POST['employeurparent'.$tabnewparent[$i]]);
    $postaleadressparent=htmlspecialchars($_POST['postaleadressparent'.$tabnewparent[$i]]);
    $phonehomeparent=htmlspecialchars($_POST['phonehomeparent'.$tabnewparent[$i]]);
    $emailparent=htmlspecialchars($_POST['emailparent'.$tabnewparent[$i]]);
    $mobileparent=htmlspecialchars($_POST['mobileparent'.$tabnewparent[$i]]);

    $idparentcompte=$parentx->AddparentchapMixte($nomparent,$prenomparent,$phoneparent,$sexeparent,$metierparent,$employeurparent,$postaleadressparent,$phonehomeparent,$emailparent,$datecreaparent,$type_cpteParent,$statutparent,$codeEtab,$mobileparent);

    $allparentids=$allparentids.$idparentcompte."@";

  }

  $allparentids=$allparentids.$concatoldparent;

  for($i=0;$i<$nbparentold;$i++)
  {

    $parentid=$taboldparent[$i];

    $nombreP=$parentx->getspecificParentSchool($parentid,$codeEtab);

    if($nombreP>0)
    {

    }else {
      //nous allons ajouter ce parent dans cet etablissement

      $parentx->Addoldparent($parentid,$codeEtab);
    }
  }

  $taballparents=explode("@",$allparentids);
  $nball=count($taballparents)-1;

  //ajout du nouveau eleve

  $studentid=$student->AddStudentMIxte($matri,$nomad,$prenomad,$classeEtab,$datenaisad,$sexe,$datecrea,$type_cpteStudent,$statut,$lieunais,$sessionscolaire,$codeEtab,$cantineEtab,$lastschool);

  for($i=0;$i<$nball;$i++)
  {
    $parentid=$taballparents[$i];

    $verif=$student->DetermineNumberOfparenter($parentid,$studentid);

    if($verif==0)
    {
      // echo "ajouter";
      $student->Addparenter($parentid,$studentid);
    }else {
      // echo "non ajouter";
    }
  }

  //ajout des allergies et maladies infantiles

  $allergies="";
  $infantiles="";

  $nballergies=htmlspecialchars($_POST['nballergies']);
  $nbinfantilesD=htmlspecialchars($_POST['nbinfantilesD']);

  if($nballergies>0)
  {
    $concatnballergies=htmlspecialchars($_POST['concatnballergies']);
    $concatnballergies=substr($concatnballergies, 0, -1);
    $taballergies=explode("@",$concatnballergies);
    $nb=count($taballergies);


    for($i=0;$i<$nb;$i++)
    {
      $Allergies=htmlspecialchars($_POST['Allergies_'.$taballergies[$i]]);
      $type="ALLERGIES";

      $allergiesid=$student->AddAllergiesStudent($Allergies,$codeEtab,$type);

      $allergies=$allergies.$allergiesid.",";

    }

  }

  //ajouter les maladies infantiles

  if($nbinfantilesD>0)
  {

    $concatnbinfantilesD=htmlspecialchars($_POST['concatnbinfantilesD']);
    $concatnbinfantilesD=substr($concatnbinfantilesD, 0, -1);
    $tabmaladie=explode("@",$concatnbinfantilesD);
    $nb=count($tabmaladie);

    for($i=0;$i<$nb;$i++)
    {
      $maladies=htmlspecialchars($_POST['Infantiles_'.$tabmaladie[$i]]);
      $type="INFANTILES";

      $infantilesid=$student->AddInfantilesStudent($maladies,$codeEtab,$type);

      $infantiles=$infantiles.$infantilesid.",";

    }
  }

  //nous allons ajouter le formulaire medical

  $medicalformid=$student->AddMedicalForm($codeEtab,$studentid,$blood,$pinchildhopial,$hopital,$doctor,$phonedoctor,$allergies,$infantiles,$localisation,$adresgeodetails,$guardian,$telBguardian,$telMobguardian,$domicileguardian);
  //
  $concatantecedent=htmlspecialchars($_POST['concatantecedent']);
  $tabantecedent=explode("@",$concatantecedent);
  $nbconcatantecedent=htmlspecialchars($_POST['nbconcatantecedent']);
  //
  for($i=0;$i<$nbconcatantecedent;$i++)
  {
    $libelleantecedet=htmlspecialchars($_POST['libelleantecedet'.$tabantecedent[$i]]);
    $childantecedet=htmlspecialchars($_POST['childantecedet'.$tabantecedent[$i]]);
    $fatherantecedet=htmlspecialchars($_POST['fatherantecedet'.$tabantecedent[$i]]);
    $motherantecedet=htmlspecialchars($_POST['motherantecedet'.$tabantecedent[$i]]);

    $type="ANTECEDENTS";

    $antecedentid=$student->AddAntecedentsStudent($libelleantecedet,$codeEtab,$type);

    echo $antecedentid;


    $student->AddantecedentMedical($studentid,$medicalformid,$antecedentid,$childantecedet,$fatherantecedet,$motherantecedet);
  }

  //nous allons uploader les fichiers

  $fileError = $_FILES['photoad']['error'];
  $fileCarnetError = $_FILES['carnet']['error'];
  $fileContratError = $_FILES['contrat']['error'];

  if($fileError==0)
  {
    $file_name = @$_FILES['photoad']['name'];

    $_SESSION["photoad"] = $file_name;

    $file_size =@$_FILES['photoad']['size'];

    $file_tmp =@$_FILES['photoad']['tmp_name'];

    $file_type=@$_FILES['photoad']['type'];

    @$file_ext=strtolower(end(explode('.',@$_FILES['photoad']['name'])));

    $fichierTemp = uniqid() . "." . $file_ext;

    $transactionId =  "STU".date("Y") . date("m").strtoupper(substr($nomad, 0, 2)).$matri;

    $fichierad=$transactionId.".".$file_ext;

    move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

    $dossier="../photo/Students/";

    $dossier1="../photo/Students/".$matri;

    if(!is_dir($dossier)) {
          //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
          @mkdir($dossier);
          @mkdir($dossier1);

              }else
              {
         @mkdir($dossier1);
              }
        @rename('../temp/' . $fichierTemp , "../photo/Students/" . $matri."/".$fichierad);

             //Suppression du fichier se trouvant dans le dossier temp

        @unlink("../temp/" . $fichierTemp);

        //nous allons mettre a jour l'image dans le compte de l'eleve

        $student->UpdateStudentProfiletof($fichierad,$studentid);


  }

  if($fileCarnetError==0)
  {
    $file_name = @$_FILES['carnet']['name'];

    $_SESSION["carnet"] = $file_name;

    $file_size =@$_FILES['carnet']['size'];

    $file_tmp =@$_FILES['carnet']['tmp_name'];

    $file_type=@$_FILES['carnet']['type'];

    @$file_ext=strtolower(end(explode('.',@$_FILES['carnet']['name'])));

    $fichierTemp = uniqid() . "." . $file_ext;

    $transactionId =  "STU".date("Y") . date("m").strtoupper(substr($nomad, 0, 2)).$matri;

    $fichierad=$transactionId.".".$file_ext;

    move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

    $dossier="../photo/Carnets/";

    $dossier1="../photo/Carnets/".$matri;

    if(!is_dir($dossier)) {
          //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
          @mkdir($dossier);
          @mkdir($dossier1);

              }else
              {
         @mkdir($dossier1);
              }

              @rename('../temp/' . $fichierTemp , "../photo/Carnets/" . $matri."/".$fichierad);

                   //Suppression du fichier se trouvant dans le dossier temp

              @unlink("../temp/" . $fichierTemp);

      $student->UpdateStudentCarnetFile($fichierad,$medicalformid,$studentid);



  }

  if($fileContratError==0)
  {
    $file_name = @$_FILES['contrat']['name'];

    $_SESSION["contrat"] = $file_name;

    $file_size =@$_FILES['contrat']['size'];

    $file_tmp =@$_FILES['contrat']['tmp_name'];

    $file_type=@$_FILES['contrat']['type'];

    @$file_ext=strtolower(end(explode('.',@$_FILES['contrat']['name'])));

    $fichierTemp = uniqid() . "." . $file_ext;

    $transactionId =  "STU".date("Y") . date("m").strtoupper(substr($nomad, 0, 2)).$matri;

    $fichierad=$transactionId.".".$file_ext;

    move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

    $dossier="../photo/Contrats/";

    $dossier1="../photo/Contrats/".$matri;

    if(!is_dir($dossier)) {
          //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
          @mkdir($dossier);
          @mkdir($dossier1);

              }else
              {
         @mkdir($dossier1);
              }

              @rename('../temp/' . $fichierTemp , "../photo/Contrats/" . $matri."/".$fichierad);

                   //Suppression du fichier se trouvant dans le dossier temp

              @unlink("../temp/" . $fichierTemp);

$student->UpdateStudentContratFile($fichierad,$codeEtab,$sessionscolaire,$studentid);

  }




}else if($inscript==2)
{
  $matri=htmlspecialchars($_POST['matriselect']);
  $studentid=$student->getstudentcompteid($matri);
  $classeEtab=htmlspecialchars($_POST['classeEtab']);
  $lastschool=htmlspecialchars($_POST['lastschool']);
  $cantineEtab=htmlspecialchars($_POST['cantineEtab']);
  $sessionscolaire=htmlspecialchars($_POST['sessionscolaire']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $datecrea=date("Y-m-d");
  $type_cpteStudent="Student";
  $statut=1;
  $blood=htmlspecialchars($_POST['blood']);
  $pinchildhopial=htmlspecialchars($_POST['pinchildhopial']);
  $hopital=htmlspecialchars($_POST['hopital']);
  $doctor=htmlspecialchars($_POST['doctor']);
  $phonedoctor=htmlspecialchars($_POST['phonedoctor']);
  $localisation=htmlspecialchars($_POST['localisation']);
  $adresgeodetails=htmlspecialchars($_POST['adresgeodetails']);
  $guardian=htmlspecialchars($_POST['guardian']);
  $telBguardian=htmlspecialchars($_POST['telBguardian']);
  $telMobguardian=htmlspecialchars($_POST['telMobguardian']);
  $domicileguardian=htmlspecialchars($_POST['domicileguardian']);

  //le nombre de chaque element

  $nbparent=htmlspecialchars($_POST['nbparent']);
  $nbparentold=htmlspecialchars($_POST['nboldparent']);
  $nbconcatantecedent=htmlspecialchars($_POST['nbconcatantecedent']);

  //gestion des parents

  $concatoldparent=htmlspecialchars($_POST['concatoldparents']);
  $concatparents=htmlspecialchars($_POST['concatparents']);
  $tabnewparent=explode("@",$concatparents);
  $taboldparent=explode("@",$concatoldparent);
  $nbnewparent=count($tabnewparent);
  $allparentids="";

  for($i=0;$i<$nbparent;$i++)
  {
    //insertion dans la table parent et retour de l'id


    $datecreaparent=date("Y-m-d");
    $type_cpteParent="Parent";
    $statutparent=1;
    $nomparent=htmlspecialchars($_POST['nomparent'.$tabnewparent[$i]]);
    $prenomparent=htmlspecialchars($_POST['prenomparent'.$tabnewparent[$i]]);
    $phoneparent=htmlspecialchars($_POST['phoneparent'.$tabnewparent[$i]]);
    $sexeparent=htmlspecialchars($_POST['sexeparent'.$tabnewparent[$i]]);
    $metierparent=htmlspecialchars($_POST['metierparent'.$tabnewparent[$i]]);
    $employeurparent=htmlspecialchars($_POST['employeurparent'.$tabnewparent[$i]]);
    $postaleadressparent=htmlspecialchars($_POST['postaleadressparent'.$tabnewparent[$i]]);
    $phonehomeparent=htmlspecialchars($_POST['phonehomeparent'.$tabnewparent[$i]]);
    $emailparent=htmlspecialchars($_POST['emailparent'.$tabnewparent[$i]]);

    $idparentcompte=$parentx->AddparentchapMixte($nomparent,$prenomparent,$phoneparent,$sexeparent,$metierparent,$employeurparent,$postaleadressparent,$phonehomeparent,$emailparent,$datecreaparent,$type_cpteParent,$statutparent,$codeEtab);

    $allparentids=$allparentids.$idparentcompte."@";

  }

  $allparentids=$allparentids.$concatoldparent;

  for($i=0;$i<$nbparentold;$i++)
  {

    $parentid=$taboldparent[$i];

    $nombreP=$parentx->getspecificParentSchool($parentid,$codeEtab);

    if($nombreP>0)
    {

    }else {
      //nous allons ajouter ce parent dans cet etablissement

      $parentx->Addoldparent($parentid,$codeEtab);
    }
  }

  $taballparents=explode("@",$allparentids);
  $nball=count($taballparents)-1;

  for($i=0;$i<$nball;$i++)
  {
    $parentid=$taballparents[$i];

    $verif=$student->DetermineNumberOfparenter($parentid,$studentid);

    if($verif==0)
    {
      $student->Addparenter($parentid,$studentid);
    }
  }


  //ajout des allergies et maladies infantiles

  $allergies="";
  $infantiles="";

  $nballergies=htmlspecialchars($_POST['nballergies']);
  $nbinfantilesD=htmlspecialchars($_POST['nbinfantilesD']);

  if($nballergies>0)
  {
    $concatnballergies=htmlspecialchars($_POST['concatnballergies']);
    $concatnballergies=substr($concatnballergies, 0, -1);
    $taballergies=explode("@",$concatnballergies);
    $nb=count($taballergies);


    for($i=0;$i<$nb;$i++)
    {
      $Allergies=htmlspecialchars($_POST['Allergies_'.$taballergies[$i]]);
      $type="ALLERGIES";

      $allergiesid=$student->AddAllergiesStudent($Allergies,$codeEtab,$type);

      $allergies=$allergies.$allergiesid.",";

    }

  }

  //ajouter les maladies infantiles

  if($nbinfantilesD>0)
  {

    $concatnbinfantilesD=htmlspecialchars($_POST['concatnbinfantilesD']);
    $concatnbinfantilesD=substr($concatnbinfantilesD, 0, -1);
    $tabmaladie=explode("@",$concatnbinfantilesD);
    $nb=count($tabmaladie);

    for($i=0;$i<$nb;$i++)
    {
      $maladies=htmlspecialchars($_POST['Infantiles_'.$tabmaladie[$i]]);
      $type="INFANTILES";

      $infantilesid=$student->AddInfantilesStudent($maladies,$codeEtab,$type);

      $infantiles=$infantiles.$infantilesid.",";

    }
  }

  //nous allons ajouter le formulaire medical

  $medicalformid=$student->AddMedicalForm($codeEtab,$studentid,$blood,$pinchildhopial,$hopital,$doctor,$phonedoctor,$allergies,$infantiles,$localisation,$adresgeodetails,$guardian,$telBguardian,$telMobguardian,$domicileguardian);
  //
  $concatantecedent=htmlspecialchars($_POST['concatantecedent']);
  $tabantecedent=explode("@",$concatantecedent);
  $nbconcatantecedent=htmlspecialchars($_POST['nbconcatantecedent']);
  //
  for($i=0;$i<$nbconcatantecedent;$i++)
  {
    $libelleantecedet=htmlspecialchars($_POST['libelleantecedet'.$tabantecedent[$i]]);
    $childantecedet=htmlspecialchars($_POST['childantecedet'.$tabantecedent[$i]]);
    $fatherantecedet=htmlspecialchars($_POST['fatherantecedet'.$tabantecedent[$i]]);
    $motherantecedet=htmlspecialchars($_POST['motherantecedet'.$tabantecedent[$i]]);

    $type="ANTECEDENTS";

    $antecedentid=$student->AddAntecedentsStudent($libelleantecedet,$codeEtab,$type);

    echo $antecedentid;


    $student->AddantecedentMedical($studentid,$medicalformid,$antecedentid,$childantecedet,$fatherantecedet,$motherantecedet);
  }

  //nous allons uploader les fichiers

  $fileError = $_FILES['photoad']['error'];
  $fileCarnetError = $_FILES['carnet']['error'];
  $fileContratError = $_FILES['contrat']['error'];

  if($fileError==0)
  {
    $file_name = @$_FILES['photoad']['name'];

    $_SESSION["photoad"] = $file_name;

    $file_size =@$_FILES['photoad']['size'];

    $file_tmp =@$_FILES['photoad']['tmp_name'];

    $file_type=@$_FILES['photoad']['type'];

    @$file_ext=strtolower(end(explode('.',@$_FILES['photoad']['name'])));

    $fichierTemp = uniqid() . "." . $file_ext;

    $transactionId =  "STU".date("Y") . date("m").strtoupper(substr($nomad, 0, 2)).$matri;

    $fichierad=$transactionId.".".$file_ext;

    move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

    $dossier="../photo/Students/";

    $dossier1="../photo/Students/".$matri;

    if(!is_dir($dossier)) {
          //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
          @mkdir($dossier);
          @mkdir($dossier1);

              }else
              {
         @mkdir($dossier1);
              }
        @rename('../temp/' . $fichierTemp , "../photo/Students/" . $matri."/".$fichierad);

             //Suppression du fichier se trouvant dans le dossier temp

        @unlink("../temp/" . $fichierTemp);

        //nous allons mettre a jour l'image dans le compte de l'eleve

        $student->UpdateStudentProfiletof($fichierad,$studentid);


  }

  if($fileCarnetError==0)
  {
    $file_name = @$_FILES['carnet']['name'];

    $_SESSION["carnet"] = $file_name;

    $file_size =@$_FILES['carnet']['size'];

    $file_tmp =@$_FILES['carnet']['tmp_name'];

    $file_type=@$_FILES['carnet']['type'];

    @$file_ext=strtolower(end(explode('.',@$_FILES['carnet']['name'])));

    $fichierTemp = uniqid() . "." . $file_ext;

    $transactionId =  "STU".date("Y") . date("m").strtoupper(substr($nomad, 0, 2)).$matri;

    $fichierad=$transactionId.".".$file_ext;

    move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

    $dossier="../photo/Carnets/";

    $dossier1="../photo/Carnets/".$matri;

    if(!is_dir($dossier)) {
          //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
          @mkdir($dossier);
          @mkdir($dossier1);

              }else
              {
         @mkdir($dossier1);
              }

              @rename('../temp/' . $fichierTemp , "../photo/Carnets/" . $matri."/".$fichierad);

                   //Suppression du fichier se trouvant dans le dossier temp

              @unlink("../temp/" . $fichierTemp);

      $student->UpdateStudentCarnetFile($fichierad,$medicalformid,$studentid);



  }

  if($fileContratError==0)
  {
    $file_name = @$_FILES['contrat']['name'];

    $_SESSION["contrat"] = $file_name;

    $file_size =@$_FILES['contrat']['size'];

    $file_tmp =@$_FILES['contrat']['tmp_name'];

    $file_type=@$_FILES['contrat']['type'];

    @$file_ext=strtolower(end(explode('.',@$_FILES['contrat']['name'])));

    $fichierTemp = uniqid() . "." . $file_ext;

    $transactionId =  "STU".date("Y") . date("m").strtoupper(substr($nomad, 0, 2)).$matri;

    $fichierad=$transactionId.".".$file_ext;

    move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

    $dossier="../photo/Contrats/";

    $dossier1="../photo/Contrats/".$matri;

    if(!is_dir($dossier)) {
          //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
          @mkdir($dossier);
          @mkdir($dossier1);

              }else
              {
         @mkdir($dossier1);
              }

              @rename('../temp/' . $fichierTemp , "../photo/Contrats/" . $matri."/".$fichierad);

                   //Suppression du fichier se trouvant dans le dossier temp

              @unlink("../temp/" . $fichierTemp);

$student->UpdateStudentContratFile($fichierad,$codeEtab,$sessionscolaire,$studentid);

  }


}


//nous avons fini les opérations alors nous allons faire la redirection

// $_SESSION['user']['addStudok']="Une nouvelle inscription à bien été prise en compte dans le système avec succès";
$_SESSION['user']['addStudok']=L::InscriptionMessageSuccess;

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

if($_SESSION['user']['profile'] == "Admin_globale")

{

header("Location:../manager/admission.php");

}else if($_SESSION['user']['profile'] == "Admin_locale")
{
header("Location:../locale".$libelleEtab."/admission.php");


}



}




?>
