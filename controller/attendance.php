<?php

session_start();

require_once('../controller/functions.php');

require_once('../class/Student.php');

require_once('../class/Classe.php');

require_once('../class/Matiere.php');

require_once('../class/Etablissement.php');

$classe = new Classe();

$student=new Student();

$matiere=new Matiere();

$etabs=new Etab();



if(isset($_POST['etape'])&&($_POST['etape']==1))

{



  //recupération des variables

  $allpresent=htmlspecialchars(addslashes($_POST['allpresent']));

  $classeId=htmlspecialchars(addslashes($_POST['classeId']));

  $datePresence=htmlspecialchars(addslashes($_POST['datePresence']));

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));

  $profid=htmlspecialchars(addslashes($_POST['profid']));

  $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));

//format date 14/08/2019 actuel

  $studentmat=htmlspecialchars(addslashes($_POST['studentmat']));

  $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));

  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

  $LibelleHeurePresence=htmlspecialchars(addslashes($_POST['LibelleHeurePresence']));


  $tanmatstudent=explode("*",$studentmat);



  $newdate=dateFormat($datePresence);



  $tabdate=explode("/",$datePresence);



  $libellematiere=$matiere->getMatiereLibelleByIdMat($matiereid,$codeEtab);



 $jour=$tabdate[0];

 $mois=$tabdate[1];

 $annee=$tabdate[2];



 $num = cal_days_in_month(CAL_GREGORIAN, retranscrireMois($mois), $annee);





 //recuperer le mois en lettre

 $libellemois=obtenirLibelleMois($mois);


$phonenumber="";
//recuperons l'indicatif du pays

 $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);


  //insertion dans la tables présences



  for ($i=1;$i<$nbstudent;$i++)

  {

      $statut=$_POST["optradio".$tanmatstudent[$i]];

      $matricule=$tanmatstudent[$i];



      if($statut==0)

      {

        //envoi de mail au parent de cet elève et les infos de l'eleve

        //nous allons verifier si le nombre de parent qui ont au moins un mail

        //nous allons detrminer idcompte de l'eleve

        $idcompteparent=$student->getstudentcompteid($matricule);

        $nbparentmail=$student->getNbofparentmail($idcompteparent);

        // echo $nbparentmail;

        if($nbparentmail>0)
        {

          //nous allons recuperer la liste des mails parents

          $donnees=$student->getEmailParentOfThisStudentBoucle($idcompteparent);
          // echo $idcompteparent;

          $studentDatas=$student->getAllInformationsOfStudent($idcompteparent,$libellesession);
          $studentData=explode("*",$studentDatas);
          $nomEleve=$studentData[2]." ".$studentData[3];
          $classeName=$studentData[9];

          // $tabdata=explode('*',$donnees);
          // $emailparent=$tabdata[0];
          // $nomEleve=$tabdata[1]." ".$tabdata[2];
          // $classeName=$tabdata[3];
          $EtabName=$etabs->getEtabLibellebyCodeEtab($codeEtab);
          $imageEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
          // $EtabName=$tabdata[4];
          // $imageEtab=$tabdata[5];

          //creation des variables de session
         //recuperer la date l'heure de debut et fin du cours

         foreach ($donnees as $value):
           $emailparent=$value->email_parent;
           $phonenumber=$indicatifEtab.$value->tel_parent;

           // echo $emailparent;
           $routinesdata=$etabs->getHoursOfSubject($classeId,$matiereid,$profid,$codeEtab);
           $tabroutine=explode("*",$routinesdata);
           $debutHours=$tabroutine[0];
           $finHours=$tabroutine[1];
           $msg=$student->AbsenceParentMailler($emailparent,$EtabName,$nomEleve,$classeName,$libellematiere,$debutHours,$finHours,$codeEtab,$imageEtab);
           // $etabs->Attendancesmssending($phonenumber,$emailparent,$EtabName,$nomEleve,$classeName,$libellematiere,$debutHours,$finHours,$codeEtab,$imageEtab);
           // echo $phonenumber;
           if($_SESSION['user']['paysid']==1)
           {
               // OrangecismsAttendance($phonenumber,$emailparent,$EtabName,$nomEleve,$classeName,$libellematiere,$debutHours,$finHours,$codeEtab,$imageEtab);
           }else {
             // $etabs->Attendancesmssending($phonenumber,$emailparent,$EtabName,$nomEleve,$classeName,$libellematiere,$debutHours,$finHours,$codeEtab,$imageEtab);
           }


         endforeach;


        }else
        {
          // code...
        }






      }



      //echo $msg;



      //insertion dans la table présence



        // $student->AddAttendance($newdate,$libellemois,$annee,$classeId,$matricule,$statut,$num,$matiereid,$profid,$codeEtab,$libellesession);
        $student->AddAttendanceHeure($newdate,$libellemois,$annee,$classeId,$matricule,$statut,$num,$matiereid,$profid,$codeEtab,$libellesession,$LibelleHeurePresence);
  }



//     //code etablissement de la classe
//
     $codeEtabchoolclasse=$classe->getCodeEtabOfClassesByClasseId($classeId);
//
// //

  // $_SESSION['user']['addattendailyok']="Présence classe ajouter avec succès";

  $_SESSION['user']['addattendailyok']=L::PresenceMessageSuccess;

//

$libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

   if($_SESSION['user']['profile'] == "Admin_globale")

  {

header("Location:../manager/dailyattendance.php?codeEtab=".$codeEtab);

  }else if($_SESSION['user']['profile'] == "Admin_locale")

   {
     if($_SESSION['user']['paysid']==4)
     {
       header("Location:../localecmr/dailyattendance.php");
     }else {
       $etablissementType=$etabs->DetermineTypeEtab($codeEtab);
       if($etablissementType==5)
       {
         header("Location:../locale".$libelleEtab."/dailyattendance.php");
       }else {
          header("Location:../locale/dailyattendance.php");
       }

     }


   }else if($_SESSION['user']['profile'] == "Teatcher")

   {


     $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
                 $typeetab=$etabs->DetermineTypeEtab($codeEtab);
                 $IBSAschool=$etabs->getIbsaschools();
                 $codeetabs="";
                 foreach ($IBSAschool as  $value):
                   $codeetabs=$codeetabs.$value->code_etab."*";
                 endforeach;
                 $tabetabs=explode("*",$codeetabs);

                 $nbdispenser=0;

                 $dispenserdatas=$etabs->getdispenserTeatcher($_SESSION['user']['IdCompte']);

                 foreach ($dispenserdatas as $value):
                   $code=$value->codeEtab;
                   if (in_array($code, $tabetabs)) {
                     $nbdispenser++;
                    }
                 endforeach;

                 echo $nbdispenser;

                 if($nbdispenser>0)
                 {
                   header("Location:../teatcher".$libelleEtab."/dailyattendance.php?classe=".$classeId."&codeEtab=".$codeEtab);
                 }else {
                   header("Location:../teatcher/dailyattendance.php?classe=".$classeId."&codeEtab=".$codeEtab);
                 }

 // header("Location:../teatcher/dailyattendance.php?classe=".$classeId."&codeEtab=".$codeEtab);

   }





}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recupération des variables

  $allpresent=htmlspecialchars(addslashes($_POST['allpresent']));

  $classeId=htmlspecialchars(addslashes($_POST['classeId']));

  $datePresence=htmlspecialchars(addslashes($_POST['datePresence']));

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));

  $profid=htmlspecialchars(addslashes($_POST['profid']));

  $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));

//format date 14/08/2019 actuel

  $studentmat=htmlspecialchars(addslashes($_POST['studentmat']));

  $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));

  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

  $LibelleHeurePresence=htmlspecialchars(addslashes($_POST['LibelleHeurePresence']));


  $tanmatstudent=explode("*",$studentmat);



  $newdate=dateFormat($datePresence);



  $tabdate=explode("/",$datePresence);



  $libellematiere=$matiere->getMatiereLibelleByIdMat($matiereid,$codeEtab);



 $jour=$tabdate[0];

 $mois=$tabdate[1];

 $annee=$tabdate[2];



 $num = cal_days_in_month(CAL_GREGORIAN, retranscrireMois($mois), $annee);





 //recuperer le mois en lettre

 $libellemois=obtenirLibelleMois($mois);


$phonenumber="";
//recuperons l'indicatif du pays

 $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);


  //insertion dans la tables présences



  for ($i=1;$i<$nbstudent;$i++)

  {

      $statut=$_POST["optradio".$tanmatstudent[$i]];

      $matricule=$tanmatstudent[$i];



      if($statut==0)

      {

        //envoi de mail au parent de cet elève et les infos de l'eleve

        //nous allons verifier si le nombre de parent qui ont au moins un mail

        //nous allons detrminer idcompte de l'eleve

        $idcompteparent=$student->getstudentcompteid($matricule);

        $nbparentmail=$student->getNbofparentmail($idcompteparent);

        // echo $nbparentmail;

        if($nbparentmail>0)
        {

          //nous allons recuperer la liste des mails parents

          $donnees=$student->getEmailParentOfThisStudentBoucle($idcompteparent);
          // echo $idcompteparent;

          $studentDatas=$student->getAllInformationsOfStudent($idcompteparent,$libellesession);
          $studentData=explode("*",$studentDatas);
          $nomEleve=$studentData[2]." ".$studentData[3];
          $classeName=$studentData[9];

          // $tabdata=explode('*',$donnees);
          // $emailparent=$tabdata[0];
          // $nomEleve=$tabdata[1]." ".$tabdata[2];
          // $classeName=$tabdata[3];
          $EtabName=$etabs->getEtabLibellebyCodeEtab($codeEtab);
          $imageEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
          // $EtabName=$tabdata[4];
          // $imageEtab=$tabdata[5];

          //creation des variables de session
         //recuperer la date l'heure de debut et fin du cours

         foreach ($donnees as $value):
           $emailparent=$value->email_parent;
           $phonenumber=$indicatifEtab.$value->tel_parent;

           // echo $emailparent;
           $routinesdata=$etabs->getHoursOfSubject($classeId,$matiereid,$profid,$codeEtab);
           $tabroutine=explode("*",$routinesdata);
           $debutHours=$tabroutine[0];
           $finHours=$tabroutine[1];
           $msg=$student->AbsenceParentMailler($emailparent,$EtabName,$nomEleve,$classeName,$libellematiere,$debutHours,$finHours,$codeEtab,$imageEtab);
           // $etabs->Attendancesmssending($phonenumber,$emailparent,$EtabName,$nomEleve,$classeName,$libellematiere,$debutHours,$finHours,$codeEtab,$imageEtab);
           // echo $phonenumber;
           if($_SESSION['user']['paysid']==1)
           {
               // OrangecismsAttendance($phonenumber,$emailparent,$EtabName,$nomEleve,$classeName,$libellematiere,$debutHours,$finHours,$codeEtab,$imageEtab);
           }else {
             // $etabs->Attendancesmssending($phonenumber,$emailparent,$EtabName,$nomEleve,$classeName,$libellematiere,$debutHours,$finHours,$codeEtab,$imageEtab);
           }


         endforeach;


        }else
        {
          // code...
        }






      }



      //echo $msg;



      //insertion dans la table présence



        // $student->AddAttendance($newdate,$libellemois,$annee,$classeId,$matricule,$statut,$num,$matiereid,$profid,$codeEtab,$libellesession);
        $student->AddAttendanceHeure($newdate,$libellemois,$annee,$classeId,$matricule,$statut,$num,$matiereid,$profid,$codeEtab,$libellesession,$LibelleHeurePresence);
  }



//     //code etablissement de la classe
//
     $codeEtabchoolclasse=$classe->getCodeEtabOfClassesByClasseId($classeId);
//
// //

  // $_SESSION['user']['addattendailyok']="Présence classe ajouter avec succès";

  $_SESSION['user']['addattendailyok']=L::PresenceMessageSuccess;

//

$libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

   if($_SESSION['user']['profile'] == "Admin_globale")

  {

header("Location:../manager/dailyattendance.php?codeEtab=".$codeEtab);

  }else if($_SESSION['user']['profile'] == "Admin_locale")

   {
     if($_SESSION['user']['paysid']==4)
     {
       // header("Location:../localecmr/dailyattendance.php");
     }else {
       // header("Location:../locale/dailyattendance.php");
       header("Location:../locale".$libelleEtab."/dailyattendance.php");
     }


   }else if($_SESSION['user']['profile'] == "Teatcher")

   {

     $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etabs->DetermineTypeEtab($codeEtab);
            $IBSAschool=$etabs->getIbsaschools();
            $codeetabs="";
            foreach ($IBSAschool as  $value):
              $codeetabs=$codeetabs.$value->code_etab."*";
            endforeach;
            $tabetabs=explode("*",$codeetabs);

            $nbdispenser=0;

            $dispenserdatas=$etabs->getdispenserTeatcher($_SESSION['user']['IdCompte']);

            foreach ($dispenserdatas as $value):
              $code=$value->codeEtab;
              if (in_array($code, $tabetabs)) {
                $nbdispenser++;
               }
            endforeach;

            echo $nbdispenser;

            if($nbdispenser>0)
            {
              header("Location:../teatcher".$libelleEtab."/dailyattendance.php?classe=".$classeId."&codeEtab=".$codeEtab);
            }else {
              header("Location:../teatcher/dailyattendance.php?classe=".$classeId."&codeEtab=".$codeEtab);
            }

 // header("Location:../teatcher/dailyattendance.php?classe=".$classeId."&codeEtab=".$codeEtab);

   }



}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recuperation des variables

  // example3_length: 10
  // optradio254585JK: 1
  // statut254585JK: 1
  // optradioMAT00123: 0
  // statutMAT00123: 1
  // optradioMAT00124: 2
  // statutMAT00124: 1
  // heureMAT00124: 08:30:00
  // codeEtab: 0001
  // studentmat: *MAT00124*MAT00123*254585JK
  // etape: 3
  // nbstudent: 4
  // allpresent:
  // classeId: 1
  // datePresence: 03/08/2020
  // libellesession: 2020-2021

  $allpresent=htmlspecialchars(addslashes($_POST['allpresent']));
  $classeId=htmlspecialchars(addslashes($_POST['classeId']));
  $datePresence=htmlspecialchars(addslashes($_POST['datePresence']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
//format date 14/08/2019 actuel
  $studentmat=htmlspecialchars(addslashes($_POST['studentmat']));
  $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
  $tanmatstudent=explode("*",$studentmat);

  $newdate=dateFormat($datePresence);
  echo $newdate;
  $tabdate=explode("/",$datePresence);
  $jour=$tabdate[0];
  $mois=$tabdate[1];
  $annee=$tabdate[2];
  $num = cal_days_in_month(CAL_GREGORIAN, retranscrireMois($mois), $annee);
 //recuperer le mois en lettre
  $libellemois=obtenirLibelleMois($mois);
  $phonenumber="";
//recuperons l'indicatif du pays
  $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);
  $userad=$_SESSION['user']['IdCompte'];


  for ($i=1;$i<$nbstudent;$i++)
  {

    $retard=0;

    $statut=$_POST["optradio".$tanmatstudent[$i]];
    $matricule=$tanmatstudent[$i];

    if($statut==0)
    {
      //Absent

      $student->AddAttendanceAbsent($newdate,$libellemois,$annee,$classeId,$matricule,$statut,$num,$codeEtab,$libellesession,$retard,$userad);
    }else if($statut==1)
    {
      //présent
      $student->AddAttendanceAbsent($newdate,$libellemois,$annee,$classeId,$matricule,$statut,$num,$codeEtab,$libellesession,$retard,$userad);

    }else if($statut==2)
    {
      //retard
       $retard=1;

       $heure=$_POST["heure".$tanmatstudent[$i]];
       if(isset($_POST["observation".$tanmatstudent[$i]]))
       {
         $observation=$_POST["observation".$tanmatstudent[$i]];
         $student->AddAttendanceAbsentRetardOne($newdate,$libellemois,$annee,$classeId,$matricule,$statut,$num,$codeEtab,$libellesession,$retard,$heure,$userad,$observation);
       }else {
         $observation=" ";
         $student->AddAttendanceAbsentRetardOne($newdate,$libellemois,$annee,$classeId,$matricule,$statut,$num,$codeEtab,$libellesession,$retard,$heure,$userad,$observation);
       }



    }
  }

  $_SESSION['user']['addattendailyok']=L::PresenceMessageSuccess;

$libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

   if($_SESSION['user']['profile'] == "Admin_globale")

  {

header("Location:../manager/dailyattendance.php?codeEtab=".$codeEtab);

  }else if($_SESSION['user']['profile'] == "Admin_locale")

   {
     $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
     $typeetab=$etabs->DetermineTypeEtab($codeEtab);

     if($typeetab==5)
     {
       header("Location:../locale".$libelleEtab."/dailyattendance.php");
     }else {
        header("Location:../locale/dailyattendance.php");
     }




   }else if($_SESSION['user']['profile'] == "Teatcher")

   {


     $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etabs->DetermineTypeEtab($codeEtab);
            $IBSAschool=$etabs->getIbsaschools();
            $codeetabs="";
            foreach ($IBSAschool as  $value):
              $codeetabs=$codeetabs.$value->code_etab."*";
            endforeach;
            $tabetabs=explode("*",$codeetabs);

            $nbdispenser=0;

            $dispenserdatas=$etabs->getdispenserTeatcher($_SESSION['user']['IdCompte']);

            foreach ($dispenserdatas as $value):
              $code=$value->codeEtab;
              if (in_array($code, $tabetabs)) {
                $nbdispenser++;
               }
            endforeach;

            echo $nbdispenser;

              header("Location:../teatcher".$libelleEtab."/dailyattendance.php?classe=".$classeId."&codeEtab=".$codeEtab);

            // if($nbdispenser>0)
            // {
            //   header("Location:../teatcher".$libelleEtab."/dailyattendance.php?classe=".$classeId."&codeEtab=".$codeEtab);
            // }else {
            //   header("Location:../teatcher/dailyattendance.php?classe=".$classeId."&codeEtab=".$codeEtab);
            // }



  //insertion dans la tables présences

}

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
//recuperation des variables

$allpresent=htmlspecialchars(addslashes($_POST['allpresent']));
$classeId=htmlspecialchars(addslashes($_POST['classeId']));
$datePresence=htmlspecialchars(addslashes($_POST['datePresence']));
$codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
$profid=htmlspecialchars(addslashes($_POST['profid']));
$matiereid=htmlspecialchars(addslashes($_POST['matiereid']));

//format date 14/08/2019 actuel

$studentmat=htmlspecialchars(addslashes($_POST['studentmat']));
$nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
$libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
$LibelleHeurePresence=htmlspecialchars(addslashes($_POST['LibelleHeurePresence']));
$tanmatstudent=explode("*",$studentmat);
$newdate=dateFormat($datePresence);
$tabdate=explode("/",$datePresence);
$libellematiere=$matiere->getMatiereLibelleByIdMat($matiereid,$codeEtab);
$jour=$tabdate[0];
$mois=$tabdate[1];
$annee=$tabdate[2];
$num = cal_days_in_month(CAL_GREGORIAN, retranscrireMois($mois), $annee);

//recuperer le mois en lettre

$libellemois=obtenirLibelleMois($mois);
$phonenumber="";
//recuperons l'indicatif du pays
$indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);
$userad=$_SESSION['user']['IdCompte'];

if($_SESSION['user']['primaire']==1)
{
  for ($i=1;$i<$nbstudent;$i++)
  {

    $retard=0;

    $statut=$_POST["optradio".$tanmatstudent[$i]];
    $matricule=$tanmatstudent[$i];
    $statutcomment=$_POST["statutcomment".$tanmatstudent[$i]];
    $observation=$_POST["observation".$tanmatstudent[$i]];

    if($statut==0)
    {
      //Absent

      $student->AddAttendanceAbsent($newdate,$libellemois,$annee,$classeId,$matricule,$statut,$num,$codeEtab,$libellesession,$retard,$userad);
    }else if($statut==1)
    {
      //présent
      $student->AddAttendanceAbsent($newdate,$libellemois,$annee,$classeId,$matricule,$statut,$num,$codeEtab,$libellesession,$retard,$userad);

    }else if($statut==2)
    {
      //retard
       $retard=1;

       $heure=$_POST["heure".$tanmatstudent[$i]];
       $student->AddAttendanceAbsentRetardOne($newdate,$libellemois,$annee,$classeId,$matricule,$statut,$num,$codeEtab,$libellesession,$retard,$heure,$userad,$observation);

    }
  }
}else {
  echo "coucou";

  for ($i=1;$i<$nbstudent;$i++)
  {

    $retard=0;

    $statut=$_POST["optradio".$tanmatstudent[$i]];
    $matricule=$tanmatstudent[$i];
    $statutcomment=$_POST["statutcomment".$tanmatstudent[$i]];
    $observation=$_POST["observation".$tanmatstudent[$i]];

    if($statut==0)
    {
      //Absent

      $student->AddAttendanceAbsentNew($newdate,$libellemois,$annee,$classeId,$matricule,$statut,$num,$codeEtab,$libellesession,$retard,$userad,$LibelleHeurePresence,$observation,$matiereid);
    }else if($statut==1)
    {
      //présent
      $student->AddAttendanceAbsentNew($newdate,$libellemois,$annee,$classeId,$matricule,$statut,$num,$codeEtab,$libellesession,$retard,$userad,$LibelleHeurePresence,$observation,$matiereid);

    }else if($statut==2)
    {
      //retard
       $retard=1;

       $heure=$_POST["heure".$tanmatstudent[$i]];
       $student->AddAttendanceAbsentRetardOneplus($newdate,$libellemois,$annee,$classeId,$matricule,$statut,$num,$codeEtab,$libellesession,$retard,$heure,$userad,$observation,$LibelleHeurePresence,$matiereid);

    }
  }

}




$_SESSION['user']['addattendailyok']=L::PresenceMessageSuccess;

$libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

 if($_SESSION['user']['profile'] == "Admin_globale")

{

header("Location:../manager/dailyattendances.php?codeEtab=".$codeEtab);

}else if($_SESSION['user']['profile'] == "Admin_locale")

 {
   $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
   $typeetab=$etabs->DetermineTypeEtab($codeEtab);

   if($typeetab==5)
   {
     header("Location:../locale".$libelleEtab."/dailyattendances.php");
   }else {
      header("Location:../locale/dailyattendances.php");
   }




 }else if($_SESSION['user']['profile'] == "Teatcher")

 {

   $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
          $typeetab=$etabs->DetermineTypeEtab($codeEtab);
          $IBSAschool=$etabs->getIbsaschools();
          $codeetabs="";
          foreach ($IBSAschool as  $value):
            $codeetabs=$codeetabs.$value->code_etab."*";
          endforeach;
          $tabetabs=explode("*",$codeetabs);

          $nbdispenser=0;

          $dispenserdatas=$etabs->getdispenserTeatcher($_SESSION['user']['IdCompte']);

          foreach ($dispenserdatas as $value):
            $code=$value->codeEtab;
            if (in_array($code, $tabetabs)) {
              $nbdispenser++;
             }
          endforeach;

          echo $nbdispenser;

            header("Location:../teatcher".$libelleEtab."/dailyattendances.php?classe=".$classeId."&codeEtab=".$codeEtab);

          // if($nbdispenser>0)
          // {
          //   header("Location:../teatcher".$libelleEtab."/dailyattendances.php?classe=".$classeId."&codeEtab=".$codeEtab);
          // }else {
          //   header("Location:../teatcher/dailyattendances.php?classe=".$classeId."&codeEtab=".$codeEtab);
          // }



//insertion dans la tables présences

}


}

?>
