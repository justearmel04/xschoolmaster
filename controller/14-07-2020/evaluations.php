<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../controller/functions.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$classe = new Classe();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //ajout d'un controle


$evaltype=htmlspecialchars($_POST['evaltype']);
$libellesession=htmlspecialchars($_POST['libellesession']);
$typesess=htmlspecialchars($_POST['typesess']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$controle=htmlspecialchars($_POST['controle']);
$classes="";
$typeEvaluation="";

if($evaltype==2)
{
  $datedeb=dateFormat(htmlspecialchars($_POST['datedeb']));
  $datefin=dateFormat(htmlspecialchars($_POST['datefin']));
  $typeEvaluation="EXAMENS";

  foreach($_POST['classeselect'] as $valeur)
 {
   $classes=$classes.$valeur.'-';

 }



//nous allons ajouter l'examen et l'evaluation

//nous allons verifier si cet exmaen n'exsite pas deja

$nbexiste=$etab->AddEvaluationExamenNb($controle,$typeEvaluation,$codeEtab,$libellesession,$typesess,$classes,$datedeb,$datefin);

if($nbexiste==0)
{
$etab->AddEvaluationExamen($controle,$typeEvaluation,$codeEtab,$libellesession,$typesess,$classes,$datedeb,$datefin);
$_SESSION['user']['addctrleok']=L::AddEvaluationOk;
}else {
  // code...
}





}else {
  $classe=htmlspecialchars($_POST['classe']);
  $matiere=htmlspecialchars($_POST['matiere']);
  $teatcher=htmlspecialchars($_POST['teatcher']);
  $coef=htmlspecialchars($_POST['coef']);
  $datectrl=dateFormat(htmlspecialchars($_POST['datectrl']));
  if($evaltype==1)
  {
    $typeEvaluation="CONTROLES";

    $nbexiste=$etab->AddEvaluationscontrolesNb($controle,$datectrl,$classe,$matiere,$teatcher,$coef,$codeEtab,$libellesession,$typesess,$typeEvaluation);
    if($nbexiste==0)
    {
        $etab->AddEvaluationscontroles($controle,$datectrl,$classe,$matiere,$teatcher,$coef,$codeEtab,$libellesession,$typesess,$typeEvaluation);
        $_SESSION['user']['addctrleok']=L::AddEvaluationOk;
    }else {
      // code...
    }

  }else {
    $typeEvaluation="DEVOIRS";

   $etab->AddEvaluationsdevoirs($controle,$datectrl,$classe,$matiere,$teatcher,$coef,$codeEtab,$libellesession,$typesess,$typeEvaluation);

  }


}

  //insertion dans la table controle

// $etab->AddControleClasseSchool($controle,$datectrl,$classe,$matiere,$teatcher,$coef,$codeEtab,$libellesession,$typesess);

if($_SESSION['user']['profile'] == "Admin_globale") {

      //header("Location:../manager/index.php");
      header("Location:../manager/controles.php?codeEtab=".$codeEtab);

  }else if($_SESSION['user']['profile'] == "Admin_locale") {
    // if($_SESSION['user']['paysid']==4)
    // {
    //   header("Location:../localecmr/controles.php");
    // }else {
    //   header("Location:../locale/controles.php");
    // }

    $etablissementType=$etab->DetermineTypeEtab($codeEtab);
    $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);

    if($etablissementType==5)
    {
      header("Location:../locale".$libelleEtab."/evaluations.php");
    }else {
      header("Location:../locale/evaluations.php");
    }


    }else if($_SESSION['user']['profile'] == "Teatcher") {

      header("Location:../teatcher/controles.php");

      }




}
?>
