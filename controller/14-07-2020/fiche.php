<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Matiere.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$classe = new Classe();
$etab=new Etab();
$matiere=new Matiere();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //arecuperation des variables

  $classeEtab=htmlspecialchars(addslashes($_POST['classeEtab']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $matclasse=htmlspecialchars(addslashes($_POST['matclasse']));
  $details=trim(htmlspecialchars(addslashes($_POST['descri'])));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
  $tabmat=explode('-',$matclasse);
  $matiereid=$tabmat[0];
  $teatcherid=$tabmat[1];
  $datecrea=date("Y-m-d");
  //retrouver le libelle de la matieres
  $libellemat=$matiere->getMatiereLibelleByIdMat($matiereid,$codeEtab);
  //nous allons retrouver le libelle de la classeEtab
  $libelleclasse=$classe->getInfosofclassesbyId($classeEtab,$libellesession);

  $fileError = $_FILES['fichier']['error'];

  if($fileError==0)
  {
    //avec fichier
    //traitemement du support

    $file_name = @$_FILES['fichier']['name'];

    $_SESSION["fichier"] = $file_name;

    $file_size =@$_FILES['fichier']['size'];

    $file_tmp =@$_FILES['fichier']['tmp_name'];

    $file_type=@$_FILES['fichier']['type'];

    @$file_ext=strtolower(end(explode('.',@$_FILES['fichier']['name'])));

    $fichierTemp = uniqid() . "." . $file_ext;
    $transactionId =  "FICHE".date("Y").strtoupper(substr($libellemat, 0, 4)).$libelleclasse;
    //$fichierad=$transactionId.".".$file_ext;
    move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

    //insertion dans la table fiche

    $idfiche=$etab->AddLectureFiche($datecrea,$matiereid,$classeEtab,$teatcherid,$codeEtab,$libellesession);

    $fichierad=$transactionId.$idfiche.".".$file_ext;

    //nous allons deplacer le fichier dans le dossier correspondant

    $dossier="../fiches/";

    $dossier1="../fiches/".date("Y");
    $dossier2="../fiches/".date("Y")."/".$libelleclasse;

    if(!is_dir($dossier)) {

          //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation

          @mkdir($dossier);

          @mkdir($dossier1);
          @mkdir($dossier2);


              }else
              {
                 @mkdir($dossier1);
                   @mkdir($dossier2);
              }
              @rename('../temp/' . $fichierTemp , "../fiches/".date("Y")."/".$libelleclasse."/".$fichierad);

               @unlink("../temp/" . $fichierTemp);
    //mise a jour de la table fiche

    $etab->UpdateLectureFicheFileName($fichierad,$idfiche,$teatcherid,$codeEtab,$matiereid,$classeEtab);

    //nous allons ajouter les elements constituant la sous fiche

    $tabdetails=explode(';',$details);
    $nbdetails=count($tabdetails);
    $nb="";
    if($nbdetails==1)
    {
      $nb=1;
    }else if($nbdetails>1)
    {
      $details=substr($details, 0, -1);
      $tabdetails=explode(';',$details);
      $nb=count($tabdetails);

    }

    //insertion dans la table soufiche
    for($i=0;$i<$nb;$i++)
    {
      $libellesous=$tabdetails[$i];
      $etab->AddSousFiche($idfiche,trim(utf8_decode($tabdetails[$i])));

    }

    //redirection

    // $_SESSION['user']['addprogra']="Nouvelle Fiche de Lecture ajouté avec succès";
    $_SESSION['user']['addprogra']=L::NewficheAddMessageSuccess;
    if($_SESSION['user']['profile'] == "Admin_globale") {

          //header("Location:../manager/index.php");
          header("Location:../manager/fiches.php");

      }else if($_SESSION['user']['profile'] == "Admin_locale") {
        if($_SESSION['user']['paysid']==4)
        {
          header("Location:../locale/fiches.php");
        }else {
          header("Location:../locale/fiches.php");
        }


      }else if($_SESSION['user']['profile'] == "Teatcher") {

          header("Location:../teatcher/fiches.php");

          }




  }


  //fichier=htmlspecialchars(addslashes($_POST['classe']));

  // $classe=htmlspecialchars(addslashes($_POST['classe']));
  // $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  // $teatcher=htmlspecialchars(addslashes($_POST['teatcher']));
  // $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  // $controle=htmlspecialchars(addslashes($_POST['controle']));
  // $coef=htmlspecialchars(addslashes($_POST['coef']));
  // $datectrl=htmlspecialchars(addslashes($_POST['datectrl']));







}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recuperation des variables

  // $classeEtab: 1
  // $matclasse: 7
  // $desi1: Leçon1:Gestion des Etablisements
  // $fichier: Avis-de-recrutement-MZK.pdf
  // $oldfichier: FICHE2019MATHCM11.pdf
  // $ficheid: 1
  // $years: 2019
  // $classelibelle: CM1
  // $etape: 2
  // $codeEtab: C00123

  $classeEtab=htmlspecialchars(addslashes($_POST['classeEtab']));
  $matclasse=htmlspecialchars(addslashes($_POST['matclasse']));
  // $fichier=htmlspecialchars(addslashes($_POST['classe']));
  $oldfichier=htmlspecialchars(addslashes($_POST['oldfichier']));
  $ficheid=htmlspecialchars(addslashes($_POST['ficheid']));
  $years=htmlspecialchars(addslashes($_POST['years']));
  $classelibelle=htmlspecialchars(addslashes($_POST['classelibelle']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));

  $taboldfic=explode(".",$oldfichier);
  $ficName=$taboldfic[0];

  $fileError = $_FILES['fichier']['error'];

  if($fileError==0)
  {

    $file_name = @$_FILES['fichier']['name'];

    $_SESSION["fichier"] = $file_name;

    $file_size =@$_FILES['fichier']['size'];

    $file_tmp =@$_FILES['fichier']['tmp_name'];

    $file_type=@$_FILES['fichier']['type'];

    @$file_ext=strtolower(end(explode('.',@$_FILES['fichier']['name'])));

    $fichierTemp = uniqid() . "." . $file_ext;

    $fichierad=$ficName.".".$file_ext;

    move_uploaded_file($file_tmp,"../temp/".$fichierTemp);
    @unlink("../fiches/".$years."/".$classelibelle."/".$oldfichier);

    @rename('../temp/' . $fichierTemp , "../fiches/".$years."/".$classelibelle."/".$fichierad);

     @unlink("../temp/" . $fichierTemp);

     $etab->UpdateFicheLectureWithFile($fichierad,$ficheid,$codeEtab,$classeEtab,$matclasse);

     $_SESSION['user']['addprogra']="Fiche de Lecture modifié avec succès";
     if($_SESSION['user']['profile'] == "Admin_globale") {

           //header("Location:../manager/index.php");
           header("Location:../manager/fiches.php");

       }else if($_SESSION['user']['profile'] == "Admin_locale") {
           if($_SESSION['user']['paysid']==4)
           {
             header("Location:../localecmr/fiches.php");
           }else {
             header("Location:../locale/fiches.php");
           }


       }else if($_SESSION['user']['profile'] == "Teatcher") {

           header("Location:../teatcher/fiches.php");

           }

  }else {
    // nous n'avons aucune modification a faire donc redirection vers fiches.php
  }



}



if(isset($_GET['etape'])&&($_GET['etape']==3))
{
  //supprimer une sous fiche dont id est le suivant
  $sousficheid=htmlspecialchars(addslashes($_GET['programme']));
  $ficheid=htmlspecialchars(addslashes($_GET['ficheid']));

   $etab->DeletedDousFichesById($sousficheid,$ficheid);

   $_SESSION['user']['addprogra']="Element supprimer avec succès";
   if($_SESSION['user']['profile'] == "Admin_globale") {

         //header("Location:../manager/index.php");
         header("Location:../manager/updatefiche.php?programme=".$ficheid);

     }else if($_SESSION['user']['profile'] == "Admin_locale") {
       if($_SESSION['user']['paysid']==4)
       {
         header("Location:../localecmr/updatefiche.php?programme=".$ficheid);
       }else {
         header("Location:../locale/updatefiche.php?programme=".$ficheid);
       }


     }else if($_SESSION['user']['profile'] == "Teatcher") {

         header("Location:../teatcher/updatefiche.php?programme=".$ficheid);

         }


}

?>
