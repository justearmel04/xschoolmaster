<?php
session_start();
require_once('../class/Sessionsacade.php');
require_once('../class/Etablissement.php');
require_once('../controller/functions.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$session= new Sessionacade();
$etabs=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //ajout du diplôme

  //recupération des variables
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $libellesession=htmlspecialchars(addslashes($_POST['titre']));
  $semestre=htmlspecialchars(addslashes($_POST['semestre']));
  $datedeb=htmlspecialchars(addslashes(dateFormat($_POST['datedeb'])));
  $datefin=htmlspecialchars(addslashes(dateFormat($_POST['datefin'])));
  $statut=0;
  $encours=1;

  $idsession=$session->AddSessionSchool($libellesession,$codeEtab,$statut,$encours,$semestre,$datedeb,$datefin);
  $tabsemesterid=array();

  //insertion dans la table semestre

  if($semestre==2)
  {
    $suivant=1;
    $down=-1;
    //semestre
    //creation des semestres
    for($i=1;$i<=$semestre;$i++)
    {
      $libellesemestre="SEMESTRE ".$i;
      $statut=0;
      //insertion dans la tables semestre
      $idsemestreadd=$session->AddSemestreFirst($libellesemestre,$idsession,$statut,$codeEtab);
      $suivant=$idsemestreadd+1;
      $tabsemesterid[$i]=$idsemestreadd;

      if($i==2)
      {
        $session->UpdateNextSemestreFinal($idsemestreadd,$idsession,$codeEtab,$down);
      }else {
        $session->UpdateNextSemestre($idsemestreadd,$idsession,$codeEtab,$suivant);
      }



    }

  }else if($semestre==3)
  {
    //trimestre
    $suivant=1;
    $down=-1;
    for($i=1;$i<=$semestre;$i++)
    {
      $libellesemestre="TRIMESTRE ".$i;
      $statut=0;
      //insertion dans la tables semestre
      $idsemestreadd=$session->AddSemestreFirst($libellesemestre,$idsession,$statut,$codeEtab);
      $suivant=$idsemestreadd+1;
      $tabsemesterid[$i]=$idsemestreadd;

      if($i==3)
      {
        $session->UpdateNextSemestreFinal($idsemestreadd,$idsession,$codeEtab,$down);
      }else {
        $session->UpdateNextSemestre($idsemestreadd,$idsession,$codeEtab,$suivant);
      }
    }
  }

  //var_dump($tabsemesterid);

  //nous allons mettre le statut du premier semestre à 1

  $firstsemesterid=$tabsemesterid[1];

  $session->UpdateCurrentlySemester($firstsemesterid,$encours,$libellesession,$codeEtab,$idsession);


  // $_SESSION['user']['Updateadminok']="Nouvelle Session ajouté avec succès";
  $_SESSION['user']['Updateadminok']=L::SessionAddMessageSuccess;
  if($_SESSION['user']['profile'] == "Admin_globale") {
  header("Location:../manager/sessions.php=");
   // header("Location:../manager/addclasses.php");
     }else if($_SESSION['user']['profile'] == "Admin_locale") {
       //nous allons voir si nous sommes au cameroun
       if($_SESSION['user']['paysid']==4)
       {
         header("Location:../localecmr/sessions.php");
       }else {
         header("Location:../locale/sessions.php");
       }

  }
}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
//insert dans la table session cas etablissment primaire et maternelle

$titre=htmlspecialchars(addslashes($_POST['titre']));
$typesession=htmlspecialchars(addslashes($_POST['typesession']));
$codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
$datedeb=htmlspecialchars(addslashes(dateFormat($_POST['datedeb'])));
$datefin=htmlspecialchars(addslashes(dateFormat($_POST['datefin'])));
$statut=0;
$encours=1;
//ajouter dans la table session

$session->AddsessionPrimary($titre,$typesession,$codeEtab,$datedeb,$datefin,$statut,$encours);

$_SESSION['user']['Updateadminok']="Nouvelle Session ajouté avec succès";
if($_SESSION['user']['profile'] == "Admin_globale") {
header("Location:../manager/sessions.php=");
 // header("Location:../manager/addclasses.php");
   }else if($_SESSION['user']['profile'] == "Admin_locale") {
     if($_SESSION['user']['paysid']==4)
     {
       header("Location:../localecmr/sessions.php");
     }else {
       header("Location:../locale/sessions.php");
     }

}

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //il est question de la cloture de la session pour un etablissement primaire ou maternelle

  //recuperation des variables

  $sessionid=htmlspecialchars(addslashes($_POST['sessionid']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $statut=1;
  $encours=0;

  $session->Updatesessionprimary($sessionid,$codeEtab,$statut,$encours);

    $_SESSION['user']['Updateadminok']="session clôturer avec succès";

}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //ajouter une session pour le cameroun
}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  //ajouter une session pour les etablissements mixtes

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $libellesession=htmlspecialchars(addslashes($_POST['titre']));
  $semestre=htmlspecialchars(addslashes($_POST['semestre']));
  $datedeb=htmlspecialchars(addslashes(dateFormat($_POST['datedeb'])));
  $datefin=htmlspecialchars(addslashes(dateFormat($_POST['datefin'])));
  $statut=0;
  $encours=1;

  $idsession=$session->AddSessionSchool($libellesession,$codeEtab,$statut,$encours,$semestre,$datedeb,$datefin);
  $tabsemesterid=array();

  //insertion dans la table semestre

  if($semestre==2)
  {
    $suivant=1;
    $down=-1;
    //semestre
    //creation des semestres
    for($i=1;$i<=$semestre;$i++)
    {
      $libellesemestre="SEMESTRE ".$i;
      $statut=0;
      //insertion dans la tables semestre
      $idsemestreadd=$session->AddSemestreFirst($libellesemestre,$idsession,$statut,$codeEtab);
      $suivant=$idsemestreadd+1;
      $tabsemesterid[$i]=$idsemestreadd;

      if($i==2)
      {
        $session->UpdateNextSemestreFinal($idsemestreadd,$idsession,$codeEtab,$down);
      }else {
        $session->UpdateNextSemestre($idsemestreadd,$idsession,$codeEtab,$suivant);
      }



    }

  }else if($semestre==3)
  {
    //trimestre
    $suivant=1;
    $down=-1;
    for($i=1;$i<=$semestre;$i++)
    {
      $libellesemestre="TRIMESTRE ".$i;
      $statut=0;
      //insertion dans la tables semestre
      $idsemestreadd=$session->AddSemestreFirst($libellesemestre,$idsession,$statut,$codeEtab);
      $suivant=$idsemestreadd+1;
      $tabsemesterid[$i]=$idsemestreadd;

      if($i==3)
      {
        $session->UpdateNextSemestreFinal($idsemestreadd,$idsession,$codeEtab,$down);
      }else {
        $session->UpdateNextSemestre($idsemestreadd,$idsession,$codeEtab,$suivant);
      }
    }
  }

  $firstsemesterid=$tabsemesterid[1];

  $session->UpdateCurrentlySemester($firstsemesterid,$encours,$libellesession,$codeEtab,$idsession);

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

  // $_SESSION['user']['Updateadminok']="Une nouvelle année scolaire a bien été ajoutée avec succès";
  $_SESSION['user']['Updateadminok']=L::SessionAddMessageSuccess;
  if($_SESSION['user']['profile'] == "Admin_globale") {
  header("Location:../manager/sessions.php=");
   // header("Location:../manager/addclasses.php");
     }else if($_SESSION['user']['profile'] == "Admin_locale") {
       //nous allons voir si nous sommes au cameroun
       if($_SESSION['user']['paysid']==4)
       {
         header("Location:../localecmr/sessions.php");
       }else {
         header("Location:../locale".$libelleEtab."/sessions.php");
       }

  }


}

if(isset($_GET['etape'])&&($_GET['etape']==2))
{
  $sessionid=htmlspecialchars(addslashes($_GET['sessionid']));
  $typesess=htmlspecialchars(addslashes($_GET['typesess']));
  $suivant=htmlspecialchars(addslashes($_GET['suivantid']));
  $statutsuivant=1;
  $status=2;

  //nous allons changer le statut de semestre en 2

  //nous allons determiner le semestre suivant

  $session->UpdateSessionType($sessionid,$typesess,$status,$suivant,$statutsuivant);

  $_SESSION['user']['Updateadminok']="Type session Clôturer avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {
  header("Location:../manager/sessions.php");

     }else if($_SESSION['user']['profile'] == "Admin_locale") {

       //nous allons voir si nous sommes au cameroun
       if($_SESSION['user']['paysid']==4)
       {
         header("Location:../localecmr/sessions.php");
       }else {
         header("Location:../locale/sessions.php");
       }
  }
}

?>
