<?php
session_start();
require_once('../class/User.php');
require_once('../class/LocalAdmin.php');
require_once('../controller/functions.php');
require_once('../class/Etablissement.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$admin = new Localadmin();
$etabs=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //ajout d'un admin local

  //recupération des variables

  $nomad=htmlspecialchars(addslashes($_POST['nomad']));
  $prenomad=htmlspecialchars(addslashes($_POST['prenomad']));
  $datenaisad1=htmlspecialchars(addslashes($_POST['datenaisad']));
  $contactad=htmlspecialchars(addslashes($_POST['contactad']));
  $emailad=htmlspecialchars(addslashes($_POST['emailad']));
  $loginad=htmlspecialchars(addslashes($_POST['loginad']));
  $passad=htmlspecialchars(addslashes($_POST['passad']));
  $fonctionad=htmlspecialchars(addslashes($_POST['fonctionad']));
  $codeEtab=htmlspecialchars(addslashes($_POST['libetab']));

  $datecrea=date("Y-m-d");
  $type_cpte="Admin_locale";
  $statut=1;

  $datenaisad=dateFormat($datenaisad1);

  // echo $codeEtab;
  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
  $etablissementType=$etabs->DetermineTypeEtab($codeEtab);

  //traitement de l'images

  $fileError = $_FILES['photoad']['error'];

  if($fileError==0)
 {
            $file_name = @$_FILES['photoad']['name'];

            $_SESSION["photoad"] = $file_name;

            $file_size =@$_FILES['photoad']['size'];

            $file_tmp =@$_FILES['photoad']['tmp_name'];

            $file_type=@$_FILES['photoad']['type'];

            @$file_ext=strtolower(end(explode('.',@$_FILES['photoad']['name'])));

            $fichierTemp = uniqid() . "." . $file_ext;

            $transactionId =  "ADL".date("Y") . date("m").strtoupper(substr($emailad, 0, 2));

             $fichierad=$transactionId.".".$file_ext;

             move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

             $dossier="../photo/";

             $dossier1="../photo/".$emailad;

             if(!is_dir($dossier)) {

                   //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation

                   @mkdir($dossier);

                   @mkdir($dossier1);


                       }else
                       {
                          @mkdir($dossier1);
                       }
                       @rename('../temp/' . $fichierTemp , "../photo/" . $emailad ."/".$fichierad);

                       //Suppression du fichier se trouvant dans le dossier temp

                @unlink("../temp/" . $fichierTemp);

                //insertion dans la base de données

    $admin->Addlocalwithfile($nomad,$prenomad,$datenaisad,$contactad,$emailad,$fonctionad,$loginad,$passad,$type_cpte,$statut,$datecrea,$fichierad,$codeEtab);
 }else {
   //nous enregistrons le adminlocal sans photo
    $admin->Addlocalwithoutfile($nomad,$prenomad,$datenaisad,$contactad,$emailad,$fonctionad,$loginad,$passad,$type_cpte,$statut,$datecrea,$codeEtab);

 }




 // $_SESSION['user']['addlocalok']="Une nouveau Admin Locale a été ajouté avec succès";
 $_SESSION['user']['addlocalok']=L::AdminLocalAddMessageSuccess;

 if($_SESSION['user']['profile'] == "Admin_globale") {

 header("Location:../manager/addlocal.php");
   }else if($_SESSION['user']['profile'] == "Admin_locale") {
     if($_SESSION['user']['paysid']==4)
     {
       header("Location:../localecmr/addlocaladmin.php");
     }else {
       if($etablissementType==5)
       {
         header("Location:../locale".$libelleEtab."/addlocaladmin.php");
       }else {
         header("Location:../locale/addlocaladmin.php");
       }

     }


     }



}if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recuperation des variables

  $nomad=htmlspecialchars(addslashes($_POST['nomad']));
  $prenomad=htmlspecialchars(addslashes($_POST['prenomad']));
  $datenaisad1=htmlspecialchars(addslashes($_POST['datenaisad']));
  $contactad=htmlspecialchars(addslashes($_POST['contactad']));
  $emailad=htmlspecialchars(addslashes($_POST['emailad']));
  $loginad=htmlspecialchars(addslashes($_POST['loginad']));
  //$passad=htmlspecialchars(addslashes($_POST['passad']));
  $fonctionad=htmlspecialchars(addslashes($_POST['fonctionad']));
  $datenaisad=dateFormat($datenaisad1);
  $codeetab=htmlspecialchars(addslashes($_POST['libetab']));

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
  $etablissementType=$etabs->DetermineTypeEtab($codeEtab);

  $idadLocal=$admin->getIdlocal($emailad);
  //$oldcodeetab=htmlspecialchars(addslashes($_POST['oldcodeetab']));
  /*
  $admin->AssignerEtab($codeetab,$idadLocal);*/

  $fileError = $_FILES['photoad']['error'];

  if($fileError==0)
  {
            $file_name = @$_FILES['photoad']['name'];

            $_SESSION["photoad"] = $file_name;

            $file_size =@$_FILES['photoad']['size'];

            $file_tmp =@$_FILES['photoad']['tmp_name'];

            $file_type=@$_FILES['photoad']['type'];

            @$file_ext=strtolower(end(explode('.',@$_FILES['photoad']['name'])));

            $fichierTemp = uniqid() . "." . $file_ext;

            $transactionId =  "ADL".date("Y") . date("m").strtoupper(substr($emailad, 0, 2));

             $fichierad=$transactionId.".".$file_ext;

             move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

             $dossier="../photo/";

             $dossier1="../photo/".$emailad;

             if(!is_dir($dossier)) {

                   //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation

                   @mkdir($dossier);

                   @mkdir($dossier1);


                       }else
                       {
                          @mkdir($dossier1);
                       }
                       @rename('../temp/' . $fichierTemp , "../photo/" . $emailad ."/".$fichierad);

                       //Suppression du fichier se trouvant dans le dossier temp

                @unlink("../temp/" . $fichierTemp);

                //insertion dans la base de données

  $admin->Updatelocalwithfile($nomad,$prenomad,$datenaisad,$contactad,$emailad,$loginad,$fonctionad,$codeetab,$idadLocal,$fichierad,$oldcodeetab);
  }else {
   //nous enregistrons le adminlocal sans photo
   $admin->Updatelocalwithoutfile($nomad,$prenomad,$datenaisad,$contactad,$emailad,$loginad,$fonctionad,$codeetab,$idadLocal,$oldcodeetab);


  }


  // $_SESSION['user']['updatelocalok']="Le compte de cet admin Local a été modifié avec succès";
  $_SESSION['user']['updatelocalok']=L::AdminLocalModMessageSuccess;

  if($_SESSION['user']['profile'] == "Admin_globale") {

header("Location:../manager/localadmins.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/localadmins.php");
      }else {

        if($etablissementType==5)
        {
          header("Location:../locale".$libelleEtab."/localadmins.php");
        }else {
          header("Location:../locale/localadmins.php");
        }
      }


      }

}if(isset($_POST['etape'])&&($_POST['etape']==3))
{

}

if(isset($_GET['etape'])&&($_GET['etape']==3))
{
  //suppression du compte localadmin

  $compte=htmlspecialchars($_GET['compte']);
  $codeEtab=htmlspecialchars($_GET['codeEtab']);

  $admin->suppressionLocalInThisSystem($compte);

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
  $etablissementType=$etabs->DetermineTypeEtab($codeEtab);

  $_SESSION['user']['deletelocalok']=L::AdminLocalDeleteMessageSuccess;
  // $_SESSION['user']['deletelocalok']="Admin Local supprimer avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

  header("Location:../manager/localadmins.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
          header("Location:../localecmr/localadmins.php");
      }else {
        if($etablissementType==5)
        {
          header("Location:../locale".$libelleEtab."/localadmins.php");
        }else {
          header("Location:../locale/localadmins.php");
        }

      }


      }

}

 ?>
