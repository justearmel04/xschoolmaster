<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Matiere.php');
require_once('../class/Fileuploader.php');
require_once('../controller/functions.php');
$classe = new Classe();
$etab=new Etab();
$matiere=new Matiere();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recuperation des variables
$libellecourse=htmlspecialchars($_POST['libellecourse']);
$classeEtab=htmlspecialchars($_POST['classeEtab']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);
$durationcourse=htmlspecialchars($_POST['durationcourse']);
$detailscourse=htmlspecialchars($_POST['detailscourse']);
$concatmatiere=htmlspecialchars($_POST['matclasse']);
$tabmatiere=explode("-",$concatmatiere);
$matiereid=$tabmatiere[0];
$teatcherid=$tabmatiere[1];
$datecourse=dateFormat(htmlspecialchars($_POST['datecourse']));

$libellematiere=$etab->getMatiereLibelleByIdMat($matiereid,$codeEtab);
$libelleclasse=$classe->getInfosofclassesbyId($classeEtab,$sessionEtab);

// insertion dans la table courses

$courseid=$etab->Addcourses($libellecourse,$datecourse,$matiereid,$teatcherid,$classeEtab,$codeEtab,$sessionEtab,$detailscourse,$durationcourse);

//gestion des sections
$concatsection=htmlspecialchars($_POST['concatsection']);
$tabsection=explode("@",$concatsection);
$nbsection=count($tabsection)-1;

if($nbsection>0)
{
  for($i=0;$i<$nbsection;$i++)
  {
    $section=htmlspecialchars($_POST['section_'.$tabsection[$i]]);

    $etab->AddCourseSection($section,$courseid);
  }
}

//gestion des competences

$concatcomp=htmlspecialchars($_POST['concatcomp']);
$tabcomp=explode("@",$concatcomp);
$nbcomp=count($tabcomp)-1;

if($nbcomp>0)
{
  for($i=0;$i<$nbcomp;$i++)
  {
    $competence=htmlspecialchars($_POST['comp_'.$tabcomp[$i]]);

    $etab->AddCourseComp($competence,$courseid);
  }
}

//gestion des exercice

$concattaches=htmlspecialchars($_POST['concattaches']);
$tabtache=explode("@",$concattaches);
$nbtache=count($tabtache)-1;

if($nbtache>0)
{
  for($i=0;$i<$nbtache;$i++)
  {
    $tache=htmlspecialchars($_POST['taches_'.$tabtache[$i]]);

    $etab->AddCourseHomework($tache,$courseid);
  }
}

//gestion du support de cours

$fileError = $_FILES['fichier3']['error'];
if($fileError==0)
{
  //fichier uploader
  $file_name = @$_FILES['fichier3']['name'];

  $_SESSION["fichier3"] = $file_name;

  $file_size =@$_FILES['fichier3']['size'];

  $file_tmp =@$_FILES['fichier3']['tmp_name'];

  $file_type=@$_FILES['fichier3']['type'];

  @$file_ext=strtolower(end(explode('.',@$_FILES['fichier3']['name'])));

  $fichierTemp = uniqid() . "." . $file_ext;

  $transactionId =  "COURSES".date("Y") . date("m").strtoupper(substr($libellematiere, 0, 3)).$courseid;

  $fichierad=$transactionId.".".$file_ext;

  move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

  $dossier="../courses/".$libelleclasse."/";

  $dossier1="../courses/".$libelleclasse."/".$datecourse."/";

  $dossier2="../courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/";

  if(!is_dir($dossier)) {
        //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
        @mkdir($dossier);
        @mkdir($dossier1);
        @mkdir($dossier2);

            }else
            {

       @mkdir($dossier1);
       @mkdir($dossier2);

            }

      @rename('../temp/' . $fichierTemp , "../courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/".$fichierad);

                 //Suppression du fichier se trouvant dans le dossier temp

      @unlink("../temp/" . $fichierTemp);

      //nous allons mettre à jour le support du cours

      $etab->UpdatecoursesFilesupport($fichierad,$courseid);


}else {
  // fichier non uploader
}




 $_SESSION['user']['addclasseok']="Un nouveau cours a été ajouté avec succès";

 if($_SESSION['user']['profile'] == "Teatcher") {

     header("Location:../teatcher/listcourses.php");

     }

}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  $courseid=htmlspecialchars($_POST['courseid']);
  $libellecourse=htmlspecialchars($_POST['libellecourse']);
  $classeEtab=htmlspecialchars($_POST['classeEtab']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $durationcourse=htmlspecialchars($_POST['durationcourse']);
  $detailscourse=htmlspecialchars($_POST['detailscourse']);
  $concatmatiere=htmlspecialchars($_POST['matclasse']);
  $tabmatiere=explode("-",$concatmatiere);
  $matiereid=$tabmatiere[0];
  $teatcherid=$tabmatiere[1];
  $datecourse=dateFormat(htmlspecialchars($_POST['datecourse']));

  $libellematiere=$etab->getMatiereLibelleByIdMat($matiereid,$codeEtab);
  $libelleclasse=$classe->getInfosofclassesbyId($classeEtab,$sessionEtab);

  $fileError = $_FILES['fichier3']['error'];
  if($fileError==0)
  {
    // echo "fichier uploadé";

    $file_name = @$_FILES['fichier3']['name'];

    $_SESSION["fichier3"] = $file_name;

    $file_size =@$_FILES['fichier3']['size'];

    $file_tmp =@$_FILES['fichier3']['tmp_name'];

    $file_type=@$_FILES['fichier3']['type'];

    @$file_ext=strtolower(end(explode('.',@$_FILES['fichier3']['name'])));

    $fichierTemp = uniqid() . "." . $file_ext;

 $etab->UpdateCoursesDetails($libellecourse,$detailscourse,$durationcourse,$classeEtab,$matiereid,$datecourse,$courseid,$teatcherid,$codeEtab,$sessionEtab);

 $oldfilename=$etab->getCoursesFileName($courseid,$teatcherid,$codeEtab,$sessionEtab);

 if(strlen($oldfilename)>0)
 {
@unlink("../courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/".$oldfilename);

$transactionId =  "COURSES".date("Y") . date("m").strtoupper(substr($libellematiere, 0, 3)).$courseid;

$fichierad=$transactionId.".".$file_ext;

   move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

   $dossier="../courses/".$libelleclasse."/";

   $dossier1="../courses/".$libelleclasse."/".$datecourse."/";

   $dossier2="../courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/";

   if(!is_dir($dossier)) {
         //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
         @mkdir($dossier);
         @mkdir($dossier1);
         @mkdir($dossier2);

             }else
             {

        @mkdir($dossier1);
        @mkdir($dossier2);

             }

       @rename('../temp/' . $fichierTemp , "../courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/".$fichierad);

                  //Suppression du fichier se trouvant dans le dossier temp

       @unlink("../temp/" . $fichierTemp);

       //nous allons mettre à jour le support du cours

       $etab->UpdatecoursesFilesupport($fichierad,$courseid);
 }else {
   $transactionId =  "COURSES".date("Y") . date("m").strtoupper(substr($libellematiere, 0, 3)).$courseid;

   $fichierad=$transactionId.".".$file_ext;

   move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

   $dossier="../courses/".$libelleclasse."/";

   $dossier1="../courses/".$libelleclasse."/".$datecourse."/";

   $dossier2="../courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/";

   if(!is_dir($dossier)) {
         //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
         @mkdir($dossier);
         @mkdir($dossier1);
         @mkdir($dossier2);

             }else
             {

        @mkdir($dossier1);
        @mkdir($dossier2);

             }

       @rename('../temp/' . $fichierTemp , "../courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/".$fichierad);

                  //Suppression du fichier se trouvant dans le dossier temp

       @unlink("../temp/" . $fichierTemp);

       //nous allons mettre à jour le support du cours

       $etab->UpdatecoursesFilesupport($fichierad,$courseid);
 }

  }else {
    // echo "fichier non uploadé";

    $etab->UpdateCoursesDetails($libellecourse,$detailscourse,$durationcourse,$classeEtab,$matiereid,$datecourse,$courseid,$teatcherid,$codeEtab,$sessionEtab);

  }

  $_SESSION['user']['addclasseok']="Le cours a été modifié avec succès";

  if($_SESSION['user']['profile'] == "Teatcher") {

      header("Location:../teatcher/listcourses.php");

      }

}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{




  // initialize FileUploader
      $FileUploader = new FileUploader('files', array(
          'uploadDir' => '../temp/',
      ));

      $data = $FileUploader->upload();



      if($data['isSuccess'] && count($data['files']) > 0) {

        $libellecourse=htmlspecialchars($_POST['libellecourse']);
        $classeEtab=htmlspecialchars($_POST['classeEtab']);
        $codeEtab=htmlspecialchars($_POST['codeEtab']);
        $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
        $durationcourse=htmlspecialchars($_POST['durationcourse']);
        $detailscourse=htmlspecialchars($_POST['detailscourse']);
        $concatmatiere=htmlspecialchars($_POST['matclasse']);
        $tabmatiere=explode("-",$concatmatiere);
        $matiereid=$tabmatiere[0];
        $teatcherid=$tabmatiere[1];
        $datecourse=dateFormat(htmlspecialchars($_POST['datecourse']));

        $libellematiere=$etab->getMatiereLibelleByIdMat($matiereid,$codeEtab);
        $libelleclasse=$classe->getInfosofclassesbyId($classeEtab,$sessionEtab);

        // insertion dans la table courses

        $courseid=$etab->Addcourses($libellecourse,$datecourse,$matiereid,$teatcherid,$classeEtab,$codeEtab,$sessionEtab,$detailscourse,$durationcourse);

        //gestion des sections
        $concatsection=htmlspecialchars($_POST['concatsection']);
        $tabsection=explode("@",$concatsection);
        $nbsection=count($tabsection)-1;

        if($nbsection>0)
        {
          for($i=0;$i<$nbsection;$i++)
          {
            $section=htmlspecialchars($_POST['section_'.$tabsection[$i]]);

            $etab->AddCourseSection($section,$courseid);
          }
        }

        //gestion des competences

        $concatcomp=htmlspecialchars($_POST['concatcomp']);
        $tabcomp=explode("@",$concatcomp);
        $nbcomp=count($tabcomp)-1;

        if($nbcomp>0)
        {
          for($i=0;$i<$nbcomp;$i++)
          {
            $competence=htmlspecialchars($_POST['comp_'.$tabcomp[$i]]);

            $etab->AddCourseComp($competence,$courseid);
          }
        }

        //gestion des exercice

        $concattaches=htmlspecialchars($_POST['concattaches']);
        $tabtache=explode("@",$concattaches);
        $nbtache=count($tabtache)-1;

        if($nbtache>0)
        {
          for($i=0;$i<$nbtache;$i++)
          {
            $tache=htmlspecialchars($_POST['taches_'.$tabtache[$i]]);

            $etab->AddCourseHomework($tache,$courseid);
          }
        }



         $transactionId =  "COURSES".date("Y") . date("m").strtoupper(substr($libellematiere, 0, 3)).$courseid;

        $dossier="../courses/".$libelleclasse."/";

        $dossier1="../courses/".$libelleclasse."/".$datecourse."/";

        $dossier2="../courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/";

        if(!is_dir($dossier)) {
              //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
              @mkdir($dossier);
              @mkdir($dossier1);
              @mkdir($dossier2);

                  }else
                  {

             @mkdir($dossier1);
             @mkdir($dossier2);

                  }


        $datas=$data['files'];
        $i=1;
      foreach ($datas as $value) {

        var_dump($value);
        // echo $value['date'];

        $file_ext=$value['extension'];
        $file_tmp=$value['file'];
        $typesupport="COURSES";

        $fichierad=$transactionId.$i.".".$file_ext;

        @rename($file_tmp , "../courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/".$fichierad);

             //insertion dans la base de données

             $etab->Addsupports($courseid,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$classeEtab,$fichierad,$typesupport);


                   //Suppression du fichier se trouvant dans le dossier temp

        @unlink($file_tmp);

        $i++;
      }

      $_SESSION['user']['addclasseok']="Un nouveau cours a été ajouté avec succès";

      if($_SESSION['user']['profile'] == "Teatcher") {

          header("Location:../teatcher/listcourses.php");

          }


     }

     if($data['hasWarnings']) {

       $_SESSION['user']['addclasseok']="Le nouveau cours n'a pas été ajouté avec succès";

       if($_SESSION['user']['profile'] == "Teatcher") {

           header("Location:../teatcher/listcourses.php");

           }
    }





}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{

  

}







 ?>
