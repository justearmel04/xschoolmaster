<?php
session_start();
require_once('../class/Student.php');
require_once('../class/Parent.php');
require_once('../class/Sessionsacade.php');
require_once('../controller/functions.php');
$student = new Student();
$parentx = new ParentX();
$sessionX= new Sessionacade();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //nous sommes dans un cas de matricule manuel

//recuperation des informations de l'eleve

$matri=htmlspecialchars($_POST['matri']);
$nomad=htmlspecialchars($_POST['nomad']);
$prenomad=htmlspecialchars($_POST['prenomad']);
$statuseleve=htmlspecialchars($_POST['statuseleve']);
$doublant=htmlspecialchars($_POST['doublant']);
$classeEtab=htmlspecialchars($_POST['classeEtab']);
$datenaisad=htmlspecialchars($_POST['datenaisad']);
$datenaisad=dateFormat($datenaisad);
$sexe=htmlspecialchars($_POST['sexe']);
$lieunais=htmlspecialchars($_POST['lieunais']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$newStudent=htmlspecialchars($_POST['newStudent']);
$sessionscolaire=htmlspecialchars($_POST['sessionscolaire']);
$allergie=htmlspecialchars($_POST['allergie']);
$condphy=htmlspecialchars($_POST['condphy']);
$datecrea=date("Y-m-d");
$type_cpte="Student";
$statut=1;

$nbsessionOn=$sessionX->getNumberSessionEncoursOn($codeEtab);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$sessionX->getSessionEncours($codeEtab);
  $tabsessionencours=explode("*",$sessionencours);
  $session=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
}

//gestion des parents
$concatoldparent=htmlspecialchars($_POST['concatoldparents']);
$concatparents=htmlspecialchars($_POST['concatparents']);

$tabnewparent=explode("@",$concatparents);
$taboldparent=explode("@",$concatoldparent);
$nbnewparent=count($tabnewparent);
$nbparent=htmlspecialchars($_POST['nbparent']);
$nbparentold=htmlspecialchars($_POST['nboldparent']);

$allparentids="";


for($i=0;$i<$nbparent;$i++)
{
  //insertion dans la table parent et retour de l'id


  $datecrea=date("Y-m-d");
  $type_cpte="Parent";
  $statut=1;
  $nom=htmlspecialchars($_POST['nomparent'.$tabnewparent[$i]]);
  $prenom=htmlspecialchars($_POST['prenomparent'.$tabnewparent[$i]]);
  $phone=htmlspecialchars($_POST['phoneparent'.$tabnewparent[$i]]);
  $sexe=htmlspecialchars($_POST['sexeparent'.$tabnewparent[$i]]);

  //insertion dans la table compte
  $idparentcompte=$parentx->Addparentchap($nom,$prenom,$phone,$datecrea,$type_cpte,$statut,$sexe,$codeEtab);

  $allparentids=$allparentids.$idparentcompte."@";

}

$allparentids=$allparentids.$concatoldparent;

//nous allons voir si les anciens parents sont enregister dans cet etablissement

for($i=0;$i<$nbparentold;$i++)
{

  $parentid=$taboldparent[$i];

  $nombreP=$parentx->getspecificParentSchool($parentid,$codeEtab);

  if($nombreP>0)
  {

  }else {
    //nous allons ajouter ce parent dans cet etablissement

    $parentx->Addoldparent($parentid,$codeEtab);
  }
}


$taballparents=explode("@",$allparentids);
$nball=count($taballparents)-1;

//nous allons voir si nous avons charger le photo de l'eleve

$fileError = $_FILES['photoad']['error'];

if($fileError==0)
{
//photo uploadé

$file_name = @$_FILES['photoad']['name'];

$_SESSION["photoad"] = $file_name;

$file_size =@$_FILES['photoad']['size'];

$file_tmp =@$_FILES['photoad']['tmp_name'];

$file_type=@$_FILES['photoad']['type'];

@$file_ext=strtolower(end(explode('.',@$_FILES['photoad']['name'])));

$fichierTemp = uniqid() . "." . $file_ext;

$transactionId =  "STU".date("Y") . date("m").strtoupper(substr($nomad, 0, 2)).$matri;

$fichierad=$transactionId.".".$file_ext;

move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

$dossier="../photo/Students/";

$dossier1="../photo/Students/".$matri;

if(!is_dir($dossier)) {
      //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
      @mkdir($dossier);
      @mkdir($dossier1);

          }else
          {
     @mkdir($dossier1);
          }
    @rename('../temp/' . $fichierTemp , "../photo/Students/" . $matri."/".$fichierad);

         //Suppression du fichier se trouvant dans le dossier temp

    @unlink("../temp/" . $fichierTemp);

    $studentid=$student->AddStudentFile($matri,$nomad,$prenomad,$classeEtab,$datenaisad,$sexe,$datecrea,$type_cpte,$statut,$lieunais,$sessionscolaire,$codeEtab,$fichierad,$allergie,$condphy,$statuseleve,$doublant);
    //insertion dans la table parenter

    for($i=0;$i<$nball;$i++)
    {
      $parentid=$taballparents[$i];

      $verif=$student->DetermineNumberOfparenter($parentid,$studentid);

      if($verif==0)
      {
        $student->Addparenter($parentid,$studentid);
      }
    }

    $_SESSION['user']['addStudok']="Inscription à bien été pris en compte dans le système avec succès";


    if($_SESSION['user']['profile'] == "Admin_globale")

    {

    header("Location:../manager/admission.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {
      if($_SESSION['user']['paysid']==4)
      {

        header("Location:../localecmr/admission.php");
      }else {
        header("Location:../locale/admission.php");
      }


    }

}else {
  //aucune photo uploadé

  $studentid=$student->AddStudent($matri,$nomad,$prenomad,$classeEtab,$datenaisad,$sexe,$datecrea,$type_cpte,$statut,$lieunais,$sessionscolaire,$codeEtab,$allergie,$condphy,$statuseleve,$doublant);
  for($i=0;$i<$nball;$i++)
  {
    $parentid=$taballparents[$i];

    $verif=$student->DetermineNumberOfparenter($parentid,$studentid);

    if($verif==0)
    {
      $student->Addparenter($parentid,$studentid);
    }
  }

  $_SESSION['user']['addStudok']="Inscription à bien été pris en compte dans le système avec succès";


  if($_SESSION['user']['profile'] == "Admin_globale")

  {

  header("Location:../manager/admission.php");

  }else if($_SESSION['user']['profile'] == "Admin_locale")

  {

    if($_SESSION['user']['paysid']==4)
    {

      header("Location:../localecmr/admission.php");
    }else {
      header("Location:../locale/admission.php");
    }

  }

}


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  // nous sommes dans un cas de matricule automatique
}




?>
