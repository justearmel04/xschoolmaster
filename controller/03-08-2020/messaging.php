<?php
session_start();
require_once('../class/Student.php');
require_once('../class/Etablissement.php');
require_once('../controller/functions.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$student = new Student();
$etabs=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
    //recuperation des variables

    //sauvegarder et envoye ultérieurement

    $emailvalue=htmlspecialchars($_POST['emailvalue']);
    $smsvalue=htmlspecialchars($_POST['smsvalue']);
    $codeEtab=htmlspecialchars($_POST['codeEtab']);
    $libellesession=htmlspecialchars($_POST['libellesession']);
    $commentaire=htmlspecialchars($_POST['commentaire']);
    $objet=htmlspecialchars($_POST['objet']);
    $tabobjet=explode('-',$objet);
    $objetid=$tabobjet[0];
    $libelleobjet=$tabobjet[1];
    $desti=htmlspecialchars($_POST['destinataires']);
    $emailvalue=htmlspecialchars($_POST['emailvalue']);
    $smsvalue=htmlspecialchars($_POST['smsvalue']);
    $datemsg=htmlspecialchars($_POST['titre']);
    $useradd=$_SESSION['user']['IdCompte'];

    $destinataires="Parent-";
    $studentSchool="";
    $classes="";
    $statutNotif=0;
    $paranotif=0;
    $dateday=date("Y-m-d");

    foreach($_POST['classeEtab'] as $valeur)
   {
     $classes=$classes.$valeur.'-';

   }

   // $classes=substr($classes, 0, -1);

   if($desti==1)
   {
     $precis=0;
     //le message à envoyer à tous les parents des classes selectionnées

     if(isset($_POST['otherobjet']))
     {
       $other=htmlspecialchars($_POST['otherobjet']);
       $etabs->AddNotificationToAllparentsclasseselectwithOther($objetid,$commentaire,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$libelleobjet,$useradd);

     }else {
       $etabs->AddNotificationToAllparentsclasseselect($objetid,$commentaire,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$libellesession,$codeEtab,$libelleobjet,$useradd);

     }


   }else if($desti==2)
   {
     //le message envoyer aux parents des enfants selectionner

     $precis=1;

     foreach($_POST['eleves'] as $valeur)
    {
      $studentSchool=$studentSchool.$valeur.'-';

    }

     // $studentSchool=substr($studentSchool, 0, -1);

     if(isset($_POST['otherobjet']))
     {
       $other=htmlspecialchars($_POST['otherobjet']);
       $etabs->AddNotificationToParentStudentSelectedwithOther($objetid,$commentaire,$destinataires,$classes,$precis,$studentSchool,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$libelleobjet,$useradd);


     }else {
       $etabs->AddNotificationToParentStudentSelected($objetid,$commentaire,$destinataires,$classes,$precis,$studentSchool,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$libellesession,$codeEtab,$libelleobjet,$useradd);

     }


   }

   // $_SESSION['user']['updateteaok']="Une notification a été ajouté avec succès";
   $_SESSION['user']['updateteaok']=L::NotificationCreateMessageSuccess ;
   $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
   if($_SESSION['user']['profile'] == "Admin_globale") {

         //header("Location:../manager/index.php");
         header("Location:../manager/allmessages.php");

     }else if($_SESSION['user']['profile'] == "Admin_locale") {
       if($_SESSION['user']['paysid']==4)
       {
         header("Location:../locale/allmessages.php");
       }else {
         $etablissementType=$etabs->DetermineTypeEtab($codeEtab);

         if($etablissementType==5)
         {
             header("Location:../locale".$libelleEtab."/allmessages.php");
         }else {
            header("Location:../locale/allmessages.php");
         }


       }


     }else if($_SESSION['user']['profile'] == "Teatcher") {

         header("Location:../teatcher/allmessages.php");

         }


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
    //enregistrer et envoyer

      //recuperation des variables

      $emailvalue=htmlspecialchars($_POST['emailvalue']);
      $smsvalue=htmlspecialchars($_POST['smsvalue']);
      $codeEtab=htmlspecialchars($_POST['codeEtab']);
      $libellesession=htmlspecialchars($_POST['libellesession']);
      $commentaire=htmlspecialchars($_POST['commentaire']);
      $objet=htmlspecialchars($_POST['objet']);
      $tabobjet=explode('-',$objet);
      $objetid=$tabobjet[0];
      $libelleobjet=$tabobjet[1];
      $desti=htmlspecialchars($_POST['destinataires']);
      $emailvalue=htmlspecialchars($_POST['emailvalue']);
      $smsvalue=htmlspecialchars($_POST['smsvalue']);
      $datemsg=htmlspecialchars($_POST['titre']);
      $useradd=$_SESSION['user']['IdCompte'];

      $destinataires="Parent-";
      $studentSchool="";
      $classes="";
      $statutNotif=0;
      $paranotif=0;
      $dateday=date("Y-m-d");

      foreach($_POST['classeEtab'] as $valeur)
     {
       $classes=$classes.$valeur.'-';

     }

     // $classes=substr($classes, 0, -1);

     if($desti==1)
     {
       $precis=0;
       //le message à envoyer à tous les parents des classes selectionnées

       if(isset($_POST['otherobjet']))
       {
         $other=htmlspecialchars($_POST['otherobjet']);
         $notificationId=$etabs->AddNotificationToAllparentsclasseselectwithOtherSend($objetid,$commentaire,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$libelleobjet,$useradd);

       }else {
         $notificationId=$etabs->AddNotificationToAllparentsclasseselectSend($objetid,$commentaire,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$libellesession,$codeEtab,$libelleobjet,$useradd);

       }


     }else if($desti==2)
     {
       //le message envoyer aux parents des enfants selectionner

       $precis=1;

       foreach($_POST['eleves'] as $valeur)
      {
        $studentSchool=$studentSchool.$valeur.'-';

      }

       // $studentSchool=substr($studentSchool, 0, -1);

       if(isset($_POST['otherobjet']))
       {
         $other=htmlspecialchars($_POST['otherobjet']);
         $notificationId=$etabs->AddNotificationToParentStudentSelectedwithOther($objetid,$commentaire,$destinataires,$classes,$precis,$studentSchool,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$libelleobjet,$useradd);


       }else {
         $notificationId=$etabs->AddNotificationToParentStudentSelectedSend($objetid,$commentaire,$destinataires,$classes,$precis,$studentSchool,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$libellesession,$codeEtab,$libelleobjet,$useradd);

       }


     }

     // $datas=$etabs->getMessagesInfos($notificationId);
     $datas=$etabs->getMessagesInfosOnly($notificationId);

     $tabcontent=explode("*",$datas);

     $smssender=$tabcontent[3];
     $emailsender=$tabcontent[4];
     $codeEtab=$tabcontent[2];
     $destinataires=$tabcontent[0];
     $classes=$tabcontent[1];
     $sessionEtab=$libellesession;
     $precis=$tabcontent[5];
     $eleves=$tabcontent[6];

     $tabdatadestinataires=explode("-",$destinataires);
     $nbtabdestinataires=count($tabdatadestinataires);
     $cptedestinatairesval=$nbtabdestinataires-1;

     $tabclasses=explode("-",$classes);
     $nbtabclasses=count($tabclasses);
     $cpteclassesval=$nbtabclasses-1;


       //determiner l'indicatif de ce pays

       $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

       $destimails="";
       $destiphone="";

       if($precis==0)
       {
       //la liste des parents de toutes les classes selectionnées

       for($i=0;$i<$cpteclassesval;$i++)
       {
         for($j=0;$j<$cptedestinatairesval;$j++)
         {

         if($tabdatadestinataires[$j]=="Parent")
         {
             // echo "Parent de la classe ".$tabclasses[$i]."<br>";
             //rechercher le mail des parents dont un eleve appartient à cette classeEtab

             // $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab,$sessionEtab);

             $dataParents=$etabs->getEmailsOfParentOfStudentInThisClassesParenter($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab,$sessionEtab);


              // var_dump($dataParents);
              $ka=1;
              foreach ($dataParents as $parents):

                $destimails=$destimails.$parents->email_parent."*";
                $destiphone=$destiphone.$indicatifEtab.$parents->tel_parent."*";


                $ka++;
              endforeach;

         }
       }
       }



       }else if($precis==1)
       {
           //listes des parents en fonction des eleves selectionnées

           $tabeleves=explode("-",$eleves);
           $nbtabeleves=count($tabeleves);
           $cpteelevesval=$nbtabeleves-1;

           // echo $classes." ".$eleves;

           $classes=substr($classes, 0, -1);
           $eleves=substr($eleves, 0, -1);

           $classes = str_replace("-", ",",$classes);
           $eleves=str_replace("-", ",",$eleves);

           // $dataParents=$etabs->getEmailsAndPhoneOfParentOfStudentclasses($classes,$eleves,$sessionEtab,$codeEtab);
           $dataParents=$etabs->getEmailsAndPhoneOfParentOfStudentclassesParenter($classes,$eleves,$sessionEtab,$codeEtab);


           // var_dump($dataParents);
           $ka=1;
           foreach ($dataParents as $parents):

             $destimails=$destimails.$parents->email_parent."*";
             $destiphone=$destiphone.$indicatifEtab.$parents->tel_parent."*";


             $ka++;
           endforeach;


       }

       $paraid=$paranotif;
       $scolarities=0;


       header("Location:messaging.php?etape=3&notifid=".$notificationId."&destimails=".$destimails."&destiphones=".$destiphone."&codeEtab=".$codeEtab."&smssender=".$smssender."&emailsender=".$emailsender."&precis=".$precis."&eleves=".$eleves."&paraid=".$paraid."&scolarities=".$scolarities);








}

if(isset($_GET['etape'])&&($_GET['etape']==3))
{

  $notifid=htmlspecialchars($_GET['notifid']);
  $destimails=htmlspecialchars($_GET['destimails']);
  $destiphones=htmlspecialchars($_GET['destiphones']);
  $codeEtab=htmlspecialchars($_GET['codeEtab']);
  $smssender=htmlspecialchars($_GET['smssender']);
  $emailsender=htmlspecialchars($_GET['emailsender']);
  $precis=htmlspecialchars($_GET['precis']);
  $eleves=htmlspecialchars($_GET['eleves']);
  $paraid=htmlspecialchars($_GET['paraid']);
  $scolarities=htmlspecialchars($_GET['scolarities']);
  $notificationsStatus=1;
  $parascolaireStatus=2;


  //recuperer les informations du message(titre & contenu)
  $destimails=substr($destimails, 0, -1);
  $destiphones=substr($destiphones, 0, -1);

  //nous allons recuperer le nom de l'etablissement et le logo

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
  $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);

  //nous allons faire une verification pour savoir si c'est une activité parascolaire ou pas

  if($paraid==0)
  {
    echo "n'est pas une activité parascolaire<br>";

    //nous allons verifier si il est question d'une reglement de scolarité

    if($scolarities>0)
    {
      //il est question de la scolarité
    }else
     {
      // il est question d'un simple message

      $datas=$etabs->getMessagesInfosOnly($notifid);
      $tabdatas=explode("*",$datas);
      $objetmsg=$tabdatas[11];
      if($objetmsg==8)
      {
        $titremessage=$tabdatas[12];
      }else {
        $titremessage=$tabdatas[10];
      }
      // $titremessage=$tabdatas[7];
      $contenumessage=$tabdatas[13];

      $etabs->UpdateNotificationStatusActivities($notifid,$notificationsStatus,$codeEtab);
    }

  }else if($paraid>0)
  {
    //changer le statut de notification à 1 puis parascolaire à 2
    echo "est une activité parascolaire<br>";

    //recuperer la date de notification

   //nous allons recuperer le titre et le contenu du message

   $datas=$etabs->getMessagesInfos($notifid);
   $tabdatas=explode("*",$datas);
   $titremessage=$tabdatas[7];
   $contenumessage=$tabdatas[8];

   $etabs->UpdateNotificationStatusAndParascolairesActivities($notifid,$notificationsStatus,$codeEtab,$_GET['paraid'],$parascolaireStatus);

  }


  if($emailsender==1)
  {
    // echo "email ";
    //envoi de mail aux destinataires
    if($smssender==1)
    {
      // echo "sms<br>";
  //envoi de mail plus sms
  $etabs->SendNotifiactionWithoutToDestinataires($destimails,$titremessage,$contenumessage,$libelleEtab,$logoEtab,$codeEtab);




  /*envoi du sms selon clicksend*/

  // $phonenumber=str_replace("*",",",$destiphones);
  // echo $_GET['destiphones'];



  if($_SESSION['user']['paysid']==1)
  {
    $tabphoness=explode("*",$destiphones);
    $nbphoness=count($tabphoness);
    for($i=0;$i<$nbphoness;$i++)
    {
      // $etabs->smssending($contenumessage,$tabphoness[$i]);



       // Orangecismssender($tabphoness[$i],$contenumessage);
    }

  }else {
    // $etabs->clicksendersms($phonenumber,$contenumessage);
    // $etabs->d7networkssmssender($phonenumber,$contenumessage);
  }


    }else {
      // echo "sans sms<br>";
      //mail seulement

  $etabs->SendNotifiactionWithoutToDestinataires($destimails,$titremessage,$contenumessage,$libelleEtab,$logoEtab,$codeEtab);

    }



  }





    // $_SESSION['user']['updateteaok']="La notification a bien été envoyé avec succès";
    $_SESSION['user']['updateteaok']=L::NotificationAddMessageSuccess;

    if($_SESSION['user']['profile'] == "Admin_globale") {


          header("Location:../manager/allmessages.php");

      }else if($_SESSION['user']['profile'] == "Admin_locale") {


        if($_SESSION['user']['paysid']==4)
        {
          header("Location:../localecmr/allmessages.php");
        }else {
        header("Location:../locale/allmessages.php");
        }

        }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/allmessages.php");

            }



}else if(isset($_GET['etape'])&&($_GET['etape']==4))
{
  //recupertaion des variables

  $notifid=htmlspecialchars($_GET['notifid']);
  $paraid=htmlspecialchars($_GET['paraid']);
  $scolaid=htmlspecialchars($_GET['scolaid']);
  $statusNotification=-1;
  $statusparasco=0;
  $statutsolde=4;

  if($paraid>0)
    {
      //echo "bonjour";
  //$etabs->DeleteNotificationParascoActivity($paraid,$notifid,$statusNotification,$statusparasco);
    }

    if($scolaid>0)
      {
        //echo "bonjour";
    //$etabs->DeleteNotificationScolariteActivities($scolaid,$notifid,$statusNotification,$statutsolde);
      }

    if($paraid==0 && $scolaid==0 )
    {
      // echo "selut";
      $etabs->DeleteNotificationActivities($notifid,$statusNotification);

      $_SESSION['user']['updateteaok']="Notification supprimer avec succès";
      if($_SESSION['user']['profile'] == "Admin_globale") {

            //header("Location:../manager/index.php");
            header("Location:../manager/allmessages.php");

        }else if($_SESSION['user']['profile'] == "Admin_locale") {
          if($_SESSION['user']['paysid']==4)
          {
            header("Location:../localecmr/allmessages.php");
          }else {
            header("Location:../locale/allmessages.php");
          }


        }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/allmessages.php");

            }
    }



//   if($paraid>0)
//   {
//     echo "bonjour";
// $etabs->DeleteNotificationParasco($paraid,$notifid,$statusNotification,$statusparasco);
//   }else if($paraid==0)
//   {
//     echo "bonsoir";
//     $etabs->DeleteNotification($notifid,$statusNotification);
//   }

}else if(isset($_GET['etape'])&&($_GET['etape']==5))
{
  //recupertaion des variables

  //statut notification==2 pour archiver
  //statut parascolaire==3 pour archiver

  $notifid=htmlspecialchars($_GET['notifid']);
  $paraid=htmlspecialchars($_GET['paraid']);
  $scolarid=htmlspecialchars($_GET['scolarid']);
  $session=htmlspecialchars($_GET['session']);
  $codeEtab=htmlspecialchars($_GET['codeEtab']);
  $notificationstatus=2;
  $notificationsparastatus=3;
  $notificationsoldesatatus=3;

  if($paraid>0)
  {
    //activité parascolaire

    echo "para";

    $etabs->ArchivedParascolaireActivities($paraid,$notifid,$session,$codeEtab,$notificationstatus,$notificationsparastatus);

    $_SESSION['user']['updateteaok']="Activité parascolaire archivé avec succès";
    if($_SESSION['user']['profile'] == "Admin_locale") {

      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/allmessages.php");
      }else {
        header("Location:../locale/allmessages.php");
      }


      }
  }


  if($scolarid>0)
  {
    //soldenotification
    echo "aurevoir";
  $etabs->ArchivedScolaritesoldeActivities($scolarid,$notifid,$session,$codeEtab,$notificationstatus,$notificationsoldesatatus);

  $_SESSION['user']['updateteaok']="Notification Solde archivé avec succès";
  if($_SESSION['user']['profile'] == "Admin_locale") {

      header("Location:../locale/allmessages.php");

    }

  }

  if($paraid==0 && $scolarid==0)
  {
    //notification simple
    $etabs->ArchivedNotificationActivities($notificationstatus,$notifid,$session,$codeEtab);

    $_SESSION['user']['updateteaok']="Notification archiver avec succès";
    if($_SESSION['user']['profile'] == "Admin_globale") {

          //header("Location:../manager/index.php");
          header("Location:../manager/allmessages.php");

      }else if($_SESSION['user']['profile'] == "Admin_locale") {
        if($_SESSION['user']['paysid']==4)
        {
          header("Location:../localecmr/allmessages.php");
        }else {
          header("Location:../locale/allmessages.php");
        }


      }else if($_SESSION['user']['profile'] == "Teatcher") {

          header("Location:../teatcher/allmessages.php");

          }
  }


}else if(isset($_GET['etape'])&&($_GET['etape']==6))
{

  $notifid=htmlspecialchars($_GET['notifid']);
  $destimails=htmlspecialchars($_GET['destimails']);
  $destiphones=htmlspecialchars($_GET['destiphones']);
  $codeEtab=htmlspecialchars($_GET['codeEtab']);
  $smssender=htmlspecialchars($_GET['smssender']);
  $emailsender=htmlspecialchars($_GET['emailsender']);
  $precis=htmlspecialchars($_GET['precis']);
  $eleves=htmlspecialchars($_GET['eleves']);
  $paraid=htmlspecialchars($_GET['paraid']);
  $scolarities=htmlspecialchars($_GET['scolarities']);
  $notificationsStatus=1;
  $parascolaireStatus=2;


  //recuperer les informations du message(titre & contenu)
  $destimails=substr($destimails, 0, -1);
  $destiphones=substr($destiphones, 0, -1);

  //nous allons recuperer le nom de l'etablissement et le logo

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
  $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);

  //nous allons faire une verification pour savoir si c'est une activité parascolaire ou pas

  if($paraid==0)
  {
    echo "n'est pas une activité parascolaire<br>";

    //nous allons verifier si il est question d'une reglement de scolarité

    if($scolarities>0)
    {
      //il est question de la scolarité
    }else
     {
      // il est question d'un simple message

      $datas=$etabs->getMessagesInfosOnly($notifid);
      $tabdatas=explode("*",$datas);
      $objetmsg=$tabdatas[11];
      if($objetmsg==8)
      {
        $titremessage=$tabdatas[12];
      }else {
        $titremessage=$tabdatas[10];
      }
      // $titremessage=$tabdatas[7];
      $contenumessage=$tabdatas[13];

      $etabs->UpdateNotificationStatusActivities($notifid,$notificationsStatus,$codeEtab);
    }

  }else if($paraid>0)
  {
    //changer le statut de notification à 1 puis parascolaire à 2
    echo "est une activité parascolaire<br>";

    //recuperer la date de notification

   //nous allons recuperer le titre et le contenu du message

   $datas=$etabs->getMessagesInfos($notifid);
   $tabdatas=explode("*",$datas);
   $titremessage=$tabdatas[7];
   $contenumessage=$tabdatas[8];

   $etabs->UpdateNotificationStatusAndParascolairesActivities($notifid,$notificationsStatus,$codeEtab,$_GET['paraid'],$parascolaireStatus);

  }


  if($emailsender==1)
  {
    // echo "email ";
    //envoi de mail aux destinataires
    if($smssender==1)
    {
      // echo "sms<br>";
  //envoi de mail plus sms
  $etabs->SendNotifiactionWithoutToDestinataires($destimails,$titremessage,$contenumessage,$libelleEtab,$logoEtab,$codeEtab);




  /*envoi du sms selon clicksend*/

  // $phonenumber=str_replace("*",",",$destiphones);
  // echo $_GET['destiphones'];



  if($_SESSION['user']['paysid']==1)
  {
    $tabphoness=explode("*",$destiphones);
    $nbphoness=count($tabphoness);
    for($i=0;$i<$nbphoness;$i++)
    {
      // $etabs->smssending($contenumessage,$tabphoness[$i]);



       // Orangecismssender($tabphoness[$i],$contenumessage);
    }

  }else {
    // $etabs->clicksendersms($phonenumber,$contenumessage);
    // $etabs->d7networkssmssender($phonenumber,$contenumessage);
  }


    }else {
      // echo "sans sms<br>";
      //mail seulement

  $etabs->SendNotifiactionWithoutToDestinataires($destimails,$titremessage,$contenumessage,$libelleEtab,$logoEtab,$codeEtab);

    }



  }





    // $_SESSION['user']['updateteaok']="La Notification a bien été envoyé avec succès";
    $_SESSION['user']['updateteaok']=L::NotificationAddMessageSuccess;

    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

    if($_SESSION['user']['profile'] == "Admin_globale") {


          header("Location:../manager/allmessages.php");

      }else if($_SESSION['user']['profile'] == "Admin_locale") {


        if($_SESSION['user']['paysid']==4)
        {
          header("Location:../localecmr/allmessages.php");
        }else {
        header("Location:../locale".$libelleEtab."/allmessages.php");
        }

        }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/allmessages.php");

            }



}else if(isset($_GET['etape'])&&($_GET['etape']==7))
{
  //recupertaion des variables

  $notifid=htmlspecialchars($_GET['notifid']);
  $paraid=htmlspecialchars($_GET['paraid']);
  $scolaid=htmlspecialchars($_GET['scolaid']);
  $statusNotification=-1;
  $statusparasco=0;
  $statutsolde=4;

  if($paraid>0)
    {
      //echo "bonjour";
  //$etabs->DeleteNotificationParascoActivity($paraid,$notifid,$statusNotification,$statusparasco);
    }

    if($scolaid>0)
      {
        //echo "bonjour";
    //$etabs->DeleteNotificationScolariteActivities($scolaid,$notifid,$statusNotification,$statutsolde);
      }

    if($paraid==0 && $scolaid==0 )
    {
      // echo "selut";
      $etabs->DeleteNotificationActivitiesMixte($notifid,$statusNotification);
    }



//   if($paraid>0)
//   {
//     echo "bonjour";
// $etabs->DeleteNotificationParasco($paraid,$notifid,$statusNotification,$statusparasco);
//   }else if($paraid==0)
//   {
//     echo "bonsoir";
//     $etabs->DeleteNotification($notifid,$statusNotification);
//   }

// $_SESSION['user']['updateteaok']="La Notification a bien été supprimée avec succès";
$_SESSION['user']['updateteaok']=L::NotificationDeleteMessageSuccess;

$libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

if($_SESSION['user']['profile'] == "Admin_globale") {

      //header("Location:../manager/index.php");
      header("Location:../manager/allmessages.php");

  }else if($_SESSION['user']['profile'] == "Admin_locale") {
    if($_SESSION['user']['paysid']==4)
    {
      header("Location:../localecmr/allmessages.php");
    }else {
      header("Location:../locale".$libelleEtab."/allmessages.php");
    }


  }else if($_SESSION['user']['profile'] == "Teatcher") {

      header("Location:../teatcher/allmessages.php");

      }


}



 ?>
