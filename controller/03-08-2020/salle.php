<?php
session_start();

require_once('../class/Salle.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$sallex = new Salle();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //ajout du diplôme

  //recupération des variables
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $libellesalle=htmlspecialchars(addslashes($_POST['sallex']));
  $capacitesalle=htmlspecialchars(addslashes($_POST['capacitex']));
  $classe=htmlspecialchars(addslashes($_POST['classe']));

  $sallex->Addsalle($libellesalle,$codeEtab,$capacitesalle,$classe);

  $_SESSION['user']['addsalleok']="Salle ajouté avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

    header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
// header("Location:../manager/addsalle.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/salles.php");
      }else {
        header("Location:../locale/salles.php");
      }


      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }



}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //ajout du diplôme

  //recupération des variables
  $codeEtab=htmlspecialchars(addslashes($_POST['libetab']));
  $libellesalle=htmlspecialchars(addslashes($_POST['sallex']));
  $capacitesalle=htmlspecialchars(addslashes($_POST['capacitex']));
  $idsalle=htmlspecialchars(addslashes($_POST['classeid']));
  //
  $sallex->Updatesalle($libellesalle,$codeEtab,$capacitesalle,$idsalle);

  $_SESSION['user']['addsalleok']="Salle modifié avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

    header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
// header("Location:../manager/addsalle.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/salles.php");
      }else {
        header("Location:../locale/salles.php");
      }


      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }



}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recuperation des variables

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $libellesalle=htmlspecialchars(addslashes($_POST['sallex']));
  $capacitesalle=htmlspecialchars(addslashes($_POST['capacitex']));
  $statutsalle=htmlspecialchars(addslashes($_POST['statutsalle']));

  $sallex->AddsalleLibre($libellesalle,$codeEtab,$capacitesalle);

  $_SESSION['user']['addsalleok']="Salle ajouté avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

    header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
// header("Location:../manager/addsalle.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/salles.php");
      }else {
        header("Location:../locale/salles.php");
      }


      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }


}

if(isset($_GET['etape'])&&($_GET['etape']==3))
{
  //ajout du diplôme

  //recupération des variables

  $idsalle=htmlspecialchars(addslashes($_GET['compte']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));

  //
  $sallex->Deletedsalle($idsalle,$codeEtab);

  $_SESSION['user']['addsalleok']="Salle supprimer avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

    header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
// header("Location:../manager/addsalle.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/salles.php");
      }else {
        header("Location:../locale/salles.php");
      }


      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }



}


?>
