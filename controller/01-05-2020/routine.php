<?php
session_start();

require_once('../class/Etablissement.php');
require_once('../class/Matiere.php');
require_once('../class/Teatcher.php');
require_once('../class/Classe.php');
require_once('../controller/functions.php');
$routine = new Etab();
$subject= new Matiere();
$teatcher=new Teatcher();
$classesT=new Classe();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //ajout de la routine

  //recupération des variables

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $day=htmlspecialchars(addslashes($_POST['day']));
  $heuredeb=htmlspecialchars(addslashes($_POST['heuredeb']));
  $heurefin=htmlspecialchars(addslashes($_POST['heurefin']));
  $Etab=htmlspecialchars(addslashes($_POST['Etab']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

  //ajout de la routine

  $routine->AddRoutine($classe,$Etab,$heuredeb,$heurefin,$matiere,$day,$libellesession);

}if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recupération des variables

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $day=htmlspecialchars(addslashes($_POST['day']));
  $heuredeb=htmlspecialchars(addslashes($_POST['heuredeb']));
  $heurefin=htmlspecialchars(addslashes($_POST['heurefin']));
  $idroutine=htmlspecialchars(addslashes($_POST['idroutine']));
  $Etab=htmlspecialchars(addslashes($_POST['Etab']));
  $jourold=htmlspecialchars(addslashes($_POST['jourold']));

  $debhourold=htmlspecialchars(addslashes($_POST['debhourold']));
  $finhourold=htmlspecialchars(addslashes($_POST['finhourold']));
  $libellejourold=AfficherSemaine($jourold);
  $libelledaymodify=AfficherSemaine($day);
  $libellematieres=$subject->getMatiereLibelleByIdMat($matiere,$Etab);
  $libellession=htmlspecialchars(addslashes($_POST['libellession']));
  $libelleclasse=$classesT->getInfosofclassesbyId($classe,$libellession);
  $parentsMails="";
  $parentsContacts="";
  $tabdatadestinataires="Parent";

  //nous allons recuperer le nom de l'etablissement et le logo

  $libelleEtab=$routine->getEtabLibellebyCodeEtab($Etab);
  $logoEtab=$routine->getEtabLogobyCodeEtab($Etab);





  //modification de la routine

  $routine->UpdateRoutine($classe,$Etab,$heuredeb,$heurefin,$matiere,$day,$idroutine);

  //envoie de notification aux parents et au professeur enseignant cette matiere dans cette classe

  //retrouver le mail du professeur de cette matieres

  $mailteatcher=$teatcher->getterMailOfTeatcherBySubjectidAndClasses($matiere,$classe,$Etab);

  // retrouver les emails des parents des eleves de cette classe

  $dataParents=$routine->getEmailsOfParentOfStudentInThisClasses($classe,$tabdatadestinataires,$Etab,$libellession);
  $indicatifEtab=$routine->getIndictatifOfThisSchool($Etab);

  $ka=1;
  foreach ($dataParents as $parents):

    $parentsMails=$parentsMails.$parents->email_parent."*";
    $parentsContacts=$parentsContacts.$indicatifEtab.$parents->tel_parent."*";


    $ka++;
  endforeach;

    $parentsMails=substr($parentsMails, 0, -1);
    $parentsContacts=substr($parentsContacts, 0, -1);

    //envoi du mail au preofesseur
    $routine->TeatcherRoutineUpdateMailer($mailteatcher,$libelleclasse,$libellematieres,$libellejourold,$libelledaymodify,$debhourold,$heuredeb,$finhourold,$heurefin,$libelleEtab,$logoEtab,$Etab);

    //envoi du mail aux parents

    $routine->ParentsRoutineUpdateMailer($parentsMails,$libelleclasse,$libellematieres,$libellejourold,$libelledaymodify,$debhourold,$heuredeb,$finhourold,$heurefin,$libelleEtab,$logoEtab,$Etab);


    $_SESSION['user']['updateroutineok']=" Routine modifié avec succès";

     // header("Location:../manager/matieres.php?classe=".$classe);
     if($_SESSION['user']['profile'] == "Admin_globale") {

           //header("Location:../manager/index.php");
           header("Location:../manager/routines.php?classe=".$classe);

       }else if($_SESSION['user']['profile'] == "Admin_locale") {
         if($_SESSION['user']['paysid']==4)
         {
           header("Location:../localecmr/routines.php?classe=".$classe);
         }else {
           header("Location:../locale/routines.php?classe=".$classe);
         }


         }

}

if(isset($_GET['etape'])&&($_GET['etape']==3))
{
  //recupération des variables
  $routineid=htmlspecialchars(addslashes($_GET['compte']));
  $classeid=htmlspecialchars(addslashes($_GET['classe']));

  $routine->deleteRoutineById($routineid,$classeid);
  $_SESSION['user']['updateroutineok']=" Routine supprimer avec succès";

   // header("Location:../manager/matieres.php?classe=".$classe);
   if($_SESSION['user']['profile'] == "Admin_globale") {

         //header("Location:../manager/index.php");
         header("Location:../manager/routines.php?classe=".$classe);

     }else if($_SESSION['user']['profile'] == "Admin_locale") {
       if($_SESSION['user']['paysid']==4)
       {
          header("Location:../localecmr/routines.php?classe=".$classe);
       }else {
          header("Location:../locale/routines.php?classe=".$classe);
       }


       }

}else if(isset($_GET['etape'])&&($_GET['etape']==4))
{
  $routineid=htmlspecialchars(addslashes($_GET['compte']));
  $classeid=htmlspecialchars(addslashes($_GET['classe']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));

  $routine->deleteRoutinesById($routineid,$classeid,$codeEtab);
}else if(isset($_GET['etape'])&&($_GET['etape']==5))
{
  //recupération des variables
  $routineid=htmlspecialchars(addslashes($_GET['compte']));
  $classeid=htmlspecialchars(addslashes($_GET['classe']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
  $sessionEtab=htmlspecialchars(addslashes($_GET['sessionEtab']));

  $routine->deleteRoutineById($routineid,$classeid);
  $_SESSION['user']['updateroutineok']="Le cours a été supprimé avec succès";

   // header("Location:../manager/matieres.php?classe=".$classe);
   if($_SESSION['user']['profile'] == "Admin_globale") {

         //header("Location:../manager/index.php");
         header("Location:../manager/routines.php?classe=".$classeid);

     }else if($_SESSION['user']['profile'] == "Admin_locale") {
       if($_SESSION['user']['paysid']==4)
       {
          header("Location:../localecmr/routines.php?classe=".$classeid);
       }else {
           $etablissementType=$routine->DetermineTypeEtab($codeEtab);
           $libelleEtab=$routine->getEtabLibellebyCodeEtab($codeEtab);
           // echo $etablissementType;
           if($etablissementType==5)
           {

  header("Location:../locale".$libelleEtab."/routines.php?classe=".$classeid);
           }else {
             header("Location:../locale/routines.php?classe=".$classeid);
           }

       }


       }

}

?>
