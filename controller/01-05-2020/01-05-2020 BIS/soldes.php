<?php
session_start();
require_once('../class/Student.php');
require_once('../class/Etablissement.php');
$student = new Student();
$etabs=new Etab();
if(isset($_POST['etape'])&&($_POST['etape']==1))
{

  $titre=htmlspecialchars(addslashes($_POST['titre']));
  $destinataires="";
  $classes="";
  $message=htmlspecialchars(addslashes($_POST['message']));
  $statutFile=0;
  $statutNotif=0;
  $dateday=date("Y-m-d");
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $smsvalue=htmlspecialchars(addslashes($_POST['smsvalue']));
  $emailvalue=htmlspecialchars(addslashes($_POST['emailvalue']));
  $paranotif=0;
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
  $scolanotif=1;


  foreach($_POST['destinataires'] as $valeur)
  {
   $destinataires=$destinataires.$valeur.'-';

  }

  foreach($_POST['classeEtab'] as $valeur)
  {
  $classes=$classes.$valeur.'-';

  }

  //echo $titre."/".$destinataires."/".$classes."/".$message."/".$statutFile."/".$statutNotif."/".$dateday."/".$codeEtab."/".$smsvalue."/".$emailvalue."/".$paranotif."/".$libellesession."-".$scolanotif;

  $etabs->AddNotificationScola($titre,$destinataires,$classes,$message,$statutFile,$statutNotif,$dateday,$codeEtab,$smsvalue,$emailvalue,$paranotif,$libellesession,$scolanotif);
}if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //modifier la notication de solde

  $titre=htmlspecialchars(addslashes($_POST['titre']));
  $message=htmlspecialchars(addslashes($_POST['message']));
  //$destinataires[]: Parent
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
  $smsvalue=htmlspecialchars(addslashes($_POST['smsvalue']));
  $emailvalue=htmlspecialchars(addslashes($_POST['emailvalue']));
  $soldeid=htmlspecialchars(addslashes($_POST['soldeid']));
  $notifid=htmlspecialchars(addslashes($_POST['notifid']));
  $classes="";

  if(isset($_POST['classeEtab']))
  {
    //avec modification de la classe

    foreach($_POST['classeEtab'] as $valeur)
    {
     $classes=$classes.$valeur.'-';
    }

    //nous allons modifier soldenotification et notification
    echo "modification avec classes";

    $etabs->UpdateSolderetardNotificationWithClasses($titre,$message,$codeEtab,$libellesession,$smsvalue,$emailvalue,$classes,$soldeid,$notifid);

  }else {
    echo "modification sans classes <br>";
    //echo $titre."-".$soldeid."-".$notifid.'-'.$message;
    $etabs->UpdateSolderetardNotificationWithoutClasses($titre,$message,$codeEtab,$libellesession,$smsvalue,$emailvalue,$soldeid,$notifid);
  }
}

if(isset($_GET['etape'])&&($_GET['etape']==3))
{
  //supprimer soldenotification

  //recuperation des variables

  $notifid=htmlspecialchars(addslashes($_GET['notifid']));
  $soldeid=htmlspecialchars(addslashes($_GET['soldeid']));
  $session=htmlspecialchars(addslashes($_GET['session']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
  $statusNotification=-1;
  $statutsolde=4;


  $etabs->DeleteNotificationScolarite($soldeid,$notifid,$statusNotification,$statutsolde);


}

if(isset($_GET['etape'])&&($_GET['etape']==4))
{
  $notifid=htmlspecialchars(addslashes($_GET['notifid']));
  $destimails=htmlspecialchars(addslashes($_GET['destimails']));
  $destiphones=htmlspecialchars(addslashes($_GET['destiphones']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
  $smssender=htmlspecialchars(addslashes($_GET['smssender']));
  $emailsender=htmlspecialchars(addslashes($_GET['emailsender']));
  $joinfile=htmlspecialchars(addslashes($_GET['joinfile']));
  $file=htmlspecialchars(addslashes($_GET['file']));
  $scolarnotifyid=htmlspecialchars(addslashes($_GET['paraid']));

  $notificationsStatus=1;
  $scolaireStatus=2;

  //recuperer les informations du message(titre & contenu)
  $destimails=substr($destimails, 0, -1);
  $destiphones=substr($destiphones, 0, -1);

  //recuperer la date de notification

  $datanotification=$etabs->getNotificationInfos($notifid);
  $tabnotification=explode("*",$datanotification);
  $datenotif=$tabnotification[9];

  //nous allons recuperer le nom de l'etablissement et le logo

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
  $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);

  $datas=$etabs->getNotificationInfos($notifid);
  $tabdatas=explode("*",$datas);
  $titremessage=$tabdatas[7];
  $contenumessage=$tabdatas[8];

  if($joinfile==0)
  {
  //pas de fichier joint
  if($emailsender==1)
  {
  //envoi de mail aux destinataires
  if($smssender==1)
  {
  //envoi de mail plus sms
  $etabs->SendNotifiactionWithoutToDestinataires($destimails,$titremessage,$contenumessage,$libelleEtab,$logoEtab,$codeEtab);

  }else {
    //mail seulement

  $etabs->SendNotifiactionWithoutToDestinataires($destimails,$titremessage,$contenumessage,$libelleEtab,$logoEtab,$codeEtab);

  }



  }

  }

    //message notification envoyer avec succès

      //changer le statut de la notification à 1

      if($scolarnotifyid==0)
      {
        echo "bonjour";
        //$etabs->UpdateNotificationStatusPara($notifid,$notificationsStatus,$codeEtab);
        $etabs->UpdateNotificationStatusParasclar($notifid,$notificationsStatus,$codeEtab);
      }else if($scolarnotifyid>0)
      {
        //changer le statut de notification à 1 puis parascolaire à 2
        echo "bonsoir";
        $etabs->UpdateNotificationStatusAndParascolairesParascolar($notifid,$notificationsStatus,$codeEtab,$_GET['paraid'],$scolaireStatus);
      }

      // $_SESSION['user']['addctrleok']="Notification Solde/Retard envoyé avec succès";
      //
      // if($_SESSION['user']['profile'] == "Admin_globale") {
      //
      //       //header("Location:../manager/index.php");
      //       header("Location:../manager/soldenotifications.php");
      //
      //   }else if($_SESSION['user']['profile'] == "Admin_locale") {
      //
      //     header("Location:../locale/soldenotifications.php");
      //
      //     }

}
if(isset($_GET['etape'])&&($_GET['etape']==5))
{
    //archivage de notification solde

    $notifid=htmlspecialchars(addslashes($_GET['notifid']));
    $soldeid=htmlspecialchars(addslashes($_GET['soldeid']));
    $session=htmlspecialchars(addslashes($_GET['session']));
    $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
    $notificationstatus=2;
    $notificationsoldesatatus=3;

    $etabs->ArchivedScolaritesoldeScolar($soldeid,$notifid,$session,$codeEtab,$notificationstatus,$notificationsoldesatatus);
}
 ?>
