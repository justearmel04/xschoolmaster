<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../controller/functions.php');
$classe = new Classe();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //ajout d'un controle


  $classe=htmlspecialchars($_POST['classe']);
  $matiere=htmlspecialchars($_POST['matiere']);
  $teatcher=htmlspecialchars($_POST['teatcher']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $controle=htmlspecialchars($_POST['controle']);
  $coef=htmlspecialchars($_POST['coef']);
  $datectrl=htmlspecialchars($_POST['datectrl']);
  $libellesession=htmlspecialchars($_POST['libellesession']);
  $typesess=htmlspecialchars($_POST['typesess']);

  //insertion dans la table controle

$etab->AddControleClasseSchool($controle,$datectrl,$classe,$matiere,$teatcher,$coef,$codeEtab,$libellesession,$typesess);



}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recuperation des variables

  $idctrl=htmlspecialchars($_POST['idctrl']);

  $classe=htmlspecialchars($_POST['classe'.$idctrl]);
  $matiere=htmlspecialchars($_POST['matiere'.$idctrl]);
  $teatcher=htmlspecialchars($_POST['teatcher'.$idctrl]);
  $controle=htmlspecialchars($_POST['controle'.$idctrl]);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $coef=htmlspecialchars($_POST['coef'.$idctrl]);
  $datectrl=dateFormat($_POST['datectrl'.$idctrl]);

  //mise à jour de la table $controle

  $etab->UpdateControleClasseSchool($classe,$matiere,$teatcher,$controle,$codeEtab,$coef,$datectrl,$idctrl);


}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //recuperation des variables

  $classe=htmlspecialchars($_POST['classe']);
  $matiere=htmlspecialchars($_POST['matiere']);
  $teatcher=htmlspecialchars($_POST['teatcher']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $libellesession=htmlspecialchars($_POST['libellesession']);
  $controle=htmlspecialchars($_POST['controle']);
  $coef=htmlspecialchars($_POST['coef']);
  $datectrl=dateFormat($_POST['datectrl']);

  //ajouter dans la table controle

  $etab->AddControleClasseSchoolPrimary($controle,$datectrl,$classe,$matiere,$teatcher,$coef,$codeEtab,$libellesession);



}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{

  //ajout d'un controle


  $classe=htmlspecialchars($_POST['classe']);
  $matiere=htmlspecialchars($_POST['matiere']);
  $teatcher=htmlspecialchars($_POST['teatcher']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $controle=htmlspecialchars($_POST['controle']);
  $coef=htmlspecialchars($_POST['coef']);
  $datectrl=htmlspecialchars($_POST['datectrl']);
  $libellesession=htmlspecialchars($_POST['libellesession']);
  // $typesess=htmlspecialchars(addslashes($_POST['typesess']));

  //insertion dans la table controle

  $etab->AddControleClasseSchoolPrimary($controle,$datectrl,$classe,$matiere,$teatcher,$coef,$codeEtab,$libellesession);





}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  $classe=htmlspecialchars($_POST['classe']);
  $matiere=htmlspecialchars($_POST['matiere']);
  $teatcher=htmlspecialchars($_POST['teatcher']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $controle=htmlspecialchars($_POST['controle']);
  $coef=htmlspecialchars($_POST['coef']);
  $datectrl=dateFormat($_POST['datectrl']);
  $libellesession=htmlspecialchars($_POST['libellesession']);
  $typesess=htmlspecialchars($_POST['typesess']);


  //insertion dans la table controle

$etab->AddControleClasseSchoolMixte($controle,$datectrl,$classe,$matiere,$teatcher,$coef,$codeEtab,$libellesession,$typesess);


$_SESSION['user']['addctrleok']="Un nouveau Contrôle a été ajouté avec succès";

$libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);

 // header("Location:../manager/matieres.php?classe=".$classe);
 if($_SESSION['user']['profile'] == "Admin_globale") {

       //header("Location:../manager/index.php");
       header("Location:../manager/controles.php?codeEtab=".$codeEtab);

   }else if($_SESSION['user']['profile'] == "Admin_locale") {
     if($_SESSION['user']['paysid']==4)
     {
       header("Location:../localecmr/controles.php");
     }else {
       header("Location:../locale".$libelleEtab."/controles.php");
     }


     }else if($_SESSION['user']['profile'] == "Teatcher") {

       header("Location:../teatcher/controles.php");

       }



}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{
  //recuperation des variables

  $idctrl=htmlspecialchars($_POST['idctrl']);

  $classe=htmlspecialchars($_POST['classe'.$idctrl]);
  $matiere=htmlspecialchars($_POST['matiere'.$idctrl]);
  $teatcher=htmlspecialchars($_POST['teatcher'.$idctrl]);
  $controle=htmlspecialchars($_POST['controle'.$idctrl]);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $coef=htmlspecialchars($_POST['coef'.$idctrl]);
  $datectrl=dateFormat($_POST['datectrl'.$idctrl]);

  //mise à jour de la table $controle

  // echo $datectrl;

  $etab->UpdateControleClasseSchoolMixte($classe,$matiere,$teatcher,$controle,$codeEtab,$coef,$datectrl,$idctrl);

  $_SESSION['user']['addctrleok']="Le Contrôle a été modifié avec succès";

  $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);

  if($_SESSION['user']['profile'] == "Admin_globale") {

        //header("Location:../manager/index.php");
        header("Location:../manager/controles.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/controles.php");
      }else {
        header("Location:../locale".$libelleEtab."/controles.php");
      }


    }else if($_SESSION['user']['profile'] == "Teatcher") {

        header("Location:../teatcher/controles.php");

        }
}

if(isset($_GET['etape'])&&($_GET['etape']==3))
{
  //recupération des variables

  $matiere=htmlspecialchars($_GET['matiere']);
  $classe=htmlspecialchars($_GET['classe']);
  $codeEtab=htmlspecialchars($_GET['codeEtab']);
  $controleid=htmlspecialchars($_GET['controleid']);

  //nous allons supprimer le controle

  $etab->deletedControleClassesSchool($controleid,$codeEtab,$classe,$matiere);

}


?>
