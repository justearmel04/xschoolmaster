<?php
session_start();
//require_once('../class/Matiere.php');
require_once('../class/Etablissement.php');
$etab = new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //ajout du diplôme

  //recupération des variables
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $teatcher=htmlspecialchars(addslashes($_POST['teatcher']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $coefficient=htmlspecialchars(addslashes($_POST['coef']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));


  //inscription dans la table matiere et enseigner

  $etab->AddMatiere($matiere,$classe,$teatcher,$codeEtab,$coefficient,$libellesession);




}if(isset($_POST['etape'])&&($_POST['etape']==2))
{
    //recupération des variables
    $idmat=htmlspecialchars(addslashes($_POST['idmat']));
    $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
    $matiere=htmlspecialchars(addslashes($_POST['matiere'.$idmat]));
    $classe=htmlspecialchars(addslashes($_POST['classe'.$idmat]));
    $teatcher=htmlspecialchars(addslashes($_POST['teatcher'.$idmat]));
    $coefficient=htmlspecialchars(addslashes($_POST['coef'.$idmat]));

    //update de la table matiere et dispenser

    $etab->UpdateMatiere($matiere,$classe,$teatcher,$codeEtab,$coefficient,$idmat);


}

if(isset($_GET['etape'])&&($_GET['etape']==3))
{
  //il est question de supprimer la matière

  //recupération des variables

  $classe=htmlspecialchars(addslashes($_GET['classe']));
  $idmat=htmlspecialchars(addslashes($_GET['matiere']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));

    $etab->DelatedMatiere($idmat,$classe,$codeEtab);

}

?>
