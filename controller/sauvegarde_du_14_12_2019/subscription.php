<?php
session_start();
require_once('../class/User.php');
require_once('../class/Parent.php');
require_once('../class/Etablissement.php');
require_once('../controller/functions.php');
$parent = new ParentX();
$etabs=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

  //informations Parents

$nomTea=utf8_encode(utf8_decode(htmlspecialchars(addslashes($_POST['nomTea']))));
$prenomTea=utf8_decode(utf8_encode(htmlspecialchars(addslashes($_POST['prenomTea']))));
$pays=htmlspecialchars(addslashes($_POST['pays']));
$telTea=htmlspecialchars(addslashes($_POST['telTea']));
$datenaisTea=htmlspecialchars(addslashes($_POST['datenaisTea']));
$fonctionTea=htmlspecialchars(addslashes($_POST['fonctionTea']));
$sexeTea=htmlspecialchars(addslashes($_POST['sexeTea']));
$emailTea=htmlspecialchars(addslashes($_POST['emailTea']));
$loginTea=htmlspecialchars(addslashes($_POST['loginTea']));
$passTea=htmlspecialchars(addslashes($_POST['passTea']));
$cniTea=htmlspecialchars(addslashes($_POST['cniTea']));



//informations

$studentsarray=htmlspecialchars(addslashes($_POST['studentsarray']));
$subscribearray=htmlspecialchars(addslashes($_POST['subscribearray']));
$subscribeIdarray=htmlspecialchars(addslashes($_POST['subscribeIdarray']));
$codeEtabarray=htmlspecialchars(addslashes($_POST['codeEtabarray']));
$studentIdentarray=htmlspecialchars(addslashes($_POST['studentIdentarray']));
$montanttotale=htmlspecialchars(addslashes($_POST['montanttotale']));
$subscribeIdarray=htmlspecialchars(addslashes($_POST['subscribeIdarray']));
$nbselect=htmlspecialchars(addslashes($_POST['nbselect']));
$paymentno=htmlspecialchars(addslashes($_POST['paymentno']));
$transacId=htmlspecialchars(addslashes($_POST['transacId']));
$selectmobileop=htmlspecialchars(addslashes($_POST['selectmobileop']));
$devisecontry=htmlspecialchars(addslashes($_POST['devisecontry']));


//insertion dans la table compte

$datecrea=date("Y-m-d");
$type_cpte="Parent";
$statut=1;

// $parent->AddParentwithoutfile($nomTea,$prenomTea,$contactTea,$fonction,$cni,$emailTea,$datenaisTea,$loginTea,$passTea,$type_cpte,$statut,$datecrea,$sexeTea,$codeEtab);

$idparentcpte=$parent->AddParentSubscribe($nomTea,$prenomTea,$telTea,$fonctionTea,$cniTea,$emailTea,$datenaisTea,$loginTea,$passTea,$type_cpte,$statut,$datecrea,$sexeTea);

//insertion dans la table enregistrer

$newnbselect=$nbselect-1;
$codeEtabarray=substr($codeEtabarray, 0, -1);
$subscribeIdarray=substr($subscribeIdarray, 0, -1);



$tabcodeEtab=explode(",",$codeEtabarray);

for($i=0;$i<=$newnbselect;$i++)
{
  $parent->AddBySchool($idparentcpte,$tabcodeEtab[$i]);
}

//insertion dans la table paiement
$studentIdentarray=substr($studentIdentarray, 0, -1);
$statutpaie=0;

$parent->AddParentPaiement($datecrea,$idparentcpte,$studentIdentarray,$montanttotale,$selectmobileop,$transacId,$paymentno,$statutpaie,$nbselect,$subscribeIdarray,$devisecontry);

//envoi du mail au responsable et redirection pour la connexion au compte parent

$etabs->SendSouscriptionMailler();

// $retourmsg=$parent->SendSouscriptionMailler();
//
// if($retourmsg=="ok")
// {
//   //redirection
//
//   header("Location:../signup.php");
// }

$_SESSION['user']['addprogra']="Souscription soumise avec succès , Votre demande sera traiter par un Administrateur dans les plus bref délais";

header("Location:../signup.php");

}


 ?>
