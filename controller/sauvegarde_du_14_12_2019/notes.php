<?php
session_start();
require_once('../controller/functions.php');
require_once('../class/Student.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Matiere.php');
$classe = new Classe();
$student=new Student();
$etabs=new Etab();
$matierestud= new Matiere();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //il est question des notes de controle

  //reciprération des variables
  // $Note28: 15
  // $obser28: TRES BIEN
  // $studentmat: 28*
  $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
  $classeId=htmlspecialchars(addslashes($_POST['classeId']));
  $typenote=htmlspecialchars(addslashes($_POST['typenote']));
  $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
  $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
  $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $sessionlibelle=htmlspecialchars(addslashes($_POST['sessionlibelle']));

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
  $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
  $libelleclasse=$classe->getInfosofclassesbyId($classeId,$sessionlibelle);
  $datepiste=date("Y-m-d");
  $controlepiste=1;
  $reglementpiste=0;
  $examenpiste=0;
  $actionpiste=1;
  $tabNameofstudents="";

  //nous allons verifier si nous sommes dans le cas d'un etablissement primaire ou secondaire

  $etablissementType=$etabs->DetermineTypeEtab($codeEtab);

  if($etablissementType==1||$etablissementType==3)
  {
    //cas etablissement primaire ou maternelle

    echo "etablissement primaire";

    //insertion dans la table piste

    $idpiste=$etabs->AddpistePrimary($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$sessionlibelle,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);
    $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);
    $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentmat'])));

    //nous allons voir si nous avons au moins notes pour ce pour les eleves de cette classe pour ce semestre et pour cette matiere

    for($i=0;$i<$nbstudent;$i++)
   {
      $notes=htmlspecialchars(addslashes($_POST['note'.$tabstudent[$i]]));
      $observation=htmlspecialchars(addslashes($_POST['obser'.$tabstudent[$i]]));
      $ideleve=$tabstudent[$i];

      //nous allons verifier pour voir si l'eleve a deja une note pour ce controle precis

      //nous allons donc ajouter la note dans le système


    $studentDatas=$student->getAllInformationsOfStudent($ideleve,$sessionlibelle);
    $studentData=explode("*",$studentDatas);
    $nomcompletStudent=$studentData[2]." ".$studentData[3];
    $tabNameofstudents=$tabNameofstudents.$nomcompletStudent."*";

    //insertion dans la table pisteNotes
    $firstnote=1;
    $etabs->AddNotesPiste($idpiste,$idtypenote,$firstnote,$notes,$ideleve);

    //retrouver les informations du parent à savoir son email et son contact

    $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

    $tabdatadestinataires="Parent";
    //nous allons rechercher l'email du parent
    $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($classeId,$tabdatadestinataires,$codeEtab,$sessionlibelle,$ideleve);
    $ka=1;
    foreach ($dataParents as $parents):

      $destimails=$parents->email_parent;
      $destiphone=$indicatifEtab.$parents->tel_parent;
    $ka++;
    endforeach;

      $nbnotes=$student->DetermineNoteNumbercontroles($tabstudent[$i],$classeId,$matiereid,$idtypenote,$codeEtab);
      if($nbnotes==0)
      {
        //cet eleve n'a pas deja de note enregistrer pour ce controle





      $student->AddNotesControle($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation,$sessionlibelle);

      // echo $observation;


      }else if($nbnotes>0)
      {
        //cet eleve à deja une note enregistrer pour ce controle

        //nous allons donc modifier la note de cet eleve pour ce controle

        $student->UpdatenotesPrimary($ideleve,$idtypenote,$notes,$observation,$classeId,$matiereid,$codeEtab,$teatcherid,$sessionlibelle);
      }

      // $etabs->SendAddedControleNotesMailerToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$sessionlibelle,$codeEtab,$libellematiere);

   }



  }else
  {
    //cas etablissement secondaire

      //nous allons rechercher le coefficient du controle
    $dataNotes=$etabs->getControlecoef($idtypenote,$matiereid,$teatcherid,$codeEtab,$sessionlibelle);
    $tabdata=explode("*",$dataNotes);
    $coefficientNotes=$tabdata[0];
    $typesessionNotes=$tabdata[1];

    $idpiste=$etabs->Addpiste($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$sessionlibelle,$typesessionNotes,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);
    $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);
    $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentmat'])));

    //nous allons voir si nous avons au moins notes pour ce pour les eleves de cette classe pour ce semestre et pour cette matiere
    //nous allons voir si nous avons au moins notes pour ce pour les eleves de cette classe pour ce semestre et pour cette matiere

      $nbnotessemesterclasses=$etabs->DeterminationOfNotesNumberForThisclasses($matiereid,$teatcherid,$codeEtab,$sessionlibelle,$typesessionNotes,$classeId);

      if($nbnotessemesterclasses==0)
      {
        //c'est la première notes que nous ajoutons pour cette classe pour ce semetre

        for($i=0;$i<$nbstudent;$i++)
        {

          $notes=htmlspecialchars(addslashes($_POST['note'.$tabstudent[$i]]));
          $observation=htmlspecialchars(addslashes($_POST['obser'.$tabstudent[$i]]));
          //insertion dans la table note

          $ideleve=$tabstudent[$i];
          $studentDatas=$student->getAllInformationsOfStudent($ideleve,$sessionlibelle);
          $studentData=explode("*",$studentDatas);
          $nomcompletStudent=$studentData[2]." ".$studentData[3];
          $tabNameofstudents=$tabNameofstudents.$nomcompletStudent."*";
          //$libelleclasse=$studentData[9];

          $notescoef=$notes*$coefficientNotes;

          $moyennestudent=$notescoef/$coefficientNotes;

          //nous allons ajouter la note dans rating

          $etabs->AddOldRatingStudent($sessionlibelle,$typesessionNotes,$ideleve,$classeId,$teatcherid,$matiereid,$notes,$notescoef,$coefficientNotes,$moyennestudent,$codeEtab);

          //insertion dans la table pisteNotes
          $firstnote=1;

          $etabs->AddNotesPiste($idpiste,$idtypenote,$firstnote,$notes,$ideleve);

          //retrouver les informations du parent à savoir son email et son contact

          $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

          $tabdatadestinataires="Parent";
          //nous allons rechercher l'email du parent
          $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($classeId,$tabdatadestinataires,$codeEtab,$sessionlibelle,$libellematiere,$codeEtab);
          $ka=1;
          foreach ($dataParents as $parents):

            $destimails=$parents->email_parent;
            $destiphone=$indicatifEtab.$parents->tel_parent;


            $ka++;
          endforeach;

          //$etabs->SendAddedControleNotesMailerToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$sessionlibelle,$codeEtab,$libellematiere);

          $student->AddNotesControle($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation,$sessionlibelle);

          // echo $observation;
        }


      }else if($nbnotessemesterclasses>0)
      {

        for($i=0;$i<$nbstudent;$i++)
        {
          $notes=htmlspecialchars(addslashes($_POST['note'.$tabstudent[$i]]));
          $observation=htmlspecialchars(addslashes($_POST['obser'.$tabstudent[$i]]));
          //insertion dans la table note
          $ideleve=$tabstudent[$i];
          $studentDatas=$student->getAllInformationsOfStudent($ideleve,$sessionlibelle);
          $studentData=explode("*",$studentDatas);
          $nomcompletStudent=$studentData[2]." ".$studentData[3];

          $tabNameofstudents=$tabNameofstudents.$nomcompletStudent."*";

          //nous allons recuperer le totalnotes,totalnotescoef,totalcoef

          $LastratingInfos=$etabs->getRatingtypesessionInfos($ideleve,$idtypenote,$matiereid,$teatcherid,$codeEtab,$sessionlibelle,$classeId,$typesessionNotes);
          $tabLastInfos=explode("*",$LastratingInfos);
          $alltotalnotes=$tabLastInfos[0];
          $alltotalnotescoef=$tabLastInfos[1];
          $totalcoefnotes=$tabLastInfos[2];
          $ratingId=$tabLastInfos[3];

          //nouvelles Informations

          $sommesNotes=$alltotalnotes+$notes;
          $sommesNotescoef=$alltotalnotescoef+($notes*$coefficientNotes);
          $sommescoef=$totalcoefnotes+$coefficientNotes;
          $moyenne=$sommesNotescoef/$sommescoef;

          $etabs->UpdateRatingStudent($sessionlibelle,$typesessionNotes,$ideleve,$classeId,$teatcherid,$matiereid,$sommesNotes,$sommesNotescoef,$sommescoef,$moyenne,$ratingId,$codeEtab);

          $firstnote=1;

          $etabs->AddNotesPiste($idpiste,$idtypenote,$firstnote,$notes,$ideleve);

        }

      }


  }





  $_SESSION['user']['addattendailyok']="Notes de classe ajouter avec succès";



  if($_SESSION['user']['profile'] == "Admin_globale")

  {

header("Location:../manager/notes.php");

  }else if($_SESSION['user']['profile'] == "Admin_locale")

  {

header("Location:../locale/notes.php");

}else if($_SESSION['user']['profile'] == "Teatcher")

  {

header("Location:../teatcher/notes.php");

  }


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //il est question des notes d'examen

  $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
  $classeId=htmlspecialchars(addslashes($_POST['classeId']));
  $typenote=htmlspecialchars(addslashes($_POST['typenote']));
  $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
  $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
  $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $sessionlibelle=htmlspecialchars(addslashes($_POST['sessionlibelle']));

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
  $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
  $libelleclasse=$classe->getInfosofclassesbyId($classeId,$sessionlibelle);
  $datepiste=date("Y-m-d");
  $controlepiste=0;
  $reglementpiste=0;
  $examenpiste=1;
  $actionpiste=1;

  $etablissementType=$etabs->DetermineTypeEtab($codeEtab);

  $tabNameofstudents="";

  if($etablissementType==1||$etablissementType==3)
  {
    //etablissement primaire et maternelle

      $idpiste=$etabs->AddpistePrimary($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$sessionlibelle,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);
      // echo $idpiste;
      $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);
      $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentmat'])));

      for($i=0;$i<$nbstudent;$i++)
      {
        $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
        $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
        $ideleve=$tabstudent[$i];

        //nous allons verifier pour voir si l'eleve a deja une note pour ce controle precis

        //nous allons donc ajouter la note dans le système

        $studentDatas=$student->getAllInformationsOfStudent($ideleve,$sessionlibelle);
        $studentData=explode("*",$studentDatas);
        $nomcompletStudent=$studentData[2]." ".$studentData[3];

        $tabNameofstudents=$tabNameofstudents.$nomcompletStudent."*";

        //insertion dans la table pisteNotes
        $firstnote=1;
        $etabs->AddNotesPisteExam($idpiste,$idtypenote,$firstnote,$notes,$ideleve);

        //retrouver les informations du parent à savoir son email et son contact

        $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

        $tabdatadestinataires="Parent";
        //nous allons rechercher l'email du parent
        $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($classeId,$tabdatadestinataires,$codeEtab,$sessionlibelle,$ideleve);
        $ka=1;
        foreach ($dataParents as $parents):

          $destimails=$parents->email_parent;
          $destiphone=$indicatifEtab.$parents->tel_parent;
        $ka++;
        endforeach;


        // $nbnotes=$student->DetermineNoteNumbercontroles($tabstudent[$i],$classeId,$matiereid,$idtypenote,$codeEtab);
        $nbnotes=$student->DetermineNoteNumberexamen($tabstudent[$i],$classeId,$idtypenote,$matiereid,$teatcherid,$codeEtab);

        // echo $nbnotes."<br>";

        if($nbnotes==0)
        {
          //cet eleve n'a pas deja de note enregistrer pour ce controle


      $student->AddNotesExamen($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation,$sessionlibelle);

        // echo $observation;


        }else if($nbnotes>0)
        {
          //cet eleve à deja une note enregistrer pour ce controle

          //nous allons donc modifier la note de cet eleve pour ce controle

          $student->UpdatenoteExamPrimary($ideleve,$idtypenote,$notes,$observation,$classeId,$matiereid,$codeEtab,$teatcherid,$sessionlibelle);
        }



      }

      // $etabs->SendAddedControleNotesMailerToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$sessionlibelle,$codeEtab,$libellematiere);





  }else {
    //etablissement secondaire

     $idpiste=$etabs->Addpiste($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$libelleclasse,$typesessionNotes,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);
     $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);
     $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentmat'])));

     for($i=0;$i<$nbstudent;$i++)
     {
       $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
       $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
       $ideleve=$tabstudent[$i];

       $studentDatas=$student->getAllInformationsOfStudent($ideleve,$sessionlibelle);
       $studentData=explode("*",$studentDatas);
       $nomcompletStudent=$studentData[2]." ".$studentData[3];

       $tabNameofstudents=$tabNameofstudents.$nomcompletStudent."*";

       $etabs->AddNotesPisteExam($idpiste,$idtypenote,$firstnote,$notes,$ideleve);

      //retrouver les informations du parent à savoir son email et son contact

      $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

      $tabdatadestinataires="Parent";
    //nous allons rechercher l'email du parent
      $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($classeId,$tabdatadestinataires,$codeEtab,$sessionlibelle,$libellematiere,$codeEtab);
      $ka=1;
      foreach ($dataParents as $parents):

        $destimails=$parents->email_parent;
        $destiphone=$indicatifEtab.$parents->tel_parent;


        $ka++;
      endforeach;

      $nbnotes=$student->DetermineNoteNumberexamen($tabstudent[$i],$classeId,$idtypenote,$matiereid,$teatcherid,$codeEtab);

      // echo $nbnotes."<br>";

      if($nbnotes==0)
      {
        //cet eleve n'a pas deja de note enregistrer pour ce controle


    $student->AddNotesExamen($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation,$sessionlibelle);

      // echo $observation;


      }else if($nbnotes>0)
      {
        //cet eleve à deja une note enregistrer pour ce controle

        //nous allons donc modifier la note de cet eleve pour ce controle

        $student->UpdatenoteExamPrimary($ideleve,$idtypenote,$notes,$observation,$classeId,$matiereid,$codeEtab,$teatcherid,$sessionlibelle);
      }


     }

     // $etabs->SendAddedExamenNotesMailerToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$sessionlibelle,$codeEtab);

  }

  $_SESSION['user']['addattendailyok']="Notes de classe ajouter avec succès";



  if($_SESSION['user']['profile'] == "Admin_globale")

  {

header("Location:../manager/notes.php");

  }else if($_SESSION['user']['profile'] == "Admin_locale")

  {

header("Location:../locale/notes.php");

}else if($_SESSION['user']['profile'] == "Teatcher")

  {

header("Location:../teatcher/notes.php");

  }






}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recupération des variables
  $noteid=htmlspecialchars(addslashes($_POST['noteid']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $studentid=htmlspecialchars(addslashes($_POST['studentid']));
  $classeid=htmlspecialchars(addslashes($_POST['classeid']));
  $note=htmlspecialchars(addslashes($_POST['note']));
  $observation=htmlspecialchars(addslashes($_POST['observation']));

  $student->UpdateNotesOfStudent($noteid,$codeEtab,$studentid,$classeid,$note,$observation);

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
    if($_POST['typenote']==1)
    {
      //il est question d'un controle
      //recuperation des variables

      $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
      $classeId=htmlspecialchars(addslashes($_POST['classeId']));
      $typenote=htmlspecialchars(addslashes($_POST['typenote']));
      $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
      $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
      $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
      $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
      $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

      $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
      $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
      $libelleclasse=$classe->getInfosofclassesbyId($classeId,$libellesession);
      $datepiste=date("Y-m-d");
      $controlepiste=1;
      $reglementpiste=0;
      $examenpiste=0;
      $actionpiste=2;

      $dataNotes=$etabs->getControlecoef($idtypenote,$matiereid,$teatcherid,$codeEtab,$libellesession);
      $tabdata=explode("*",$dataNotes);

      $coefficientNotes=$tabdata[0];
      $typesessionNotes=$tabdata[1];

      $idpiste=$etabs->Addpiste($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$libelleclasse,$typesessionNotes,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);


      //$typesessionNotes=$etabs->getSessionTypeExam($idtypenote,$codeEtab,$libellesession);

      $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);
      // $oldnote=htmlspecialchars(addslashes($_POST['oldnote']));

      $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentmat'])));

      //mise à jour des notes

      for($i=0;$i<$nbstudent;$i++)
      {

        $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
        $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
        $ideleve=$tabstudent[$i];
        $studentDatas=$student->getAllInformationsOfStudent($ideleve,$libellesession);
        $studentData=explode("*",$studentDatas);
        $nomcompletStudent=$studentData[2]." ".$studentData[3];
        //$libelleclasse=$studentData[9];

        //nous allons verifier si cet eleve à deja une note pour ce controle si non alors nous allons ajouter la notes

        $check=$student->getNotesAndObservControleNb($ideleve,$typenote,$classeId,$idtypenote,$matiereid,$teatcherid,$libellesession,$codeEtab);

         // echo $check."<br>";

        if($check==0)
        {
          //ajout de la note

          $notescoef=$notes*$coefficientNotes;

          $moyennestudent=$notescoef/$coefficientNotes;

          //nous allons ajouter la note dans rating

          $etabs->AddOldRatingStudent($sessionlibelle,$typesessionNotes,$ideleve,$classeId,$teatcherid,$matiereid,$notes,$notescoef,$coefficientNotes,$moyennestudent,$codeEtab);

          $student->AddNotesControle($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation,$libellesession);

        }else if($check>0)
        {
          //notification aux parents puis modifications de notes

          $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

          $tabdatadestinataires="Parent";
          //nous allons rechercher l'email du parent
          $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($classeId,$tabdatadestinataires,$codeEtab,$libellesession);
          $ka=1;
          foreach ($dataParents as $parents):

            $destimails=$parents->email_parent;
            $destiphone=$indicatifEtab.$parents->tel_parent;


            $ka++;
          endforeach;

          //$etabs->SendModificationNotesToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$libellematiere,$codeEtab);


        }
        // echo "bonjour";
        //nous allons recuperer les informations de la table rating (id_rating,	totalnotes_rating,totalnotescoef_rating,	totalcoef_rating,rating)
        $ratinginformations=$etabs->getStudentRatingInfos($ideleve,$typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$codeEtab,$libellesession,$typesessionNotes);

        $tabInfosrating=explode("*",$ratinginformations);

        //var_dump($tabInfosrating);
        //recuperation des informations

        $ratingId=$tabInfosrating[0];
        $totalnotesrating=$tabInfosrating[1];
        $totalnotescoefrating=$tabInfosrating[2];
        $totalcoefrating=$tabInfosrating[3];
        $rating=$tabInfosrating[4];

        //retrouver la note correspndant a ce controle

        $oldnotes=$etabs->getLastNotescontroleAddedd($ideleve,$typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$codeEtab,$libellesession,$typesessionNotes);

        //nouvelle operation

        // echo $oldnotes."<br>";

        $sommesNotes=($totalnotesrating-$oldnotes)+$notes;
        $sommesNotescoef=$totalnotescoefrating-($oldnotes*$coefficientNotes)+($notes*$coefficientNotes);
        $sommescoef=$totalcoefrating;
        $moyenne=$sommesNotescoef/$totalcoefrating;

        echo "ancienne informations<br>";
        echo $totalnotesrating." ".$totalnotescoefrating." ".$totalcoefrating." ".$rating."<br>";
        echo "nouvelle informations<br>";
        echo $sommesNotes." ".$sommesNotescoef." ".$sommescoef." ".$moyenne." ".$ratingId." ".$oldnotes." <br>";
        echo "calcul<br>";
        echo $sommesNotescoef/$totalcoefrating."<br>";

        //insertion dans la tables pisteNotes

        //$etabs->UpdatepisteNotes();

        $etabs->UpdateRatingStudent($libellesession,$typesessionNotes,$ideleve,$classeId,$teatcherid,$matiereid,$sommesNotes,$sommesNotescoef,$sommescoef,$moyenne,$ratingId,$codeEtab);
        //
         $firstnote=0;
        //
         // $etabs->AddNotesPisteAfter($idpiste,$idtypenote,$firstnote,$notes,$ideleve,$oldnotes);



        //insertion dans la table note

        // $student->AddNotesControle($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation);

        $student->UpdateControleNotes($notes,$observation,$ideleve,$typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$codeEtab);

        // echo $observation;
      }



    }else if($_POST['typenote']==2)
    {
      //il est question d'un examen
      $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
      $classeId=htmlspecialchars(addslashes($_POST['classeId']));
      $typenote=htmlspecialchars(addslashes($_POST['typenote']));
      $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
      $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
      $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
      $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
      $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
      $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
      $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
      $libelleclasse=$classe->getInfosofclassesbyId($classeId,$libellesession);
      $datepiste=date("Y-m-d");
      $controlepiste=0;
      $reglementpiste=0;
      $examenpiste=1;
      $actionpiste=2;
      $typesessionNotes=$etabs->getSessionTypeExam($idtypenote,$codeEtab,$libellesession);

      $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);




      $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentmat'])));

      for($i=0;$i<$nbstudent;$i++)
      {

        $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
        $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
        $ideleve=$tabstudent[$i];
        $studentDatas=$student->getAllInformationsOfStudent($ideleve,$libellesession);
        $studentData=explode("*",$studentDatas);
        $nomcompletStudent=$studentData[2]." ".$studentData[3];
        //$libelleclasse=$studentData[9];

        //nous allons verifier si cet eleve à deja une note pour cet examen si non alors nous allons ajouter la notes

        $check=$student->getNotesAndObservExamNb($ideleve,$typenote,$classeId,$idtypenote,$matiereid,$teatcherid,$libellesession,$codeEtab);

        if($check==0)
        {
  $student->AddNotesExamen($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation,$libellesession);
        }else if($check>0)
        {
          $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

          $tabdatadestinataires="Parent";
          //nous allons rechercher l'email du parent
          $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($classeId,$tabdatadestinataires,$codeEtab,$libellesession);
          $ka=1;
          foreach ($dataParents as $parents):

            $destimails=$parents->email_parent;
            $destiphone=$indicatifEtab.$parents->tel_parent;


            $ka++;
          endforeach;

          $etabs->SendModificationNotesToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$libellematiere,$codeEtab);


        }

        //insertion dans la table note

        // $student->AddNotesControle($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation);

        $student->UpdateControleNotes($notes,$observation,$ideleve,$typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$codeEtab);

        // echo $observation;
      }


    }
}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //nous avons initier une modification de note qui sera soumise à validation

  if($_POST['typenote']==1)
  {
    //il est question d'un controle

    $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
    $classeId=htmlspecialchars(addslashes($_POST['classeId']));
    $typenote=htmlspecialchars(addslashes($_POST['typenote']));
    $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
    $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
    $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
    $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
    $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
    $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
    $libelleclasse=$classe->getInfosofclassesbyId($classeId,$libellesession);

     $datepiste=date("Y-m-d");
     $controlepiste=1;
     $reglementpiste=0;
     $examenpiste=0;
     $actionpiste=2;
     $validationstatus=0;

     $dataNotes=$etabs->getControlecoef($idtypenote,$matiereid,$teatcherid,$codeEtab,$libellesession);
     $tabdata=explode("*",$dataNotes);
     $coefficientNotes=$tabdata[0];
     $typesessionNotes=$tabdata[1];

      $idpiste=$etabs->Addpiste($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$libellesession,$typesessionNotes,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);

      $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);

       $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentids'])));

       //stocker la valeur à modifier pour etre en attente de validation

       for($i=0;$i<$nbstudent;$i++)
      {
        $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
        $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
        $ideleve=$tabstudent[$i];

        //ajout dans la table  notesmodification

        $student->AddstandbyUpdatenotes($idpiste,$controlepiste,$examenpiste,$idtypenote,$matiereid,$teatcherid,$classeId,$validationstatus,$ideleve,$notes,$observation);

      }

      $_SESSION['user']['addattendailyok']="Modification de note soumise et en attente de validation";

      if($_SESSION['user']['profile'] == "Admin_globale")

      {

    header("Location:../manager/notes.php");

      }else if($_SESSION['user']['profile'] == "Admin_locale")

      {

    header("Location:../locale/notes.php");

  }else if($_SESSION['user']['profile'] == "Teatcher")

      {

    header("Location:../locale/notes.php");

      }

  }else if($_POST['typenote']==2)
  {
    //il est question d'un examen

    $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
    $classeId=htmlspecialchars(addslashes($_POST['classeId']));
    $typenote=htmlspecialchars(addslashes($_POST['typenote']));
    $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
    $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
    $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
    $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
    $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
    $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
    $libelleclasse=$classe->getInfosofclassesbyId($classeId,$libellesession);

     $datepiste=date("Y-m-d");
     $controlepiste=0;
     $reglementpiste=0;
     $examenpiste=1;
     $actionpiste=2;
     $validationstatus=0;

     $dataNotes=$etabs->getControlecoef($idtypenote,$matiereid,$teatcherid,$codeEtab,$libellesession);
     $tabdata=explode("*",$dataNotes);
     $coefficientNotes=$tabdata[0];
     $typesessionNotes=$tabdata[1];

     $idpiste=$etabs->Addpiste($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$libellesession,$typesessionNotes,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);
     $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);
     $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentids'])));

     //stocker la valeur à modifier pour etre en attente de validation

     for($i=0;$i<$nbstudent;$i++)
    {
      $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
      $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
      $ideleve=$tabstudent[$i];

      //ajout dans la table  notesmodification

      $student->AddstandbyUpdatenotes($idpiste,$controlepiste,$examenpiste,$idtypenote,$matiereid,$teatcherid,$classeId,$validationstatus,$ideleve,$notes,$observation);

    }

    $_SESSION['user']['addattendailyok']="Modification de note soumise et en attente de validation";

    if($_SESSION['user']['profile'] == "Admin_globale")

    {

  header("Location:../manager/notes.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {

  header("Location:../locale/notes.php");

}else if($_SESSION['user']['profile'] == "Teatcher")

    {

  header("Location:../locale/notes.php");

    }



  }




}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{

  if($_POST['typenote']==1)
  {
    //il est question d'un controle

    $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
    $classeId=htmlspecialchars(addslashes($_POST['classeId']));
    $typenote=htmlspecialchars(addslashes($_POST['typenote']));
    $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
    $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
    $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
    $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
    $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
    $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
    $libelleclasse=$classe->getInfosofclassesbyId($classeId,$libellesession);

     $datepiste=date("Y-m-d");
     $controlepiste=1;
     $reglementpiste=0;
     $examenpiste=0;
     $actionpiste=2;
     $validationstatus=0;

     $dataNotes=$etabs->getControlecoef($idtypenote,$matiereid,$teatcherid,$codeEtab,$libellesession);
     $tabdata=explode("*",$dataNotes);
     $coefficientNotes=$tabdata[0];
     $typesessionNotes=$tabdata[1];

     $idpiste=$etabs->Addpiste($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$libellesession,$typesessionNotes,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);

     $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);

      $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentids'])));

      //stocker la valeur à modifier pour etre en attente de validation

      for($i=0;$i<$nbstudent;$i++)
     {
       $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
       $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
       $ideleve=$tabstudent[$i];

       //ajout dans la table  notesmodification

       $student->AddstandbyUpdatenotes($idpiste,$controlepiste,$examenpiste,$idtypenote,$matiereid,$teatcherid,$classeId,$validationstatus,$ideleve,$notes,$observation);

     }

     $_SESSION['user']['addattendailyok']="Modification de note soumise et en attente de validation";

     if($_SESSION['user']['profile'] == "Admin_globale")

     {

   header("Location:../manager/noteseleves.php");

     }else if($_SESSION['user']['profile'] == "Admin_locale")

     {

   header("Location:../locale/noteseleves.php");

 }else if($_SESSION['user']['profile'] == "Teatcher")

     {

   header("Location:../locale/noteseleves.php");

     }





  }else if($_POST['typenote']==2)
  {
    //il est question d'un examen

    $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
    $classeId=htmlspecialchars(addslashes($_POST['classeId']));
    $typenote=htmlspecialchars(addslashes($_POST['typenote']));
    $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
    $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
    $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
    $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
    $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
    $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
    $libelleclasse=$classe->getInfosofclassesbyId($classeId,$libellesession);

     $datepiste=date("Y-m-d");
     $controlepiste=0;
     $reglementpiste=0;
     $examenpiste=1;
     $actionpiste=2;
     $validationstatus=0;

     $dataNotes=$etabs->getControlecoef($idtypenote,$matiereid,$teatcherid,$codeEtab,$libellesession);
     $tabdata=explode("*",$dataNotes);
     $coefficientNotes=$tabdata[0];
     $typesessionNotes=$tabdata[1];

     $idpiste=$etabs->Addpiste($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$libellesession,$typesessionNotes,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);
     $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);
     $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentids'])));

     //stocker la valeur à modifier pour etre en attente de validation

     for($i=0;$i<$nbstudent;$i++)
    {
      $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
      $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
      $ideleve=$tabstudent[$i];

      //ajout dans la table  notesmodification

      $student->AddstandbyUpdatenotes($idpiste,$controlepiste,$examenpiste,$idtypenote,$matiereid,$teatcherid,$classeId,$validationstatus,$ideleve,$notes,$observation);

    }

    $_SESSION['user']['addattendailyok']="Modification de note soumise et en attente de validation";

    if($_SESSION['user']['profile'] == "Admin_globale")

    {

  header("Location:../manager/noteseleves.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {

  header("Location:../locale/noteseleves.php");

}else if($_SESSION['user']['profile'] == "Teatcher")

    {

  header("Location:../locale/noteseleves.php");

    }
  }


}
