<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
$classe = new Classe();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recuperation des variables

  $exam=htmlspecialchars(addslashes($_POST['examen']));
  $datedeb=htmlspecialchars(addslashes($_POST['datedeb']));
  $datefin=htmlspecialchars(addslashes($_POST['datefin']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
  $typesess=htmlspecialchars(addslashes($_POST['typesess']));

//inserion dans la table examens

  $etab->AddExamenSchool($exam,$datedeb,$datefin,$codeEtab,$libellesession,$typesess);

}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recuperation des variables
$idexam=htmlspecialchars(addslashes($_POST['idexam']));

$exam=htmlspecialchars(addslashes($_POST['exam'.$idexam]));
$datedeb=htmlspecialchars(addslashes($_POST['datedeb'.$idexam]));
$datefin=htmlspecialchars(addslashes($_POST['datefin'.$idexam]));
$codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));

$etab->UpdateExamenOfClasses($exam,$datedeb,$datefin,$codeEtab,$idexam);


}

if(isset($_GET['etape'])&&($_GET['etape']==3))
{
    //recupération des variables
    $examen=htmlspecialchars(addslashes($_GET['examen']));
    $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));

    $etab->supprimerExamenOfthisSchool($examen,$codeEtab);
}


 ?>
