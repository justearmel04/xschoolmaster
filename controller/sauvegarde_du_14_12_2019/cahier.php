<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Matiere.php');
$classe = new Classe();
$etab=new Etab();
$matiere=new Matiere();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recuperation des variables

$libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
$classeEtab=htmlspecialchars(addslashes($_POST['classeEtab']));
$codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
$matclasse=htmlspecialchars(addslashes($_POST['matclasse']));
$descri=htmlspecialchars(addslashes($_POST['descri']));
$tabmatiere=explode("-",$matclasse);
$matiereid=$tabmatiere[0];
$teatcherid=$tabmatiere[1];
$dateday=date("Y-m-d");

//section
$concatsection=htmlspecialchars(addslashes($_POST['concatsection']));
$concatnbsection=htmlspecialchars(addslashes($_POST['concatnbsection']));
$tabsection=explode("@",$concatsection);

//tache

$concattache=htmlspecialchars(addslashes($_POST['concattache']));
$concatnbtache=htmlspecialchars(addslashes($_POST['concatnbtache']));
$tabtache=explode("@",$concattache);

//insertion dans la table cahier

$idcahier=$etab->Addcahier($libellesession,$codeEtab,$matiereid,$teatcherid,$dateday,$classeEtab,$descri);

if($concatnbsection>0)
{
  //nous avons des sections a renseigner $tabsection

for($i=0;$i<$concatnbsection;$i++)
{
  $section=htmlspecialchars(addslashes($_POST['section_'.$tabsection[$i]]));

  $etab->Addsectioncahier($idcahier,$section);

}
}
//insertion dans la table tache

if($concatnbtache>0)
{
    //nous avons des taches pour cet cahier

for($j=0;$j<$concatnbtache;$j++)
{
    $tache=htmlspecialchars(addslashes($_POST['tache_'.$tabtache[$j]]));

    $etab->Addtachecahier($idcahier,$tache);
}

}
  //nous allons faire la redirection sur la page cahiers.php

  // $_SESSION['user']['addprogra']="Cahier de texte ajouter avec succès";
  //
  // if($_SESSION['user']['profile'] == "Admin_globale") {
  //
  //       //header("Location:../manager/index.php");
  //       header("Location:../manager/cahiers.php");
  //
  //   }else if($_SESSION['user']['profile'] == "Admin_locale") {
  //
  //     header("Location:../locale/cahiers.php");
  //
  //   }else if($_SESSION['user']['profile'] == "Teatcher") {
  //
  //       header("Location:../teatcher/cahiers.php");
  //
  //       }



}







 ?>
