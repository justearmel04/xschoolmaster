<?php
session_start();
require_once('../class/User.php');
require_once('../class/Parent.php');
require_once('../controller/functions.php');
$parent = new ParentX();
/*
<input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabLocal;?>"/>
<input type="hidden" name="newparent" id="newparent" value="1"/>
*/

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recupération des variables

  $nomTea=htmlspecialchars(addslashes($_POST['nomTea']));
  $prenomTea=htmlspecialchars(addslashes($_POST['prenomTea']));
  $datenaisTea=htmlspecialchars(addslashes(dateFormat($_POST['datenaisTea'])));
  $sexeTea=htmlspecialchars(addslashes($_POST['sexeTea']));
  $contactTea=htmlspecialchars(addslashes($_POST['contactTea']));
  $fonction=htmlspecialchars(addslashes($_POST['fonctionTea']));
  $cni=htmlspecialchars(addslashes($_POST['cniTea']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $newparent=htmlspecialchars(addslashes($_POST['newparent']));
  $emailTea=htmlspecialchars(addslashes($_POST['emailTea']));
  $nationalite=htmlspecialchars(addslashes($_POST['nation']));
  $lieuH=htmlspecialchars(addslashes($_POST['lieuH']));
  $nbchild=htmlspecialchars(addslashes($_POST['nbchild']));
  $nbchildsco=htmlspecialchars(addslashes($_POST['nbchildsco']));
  $adrespro=htmlspecialchars(addslashes($_POST['adrespro']));
  $societe=htmlspecialchars(addslashes($_POST['societe']));
  $session=htmlspecialchars(addslashes($_POST['session']));

  // echo $datenaisTea;

  $datecrea=date("Y-m-d");
  $type_cpte="Parent";
  $statut=1;

  if($newparent==0)
  {
    //nous sommes dans un cas d'un ancien parent
    //recuperation de l'id compte du parent
    $idcompte=$parent->getIdcompteParentByCniandEmail($cni,$emailTea,$codeEtab);
    // echo "ancien";
    //enregistrer l'ancien parent
    $parent->Addoldparent($idcompte,$codeEtab);




  }else {
    //nous sommes dans le cas d'un nouveau parent
  // $parent->AddParentwithfile($nomTea,$prenomTea,$contactTea,$fonction,$cni,$emailTea,$datenaisTea,$loginTea,$passTea,$type_cpte,$statut,$datecrea,$fichierad,$sexeTea,$codeEtab);

  $parent->AddParent($nomTea,$prenomTea,$datenaisTea,$emailTea,$sexeTea,$contactTea,$fonction,$cni,$codeEtab,$nationalite,$lieuH,$nbchild,$nbchildsco,$adrespro,$societe,$session,$datecrea,$type_cpte,$statut);


  }



}else if(isset($_POST['etape'])&&($_POST['etape']==2)) {

  //modification du compte parent

  $nomTea=htmlspecialchars(addslashes($_POST['nomTea']));
  $prenomTea=htmlspecialchars(addslashes($_POST['prenomTea']));
  $datenaisTea=htmlspecialchars(addslashes(dateFormat($_POST['datenaisTea'])));
  $sexeTea=htmlspecialchars(addslashes($_POST['sexeTea']));
  $contactTea=htmlspecialchars(addslashes($_POST['contactTea']));
  $fonction=htmlspecialchars(addslashes($_POST['fonctionTea']));
  $cni=htmlspecialchars(addslashes($_POST['cniTea']));
  $emailTea=htmlspecialchars(addslashes($_POST['emailTea']));
  $idcompte=htmlspecialchars(addslashes($_POST['compte']));
  // $loginTea=htmlspecialchars(addslashes($_POST['loginTea']));
  // $passTea=htmlspecialchars(addslashes($_POST['passTea']));
  $nationalite=htmlspecialchars(addslashes($_POST['nation']));
  $lieuH=htmlspecialchars(addslashes($_POST['lieuH']));
  $nbchild=htmlspecialchars(addslashes($_POST['nbchild']));
  $nbchildsco=htmlspecialchars(addslashes($_POST['nbchildsco']));
  $adrespro=htmlspecialchars(addslashes($_POST['adrespro']));
  $societe=htmlspecialchars(addslashes($_POST['societe']));
  $session=htmlspecialchars(addslashes($_POST['session']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));


$parent->UpdateParent($nomTea,$prenomTea,$datenaisTea,$sexeTea,$contactTea,$fonction,$cni,$emailTea,$nationalite,$lieuH,$nbchild,$nbchildsco,$adrespro,$societe,$session,$codeEtab,$idcompte);

//echo date_format(date_create($datenaisTea),"Y-m-d");



// $parent->UpdateParentwithOutfilePassNot($nomTea,$prenomTea,$datenaisTea,$sexeTea,$contactTea,$fonction,$cni,$emailTea,$loginTea,$idcompte);




}else if(isset($_GET['etape'])&&($_GET['etape']==3)) {

  //recuperation des variables

  $compte=htmlspecialchars(addslashes($_GET['compte']));
  $statut=0;
  $parent->Deleteparent($compte,$statut);



}else if(isset($_POST['etape'])&&($_POST['etape']==4)) {

//recuperation des variables



$idparent=htmlspecialchars(addslashes($_POST['idparent']));
$nomTea=htmlspecialchars(addslashes($_POST['nomTea']));
$prenomTea=htmlspecialchars(addslashes($_POST['prenomTea']));
$datenaisTea=htmlspecialchars(addslashes(dateFormat($_POST['datenaisTea'])));
$lieunaisTea=htmlspecialchars(addslashes($_POST['lieunaisTea']));
$lieuH=htmlspecialchars(addslashes($_POST['lieuH']));
$sexeTea=htmlspecialchars(addslashes($_POST['sexeTea']));
$natTea=htmlspecialchars(addslashes($_POST['natTea']));
$emailTea=htmlspecialchars(addslashes($_POST['emailTea']));
$contactTea=htmlspecialchars(addslashes($_POST['contactTea']));
$situationTea=htmlspecialchars(addslashes($_POST['situationTea']));
$nbchildscoTea=htmlspecialchars(addslashes($_POST['nbchildscoTea']));
$nbchildTea=htmlspecialchars(addslashes($_POST['nbchildTea']));
$societe=htmlspecialchars(addslashes($_POST['societe']));
$adresspro=htmlspecialchars(addslashes($_POST['adresspro']));
$fonctionad=htmlspecialchars(addslashes($_POST['fonctionad']));

echo $datenaisTea;
$parent->UpdateParentAll($nomTea,$prenomTea,$datenaisTea,$lieunaisTea,$lieuH,$sexeTea,$natTea,$emailTea,$contactTea,$situationTea,$nbchildscoTea,$nbchildTea,$societe,$adresspro,$fonctionad,$idparent);

}else if(isset($_POST['etape'])&&($_POST['etape']==5)) {

//recuperation des des variables

$loginTea=htmlspecialchars(addslashes($_POST['loginTea']));
$passTea=htmlspecialchars(addslashes($_POST['passTea']));
$idparent=htmlspecialchars(addslashes($_POST['idparent']));

$parent->UpdateConnexionInfos($loginTea,$passTea,$idparent);

}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  //recuperation des variables

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $sessionEtab=htmlspecialchars(addslashes($_POST['session']));
  $concatparents=htmlspecialchars(addslashes($_POST['concatparents']));
  $datecrea=date("Y-m-d");
  $type_cpte="Parent";
  $statut=1;
  $concatparents=substr($concatparents, 0, -1);
  $dataparent=explode("@",$concatparents);
  $nbcountparent=count($dataparent);

  for($i=0;$i<$nbcountparent;$i++)
  {
    $nomTea=htmlspecialchars(addslashes($_POST['nomTea'.$dataparent[$i]]));
    $prenomTea=htmlspecialchars(addslashes($_POST['prenomTea'.$dataparent[$i]]));
    $sexeTea=htmlspecialchars(addslashes($_POST['sexeTea'.$dataparent[$i]]));
    $contactTea=htmlspecialchars(addslashes($_POST['contactTea'.$dataparent[$i]]));

    $parent->AddParentBoucle($nomTea,$prenomTea,$sexeTea,$contactTea,$type_cpte,$codeEtab,$sessionEtab,$statut,$datecrea);

  }

  $_SESSION['user']['addparok']="Parent ajouté avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/addallparents.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {
header("Location:../locale/addallparents.php");
  }

}



 ?>
