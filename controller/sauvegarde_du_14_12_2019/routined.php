<?php
session_start();

require_once('../class/Etablissement.php');
require_once('../class/Matiere.php');
require_once('../class/Teatcher.php');
require_once('../class/Classe.php');
require_once('../controller/functions.php');
$routine = new Etab();
$subject= new Matiere();
$teatcher=new Teatcher();
$classesT=new Classe();

if(isset($_POST['etape'])&&($_POST['etape']==1||$_POST['etape']==2))
{
//enregistrer et consigner un autre jour

  //recuperation des variables

  $sessioncourante=htmlspecialchars(addslashes($_POST['sessioncourante']));
  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $day=htmlspecialchars(addslashes($_POST['day']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $nbconcatroutines=htmlspecialchars(addslashes($_POST['nbconcatroutines']));
  $concatroutines=htmlspecialchars(addslashes($_POST['concatroutines']));

  $tabconcatroutines=explode("@",$concatroutines);

  //insertion dans la table routine

  for($i=0;$i<$nbconcatroutines;$i++)
  {

      $matiere=htmlspecialchars(addslashes($_POST['matiere'.$tabconcatroutines[$i]]));
      $heuredeb=htmlspecialchars(addslashes($_POST['heuredeb'.$tabconcatroutines[$i]]));
      $heurefin=htmlspecialchars(addslashes($_POST['heurefin'.$tabconcatroutines[$i]]));

      //insertion dans la base

      $routine->AddRoutineRow($classe,$codeEtab,$heuredeb,$heurefin,$matiere,$day,$sessioncourante);


  }

  if($_POST['etape']==1)
  {
    $_SESSION['user']['addprogra']="Routines ajouter avec succès";

    if($_SESSION['user']['profile'] == "Admin_globale") {

          //header("Location:../manager/index.php");
          header("Location:../manager/routinesadd.php");

      }else if($_SESSION['user']['profile'] == "Admin_locale") {

        header("Location:../locale/routinesadd.php");

        }
  }else if($_POST['etape']==2){

    $_SESSION['user']['addprogra']="Routines ajouter avec succès";

    if($_SESSION['user']['profile'] == "Admin_globale") {

          //header("Location:../manager/index.php");
          header("Location:../manager/routines.php?classe=".$classe);

      }else if($_SESSION['user']['profile'] == "Admin_locale") {

        header("Location:../locale/routines.php?classe=".$classe);

        }
  }

  //nous allons retourner sur la page precedente pour une nouvelle saisie







}



 ?>
