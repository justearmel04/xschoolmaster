<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$classe = new Classe();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //ajout du diplôme

  //recupération des variables
  $codeEtab=htmlspecialchars($_POST['libetab']);
  $libelleclasse=htmlspecialchars($_POST['classe']);
  $libellesession=htmlspecialchars($_POST['libellesession']);
  $montantscola=htmlspecialchars(addslashes($_POST['montantscola']));
  $etablissementType=htmlspecialchars($_POST['typeEtab']);
  // $section=htmlspecialchars(addslashes($_POST['section']));

  if($etablissementType==2)
  {
    $montantscolaaff=htmlspecialchars($_POST['montantscolaaff']);
    $classe->Addclassesupother($codeEtab,$libelleclasse,$libellesession,$montantscola,$montantscolaaff);

  }else {
    $classe->Addclassesup($codeEtab,$libelleclasse,$libellesession,$montantscola);
  }

  $_SESSION['user']['addclasseok']="Classe ajouté avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {
header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
  // header("Location:../manager/addclasses.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/classes.php");
      }else {
        header("Location:../locale/classes.php");
      }


      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }

  // $classe->Addclasse($libelleclasse,$codeEtab,$libellesession,$section);



}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //modification de la classes

  //$codeEtab=$_POST['libetab'];
  $codeEtab=htmlspecialchars($_POST['libetab']);
  //$classe=$_POST['classe'];
  $classe=htmlspecialchars($_POST['classe']);

  $idclasse=htmlspecialchars($_POST['idclasse']);

  //$etab->Updateclasses($classe,$codeEtab,$idclasse);
  //$classe->getAllclassesT();



  $classe->getAllclasses();

}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recuperation des variables



  $codeEtab=htmlspecialchars($_POST['libetab']);
  $classeid=htmlspecialchars($_POST['classeid']);
  $enseignantid=htmlspecialchars($_POST['enseignantid']);
  $session=htmlspecialchars($_POST['session']);


  //affecter l'enseignant

  $etab->AffectedEnseignant($codeEtab,$classeid,$enseignantid,$session);

  $_SESSION['user']['updateteaok']="Enseigner affecter avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

        //header("Location:../manager/index.php");
        header("Location:../manager/classes.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale") {

      header("Location:../locale/classes.php");

      }

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
//recuperation des variables
$classeEtab=htmlspecialchars($_POST['classe']);
$codeEtab=htmlspecialchars($_POST['libetab']);
$libellesession=htmlspecialchars($_POST['libellesession']);
$montantscola=htmlspecialchars($_POST['montantscola']);

$classe->Addclassecmr($classeEtab,$codeEtab,$libellesession,$montantscola);

$_SESSION['user']['addclasseok']="Classe ajouté avec succès";

if($_SESSION['user']['profile'] == "Admin_globale") {
header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
// header("Location:../manager/addclasses.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale") {
    if($_SESSION['user']['paysid']==4)
    {
      header("Location:../localecmr/classes.php");
    }else {
      header("Location:../locale/classes.php");
    }


    }else if($_SESSION['user']['profile'] == "Teatcher") {

          header("Location:../teatcher/index.php");

      }else if($_SESSION['user']['profile'] == "Student") {

            header("Location:../student/index.php");

        }else if($_SESSION['user']['profile'] == "Parent") {

              header("Location:../parent/index.php");

          }


}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
//recupertaion des variables

$classes=htmlspecialchars($_POST['classe']);
$codeEtab=htmlspecialchars($_POST['libetab']);
$libellesession=htmlspecialchars($_POST['libellesession']);
$typeEtab=htmlspecialchars($_POST['typeEtab']);
$montantinscrip=htmlspecialchars($_POST['montantinscrip']);
$montantscola=htmlspecialchars($_POST['montantscola']);
$montantAES=htmlspecialchars($_POST['montantAES']);
$montantMenscant=htmlspecialchars($_POST['montantMenscant']);
$montantTrimcant=htmlspecialchars($_POST['montantTrimcant']);
$montantAnncant=htmlspecialchars($_POST['montantAnncant']);
$cantineorder=htmlspecialchars($_POST['cantineorder']);
$typeclasse=htmlspecialchars($_POST['typeclasse']);
$montantreins=htmlspecialchars($_POST['montantreins']);

$classeid=$classe->AddclasseSingle($classes,$codeEtab,$libellesession,$montantinscrip,$montantscola,$montantAES,$typeclasse,$montantreins);

if($cantineorder==1)
{
//tous les champs de la cantine sont a saisir

//insertion dans la table fraiscolaire

$classe->AddAllfraiscantineMonth($montantMenscant,$classeid,$codeEtab,$libellesession,$cantineorder);

$classe->AddAllfraiscantineTrim($montantTrimcant,$classeid,$codeEtab,$libellesession,$cantineorder);

$classe->AddAllfraiscantineAnn($montantAnncant,$classeid,$codeEtab,$libellesession,$cantineorder);

$classe->AddAllfraisAES($montantAES,$classeid,$codeEtab,$libellesession,$cantineorder);


//$classe->AddAllfraiscolaire();

}else {
  //tous les champs de la cantine ne sont pas obligatoire

  if(strlen($montantMenscant)>0 && $montantMenscant>0)
  {
    $classe->AddAllfraiscantineMonth($montantMenscant,$classeid,$codeEtab,$libellesession,$cantineorder);
  }

  if(strlen($montantTrimcant)>0 && $montantTrimcant>0)
  {
    $classe->AddAllfraiscantineTrim($montantTrimcant,$classeid,$codeEtab,$libellesession,$cantineorder);
  }

  if(strlen($montantAnncant)>0 && $montantAnncant>0)
  {
    $classe->AddAllfraiscantineAnn($montantAnncant,$classeid,$codeEtab,$libellesession,$cantineorder);
  }
$classe->AddAllfraisAES($montantAES,$classeid,$codeEtab,$libellesession,$cantineorder);
}

$Etablibelle=$etab->getEtabLibellebyCodeEtab($codeEtab);

// $_SESSION['user']['addclasseok']="Une nouvelle Classe a été ajoutée avec succès";
$_SESSION['user']['addclasseok']=L::ClasseAddMessageSuccess;

if($_SESSION['user']['profile'] == "Admin_globale") {
header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
// header("Location:../manager/addclasses.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale") {
    if($_SESSION['user']['paysid']==4)
    {
      header("Location:../localecmr/classes.php");
    }else {
      header("Location:../locale".$Etablibelle."/classes.php");
    }


    }else if($_SESSION['user']['profile'] == "Teatcher") {

          header("Location:../teatcher/index.php");

      }else if($_SESSION['user']['profile'] == "Student") {

            header("Location:../student/index.php");

        }else if($_SESSION['user']['profile'] == "Parent") {

              header("Location:../parent/index.php");

          }



}

if(isset($_GET['etape'])&&($_GET['etape']==3))
{
  //il est question de la suppression  d'une classe

  $idclasse=htmlspecialchars(addslashes($_GET['compte']));

  $classe->suppressionClasse($idclasse);

  $_SESSION['user']['delclasseok']="Classe supprimé avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {
header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
  // header("Location:../manager/addclasses.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/classes.php");
      }else {
        header("Location:../locale/classes.php");
      }


      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }
}

?>
