<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Matiere.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Teatcher.php');
require_once('../controller/functions.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$teatcherschool=new Teatcher();
$classeschool = new Classe();
$etab=new Etab();
$matiere=new Matiere();
$localadminschool= new Localadmin();
if(isset($_POST['etape'])&&($_POST['etape']==1))
{
    //ajout d'un programme / syllabus

    //recuperation des variables

    $classe=htmlspecialchars(addslashes($_POST['classe']));
    $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
    $matiere=htmlspecialchars(addslashes($_POST['matiere']));
    $tabmatiere=explode("-",$matiere);
    $matiereid=$tabmatiere[0];
    $teatcherid=$tabmatiere[1];
    $descri=htmlspecialchars(addslashes($_POST['descri']));
    $sessionsyllabus=htmlspecialchars(addslashes($_POST['sessionsyllabus']));
    $addby=$_SESSION['user']['IdCompte'];
    $dateday=date("Y-m-d");

    //insertion dans la table syllabus et retour de l'id syllabus

    $idsyllabus=$etab->Addsyllabus($teatcherid,$classe,$matiereid,$sessionsyllabus,$descri,$addby,$dateday,$codeEtab);

    //insertion dans la table objectif

      $concatobjectif=htmlspecialchars(addslashes($_POST['concatobjectif']));
      $tabidobjectif=explode("@",$concatobjectif);
      $concatnbobjectif=htmlspecialchars(addslashes($_POST['concatnbobjectif']));

      for($i=0;$i<$concatnbobjectif;$i++)
      {
        $objectif= htmlspecialchars(addslashes($_POST['objectif_'.$tabidobjectif[$i]]));



        $etab->Addobjectisyllabus($idsyllabus,$objectif,$dateday);

      }

      //insertion dans la table theme

      $concatcontenu=htmlspecialchars(addslashes($_POST['concatcontenu']));
      $tabcontenu=explode("@",$concatcontenu);
      $concatnbcontenu=htmlspecialchars(addslashes($_POST['concatnbcontenu']));

      for($j=0;$j<$concatnbcontenu;$j++)
      {
        $contenu=htmlspecialchars(addslashes($_POST['theme_'.$tabcontenu[$j]]));

          //insertion dans la table theme

          $etab->Addthemesyllabus($idsyllabus,$contenu,$dateday);


      }
      //
      // //insertion dans la table requis
      //
      $concatrequis=htmlspecialchars(addslashes($_POST['concatrequis']));
      $tabconcatrequis=explode("@",$concatrequis);
      $concatnbrequis=htmlspecialchars(addslashes($_POST['concatnbrequis']));

      for($l=0;$l<$concatnbcontenu;$l++)
      {
        $requis=htmlspecialchars(addslashes($_POST['requis_'.$tabconcatrequis[$l]]));

          //insertion dans la table theme



          $etab->Addrequissyllabus($idsyllabus,$requis,$dateday);
      }

      // //insertion dans la table compétence
      //
      $concatcomp=htmlspecialchars(addslashes($_POST['concatcomp']));
      $tabconcatcomp=explode("@",$concatcomp);
      $concatnbcomp=htmlspecialchars(addslashes($_POST['concatnbcomp']));

      for($k=0;$k<$concatnbcomp;$k++)
      {
        $comp=htmlspecialchars(addslashes($_POST['comp_'.$tabconcatcomp[$k]]));

          //insertion dans la table theme

          $etab->Addcompsyllabus($idsyllabus,$comp,$dateday);
      }

      // //insertion dans la table doc
      //
      $concatdoc=htmlspecialchars(addslashes($_POST['concatdoc']));
      $tabconcatdoc=explode("@",$concatdoc);
      $concatnbdoc=htmlspecialchars(addslashes($_POST['concatnbdoc']));

      for($k=0;$k<$concatnbdoc;$k++)
      {
        $doc=htmlspecialchars(addslashes($_POST['doc_'.$tabconcatdoc[$k]]));

          //insertion dans la table doc



          $etab->Adddocsyllabus($idsyllabus,$doc,$dateday);
      }

      // //insertion dans la table document facultatif
      //
      $concatdocfac=htmlspecialchars(addslashes($_POST['concatdocfac']));
      $tabconcatdocfac=explode("@",$concatdocfac);
      $concatnbdocfac=htmlspecialchars(addslashes($_POST['concatnbdocfac']));

      for($k=0;$k<$concatnbdocfac;$k++)
      {
        $docfac=htmlspecialchars(addslashes($_POST['docfac_'.$tabconcatdocfac[$k]]));
        $facultatif=1;

          //insertion dans la table doc
          $etab->Adddocfacsyllabus($idsyllabus,$docfac,$facultatif,$dateday);


      }

      // //insertion dans la table calendar
      //
      //
      $concatcalendar=htmlspecialchars(addslashes($_POST['concatcalendar']));
      $tabconcatcalendar=explode("@",$concatcalendar);
      $concatnbcalandar=htmlspecialchars(addslashes($_POST['concatnbcalandar']));

      for($k=0;$k<$concatnbcalandar;$k++)
      {
        $dateCalandar=htmlspecialchars(addslashes($_POST['dateCalandar_'.$tabconcatcalendar[$k]]));
        $seanceCalandar=htmlspecialchars(addslashes($_POST['seanceCalandar_'.$tabconcatcalendar[$k]]));
        $contentCalandar=htmlspecialchars(addslashes($_POST['contentCalandar_'.$tabconcatcalendar[$k]]));
        $workCalandar=htmlspecialchars(addslashes($_POST['workCalandar_'.$tabconcatcalendar[$k]]));

        //insertion dans la table doc



          $etab->Addcalendarsyllabus($idsyllabus,dateFormat($dateCalandar),$seanceCalandar,$contentCalandar,$workCalandar,$dateday);
      }

      // //insertion dans la table evaluation
      //
      $concatevaluation=htmlspecialchars(addslashes($_POST['concatevaluation']));
      $tabconcatevaluation=explode("@",$concatevaluation);
      $concatnbevaluation=htmlspecialchars(addslashes($_POST['concatnbevaluation']));

      for($k=0;$k<$concatnbevaluation;$k++)
      {
        $dateEvaluation=htmlspecialchars(addslashes($_POST['dateEvaluation_'.$tabconcatevaluation[$k]]));
        $typeEvaluation=htmlspecialchars(addslashes($_POST['typeEvaluation_'.$tabconcatevaluation[$k]]));
        $competenceEvaluation=htmlspecialchars(addslashes($_POST['competenceEvaluation_'.$tabconcatevaluation[$k]]));
        $ponderationEvaluation=htmlspecialchars(addslashes($_POST['ponderationEvaluation_'.$tabconcatevaluation[$k]]));

        //insertion dans la table doc

        // echo dateFormat($dateEvaluation)."</br>".$concatevaluation."</br>";

          $etab->AddEvaluationsyllabus($idsyllabus,dateFormat($dateEvaluation),$typeEvaluation,$competenceEvaluation,$ponderationEvaluation,$dateday);
      }

      // //insertion dans la table regle_
      //
      $concatregl=htmlspecialchars(addslashes($_POST['concatregl']));
      $tabconcatregl=explode("@",$concatregl);
      $concatnbregl=htmlspecialchars(addslashes($_POST['concatnbregl']));

      for($k=0;$k<$concatnbregl;$k++)
      {
        $regle=htmlspecialchars(addslashes($_POST['regle_'.$tabconcatregl[$k]]));

        //insertion dans la table regle



          $etab->AddReglesyllabus($idsyllabus,$regle,$dateday);
      }

      // $_SESSION['user']['addprogra']="Programme académique ajouter avec succès";
      $_SESSION['user']['addprogra']=L::AddprogrammeMessageSuccess;


      if($_SESSION['user']['profile'] == "Admin_globale") {

            header("Location:../manager/programmes.php");

        }else if($_SESSION['user']['profile'] == "Admin_locale") {
          if($_SESSION['user']['paysid']==4)
          {
            header("Location:../localecmr/programmes.php");
          }else {
            header("Location:../locale/programmes.php");
          }


          }else if($_SESSION['user']['profile'] == "Teatcher") {

                header("Location:../teatcher/programmes.php");

            }


}

 ?>
