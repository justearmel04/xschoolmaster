<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
$classe = new Classe();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //ajout du diplôme

  //recupération des variables

  $codeEtab=htmlspecialchars(addslashes($_POST['libetab']));
  $libellesection=htmlspecialchars(addslashes($_POST['section']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
  $montantsection=htmlspecialchars(addslashes($_POST['montantsection']));
  $devise=htmlspecialchars(addslashes($_POST['devise']));
  $dateday=date("Y-m-d");
  $statut=1;

  $classe->AddSection($libellesection,$codeEtab,$libellesession,$montantsection,$devise,$dateday,$statut);



}if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recuperation des variables

  $section=htmlspecialchars(addslashes($_POST['section']));
  $codeEtab=htmlspecialchars(addslashes($_POST['libetab']));
  $idsection=htmlspecialchars(addslashes($_POST['idsection']));
  $montantsection=htmlspecialchars(addslashes($_POST['montantsection']));
  $session=htmlspecialchars(addslashes($_POST['libellesession']));

  $classe->UpdateSection($section,$montantsection,$codeEtab,$session,$idsection);

}

if(isset($_GET['etape'])&&($_GET['etape']==3))
{
  //recuperation des variables


  $idsection=htmlspecialchars(addslashes($_GET['compte']));
  $statut=0;
  $classe->DeletedSection($idsection,$statut);

}

?>
