<?php
session_start();
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Classe.php');
require_once('../class/Parent.php');
require_once('../class/Etablissement.php');
require_once('../controller/functions.php');
$student = new Student();
$sessionX= new Sessionacade();
$classeSchool=new Classe();
$etabs=new Etab();
$parentclass=new ParentX();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //ajouter un versement

  //recuperation des variables

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $studentid=htmlspecialchars(addslashes($_POST['student']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
  $modepaie=htmlspecialchars(addslashes($_POST['modepaie']));
  $montapayer=htmlspecialchars(addslashes($_POST['montapayer']));
  $montvers=htmlspecialchars(addslashes($_POST['montvers']));
  $montrest=htmlspecialchars(addslashes($_POST['montrest']));
  $devisepaie=htmlspecialchars(addslashes($_POST['devisepaie']));
  $datecrea=date("Y-m-d");
  $datepiste=date("Y-m-d");
  $controlepiste=0;
  $reglementpiste=1;
  $examenpiste=0;
  $actionpiste=1;
  $soldeancien=0;
  $libelleclasse=$classeSchool->getInfosofclassesbyId($classe,$libellesession);


  //verifier si l'inscription en  cours pour cette annés est solder

  $soldeinscript=$student->DetermineInscriptionSolder($classe,$codeEtab,$studentid,$libellesession);



    if($soldeinscript==0)
    {

      //nous allons determiner le montant restant à montapayer

      $nbversement=$student->DetermineNumberOfversement($codeEtab,$classe,$libellesession,$studentid);

      if($nbversement==0)
      {
        //le parent n'a pas encore fait de paiement alors nous allons recuperer le montant de la scolarite pour cette section
        $datavers=$classeSchool->getAllInformationsOfclassesByClasseId($classe,$libellesession);
        $tabdatavers=explode("*",$datavers);
        $etablissementType=$etabs->DetermineTypeEtab($codeEtab);

        if($etablissementType==2)
        {
          $profilestudent=$student->getStudentProfile($studentid);

          if($profilestudent==0)
          {
            //non affecté
          $soldeancien=$tabdatavers[4];
          }else if($profilestudent==1){
            // affecté
            $soldeancien=$tabdatavers[5];
          }

        }else {
          $soldeancien=$tabdatavers[4];
        }

      }else if($nbversement>0)
      {
        //nous avons au moins un versement alors nous allons chercher les informations du dernier versement

        $datavers=$student->SelectInformationsOfLastVersement($codeEtab,$classe,$libellesession,$studentid);
        $tabdatavers=explode("*",$datavers);
        $soldeancien=$tabdatavers[0];
      }

      // creation du code versement

      $codeVers =  "VER".date("Y") . date("m").strtoupper(substr($codeEtab, 0, 2));
      //ajout dans la table versement

      //insertion dans la piste



      $idversement=$student->AddVersement($codeVers,$datecrea,$modepaie,$montvers,$montrest,$classe,$studentid,$libellesession,$codeEtab,$devisepaie,$_SESSION['user']['IdCompte']);

      $newcodeVers=$codeVers.$idversement;
      $student->UpdateCodeversement($newcodeVers,$idversement,$libellesession,$codeEtab,$classe,$studentid);

//insertuin dana la piste
      $idpiste=$etabs->AddpisteForReglement($datepiste,$_SESSION['user']['IdCompte'],$classe,$codeEtab,$libelleclasse,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);

      //insertion dans la table pistereglement
      $fisrtreglement=1;
      $etabs->AddReglementPiste($idpiste,$fisrtreglement,$soldeancien,$montvers,$idversement,$studentid);
      //nous allons envoyer le mail aux parents pour dire que le versement à bien ete fait

      $donnees=$student->getEmailParentOfThisStudentByID($studentid,$libellesession);

      $tabdata=explode('*',$donnees);



      $emailparent=$tabdata[0];

      $nomEleve=$tabdata[1]." ".$tabdata[2];

      $classeName=$tabdata[3];

      $EtabName=$tabdata[4];

      $imageEtab=$tabdata[5];

      //envoi du mail au parent

      //$student->SendVersementsMaillerToParent($newcodeVers,$emailparent,$nomEleve,$classeName,$EtabName,$imageEtab,$montvers,$montrest,$modepaie,$datecrea,$devisepaie,$codeEtab);

      //verifie si le montant du solde restant est nulle ou pas

      if($montrest==0)
      {
        // nous allons mettre solder_inscription à 1

        $statesolde=1;

        $student->SolderInscription($statesolde,$studentid,$codeEtab,$classe,$libellesession);
      }



      $_SESSION['user']['addctrleok']="Versement effectué avec succès";

      if($_SESSION['user']['profile'] == "Admin_locale")

      {
        if($_SESSION['user']['paysid']==4)
        {
          header("Location:../localecmr/scolarites.php");
        }else {
          header("Location:../locale/scolarites.php");
        }


      }

    }else if($soldeinscript==1)
    {

    }


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //ajouter un versement

  //recuperation des variables

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $studentid=htmlspecialchars(addslashes($_POST['student']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
  $modepaie=htmlspecialchars(addslashes($_POST['modepaie']));
  $montapayer=htmlspecialchars(addslashes($_POST['montapayer']));
  $montvers=htmlspecialchars(addslashes($_POST['montvers']));
  $montrest=htmlspecialchars(addslashes($_POST['montrest']));
  $devisepaie=htmlspecialchars(addslashes($_POST['devisepaie']));
  $datecrea=date("Y-m-d");
  $datepiste=date("Y-m-d");
  $controlepiste=0;
  $reglementpiste=1;
  $examenpiste=0;
  $actionpiste=1;
  $soldeancien=0;
  $libelleclasse=$classeSchool->getInfosofclassesbyId($classe,$libellesession);


  //verifier si l'inscription en  cours pour cette annés est solder

  $soldeinscript=$student->DetermineInscriptionSolder($classe,$codeEtab,$studentid,$libellesession);

    if($soldeinscript==0)
    {

      //nous allons determiner le montant restant à montapayer

      $nbversement=$student->DetermineNumberOfversement($codeEtab,$classe,$libellesession,$studentid);

      if($nbversement==0)
      {
        //le parent n'a pas encore fait de paiement alors nous allons recuperer le montant de la scolarite pour cette section
        $datavers=$classeSchool->getAllInformationsOfclassesByClasseId($classe,$libellesession);
        $tabdatavers=explode("*",$datavers);

        $etablissementType=$etabs->DetermineTypeEtab($codeEtab);

        if($etablissementType==2)
        {
          $profilestudent=$student->getStudentProfile($studentid);

          if($profilestudent==0)
          {
            //non affecté
          $soldeancien=$tabdatavers[4];
          }else if($profilestudent==1){
            // affecté
            $soldeancien=$tabdatavers[5];
          }

        }else {
          $soldeancien=$tabdatavers[4];
        }


      }else if($nbversement>0)
      {
        //nous avons au moins un versement alors nous allons chercher les informations du dernier versement

        $datavers=$student->SelectInformationsOfLastVersement($codeEtab,$classe,$libellesession,$studentid);
        $tabdatavers=explode("*",$datavers);
        $soldeancien=$tabdatavers[0];
      }

      // creation du code versement

      $codeVers =  "VER".date("Y") . date("m").strtoupper(substr($codeEtab, 0, 2));
      //ajout dans la table versement

      //insertion dans la piste



      $idversement=$student->AddVersement($codeVers,$datecrea,$modepaie,$montvers,$montrest,$classe,$studentid,$libellesession,$codeEtab,$devisepaie,$_SESSION['user']['IdCompte']);

      $newcodeVers=$codeVers.$idversement;
      $student->UpdateCodeversement($newcodeVers,$idversement,$libellesession,$codeEtab,$classe,$studentid);

      //insertuin dana la piste
      $idpiste=$etabs->AddpisteForReglement($datepiste,$_SESSION['user']['IdCompte'],$classe,$codeEtab,$libelleclasse,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);

      //insertion dans la table pistereglement
      $fisrtreglement=1;
      $etabs->AddReglementPiste($idpiste,$fisrtreglement,$soldeancien,$montvers,$idversement,$studentid);
      //nous allons envoyer le mail aux parents pour dire que le versement à bien ete fait

      //nous allons recuperer les numeros de telephone des parents de cet elève
      //recuperer l'indicatif du pays
      $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);
      $infosparents=$parentclass->ParentInfostudent($studentid);
      $mobilephones="";

      foreach ($infosparents as $value):
        $mobilephones=$mobilephones.$indicatifEtab.$value->tel_parent.",";
      endforeach;


      $donnees=$student->getEmailParentOfThisStudentByID($studentid,$libellesession);

      $tabdata=explode('*',$donnees);
      $emailparent=$tabdata[0];
      $nomEleve=$tabdata[1]." ".$tabdata[2];
      $classeName=$tabdata[3];
      $EtabName=$tabdata[4];
      $imageEtab=$tabdata[5];

      //envoi du mail au parent

      //$student->SendVersementsMaillerToParent($newcodeVers,$emailparent,$nomEleve,$classeName,$EtabName,$imageEtab,$montvers,$montrest,$modepaie,$datecrea,$devisepaie,$codeEtab);

      //envoi du sms au parent
        //sms mis en commentaire
      $etabs->paiementsmssender($mobilephones,$newcodeVers,$emailparent,$nomEleve,$classeName,$EtabName,$imageEtab,$montvers,$montrest,$modepaie,$datecrea,$devisepaie,$codeEtab);

      //verifie si le montant du solde restant est nulle ou pas

      if($montrest==0)
      {
        // nous allons mettre solder_inscription à 1

        $statesolde=1;

        $student->SolderInscription($statesolde,$studentid,$codeEtab,$classe,$libellesession);
      }



      $_SESSION['user']['addctrleok']="Versement effectué avec succès";

      if($_SESSION['user']['profile'] == "Admin_locale")

      {
        if($_SESSION['user']['paysid']==4)
        {
          header("Location:../localecmr/scolaritestudent.php?student=".$studentid."&classe=".$classe);
        }else {
          header("Location:../locale/scolaritestudent.php?student=".$studentid."&classe=".$classe);
        }


      }

    }else if($soldeinscript==1)
    {

    }


}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  // nous sommes dans le cas du paiement des frais de scolarites ou d'inscription

  //recuperation des variables

  $classe=htmlspecialchars($_POST['classe']);
  $studentid=htmlspecialchars($_POST['student']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $libellesession=htmlspecialchars($_POST['libellesession']);
  $modepaie=htmlspecialchars($_POST['modepaie']);
  $montapayer=htmlspecialchars($_POST['montapayer']);
  $montvers=htmlspecialchars($_POST['montvers']);
  $montrest=htmlspecialchars($_POST['montrest']);
  $devisepaie=htmlspecialchars($_POST['devisepaie']);
  $motifpaie=htmlspecialchars($_POST['motifpaie']);
  $datecrea=date("Y-m-d");
  $datepiste=date("Y-m-d");
  $controlepiste=0;
  $reglementpiste=1;
  $examenpiste=0;
  $actionpiste=1;
  $soldeancien=$montapayer;
  $libelleclasse=$classeSchool->getInfosofclassesbyId($classe,$libellesession);
  $libellemotif="";

  //verifier si l'inscription ou la scolarité en  cours pour cette annés est solder

  if($motifpaie==1)
  {
    //nous sommes dans un cas d'inscription

    $libellemotif="INSCRIPTIONS";


  }else if($motifpaie==2) {
    // nous sommes dans le cas de scolarites

    $libellemotif="SCOLARITES";
  }

  $codeVers =  "VER".date("Y") . date("m").strtoupper(substr($codeEtab, 0, 2));
  //ajout dans la table versement

  //insertion dans la piste

  $idversement=$student->AddVersementByMotif($codeVers,$datecrea,$modepaie,$montvers,$montrest,$classe,$studentid,$libellesession,$codeEtab,$devisepaie,$_SESSION['user']['IdCompte'],$libellemotif);

  $newcodeVers=$codeVers.$idversement;

  $student->UpdateCodeversement($newcodeVers,$idversement,$libellesession,$codeEtab,$classe,$studentid);

  //insertuin dana la piste
  $idpiste=$etabs->AddpisteForReglement($datepiste,$_SESSION['user']['IdCompte'],$classe,$codeEtab,$libelleclasse,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);

  //insertion dans la table pistereglement
  $fisrtreglement=1;
  $etabs->AddReglementPiste($idpiste,$fisrtreglement,$soldeancien,$montvers,$idversement,$studentid);
  //nous allons envoyer le mail aux parents pour dire que le versement à bien ete fait

  //nous allons recuperer les numeros de telephone des parents de cet elève
  //recuperer l'indicatif du pays
  $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);
  $infosparents=$parentclass->ParentInfostudent($studentid);
  $mobilephones="";

  foreach ($infosparents as $value):
    $mobilephones=$mobilephones.$indicatifEtab.$value->tel_parent.",";
  endforeach;


  $donnees=$student->getEmailParentOfThisStudentByID($studentid,$libellesession);

  $tabdata=explode('*',$donnees);
  $emailparent=$tabdata[0];
  $nomEleve=$tabdata[1]." ".$tabdata[2];
  $classeName=$tabdata[3];
  $EtabName=$tabdata[4];
  $imageEtab=$tabdata[5];

  //envoi du mail au parent

  //$student->SendVersementsMaillerToParent($newcodeVers,$emailparent,$nomEleve,$classeName,$EtabName,$imageEtab,$montvers,$montrest,$modepaie,$datecrea,$devisepaie,$codeEtab);

  //envoi du sms au parent
    //sms mis en commentaire
  $etabs->paiementsmssender($mobilephones,$newcodeVers,$emailparent,$nomEleve,$classeName,$EtabName,$imageEtab,$montvers,$montrest,$modepaie,$datecrea,$devisepaie,$codeEtab);

  if($montrest==0)
  {
    // nous allons mettre solder_inscription à 1

    $statesolde=1;

    if($motifpaie==1)
    {
      //nous sommes dans un cas d'inscription

      // $libellemotif="INSCRIPTIONS";
      // $student->SolderInscription($statesolde,$studentid,$codeEtab,$classe,$libellesession);

      $student->SolderInscriptionVersement($statesolde,$studentid,$codeEtab,$classe,$libellesession,$libellemotif,$idversement);


    }else if($motifpaie==2) {
      // nous sommes dans le cas de scolarites

      // $libellemotif="SCOLARITES";

      $student->SolderScolariteVersement($statesolde,$studentid,$codeEtab,$classe,$libellesession,$libellemotif,$idversement);
    }




  }



  $_SESSION['user']['addctrleok']="Un Versement a été effectué avec succès";

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

  if($_SESSION['user']['profile'] == "Admin_locale")

  {
    // header("Location:../locale/scolaritestudent.php?student=".$studentid."&classe=".$classe);

    header("Location:../locale".$libelleEtab."/scolaritestudent.php?student=".$studentid."&classe=".$classe);


  }





}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //cas de paiement des frais de cantine

  $classe=htmlspecialchars($_POST['classe']);
  $studentid=htmlspecialchars($_POST['student']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $libellesession=htmlspecialchars($_POST['libellesession']);
  $modepaie=htmlspecialchars($_POST['modepaie']);
  $montapayer=htmlspecialchars($_POST['montapayer']);
  $montvers=htmlspecialchars($_POST['montvers']);
  $montrest=htmlspecialchars($_POST['montrest']);
  $devisepaie=htmlspecialchars($_POST['devisepaie']);
  $motifpaie=htmlspecialchars($_POST['motifpaie']);
  $datecrea=date("Y-m-d");
  $datepiste=date("Y-m-d");
  $controlepiste=0;
  $reglementpiste=1;
  $examenpiste=0;
  $actionpiste=1;
  $soldeancien=0;
  $montrest=0;
  $montvers=$montapayer;
  $libelleclasse=$classeSchool->getInfosofclassesbyId($classe,$libellesession);
  $libellemotif="CANTINES";
  $months="";

  $codeVers =  "VER".date("Y") . date("m").strtoupper(substr($codeEtab, 0, 2));

  foreach($_POST['monthpaie'] as $valeur)
  {
   $moischoice=$valeur;
   $indicemois=obtenirindiceMois($moischoice);

   // echo $codeVers." - ".$datecrea." - ".$modepaie." - ".$montvers." - ".$classe." - ".$studentid." - ".$libellesession." - ".$codeEtab." - ".$devisepaie." - ".$_SESSION['user']['IdCompte']." - ".$libellemotif." - ".$moischoice;

   $idversement=$student->AddVersementByMotifCantines($codeVers,$datecrea,$modepaie,$montvers,$classe,$studentid,$libellesession,$codeEtab,$devisepaie,$_SESSION['user']['IdCompte'],$libellemotif,$moischoice,$indicemois);
   //
   $newcodeVers=$codeVers.$idversement;

    $student->UpdateCodeversement($newcodeVers,$idversement,$libellesession,$codeEtab,$classe,$studentid);

    //insertuin dana la piste
    $idpiste=$etabs->AddpisteForReglement($datepiste,$_SESSION['user']['IdCompte'],$classe,$codeEtab,$libelleclasse,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);

    //insertion dans la table pistereglement
    $fisrtreglement=1;
    $etabs->AddReglementPiste($idpiste,$fisrtreglement,$soldeancien,$montvers,$idversement,$studentid);

    //nous allons envoyer le mail aux parents pour dire que le versement à bien ete fait

    //nous allons recuperer les numeros de telephone des parents de cet elève
    //recuperer l'indicatif du pays
    $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);
    $infosparents=$parentclass->ParentInfostudent($studentid);
    $mobilephones="";

    foreach ($infosparents as $value):
      $mobilephones=$mobilephones.$indicatifEtab.$value->tel_parent.",";
    endforeach;


    $donnees=$student->getEmailParentOfThisStudentByID($studentid,$libellesession);

    $tabdata=explode('*',$donnees);
    $emailparent=$tabdata[0];
    $nomEleve=$tabdata[1]." ".$tabdata[2];
    $classeName=$tabdata[3];
    $EtabName=$tabdata[4];
    $imageEtab=$tabdata[5];


    $etabs->paiementsmssender($mobilephones,$newcodeVers,$emailparent,$nomEleve,$classeName,$EtabName,$imageEtab,$montvers,$montrest,$modepaie,$datecrea,$devisepaie,$codeEtab);



  }


  $_SESSION['user']['addctrleok']="Un Versement a été effectué avec succès";

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

  if($_SESSION['user']['profile'] == "Admin_locale")

  {


    header("Location:../locale".$libelleEtab."/scolaritestudent.php?student=".$studentid."&classe=".$classe);


  }



}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //cas des activites AES

  $classe=htmlspecialchars($_POST['classe']);
  $studentid=htmlspecialchars($_POST['student']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $libellesession=htmlspecialchars($_POST['libellesession']);
  $modepaie=htmlspecialchars($_POST['modepaie']);
  $montapayer=htmlspecialchars($_POST['montapayer']);
  $montvers=htmlspecialchars($_POST['montvers']);
  $montrest=htmlspecialchars($_POST['montrest']);
  $devisepaie=htmlspecialchars($_POST['devisepaie']);
  $motifpaie=htmlspecialchars($_POST['motifpaie']);
  $datecrea=date("Y-m-d");
  $datepiste=date("Y-m-d");
  $controlepiste=0;
  $reglementpiste=1;
  $examenpiste=0;
  $actionpiste=1;
  $soldeancien=0;
  $montrest=0;
  $montvers=$montapayer;
  $libelleclasse=$classeSchool->getInfosofclassesbyId($classe,$libellesession);
  $libellemotif="AES";
  $months="";

  $codeVers =  "VER".date("Y") . date("m").strtoupper(substr($codeEtab, 0, 2));

  $idversement=$student->AddVersementByMotifAES($codeVers,$datecrea,$modepaie,$montvers,$classe,$studentid,$libellesession,$codeEtab,$devisepaie,$_SESSION['user']['IdCompte'],$libellemotif);
  //
  $newcodeVers=$codeVers.$idversement;

   $student->UpdateCodeversement($newcodeVers,$idversement,$libellesession,$codeEtab,$classe,$studentid);

   //insertuin dana la piste
   $idpiste=$etabs->AddpisteForReglement($datepiste,$_SESSION['user']['IdCompte'],$classe,$codeEtab,$libelleclasse,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);

   //insertion dans la table pistereglement
   $fisrtreglement=1;
   $etabs->AddReglementPiste($idpiste,$fisrtreglement,$soldeancien,$montvers,$idversement,$studentid);

   //nous allons envoyer le mail aux parents pour dire que le versement à bien ete fait

   //nous allons recuperer les numeros de telephone des parents de cet elève
   //recuperer l'indicatif du pays
   $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);
   $infosparents=$parentclass->ParentInfostudent($studentid);
   $mobilephones="";

   foreach ($infosparents as $value):
     $mobilephones=$mobilephones.$indicatifEtab.$value->tel_parent.",";
   endforeach;


   $donnees=$student->getEmailParentOfThisStudentByID($studentid,$libellesession);

   $tabdata=explode('*',$donnees);
   $emailparent=$tabdata[0];
   $nomEleve=$tabdata[1]." ".$tabdata[2];
   $classeName=$tabdata[3];
   $EtabName=$tabdata[4];
   $imageEtab=$tabdata[5];


   $etabs->paiementsmssender($mobilephones,$newcodeVers,$emailparent,$nomEleve,$classeName,$EtabName,$imageEtab,$montvers,$montrest,$modepaie,$datecrea,$devisepaie,$codeEtab);


   $_SESSION['user']['addctrleok']="Un Versement a été effectué avec succès";

   $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

   if($_SESSION['user']['profile'] == "Admin_locale")

   {


     header("Location:../locale".$libelleEtab."/scolaritestudent.php?student=".$studentid."&classe=".$classe);


   }


}
 ?>
