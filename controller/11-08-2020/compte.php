<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$classe = new Classe();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //Modification du compte pour un etablissement mixte

  //recupération des variables
  $loginTea=htmlspecialchars($_POST['loginTea']);
  $passTea=htmlspecialchars($_POST['passTea']);
  $idcompte=htmlspecialchars($_POST['idcompte']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);

  $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);

  // $_SESSION['user']['addteaok']="Les paramètres du compte ont bien été modifiés avec succès";
  $_SESSION['user']['addteaok']=L::ComptesAddMessageSuccess;

  $etab->UpdateStudentAccountCnx($loginTea,$passTea,$idcompte);

  if($_SESSION['user']['profile'] == "Admin_globale") {

  header("Location:../manager/adddiplomes.php?compte=".$idcompte);
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/adddiplomes.php?compte=".$idcompte);
      }else {

        $etablissementType=$etab->DetermineTypeEtab($codeEtab);

        if($etablissementType==5)
        {
          header("Location:../locale".$libelleEtab."/detailsstudent.php?compte=".$idcompte);
        }else {
          header("Location:../locale/detailsstudent.php?compte=".$idcompte);
        }


        // header("Location:../locale/adddiplomes.php?compte=".$idcompte);
      }


    } else if($_SESSION['user']['profile'] == "Student")
    {

    }


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //Modification du compte pour un etablissement mixte

  //recupération des variables
  $loginTea=htmlspecialchars($_POST['loginTea']);
  $passTea=htmlspecialchars($_POST['passTea']);
  $idcompte=htmlspecialchars($_POST['idcompte']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);



  $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);

  // $_SESSION['user']['addteaok']="Les paramètres du compte ont bien été modifiés avec succès";
  $_SESSION['user']['addteaok']=L::ComptesAddMessageSuccess;

  $etab->UpdateStudentAccountCnx($loginTea,$passTea,$idcompte);

  if($_SESSION['user']['profile'] == "Admin_globale") {

  header("Location:../manager/adddiplomes.php?compte=".$idcompte);
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/adddiplomes.php?compte=".$idcompte);
      }else {

        $etablissementType=$etab->DetermineTypeEtab($codeEtab);

        if($etablissementType==5)
        {
          header("Location:../locale".$libelleEtab."/detailslocal.php?compte=".$idcompte);
        }else {
          header("Location:../locale/detailslocal.php?compte=".$idcompte);
        }


        // header("Location:../locale/adddiplomes.php?compte=".$idcompte);
      }


      }


}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //Modification du compte pour un etablissement mixte

  //recupération des variables
  $loginTea=htmlspecialchars($_POST['loginTea']);
  $passTea=htmlspecialchars($_POST['passTea']);
  $idcompte=htmlspecialchars($_POST['idcompte']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);



  $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);

  // $_SESSION['user']['addteaok']="Les paramètres du compte ont bien été modifiés avec succès";

  $_SESSION['user']['addteaok']=L::ComptesAddMessageSuccess;

  $etab->UpdateStudentAccountCnx($loginTea,$passTea,$idcompte);

  if($_SESSION['user']['profile'] == "Admin_globale") {

  // header("Location:../manager/adddiplomes.php?compte=".$idcompte);
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/adddiplomes.php?compte=".$idcompte);
      }else {

        $etablissementType=$etab->DetermineTypeEtab($codeEtab);

        if($etablissementType==5)
        {
          header("Location:../locale".$libelleEtab."/detailsparents.php?compte=".$idcompte);
        }else {
          header("Location:../locale/detailsparents.php?compte=".$idcompte);
        }


        // header("Location:../locale/adddiplomes.php?compte=".$idcompte);
      }


    }else if($_SESSION['user']['profile'] == "Parent")
    {
        // header("Location:../locale/detailsparents.php?compte=".$idcompte);
    }


}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //Modification du compte pour un etablissement mixte

  //recupération des variables
  $loginTea=htmlspecialchars($_POST['loginTea']);
  $passTea=htmlspecialchars($_POST['passTea']);
  $idcompte=htmlspecialchars($_POST['idcompte']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);



  $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);

  // $_SESSION['user']['addteaok']="Le profile a  été modifié avec succès";
  $_SESSION['user']['addteaok']=L::ProfileAddMessageSuccess;

  $etab->UpdateStudentAccountCnx($loginTea,$passTea,$idcompte);

  if($_SESSION['user']['profile'] == "Admin_globale") {

  // header("Location:../manager/adddiplomes.php?compte=".$idcompte);
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/adddiplomes.php?compte=".$idcompte);
      }else {

        $etablissementType=$etab->DetermineTypeEtab($codeEtab);

        if($etablissementType==5)
        {
          header("Location:../locale".$libelleEtab."/profile.php");
        }else {
          header("Location:../locale/profile.php");
        }


        // header("Location:../locale/adddiplomes.php?compte=".$idcompte);
      }


    }else if($_SESSION['user']['profile'] == "Parent")
    {
        // header("Location:../locale/detailsparents.php?compte=".$idcompte);
    }


}

?>
