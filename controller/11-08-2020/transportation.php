<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../controller/functions.php');

$classe = new Classe();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //ajout d'un controle


  $fraismensuel=htmlspecialchars($_POST['fraismensuel']);
  $fraistrimes=htmlspecialchars($_POST['fraistrimes']);
  $fraisannuel=htmlspecialchars($_POST['fraisannuel']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $devises=htmlspecialchars($_POST['devises']);

  $mensualtype="MENSUEL";
  $trimestrialtype="TRIMESTRIEL";
  $annualtype="ANNUEL";
  $dateday=date("Y-m-d");

  $idcompte=$_SESSION['user']['IdCompte'];

  // echo $idcompte;

  //insertion dans la table transportations

  $etab->AddMensualTransportation($fraismensuel,$devises,$codeEtab,$sessionEtab,$mensualtype,$idcompte,$dateday);
  $etab->AddTrimestrialTransportation($fraistrimes,$devises,$codeEtab,$sessionEtab,$trimestrialtype,$idcompte,$dateday);
  $etab->AddannualTransportation($fraisannuel,$devises,$codeEtab,$sessionEtab,$annualtype,$idcompte,$dateday);

  $_SESSION['user']['addctrleok']=L::TransportationFeesAddedSuccessfully;

  $etablissementType=$etab->DetermineTypeEtab($codeEtab);
  $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);

  if($etablissementType==5)
  {

  header("Location:../locale".$libelleEtab."/transportations.php");
  }else {
    header("Location:../locale/transportations.php");
  }




}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{

  $typefrais=htmlspecialchars($_POST['typefrais']);
  $transid=htmlspecialchars($_POST['transid']);
  $codeEtab=htmlspecialchars($_POST['codeEtabfrais']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtabfrais']);
  $montfrais=htmlspecialchars($_POST['montfrais']);
  $devisefrais=htmlspecialchars($_POST['devisefrais']);

  $etab->UpdateTransportations($montfrais,$devisefrais,$transid,$typefrais,$codeEtab,$sessionEtab);

  $_SESSION['user']['addctrleok']=L::TransportationFeesModSuccessfully;

  $etablissementType=$etab->DetermineTypeEtab($codeEtab);
  $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);

  if($etablissementType==5)
  {

  header("Location:../locale".$libelleEtab."/transportations.php");
  }else {
    header("Location:../locale/transportations.php");
  }




}


?>
