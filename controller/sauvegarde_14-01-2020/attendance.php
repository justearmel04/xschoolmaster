<?php

session_start();

require_once('../controller/functions.php');

require_once('../class/Student.php');

require_once('../class/Classe.php');

require_once('../class/Matiere.php');

require_once('../class/Etablissement.php');

$classe = new Classe();

$student=new Student();

$matiere=new Matiere();

$etabs=new Etab();



if(isset($_POST['etape'])&&($_POST['etape']==1))

{



  //recupération des variables

  $allpresent=htmlspecialchars(addslashes($_POST['allpresent']));

  $classeId=htmlspecialchars(addslashes($_POST['classeId']));

  $datePresence=htmlspecialchars(addslashes($_POST['datePresence']));

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));

  $profid=htmlspecialchars(addslashes($_POST['profid']));

  $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));

//format date 14/08/2019 actuel

  $studentmat=htmlspecialchars(addslashes($_POST['studentmat']));

  $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));

  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));



  $tanmatstudent=explode("*",$studentmat);



  $newdate=dateFormat($datePresence);



  $tabdate=explode("/",$datePresence);



  $libellematiere=$matiere->getMatiereLibelleByIdMat($matiereid,$codeEtab);



 $jour=$tabdate[0];

 $mois=$tabdate[1];

 $annee=$tabdate[2];



 $num = cal_days_in_month(CAL_GREGORIAN, retranscrireMois($mois), $annee);





 //recuperer le mois en lettre

 $libellemois=obtenirLibelleMois($mois);


$phonenumber="";
//recuperons l'indicatif du pays

 $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);


  //insertion dans la tables présences



  for ($i=1;$i<$nbstudent;$i++)

  {

      $statut=$_POST["optradio".$tanmatstudent[$i]];

      $matricule=$tanmatstudent[$i];



      if($statut==0)

      {

        //envoi de mail au parent de cet elève et les infos de l'eleve

        //nous allons verifier si le nombre de parent qui ont au moins un mail

        //nous allons detrminer idcompte de l'eleve

        $idcompteparent=$student->getstudentcompteid($matricule);

        $nbparentmail=$student->getNbofparentmail($idcompteparent);

        // echo $nbparentmail;

        if($nbparentmail>0)
        {

          //nous allons recuperer la liste des mails parents

          $donnees=$student->getEmailParentOfThisStudentBoucle($idcompteparent);
          // echo $idcompteparent;

          $studentDatas=$student->getAllInformationsOfStudent($idcompteparent,$libellesession);
          $studentData=explode("*",$studentDatas);
          $nomEleve=$studentData[2]." ".$studentData[3];
          $classeName=$studentData[9];

          // $tabdata=explode('*',$donnees);
          // $emailparent=$tabdata[0];
          // $nomEleve=$tabdata[1]." ".$tabdata[2];
          // $classeName=$tabdata[3];
          $EtabName=$etabs->getEtabLibellebyCodeEtab($codeEtab);
          $imageEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
          // $EtabName=$tabdata[4];
          // $imageEtab=$tabdata[5];

          //creation des variables de session
         //recuperer la date l'heure de debut et fin du cours

         foreach ($donnees as $value):
           $emailparent=$value->email_parent;
           $phonenumber=$indicatifEtab.$value->tel_parent;

           // echo $emailparent;
           $routinesdata=$etabs->getHoursOfSubject($classeId,$matiereid,$profid,$codeEtab);
           $tabroutine=explode("*",$routinesdata);
           $debutHours=$tabroutine[0];
           $finHours=$tabroutine[1];
           $msg=$student->AbsenceParentMailler($emailparent,$EtabName,$nomEleve,$classeName,$libellematiere,$debutHours,$finHours,$codeEtab,$imageEtab);
           $etabs->Attendancesmssending($phonenumber,$emailparent,$EtabName,$nomEleve,$classeName,$libellematiere,$debutHours,$finHours,$codeEtab,$imageEtab);
         endforeach;


        }else
        {
          // code...
        }






      }



      //echo $msg;



      //insertion dans la table présence



        $student->AddAttendance($newdate,$libellemois,$annee,$classeId,$matricule,$statut,$num,$matiereid,$profid,$codeEtab,$libellesession);

  }



//     //code etablissement de la classe
//
     $codeEtabchoolclasse=$classe->getCodeEtabOfClassesByClasseId($classeId);
//
// //

  $_SESSION['user']['addattendailyok']="Présence classe ajouter avec succès";

//

   if($_SESSION['user']['profile'] == "Admin_globale")

  {

header("Location:../manager/dailyattendance.php?codeEtab=".$codeEtab);

  }else if($_SESSION['user']['profile'] == "Admin_locale")

   {
     if($_SESSION['user']['paysid']==4)
     {
       header("Location:../localecmr/dailyattendance.php");
     }else {
       header("Location:../locale/dailyattendance.php");
     }


   }else if($_SESSION['user']['profile'] == "Teatcher")

   {

 header("Location:../teatcher/dailyattendance.php?classe=".$classeId."&codeEtab=".$codeEtab);

   }





}



?>
