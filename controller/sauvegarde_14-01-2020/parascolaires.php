<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
$classe = new Classe();
$etabs=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recuperation des variables

  $emailvalue=htmlspecialchars(addslashes($_POST['emailvalue']));
  $smsvalue=htmlspecialchars(addslashes($_POST['smsvalue']));
  $typesess=htmlspecialchars(addslashes($_POST['typesess']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $dataactivite=htmlspecialchars(addslashes($_POST['typeactivite']));
  $othertype="";
  $tabactivite=explode("-",$dataactivite);
  $typeactivite=$tabactivite[0];
  if($typeactivite==6)
  {
    $othertype=htmlspecialchars(addslashes($_POST['othertype']));
  }
  $denomination=htmlspecialchars(addslashes($_POST['denomination']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
  $gratuitcheck=htmlspecialchars(addslashes($_POST['gratuitcheck']));
  $paiecheck=htmlspecialchars(addslashes($_POST['paiecheck']));
  $respoactivite=htmlspecialchars(addslashes($_POST['respoactivite']));
  $contactrespo=htmlspecialchars(addslashes($_POST['contactrespo']));
  $locationactivite=htmlspecialchars(addslashes($_POST['locationactivite']));
  $descripactivite=htmlspecialchars(addslashes($_POST['descripactivite']));
  $datedeb=htmlspecialchars(addslashes($_POST['datedeb']));
  $datefin=htmlspecialchars(addslashes($_POST['datefin']));
  $objet_msg=8;
  $other="Activité parascolaire";
  $other=htmlspecialchars(addslashes($other));
  $useradd=$_SESSION['user']['IdCompte'];


  //information date de debut
  $tabdatedeb=explode(" ",$datedeb);
  $heurededeb=$tabdatedeb[1];
  $datededeb=$tabdatedeb[0];

    //information date de fin
  $tabdatefin=explode(" ",$datefin);
  $heuredefin=$tabdatefin[1];
  $datedefin=$tabdatefin[0];

  // echo $datededeb."</br>".$datedefin."</br>".$heurededeb."</br>".$heuredefin;



  $destinataires="Parent-";
  $studentSchool="";
  $statutNotif=0;
  $dateday=date("Y-m-d");
  $classes="";
  $desti=htmlspecialchars(addslashes($_POST['destinataires']));
  $paranotif=1;
  $statutpara=1;



  foreach($_POST['classeEtab'] as $valeur)
  {
   $classes=$classes.$valeur.'-';

  }

  if($desti==1)
  {
     $precis=0;

     if($paiecheck==1)
     {
       //activité payante

       $montantAct=htmlspecialchars(addslashes($_POST['montantAct']));



       //activite payante et envoie au parent de tous les eleves des classes selectionnes

       //ajouter dans la table $typesess

       $idmessage=$etabs->AddparascolaireMessagePayantAll($objet_msg,$destinataires,$classes,$precis,$paranotif,$statutpara,$dateday,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$useradd);

       // $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin);

       $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin,$typesess,$emailvalue,$smsvalue,$othertype);



     }else if($paiecheck==0){
       //activité non payante
       $montantAct=0;


          //activite non payante et envoie au parent de tous les eleves des classes selectionnes

      $idmessage=$etabs->AddparascolaireMessageNoPayantAll($objet_msg,$destinataires,$classes,$precis,$paranotif,$statutpara,$dateday,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$useradd);


      // $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin);

      $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin,$typesess,$emailvalue,$smsvalue,$othertype);

     }


  }else if($desti==2)
  {
    // echo "destinataires egale à 2";
    $precis=1;

    foreach($_POST['eleves'] as $valeur)
   {
     $studentSchool=$studentSchool.$valeur.'-';

   }


   if($paiecheck==1)
   {
     //activité payante

     $montantAct=htmlspecialchars(addslashes($_POST['montantAct']));

        //activite payante et envoie au parent de tous les eleves selectionnes
        //
        $idmessage=$etabs->AddparascolaireMessageToStudentselectParent($objet_msg,$destinataires,$classes,$precis,$studentSchool,$paranotif,$statutpara,$dateday,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$useradd);
        // $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin);

        $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin,$typesess,$emailvalue,$smsvalue,$othertype);


   }else if($paiecheck==0){
     //activité non payante

     //activite payante et envoie au parent de tous les eleves selectionnes $typesess

     $montantAct=0;

     $idmessage=$etabs->AddparascolaireMessageToStudentselectParentNopay($objet_msg,$destinataires,$classes,$precis,$studentSchool,$paranotif,$statutpara,$dateday,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$useradd);
     // $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin);

     $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin,$typesess,$emailvalue,$smsvalue,$othertype);


   }


  }

  //insertion des interet de l'activite

    $concatinteret=htmlspecialchars(addslashes($_POST['concatinteret']));
    $concatnbinteret=htmlspecialchars(addslashes($_POST['concatnbinteret']));
    $tabconcatinteret=explode("@",$concatinteret);


    for($j=0;$j<$concatnbinteret;$j++)
    {
      $etabs->AddInteretactivite($idactivity,htmlspecialchars(addslashes($_POST['interet_'.$tabconcatinteret[$j]])));
    }


    $_SESSION['user']['updateteaok']="Activité parascolaire ajouter avec succès";
    //
    if($_SESSION['user']['profile'] == "Admin_globale") {

          //header("Location:../manager/index.php");
          header("Location:../manager/allparascolaires.php");

      }else if($_SESSION['user']['profile'] == "Admin_locale") {

        if($_SESSION['user']['paysid']==4)
        {
          header("Location:../localecmr/allparascolaires.php");
        }else {
          header("Location:../locale/allparascolaires.php");
        }


        }


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recuperation des variables


    $emailvalue=htmlspecialchars(addslashes($_POST['emailvalue']));
    $smsvalue=htmlspecialchars(addslashes($_POST['smsvalue']));
    $eleves="";
    // $typesess=htmlspecialchars(addslashes($_POST['typesess']));
    $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
    // $typeactivite=htmlspecialchars(addslashes($_POST['typeactivite']));
    $denomination=htmlspecialchars(addslashes($_POST['denomination']));
    $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
    $gratuitcheck=htmlspecialchars(addslashes($_POST['gratuitcheck']));
    $paiecheck=htmlspecialchars(addslashes($_POST['paiecheck']));
    $respoactivite=htmlspecialchars(addslashes($_POST['respoactivite']));
    $contactrespo=htmlspecialchars(addslashes($_POST['contactrespo']));
    $locationactivite=htmlspecialchars(addslashes($_POST['locationactivite']));
    $descripactivite=htmlspecialchars(addslashes($_POST['descripactivite']));
    $datedeb=htmlspecialchars(addslashes($_POST['datedeb']));
    $datefin=htmlspecialchars(addslashes($_POST['datefin']));
    $objet_msg=8;
    $parascolaireid=htmlspecialchars(addslashes($_POST['parascolaireid']));
    $messagesid=htmlspecialchars(addslashes($_POST['messagesid']));
    $other="Activité parascolaire";
    $other=htmlspecialchars(addslashes($other));


    //information date de debut
    $tabdatedeb=explode(" ",$datedeb);
    $heurededeb=$tabdatedeb[1];
    $datededeb=$tabdatedeb[0];



      //information date de fin
    $tabdatefin=explode(" ",$datefin);
    $heuredefin=$tabdatefin[1];
    $datedefin=$tabdatefin[0];

    // echo $datededeb."</br>".$datedefin."</br>".$heurededeb."</br>".$heuredefin;



    $destinataires="Parent-";
    $studentSchool="";
    $statutNotif=0;
    $dateday=date("Y-m-d");
    $classes="";
    $desti=htmlspecialchars(addslashes($_POST['destinataires']));
    $paranotif=1;
    $statutpara=1;



    foreach($_POST['classeEtab'] as $valeur)
    {
     $classes=$classes.$valeur.'-';

    }

    if($desti==1)
    {
      //selection non precise
      $precis=0;

      if($paiecheck==1)
      {
        //activité payante

        $montantAct=htmlspecialchars(addslashes($_POST['montantAct']));
        $eleves="";

        //nous allons proceder a la modification de l'activité et du message

        $etabs->Updateparascolairepayantenonprecise($parascolaireid,$messagesid,$precis,$paiecheck,$montantAct,$datededeb,$heurededeb,$datedefin,$heuredefin,$classes,$eleves,$respoactivite,$contactrespo,$emailvalue,$smsvalue);


        //activite payante et envoie au parent de tous les eleves des classes selectionnes

        //ajouter dans la table $typesess

        // $idmessage=$etabs->AddparascolaireMessagePayantAll($objet_msg,$destinataires,$classes,$precis,$paranotif,$statutpara,$dateday,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other);

        // $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin);

        // $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin,$typesess,$emailvalue,$smsvalue);



      }else if($paiecheck==0){
        //activité non payante
        $montantAct=0;
        $eleves="";

          $etabs->Updateparascolairepayantenonprecise($parascolaireid,$messagesid,$precis,$paiecheck,$montantAct,$datededeb,$heurededeb,$datedefin,$heuredefin,$classes,$eleves,$respoactivite,$contactrespo,$emailvalue,$smsvalue);

           //activite non payante et envoie au parent de tous les eleves des classes selectionnes

       // $idmessage=$etabs->AddparascolaireMessageNoPayantAll($objet_msg,$destinataires,$classes,$precis,$paranotif,$statutpara,$dateday,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other);


       // $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin);

       // $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin,$typesess,$emailvalue,$smsvalue);

      }


    }else if($desti==2)
    {

          foreach($_POST['eleves'] as $valeur)
         {
           $eleves=$eleves.$valeur.'-';

         }
        // selection precise
        $precis=1;

        if($paiecheck==1)
        {
          //activité payante

          $montantAct=htmlspecialchars(addslashes($_POST['montantAct']));


          //nous allons proceder a la modification de l'activité et du message

          $etabs->Updateparascolairepayantenonprecise($parascolaireid,$messagesid,$precis,$paiecheck,$montantAct,$datededeb,$heurededeb,$datedefin,$heuredefin,$classes,$eleves,$respoactivite,$contactrespo,$emailvalue,$smsvalue);


          //activite payante et envoie au parent de tous les eleves des classes selectionnes

          //ajouter dans la table $typesess

          // $idmessage=$etabs->AddparascolaireMessagePayantAll($objet_msg,$destinataires,$classes,$precis,$paranotif,$statutpara,$dateday,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other);

          // $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin);

          // $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin,$typesess,$emailvalue,$smsvalue);



        }else if($paiecheck==0){
          //activité non payante
          $montantAct=0;

            $etabs->Updateparascolairepayantenonprecise($parascolaireid,$messagesid,$precis,$paiecheck,$montantAct,$datededeb,$heurededeb,$datedefin,$heuredefin,$classes,$eleves,$respoactivite,$contactrespo,$emailvalue,$smsvalue);

             //activite non payante et envoie au parent de tous les eleves des classes selectionnes

         // $idmessage=$etabs->AddparascolaireMessageNoPayantAll($objet_msg,$destinataires,$classes,$precis,$paranotif,$statutpara,$dateday,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other);


         // $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin);

         // $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin,$typesess,$emailvalue,$smsvalue);

        }

    }


}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  //recuperation des variables

  $emailvalue=htmlspecialchars(addslashes($_POST['emailvalue']));
  $smsvalue=htmlspecialchars(addslashes($_POST['smsvalue']));
  // $typesess=htmlspecialchars(addslashes($_POST['typesess']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $dataactivite=htmlspecialchars(addslashes($_POST['typeactivite']));
  $othertype="";
  $tabactivite=explode("-",$dataactivite);
  $typeactivite=$tabactivite[0];
  if($typeactivite==6)
  {
    $othertype=htmlspecialchars(addslashes($_POST['othertype']));
  }
  $denomination=htmlspecialchars(addslashes($_POST['denomination']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
  $gratuitcheck=htmlspecialchars(addslashes($_POST['gratuitcheck']));
  $paiecheck=htmlspecialchars(addslashes($_POST['paiecheck']));
  $respoactivite=htmlspecialchars(addslashes($_POST['respoactivite']));
  $contactrespo=htmlspecialchars(addslashes($_POST['contactrespo']));
  $locationactivite=htmlspecialchars(addslashes($_POST['locationactivite']));
  $descripactivite=htmlspecialchars(addslashes($_POST['descripactivite']));
  $datedeb=htmlspecialchars(addslashes($_POST['datedeb']));
  $datefin=htmlspecialchars(addslashes($_POST['datefin']));
  $objet_msg=8;
  $other="Activité parascolaire";
  $other=htmlspecialchars(addslashes($other));
  $useradd=$_SESSION['user']['IdCompte'];


  //information date de debut
  $tabdatedeb=explode(" ",$datedeb);
  $heurededeb=$tabdatedeb[1];
  $datededeb=$tabdatedeb[0];

    //information date de fin
  $tabdatefin=explode(" ",$datefin);
  $heuredefin=$tabdatefin[1];
  $datedefin=$tabdatefin[0];

  // echo $datededeb."</br>".$datedefin."</br>".$heurededeb."</br>".$heuredefin;

  $destinataires="Parent-";
  $studentSchool="";
  $statutNotif=0;
  $dateday=date("Y-m-d");
  $classes="";
  $desti=htmlspecialchars(addslashes($_POST['destinataires']));
  $paranotif=1;
  $statutpara=1;

  foreach($_POST['classeEtab'] as $valeur)
  {
   $classes=$classes.$valeur.'-';

  }

  if($desti==1)
  {
     $precis=0;

     if($paiecheck==1)
     {
       //activité payante

       $montantAct=htmlspecialchars(addslashes($_POST['montantAct']));



       //activite payante et envoie au parent de tous les eleves des classes selectionnes

       //ajouter dans la table $typesess

       $idmessage=$etabs->AddparascolaireMessagePayantAll($objet_msg,$destinataires,$classes,$precis,$paranotif,$statutpara,$dateday,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$useradd);

       // $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin);

       $idactivity=$etabs->AddActiviteparascolairesPrimary($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin,$emailvalue,$smsvalue,$othertype);



     }else if($paiecheck==0){
       //activité non payante
       $montantAct=0;


          //activite non payante et envoie au parent de tous les eleves des classes selectionnes

      $idmessage=$etabs->AddparascolaireMessageNoPayantAll($objet_msg,$destinataires,$classes,$precis,$paranotif,$statutpara,$dateday,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$useradd);


      // $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin);

      $idactivity=$etabs->AddActiviteparascolairesPrimary($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin,$emailvalue,$smsvalue,$othertype);

     }


  }else if($desti==2)
  {
    // echo "destinataires egale à 2";
    $precis=1;

    foreach($_POST['eleves'] as $valeur)
   {
     $studentSchool=$studentSchool.$valeur.'-';

   }


   if($paiecheck==1)
   {
     //activité payante

     $montantAct=htmlspecialchars(addslashes($_POST['montantAct']));

        //activite payante et envoie au parent de tous les eleves selectionnes
        //
        $idmessage=$etabs->AddparascolaireMessageToStudentselectParent($objet_msg,$destinataires,$classes,$precis,$studentSchool,$paranotif,$statutpara,$dateday,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$useradd);
        // $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin);

        $idactivity=$etabs->AddActiviteparascolairesPrimary($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin,$emailvalue,$smsvalue,$othertype);


   }else if($paiecheck==0){
     //activité non payante

     //activite payante et envoie au parent de tous les eleves selectionnes $typesess

     $montantAct=0;

     $idmessage=$etabs->AddparascolaireMessageToStudentselectParentNopay($objet_msg,$destinataires,$classes,$precis,$studentSchool,$paranotif,$statutpara,$dateday,$emailvalue,$smsvalue,$libellesession,$codeEtab,$other,$useradd);
     // $idactivity=$etabs->AddActiviteparascolaires($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin);

     $idactivity=$etabs->AddActiviteparascolairesPrimary($libellesession,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$idmessage,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin,$emailvalue,$smsvalue,$othertype);


   }


 }
 //last ajouter

 //insertion des interet de l'activite

   $concatinteret=htmlspecialchars(addslashes($_POST['concatinteret']));
   $concatnbinteret=htmlspecialchars(addslashes($_POST['concatnbinteret']));
   $tabconcatinteret=explode("@",$concatinteret);


   for($j=0;$j<$concatnbinteret;$j++)
   {
     $etabs->AddInteretactivite($idactivity,htmlspecialchars(addslashes($_POST['interet_'.$tabconcatinteret[$j]])));
   }


   $_SESSION['user']['updateteaok']="Activité parascolaire ajouter avec succès";
   //
   if($_SESSION['user']['profile'] == "Admin_globale") {

         //header("Location:../manager/index.php");
         header("Location:../manager/allparascolaires.php");

     }else if($_SESSION['user']['profile'] == "Admin_locale") {

       if($_SESSION['user']['paysid']==4)
       {
         header("Location:../localecmr/allparascolaires.php");
       }else {
         header("Location:../locale/allparascolaires.php");
       }


       }


}

if(isset($_GET['etape'])&&($_GET['etape']==3))
{
  //recuperation des variables

  $paraid=htmlspecialchars(addslashes($_GET['paraid']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
  $notifid=htmlspecialchars(addslashes($_GET['notifid']));

  $statusNotification=-1;
  $statusparasco=0;

  if($paraid>0)
  {

$etabs->DeleteNotificationParascoActivity($paraid,$notifid,$statusNotification,$statusparasco);
  }else if($paraid==0)
  {

    $etabs->DeleteNotificationActivity($notifid,$statusNotification);
  }


}else if(isset($_GET['etape'])&&($_GET['etape']==4))
{
  //recuperation des variables

  $notifid=htmlspecialchars(addslashes($_GET['notifid']));
  $destimails=htmlspecialchars(addslashes($_GET['destimails']));
  $destiphones=htmlspecialchars(addslashes($_GET['destiphones']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
  $smssender=htmlspecialchars(addslashes($_GET['smssender']));
  $emailsender=htmlspecialchars(addslashes($_GET['emailsender']));
  $precis=htmlspecialchars(addslashes($_GET['precis']));
  $eleves=htmlspecialchars(addslashes($_GET['eleves']));
  $paraid=htmlspecialchars(addslashes($_GET['paraid']));

  $notificationsStatus=1;
  $parascolaireStatus=2;

  //recuperer les informations du message(titre & contenu)
  $destimails=substr($destimails, 0, -1);
  $destiphones=substr($destiphones, 0, -1);

  $datanotification=$etabs->getMessagesInfos($notifid);
  $tabnotification=explode("*",$datanotification);
  $datenotif=$tabnotification[9];

  //nous allons recuperer le nom de l'etablissement et le logo

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
  $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);

  $datas=$etabs->getMessagesInfos($notifid);
  $tabdatas=explode("*",$datas);
  $titremessage=$tabdatas[7];
  $contenumessage=$tabdatas[8];

  if($emailsender==1)
  {
  //envoi de mail aux destinataires
  if($smssender==1)
  {
  //envoi de mail plus sms
   echo "envoi de mail plus sms";

   // echo $destimails." -".$titremessage." -".$contenumessage." -".$libelleEtab." -".$logoEtab." -".$codeEtab;

  $etabs->SendNotifiactionWithoutToDestinataires($destimails,$titremessage,$contenumessage,$libelleEtab,$logoEtab,$codeEtab);

  //nous allons envoyer les sms ici

  /*envoi du sms selon clicksend*/

  $phonenumber=str_replace("*",",",$destiphones);
  echo $phonenumber;
  //sms mis en commentaire
  // $etabs->clicksendersms($phonenumber,$contenumessage);

  }else {
    //mail seulement
   echo "mail seulement";
  $etabs->SendNotifiactionWithoutToDestinataires($destimails,$titremessage,$contenumessage,$libelleEtab,$logoEtab,$codeEtab);

  }



}else if($emailsender==0)
{
  //envoi de sms seulement
}

// //message notification envoyer avec succès
//
// //changer le statut de la notification à 1
//
if($paraid==0)
{
  echo "bonjour";
  $etabs->UpdateNotificationStatusParaActivity($notifid,$notificationsStatus,$codeEtab);
}else if($paraid>0)
{
  //changer le statut de notification à 1 puis parascolaire à 2
  // echo "bonsoir"; $parascolaireStatus,$notifid,$codeEtab,$paraid

  // echo $notifid." -".$notificationsStatus." -".$codeEtab." -".$_GET['paraid']." -".$parascolaireStatus;

  $etabs->UpdateNotificationStatusAndParascolairesParaActivity($notifid,$notificationsStatus,$codeEtab,$_GET['paraid'],$parascolaireStatus);
}

$_SESSION['user']['updateteaok']="Notification Activité parascolaire envoyé avec succès";
//
if($_SESSION['user']['profile'] == "Admin_globale") {


      header("Location:../manager/allparascolaires.php");

  }else if($_SESSION['user']['profile'] == "Admin_locale") {

    if($_SESSION['user']['paysid']==4)
    {
      header("Location:../localecmr/allparascolaires.php");
    }else {
      header("Location:../locale/allparascolaires.php");
    }


    }

}else if(isset($_GET['etape'])&&($_GET['etape']==5))
{
  //recuperation des variables

  $paraid=htmlspecialchars(addslashes($_GET['paraid']));
  $notifid=htmlspecialchars(addslashes($_GET['notifid']));
  $session=htmlspecialchars(addslashes($_GET['session']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
  $notificationstatut=2;
  $parascolairestatut=3;

  $etabs->ArchivedParascolaireActivitiesX($paraid,$notifid,$session,$codeEtab,$notificationstatut,$parascolairestatut);


}


 ?>
