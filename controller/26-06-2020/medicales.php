<?php
session_start();
require_once('../class/Student.php');
require_once('../class/Etablissement.php');
require_once('../class/Sessionsacade.php');
require_once('../controller/functions.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$student = new Student();
$etabs = new Etab();
$sessionX= new Sessionacade();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recuperation des variables

  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $studentid=htmlspecialchars($_POST['idcompte']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);

  $blood=htmlspecialchars($_POST['blood']);
  $pinchildhopial=htmlspecialchars($_POST['pinchildhopial']);
  $hopital=htmlspecialchars($_POST['hopital']);
  $doctor=htmlspecialchars($_POST['doctor']);
  $phonedoctor=htmlspecialchars($_POST['phonedoctor']);
  $localisation=htmlspecialchars($_POST['localisation']);
  $adresgeodetails=htmlspecialchars($_POST['adresgeodetails']);
  $guardian=htmlspecialchars($_POST['guardian']);
  $telBguardian=htmlspecialchars($_POST['telBguardian']);
  $telMobguardian=htmlspecialchars($_POST['telMobguardian']);
  $domicileguardian=htmlspecialchars($_POST['domicileguardian']);
  $matri=htmlspecialchars($_POST['matricule']);
  $nomad=htmlspecialchars($_POST['nomad']);

  //ajout des allergies et maladies infantiles

  $allergies="";
  $infantiles="";

  $nballergies=htmlspecialchars($_POST['nballergies']);
  $nbinfantilesD=htmlspecialchars($_POST['nbinfantilesD']);

  if($nballergies>0)
  {
    $concatnballergies=htmlspecialchars($_POST['concatnballergies']);
    $concatnballergies=substr($concatnballergies, 0, -1);
    $taballergies=explode("@",$concatnballergies);
    $nb=count($taballergies);


    for($i=0;$i<$nb;$i++)
    {
      $Allergies=htmlspecialchars($_POST['Allergies_'.$taballergies[$i]]);
      $type="ALLERGIES";

      $allergiesid=$student->AddAllergiesStudent($Allergies,$codeEtab,$type);

      $allergies=$allergies.$allergiesid.",";

    }

  }

  //ajouter les maladies infantiles

  if($nbinfantilesD>0)
  {

    $concatnbinfantilesD=htmlspecialchars($_POST['concatnbinfantilesD']);
    $concatnbinfantilesD=substr($concatnbinfantilesD, 0, -1);
    $tabmaladie=explode("@",$concatnbinfantilesD);
    $nb=count($tabmaladie);

    for($i=0;$i<$nb;$i++)
    {
      $maladies=htmlspecialchars($_POST['Infantiles_'.$tabmaladie[$i]]);
      $type="INFANTILES";

      $infantilesid=$student->AddInfantilesStudent($maladies,$codeEtab,$type);

      $infantiles=$infantiles.$infantilesid.",";

    }
  }


  //nous allons ajouter le formulaire medical

  $medicalformid=$student->AddMedicalForm($codeEtab,$studentid,$blood,$pinchildhopial,$hopital,$doctor,$phonedoctor,$allergies,$infantiles,$localisation,$adresgeodetails,$guardian,$telBguardian,$telMobguardian,$domicileguardian);
  //
  $concatantecedent=htmlspecialchars($_POST['concatantecedent']);
  $tabantecedent=explode("@",$concatantecedent);
  $nbconcatantecedent=htmlspecialchars($_POST['nbconcatantecedent']);
  //
  for($i=0;$i<$nbconcatantecedent;$i++)
  {
    $libelleantecedet=htmlspecialchars($_POST['libelleantecedet'.$tabantecedent[$i]]);
    $childantecedet=htmlspecialchars($_POST['childantecedet'.$tabantecedent[$i]]);
    $fatherantecedet=htmlspecialchars($_POST['fatherantecedet'.$tabantecedent[$i]]);
    $motherantecedet=htmlspecialchars($_POST['motherantecedet'.$tabantecedent[$i]]);

    $type="ANTECEDENTS";

    $antecedentid=$student->AddAntecedentsStudent($libelleantecedet,$codeEtab,$type);

    echo $antecedentid;


    $student->AddantecedentMedical($studentid,$medicalformid,$antecedentid,$childantecedet,$fatherantecedet,$motherantecedet);
  }

//gestion du carnet

$fileCarnetError = $_FILES['carnet']['error'];

if($fileCarnetError==0)
{
  $file_name = @$_FILES['carnet']['name'];

  $_SESSION["carnet"] = $file_name;

  $file_size =@$_FILES['carnet']['size'];

  $file_tmp =@$_FILES['carnet']['tmp_name'];

  $file_type=@$_FILES['carnet']['type'];

  @$file_ext=strtolower(end(explode('.',@$_FILES['carnet']['name'])));

  $fichierTemp = uniqid() . "." . $file_ext;

  $transactionId =  "STU".date("Y") . date("m").strtoupper(substr($nomad, 0, 2)).$matri;

  $fichierad=$transactionId.".".$file_ext;

  move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

  $dossier="../photo/Carnets/";

  $dossier1="../photo/Carnets/".$matri;

  if(!is_dir($dossier)) {
        //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
        @mkdir($dossier);
        @mkdir($dossier1);

            }else
            {
       @mkdir($dossier1);
            }

            @rename('../temp/' . $fichierTemp , "../photo/Carnets/" . $matri."/".$fichierad);

                 //Suppression du fichier se trouvant dans le dossier temp

            @unlink("../temp/" . $fichierTemp);

    $student->UpdateStudentCarnetFile($fichierad,$medicalformid,$studentid);



}


$_SESSION['user']['addStudok']=L::RenseignementAddsuccessfully;

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

if($_SESSION['user']['profile'] == "Admin_globale")

{

header("Location:../manager/admission.php");

}else if($_SESSION['user']['profile'] == "Admin_locale")
{
header("Location:../locale".$libelleEtab."/detailsstudent.php?compte=".$studentid);

}




}




?>
