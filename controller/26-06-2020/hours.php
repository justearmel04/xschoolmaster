<?php
session_start();
//require_once('../class/Matiere.php');
require_once('../class/Etablissement.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$etab = new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //ajout du diplôme

  //recupération des variables
  // $libelleheure=htmlspecialchars(addslashes($_POST['heurelib']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtablib']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesessionlib']));
  $nbLib=htmlspecialchars(addslashes($_POST['nbLib']));
  $concatLibs=htmlspecialchars(addslashes($_POST['concatLibs']));

  $tabs=explode("@",$concatLibs);
  $nbtabs=count($tabs)-1;

   echo $nbtabs;

  for($i=0;$i<$nbtabs;$i++)
  {
  $libelleheure=htmlspecialchars(addslashes($_POST['heurelib'.$tabs[$i]]));
  //echo $libelleheure;
  $etab->AddHeureLib($libelleheure,$codeEtab,$libellesession);
  }


  //inscription dans la table heurelib



  // $_SESSION['user']['updateparentok']="Libellé heure ajouter avec succès";
  $_SESSION['user']['updateparentok']=L::HeureLibAddMessageSuccess;

  if($_SESSION['user']['profile'] == "Admin_globale") {

        //header("Location:../manager/index.php");
        header("Location:../manager/hours.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/hours.php");
      }else {
        header("Location:../locale/hours.php");
      }


    }




}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recuperation des variables

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
  $concatidH=htmlspecialchars(addslashes($_POST['concatidH']));
  $concatidH=substr($concatidH, 0, -1);
  // echo $concatidH;
  $tabconcat=explode(",",$concatidH);
  $nb=count($tabconcat);

  for ($i=0; $i <$nb ; $i++) {
    $libelleheure=htmlspecialchars(addslashes($_POST['libelleheure'.$tabconcat[$i]]));
    $heuredeb=htmlspecialchars(addslashes($_POST['heuredeb'.$tabconcat[$i]]));
    $heurefin=htmlspecialchars(addslashes($_POST['heurefin'.$tabconcat[$i]]));

    $etab->AddHoursLibelle($libelleheure,$heuredeb,$heurefin);

  }

  // $_SESSION['user']['updateparentok']="Heure de cours ajouter avec succès";
  $_SESSION['user']['updateparentok']=L::HourscoursesAddMessageSuccess;

  if($_SESSION['user']['profile'] == "Admin_globale") {

        //header("Location:../manager/index.php");
        header("Location:../manager/hours.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/hours.php");
      }else {
        header("Location:../locale/hours.php");
      }


    }

}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //ajout du diplôme

  //recupération des variables
  // $libelleheure=htmlspecialchars(addslashes($_POST['heurelib']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtablib']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesessionlib']));
  $nbLib=htmlspecialchars(addslashes($_POST['nbLib']));
  $concatLibs=htmlspecialchars(addslashes($_POST['concatLibs']));

  $tabs=explode("@",$concatLibs);
  $nbtabs=count($tabs)-1;

   echo $nbtabs;

  for($i=0;$i<$nbtabs;$i++)
  {
  $libelleheure=htmlspecialchars(addslashes($_POST['heurelib'.$tabs[$i]]));
  //echo $libelleheure;
  $etab->AddHeureLib($libelleheure,$codeEtab,$libellesession);
  }


  //inscription dans la table heurelib



  // $_SESSION['user']['updateparentok']="Un Libellé a été ajouté avec succès";
  $_SESSION['user']['updateparentok']=L::LibelleAddMessageSuccess;

    $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);

  if($_SESSION['user']['profile'] == "Admin_globale") {

        //header("Location:../manager/index.php");
        header("Location:../manager/hours.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/hours.php");
      }else {
        header("Location:../locale".$libelleEtab."/hours.php");
      }


    }




}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //recuperation des variables

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
  $concatidH=htmlspecialchars(addslashes($_POST['concatidH']));
  $concatidH=substr($concatidH, 0, -1);
  // echo $concatidH;
  $tabconcat=explode(",",$concatidH);
  $nb=count($tabconcat);

  for ($i=0; $i <$nb ; $i++) {
    $libelleheure=htmlspecialchars(addslashes($_POST['libelleheure'.$tabconcat[$i]]));
    $heuredeb=htmlspecialchars(addslashes($_POST['heuredeb'.$tabconcat[$i]]));
    $heurefin=htmlspecialchars(addslashes($_POST['heurefin'.$tabconcat[$i]]));

    $etab->AddHoursLibelle($libelleheure,$heuredeb,$heurefin);

  }

  // $_SESSION['user']['updateparentok']="Une nouvelle Heure de cours a été ajoutée avec succès";
  $_SESSION['user']['updateparentok']=L::NewCoursesHoursAddMessageSuccess;

    $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);

  if($_SESSION['user']['profile'] == "Admin_globale") {

        //header("Location:../manager/index.php");
        header("Location:../manager/hours.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/hours.php");
      }else {
        header("Location:../locale".$libelleEtab."/hours.php");
      }


    }

}

?>
