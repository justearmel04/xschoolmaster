<?php
session_start();
require_once('../controller/functions.php');
require_once('../class/Student.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Matiere.php');
$classe = new Classe();
$student=new Student();
$etabs=new Etab();
$matierestud= new Matiere();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //il est question des notes de controle

  //reciprération des variables
  // $Note28: 15
  // $obser28: TRES BIEN
  // $studentmat: 28*
  $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
  $classeId=htmlspecialchars(addslashes($_POST['classeId']));
  $typenote=htmlspecialchars(addslashes($_POST['typenote']));
  $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
  $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
  $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $sessionlibelle=htmlspecialchars(addslashes($_POST['sessionlibelle']));

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
  $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
  $libelleclasse=$classe->getInfosofclassesbyId($classeId,$sessionlibelle);
  


  $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);

  $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentmat'])));



  for($i=0;$i<$nbstudent;$i++)
  {

    $notes=htmlspecialchars(addslashes($_POST['note'.$tabstudent[$i]]));
    $observation=htmlspecialchars(addslashes($_POST['obser'.$tabstudent[$i]]));
    //insertion dans la table note

    $ideleve=$tabstudent[$i];
    $studentDatas=$student->getAllInformationsOfStudent($ideleve,$sessionlibelle);
    $studentData=explode("*",$studentDatas);
    $nomcompletStudent=$studentData[2]." ".$studentData[3];
    //$libelleclasse=$studentData[9];

    //retrouver les informations du parent à savoir son email et son contact

    $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

    $tabdatadestinataires="Parent";
    //nous allons rechercher l'email du parent
    $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($classeId,$tabdatadestinataires,$codeEtab,$sessionlibelle,$libellematiere,$codeEtab);
    $ka=1;
    foreach ($dataParents as $parents):

      $destimails=$parents->email_parent;
      $destiphone=$indicatifEtab.$parents->tel_parent;


      $ka++;
    endforeach;

    //$etabs->SendAddedControleNotesMailerToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$sessionlibelle,$codeEtab,$libellematiere);

    $student->AddNotesControle($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation,$sessionlibelle);

    // echo $observation;
  }

  $_SESSION['user']['addattendailyok']="Notes de classe ajouter avec succès";



  if($_SESSION['user']['profile'] == "Admin_globale")

  {

header("Location:../manager/notes.php");

  }else if($_SESSION['user']['profile'] == "Admin_locale")

  {

header("Location:../locale/notes.php");

}else if($_SESSION['user']['profile'] == "Teatcher")

  {

header("Location:../teatcher/notes.php");

  }


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //il est question des notes d'examen

  $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
  $classeId=htmlspecialchars(addslashes($_POST['classeId']));
  $typenote=htmlspecialchars(addslashes($_POST['typenote']));
  $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
  $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
  $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $sessionlibelle=htmlspecialchars(addslashes($_POST['sessionlibelle']));

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
  $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);


  $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);

  $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentmat'])));

  for($i=0;$i<$nbstudent;$i++)
  {

    $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
    $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
    //insertion dans la table note
    $ideleve=$tabstudent[$i];
    $studentDatas=$student->getAllInformationsOfStudent($ideleve,$sessionlibelle);
    $studentData=explode("*",$studentDatas);
    $nomcompletStudent=$studentData[2]." ".$studentData[3];
    $libelleclasse=$studentData[9];

    //retrouver les informations du parent à savoir son email et son contact

    $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

    $tabdatadestinataires="Parent";
    //nous allons rechercher l'email du parent
    $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($classeId,$tabdatadestinataires,$codeEtab,$sessionlibelle,$libellematiere,$codeEtab);
    $ka=1;
    foreach ($dataParents as $parents):

      $destimails=$parents->email_parent;
      $destiphone=$indicatifEtab.$parents->tel_parent;


      $ka++;
    endforeach;

    $etabs->SendAddedExamenNotesMailerToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$sessionlibelle,$codeEtab);


// echo $tabstudent[$i];
    $student->AddNotesExamen($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation,$sessionlibelle);

    // echo $observation;
  }



}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recupération des variables
  $noteid=htmlspecialchars(addslashes($_POST['noteid']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $studentid=htmlspecialchars(addslashes($_POST['studentid']));
  $classeid=htmlspecialchars(addslashes($_POST['classeid']));
  $note=htmlspecialchars(addslashes($_POST['note']));
  $observation=htmlspecialchars(addslashes($_POST['observation']));

  $student->UpdateNotesOfStudent($noteid,$codeEtab,$studentid,$classeid,$note,$observation);

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
    if($_POST['typenote']==1)
    {
      //il est question d'un controle
      //recuperation des variables

      $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
      $classeId=htmlspecialchars(addslashes($_POST['classeId']));
      $typenote=htmlspecialchars(addslashes($_POST['typenote']));
      $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
      $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
      $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
      $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
      $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

      $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
      $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);

      $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);
      // $oldnote=htmlspecialchars(addslashes($_POST['oldnote']));

      $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentmat'])));

      //mise à jour des notes

      for($i=0;$i<$nbstudent;$i++)
      {

        $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
        $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
        $ideleve=$tabstudent[$i];
        $studentDatas=$student->getAllInformationsOfStudent($ideleve,$libellesession);
        $studentData=explode("*",$studentDatas);
        $nomcompletStudent=$studentData[2]." ".$studentData[3];
        $libelleclasse=$studentData[9];

        //nous allons verifier si cet eleve à deja une note pour ce controle si non alors nous allons ajouter la notes

        $check=$student->getNotesAndObservControleNb($ideleve,$typenote,$classeId,$idtypenote,$matiereid,$teatcherid,$libellesession,$codeEtab);

        if($check==0)
        {
          //ajout de la note

            $student->AddNotesControle($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation,$libellesession);

        }else if($check>0)
        {
          //notification aux parents puis modifications de notes

          $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

          $tabdatadestinataires="Parent";
          //nous allons rechercher l'email du parent
          $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($classeId,$tabdatadestinataires,$codeEtab,$libellesession);
          $ka=1;
          foreach ($dataParents as $parents):

            $destimails=$parents->email_parent;
            $destiphone=$indicatifEtab.$parents->tel_parent;


            $ka++;
          endforeach;

          $etabs->SendModificationNotesToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$libellematiere,$codeEtab);


        }

        //insertion dans la table note

        // $student->AddNotesControle($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation);

        $student->UpdateControleNotes($notes,$observation,$ideleve,$typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$codeEtab);

        // echo $observation;
      }



    }else if($_POST['typenote']==2)
    {
      //il est question d'un examen
      $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
      $classeId=htmlspecialchars(addslashes($_POST['classeId']));
      $typenote=htmlspecialchars(addslashes($_POST['typenote']));
      $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
      $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
      $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
      $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
      $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
      $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
      $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);

      $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);




      $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentmat'])));

      for($i=0;$i<$nbstudent;$i++)
      {

        $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
        $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
        $ideleve=$tabstudent[$i];
        $studentDatas=$student->getAllInformationsOfStudent($ideleve,$libellesession);
        $studentData=explode("*",$studentDatas);
        $nomcompletStudent=$studentData[2]." ".$studentData[3];
        $libelleclasse=$studentData[9];

        //nous allons verifier si cet eleve à deja une note pour cet examen si non alors nous allons ajouter la notes

        $check=$student->getNotesAndObservExamNb($ideleve,$typenote,$classeId,$idtypenote,$matiereid,$teatcherid,$libellesession,$codeEtab);

        if($check==0)
        {
  $student->AddNotesExamen($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation,$libellesession);
        }else if($check>0)
        {
          $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

          $tabdatadestinataires="Parent";
          //nous allons rechercher l'email du parent
          $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($classeId,$tabdatadestinataires,$codeEtab,$libellesession);
          $ka=1;
          foreach ($dataParents as $parents):

            $destimails=$parents->email_parent;
            $destiphone=$indicatifEtab.$parents->tel_parent;


            $ka++;
          endforeach;

          $etabs->SendModificationNotesToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$libellematiere,$codeEtab);


        }

        //insertion dans la table note

        // $student->AddNotesControle($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation);

        $student->UpdateControleNotes($notes,$observation,$ideleve,$typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$codeEtab);

        // echo $observation;
      }


    }
}
