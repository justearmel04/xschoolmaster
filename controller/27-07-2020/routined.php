<?php
session_start();

require_once('../class/Etablissement.php');
require_once('../class/Matiere.php');
require_once('../class/Teatcher.php');
require_once('../class/Classe.php');
require_once('../controller/functions.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$routine = new Etab();
$subject= new Matiere();
$teatcher=new Teatcher();
$classesT=new Classe();

if(isset($_POST['etape'])&&($_POST['etape']==1||$_POST['etape']==2||$_POST['etape']==4||$_POST['etape']==5))
{
//enregistrer et consigner un autre jour

  //recuperation des variables

  $sessioncourante=htmlspecialchars($_POST['sessioncourante']);
  $classe=htmlspecialchars($_POST['classe']);
  $day=htmlspecialchars($_POST['day']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $nbconcatroutines=htmlspecialchars($_POST['nbconcatroutines']);
  $concatroutines=htmlspecialchars($_POST['concatroutines']);

  $tabconcatroutines=explode("@",$concatroutines);

  //insertion dans la table routine

  for($i=0;$i<$nbconcatroutines;$i++)
  {

      $matiere=htmlspecialchars($_POST['matiere'.$tabconcatroutines[$i]]);
      $heuredeb=htmlspecialchars($_POST['heuredeb'.$tabconcatroutines[$i]]);
      $heurefin=htmlspecialchars($_POST['heurefin'.$tabconcatroutines[$i]]);
      $heureLibelles=htmlspecialchars($_POST['heureLibelles'.$tabconcatroutines[$i]]);

      // echo $heureLibelles;

      //insertion dans la base

      $routine->AddRoutineRow($classe,$codeEtab,$heuredeb,$heurefin,$matiere,$day,$sessioncourante,$heureLibelles);


  }

    $libelleEtab=$routine->getEtabLibellebyCodeEtab($codeEtab);

  if($_POST['etape']==1)
  {
    // $_SESSION['user']['addprogra']="Le cours a été ajouté avec succès";

    $_SESSION['user']['addprogra']=L::RoutineAddMessageSuccess;

    if($_SESSION['user']['profile'] == "Admin_globale") {


          header("Location:../manager/routinesadd.php?codeEtab=".$codeEtab);

      }else if($_SESSION['user']['profile'] == "Admin_locale") {
        if($_SESSION['user']['paysid']==4)
        {
          header("Location:../localecmr/routinesadd.php");
        }else {
          header("Location:../locale/routinesadd.php");
        }


        }
  }else if($_POST['etape']==2){

    // $_SESSION['user']['addprogra']="Le cours a été ajouté avec succès";
    $_SESSION['user']['addprogra']=L::RoutineAddMessageSuccess;

    if($_SESSION['user']['profile'] == "Admin_globale") {


          header("Location:../manager/routines.php?codeEtab=".$codeEtab."&classe=".$classe);

      }else if($_SESSION['user']['profile'] == "Admin_locale") {
        if($_SESSION['user']['paysid']==4)
        {
          header("Location:../localecmr/routines.php?classe=".$classe);
        }else {
            $etablissementType=$routine->DetermineTypeEtab($codeEtab);

            if($etablissementType==5)
            {
                header("Location:../locale".$libelleEtab."/routines.php?classe=".$classe);
            }else {
              header("Location:../locale/routines.php?classe=".$classe);
            }


        }


        }
  }else if($_POST['etape']==4){

    $libelleEtab=$routine->getEtabLibellebyCodeEtab($codeEtab);

    // $_SESSION['user']['addprogra']="Le cours a été ajouté avec succès";
    $_SESSION['user']['addprogra']=L::RoutineAddMessageSuccess;

    if($_SESSION['user']['profile'] == "Admin_globale") {


          header("Location:../manager/routines.php?codeEtab=".$codeEtab."&classe=".$classe);

      }else if($_SESSION['user']['profile'] == "Admin_locale") {
        if($_SESSION['user']['paysid']==4)
        {
          header("Location:../localecmr/routines.php?classe=".$classe);
        }else {
          $etablissementType=$routine->DetermineTypeEtab($codeEtab);

          if($etablissementType==5)
          {
            header("Location:../locale".$libelleEtab."/routines.php?classe=".$classe);
          }else {
            header("Location:../locale/routines.php?classe=".$classe);
          }

        }


        }

  }else if($_POST['etape']==5){

    // $_SESSION['user']['addprogra']="Le cours a été ajouté avec succès";
    $_SESSION['user']['addprogra']=L::RoutineAddMessageSuccess;

      $libelleEtab=$routine->getEtabLibellebyCodeEtab($codeEtab);

    if($_SESSION['user']['profile'] == "Admin_globale") {


          header("Location:../manager/routines.php?codeEtab=".$codeEtab."&classe=".$classe);

      }else if($_SESSION['user']['profile'] == "Admin_locale") {
        if($_SESSION['user']['paysid']==4)
        {
          header("Location:../localecmr/routines.php?classe=".$classe);
        }else {
            $etablissementType=$routine->DetermineTypeEtab($codeEtab);
            if($etablissementType==5)
            {
              header("Location:../locale".$libelleEtab."/routines.php?classe=".$classe);
            }else {
              header("Location:../locale/routines.php?classe=".$classe);
            }

        }


        }
  }

  //nous allons retourner sur la page precedente pour une nouvelle saisie







}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recuperation des variables

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $day=htmlspecialchars(addslashes($_POST['day']));
  $heuredeb=htmlspecialchars(addslashes($_POST['debhournew']));
  $heurefin=htmlspecialchars(addslashes($_POST['finhournew']));
  $idroutine=htmlspecialchars(addslashes($_POST['idroutine']));
  $Etab=htmlspecialchars(addslashes($_POST['Etab']));
  $jourold=htmlspecialchars(addslashes($_POST['jourold']));
  $libhours=htmlspecialchars(addslashes($_POST['libhours']));

  $debhourold=htmlspecialchars(addslashes($_POST['debhourold']));
  $finhourold=htmlspecialchars(addslashes($_POST['finhourold']));
  $libellejourold=AfficherSemaine($jourold);
  $libelledaymodify=AfficherSemaine($day);
  $libellematieres=$subject->getMatiereLibelleByIdMat($matiere,$Etab);
  $libellession=htmlspecialchars(addslashes($_POST['libellession']));
  $libelleclasse=$classesT->getInfosofclassesbyId($classe,$libellession);

  $parentsMails="";
  $parentsContacts="";
  $tabdatadestinataires="Parent";

  //nous allons recuperer le nom de l'etablissement et le logo

  $libelleEtab=$routine->getEtabLibellebyCodeEtab($Etab);
  $logoEtab=$routine->getEtabLogobyCodeEtab($Etab);

  //modification de la routine

  $routine->UpdateRoutineAndLib($classe,$Etab,$heuredeb,$heurefin,$matiere,$day,$libhours,$idroutine);

  //envoie de notification aux parents et au professeur enseignant cette matiere dans cette classe

  //retrouver le mail du professeur de cette matieres

  $mailteatcher=$teatcher->getterMailOfTeatcherBySubjectidAndClasses($matiere,$classe,$Etab);
  // retrouver les emails des parents des eleves de cette classe
  $dataParents=$routine->getEmailsOfParentOfStudentInThisClasses($classe,$tabdatadestinataires,$Etab,$libellession);
  $indicatifEtab=$routine->getIndictatifOfThisSchool($Etab);

  $ka=1;
  foreach ($dataParents as $parents):

    $parentsMails=$parentsMails.$parents->email_parent."*";
    $parentsContacts=$parentsContacts.$indicatifEtab.$parents->tel_parent."*";


    $ka++;
  endforeach;

    $parentsMails=substr($parentsMails, 0, -1);
    $parentsContacts=substr($parentsContacts, 0, -1);

    //envoi du mail au preofesseur
    $routine->TeatcherRoutineUpdateMailer($mailteatcher,$libelleclasse,$libellematieres,$libellejourold,$libelledaymodify,$debhourold,$heuredeb,$finhourold,$heurefin,$libelleEtab,$logoEtab,$Etab);

    //envoi du mail aux parents

    $routine->ParentsRoutineUpdateMailer($parentsMails,$libelleclasse,$libellematieres,$libellejourold,$libelledaymodify,$debhourold,$heuredeb,$finhourold,$heurefin,$libelleEtab,$logoEtab,$Etab);


    // $_SESSION['user']['updateroutineok']=" Cours modifié avec succès";

    $_SESSION['user']['updateroutineok']=L::RoutineModMessageSuccess;


    if($_SESSION['user']['profile'] == "Admin_globale") {

          //header("Location:../manager/index.php");
          header("Location:../manager/routines.php?classe=".$classe);

      }else if($_SESSION['user']['profile'] == "Admin_locale") {
        if($_SESSION['user']['paysid']==4)
        {
          header("Location:../localecmr/routines.php?classe=".$classe);
        }else {

            $etablissementType=$routine->DetermineTypeEtab($codeEtab);

            if($etablissementType==5)
            {
header("Location:../locale".$libelleEtab."/routines.php?classe=".$classe);
            }else {
              header("Location:../locale/routines.php?classe=".$classe);
            }


        }


        }




}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{

  $classeid=htmlspecialchars($_POST['classe']);
  $matiereid=htmlspecialchars($_POST['matiere']);
  $idroutine=htmlspecialchars($_POST['idroutine']);
  $codeEtab=htmlspecialchars($_POST['Etab']);
  $jourold=htmlspecialchars($_POST['jourold']);
  $sessionEtab=htmlspecialchars($_POST['libellession']);
  $libellematieres=$subject->getMatiereLibelleByIdMat($matiereid,$codeEtab);
  $libelleclasse=$classesT->getInfosofclassesbyId($classeid,$sessionEtab);
  $libelleEtab=$routine->getEtabLibellebyCodeEtab($codeEtab);
  $logoEtab=$routine->getEtabLogobyCodeEtab($codeEtab);
  $libellejourold=AfficherSemaine($jourold);

  $dataroutineInfos=$routine->getRoutineInfos($idroutine,$codeEtab,$sessionEtab,$classeid);

  $tabroutesdatas=explode("*",$dataroutineInfos);
  $heuredeb=$tabroutesdatas[0];
  $heurefin=$tabroutesdatas[1];

  $parentsMails="";
  $parentsContacts="";
  $tabdatadestinataires="Parent";


  $routine->UpdateRoutineOnly($classeid,$matiereid,$idroutine,$codeEtab,$jourold,$sessionEtab);

  //envoie de notification aux parents et au professeur enseignant cette matiere dans cette classe

  //retrouver le mail du professeur de cette matieres

  $mailteatcher=$teatcher->getterMailOfTeatcherBySubjectidAndClasses($matiereid,$classeid,$codeEtab);

  $dataParents=$routine->getEmailsOfParentOfStudentInThisClasses($classeid,$tabdatadestinataires,$codeEtab,$sessionEtab);
  $indicatifEtab=$routine->getIndictatifOfThisSchool($codeEtab);

  $ka=1;
  foreach ($dataParents as $parents):

    $parentsMails=$parentsMails.$parents->email_parent."*";
    $parentsContacts=$parentsContacts.$indicatifEtab.$parents->tel_parent."*";


    $ka++;
  endforeach;

    $parentsMails=substr($parentsMails, 0, -1);
    $parentsContacts=substr($parentsContacts, 0, -1);

    if(strlen($parentsMails)>0)
    {
      $routine->ParentsRoutineUpdateMailerOne($parentsMails,$libelleEtab,$libelleclasse,$libellematieres,$codeEtab,$logoEtab,$libellejourold,$heuredeb,$heurefin);
    }



    if(strlen($mailteatcher)>0)
    {
        $routine->TeatcherRoutineUpdateMailerOne($mailteatcher,$libelleEtab,$libelleclasse,$libellematieres,$codeEtab,$logoEtab,$libellejourold,$heuredeb,$heurefin);
    }


    // $_SESSION['user']['updateroutineok']=" Cours modifié avec succès";
    $_SESSION['user']['updateroutineok']=L::RoutineModMessageSuccess;


    if($_SESSION['user']['profile'] == "Admin_globale") {


          header("Location:../manager/routines.php?codeEtab=".$codeEtab."&classe=".$classeid);

      }else if($_SESSION['user']['profile'] == "Admin_locale") {
        if($_SESSION['user']['paysid']==4)
        {
          header("Location:../localecmr/routines.php?classe=".$classeid);
        }else {
          $etablissementType=$routine->DetermineTypeEtab($codeEtab);

          if($etablissementType==5)
          {
            header("Location:../locale".$libelleEtab."/routines.php?classe=".$classeid);
          }else {
              header("Location:../locale/routines.php?classe=".$classeid);
          }


        }


        }


}



 ?>
