<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../controller/functions.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$classe = new Classe();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recuperation des variables

  $exam=htmlspecialchars($_POST['examen']);
  $datedeb=dateFormat($_POST['datedeb']);
  $datefin=dateFormat($_POST['datefin']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $libellesession=htmlspecialchars($_POST['libellesession']);
  $typesess=htmlspecialchars($_POST['typesess']);

//inserion dans la table examens

  $etab->AddExamenSchool($exam,$datedeb,$datefin,$codeEtab,$libellesession,$typesess);

  $_SESSION['user']['updateExamok']="Examen ajouté avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {


        header("Location:../manager/examens.php?codeEtab=".$codeEtab);

    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      // if($_SESSION['user']['paysid']==4)
      // {
      //   header("Location:../localeclr/examens.php");
      // }else {
      //   header("Location:../locale/examens.php");
      // }

            $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etab->DetermineTypeEtab($codeEtab);

            if($typeetab==5)
            {
              header("Location:../locale".$libelleEtab."/examens.php");
            }else {
              header("Location:../locale/examens.php");
            }


      }

}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recuperation des variables
$idexam=htmlspecialchars($_POST['idexam']);

$exam=htmlspecialchars($_POST['exam'.$idexam]);
$datedeb=dateFormat($_POST['datedeb'.$idexam]);
$datefin=dateFormat($_POST['datefin'.$idexam]);
$codeEtab=htmlspecialchars($_POST['codeEtab']);

$etab->UpdateExamenOfClasses($exam,$datedeb,$datefin,$codeEtab,$idexam);

$_SESSION['user']['updateExamok']="Informations Examen modifié avec succès";
if($_SESSION['user']['profile'] == "Admin_globale") {

      //header("Location:../manager/index.php");
      header("Location:../manager/examens.php");

  }else if($_SESSION['user']['profile'] == "Admin_locale") {
    // if($_SESSION['user']['paysid']==4)
    // {
    //   header("Location:../localecmr/examens.php");
    // }else {
    //   header("Location:../locale/examens.php");
    // }

          $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
          $typeetab=$etab->DetermineTypeEtab($codeEtab);

          if($typeetab==5)
          {
            header("Location:../locale".$libelleEtab."/examens.php");
          }else {
            header("Location:../locale/examens.php");
          }

    }


}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recuperation des variables

  $exam=htmlspecialchars($_POST['examen']);
  $datedeb=dateFormat($_POST['datedeb']);
  $datefin=dateFormat($_POST['datefin']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $libellesession=htmlspecialchars($_POST['libellesession']);
  $typesess=htmlspecialchars($_POST['typesess']);

//inserion dans la table examens

  $etab->AddExamenSchoolMixte($exam,$datedeb,$datefin,$codeEtab,$libellesession,$typesess);

  // $_SESSION['user']['updateExamok']="Un nouvel Examen a été ajouté avec succès";
  $_SESSION['user']['updateExamok']=L::ExamenAddMessageSuccess;

  $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);

  if($_SESSION['user']['profile'] == "Admin_globale") {


        header("Location:../manager/examens.php?codeEtab=".$codeEtab);

    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      // if($_SESSION['user']['paysid']==4)
      // {
      //   header("Location:../localeclcmr/examens.php");
      // }else {
      //   header("Location:../locale".$libelleEtab."/examens.php");
      // }

            $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etab->DetermineTypeEtab($codeEtab);

            if($typeetab==5)
            {
              header("Location:../locale".$libelleEtab."/examens.php");
            }else {
              header("Location:../locale/examens.php");
            }

      }

}

if(isset($_GET['etape'])&&($_GET['etape']==3))
{
    //recupération des variables
    $examen=htmlspecialchars($_GET['examen']);
    $codeEtab=htmlspecialchars($_GET['codeEtab']);

    $etab->supprimerExamenOfthisSchool($examen,$codeEtab);

    $_SESSION['user']['updateExamok']="Examen supprimer avec succès";

    if($_SESSION['user']['profile'] == "Admin_globale") {

          //header("Location:../manager/index.php");
          header("Location:../manager/examens.php");

      }else if($_SESSION['user']['profile'] == "Admin_locale") {
        if($_SESSION['user']['paysid']==4)
        {
          header("Location:../localecmr/examens.php");
        }else {
          header("Location:../locale/examens.php");
        }


        }

}else if(isset($_GET['etape'])&&($_GET['etape']==4))
{
    //recupération des variables
    $examen=htmlspecialchars($_GET['examen']);
    $codeEtab=htmlspecialchars($_GET['codeEtab']);

    $etab->supprimerExamenOfthisSchoolMixte($examen,$codeEtab);

    // $_SESSION['user']['updateExamok']="L'Examen a été supprimé avec succès";
    $_SESSION['user']['updateExamok']=L::ExamenDeletedMessageSuccess;

    if($_SESSION['user']['profile'] == "Admin_globale") {

          //header("Location:../manager/index.php");
          header("Location:../manager/examens.php");

      }else if($_SESSION['user']['profile'] == "Admin_locale") {
        if($_SESSION['user']['paysid']==4)
        {
          header("Location:../localecmr/examens.php");
        }else {
          header("Location:../locale".$libelleEtab."/examens.php");
        }


        }

}


 ?>
