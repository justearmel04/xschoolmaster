<?php
session_start();
//require_once('../class/Matiere.php');
require_once('../class/Etablissement.php');
$etab = new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //ajout du diplôme

  //recupération des variables
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $teatcher=htmlspecialchars(addslashes($_POST['teatcher']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $coefficient=htmlspecialchars(addslashes($_POST['coef']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));


  //inscription dans la table matiere et enseigner

  $etab->AddMatiere($matiere,$classe,$teatcher,$codeEtab,$coefficient,$libellesession);




}if(isset($_POST['etape'])&&($_POST['etape']==2))
{
    //recupération des variables
    $idmat=htmlspecialchars(addslashes($_POST['idmat']));
    $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
    $matiere=htmlspecialchars(addslashes($_POST['matiere'.$idmat]));
    $classe=htmlspecialchars(addslashes($_POST['classe'.$idmat]));
    $teatcher=htmlspecialchars(addslashes($_POST['teatcher'.$idmat]));
    $coefficient=htmlspecialchars(addslashes($_POST['coef'.$idmat]));

    //update de la table matiere et dispenser

    $etab->UpdateMatiere($matiere,$classe,$teatcher,$codeEtab,$coefficient,$idmat);


}if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //recuperation des variables

  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  // $classe=htmlspecialchars(addslashes($_POST['classe']));
  $teatcher=htmlspecialchars(addslashes($_POST['teatcher']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $coefficient=htmlspecialchars(addslashes($_POST['coef']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

  foreach($_POST['classe'] as $valeur)
 {
   $classe=$valeur;

    $etab->AddMatiereBoucle($matiere,$classe,$teatcher,$codeEtab,$coefficient,$libellesession);

 }

 $_SESSION['user']['addsubjectok']="Nouvelle Matière ajoutée avec succès";

  // header("Location:../manager/matieres.php?classe=".$classe);
    $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);

  if($_SESSION['user']['profile'] == "Admin_globale") {


        header("Location:../manager/schoolInfos.php?compte=".$codeEtab);

    }else if($_SESSION['user']['profile'] == "Admin_locale") {

      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/matieres.php");
      }else {
        header("Location:../locale".$libelleEtab."/matieres.php");
      }


      }

}if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //recuperation des variables

  $matierelib=htmlspecialchars(addslashes(utf8_encode($_POST['matierelib'])));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $session=htmlspecialchars(addslashes($_POST['libellesession']));

  // echo $matierelib;

    $etab->AddMatiereLibelle($matierelib,$codeEtab,$session);

}

if(isset($_GET['etape'])&&($_GET['etape']==3))
{
  //il est question de supprimer la matière

  //recupération des variables

  $classe=htmlspecialchars(addslashes($_GET['classe']));
  $idmat=htmlspecialchars(addslashes($_GET['matiere']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));

    $etab->DelatedMatiere($idmat,$classe,$codeEtab);

}

?>
