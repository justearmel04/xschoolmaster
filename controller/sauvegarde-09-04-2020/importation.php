<?php
session_start();
// require_once('../class/Student.php');
// require_once('../class/Sessionsacade.php');
//
require_once('../class/Etablissement.php');
require_once('../class/Importation.php');
require_once('../controller/functions.php');
// $student = new Student();
// $sessionX= new Sessionacade();
$etabs=new Etab();
$imports=new Import();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //RECUPERATION DES VARIABLES

  $classeid=htmlspecialchars($_POST['classeid']);
  $concatcodeEtab=htmlspecialchars($_POST['codeEtab']);
  $pays=htmlspecialchars($_POST['pays']);
  $tabconcat=explode("*",$concatcodeEtab);
  $codeEtab=$tabconcat[0];
  $sessionEtab=$tabconcat[1];
  $userid=$_SESSION['user']['IdCompte'];

  $fileError = $_FILES['fichier1']['error'];

  if($fileError==0)
  {
    // echo "fichier";

    $file_name = @$_FILES['fichier1']['name'];
    $_SESSION["fichier1"] = $file_name;
    $file_size =@$_FILES['fichier1']['size'];
    $file_tmp =@$_FILES['fichier1']['tmp_name'];
    $file_type=@$_FILES['fichier1']['type'];
    @$file_ext=strtolower(end(explode('.',@$_FILES['fichier1']['name'])));
    $fichierTemp = uniqid() . "." . $file_ext;

    $date=date("Y-m-d");

    $tabdate=explode("-",$date);
    $years=$tabdate[0];
    $mois=$tabdate[1];
    $days=$tabdate[2];
    $libellemois=obtenirLibelleMois($mois);

    //insertion dans la table importation

    $importationid=$imports->AddImportationFile($classeid,$codeEtab,$sessionEtab,$pays,$userid);

    $transactionId =  "IMPORT_".date("Y")."_".$codeEtab."_".$classeid."_".$importationid;
    $fichierad=$transactionId.".".$file_ext;
    // echo $fichierad;

    $imports->UpdateImportationFileName($fichierad,$importationid);

      move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

      $dossier="../importations/Etablissements/";
      $dossier1="../importations/Etablissements/".$codeEtab."/";
      $dossier2="../importations/Etablissements/".$codeEtab."/Classes/";

      if(!is_dir($dossier)) {
          @mkdir($dossier);
          if(!is_dir($dossier1)) {
              @mkdir($dossier1);

              if(!is_dir($dossier2)) {
                  @mkdir($dossier2);

              }

          }
      }else {
        if(!is_dir($dossier1)) {
            @mkdir($dossier1);

            if(!is_dir($dossier2)) {
                @mkdir($dossier2);

            }

        }
      }

      @rename('../temp/' . $fichierTemp ,"../importations/Etablissements/".$codeEtab."/Classes/".$fichierad);

     //Suppression du fichier se trouvant dans le dossier temp

      @unlink("../temp/" . $fichierTemp);

      //nous allons faire le traitement du fichier

      $i = 0 ;

                   if (($handle = fopen("../importations/Etablissements/".$codeEtab."/Classes/".$fichierad, "r")) !== FALSE) {

                     $tabparents=array();
                     $tabstudents=array();
                     $tabparenter=array();

                    while (($cont  = fgetcsv($handle,1000,";")) !== false) {




                      if($cont[0]=="parent")
                      {
                        //insertion dans la table parent

                        // echo "cas parent"."</br>";

                        //nous allons verifier si le parent n'exite pas deja dans le systeme




                        $typeCompte="Parent";
                        $dateday=date("Y-m-d");
                        $nom=$cont[2];
                        $prenom=$cont[3];
                        $datenais=dateFormat($cont[4]);
                        $lieunais=$cont[5];
                        $mobile=$cont[6];
                        $email=$cont[7];
                        $fonction=$cont[8];
                        $cni=$cont[9];
                        $nationalite=$cont[10];
                        $lieuH=$cont[11];
                        $situation=$cont[12];
                        $nbchild=$cont[13];
                        $sexe=$cont[17];
                        $societe=$cont[18];
                        $adressepostale=$cont[19];
                        $telburo=$cont[20];
                        // $codeEtab=$cont[24];
                        $statut=$cont[16];

                        // echo $cont[24];

                        $nbphone=$imports->parentphonechecker($mobile);
                        // if($nbphone==0)
                        // {
                        //
                        //
                        // }else {
                        //   // nous allons recuperer l'id de ce parent
                        // }

                        $idparent=$imports->AddparentExcelFile($nom,$prenom,$datenais,$lieunais,$mobile,$email,$fonction,$cni,$nationalite,$lieuH,$situation,$nbchild,$sexe,$societe,$adressepostale,$telburo,$codeEtab,$dateday,$typeCompte,$statut);
                        $tabparents[]=$idparent;




                      }else if($cont[0]=="eleve")
                      {
                        // echo "cas eleve"."</br>";

                        //insertion dans l'eleve

                        $typeCompte="Student";
                        $dateday=date("Y-m-d");
                        $matricule=$cont[1];
                        $nom=$cont[2];
                        $prenom=$cont[3];
                        $datenais=dateFormat($cont[4]);
                        $lieunais=$cont[5];
                        $mobile=$cont[6];
                        $email=$cont[7];
                        $fonction=$cont[8];
                        $cni=$cont[9];
                        $nationalite=$cont[10];
                        $lieuH=$cont[11];
                        $sexe=$cont[17];
                        $adressepostale=$cont[19];
                        // $codeEtab=$cont[24];
                        // $codeEtab=$codeEtab;
                        $statut=$cont[16];
                        $doublant=$cont[21];
                        $daffecter=$cont[22];
                        $classe=$cont[23];
                        // $classe=$classeid;
                        // $sessionEtab=$cont[25];
                        // $sessionEtab=$sessionEtab;

                        $studentid=$imports->AddStudent($matricule,$nom,$prenom,$datenais,$lieunais,$mobile,$email,$fonction,$cni,$nationalite,$lieuH,$sexe,$adressepostale,$doublant,$daffecter,$classeid,$codeEtab,$sessionEtab,$typeCompte,$dateday,$statut);

                        // echo $studentid."  </br>";

                    $nbtabparent=count($tabparents);
                    if($nbtabparent>0)
                    {
                      for($i=0;$i<$nbtabparent;$i++)
                      {
                        $parentid=$tabparents[$i];

                        $imports->Addparenter($parentid,$studentid);

                      }
                    }

                    // var_dump($tabparents);
                    // echo "</br>";

                    $tabparents=array();

                      }


                       $i++;





                    }

                    // var_dump($tabparents);
				   }


    // echo $userid;



  }else {
    echo "aucun fichier";
  }



  $_SESSION['user']['addetabok']="Les données ont été importées avec succès";

  header("Location:../manager/importations.php");


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{

  $classeid=htmlspecialchars($_POST['classeid']);
  $concatcodeEtab=htmlspecialchars($_POST['codeEtab']);
  $pays=htmlspecialchars($_POST['pays']);
  $tabconcat=explode("*",$concatcodeEtab);
  $codeEtab=$tabconcat[0];
  $sessionEtab=$tabconcat[1];
  $userid=$_SESSION['user']['IdCompte'];

  $fileError = $_FILES['fichier1']['error'];

  if($fileError==0)
  {
    // echo "fichier";

    $file_name = @$_FILES['fichier1']['name'];
    $_SESSION["fichier1"] = $file_name;
    $file_size =@$_FILES['fichier1']['size'];
    $file_tmp =@$_FILES['fichier1']['tmp_name'];
    $file_type=@$_FILES['fichier1']['type'];
    @$file_ext=strtolower(end(explode('.',@$_FILES['fichier1']['name'])));
    $fichierTemp = uniqid() . "." . $file_ext;

    $date=date("Y-m-d");

    $tabdate=explode("-",$date);
    $years=$tabdate[0];
    $mois=$tabdate[1];
    $days=$tabdate[2];
    $libellemois=obtenirLibelleMois($mois);

    //insertion dans la table importation

    $importationid=$imports->AddImportationFile($classeid,$codeEtab,$sessionEtab,$pays,$userid);

    $transactionId =  "IMPORT_".date("Y")."_".$codeEtab."_".$classeid."_".$importationid;
    $fichierad=$transactionId.".".$file_ext;
    // echo $fichierad;

    $imports->UpdateImportationFileName($fichierad,$importationid);

      move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

      $dossier="../importations/Etablissements/";
      $dossier1="../importations/Etablissements/".$codeEtab."/";
      $dossier2="../importations/Etablissements/".$codeEtab."/Classes/";

      if(!is_dir($dossier)) {
          @mkdir($dossier);
          if(!is_dir($dossier1)) {
              @mkdir($dossier1);

              if(!is_dir($dossier2)) {
                  @mkdir($dossier2);

              }

          }
      }else {
        if(!is_dir($dossier1)) {
            @mkdir($dossier1);

            if(!is_dir($dossier2)) {
                @mkdir($dossier2);

            }

        }
      }

      @rename('../temp/' . $fichierTemp ,"../importations/Etablissements/".$codeEtab."/Classes/".$fichierad);

     //Suppression du fichier se trouvant dans le dossier temp

      @unlink("../temp/" . $fichierTemp);

      //nous allons faire le traitement du fichier

      $i = 0 ;

                   if (($handle = fopen("../importations/Etablissements/".$codeEtab."/Classes/".$fichierad, "r")) !== FALSE) {

                     $tabparents=array();
                     $tabstudents=array();
                     $tabparenter=array();

                    while (($cont  = fgetcsv($handle,1000,";")) !== false) {




                      if($cont[0]=="parent")
                      {
                        //insertion dans la table parent

                        // echo "cas parent"."</br>";

                        //nous allons verifier si le parent n'exite pas deja dans le systeme




                        $typeCompte="Parent";
                        $dateday=date("Y-m-d");
                        $nom=$cont[2];
                        $prenom=$cont[3];
                        $datenais=dateFormat($cont[4]);
                        $lieunais=$cont[5];
                        $mobile=$cont[6];
                        $email=$cont[7];
                        $fonction=$cont[8];
                        $cni=$cont[9];
                        $nationalite=$cont[10];
                        $lieuH=$cont[11];
                        $situation=$cont[12];
                        $nbchild=$cont[13];
                        $sexe=$cont[17];
                        $societe=$cont[18];
                        $adressepostale=$cont[19];
                        $telburo=$cont[20];
                        // $codeEtab=$cont[24];
                        $statut=$cont[16];

                        // echo $cont[24];

                        $nbphone=$imports->parentphonechecker($mobile);
                        // if($nbphone==0)
                        // {
                        //
                        //
                        // }else {
                        //   // nous allons recuperer l'id de ce parent
                        // }

                        $idparent=$imports->AddparentExcelFile($nom,$prenom,$datenais,$lieunais,$mobile,$email,$fonction,$cni,$nationalite,$lieuH,$situation,$nbchild,$sexe,$societe,$adressepostale,$telburo,$codeEtab,$dateday,$typeCompte,$statut);
                        $tabparents[]=$idparent;




                      }else if($cont[0]=="eleve")
                      {
                        // echo "cas eleve"."</br>";

                        //insertion dans l'eleve

                        $typeCompte="Student";
                        $dateday=date("Y-m-d");
                        $matricule=$cont[1];
                        $nom=$cont[2];
                        $prenom=$cont[3];
                        $datenais=dateFormat($cont[4]);
                        $lieunais=$cont[5];
                        $mobile=$cont[6];
                        $email=$cont[7];
                        $fonction=$cont[8];
                        $cni=$cont[9];
                        $nationalite=$cont[10];
                        $lieuH=$cont[11];
                        $sexe=$cont[17];
                        $adressepostale=$cont[19];
                        // $codeEtab=$cont[24];
                        // $codeEtab=$codeEtab;
                        $statut=$cont[16];
                        $doublant=$cont[21];
                        $daffecter=$cont[22];
                        $classe=$cont[23];
                        // $classe=$classeid;
                        // $sessionEtab=$cont[25];
                        // $sessionEtab=$sessionEtab;

                        $studentid=$imports->AddStudent($matricule,$nom,$prenom,$datenais,$lieunais,$mobile,$email,$fonction,$cni,$nationalite,$lieuH,$sexe,$adressepostale,$doublant,$daffecter,$classeid,$codeEtab,$sessionEtab,$typeCompte,$dateday,$statut);

                        // echo $studentid."  </br>";

                    $nbtabparent=count($tabparents);
                    if($nbtabparent>0)
                    {
                      for($i=0;$i<$nbtabparent;$i++)
                      {
                        $parentid=$tabparents[$i];

                        $imports->Addparenter($parentid,$studentid);

                      }
                    }

                    // var_dump($tabparents);
                    // echo "</br>";

                    $tabparents=array();

                      }


                       $i++;





                    }

                    // var_dump($tabparents);
				   }


    // echo $userid;



  }else {
    echo "aucun fichier";
  }

  $_SESSION['user']['addetabok']="Les données ont été importées avec succès";

  if($_SESSION['user']['paysid']==4)
  {
    header("Location:../localecmr/importations.php");
  }else {
    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

    $etablissementType=$etabs->DetermineTypeEtab($codeEtab);

    if($etablissementType==5)
    {
        header("Location:../locale".$libelleEtab."/importations.php");
    }else {
      header("Location:../locale/importations.php");
    }


  }



}




?>
