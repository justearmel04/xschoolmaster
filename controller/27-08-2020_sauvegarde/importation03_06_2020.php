<?php
session_start();
// require_once('../class/Student.php');
// require_once('../class/Sessionsacade.php');
//
require_once('../class/Etablissement.php');
require_once('../class/Classe.php');
require_once('../class/Teatcher.php');
require_once('../class/Matiere.php');
require_once('../class/Importation.php');
require_once('../controller/functions.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();

// $student = new Student();
// $sessionX= new Sessionacade();
$etabs=new Etab();
$imports=new Import();
$classes=new Classe();
$teatcher=new Teatcher();
$matiere=new Matiere();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //RECUPERATION DES VARIABLES

  $classeid=htmlspecialchars($_POST['classeid']);
  $concatcodeEtab=htmlspecialchars($_POST['codeEtab']);
  $pays=htmlspecialchars($_POST['pays']);
  $tabconcat=explode("*",$concatcodeEtab);
  $codeEtab=$tabconcat[0];
  $sessionEtab=$tabconcat[1];
  $userid=$_SESSION['user']['IdCompte'];
  $datatype=htmlspecialchars($_POST['datatype']);

  $fileError = $_FILES['fichier1']['error'];

  if($fileError==0)
  {
    // echo "fichier";

    if($datatype=="PARENTS-ELEVES")
    {

      //importations fichier parents-eleves

      $file_name = @$_FILES['fichier1']['name'];
      $_SESSION["fichier1"] = $file_name;
      $file_size =@$_FILES['fichier1']['size'];
      $file_tmp =@$_FILES['fichier1']['tmp_name'];
      $file_type=@$_FILES['fichier1']['type'];
      @$file_ext=strtolower(end(explode('.',@$_FILES['fichier1']['name'])));
      $fichierTemp = uniqid() . "." . $file_ext;

      $date=date("Y-m-d");

      $tabdate=explode("-",$date);
      $years=$tabdate[0];
      $mois=$tabdate[1];
      $days=$tabdate[2];
      $libellemois=obtenirLibelleMois($mois);

      //insertion dans la table importation

      $importationid=$imports->AddImportationFile($classeid,$codeEtab,$sessionEtab,$pays,$userid,$datatype);

      $transactionId =  "IMPORT_".date("Y")."_".$codeEtab."_".$classeid."_".$importationid;
      $fichierad=$transactionId.".".$file_ext;
      // echo $fichierad;

      $imports->UpdateImportationFileName($fichierad,$importationid);

        move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

        $dossier="../importations/Etablissements/";
        $dossier1="../importations/Etablissements/".$codeEtab."/";
        $dossier2="../importations/Etablissements/".$codeEtab."/Classes/";

        if(!is_dir($dossier)) {
            @mkdir($dossier);
            if(!is_dir($dossier1)) {
                @mkdir($dossier1);

                if(!is_dir($dossier2)) {
                    @mkdir($dossier2);

                }

            }
        }else {
          if(!is_dir($dossier1)) {
              @mkdir($dossier1);

              if(!is_dir($dossier2)) {
                  @mkdir($dossier2);

              }

          }
        }

        @rename('../temp/' . $fichierTemp ,"../importations/Etablissements/".$codeEtab."/Classes/".$fichierad);

       //Suppression du fichier se trouvant dans le dossier temp

        @unlink("../temp/" . $fichierTemp);

        //nous allons faire le traitement du fichier

        $i = 0 ;

                     if (($handle = fopen("../importations/Etablissements/".$codeEtab."/Classes/".$fichierad, "r")) !== FALSE) {

                       $tabparents=array();
                       $tabstudents=array();
                       $tabparenter=array();

                      while (($cont  = fgetcsv($handle,1000,";")) !== false) {




                        if($cont[0]=="parent")
                        {
                          //insertion dans la table parent

                          // echo "cas parent"."</br>";

                          //nous allons verifier si le parent n'exite pas deja dans le systeme




                          $typeCompte="Parent";
                          $dateday=date("Y-m-d");
                          $nom=$cont[2];
                          $prenom=$cont[3];
                          $datenais=dateFormat($cont[4]);
                          $lieunais=$cont[5];
                          $mobile=$cont[6];
                          $email=$cont[7];
                          $fonction=$cont[8];
                          $cni=$cont[9];
                          $nationalite=$cont[10];
                          $lieuH=$cont[11];
                          $situation=$cont[12];
                          $nbchild=$cont[13];
                          $sexe=$cont[17];
                          $societe=$cont[18];
                          $adressepostale=$cont[19];
                          $telburo=$cont[20];
                          // $codeEtab=$cont[24];
                          $statut=$cont[16];

                          // echo $cont[24];

                          $nbphone=$imports->parentphonechecker($mobile);
                          // if($nbphone==0)
                          // {
                          //
                          //
                          // }else {
                          //   // nous allons recuperer l'id de ce parent
                          // }

                          $idparent=$imports->AddparentExcelFile($nom,$prenom,$datenais,$lieunais,$mobile,$email,$fonction,$cni,$nationalite,$lieuH,$situation,$nbchild,$sexe,$societe,$adressepostale,$telburo,$codeEtab,$dateday,$typeCompte,$statut);
                          $tabparents[]=$idparent;




                        }else if($cont[0]=="eleve")
                        {
                          // echo "cas eleve"."</br>";

                          //insertion dans l'eleve

                          $typeCompte="Student";
                          $dateday=date("Y-m-d");
                          $matricule=$cont[1];
                          $nom=$cont[2];
                          $prenom=$cont[3];
                          $datenais=dateFormat($cont[4]);
                          $lieunais=$cont[5];
                          $mobile=$cont[6];
                          $email=$cont[7];
                          $fonction=$cont[8];
                          $cni=$cont[9];
                          $nationalite=$cont[10];
                          $lieuH=$cont[11];
                          $sexe=$cont[17];
                          $adressepostale=$cont[19];
                          // $codeEtab=$cont[24];
                          // $codeEtab=$codeEtab;
                          $statut=$cont[16];
                          $doublant=$cont[21];
                          $daffecter=$cont[22];
                          $classe=$cont[23];
                          // $classe=$classeid;
                          // $sessionEtab=$cont[25];
                          // $sessionEtab=$sessionEtab;

                          $studentid=$imports->AddStudent($matricule,$nom,$prenom,$datenais,$lieunais,$mobile,$email,$fonction,$cni,$nationalite,$lieuH,$sexe,$adressepostale,$doublant,$daffecter,$classeid,$codeEtab,$sessionEtab,$typeCompte,$dateday,$statut);

                          // echo $studentid."  </br>";

                      $nbtabparent=count($tabparents);
                      if($nbtabparent>0)
                      {
                        for($i=0;$i<$nbtabparent;$i++)
                        {
                          $parentid=$tabparents[$i];

                          $imports->Addparenter($parentid,$studentid);

                        }
                      }

                      // var_dump($tabparents);
                      // echo "</br>";

                      $tabparents=array();

                        }


                         $i++;





                      }

                      // var_dump($tabparents);
             }


           }else if($datatype=="CLASSE-ENSEIGNANT-MATIERE")
           {
             // importation CLASSE-ENSEIGNANT-MATIERE

             $file_name = @$_FILES['fichier1']['name'];
             $_SESSION["fichier1"] = $file_name;
             $file_size =@$_FILES['fichier1']['size'];
             $file_tmp =@$_FILES['fichier1']['tmp_name'];
             $file_type=@$_FILES['fichier1']['type'];
             @$file_ext=strtolower(end(explode('.',@$_FILES['fichier1']['name'])));
             $fichierTemp = uniqid() . "." . $file_ext;

             $date=date("Y-m-d");

             $tabdate=explode("-",$date);
             $years=$tabdate[0];
             $mois=$tabdate[1];
             $days=$tabdate[2];
             $libellemois=obtenirLibelleMois($mois);

             // $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

             move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

             $i = 0 ;

             if (($handle = fopen("../temp/".$fichierTemp, "r")) !== FALSE) {

               $tabclasses=array();
               $tabteatchers=array();
               $tabmatieres=array();
               $concatclasses=array();
               $concatteatchers=array();

               while (($cont  = fgetcsv($handle,1000,";")) !== false) {

                 if($cont[0]=="classe")
                 {

                   //nous allons verifier si la classe existe deja dans le systeme

                   $libelleclasse=$cont[18];
                   $montantinscrip=$cont[19];
                   $montantscola=$cont[20];
                   $montantAES=0;
                   $typeclasse=$cont[22];
                   $scolariteaff=$cont[21];

                   $existeclasse=$classes->ExisteClasses($codeEtab,$libelleclasse,$sessionEtab);

                   if($existeclasse==0)
                   {
                     //la classe n'existe pas nous allons l'ajouter
                     $classeid=$classes->AddclasseSingleOne($libelleclasse,$codeEtab,$sessionEtab,$montantinscrip,$montantscola,$montantAES,$typeclasse,$scolariteaff);
                     $tabclasses[]=$classeid;
                     $concatclasses[]=$classeid;



                   }else {
                     // la classe existe nous allons recuperer son id
                     $classeid=$classes->getClassesId($libelleEtab,$sessionEtab,$codeEtab);
                     $tabclasses[]=$classeid;
                     $concatclasses[]=$classeid;
                   }




                 }else if($cont[0]=="enseignant")
                 {
                   //nous allons verifier si nous avons un enseignant avec ce matricule

                   $matricule=$cont[1];

                   $existeteatcher=$teatcher->ExisteTeatcherInschool($matricule);

                   // $concatteatchers

                   if($existeteatcher==0)
                   {
                     //le professeur n'existe pas alors nous le créons

                     //recuperation des variables

                     $nomTea=$cont[4];
                     $prenomTea=$cont[5];
                     $datenaisTea=$cont[6];
                     $sexeTea=$cont[8];
                     $contactTea=$cont[9];
                     $dateEmbTea=$cont[13];
                     $emailTea=$cont[10];
                     $loginTea="";
                     $passTea="";
                     $fonction="";
                     $lieunaisTea=$cont[7];
                     $natTea=$cont[12];
                     $situationTea=$cont[11];
                     $nbchildTea=$cont[14];
                     $cnpsTea=$cont[2];
                     $matTea=$cont[1];
                     $mutuelTea=$cont[3];
                     $brutTea=$cont[15];
                     $netTea=$cont[16];
                     $libetab=$codeEtab;
                     $typeTea=$cont[17];
                     $session=$sessionEtab;
                     $matTea=$matricule;

                     if($matTea=="")
                     {
                       $tabnais=explode("-",$datenaisTea);
                       $concatteatchername=$nomTea.$prenomTea;
                       //le nolbre de Professeur
                       $nbteactherx=$teatcher->getNumberOfSystemTeatcher();
                       $newnbteatcherx=$nbteactherx+1;
                       //creation du matricule suivant le format
                       $matTea="MAT".date("y").$tabnais[1].rand_chars($concatteatchername,1).$newnbteatcherx;
                     }

                     $datecrea=date("Y-m-d");
                     $type_cpte="Teatcher";
                     $statut=1;

                     $teatcherid=$teatcher->AddTeatcherwithoutFileSMixte($nomTea,$prenomTea,$emailTea,$sexeTea,$statut,$dateEmbTea,$datenaisTea,$contactTea,$fonction,$loginTea,$passTea,$type_cpte,$datecrea,$lieunaisTea,$natTea,$situationTea,$nbchildTea,$cnpsTea,$matTea,$mutuelTea,$brutTea,$netTea,$codeEtab,$typeTea,$sessionEtab);
                     $concatteatchers[]=$teatcherid;
                     $tabteatchers[]=$teatcherid;

                   }else {
                     // le professeur existe alirs nous recuperons son id

                     //nous allons recuperer l'id du professeur

                     $teatcherid=$teatcher->getIdTeatcherByMatricule($matTea);
                     $concatteatchers[]=$teatcherid;
                     $tabteatchers[]=$teatcherid;

                   }



                 }else if($cont[0]=="matiere")
                 {
                   //nous allons verifier si cette matiere existe deja pour cette classe
                   $libelleMat=$cont[23];
                   $coefMat=$cont[24];

                   $nbclasses=count($tabclasses);

                   for($i=0;$i<$nbclasses;$i++)
                   {
                     $existeMatiere=$matiere->existeMatiere($libelleMat,$codeEtab,$sessionEtab,$tabteatchers[0],$tabclasses[$i]);
                     if($existeMatiere==0)
                     {
                       //nous allons ajouter la matiere

                       $nbmatierelib=$etabs->ExisteMatiereLib($libelleMat,$codeEtab,$sessionEtab);

                       if($nbmatierelib>0)
                       {
                          $etabs->AddMatiereOne($libelleMat,$tabclasses[$i],$tabteatchers[0],$codeEtab,$coefMat,$sessionEtab);
                       }else {
                         $etabs->AddMatiereLibelle($libelleMat,$codeEtab,$sessionEtab);
                         $etabs->AddMatiereOne($libelleMat,$tabclasses[$i],$tabteatchers[0],$codeEtab,$coefMat,$sessionEtab);
                       }



                     }else {
                       // la matiere existe deja dans le système
                     }

                     $importationid=$imports->AddImportationFile($tabclasses[$i],$codeEtab,$sessionEtab,$pays,$userid,$datatype);

                     $transactionId =  "IMPORT_".date("Y")."_".$codeEtab."_".$tabclasses[$i]."_".$importationid;
                     $fichierad=$transactionId.".".$file_ext;
                     // echo $fichierad;

                     $imports->UpdateImportationFileName($fichierad,$importationid);

                       move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

                       $dossier="../importations/Etablissements/";
                       $dossier1="../importations/Etablissements/".$codeEtab."/";
                       $dossier2="../importations/Etablissements/".$codeEtab."/Classes/";

                       if(!is_dir($dossier)) {
                           @mkdir($dossier);
                           if(!is_dir($dossier1)) {
                               @mkdir($dossier1);

                               if(!is_dir($dossier2)) {
                                   @mkdir($dossier2);

                               }

                           }
                       }else {
                         if(!is_dir($dossier1)) {
                             @mkdir($dossier1);

                             if(!is_dir($dossier2)) {
                                 @mkdir($dossier2);

                             }

                         }
                       }

                       @rename('../temp/' . $fichierTemp ,"../importations/Etablissements/".$codeEtab."/Classes/".$fichierad);

                      //Suppression du fichier se trouvant dans le dossier temp

                       @unlink("../temp/" . $fichierTemp);


                   }





                   $tabclasses=array();
                   $tabmatieres=array();
                   $tabteatchers=array();
                 }
               }


             }


           }




    // echo $userid;



  }else {
    echo "aucun fichier";
  }



  // $_SESSION['user']['addetabok']="Les données ont été importées avec succès";
  $_SESSION['user']['addetabok']=L::DataImportationAddMessageSuccess;

  // header("Location:../manager/importations.php");


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{

  $classeid=htmlspecialchars($_POST['classeid']);
  $concatcodeEtab=htmlspecialchars($_POST['codeEtab']);
  $pays=htmlspecialchars($_POST['pays']);
  $tabconcat=explode("*",$concatcodeEtab);
  $codeEtab=$tabconcat[0];
  $sessionEtab=$tabconcat[1];
  $userid=$_SESSION['user']['IdCompte'];
  $datatype=htmlspecialchars($_POST['datatype']);

  // echo $datatype;

  $fileError = $_FILES['fichier1']['error'];

  if($fileError==0)
  {
    // echo "fichier";

    if($datatype=="PARENTS-ELEVES")
    {
      $file_name = @$_FILES['fichier1']['name'];
      $_SESSION["fichier1"] = $file_name;
      $file_size =@$_FILES['fichier1']['size'];
      $file_tmp =@$_FILES['fichier1']['tmp_name'];
      $file_type=@$_FILES['fichier1']['type'];
      @$file_ext=strtolower(end(explode('.',@$_FILES['fichier1']['name'])));
      $fichierTemp = uniqid() . "." . $file_ext;

      $date=date("Y-m-d");

      $tabdate=explode("-",$date);
      $years=$tabdate[0];
      $mois=$tabdate[1];
      $days=$tabdate[2];
      $libellemois=obtenirLibelleMois($mois);

      //insertion dans la table importation

      $importationid=$imports->AddImportationFile($classeid,$codeEtab,$sessionEtab,$pays,$userid,$datatype);

      $transactionId =  "IMPORT_".date("Y")."_".$codeEtab."_".$classeid."_".$importationid;
      $fichierad=$transactionId.".".$file_ext;
      // echo $fichierad;

      $imports->UpdateImportationFileName($fichierad,$importationid);

        move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

        $dossier="../importations/Etablissements/";
        $dossier1="../importations/Etablissements/".$codeEtab."/";
        $dossier2="../importations/Etablissements/".$codeEtab."/Classes/";

        if(!is_dir($dossier)) {
            @mkdir($dossier);
            if(!is_dir($dossier1)) {
                @mkdir($dossier1);

                if(!is_dir($dossier2)) {
                    @mkdir($dossier2);

                }

            }
        }else {
          if(!is_dir($dossier1)) {
              @mkdir($dossier1);

              if(!is_dir($dossier2)) {
                  @mkdir($dossier2);

              }

          }
        }

        @rename('../temp/' . $fichierTemp ,"../importations/Etablissements/".$codeEtab."/Classes/".$fichierad);

       //Suppression du fichier se trouvant dans le dossier temp

        @unlink("../temp/" . $fichierTemp);

        //nous allons faire le traitement du fichier

        $w = 0 ;

                     if (($handle = fopen("../importations/Etablissements/".$codeEtab."/Classes/".$fichierad, "r")) !== FALSE) {

                       $tabparents=array();
                       $tabstudents=array();
                       $tabparenter=array();

                      while (($cont  = fgetcsv($handle,1000,";")) !== false) {


                        if($w>0)
                        {
                          if($cont[0]=="parent")
                          {
                            //insertion dans la table parent

                            // echo "cas parent"."</br>";

                            //nous allons verifier si le parent n'exite pas deja dans le systeme




                            $typeCompte="Parent";
                            $dateday=date("Y-m-d");
                            $nom=$cont[2];
                            $prenom=$cont[3];
                            $datenais=dateFormat($cont[4]);
                            $lieunais=$cont[5];
                            $mobile=$cont[6];
                            $email=$cont[7];
                            $fonction=$cont[8];
                            $cni=$cont[9];
                            $nationalite=$cont[10];
                            $lieuH=$cont[11];
                            $situation=$cont[12];
                            $nbchild=$cont[13];
                            $sexe=$cont[17];
                            $societe=$cont[18];
                            $adressepostale=$cont[19];
                            $telburo=$cont[20];
                            // $codeEtab=$cont[24];
                            $statut=$cont[16];

                            // echo $cont[24];

                            $nbphone=$imports->parentphonechecker($mobile);
                            // if($nbphone==0)
                            // {
                            //
                            //
                            // }else {
                            //   // nous allons recuperer l'id de ce parent
                            // }

                            $idparent=$imports->AddparentExcelFile($nom,$prenom,$datenais,$lieunais,$mobile,$email,$fonction,$cni,$nationalite,$lieuH,$situation,$nbchild,$sexe,$societe,$adressepostale,$telburo,$codeEtab,$dateday,$typeCompte,$statut);
                            $tabparents[]=$idparent;




                          }else if($cont[0]=="eleve")
                          {
                            // echo "cas eleve"."</br>";

                            //insertion dans l'eleve

                            $typeCompte="Student";
                            $dateday=date("Y-m-d");
                            $matricule=$cont[1];
                            $nom=$cont[2];
                            $prenom=$cont[3];
                            $datenais=dateFormat($cont[4]);
                            $lieunais=$cont[5];
                            $mobile=$cont[6];
                            $email=$cont[7];
                            $fonction=$cont[8];
                            $cni=$cont[9];
                            $nationalite=$cont[10];
                            $lieuH=$cont[11];
                            $sexe=$cont[17];
                            $adressepostale=$cont[19];
                            // $codeEtab=$cont[24];
                            // $codeEtab=$codeEtab;
                            $statut=$cont[16];
                            $doublant=$cont[21];
                            $daffecter=$cont[22];
                            $classe=$cont[23];
                            // $classe=$classeid;
                            // $sessionEtab=$cont[25];
                            // $sessionEtab=$sessionEtab;

                            $studentid=$imports->AddStudent($matricule,$nom,$prenom,$datenais,$lieunais,$mobile,$email,$fonction,$cni,$nationalite,$lieuH,$sexe,$adressepostale,$doublant,$daffecter,$classeid,$codeEtab,$sessionEtab,$typeCompte,$dateday,$statut);

                            // echo $studentid."  </br>";

                        $nbtabparent=count($tabparents);
                        if($nbtabparent>0)
                        {
                          for($i=0;$i<$nbtabparent;$i++)
                          {
                            $parentid=$tabparents[$i];

                            $imports->Addparenter($parentid,$studentid);

                          }
                        }

                        // var_dump($tabparents);
                        // echo "</br>";

                        if($cont[27]==0)
                        {
                          $tabparents=array();
                        }else {
                          // code...
                        }



                          }
                        }




                         $w++;





                      }

                      // var_dump($tabparents);
  				   }

    }else if($datatype=="CLASSE-ENSEIGNANT-MATIERE")
    {
      // importation CLASSE-ENSEIGNANT-MATIERE

      $file_name = @$_FILES['fichier1']['name'];
      $_SESSION["fichier1"] = $file_name;
      $file_size =@$_FILES['fichier1']['size'];
      $file_tmp =@$_FILES['fichier1']['tmp_name'];
      $file_type=@$_FILES['fichier1']['type'];
      @$file_ext=strtolower(end(explode('.',@$_FILES['fichier1']['name'])));
      $fichierTemp = uniqid() . "." . $file_ext;

      $date=date("Y-m-d");

      $tabdate=explode("-",$date);
      $years=$tabdate[0];
      $mois=$tabdate[1];
      $days=$tabdate[2];
      $libellemois=obtenirLibelleMois($mois);

      $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

      move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

      $i = 0 ;

      if (($handle = fopen("../temp/".$fichierTemp, "r")) !== FALSE) {

        $tabclasses=array();
        $tabteatchers=array();
        $tabmatieres=array();
        $concatclasses=array();
        $concatteatchers=array();

        $indicetab=0;


        while (($cont  = fgetcsv($handle,1000,";")) !== false) {



          if($cont[0]=="classe")
          {

            //nous allons verifier si la classe existe deja dans le systeme

            $libelleclasse=$cont[18];
            $montantinscrip=$cont[19];
            $montantscola=$cont[20];
            $montantAES=0;
            $typeclasse=$cont[22];
            $scolariteaff=$cont[21];

            $existeclasse=$classes->ExisteClasses($codeEtab,$libelleclasse,$sessionEtab);

            if($existeclasse==0)
            {
              //la classe n'existe pas nous allons l'ajouter
              $classeid=$classes->AddclasseSingleOne($libelleclasse,$codeEtab,$sessionEtab,$montantinscrip,$montantscola,$montantAES,$typeclasse,$scolariteaff);
              $tabclasses[]=$classeid;
              $concatclasses[]=$classeid;



            }else {
              // la classe existe nous allons recuperer son id
              $classeid=$classes->getClassesId($libelleclasse,$sessionEtab,$codeEtab);
              $tabclasses[]=$classeid;
              $concatclasses[]=$classeid;
            }

            // echo "classe stock id classe </br> ";


          }else if($cont[0]=="enseignant")
          {
            //nous allons verifier si nous avons un enseignant avec ce matricule

            $matricule=$cont[1];

            $existeteatcher=$teatcher->ExisteTeatcherInschool($matricule);

            // $matTea=$matricule;

            // $concatteatchers

            if($existeteatcher==0)
            {
              //le professeur n'existe pas alors nous le créons

              //recuperation des variables

              $nomTea=$cont[4];
              $prenomTea=$cont[5];
              $datenaisTea=$cont[6];
              $sexeTea=$cont[8];
              $contactTea=$cont[9];
              $dateEmbTea=$cont[13];
              $emailTea=$cont[10];
              $loginTea="";
              $passTea="";
              $fonction="";
              $lieunaisTea=$cont[7];
              $natTea=$cont[12];
              $situationTea=$cont[11];
              $nbchildTea=$cont[14];
              $cnpsTea=$cont[2];
              $matTea=$cont[1];
              $mutuelTea=$cont[3];
              $brutTea=$cont[15];
              $netTea=$cont[16];
              $libetab=$codeEtab;
              $typeTea=$cont[17];
              $session=$sessionEtab;


              if($matTea=="")
              {
                $tabnais=explode("-",$datenaisTea);
                $concatteatchername=$nomTea.$prenomTea;
                //le nolbre de Professeur
                $nbteactherx=$teatcher->getNumberOfSystemTeatcher();
                $newnbteatcherx=$nbteactherx+1;
                //creation du matricule suivant le format
                $matTea="MAT".date("y").$tabnais[1].rand_chars($concatteatchername,1).$newnbteatcherx;
              }

              $datecrea=date("Y-m-d");
              $type_cpte="Teatcher";
              $statut=1;

              $teatcherid=$teatcher->AddTeatcherwithoutFileSMixte($nomTea,$prenomTea,$emailTea,$sexeTea,$statut,$dateEmbTea,$datenaisTea,$contactTea,$fonction,$loginTea,$passTea,$type_cpte,$datecrea,$lieunaisTea,$natTea,$situationTea,$nbchildTea,$cnpsTea,$matTea,$mutuelTea,$brutTea,$netTea,$codeEtab,$typeTea,$sessionEtab);
              $concatteatchers[]=$teatcherid;
              $tabteatchers[]=$teatcherid;

            }else {
              // le professeur existe alirs nous recuperons son id

              //nous allons recuperer l'id du professeur

              $teatcherid=$teatcher->getIdTeatcherByMatricule($matricule);
              $concatteatchers[]=$teatcherid;
              $tabteatchers[]=$teatcherid;

            }



          }else if($cont[0]=="matiere")
          {

            $libelleMat=$cont[23];
            $coefMat=$cont[24];
            $existeMatiere=$matiere->existeMatiere($libelleMat,$codeEtab,$sessionEtab,$teatcherid,$tabclasses[$indicetab]);
            if($existeMatiere==0)
            {
              //nous allons ajouter la matiere

              $nbmatierelib=$etabs->ExisteMatiereLib($libelleMat,$codeEtab,$sessionEtab);

              if($nbmatierelib>0)
              {
                 $etabs->AddMatiereOne($libelleMat,$tabclasses[$indicetab],$teatcherid,$codeEtab,$coefMat,$sessionEtab);
              }else {
                $etabs->AddMatiereLibelle($libelleMat,$codeEtab,$sessionEtab);
                $etabs->AddMatiereOne($libelleMat,$tabclasses[$indicetab],$teatcherid,$codeEtab,$coefMat,$sessionEtab);
              }



            }else {
              // la matiere existe deja dans le système
            }

              // echo "matiere stock id matiere ".$indicetab." et enseigant". $teatcherid." et classe".$tabclasses[$indicetab]."</br>";

              $importationid=$imports->AddImportationFile($tabclasses[$indicetab],$codeEtab,$sessionEtab,$pays,$userid,$datatype);

              $transactionId =  "IMPORT_".date("Y")."_".$codeEtab."_".$tabclasses[$indicetab]."_".$importationid;
              $fichierad=$transactionId.".".$file_ext;
              // echo $fichierad;

              $imports->UpdateImportationFileName($fichierad,$importationid);

                move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

                $dossier="../importations/Etablissements/";
                $dossier1="../importations/Etablissements/".$codeEtab."/";
                $dossier2="../importations/Etablissements/".$codeEtab."/Classes/";

                if(!is_dir($dossier)) {
                    @mkdir($dossier);
                    if(!is_dir($dossier1)) {
                        @mkdir($dossier1);

                        if(!is_dir($dossier2)) {
                            @mkdir($dossier2);

                        }

                    }
                }else {
                  if(!is_dir($dossier1)) {
                      @mkdir($dossier1);

                      if(!is_dir($dossier2)) {
                          @mkdir($dossier2);

                      }

                  }
                }

                @rename('../temp/' . $fichierTemp ,"../importations/Etablissements/".$codeEtab."/Classes/".$fichierad);

               //Suppression du fichier se trouvant dans le dossier temp

                @unlink("../temp/" . $fichierTemp);




              $indicetab++;

          }



        }


      }


    }



    // echo $userid;



  }else {
    echo "aucun fichier";
  }

  // $_SESSION['user']['addetabok']="Les données ont été importées avec succès";
  $_SESSION['user']['addetabok']=L::DataImportationAddMessageSuccess;

  if($_SESSION['user']['paysid']==4)
  {
    header("Location:../localecmr/importations.php");
  }else {
    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

    $etablissementType=$etabs->DetermineTypeEtab($codeEtab);

    if($etablissementType==5)
    {
        header("Location:../locale".$libelleEtab."/importations.php");
    }else {
      header("Location:../locale/importations.php");
    }


  }



}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  $concatcodeEtab=htmlspecialchars($_POST['codeEtab']);
  $pays=htmlspecialchars($_POST['pays']);
  $tabconcat=explode("*",$concatcodeEtab);
  $codeEtab=$tabconcat[0];
  $sessionEtab=$tabconcat[1];
  $userid=$_SESSION['user']['IdCompte'];
  $datatype=htmlspecialchars($_POST['datatype']);

  // echo $codeEtab." ".$sessionEtab." ".$userid;

  $fileError = $_FILES['fichier1']['error'];


  if($fileError==0)
  {
    //fichier uploder

    $file_name = @$_FILES['fichier1']['name'];
    $_SESSION["fichier1"] = $file_name;
    $file_size =@$_FILES['fichier1']['size'];
    $file_tmp =@$_FILES['fichier1']['tmp_name'];
    $file_type=@$_FILES['fichier1']['type'];
    @$file_ext=strtolower(end(explode('.',@$_FILES['fichier1']['name'])));
    $fichierTemp = uniqid() . "." . $file_ext;

    $date=date("Y-m-d");

    $tabdate=explode("-",$date);
    $years=$tabdate[0];
    $mois=$tabdate[1];
    $days=$tabdate[2];
    $libellemois=obtenirLibelleMois($mois);





  }else {
    //aucunn fichier uploader
  }

}




?>
