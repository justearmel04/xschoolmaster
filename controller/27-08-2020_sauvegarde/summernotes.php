<?php
session_start();
require_once('../class/Student.php');
require_once('../class/Etablissement.php');
require_once('../controller/functions.php');

$student = new Student();
$etabs=new Etab();


if(isset($_POST['etape']))
{
  if($_POST['etape']==1||$_POST['etape']==2)
  {
    echo "enregistrer";

    //recuperation des variables

    $objet=htmlspecialchars($_POST['objet']);
    $codeEtab=htmlspecialchars($_POST['codeEtab']);
    $sessionEtab=htmlspecialchars($_POST['libellesession']);
    $otherobjet=htmlspecialchars($_POST['otherobjet']);
    $tabobjet=explode('-',$objet);
    $objetid=$tabobjet[0];
    $libelleobjet=$tabobjet[1];
    $destinataires=htmlspecialchars($_POST['destinataires']);
    $desti=htmlspecialchars($_POST['precis']);
    $summernote=$_POST['summernote'];
    $useradd=$_SESSION['user']['IdCompte'];
    $studentSchool="";
    $classes="";
    $statutNotif=0;
    $paranotif=0;
    $emailvalue=1;
    $smsvalue=1;
    $withfile=0;
    $messageobjetsend=$libelleobjet;
    $dateday=date("Y-m-d");
    $datemsg=date("Y-m-d");
    $type="INFORMATIONS";
    $precisiondestinataires="";
    $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

    if(strlen($summernote)>0)
    {

      if($destinataires=="Parent")
      {
        if($desti==1)
        {
          $precis=0;

          //recuperation des classes

          foreach($_POST['classeEtab'] as $valeur)
         {
           $classes=$classes.$valeur.'-';

         }

         $tabclasses=explode("-",$classes);
         $nbtabclasses=count($tabclasses);
         $cpteclassesval=$nbtabclasses-1;

         if(strlen($otherobjet)>0)
         {
           $messageobjetsend=$otherobjet;

           $messagesid=$etabs->AddnotificationsToparentsclasseselectedOthernew($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$otherobjet,$libelleobjet,$useradd,$type);
         }else {
           $messagesid=$etabs->AddNotificationToAllparentsclasseselect($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$libelleobjet,$useradd,$type);
         }

         for($i=0;$i<$cpteclassesval;$i++)
         {
           $dataParents=$etabs->getEmailsOfParentOfStudentInThisClassesParenterid($tabclasses[$i],$destinataires,$codeEtab,$sessionEtab);
           // $type="INFORMATIONS";

           foreach ($dataParents as $value):
             $etabs->AddparentLecturemessages($messagesid,$value->id_compte,$dateday,$type,$codeEtab,$sessionEtab);
           endforeach;


         }


        }else if($desti==2)
        {
          $precis=1;

          foreach($_POST['eleves'] as $valeur)
         {
           $studentSchool=$studentSchool.$valeur.'-';

         }

         $tabeleves=explode("-",$studentSchool);
         $nbtabeleves=count($tabeleves);
         $cpteelevesval=$nbtabeleves-1;

         $classes=substr($classes, 0, -1);
         $studentSchool=substr($studentSchool, 0, -1);
         $classes = str_replace("-", ",",$classes);
         $studentSchool=str_replace("-", ",",$studentSchool);

         echo $studentSchool;

         if(strlen($otherobjet)>0)
         {
           $messageobjetsend=$otherobjet;
           $messagesid=$etabs->AddnotificationsToparentStudentclasseselectedOthernew($objetid,$summernote,$destinataires,$classes,$precis,$studentSchool,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$otherobjet,$libelleobjet,$useradd,$type);

         }else {
           $messagesid=$etabs->AddNotificationToAllparentStudentclasseselect($objetid,$summernote,$destinataires,$classes,$precis,$studentSchool,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$libelleobjet,$useradd,$type);

         }

         // $dataParents=$etabs->getEmailsAndPhoneOfParentOfStudentclassesParenter($classes,$studentSchool,$sessionEtab,$codeEtab);
         $dataParents=$etabs->getEmailsAndPhoneOfParentOfStudentclassesParenterPrecis($studentSchool);

         foreach ($dataParents as $value):
                 $etabs->AddparentLecturemessages($messagesid,$value->id_compte,$dateday,$type,$codeEtab,$sessionEtab);
         endforeach;

         var_dump($dataParents);

        }


      }else if($destinataires=="Teatcher")
      {
        if($desti==1)
        {
          $precis=0;

          foreach($_POST['classeEtab'] as $valeur)
         {
           $classes=$classes.$valeur.'-';

         }

         $tabclasses=explode("-",$classes);
         $nbtabclasses=count($tabclasses);
         $cpteclassesval=$nbtabclasses-1;

         if(strlen($otherobjet)>0)
         {
           $messageobjetsend=$otherobjet;
           $messagesid=$etabs->AddnotificationsToparentsclasseselectedOthernew($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$otherobjet,$libelleobjet,$useradd,$type);
         }else {
           $messagesid=$etabs->AddNotificationToAllparentsclasseselect($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$libelleobjet,$useradd,$type);
         }

         for($i=0;$i<$cpteclassesval;$i++)
         {
           $dataTeatchers=$etabs->getEmailsOfTeatchersInThisclasses($tabclasses[$i],$destinataires,$codeEtab,$sessionEtab);
           // $type="INFORMATIONS";

           foreach ($dataTeatchers as $value):
             $etabs->AddparentLecturemessages($messagesid,$value->id_compte,$dateday,$type,$codeEtab,$sessionEtab);
           endforeach;


         }



        }else if($desti==2)
        {
          $precis=1;

          if(strlen($otherobjet)>0)
          {
            $messageobjetsend=$otherobjet;
            $messagesid=$etabs->AddnotificationsToparentsclasseselectedOthernew($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$otherobjet,$libelleobjet,$useradd,$type);
          }else {
            $messagesid=$etabs->AddNotificationToAllparentsclasseselect($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$libelleobjet,$useradd,$type);
          }

          foreach($_POST['precisiondestinataires'] as $valeur)
         {
           $precisiondestinataires=$precisiondestinataires.$valeur.'-';

         }
         echo $precisiondestinataires."</br>";

         $precisiondestinataires=substr($precisiondestinataires, 0, -1);
         echo $precisiondestinataires."</br>";
         $precisiondestinataires = str_replace("-", ",",$precisiondestinataires);
         echo $precisiondestinataires."</br>";
         echo $destinataires."</br>";

         $dataTeatchers=$etabs->getEmailsOfTeatchersInThisclassesByIds($destinataires,$precisiondestinataires);
         var_dump($dataTeatchers);

         foreach ($dataTeatchers as $value):
           $etabs->AddparentLecturemessages($messagesid,$value->id_compte,$dateday,$type,$codeEtab,$sessionEtab);
         endforeach;


        }
      }else if($destinataires=="Admin_locale")
      {
        if($desti==1)
        {
          $precis=0;
          //liste des responsables de l'établissement

          if(strlen($otherobjet)>0)
          {
            $messageobjetsend=$otherobjet;
             $messagesid=$etabs->AddnotificationsToparentsclasseselectedOthernew($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$otherobjet,$libelleobjet,$useradd,$type);
          }else {
            $messagesid=$etabs->AddNotificationToAllparentsclasseselect($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$libelleobjet,$useradd,$type);
          }

          $dataRespos=$etabs->getresponsablesbyEtabs($destinataires,$codeEtab);

          foreach ($dataRespos as $value):
            $etabs->AddparentLecturemessages($messagesid,$value->id_compte,$dateday,$type,$codeEtab,$sessionEtab);
          endforeach;

        }else if($desti==2)
        {
          $precis=1;

          if(strlen($otherobjet)>0)
          {
            $messageobjetsend=$otherobjet;
             $messagesid=$etabs->AddnotificationsToparentsclasseselectedOthernew($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$otherobjet,$libelleobjet,$useradd,$type);
          }else {
            $messagesid=$etabs->AddNotificationToAllparentsclasseselect($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$libelleobjet,$useradd,$type);
          }

          //la liste precise des responsables d'etablissements

          foreach($_POST['precisiondestinataires'] as $valeur)
         {
           $precisiondestinataires=$precisiondestinataires.$valeur.'-';

         }



         $precisiondestinataires=substr($precisiondestinataires, 0, -1);
         $precisiondestinataires = str_replace("-", ",",$precisiondestinataires);

         $dataRespos=$etabs->getresponsablesEtabsByIds($destinataires,$codeEtab,$precisiondestinataires);

         foreach ($dataRespos as $value):
           $etabs->AddparentLecturemessages($messagesid,$value->id_compte,$dateday,$type,$codeEtab,$sessionEtab);
         endforeach;


        }



      }

      //nous allons chercher a savoir si nous devons envoyer directement le message ou si nous devons le sauvegarder

      //nous allons verifier si le un fichier a été uploader

      $fileError = $_FILES['fichier']['error'];
      if($fileError==0)
      {
        echo "fichier";
        //nous allons changer files à 1 pour dire qu'un fichier est joint

        $withfile=1;

        $etabs->AddfilejointMessages($messagesid,$type,$codeEtab,$sessionEtab);
        $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

        //traitement du fichier

        $file_name = @$_FILES['fichier']['name'];
        $_SESSION["fichier"] = $file_name;
        $file_size =@$_FILES['fichier']['size'];
        $file_tmp =@$_FILES['fichier']['tmp_name'];
        $file_type=@$_FILES['fichier']['type'];
        @$file_ext=strtolower(end(explode('.',@$_FILES['fichier']['name'])));
        $fichierTemp = uniqid() . "." . $file_ext;
        $transactionId =  "INFOS".date("Y") . date("m").strtoupper(substr($libelleEtab, 0, 2))."_".$messagesid;
        $fichierad=$transactionId.".".$file_ext;
        move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

        $dossier="../messages/";
        $dossier1="../messages/".$libelleEtab;
        $dossier2="../messages/".$libelleEtab."/".$codeEtab;
        $dossier3="../messages/".$libelleEtab."/".$codeEtab."/".$sessionEtab."/";

        if(!is_dir($dossier))
        {
          @mkdir($dossier);
          @mkdir($dossier1);
          @mkdir($dossier2);
          @mkdir($dossier3);
        }else {
          @mkdir($dossier1);
          @mkdir($dossier2);
          @mkdir($dossier3);
        }

        @rename('../temp/' . $fichierTemp , "../messages/".$libelleEtab."/".$codeEtab."/".$sessionEtab."/".$fichierad);
        @unlink("../temp/" . $fichierTemp);

        $etabs->AddmessagesFiles($fichierad,$messagesid,$codeEtab,$sessionEtab);

     $lienfiles="../messages/".$libelleEtab."/".$codeEtab."/".$sessionEtab."/".$fichierad;
      }

      $destiphones="";
      $destimails="";

      if($_POST['etape']==2)
      {
        //envoi du message



        if($destinataires=="Parent")
        {
          //le message est destiné aux parents

          //nous allons recuperer les téléphones et emails des parents dont le message leur ai adressé

          $receiverdatas=$etabs->getAllParentreceiversdatas($messagesid,$codeEtab,$sessionEtab);


          foreach ($receiverdatas as $values):
            if(strlen($values->email_parent)>0 && $values->email_parent!="")
            {
              $destimails=$destimails.$values->email_parent."*";
            }

            if(strlen($values->tel_parent)>0 && $values->tel_parent!="")
            {
              $destiphones=$destiphones.$indicatifEtab.$values->tel_parent."*";
            }


          endforeach;

          $destimails=substr($destimails, 0, -1);
          $destiphones=substr($destiphones, 0, -1);




        }else if($destinataires=="Teatcher")
        {
          //le message est destiné aux enseignants

          $receiverdatas=$etabs->getAllTeatchereceiversdatas($messagesid,$codeEtab,$sessionEtab);

          foreach ($receiverdatas as $values):

            if(strlen($values->email_enseignant)>0 && $values->email_enseignant!="")
            {
              $destimails=$destimails.$values->email_enseignant."*";
            }

            if(strlen($values->tel_compte)>0 && $values->tel_compte!="")
            {
              $destiphones=$destiphones.$indicatifEtab.$values->tel_compte."*";
            }


          endforeach;

          $destimails=substr($destimails, 0, -1);
          $destiphones=substr($destiphones, 0, -1);


        }else if($destinataires=="Admin_locale")
        {
          //le message est destiné aux responsables

          $receiverdatas=$etabs->getAllResposreceiversdatas($messagesid,$codeEtab,$sessionEtab);

          foreach ($receiverdatas as $values):
            if(strlen($values->tel_compte)>0 && $values->tel_compte!="")
            {
              $destiphones=$destiphones.$indicatifEtab.$values->tel_compte."*";
            }

            if(strlen($values->email_compte)>0 && $values->email_compte!="")
            {
              $destimails=$destimails.$values->email_compte."*";
            }


          endforeach;

          $destimails=substr($destimails, 0, -1);
          $destiphones=substr($destiphones, 0, -1);



        }


        //envoi du mail au destinataires
        $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
        $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);

        if($withfile==0)
        {
          $etabs->SenderEmail($messageobjetsend,$destimails,$summernote,$sessionEtab,$codeEtab,$libelleEtab,$logoEtab);
        }else if($withfile==1)
        {
          $lienfiles="../messages/".$libelleEtab."/".$codeEtab."/".$sessionEtab."/".$fichierad;
        $etabs->SenderEmailwithFiles($messageobjetsend,$destimails,$summernote,$sessionEtab,$codeEtab,$libelleEtab,$logoEtab,$lienfiles);
        }

        $notificationsStatus=1;
        $etabs->UpdateNotificationStatusActivities($messagesid,$notificationsStatus,$codeEtab);

        echo $destimails;

        //changement du statut sending lecture
        $statutlecture=1;
        $etabs->UpdatestatutLecturemessages($messagesid,$statutlecture);

      }

      //redirection

      $_SESSION['user']['updateteaok']=L::NotificationCreateMessageSuccess ;
      $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
      if($_SESSION['user']['profile'] == "Admin_globale") {

            //header("Location:../manager/index.php");
            header("Location:../manager/allmessages.php");

        }else if($_SESSION['user']['profile'] == "Admin_locale") {

          // header("Location:../locale".$libelleEtab."/allmessages.php");

          $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
      $typeetab=$etabs->DetermineTypeEtab($codeEtab);

      if($typeetab==5)
      {
        header("Location:../locale".$libelleEtab."/allmessages.php");
      }else {
        header("Location:../locale/allmessages.php");
      }


        }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/allmessages.php");

            $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etabs->DetermineTypeEtab($codeEtab);
            $IBSAschool=$etabs->getIbsaschools();
            $codeetabs="";
            foreach ($IBSAschool as  $value):
              $codeetabs=$codeetabs.$value->code_etab."*";
            endforeach;
            $tabetabs=explode("*",$codeetabs);

            $nbdispenser=0;

            $dispenserdatas=$etabs->getdispenserTeatcher($_SESSION['user']['IdCompte']);

            foreach ($dispenserdatas as $value):
              $code=$value->codeEtab;
              if (in_array($code, $tabetabs)) {
                $nbdispenser++;
               }
            endforeach;

            echo $nbdispenser;

            if($nbdispenser>0)
            {
              header("Location:../teatcher".$libelleEtab."/allmessages.php");
            }else {
              header("Location:../teatcher/allmessages.php");
            }


            }

    }else {
      $_SESSION['user']['addprogra']=L::PleaseEnterActivityMesaage;
      $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
      if($_SESSION['user']['profile'] == "Admin_globale") {

            //header("Location:../manager/index.php");
            header("Location:../manager/allmessages.php");

        }else if($_SESSION['user']['profile'] == "Admin_locale") {

          header("Location:../locale".$libelleEtab."/addnotifications.php");


  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
      $typeetab=$etabs->DetermineTypeEtab($codeEtab);

      if($typeetab==5)
      {
        header("Location:../locale".$libelleEtab."/addnotifications.php");
      }else {
        header("Location:../locale/addnotifications.php");
      }

        }else if($_SESSION['user']['profile'] == "Teatcher") {

            // header("Location:../teatcher/allmessages.php");

            $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etabs->DetermineTypeEtab($codeEtab);
            $IBSAschool=$etabs->getIbsaschools();
            $codeetabs="";
            foreach ($IBSAschool as  $value):
              $codeetabs=$codeetabs.$value->code_etab."*";
            endforeach;
            $tabetabs=explode("*",$codeetabs);

            $nbdispenser=0;

            $dispenserdatas=$etabs->getdispenserTeatcher($_SESSION['user']['IdCompte']);

            foreach ($dispenserdatas as $value):
              $code=$value->codeEtab;
              if (in_array($code, $tabetabs)) {
                $nbdispenser++;
               }
            endforeach;

            echo $nbdispenser;

            if($nbdispenser>0)
            {
              header("Location:../teatcher".$libelleEtab."/allmessages.php");
            }else {
              header("Location:../teatcher/allmessages.php");
            }






            }



    echo "Aucun message";
    }



  }else if($_POST['etape']==3||$_POST['etape']==4)
  {
    echo "bonjour";
    //recuperation des variables
    $emailvalue=1;
    $smsvalue=1;
    $typesess=htmlspecialchars($_POST['typesess']);
    $codeEtab=htmlspecialchars($_POST['codeEtab']);
    $dataactivite=htmlspecialchars($_POST['typeactivite']);
    $othertype="";
    $tabactivite=explode("-",$dataactivite);
    $typeactivite=$tabactivite[0];
    if($typeactivite==6)
    {
      $othertype=htmlspecialchars($_POST['otherobjet']);
    }
    $denomination=htmlspecialchars($_POST['denomination']);
    $sessionEtab=htmlspecialchars($_POST['libellesession']);
    $messageobjetsend=$denomination;
    $withfile=0;
    $gratuitcheck=0;
    $paiecheck=1;
    $respoactivite=htmlspecialchars($_POST['respoactivite']);
    $contactrespo=htmlspecialchars($_POST['contactrespo']);
    $locationactivite=htmlspecialchars($_POST['locationactivite']);
    $descripactivite=htmlspecialchars($_POST['summernote']);
    $datedeb=htmlspecialchars($_POST['datedeb']);
    $datefin=htmlspecialchars($_POST['datefin']);

    $objet_msg=8;
    $other="Activité parascolaire";
    $other=htmlspecialchars(addslashes($other));
    $useradd=$_SESSION['user']['IdCompte'];
    $type="PARASCOLAIRES";
    $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

    //information date de debut
    $tabdatedeb=explode(" ",$datedeb);
    $heurededeb=$tabdatedeb[1];
    $datededeb=$tabdatedeb[0];

      //information date de fin
    $tabdatefin=explode(" ",$datefin);
    $heuredefin=$tabdatefin[1];
    $datedefin=$tabdatefin[0];
    $destinataires="";
    $studentSchool="";
    $statutNotif=0;
    $dateday=date("Y-m-d");
    $classes="";
    $paranotif=1;
    $statutpara=1;
    $precis=0;
    $montantAct=htmlspecialchars($_POST['montantAct']);

    if(isset($_POST['classeEtab'])&& isset($_POST['destinataires']))
    {
      foreach($_POST['classeEtab'] as $valeur)
      {
       $classes=$classes.$valeur.'-';

      }

      foreach($_POST['destinataires'] as $valeur)
      {
       $destinataires=$destinataires.$valeur.'-';

      }

      $tabclasses=explode("-",$classes);
      $nbtabclasses=count($tabclasses);
      $cpteclassesval=$nbtabclasses-1;

      $destiphones="";
      $destimails="";

      if(strlen($_POST['summernote'])>0)
      {

        //insertion dans la table messages

        $messagesid=$etabs->AddExtractivities($objet_msg,$destinataires,$classes,$precis,$paranotif,$statutpara,$dateday,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$othertype,$useradd,$type,$descripactivite);
        $idactivity=$etabs->AddActiviteparascolaires($sessionEtab,$denomination,$typeactivite,$locationactivite,$respoactivite,$contactrespo,$classes,$descripactivite,$codeEtab,$montantAct,$paiecheck,$messagesid,$statutpara,$datededeb,$datedefin,$heurededeb,$heuredefin,$typesess,$emailvalue,$smsvalue,$othertype);

        // $destinataires=substr($destinataires, 0, -1);

        $tabdestinataires=explode("-",$destinataires);

        for($i=0;$i<count($tabdestinataires);$i++)
        {
          if($tabdestinataires[$i]=="Parent")
          {
            //nous allons chercher les parents des classes selectionnées
             $classes = str_replace("-", ",",$classes);
             $dataParents=$etabs->getEmailsOfParentOfStudentInThisClassesParenteridNew($classes,$tabdestinataires[$i],$codeEtab,$sessionEtab);
             foreach ($dataParents as $values):
               $etabs->AddparentLecturemessages($messagesid,$values->id_compte,$dateday,$type,$codeEtab,$sessionEtab);
               if(strlen($values->email_parent)>0 && $values->email_parent!="")
               {
                 $destimails=$destimails.$values->email_parent."*";
               }

               if(strlen($values->tel_parent)>0 && $values->tel_parent!="")
               {
                 $destiphones=$destiphones.$indicatifEtab.$values->tel_parent."*";
               }
             endforeach;
          }else if($tabdestinataires[$i]=="Teatcher")
          {
            $dataTeatchers=$etabs->getEmailsOfTeatchersInThisclassesNew($classes,$tabdestinataires[$i],$codeEtab,$sessionEtab);
            // $type="INFORMATIONS";

            foreach ($dataTeatchers as $values):
              $etabs->AddparentLecturemessages($messagesid,$values->id_compte,$dateday,$type,$codeEtab,$sessionEtab);
              if(strlen($values->email_enseignant)>0 && $values->email_enseignant!="")
              {
                $destimails=$destimails.$values->email_enseignant."*";
              }

              if(strlen($values->tel_compte)>0 && $values->tel_compte!="")
              {
                $destiphones=$destiphones.$indicatifEtab.$values->tel_compte."*";
              }
            endforeach;
          }else if($tabdestinataires[$i]=="Admin_locale")
          {
            //liste des responsables de l'etablissement

            $dataRespos=$etabs->getresponsablesbyEtabs($destinataires,$codeEtab);

            foreach ($dataRespos as $values):
              $etabs->AddparentLecturemessages($messagesid,$value->id_compte,$dateday,$type,$codeEtab,$sessionEtab);
              if(strlen($values->tel_compte)>0 && $values->tel_compte!="")
              {
                $destiphones=$destiphones.$indicatifEtab.$values->tel_compte."*";
              }

              if(strlen($values->email_compte)>0 && $values->email_compte!="")
              {
                $destimails=$destimails.$values->email_compte."*";
              }
            endforeach;

          }
        }

        $fileError = $_FILES['fichier']['error'];
        if($fileError==0)
        {
          echo "fichier";
          //nous allons changer files à 1 pour dire qu'un fichier est joint

          $withfile=1;

          $etabs->AddfilejointMessages($messagesid,$type,$codeEtab,$sessionEtab);
          $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

          //traitement du fichier

          $file_name = @$_FILES['fichier']['name'];
          $_SESSION["fichier"] = $file_name;
          $file_size =@$_FILES['fichier']['size'];
          $file_tmp =@$_FILES['fichier']['tmp_name'];
          $file_type=@$_FILES['fichier']['type'];
          @$file_ext=strtolower(end(explode('.',@$_FILES['fichier']['name'])));
          $fichierTemp = uniqid() . "." . $file_ext;
          $transactionId =  "INFOS".date("Y") . date("m").strtoupper(substr($libelleEtab, 0, 2))."_".$messagesid;
          $fichierad=$transactionId.".".$file_ext;
          move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

          $dossier="../messages/";
          $dossier1="../messages/".$libelleEtab;
          $dossier2="../messages/".$libelleEtab."/".$codeEtab;
          $dossier3="../messages/".$libelleEtab."/".$codeEtab."/".$sessionEtab."/";

          if(!is_dir($dossier))
          {
            @mkdir($dossier);
            @mkdir($dossier1);
            @mkdir($dossier2);
            @mkdir($dossier3);
          }else {
            @mkdir($dossier1);
            @mkdir($dossier2);
            @mkdir($dossier3);
          }

          @rename('../temp/' . $fichierTemp , "../messages/".$libelleEtab."/".$codeEtab."/".$sessionEtab."/".$fichierad);
          @unlink("../temp/" . $fichierTemp);

          $etabs->AddmessagesFiles($fichierad,$messagesid,$codeEtab,$sessionEtab);

       $lienfiles="../messages/".$libelleEtab."/".$codeEtab."/".$sessionEtab."/".$fichierad;
        }

        if($_POST['etape']==4)
        {
          $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
          $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);

          if($withfile==0)
          {
            $etabs->SenderEmail($messageobjetsend,$destimails,$descripactivite,$sessionEtab,$codeEtab,$libelleEtab,$logoEtab);
          }else if($withfile==1)
          {
            $lienfiles="../messages/".$libelleEtab."/".$codeEtab."/".$sessionEtab."/".$fichierad;
          $etabs->SenderEmailwithFiles($messageobjetsend,$destimails,$descripactivite,$sessionEtab,$codeEtab,$libelleEtab,$logoEtab,$lienfiles);
          }

          $notificationsStatus=1;
          $etabs->UpdateNotificationStatusActivities($messagesid,$notificationsStatus,$codeEtab);

          echo $destimails;

          //changement du statut sending lecture
          $statutlecture=1;
          $etabs->UpdatestatutLecturemessages($messagesid,$statutlecture);
        }

        $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

        //
        if($_SESSION['user']['profile'] == "Admin_globale") {
      $_SESSION['user']['updateteaok']=L::ParascoAddMessageSuccess;
              //header("Location:../manager/index.php");
              header("Location:../manager/allparascolaires.php");

          }else if($_SESSION['user']['profile'] == "Admin_locale") {
              $_SESSION['user']['updateteaok']=L::AddExtrascolairesuccessfully;

            // header("Location:../locale".$libelleEtab."/allparascolaires.php");
            $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
      $typeetab=$etabs->DetermineTypeEtab($codeEtab);

      if($typeetab==5)
      {
        header("Location:../locale".$libelleEtab."/allparascolaires.php");
      }else {
        header("Location:../locale/allparascolaires.php");
      }
            }

      }else {
        $_SESSION['user']['addprogra']=L::PleaseEnterActivityMesaage;
        $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
        if($_SESSION['user']['profile'] == "Admin_globale") {

              //header("Location:../manager/index.php");
              header("Location:../manager/allmessages.php");

          }else if($_SESSION['user']['profile'] == "Admin_locale") {

            // header("Location:../locale".$libelleEtab."/parascolairesadd.php");

            $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
      $typeetab=$etabs->DetermineTypeEtab($codeEtab);

      if($typeetab==5)
      {
        header("Location:../locale".$libelleEtab."/parascolairesadd.php");
      }else {
        header("Location:../locale/parascolairesadd.php");
      }

          }else if($_SESSION['user']['profile'] == "Teatcher") {

              // header("Location:../teatcher/allmessages.php");

              }
      }

    }else {
      echo "retour";
      $_SESSION['user']['addprogra']=L::PleaseEnterActivityMesaage;
      $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
      if($_SESSION['user']['profile'] == "Admin_globale") {

            //header("Location:../manager/index.php");
            header("Location:../manager/allmessages.php");

        }else if($_SESSION['user']['profile'] == "Admin_locale") {

          // header("Location:../locale".$libelleEtab."/parascolairesadd.php");

          $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
      $typeetab=$etabs->DetermineTypeEtab($codeEtab);

      if($typeetab==5)
      {
        header("Location:../locale".$libelleEtab."/parascolairesadd.php");
      }else {
        header("Location:../locale/parascolairesadd.php");
      }

        }else if($_SESSION['user']['profile'] == "Teatcher") {

            // header("Location:../teatcher/allmessages.php");

            }
    }







  }else if($_POST['etape']==5)
  {
    echo "bonjour";
    //recuperation des variables

    // otherobjet: ddddddddddddddddd
    // codeEtab: 0001
    // libellesession: 2020-2021
    // destinataires: Admin_locale
    // summernote

    $objet="8-Autres";
    $codeEtab=htmlspecialchars($_POST['codeEtab']);
    $messageoldid=htmlspecialchars($_POST['messageid']);
    $sessionEtab=htmlspecialchars($_POST['libellesession']);
    $otherobjet=htmlspecialchars($_POST['otherobjet']);
    $tabobjet=explode('-',$objet);
    $objetid=$tabobjet[0];
    $libelleobjet=$tabobjet[1];
    $destinataires=htmlspecialchars($_POST['destinataires']);
    $desti=0;
    $summernote=$_POST['summernote'];
    $useradd=$_SESSION['user']['IdCompte'];
    $studentSchool="";
    $classes="";
    $statutNotif=0;
    $paranotif=0;
    $emailvalue=1;
    $smsvalue=1;
    $withfile=0;
    $messageobjetsend=$otherobjet;
    $dateday=date("Y-m-d");
    $datemsg=date("Y-m-d");
    $type="INFORMATIONS";
    $precis=0;
    $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

    if(strlen($summernote)>0)
    {

   $messagesid=$etabs->AddnotificationsToparentsclasseselectedOthernew($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$otherobjet,$libelleobjet,$useradd,$type);

   $dataRespos=$etabs->getresponsablesbyEtabs($destinataires,$codeEtab);

   foreach ($dataRespos as $value):
     $etabs->AddparentLecturemessages($messagesid,$value->id_compte,$dateday,$type,$codeEtab,$sessionEtab);
   endforeach;
   $destiphones="";
   $destimails="";

   $receiverdatas=$etabs->getAllResposreceiversdatas($messagesid,$codeEtab,$sessionEtab);

   foreach ($receiverdatas as $values):
     if(strlen($values->tel_compte)>0 && $values->tel_compte!="")
     {
       $destiphones=$destiphones.$indicatifEtab.$values->tel_compte."*";
     }

     if(strlen($values->email_compte)>0 && $values->email_compte!="")
     {
       $destimails=$destimails.$values->email_compte."*";
     }


   endforeach;

   $destimails=substr($destimails, 0, -1);
   $destiphones=substr($destiphones, 0, -1);

   $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
$logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
$etabs->SenderEmail($messageobjetsend,$destimails,$summernote,$sessionEtab,$codeEtab,$libelleEtab,$logoEtab);

$notificationsStatus=1;
        $etabs->UpdateNotificationStatusActivities($messagesid,$notificationsStatus,$codeEtab);

        echo $destimails;

        //changement du statut sending lecture
        $statutlecture=1;
        $etabs->UpdatestatutLecturemessages($messagesid,$statutlecture);

//ajouter dans la table reponsesmessages

$etabs->addresponsemessages($messagesid,$messageoldid,$useradd,$codeEtab,$sessionEtab);



        $_SESSION['user']['updateteaok']=L::NotificationCreateMessageSuccess ;
        $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
        if($_SESSION['user']['profile'] == "Admin_globale") {

              //header("Location:../manager/index.php");
              header("Location:../manager/allmessages.php");

          }else if($_SESSION['user']['profile'] == "Admin_locale") {

            header("Location:../locale".$libelleEtab."/allmessages.php");

            $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
      $typeetab=$etabs->DetermineTypeEtab($codeEtab);

      if($typeetab==5)
      {
        header("Location:../locale".$libelleEtab."/allmessages.php");
      }else {
        header("Location:../locale/allmessages.php");
      }

          }else if($_SESSION['user']['profile'] == "Teatcher") {

              header("Location:../teatcher/allmessages.php");

              $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etabs->DetermineTypeEtab($codeEtab);
            $IBSAschool=$etabs->getIbsaschools();
            $codeetabs="";
            foreach ($IBSAschool as  $value):
              $codeetabs=$codeetabs.$value->code_etab."*";
            endforeach;
            $tabetabs=explode("*",$codeetabs);

            $nbdispenser=0;

            $dispenserdatas=$etabs->getdispenserTeatcher($_SESSION['user']['IdCompte']);

            foreach ($dispenserdatas as $value):
              $code=$value->codeEtab;
              if (in_array($code, $tabetabs)) {
                $nbdispenser++;
               }
            endforeach;

            echo $nbdispenser;

            if($nbdispenser>0)
            {
              header("Location:../teatcher".$libelleEtab."/allmessages.php");
            }else {
              header("Location:../teatcher/allmessages.php");
            }







            }else if($_SESSION['user']['profile'] == "Parent") {

                  header("Location:../parent".$libelleEtab."/notifications.php");

                  }


    }else {
      $_SESSION['user']['addprogra']=L::PleaseEnterActivityMesaage;
      $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
      if($_SESSION['user']['profile'] == "Admin_globale") {

            //header("Location:../manager/index.php");
            // header("Location:../manager/allmessages.php");

        }else if($_SESSION['user']['profile'] == "Admin_locale") {

          // header("Location:../locale".$libelleEtab."/addnotifications.php");

        }else if($_SESSION['user']['profile'] == "Teatcher") {

            // header("Location:../teatcher/allmessages.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent".$libelleEtab."/replynotifications.php?msg=".$messageid);

                }
    }

  }else if($_POST['etape']==6)
  {
      //recuperation des variables

      // otherobjet: ddddddddddddddddd
      // etape: 6
      // codeEtab: 0001
      // libellesession: 2020-2021
      // destinataires: Teatcher
      // precisiondestinataires: ALL
      // summernote: <p>eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee</p>

      $objet="8-Autres";
      $codeEtab=htmlspecialchars($_POST['codeEtab']);
      $sessionEtab=htmlspecialchars($_POST['libellesession']);
      $otherobjet=htmlspecialchars($_POST['otherobjet']);
      $studentid=htmlspecialchars($_POST['studentid']);
      $tabobjet=explode('-',$objet);
      $objetid=$tabobjet[0];
      $libelleobjet=$tabobjet[1];
      $destinataires=htmlspecialchars($_POST['destinataires']);
      $destinataires1="Admin_locale";
      $desti=0;
      $summernote=$_POST['summernote'];
      $useradd=$_SESSION['user']['IdCompte'];
      $studentSchool="";
      $classes=htmlspecialchars($_POST['classes']);
      $statutNotif=0;
      $paranotif=0;
      $emailvalue=1;
      $smsvalue=1;
      $withfile=0;
      $messageobjetsend=$otherobjet;
      $dateday=date("Y-m-d");
      $datemsg=date("Y-m-d");
      $type="INFORMATIONS";
      $precis=0;
      $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);
      $precisiondestinataires=htmlspecialchars($_POST['precisiondestinataires']);

      if(strlen($summernote)>0)
      {
        $messagesid=$etabs->AddnotificationsToparentsclasseselectedOthernew($objetid,$summernote,$destinataires,$classes,$precis,$paranotif,$statutNotif,$datemsg,$emailvalue,$smsvalue,$sessionEtab,$codeEtab,$otherobjet,$libelleobjet,$useradd,$type);

        $dataRespos=$etabs->getresponsablesbyEtabs($destinataires1,$codeEtab);

        foreach ($dataRespos as $value):
          $etabs->AddparentLecturemessages($messagesid,$value->id_compte,$dateday,$type,$codeEtab,$sessionEtab);
        endforeach;
        $destiphones="";
        $destimails="";

        $receiverdatas=$etabs->getAllResposreceiversdatas($messagesid,$codeEtab,$sessionEtab);

        foreach ($receiverdatas as $values):
          if(strlen($values->tel_compte)>0 && $values->tel_compte!="")
          {
            $destiphones=$destiphones.$indicatifEtab.$values->tel_compte."*";
          }

          if(strlen($values->email_compte)>0 && $values->email_compte!="")
          {
            $destimails=$destimails.$values->email_compte."*";
          }


        endforeach;

        //gestion pour l'enseignant

        if($destinataires=="Teatcher")
        {
          if($precisiondestinataires=="ALL")
          {
            $dataTeatchers=$etabs->getEmailsOfTeatchersInThisclassesNew($classes,$destinataires,$codeEtab,$sessionEtab);


            foreach ($dataTeatchers as $values):
              $etabs->AddparentLecturemessages($messagesid,$values->id_compte,$dateday,$type,$codeEtab,$sessionEtab);
              if(strlen($values->email_enseignant)>0 && $values->email_enseignant!="")
              {
                $destimails=$destimails.$values->email_enseignant."*";
              }

              if(strlen($values->tel_compte)>0 && $values->tel_compte!="")
              {
                $destiphones=$destiphones.$indicatifEtab.$values->tel_compte."*";
              }
            endforeach;
          }else {
            $dataTeatchers=$etabs->getEmailsOfTeatchersInThisclassesByIds($destinataires,$precisiondestinataires);
            var_dump($dataTeatchers);

            foreach ($dataTeatchers as $value):
              $etabs->AddparentLecturemessages($messagesid,$value->id_compte,$dateday,$type,$codeEtab,$sessionEtab);
              if(strlen($values->email_enseignant)>0 && $values->email_enseignant!="")
              {
                $destimails=$destimails.$values->email_enseignant."*";
              }

              if(strlen($values->tel_compte)>0 && $values->tel_compte!="")
              {
                $destiphones=$destiphones.$indicatifEtab.$values->tel_compte."*";
              }
            endforeach;
          }
        }
        $destimails=substr($destimails, 0, -1);
        $destiphones=substr($destiphones, 0, -1);

        $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
       $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
       $etabs->SenderEmail($messageobjetsend,$destimails,$summernote,$sessionEtab,$codeEtab,$libelleEtab,$logoEtab);

       $notificationsStatus=1;
             $etabs->UpdateNotificationStatusActivities($messagesid,$notificationsStatus,$codeEtab);

             echo $destimails;

             //changement du statut sending lecture
             $statutlecture=1;
             $etabs->UpdatestatutLecturemessages($messagesid,$statutlecture);

             // $_SESSION['user']['updateteaok']=L::ParascoAddMessageSuccess;
                     //header("Location:../manager/index.php");
                     header("Location:../manager/allparascolaires.php");

                 }else if($_SESSION['user']['profile'] == "parent") {
                       $_SESSION['user']['updateteaok']=L::NotificationCreateMessageSuccess ;

                   // header("Location:../locale".$libelleEtab."/allparascolaires.php");
                   $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
             $typeetab=$etabs->DetermineTypeEtab($codeEtab);

             if($typeetab==5)
             {
               header("Location:../parent".$libelleEtab."/notifications.php");
             }else {
               header("Location:../locale/notifications.php");
             }

      }else {
        $_SESSION['user']['addprogra']=L::PleaseEnterActivityMesaage;
        $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
        if($_SESSION['user']['profile'] == "Admin_globale") {

              //header("Location:../manager/index.php");
              // header("Location:../manager/allmessages.php");

          }else if($_SESSION['user']['profile'] == "Admin_locale") {

            // header("Location:../locale".$libelleEtab."/addnotifications.php");

          }else if($_SESSION['user']['profile'] == "Teatcher") {

              // header("Location:../teatcher/allmessages.php");

            }else if($_SESSION['user']['profile'] == "Parent") {

                  header("Location:../parent".$libelleEtab."/addnotifications.php?compte=".$studentid);

                  }
      }


  }
}


?>
