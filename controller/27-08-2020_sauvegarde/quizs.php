<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Matiere.php');
require_once('../controller/functions.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$classe = new Classe();
$etabs=new Etab();
$matiere=new Matiere();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recuperation des variables
$libellequiz=htmlspecialchars($_POST['libellecourse']);
$classeEtab=htmlspecialchars($_POST['classeEtab']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);
$durationquiz=htmlspecialchars($_POST['durationcourse']);
$instructionquiz=htmlspecialchars($_POST['detailscourse']);
$concatmatiere=htmlspecialchars($_POST['matclasse']);
$datelimite=dateFormat(htmlspecialchars($_POST['datecourse']));
$tabmatiere=explode("-",$concatmatiere);
$matiereid=$tabmatiere[0];
$teatcherid=$tabmatiere[1];
$verouillerquiz=htmlspecialchars($_POST['verouiller']);
$statutquiz=0;

$libellematiere=$etabs->getMatiereLibelleByIdMat($matiereid,$codeEtab);
$libelleclasse=$classe->getInfosofclassesbyId($classeEtab,$sessionEtab);


//insertion dans la table quiz

$quizid=$etabs->Addquiz($libellequiz,$durationquiz,$instructionquiz,$classeEtab,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$verouillerquiz,$statutquiz,$datelimite);

//nous allons renseigner les trueorfalse

$concatquesttrueorfalse=htmlspecialchars($_POST['concatquesttrueorfalse']);

$concatquesttrueorfalse=substr($concatquesttrueorfalse, 0, -1);

$tabconcatquesttrueorfalse=explode("@",$concatquesttrueorfalse);

$nb=count($tabconcatquesttrueorfalse);

for($i=0;$i<$nb;$i++)
{
  //il est question d'une question trueorfalse

$libellequestion=htmlspecialchars($_POST['libellequestion'.$tabconcatquesttrueorfalse[$i]]);
$pointquestion=htmlspecialchars($_POST['pointquestion'.$tabconcatquesttrueorfalse[$i]]);
$answer=htmlspecialchars($_POST['answer'.$tabconcatquesttrueorfalse[$i]]);
$moderep=htmlspecialchars($_POST['moderep'.$tabconcatquesttrueorfalse[$i]]);

// echo $libellequestion." /".$pointquestion." /".$answer." /".$moderep;


$questionid=$etabs->AddQuestion($libellequestion,$moderep,$pointquestion,$quizid);


if($answer==1)
{
  //cas de vrai
  $etabs->AddPropositionTrueActive($questionid);
  $etabs->AddPropositionFalse($questionid);

}else {
  // cos de faux
  $etabs->AddPropositionTrue($questionid);
  $etabs->AddPropositionFalseActive($questionid);
}



}

//nous allons renseigner les choix multiple


$concatquestmultiple=htmlspecialchars($_POST['concatquestmultiple']);
$concatquestmultiple=substr($concatquestmultiple, 0, -1);
$tabconcatquestmultiple=explode("@",$concatquestmultiple);

$nbMulti=count($tabconcatquestmultiple);

for($i=0;$i<$nbMulti;$i++)
{

  $libellequestion=htmlspecialchars($_POST['libellequestion'.$tabconcatquestmultiple[$i]]);
  $pointquestion=htmlspecialchars($_POST['pointquestion'.$tabconcatquestmultiple[$i]]);
  // $answer=htmlspecialchars($_POST['answer'.$tabconcatquesttrueorfalse[$i]]);
  $moderep=htmlspecialchars($_POST['moderep'.$tabconcatquestmultiple[$i]]);
  $propositionNbquestion=htmlspecialchars($_POST['propositionNbquestion'.$tabconcatquestmultiple[$i]]);

  $questionid=$etabs->AddQuestion($libellequestion,$moderep,$pointquestion,$quizid);

  for($j=1;$j<=$propositionNbquestion;$j++)
  {

    if(isset($_POST['libelle_proposition'.$tabconcatquestmultiple[$i].$j]))
    {
      $libelle_proposition=htmlspecialchars($_POST['libelle_proposition'.$tabconcatquestmultiple[$i].$j]);
      $proposiionid=$etabs->AddProposition($libelle_proposition,$questionid);

      if(isset($_POST['chk_proposition'.$tabconcatquestmultiple[$i].$j]))
      {
        $chk_proposition=$_POST['chk_proposition'.$tabconcatquestmultiple[$i].$j];
        $etabs->UpdatePropositionValue($chk_proposition,$proposiionid);
      }
    }


  }


  $_SESSION['user']['addclasseok']=L::QuizAddMessageSuccess;

  if($_SESSION['user']['profile'] == "Teatcher") {

      // header("Location:../teatcher/listquizs.php");

      $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etabs->DetermineTypeEtab($codeEtab);
            $IBSAschool=$etabs->getIbsaschools();
            $codeetabs="";
            foreach ($IBSAschool as  $value):
              $codeetabs=$codeetabs.$value->code_etab."*";
            endforeach;
            $tabetabs=explode("*",$codeetabs);

            $nbdispenser=0;

            $dispenserdatas=$etabs->getdispenserTeatcher($_SESSION['user']['IdCompte']);

            foreach ($dispenserdatas as $value):
              $code=$value->codeEtab;
              if (in_array($code, $tabetabs)) {
                $nbdispenser++;
               }
            endforeach;

            echo $nbdispenser;

            if($nbdispenser>0)
            {
              header("Location:../teatcher".$libelleEtab."/listquizs.php");
            }else {
              header("Location:../teatcher/listquizs.php");
            }



      }


}


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //il est question de modifier le quiz

  $libellequiz=htmlspecialchars($_POST['libellecourse']);
  $classeEtab=htmlspecialchars($_POST['classeEtab']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $durationquiz=htmlspecialchars($_POST['durationcourse']);
  $instructionquiz=htmlspecialchars($_POST['detailscourse']);
  $concatmatiere=htmlspecialchars($_POST['matclasse']);
  $datelimite=dateFormat(htmlspecialchars($_POST['datecourse']));
  $tabmatiere=explode("-",$concatmatiere);
  $matiereid=$tabmatiere[0];
  $teatcherid=$tabmatiere[1];
  $verouillerquiz=htmlspecialchars($_POST['verouiller']);
  $idquiz=htmlspecialchars($_POST['courseid']);

// echo $verouillerquiz;

  $etabs->UpdateQuizInfos($libellequiz,$classeEtab,$codeEtab,$sessionEtab,$durationquiz,$instructionquiz,$datelimite,$matiereid,$teatcherid,$verouillerquiz,$idquiz);

  // $_SESSION['user']['addclasseok']="Le quiz a été modifié avec succès";
  $_SESSION['user']['addclasseok']=L::QuizModMessageSuccess;

  if($_SESSION['user']['profile'] == "Teatcher") {

      // header("Location:../teatcher/updatequizs.php?courseid=".$idquiz."&classeid=".$classeEtab);

      $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etabs->DetermineTypeEtab($codeEtab);
            $IBSAschool=$etabs->getIbsaschools();
            $codeetabs="";
            foreach ($IBSAschool as  $value):
              $codeetabs=$codeetabs.$value->code_etab."*";
            endforeach;
            $tabetabs=explode("*",$codeetabs);

            $nbdispenser=0;

            $dispenserdatas=$etabs->getdispenserTeatcher($_SESSION['user']['IdCompte']);

            foreach ($dispenserdatas as $value):
              $code=$value->codeEtab;
              if (in_array($code, $tabetabs)) {
                $nbdispenser++;
               }
            endforeach;

            echo $nbdispenser;

            if($nbdispenser>0)
            {
              header("Location:../teatcher/updatequizs.php?courseid=".$idquiz."&classeid=".$classeEtab);
            }else {
              header("Location:../teatcher/updatequizs.php?courseid=".$idquiz."&classeid=".$classeEtab);
            }



      }


}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
//recuperation des variables

$libellequestion=htmlspecialchars($_POST['libellesquestTrue']);
$quizid=htmlspecialchars($_POST['idquiztrue']);
$pointquestion=htmlspecialchars($_POST['pointquestTrue']);
$answer=htmlspecialchars($_POST['answer']);
$classeid=htmlspecialchars($_POST['classequiztrue']);

//nous allons chercher le codeEtab du quiz

$datasquizs=$etabs->getInfosbyquizid($quizid);
$codeEtab="";
foreach ($datasquizs as  $value):
  $codeEtab=$value->codeEtab_quiz;
endforeach;

$moderep=1;

$questionid=$etabs->AddQuestion($libellequestion,$moderep,$pointquestion,$quizid);

if($answer==1)
{
  //cas de vrai
  $etabs->AddPropositionTrueActive($questionid);
  $etabs->AddPropositionFalse($questionid);

}else {
  // cos de faux
  $etabs->AddPropositionTrue($questionid);
  $etabs->AddPropositionFalseActive($questionid);
}

// $_SESSION['user']['addclasseok']="Une nouvelle question a été ajoutée au quiz avec succès";
$_SESSION['user']['addclasseok']=L::NewQuestionQuizAddMessageSuccess;


if($_SESSION['user']['profile'] == "Teatcher") {

    // header("Location:../teatcher/updatequizs.php?courseid=".$quizid."&classeid=".$classeid);

    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etabs->DetermineTypeEtab($codeEtab);
            $IBSAschool=$etabs->getIbsaschools();
            $codeetabs="";
            foreach ($IBSAschool as  $value):
              $codeetabs=$codeetabs.$value->code_etab."*";
            endforeach;
            $tabetabs=explode("*",$codeetabs);

            $nbdispenser=0;

            $dispenserdatas=$etabs->getdispenserTeatcher($_SESSION['user']['IdCompte']);

            foreach ($dispenserdatas as $value):
              $code=$value->codeEtab;
              if (in_array($code, $tabetabs)) {
                $nbdispenser++;
               }
            endforeach;

            echo $nbdispenser;

            if($nbdispenser>0)
            {
              header("Location:../teatcher".$libelleEtab."/updatequizs.php?courseid=".$quizid."&classeid=".$classeid);
            }else {
              header("Location:../teatcher/updatequizs.php?courseid=".$quizid."&classeid=".$classeid);
            }



    }

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{

  $libellequestion=htmlspecialchars($_POST['libellesquestmuluti']);
  $quizid=htmlspecialchars($_POST['idquizmulti']);
  $pointquestion=htmlspecialchars($_POST['pointquestMulti']);
  $classeid=htmlspecialchars($_POST['classequizmulti']);

  $moderep=2;

  $questionid=$etabs->AddQuestion($libellequestion,$moderep,$pointquestion,$quizid);

  $concatquestmultiple=htmlspecialchars($_POST['concatmultipleprop']);
  $concatquestmultiple=substr($concatquestmultiple, 0, -1);
  $tabconcatquestmultiple=explode("@",$concatquestmultiple);

  $nbMulti=count($tabconcatquestmultiple);

  for($i=0;$i<$nbMulti;$i++)
  {
    $libelle_proposition=htmlspecialchars($_POST['libelle_proposition'.$tabconcatquestmultiple[$i]]);


    $proposiionid=$etabs->AddProposition($libelle_proposition,$questionid);

    if(isset($_POST['chk_proposition'.$tabconcatquestmultiple[$i]]))
    {
      $chk_proposition=htmlspecialchars($_POST['chk_proposition'.$tabconcatquestmultiple[$i]]);
      $etabs->UpdatePropositionValue($chk_proposition,$proposiionid);
    }

  }

  $datasquizs=$etabs->getInfosbyquizid($quizid);
  $codeEtab="";
  foreach ($datasquizs as  $value):
    $codeEtab=$value->codeEtab_quiz;
  endforeach;


  // $_SESSION['user']['addclasseok']="Une nouvelle question a été ajoutée au quiz avec succès";
  $_SESSION['user']['addclasseok']=L::NewQuestionQuizAddMessageSuccess;

  if($_SESSION['user']['profile'] == "Teatcher") {

      // header("Location:../teatcher/updatequizs.php?courseid=".$quizid."&classeid=".$classeid);

      $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etabs->DetermineTypeEtab($codeEtab);
            $IBSAschool=$etabs->getIbsaschools();
            $codeetabs="";
            foreach ($IBSAschool as  $value):
              $codeetabs=$codeetabs.$value->code_etab."*";
            endforeach;
            $tabetabs=explode("*",$codeetabs);

            $nbdispenser=0;

            $dispenserdatas=$etabs->getdispenserTeatcher($_SESSION['user']['IdCompte']);

            foreach ($dispenserdatas as $value):
              $code=$value->codeEtab;
              if (in_array($code, $tabetabs)) {
                $nbdispenser++;
               }
            endforeach;

            echo $nbdispenser;

            if($nbdispenser>0)
            {
              header("Location:../teatcher".$libelleEtab."/updatequizs.php?courseid=".$quizid."&classeid=".$classeid);
            }else {
              header("Location:../teatcher/updatequizs.php?courseid=".$quizid."&classeid=".$classeid);
            }



      }

}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //recuperation des variables

$concattrueorfalseid=htmlspecialchars($_POST['concattrueorfalseid']);
$studentid=htmlspecialchars($_POST['studentid']);
$quizid=htmlspecialchars($_POST['quizid']);
$classeid=htmlspecialchars($_POST['classeid']);
$matiereid=htmlspecialchars($_POST['matiereid']);
$concatqestmulti=htmlspecialchars($_POST['concatqestmulti']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);

//reponses des questions trueorfalse

$tabtrueorfalseid=explode("@",$concattrueorfalseid);
$nbtrueorfalse=count($tabtrueorfalseid)-1;
// echo $nbtrueorfalse;

for($i=0;$i<$nbtrueorfalse;$i++)
{

  $answer=htmlspecialchars($_POST['answer'.$tabtrueorfalseid[$i]]);


  if($answer==1)
  {
    $answer1=0;
    // echo "true";
    //nous allons chercher id de la proposition vrai et proposition faux

    $idtrue=$etabs->getPropositionTrueidofQuestion($tabtrueorfalseid[$i]);
    $idfalse=$etabs->getPropositionFalseidofQuestion($tabtrueorfalseid[$i]);


    $etabs->AddreponsequizStudentTrue($quizid,$studentid,$tabtrueorfalseid[$i],$idtrue,$answer,$matiereid,$classeid,$codeEtab,$sessionEtab);
    $etabs->AddreponsequizStudentFalse($quizid,$studentid,$tabtrueorfalseid[$i],$idfalse,$answer1,$matiereid,$classeid,$codeEtab,$sessionEtab);

    // echo $tabtrueorfalseid[$i]." - ".$idtrue." - ".$idfalse."</br>";

  }else if($answer==0)
  {
    $answer1=1;
    $idtrue=$etabs->getPropositionTrueidofQuestion($tabtrueorfalseid[$i]);
    $idfalse=$etabs->getPropositionFalseidofQuestion($tabtrueorfalseid[$i]);

    $etabs->AddreponsequizStudentTrue($quizid,$studentid,$tabtrueorfalseid[$i],$idtrue,$answer,$matiereid,$classeid,$codeEtab,$sessionEtab);
    $etabs->AddreponsequizStudentFalse($quizid,$studentid,$tabtrueorfalseid[$i],$idfalse,$answer1,$matiereid,$classeid,$codeEtab,$sessionEtab);


    // echo $tabtrueorfalseid[$i]." - ".$idtrue." - ".$idfalse."</br>";

  }


    //

}

//reponse des questions multiples

$tabconcatqestmulti=explode("@",$concatqestmulti);
$nbmultiple=count($tabconcatqestmulti)-1;

for($j=0;$j<$nbmultiple;$j++)
{

// echo $tabconcatqestmulti[$j]."</br>";
$cocher=htmlspecialchars($_POST['concatquestmulticocher'.$tabconcatqestmulti[$j]]);
$cocher=substr($cocher, 0, -1);
$tabcocher=explode("@",$cocher);
$nbtabcocher=count($tabcocher);

  $questionid=$tabconcatqestmulti[$j];
  //nous allons rechercher la liste des propositions pour cette question

  $datapropos=$etabs->getPropositionofQuestion($questionid);
  $concatpropos="";

  foreach ($datapropos as $value):
    $concatpropos=$concatpropos.$value->id_proprep."@";
  endforeach;

  $concatpropos=substr($concatpropos, 0, -1);
  $tabconcatpropos=explode("@",$concatpropos);
  $nbconcatpropos=count($tabconcatpropos);



for($k=0;$k<$nbconcatpropos;$k++)
{


     $propositionid=$tabconcatpropos[$k];

     if(in_array($propositionid,$tabcocher))
     {
       $answer=1;
         $etabs->AddreponsequizStudentTrue($quizid,$studentid,$questionid,$propositionid,$answer,$matiereid,$classeid,$codeEtab,$sessionEtab);
    // echo $propositionid."</br>";
      }else{
        $answer=0;
      // echo "pas trouvé"." -".$propositionid."</br>";
       $etabs->AddreponsequizStudentTrue($quizid,$studentid,$questionid,$propositionid,$answer,$matiereid,$classeid,$codeEtab,$sessionEtab);
     }





}

// echo $nbtabcocher."</br>";


// var_dump($tabcocher);

}

//nous allons faire le resultat de l'eleve au quiz

$coursesdetails=$etabs->getAllquizsdetailstudent($quizid,$classeid,$codeEtab,$sessionEtab);

foreach ($coursesdetails as  $datacourses):
  $descricourses=$datacourses->instruction_quiz;
  $durationcourses=$datacourses->duree_quiz;
  $teatchercourses=$datacourses->nom_compte;
  $datercourses=$datacourses->datelimite_quiz;
  $namecourses=$datacourses->libelle_quiz;
  $classecourses=$datacourses->libelle_classe;
  $classeidcourses=$datacourses->id_classe;
  $matiereidcourses=$datacourses->id_mat;
  $libellematcourses=$datacourses->libelle_mat;
  $statutcourses=$datacourses->statut_quiz;
  $teatcheridcourses=$datacourses->teatcher_quiz;
  $filescourses="";

endforeach;

$questions=$etabs->getAllquizQuestion($quizid,$classeid,$codeEtab,$sessionEtab,$teatcheridcourses);

$questiontrueorfalse=$etabs->getAllquizQuestionTrueOrfalse($quizid,$classeid,$codeEtab,$sessionEtab,$teatcheridcourses);

$questionmultiplechoice=$etabs->getAllquizQuestionMultiplechoice($quizid,$classeid,$codeEtab,$sessionEtab,$teatcheridcourses);


$concattrueorfalse="";
  $i=1;

  foreach ($questiontrueorfalse as  $value):
    $concattrueorfalse=$concattrueorfalse.$value->id_quest."@";

    $solutions="";
    $solutionids="";
    $solutionspropositions=$etabs->getsolutionsPropoReponses($quizid,$value->id_quest);

    foreach ($solutionspropositions as $valuesolution):
      $solutions=$solutions." ".$valuesolution->libelle_proprep;
      $solutionids=$solutionids.$valuesolution->id_proprep."@";
    endforeach;

    $propositionsAndreponses=$etabs->getPropositionsAndreponsesChild($studentid,$value->id_quest,$quizid,$codeEtab,$sessionEtab);

    // var_dump($propositionsAndreponses);
    $solutionsidStudent="";
    foreach ($propositionsAndreponses as $valueRep):
      if($valueRep->reponse_repquiz==1)
      {
        $solutionsidStudent=$solutionsidStudent.$valueRep->propositionid_repquiz."@";
      }
    endforeach;

    $tabsolutionsStudent=explode("@",substr($solutionsidStudent,0,-1));
    $tabsolutionids=explode("@",substr($solutionids,0,-1));
    $nbcont=count($tabsolutionsStudent);

    for($i=0;$i<$nbcont;$i++)
    {
      if($tabsolutionids[$i]==$tabsolutionsStudent[$i])
      {
        // echo "point obtenu";
        //Nous allons verifier si nous avons une note pour cette question
        $nbnotes=$etabs->getNumberOfstudentquizquestion($value->id_quest,$studentid,$quizid,$codeEtab,$sessionEtab);
        if($nbnotes==0)
        {
          $etabs->AddnotesQuizquestion($value->id_quest,$studentid,$quizid,$value->point_quest,$codeEtab,$sessionEtab);

        }


      }else {

         $nbnotes=$etabs->getNumberOfstudentquizquestion($value->id_quest,$studentid,$quizid,$codeEtab,$sessionEtab);
           $point=0;
         if($nbnotes==0)
         {

           $etabs->AddnotesQuizquestion($value->id_quest,$studentid,$quizid,$point,$codeEtab,$sessionEtab);

         }


      }
    }

    $i++;
  endforeach;


  $i=1;
  $concatqestmulti="";
    foreach ($questionmultiplechoice as  $value):
      $solutions="";
      $solutionids="";
      $solutionspropositions=$etabs->getsolutionsPropoReponses($quizid,$value->id_quest);

      foreach ($solutionspropositions as $valuesolution):
        $solutions=$solutions." ".$valuesolution->libelle_proprep;
        $solutionids=$solutionids.$valuesolution->id_proprep."@";
      endforeach;

      $concatqestmulti=$concatqestmulti.$value->id_quest."@";

      $propositionsreponses=$etabs->getPropositionsOfQuestions($value->id_quest);
      $j=1;
      $solutionsidStudent="";
      foreach ($propositionsreponses as $propositions):
        //retrouvons la reponse de l'etudiant face a cette proposition
        $valeurpropositionchoice=$etabs->getValeurOfpropositionchoice($value->id_quest,$quizid,$propositions->id_proprep,$codeEtab,$sessionEtab,$studentid);
        if($valeurpropositionchoice==1)
        {
          $solutionsidStudent=$solutionsidStudent.$propositions->id_proprep."@";
        }

      endforeach;

      $tabsolutionids=explode("@",substr($solutionids,0,-1));
      $tabsolutionsStudent=explode("@",substr($solutionsidStudent,0,-1));

      $cmp = array_diff($tabsolutionids, $tabsolutionsStudent);
      $nbcont=count($cmp);

      if($nbcont==0)
      {

        $nbnotes=$etabs->getNumberOfstudentquizquestion($value->id_quest,$studentid,$quizid,$codeEtab,$sessionEtab);
        if($nbnotes==0)
        {
          $etabs->AddnotesQuizquestion($value->id_quest,$studentid,$quizid,$value->point_quest,$codeEtab,$sessionEtab);

        }
      }else {
        // echo "point non obtenu";
        $nbnotes=$etabs->getNumberOfstudentquizquestion($value->id_quest,$studentid,$quizid,$codeEtab,$sessionEtab);
        $point=0;
        if($nbnotes==0)
        {
          $etabs->AddnotesQuizquestion($value->id_quest,$studentid,$quizid,$point,$codeEtab,$sessionEtab);

        }
     }

     $i++;
    endforeach;


// $_SESSION['user']['addclasseok']="Le quiz a été soumis avec succès";
$_SESSION['user']['addclasseok']=L::QuizSoumissMessageSuccess;


if($_SESSION['user']['profile'] == "Student") {

  $datasinscriptions=$etabs->getCodeEtabOfStudentInscript($_SESSION['user']['IdCompte']);
  foreach ($datasinscriptions as $datasinscription):
    $codeEtabInscript=$datasinscription->codeEtab_inscrip;
    $sessionEtabInscript=$datasinscription->session_inscrip;
    $classeidInscript=$datasinscription->idclasse_inscrip;
  endforeach;

     $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtabInscript);
      $typeetab=$etab->DetermineTypeEtab($codeEtabInscript);

      if($typeetab==5)
      {
        header("Location:../locale".$libelleEtab."/detailquizs.php?course=".$quizid."&classeid=".$classeid);
      }else {
        header("Location:../locale/detailquizs.php?course=".$quizid."&classeid=".$classeid);
      }

    // header("Location:../student/detailquizs.php?course=".$quizid."&classeid=".$classeid);

    }

}







 ?>
