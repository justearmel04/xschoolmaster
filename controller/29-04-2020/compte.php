<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
$classe = new Classe();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //Modification du compte pour un etablissement mixte

  //recupération des variables
  $loginTea=htmlspecialchars($_POST['loginTea']);
  $passTea=htmlspecialchars($_POST['passTea']);
  $idcompte=htmlspecialchars($_POST['idcompte']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);

  $_SESSION['user']['addteaok']="Les paramètres du compte ont bien été modifiés avec succès";

  $etab->UpdateStudentAccountCnx($loginTea,$passTea,$idcompte);

  if($_SESSION['user']['profile'] == "Admin_globale") {

  header("Location:../manager/adddiplomes.php?compte=".$idcompte);
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/adddiplomes.php?compte=".$idcompte);
      }else {


        header("Location:../locale".$libelleEtab."/detailsstudent.php?compte=".$idcompte);
        // header("Location:../locale/adddiplomes.php?compte=".$idcompte);
      }


      }


}

?>
