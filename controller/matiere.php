<?php
session_start();
//require_once('../class/Matiere.php');
require_once('../class/Etablissement.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$etab = new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //ajout du diplôme

  //recupération des variables
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $teatcher=htmlspecialchars(addslashes($_POST['teatcher']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $coefficient=htmlspecialchars(addslashes($_POST['coef']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));


  //inscription dans la table matiere et enseigner

  $etab->AddMatiere($matiere,$classe,$teatcher,$codeEtab,$coefficient,$libellesession);

  $_SESSION['user']['addsubjectok']=L::NewMatiereAddsuccessfully;



   // header("Location:../manager/matieres.php?classe=".$classe);
   if($_SESSION['user']['profile'] == "Admin_globale") {

         //header("Location:../manager/index.php");
         header("Location:../manager/schoolInfos.php?compte=".$codeEtab);

     }else if($_SESSION['user']['profile'] == "Admin_locale") {
       // if($_SESSION['user']['paysid']==4)
       // {
       //   header("Location:../localecmr/matieres.php");
       // }else {
       //  header("Location:../locale/matieres.php");
       // }

            $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
             $typeetab=$etab->DetermineTypeEtab($codeEtab);

             if($typeetab==5)
             {
               header("Location:../locale".$libelleEtab."/matieres.php");
             }else {
               header("Location:../locale/matieres.php");
             }

       }


}if(isset($_POST['etape'])&&($_POST['etape']==2))
{
    //recupération des variables
    $idmat=htmlspecialchars(addslashes($_POST['idmat']));
    $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
    $matiere=htmlspecialchars(addslashes($_POST['matiere'.$idmat]));
    $classe=htmlspecialchars(addslashes($_POST['classe'.$idmat]));
    $teatcher=htmlspecialchars(addslashes($_POST['teatcher'.$idmat]));
    $coefficient=htmlspecialchars(addslashes($_POST['coef'.$idmat]));

    //update de la table matiere et dispenser

    $etab->UpdateMatiere($matiere,$classe,$teatcher,$codeEtab,$coefficient,$idmat);
    $_SESSION['user']['updatesubjectok']=L::TheSubjectsModsuccessfully;


     // header("Location:../manager/matieres.php?classe=".$classe);
     if($_SESSION['user']['profile'] == "Admin_globale") {

           //header("Location:../manager/index.php");
           header("Location:../manager/matieres.php?classe=".$classe);

       }else if($_SESSION['user']['profile'] == "Admin_locale") {
         // if($_SESSION['user']['paysid']==4)
         // {
         //   header("Location:../localecmr/matieres.php?classe=".$classe);
         // }else {
         //   header("Location:../locale/matieres.php?classe=".$classe);
         // }

         $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
               $typeetab=$etab->DetermineTypeEtab($codeEtab);

               if($typeetab==5)
               {
                 header("Location:../locale".$libelleEtab."/matieres.php?classe=".$classe);
               }else {
                 header("Location:../locale/matieres.php?classe=".$classe);
               }
         }


}if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //recuperation des variables

  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  // $classe=htmlspecialchars(addslashes($_POST['classe']));
  $teatcher=htmlspecialchars(addslashes($_POST['teatcher']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $coefficient=htmlspecialchars(addslashes($_POST['coef']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

  foreach($_POST['classe'] as $valeur)
 {
   $classe=$valeur;

    $etab->AddMatiereBoucle($matiere,$classe,$teatcher,$codeEtab,$coefficient,$libellesession);

 }

 // $_SESSION['user']['addsubjectok']="Nouvelle Matière ajoutée avec succès";
 $_SESSION['user']['addsubjectok']=L::MatiereAddMessageSuccess;


  // header("Location:../manager/matieres.php?classe=".$classe);
    $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);

  if($_SESSION['user']['profile'] == "Admin_globale") {


        header("Location:../manager/schoolInfos.php?compte=".$codeEtab);

    }else if($_SESSION['user']['profile'] == "Admin_locale") {


        $etablissementType=$etab->DetermineTypeEtab($codeEtab);


        if($etablissementType==5)
        {
          header("Location:../locale".$libelleEtab."/matieres.php");
        }else {
          header("Location:../locale/matieres.php");
        }



      }

}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //recuperation des variables

  $matierelib=htmlspecialchars(utf8_encode($_POST['matierelib']));
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $session=htmlspecialchars($_POST['libellesession']);

  // echo $matierelib;

    $etab->AddMatiereLibelle($matierelib,$codeEtab,$session);

    $_SESSION['user']['addsubjectok']=L::LibMatiereAddMessageSuccess;

    // $_SESSION['user']['addsubjectok']="Libelle matière ajouté avec succès";


     // header("Location:../manager/matieres.php?classe=".$classe);
     if($_SESSION['user']['profile'] == "Admin_globale") {

           //header("Location:../manager/index.php");
           header("Location:../manager/schoolInfos.php?compte=".$codeEtab);

       }else if($_SESSION['user']['profile'] == "Admin_locale") {

           $etablissementType=$etab->DetermineTypeEtab($codeEtab);
           $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);

           if($etablissementType==5)
           {
             header("Location:../locale".$libelleEtab."/matieres.php");
           }else {
             header("Location:../locale/matieres.php");
           }





         }

}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
//recupeartion des variables

$matierelib=htmlspecialchars(utf8_encode($_POST['matiere']));
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['libellesession']);
$coef=htmlspecialchars($_POST['coef']);
$classe=htmlspecialchars($_POST['classe']);

//nous allons ajouter la matiere

$etab->AddMateieresclassiques($matierelib,$codeEtab,$sessionEtab,$coef,$classe);

$_SESSION['user']['addsubjectok']=L::MatiereAddMessageSuccess;


$libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);

if($_SESSION['user']['profile'] == "Admin_globale") {


    header("Location:../manager/schoolInfos.php?compte=".$codeEtab);

}else if($_SESSION['user']['profile'] == "Admin_locale") {


    $etablissementType=$etab->DetermineTypeEtab($codeEtab);


    if($etablissementType==5)
    {
      header("Location:../locale".$libelleEtab."/matieres.php");
    }else {
      header("Location:../locale/matieres.php");
    }



  }



}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{
  $matierelib=htmlspecialchars(utf8_encode($_POST['matiere']));
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['libellesession']);
  $coef=htmlspecialchars($_POST['coef']);
  $classe=htmlspecialchars($_POST['classe']);
  $langue=htmlspecialchars($_POST['langue']);

$etab->AddMateieresclassiquesWithlangue($matierelib,$codeEtab,$sessionEtab,$coef,$classe,$langue);

$_SESSION['user']['addsubjectok']=L::MatiereAddMessageSuccess;


$libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);

if($_SESSION['user']['profile'] == "Admin_globale") {


    header("Location:../manager/schoolInfos.php?compte=".$codeEtab);

}else if($_SESSION['user']['profile'] == "Admin_locale") {


    $etablissementType=$etab->DetermineTypeEtab($codeEtab);


    if($etablissementType==5)
    {
      header("Location:../locale".$libelleEtab."/matieres.php");
    }else {
      header("Location:../locale/matieres.php");
    }



  }

}

if(isset($_GET['etape'])&&($_GET['etape']==3))
{
  //il est question de supprimer la matière

  //recupération des variables

  $classe=htmlspecialchars(addslashes($_GET['classe']));
  $idmat=htmlspecialchars(addslashes($_GET['matiere']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));

    $etab->DelatedMatiere($idmat,$classe,$codeEtab);

    // $_SESSION['user']['addsubjectok']="La matière a bien été supprimée avec succès";
    $_SESSION['user']['addsubjectok']=L::MatiereDeleteMessageSuccess;


     // header("Location:../manager/matieres.php?classe=".$classe);
     if($_SESSION['user']['profile'] == "Admin_globale") {

           //header("Location:../manager/index.php");
           // header("Location:../manager/matieres.php?classe=".$classe);
           header("Location:../manager/schoolInfos.php?compte=".$codeEtab);

       }else if($_SESSION['user']['profile'] == "Admin_locale") {

           $etablissementType=$etab->DetermineTypeEtab($codeEtab);
           $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);

           if($etablissementType==5)
           {
             header("Location:../locale".$libelleEtab."/matieres.php");
           }else {
             header("Location:../locale/matieres.php");
           }




         }


}

?>
