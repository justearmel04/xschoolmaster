<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Matiere.php');
require_once('../controller/functions.php');
require_once('../class/Fileuploader.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$classe = new Classe();
$etabs=new Etab();
$matiere=new Matiere();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recuperation des variables
$libelledevoir=htmlspecialchars($_POST['libellecourse']);
$classeEtab=htmlspecialchars($_POST['classeEtab']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);
$destinataires="";

$instructionsdevoir=htmlspecialchars($_POST['detailscourse']);
$concatmatiere=htmlspecialchars($_POST['matclasse']);
$tabmatiere=explode("-",$concatmatiere);
$matiereid=$tabmatiere[0];
$teatcherid=$tabmatiere[1];
$datelimitedevoir=dateFormat(htmlspecialchars($_POST['datecourse']));
$verouillerdevoir=htmlspecialchars($_POST['verouiller']);
$libellematiere=$etabs->getMatiereLibelleByIdMat($matiereid,$codeEtab);
$libelleclasse=$classe->getInfosofclassesbyId($classeEtab,$sessionEtab);
$statutdevoir=0;
$destinataires="";
foreach($_POST['destiEtab'] as $valeur)
{
 $destinataires=$destinataires.$valeur.'@';

}

$destinataires=substr($destinataires, 0, -1);

$fileError = $_FILES['fichier3']['error'];

if($fileError==0)
{
  //fichier uploader

  $file_name = @$_FILES['fichier3']['name'];
  $_SESSION["fichier3"] = $file_name;
  $file_size =@$_FILES['fichier3']['size'];
  $file_tmp =@$_FILES['fichier3']['tmp_name'];
  $file_type=@$_FILES['fichier3']['type'];
  @$file_ext=strtolower(end(explode('.',@$_FILES['fichier3']['name'])));
  $fichierTemp = uniqid() . "." . $file_ext;

  $date=date("Y-m-d");

  $tabdate=explode("-",$date);
  $years=$tabdate[0];
  $mois=$tabdate[1];
  $days=$tabdate[2];
  $libellemois=obtenirLibelleMois($mois);

  $devoirid=$etabs->AddDevoirs($libelledevoir,$datelimitedevoir,$classeEtab,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$instructionsdevoir,$verouillerdevoir,$destinataires,$statutdevoir);

  $transactionId =  "DEVOIR_".date("Y")."_".$codeEtab."_".$classeEtab."_".$devoirid;
  $fichierad=$transactionId.".".$file_ext;

  $etabs->UpdatedevoirFileName($fichierad,$devoirid,$codeEtab,$sessionEtab);

  move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

  $dossier="../devoirs/Etablissements/";
  $dossier1="../devoirs/Etablissements/".$codeEtab."/";
  $dossier2="../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere);
  $dossier3="../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/";

  if(!is_dir($dossier)) {
      @mkdir($dossier);
      if(!is_dir($dossier1)) {
          @mkdir($dossier1);

          if(!is_dir($dossier2)) {
              @mkdir($dossier2);

          }

          if(!is_dir($dossier3)) {
              @mkdir($dossier3);

          }

      }
  }else {
    if(!is_dir($dossier1)) {
        @mkdir($dossier1);

        if(!is_dir($dossier2)) {
            @mkdir($dossier2);

        }

        if(!is_dir($dossier3)) {
            @mkdir($dossier3);

        }

    }
  }

  @rename('../temp/' . $fichierTemp ,"../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/".$fichierad);

 //Suppression du fichier se trouvant dans le dossier temp

  @unlink("../temp/" . $fichierTemp);


// $_SESSION['user']['addclasseok']="Le devoir a bien été ajouté avec succès";
$_SESSION['user']['addclasseok']=L::DevoirsAddMessageSuccess;


}else {
  // aucun fichier uploader
}


// header("Location:../teatcher/listdevoirs.php");

            $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etabs->DetermineTypeEtab($codeEtab);
            $IBSAschool=$etabs->getIbsaschools();
            $codeetabs="";
            foreach ($IBSAschool as  $value):
              $codeetabs=$codeetabs.$value->code_etab."*";
            endforeach;
            $tabetabs=explode("*",$codeetabs);

            $nbdispenser=0;

            $dispenserdatas=$etabs->getdispenserTeatcher($_SESSION['user']['IdCompte']);

            foreach ($dispenserdatas as $value):
              $code=$value->codeEtab;
              if (in_array($code, $tabetabs)) {
                $nbdispenser++;
               }
            endforeach;

            echo $nbdispenser;

            header("Location:../teatcher".$libelleEtab."/listdevoirs.php");

            // if($nbdispenser>0)
            // {
            //   header("Location:../teatcher".$libelleEtab."/listdevoirs.php");
            // }else {
            //   header("Location:../teatcher/listdevoirs.php");
            // }







}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  $libelledevoir=htmlspecialchars($_POST['libellecourse']);
  $classeEtab=htmlspecialchars($_POST['classeEtab']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $devoirid=htmlspecialchars($_POST['courseid']);

  $instructionsdevoir=htmlspecialchars($_POST['detailscourse']);
  $concatmatiere=htmlspecialchars($_POST['matclasse']);
  $tabmatiere=explode("-",$concatmatiere);
  $matiereid=$tabmatiere[0];
  $teatcherid=$tabmatiere[1];
  $datelimitedevoir=dateFormat(htmlspecialchars($_POST['datecourse']));
  $verouillerdevoir=htmlspecialchars($_POST['verouiller']);
  $libellematiere=$etabs->getMatiereLibelleByIdMat($matiereid,$codeEtab);
  $libelleclasse=$classe->getInfosofclassesbyId($classeEtab,$sessionEtab);



  $destinataires="";
  foreach($_POST['destiEtab'] as $valeur)
  {
   $destinataires=$destinataires.$valeur.'@';

  }

  $destinataires=substr($destinataires, 0, -1);

  $fileError = $_FILES['fichier3']['error'];

  if($fileError==0)
  {
    //fichier uploader
    // echo "fichier uploader";

    $oldfilename=htmlspecialchars($_POST['oldfilename']);

    $file_name = @$_FILES['fichier3']['name'];
    $_SESSION["fichier3"] = $file_name;
    $file_size =@$_FILES['fichier3']['size'];
    $file_tmp =@$_FILES['fichier3']['tmp_name'];
    $file_type=@$_FILES['fichier3']['type'];
    @$file_ext=strtolower(end(explode('.',@$_FILES['fichier3']['name'])));
    $fichierTemp = uniqid() . "." . $file_ext;

    $date=date("Y-m-d");

    $tabdate=explode("-",$date);
    $years=$tabdate[0];
    $mois=$tabdate[1];
    $days=$tabdate[2];
    $libellemois=obtenirLibelleMois($mois);



    if(strlen($oldfilename)>0)
    {
      //pas la premiere fois
      $taboldfilename=explode(".",$oldfilename);
      $libelleoldfilename=$taboldfilename[0];

      $fichierad=$libelleoldfilename.".".$file_ext;

        @unlink("../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/".$oldfilename);

    }else {
      // premiere fois
      $transactionId =  "DEVOIR_".date("Y")."_".$codeEtab."_".$classeEtab."_".$devoirid;
      $fichierad=$transactionId.".".$file_ext;

    }

    move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

    $dossier="../devoirs/Etablissements/";
    $dossier1="../devoirs/Etablissements/".$codeEtab."/";
    $dossier2="../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere);
    $dossier3="../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/";

    if(!is_dir($dossier)) {
        @mkdir($dossier);
        if(!is_dir($dossier1)) {
            @mkdir($dossier1);

            if(!is_dir($dossier2)) {
                @mkdir($dossier2);

            }

            if(!is_dir($dossier3)) {
                @mkdir($dossier3);

            }

        }
    }else {
      if(!is_dir($dossier1)) {
          @mkdir($dossier1);

          if(!is_dir($dossier2)) {
              @mkdir($dossier2);

          }

          if(!is_dir($dossier3)) {
              @mkdir($dossier3);

          }

      }
    }




    @rename('../temp/' . $fichierTemp ,"../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/".$fichierad);

   //Suppression du fichier se trouvant dans le dossier temp

    @unlink("../temp/" . $fichierTemp);

      $etabs->UpdateDevoirWithoutFile($libelledevoir,$datelimitedevoir,$classeEtab,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$instructionsdevoir,$verouillerdevoir,$destinataires,$devoirid);

      $etabs->UpdatedevoirFileName($fichierad,$devoirid,$codeEtab,$sessionEtab);




  }else {
    //fichier non uploader
    echo "fichier non uploader";

    //nous allons modifier les elements du devoir

    $etabs->UpdateDevoirWithoutFile($libelledevoir,$datelimitedevoir,$classeEtab,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$instructionsdevoir,$verouillerdevoir,$destinataires,$devoirid);

  }

  // $_SESSION['user']['addclasseok']="Le devoir a bien été modifié avec succès";
  $_SESSION['user']['addclasseok']=L::DevoirsModMessageSuccess;

  // header("Location:../teatcher/listdevoirs.php");

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
              $typeetab=$etabs->DetermineTypeEtab($codeEtab);
              $IBSAschool=$etabs->getIbsaschools();
              $codeetabs="";
              foreach ($IBSAschool as  $value):
                $codeetabs=$codeetabs.$value->code_etab."*";
              endforeach;
              $tabetabs=explode("*",$codeetabs);

              $nbdispenser=0;

              $dispenserdatas=$etabs->getdispenserTeatcher($_SESSION['user']['IdCompte']);

              foreach ($dispenserdatas as $value):
                $code=$value->codeEtab;
                if (in_array($code, $tabetabs)) {
                  $nbdispenser++;
                 }
              endforeach;

              echo $nbdispenser;

              // if($nbdispenser>0)
              // {
              //   header("Location:../teatcher".$libelleEtab."/listdevoirs.php");
              // }else {
              //   header("Location:../teatcher/listdevoirs.php");
              // }

              header("Location:../teatcher".$libelleEtab."/listdevoirs.php");




}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{

  $iddevoir=htmlspecialchars($_POST['iddevoir']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $matiereid=htmlspecialchars($_POST['matiereid']);
  $classeEtab=htmlspecialchars($_POST['classedevoir']);
  $teatcherid=htmlspecialchars($_POST['teatcherid']);



  $libellematiere=$etabs->getMatiereLibelleByIdMat($matiereid,$codeEtab);
  $libelleclasse=$classe->getInfosofclassesbyId($classeEtab,$sessionEtab);

  $fileError = $_FILES['fichier3']['error'];

  if($fileError==0)
  {
    $file_name = @$_FILES['fichier3']['name'];
    $_SESSION["fichier3"] = $file_name;
    $file_size =@$_FILES['fichier3']['size'];
    $file_tmp =@$_FILES['fichier3']['tmp_name'];
    $file_type=@$_FILES['fichier3']['type'];
    @$file_ext=strtolower(end(explode('.',@$_FILES['fichier3']['name'])));
    $fichierTemp = uniqid() . "." . $file_ext;

    $date=date("Y-m-d");

    $tabdate=explode("-",$date);
    $years=$tabdate[0];
    $mois=$tabdate[1];
    $days=$tabdate[2];
    $libellemois=obtenirLibelleMois($mois);

    $transactionId =  "DEVOIRSTUDENT_".date("Y")."_".$codeEtab."_".$classeEtab."_".$iddevoir."_".$_SESSION['user']['IdCompte'];
    $fichierad=$transactionId.".".$file_ext;

    move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

    $dossier="../devoirs/Etablissements/";
    $dossier1="../devoirs/Etablissements/".$codeEtab."/";
    $dossier2="../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere);
    $dossier3="../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/";
    $dossier4="../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/Rendus/";


    if(!is_dir($dossier)) {
        @mkdir($dossier);
        if(!is_dir($dossier1)) {
            @mkdir($dossier1);

            if(!is_dir($dossier2)) {
                @mkdir($dossier2);

            }

            if(!is_dir($dossier3)) {
                @mkdir($dossier3);

            }

            if(!is_dir($dossier4)) {
                @mkdir($dossier4);

            }

        }
    }else {
      if(!is_dir($dossier1)) {
          @mkdir($dossier1);

          if(!is_dir($dossier2)) {
              @mkdir($dossier2);

          }

          if(!is_dir($dossier3)) {
              @mkdir($dossier3);

          }

          if(!is_dir($dossier4)) {
              @mkdir($dossier4);

          }

      }
    }

      @mkdir($dossier4);


    @rename('../temp/' . $fichierTemp ,"../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/Rendus/".$fichierad);

   //Suppression du fichier se trouvant dans le dossier temp

    @unlink("../temp/" . $fichierTemp);

    //ajout dans la table rendus

    $type="DEVOIR";
    $statut=1;

    $etabs->Addrendustudent($type,$iddevoir,$_SESSION['user']['IdCompte'],$classeEtab,$matiereid,$codeEtab,$sessionEtab,$fichierad,$teatcherid,$statut);


  }

  // $_SESSION['user']['addclasseok']="Le devoir a bien été déposé avec succès";
  $_SESSION['user']['addclasseok']=L::DevoirsDeposerMessageSuccess;

  $datasinscriptions=$etabs->getCodeEtabOfStudentInscript($_SESSION['user']['IdCompte']);
  foreach ($datasinscriptions as $datasinscription):
    $codeEtabInscript=$datasinscription->codeEtab_inscrip;
    $sessionEtabInscript=$datasinscription->session_inscrip;
    $classeidInscript=$datasinscription->idclasse_inscrip;

    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtabInscript);
      $typeetab=$etabs->DetermineTypeEtab($codeEtabInscript);

      if($typeetab==5)
      {
        header("Location:../locale".$libelleEtab."/detaildevoirs.php?course=".$iddevoir."&classeid=".$classeEtab);
      }else {
        header("Location:../locale/detaildevoirs.php?course=".$iddevoir."&classeid=".$classeEtab);
      }

  endforeach;

  // header("Location:../student/detaildevoirs.php?course=".$iddevoir."&classeid=".$classeEtab);


}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{


  // initialize FileUploader
      $FileUploader = new FileUploader('files', array(
          'uploadDir' => '../temp/',
      ));

      $data = $FileUploader->upload();
      // ici j'ai inseré le code qui permet d'ajouter sans la photo
        if($data['isSuccess'] ) {

          $libelledevoir=htmlspecialchars($_POST['libellecourse']);
          $classeEtab=htmlspecialchars($_POST['classeEtab']);
          $codeEtab=htmlspecialchars($_POST['codeEtab']);
          $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
          $destinataires="";



          $instructionsdevoir=htmlspecialchars($_POST['detailscourse']);
          $concatmatiere=htmlspecialchars($_POST['matclasse']);
          $tabmatiere=explode("-",$concatmatiere);
          $matiereid=$tabmatiere[0];
          $teatcherid=$tabmatiere[1];
          $datelimitedevoir=dateFormat(htmlspecialchars($_POST['datecourse']));
          $verouillerdevoir=htmlspecialchars($_POST['verouiller']);
          $libellematiere=$etabs->getMatiereLibelleByIdMat($matiereid,$codeEtab);
          $libelleclasse=$classe->getInfosofclassesbyId($classeEtab,$sessionEtab);
          $statutdevoir=0;
          $destinataires="";
          foreach($_POST['destiEtab'] as $valeur)
          {
           $destinataires=$destinataires.$valeur.'@';

          }

          $destinataires=substr($destinataires, 0, -1);

          $date=date("Y-m-d");

          $tabdate=explode("-",$date);
          $years=$tabdate[0];
          $mois=$tabdate[1];
          $days=$tabdate[2];
          $libellemois=obtenirLibelleMois($mois);

          $devoirid=$etabs->AddDevoirs($libelledevoir,$datelimitedevoir,$classeEtab,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$instructionsdevoir,$verouillerdevoir,$destinataires,$statutdevoir);
          $transactionId =  "DEVOIR_".date("Y")."_".$codeEtab."_".$classeEtab."_".$devoirid;

          $dossier="../devoirs/Etablissements/";
          $dossier1="../devoirs/Etablissements/".$codeEtab."/";
          $dossier2="../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere);
          $dossier3="../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/";

          if(!is_dir($dossier)) {
              @mkdir($dossier);
              if(!is_dir($dossier1)) {
                  @mkdir($dossier1);
                  @mkdir($dossier2);
                  @mkdir($dossier3);




              }
          }else {
            echo "premier dossier existe<br>";
            if(!is_dir($dossier1)) {
                @mkdir($dossier1);
                @mkdir($dossier2);
                @mkdir($dossier3);

                if(!is_dir($dossier2)) {
                    @mkdir($dossier2);
                    @mkdir($dossier3);

                }else {
                  echo "dossier2 existe<br>";
                  if(!is_dir($dossier3)) {
                      @mkdir($dossier3);

                  }
                }

                if(!is_dir($dossier3)) {
                    @mkdir($dossier3);

                }else {
                echo "dossier3 existe<br>";
                }

            }else{
              echo "dossier1 existe<br>";
              if(!is_dir($dossier2)) {
                  @mkdir($dossier2);
                  @mkdir($dossier3);

              }else {
                echo "dossier2 existe<br>";
                if(!is_dir($dossier3)) {
                    @mkdir($dossier3);

                }else {
                  echo "dossier3 existe<br>";
                }
              }
            }
          }

          $datas=$data['files'];
          $i=1;
        foreach ($datas as $value) {

          // var_dump($value);
          // echo $value['date'];

          $file_ext=$value['extension'];
          $file_tmp=$value['file'];
          $typesupport="DEVOIRS";

          echo $file_tmp."</br>";
            // $fichierad=$transactionId.".".$file_ext;

          $fichierad=$transactionId.$i.".".$file_ext;

          echo "../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/".$fichierad;


          @rename($file_tmp ,"../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/".$fichierad);

               //insertion dans la base de données

               $etabs->Addsupports($devoirid,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$classeEtab,$fichierad,$typesupport);


                     //Suppression du fichier se trouvant dans le dossier temp

          @unlink($file_tmp);

          $i++;
        }

        // $_SESSION['user']['addclasseok']="Un nouveau devoir a été ajouté avec succès";
        $_SESSION['user']['addclasseok']=L::NewDevoirsDeposerMessageSuccess;

        if($_SESSION['user']['profile'] == "Teatcher") {

            // header("Location:../teatcher/listdevoirs.php");

            $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etabs->DetermineTypeEtab($codeEtab);
            $IBSAschool=$etabs->getIbsaschools();
            $codeetabs="";
            foreach ($IBSAschool as  $value):
              $codeetabs=$codeetabs.$value->code_etab."*";
            endforeach;
            $tabetabs=explode("*",$codeetabs);

            $nbdispenser=0;

            $dispenserdatas=$etabs->getdispenserTeatcher($_SESSION['user']['IdCompte']);

            foreach ($dispenserdatas as $value):
              $code=$value->codeEtab;
              if (in_array($code, $tabetabs)) {
                $nbdispenser++;
               }
            endforeach;

            echo $nbdispenser;

              header("Location:../teatcher".$libelleEtab."/listdevoirs.php");

            // if($nbdispenser>0)
            // {
            //   header("Location:../teatcher".$libelleEtab."/listdevoirs.php");
            // }else {
            //   header("Location:../teatcher/listdevoirs.php");
            // }


          }else if($_SESSION['user']['profile'] == "Admin_locale")
          {

            $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etabs->DetermineTypeEtab($codeEtab);

            echo $typeetab;

            if($typeetab==5)
            {
                header("Location:../locale".$libelleEtab."/devoirs.php");
            }else {
              header("Location:../locale/devoirs.php");
            }

          }


        }else {
          echo "bonsoir";
        }





}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{


$FileUploader = new FileUploader('files', array(
    'uploadDir' => '../temp/',
));

$data = $FileUploader->upload();

if($data['isSuccess'] && count($data['files']) > 0) {

  $classeEtab=htmlspecialchars($_POST['classeEtab']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $devoirsid=htmlspecialchars($_POST['devoirsid']);
  $matiereid=htmlspecialchars($_POST['matiereid']);
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $typedevoirs="DEVOIRS";

  $libellematiere=$etabs->getMatiereLibelleByIdMat($matiereid,$codeEtab);
  $libelleclasse=$classe->getInfosofclassesbyId($classeEtab,$sessionEtab);

  $transactionId =  "DEVOIR_".date("Y")."_".$codeEtab."_".$classeEtab."_".$devoirsid;

  $dossier="../devoirs/Etablissements/";
  $dossier1="../devoirs/Etablissements/".$codeEtab."/";
  $dossier2="../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere);
  $dossier3="../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/";

  if(!is_dir($dossier)) {
      @mkdir($dossier);
      if(!is_dir($dossier1)) {
          @mkdir($dossier1);

          if(!is_dir($dossier2)) {
              @mkdir($dossier2);

          }

          if(!is_dir($dossier3)) {
              @mkdir($dossier3);

          }

      }
  }else {
    if(!is_dir($dossier1)) {
        @mkdir($dossier1);

        if(!is_dir($dossier2)) {
            @mkdir($dossier2);

        }

        if(!is_dir($dossier3)) {
            @mkdir($dossier3);

        }

    }
  }

  $datas=$data['files'];
  $i=$etabs->getLastFilesupportsDevoirs($devoirsid,$teatcherid,$codeEtab,$sessionEtab,$classeEtab,$typedevoirs);

  echo $i;

  foreach ($datas as $value) {
    $i++;

    // var_dump($value);
    // echo $value['date'];

    $file_ext=$value['extension'];
    $file_tmp=$value['file'];
    $typesupport="DEVOIRS";
      // $fichierad=$transactionId.".".$file_ext;

    $fichierad=$transactionId.$i.".".$file_ext;


    @rename($file_tmp ,"../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/".$fichierad);

         //insertion dans la base de données

         $etabs->Addsupports($devoirsid,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$classeEtab,$fichierad,$typesupport);


               //Suppression du fichier se trouvant dans le dossier temp

    @unlink($file_tmp);

    $i++;
  }

  $_SESSION['user']['addclasseok']=L::SupportsDevAddedOk;

  if($_SESSION['user']['profile'] == "Teatcher") {

      // header("Location:../teatcher/listdevoirs.php");
      // header("Location:../teatcher/updatedevoirs.php?courseid=".$devoirsid."&classeid=".$classeEtab);
      // updatedevoirs.php?courseid=10&classeid=1

      $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etabs->DetermineTypeEtab($codeEtab);
            $IBSAschool=$etabs->getIbsaschools();
            $codeetabs="";
            foreach ($IBSAschool as  $value):
              $codeetabs=$codeetabs.$value->code_etab."*";
            endforeach;
            $tabetabs=explode("*",$codeetabs);

            $nbdispenser=0;

            $dispenserdatas=$etabs->getdispenserTeatcher($_SESSION['user']['IdCompte']);

            foreach ($dispenserdatas as $value):
              $code=$value->codeEtab;
              if (in_array($code, $tabetabs)) {
                $nbdispenser++;
               }
            endforeach;

            echo $nbdispenser;

              header("Location:../teatcher".$libelleEtab."/updatedevoirs.php?courseid=".$devoirsid."&classeid=".$classeEtab);

            // if($nbdispenser>0)
            // {
            //   header("Location:../teatcher".$libelleEtab."/updatedevoirs.php?courseid=".$devoirsid."&classeid=".$classeEtab);
            // }else {
            //   header("Location:../teatcher/updatedevoirs.php?courseid=".$devoirsid."&classeid=".$classeEtab);
            // }


    }else if($_SESSION['user']['profile'] == "Admin_locale")
    {
      $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
      $typeetab=$etabs->DetermineTypeEtab($codeEtab);

      if($typeetab==5)
      {
          header("Location:../locale".$libelleEtab."/updatedevoirs.php?courseid=".$devoirsid."&classeid=".$classeEtab);
      }else {
          header("Location:../locale/updatedevoirs.php?courseid=".$devoirsid."&classeid=".$classeEtab);
      }

    }





}

// etape: 5
// codeEtab: 2019-2020
// sessionEtab: C00123
// classeEtab: 1
// devoirsid: 10
// fileuploader-list-files: ["0:/COURSES202005FRA21 (1).csv"]
// files[]: COURSES202005FRA21 (1).csv
// files[]:


}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{

}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{
  echo "bonsoir";
}





 ?>
