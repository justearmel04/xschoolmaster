<?php

// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}


//require_once('../../class1/User.php');

//require_once('../../class1/cnx.php');

//require_once('../../class1/Parent.php');

//require_once('../../class1/Etablissement.php');

require_once('../../class/cnx.php');

require_once('../../class1/functions.php');

$db = new mysqlConnector();

//$users = new User();

//$parents=new ParentX();

//$etabs=new Etab();





if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Recuperation des donnees de la premiere etape : Infos Client

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

foreach ($request as  $value):

$datedeb=$value->datedeb;
$datefin=$value->datefin;
$classeid=$value->classeid;
$codeEtab=$value->codeEtab;
$sessionEtab=$value->sessionEtab;

$datetime1 = date_create(datedeb); // Date fixe
$datetime2 = date_create($datefin); // Date fixe
$interval = date_diff($datetime1, $datetime2);
$nb= $interval->format('%a');
 $outp = "";


for($x=0;$x<=$nb;$x++)
{

$dateday= date("Y-m-d", strtotime("+".$x."day", strtotime($datedeb])));

$code="SELECT * FROM presences where date_presence='$dateday' and codeEtab_presence='$codeEtab' and session_presence='$sessionEtab' and classe_presence='$classeid' and statut_presence=0";
$req=$db->dataBase->prepare($req);
$req->execute([]);
$data=$req->fetchAll();
$nbabsebnces=count($data);

if($nbabsebnces>0)
 {
   $code = $this->db->prepare("SELECT * FROM presences,eleve where presences.matricule_presence=eleve.matricule_eleve  and date_presence=? and codeEtab_presence=? and session_presence=? and classe_presence=? and statut_presence=0 or statut_presence=2  ");

    $req1=$db->dataBase->prepare($code);
    $req1->execute([]);
    $datas=$req1->fetchAll();

	foreach ($datas as $value):
	if ($outp != "") {$outp .= ",";}
	$matricule=$value->matricule_presence;
        $nomcomplet=$value->nom_eleve." ".$value->prenom_eleve;
        $dateabsencesday=date_format(date_create($dateday), "d-m-Y");
        $heure=$value->libelleheure_presence;
        $statut="A";
	$outp .= '{"matricule":"'.$matricule.'",';
        $outp .= '"studentnames":"'.$nomcomplet.'",';
        $outp .= '"dateabsences":"'.$dateabsencesday.'",';
        $outp .= '"status":"'.$statut.'",';
        $outp .= '"heure":"'.$heure. '"}';

        endforeach;


 }




}

$concat="[".$outp."]";
  return $concat;


}



?>
