<div class="white-sidebar-color sidebar-container">
 				<div class="sidemenu-container navbar-collapse collapse fixed-menu">
	                <div id="remove-scroll" class="left-sidemenu">
	                    <ul class="sidemenu  page-header-fixed slimscroll-style" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
	                        <li class="sidebar-toggler-wrapper hide">
	                            <div class="sidebar-toggler">
	                                <span></span>
	                            </div>
	                        </li>
	                        <li class="sidebar-user-panel">
	                            <div class="user-panel">
	                                <div class="pull-left image">
	                                    <img src="<?php echo $lienphoto?>" class="img-circle user-img-circle" alt="User Image" />
	                                </div>
	                                <div class="pull-left info">
	                                    <p> <?php echo $tablogin[0];?></p>
	                                    <small>
                                        <?php

                                          if($tablogin[1]=="Admin_globale")
                                          {
                                            echo "Administrateur";
                                          }else if($tablogin[1]=="Admin_local")
                                          {
                                            echo "Admin Local";
                                          }else if($tablogin[1]=="Teatcher")
                                          {
                                            echo "Professeur";
                                          }else if($tablogin[1]=="Parent")
                                          {
                                            echo "Parent";
                                          }
                                        ?>
                                      </small>
	                                </div>
	                            </div>
	                        </li>
                          <li class="nav-item active open">
	                            <a href="index.php" class="nav-link nav-toggle">
	                                <i class="material-icons">dashboard</i>
	                                <span class="title">Dashboard</span>
	                                <span class="selected"></span>

	                            </a>

	                        </li>
                          <li class="nav-item">
	                            <a href="javascript:;" class="nav-link nav-toggle">
	                                <i class="material-icons">slideshow</i>
	                                <span class="title">Elèves</span>
	                                <span class="arrow "></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="javascript:;" class="nav-link nav-toggle">
	                                        <i class="fa fa-university"></i>Classe
	                                        <span class="arrow"></span>
	                                    </a>
	                                    <ul class="sub-menu">
                                          <?php
                                          if($nbclasse>0)
                                          {
                                           ?>
	                                        <li class="nav-item">
	                                            <a href="#" class="nav-link">
	                                                <i class="fa fa-file-pdf-o"></i> Sample Link 1</a>
	                                        </li>
	                                      <?php
                                      }else {
                                        ?>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                <i class=""></i>Aucune classe</a>
                                        </li>
                                        <?php
                                      }
                                         ?>

	                                    </ul>
	                                </li>


	                            </ul>
	                        </li>
                          <li class="nav-item">
	                            <a href="teatchers.php" class="nav-link nav-toggle">
	                                <i class="material-icons">dashboard</i>
	                                <span class="title">Professeur</span>
	                                <span class="selected"></span>

	                            </a>

	                        </li>
                          <li class="nav-item">
	                            <a href="javascript:;" class="nav-link nav-toggle">
	                                <i class="material-icons">slideshow</i>
	                                <span class="title"><?php echo L::MatiereMenu ?></span>
	                                <span class="arrow "></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="javascript:;" class="nav-link nav-toggle">
	                                        <i class="fa fa-university"></i>Classe
	                                        <span class="arrow"></span>
	                                    </a>
	                                    <ul class="sub-menu">
                                          <?php
                                          if($nbclasse>0)
                                          {
                                           ?>
	                                        <li class="nav-item">
	                                            <a href="#" class="nav-link">
	                                                <i class="fa fa-file-pdf-o"></i> Sample Link 1</a>
	                                        </li>
	                                      <?php
                                      }else {
                                        ?>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                <i class=""></i>Aucune classe</a>
                                        </li>
                                        <?php
                                      }
                                         ?>

	                                    </ul>
	                                </li>


	                            </ul>
	                        </li>
                          <li class="nav-item">
	                            <a href="javascript:;" class="nav-link nav-toggle">
	                                <i class="material-icons">slideshow</i>
	                                <span class="title">Emploi du temps</span>
	                                <span class="arrow "></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="javascript:;" class="nav-link nav-toggle">
	                                        <i class="fa fa-university"></i>Classe
	                                        <span class="arrow"></span>
	                                    </a>
	                                    <ul class="sub-menu">
                                          <?php
                                          if($nbclasse>0)
                                          {
                                           ?>
	                                        <li class="nav-item">
	                                            <a href="#" class="nav-link">
	                                                <i class="fa fa-file-pdf-o"></i> Sample Link 1</a>
	                                        </li>
	                                      <?php
                                      }else {
                                        ?>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                <i class=""></i>Aucune classe</a>
                                        </li>
                                        <?php
                                      }
                                         ?>

	                                    </ul>
	                                </li>


	                            </ul>
	                        </li>
                          <li class="nav-item">
	                            <a href="javascript:;" class="nav-link nav-toggle">
	                                <i class="material-icons">slideshow</i>
	                                <span class="title">Présences</span>
	                                <span class="arrow "></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item">
	                                    <a href="javascript:;" class="nav-link nav-toggle">
	                                        <i class="fa fa-university"></i>Classe
	                                        <span class="arrow"></span>
	                                    </a>
	                                    <ul class="sub-menu">
                                          <?php
                                          if($nbclasse>0)
                                          {
                                           ?>
	                                        <li class="nav-item">
	                                            <a href="#" class="nav-link">
	                                                <i class="fa fa-file-pdf-o"></i> Sample Link 1</a>
	                                        </li>
	                                      <?php
                                      }else {
                                        ?>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                <i class=""></i>Aucune classe</a>
                                        </li>
                                        <?php
                                      }
                                         ?>

	                                    </ul>
	                                </li>


	                            </ul>
	                        </li>
                          <li class="nav-item">
	                            <a href="teatchers.php" class="nav-link nav-toggle">
	                                <i class="material-icons">dashboard</i>
	                                <span class="title">Support de cours</span>
	                                <span class="selected"></span>

	                            </a>

	                        </li>
                          <li class="nav-item">
	                            <a href="teatchers.php" class="nav-link nav-toggle">
	                                <i class="material-icons">dashboard</i>
	                                <span class="title">Programme academique</span>
	                                <span class="selected"></span>

	                            </a>

	                        </li>

	                        <li class="nav-item">
	                            <a href="#" class="nav-link nav-toggle">
	                                <i class="material-icons">home</i>
	                                <span class="title">Elèves</span>
	                                <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item  ">
	                                    <a href="schools.php" class="nav-link ">
	                                        <span class="title">Toutes les écoles</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item  ">
	                                    <a href="addschool.php" class="nav-link ">
	                                        <span class="title">Ajouter école</span>
	                                    </a>
	                                </li>

	                            </ul>
	                        </li>
	                        <li class="nav-item  ">
	                            <a href="#" class="nav-link nav-toggle"><i class="material-icons">assignment</i>
	                            <span class="title">Contrôle</span><span class="arrow"></span></a>
	                            <ul class="sub-menu">
	                                <li class="nav-item  ">
	                                    <a href="#" class="nav-link "> <span class="title">Gestion des Notes</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item  ">
	                                    <a href="#" class="nav-link "> <span class="title">Sujet de composition</span>
	                                    </a>
	                                </li>



	                            </ul>
	                        </li>
                          <li class="nav-item">
	                            <a href="teatchers.php" class="nav-link nav-toggle">
	                                <i class="material-icons">dashboard</i>
	                                <span class="title">Tableau d'information</span>
	                                <span class="selected"></span>

	                            </a>

	                        </li>
                          <li class="nav-item">
	                            <a href="teatchers.php" class="nav-link nav-toggle">
	                                <i class="material-icons">dashboard</i>
	                                <span class="title">Message</span>
	                                <span class="selected"></span>

	                            </a>

	                        </li>














                          <!--li class="nav-item ">
	                            <a href="exams.php" class="nav-link nav-toggle">
	                                <i class="material-icons">hotel</i>
	                                <span class="title">Examens de contrôles</span>
	                                <span class="selected"></span>

	                            </a>

	                        </li-->
                          <!--li class="nav-item  ">
	                            <a href="#" class="nav-link nav-toggle"> <i class="material-icons">hotel</i>
	                                <span class="title">Examens de contrôles</span> <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item  ">
	                                    <a href="#" class="nav-link "> <span class="title">Toutes les Examens de contrôles</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item  ">
	                                    <a href="#" class="nav-link "> <span class="title">Ajouter Examen de contrôle</span>
	                                    </a>
	                                </li>

	                            </ul>
	                        </li-->

                          <!--li class="nav-item  ">
	                            <a href="#" class="nav-link nav-toggle"> <i class="material-icons">hotel</i>
	                                <span class="title">Absences</span> <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item  ">
	                                    <a href="#" class="nav-link "> <span class="title">Toutes les Absences</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item  ">
	                                    <a href="#" class="nav-link "> <span class="title">Ajouter Absence</span>
	                                    </a>
	                                </li>

	                            </ul>
	                        </li-->

                          <!--li class="nav-item ">
                              <a href="attendances.php" class="nav-link nav-toggle">
                                  <i class="material-icons">hotel</i>
                                  <span class="title">Présences</span>
                                  <span class="selected"></span>

                              </a>

                          </li-->

                          <!--li class="nav-item  ">
	                            <a href="#" class="nav-link nav-toggle"> <i class="material-icons">hotel</i>
	                                <span class="title">Emploi du temps</span> <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item  ">
	                                    <a href="#" class="nav-link "> <span class="title">Toutes les Emplois du temps</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item  ">
	                                    <a href="#" class="nav-link "> <span class="title">Ajouter Emploi du temps</span>
	                                    </a>
	                                </li>

	                            </ul>
	                        </li-->

                          <!--li class="nav-item ">
                              <a href="times.php" class="nav-link nav-toggle">
                                  <i class="material-icons">hotel</i>
                                  <span class="title">Emploi du temps</span>
                                  <span class="selected"></span>

                              </a>

                          </li-->



                </div>
            </div>
