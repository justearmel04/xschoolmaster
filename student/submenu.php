<div class="page-container">
    <!-- start sidebar menu -->
<div class="sidebar-container">
<div class="sidemenu-container navbar-collapse collapse fixed-menu">
          <div id="remove-scroll" class="left-sidemenu">
            
              <ul class="sidemenu  page-header-fixed slimscroll-style" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                  <li class="sidebar-toggler-wrapper hide">
                      <div class="sidebar-toggler">
                          <span></span>
                      </div>
                  </li>
                  <li class="sidebar-user-panel">
                      <div class="user-panel">
                          <div class="pull-left image">
                              <img src="<?php echo $lienphoto?>" class="img-circle user-img-circle" alt="User Image" />
                          </div>
                          <div class="pull-left info">
                              <p> <?php echo $_SESSION['user']['nom']." ".$_SESSION['user']['prenoms']; ?></p>
                              <small>
                                <?php
                                if($tablogin[1]=="Admin_globale")
                                {
                                  echo "Administrateur";
                                }else if($tablogin[1]=="Admin_locale")
                                {
                                  echo "Admin Locale";
                                }else if($tablogin[1]=="Teacher")
                                {
                                  echo "Enseignant";
                                }else if($tablogin[1]=="Parent")
                                {
                                  echo "Parent";
                                }
                                 ?>
                              </small>
                          </div>
                      </div>
                  </li>
                  <li class="nav-item">
                      <a href="#" class="nav-link nav-toggle">
                          <i class="material-icons">dashboard</i>
                          <span class="title">Dashboard</span>

                      </a>

                  </li>
                  <li class="nav-item">
                      <a href="#" class="nav-link nav-toggle">
                          <i class="material-icons">email</i>
                          <span class="title">Elèves</span>
                          <span class="arrow"></span>
                      </a>
                      <ul class="sub-menu">
                          <li class="nav-item  ">
                              <a href="#" class="nav-link ">
                                  <span class="title"><?php echo L::StudentNews ?></span>
                              </a>
                          </li>
                          <li class="nav-item  ">
                              <a href="#" class="nav-link ">
                                  <span class="title"><?php echo L::FichestudMenu ?></span>
                              </a>
                          </li>
                          <li class="nav-item  ">
                              <a href="#" class="nav-link ">
                                  <span class="title"><?php echo L::ClassesMenu ?></span>
                              </a>
                          </li>
                      </ul>
                  </li>
                  <li class="nav-item  ">
                      <a href="#" class="nav-link nav-toggle"><i class="material-icons">dashboard</i>
                      <span class="title">Enseignements</span><span class="arrow"></span></a>
                      <ul class="sub-menu">
                          <li class="nav-item  ">
                              <a href="#" class="nav-link "> <span class="title">Année scolaire</span>
                              </a>
                          </li>
                          <li class="nav-item  ">
                              <a href="#" class="nav-link "> <span class="title">Syllabus</span>
                              </a>
                          </li>
                          <li class="nav-item  ">
                              <a href="#" class="nav-link "> <span class="title">Emplois du temps</span>
                              </a>
                          </li>
                           <li class="nav-item  ">
                              <a href="#" class="nav-link "> <span class="title"><?php echo L::CourseMenu  ?></span>
                              </a>
                          </li>
                          <li class="nav-item  ">
                              <a href="#" class="nav-link "> <span class="title"><?php echo L::CahierMenu  ?></span>
                              </a>
                          </li>
                      </ul>
                  </li>
                  <li class="nav-item  ">
                      <a href="#" class="nav-link nav-toggle"> <i class="material-icons">person</i>
                          <span class="title">Absences</span> <span class="arrow"></span>
                      </a>
                      <ul class="sub-menu">
                          <li class="nav-item  ">
                              <a href="#" class="nav-link "> <span class="title"><?php echo L::Reacpsdesabsences ?></span>
                              </a>
                          </li>
                          <li class="nav-item  ">
                              <a href="#" class="nav-link "> <span class="title"><?php echo L::RecapCourbeMenu ?></span>
                              </a>
                          </li>

                      </ul>
                  </li>
                  <li class="nav-item  ">
                      <a href="#" class="nav-link nav-toggle"> <i class="material-icons">group</i>
                          <span class="title">Evaluations et notes</span> <span class="arrow"></span>
                      </a>
                      <ul class="sub-menu">
                          <li class="nav-item  ">
                              <a href="#" class="nav-link "> <span class="title"><?php echo L::Evaluations ?></span>
                              </a>
                          </li>
                          <li class="nav-item  ">
                              <a href="#" class="nav-link "> <span class="title">Notes</span>
                              </a>
                          </li>
                          <li class="nav-item  ">
                              <a href="#" class="nav-link "> <span class="title"><?php echo L::QuizLib ?></span>
                              </a>
                          </li>
                          <li class="nav-item  ">
                              <a href="#" class="nav-link "> <span class="title"><?php echo L::DevoirsMenu ?></span>
                              </a>
                          </li>
                          <li class="nav-item  ">
                              <a href="#" class="nav-link "> <span class="title"><?php echo L::CurvesNotes ?></span>
                              </a>
                          </li>
                      </ul>
                  </li>
                  <li class="nav-item  ">
                      <a href="#" class="nav-link nav-toggle"> <i class="material-icons">accessible</i>
                          <span class="title">Activités parascolaires</span>
                      </a>

                  </li>
                  <li class="nav-item  ">
                      <a href="#" class="nav-link nav-toggle"> <i class="material-icons">hotel</i>
                          <span class="title">Notifications</span>
                      </a>

                  </li>
                  <li class="nav-item  ">
                      <a href="#" class="nav-link nav-toggle"> <i class="material-icons">monetization_on</i>
                          <span class="title">Administrations</span> <span class="arrow"></span>
                      </a>
                      <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="#" class="nav-link "> <span class="title"><?php echo L::SallesMenu ?></span>
                            </a>
                        </li>
                          <li class="nav-item  ">
                              <a href="#" class="nav-link "> <span class="title"><?php echo L::ClassesMenu ?></span>
                              </a>
                          </li>
                          <li class="nav-item  ">
                              <a href="#" class="nav-link "> <span class="title"><?php echo L::MatiereMenu ?></span>
                              </a>
                          </li>
                          <li class="nav-item  ">
                              <a href="#" class="nav-link "> <span class="title"><?php echo L::UsersMenu ?></span>
                              </a>
                          </li>
                          <li class="nav-item  ">
                             <a href="#" class="nav-link "> <span class="title"><?php echo L::Importations ?></span>
                             </a>
                         </li>
                      </ul>
                  </li>

              </ul>
          </div>
        </div>
    </div>
</div>
