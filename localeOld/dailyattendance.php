<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

  <!-- data tables -->
   <!-- <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/> -->
   <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
     <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>


	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Consigner les absences :</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>Home&nbsp;<i class="fa fa-angle-right"></i><a class="parent-item" href="#">Absences</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Consigner les absences</li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">
              <?php

                    if(isset($_SESSION['user']['addattendailyok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
            <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['addattendailyok']; ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addattendailyok']);
                    }

                     ?>
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

<br/>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="card card-topline-green">
                   <div class="card-head">
                       <header></header>
                       <div class="tools">
                           <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                       </div>
                   </div>
                   <div class="card-body ">
                     <form method="post" id="FormSearch" action="dailyattendance.php">
                         <div class="row">
                           <div class="col-md-4 col-sm-4">
                           <!-- text input -->
                           <div class="form-group">
                               <label style="margin-top:3px;">Date</label>
                               <input type="text" id="datepre" name ="datepre" class="floating-label mdl-textfield__input"  placeholder="Date présence">
                               <input type="hidden" name="search" id="search" />
                           </div>


                       </div>

                           <div class="col-md-4 col-sm-4">
                           <!-- text input -->
                           <div class="form-group" style="margin-top:8px;">
                               <label>Classe</label>
                               <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                               <select class="form-control input-height" id="classeEtab" name="classeEtab" style="width:100%;" onchange="searchingMatiere('<?php echo $codeEtabAssigner; ?>')">
                                   <option value=""><?php echo L::Selectclasses ?></option>
                                   <?php
                                   $i=1;
                                     foreach ($classes as $value):
                                     ?>
                                     <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                     <?php
                                                                      $i++;
                                                                      endforeach;
                                                                      ?>

                               </select>
                           </div>


                       </div>
                       <div class="col-md-4 col-sm-4">
                       <!-- text input -->
                       <div class="form-group" style="margin-top:8px;">
                           <label><?php echo L::MatiereMenusingle ?></label>
                           <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                           <select class="form-control input-height" id="matiereEtab" name="matiereEtab" style="width:100%;" onchange="selectedHours()">
                               <option value=""><?php echo L::SelectSubjects ?></option>
                            </select>
                       </div>


                   </div>
                   <div class="col-md-4 col-sm-4">
                   <!-- text input -->
                   <div class="form-group" style="margin-top:8px;">
                       <label>Heure de cours</label>
                       <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                       <select class="form-control input-height" id="HeureLibEtab" name="HeureLibEtab" style="width:100%;" onchange="Addhiddencaps()">
                           <option value="">Selectionner une Heure de cours</option>
                        </select>
                        <input type="hidden" name="datedebhours" id="datedebhours" value="">
                        <input type="hidden" name="datefinhours" id="datefinhours" value="">
                   </div>


               </div>

                   <?php
                   if($nbsessionOn>0)
                   {
                     ?>
                     <button type="submit" class="btn btn-success" onclick="affichage()" style="width:150px;height:35px;margin-top:30px;">Consigner Absence</button>
                     <?php
                   }else if($nbsessionOn==0)
                   {
                     ?>
<button type="submit" class="btn btn-success" onclick="affichage()" style="width:150px;height:35px;margin-top:30px;" disabled >Consigner Absence</button>
                     <?php
                   }
                    ?>


                         </div>


                     </form>
                   </div>
               </div>
                        </div>

          </div>
<!-- element à faire apparaitre au clique du bouton rechercher -->
<div class="row" style="" id="affichage">
  <?php
      if(isset($_POST['search']))
      {
          if(isset($_POST['classeEtab'])&&isset($_POST['datepre'])&&isset($_POST['matiereEtab'])&&isset($_POST['HeureLibEtab'])&&isset($_POST['datedebhours'])&&isset($_POST['datefinhours']))
          {
              //nous devons recupérer la liste des elèves de cette classe

              //$students=$student->getAllstudentofthisclasses($_POST['classeEtab']);

              $students=$student->getAllstudentofthisclassesSession($_POST['classeEtab'],$libellesessionencours);

              $classeInfos=$classe->getInfosofclassesbyId($_POST['classeEtab'],$libellesessionencours);

              ///var_dump($students);
              $matierepost=explode('-',$_POST['matiereEtab']);

          }
          ?>

          <div class="offset-md-4 col-md-4"  id="affichage1">
            <div class="card" style="">
            <div class="card-body">
              <h5 class="card-title"></h5>
              <h4 style="font-style: italic;font-weight: bold;text-align:center;font-size:font-size: large;">Présence Journalière de la Classe</h4>
              <p class="card-text" style="text-align:center;font-weight: bold;"><?php echo $classeInfos; ?></p>
              <p class="card-text" style="text-align:center;"><?php echo $_POST['datepre']?></p>

            </div>
          </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="affichage2">
                                  <div class="card card-topline-green">
                                      <div class="card-head">
                                          <header>Statut des présences</header>
                                          <div class="tools">
                                              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                      <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                      <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                          </div>
                                      </div>
                                      <div class="card-body ">
                                        <div class="pull-right"><a class="btn btn-primary " onclick="touspresent(<?php echo $_POST['classeEtab'];?>)" >Marquer tous Présents</a> <a class="btn btn-primary " onclick="tousabsent(<?php echo $_POST['classeEtab'];?>)">Marquer tous Absents</a></div>
                                        <form method="post" action="../controller/attendance.php" id="FormAttendance">
                                          <br>
                                  <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example3"  >
                                      <thead>
                                          <tr>
                                              <th style="width:5%">
                                                  #
                                              </th>
                                              <th style="width:15%"> Matricule </th>
                                              <th style="width:35%"> Nom & Prénoms </th>
                                              <th> Statut </th>

                                          </tr>
                                      </thead>
                                      <tbody>
                                        <?php
                                        $matricules="";
                                        $j=0;
                                        $i=1;

                                        foreach ($students as $value):

                                          $matricules=$matricules."*".$value->matricule_eleve;
                                        ?>
                                          <tr class="odd gradeX">
                                              <td>
                                                <?php echo $i;?>
                                              </td>
                                              <td> <?php echo $value->matricule_eleve;?></td>
                                              <td>
                                                  <a href="#"><?php echo $value->nom_eleve." ".$value->prenom_eleve;?> </a>
                                              </td>
                                              <td>
                                                <label class="radio-inline">
                                          <input type="radio" value="1" name="optradio<?php echo $value->matricule_eleve;?>" id="P<?php echo $value->matricule_eleve;?>" onclick="present('<?php echo $value->matricule_eleve;?>')" checked> présent
                                        </label>&nbsp;
                                        <label class="radio-inline">
                                          <input type="radio" value="0" onclick="absent('<?php echo $value->matricule_eleve;?>')" id="A<?php echo $value->matricule_eleve;?>" name="optradio<?php echo $value->matricule_eleve;?>"> absent
                                        </label>&nbsp;
                                        <input type="hidden" name="statut<?php echo $value->matricule_eleve;?>" id="statut<?php echo $value->matricule_eleve;?>" value="1"/>

                                              </td>

                                          </tr>

                                          <?php
                                             $i++;
                                             $j++;
                                                 endforeach;
                                               ?>



                                      </tbody>
                                  </table>

                                  <?php
                                  //echo $matricules;
                                  $tabMat=explode("*",$matricules);
                                  $nb=count($tabMat);



                                  ?>
                                  <input type="hidden" name="matiereid" id="matiereid" value="<?php echo $matierepost[0]; ?>">
                                  <input type="hidden" name="profid" id="profid" value="<?php echo $matierepost[1]; ?>">
                                  <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner; ?>">
                                  <input type="hidden" name="studentmat" id="studentmat" value="<?php echo $matricules;?>"/>
                                  <input type="hidden" name="etape" id="etape" value="1"/>
                                  <input type="hidden" name="nbstudent" id="nbstudent" value="<?php echo $nb;?>"/>
                                  <input type="hidden" name="allpresent" id="allpresent" value=""/>
                                  <input type="hidden" name="classeId" id="classeId" value="<?php echo $_POST['classeEtab']?>"/>
                                  <input type="hidden" name="datePresence" id="datePresence" value="<?php echo $_POST['datepre']?>"/>
                                  <input type="hidden" name="LibelleHeurePresence" id="LibelleHeurePresence" value="<?php echo $_POST['HeureLibEtab']?>"/>
                                  <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                  <center><button type="submit"  class="btn btn-success"><i class="fa fa-check-circle"></i>Validation Statuts</button></center>
                                </form>

                              </div>
                                  </div>
                              </div>


          <?php
      }
   ?>



          </div>

<!-- fin affichage du bouton rechercher -->

                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
      <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
<?php
  if(isset($_POST))
  {
    ?>
    function touspresent(idclasse)
    {

       var nb="<?php echo @$nb;?>";
       var matricules="<?php echo @$matricules?>";

       var array=matricules.split('*');

       for(i=1;i<nb;i++)
       {
         //alert(array[i]);
         $("statut"+array[i]).val(1);
         //$("P"+array[i]).prop("checked", true);
         document.getElementById("P"+array[i]).checked = true;

       }
       $("#allpresent").val(1);

    }

    function tousabsent(idclasse)
    {
      var nb="<?php echo @$nb;?>";
      var matricules="<?php echo @$matricules?>";

      var array=matricules.split('*');

      for(i=1;i<nb;i++)
      {
        $("statut"+array[i]).val(0);
        document.getElementById("A"+array[i]).checked = true;

      }
      $("#allpresent").val(0);
    }
<?php
  }
 ?>

 function selectedHours()
 {
   var classe=$("#classeEtab").val();
   var codeEtab="<?php echo $codeEtabAssigner; ?>";
   var session="<?php echo $libellesessionencours; ?>";
   var matiere=$("#matiereEtab").val();
   var datechoice=$("#datepre").val();
   var etape=10;

   if(datechoice=="")
   {
     Swal.fire({
     type: 'warning',
     title: '<?php echo L::WarningLib ?>',
     text: "La présence de cette classe existe dejà dans le système pour cette date",

   })
   }else {
     $.ajax({
       url: '../ajax/hours.php',
       type: 'POST',
       async:false,
       data: 'classe=' +classe+'&code='+codeEtab+'&etape='+etape+'&session='+session+'&matiere='+matiere+'&datechoice='+datechoice,
       dataType: 'text',
       success: function (content, statut)
       {
         $("#HeureLibEtab").html("");
          $("#HeureLibEtab").html(content);

       }
     });
   }



 }

 function Addhiddencaps()
 {
   var libelleheure=$("#HeureLibEtab").val();
   var codeEtab="<?php echo $codeEtabAssigner; ?>";
   var session="<?php echo $libellesessionencours; ?>";
   var datechoice=$("#datepre").val();
   var nouveau="";
   var etape=7;

   if(datechoice=="")
   {
     Swal.fire({
     type: 'warning',
     title: '<?php echo L::WarningLib ?>',
     text: "La présence de cette classe existe dejà dans le système pour cette date",

   })
   }else {
     $.ajax({
       url: '../ajax/hours.php',
       type: 'POST',
       async:true,
       data: 'libelleheure='+libelleheure+'&etape='+etape+'&nouveau='+nouveau+'&codeEtab='+codeEtab+'&session='+session+'&datechoice='+datechoice,
       dataType: 'text',
       success: function (content, statut) {
         var heuredeb=content.split("*")[0];
         var heurefin=content.split("*")[1];

         $("#datedebhours").val(heuredeb);
         $("#datefinhours").val(heurefin);

       }
     });
   }



 }

 function searchingMatiere(codeEtab)
 {
   var classe=$("#classeEtab").val();
   var datechoice=$("#datepre").val();
   var etape=14;

   if(datechoice=="")
   {
     Swal.fire({
     type: 'warning',
     title: '<?php echo L::WarningLib ?>',
     text: "Merci de renseigner la date de présences",

   })
   }else {
     $.ajax({
       url: '../ajax/matiere.php',
       type: 'POST',
       async:false,
       data: 'classe=' +classe+'&code='+codeEtab+'&etape='+etape+'&datechoice='+datechoice,
       dataType: 'text',
       success: function (content, statut)
       {
         $("#matiereEtab").html("");
          $("#matiereEtab").html(content);

       }
     });
   }



 }


   function present(id)
   {
    $("statut"+id).val(1);
   }

   function absent(id)
   {
    $("statut"+id).val(0);
   }

   $('#example5').DataTable( {
       "scrollX": true

   } );
   $("#classeEtab").select2();
  $("#matiereEtab").select2();
  $("#HeureLibEtab").select2();
   $('#datepre').bootstrapMaterialDatePicker
   ({
     date: true,
     time: false,
     format: 'DD/MM/YYYY',
     lang: 'fr',
     // minDate : new Date(),
    cancelText: '<?php echo L::AnnulerBtn ?>',
    okText: 'OK',
    clearText: '<?php echo L::Eraser ?>',
    nowText: '<?php echo L::Now ?>'

   });
   $(document).ready(function() {

//
$("#FormSearch").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
 },
 highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
 },
 success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    classeEtab:"required",
    datepre:"required",
    matiereEtab:"required",
    HeureLibEtab:"required"



  },
  messages: {
    classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
    datepre:"Merci de choisir la date de présence",
    matiereEtab:"Merci de <?php echo L::SelectSubjects ?>",
    HeureLibEtab:"Merci de selectionner un Libelle heure"

  },
  submitHandler: function(form) {
    //form.submit();
    //nous allons verifier si nous n'avons pas deja fait les présences en ce jour et pour cette heure de cours

    var datepre=$("#datepre").val();
    var matiereEtab=$("#matiereEtab").val();
    var HeureLibEtab=$("#HeureLibEtab").val();
    var classeEtab=$("#classeEtab").val();
    var codeEtab="<?php echo $codeEtabAssigner; ?>";
    var session="<?php echo $libellesessionencours; ?>";
    var etape=11;

    $.ajax({
      url: '../ajax/hours.php',
      type: 'POST',
      async:false,
      data: 'classe=' +classeEtab+'&codeEtab='+codeEtab+'&etape='+etape+'&matiereEtab='+matiereEtab+'&HeureLibEtab='+HeureLibEtab+'&datepre='+datepre+'&session='+session,
      dataType: 'text',
      success: function (content, statut)
      {

        if(content==0)
        {
          form.submit();
        }else {
          Swal.fire({
          type: 'warning',
          title: '<?php echo L::WarningLib ?>',
          text: "La présence pour cette heure de cours existe dejà dans le système",

        })
        }

        // $("#matiereEtab").html("");
        //  $("#matiereEtab").html(content);

      }
    });
  }
});


$("#FormAttendance").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
  },
  highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
  },
  success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    classeEtab:"required",
    datepre:"required"



  },
  messages: {
    classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
    datepre:"Merci de choisir la date de présence"

  },
  submitHandler: function(form) {
    //form.submit();
    //classeId
//datePresence
    var etape=1;

form.submit();

    // $.ajax({
    //
    //   url: '../ajax/attendance.php',
    //   type: 'POST',
    //   async:false,
    //   data: 'classe=' + $("#classeId").val()+'&datepre='+$("#datePresence").val()+'&etape='+etape,
    //   dataType: 'text',
    //   success: function (content, statut)
    //   {
    //       if(content==0)
    //       {
    //         form.submit();
    //
    //       }else if(content==1) {
    //         Swal.fire({
    //         type: 'warning',
    //         title: '<?php echo L::WarningLib ?>',
    //         text: "La présence de cette classe existe dejà dans le système pour cette date",
    //
    //       })
    //     }
    //   }
    //
    // });
  }


});


   });

   </script>
    <!-- end js include path -->
  </body>

</html>
