<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Teatcher.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Student.php');

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$userId=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$student=new Student();
$matiere=new Matiere();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement
$schoolsofassign=$etabs->getEtablissementbyCodeEtab($codeEtabAssigner);

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$codesEtab=$schoolsofassign;
$codeEtabLocal=$etabs->getcodeEtabByLocalId($userId);
$classes=$classe->getAllClassesbyschoolCode($codeEtabLocal);
$teatchers=$teatcher->getAllTeatchersBySchoolCode($codeEtabLocal);
$matieres=$matiere->getAllMatiereOfThisSchool($codeEtabLocal);

//recuperer tous les cntrole de cet etablissement



$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabLocal);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabLocal);


if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabLocal);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabLocal,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabLocal,$libellesessionencours);
}

$controles=$matiere->getAllControleMatiereOfThisSchool($codeEtabLocal,$libellesessionencours);

$scolarites=$etabs->DetermineAllversements($codeEtabLocal,$libellesessionencours);


// session_start();
// require_once('../class/User.php');
// require_once('../class/Parent.php');
// require_once('../class/Classe.php');
//
// require_once('../class/Teatcher.php');
// require_once('../class/Matiere.php');
// $etab=new Etab();
// $parent=new ParentX();
// $user=new User();
// $classe=new Classe();

// $emailUti=$_SESSION['user']['email'];
//
// $imageprofile=$user->getImageProfile($emailUti);
// $logindata=$user->getLoginProfile($emailUti);
// $tablogin=explode("*",$logindata);
// $datastat=$user->getStatis();
// $tabstat=explode("*",$datastat);
// $classeId=$_GET['classe'];
//
//
// if(strlen($imageprofile)>0)
// {
//   $lienphoto="../photo/".$emailUti."/".$imageprofile;
// }else {
//   $lienphoto="../photo/user5.jpg";
// }
//
// $parents=$parent->getAllParent();
//


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
        <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
  <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title">Comptabilités :</div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">

                          <li><a class="parent-item" href="#">Comptabilités</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active">Scolarités :</li>
                      </ol>
                  </div>
              </div>

              <?php

                    if(isset($_SESSION['user']['addctrleok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
              <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
              <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['addctrleok']; ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addctrleok']);
                    }

                     ?>




              <?php

                    if(isset($_SESSION['user']['deletesubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['deletesubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'OK',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })


                </script>
                      <?php
                      unset($_SESSION['user']['deletesubjectok']);
                    }

                     ?>


              <?php

                    if(isset($_SESSION['user']['updatesubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['updatesubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'OK',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })


                </script>
                      <?php
                      unset($_SESSION['user']['updatesubjectok']);
                    }

                     ?>
              <?php

                    if(isset($_SESSION['user']['addsubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>


                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['addsubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'OK',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })
                </script>
                      <?php
                      unset($_SESSION['user']['addsubjectok']);
                    }

                     ?>


                     <div class="col-md-12 col-sm-12">
                                   <div class="panel tab-border card-box">
                                       <header class="panel-heading panel-heading-gray custom-tab ">
                                           <ul class="nav nav-tabs">
                                               <li class="nav-item"><a href="#home" data-toggle="tab" class="active"><i class="fa fa-bars"></i> Liste des Vesements</a>
                                               </li>

                                               <li class="nav-item"><a href="#about" data-toggle="tab"><i class="fa fa-plus-circle"></i> Ajouter Un Versement</a>
                                               </li>



                                           </ul>
                                       </header>
                                       <div class="panel-body">
                                           <div class="tab-content">
                                               <div class="tab-pane active" id="home">
                                                 <div class="row">
                        <div class="col-md-12">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header></header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">

                                    <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                        <thead>
                                            <tr>

                                                <th style="width:150px;"> Versements </th>
                                                <th> Date versement</th>
                                                <th> Montant versement </th>
                                                <th>Beneficiaire</th>
                                                  <th>Actions</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                            //$matieres

                                            $i=1;
                                              foreach ($scolarites as $value):
                                           ?>
                                            <tr class="odd gradeX">

                                                <td> <?php echo $value->code_versement;?></td>
                                                <td>
                                                    <?php echo date_format(date_create($value->date_versement),"d/m/Y");?>
                                                </td>
                                                <td>
                                                  <?php echo $value->montant_versement." ".$value->devise_versement;?>
                                                </td>
                                                <td>
                                                  <?php
                                                  $donnees=$student->getEmailParentOfThisStudentByID($value->ideleve_versement,$value->session_versement);

                                                  $tabdata=explode('*',$donnees);
                                                  $emailparent=$tabdata[0];
                                                  echo $nomEleve=$tabdata[1]." ".$tabdata[2];
                                                  $classeName=$tabdata[3];
                                                  $EtabName=$tabdata[4];
                                                  $imageEtab=$tabdata[5];
                                                  ?>
                                                </td>
                                                <td>
                                                  <a href="#" class="btn btn-warning  btn-md " style="border-radius:3px;" onclick="generatefichevers(<?php echo $value->ideleve_versement;?>,'<?php echo $value->session_versement;?>','<?php echo $value->codeEtab_versement; ?>','<?php echo $value->id_versement ?>')">  <i class="fa fa-print"></i></a>
                                                </td>

                                                <?php
                                                /*
                                                 ?>
                                                <td class="valigntop">
                                                  <a href="#"  data-toggle="modal" data-target="#exampleModal<?php echo $value->id_mat?>"class="btn btn-info  btn-md " style="border-radius:3px;">
                                                    <i class="fa fa-pencil"></i>
                                                  </a>
                                                  <!--a href="#"  onclick="modify(<?php //echo $value->id_mat;?>)" class="btn btn-info  btn-md " style="border-radius:3px;">
                                                    <i class="fa fa-pencil"></i>
                                                  </a-->
                                                  <a href="#"  onclick="deleted(<?php echo $value->id_mat;?>)" class="btn btn-danger  btn-md " style="border-radius:3px;">
                                                    <i class="fa fa-trash-o"></i>
                                                  </a>
                                                </td>
                                                <?php
                                                */
                                                 ?>
                                            </tr>
                                            <?php
                                            $i++;
                                            endforeach;
                                            ?>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                                               </div>
                                               <div class="tab-pane" id="about">
                                                 <?php
                                                 if($nbsessionOn>0)
                                                 {
                                                  ?>
                                                  <div class="row">
                                                      <div class="col-md-12 col-sm-12">
                                                          <div class="card card-box">
                                                              <div class="card-head">
                                                                  <header></header>

                                                              </div>

                                                              <div class="card-body" id="bar-parent">
                                                                  <form  id="FormAddCtrl" class="form-horizontal" action="../controller/scolarites.php" method="post">
                                                                      <div class="form-body">
                                                                        <div class="form-group row">
                                                                                <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>s
                                                                                    <span class="required"> * </span>
                                                                                </label>
                                                                                <div class="col-md-5">
                                                                                    <select class="form-control input-height" name="classe" id="classe"  style="width:100%" onchange="searchstudent()">
                                                                                        <option value="" selected >Selectionner une classe</option>
                                                                                        <?php
                                                                                        $i=1;
                                                                                          foreach ($classes as $value):
                                                                                          ?>
                                                                                          <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                                                          <?php
                                                                                                                           $i++;
                                                                                                                           endforeach;
                                                                                                                           ?>

                                                                                    </select>
                                                                            </div>
                                                                          </div>
                                                                        <div class="form-group row">
                                                                                <label class="control-label col-md-3">Elèves
                                                                                    <span class="required"> * </span>
                                                                                </label>
                                                                                <div class="col-md-5">
                                                                                    <!--input type="text" name="matiere" id="matiere" data-required="1" placeholder="Entrer la classe" class="form-control input-height" /-->
                                                                                    <select class="form-control input-height" name="student" id="student"  style="width:100%" onchange='dueamount()'>
                                                                                        <option value="">Selectionner un Elève</option>

                                                                                    </select>
                                                                                    <input type="hidden" name="etape" id="etape" value="1"/>
                                                                                    <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabLocal; ?>"/>
                                                                                      <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                                                                  </div>

                                                                         </div>


                                                                         <div class="form-group row">
                                                                                 <label class="control-label col-md-3">Mode de paiement
                                                                                     <span class="required"> * </span>
                                                                                 </label>
                                                                                 <div class="col-md-5">
                                                                                   <select class="form-control input-height" name="modepaie" id="modepaie"  style="width:100%">
                                                                                       <option value="" selected>Selectionner un mode de paiement</option>
                                                                                       <option value="1">Chèque</option>
                                                                                       <option value="2">Espèce</option>

                                                                                   </select>

                                                                                   </div>

                                                                          </div>
                                                                          <div class="form-group row">
                                                                                  <label class="control-label col-md-3">Montant à payer
                                                                                      <span class="required"> * </span>
                                                                                  </label>
                                                                                  <div class="col-md-5">
                                                                                      <input type="number" min="1" name="montapayer" id="montapayer" data-required="1" placeholder="" class="form-control input-height" readonly /> </div>
                                                                           </div>
                                                                         <div class="form-group row">
                                                                                 <label class="control-label col-md-3">Montant versement
                                                                                     <span class="required"> * </span>
                                                                                 </label>
                                                                                 <div class="col-md-5">
                                                                                     <input type="number" min="1" name="montvers" id="montvers" data-required="1" placeholder="Entrer le montant du versement" class="form-control input-height" onkeyup="recalculrest()" /> </div>
                                                                          </div>
                                                                          <div class="form-group row">
                                                                                  <label class="control-label col-md-3">Reste à payer
                                                                                      <span class="required"> * </span>
                                                                                  </label>
                                                                                  <div class="col-md-5">
                                                                                      <input type="number" min="0" name="montrest" id="montrest" data-required="1" placeholder="" class="form-control input-height" readonly /> </div>
                                                                           </div>
                                                                           <div class="form-group row">
                                                                                   <label class="control-label col-md-3">Devise
                                                                                       <span class="required"> * </span>
                                                                                   </label>
                                                                                   <div class="col-md-5">
                                                                                       <input type="text"  name="devisepaie" id="devisepaie" data-required="1" placeholder="" class="form-control input-height" readonly /> </div>
                                                                            </div>







                                                    <div class="form-actions">
                                                                          <div class="row">
                                                                              <div class="offset-md-3 col-md-9">

                                                                                  <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                                                                  <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                              </div>
                                                                            </div>
                                                                         </div>
                                                  </div>
                                                                  </form>
                                                              </div>
                                                          </div>
                                                      </div>

                                                  </div>


                                                  <?php
                                                }
                                                  ?>



                                                </div>
                                                <div class="tab-pane" id="about1">
                                                  <div class="offset-md-1 col-md-10 col-sm-10">
                                   <div class="card card-box">
                                     <?php
                                     if($nbsessionOn>0)
                                     {
                                      ?>

                                       <div class="card-body " id="bar-parent">
                                           <form method="post" action="../controller/messages.php" id="FormAddmessage" enctype="multipart/form-data" >
                                               <div class="form-group">
                                                   <label for="titre">Titre :</label>
                                                   <input type="text" class="form-control col-md-8" id="titre" name="titre" placeholder="Renseigner le titre du message">
                                               </div>
                                               <div class="form-group">
                                                   <label for="message">Message</label>
                                                   <textarea class="form-control col-md-8" name="message" id="message" rows="8" cols="80" placeholder="Renseigner le Message"></textarea>

                                               </div>
                                               <div class="form-group">
                                                   <label for="simpleFormPassword">Fichier joint</label>
                                                   <input type="file" id="joinfile" name="joinfile" class="joinfile"  data-show-loader="true" data-max-file-size="1mb" data-default-file="../assets2/img/user/user5.jpg" data-allowed-file-extensions="gif png jpg jpeg pjpeg pdf docx xlsx xls doc" />

                                               </div>
                                               <div class="form-group row">
                                                   <label class="control-label col-md-4">Classe(s) :
                                                       <span class="required">  </span>
                                                   </label>
                                                   <div class="col-md-8">
                                                     <select class="form-control input-height" multiple="multiple" id="classeEtab" name="classeEtab[]" style="width:100%">
                                                         <option value="">Selectionner le(s) classe(s)</option>
                                                         <?php
                                                         $i=1;
                                                           foreach ($classes as $value):
                                                           ?>
                                                           <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                                           <?php
                                                                                            $i++;
                                                                                            endforeach;
                                                                                            ?>

                                                     </select>
                                                     </div>
                                               </div>

                                               <div class="form-group row">
                                                   <label class="control-label col-md-4">Destinataire(s) :
                                                       <span class="required">  </span>
                                                   </label>
                                                   <div class="col-md-8">
                                                     <select class="form-control input-height" multiple="multiple" id="destinataires" name="destinataires[]" style="width:100%">
                                                         <option value="">Selectionner le(s) destinataire(s)</option>
                                                         <option value="Parent" selected >PARENTS</option>


                                                     </select>
                                                     <input type="hidden" name="etape" id="etape"  value="8">
                                                     <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner; ?>">
                                                     <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                                     </div>
                                               </div>
                                               <br>


                                               <div class="form-group">
                                                 <div class="custom-control custom-checkbox custom-control-inline">
             <input type="checkbox" class="custom-control-input" id="smssender" name="smssender" onclick='checksms()'>
             <input type="hidden" name="smsvalue" id="smsvalue"  value="0">
             <label class="custom-control-label" for="smssender">Notification SMS</label>

                                                 </div>
                                                 <div class="custom-control custom-checkbox custom-control-inline">
             <input type="checkbox" class="custom-control-input" id="emailsender" name="emailsender" onclick='checkmail()'>
             <input type="hidden" name="emailvalue" id="emailvalue"  value="0">
             <label class="custom-control-label" for="emailsender">Notification Email</label>

                                                 </div>


                                               </div>
                                               <button type="submit" class="btn btn-primary">Enregistrer</button>
                                           </form>
                                       </div>
                                       <?php
                                     }

                                       ?>


                                   </div>
                               </div>

                                                </div>

                                           </div>
                                       </div>
                                   </div>
                               </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->

  <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>

   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

 <script>

 function determinestudent()
 {
   var matricule=$("#matricule").val();
   var codeEtab=$("#etablissement").val();
   var etape=1;

   $.ajax({
     url: 'ajax/determination.php',
     type: 'POST',
     async:false,
     data: 'codeEtab='+codeEtab+'&etape='+etape+'&matricule='+matricule,
     dataType: 'text',
     success: function (response, statut) {



     }
   });
 }

function generatefichevers(ideleve,session,codeEtab,id_versement)
{
  var etape=2;
  $.ajax({
    url: '../ajax/etat.php',
    type: 'POST',
    async:false,
    data: 'codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&eleveid='+ideleve+'&versementid='+id_versement,
    dataType: 'text',
    success: function (response, statut) {

        window.open(response, '_blank');

    }
  });
}

 function recalculrest()
 {
   var montantapayer=$("#montapayer").val();
   var montantpayer=$("#montvers").val();
   var etape=5;
   if(montantapayer!="")
   {
   $.ajax({
     url: '../ajax/admission.php',
     type: 'POST',
     async:true,
     data: 'montantapayer=' + montantapayer+ '&etape=' + etape+'&montantpayer='+montantpayer,
     dataType: 'text',
     success: function (content, statut) {

       if(content>=0)
         {
           $("#montrest").val(content);
         }else {
           if(montantpayer!="")
           {
             Swal.fire({
             type: 'warning',
             title: '<?php echo L::WarningLib ?>',
             text: 'Le montant saisi est supérieur au montant à payer',

             })

             $("#montrest").val(montantapayer);
           }

           $("#montvers").val(0);
         }

     }
   });

 }else {
   $("#montvers").val(0);
 }

   // if(montantapayer!="")
   // {
   //   var montrest=montantapayer-montantpayer;
   //   if(montrest>0)
   //   {
   //     $("#montrest").val(montrest);
   //   }
   // }


 }

 function dueamount()
 {
   var codeEtab=$("#codeEtab").val();
   var session=$("#libellesession").val();
   var classe=$("#classe").val();
   var student=$("#student").val();
    var etape=4;

    $.ajax({
      url: '../ajax/admission.php',
      type: 'POST',
      async:true,
      data: 'codeEtab=' + codeEtab+ '&etape=' + etape+'&classe='+classe+'&session='+session+'&student='+student,
      dataType: 'text',
      success: function (content, statut) {

        $tabcontent=content.split("-");

         $("#montvers").val("");
       $("#montapayer").val($tabcontent[0]);
       $("#montrest").val($tabcontent[0]);
       $("#devisepaie").val($tabcontent[1]);

      }
    });

 }

 function searchstudent()
 {
   var codeEtab=$("#codeEtab").val();
   var session=$("#libellesession").val();
   var classe=$("#classe").val();
   var etape=3;

   $.ajax({
     url: '../ajax/admission.php',
     type: 'POST',
     async:true,
     data: 'codeEtab=' + codeEtab+ '&etape=' + etape+'&classe='+classe+'&session='+session,
     dataType: 'text',
     success: function (content, statut) {

       $("#student").html("");
       $("#student").html(content);

     }
   });

 }

 function searchmatiere()
 {
     var codeEtab="<?php echo $codeEtabLocal;?>";
     var classe=$("#classe").val();
     var etape=2;
      $.ajax({

        url: '../ajax/matiere.php',
        type: 'POST',
        async:true,
        data: 'code=' + codeEtab+ '&etape=' + etape+'&classe='+classe,
        dataType: 'text',
        success: function (content, statut) {

          $("#matiere").html("");
          $("#matiere").html(content);

        }
      });
 }

 function searchprofesseur()
 {
   var codeEtab="<?php echo $codeEtabLocal;?>";
   var classe=$("#classe").val();
   var matiere=$("#matiere").val();
   var etape=4;
   $.ajax({

     url: '../ajax/teatcher.php',
     type: 'POST',
     async:true,
     data: 'code=' + codeEtab+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
     dataType: 'text',
     success: function (content, statut) {

       // alert(content);
       $("#teatcher").html("");
       $("#teatcher").html(content);

     }
   });
 }

 function checksms()
 {


   if($('#smssender').prop('checked') == true){
    $("#smsvalue").val(1);
  }
  else {

    $("#smsvalue").val(0);
  }

 }

 function checkmail()
 {


   if($('#emailsender').prop('checked') == true){
    $("#emailvalue").val(1);
  }
  else {

    $("#emailvalue").val(0);
  }



 }


 $("#classeEtab").select2({
   tags: true,
 tokenSeparators: [',', ' ']
 });

 $("#destinataires").select2({
   tags: true,
 tokenSeparators: [',', ' ']
 });

 $('.joinfile').dropify({
     messages: {
         'default': 'Selectionner un fichier joint',
         'replace': 'Remplacer le fichier joint',
         'remove':  'Retirer',
         'error':   'Ooops, Une erreur est survenue.'
     }
 });

 jQuery(document).ready(function() {




$("#classe").select2();
$("#teatcher").select2();
$("#classeEtab").select2();
$("#matiere").select2();
$("#typesess").select2();
$("#student").select2();
$("#modepaie").select2();


   $("#FormAddCtrl").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

        // matiere:"required",
        // classe:"required",
        // teatcher:"required",
        // coef:"required",
        classe:"required",
        student:"required",
        modepaie:"required",

        montvers:{
          'required': {
              depends: function (element) {
                  return ($('#montapayer').val() !=''|| $('#montrest').val() !='' );

              }
          }
        },


      },
      messages: {
        // matiere:"Merci de renseigner la matière",
        // classe:"<?php echo L::PleaseSelectclasserequired ?>",
        // teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
        // coef:"Merci de renseigner le coefficient de la matière"
          classe:"<?php echo L::PleaseSelectclasserequired ?>",
          montvers:"Merci de renseigner le montant du versement",
          student:"Merci de selectionner un elève",
          modepaie:"Merci de selectionner le mode de paiement",


      },
      submitHandler: function(form) {


// nous allons verifier un controle similaire n'existe pas

  form.submit();


             }


           });
      });






 </script>
    <!-- end js include path -->
  </body>

</html>
