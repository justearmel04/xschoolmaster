<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Teatcher.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$userId=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$matiere=new Matiere();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement
$schoolsofassign=$etabs->getEtablissementbyCodeEtab($codeEtabAssigner);

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$codesEtab=$schoolsofassign;
$codeEtabLocal=$etabs->getcodeEtabByLocalId($userId);
$classes=$classe->getAllClassesbyschoolCode($codeEtabLocal);
$teatchers=$teatcher->getAllTeatchersBySchoolCode($codeEtabLocal);
$matieres=$matiere->getAllMatiereOfThisSchool($codeEtabLocal);

$examens=$etabs->getAllExamensOfThisSchool($codeEtabLocal);



$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabLocal);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabLocal);


if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabLocal);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];

  $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);

  // $parascos=$etabs->getAllParascolairesOfThisSchool($codeEtabLocal,$libellesessionencours);
  $parascos=$etabs->getAllParascolairesActivityOfThisSchool($codeEtabLocal,$libellesessionencours);
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabLocal,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabLocal,$libellesessionencours);
}

// var_dump($parascos);

// session_start();
// require_once('../class/User.php');
// require_once('../class/Parent.php');
// require_once('../class/Classe.php');
//
// require_once('../class/Teatcher.php');
// require_once('../class/Matiere.php');
// $etab=new Etab();
// $parent=new ParentX();
// $user=new User();
// $classe=new Classe();

// $emailUti=$_SESSION['user']['email'];
//
// $imageprofile=$user->getImageProfilebyId($_SESSION['user']['IdCompte'])
// $logindata=$user->getLoginProfile($emailUti);
// $tablogin=explode("*",$logindata);
// $datastat=$user->getStatis();
// $tabstat=explode("*",$datastat);
// $classeId=$_GET['classe'];
//
//
// if(strlen($imageprofile)>0)
// {
//   $lienphoto="../photo/".$emailUti."/".$imageprofile;
// }else {
//   $lienphoto="../photo/user5.jpg";
// }
//
// $parents=$parent->getAllParent();
//

 // var_dump($parascos);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
  <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::ParascolairesActivitiesLists ?> </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="#"><?php echo L::parascoMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::ParascolairesActivitiesLists ?></li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['updateteaok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: 'Félicitations',
            text: '<?php echo $_SESSION['user']['updateteaok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['updateteaok']);
                }

                 ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

                 <div class="row">
                   <div class="col-md-12 col-sm-12">
                                             <div class="card card-box">
                                                 <div class="card-head">
                                                     <header><?php echo L::Seacher ?></header>

                                                 </div>
                                                 <div class="card-body " id="bar-parent">
                                                   <form method="post" id="FormSearch">
                                                       <div class="row">
                                                         <div class="col-md-6 col-sm-6">
                                                         <!-- text input -->
                                                         <div class="form-group">
                                                             <label><?php echo L::ActivitiesDate ?></label>
                                                             <input type="text" id="dateactivite" name="dateactivite" class="form-control" placeholder="<?php echo L::ActivitiesDate ?>"/>

                                                         </div>
                                                         <div class="form-group">
                                                             <label><?php echo L::ActivitiesCosts ?></label>
                                                             <input type="text" id="coutactivite" name="coutactivite" class="form-control" placeholder="<?php echo L::ActivitiesCosts ?>"/>

                                                         </div>

                                                     </div>
                                                     <div class="col-md-6 col-sm-6">
                                                     <!-- text input -->
                                                     <div class="form-group">
                                                         <label><?php echo L::ActivitiesRespos ?></label>
                                                         <input type="text" id="respoactivite" name="respoactivite" class="form-control" placeholder="<?php echo L::ActivitiesRespos ?>"/>

                                                         <input type="hidden" name="search" id="search"/>
                                                     </div>


                                                 </div>
                                                       </div>

                                                       <button type="submit" class="btn btn-danger"><?php echo L::Seacher ?></button>
                                                   </form>
                                                 </div>
                                             </div>
                                         </div>
                 </div>

<div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line">
                               <!--ul class="nav nav-pills nav-pills-rose">
									<li class="nav-item tab-all"><a class="nav-link active show"
										href="#tab1" data-toggle="tab">Liste</a></li>
									<li class="nav-item tab-all"><a class="nav-link" href="#tab2"
										data-toggle="tab">Grille</a></li>
								</ul-->

<?php
  if(isset($_POST['search']))
  {
    if(isset($_POST['dateactivite']))
    {
      if(isset($_POST['coutactivite']))
      {
        if(isset($_POST['respoactivite']))
        {
            //dateactivite,coutactivite,responsableactivite
        }else
        {
          //dateactivite,coutactivite
        }
      }else {
        if(isset($_POST['respoactivite']))
        {
            //dateactivite,responsableactivite
        }else
        {
          //dateactivite
        }
      }
    }else {

      if(isset($_POST['coutactivite']))
      {
        if(isset($_POST['respoactivite']))
        {
            //coutactivite,responsableactivite
        }else
        {
          //coutactivite
        }
      }else {
        if(isset($_POST['respoactivite']))
        {
            //responsableactivite
        }
      }


    }
  }

 ?>


 <div class="row">
              <div class="col-12 col-md-6 col-lg-6">
                <div class="card card-primary">
                  <div class="card-header">
                    <h4>Card Header</h4>
                    <div class="card-header-action">
                      <a href="#" class="btn btn-primary">
                        View All
                      </a>
                    </div>
                  </div>
                  <div class="card-body">
                    <p>Write something here</p>
                  </div>
                </div>
                <div class="card card-primary">
                  <div class="card-header">
                    <h4>Button Group</h4>
                    <div class="card-header-action">
                      <div class="btn-group">
                        <a href="#" class="btn btn-primary">Home</a>
                        <a href="#" class="btn btn-primary">Profile</a>
                        <a href="#" class="btn btn-primary">Setting</a>
                      </div>
                    </div>
                  </div>
                  <div class="card-body">
                    <p>Write something here</p>
                  </div>
                </div>
                <div class="card card-success">
                  <div class="card-header">
                    <h4>Input Text</h4>
                    <form class="card-header-form">
                      <input type="text" name="search" class="form-control" placeholder="Search">
                    </form>
                  </div>
                  <div class="card-body">
                    <p>Write something here</p>
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-6 col-lg-6">
                <div class="card card-secondary">
                  <div class="card-header">
                    <h4>Card Header</h4>
                    <div class="card-header-action">
                      <a href="#" class="btn btn-primary">View All</a>
                      <a href="#" class="btn btn-danger">Delete All</a>
                    </div>
                  </div>
                  <div class="card-body">
                    <p>Write something here</p>
                  </div>
                </div>
                <div class="card card-danger">
                  <div class="card-header">
                    <h4>Dropdown Button</h4>
                    <div class="card-header-action">
                      <a href="#" class="btn btn-primary">View All</a>
                      <div class="dropdown">
                        <a href="#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle">Options</a>
                        <div class="dropdown-menu">
                          <a href="#" class="dropdown-item has-icon"><i class="fas fa-eye"></i> View</a>
                          <a href="#" class="dropdown-item has-icon"><i class="far fa-edit"></i> Edit</a>
                          <div class="dropdown-divider"></div>
                          <a href="#" class="dropdown-item has-icon text-danger"><i class="far fa-trash-alt"></i>
                            Delete</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card-body">
                    <p>Write something here</p>
                  </div>
                </div>
                <div class="card card-warning">
                  <div class="card-header">
                    <h4>Input Button</h4>
                    <form class="card-header-form">
                      <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Search">
                        <div class="input-group-btn">
                          <button class="btn btn-primary btn-icon"><i class="fas fa-search"></i></button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="card-body">
                    <p>Write something here</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12 col-md-6 col-lg-6">
                <div class="card">
                  <div class="card-header">
                    <h4>Pagination</h4>
                  </div>
                  <div class="card-body">
                    <nav aria-label="Page navigation example">
                      <ul class="pagination">
                        <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">Next</a></li>
                      </ul>
                    </nav>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header">
                    <h4>Disabled &amp; Active State</h4>
                  </div>
                  <div class="card-body">
                    <nav aria-label="...">
                      <ul class="pagination">
                        <li class="page-item disabled">
                          <a class="page-link" href="#" tabindex="-1">Previous</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item active">
                          <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                          <a class="page-link" href="#">Next</a>
                        </li>
                      </ul>
                    </nav>
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-6 col-lg-6">
                <div class="card">
                  <div class="card-header">
                    <h4>Icon</h4>
                  </div>
                  <div class="card-body">
                    <div class="buttons">
                      <nav aria-label="Page navigation example">
                        <ul class="pagination">
                          <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                              <span aria-hidden="true">&laquo;</span>
                              <span class="sr-only">Previous</span>
                            </a>
                          </li>
                          <li class="page-item"><a class="page-link" href="#">1</a></li>
                          <li class="page-item"><a class="page-link" href="#">2</a></li>
                          <li class="page-item"><a class="page-link" href="#">3</a></li>
                          <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                              <span aria-hidden="true">&raquo;</span>
                              <span class="sr-only">Next</span>
                            </a>
                          </li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header">
                    <h4>Sizing</h4>
                  </div>
                  <div class="card-body">
                    <nav aria-label="...">
                      <ul class="pagination pagination-sm">
                        <li class="page-item disabled">
                          <a class="page-link" href="#" tabindex="-1">1</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                      </ul>
                    </nav>
                    <nav aria-label="...">
                      <ul class="pagination pagination-lg">
                        <li class="page-item disabled">
                          <a class="page-link" href="#" tabindex="-1">1</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                      </ul>
                    </nav>
                  </div>
                </div>
              </div>
            </div>


                            </div>
                        </div>
                    </div>



                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
 <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
 <script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
 <script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   // function classeliste()
   // {
   //   //listeEnseignant.php?$codeEtabAssigner=<?php //echo $codeEtabAssigner?>
   //
   //   var url="listeClasse.php?$idClasse=<?php //echo $idClasse?>& $codeEtabAssigner=<?php //echo $codeEtabAssigner?>";
   //
   //   window.open(url, '_blank');
   // }

   $('#dateactivite').bootstrapMaterialDatePicker
  ({
   date: true,
   shortTime: false,
   time: false,
   format: 'YYYY-MM-DD',
   lang: 'fr',
  cancelText: '<?php echo L::AnnulerBtn ?>',
  okText: 'OK',
  clearText: '<?php echo L::Eraser ?>',
  nowText: '<?php echo L::Now ?>',
  switchOnClick : true

  });

   function searchlibetab()
   {

     var etape=2;

     $.ajax({
              url: '../ajax/school.php',
              type: 'POST',
              async:false,
              data: 'code='+ $("#codeetab").val()+'&etape='+etape,
              dataType: 'text',
              success: function (content, statut) {

                $("#libetab").html("");
                $("#libetab").html(content);

                //nous allons chercher la liste des admins local de cet etablissement

                $.ajax({
                         url: '../ajax/classe.php',
                         type: 'POST',
                         async:false,
                         data: 'code='+ $("#codeetab").val()+'&etape='+etape,
                         dataType: 'text',
                         success: function (response, statut) {


                           $("#classex").html("");
                           $("#classex").html(response);
                         }
                       });

              }

            });

   }
   $(document).ready(function() {

     $('#example5').DataTable( {
         "scrollX": true

     } );

     $("#codeetab").select2();

     $("#libetab").select2();

   });

$("#FormSearch").validate({


       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
     },
     highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
     },
     success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },
  rules:{
    dateactivite:{
      required:function(element){
              return $("#coutactivite").val()=="" && $("#respoactivite").val()=="" && $("#dateactivite").val()=="";
          }
    }
      },
      messages:{
dateactivite:"<?php echo L::PleaserenseignerActivitydate ?>"
      }


});




   </script>
    <!-- end js include path -->
  </body>

</html>
