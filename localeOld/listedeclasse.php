<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Teatcher.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../controller/functions.php');


$codeEtab=$_GET['codeEtab'] ;
$idClasse=$_GET['idClasse'];
$session=$_GET['session'];

$etabs=new Etab();
$student=new Student();
$classe=new Classe();

$libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
$libelleclasse=$classe->getInfosofclassesbyId($idClasse,$session);
$logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
$etablissementType=$etabs->DetermineTypeEtab($codeEtab);


$students=$student->getAllStudentOfClassesId($idClasse,$session);

//nous allons faire nos différents calculs

//le nombre d'eleve de cette classe

$nbclassestudents=$classe->getAllStudentOfThisClassesNb($idClasse,$session);

//le nombre de fille affecté

$nbaffectefilles=$classe->getAllStudentFilleAffectOfThisClassesNb($idClasse,$session);

//nombre de fille non Affecté

$nbnonaffectefilles=$classe->getAllStudentFilleNAffectOfThisClassesNb($idClasse,$session);

//nombre de mec affectés

$nbaffectemecs=$classe->getAllStudentMecAffectOfThisClassesNb($idClasse,$session);

//nombre de mec non affectés

$nbnonaffectemecs=$classe->getAllStudentMecNAffectOfThisClassesNb($idClasse,$session);

//nombre de fille redoublante

$nbredoubfilles=$classe->getAllStudentFilleRedtOfThisClassesNb($idClasse,$session);

//nombre non redoublante fille

$nbnredoubfilles=$classe->getAllStudentFilleNRedtOfThisClassesNb($idClasse,$session);

//nombre de mec redoublant

$nbredoubmecs=$classe->getAllStudentMecRedtOfThisClassesNb($idClasse,$session);

//nombre non redoublant mec

$nbnredoubmecs=$classe->getAllStudentMecNRedtOfThisClassesNb($idClasse,$session);


require('fpdf/fpdf.php');

class PDF extends FPDF
{
function Footer()
{
    // Positionnement à 1,5 cm du bas
    $this->SetY(-15);
    // Police Arial italique 8
    $this->SetFont('Arial','I',8);
    // Numéro et nombre de pages
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    // $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
}

// Tableau simple
function BasicTable($header, $data)
{
    // En-tête
    foreach($header as $col)
        $this->Cell(40,7,$col,1);
    $this->Ln();
    // Données
    foreach($data as $row)
    {
        foreach($row as $col)
            $this->Cell(40,6,$col,1);
        $this->Ln();
    }
}
}

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->SetMargins(3,0);
$pdf->SetFont('Times','B',  16);
$pdf->AddPage();
$pdf->Ln(20);
$pdf->Image("../logo_etab/".$codeEtab."/".$logoEtab,5,5,25,25);
$pdf->Ln(12);
$pdf -> SetX(20);
$pdf-> SetFont('Times','B',  15);
$pdf->Cell(176,5,$libelleEtab,0,0,'C');
$pdf->Ln(8);
$pdf -> SetX(20);
$pdf-> SetFont('Times','',  13);
$pdf->Cell(176,5,'Listes completes  des  eleves ',0,0,'C');
$pdf->Ln(8);
$pdf -> SetX(20);
$pdf-> SetFont('Times','',  11);
$pdf->Cell(176,5,'Annees scolaire : 2019-2020',0,0,'C');
$pdf->Ln(8);
$pdf -> SetX(20);
$pdf-> SetFont('Times','',  11);
$pdf->Cell(176,5,$libelleclasse,0,0,'C');
$pdf->Ln(20);

$pdf->SetFillColor(230,230,0);
$pdf->SetLineWidth(.3);
$pdf->SetFont('Times','B',12);
$pdf->Cell(8,8,'N',1,0,'C');
$pdf->Cell(25,8,'Matricule',1,0,'C');
$pdf->Cell(60,8,'Nom & prenoms',1,0,'C');
$pdf->Cell(14,8,'SEX',1,0,'C');
$pdf->Cell(14,8,'STAT',1,0,'C');
$pdf->Cell(18,8,'RED/NR',1,0,'C');
$pdf->Cell(25,8,'DAT-NAIS',1,0,'C');
$pdf->Cell(30,8,'LIEU-NAIS',1,0,'C');
$pdf->Cell(10,8,'LV2',1,0,'C');
// $pdf->Cell(18,8,'DAT-NAIS',1,0,'C');

$pdf->Ln();
$pdf->SetFont('Times','B',6);
$pdf->SetFillColor(96,96,96);
$i=1;

    foreach ($students as $value):
      $statut=$value->affecter_Id_eleve;
      if($statut==0)
      {
        $libellestat="NAFF";
      }else if($statut==1)
      {
        $libellestat="AFF";
      }
      $pdf->Cell(8,8,$i,1,0,'C');
      $pdf->Cell(25,8,$value->matricule_eleve,1,0,'L');
      $pdf->Cell(60,8,$value->nom_eleve.' '.$value->prenom_eleve,1,0,'L');
      $pdf->Cell(14,8,$value->sexe_eleve,1,0,'C');
      $pdf->Cell(14,8,$libellestat,1,0,'C');
      $pdf->Cell(18,8,'',1,0,'C');
      $pdf->Cell(25,8,date_format(date_create($value->datenais_eleve), "d/m/Y"),1,0,'C');
      $pdf->Cell(30,8,$value->lieunais_eleve,1,0,'C');
      $pdf->Cell(10,8,'',1,0,'C');

      // $pdf->Cell(8,8,'',1,0,'C');

      $pdf->Ln();
$i++;
    endforeach;

    $pdf->Ln();
    $pdf -> SetX(50);
    $pdf->SetFillColor(230,230,0);
    $pdf->SetLineWidth(.3);
    $pdf->SetFont('Times','B',11);
    $pdf->Cell(15,8,'',1,0,'C');
    $pdf->Cell(30,8,'Filles',1,0,'C');
    $pdf->Cell(30,8,'Garcons',1,0,'C');
    $pdf->Cell(30,8,'Total',1,0,'C');
    $pdf->Ln();
    $pdf -> SetX(50);
    $pdf->Cell(15,8,'AFF',1,0,'C');
    $pdf->Cell(30,8,$nbaffectefilles,1,0,'C');
    $pdf->Cell(30,8,$nbaffectemecs,1,0,'C');
    $pdf->Cell(30,8,$nbaffectefilles+$nbaffectemecs,1,0,'C');
    $pdf->Ln();
    $pdf -> SetX(50);
    $pdf->Cell(15,8,'NAFF',1,0,'C');
    $pdf->Cell(30,8,$nbnonaffectefilles,1,0,'C');
    $pdf->Cell(30,8,$nbnonaffectemecs,1,0,'C');
    $pdf->Cell(30,8,$nbnonaffectefilles+$nbnonaffectemecs,1,0,'C');
    $pdf->Ln();
    $pdf -> SetX(50);
    $pdf->Cell(15,8,'RED',1,0,'C');
    $pdf->Cell(30,8,$nbredoubfilles,1,0,'C');
    $pdf->Cell(30,8,$nbredoubmecs,1,0,'C');
    $pdf->Cell(30,8,$nbredoubfilles+$nbredoubmecs,1,0,'C');
    $pdf->Ln();
    $pdf -> SetX(50);
    $pdf->Cell(15,8,'NRED',1,0,'C');
    $pdf->Cell(30,8,$nbnredoubfilles,1,0,'C');
    $pdf->Cell(30,8,$nbnredoubmecs,1,0,'C');
    $pdf->Cell(30,8,$nbnredoubmecs+$nbnredoubfilles,1,0,'C');







$pdf->Output();



 ?>
